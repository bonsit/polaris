This is the guide to get Polaris setup and running quickly. The guide
assumes you are familiar with the UNIX system environment. Windows
users will need to make adjustments where necessary.


## INSTALL POLARIS LIBRARY FILES ##

Install Mercurial (probably already installed on your system, if you use Linux or Mac OSX).

Go to your webserver home directory.

Type:

```shell
hg clone https://bartbons@bitbucket.org/bartbons/polaris
```

Now you have a Polaris folder webserver home directory.


## SETUP POLARIS TEMPLATE DIRECTORIES ##

You will need to change five directories for Polaris to work. The web server PHP user
will need write access to the temporary (template and cache) directories.

We assume the your temporary folder is /var/tmp/ and the web server username is "www".
On other systems the web server username can be different: for example 'nobody' or 'apache'.

```shell
cd /var/tmp/
mkdir cache
mkdir smarty
chown www:www cache
chown www:www smarty
chmod 775 cache
chmod 775 smarty
```

## SETUP POLARIS DATABASE ##

At this moment the database needs to be setup manually.

Create a MySQL user for the Polaris database and give it all permissions.

Start Polaris en when no connection can be made to the database, you will be redirected
to a configuration page. There you can enter the information about the polaris
database (server, port, database, user, pw).

docker-compose exec db /bin/bash
mysql -h127.0.0.1 -uroot -p (no password)
grant all privileges on polaris.* to root@`%` identified by '...';


also for
docker-compose exec db-clients /bin/bash

## SETUP THE CONFIG FILE ##

You can find the Polaris configurations in the file: <polaris>/includes/configuration_default.inc.php
If you want to override a setting, rename or copy the file
<polaris>/configuration.inc.default to <polaris>/configuration.inc.php
and place the setting in that file.

So basically, don't change anything in the file <polaris>/includes/configuration_default.inc.php.
Change settings in your own configuration file, named <polaris>/configuration.inc.php

## TESTING POLARIS

This project is tested with BrowserStack.

We are using Cypress for testing Polaris. To start the Cypress test runner:

> npx cypress open
