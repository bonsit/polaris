<?php

require 'init_tests.php';

use PHPUnit\Framework\TestCase;

class BasePlrActionTest extends TestCase
{
    public function testGetAction()
    {
        global $polaris;

        $owner = $polaris; // Replace with the appropriate owner instance

        // Create an instance of the base_plrAction class
        $action = new base_plrAction($owner);

        // Set the necessary properties for testing
        $action->record = new stdClass();
        $action->record->ACTIONNAME = 'Test Action';
        $action->record->JASPERREPORTNAME = 'test_report.jasper';
        $action->record->ACTIONURL = 'https://example.com/action';
        // Set other required properties for the specific test case

        // Invoke the GetAction method with the desired parameters
        $result = $action->GetAction(1, 'allitems', 'medium', false);

        // Assert that the result matches the expected output
        $this->assertEquals('<button rel="" class="btn btn-medium btn-white jasper needsonerow refresh"  href="app/macs/module/plr_execjasperreport/?action=1&jr=test_report.jasper&amp;firsttime=true" target="_blank">Test&nbsp;Action...</button>', $result);
    }
}
