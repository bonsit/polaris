<?php

/**
 * Set PLR_DIR to the directory where the root of Polaris resides...
 */
if (!defined('PLR_DIR')) define('PLR_DIR', dirname(dirname(__FILE__)).'/public');

/**
 * Set the class autoloader, so it finds the Polaris classes in the right folder
 */
spl_autoload_register(function($class) {
    $class = str_replace('_', DIRECTORY_SEPARATOR, $class);
    // get full name of file containing the required class
    $file = PLR_DIR.'/classes/'.$class.'.class.php';
    // get file if it is readable
    if (is_readable($file)) {
        require $file;
    }
});

require_once PLR_DIR."/vendor/autoload.php";
require_once PLR_DIR."/includes/global.inc.php";
require_once PLR_DIR."/includes/constants.inc.php";
require_once PLR_DIR."/classes/temp_engine/dsg_smarty.class.php";
require_once PLR_DIR."/classes/temp_engine/app_smarty.class.php";
require_once PLR_DIR."/sql/mysql-lang.inc.php";
require_once PLR_DIR."/includes/basefunctions.inc.php";
require_once PLR_DIR."/includes/dbroutines.inc.php";
require_once PLR_DIR."/languages/lang_api.php";

/**
 * Create the Polaris object
 */
$polaris = new base_Polaris();
$polaris->initialize();
