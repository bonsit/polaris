<html>
<head>
<script type="text/javascript" language="javascript" src="/assets/dist/md5.js"></script>
<script type="text/javascript" language="javascript">
function calculateSig() {
    var api_sig = hex_md5(document.getElementById('api_secret').value
    + 'api_call'+document.getElementById('app').value+document.getElementById('const').value
    + 'api_key'+document.getElementById('api_key').value
    + 'api_pwd'+ hex_md5(document.getElementById('password').value));
    document.getElementById('api_sig').value = api_sig;
    var api_call = window.location.href.substr(0,window.location.href.lastIndexOf('/')) + '/services/xhtml/app/'+document.getElementById('app').value+'/const/'+document.getElementById('const').value
    + '/?api_key='+document.getElementById('api_key').value+'&api_sig='+api_sig+'&api_user='+document.getElementById('name').value;
    document.getElementById('api_call').value = api_call;

}
</script>
</head>
<body>
<h1>Bepaal de signature van een Polaris API service call</h1>
<table>
<tr><td>Application (metaname): </td><td><input type="text" id="app" value="" /></td></tr>
<tr><td>Construct (metaname): </td><td><input type="text" id="const" value="" /></td></tr>
<tr><td>API key: </td><td><input type="text" id="api_key" value="" /></td></tr>
<tr><td>API secret: </td><td><input type="text" id="api_secret" value="" /></td></tr>
<tr><td>User name: </td><td><input type="text" id="name" value="" /></td></tr>
<tr><td>User password: </td><td><input type="text" id="password" value="" /></td></tr>
</table>
<input type="button" value="Bereken" onclick="calculateSig()"><br />

API Signature: <input type="text" id="api_sig" value="" size="40" /><br>
API Call: <input type="text" id="api_call" value="" size="140" /><br>

<small>Er wordt geen informatie via het Internet verstuurd. De functie wordt op de client uitgevoerd met Javascript.</small>
</body>
</html>