<?php

require_once "../includes/app_includes.inc.php";
require_once 'adodb.inc.php';

$polaris = new base_Polaris();
$polaris->initialize();

$ok = true;

echo "Polaris is running on server (servername): ".$_SERVER['SERVER_NAME']."<br />";
echo "ipadres: ".$_SERVER['SERVER_ADDR'];
echo "<br /><br />";

/***
* Check if Smarty template folder exists and is writable
**/
if (!file_exists($_CONFIG['smarty_template_folder'])) {
    echo $_CONFIG['smarty_template_folder']." does not exists <br />";
    mkdir($_CONFIG['smarty_template_folder'], 0775);
    echo "Creating '".$_CONFIG['smarty_template_folder']."' <br />";
    $ok = false;
} else {
    echo $_CONFIG['smarty_template_folder']." exists <br />";
}

if (!is_writable($_CONFIG['smarty_template_folder'])) {
    echo $_CONFIG['smarty_template_folder']." is not writable.<br />";
    chmod($_CONFIG['smarty_template_folder'], 0775);
    echo "'".$_CONFIG['smarty_template_folder']."' made writable.<br />";
    $ok = false;
} else {
    echo $_CONFIG['smarty_template_folder']." is writable.<br />";
}

foreach($_PLRINSTANCE as $key => $pi) {
    echo 'Instance '.$key.' ('.$pi['name'].' '.$pi['host'].')';

    $_userdb = ADONewConnection($pi['dbtype']);

    $conn = $_userdb->PConnect($pi['host'], $pi['username'], $pi['password'], 'admin_polaris5');

    if (!$conn) {
        echo " <span style=\"color:red\">connection failed</span>";
        $ok = false;
    } else {
        echo " <span style=\"color:green\">connection succeeded!</span><br />";

        $rs1 = $polaris->dbExecute("
        SELECT L.CLIENTID, L.DATABASEID , L.TABLENAME , L.COLUMNNAME , KEYCOLUMN , DATATYPE , TOTALLENGTH , DECIMALCOUNT , SIGNED , COLUMNCHECK , FORMULA, MASK, REQUIRED , FULLNAME , DESCRIPTION, GROUPID, COLUMNGROUPINDEX , SUPERSHOWCOLUMNNAME , LISTVALUES , SUPERSHOWBOTH , DEFAULTVALUE , POSITION , OPTIONGROUP , COLUMNVISIBLE , DEFAULTCOLUMN, FORMID, PAGEID, LINEID, LINEDESCRIPTION , LINEDESCRIPTION_SHORT, HELPNR, LINETYPE, READONLY, READONLYONVALUE, LISTVALUES, BINARYVALUES , LINEDEFAULTVALUE, LINEREQUIRED, FORMAT, FORMATPARAMS, UNIT, IMAGEDIR, IMAGEURL , POSITIONTYPE, XPOSITION, YPOSITION, VISIBLE,
         VISIBLEINLIST, POSITIONINDEX , PREVIEWURL, ISSELECTFIELD, SELECTSOURCE, ISGROUPFIELD, GROUPSOURCE
         , EVENTS, CSSIDENTIFIER , UPDATECOLUMN, CUTOFFTHRESHOLD, CHECKBOXGROUP, FORCEDEFAULTONUPDATE
         , SUPERCOLUMNNAME, S.SUBSETID, S.LINKEDFORM, S.SUPERFORM , AUTOFOCUS FROM plr_line L
         LEFT JOIN plr_column C ON L.CLIENTID= C.CLIENTID AND L.DATABASEID = C.DATABASEID AND L.TABLENAME = C.TABLENAME AND L.COLUMNNAME = C.COLUMNNAME
         LEFT JOIN plr_subsetitem SI ON C.CLIENTID= SI.CLIENTID AND C.DATABASEID = SI.DATABASEID AND C.TABLENAME = SI.TABLENAME AND C.COLUMNNAME = SI.COLUMNNAME
         LEFT JOIN plr_subset S ON SI.CLIENTID= S.CLIENTID AND SI.DATABASEID = S.DATABASEID AND SI.SUBSETID = S.SUBSETID
         WHERE L.CLIENTID = 12 AND FORMID = 1 AND PAGEID = 1
         GROUP BY FORMID, PAGEID, LINEID
         ORDER BY L.COLUMNGROUPINDEX, L.POSITIONINDEX
        ");

        // SELECT DISTINCT F.CLIENTID,  F.FORMID,  F.FORMNAME,  F.METANAME,  F.HELPNR,  F.PAGECOLUMNCOUNT,  F.WEBPAGEURL,  F.DATABASEID ,  F.TABLENAME,  F.UPDATETABLENAME,  F.FILTER,  F.MODULE,  F.MODULEID,  F.DEFAULTVIEWTYPE,  F.VIEWTYPEPARAMS,  F.SHOWINFRAME,  F.ITEMNAME,  F.WEBPAGENAME ,  F.WEBPAGECONSTRUCT,  F.SHOWRECORDSASCONSTRUCTS,  F.CAPTIONCOLUMNNAME,  F.SORTCOLUMNNAME,  F.SEARCHCOLUMNNAME ,  F.PANELHELPTEXT,  F.PRELOAD,  F.HISTORYTYPE,  F.SELECTVIEW,  F.SELECTCOLUMNNAMES,  F.SELECTSHOWCOLUMNS ,  F.SELECTQUERY,  F.OPTIONS,  F.SENTENCEPATTERN,  BIT_OR(FP.permissiontype) AS PERMISSIONTYPE ,  F.REFRESHFORM,  F.DEFAULTREFRESHINTERVAL,  F.CSSIDENTIFIERCOLUMN,  F.RSSCOLUMNS,  F.RECORDLIMIT ,  F.ROWREADONLYONVALUE ,  T.ISVIEW ,  MD5(CONCAT(F.CLIENTID,  '{$_ENV['POLARIS_HASH']}')) as CLIENTHASH ,  MD5(CONCAT(F.CLIENTID,  '_',  F.FORMID,  '{$_ENV['POLARIS_HASH']}')) as RECORDID  FROM (((plr_form F)  LEFT JOIN plr_formpermission FP ON F.clientid = FP.clientid AND F.formid = FP.formid)  LEFT JOIN plr_table T ON F.clientid = T.clientid AND F.databaseid = T.databaseid AND F.tablename = T.tablename)  WHERE F.CLIENTID = 12 AND FP.USERGROUPID in (1,2) AND F.FORMID = 1
        // $rs = $_userdb->Execute("SELECT count(*) FROM plr_client");
        $rs = $_userdb->SelectLimit("
        SELECT DISTINCT F.CLIENTID,  F.FORMID,  F.FORMNAME,  F.METANAME,  F.HELPNR,  F.PAGECOLUMNCOUNT,  F.WEBPAGEURL,  F.DATABASEID ,  F.TABLENAME,  F.UPDATETABLENAME,  F.FILTER,  F.MODULE,  F.MODULEID,  F.DEFAULTVIEWTYPE,  F.VIEWTYPEPARAMS,  F.SHOWINFRAME,  F.ITEMNAME,  F.WEBPAGENAME ,  F.WEBPAGECONSTRUCT,  F.SHOWRECORDSASCONSTRUCTS,  F.CAPTIONCOLUMNNAME,  F.SORTCOLUMNNAME,  F.SEARCHCOLUMNNAME ,  F.PANELHELPTEXT,  F.PRELOAD,  F.HISTORYTYPE,  F.SELECTVIEW,  F.SELECTCOLUMNNAMES,  F.SELECTSHOWCOLUMNS ,  F.SELECTQUERY,  F.OPTIONS,  F.SENTENCEPATTERN,  BIT_OR(FP.permissiontype) AS PERMISSIONTYPE ,  F.REFRESHFORM,  F.DEFAULTREFRESHINTERVAL,  F.CSSIDENTIFIERCOLUMN,  F.RSSCOLUMNS,  F.RECORDLIMIT ,  F.ROWREADONLYONVALUE ,  T.ISVIEW
        ,  MD5(CONCAT(F.CLIENTID,  '{$_ENV['POLARIS_HASH']}')) as CLIENTHASH
        ,  MD5(CONCAT(F.CLIENTID,  '_',  F.FORMID,  '{$_ENV['POLARIS_HASH']}')) as RECORDID
        FROM (((plr_form F)
        LEFT JOIN plr_formpermission FP ON F.clientid = FP.clientid AND F.formid = FP.formid)  LEFT JOIN plr_table T ON F.clientid = T.clientid AND F.databaseid = T.databaseid AND F.tablename = T.tablename)  WHERE F.CLIENTID = 12 AND FP.USERGROUPID in (1,2) AND F.FORMID = 1
        ");
        $row = $rs->GetAll();
        $count = count($row);
        echo "Number of clients: ".$count."<br />";
    }
    echo "<br />";
}

$_userdb = ADONewConnection('mysqli');

$_password = $_GET['userdbpw'];

if (isset($_password)) {

    $conn = $_userdb->PConnect('db-clients', 'plr_clients', $_password, 'admin_maxia');

    if (!$conn) {
        echo " <span style=\"color:red\">Client connection failed</span>";
        $ok = false;
    } else {
        echo " <span style=\"color:green\">Client connection succeeded!</span><br />";

        $rs = $_userdb->Execute("SELECT count(*) FROM fb_meldingen");
        $row = $rs->GetAll();
        $count = $row[0][0];
        echo "Number of fb_meldingen: ".$count."<br />";
    }
    echo "<br />";

    if (!isset($_CONFIG['currentinstance'])) {
        echo '$_CONFIG[\'currentinstance\'] is not set (includes/configuration.inc.php)<br />';
    } else {
        echo 'current database instance is '.$_CONFIG['currentinstance'].'<br />';
    }
} else {
    echo "No userpassword (?userdbpw=&lt;password&gt;) is given... skipping db-clients checks.<br>";
}

$worker = new base_QueueWorker();
$queue = $polaris->GetQueue();

echo "Number of beanstalk jobs in default tube: ".$queue->statsTube('default')['current-jobs-ready']."<br>";;

global $_GVARS;
global $scramble;

$_linkhash = hashstr($id.$scramble);
$values['ID'] = $id;
$values['FORMHASH'] = 'e4233030af7fae6e0e965016d6c861e4'; // PZN_UITNODIGING
$values['LINK'] = $_GVARS['serverroot']."/createaccount/{$values['FORMHASH']}/{$_linkhash}/";

echo "Sending an item to the Beanstalk queue<br>";
$polaris->AddToQueue('send_invite_mail', array('email' => $email, 'insertValues' => $values, 'ID' => $id));

echo "Number of beanstalk jobs in default tube: ".$queue->statsTube('default')['current-jobs-ready']."<br>";

while($job = $queue->reserve(2)) {

    if ($job) {
        $received = json_decode($job->getData(), true);
        $action = $received['action'];
        if(isset($received['data'])) {
            $data = $received['data'];
        } else {
            $data = array();
        }

        echo "Received a $action (" . current($data) . ") ...<br>";
        if(method_exists($worker, $action)) {
            $queue->delete($job);
        } else {
            echo "action not found\n";
            $queue->bury($job);
        }
    }

}

$_queueCount = $queue->statsTube('default')['current-jobs-ready'];
echo "Number of beanstalk jobs in default tube: {$_queueCount}<br>";

if ($_queueCount !== 0) {
    $ok = false;
}


if ($ok)
    echo "<br />Everything is okay!<br />";

