#!/bin/bash

default_github_username=bartb
default_project_name='ontwikkel' # kan 'test', 'ontwikkel' of iets anders zijn

read -p "Wat is de projectnaam van deze Polaris installatie? [$default_project_name]: " -e tpp
if [ -n "$tpp" ]
then
  polaris_project_name="$tpp"
else
  polaris_project_name="$default_project_name"
fi

polaris_dir=/var/www/html/polaris_${polaris_project_name}


read -p "Wat is uw github gebruikersnaam [$default_github_username]: " -e tun
if [ -n "$tun" ]
then
  github_username="$tun"
else
  github_username="$default_github_username"
fi

read -p "Wat is uw github wachtwoord: " -e github_passwd


color='\e[1;37;44m'
endColor='\e[0m'

echo -e "$color\n======================== SYSTEM INSTALL / UPDATE ============================$endColor"

echo -e "$color##### SO UPDATE #####$endColor" 
#yum clean all
#yum -y update

echo -e "$color##### Autostart Apache httpd #####$endColor" 
/sbin/chkconfig httpd on
/sbin/chkconfig --list httpd

echo -e "$color##### INSTALL PHP #####$endColor"  
yum -y groupinstall "Development Tools" --exclude=dogtail
yum -y install openssl-devel php php-common php-gd php-mcrypt php-pear php-pecl-memcache php-mhash php-mysql php-xml php-mbstring php-soap php-tidy php-devel zlib zlib-devel

echo -e "$color##### INSTALL PHP eaccelerator #####$endColor"  

eaccel=/usr/lib/php/modules/eaccelerator.so
if [ ! -f $eaccel ];
then
    wget http://bart.eaccelerator.net/source/0.9.6/eaccelerator-0.9.6.tar.bz2
    bunzip2 eaccelerator-0.9.6.tar.bz2
    tar -xvf eaccelerator-0.9.6.tar
    cd eaccelerator-0.9.6
    phpize
    ./configure
    make
    make install
    cd ..
    rm -f eaccelerator-0.9.6.*
    rm -Rf eaccelerator-0.9.6
    
    mkdir -p /var/cache/eaccelerator
    chmod 0777 /var/cache/eaccelerator
    
    echo "
    extension=\"eaccelerator.so\"
    eaccelerator.shm_size=\"0\"
    eaccelerator.cache_dir=\"/var/cache/eaccelerator\"
    eaccelerator.enable=\"1\"
    eaccelerator.optimizer=\"1\"
    eaccelerator.check_mtime=\"1\"
    eaccelerator.debug=\"0\"
    eaccelerator.filter=\"\"
    eaccelerator.shm_max=\"0\"
    eaccelerator.shm_ttl=\"0\"
    eaccelerator.shm_prune_period=\"0\"
    eaccelerator.shm_only=\"0\"
    eaccelerator.compress=\"1\"
    eaccelerator.compress_level=\"9\"
    " > /etc/php.d/eaccelerator.ini
fi

pear install pecl/json
echo "extension=json.so" > /etc/php.d/json.ini

echo -e "$color##### INSTALL GIT #####$endColor"  
yum -y install curl-devel expat
rpm -ivh http://repo.webtatic.com/yum/centos/5/`uname -i`/webtatic-release-5-0.noarch.rpm
yum -y install --enablerepo=webtatic git-all


echo -e "$color\n============================== MYSQL CONFIG =================================$endColor"

echo -e "$color##### Install mysqlserver  #####$endColor"
yum -y install mysql-server
echo "lower_case_table_names=0" >> /etc/my.cnf

echo -e "$color##### Start mysqld  #####$endColor"
/etc/init.d/mysqld start


mysql_username='root'
mysql_passwd=''
plr_user='nhadev'
plr_passwd='nha2009'
mysql -u$mysql_username --password=$mysql_passwd -e "CREATE DATABASE polaris_$polaris_project_name;"
mysql -u$mysql_username --password=$mysql_passwd -e "GRANT ALL ON polaris_$polaris_project_name.* TO \"$plr_user\"@'localhost' IDENTIFIED BY \"$plr_passwd\";"
mysql -u$mysql_username --password=$mysql_passwd -e "GRANT ALL ON polaris_$polaris_project_name.* TO \"$plr_user\"@'%' IDENTIFIED BY \"$plr_passwd\";"

# get the mysql polaris database

read -p "Wilt u de database polaris_$polaris_project_name inlezen/overschrijven [j/N]? : " -e overwritedb
if [ "$overwritedb" == 'j' ]
then
    cd ~
    wget http://github.com/bartb/polaris/raw/master/admin/polaris_nha_2010-06-11.sql.zip -O ~/polaris_mysql_db.sql.zip
    unzip polaris_mysql_db.sql.zip -d ~
    mv ~/polaris_nha_*.sql ~/polaris_mysql_db.sql
    mysql -u$plr_user --password=$plr_passwd --default_character_set=utf8 polaris_$polaris_project_name < ~/polaris_mysql_db.sql
    
    rm ~/polaris_mysql_db.sql*
fi


echo -e "$color\n============================== POLARIS INSTALL/CONFIG =================================$endColor"

# Create caching directories
mkdir -p /var/tmp/polaris/{smarty,cache}
chown -R apache /var/tmp/polaris
chmod -R 775 /var/tmp/polaris

# Get the (latest) Polaris source code from Github: readonly version!!
#git clone https://$github_username:$github_passwd@github.com/bartb/polaris.git $polaris_dir

git clone git://github.com/nha/polaris.git $polaris_dir
git checkout master

# Get the NHA Polaris configuration file
wget http://github.com/bartb/polaris/raw/master/admin/configuration_nha.inc.php -O $polaris_dir/configuration.inc.php

rm -f /etc/php.ini.bck
cp /etc/php.ini /etc/php.ini.bck

# php.ini: Set auto compression to 'On'
sed "/^zlib.output_compression = Off.*/ s/^zlib.output_compression = Off/zlib.output_compression = On/" /etc/php.ini > tmp
cat tmp > /etc/php.ini

# php.ini: Set gc_maxlifetime 28800
sed "/^session.gc_maxlifetime = 1440.*/ s/^session.gc_maxlifetime = 1440/session.gc_maxlifetime = 28800/" /etc/php.ini > tmp
cat tmp > /etc/php.ini



# create backup of httpd.conf
rm -f /etc/httpd/conf/httpd.conf.bck
cp /etc/httpd/conf/httpd.conf /etc/httpd/conf/httpd.conf.bck

# php.ini: Change DirectoryIndex
sed "/^DirectoryIndex index.html index.html.var.*/ s/^DirectoryIndex index.html index.html.var/DirectoryIndex index.php index.html index.html.var/" /etc/httpd/conf/httpd.conf > tmp
cat tmp > /etc/httpd/conf/httpd.conf

echo "
<Directory \"/var/www/html\">
    AllowOverride All
</Directory>
" >> /etc/httpd/conf/httpd.conf

echo -e "$color\n============================== ORACLE CLIENT INSTALLATION =================================$endColor"

# tijdelijk Security Enhanced Linux uitzetten
setenforce 0

# tijdelijke http server uitzetten
service httpd stop

# download the instantclient rpm's 
wget http://github.com/downloads/nha/polaris/oracle-instantclient-basic-10.2.0.3-1.i386.rpm
wget http://github.com/downloads/nha/polaris/oracle-instantclient-devel-10.2.0.3-1.i386.rpm
wget http://github.com/downloads/nha/polaris/oracle-instantclient-sqlplus-10.2.0.3-1.i386.rpm

rpm -Uvh ~/oracle-instantclient-basic-10.2.0.3-1.i386.rpm
rpm -Uvh ~/oracle-instantclient-devel-10.2.0.3-1.i386.rpm
rpm -Uvh ~/oracle-instantclient-sqlplus-10.2.0.3-1.i386.rpm

rm -Rf ~/oracle-instantclient*

echo /usr/lib/oracle/10.2.0.3/client/lib/ > /etc/ld.so.conf.d/oracle_client.conf

count=`egrep -ic "ORACLE_HOME" ~/.bash_profile`
if [ $count -eq 0 ]
then
    echo "
    export ORACLE_HOME=/usr/lib/oracle/10.2.0.3/client     
    export LD_LIBRARY_PATH=$ORACLE_HOME/lib
    
    PATH=$PATH:$ORACLE_HOME/bin:$LD_LIBRARY_PATH
    " >> ~/.bash_profile
fi

yum install prelink

## handmatig de execstack excluden van php5 en oracle. Je kunt ook 
execstack -c /usr/lib/httpd/modules/libphp5.so /usr/lib/libssl.so /usr/lib/oracle/10.2.0.3/client/lib/libclntsh.so.10.1 /usr/lib/oracle/10.2.0.3/client/lib/libnnz10.so /usr/lib/libcrypto.so /usr/lib/libcurl.so
chcon -R system_u:object_r:httpd_log_t /var/log/httpd/
chcon -R system_u:object_r:httpd_tmp_t /var/tmp
chcon -t textrel_shlib_t /usr/lib/httpd/modules/libphp5.so /usr/lib/oracle/10.2.0.3/client/lib/libclntsh.so.10.1 /usr/lib/oracle/10.2.0.3/client/lib/libnnz10.so

# Security Enhanced Linux weer aanzetten
# EVEN NIET, BOVENSTAANDE WERKT NIET VOOR OraClient 10.2??
# setenforce 1

echo -e "$color##### Download OCI PECL module  #####$endColor"
wget http://pecl.php.net/get/oci8-1.4.1.tgz
tar -xzvf oci8-1.4.1.tgz
cd oci8-1.4.1
echo -e "$color##### Install OCI PECL module  #####$endColor"
phpize
./configure -with-oci8=shared,instantclient,/usr/lib/oracle/10.2.0.3/client/lib/
make
make install
cd ..
rm -Rf oci8-1-41*
echo extension=oci8.so > /etc/php.d/oci8.ini

## SQLPLUS CONNECT: sqlplus <user>/<password>@172.27.0.7/nhatest.panningen.nha.nl


echo -e "$color\n============================== RESTART HTTPD WERSERVICE =================================$endColor"

service httpd start

