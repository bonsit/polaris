<?php
require_once('../includes/app_includes.inc.php');

if ($_POST) {
    /**
     * Create the Polaris object
     */
    $polaris = new base_Polaris();
    $polaris->initialize();
    $returnpage = $polaris->processWebForm($_POST);
    header("Location: $returnpage");
}
