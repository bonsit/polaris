<?php
# Polaris - a weblapplication generator
# Copyright (C) 2000 - 2008  Debster - Bart Bons
# This is the 'KennisKunde' module to create IGD.

# this module is accessable as a php class
# and through query parameters (via kk.php):
#   plrmodeler.php?show=schema&db=<...>
#   >> result: Kenniskunde schema

error_reporting(E_ALL & ~E_DEPRECATED & ~E_STRICT & ~E_WARNING);

include_once('includes/dsg_includes.inc.php');
include_once('includes/dbtypes.inc.php');

/**
 * Create the Polaris object
 */
$polaris = new base_Polaris();
$polaris->initialize();

/**
 * Create the KennisKunde object
 */
$modeler = new base_plrModeler($polaris);

$domainid = $_GET['dm'];
$showentities = $_GET['show'] == 'erd';
$skipprefix = $_GET['skipprefix'] != '' ? $_GET['skipprefix'] : false;
$currenttable = isset($_GET['ftd'])?$_GET['ftd']:false;

$smarty = new tempengine_dsgSmarty( $langid );
$smarty->assign( 'currentdbmenuitem', 'db_kk' );
$smarty->assign( 'domainhash', $_GET['db'] );
$smarty->assign( 'ftd', $_GET['ftd'] );
$smarty->assign( 'onlyDisplayFTD', !(isset($_GET['debug'])) );

$smarty->assign( 'showModelerScripts', true );
$schema = $modeler->getSchema($domainid, $showentities, $skipprefix);
$smarty->assign( 'schema', $schema);
$smarty->assign( 'currenttable', $modeler->getFactType($domainid, $currenttable));
$smarty->display( 'dsg_modeler.tpl.php' );
