<?php
require_once '../includes/app_includes.inc.php';
require('_shared/module.plr_maxia_vragenlijst.php');

error_reporting(E_ALL & ~E_DEPRECATED & ~E_STRICT & ~E_WARNING);

class GezondheidVragenlijst
{
    private $polaris;
    private $modVragenLijst;
    private $vragenlijstId;
    private $client;
    private $language;

    public function __construct()
    {
        $this->polaris = new base_Polaris();
        $this->polaris->initialize();
    }

    public function run()
    {
        $this->setClient();
        $this->setVragenlijstId();
        $this->setLanguage();

        if ($_POST) {
            $this->processVragenlijst();
        } else {
            $this->showVragenlijst();
        }
    }

    private function setVragenlijstId()
    {
        $this->vragenlijstId = isset($_REQUEST['id']) ? str_replace(' ', '+', $_REQUEST['id']) : null;
        if (!isset($this->vragenlijstId)) {
            echo 'Vragenlijst parameter is required';
            exit;
        }
    }

    private function setClient()
    {
        $this->polaris->plrClient = new base_plrClient($this->polaris, $this->polaris->GetCurrentClient($_GET['client']));
        if (!isset($this->polaris->plrClient)) {
            echo 'Client not found';
            exit;
        }
    }

    private function setLanguage()
    {
        $this->language = $_POST['lang'] ?? $_GET['lang'] ?? 'NL';
    }

    private function createModuleObject() {
        $moduleid = 1;
        $module = 'plr_maxia_vragenlijst';
        $this->modVragenLijst = new ModulePLR_Maxia_Vragenlijst($moduleid, $module);
        $this->modVragenLijst->client = $this->polaris->plrClient;
        $this->modVragenLijst->database = new base_plrDatabase($this->polaris, [$this->polaris->plrClient->record->CLIENTID, 1]);
        $this->modVragenLijst->database->LoadRecord();
        $this->modVragenLijst->database->connectUserDB();
    }

    private function getPositieveGezondheid($hash_id) {
        $_sql = "
        SELECT
            POSITIEVEGEZONDHEID_ID,
            VERSIE,
            STATUS_ID,
            DEELNEMER_ID,
            SOORTVRAGENLIJST
        FROM
            vw_do_positievegezondheid_alerting
        WHERE
            __DELETED = 0
            AND MESSAGEHASH = ?
        ";
        $row = $this->modVragenLijst->database->userdb->GetRow($_sql, [$hash_id]);
        return $row;
    }

    private function processVragenlijst()
    {
        $this->createModuleObject();

        $_posgezondheid = $this->getPositieveGezondheid($this->vragenlijstId);
        if (!$_posgezondheid) {
            $this->modVragenLijst->DisplayResponse('De vragenlijst niet gevonden');
            return;
        }

        if ($_posgezondheid['STATUS_ID'] != 2) {
            $this->modVragenLijst->DisplayResponse('De vragenlijst is al ingevuld.');
            return;
        }

        $this->modVragenLijst->ProcessAntwoorden($_POST, $_posgezondheid);

        $averages = $this->calculateAverages();
        $result = $this->updatePositieveGezondheid($_posgezondheid, $averages);

        if ($result) {
            $this->showResults($averages);
        } else {
            $this->modVragenLijst->DisplayResponse("Er is een fout opgetreden bij het opslaan van de resultaten.");
        }
    }

    private function calculateAverages()
    {
        $dimensions = [
            1 => 'lichaamsfuncties',
            2 => 'mentaal_welbevinden',
            3 => 'zingeving',
            4 => 'kwaliteit_van_leven',
            5 => 'sociale_participatie',
            6 => 'dagelijks_functioneren'
        ];

        $averages = [];
        foreach ($dimensions as $dim_id => $dim_name) {
            $sum = 0;
            $count = 0;
            foreach ($_POST as $key => $value) {
                if (strpos($key, "vraag_{$dim_id}_") === 0) {
                    $sum += intval($value);
                    $count++;
                }
            }
            $averages[$dim_name] = $count > 0 ? round($sum / $count) : null;
        }

        return $averages;
    }

    private function updatePositieveGezondheid($_posgezondheid, $averages)
    {
        $sql = "UPDATE do_positievegezondheid SET
            lichaamsfuncties = ?,
            mentaal_welbevinden = ?,
            zingeving = ?,
            kwaliteit_van_leven = ?,
            sociale_participatie = ?,
            dagelijks_functioneren = ?,
            status = 1,
            ingevuld_door = 1 -- Deelnemer zelf
            WHERE positievegezondheid_id = ?
            AND client_hash = ?";

        $params = array_merge(
            array_values($averages),
            [$_posgezondheid['POSITIEVEGEZONDHEID_ID'], $this->polaris->plrClient->record->CLIENTHASHID]
        );

        return $this->modVragenLijst->database->userdb->Execute($sql, $params);
    }

    private function showResults($averages)
    {
        $_message = "<h3>Resultaten</h3>";
        foreach ($averages as $dim_name => $average) {
            $_message .= "<p>" . ucfirst(str_replace('_', ' ', $dim_name)) . ": " . $average . "</p>";
        }
        $_message .= "<br/><br/><small>U kunt deze pagina sluiten.</small>";
        $this->modVragenLijst->DisplayResponse($_message);
    }

    private function showVragenlijst()
    {
        $this->createModuleObject();

        $_posgezondheid = $this->GetPositieveGezondheid($this->vragenlijstId);
        if ($_posgezondheid) {
            if ($_posgezondheid['STATUS_ID'] == 2) {
                if ($_posgezondheid['SOORTVRAGENLIJST'] == 1) {
                    $this->modVragenLijst->ShowGeneriekeVragenlijst($this->polaris->plrClient->record->CUSTOMCSS, $_posgezondheid['VERSIE'], $_posgezondheid['DEELNEMER_ID'], $this->language);
                } else {
                    $_aantalVragen = 2;
                    $this->modVragenLijst->ShowWillekeurigeVragenlijst($this->polaris->plrClient->record->CUSTOMCSS, $_posgezondheid['VERSIE'], $_posgezondheid['DEELNEMER_ID'], $_aantalVragen, $this->language);
                }
            } else {
                if ($this->language == 'NL') {
                    $_message = '<h3>Bedankt</h3>';
                    $_message .= 'U heeft deze meting al ingevuld. <br>Wilt u een nieuwe meting invullen? Vraag dan een nieuwe uitnodiging aan.';
                } else {
                    $_message = '<h3>Thank you</h3>';
                    $_message .= 'You have already filled in this measurement. Do you want to fill in a new positive health measurement? Then request a new invitation.';
                }

                $this->modVragenLijst->DisplayResponse($_message);
            }
        } else {
            $this->modVragenLijst->DisplayResponse('De vragenlijst niet gevonden');
        }
    }
}

// Run the application
$app = new GezondheidVragenlijst();
$app->run();