<?php
require_once '../includes/app_includes.inc.php';
require('_shared/module.plr_maxia_externereservering.php');

error_reporting(E_ALL & ~E_DEPRECATED & ~E_STRICT & ~E_WARNING);

/**
 * Create the Polaris object
 */
$polaris = new base_Polaris();
$polaris->initialize();

$polaris->plrClient = new base_plrClient($polaris, $polaris->GetCurrentClient());

$moduleid = 1;
$module = 'plr_maxia_externereservering';
$ext_res = new ModulePLR_Maxia_ExterneReserveringen($moduleid, $module);
$ext_res->client = $polaris->plrClient;
$ext_res->database = new base_plrDatabase($polaris, array($polaris->plrClient->record->CLIENTID, 1));
$ext_res->database->LoadRecord();
$ext_res->database->connectUserDB();
$ext_res->ShowReservering($polaris->plrClient->record->CUSTOMCSS);
