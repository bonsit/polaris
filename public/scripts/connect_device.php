<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Device Initialization</title>
    <style>
        body {
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            height: 100vh;
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
        }
        h1 {
            color: #333;
        }
        #qrcode {
            margin-top: 20px;
        }
    </style>
</head>
<body>
    <h1>Initialiseer uw apparaat</h1>
    <p>Scan the QR code below to initialize your device to the web application.</p>
    <div id="qrcode">
        <?php
        require_once '../includes/app_includes.inc.php';

        use chillerlan\QRCode\QRCode;
        use chillerlan\QRCode\QROptions;

        $url = 'http://main.maxia.local:8080/app/facility/'; // Replace with your web application URL

        $options = new QROptions([
            'outputType' => QRCode::OUTPUT_IMAGE_PNG,
            'eccLevel' => QRCode::ECC_L,
        ]);

        $qr = new QRCode($options);
        $qrcode = $qr->render($url);

        echo '<img src="' . $qrcode . '" alt="QR Code">';

        ?>
    </div>

</body>
</html>
