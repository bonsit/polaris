<?php
require_once('../includes/app_includes.inc.php');
require_once('../includes/miscfunc.inc.php');

/**
Format calls:
http://polarisnow.com/services/php/geo/postcodes/nl/6225/35/?output=php
http://polarisnow.com/services/php/geo/postcodes/<land>/<postcodecijfers>/<radius in km>/?output=<{json|csv|php|xml}|default:json>

http://polarisnow.com/services/php/geo/adres/nl/6225CA/13/?output=json
http://polarisnow.com/services/php/geo/adres/<land>/<postcode>/<huisnummer>/?output=<{json|csv|php|xml}|default:json>
*/

/**
Create the Polaris object
*/
$polaris = new base_Polaris();
$polaris->initialize();

$func = $_GET['func'];
$land = $_GET['land'];
$postcode = strtoupper($_GET['postcode']);
$format = isset($_GET['output'])?$_GET['output']:'json';
$filter = $_GET['filter'];
switch ($func) {
    case 'postcodes':
        $radius = $_GET['radius']; // in KM

        $_sqlPostcode2lanlon = "SELECT latitude, longitude FROM plr_geocodes WHERE land = ? AND postcode = ?";
        $_sqlPostcodes = "SELECT postcode, land, plaats FROM plr_geocodes WHERE latitude between ? and ? AND longitude between ? and ? " ;
        // filter even weggehaald... waarom was dit?

        $_rec = $polaris->instance->CacheGetRow($_sqlPostcode2lanlon, array($land, $postcode));
        list($lat1, $lat2, $lon1, $lon2) = getPlatteBox($_rec['latitude'], $_rec['longitude'], $radius);
        $polaris->instance->SetFetchMode(ADODB_FETCH_ASSOC);
        try {
            $cur = $polaris->instance->Execute($_sqlPostcodes, array($lat1, $lat2, $lon1, $lon2));
            echo $polaris->rs2format($cur, $format, $_GET['subformat']);
        } catch (Exception $e) {
            echo "Wrong parameters.";
        }

        break;
    case 'adres':
        $huisnummer = $_GET['radius'];
        // $polaris->instance->debug=true;

        $_sqlAdres = 'SELECT STRAAT, PLAATS FROM plr_postcodetabel
        where postcode = ? and CAST(huisnrvan AS DECIMAL) < ? and CAST(huisnrtm AS DECIMAL) > ?';
        try {
            $_rec = $polaris->instance->CacheGetRow($_sqlAdres, array($postcode, $huisnummer, $huisnummer));
            echo $polaris->rs2format($_rec, $format, $_GET['subformat']);
        } catch (Exception $e) {
            echo "Wrong parameters.";
        }
    default:
        break;
}
