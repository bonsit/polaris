<?php
require_once('includes/global.inc.php');
require_once('includes/constants.inc.php');
require_once('sql/mysql-lang.inc.php');
require_once('classes/base/polaris.class.php');

/**
 * Create the Polaris object
 */
$polaris = new Polaris();
$polaris->initialize();

if ( isset($_GET['a']) ) {
    $accounthash = $_GET['a'];

    $polaris->instance->Execute($_sqlConfirmAccount, array($accounthash) ) or die ('$_sqlConfirmAccount failed');
    if ( isset($_GET['page']) ) {
        header( 'location: '. urldecode($_GET['page']) );
    } else {
        header( 'location: '. $_SERVER['REQUEST_URI'] );
    }
}
