<?php
require_once '../includes/app_includes.inc.php';
require_once '../includes/mainactions.inc.php';
/**
 * Create the Polaris object
 */
$polaris = new base_Polaris();
$polaris->initialize();

$_event = isset($_GET['method'])?$_GET['method']:FALSE;
if (!$_event) $_event = isset($_REQUEST['_hdnAction'])?$_REQUEST['_hdnAction']:FALSE;

/**
 * Dispatch the event to the right event handler (if not handled by the custom module)
 */
$_processedByModule = (isset($_REQUEST['_hdnProcessedByModule']) and ($_REQUEST['_hdnProcessedByModule'] == 'true'));

if (isset($_event) and $_event) {
    try {
        /***
            handle the event
        ***/
        if ($_processedByModule) {
            $_form = new base_plrForm($polaris);
            $_form->LoadRecordHashed($_REQUEST['_hdnForm']);
            $_form->LoadDatabaseObject();
            if (isset($_form->database)) {
                $_form->database->connectUserDB();
            }
            $_module = $polaris->plrClient->GetModule($_form->record->MODULE, $_form->record->MODULEID, $_form->record->VIEWTYPEPARAMS);
            $_module->form = $_form;
            $_module->userdatabase = $_form->database->userdb;

            $response = $_module->Process($_event);
        } else {
            $disp = new controllers_dispatcherPolarisAjax($_event);
            $response = $disp->processEvent();
        }
        if (!$_processedByModule) {
            if ($_GET['output'] == 'json') {
                $_result = isset($response['result']) ? $response['result'] : ($response === TRUE);
                $_new_values = isset($response['new_values']) ? $response['new_values'] : "";
                $resultArray = Array('result'=>$_result, 'query'=>$_SERVER['REQUEST_URI'], 'error'=>FALSE, 'new_values'=>$_new_values);
                echo json_encode($resultArray);
            } elseif ($_GET['output'] == 'xhtml') {
                echo $response;
            }
        }

    } catch (Exception $E) {
        $detailedmessage = $E->getMessage()."\r\n".parse_backtrace(debug_backtrace());
        $resultArray = Array('result'=>FALSE, 'query'=>$_SERVER['REQUEST_URI'], 'error'=>$polaris->getPrettyErrorMessage($E->GetCode(), $E->getMessage()), 'detailed_error'=>$detailedmessage);
        echo json_encode($resultArray);
    }

} else {
    if ($_GET['output'] == 'json') {
       echo json_encode(array('result'=>FALSE));
    } elseif ($_GET['output'] == 'xhtml') {
        echo '';
    }
}
