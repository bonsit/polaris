<?php
require_once '../includes/app_includes.inc.php';
require '_shared/module.plr_maxia_ideeenbus.php';

error_reporting(E_ALL & ~E_DEPRECATED & ~E_STRICT & ~E_WARNING);

class IdeeenBus
{
    private $polaris;
    private $modIdeeenBus;
    private $qrCodeID;
    private $client;
    private $language;

    public function __construct()
    {
        $this->polaris = new base_Polaris();
        $this->polaris->initialize();
    }

    public function run()
    {
        $this->setClient();

        if ($_POST) {
            $this->processIdeeenbusForm();
        } else {
            $this->showIdeeenBusForm();
        }
    }

    private function setClient()
    {
        $this->polaris->plrClient = new base_plrClient($this->polaris, $this->polaris->GetCurrentClient($_GET['client']));
        if (!isset($this->polaris->plrClient)) {
            echo 'Client not found';
            exit;
        }
    }

    private function createModuleObject() {
        $moduleid = 1;
        $module = 'plr_maxia_ideeenbus';
        $this->modIdeeenBus = new ModulePLR_Maxia_Ideeenbus($moduleid, $module);
        $this->modIdeeenBus->polaris = $this->polaris;
        $this->modIdeeenBus->client = $this->polaris->plrClient;
        $this->modIdeeenBus->userdatabase = new base_plrDatabase($this->polaris, [$this->polaris->plrClient->record->CLIENTID, 1]);
        $this->modIdeeenBus->userdatabase->LoadRecord();
        $this->modIdeeenBus->userdatabase->connectUserDB();
    }

    private function processIdeeenbusForm()
    {
        $this->createModuleObject();

        $_result = $this->modIdeeenBus->ProcessRequest(event: 'ideeenbusform');
        pr($_result);
        if (isset($_result) and $_result['success'] == true) {
            header('Location: /ideeenbus/bedankt/');
            exit;
        }

    }

    private function showIdeeenBusForm()
    {
        $this->createModuleObject();
        if (isset($_GET['id']) and $_GET['id'] == 'bedankt') {
            $this->modIdeeenBus->ShowBedanktBoodschap();
        } else {
            $this->modIdeeenBus->ShowIdeeenBusForm($_GET['id']);
        }
    }
}

// Run the application
$app = new IdeeenBus();
$app->run();