<?php
include_once('includes/app_includes.inc.php');
include_once('classes/base/polaris.class.php');
include_once('classes/base/plr_client.class.php');
//header("Content-Type: text/xml");

/**
 * Create the Polaris object
 */
$polaris = new Polaris();
$polaris->initialize();

$search = $_GET['q'];
$appmeta = $_GET['app'];
$clientid = $_SESSION['clientid'];

$apprs = $polaris->instance->GetAll($_sqlApplicationMeta, array($clientid, $appmeta));

$appid = $apprs[0]['RECORDID'];
$groupvalues = $polaris->GetGroupValues($_SESSION['userid']).'@!integer';

$_sqlLiveSearchConstructs = str_replace('#subselect#', $groupvalues, $_sqlLiveSearchConstructs);
$params = array(
  $appid,
  '%'.$search.'%',
  '%'.$search.'%'
 );
$func = $polaris->dbGetAll($_sqlLiveSearchConstructs, $params);

$polaris->plrClient = new plrClient($polaris);

$currentApp =& $polaris->plrClient->LoadApplication($appid);
$currentApp->LoadConstructs();

if ( is_array($currentApp->constructs) )
  foreach($currentApp->constructs as $construct) {
    if ( isset($construct->masterform->record->DATABASEID) ) {
      $construct->masterform->database->connectUserDB();
      if ($construct->masterform->database->userdb) {
        $sql = $construct->masterform->MakeSQL(false, true, $search);
        $rs = $construct->masterform->database->userdb->Execute($sql);
        if ($rs) {
            if(isset($rs->fields['recordcount']))
                $count = $rs->fields[0];
            else
                $count = $rs->RecordCount();
            if ($count > 0)
                $items[] = array('constructid' => $construct->record->CONSTRUCTID, 'metaname' => $construct->record->METANAME, 'constructname' => $construct->record->CONSTRUCTNAME , 'recordcount' => $count);
        }
      }
    }
  }

$polaris->tpl->assign('appmeta', $appmeta);
$polaris->tpl->assign('functions', $func);
$polaris->tpl->assign('items', $items);
$search = str_replace('+', '%2B', $_GET['q']);
$polaris->tpl->assign('searchvalue', $search);
echo $polaris->tpl->fetch('livesearch.tpl.php');
?>
