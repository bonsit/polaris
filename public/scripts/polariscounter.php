<?php
require_once '../includes/app_includes.inc.php';

/**
 * Create the Polaris object
 */
$polaris = new base_Polaris();
$polaris->initialize();
$polaris->timer->startTimer('page');
$cons = $polaris->LoadAllRecordCountConstructs();
$debug = isset($_GET['debug']);

foreach($cons as $key => $construct) {
    if ($construct->record !== false && !empty($construct->record->BADGEUPDATEPATTERN)) {
        $cron = new Cron\CronExpression($construct->record->BADGEUPDATEPATTERN);
        if ($debug) {
            echo "Debug: CLIENT {$construct->record->CLIENTID} APP {$construct->record->APPLICATIONID} CONSTRUCT {$construct->record->CONSTRUCTID} \r\n";
            echo "Debug: {$construct->record->LASTRECORDCOUNT} - {$construct->record->BADGEUPDATEPATTERN} - \r\n";
            $_isDue = json_encode($cron->isDue());
            echo "Debug: IsDue: {$_isDue}\r\n";
            echo "Debug: PreviousRunDate: {$cron->getPreviousRunDate()->format('Y-m-d H:i:s')}\r\n";
            echo "Debug: NextRunDate: {$cron->getNextRunDate()->format('Y-m-d H:i:s')}\r\n\r\n";
        }
        if ($cron->isDue()) {
            $plrClient = new base_plrClient($polaris, $construct->record->CLIENTID);
            $plrClient->LoadRecord();
            $construct->masterform->database->connectUserDB();
            if (isset($construct->record->BADGEFILTER))
                $customfilter = $construct->record->BADGEFILTER;
            else
                $customfilter = false;

            // store the session clienthash so that it is processed by plr_datascope's Filtervalue
            $_SESSION['clienthash'] = $plrClient->record->CLIENTHASHID;
            $customfilter = $construct->masterform->AddCustomFilter($customfilter, $plrClient->GetDataScopeFilter($construct->masterform->database->record->DATABASEID, $construct->masterform->GetDataDestination()));
            $theSQL = $construct->masterform->MakeSQL($edit = false, $countselect = false, $thesearchvalue = false, $masterfields = false, $countdetails = false, $detailtable = false, $customfilter);

            // Wrap a Count from around the SQL, so it also works with complex queries that have a GROUP BY
            $_countSQL = "SELECT COUNT(*) AS RECORDCOUNT FROM ( {$theSQL} ) AS subquery";
            $construct->masterform->database->userdb->debug = $debug;
            try {
                $countrs = $construct->masterform->database->userdb->Execute( $_countSQL );
            } catch (Exception $E) {
                if ($debug) echo "Error: {$E->getMessage()} \r\n";
            }

            if ( $countrs ) {
                if(isset($countrs->fields['RECORDCOUNT']) or isset($countrs->fields['recordcount'])) {
                    $result = $countrs->fields[0];
                } else {
                    $result = $countrs->RecordCount();
                }
                $polaris->UpdateConstructRecordCount($construct->record->CLIENTID, $construct->record->APPLICATIONID, $construct->record->CONSTRUCTID, $result);
                echo "Updated counter to: {$result} for clientid: {$construct->record->CLIENTID} constructid: {$construct->record->CONSTRUCTID} formid: {$construct->record->MASTERFORMID} \r\n";
            }
        }
    }
}
