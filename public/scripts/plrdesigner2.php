<?php
require_once 'includes/app_includes.inc.php';
include_once('includes/dsg_includes.inc.php');

// $locations = array ( $bc_start );

/**
 * Create the Polaris object
 */
$polaris = new base_Polaris();
$polaris->initialize();

$ClientID = $_SESSION['clientid'];
$plrClient = new base_plrClient($polaris, $ClientID);
$plrClient->LoadRecord();

// *************************
// global Smarty assignments
// *************************
$polaris->tpl->assign('username', $_SESSION['name']);
$polaris->tpl->assign('usertype', $_SESSION['usertype']);
$polaris->tpl->assign('headercolor', $polaris->userSettings('headercolor'));
$polaris->tpl->assign('basemenuid', 2);
$actions = array();
if (isset($usergroupactions) and is_array($usergroupactions) and is_array($clientactions))
    $actions = array_merge($usergroupactions, $clientactions);
// only rootadmins can generate apps (for now)
if (($_SESSION['usertype'] == 'root') and (isset($actions)
    and is_array($actions)
    and isset($applicationactions)
    and is_array($applicationactions)))
    $actions = array_merge($actions, $applicationactions);
//$polaris->tpl->assign('actions', $actions);
$polaris->tpl->assign('basemenuid', 2);

if (!isset($_GET['module'])) {
    $polaris->tpl->display('dsg_designer.tpl.php');
} else {
    include('designer/plr'.$_GET['module'].'.php');
}
