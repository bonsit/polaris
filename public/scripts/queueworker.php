<?php

## How does the Polaris Queuing work?
#
#  In your client module add two functions. One for sending a message to the queue,
#  which contains the action to be performed and the values that this action needs.
#  And a second one that performs the actual action
#
#    function QueueDeelnemerUitnodiging($email, $values, $deelnemerid, $recordid) {
#        global $polaris;
#        global $_GVARS;

#        # Add the neccessary values
#        $values['...'] = '';
#
#        # And send this message to the queue
#        $polaris->AddToQueue('queue_someaction', array('insertValues' => $values));
#    }

#     public function queue_someaction($data) {
#        # Do something with the #data
#        ...
#
#        # Always return true, when everything is handled correctly
#        return true;
#     }

require_once "../includes/app_includes.inc.php";

$worker = new base_QueueWorker();

$polaris = new base_Polaris();
$polaris->initialize();
$queue = $polaris->GetQueue();

$job = $queue->reserveWithTimeout(30); //in seconds

if ($job) {
    echo "Number of beanstalk jobs in default tube: ".$queue->statsTube($polaris->queue_tube)->currentJobsReady."\n\r";

    try {
        $jobPayload = $job->getData();

        echo "Starting job with payload: {$jobPayload}\n";

        $received = json_decode($job->getData(), true);
        $action = $received['action'];
        if(isset($received['data'])) {
            $data = $received['data'];
        } else {
            $data = array();
        }

        $_displayData = $data;
        if (is_array($_displayData)) $_displayData = current($_displayData);
        echo "Received a $action (" . $_displayData . ") ...\n\r";
        if(method_exists($worker, $action)) {
            $result = $worker->$action($data);
            if ($result) {
                $queue->delete($job);
            } else {
                $queue->bury($job);
            }
        } else {
            echo "action not found\n";
            $queue->bury($job);
        }

    } catch(\Exception $e) {
        echo "Caught exception: " . $e->getMessage() . "\n";
        // handle exception.
        // and let some other worker retry.
        $queue->release($job);
    }
}
