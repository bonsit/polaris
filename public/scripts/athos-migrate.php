<?php
require_once('includes/app_includes.inc.php');

require_once('athos-migrate.class.php');

$polaris = new base_Polaris();
$polaris->initialize();

function showError($message) {
    echo $message."\r\n\r\n";
    exit(1);
}

//**
// * Athos Migratiescript
// *
// * Dit script is bedoeld om de database van Athos te migreren naar een nieuwe versie.
// *

// All of the command line classes are in the Garden\Cli namespace.
use Garden\Cli\Cli;

// Require composer's autoloader.
// require_once 'vendor/autoload.php';

// Define a cli with commands.
$cli = Cli::create()
    ->command('convert')
    ->description('Converteert een Wordpress meta tabel naar relationele tabellen.')
    ->opt('database:d', 'Specificeer de naam van de import database.', true, 'string')
    ->opt('wpversion', 'Specificeer het type nummer van de wp tabellen.', true, 'integer')

    ->command('import')
    ->description('Importeert de Athos gegevens naar Maxia.')
    ->opt('maxiadb:m', 'Specificeer de maxia database.', true, 'string')
    ->opt('type:t', 'Specificeer het type resource om te importeren.', true, 'string')
    ->opt('dryrun', 'Toon alleen de acties, maar voegt geen records toe.', false, 'boolean')

    ->command('list_types')
    ->description('Toont de resource types van de Athos database.')
    ->opt('database:d', 'Specificeer de naam van de import database.', true, 'string');

    // ->command('showapps')
    // ->description('Toont alle applicaties van een klant.')
    // ->opt('client:c', 'Specificeer de id van de client.', true, 'integer')

    // ->command('showdbs')
    // ->description('Toont alle databases van een klant.')
    // ->opt('client:c', 'Specificeer de id van de client.', true, 'integer');

// Parse and return cli args.
$args = $cli->parse($argv, true);

// Execute the commands
switch($args->getCommand()) {
    case 'convert':
        $athosMigrate = new athosMigrate($args->getOpt('database'));
        $athosMigrate->convert($args->getOpt('wpversion'));
        break;

    case 'import':
        $maxiadb = $args->getOpt('maxiadb');
        $type = $args->getOpt('type');
        $dryrun = $args->getOpt('dryrun');
        $athosMigrate = new athosMigrate();
        $athosMigrate->import($maxiadb, $type, $dryrun);
        break;

    case 'list_types':
        $athosMigrate = new athosMigrate($args->getOpt('database'));
        $athosMigrate->listTypes();
        break;

    default:
    	showError('Commando is nog niet verwerkt.');
        break;
}
