<?php
require_once "../includes/app_includes.inc.php";

$polaris = new base_Polaris();
$polaris->initialize();

$_limit = 10000;  // keep this many records
$_cleanupLog = "
    DELETE FROM `plr_logbase`
    WHERE logid NOT IN (
      SELECT logid
      FROM (
        SELECT logid
        FROM `plr_logbase`
        ORDER BY logid DESC
        LIMIT $_limit
      ) foo
    )
";
$polaris->instance->Execute($_cleanupLog);

$_affectedRows = $polaris->instance->Affected_Rows();
$polaris->logUserAction($status="LOGCLEANUP", $sessid='CRONJOB', $username='__INTERNAL', "$_affectedRows rows removed");
echo "Polaris log cleanup... check! \n";
