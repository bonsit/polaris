<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once('includes/dsh_includes.inc.php');

/**
 * Create the Polaris object
 */
$polaris = new base_Polaris();
$polaris->initialize();

/***
    Handle the events
**/
$event = false;
if (isset($_GET['method']))
    $event = $_GET['method'];
elseif (isset($_REQUEST['_hdnAction']))
    $event = $_REQUEST['_hdnAction'];

if ($event) {
    $disp = new controllers_dispatcherPolaris($event);
    $response = $disp->processEvent();
}

$dashboard = new base_plrDashboard($polaris);
$dashboard->LoadRecords();

$polaris->tpl->assign('title', 'TrafficAssistant');
$polaris->tpl->assign('windows', $dashboard->items);
$polaris->tpl->assign('plruserconstructs', $dashboard->getAllConstruct());

if ((isset($_SESSION['plrauth']) and !$_SESSION['plrauth']) or !isset($_SESSION['plrauth']) ) {
    $_SESSION['encryptsalt'] = randomString(16);
    $polaris->tpl->assign('encryptsalt', $_SESSION['encryptsalt']);
    $polaris->tpl->display('dashboard.login.tpl.php');
} else {
    $polaris->tpl->display('dashboard.desk.tpl.php');
}
