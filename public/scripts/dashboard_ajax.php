<?php
require_once('includes/app_includes.inc.php');
require_once('classes/eventdispatcher_dashboardajax.class.php');
require_once('JSON/JSON.php');

if (isset($_REQUEST['method'])) {
    /**
     * handle the event
     */
	$disp = new dashboardAjax_dispatcher($_REQUEST['method']);
	$response = $disp->processEvent();
	
    /**
     * transform the result into JSON format
     */
    $json = new Services_JSON();
    echo $json->encode($response);  
} else {
    echo '{ result: false }';
}
?>