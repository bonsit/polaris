<?php

require_once "../includes/app_includes.inc.php";

$polaris = new base_Polaris();
$polaris->initialize();

/**
 * Get the POST'ed content
 * (Won't work via $_POST because PHP does not convert JSON content to $_POST)
 */
$json = file_get_contents('php://input');
$values = json_decode($json, true);

/**
 * The mail message is send with a certain Tag. Use that tag to find
 * the correct ReplyRule (so we can get access to the client table information)
 */
$_reply = $polaris->instance->GetRow($_sqlReplyRuleWithHash, array($values['Tag']));

/**
 * Create the form (base on the FORMHASH constructed by the previous SQL),
 * then connect to the client database from that form
 */
if (!empty($_reply['FORMHASH'])) {
    $_form = new base_plrForm($polaris);
    $_form->LoadRecordHashed($_reply['FORMHASH']);
    $_form->database->connectUserDB();

    /**
     * Use the column in the reply rule to update the Status of the message
     * so we now when a message was opened by the recipient
     */
    if (!empty($values['MessageID'])) {
        $_form->database->userdb->AutoExecute($_form->GetDataDestination(), $_record, 'UPDATE', 'MESSAGEID = "'.$values['MessageID'].'"');
    }
}