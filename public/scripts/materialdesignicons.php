<?php

// Read all lines from material-design-icons.json
$_lines = file('material-design-icons.json');

// Loop through the lines and create a nice 10x10 grid with bootstrap
$_html = '
    <div class="form-group">
        <input type="search" class="form-control" id="icons-picker-search" autofocus placeholder="Zoek icoon...">
    </div>
    <div class="icons-picker">
        <div class="icons-picker">
';
foreach ($_lines as $_index => $_line) {
    $_icon = explode(' ', $_line);
    $_html .= '<div class="icons-picker-icon icons-picker-icon-filter">';
    $_html .= '<i class="material-symbols-outlined md-3x">' . $_icon[0] . '</i>';
    $_html .= '<div><div>'.$_icon[0].'</div></div>';
    $_html .= '</div>';
}
$_html .= '</div>';
echo $_html;
