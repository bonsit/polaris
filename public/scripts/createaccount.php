<?php
require_once('../includes/app_includes.inc.php');

/**
 * Create the Polaris object
 */
$polaris = new base_Polaris();
$polaris->initialize();

/**
 * What is the event (Save record, Delete record, etc) ?
 */
$_event = isset($_GET['method'])?$_GET['method']:false;
if (!$_event) $_event = isset($_REQUEST['_hdnAction'])?$_REQUEST['_hdnAction']:false;

/*
 * Dispatch the event to the right event handler (if not handled by the custom module)
 */

$_processedByModule = (isset($_REQUEST['_hdnProcessedByModule'])
    and ($_REQUEST['_hdnProcessedByModule'] == 'true'));

try {
    $_error = '';
    if ($_event and !$_processedByModule) {
        $dispatcher = new controllers_dispatcherPolaris($_event);
        $response = $dispatcher->processEvent();
    }
} catch (Exception $e) {
    $_error = $e->getMessage();
}

$polaris->showCreateAccountForm($_error);
