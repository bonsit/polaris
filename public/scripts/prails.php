<?php
require_once('includes/app_includes.inc.php');

function showMessage($message) {
    echo $message."\r\n\r\n";
    exit(0);
}

function showError($message) {
    echo $message."\r\n\r\n";
    exit(1);
}

function pretty_var_dump($variable) {
    $output = print_r($variable, true);
    $output = preg_replace('/^(\s+)/m', str_repeat(' ', 4), $output);
    $output = preg_replace('/Array\n(\s+)\(/', 'Array (', $output);
    $output = preg_replace('/^([A-Za-z_0-9]+) Object\n(\s+)\(/', '$1 Object (', $output);

    echo $output . PHP_EOL;
}

/**
 * Create the Polaris object
 */
$polaris = new base_Polaris();
$polaris->initialize();

// All of the command line classes are in the Garden\Cli namespace.
use Garden\Cli\Cli;

// Require composer's autoloader.
require_once 'vendor/autoload.php';

// Define a cli with commands.
$cli = Cli::create()
    ->command('showclients')
    ->description('Toont alle klanten.')

    ->command('showapps')
    ->description('Toont alle applicaties van een klant.')
    ->opt('client:c', 'Specificeer de id van de client.', true, 'integer')

    ->command('showdbs')
    ->description('Toont alle databases van een klant.')
    ->opt('client:c', 'Specificeer de id van de client.', true, 'integer')

    ->command('showdomains')
    ->description('Toont alle kennisdomeinen van een klant.')
    ->opt('client:c', 'Specificeer de id van de client.', true, 'integer')

    ->command('deleteapp')
    ->description('Verwijdert een applicatie.')
    ->opt('client:c', 'Specificeer de id van de client.', true, 'integer')
    ->opt('app:a', 'Specificeer de id van de applicatie.', true, 'integer')

    ->command('copyform')
    ->description('Maakt een kopie van een formulier.')
    ->opt('client:c', 'Specificeer de id van de client.', true, 'integer')
    ->opt('form:f', 'Specificeer de id van de form.', true, 'integer')

    ->command('deleteform')
    ->description('Verwijdert een formulier.')
    ->opt('client:c', 'Specificeer de id van de client.', true, 'integer')
    ->opt('form:f', 'Specificeer de id van de form.', true, 'integer')

    ->command('addform')
    ->description('Voegt een formulier toe aan een bestaande applicatie.')
    ->opt('client:c', 'Specificeer de id van de client.', true, 'integer')
    ->opt('app:a', 'Specificeer de id van de applicatie.', true, 'integer')
    ->opt('database:d', 'Specificeer de id van de database.', true, 'integer')
    ->opt('table:t', 'Specificeer de naam van de tabel.', true, 'string')

    ->command('addfacttype')
    ->description('Voegt een feittype toe aan een kennis domein.')
    ->opt('client:c', 'Specificeer de id van de client.', true, 'integer')
    ->opt('domain', 'Specificeer de id van het kennisdomein.', true, 'integer')
    ->opt('database:d', 'Specificeer de id van de database.', true, 'integer')
    ->opt('table:t', 'Specificeer de naam van de tabel.', true, 'string')

    ->command('info')
    ->description('Toont info over de huidige Polaris instantie.')

    ->command('apicall')
    ->description('Maakt een Polaris API Service call.')
    ->opt('client:c', 'Specificeer de id van de client.', true, 'integer')
    ->opt('app:a', 'Specificeer de id van de applicatie.', true, 'integer')
    ->opt('construct:co', 'Specificeer de id van de construct.', true, 'string')
    ->opt('apikey', 'Specificeer de apikey.', true, 'string')
    ->opt('apisecret', 'Specificeer de apisecret.', true, 'string')
    ->opt('username', 'Specificeer de gebruikersnaam.', true, 'string')

    ->command('showtablemeta')
    ->description('Toont de meta info van een tabel.')
    ->opt('client:c', 'Specificeer de id van de client.', true, 'integer')
    ->opt('database:d', 'Specificeer de id van de database.', true, 'integer')
    ->opt('table:t', 'Specificeer de naam van de tabel.', true, 'string')

    ->command('exportapp')
    ->description('Exporteer de app gegevens als SQL Inserts.')
    ->opt('client:c', 'Specificeer de id van de client.', true, 'integer')
    ->opt('app:a', 'Specificeer de id van de applicatie.', true, 'integer')
    ->opt('filename:f', 'Specificeer naam van de outputfile.', false, 'string')
    ->opt('template:t', 'Exporteer als een template applicatie.', false, 'boolean')

    ->command('exportform')
    ->description('Exporteer de form gegevens als SQL Inserts.')
    ->opt('client:c', 'Specificeer de id van de client.', true, 'integer')
    ->opt('form:f', 'Specificeer de id van de form.', true, 'integer');

// Parse and return cli args.
$args = $cli->parse($argv, true);

// Execute the commands
switch($args->getCommand()) {
    case 'showclients':
        $polaris->showAsList("SELECT CLIENTID AS ID, NAME FROM `plr_client`");
    break;
    case 'showapps':
        $_clientid = $args->getOpt('client');
        if (!isset($_clientid)) {
            showError("U dient een clientid mee te geven.");
        } else {
            $polaris->showAsList("SELECT CLIENTID, APPLICATIONID AS ID, APPLICATIONNAME, DESCRIPTION FROM `plr_application` WHERE CLIENTID = ?", array($_clientid));
        }
    break;
    case 'showdbs':
        $_clientid = $args->getOpt('client');
		$polaris->showAsList("SELECT CLIENTID, DATABASEID AS ID, DATABASENAME FROM `plr_database` WHERE CLIENTID = ?", array($_clientid));
    break;
    case 'showdomains':
        $_clientid = $args->getOpt('client');
        $polaris->showAsList("SELECT CLIENTID, DOMAINID AS ID, DOMAINNAME FROM `mod_domain` WHERE CLIENTID = ?", array($_clientid));
    break;
    case 'deleteapp':
        $_clientid = $args->getOpt('client');
        $_appid = $args->getOpt('app');
        $_rs = $polaris->instance->GetRow("SELECT APPLICATIONID AS ID, APPLICATIONNAME, DESCRIPTION FROM `plr_application` WHERE CLIENTID = ? AND APPLICATIONID = ? ", array($_clientid, $_appid));
        echo "Weet u zeker dat de applicatie '{$_rs['APPLICATIONNAME']}' ({$_rs['DESCRIPTION']}) wilt verwijderen?\r\n";
        echo "Typ JA: ";
        $confirm = fgets(STDIN);
        if (trim($confirm) == 'JA') {
            $polaris->deleteApp($_clientid, $_appid);
        }
    break;
    case 'copyform':
        $_clientid = $args->getOpt('client');
        $_oldformid = $args->getOpt('form');

        $_clientObj = new base_plrClient($polaris, $_clientid);

        $_newformid = $_clientObj->getNewFormId($clientid);

        $polaris->instance->Execute("
        INSERT INTO `plr_form` (`CLIENTID`,`FORMID`,`FORMNAME`,`METANAME`,`HELPNR`,`PAGECOLUMNCOUNT`,`WEBPAGEURL`,`DATABASEID`,`TABLENAME`,`UPDATETABLENAME`,`FILTER`,`MODULE`,`MODULEID`,`DEFAULTVIEWTYPE`,`VIEWTYPEPARAMS`,`SHOWINFRAME`,`ITEMNAME`,`WEBPAGENAME`,`WEBPAGECONSTRUCT`,`SHOWRECORDSASCONSTRUCTS`,`CAPTIONCOLUMNNAME`,`SORTCOLUMNNAME`,`SEARCHCOLUMNNAME`,`PANELHELPTEXT`,`PRELOAD`,`HISTORYTYPE`,`SELECTVIEW`,`SELECTCOLUMNNAMES`,`SELECTSHOWCOLUMNS`,`SELECTQUERY`,`OPTIONS`,`SENTENCEPATTERN`,`REFRESHFORM`,`DEFAULTREFRESHINTERVAL`,`CSSIDENTIFIERCOLUMN`,`RSSCOLUMNS`,`CRYSTALREPORTNAME`,`LIBRARYNAME`,`PROCEDURENAME`,`RECORDLIMIT`)
        SELECT `CLIENTID`,$_newformid,`FORMNAME`,CONCAT(`METANAME`,'_', $_newformid),`HELPNR`,`PAGECOLUMNCOUNT`,`WEBPAGEURL`,`DATABASEID`,`TABLENAME`,`UPDATETABLENAME`,`FILTER`,`MODULE`,`MODULEID`,`DEFAULTVIEWTYPE`,`VIEWTYPEPARAMS`,`SHOWINFRAME`,`ITEMNAME`,`WEBPAGENAME`,`WEBPAGECONSTRUCT`,`SHOWRECORDSASCONSTRUCTS`,`CAPTIONCOLUMNNAME`,`SORTCOLUMNNAME`,`SEARCHCOLUMNNAME`,`PANELHELPTEXT`,`PRELOAD`,`HISTORYTYPE`,`SELECTVIEW`,`SELECTCOLUMNNAMES`,`SELECTSHOWCOLUMNS`,`SELECTQUERY`,`OPTIONS`,`SENTENCEPATTERN`,`REFRESHFORM`,`DEFAULTREFRESHINTERVAL`,`CSSIDENTIFIERCOLUMN`,`RSSCOLUMNS`,`CRYSTALREPORTNAME`,`LIBRARYNAME`,`PROCEDURENAME`,`RECORDLIMIT`
        FROM plr_form WHERE clientid = $_clientid AND formid = $_oldformid
        ");

        $polaris->instance->Execute("
        INSERT INTO `plr_page` (`CLIENTID`,`FORMID`,`PAGEID`,`PAGENAME`,`HELPNR`,`PAGECOLUMNCOUNT`,`LIBRARYNAME`,`PROCEDURENAME`,`IMAGEINDEX`,`CRYSTALREPORTNAME`,`ACTIONID`,`BEFORECRYSTALREPORTACTION`,`JASPERREPORTNAME`,`JASPERREPORTPARAMS`)
        SELECT `CLIENTID`,$_newformid,`PAGEID`,`PAGENAME`,`HELPNR`,`PAGECOLUMNCOUNT`,`LIBRARYNAME`,`PROCEDURENAME`,`IMAGEINDEX`,`CRYSTALREPORTNAME`,`ACTIONID`,`BEFORECRYSTALREPORTACTION`,`JASPERREPORTNAME`,`JASPERREPORTPARAMS`
        FROM plr_page WHERE clientid = $_clientid AND formid = $_oldformid
        ");

        $polaris->instance->Execute("
        INSERT INTO `plr_line` (`CLIENTID`,`FORMID`,`PAGEID`,`LINEID`,`LINEDESCRIPTION`,`LINEDESCRIPTION_SHORT`,`HELPNR`,`LINETYPE`,`GROUPID`,`DATABASEID`,`TABLENAME`,`COLUMNNAME`,`READONLY`,`LINEDEFAULTVALUE`,`LINEREQUIRED`,`FORMAT`,`FORMATPARAMS`,`UNIT`,`IMAGEDIR`,`IMAGEURL`,`POSITIONTYPE`,`XPOSITION`,`YPOSITION`,`VISIBLE`,`VISIBLEINLIST`,`POSITIONINDEX`,`PREVIEWURL`,`ISSELECTFIELD`,`SELECTSOURCE`,`ISGROUPFIELD`,`GROUPSOURCE`,`EVENTS`,`CSSIDENTIFIER`,`CUTOFFTHRESHOLD`,`AUTOFOCUS`)
        SELECT `CLIENTID`,$_newformid,`PAGEID`,`LINEID`,`LINEDESCRIPTION`,`LINEDESCRIPTION_SHORT`,`HELPNR`,`LINETYPE`,`GROUPID`,`DATABASEID`,`TABLENAME`,`COLUMNNAME`,`READONLY`,`LINEDEFAULTVALUE`,`LINEREQUIRED`,`FORMAT`,`FORMATPARAMS`,`UNIT`,`IMAGEDIR`,`IMAGEURL`,`POSITIONTYPE`,`XPOSITION`,`YPOSITION`,`VISIBLE`,`VISIBLEINLIST`,`POSITIONINDEX`,`PREVIEWURL`,`ISSELECTFIELD`,`SELECTSOURCE`,`ISGROUPFIELD`,`GROUPSOURCE`,`EVENTS`,`CSSIDENTIFIER`,`CUTOFFTHRESHOLD`,`AUTOFOCUS`
        FROM plr_line WHERE clientid = $_clientid AND formid = $_oldformid
        ");

        showMessage("Het formulier $_oldformid is gekopieerd naar form $_newformid.");
    break;
    case 'addform':
        $_clientid = $args->getOpt('client');
        $_applicationid = $args->getOpt('app');
        $_databaseid = $args->getOpt('database');
        $_tablename = $args->getOpt('table');

        $_clientObj = new base_plrClient($polaris, $_clientid, false);
        $_appObj = $_clientObj->LoadApplication($_applicationid);
        if ($_appObj->record) {
            $formname = $_appObj->GenerateBaseConstruct($_databaseid, $_tablename, $usergroupid=1);
            showMessage("Het formulier is aangemaakt: $formname");
        } else {
            showError("De applicatieid $_applicationid is niet bekend.");
        }
    break;
    case 'deleteform':
        $_clientid = $args->getOpt('client');
        $_oldformid = $args->getOpt('form');

        $polaris->instance->Execute("
        DELETE FROM `plr_form` WHERE clientid = $_clientid AND formid = $_oldformid
        ");
        $polaris->instance->Execute("
        DELETE FROM `plr_page` WHERE clientid = $_clientid AND formid = $_oldformid
        ");
        $polaris->instance->Execute("
        DELETE FROM `plr_line` WHERE clientid = $_clientid AND formid = $_oldformid
        ");
        showMessage("Het formulier $_oldformid is verwijderd.");
    break;
    case 'addfacttype':
        $_clientid = $args->getOpt('client');
        $_domainid = $args->getOpt('domain');
        $_databaseid = $args->getOpt('database');
        $_tablename = $args->getOpt('table');

        require_once 'classes/base/plrClient.class.php';
        require_once 'classes/base/plrDatabase.class.php';
        $_clientObj = new base_plrClient($polaris, $_clientid);
        $_dbObj = new base_plrDatabase($polaris, array('CLIENTID'=>$_clientid, 'DATABASEID'=>$_databaseid));
        $_dbObj->LoadRecord();
        if ($_dbObj->record) {
            $_dbObj->ImportFactTypes($_clientid, $_domainid, $_tablename, $overwritetables=false);
            showMessage("Het feittype is aangemaakt: $formname");
        } else {
            showError("De database $_databaseid is niet bekend.");
        }
    break;
    case 'apicall':
        $_clientid = $args->getOpt('client');
        $_appid = $args->getOpt('app');
        $_constmeta = $args->getOpt('construct');
        $_apisecret = $args->getOpt('apisecret');
        $_username = $args->getOpt('username');
        $_rs = $polaris->instance->GetRow("
        SELECT apikey, apikeyactive FROM plr_client WHERE clientid = ?
        ", array($_clientid));
        if ($_rs['apikeyactive'] == 'Y') {
            $_apikey = $_rs['apikey'];
            $_app = $polaris->instance->GetRow("
            SELECT metaname FROM plr_application WHERE clientid = ? AND applicationid = ?
            ", array($_clientid, $_appid));
            if ($_app) {
                $_appmeta = $_app['metaname'];

                $_usr = $polaris->instance->GetRow("
                SELECT userpassword FROM plr_usergroup WHERE usergroupname = ?
                ", array($_username));
                $_hashedPassword = $_usr['userpassword'];

                var_dump($_apisecret.'api_call'.$_appmeta.$_constmeta.'api_key'.$_apikey.'api_pwd'.$_hashedPassword);
                $_api_sig = hashstr($_apisecret.'api_call'.$_appmeta.$_constmeta.'api_key'.$_apikey.'api_pwd'.$_hashedPassword);
                showMessage("API Signature: ".$_api_sig."\n\n"."HTTP call: ".$_api_sig);
            }

        } else {
            showMessage("De functie API Service call is niet actief.");
        }
    break;
    case 'showtablemeta':
        $_clientid = $args->getOpt('client');
        $_databaseid = $args->getOpt('database');
        $_tablename = $args->getOpt('table');

        require_once 'classes/base/plrClient.class.php';
        require_once 'classes/base/plrDatabase.class.php';
        $_clientObj = new base_plrClient($polaris, $_clientid);
        $_dbObj = new base_plrDatabase($polaris, array('CLIENTID'=>$_clientid, 'DATABASEID'=>$_databaseid));
        $_dbObj->LoadRecord();
        $_dbObj->ConnectUserDB();
        if ($_dbObj->userdb) {
            pretty_var_dump("Tablename: {$_tablename}");
            pretty_var_dump($_dbObj->userdb->MetaColumns($_tablename));
        }
    break;
    case 'exportapp':
        $_clientid = $args->getOpt('client');
        $_appid = $args->getOpt('app');
        $_databaseid = $args->getOpt('database');
        $_astemplate = $args->getOpt('template');

        require_once 'classes/base/plrClient.class.php';
        require_once 'classes/base/plrDatabase.class.php';
        $_clientObj = new base_plrClient($polaris, $_clientid);
        $_dbObj = new base_plrDatabase($polaris, array('CLIENTID'=>$_clientid, 'DATABASEID'=>$_databaseid));
        $_dbObj->LoadRecord();
        $_appObj = $_clientObj->LoadApplication($_appid);

        if ($_appObj->record) {
            $_appObj->ExportAppRecords(filename: $args->getOpt('filename'), astemplate: $_astemplate);
        } else {
            showError("De applicatieid $_appid is niet bekend.");
        }
    break;
    case 'exportform':
        $_clientid = $args->getOpt('client');
        $_formid = $args->getOpt('form');
        $_settings = $_PLRINSTANCE[$_CONFIG['currentinstance']];
        system("mysqldump {$_settings['database']} plr_form plr_page plr_line --where='clientid=$_clientid and formid=$_formid' --no-create-info --complete-insert --host={$_settings['host']} --user={$_settings['username']} --password={$_settings['password']}");
    break;
    default:
    	showError('Commando is nog niet verwerkt.');
    break;
}
