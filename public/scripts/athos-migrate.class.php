<?php

/**
 * Class athosMigrate
    @property null $importdb
    @property null $destinationdb
 */
class athosMigrate {

    var $clienthash = '64a7edd27dc88bd3edd7c3bd80202e2d'; // md5 hash of the client id
    var $src = null;
    var $dst = null;
    var $mxdst = null;
    var $dbserver = 'db-clients';
    var $dbuser = 'root';
    var $dryrun = false;
    var $maxiadb = null;
    protected string $dbpassword = '';

    var $destinationdb = 'athos_intermediate2';

    function __construct(
        private $importdb = null,
    ) {
        $this->dbpassword = $_ENV['MYSQL_BACKUP_CLIENTS_ROOTPASSWORD'];
        if (!empty($this->importdb)) {
            try {
                $this->src = NewADOConnection('mysqli');
                $this->src->SetFetchMode(ADODB_FETCH_ASSOC);
                $this->src->Connect($this->dbserver, $this->dbuser, $this->dbpassword, $this->importdb);
            } catch (Exception $e) {
                showError("Kan geen verbinding maken met de import database '{$this->importdb}'.");
            }
        }

        $this->ConnectDestDatabase();

        if ($this->src && !$this->src->IsConnected()) { showError("Geen verbinding met de import database."); }
        if (!$this->dst->IsConnected()) { showError("Geen verbinding met de destination database."); }
    }

    function ConnectDestDatabase() {
        if ($this->destinationdb) {
            try {
                $this->dst = NewADOConnection('mysqli');
                $this->dst->Connect($this->dbserver, $this->dbuser, $this->dbpassword, $this->destinationdb);
            } catch (Exception $e) {
                showError("Kan geen verbinding maken met de import database '{$this->destinationdb}'.");
            }
        }
    }

    function ConnectMaxiaDestDatabase() {
        try {
            $this->mxdst = NewADOConnection('mysqli');
            $this->mxdst->Connect($this->dbserver, $this->dbuser, $this->dbpassword, $this->maxiadb);
        } catch (Exception $e) {
            showError("Kan geen verbinding maken met de Maxia database '{$this->maxiadb}'.");
        }
    }

    function EnsureConnection() {
        try {
            echo "Ensuring connection of mxdst\n";
            $this->mxdst->execute('SELECT 1');
            echo "Connection active\n\n";
        } catch (ADODB_Exception $e) {
            switch ($e->getCode()) {
                case '2006':
                    $this->ConnectMaxiaDestDatabase();
                    break;
                default:
                    throw $e;
            }
        }

        try {
            echo "Ensuring connection of dst\n";
            $this->dst->execute('SELECT 1');
            echo "Connection active\n\n";
        } catch (ADODB_Exception $e) {
            switch ($e->getCode()) {
                case '2006':
                    $this->ConnectDestDatabase();
                    break;
                default:
                    throw $e;
            }
        }
    }

    function is_serialized($str) {
        // Trim whitespace
        $str = trim($str);

        // If the string is 'N;' then it's serialized null
        if ($str === 'N;') {
            return true;
        }

        // Check the string length
        if (strlen($str) < 4) {
            return false;
        }

        // Check the first character to determine the serialized type
        if ($str[1] !== ':') {
            return false;
        }

        $lastChar = substr($str, -1);
        if ($lastChar !== ';' && $lastChar !== '}') {
            return false;
        }

        // Attempt to unserialize the string
        $unserialized = @unserialize($str);
        if ($unserialized === false && $str !== 'b:0;') {
            return false;
        }

        return true;
    }

    function serialized($serialized_str) {
        // Controleer of de string geserialiseerd is
        if (!$this->is_serialized($serialized_str)) {
            return false;
        }

        // Deserialiseer de string
        return unserialize($serialized_str);
    }

    function serialized_to_json($serialized_str) {
        $unserialized_array = $this->serialized($serialized_str);

        // Converteer de array naar JSON
        $json_str = json_encode($unserialized_array);

        // Controleer op JSON encode errors
        if (json_last_error() !== JSON_ERROR_NONE) {
            return false;
        }

        return $json_str;
    }

    function typeToTableName($type) {
        return "AA_" . str_replace('-', '_', $type);
    }

    function getResourceTypes() {
        try {
            // Get all relevant post types
            $sql = 'SELECT DISTINCT post_type FROM wp_posts';
            $_postTypes = $this->src->GetAll($sql);
        } catch (Exception $e) {
            if ($e->getCode() == 1046) {
                showError("Geen geldige database. ".$e->getMessage());
            } else {
                throw new Exception("Kan geen post types ophalen. " . $e->getMessage());
            }
        }
        return $_postTypes;
    }

    function convert($wp_type=1) {
        switch($wp_type) {
            case 1:
                $procname = 'wp_posts_pivot';
                $tablepostfix = '';
                $skip_posttype = ['revision','page'];
                break;
            case 2:
                $procname = 'wp_2_posts_pivot';
                $tablepostfix = '_2';
                $skip_posttype = ['revision','page', 'allowances'];
                break;
            case 3:
                $procname = 'wp_3_posts_pivot';
                $tablepostfix = '_3';
                $skip_posttype = ['revision','page', 'allowances'];
                break;
        }

        foreach($this->getResourceTypes() as $type) {

            if (in_array($type['post_type'], $skip_posttype) ) continue;

            $tablename = $this->typeToTableName($type['post_type']);
            echo "Processing {$type['post_type']}\n";

            try {

                $stm = "DROP TABLE IF EXISTS {$tablename}{$tablepostfix};";
                echo "Executing: {$stm}\n";
                $this->dst->Execute($stm);

                $stm = "CALL {$procname}('{$type['post_type']}');";
                echo "Executing: {$stm}\n\n";
                $_result = $this->dst->Execute($stm);

            } catch (ADODB_Exception $e) {
                // Rollback the transaction if an error occurs
                // $this->dst->RollbackTrans();

                // Log the error
                error_log("Database error: " . $e->getMessage());

                // Rethrow the exception or handle it as needed
                throw $e;
            }

        }
        return true;
    }

    function import($maxiadb, $type, $dryrun) {
        $this->maxiadb = $maxiadb;
        $this->dryrun = $dryrun;

        $this->ConnectMaxiaDestDatabase();

        $functionName = "process{$type}";
        if (method_exists($this, $functionName)) {
            // $this->$functionName();
            call_user_func([$this, $functionName]);
        } else {
            showError("Functie {$functionName} bestaat niet.");
        }

        return true;
    }

    function listTypes() {
        echo "\n";
        foreach($this->getResourceTypes() as $type) {
            echo "{$type['post_type']}\n";
        }
        return true;
    }

    private function _processTable($mapping, $sourcetable, $destinationtable, $destinationkeycolumn) {
        $sql = "
        SELECT
            ttt.*,
            CASE
                WHEN ttt.post_status = 'publish' THEN 0
                WHEN ttt.post_status = 'trash' THEN 1
                ELSE NULL  -- Handle other post statuses if needed
            END AS post_status
        FROM {$sourcetable} ttt;
        ";

        $rows = $this->dst->GetAll($sql);

        $updatedrows = 0;
        $insertedrows = 0;

        foreach ($rows as $item) {
            $dbData = [];

            // Vul de $dbData array met gemapte waarden
            foreach ($mapping as $dbColumn => $importKey) {
                if (!is_array($importKey) && isset($item[$importKey])) {
                    $dbData[$dbColumn] = $item[$importKey];
                } else {
                    if (is_string($importKey) && substr($importKey, 0, 1) == "'") {
                        // fixed value
                        $dbData[$dbColumn] = substr($importKey, 1, -1); // Haal de quotes weg
                    } elseif (is_string($importKey) && substr($importKey, 0, 5) == 'JSON:') {
                        // JSON value
                        $jsonKey = substr($importKey, 5);
                        $res = $this->serialized_to_json($item[$jsonKey]);
                        if ($res) {
                            $dbData[$dbColumn] = $res;
                        } else {
                            $dbData[$dbColumn] = '[]';
                        }
                    } elseif (is_string($importKey) && substr($importKey, 0, 13) == 'CLEANCOUNTRY:') {
                        // Do some cleaning
                        $cleanKey = substr($importKey, 13);
                        // Take the value and find it in the country table
                        $sqlCountry = "SELECT countryid FROM mx_landen
                        WHERE ? SOUNDS LIKE CountryName OR ? LIKE CONCAT(CountryName, '%')
                        OR FIND_IN_SET(?, __MIGRATE_ALTS) > 0";
                        $stmtCountry = $this->mxdst->prepare($sqlCountry);
                        if (trim($item[$cleanKey]) !== '') {
                            try {
                                $countryId = $this->mxdst->GetOne($stmtCountry, [$item[$cleanKey],$item[$cleanKey],$item[$cleanKey]]);
                            } catch (Exception $e) {
                                echo $e->getMessage();
                                $countryId = -100;
                            }
                            if (!isset($countryId)) {
                                $countryId = -90;
                            }
                        } else {
                            $countryId = '';
                        }

                        $dbData[$dbColumn] = $countryId;
                    } elseif (is_array($importKey) && $importKey[0] == 'LOOKUP') {
                        // Lookup value in other table
                        $lookup = $importKey[1];
                        $lookupTable = $lookup['table'];
                        $lookupColumn = $lookup['column'];
                        $lookupField = $lookup['field'];

                        if (substr($lookupField, 0, 5) == 'JSON:') {
                            // JSON value
                            $lookupField = substr($lookupField, 5);
                            $res = $this->serialized($item[$lookupField]);
                            if ($res) {
                                $values = implode(", ", $res);
                                $sqlLookup = "SELECT {$lookupColumn} FROM {$lookupTable} WHERE __MIGRATE_ID IN ($values)";
                                $stmtLookup = $this->mxdst->prepare($sqlLookup);
                                $lookupResult = $this->mxdst->GetCol($stmtLookup);
                                if ($lookupResult) {
                                    $lookupResult = json_encode($lookupResult);
                                } else {
                                    $lookupResult = '[]';
                                }
                            } else {
                                $lookupResult = '[]';
                            }

                            $dbData[$dbColumn] = $lookupResult;
                        } else {
                            $lookupValue = $item[$lookupField];
                            $sqlLookup = "SELECT {$lookupColumn} FROM {$lookupTable} WHERE __MIGRATE_ID = ?";
                            $stmtLookup = $this->mxdst->prepare($sqlLookup);
                            $lookupResult = $this->mxdst->GetOne($stmtLookup, [$lookupValue]);

                            $dbData[$dbColumn] = $lookupResult;
                        }
                    } elseif (is_array($importKey)) {
                        // lookup value in array
                        $columnname = $importKey[0];
                        $valuemapping = $importKey[1];
                        try {
                            if (isset($valuemapping[$item[$columnname]]))
                                // throw new Exception("Problem with client {$item['client_id']}");
                                $dbData[$dbColumn] = $valuemapping[$item[$columnname]]; // Haal de waarde uit de array (bijv. geslacht)
                        } catch (Exception $e) {
                            echo $e->getMessage();
                        }
                    } else {
                        $dbData[$dbColumn] = null;
                    }
                }
            }

            // Controleer of de client al bestaat op basis van 'deelnemer_code'
            $clientId = $dbData['__MIGRATE_ID'];
            $sqlCheck = "SELECT {$destinationkeycolumn} FROM {$destinationtable} WHERE __MIGRATE_ID = ?";
            $stmtCheck = $this->mxdst->prepare($sqlCheck);
            $existingClient = $this->mxdst->GetOne($stmtCheck, [$clientId]);

            if ($existingClient) {
                // Client bestaat, dus we updaten de gegevens
                $updateFields = [];
                $updateValues = [];
                foreach ($dbData as $column => $value) {
                    $updateFields[] = "$column = ?";
                    $updateValues[] = $value;
                }

                $updateValues[] = $existingClient; // Voeg de ID toe aan het einde voor de WHERE clausule

                $sqlUpdate = "UPDATE {$destinationtable} SET " . implode(", ", $updateFields) . " WHERE {$destinationkeycolumn} = ?";
                $stmtUpdate = $this->mxdst->prepare($sqlUpdate);
                if ($this->dryrun) {
                    echo "Dryrun: {$sqlUpdate}\n";
                    echo "Values: " . implode(", ", $updateValues) . "\n";
                } else {
                    try {
                        if ($this->mxdst->execute($stmtUpdate, $updateValues)) {
                            $updatedrows += $this->mxdst->affected_rows();
                        }
                    } catch (Exception $e) {
                        echo "$stmtUpdate\n\n";
                        echo "Values: " . implode(", ", $updateValues) . "\n\n";
                        // throw $e;
                        echo $e->getMessage() . "\n";
                    }
                }
            } else {
                // Client bestaat niet, dus we voegen een nieuw record toe
                $columns = implode(", ", array_keys($dbData));
                $placeholders = implode(", ", array_fill(0, count($dbData), "?"));
                $values = array_values($dbData);

                $sqlInsert = "INSERT INTO {$destinationtable} ($columns) VALUES ($placeholders)";
                $stmtInsert = $this->mxdst->prepare($sqlInsert);
                if ($this->mxdst->execute($stmtInsert, $values)) {
                    $insertedrows += $this->mxdst->affected_rows();
                }
            }
        }
        echo "Aantal records geupdate {$updatedrows}\n";
        echo "Aantal records geinsert {$insertedrows}\n";
    }

    function processlocations() {
        $sql = "SELECT * FROM aa_locations";
        $locations = $this->dst->GetAll($sql);
    }

    function removeSubTableRecords($destinationtable) {
        // verwijder alle rijen, omdat de record van de subtabellen lastig te updaten zijn omdat
        // de id's matchen met meerdere subrecords
        $deletesql = "DELETE FROM {$destinationtable} WHERE CLIENT_HASH = '{$this->clienthash}'";
        $this->mxdst->Execute($deletesql);
        echo "{$this->mxdst->affected_rows()} records verwijderd uit {$destinationtable}\n";
    }

    function _processSubtable($mapping, $sourcetable, $destinationtable, $destinationkeycolumn) {
        // Haal alle rijen op uit de bron-tabel
        $sql = "
        SELECT
            ttt.*,
            CASE
                WHEN ttt.post_status = 'publish' THEN 0
                WHEN ttt.post_status = 'trash' THEN 1
                ELSE NULL  -- Handle other post statuses if needed
            END AS post_status
        FROM {$sourcetable} ttt;
        ";
        $rows = $this->dst->GetAll($sql);

        $updatedrows = 0;
        $insertedrows = 0;
        // Loop door elke rij
        foreach ($rows as $row) {
            // We nemen aan dat er maximaal 10 contactpersonen per client kunnen zijn
            // (client_contacts_0_xxxx tot client_contacts_9_xxxx)
            for ($n = 0; $n < 20; $n++) {
                $contact_data = [];
                foreach ($mapping as $destination_column => $source_info) {
                    // Controleer of de mapping een array is met 'LOOKUP'
                    if (is_string($source_info) && substr($source_info, 0, 1) == "'") {
                        // fixed value
                        $contact_data[$destination_column] = substr($source_info, 1, -1); // Haal de quotes weg
                    } elseif (is_array($source_info) && $source_info[0] === 'LOOKUP') {
                        $lookup_table = $source_info[1]['table'];
                        $lookup_column = $source_info[1]['column'];
                        $lookup_field = $source_info[1]['field'];
                         // Vervang {n} in het veld voor lookup
                        $lookup_field = str_replace('{n}', $n, $lookup_field);

                        if (substr($lookup_field, 0, 5) == 'JSON:') {
                            // JSON value
                            $lookup_field = substr($lookup_field, 5);
                            if (isset($row[$lookup_field])) {
                                $res = $this->serialized($row[$lookup_field]);
                                if ($res) {
                                    // pr($row, $lookup_field, $lookup_value, $row[$lookup_field], $res);
                                    $values = implode(", ", $res);
                                    $sqlLookup = "SELECT {$lookup_column} FROM {$lookup_table} WHERE __MIGRATE_ID IN ($values)";
                                    $stmtLookup = $this->mxdst->prepare($sqlLookup);
                                    $lookup_result = $this->mxdst->GetCol($stmtLookup);
                                    // pr($lookup_result);
                                    if ($lookup_result) {
                                        $lookup_result = json_encode($lookup_result);
                                    } else {
                                        $lookup_result = '[]';
                                    }
                                } else {
                                    $lookup_result = '[]';
                                }
                            } else {
                                $lookup_result = '[]';
                            }

                            $contact_data[$destination_column] = $lookup_result;
                        } else {

                            if (isset($row[$lookup_field])) {
                                $lookup_value = $row[$lookup_field];
                                // Voer de lookup uit
                                $lookup_sql = "SELECT {$lookup_column} FROM {$lookup_table} WHERE __MIGRATE_ID = '{$lookup_value}'";
                                $lookup_result = $this->mxdst->GetOne($lookup_sql);

                                if ($lookup_result !== false) {
                                    $contact_data[$destination_column] = $lookup_result;
                                } else {
                                    $contact_data[$destination_column] = null;
                                }
                            } else {
                                // Als het lookup_field niet bestaat in de $row, zet de waarde op null
                                $contact_data[$destination_column] = null;
                            }
                        }
                    } elseif (is_string($source_info) && strpos($source_info, '{n}') !== false) {
                        // Indien string begint met 'REQ:' dan moet de value niet leeg zijn
                        // anders de twee loops verlaten
                        if (substr($source_info, 0, 4) == 'REQ:') {
                            $source_column = substr($source_info, 4);
                            $source_column = str_replace('{n}', $n, $source_column);

                            if (empty($row[$source_column])) {
                                break 2;
                            }
                        } elseif (substr($source_info, 0, 5) == 'JSON:') {
                            // JSON value
                            $source_column = substr($source_info, 5);
                            $source_column = str_replace('{n}', $n, $source_column);
                        } elseif (substr($source_info, 0, 5) == 'DATE:') {
                            // DATE value
                            $source_column = substr($source_info, 5);
                            $source_column = str_replace('{n}', $n, $source_column);
                        } else {
                            // Vervang '{n}' met het huidige nummer
                            $source_column = str_replace('{n}', $n, $source_info);
                        }

                        if (isset($row[$source_column])) {
                            if (is_string($source_info) && substr($source_info, 0, 5) == 'JSON:') {
                                // JSON value
                                if (isset($row[$source_column])) {
                                    $res = $this->serialized_to_json($row[$source_column]);
                                    if ($res) {
                                        $contact_data[$destination_column] = $res;
                                    } else {
                                        $contact_data[$destination_column] = null;
                                    }
                                }
                            } elseif (is_string($source_info) && substr($source_info, 0, 5) == 'DATE:') {
                                $contact_data[$destination_column] = DateTime::createFromFormat('Ymd', $row[$source_column])->format('Y-m-d');
                            } else {
                                $contact_data[$destination_column] = $row[$source_column];
                            }

                        } else {
                            $contact_data[$destination_column] = null;
                        }
                    } elseif (is_string($source_info)) {
                        if (isset($row[$source_info])) {
                            $contact_data[$destination_column] = $row[$source_info];
                        } else {
                            $contact_data[$destination_column] = null;
                        }
                    } else {
                        $contact_data[$destination_column] = $source_info;
                    }
                }

                // Controleer of de contactpersoon al bestaat op basis van 'deelnemer'
                $contactId = $contact_data['__MIGRATE_ID'];
                $sqlCheck = "SELECT {$destinationkeycolumn} FROM {$destinationtable} WHERE __MIGRATE_ID = ?";
                $stmtCheck = $this->mxdst->prepare($sqlCheck);
                $existingSubRecord = $this->mxdst->GetOne($stmtCheck, [$contactId]);


                // Voer de insert uit in de bestemmingstabel
                $columns = implode(", ", array_keys($contact_data));
                $values = implode(", ", array_map([$this->dst, 'qstr'], array_values($contact_data)));

                $insert_sql = "INSERT INTO {$destinationtable} ({$columns}) VALUES ({$values})";
                try {
                    if ($this->mxdst->execute($insert_sql)) {
                        $insertedrows += $this->mxdst->affected_rows();
                    }
                } catch (Exception $e) {
                    if ($e->getCode() == 1048) {
                        // echo "Error: " . $e->getMessage() . "\n";
                    } else {
                        throw new Exception($e->getMessage());
                    }
                }
            }
        }

        // echo "Aantal records geupdate {$updatedrows}\n";
        echo "Aantal records geinsert {$insertedrows} aan {$destinationtable} \n\n";
    }

    function processclients() {
        $mapping = [
            '__MIGRATE_ID' => 'post_title',
            '__CREATEDON' => 'post_date',
            '__CHANGEDON' => 'post_modified',
            '__DELETED' => 'post_status',
            'client_hash' => "'{$this->clienthash}'",
            'deelnemer_code' => 'client_id',
            'voornaam' => 'client_first_name',
            'voorvoegsel' => 'client_prefix',
            'achternaam' => 'client_last_name',
            'geboortedatum' => 'client_date_of_birth',
            'geslacht' => [
                'client_gender', [
                    1 => 'V',
                    2 => 'M',
                    3 => 'X',
                    4 => 'O',
                    '' => 'O', // Geen geslacht dan O(nbekend) aannemen
                ]
            ],
            'straat' => 'client_street',
            'huisnummer' => 'client_street_number',
            'postcode' => 'client_postal_code',
            'stad' => 'client_city',
            'land' => 'CLEANCOUNTRY:client_country',
            '__MIGRATE_LAND' => 'client_country',
            'telefoon' => 'client_telephone',
            'email' => 'client_email',
            'gemeente' => 'client_municipality',
            // 'land_van_herkomst' => 'client_country_of_origin',
            'land_van_herkomst' => 'CLEANCOUNTRY:client_country_of_origin',
            '__MIGRATE_LAND_VAN_HERKOMST' => 'client_country_of_origin',
            'instroom_datum' => 'client_date_of_entry',
            'locatie' => 'client_main_location',
            'uitstroom_datum' => 'client_date_of_exit',
            'uitstroom_reden' => [
                'client_reason_for_exit', [
                    0 => 7,
                    1 => 8,
                    2 => 9,
                    3 => 10,
                    4 => 11
                ]
            ],
            'uitstroom_notitie' => 'client_exit_note',
            'meenemen_in_meting' => [
                'client_include_in_measurement', [
                    1 => 'N',
                    2 => 'Y',
                ]
            ],
            'bsn' => 'client_bsn',
            'iban' => 'client_iban',
            'dagelijkse_reiskostenvergoeding' => 'client_daily_travel_allowances',
            'zorgverzekering' => 'client_health_insurance',
            'zorgverzekering_polisnummer' => 'client_insurance_number',
            'deelnemer_status' => 'client_status',
            'traject' => 'client_trajectory',
            'oude_wmo_financieringsmethodes' => [
                'LOOKUP', [
                    'table' => 'do_financieringsmethodes',
                    'column' => 'financieringsmethode_id',
                    'field' => 'JSON:client_wmo',
                ]
            ],
        ];

        echo "Clients aan het verwerken \n";
        $mapping['locatie'] = "'6'";  // Athos Maastricht
        $this->_processTable($mapping, 'aa_clients', 'do_deelnemers', 'deelnemer_id');

        echo "Clients 2 aan het verwerken \n";
        $mapping['locatie'] = "'7'";  // Porthos Maastricht
        $this->_processTable($mapping, 'aa_clients_2', 'do_deelnemers', 'deelnemer_id');

        echo "Clients 3 aan het verwerken \n";
        $mapping['locatie'] = "'8'";  // Porthos Watersley
        $this->_processTable($mapping, 'aa_clients_3', 'do_deelnemers', 'deelnemer_id');

        $this->processcontacts();
        $this->processagreements();
        $this->processtrajects();
        $this->processstatuses();
    }

    function processcontacts() {
        $mapping = [
            '__MIGRATE_ID' => 'post_title',
            '__CREATEDON' => 'post_date',
            '__CHANGEDON' => 'post_modified',
            '__DELETED' => 'post_status',
            'client_hash' => "'{$this->clienthash}'",
            'deelnemer' => [
                'LOOKUP', [
                    'table' => 'do_deelnemers',
                    'column' => 'deelnemer_id',
                    'field' => 'post_title',
                ]
            ],
            'naam' => 'REQ:client_contacts_{n}_name',
            'adres' => 'client_contacts_{n}_address',
            'telefoon' => 'client_contacts_{n}_telephone',
            'email' => '',
            'relatie' => 'client_contacts_{n}_relation',
        ];

        $this->removeSubTableRecords('do_contactpersonen');

        echo "Contacten aan het verwerken \n";
        $this->_processSubtable($mapping, 'aa_clients', 'do_contactpersonen', 'contactpersoon_id');

        echo "Contacten 2 aan het verwerken \n";
        $this->_processSubtable($mapping, 'aa_clients_2', 'do_contactpersonen', 'contactpersoon_id');

        echo "Contacten 3 aan het verwerken \n";
        $this->_processSubtable($mapping, 'aa_clients_3', 'do_contactpersonen', 'contactpersoon_id');
    }

    function processagreements() {
        $mapping = [
            '__MIGRATE_ID' => 'post_title',
            '__CREATEDON' => 'post_date',
            '__CHANGEDON' => 'post_modified',
            '__DELETED' => 'post_status',
            'client_hash' => "'{$this->clienthash}'",
            'deelnemer' => [
                'LOOKUP', [
                    'table' => 'do_deelnemers',
                    'column' => 'deelnemer_id',
                    'field' => 'post_title',
                ]
            ],
            'ontwikkeltypes' => [
                'LOOKUP', [
                    'table' => 'fb_ontwikkelsferen',
                    'column' => 'ontwikkelsfeer_id',
                    'field' => 'JSON:client_event_agreements_{n}_event_types',
                ]
            ],
            'datum_vanaf' => 'DATE:client_event_agreements_{n}_from',
            'aantal_activiteiten' => 'client_event_agreements_{n}_number_of_events',
        ];

        $this->removeSubTableRecords('do_deelnemerontwikkelafspraken');

        echo "Overeenkomsten aan het verwerken \n";
        $this->_processSubtable($mapping, 'aa_clients', 'do_deelnemerontwikkelafspraken', 'deelnemerontwikkelafspraak_id');

        echo "Overeenkomsten 2 aan het verwerken \n";
        $this->_processSubtable($mapping, 'aa_clients_2', 'do_deelnemerontwikkelafspraken', 'deelnemerontwikkelafspraak_id');

        echo "Overeenkomsten 3 aan het verwerken \n";
        $this->_processSubtable($mapping, 'aa_clients_3', 'do_deelnemerontwikkelafspraken', 'deelnemerontwikkelafspraak_id');
    }

    function processtrajects() {
        $mapping = [
            '__MIGRATE_ID' => 'post_title',
            '__CREATEDON' => 'post_date',
            '__CHANGEDON' => 'post_modified',
            '__DELETED' => 'post_status',
            'client_hash' => "'{$this->clienthash}'",
            'deelnemer' => [
                'LOOKUP', [
                    'table' => 'do_deelnemers',
                    'column' => 'deelnemer_id',
                    'field' => 'post_title',
                ]
            ],
            'traject' => 'client_history_trajectories_{n}_trajectory',
            'datum_vanaf' => 'DATE:client_history_trajectories_{n}_from',
        ];

        $this->removeSubTableRecords('do_deelnemertrajecten');

        echo "Trajecten aan het verwerken \n";
        $this->_processSubtable($mapping, 'aa_clients', 'do_deelnemertrajecten', 'deelnemertraject_id');

        echo "Trajecten 2 aan het verwerken \n";
        $this->_processSubtable($mapping, 'aa_clients_2', 'do_deelnemertrajecten', 'deelnemertraject_id');

        echo "Trajecten 3 aan het verwerken \n";
        $this->_processSubtable($mapping, 'aa_clients_3', 'do_deelnemertrajecten', 'deelnemertraject_id');
    }

    function processstatuses() {
        $mapping = [
            '__MIGRATE_ID' => 'post_title',
            '__CREATEDON' => 'post_date',
            '__CHANGEDON' => 'post_modified',
            '__DELETED' => 'post_status',
            'client_hash' => "'{$this->clienthash}'",
            'deelnemer' => [
                'LOOKUP', [
                    'table' => 'do_deelnemers',
                    'column' => 'deelnemer_id',
                    'field' => 'post_title',
                ]
            ],
            'status' => 'client_history_statuses_{n}_status',
            'datum_vanaf' => 'DATE:client_history_statuses_{n}_from',
        ];

        $this->removeSubTableRecords('do_deelnemerstatussen');

        echo "Statussen aan het verwerken \n";
        $this->_processSubtable($mapping, 'aa_clients', 'do_deelnemerstatussen', 'deelnemerstatus_id');

        echo "Statussen 2 aan het verwerken \n";
        $this->_processSubtable($mapping, 'aa_clients_2', 'do_deelnemerstatussen', 'deelnemerstatus_id');

        echo "Statussen 3 aan het verwerken \n";
        $this->_processSubtable($mapping, 'aa_clients_3', 'do_deelnemerstatussen', 'deelnemerstatus_id');
    }

    function processnotes() {
        $mapping = [
            '__MIGRATE_ID' => 'post_title', // 'note_id
            'client_hash' => "'{$this->clienthash}'",
            'gebruiker' => 'post_author',
            'gebruikernaam' => 'user_login',
            'deelnemer' => [
                'LOOKUP', [
                    'table' => 'do_deelnemers',
                    'column' => 'deelnemer_id',
                    'field' => 'note_client',
                ]
            ],
            'notitie' => 'note_text',
            '__CREATEDON' => 'post_date',
            '__CHANGEDON' => 'post_modified',
            '__DELETED' => 'post_status',
        ];

        echo "Notities aan het verwerken \n";
        $this->_processTable($mapping, 'aa_notes', 'do_notities', 'notitie_id');

        echo "Notities 2 aan het verwerken \n";
        $this->_processTable($mapping, 'aa_notes_2', 'do_notities', 'notitie_id');

        echo "Notities 3 aan het verwerken \n";
        $this->_processTable($mapping, 'aa_notes_3', 'do_notities', 'notitie_id');
    }

    function processtargets() {
        $mapping = [
            '__MIGRATE_ID' => 'post_title', // 'target_id
            'client_hash' => "'{$this->clienthash}'",
            'gebruiker' => 'post_author',
            'gebruikernaam' => 'user_login',
            'deelnemer' => [
                'LOOKUP', [
                    'table' => 'do_deelnemers',
                    'column' => 'deelnemer_id',
                    'field' => 'target_client',
                ]
            ],
            'doelstelling' => 'target_targets',
            'afgerond' => [
                'target_completed', [
                    0 => 'N', // Als de waarde '0' is, dan 'N'
                    1 => 'Y', // Als de waarde '1' is, dan 'Y'
                    '' => 'N' // Als geen waarde, dan 'N'
                ]
            ],
            '__CREATEDON' => 'post_date',
            '__CHANGEDON' => 'post_modified',
            '__DELETED' => 'post_status',
        ];

        echo "Doelstellingen aan het verwerken \n";
        $this->_processTable($mapping, 'aa_targets', 'do_doelstellingen', 'doelstelling_id');

        echo "Doelstellingen 2 aan het verwerken \n";
        $this->_processTable($mapping, 'aa_targets_2', 'do_doelstellingen', 'doelstelling_id');

        echo "Doelstellingen 3 aan het verwerken \n";
        $this->_processTable($mapping, 'aa_targets_3', 'do_doelstellingen', 'doelstelling_id');
    }

    function processallowances() {
        $mapping = [
            '__MIGRATE_ID' => 'post_title', // 'target_id
            'client_hash' => "'{$this->clienthash}'",
            'gebruiker' => 'post_author',
            'gebruikernaam' => 'user_login',
            'prijs' => 'allowance_amount',
            'type_vergoeding' => 'allowance_type',
            'deelnemer' => [
                'LOOKUP', [
                    'table' => 'do_deelnemers',
                    'column' => 'deelnemer_id',
                    'field' => 'allowance_client',
                ]
            ],
            'omschrijving' => 'allowance_description',
            '__CREATEDON' => 'post_date',
            '__CHANGEDON' => 'post_modified',
            '__DELETED' => 'post_status',
        ];

        echo "Vergoedingen aan het verwerken \n";
        $this->_processTable($mapping, 'aa_allowances', 'do_vergoedingen', 'vergoeding_id');
    }

    function processevents() {
        $mapping = [
            '__MIGRATE_ID' => 'id',
            'client_hash' => "'{$this->clienthash}'",
            'gebruiker' => 'post_author',
            'gebruikernaam' => 'user_login',
            'locatie' => "'6'",
            'groepsgrootte' => 'event_group_size',
            'tijdstip_aanvang' => 'event_start',
            'tijdstip_einde' => 'event_end',
            'ontwikkelsfeer' => [
                'LOOKUP', [
                    'table' => 'fb_ontwikkelsferen',
                    'column' => 'ontwikkelsfeer_id',
                    'field' => 'event_type',
                ]
            ],
            '__CREATEDON' => 'post_date',
            '__CHANGEDON' => 'post_modified',
            '__DELETED' => 'post_status',
        ];

        $mapping['locatie'] = "'6'";  // Athos Maastricht
        echo "Activiteiten aan het verwerken \n";
        $this->_processTable($mapping, 'aa_events', 'do_activiteiten', 'activiteit_id');

        $mapping['locatie'] = "'7'";  // Porthos Maastricht
        echo "Activiteiten 2 aan het verwerken \n";
        $this->_processTable($mapping, 'aa_events_2', 'do_activiteiten', 'activiteit_id');

        $mapping['locatie'] = "'8'";  // Porthos Watersley
        echo "Activiteiten 3 aan het verwerken \n";
        $this->_processTable($mapping, 'aa_events_3', 'do_activiteiten', 'activiteit_id');
    }

    function processeventclients() {
        $mapping = [
            '__MIGRATE_ID' => 'id',
            'client_hash' => "'{$this->clienthash}'",
            'activiteit' => [
                'LOOKUP', [
                    'table' => 'do_activiteiten',
                    'column' => 'activiteit_id',
                    'field' => 'id',
                ]
            ],
            'deelnemer' => [
                'LOOKUP', [
                    'table' => 'do_deelnemers',
                    'column' => 'deelnemer_id',
                    'field' => 'event_clients_{n}_client',
                ]
            ],
            'aanwezigheid_status' => 'event_clients_{n}_status',
            '__CREATEDON' => 'post_date',
            '__CHANGEDON' => 'post_modified',
            '__DELETED' => 'post_status',
        ];

        $this->removeSubTableRecords('do_activiteit_deelnemers');

        echo "Activiteit deelnemers aan het verwerken \n";
        $this->_processSubTable($mapping, 'aa_events', 'do_activiteit_deelnemers', 'activiteit_deelnemer_id');

        $this->EnsureConnection(); /* because of lengthy process, recheck the connection */

        echo "Activiteit deelnemers 2 aan het verwerken \n";
        $this->_processSubTable($mapping, 'aa_events_2', 'do_activiteit_deelnemers', 'activiteit_deelnemer_id');

        $this->EnsureConnection(); /* because of lengthy process, recheck the connection */

        echo "Activiteit deelnemers 3 aan het verwerken \n";
        $this->_processSubTable($mapping, 'aa_events_3', 'do_activiteit_deelnemers', 'activiteit_deelnemer_id');
    }

    function processwmoprices() {
        $mapping = [
            '__MIGRATE_ID' => 'id',
            'client_hash' => "'{$this->clienthash}'",
            'financieringsmethode' => 'post_title', // Assuming 'post_title' contains information about the financing method
            'wmo_prijs' => 'wmo_prices_price',
            'wmo_productcode' => 'wmo_prices_productcode',
            '__CREATEDON' => 'post_date',
            '__CHANGEDON' => 'post_modified',
            '__DELETED' => 'post_status',
        ];

        echo "WMO prijzen aan het verwerken \n";
        $this->_processTable($mapping, 'aa_wmoprices', 'do_financieringsmethodes', 'financieringsmethode_id');
    }

    function processpositivehealth() {
        $mapping = [
            '__MIGRATE_ID' => 'id',
            'client_hash' => "'{$this->clienthash}'",
            'gebruiker' => 'post_author',
            'gebruikernaam' => 'user_login',
            'deelnemer' => [
                'LOOKUP', [
                    'table' => 'do_deelnemers',
                    'column' => 'deelnemer_id',
                    'field' => 'positivehealth_client',
                ]
            ],
            'lichaamsfuncties' => 'positivehealth_physical_functioning',
            'mentaal_welbevinden' => 'positivehealth_mental_health',
            'zingeving' => 'positivehealth_meaning',
            'sociale_participatie' => 'positivehealth_social_participation',
            'kwaliteit_van_leven' => 'positivehealth_quality_of_life',
            'dagelijks_functioneren' => 'positivehealth_daily_routine',
            'gevoel' => 'positivehealth_feeling',
            'notitie' => 'positivehealth_notes',
            'locatie' => 'positivehealth_location',
            'ingevuld_door' => 'positivehealth_filled_in_by',
            'status' => 'positivehealth_status',
            'versie' => 'positivehealth_version',
            '__CREATEDON' => 'post_date',
            '__CHANGEDON' => 'post_modified',
            '__DELETED' => 'post_status',
        ];

        echo "Positieve gezondheid aan het verwerken \n";
        $this->_processTable($mapping, 'aa_positivehealth', 'do_positievegezondheid', 'positievegezondheid_id');

        echo "Positieve gezondheid 2 aan het verwerken \n";
        $this->_processTable($mapping, 'aa_positivehealth_2', 'do_positievegezondheid', 'positievegezondheid_id');

        echo "Positieve gezondheid 3 aan het verwerken \n";
        $this->_processTable($mapping, 'aa_positivehealth_3', 'do_positievegezondheid', 'positievegezondheid_id');
    }

    function cleanupdata() {

    }

    function processall() {
        $this->processwmoprices();
        $this->processclients();
        $this->processpositivehealth();
        $this->processnotes();
        $this->processtargets();
        $this->processallowances();
        $this->processevents();
        $this->processeventclients();
        $this->cleanupdata();
    }

}