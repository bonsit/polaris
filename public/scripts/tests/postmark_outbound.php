<?php
require "../includes/app_includes.inc.php";

if (!isset($_SERVER['PHP_AUTH_USER']) OR !isset($_SERVER['PHP_AUTH_PW'])
OR $_SERVER['PHP_AUTH_USER'] !== $_CONFIG['postmark_webhook_user']
OR $_SERVER['PHP_AUTH_PW'] !== $_CONFIG['postmark_webhook_password']
) {
    header('WWW-Authenticate: Basic realm="Maxia"');
    header('HTTP/1.0 401 Unauthorized');
    echo 'Please provide valid credentials.';
    exit;
}

// Grab the JSON data from the request
$data = json_decode(file_get_contents('php://input'), true);

/**
 * Create the Polaris object
 */
$polaris = new base_Polaris();
$polaris->initialize();
$polaris->setupSimpleSession();

$_actionDate = $data['ReceivedAt']??$data['DeliveredAt']??$data['BouncedAt'];
$_recipient = $data['Recipient']??$data['Email'];

$json = json_encode($data);
$_insertSQL = "
    INSERT INTO plr_logmail
    (action, messageid, receivedat, recipient, message)
    VALUES
    ('{$data['RecordType']}', '{$data['MessageID']}', '{$_actionDate}', '{$_recipient}', '".$json."' )";

try {
    $polaris->instance->execute($_insertSQL);
} catch (Exception $e) {
    echo $e->getMessage();
}
