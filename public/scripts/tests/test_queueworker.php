<?php
require_once "../../includes/app_includes.inc.php";

$polaris = new base_Polaris();
$polaris->initialize();

function QueueTestUitnodiging($email, $values, $deelnemerid, $recordid) {
    global $polaris;
    global $_GVARS;

    $values['FORMHASH'] = '641462623c3309b7e5c8736773439d22';
    $values['ACCEPTLINK'] = $_GVARS['serverroot']."/testing/";
    $polaris->AddToQueue('send_deelnemeruitnodiging', array('email' => $email, 'insertValues' => $values));
}


// function queue_testaction($data) {
//     # Do something with the #data
//     ...

//     # Always return true, when everything is handled correctly
//     return true;
// }

$_email = 'test@bonsit.nl';
$_values = [];
$_deelnemerid = '';
$_recordid = '';

QueueTestUitnodiging($_email, $_values, $_deelnemerid, $_recordid);