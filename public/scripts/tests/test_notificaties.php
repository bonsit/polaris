<?php
require_once "includes/app_includes.inc.php";

$polaris = new base_Polaris();
$polaris->initialize();

$clientid = 2;
$_client = new base_plrClient($polaris);
$_client->LoadRecord(array($clientid));

use SpryngApiHttpPhp\Client;
$spryng = new Client($_CONFIG['spryng_username'], $_CONFIG['spryng_password'], $_CONFIG['spryng_companyname']);

try {
    $result = $spryng->sms->send('<phonenumber>', 'Uw inlog-code voor PZN: 29419 (tien minuten geldig)', array(
        'route' => 'ECONOMY',
        'allowlong' => true,
        'reference' => 'ABC123456789')
    );
    var_dump($result);
} catch (InvalidRequestException $e) {
    echo $e->getMessage();
}

var_dump($_CONFIG['spryng_username'], $_CONFIG['spryng_password'], $_CONFIG['spryng_companyname']);