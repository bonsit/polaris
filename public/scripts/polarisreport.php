<?php
require "../includes/app_includes.inc.php";

/**
 * Create the Polaris object
 */
$polaris = new base_Polaris();
$polaris->setupSimpleSession();

// Download mariadb driver from https://mariadb.com/download-confirmation/?group-name=Data%20Access&release-notes-uri=https%3A%2F%2Fmariadb.com%2Fkb%2Fen%2Flibrary%2Fmariadb-connector-j-release-notes%2F&documentation-uri=https%3A%2F%2Fmariadb.com%2Fkb%2Fen%2Flibrary%2Fmariadb-connector-j%2F&download-uri=https%3A%2F%2Fdlm.mariadb.com%2F3418098%2FConnectors%2Fjava%2Fconnector-java-3.2.0%2Fmariadb-java-client-3.2.0-sources.jar&product-name=Java%208%2B%20connector&download-size=445.58%20KB

$params = $_GET;
$_nodebug = ($_GET['debug']??'') !== 'true';

unset($params['report']);
unset($params['output']);
unset($params['firsttime']);
unset($params['debug']);
unset($params['event']);

if ($_nodebug)
    header('Content-type: application/pdf');

use PHPJasper\PHPJasper;

$jdbc_dir = __DIR__ . '/../vendor/geekcom/phpjasper/bin/jasperstarter/jdbc';
if (!empty($_SESSION['userid'])) {
    $_tmpfolder = $_SESSION['clienthash'].'/'.$_SESSION['userid'].'/';
} else {
    $_tmpfolder = 'nosession';
}

$outputfolder = '/tmp/polaris/rpt/'.$_tmpfolder;
if (!file_exists($outputfolder)) {
    mkdir($outputfolder, 0777, true);
}
$outputfile = $outputfolder.$_GET['report'];

$jasper = new PHPJasper;

$input = __DIR__ . '/../modules/_shared/module.plr_maxia/rpt/'.$_GET['report'].'.jrxml';
if ($_nodebug) {
    // Only compile to jasper files on the Dev environment or when the jasper file does not exist
    // For now, in the pipeline, we delete the jasper files from the tmp folder
    if (($_ENV['ENVIRONMENT'] == 'dev')
    or !file_exists($outputfile.'.jasper')) {
        $jasper->compile($input, $outputfolder)->execute();
    }
} else {
    echo '<pre>' . $jasper->compile($input, $outputfolder)->output() . '</pre>';
}

$input = $outputfile.'.jasper';
$options = [
    'format' => ['pdf'],
    'locale' => 'nl',
    'params' => $params,
    'db_connection' => [
        'driver' => 'generic',
        'host' => 'db-clients',
        'port' => '3306',
        'database' => $_ENV['DB_CLIENTS'],
        'username' => $_ENV['DB_CLIENTS_USER'],
        'password' => $_ENV['DB_CLIENTS_PASSWORD'],
        'jdbc_driver' => 'com.mysql.cj.jdbc.Driver',
        'jdbc_url' => 'jdbc:mysql://db-clients:3306/'.$_ENV['DB_CLIENTS'].'?useSSL=false',
        'jdbc_dir' => $jdbc_dir
    ]
];
if ($_nodebug) {
    $jasper->process($input, $outputfolder, $options)->execute();
    $reporturl = $outputfile.'.pdf';
    echo file_get_contents($reporturl);
} else {
    echo '<pre>' . $jasper->process($input, $outputfolder, $options)->output() . '</pre>';
    echo '<pre>' . $outputfile.'.pdf' . '</pre>';
}
