{include file="header.tpl.php"}
<script type="text/javascript" src="{$serverpath}/assets/dist/modeler.js"></script>

{if isset($currenttable)}
<div id="ftdcontainer"><div class="diagram"></div><div class="sentencepattern"></div></div>
{/if}

{if !$onlyDisplayFTD}
<ul style="width:200px;float:left;"> 
{section name=i loop=$schema}
<li><a href="{$serverpath}/modeler/database/{$databasehash}/draw/{$schema[i].TABLENAME}/">{$schema[i].TABLENAME}</a></li>
{/section}
</ul>
{/if}

<script type="text/javascript">
var data = {assign var="arrayname" value=$currenttable}
{json array=$currenttable}
{*array2js name=$arrayname array=$currenttable*}

var ftdInit = {ldelim} 'object': data, 'canvas': $('#ftdcontainer') {rdelim};
</script>

{include file="footer.tpl.php"}