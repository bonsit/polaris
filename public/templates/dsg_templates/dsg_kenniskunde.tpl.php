{include file="basemenu.tpl.php" divcontent='class="contentwithhelp"'}
<style type="text/css" media="all">@import "css/plrkenniskunde.css";</style>
<script type="text/javascript" src="javascript/wz_jsgraphics.js"></script>

<h2>{lang database} '{$recset.DATABASENAME}'</h2>

{include file="databasemenu.tpl.php"}

{literal}
<script type="text/javascript">
<!--

deltakeyline = 10; 
delta = 150; 
deltasubset = 35;
deltasubsetspacing = 15;
deltasentence = 10;
keysize = 3;
minplaceholderwidth = 150;
nameletterwidth = 10;
placeholderheight = 50;
subsetradius = 15;
ftdtopspace = 20;
ftdnamespace = 60;
nnwidth = 10;
nnheight = 7;
subsetcolors = new Array(5);
subsetcolors[1] = '#639a91';
subsetcolors[2] = '#6ddd28';
subsetcolors[3] = '#402222';
subsetcolors[4] = '#402222';

function DrawArrow(canvas, x, y, type) {
  if (type == 'right') {
    x = x + delta;
    canvas.fillPolygon(new Array(x,x-keysize,x-keysize), new Array(y-deltakeyline,y-deltakeyline+keysize,y-deltakeyline-keysize));
  } else {
    canvas.fillPolygon(new Array(x,x+keysize,x+keysize), new Array(y-deltakeyline,y-deltakeyline-keysize,y-deltakeyline+keysize));
  }
}

function DrawConstraint(canvas, constraint, x, y, text) {
  if (constraint == 'listvalue') {
    x = x + delta;
    listvalue = '{' + text + '}';
    canvas.drawStringRect(listvalue, x - minplaceholderwidth, y - 20, delta, 'right');
  }
}

function DrawPrimaryKey(canvas, x, y, keyindex, lastplaceholder) {
	canvas.drawLine(x, y - deltakeyline, x + minplaceholderwidth, y - deltakeyline);
	if (keyindex == 1) {
		DrawArrow(canvas, x, y, 'left');
	}
	if ( lastplaceholder ) {
		DrawArrow(canvas, x, y, 'right');
	}
}

function DrawFTD(canvas, ftd)
{
    var x = 0;
    var y = ftdtopspace;
  
    var FTDName = ftd['FTDNAME'];
//    canvas.setFont("Verdana","80%",Font.PLAIN);
 
    canvas.setColor("#555"); // gray
//    canvas.setFont("Verdana","1.2em",Font.BOLD);
    canvas.drawStringRect(FTDName, x, y, 400, 'left');
  
  if (ftd['columns']) {
		var collength = ftd['columns'].length;
		y = y + ftdnamespace;
		var subsets = new Array();
		var keyindex = 1;
    	var placeholderwidth = minplaceholderwidth;
    	var placeholdername = datatype = kkdatatype = '';
		for(i=0;i<collength;i++) {
	//       placeholderwidth = Math.max(placeholdername.length * nameletterwidth, minplaceholderwidth);
			switch (ftd['columns'][i]['DATATYPE']) {
			case 'C': kkdatatype = 'tekenreeks'; break;
			case 'X': kkdatatype = 'tekst'; break;
			case 'D': kkdatatype = 'datum'; break;
			case 'T': kkdatatype = 'tijd'; break;
			case 'I': kkdatatype = 'numeriek'; break;
			default:  kkdatatype = ftd['columns'][i]['DATATYPE'];
			}
			datatype = kkdatatype;
			if ( ftd['columns'][i]['DATATYPE'] == 'C' || ftd['columns'][i]['DATATYPE'] == 'I' ) {
				datatype += '(' + ftd['columns'][i]['TOTALLENGTH'];
				if (ftd['columns'][i]['DECIMALCOUNT'] > 0)
					datatype += ',' + ftd['columns'][i]['DECIMALCOUNT'];
				datatype += ')';
			}
			canvas.setColor("#999");
//			canvas.setFont("Verdana","0.75em",Font.PLAIN);
			canvas.drawStringRect(datatype, x + 5, y, placeholderwidth-5, 'left');
	
			canvas.setColor("#555"); 
			placeholdername = ftd['columns'][i]['COLUMNNAME'];
			if (placeholdername) {
                placeholdername = placeholdername.toLowerCase();
//                placeholdername = placeholdername.ucFirst();
			}
			canvas.drawRect(x, y, placeholderwidth, placeholderheight);
	
			canvas.setColor("#555"); 
//			canvas.setFont("Verdana","0.9em",Font.PLAIN);
			canvas.drawStringRect(placeholdername, x, y + 15, placeholderwidth, 'center');
			
			/* Draw notnull constraint */
			if (ftd['columns'][i]['REQUIRED'] == 'Y' && ftd['columns'][i]['KEYCOLUMN'] != 'Y') {
				canvas.fillRect(x + delta - nnwidth, y + placeholderheight - nnheight, nnwidth, nnheight);
			}
			
			/* Draw listvalue constraint */
			if (ftd['columns'][i]['LISTVALUES'] != '') {
				DrawConstraint(canvas, 'listvalue', x, y, ftd['columns'][i]['LISTVALUES'] );
			}
	
			/* Draw key constraint */
			if (ftd['columns'][i]['KEYCOLUMN'] == 'Y') {
				lastplaceholder = !ftd['columns'][i+1] || (ftd['columns'][i+1] && ftd['columns'][i+1]['KEYCOLUMN'] != 'Y');
				DrawPrimaryKey(canvas, x, y, keyindex, lastplaceholder);
				keyindex++;
			}
			/* Draw subsets */
			if (ftd['columns'][i]['SUBSETID'] != '') {
				subset = ftd['columns'][i]['SUBSETID'];
				canvas.setColor(subsetcolors[1]); 
				if (isUndefined(subsets[subset])) subsets[subset] = 0;
				else subsets[subset] = subsets[subset] + 1;
				if (subsets[subset] == 0) {
					stable = ftd['columns'][i]['SUPERTABLENAME'];
					scolumn = ftd['columns'][i]['SUPERCOLUMNNAME'];
					subsettext  = 'Deelverzamelingsregel ' + subset + '<br />';
					subsettext += 'subkant: ' + ftd['TABLENAME'] + '.' + placeholdername + '<br />';
					subsettext += 'superkant: ' + stable + '.' + scolumn;
					subsetlink = '<a href="#link_'+stable+'" onmouseover="this.T_WIDTH=200;this.T_FONTCOLOR=\'#003399\';return escape(\''+subsettext+'\')">dvr ' + subset + '</a>'; 
					canvas.drawEllipse(x + (placeholderwidth / 2) - (subsetradius / 2), y - deltasubset, subsetradius, subsetradius);
					canvas.drawLine(x + (placeholderwidth / 2), y - deltasubset + subsetradius, x + (placeholderwidth / 2), y);
					canvas.drawLine(x + (placeholderwidth / 2) - 5, y - deltasubset + subsetradius -5 , x + (placeholderwidth / 2), y - subsetradius * 2);
					canvas.drawLine(x + (placeholderwidth / 2) + 5, y - deltasubset + subsetradius -5 , x + (placeholderwidth / 2), y - subsetradius * 2);
					canvas.drawStringRect(subsetlink, x + (placeholderwidth / 2) + subsetradius + 0, y - deltasubset, placeholderwidth, 'left');
				} else {
					canvas.drawLine(x + (placeholderwidth / 2), y - deltasubsetspacing, x + (placeholderwidth / 2), y);
					canvas.drawLine(x + (placeholderwidth / 2), y - deltasubsetspacing, x - placeholderwidth + (placeholderwidth / 2), y - deltasubsetspacing);
				}
			}
			
			x = x + placeholderwidth;
		}
	} else { 
//	  canvas.setFont("Verdana","1.0em",Font.PLAIN);
  	canvas.drawStringRect('Geen kolommen gedefinieerd.', 0, placeholderheight + deltasentence + y, 200, 'left');
		y = y + 40;		
	}
	
  if ( ftd['SENTENCEPATTERN'] ) {
    ZS = ftd['SENTENCEPATTERN'];
  } else {
    ZS = 'Geen aanwezig';
  }
//  canvas.setFont("Verdana","1.0em",Font.PLAIN);
  canvas.drawStringRect('Zinsjabloon: '+ZS, 0, placeholderheight + deltasentence + y, document.body.clientWidth*0.75, 'left');

  canvas.paint();

	aantalregels = Math.round(ZS.length / (document.body.clientWidth*0.75 / 8));
	ftdheight = placeholderheight + ftdnamespace + aantalregels * 15;
	return ftdheight;
}

//-->
</script>
{/literal}

{section name=i loop=$schema}
<div id="ftd_{$schema[i].TABLENAME}" style="position:relative;width:500px;height:100px;"><a name="link_{$schema[i].TABLENAME}"></a>
</div>
<div id="population_{$schema[i].TABLENAME}" style="position:relative;width:500px;height:50px"></div>
{assign var="arrayname" value=$schema[i].TABLENAME}

<script type="text/javascript">
<!--

{array2js name=$arrayname array=$schema[i]}

var doc = new jsGraphics("ftd_{$schema[i].TABLENAME}"); 

ftdheight = DrawFTD(doc, {$arrayname});
document.getElementById('ftd_{$schema[i].TABLENAME}').style.height = ftdheight + 'px';
//-->
</script>

{/section}

<script type="text/javascript" src="javascript/wz_tooltip.js"></script>

{include file="footer.tpl.php"}