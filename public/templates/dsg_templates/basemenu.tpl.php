{include file="header.tpl.php"}
<div id="headercomplete">
<div id="topcontent">
  <ul id="topinfo"><li class="first">{$smarty.now|date_format:$config.currentdatetimeformat}</li><li>{lang loggedinas} {$username}</li>
  {if !$config.intranetinstance}<li class="special"><a href="{$serverroot}/help.php" target="_blank">{lang help}</a></li>{/if}
  <li class="special"><a href="{$serverroot}/?method=logout">{lang logout}</a></li>
  {*<li class="special"><a href="#">{lang ctrlpanel}</a></li>*}
  {if $smarty.session.usertype == 'client' or $smarty.session.usertype == 'root'}
  <li class="special"><a href="{$serverroot}/allapps/">{lang polaris_apps}</a></li>
  {/if}
  </ul>

<!--<img class="plrlogo" src="images/polarissmall.gif" alt="Polaris {#PolarisVersion#}">-->
<div id="clienttitle"><span>
{lang polaris_main}<span id="version">&nbsp;{lang application_version} {#PolarisVersion#}</span></span></div>
</div>
</div>

{if $config.showuserimage}<div id="userimage"><div class="img" style="background-image:url({$serverroot}/userphotos/{$smarty.session.userphoto|default:"anon.jpg"})"></div>
{*<img src="{$serverroot}/userphotos/{$smarty.session.userphoto|default:"anon.jpg"}" />*}
{if $smarty.session.userphoto eq ''}<div class="caption">{$smarty.session.fullname|replace:" ":"<br />"}</div>{/if}
</div>{/if}

<div id="navcontainer" class="horizontal">
{if $skipmodules neq "true"}
{*include file="modules.tpl.php"*}
{if !$config.intranetinstance}<div id="mantislink">Problemen? <a href="rm18/login_page_polaris.php">Meldt het ons!</a></div>{/if}
{/if}
</div>
</div{* headercomplete *}>

{include file="functions.tpl.php"}

{if $skipcontentdiv neq 'true'}
<div id="content" class="wide-content" {$divcontent}>
{/if}

{if $skipmodules neq "true"}
{if isset($actions)}
<div class="buttons small">
{section name=i loop=$actions}
<a class="button" href="{$actions[i].location}"><img src="{$serverroot}/{#default_icon_url#}{$actions[i].icon}" height="{#small_icon_height#}" width="{#small_icon_width#}" alt="" class="im" />{$actions[i].caption}</a>
{/section}
</div>
{/if}
{/if}

{include file="error_message.tpl.php"}
