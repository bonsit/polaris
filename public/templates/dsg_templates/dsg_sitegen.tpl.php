{include file="basemenu.tpl.php" divcontent='class="contentwithhelp"'}

<h1>{lang sitegenerator_list}</h1>

{if @$clients ne 0}
<p>Hier kunt u websites toevoegen, wijzigen en verwijderen.</p>
<form name="sitegen" id="dataform" method="POST" action="{$callerpage}">
<input type="hidden" name="_hdnAction" value="delete">
<table class="group-table data striped" summary="overzicht">
<thead><tr>
<th class="action"><input type='checkbox' class='checkbox' name='_hdnAllBox' id='_hdnAllBox' /></th>
<th>{lang website}</th><th class="">{lang country}</th
><th>{lang action}</th></tr></thead>
{section name=i loop=$clients}
<tr class="{$classgroup}">
<td class="center"><input name="recordid[]" value="rec:{$clients[i].RECORDID}" type="checkbox"></td>
<td><a title="{lang edit}" href="?action=edit&rec={$clients[i].RECORDID}">{$clients[i].NAME}</a></td
><td>{$clients[i].COUNTRY}</td
><td><a href="?action=edit&rec={$clients[i].RECORDID}">{lang edit}</a>&nbsp;&nbsp;
<a href="{$serverroot}/designer/users/?clienthash={$clients[i].RECORDID}">{lang show_users}</a>
</td></tr>
{/section} 
<tfoot><td colspan="3"><input type="submit" name="_hdnDeleteButton" value="{lang but_delete}" class="protect" onclick="return CheckDel('dataform');" title="{lang but_delete_hint}" tabindex="2" /></td><td></td><td></td><td></td></thead>
</table>
</form>
{else}
    <p>Er zijn nog geen websites.</p>
{/if}
<h2>{lang what_now}</h2>
<p><img src="{$serverroot}/{#default_icon_url#}{$actions[0].icon}" height="{#large_icon_height#}" width="{#large_icon_width#}" alt="" class='im'>
<a href="{$actions[0].location}">{$actions[0].caption}</span></a></p>

<div id="inlinehelp"><span class="title"><h1>Wanneer dient u een nieuwe klant toe te voegen?</h1></span>
<p>Wanneer een bedrijf wil gaan werken met Polaris applicaties, dan dient er voor dat bedrijf
een klant(account) aangemaakt te worden. Een klant heeft dan beschikking over een of meerdere applicaties.
Een medewerker van een klant kan gebruikmaken van die applicaties door middel van een gebruikersaccount.</p>     
</div>

{include file="footer.tpl.php"}
