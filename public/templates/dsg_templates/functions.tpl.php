{strip}
<div id="breadcrumbs"><ul>
	<li class="label {if $locations|@count == 1}prelast{/if}">{lang your_location}:&nbsp;</li>
	{section name=i loop=$locations}
	<li
	{if $smarty.section.i.first and $smarty.section.i.last} class="first last"
	{elseif $smarty.section.i.total - 2 == ($smarty.section.i.index)} class="prelast"
	{elseif $smarty.section.i.last} class="last"
	{elseif $smarty.section.i.first} class="first"
	{/if}
	">
	{if not $smarty.section.i.last}
	&nbsp;<a href="{$locations[i].link}">{$locations[i].label|lower}</a>
	{else}
	{$locations[i].label|lower}
	{/if}
	</li>
	{/section}
</ul>
</div>
{/strip}