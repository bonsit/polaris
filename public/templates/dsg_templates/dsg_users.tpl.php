<h1 xstyle="clear:both">{lang user_list}</h1>
{include file="showclientselect.tpl.php"}

{section name=i loop=$usergroup}
{if $smarty.section.i.first}
{strip}
<p>{lang usergroup_desc}</p>
<form name="users" id="dataform" method="POST" action=".">
<input type="hidden" name="_hdnDatabaseID" value="{$databaseid}" />
<input type="hidden" name="_hdnAction" value="{#rec_delete#}" />
<table class="group-table data striped" summary="overzicht">
<thead><tr><th>{lang account}</th>
{if $smarty.session.currentclient eq ''}
<th>{lang client}</th>
{/if}
<th>{lang type}</th><th class="">{lang name}</th
><th>{lang last_login}</th><th width="90px" class="numeric">{lang logins}</th
><th>{lang action}</th><th class="center">{lang delete}</th></tr></thead>
{/strip}
{/if}
<tr{if $usergroup[i].ACCOUNTDISABLED eq 'Y'} class="disabled"{/if}><td><a title="{lang edit}" href="?action={#action_edit#}&rec={$usergroup[i].RECORDID}">{$usergroup[i].USERGROUPNAME}</a></td>
{if $smarty.session.currentclient eq ''}
<td>{$usergroup[i].CLIENTNAME}</td>
{/if}
<td>{if $usergroup[i].ROOTADMIN eq 'Y'}
Rootadmin
{elseif $usergroup[i].CLIENTADMIN eq 'Y'}
Clientadmin
{elseif $usergroup[i].USERORGROUP eq 'USER'}
{lang user}
{else}
{lang group}
{/if}
</td><td
>{if $usergroup[i].USERORGROUP eq 'USER'}{$usergroup[i].FULLNAME}{else}{$usergroup[i].DESCRIPTION}{/if}</td><td>{$usergroup[i].LASTLOGINDATE}</td><td class="numeric">{$usergroup[i].LOGINCOUNT}</td
><td><a href="?action={#action_edit#}&rec={$usergroup[i].RECORDID}">{lang edit}</a></td><td class="center">
<input name="recordid[]" value="rec:{$usergroup[i].RECORDID}" class="deletebox" type="checkbox"></td></tr>
{if $smarty.section.i.last}
<tfoot>
{if $smarty.session.currentclient eq ''}
<td></td>
{/if}
<td></td><td></td><td></td><td></td><td></td><td></td><td class="center"><input type="submit" class="xprotect" value="Verwijder" id="_hdnDeleteButton" /></td></tfoot>
</table>
</form>
{/if}
{sectionelse}
<p>{lang no_users_exists}</p>
<ul class="startitems">
{section name=j loop=$actions}
<li class="normalmenu"><a href="{$actions[j].location}"><img src="{$serverroot}/{#default_icon_url#}{$actions[j].icon}" height="{#large_icon_height#}" width="{#large_icon_width#}" alt="" class="im" />{$actions[j].caption}</a></li>
{/section}
</ul>
{/section}

{include file="footer.tpl.php"}