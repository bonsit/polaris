{include file="basemenu.tpl.php"}

<h1>{lang application_list}</h1>

{include file="showclientselect.tpl.php"}

{section name=i loop=$apps}
<ul id="appslist" class="appslist">
    <li style="background-color:#{$apps[i]->record->APPTILECOLOR|default:'#e3a21a'}">
        <a title="{lang click_to_start}" href="{$apps[i]->applink}">
            {if $apps[i]->record->IMAGE|substr:0:3 == 'fa-'}
            <i class="fa {$apps[i]->record->IMAGE} fa-4x"></i>
            {else}
            <img class="appicon" src="{$serverroot}/{$apps[i]->record->IMAGE|default:#def_app_icon#}" alt="{$apps[i]->record->APPLICATIONNAME}" />
            {/if}

            {if !isset($currentclient)}{$apps[i].NAME}{/if}
            <div class="appname">
    		<a title="{lang edit} {lang application}" href="?action=edit&rec={$apps[i].RECORDID}">{$apps[i].APPLICATIONNAME} {lang application_version}: {$apps[i].VERSION}</a>
        </a>
    </li>
</ul>



<ul id="appslist" class="appslist">
{section name=i loop=$apps}
<li style="background-color:#{$apps[i]->record->APPTILECOLOR|default:'#e3a21a'}">
<a title="{lang click_to_start}" href="{$apps[i]->applink}">
<div class="apps_content">
{if $apps[i]->record->IMAGE|substr:0:3 == 'fa-'}
<i class="fa {$apps[i]->record->IMAGE} fa-4x"></i>
{else}
<img class="appicon" src="{$serverroot}/{$apps[i]->record->IMAGE|default:#def_app_icon#}" alt="{$apps[i]->record->APPLICATIONNAME}" />
{/if}
<div class="appname">{$apps[i]->record->APPLICATIONNAME|get_resource:$lang}</div>
{if $usertype eq 'root'}<h3>{$apps[i]->record->NAME}</h3>{/if}
{*if $apps[i]->record->DESCRIPTION neq ''}
<p class="appdesc">{$apps[i]->record->DESCRIPTION|get_resource:$lang}</p>
{/if*}
</div>
</a>
</li>
{sectionelse}
<p>{lang apps_non_available}</p>
{/section}
{*
<li>
<a class="show_maximized" href="{$serverroot}/dashboard.php" title="Dashboard" target="_blank">Toon in apart venster</a>
<a title="{lang click_to_start}" href="{$serverroot}/dashboard.php"><img class="appicon" src="{$serverroot}//i/snowish/apps/computer.png" style="height:48px;width:48px" alt="Dashboard" />
TrafficAssistant - Dashboard</a> <span class="version">{lang application_version} 0.9</span><br />
{if $usertype eq 'root'}<h3>{$apps[i]->record->NAME}</h3>{/if}
Het Dashboard geeft u een gepersonaliseerde omgeving<br /> waar uw informatie...
</li>
*}
</ul>

{/section}
<!--

<table class="data striped clearfix" summary="overzicht">
<form name="apps" id="dataform" method="POST" action="{$callerpage}">
<input type="hidden" name="_hdnAction" value="{#rec_delete#}" />
<thead><tr><th>{lang application}</th
><th class="">{lang application_description}</th
><th class="">{lang application_version}</th
><th class="">{lang action}</th
><th class="center">{lang delete}</th></tr></thead>
{section name=i loop=$apps}
<tr><td><img src="{$apps[i].IMAGE|default:#def_app_icon#}" alt="No icon" height="{#small_icon_height#}" width="{#small_icon_width#}" class="im" /> <a title="{lang edit}" href="?action=edit&rec={$apps[i].RECORDID}">{$apps[i].APPLICATIONNAME}</a>
</td><td>{$apps[i].DESCRIPTION|truncate:25:"...":true}</td
><td>{$apps[i].VERSION}</td
><td><a href="?action={#action_edit#}&rec={$apps[i].RECORDID}">{lang edit}</a></td
><td class="center"><input name="recordid[]" value="rec:{$apps[i].RECORDID}" type="checkbox" /></td></tr>
{/section}
<tfoot><td></td><td></td><td></td><td></td><td class="center"><input type="submit" value="Verwijder" onclick="return CheckDel('dataform');" /></td></thead>
</form>
</table>
-->


{include file="footer.tpl.php"}
