{config_load file="globals.conf"}
{include file="dsg_header.tpl.php"}
{php}
include_once('miscfunc.inc.php');
{/php}
<div id="frame"><div id="topline"></div>
<div id="basefunctions"><span>{lang APPLICATIONS}</span><span class="current">{lang DESIGNER}</span
><span>{lang CONTROLPANEL}</span><span 
class="standard">{lang HELP}</span><span class="standard">{lang LOGOUT}</span></div>
{php} PNGImage('./images/polaris.png', 0, 0, 'Logo', $extra='id="logoimage"');{/php}

<div id="line"></div>
<div id="location">Uw locatie: <span><a href="">Polaris Home</a> &gt; </span><span><a href="">Beheer</a> &gt; </span><span><a href="">Gebruikers</a> &gt; </span><span class="current">Overzicht</span></div>
<div id="tabground">
<div class="tab">KLANTEN</div><div class="tab selected">GEBRUIKERS</div><div class="tab">APPLICATIES</div>
</div tabground>

<div id="subfunctions"><span>OVERZICHT</span><span class="current">IMPORT/EXPORT</span><span>ADVANCED</span><span class="standard">AFDRUKKEN</span></div>
<div id="line"></div>
<h1>Gebruikers overzicht</h1>

<p>Hieronder worden alle 
gebruikers getoond of alleen diegene die deel uitmaken van de gekozen klant.</p> 
<div class="preselect">Kies een klant: <select size="1"><option value="1">&lt;Alle gebruikers&gt;</option><option value="1">Debster</option><option value="1">Studiomax</option></select></div> 

<table class="data" summary="overzicht">
<thead><td>Account</td><td>Type</td><td>Naam</td><td>Laatste login</td><td>Actie</td><td class="center">Verwijder</td></thead>
<tbody><tr class="odd"><td>bartb</td><td>Gebruiker</td><td>Bart Bons</td><td>11-4-2003 13:33</td><td><a href="">Wijzig</a> | <a href="">Toon lidmaatschap</a></td><td class="center"><input type="checkbox"></td></tr>
<tr class="even"><td>miranda</td><td>Gebruiker</td><td>Miranda In de Braekt</td><td>17-4-2003 12:14</td><td><a href="">Wijzig</a> | <a href="">Toon lidmaatschap</a></td><td class="center"><input type="checkbox"></td></tr>
<tr class="odd group"><td>sysadmins</td><td>Groep</td><td>Systeem admins</td><td>n.v.t.</td><td><a href="">Wijzig</a> | <a href="">Toon gebruikers</a></td><td class="center"><input type="checkbox"></td></tr>
</tbody>
<tfoot><td></td><td></td><td></td><td></td><td></td><td class="center"><input type=button value="Verwijder" /></td></tfoot>
</table>

<h2>Nieuwe gebruiker of groep toevoegen</h2>
<p>{php} PNGImage('icons/beos/person.png', 48, 48, '', $extra='class="imgaction"');{/php}
<a href="#">Voeg een gebruiker toe...</span></a></p>
<p>{php} PNGImage('icons/beos/BeOS_people.png', 48, 48, '', $extra='class="imgaction"');{/php}
<a href="#">Voeg een groep toe...</a></p>
</div>

{include file="dsg_footer.tpl.php"}
