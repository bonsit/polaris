{include file="basemenu.tpl.php"}

<h2>{lang application} '{$recset.APPLICATIONNAME}'</h2>

{include file="applicationmenu.tpl.php"}

<p>Hier kunt u de gegevens van de applicatie {$recset.APPLICATIONNAME} aanpassen. Via bovenstaande tabbladen kunt u de overige onderdelen van deze applicatie wijzigen.</p>
<form name="application" method="POST" action="?action=edit&rec={$recordid}">
<input type="hidden" name="_hdnRecordID" value="{$recordid}" />
<input type="hidden" name="_hdnAction" value="{#rec_update#}" />

<table class="data striped" summary="data">
<tr><td width="30%">{lang client}:</td>
<td>{include file="showclientselect.tpl.php" style="selectonly" readonly="true"}</td></tr>
<tr><td>{lang application_name}:</td>
<td><input name="APPLICATIONNAME" class="text" size="50" maxlength="50" type="text" value="{$recset.APPLICATIONNAME}" /></td></tr>
<tr><td>{lang application_description}:</td>
<td><input name="DESCRIPTION" class="text" size="50" maxlength="255" type="text" value="{$recset.DESCRIPTION}" /></td></tr>
<tr><td>{lang orderindex}:</td>
<td><input name="ORDERINDEX" class="text" size="5" maxlength="5" type="text" value="{$recset.ORDERINDEX}" /></td></tr>
<tr><td>{lang application_picture}:</td>
<td><input name="APPLICATIONPICTURE" class="text" size="50" maxlength="100" type="text" value="{$recset.APPLICATIONPICTURE}" /></td></tr>
<tr><td>{lang application_version}:</td>
<td><input name="VERSION" class="text" size="10" maxlength="10" type="text" value="{$recset.VERSION}" /></td></tr>
<tr><td>{lang application_registered}:</td>
<td><input name="REGISTERED" class="text" size="35" maxlength="35" type="text" value="{$recset.REGISTERED}" /></td></tr>
<tr><td>{lang application_image}:</td>
<td><input name="IMAGE" class="text" size="50" maxlength="255" type="text" value="{$recset.IMAGE}" /></td></tr>
<tr><td>{lang startup_message}:</td>
<td><input name="STARTUPMESSAGE" class="text" size="50" maxlength="255" type="text" value="{$recset.STARTUPMESSAGE}" /></td></tr>
<tr><td>{lang startup_message_formcolor}:</td>
<td><input name="STARTUPMESSAGEFORMCOLOR" class="text" size="11" maxlength="11" type="text" value="{$recset.STARTUPMESSAGEFORMCOLOR}" /></td></tr>
<tr><td>{lang helpnr}:</td>
<td><input name="HELPNR" class="text" size="7" maxlength="7" type="text" value="{$recset.HELPNR}" /></td></tr>
<tr><td>{lang helpfile}:</td>
<td><input name="HELPFILE" class="text" size="25" maxlength="25" type="text" value="{$recset.HELPFILE}" /></td></tr>
<tr><td>{lang searchoptions}:</td>
<td><input name="SEARCHOPTIONS" class="text" size="5" maxlength="5" type="text" value="{$recset.SEARCHOPTIONS}" /></td></tr>
<tr><td>{lang website}:</td>
<td><input name="WEBSITE" class="text" size="50" maxlength="200" type="text" value="{$recset.WEBSITE}" /></td></tr>
<tr><td>{lang ftpserver}:</td>
<td><input name="FTPSERVER" class="text" size="50" maxlength="255" type="text" value="{$recset.FTPSERVER}" /></td></tr>
<tr><td>{lang ftpuser}:</td>
<td><input name="FTPUSER" class="text" size="50" maxlength="100" type="text" value="{$recset.FTPUSER}" /></td></tr>
<tr><td>{lang ftppassword}:</td>
<td><input name="FTPPASSWORD" class="text" size="50" maxlength="50" type="text" value="{$recset.FTPPASSWORD}" /></td></tr>
</table><input type="submit" value="{lang but_save}" /> <input type="button" value="{lang but_cancel}" class="protect" onClick="location.href='{$callerpage}'" />

</form>

{include file="footer.tpl.php"}