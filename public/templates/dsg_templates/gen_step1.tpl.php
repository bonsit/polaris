<h2>Klant gegevens</h2>
<p>Voor welke klant dient de applicatie gemaakt te worden?</p>
<p>Klant:
<select name="_hdnClientHash" size="1" onchange="window.location='{$callerpage}?client='+_hdnClientHash.value">
{section name=i loop=$clients}
<option value="{$clients[i].RECORDID}" 
{if $smarty.get.client eq $clients[i].RECORDID}
selected="selected"
{/if}
>{$clients[i].NAME}</option>
{/section}
</select>


<p><input type="radio" name="_hdnNewOrOverwriteApp" value="new" /> Nieuwe applicatie genereren.</p>
<div style="margin-left:30px;">
<h2>Applicatie basis gegevens</h2>
<p>Deze gegevens kunnen achteraf altijd nog gewijzigd worden.</p>
<table class="data striped">
<tr><td><label for="an">{lang application_name}:</label></td> 
<td><input id="an" type="text" class="text required" name="APPLICATIONNAME" size="60" maxlength="50" value="{$smarty.post.APPLICATIONNAME|default:$smarty.now|date_format:'Application %d %B %Y - %H:%I:%S'}"></td></tr>

<tr><td><label for="ad">{lang application_description}:</label></td> 
<td><input id="ad" type="text" class="text" name="DESCRIPTION" size="60" maxlength="50" value="{$smarty.post.DESCRIPTION}"></td></tr>

<tr><td><label for="ve">{lang application_version}:</label></td> 
<td><input id="ve" type="text" class="text required" name="VERSION" size="20" maxlength="10" value="{$smarty.post.VERSION|default:"1.0"}"></td></tr>

<tr><td><label for="re">{lang application_registered}:</label></td> 
<td><input id="re" type="text" class="text required" name="REGISTERED" size="45" maxlength="35" value="{$smarty.post.REGISTERED|default:"client"}"></td></tr>
</table>
</div>
OF

<p><input type="radio" name="_hdnNewOrOverwriteApp" value="overwrite" /> Overschrijf de volgende applicatie

<select name="_hdnAppHash" size="1" onchange="window.location='{$callerpage}?client='+_hdnClientHash.value+'&app='+_hdnAppHash.value">
{section name=i loop=$apps}
<option value="{$apps[i].RECORDID}" 
{if $smarty.get.app eq $apps[i].RECORDID}
selected="selected"
{/if}
>{$apps[i].APPLICATIONNAME}</option>
{sectionelse}
<option value="">&lt;geen applicaties&gt;</option>
{/section}
</select>
</p>

<h2>Quick Start</h2>
<p>Wanneer u direct met uw applicatie wilt gaan werken, dan kunt u hieronder de gebruiker kiezen die geautoriseerd dient te worden.</p>
{lang user}: <select name="_hdnUserGroupHash" size="1">
<option value="" ></option>
{section name=i loop=$users}
<option value="{$users[i].RECORDID}" >{$users[i].USERGROUPNAME}</option>
{/section}
</select>



{literal}<script>
var _checked = $("input[name=_hdnAppHash]").val() != "";
$("input[name=_hdnNewOrOverwriteApp]").attr("checked",_checked);
</script>{/literal}
