<h2>Kies uw database</h2>
<input id="cbn" type="radio" name="_hdnChoosedb" value="c" /> <label for="cbn">Cre&euml;er een geheel nieuwe database: </label><br />
<p class="smaller indent">Kies deze optie wanneer u nog geen database heeft. U dient voldoende rechten te hebben om ergens (op het Internet) een database aan te maken.</p>


<input id="cbd" type="radio" name="_hdnChoosedb" value="e" checked="checked" /> <label for="cbd">Bestaande database verbinding: </label><br />
<p class="smaller indent">Kies deze optie wanneer u gebruik wilt maken van een database die al in het Polaris systeem aanwezig is.</p>
<p class="indent">Database verbinding: {$databases}</p>

<input id="cnd" type="radio" name="_hdnChoosedb" value="n" /> <label for="cnd">Nieuwe database verbinding: </label>
<p class="smaller indent">Kies deze optie wanneer u een nieuwe verbinding wilt maken met een bestaande database. Let erop dat u voldoende rechten heeft om deze database te gebruiken.</p>

<table class="data indent striped" style="width:95%">

<tr><td><label for="de">Database omschrijving:</label></td>
<td><input id="de" type="text" class="text required" name="DESCRIPTION" size="60" maxlength="50" value="{$smarty.post.DESCRIPTION|default:$smarty.now|date_format:'DB %d %B %Y - %H:%I:%S'}" /></td></tr>

<tr><td><label for="an">Database type:</label></td>
<td>{$dbtypes}</td></tr>

<tr><td><label for="ho">Host:</label></td>
<td><input id="ho" type="text" class="text required" onkeydown="cnd.checked=true" name="HOST" size="40" maxlength="50" value="{$smarty.post.HOST}" /></td></tr>

<tr><td><label for="ho">Database:</label></td>
<td><input id="db" type="text" class="text required" onkeydown="cnd.checked=true" name="DATABASENAME" size="40" maxlength="50" value="{$smarty.post.DATABASENAME}" /></td></tr>

<tr><td><label for="ac">Account:</label></td>
<td><input id="ac" type="text" class="text" onkeydown="cnd.checked=true" name="ROOTUSERNAME" size="20" maxlength="20" value="{$smarty.post.ROOTUSERNAME}" /></td></tr>

<tr><td><label for="pw">Wachtwoord:</label></td>
<td><input id="pw" type="password" class="text" onkeydown="cnd.checked=true" name="ROOTPASSWORD" size="20" maxlength="20" value="{$smarty.post.ROOTPASSWORD}" /></td></tr>

</table>

{literal}
<script type="text/javascript">
<!--
// Make sure the right checkbox gets checked when the DATABASETYPE dropdown is changed
connect($('dt'), 'onchange', function() {$('cnd').checked=true} )
// Make sure the right checkbox gets checked when the DATABASE dropdown is changed
connect($('dbselect'), 'onchange', function() {$('cbd').checked=true} )
// -->
</script>
{/literal}