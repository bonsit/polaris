{include file="basemenu.tpl.php"}

{if $recordstate eq #rec_insert#}
<h1>{lang make_new_client}</h1>
<p>Hieronder kunt u de gegevens van de nieuwe klant invullen.</p>
{else}
<h1>{lang client_account_of} {$client.NAME}</h1>
{include file="clientmenu.tpl.php"}
<p>Hieronder kunt u de gegevens van {$client.NAME} aanpassen.</p>
{/if}

<form name="clientform" method="post" action="{$callerquery}" class="validate">
<input type="hidden" name="_hdnRecordID" value="{$recordid}" />
<input type="hidden" name="_hdnAction" value="{$recordstate}" />
<table class="data" summary="overzicht">
<tr><td width="30%">{lang name}:</td><td class="keycolumn"></td><td><input name="NAME" class="text required" type="text" maxlength="30" size="40" value="{$client.NAME}" /></td></tr>
<tr><td>{lang address}:</td><td></td><td><input name="ADDRESS" class="text" type="text" maxlength="30" size="40" value="{$client.ADDRESS}" /></td></tr>
<tr><td>{lang zipcode}:</td><td></td><td><input name="ZIPCODE" class="text" type="text" maxlength="6" size="10" value="{$client.ZIPCODE}" /></td></tr>
<tr><td>{lang city}:</td><td></td><td><input name="CITY" class="text" type="text" maxlength="30" size="40" value="{$client.CITY}" /></td></tr>
<tr><td>{lang country}:</td><td></td><td><input name="COUNTRY" class="text required" type="text" maxlength="3" size="5" value="{$client.COUNTRY}" /></td></tr>
<tr><td>{lang telephone}:</td><td></td><td><input name="TELEPHONE" class="text" type="text" maxlength="15" size="20" value="{$client.TELEPHONE}" /></td></tr>
<tr><td>{lang fax}:</td><td></td><td><input name="FAX" class="text" type="text" maxlength="15" size="20" value="{$client.FAX}" /></td></tr>
<tr><td>{lang postaddress}:</td><td></td><td><input name="POSTADDRESS" class="text" type="text" maxlength="30" size="40" value="{$client.POSTADDRESS}" /></td></tr>
<tr><td>{lang postzipcode}:</td><td></td><td><input name="POSTZIPCODE" class="text" type="text" maxlength="6" size="10" value="{$client.POSTZIPCODE}" /></td></tr>
<tr><td>{lang postcity}:</td><td></td><td><input name="POSTCITY" class="text" type="text" maxlength="30" size="40" value="{$client.POSTCITY}" /></td></tr>
<tr><td>{lang default_language}:</td><td></td><td>
<select name="DEFAULTLANGUAGE" class="required">
<option value="NL" selected="selected">NL</option>
{$languages[l]}
</option> 
{section name=l loop=$languages}
<option value="{$languages[l]}"{if $languages[l] eq $client.DEFAULTLANGUAGE} selected="selected"{/if}>
{$languages[l]}
</option> 
{/section}
</select>
</td></tr>
</table>
<input type="submit" value="{lang but_save}">&nbsp;<input type="button" value="{lang but_cancel}" class="protect" onClick="location.href='{$callerpage}'" />
</form>

{include file="footer.tpl.php"}