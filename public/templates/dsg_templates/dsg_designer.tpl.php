{strip}
{config_load file="globals.conf"}
{include file="header.tpl.php"}
<div id="wrapper">

    {if $navigationstyle == 'vertical'}
    {include file="constructs.tpl.php"}
    {/if}

    <div id="page-wrapper" class="gray-bg">
        <div class="row">
            {if $smarty.get.maximize != 'true'}
            {include file="constructs.tpl.php"}
            {if $navigationstyle == 'horizontal'}
            {/if}
            {/if}
        </div>

        <div class="fh-breadcrumb">
            {if $searchstyle == 'vertical' and $currentform and $maximizeform ne 'true'}
            <div class="fh-column">
                <div class="full-height-scroll">
                    {if $currentform and $maximizeform ne 'true'}
                    {$currentform->ShowSearchPanel($currentaction, $permission, $currentform->detail, false)}
                    {/if}
                </div>
            </div>
            {/if}
            <div class="full-heightx">
                <div class="full-height-scrollx white-bg {if $searchstyle=='vertical'}border-left{/if}">

                    <div class="element-detail-box">

                        {include file="error_message.tpl.php"}
                        {if ($currentconstructid eq 0) and (!$module) and (!$currentform)}
                            {include file="dsg_dashboard.tpl.php"}
                        {else}
                            {include file=$modulepage}
                        {/if}

                    </div>
                </div>
            </div>
        </div>

        <div class="footer fixed">
        </div>

        </form>
    </div>
</div>
{include file="footer.tpl.php"}
{/strip}
