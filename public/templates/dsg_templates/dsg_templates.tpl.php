{include file="basemenu.tpl.php" divcontent='class="contentwithhelp"'}
  <h1>{lang templates_list}</h1>
  <p>Under construction</p>

  <div id="inlinehelp">
    <h1><span class="title">Wat zijn Prefab-applicaties?</span></h1>
    <p>Een Prefab applicatie is een kant-en-klare (prefabricated) oplossing die gebruikt kan worden bij klanten die een standaard applicatie wensen.</p>
    <p>Met behulp van deze standaard applicaties is het mogelijk binnen enkele minuten een complete oplossing te cree�ren. Applicaties die gebouwd zijn op basis van deze 'prefabs' kunnen uiteraard verder worden aangepast.</p>
    <p>Normale applicaties kunnen gepromoveerd worden tot prefab applicatie om zo eenvoudig gebruikt te worden bij andere klanten.</p>
  </div>

{include file="footer.tpl.php"}
