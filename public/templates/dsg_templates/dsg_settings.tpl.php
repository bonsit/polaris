{include file="basemenu.tpl.php" divcontent='class="contentwithhelp"'}

<h1>{lang polaris_system_settings}</h1>

<h2>Polaris database coordinaten</h2>
<form name="settingsform" action="{$callerpage}" method="post" class="validate">
<input type="hidden" name="_hdnAction" value="{#rec_update#}">
<table class="data ">
<tr><td>Database type:</td><td colspan="2"><select name="plrdatabasetype" class="required" label="Database type">{html_options options=$dbtypes selected=$dbtype}</select></td></tr>
<tr><td>Host:</td><td><input name="plrhost" type="text" class="required" label="Host" size="40" maxlenght="50" value="{php}echo $_POST['plrhost'] ? $_POST['plrhost'] : PLRHOST {/php}" /></td></tr>
<tr><td>Port:</td><td><input name="plrport" type="text" class="required" label="Port" size="40" maxlenght="50" value="{php}echo $_POST['plrport'] ? $_POST['plrport'] : PLRPORT {/php}" /></td></tr>
<tr><td>Database:</td><td><input name="plrdatabase" type="text"  class="required" label="Database" size="40" maxlenght="100" value="{php}echo $_POST['plrdatabase'] ? $_POST['plrdatabase'] : PLRDATABASE {/php}" /></td></tr>
<tr><td>Username:</td><td><input name="plrusername"  class="required" label="Username" type="text" size="20" maxlenght="20" value="{php}echo $_POST['plrusername'] ? $_POST['plrusername'] : PLRUSERNAME {/php}" /></td></tr>
<tr><td>Password:</td><td><input name="plrpassword" type="password" class="required" label="Password" size="20" maxlenght="20" value="{php}echo $_POST['plrpassword'] ? $_POST['plrpassword'] : PLRPASSWORD {/php}" />&nbsp;&nbsp;<input type="submit" value="{lang test_connection}" style="font-size:0.8em" onclick="settingsform._hdnAction.value='{#rec_test#}'"></td></tr>
</table>
{if $connectiontest}
<p class="{$class}">{$connectiontest}</b></p>
{/if}

<h2>Polaris standaard waarden</h2>
<p>Standaard taal: <input name="langdefault" type="text" maxlength="2" size="3" value="{php}echo $_POST['langdefault'] ? $_POST['langdefault'] : PLRDEFAULTLANG {/php}" /></p>

<input type="submit" value="{lang but_save}" />
</form>

<h2 class="mainactions">Polaris database aanmaken</h2>
<form name="scriptform" action="{$callerpage}" method="post" class="validate">
<input type="hidden" name="_hdnAction" value="{#rec_script#}" />
<table class="data" style="width:auto;">
<tr><td><input name="createscript" type="radio" value="genaxmls" /> Genereer ADODB XML Schema op basis van huidige database</td><td></td></tr>
<tr><td><input name="createscript" type="radio" value="execute" /> Cre�er Polaris database in huidige database locatie</td><td></td></tr>
<tr><td><input name="createscript" type="radio" value="axmls" checked="checked" /> Toon ADODB XML Schema op scherm</td><td></td></tr>
<tr><td><input name="createscript" type="radio" value="sql" /> Toon SQL Script op scherm</td><td></td></tr>
<tr><td><input type="submit" value="{lang go}" /></td></tr>
</table>
</form>

<div id="inlinehelp"><h1>{lang polaris_system_settings}</h1>
<p>Het Polaris Platform en de Polaris Designer zijn zo ingericht dat zij met verschillende 
servers en databasetypes kunnen werken. Op deze pagina kunt u aangeven welke database
Polaris moet gebruiken voor het administreren van haar gegevens.</p>
<p><b>Nog geen Polaris database?</b> Indien u nog geen Polaris database heeft, dan kunt u er ook voor kiezen om er een aan te maken
op de door u aangegeven server.</p>     
</div>

{include file="footer.tpl.php"}
