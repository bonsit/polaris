{include file="basemenu.tpl.php"}

<h2>{lang application} '{$app.APPLICATIONNAME}'</h2>

{include file="applicationmenu.tpl.php"}

<form name="application" method="POST" action="?action=edit&rec={$recordid}">
<input type="hidden" name="_hdnRecordID" value="{$recordid}">
<input type="hidden" name="_hdnAction" value="u">

<table class="data striped" summary="data"><thead>
<th>{lang formname}</th>
<th>{lang databaseid}</th>
<th>{lang tablename}</th>
<th>{lang filter}</th>
<th>{lang action}</th>
<th width="20%" class="center">{lang delete}</th></thead>
<tbody>
{section name=i loop=$recset}<tr>
<td><a title="{lang edit}" href="?action=edit&rec={$recset[i].RECORDID}">{$recset[i].FORMNAME}</a></td>
<td>{$recset[i].DATABASEID}</td>
<td>{$recset[i].TABLENAME}</td>
<td>{$recset[i].FILTER}</td>
<td><a href="">{lang edit}</a>&nbsp;&nbsp;<a href="?action=edit&rec={$recset[i].RECORDID}">{lang show_pages} ({$recset[i].PAGECOUNT})</a></td>
<td class="center"><input name="recordid[]" value="rec:{$recset[i].RECORDID}" type="checkbox"></td></tr>
{/section}
<tfoot><tr><td></td><td></td><td></td><td></td><td></td><td class="center"><input type="submit" name="_hdnDeleteButton" value="{lang but_delete}" class="protect" onclick="return CheckDel('dataform');" title="{lang but_delete_hint}" tabindex="2"></td></tr></tfoot></table>
</form>

{include file="footer.tpl.php"}