{if $smarty.get.header eq true}
{include file="header.tpl.php" sitename=#appTitle#}
{include file="basemenu.tpl.php" showmaincontentdiv="true"}
{/if}

{literal}
<script language="javascript">
function charAlert(obj) {
  var textField = obj;
  var maxLength = obj.getAttribute('maxlength');
  if(textField.value.length > maxLength) {
    textField.value= textField.value.substring(0,maxLength);
    textField.blur();
    alert("No more text can be entered");
  }
}
</script>
{/literal}

<form id="userform" method="post" action="." class="validate" enctype="multipart/form-data">
<input type="hidden" name="_hdnProcessedByModule" value="true" />
<input type="hidden" name="_hdnRecordID" value="{$usergroup.RECORDID}" />
<input type="hidden" name="_hdnAction" value="{$recordstate}" />
<input type="hidden" name="PHOTOURL" value="{$usergroup.PHOTOURL}" />
<input type="hidden" name="MAX_FILE_SIZE" value="500000" />
<input type="hidden" name="_hdnUserGroupName" value="{$usergroup.USERGROUPNAME}" />
<table class="data striped" summary="overzicht">
<tr><td width="20%">{lang username}:</td><td>{$usergroup.USERGROUPNAME}</td></tr>
<tr><td>{lang fullname}:</td><td><input name="FULLNAME" class="text required" type="text" value="{$usergroup.FULLNAME}" /></td></tr>
<tr><td>{lang description}:</td><td><textarea name="DESCRIPTION" class="text" size="40" cols="80" onKeyUp="charAlert(this)" maxlength="255" errormessage="The maximum allowance of 255 characters has been reached.">{$usergroup.DESCRIPTION}</textarea></td></tr>
<tr><td>{lang email_address}:</td><td><input name="EMAILADDRESS" class="text required" type="text" size="40" value="{$usergroup.EMAILADDRESS}" /></td></tr>
<tr><td>{lang password}:</td><td><input class="text required" type="password" name="USERPASSWORD" value="{$userpassword}"></td></tr>
<tr><td>{lang retype_password}:</td><td><input class="text required"type="password" name="RETYPEUSERPASSWORD" value="{$userpassword}"></td></tr>
<tr><td>Foto van gebruiker:</td><td>{if $usergroup.PHOTOURL ne ''}<img src="{$usergroup.PHOTOURL}" alt="foto van {$usergroup.USERGROUPNAME}" />{else}Geen foto{/if}</td></tr>
<tr><td>Nieuwe foto uploaden:</td><td><input class="text" type="file" accept="*.gif,*.jpg" size="40" name="PHOTO" /><br />De foto wordt automatisch verkleind tot max. 75px breed en 100px hoog.<br />Alleen JPG, PNG en GIF bestanden worden geaccepteerd.</td></tr>
</table>
<input type="submit" value="{lang but_save}">&nbsp;<input type="button" value="{lang but_cancel}" class="protect" onClick="location.href='{$callerpage}'" />
</form>

{if $smarty.get.header eq true}
{include file="footer.tpl.php"}
{/if}
