{include file="basemenu.tpl.php"}

Met de Quick Form Generator kunt u snel een HTML/Smarty formulier genereren 
op basis van een template en een bepaalde relationele tabel.

<form id="genform" method="post" action="{$callerpage}">
<h2>Kies een template</h2>
<input type="radio" name="formtype" value="f" {if $smarty.post.formtype eq 'f' or !isset($smarty.post.formtype)}checked="checked"{/if} /> Formview <br />
<input type="radio" name="formtype" value="l" {if $smarty.post.formtype eq 'l'}checked="checked"{/if} /> Listview
{$templates}

<h2>Kies een klant:</h2>
{$currentclients}

<h2>Kies een database</h2>
{$databases}
<input type="submit" value="Go" />

<h2>Kies een tabel</h2>
{$tables}

<h2>Kies de kolommen</h2>
<div style="overflow:scroll;width:300px;height:300px">
{foreach name=outer item=column from=$columns}
	<input type="checkbox" name="column[]" value="{$column->name}" checked="checked" />{$column->name}<br /> 
{/foreach}
</div>

<input type="submit" name="generate" value="Genereer form" />
 
</form>
<br />
<textarea cols=120 rows=40 style="font-size:0.9em;">{$formhtml}</textarea>

{include file="footer.tpl.php"}