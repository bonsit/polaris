{include file="basemenu.tpl.php"}
<script type="text/javascript" src="{$serverroot}/javascript/polaris/SelectContainer.js"></script>
<script type="text/javascript">
{literal}
function ToggleButtons() {
  $('#remove_selected').attr('disabled', $('#members')[0].selectedIndex == -1 );
  $('#add_selected').attr('disabled', $('#users')[0].selectedIndex == -1);
}

$(document).ready(function() {
    SC_ContainersNoSelection('members', 'users');
});

{/literal}
</script>

<h1>{lang members_of} {$usergroup.USERGROUPNAME}</h1>
<p>Hieronder kunt u gebruikers lid maken van de groep '{$usergroup.USERGROUPNAME}'.</p>

<form name="userform" method="POST" action="{$callerpage}?action={#action_edit#}&rec={$recordid}" class="validate">
<input type="hidden" name="_hdnDatabaseID" value="{$databaseid}" />
<input type="hidden" name="_hdnRecordID" value="{$recordid}" />
<input type="hidden" name="_hdnAction" value="{#rec_members#}" />

<div class="select_container" style="width:40%;float:left;" >Huidige leden:<br />
<select id="members" name="newmembers[]" size="30" style="width:100%" multiple="multiple" ondblclick="SC_MoveSelected('members','users');return false" onchange="ToggleButtons();">
{section name=i loop=$members}
<option value="{$members[i].RECORDID}">{$members[i].USERGROUPNAME} - {$members[i].DESCRIPTION}</option>
{/section}
</select>
</div>
<div class="select_buttons" style="width:100px;float:left;" >
<button id="remove_selected" title="{lang remove_selected}" disabled="disabled" onclick="SC_MoveSelected('members','users');return false">&gt;</button>
<button id="remove_all" title="{lang remove_all}" onclick="SC_MoveAll('members','users');return false">&gt;&gt;</button>
<br /><br />
<button id="add_selected" title="{lang add_selected}" disabled="disabled" onclick="SC_MoveSelected('users','members');return false">&lt;</button>
<button id="add_all" title="{lang add_all}" onclick="SC_MoveAll('users','members');return false">&lt;&lt;</button>
</div>
<div class="select_container" style="width:40%;float:left;" >Beschikbare gebruikers:<br />
<select id="users" size="30" style="width:100%" multiple="multiple" ondblclick="SC_MoveSelected('users','members');return false" onchange="ToggleButtons();">
{section name=i loop=$users}
<option value="{$users[i].RECORDID}">{$users[i].USERGROUPNAME} - {$users[i].DESCRIPTION}</option>
{/section}
</select>
</div>

<div class="clear" style="clear:both">&nbsp;</div>
<input type="submit" value="{lang but_save}" class="protect" onclick="CS_SelectAll('members')"> <input type="button" onClick="location.href='{$callerpage}?action=edit&rec={$recordid}'" value="{lang but_cancel}" class="protect">
</form>

{include file="footer.tpl.php"}