<!DOCTYPE html>
<html lang="nl">
<head>
<title>{if $constructname ne ''}{$constructname|get_resource:$lang} - {/if}{if $applicationname ne ''}{$applicationname} - {/if}{lang pagetitle}</title>

<meta http-equiv="Content-Type" content="text/html; charset=utf8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="theme-color" content="#524b4d">

<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
<link rel="manifest" href="/site.webmanifest">

{if isset($currentform) and $currentform->record->RSSCOLUMNS}<link rel="alternate" type="application/rss+xml" title="{$currentform->record->FORMNAME}" href="{$currentform->RSSLink()}" />{/if}

<link rel="stylesheet" type="text/css" media='all' href="{$serverpath}/assets/css/fontawesome-pro/css/all.min.css" />
<link rel="stylesheet" type="text/css" media='all' href="{$serverpath}/assets/dist/main.min.css?v={#ReleaseVersion#}"  />

<link rel="stylesheet" type="text/css" media='all' href="{$serverpath}/modules/_shared/module.plr_maxia/css/default.css?v={#ReleaseVersion#}"  />

<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20,300,0..1,100" />

<script type="text/javascript" src="{$serverpath}/assets/js/jquery/dist/jquery.min.js"></script>
<script src="{$serverpath}/assets/dist/main.js"></script>

<script type="text/javascript">
var _loadevents_ = [], _serverroot = "{$serverroot}", _callerquery = _serverroot+"{$callerquery}", _basicformquery = _serverroot+"{$basicformquery}", _servicequery = _serverroot+"{$servicequery}", _ajaxquery = _serverroot+"{$ajaxquery}";
var isoracle = {$isoracle|default:'false'};
var cfp = {$permission|default:-1};
var formid = "{$currentformid|default:-1}";
var g_lang = "{$lang}";

</script>
</head>
<body class="canvas top-navigation full-height-layout {if $selectmode}body-small{/if} {$service} {$currentaction} {$currentdetailaction} {if !$maximizeform}{$navigationstyle}{/if} search-{$searchstyle}" xoncontextmenu="return false;">
