</div>
<hr class="cleaner" />
</div></div>

{*<div id="footer">{#CopyrightText#}</div>*}

<script src="{$serverpath}/assets/dist/modeler.js"></script>

{if ($showformscripts == true) and ($language|lower != 'nl')}
<script type="text/javascript" src="{$serverpath}/javascript/validation/localization-{$language|lower}.js"></script>
{/if}

{if ($showModelerScripts == true)}
<link rel="stylesheet" href="{$serverpath}/assets/css/jquery.svg.css" type="text/css" media="all" />
{/if}

</body>
</html>