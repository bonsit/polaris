{include file="basemenu.tpl.php"}

<div style="float:right">
<img src="{$serverroot}/images/step1{if $page_step eq 1}_on{/if}.png" alt="{lang step} 1" />
<img src="{$serverroot}/images/step2{if $page_step eq 2}_on{/if}.png" alt="{lang step} 2" />
<img src="{$serverroot}/images/step3{if $page_step eq 3}_on{/if}.png" alt="{lang step} 3" />
<img src="{$serverroot}/images/step4{if $page_step eq 4}_on{/if}.png" alt="{lang step} 4" />
</div>
<h1>{lang make_new_application}
</h1>

<form method="post" action="{$callerpage}" name="frmappgen">
{$hiddenfields}
<input type="hidden" name="step" value="{$page_step}">

{include file="gen_step$page_step.tpl.php"}
<p>
{if $endpage neq true}
<br /><input type="submit" value="{lang step} {math equation="x + 1" x=$page_step} &gt;&gt;" /></p>
{/if}
</form>

{include file="footer.tpl.php"}