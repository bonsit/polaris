<div class="row">
    <div class="col-lg-4">
        <a title="{lang click_to_start}" href="{$serverroot}/designer/users/">
            <div class="widget yellow-bg p-lg text-center">
                <div class="m-b-md">
                    <i class="fa fa-users fa-4x"></i>
                    <h1 class="m-xs">{lang mod_user_manager}</h1>
                </div>
            </div>
        </a>
    </div>

    <div class="col-lg-4">
        <a title="{lang click_to_start}" href="{$serverroot}/designer/backup/">
            <div class="widget yellow-bg p-lg text-center">
                    <div class="m-b-md">
                        <i class="fa fa-medkit fa-4x"></i>
                        <h1 class="m-xs">{lang mod_backup_restore}</h1>
                    </div>
            </div>
        </a>
    </div>

    <div class="col-lg-4">
        <a title="{lang click_to_start}" href="{$serverroot}/designer/appgen/">
            <div class="widget yellow-bg p-lg text-center">
                    <div class="m-b-md">
                        <i class="fa fa-cog fa-4x"></i>
                        <h1 class="m-xs">{lang mod_site_generator}</h1>
                    </div>
            </div>
        </a>
    </div>
</div>


{* WERKT NOG NIET
<li id="mod_app">
<a title="{lang click_to_start}" href="{$serverroot}/designer/apps/">
<div class="apps_content">
<span class="appname">{lang mod_application_manager}</span>
<img class="appicon" src="{$serverroot}/i/beos/blocks.png" />
<p>Beheer van de Polaris applicaties</p>
</div>
</a>
</li>
*}
<li id="mod_db"><a href='{$serverroot}/designer/db/'><img src="{$serverroot}/i/beos/query.png" class="im" alt="x" height="{$iconsize}" width="{$iconsize}">{if $size eq "large"}{lang go_to} {/if}{lang mod_db_manager}</a></li>
<li id="mod_client"><a href='{$serverroot}/designer/clients/'><img src="{$serverroot}/i/beos/logo_3d.png" class="im" alt="x" height="{$iconsize}" width="{$iconsize}">{if $size eq "large"}{lang go_to} {/if}{lang mod_client_manager}</a></li>
<li id="mod_templates"><a href='{$serverroot}/designer/templates/'><img src="{$serverroot}/i/beos/blocks_gray.png" class="im" alt="x" height="{$iconsize}" width="{$iconsize}">{if $size eq "large"}{lang go_to} {/if}{lang mod_template_manager}</a></li>
<li id="mod_settings"><a href='{$serverroot}/designer/settings/'><img src="{$serverroot}/i/beos/warn.png" class="im" alt="x" height="{$iconsize}" width="{$iconsize}">{if $size eq "large"}{lang go_to} {/if}{lang mod_system_settings}</a></li>
{if $smarty.session.usertype == 'root'}
{/if}
</ul>
