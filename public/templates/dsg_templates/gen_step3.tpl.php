<h2>Kies de tabellen die u wilt opnemen in de nieuwe applicatie:</h2>

<div class="buttonbarx"><button type="button" onclick="CheckAllCheckboxes(true);return false">{lang check_all}</button> <button type="button" onclick="CheckAllCheckboxes(false);return false">{lang check_none}</button></p>

{$tables}

<p><br />Wat wilt u doen met de bestaande kolomdefinities in Polaris?<br/>
<input type="radio" name="overwritetables" value="Y" /> Kolomdefinities overschrijven <br/>
<input type="radio" name="overwritetables" value="N" checked="checked" /> Bestaande definities aanvullen met nieuwe kolommen </p>

{literal}
<script type="text/javascript">
<!--
	var CheckAllCheckboxes = function(onoff) {
	    $('input.checkbox').attr('checked', onoff);
	}
// -->
</script>{/literal}