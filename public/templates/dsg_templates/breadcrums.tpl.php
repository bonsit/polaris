{strip}
<ol class="breadcrumb">
    {section name=i loop=$breadcrumbs}
        <li {if $smarty.section.i.last} class="active"{/if}>
        {if $breadcrumbs[i].type eq 'select'}
            &nbsp;<select id="app_select" class="breadcrumbselect" tabindex="-1">
            {section name=label loop=$breadcrumbs[i].label}
            <option value="{$breadcrumbs[i].label[label]->record->CLIENTHASHID}#{$breadcrumbs[i].label[label]->record->METANAME}"
            {if $currentapplicationmeta eq $breadcrumbs[i].label[label]->record->METANAME}selected="selected"{/if}
            >
            {$breadcrumbs[i].label[label]->record->APPLICATIONNAME|get_resource:$lang}
            </option>
            {/section}
            </select>
        {elseif $breadcrumbs[i].type eq 'raw'}
            {if $smarty.section.i.last}<strong>{/if}
            {$breadcrumbs[i].label}
            {if $smarty.section.i.last}</strong>{/if}
        {else}
            {if not $smarty.section.i.last}
            <a tabindex="-1" href="{$breadcrumbs[i].link}">
                {$breadcrumbs[i].label|get_resource:$lang|truncate:80:'...'}
            </a>
            {else}
            <strong>
                {$breadcrumbs[i].label|get_resource:$lang|truncate:80:'...'}
            </strong>
            {/if}
        {/if}
        </li>
    {/section}
</ol>
{/strip}