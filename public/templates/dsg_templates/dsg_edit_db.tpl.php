{include file="basemenu.tpl.php"}

<h2>{lang database} '{$recset.DATABASENAME}'</h2>

{include file="databasemenu.tpl.php"}

<form name="dataabase" method="post" action="?action=edit&rec={$recordid}">
<input type="hidden" name="_hdnRecordID" value="{$recordid}" />
<input type="hidden" name="_hdnAction" value="{#rec_update#}" />

<table class="data striped" summary="data"><tr><td width="30%">{lang databaseid}:</td><td><input name="DATABASEID" class="text" size="5" maxlength="5" type="text" value="{$recset.DATABASEID}" /></td></tr><tr><td>{lang client}:</td><td><input name="CLIENTID" class="text" size="5" maxlength="5" type="text" value="{$recset.CLIENTID}" /></td></tr><tr><td>{lang description}:</td><td><input name="DESCRIPTION" class="text" size="50" maxlength="50" type="text" value="{$recset.DESCRIPTION}" /></td></tr><tr><td>{lang host}:</td><td><input name="HOST" class="text" size="50" maxlength="50" type="text" value="{$recset.HOST}" /></td></tr><tr><td>{lang databasetype}:</td><td><input name="DATABASETYPE" class="text" size="13" maxlength="13" type="text" value="{$recset.DATABASETYPE}" /></td></tr><tr><td>{lang databasename}:</td><td><input name="DATABASENAME" class="text" size="50" maxlength="50" type="text" value="{$recset.DATABASENAME}" /></td></tr><tr><td>{lang rootusername}:</td><td><input name="ROOTUSERNAME" class="text" size="20" maxlength="20" type="text" value="{$recset.ROOTUSERNAME}" /></td></tr><tr><td>{lang rootpassword}:</td><td><input name="ROOTPASSWORD" class="text" size="20" maxlength="20" type="text" value="{$recset.ROOTPASSWORD}" /></td></tr><tr><td>{lang errortablename}:</td><td><input name="ERRORTABLENAME" class="text" size="50" maxlength="100" type="text" value="{$recset.ERRORTABLENAME}" /></td></tr><tr><td>{lang loginwithuseraccount}:</td><td><input name="LOGINWITHUSERACCOUNT" class="text" size="1" maxlength="1" type="text" value="{$recset.LOGINWITHUSERACCOUNT}" /></td></tr><tr><td>{lang errorcodecolumnname}:</td><td><input name="ERRORCODECOLUMNNAME" class="text" size="50" maxlength="50" type="text" value="{$recset.ERRORCODECOLUMNNAME}" /></td></tr><tr><td>{lang errormessagecolumnname}:</td><td><input name="ERRORMESSAGECOLUMNNAME" class="text" size="50" maxlength="50" type="text" value="{$recset.ERRORMESSAGECOLUMNNAME}" /></td></tr></table><input type="submit" value="{lang but_save}" /> <input type="button" value="{lang but_cancel}" class="protect" onClick="location.href='{$callerpage}'" />

</form>

{include file="footer.tpl.php"}