{include file="basemenu.tpl.php" divcontent='class="contentwithhelp"'}

<h1>{lang backup_restore}</h1>

<div>
<h2>Backup Database</h2>
<p>Kies de databases die u wilt backuppen:</p>

<form name="backupdatabase" method="get" action="{$callerquery}">
<select name="databaseid">
{section name=i loop=$databases}
  <option value="{$databases[i].RECORDID}" {if $databases[i].RECORDID eq $smarty.get.databaseid}selected="selected"{/if}>{$databases[i].DESCRIPTION} ({$databases[i].ROOTUSERNAME}@{$databases[i].DATABASENAME})</option>
{/section}
</select>

<table class="data" style="width:65%;">
<tr><td><input type="radio" id="bzip" name="backuptype" value="zip" {if $smarty.get.backuptype eq 'zip' or !isset($smarty.get.backuptype)}checked="checked"{/if}> <label for="bzip">Backup downloaden als .zip bestand</label></td><td></td></tr>
<tr><td><input type="radio" id="btxt" name="backuptype" value="txt" {if $smarty.get.backuptype eq 'txt'}checked="checked"{/if}> <label for="btxt">Backup downloaden als .txt bestand</label></td><td></td></tr>
<tr><td><input type="submit" value="{lang backup}"></td></tr>
</table>
</form>
</div>

<div><h2>Restore Database</h2>
<p class="att">Wees uitermate voorzichtig met de Restore functie! Uw huidige data wordt overschreven.</p>
<table class="data" style="width:65%">
<tr><td>Backup db-file:</td><td><input name="dbfile" type="text" size="40" value="" /></td></tr>
</table>
<input type="submit" value="{lang restore}" onclick="alert('Deze functie is nog niet beschikbaar.')">

</div>

<div id="inlinehelp"><h1>{lang backup_restore}</h1>
{if !$config.intranetinstance}<p>Heeft u een Polaris Business of Enterprise licentie dan worden automatisch iedere nacht backups van uw data gemaakt. Maar u kunt altijd zelf nog een extra backup maken voor uw eigen administratie. 
Bij een Starter licentie dient u zelf voor backups te zorgen.</p>
{else}
<p>U heeft hier de mogelijkheid backups te maken voor uw eigen administratie. 
Selecteer de gewenste database, druk op Backup en u krijgt de database-dump als Zip-bestand of tekstbestand.</p>
{/if}
<p><b>Restoren van een database?</b> Wees uitermate voorzichtig met het restore of terugzetten van een database. Doe dit het liefst in overleg met uw systeembeheerder. Het kan namelijk zijn dat u data overschrijft die u niet kwijt wilt zijn.</p>     
</div>

{include file="footer.tpl.php"}
