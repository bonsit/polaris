{include file="basemenu.tpl.php"}

{if $recordstate eq #rec_insert#}
<h1>{lang make_new_client}</h1>
<p>Hieronder kunt u de gegevens van het nieuwe formulier invullen.</p>
{else}
<h1>{lang client_account_of} {$recset.NAME}</h1>
<p>Hieronder kunt u de gegevens van {$recset.NAME} aanpassen.</p>
{/if}

<form name="clientform" method="post" action="{$callerpage}" class="validate">
<input type="hidden" name="_hdnRecordID" value="{$recordid}" />
<input type="hidden" name="_hdnAction" value="{$recordstate}" />

<table class="data striped" summary="data">
<tr><td width="30%">{lang formname}:</td>
<td><input name="FORMNAME" class="text" size="50" maxlength="50" type="text" value="{$recset.FORMNAME}" /></td></tr>
<tr><td>{lang helpnr}:</td>
<td><input name="HELPNR" class="text" size="5" maxlength="5" type="text" value="{$recset.HELPNR}" /></td></tr>
<tr><td>{lang pagecolumncount}:</td>
<td><input name="PAGECOLUMNCOUNT" class="text" size="1" maxlength="1" type="text" value="{$recset.PAGECOLUMNCOUNT}" /></td></tr>
<tr><td>{lang webpageurl}:</td>
<td><input name="WEBPAGEURL" class="text" size="50" maxlength="200" type="text" value="{$recset.WEBPAGEURL}" /></td></tr>
<tr><td>{lang databaseid}:</td>
<td><input name="DATABASEID" class="text" size="5" maxlength="5" type="text" value="{$recset.DATABASEID}" /></td></tr>
<tr><td>{lang tablename}:</td>
<td><input name="TABLENAME" class="text" size="50" maxlength="100" type="text" value="{$recset.TABLENAME}" /></td></tr>
<tr><td>{lang filter}:</td>
<td><input name="FILTER" class="text" size="50" maxlength="255" type="text" value="{$recset.FILTER}" /></td></tr>
<tr><td>{lang defaultviewtype}:</td>
<td><input name="DEFAULTVIEWTYPE" class="text" size="10" maxlength="10" type="text" value="{$recset.DEFAULTVIEWTYPE}" /></td></tr>
<tr><td>{lang showrecordsasconstructs}:</td>
<td><input name="SHOWRECORDSASCONSTRUCTS" class="text" size="1" maxlength="1" type="text" value="{$recset.SHOWRECORDSASCONSTRUCTS}" /></td></tr>
<tr><td>{lang captioncolumnname}:</td>
<td><input name="CAPTIONCOLUMNNAME" class="text" size="50" maxlength="50" type="text" value="{$recset.CAPTIONCOLUMNNAME}" /></td></tr>
<tr><td>{lang sortcolumnname}:</td>
<td><input name="SORTCOLUMNNAME" class="text" size="50" maxlength="50" type="text" value="{$recset.SORTCOLUMNNAME}" /></td></tr>
<tr><td>{lang searchcolumnname}:</td>
<td><input name="SEARCHCOLUMNNAME" class="text" size="50" maxlength="50" type="text" value="{$recset.SEARCHCOLUMNNAME}" /></td></tr>
<tr><td>{lang panelhelptext}:</td>
<td><input name="PANELHELPTEXT" class="text" size="50" maxlength="250" type="text" value="{$recset.PANELHELPTEXT}" /></td></tr>
<tr><td>{lang preload}:</td>
<td><input name="PRELOAD" class="text" size="1" maxlength="1" type="text" value="{$recset.PRELOAD}" /></td></tr>
<tr><td>{lang newrecordinformview}:</td>
<td><input name="NEWRECORDINFORMVIEW" class="text" size="1" maxlength="1" type="text" value="{$recset.NEWRECORDINFORMVIEW}" /></td></tr>
<tr><td>{lang selectview}:</td>
<td><input name="SELECTVIEW" class="text" size="50" maxlength="50" type="text" value="{$recset.SELECTVIEW}" /></td></tr>
<tr><td>{lang selectcolumnnames}:</td>
<td><input name="SELECTCOLUMNNAMES" class="text" size="50" maxlength="50" type="text" value="{$recset.SELECTCOLUMNNAMES}" /></td></tr>
<tr><td>{lang selectshowcolumns}:</td>
<td><input name="SELECTSHOWCOLUMNS" class="text" size="50" maxlength="250" type="text" value="{$recset.SELECTSHOWCOLUMNS}" /></td></tr>
<tr><td>{lang selectquery}:</td>
<td><input name="SELECTQUERY" class="text" size="20" maxlength="20" type="text" value="{$recset.SELECTQUERY}" /></td></tr>
</table><input type="submit" value="{lang but_save}" /> <input type="button" value="{lang but_cancel}" class="protect" onClick="location.href='{$callerpage}'" />

</form>

{include file="footer.tpl.php"}