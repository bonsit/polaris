{config_load file="globals.conf"}
{include file="header.tpl.php"}
{include file="basemenu.tpl.php" showmaincontentdiv="true"}

{include file="error_message.tpl.php"}
<form id="userform" class="password-change-validation">
    <input type="hidden" name="_hdnProcessedByModule" value="false" />
    <input type="hidden" name="_hdnAction" value="{$action|default:'plr_changepassword'}" />
    <input type="hidden" name="_hdnResetHash" value="{$smarty.get.resethash}" />

    <div id="dlgAddUser" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content fadeIn">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                            class="sr-only">Sluiten</span></button>

                    {if $action == 'plr_changepasswordreset'}
                    <h2 class="modal-title m-b-sm">{lang reset_your_password}</h2>
                    {else}
                    <h2 class="modal-title m-b-sm">{lang change_your_password}</h2>
                    <p style="font-size:1.3em">{lang change_password_text}</p>
                    {/if}
                </div>

                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-lg-4 control-label">{lang new_password}</label>
                        <div class="col-lg-8">
                            <input type="password" autofocus placeholder="Wachtwoord"
                                class="form-control validate cp_password required" name="newpassword" value="{$userpassword}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-4 control-label">{lang retype_password}</label>
                        <div class="col-lg-8">
                            <input type="password" placeholder="Wachtwoord"
                                class="form-control validate cp_password_comfirm required"
                                name="retypepassword" value="{$userpassword}">
                                <span class="retypecorrect"></span>
                        </div>
                    </div>

                    <div class="text-danger font-larger cp_password_strength"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" id="but_show_password_info">Wat is een sterk wachtwoord?</button>
                    <button type="button" class="btn btn-primary positive disabled" id="but_usersetting_ok">{lang but_save}</button>
                </div>
                <div class="modal-body">
                    <div id="dlgPasswordInformationBox" class="shadowbox element-detail-box" style="display:none;">
                        {include file="_changepassword_$lang.tpl.php"}
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

<script>{literal}

jQuery(function () {

    $("#but_usersetting_ok").on('click', function(e) {
        e.preventDefault();

        var form = $("#userform");
        if (form.valid()) {
            var data = Polaris.Form.convertToKeyValuePairs(form.serializeArray());
            Polaris.Ajax.postJSON(_ajaxquery+'/', data, null, 'Wachtwoord is aangepast', function(data) {
                $("#dlgAddUser").modal('hide');
                setTimeout(() => {
                    window.location.reload();
                }, 500);
            }, function(data) {
                alert(data.error);
            });
        }
    });

    $("#but_show_password_info").on('click', function(e) {
        e.preventDefault();
        $("#dlgPasswordInformationBox").show();
    });

    Polaris.Base.initPasswordValidation(".password-change-validation", function(result) {
        if (result == 'passwords_ok') {
            $("#retypecorrect").text('');
            $("#but_usersetting_ok").removeClass("disabled");
        } else {
            $("#retypecorrect").text('Wachtwoorden zijn niet gelijk!');
            $("#but_usersetting_ok").addClass("disabled");
        }
    });
});

{/literal}</script>
{include file="footer.tpl.php"}
