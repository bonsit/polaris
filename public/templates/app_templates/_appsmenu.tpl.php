<div class="xrow xequal apps-container">
{section name=i loop=$apps}
    {assign var=firstletter value=$apps[i]->record->APPTILECOLOR|substr:0:1}
    <div class="xcol-lg-3 xcol-md-5 xcol-xs-12 app-expand {if $firstletter neq '#'}{$apps[i]->record->APPTILECOLOR}{/if}"
    {if $firstletter eq '#'}
                style="background-color:{$apps[i]->record->APPTILECOLOR|default:'#e3a21a'}"{/if}>
        <a href="{$apps[i]->applink}" class="expandx">
            <div class="widget plr-app {if $firstletter neq '#'}{$apps[i]->record->APPTILECOLOR}{/if}"
                >
                <div class="app-icon">
                    {if $apps[i]->record->IMAGE|substr:0:3 == 'fa-'}
                    <i class="fa {$apps[i]->record->IMAGE} fa-3x"></i>
                    {else}
                    <i class="material-symbols-filled md-3x">{$apps[i]->record->IMAGE}</i>
                    {/if}
                </div>
                <h2 class="m-xs text-center">{$apps[i]->record->APPLICATIONNAME|replace:" ":"<br> "}</h2>
            </div>
        </a>
    </div>
{sectionelse}
    <div class="col-lg-3 col-xs-12 expand">
        <div class="panel panel-danger">
            <div class="panel-heading">
                {lang apps_non_available_header}
            </div>
            <div class="panel-body">
            <h3>{lang apps_non_available}</h3>
            </div>
        </div>
    </div>
{/section}
</div>
