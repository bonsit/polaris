{strip}
{config_load file="brandglobals.conf"}
{include file="defaultheader.tpl.php" background="gray-bg"}
<div class="xmiddle-box text-center loginscreen">
    <div class="text-center">
        <h1 class="logo-name">{$CLIENTNAME|default:$config.polarisname}</h1>
    </div>
</div>
<div class="middle-box loginscreen">
    {if $errormessage}<p class="bg-danger p-xs b-r-sm">{$errormessage}</p>{/if}
    <h3>Maak een account aan</h3>
    {if !empty($CLIENTHASH)}
    <form class="m-t" role="form" method="POST" action=".">
        <input type="hidden" name="_hdnAction" value="plr_createaccount" />
        <input type="hidden" name="CLIENTHASH" value="{$CLIENTHASH}" />
        <input type="hidden" name="LINKHASH" value="{$LINKHASH}" />
        <div class="form-group">
            <input type="text" class="form-control login-disable" placeholder="Voornaam" name="FIRSTNAME" required="required" value="{$item.VOORNAAM_VERZORGER|default:$item.VOORNAAM}">
        </div>
        <div class="form-group">
            <input type="text" class="form-control login-disable" placeholder="Achternaam" name="LASTNAME" required="required" value="{$item.ACHTERNAAM_VERZORGER|default:$item.ACHTERNAAM}">
        </div>
        <div class="form-group">
            <input type="email" class="form-control login-disable" placeholder="username" name="USERNAME" required="required" value="{$item.UITGENODIGDE_EMAIL}">
        </div>

        <div class="form-group">
            <input id="cp_password" type="password" class="form-control" placeholder="Typ nieuw wachtwoord" name="USERPASSWORD" required="">
            <span id="pwstrength"></span>
        </div>
        <div class="form-group">
            <input id="cp_retypepw" type="password" class="form-control" placeholder="Typ wachtwoord nogmaals" name="RETYPEUSERPASSWORD" required="">
            <span id="retypecorrect">
        </div>

        <div class="form-group">
            <input id="phonenumber" type="text" class="form-control" placeholder="Vul uw mobiele telefoonnummer in" name="PHONENUMBER" required=""> <small>(Als extra beveiliging worden naar dit telefoonnummer inlogcodes als SMS'jes gestuurd... Vul een telefoonnummer in met de internationale landcode. Bijvoorbeeld 31 voor Nederland)</small>
        </div>

        <button type="submit" class="btn btn-primary block full-width m-b">Aanmelden</button>
        <p class="text-muted text-center"><small>Heeft u al een account?</small> <a href="/">Login</a></p>

    </form>
    {else}
        <p>Er is geen context gevonden voor het aanmaken van een account.</p>
        <p>U dient waarschijnlijk uitgenodigd te worden om een account aan te maken.</p>
    {/if}
</div>
{include file="defaultfooter.tpl.php"}
{/strip}