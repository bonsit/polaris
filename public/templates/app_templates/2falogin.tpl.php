{if $hasbrandglobals}{config_load file="brandglobals.conf"}{/if}
{include file="defaultheader.tpl.php" title="Content Management Systeem" background="gray-bg"}

<div class="middle-box text-center loginscreen xanimated fadeInDownBig">
    <div>
        <div>
            <h1 class="logo-name">{$config.polarisname}</h1>

            {if $loginerror}
            <span style="color:#F1ABAB" class="loginerror">{$loginerror}</span>
            {/if}

            <form id="loginform" method="post" name="loginform" action="{$serverroot}">
                <input type="hidden" name="_hdnAction" value="check2fa" />
                <input type="hidden" name="_hdnPlrSes" value="{$polarissession}" />

                <h3>Vul uw controle nummer in</h3>
                <p id="login_text">Kijk op uw mobiele telefoon</p>
                <div class="form-group">
                    <input type="text" class="form-control" autofocus="true" name="twofacodecheck" id="fldTwofacodecheck" placeholder="{lang 2facodecheck}" required="" value="">
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">Check</button>
            </form>
        </div>
    </div>
</div>

{include file="defaultfooter.tpl.php"}
