{config_load file="globals.conf"}
{include file="header.tpl.php"}
{include file="basemenu.tpl.php"}

<div id="tagsky">

<div id="tagactions">
<ul>
{if $currenttag}
<li>Bekijk <a href="{$serverroot}/alltags/{$currenttag}/">ieders items<br /> met de tag <span>{$currenttag}</span></a></li>
<li>Bekijk <a href="{$serverroot}/tags/{$currenttag}/">al uw items<br /> met de tag <span>{$currenttag}</span></a></li>
{else}
<li>Bekijk <a href="{$serverroot}/alltags/">alle tags</a> van alle applicaties</li>
<li>Bekijk <a href="{$serverroot}/tags/">uw tags</a> van alle applicaties</li>
{/if}
</ul>
<hr />
<h3>Wat zijn tags?</h3>
<p>U kunt uw items (records of zoek-resultaten) een "tag" geven, oftewel een label.
Tags helpen u informatie te vinden die aan elkaar gerelateerd is. 
U kunt zoveel tags aan een item toevoegen als u wilt.</p>
</div>

<div id="tagcontainer">

<h2>
{if $currenttag}
    {if $currentaction == 'tags'}
        <a href="{$serverroot}/app/{$currentapplicationmeta}/alltags/">Alle tags</a> / <a href="{$serverroot}/app/{$currentapplicationmeta}/tags/">Uw tags</a>
    {elseif $currentaction == 'alltags'}
        <a href="{$serverroot}/app/{$currentapplicationmeta}/alltags/">Alle tags</a>
    {/if}
{else}
    {if $currentaction == 'tags'}
    <a href="{$serverroot}/app/{$currentapplicationmeta}/alltags/">Alle tags</a>
    {/if}
{/if}

<span>

{if $currenttag} / {$currenttag}
{else}
    {if $currentaction eq 'tags'} / Uw tags
    {else}Alle tags
    {/if}
{/if}</span>
<p class="fromapp">binnen {if $currentapplicationmeta ne ''}de applicatie {$applicationname}{else}alle applicaties{/if}</p>
</h2>

{if $currenttag}
    {section name=i loop=$taggedobjects}
        {if $currentapplicationmeta == ''}
            {if $taggedobjects[i].app ne $taggedobjects[i.index_prev].app}
            <h3 class="app">{$taggedobjects[i].applicationname}</h3>
            {/if}
        {/if}    
    
        {if $taggedobjects[i].const ne $taggedobjects[i.index_prev].const}
        <h4>{$taggedobjects[i].constructname}</h4>
        {/if}
        {if $taggedobjects[i].record}
        &ndash; <a href="{$serverroot}/app/{$taggedobjects[i].app}/const/{$taggedobjects[i].const}/edit/{$taggedobjects[i].record}/">Record: <strong>{$taggedobjects[i].recordcaption|default:$taggedobjects[i].record}</strong></a> &nbsp;{$taggedobjects[i].fuzzydate}, door {if $taggedobjects[i].usercount == 1}{$taggedobjects[i].usergroupname}{else}{$taggedobjects[i].usercount} personen{/if}<br />
        {else}
        &ndash; <a href="{$serverroot}/app/{$taggedobjects[i].app}/const/{$taggedobjects[i].const}/?q={$taggedobjects[i].search}">Zoekresultaat: <strong>{$taggedobjects[i].search}</strong></a> &nbsp;{$taggedobjects[i].fuzzydate}, door {if $taggedobjects[i].usercount == 1}{$taggedobjects[i].usergroupname}{else}{$taggedobjects[i].usercount} personen{/if}<br />
        {/if}
    {/section}
{else}
    <div id="tagcloud">
    {section name=i loop=$tags}
    <a href="{$serverroot}{if $currentapplicationmeta}/app/{$currentapplicationmeta}{/if}/{if $currentaction eq 'tags'}tags{else}alltags{/if}/{$tags[i].tag}/" style="font-size:{math equation="x+y" x=$tags[i].level y=12}px">
    {$tags[i].tag}
    </a>&nbsp;
    {/section}
    </div>
{/if}
</div>

</div>

{include file="footer.tpl.php"}