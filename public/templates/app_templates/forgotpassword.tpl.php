{config_load file="globals.conf"}
{include file="header.tpl.php"}
{include file="basemenu.tpl.php" showmaincontentdiv="true"}

{include file="error_message.tpl.php"}
<form id="resetform" class="password-change-validation">
    <input type="hidden" name="_hdnProcessedByModule" value="false" />
    <input type="hidden" name="_hdnAction" value="plr_forgotpassword" />

    <div id="dlgAddUser" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content fadeIn">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                            class="sr-only">Sluiten</span></button>

                    <h2 class="modal-title m-b-sm">{lang forgot_password}</h2>
                    <p style="font-size:1.3em">{lang forgot_password_text}</p>
                </div>

                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-lg-4 control-label">{lang email_address}</label>
                        <div class="col-lg-8">
                            <input type="email" autofocus placeholder="{lang email_address}"
                                class="form-control validate required" name="emailadres" value="">
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <a href="/" class="btn btn-default pull-left">{lang login}</a>
                    <button type="button" class="btn btn-primary positive" id="but_forgotpassword_ok">{lang send_email}</button>
                </div>
            </div>
        </div>
    </div>
</form>

<script>{literal}

jQuery(function () {

    $("#but_forgotpassword_ok").on('click', function(e) {
        e.preventDefault();

        var form = $("#resetform");
        if (form.valid()) {
            var data = Polaris.Form.convertToKeyValuePairs(form.serializeArray());
            Polaris.Ajax.postJSON(_ajaxquery+'/', data, null, 'We hebben een email gestuurd met een nieuwe wachtwoord link.', function(data) {
                setTimeout(() => {
                    // goto home page
                    window.location.href = '/';
                }, 500);
            }, function(data) {
                alert(data.error);
            });
        }
    });


});

{/literal}</script>
{include file="footer.tpl.php"}
