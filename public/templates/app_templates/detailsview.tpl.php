<div class="element-detail-box view-detail-box">
    <div class="row">

        <input type="hidden" name="_hdnRecordID" value="{$masterrecord->PLR__RECORDID}" />
        <div class="col-md-2 col-sm-3 col-xs-3 detailview-menu">
            {$currentform->ShowDetailformsAsMenu($currentdetailformname)}
        </div>

        <div class="col-md-10 col-sm-9 col-xs-9 detailview-content">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="detailforms-header">
                            <div class="row">
                                <div class="col-md-4">
                                    <h2>{$overviewcaption}</h2>
                                </div>
                                <div class="col-md-4">
                                    {if $currentdetailformname and !$detailformobject->HasOption('no-detailsearch')}
                                        <div class="input-group btn-sm pull-right">
                                            <input type="text" id="fldDetailSearch" class="form-control input-sm" placeholder="Zoek een item..." value="{$detailsearchvalue}">
                                            <span class="input-group-btn">
                                                <button id="btnSearchDetailform" class="btn btn-default" type="button">{icon name="search"}</button>
                                            </span>
                                    </div>
                                    {/if}
                                </div>
                                <div class="col-md-4 no-padding text-right">
                                    {if $hasinsertpermission and $currentdetailaction !== 'detail_insert'}
                                    <a href="{$detailformobject->MakeURL(false, $currentdetailformname)}insert/" type="button" class="btn btn-primary btn-sm pull-right" {*data-toggle="modal" data-target="#addNotitieModal"*}>{$addanitemlabel}</a>
                                    {/if}

                                    {if $currentform and $currentdetailformname == ''}
                                        {$currentform->actions->ShowActionButtons($currentaction, $currentdetailaction, 'TOP', 'small')}
                                    {/if}
                                    {if $detailformobject}
                                        {$detailformobject->actions->ShowActionButtons($currentaction, $currentdetailaction, 'TOP', 'small')}
                                    {/if}

                                </div>
                            </div>
                        </div>

                        {if !$currentdetailformname}
                            {$currentform->ShowFormView($currentaction, $permissiontype, $detail, $masterrecord)}
                        {else}
                            {$detailformobject->DisplayDetailTabContent($currentdetailaction, $detailpermission, $masterrecord)}

                            {if $currentdetailaction == 'detail_insert' or $currentdetailaction == 'detail_edit' or $currentdetailaction == 'detail_update'}
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="button" id="btnSaveForm" data-url="{$detailbasehref}" tabindex="1000" accesskey="F9" action="save" name="_hdnSaveButton" value="" role="button" class="btn btn-primary" title="Huidige gegevens opslaan">{lang but_save} <span>(F9)</span></button>
                                        <button type="button" tabindex="1010" accesskey="F2" role="button" class="btn btn-default xbtn-outline xnegative cancelbutton" onclick="location.href='{$detailbasehref}'" title="{lang but_cancel_hint}">{lang but_cancel} <span>(F2)</span></button>
                                    </div>
                                </div>
                            {/if}
                        {/if}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
