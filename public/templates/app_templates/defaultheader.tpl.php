<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head><title>{$config.polarisname}</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859" />
<meta name="robots" content="noindex,nofollow">

<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" media='all' href="{$serverpath}/assets/dist/main.min.css?v={#ReleaseVersion#}"  />

<!-- <link rel="stylesheet" type="text/css" media='all' href="{$serverpath}/min/g=css"  /> -->
<!-- <link rel="stylesheet" type="text/css" media='all' href="{$serverpath}/bower_components/fontawesome/css/font-awesome.css" /> -->


<!-- <link rel="stylesheet" type="text/css" media='all' href="{$serverpath}/bower_components/bootstrap/dist/css/bootstrap.css" />
<link rel="stylesheet" type="text/css" media='all' href="{$serverpath}/bower_components/toastr/toastr.css" />
<link rel="stylesheet" type="text/css" media='all' href="{$serverpath}/bower_components/animate.css/animate.css" />
<link rel="stylesheet" type="text/css" media='all' href="{$serverpath}/bower_components/bootstrap-markdown/css/bootstrap-markdown.min.css" />
<link rel="stylesheet" type="text/css" media='all' href="{$serverpath}/bower_components/iCheck/skins/square/green.css" />
<link rel="stylesheet" type="text/css" media='all' href="{$serverpath}/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.css" />
<link rel="stylesheet" type="text/css" media='all' href="{$serverpath}/css/font-financial.css" />
<link rel="stylesheet" type="text/css" media='all' href="{$serverpath}/css/font-line-icons.css" />
<link rel="stylesheet" type="text/css" media='all' href="{$serverpath}/styles/style-2.9.2.css" />
<link rel="stylesheet" type="text/css" media='all' href="{$serverpath}/styles/style-custom.css" />
<link rel="stylesheet" type="text/css" media='all' href="{$serverpath}/js2/jquery/date_input.css" />
<link rel="stylesheet" type="text/css" media='all' href="{$serverpath}/js2/jquery/date_input.css" />
<link rel="stylesheet" type="text/css" media='all' href="{$serverpath}/css/plrdefaultdark.css" /> -->

<link href="/branding/{$brand}/style.css" rel="stylesheet">

<script type="text/javascript" src="{$serverpath}/assets/js/jquery/dist/jquery.min.js"></script>
<link rel="fluid-icon" href="{$serverroot}/i/prima/plricon4.png" title="Polaris" />
<link rel="shortcut icon" type="image/x-icon" href="{$serverroot}/favicon.ico" />
<script type="">
var _loadevents_ = [], _serverroot = "{$serverroot}", _callerquery = _serverroot+"{$callerquery}", _basicformquery = _serverroot+"{$basicformquery}", _servicequery = _serverroot+"{$servicequery}", _ajaxquery = _serverroot+"{$ajaxquery}";
var _serverroot = "{$serverroot}";
</script>
</head>
<body id="{$pageid}" class="{$background}">