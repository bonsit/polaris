<div class="fh-breadcrumb wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-7 col-sm-12">
            <h1>Hallo {$smarty.session.fullname}</h1>
            <h1 class="appdescription">{lang welcome_to} {$applicationname}</h1>
            <h2>{$app->record->DESCRIPTION|get_resource:$lang}</h2>
        </div>
        <div class="col-lg-5 col-sm-12">
            <img src="{$serverroot}/branding/maxia/img/maxia_illustratie.png" width="100%" class="pull-right" />
        </div>
    </div>
    <div class="row">
        <div class="col-lg-5 col-sm-12">
            <div class="clearfix" xstyle="position:absolute;bottom:0;width:40vw;">
            <h3>{lang ready_working}</h3>
            <p>{lang logoff_text}</p>
        </div>
    </div>
</div>

{*section loop=$dashboard_containers name=i}
<div class="col-lg-3">
    <div class="ibox xfloat-e-margins">
        <div class="ibox-title">
            <span class="label label-{cycle values='success,info,primary,danger'} pull-right">Monthly</span>
            <h5>{$dashboard_containers[i].CONSTRUCTNAME|get_resource}</h5>
        </div>
        <div class="ibox-content">
            <h1 class="no-margins">{$dashboard_containers[i].RECORDCOUNTER}</h1>
            <div class="stat-percent font-bold text-success">98% <i class="fa fa-bolt"></i></div>
            <small>Total income</small>
        </div>
    </div>
</div>
{/section*}