<div class="tabs-container">
    <ul class="nav nav-tabs">
        {if ($permission & $smarty.const.EXPORT) eq  $smarty.const.EXPORT}
        <li class="active"><a href="#export" data-toggle="tab" class="selected">{lang export_action}</a></li>
        {/if}
        {if ($permission & $smarty.const.IMPORT) eq  $smarty.const.IMPORT}
        <li ><a href="#import" data-toggle="tab">{lang import_action}</a></li>
        {/if}
    </ul>
</div>

<div class="tab-content">
    <div id="import" class="tab-pane element-detail-box">
        {if ($permission & $smarty.const.IMPORT) eq  $smarty.const.IMPORT}
        <p>Under construction...</p>
        {/if}
    </div>
    <div id="export" class="tab-pane active element-detail-box">
        {if ($permission & $smarty.const.EXPORT) eq  $smarty.const.EXPORT}
        <form method="GET" action="{$baseurl}impexp/">
            <!-- <input type="hidden" name="method" value="plr_export" /> -->
            <!-- <input type="hidden" name="_hdnAction" value="plr_export" /> -->
            <input type="hidden" name="q" value="{$smarty.get.q}" />
            <input type="hidden" name="tag" value="{$smarty.get.tag}" />
            <input type="hidden" name="dest" value="download" />
            <h2>Wat dient er ge&euml;xporteerd te worden?</h2>

            <div class="radio radio-danger">
            <input name="exportresult" id="export_all" value="export_all" type="radio" class="icheck" checked="checked" /> <label for="export_all">Exporteer alle {$cons->record->CONSTRUCTNAME|get_resource:$lang}-records</label><br />
            <input name="exportresult" id="export_limit" value="export_limit" type="radio" class="icheck" /> <label for="export_limit">Exporteer <input size="5" style="font-size:0.85em" name="exportlimit" value="1000" /> {$cons->record->CONSTRUCTNAME|get_resource:$lang}-records beginnend vanaf record <input size="5" name="exportoffset" style="font-size:0.85em" value="{$smarty.get.exportoffset|default:$smarty.get.offset}" /></label><br />
            <input name="exportresult" id="export_search" value="export_search" type="radio" class="icheck" {if !isset($smarty.get.q) and !isset($smarty.get.tag)}disabled="disabled"{/if} /> <label for="export_search"><span style="{if isset($smarty.get.q) or isset($smarty.get.tag)}{else}color:silver{/if}">Exporteer het zoekresultaat {if isset($smarty.get.tag)}(met tag "{$smarty.get.tag}"){/if}</span></label>
            </div>
            <p></p>
            <h2>Naar welk bestandsformaat?</h2>
            <p>
            <div class="radio">

            <input name="exporttype" id="export_csv_comma" class="icheck" value="export_csv_comma" type="radio" checked="checked" /> <label for="export_csv_comma"> {icon name="md-export_notes"} als komma-CSV tekst bestand.</label><br />
            <input name="exporttype" id="export_csv_tab" class="icheck" value="export_csv_tab" type="radio" /> <label for="export_csv_tab"> {icon name="md-export_notes"} als Tab-CSV tekst bestand.</label><br />
            <input name="exporttype" id="export_xls" class="icheck" value="export_xls" type="radio" /> <label for="export_xls"> {icon name="md-export_notes"} als Excel (XSL) bestand.</label><br />
            <input name="exporttype" id="export_xml" class="icheck" value="export_xml" type="radio" /> <label for="export_xml"> {icon name="md-code"} als standaard XML bestand.</label><br />
            <input name="exporttype" id="export_msxml" class="icheck" value="export_msxml" type="radio" /> <label for="export_msxml"> {icon name="md-code"} als XML bestand volgens de schemas-microsoft-com:rowset definitie.</label><br />
            {*<input name="exporttype" id="export_html" class="icheck" value="export_html" type="radio" /> <label for="export_html"> als HTML bestand in een &lt;table&gt; structuur.</label>*}
            </p>
            {*<p>Exporteer de gegevens en <br />
            <input name="exportdestination" id="dest_window" type="radio" /><label for="dest_window"> toon het resultaat in een nieuw venster.</label><br />
            <input name="exportdestination" id="dest_file" type="radio" /><label for="dest_file"> download het resultaat als een bestand.</label>
            </p>*}
            </div>
            <div class="buttons">
                <button type="submit" class="btn btn-primary" name="submit">{lang but_download}</button>
                {*<button type="button" class="btn btn-default" onclick="location.href='{$basehref}'">{lang but_cancel}</button>*}
            </div>
        </form>
    </div>
</div>
{else}
<p>{lang export_no_permission}</p>
{/if}
