{strip}
<div id="topbar" class="border-bottom white-bg">
    <nav class="navbar navbar-static-top" role="navigation">
        <div class="navbar-header">
            <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                <i class="fa fa-reorder"></i>
            </button>
            <a href="#" class="navbar-brand">{$app->record->APPLICATIONNAME|get_resource}</a>
        </div>
        <div class="col-sm-6">
            {include file="breadcrumbs.tpl.php"}
        </div>
        <div class="navbar-collapse collapse" id="navbar">
            {include file="featuresection.tpl.php"}
        </div>
    </nav>
</div>
{/strip}
