{**
* Only show the action items when the form is generated.
* If it is custom made, do not show anything, unless the 'showaction' parameter is set to 'true'.
* because we cannot be sure what the actions are.
*}
{if $currentformtype == 'master'}
{assign var=currentpermission value=$permission}
{else}
{assign var=currentpermission value=$detailpermission}
{/if}

{if $smarty.get.select == 'true'}

<i id="closePopupForm" onclick="Polaris.Form.closePopup();" class="fa fa-times-circle fa-2x"></i>
<ul id="{$currentformtype|default:'master'}actions" class="mdactions action-bar">
    <li class="insert">
        {if $currentformtype == 'master'}
        <a id="action_{$currentformtype}insert" class="bnt btn-primary" accesskey="{$smarty.const.AK_NEWRECORD}"
            href="{$baseurl}insert/{$basequery}">
            {else}
            <a id="action_{$currentformtype}insert" class="bnt btn-primary" accesskey="{$smarty.const.AK_NEWRECORD}"
                href="{$baseurl}detail/{$smarty.get.rec}/{$currentform->record->METANAME}/insert/{$basequery}">
                {/if}
                {$add_item_text|sprintf:$currentform->ItemName()}</a>
    </li>

    {if (($currentpermission & $smarty.const.SEARCH) eq $smarty.const.SEARCH)}
    {if $currentformtype == 'master'}
    <li id="action_mastersearch" class="search"><a href="{$baseurl}searchext/{$basequery}"
            title="{lang search_advanced}" accesskey="{$smarty.const.AK_FIND}">{lang search}</a></li>
    <li class="skip">
        <form class="searchform" method="get" action="{$baseurl}">
            {else}
    <li class="search skip"><a href="{$baseurl}detail/{$smarty.get.rec}/detailtabel/searchext/"
            title="{lang search_advanced}" accesskey="{$smarty.const.AK_FIND}">{lang search}</a></li>
    <li class="skip">
        <form class="searchform" method="get"
            action="{$baseurl}{if $smarty.get.donly eq 'true' and $smarty.get.rec ne ''}detail/{$smarty.get.rec}/detailtabel/{/if}" />
        {/if}
        {*if $smarty.get.donly eq 'true' and $smarty.get.rec ne ''}<input type="hidden" name="rec"
            value="{$smarty.get.rec}" />{/if*}
        {if $maximizeform}<input type="hidden" name="maximize" value="true" />{/if}
        {if $selectform}
        <input type="hidden" name="select" value="true" />
        <input type="hidden" name="selectfield" value="{$selectfield}" />
        <input type="hidden" name="selectmasterfield" value="{$selectmasterfield}" />
        <input type="hidden" name="selectshowfield" value="{$selectshowfield}" />
        {/if}
        <input type="search" placeholder="" incremental="incremental" autosave="__plrsearch" results="5"
            id="searchquery" name="q" value="{if !is_array($smarty.get.q)}{$smarty.get.q}{/if}" /><input type="submit"
            tabindex="-1" value="{lang go}" /></form>
    </li>
    {/if}
</ul>
{else}
{if ($currentform and $currentform->record->WEBPAGEURL == ''
and ($currentform->record->MODULE == '' or ($currentform->record->MODULE != '' and
$currentform->HasOption('show-form-actions')))
and $cons->record->CONSTRUCTTYPE != 'GROUP')
}

<div class="row row-no-padding d-print-none header-actions">
    <div class="col-sm-12 col-md-3">
        <ul id="{$currentformtype|default:'master'}actions" class="mdactions action-bar">

            {*if $currentaction == 'edit' and !$currentform->HasOption('no-pinning')}
            <a class="pull-left" href="javascript:void(0)" style="font-size:0.7em;line-height: 0.5em;"
                id="pinrecord_single" title="{lang pin_this_record}">
                <span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i
                        class="fa {#pinrecord_icon#} fa-stack-1x  fa-inverse"></i></span></a> &nbsp;
            {/if*}

            {*if (($currentpermission & $smarty.const.INSERT) eq $smarty.const.INSERT)}
            <li class="insert">
                <a id="action_insert_side" accesskey="{$smarty.const.AK_NEWRECORD}"
                    href="{$baseurl}insert/{$basequery}">
                    Voegtoe 2</a>
            </li>
            {/if*}

            {if (($currentaction eq 'allitems' or $currentaction eq 'search') and ($currentpermission & $smarty.const.INSERT) eq $smarty.const.INSERT)}
            <li class="insert">
                {if $currentformtype == 'master'}
                    <a id="action_{$currentformtype}insert" class="btn btn-primary" accesskey="{$smarty.const.AK_NEWRECORD}"
                    href="{$baseurl}insert/{$nosearchquery}">
                {else}
                    <a id="action_{$currentformtype}insert" class="btn btn-primary"
                        accesskey="{$smarty.const.AK_NEWRECORD}"
                        href="{$baseurl}detail/{$smarty.get.rec}/{$currentform->record->METANAME}/insert/{$basequery}">
                {/if}
                    {icon name="md-add"}
                    {$add_item_text|sprintf:$currentform->ItemName()}
                </a>
            </li>
            {/if}

            {if (($currentpermission & $smarty.const.DOCLONE) eq $smarty.const.DOCLONE) and ($currentformtype ==
            'master' and ($currentaction == 'edit' or $currentaction == 'clone'))
            or
            ($currentformtype == 'detail' and ($currentdetailaction == 'detail_edit'
            or
            ($currentdetailaction == 'detail_clone')))}
            <li id="action_{$currentformtype}clone" class="clone">
                {if $currentformtype == 'master'}
                <a href="{$baseurl}clone/{$smarty.get.rec}/{$basequery}">
                {else}
                <a href="{$baseurl}detail/{$smarty.get.rec}/{$smarty.get.detailform}/clone/{$smarty.get.detailrec}/{$basequery}">
                {/if}
                {lang clone_item}</a>
            </li>
            {/if}

            {*
            <li id="action_{$currentformtype}all" class="allitems">
                {if $currentformtype == 'master'}
                <a href="{$baseurl}{$nosearchquery}" accesskey="{$smarty.const.AK_ALLRECORDS}">
                {else}
                <a href="{$baseurl}detail/{$smarty.get.rec}/{$currentform->record->METANAME}/{$nosearchquery}"
                        accesskey="{$smarty.const.AK_ALLRECORDS}">
                {/if}
                    {lang all} {if $currentformtype == 'master' and $formhasactivepins}gepinde
                {/if}{$currentform->ItemsName()}
            *}

            {if $currentformtype == 'detail' and isset($currentdetailform) and
            $currentform->GetRecordCount() > 0}({$currentform->GetRecordCount()})
            {/if}
            {*
                        </a>
            </li>
            *}

            {if $clientselect}
            <li class="skip"> &nbsp; &nbsp; Huidige klant: <form method="get" action="?method=clientselect">
                    {$clientselect}</form>
            </li>
            {/if}
        </ul>
    </div>

    {if (($currentaction eq 'allitems' or $currentaction eq 'search') and (
        ($currentpermission & $smarty.const.INSERT) eq $smarty.const.INSERT
        OR ($currentpermission & $smarty.const.EXPORT) eq $smarty.const.EXPORT
        OR ($currentpermission & $smarty.const.SELECT) eq $smarty.const.SELECT
    ))
    }

    <div class="col-sm-12 col-md-6 text-center action-bar">
        {if $currentform->formtags->tags|count > 0}
        {if $currentaction !== 'edit'}
        {$currentform->formtags->ShowTags()}
        {/if}
        {/if}
    </div>

    <div class="col-sm-12 col-md-3 action-bar">
        {if $currentform->actions->actions|@count > 2}
        <div class="btn-group pull-right" id="dropdownMenuButtonGroup">
            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                {icon name="md-flash_on"} Acties
                &nbsp;<i class="fa fa-angle-down"></i>
            </button>
            <ul class="dropdown-menu">
                {$currentform->actions->ShowActionAsDropdownItem($currentaction, false, 'TOP', 'small')}
            </ul>
        </div>
        {else}
        <div class="topbuttons buttons pull-right">
            {$currentform->actions->ShowActionButtons($currentaction, 'TOP', 'small')}

            {if (($currentpermission & $smarty.const.IMPORT) eq $smarty.const.IMPORT)
            or (($currentpermission & $smarty.const.EXPORT) eq $smarty.const.EXPORT)}
            <a href="#" class="feature dropdown-toggle" data-toggle="dropdown">{icon name="menu" size="2x"}</a>
            <ul class="dropdown-menu dropdown-menu-right feature-section-user-dropdownx" role="menu">
                <li class="dropdown-item">
                    <a id="btnDataviewImportExport" href="{$baseurl}impexp/{$basequery}">
                        {if ($currentpermission & $smarty.const.IMPORT + $smarty.const.EXPORT) eq $smarty.const.IMPORT +
                        $smarty.const.EXPORT}
                        {lang import_export}{elseif ($currentpermission & $smarty.const.IMPORT) eq
                        $smarty.const.IMPORT}{lang import}{else}{lang export}
                        {/if}
                    </a>
                </li>

            </ul>
            {/if}

        </div>
        {/if}
    </div>
    {/if}
</div>
{/if}
{/if}