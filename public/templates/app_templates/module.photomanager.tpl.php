<script language="JavaScript" type="text/javascript" src="{$serverroot}/modules/module.photomanager/javascript/core.js"></script>
<script language="JavaScript" type="text/javascript" src="{$serverroot}/modules/module.photomanager/javascript/events.js"></script>
<script language="JavaScript" type="text/javascript" src="{$serverroot}/modules/module.photomanager/javascript/css.js"></script>
<script language="JavaScript" type="text/javascript" src="{$serverroot}/modules/module.photomanager/javascript/coordinates.js"></script>
<script language="JavaScript" type="text/javascript" src="{$serverroot}/modules/module.photomanager/javascript/drag.js"></script>
<script language="JavaScript" type="text/javascript" src="{$serverroot}/modules/module.photomanager/javascript/dragsort.js"></script>

<link rel="stylesheet" type="text/css" media='all' href="{$serverroot}/modules/module.photomanager/module.photomanager.css" />

<div id="pm_addphoto"><div id="pm_addphoto_content">{lang image_add_new}: 
    </form>
    <form action="." method="post" enctype="multipart/form-data">
    <input type="hidden" name="MAX_FILE_SIZE" value="500000" />
    <input type="file" name="imagefile1" size="20" /> 
    <input type="hidden" name="_hdnProcessedByModule" value="true" />
    <input type="submit" name="upload" value="{lang but_upload}" />
    &nbsp;<input type="checkbox" name="overwrite" value="yes" /> huidig bestand overschrijven?
    </form>
    <div style="float:right"><div id="ajax_action" style="display:none;font-weight:bold">Volgorde is aangepast</div></div>
</div></div>

<form name="dataform" id="dataform" method="post" action=".">
<input type="hidden" name="_hdnAction" value="delete" />
<input type="hidden" name="_hdnDatabase" value="{$database}" />
<input type="hidden" name="_hdnTable" value="{$tablename}" />
<input type="hidden" name="_hdnProcessedByModule" value="true" />

<ul id="pm_imagebox">
{section name=i loop=$photos}
<li id="pm_{$smarty.section.i.index}" class="pm_image" itemid="{$photos[i].FOTOID}" >
<div class="handle" >
<img src="/thumb/?src={$dir}/{$photos[i].FOTOURL}&w=120&h=90" title="{$photos[i].FOTOURL}" />
</div>
<div class="pm_actions">
<div class="pm_imagename"><abbr title="{$photos[i].FOTOURL}">{$photos[i].FOTOURL|truncate:25}</abbr></div>
<a href="{$web}/{$photos[i].FOTOURL}"><img src="{$serverroot}/images/zoom.png" title="{lang image_view_normal_size}" alt="{lang image_view_normal_size}" style="vertical-align:top" /></a>
<input id="rec:{$photos[i].PLR__RECORDID}" name="rec:{$photos[i].PLR__RECORDID}" type="checkbox" class="checkbox" />
</div>
</li>
{/section}
</ul>
<hr class="fix" /><br />
<input type="submit" value="{lang but_delete}" id="pm_delete" /> &nbsp;<a href="#" onclick="javascript:selectAll()">Alles selecteren</a> &nbsp;<a href="#" onclick="javascript:selectNone()">Niets selecteren</a>

{literal}
<script type="text/javascript">
var dragsort = ToolMan.dragsort()

var _selectCheckboxes = function(onoff) {
    map(function(e) {e.checked = onoff} , getElementsByTagAndClassName('input', 'checkbox', 'pm_imagebox'));
}
var selectAll = partial(_selectCheckboxes, true);
var selectNone = partial(_selectCheckboxes, false);

/***
    Make the photo order persistent in de db via Ajax
**/
var gotResponseData = function (response) {
    if (hasMochiKitEffects) {
        appear('ajax_action', {duration:0.4});
        setTimeout("fade('ajax_action', {duration:1.2})",1500);
    } else {
        showElement('ajax_action');
        setTimeout("hideElement('ajax_action')",1500);
    }
};
var responseFetchFailed = function (err) {
  //alert('De sortering kon niet worden toegepast. Controleer uw parameters. ('+err+')');
};

var serializeList = function(list) {
    var items = list.getElementsByTagName("li")
    var array = new Array()
    for (var i = 0, n = items.length; i < n; i++) {
        var item = items[i]
    
        array.push(item.getAttribute('itemid'))
    }
    return array.join('|')
}

var saveOrder = function (item) {
    var group = item.toolManDragGroup;
    var list = group.element.parentNode;
//    group.setHandle(MochiKit.DOM.getFirstElementByTagAndClassName(null, 'handle', list))
    var id = list.getAttribute("id");
    if (id == null) return;
    group.register('dragend', function() {
        doSimpleXMLHttpRequest(window.location.href, {
            output:'xml',
            ajaxaction: 'pm_sortorder',
            order: serializeList(list)
        }).addCallbacks(gotResponseData, responseFetchFailed);
    })
}

addLoadEvent(function() {
    dragsort.makeListSortable(document.getElementById("pm_imagebox"), saveOrder);
    if (MochiKit.Base.isSafari())
        mconnect(getElementsByTagAndClassName('input', 'checkbox', 'pm_imagebox'), 'onclick', function(e) { e.stop(); e.target().checked = !(e.target().checked);  });
    connect('pm_delete', 'onclick', function(e) {
        if (CheckDel(e)) 
            e.target().form.submit();
        else
            e.stop();
    });
    roundElement('pm_addphoto');
});

</script>
{/literal}