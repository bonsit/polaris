<h2>Was ist ein sicheres Passwort?</h2>
<p style="width:300px;">Ein sicheres Passwort besteht aus mindestens 8 Zeichen, enthält eine Kombination von Zahlen, Buchstaben und Sonderzeichen.<p>
    <h3 style="color:red">SCHLECHTE Passwörter:</h3>
    <p style="color:red">12345678<br/>
    passwört<br/>
    cc1234<br/>
    Tisch</p>
    <h3 style="color:green">GUTE Passwörter <span style="font-weight:normal">(nachfolgenden Beispielen nicht gebrauchen!)</span>:</h3>
    <p style="color:green">$Uperm@n<br/>
    DiMp1960  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="color:black">(<u><b>D</b></u>ies <u><b>i</b></u>st <u><b>M</b></u>ein <u><b>p</b></u>asswort)</span><br/>
    WNHWML!1982  &nbsp;<span style="color:black">(<u><b>W</b></u>er <u><b>N</b></u>icht <u><b>H</b></u>ören <u><b>W</b></u>ill, <u><b>M</b></u>uss <u><b>L</b></u>esen)</span><br/>
    </p>
