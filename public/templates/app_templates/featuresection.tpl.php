<div class="feature-button-group">
    {if $maintenance|is_array and $maintenance|@count > 0}
    <div id="maintenance_window" style="display:none">{icon name="md-warning"} {lang polaris} {lang maintenance} {lang maintenance1} {$maintenance[0].starttime} {lang maintenance2} (over <span id="mw_start">?</span>). &nbsp; {lang maintenance3}: <span id="mw_duration">?</span> min.</div>
    {/if}

    {if $smarty.session.changepassword !== 'Y'}
    <a class="feature" href="#" id="logboek-item-quick-toevoegen" data-toggle="tooltip" data-placement="bottom" title="{lang add_logbook_entry}">{icon name="md-playlist_add_circle"}</a>
    {/if}
    <a class="feature" href="#" id="maintenance-alert" data-toggle="tooltip" data-placement="bottom" title="Geen meldingen">{icon name="md-circle_notifications"}</a>
    <a class="feature" href="{$config.servicedeskurl}" data-toggle="tooltip" data-placement="bottom" title="Krijg hulp of geef feedback" target="_blank">{icon name="md-help"}</a>
    {if $smarty.session.plrauth eq 'true'}
    <div class="dropdown" style="display:inline">
        <a href="#" class="feature dropdown-toggle" data-toggle="dropdown">{icon name="md-account_circle"}</a>
        <ul class="dropdown-menu dropdown-menu-right feature-section-user-dropdown" role="menu">
            <li class="dropdown-header"><h6>{$client->NAME|strtoupper}</h6><h4>{$username|strtoupper}</h4></li>
            <li id="usersettings" data-toggle="modal" data-target="#dlgUserSettings" class="dropdown-item" ><a tabindex="-1" href="{$serverroot}/user/">{icon name="md-manage_accounts"} {lang user_settings}</a></li>
            <li class="dropdown-divider"></li>
            {if $smarty.session.usertype == 'client' or $smarty.session.usertype == 'root'}
            <li><a href="{$applicationurl}const/gebruikersbeheer/">{icon name="md-groups"} Organisatiebeheer</a></li>
            <li><a href="{$applicationurl}const/parameters/">{icon name="md-toggle_on"} Site instellingen</a></li>
            {/if}
            {if $smarty.session.usertype == 'root'}
            <li><a tabindex="-1" target="_blank" href="{$serverroot}/designer/">{icon name="md-bolt"} {lang polaris_designer}</a></li>
            <li id="showadvancedinfo"><a tabindex="-1" href="javascript:void(0)">{icon name="md-share"} Advanced info</a></li>
            <li id="showtimertoggle"><a href="javascript:void(0)">{icon name="md-network_check"} {lang show_page_stats}</a></li>
            {/if}

            {if $config.showdatabaseconnection}
            <li class="disabled">
                <a href="javascript:void(0)">{icon name="md-link"} <span title="{$currentform->database->record->HOST}, Groups: {$groupvalues}">Connection {$currentuserdbinstance|default:'none'}</span></a>
            </li>
            <li id="btnShowHelp"><a tabindex="-1" href="#">{icon name="md-help"} {lang help}</a></li>
            <li class="divider"></li>
            {/if}
            {if $config.debug}
            <li class="disabled">
                <a href="javascript:void(0)"><span title="{$currentform->database->record->HOST}">Groups: {$groupvalues}</span>
                </a>
            </li>
            {/if}
            <li class="divider"></li>
            <li><a tabindex="-1" href="{$serverroot}/?method=logout">{icon name="md-power_settings_new"} {lang logout}</a></li>
        </ul>
    </div>
    {/if}
</div>
