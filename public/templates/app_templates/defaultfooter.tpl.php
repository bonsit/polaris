<div id="footer">{#PolarisCopyright#}</div>

<script type="text/javascript">
var g_lang = "{$lang}";

{literal}
if ($("input[name=LINKHASH]").val() !== '') {
    $('.login-disable').prop('readonly','readonly');
}
$("#fldForgotPassword").click( function(e) {
    e.preventDefault();
    if ($("#fldUserName").val() != '') {
        $.post(
            _serverroot+'/ajax/json/'
            , {_hdnAction: 'plr_forgotpassword', emailadres: $("#fldUserName").val()}
            , function(data, textStatus) {
                if (textStatus == 'success' && data.result == true) {
                    alert('We hebben een email gestuurd, waarmee u uw wachtwoord opnieuw kunt instellen.');
                }
            }
            , "json"
        );
    } else {
        alert('U dient een emailadres in te vullen');
    }
});
{/literal}
</script>

</body>
</html>