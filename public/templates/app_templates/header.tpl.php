<!DOCTYPE html>
<html lang="nl">
<head>
<title>{if isset($constructname) and $constructname ne ''}{$constructname|get_resource:$lang} - {/if}{if isset($applicationname) and $applicationname ne ''}{$applicationname} - {/if}{lang pagetitle}</title>

<meta http-equiv="Content-Type" content="text/html; charset=utf8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="theme-color" content="#524b4d">

<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
<link rel="manifest" href="/site.webmanifest">

{if $new_relic_active == 'true'}{include file="new-relic-js.tpl.php"}{/if}

{if isset($currentform) and $currentform->record->RSSCOLUMNS}<link rel="alternate" type="application/rss+xml" title="{$currentform->record->FORMNAME}" href="{$currentform->RSSLink()}" />{/if}
<link rel="stylesheet" type="text/css" media='all' href="{$serverpath}/assets/dist/main.min.css?v={#BuildVersion#}"  />
<link rel="stylesheet" type="text/css" media='all' href="{$serverpath}/modules/_shared/module.plr_maxia/css/default.css?v={#BuildVersion#}"  />

{if $smarty.session.branding}
<link rel="stylesheet" type="text/css" media='all' href="{$serverpath}/branding/{$smarty.session.brand}/style.css?v={#BuildVersion#}" />
{/if}
{if $clientstylesheet}
<link rel="stylesheet" type="text/css" media='all' href="{$serverpath}/{$clientstylesheet}" />
{/if}
{if $appstylesheet}
<link rel="stylesheet" type="text/css" media='all' href="{$serverpath}/{$appstylesheet}" />
{/if}
{*if $userstylesheet}
<link rel="stylesheet" type="text/css" media='all' href="{$serverpath}/css/{$userstylesheet}" />
{/if*}

<script type="text/javascript" src="{$serverpath}/assets/js/jquery/dist/jquery.min.js"></script>

<!-- include summernote css/js -->
<link href="{$serverpath}/assets/dist/summernote/summernote.min.css" rel="stylesheet">
<script src="{$serverpath}/assets/dist/summernote/summernote.min.js"></script>
<script src="{$serverpath}/assets/dist/summernote/lang/summernote-nl-NL.min.js"></script>

<script type="text/javascript">
var _loadevents_=[],_serverroot="{$serverroot}",_callerquery=_serverroot+"{$callerquery}",_callerquerydetail=_serverroot+"{$callerquery}",_servicequery=_serverroot+"{$servicequery}",_ajaxquery=_serverroot+"{$ajaxquery}";
var isoracle={$isoracle|default:'false'}, cfp = {$permission|default:-1}, formid = "{$currentformid|default:-1}", g_lang = "{$lang}", brand_name = "{lang polaris}";
{if $maintenance|@count > 0}
var mnt_duration={$maintenance[0].duration|default:0},mnt_start="{$maintenance[0].starttime}",mnt_secondstostart={$maintenance[0].secondstostart|default:10000},mnt_remark="{$maintenance[0].remark}";
var mnt_skip_message={if $smarty.session.usertype == 'client' or $smarty.session.usertype == 'root'}true{else}false{/if}
{else}
var mnt_duration=false,mnt_start=false,mnt_remark="",mnt_secondstostart=false;
{/if}
</script>
{if $headercolor}
<style>
    #pageheader, .breadcrumb, .constimg, .breadcrumb > li + li:before {ldelim} color:#fff; background-color: {$headercolor}; {rdelim}
</style>
{/if}
</head>
<body id="const_{$currentformid}" class="canvas {if $navigationstyle == 'horizontal'}top-navigation{/if} full-height-layout {if $selectmode}body-small{/if} {$service} {$currentaction} {$currentdetailaction} {if !$maximizeform}{$navigationstyle}{/if} search-{$searchstyle}">
<span tabindex="0" id="prevent-outside-tab"></span>