{strip}
<div id="topbar" class="border-bottom white-bg">
<nav class="navbar xnavbar-static-top" role="navigation">
    <div class="navbar-header">
        <span class="navbar-brand">
        <span href="{$serverroot}" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{icon name="md-apps" size="1x"}</span>
        {include file="_appsmenusmall.tpl.php"}
        {$app->record->APPLICATIONNAME|get_resource}
        </span>
    </div>
    <div class="navbar-collapse collapse" id="navbar">
        <ul class="nav navbar-nav">

        {if $constructs|@count > 0}
        {section name=i loop=$constructs}
        {if $constructs[i].CONSTRUCTTYPE == 'CONTENT' or $constructs[i].CONSTRUCTTYPE == 'GROUP'}
          {if ($currentconstructid == $constructs[i].CONSTRUCTID)}
            {assign var=menutype value='active '}
          {elseif $constructs[i].METANAME == 'website'}
            {assign var=menutype value='special '}
          {else}
            {assign var=menutype value='normalmenu '}
          {/if}
          <li class='desaturate {$constructs[i].CONSTRUCTNAME|get_resource:$lang|lower|replace:' ':'-'} {$menutype}
          {if ($constructs[i].ACTIVE == true) or ($constructs[i].CONSTRUCTTYPE == 'GROUP' and $constructs[i].INITIALGROUPSTATE == 'O')} active{else} {/if}
          '>
          {if $constructs[i].CONSTRUCTTYPE == 'GROUP'}
               <a tabindex="-1" href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
          {else}
               <a tabindex="-1" href="{$app->applink}const/{$constructs[i].METANAME}/"
                {if $constructs[i].ACCESSKEY != ''}accesskey="{$constructs[i].ACCESSKEY}"{/if}>
          {/if}
              {if $constructs[i].IMAGEURL neq '' and $constructs[i].IMAGEURL neq '0'}
                {if $constructs[i].IMAGEURL|substr:0:3 eq 'fa-'}
                <i class="fa {$constructs[i].IMAGEURL} fa-lg fa-fw"></i>
                {elseif $constructs[i].IMAGEURL|substr:0:9 eq 'glyphicon'}
                <i class="glyphicons {$constructs[i].IMAGEURL}" aria-hidden="true"></i>
                {elseif $constructs[i].IMAGEURL|substr:0:5 eq 'icon-' or $constructs[i].IMAGEURL|substr:0:6 eq 'ficon-'}
                <i class="icon {$constructs[i].IMAGEURL}" aria-hidden="true"></i>&nbsp;
                {/if}
              {/if}
          {if $constructs[i].CONSTRUCTTYPE == 'GROUP' and $navigationstyle eq 'vertical'}
          {/if}
          {if $constructs[i].RECORDCOUNTER !== false}
          <span class="badge badge-primary">{$constructs[i].RECORDCOUNTER}</span>
          {/if}
          <span class="nav-label">
          {if $navigationstyle eq 'horizontal'}
          {$constructs[i].CONSTRUCTNAME|get_resource:$lang|replace:' ':'&nbsp;'}
          {else}
          {$constructs[i].CONSTRUCTNAME|get_resource:$lang|truncate:28:"...":true:true}
          {/if}
          {*if ($currentconstructid == $constructs[i].CONSTRUCTID and $navigationstyle ne 'horizontal')}&nbsp;&#0187;{/if*}
          {if $constructs[i].CONSTRUCTTYPE == 'GROUP' and $navigationstyle eq 'horizontal'}
          {/if}
          </span>
          {if $constructs[i].CONSTRUCTTYPE == 'GROUP'}
          &nbsp; <span class="fa fa-angle-down"></span>
          {/if}
        </a>

          {if $constructs[i].CONSTRUCTTYPE == 'GROUP'}
            <ul role="menu" class="dropdown-menu {if $currentconstructid == $constructs[i].CONSTRUCTID}in{/if}">
                {section name=j loop=$constructs[i].groupitems}
                {if $constructs[i].groupitems[j].CONSTRUCTTYPE == 'CONTENT' or $constructs[i].groupitems[j].CONSTRUCTTYPE == 'GROUP'}
                  {if ($currentconstructid == $constructs[i].groupitems[j].CONSTRUCTID)}
                    {assign var=menutype value='active '}
                  {elseif $constructs[i].groupitems[j].METANAME == 'website'}
                    {assign var=menutype value='special '}
                  {else}
                    {assign var=menutype value='normalmenu '}
                  {/if}

                  <li class='{$menutype}'>
                  <a tabindex="-1" href="{$app->applink}const/{$constructs[i].groupitems[j].METANAME}/{if $constructs[i].groupitems[j].OPTIONS|strpos:'show-insert-form' !== false}insert/{/if}"

                    {if $constructs[i].groupitems[j].ACCESSKEY != ''}
                        accesskey="{$constructs[i].groupitems[j].ACCESSKEY}"
                    {/if}
                    >

                  {*if $constructs[i].groupitems[j].IMAGEURL neq '' and $constructs[i].IMAGEURL neq '0'}
                    {if $constructs[i].groupitems[j].IMAGEURL|substr:0:3 eq 'fa-'}
                    <i class="fa {$constructs[i].groupitems[j].IMAGEURL} fa-lg fa-fw"></i>&nbsp;
                    {elseif $constructs[i].groupitems[j].IMAGEURL|substr:0:9 eq 'glyphicon'}
                    <i class="glyphicons {$constructs[i].groupitems[j].IMAGEURL}" aria-hidden="true"></i>&nbsp;
                    {elseif $constructs[i].groupitems[j].IMAGEURL|substr:0:5 eq 'icon-' or $constructs[i].groupitems[j].IMAGEURL|substr:0:6 eq 'ficon-'}
                    <i class="icon {$constructs[i].groupitems[j].IMAGEURL}" aria-hidden="true"></i>&nbsp;
                    {else}
                    <img src="{$serverpath}{$constructs[i].groupitems[j].IMAGEURL}" class="im" alt="" />&nbsp;
                    {/if}
                  {/if*}

                  {$constructs[i].groupitems[j].CONSTRUCTNAME|get_resource:$lang}
                  {if $constructs[i].groupitems[j].RECORDCOUNTER !== false}
                    <span class="badge pull-right">{$constructs[i].groupitems[j].RECORDCOUNTER}</span>
                  {/if}

                </a>
                  </li>
                {elseif $constructs[i].groupitems[j].CONSTRUCTTYPE == 'SEPARATOR'
                 and $constructs[i].groupitems[j.index_prev].CONSTRUCTTYPE eq 'CONTENT'
                 and $constructs[i].groupitems[j.index_next].CONSTRUCTTYPE neq ''}
                  <li class='separator'></li>
                {/if}
                {/section}
            </ul>
            {/if}
          </li>
        {* show separator only when it makes sense: no two separators in a row or at the end*}
        {elseif $constructs[i].CONSTRUCTTYPE == 'SEPARATOR'
         and $constructs[i.index_prev].CONSTRUCTTYPE eq 'CONTENT'
         and $constructs[i.index_next].CONSTRUCTTYPE neq ''}
          <li class='separator'></li>
        {/if}
        {/section}
        {/if}

        <li class="desaturate normalmenu">
        <div class="xconstructs-horizontal-features pull-right">
          {include file="featuresection.tpl.php" icon_size="1x"}
        </div>
        </li>

        </ul>

    </div>
</nav>
</div>
{/strip}