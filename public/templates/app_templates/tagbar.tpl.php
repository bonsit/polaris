<div id="tagbar"><div id="tagbar_content">

<h2><a href="{$serverroot}/app/{$smarty.get.app}/tags/">Tags</a>:</h2>
{if $tags|@count == 0}<span id="tag_none"> geen</span>{/if}
<ul id="tags"></ul>

<div id="tag_addform">
<form id="tag_form" action="." method="post" onsubmit="TagBar.addTag(formContents(this)); return false;">
<input type="hidden" name="method" value="plr_addtag" />
<input type="hidden" name="app" value="{$smarty.get.app}" />
<input type="hidden" name="const" value="{$smarty.get.const}" />
<input type="hidden" name="form" value="{$currentform->record->METANAME}" />
<input type="hidden" name="record" value="{$smarty.get.rec}" />
<input type="hidden" name="search" value="{$smarty.get.q}" />
<input type="hidden" name="recordcaption" value="{$recordcaption}" />
<input type="text" name="tag" size="15" id="tag_value"/>
<input type="submit" id="tag_addbutton" value="{lang but_add}" onclick="" />
</form>
<a href="javascript:void(0)" id="tag_cancel">{lang but_cancel}</a>
</div>
<a href="javascript:void(0)" id="tag_add">Tag toevoegen</a>

<script type="text/javascript">
    addLoadEvent(TagBar.initialize);
    addLoadEvent(function() {ldelim}
        {section name=i loop=$tags}
        TagBar.addTagElement($('tags'),'{$tags[i].recordid}','{$serverroot}/app/{$smarty.get.app}/tags/{$tags[i].tag}/','{$tags[i].tag}');
        {/section}
    {rdelim});
</script>
</div></div>