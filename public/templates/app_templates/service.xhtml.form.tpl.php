{strip}
{config_load file="globals.conf"}
{include file="header.tpl.php" prototype_original=true}
{include file="error_message.tpl.php"}
{if $currentform}
	{if !$currentform->HasOption('no-page-header')}
	<div id="groupfields">
		<div class="">
			{if $searchstyle == 'horizontal' and $currentform}{$currentform->ShowGroupFields()}{/if}
		</div>
	</div>
	{/if}

<div id="content">

	{*include file="actions.tpl.php"*}
	{$currentform->ShowMaster($currentaction, 'xhtml')}
</div>
{elseif $currentaction eq 'impexp'}
	{include file="import_export.tpl.php"}
{else}
	{include file="actions.tpl.php"}
	{$cons->Show($currentaction, $currentdetailaction, $currentdetailform->record->METANAME, 'xhtml')}
{/if}
{include file="footer.tpl.php"}
{/strip}
