{include file="dashboard.header.tpl.php"}
<div id="plrlogin">
<img src="images/gprdash.png" />
<p id="login_text">{lang enter_your_account}</p>

<form method="post" name="loginform" action="{$serverroot}/dashboard.php" id="loginform" onsubmit="document.forms[0]._hdnEncrypted.value='true';encryptPasswordFields('pass','encryptpass','{$encryptsalt}');">
<input type="hidden" name="_hdnAction" value="login" />
<input type="hidden" name="_hdnEncrypted" value="false" />
<input type="hidden" id="encryptpass" name="_hdnEncryptedPassword" value="" />

<fieldset>
<table><tr>
<td><label for="user">{lang username}:&nbsp;</label></td>
<td><input id="user" name="username" accesskey="n" class="text" type="text" value="" /></td>
</tr><tr>
<td><label for="pass">{lang password}:&nbsp;</label></td>
<td><input id="pass" name="password" accesskey="w" class="text" type="password" value="" /></td>
</tr><tr>
{*<tr>
<td class="label"><input id="rem" name="remember" accesskey="o" class="checkbox" type="checkbox" value="" />&nbsp;</td>
<td><label for="rem" id="rememberlabel">{lang remember_on_this_computer}</label></td> </tr>*}

<td></td><td><input type="submit" value="Login" /></td>
</table>
</fieldset>
</form>

{literal}
<script type="text/javascript">
var init = function() {
    $('user').focus();
    refreshCountDown(240);
}

connect(window,'onload', init)
</script>
{/literal}

</div>
{include file="dashboard.footer.tpl.php"}
