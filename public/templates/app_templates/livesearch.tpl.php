<ul class='LSRes'>
{section name=i loop=$functions}
{if $smarty.section.i.first}<li class="LSRowHeader"><h2>Functies</h2></li>{/if}
<li class="LSRow"><a href="{$callerpage}/app/{$appmeta}/const/{$items[i].metaname}/">{$functions[i].constructname}</a> - {$functions[i].panelhelptext|truncate:20}</li>
{/section}

{section name=i loop=$items}
{if $smarty.section.i.first}<li class="LSRowHeader"><h2>Items</h2></li>{/if}
<li class="LSRow">{$items[i].constructname} - <a href="{$callerpage}/app/{$appmeta}/const/{$items[i].metaname}/?q={$searchvalue}">{$items[i].recordcount} items</a> gevonden</li>
{/section}
{if !$functions and !$items}
Geen resultaat gevonden!<br />
Probeer met een andere zoekwaarde.
{/if}
</ul>