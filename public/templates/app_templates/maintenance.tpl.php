{if $maintenance|@count > 0}
<div id="maintenance">
<h2>Onderhoud / Maintenance</h2>
<p>Op het volgend tijdstip wordt onderhoud gepleegd aan Polaris. U kunt dan {$maintenance[0].duration} minuten geen gebruikmaken van uw Polaris applicaties.</p>
<ul id="maintenancelist">
	{section name=i loop=$maintenance}
	<li>{$maintenance[i].startdate|date_format:'%A %d %B %Y'} &ndash; <strong>{$maintenance[i].starttime} uur</strong>
	<br />Duur: <strong>{$maintenance[i].duration/60} minuten</strong>
	<br />Reden: {$maintenance[i].remark}
	</li>
	{/section}
</ul>
</div>
{/if}
