<div class="row">
{section name=i loop=$panes}
    <div class="col-lg-{$panewidth}">
        <div class="iboxx taskview-pane" data-status="{$panes[i][0]}">
            <div class="taskview-content">
                <div class="taskview-header-bar">
                    <div class="taskview-header status-{$panes[i][0]}">
                        <b><span>{$panes[i][1]}</span> <button class="taskview-add btn btn-default btn-xs btn-outline pull-right">{icon name="md-add"}</button></b>
                    </div>
                </div>

                <ul class="sortable-list agile-list taskview" id="pane-{$panes[i][0]}" data-statusvalue="{$panes[i][0]}" data-statusfield="{$statusfield}" data-sortfield="{$sortfield}">
                {section name=j loop=$items}
                {if $items[j]['STATUS'] == $panes[i][0]}
                    <li class="status-{$items[j]['STATUS']} priority-{$items[j][{$priorityfield}]} taskview-item" data-value="{$panes[i][0]}" id="{$items[j]['PLR__RECORDID']}">
                        <button type="button" data-toggle="dropdown" class="dropdown-toggle taskview-item-more float-right btn btn-sm btn-outline btn-white">...</button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="/app/facility/">Facility beheer</a>
                        </div>
                        <span>{$items[j]['TAAK_OMSCHRIJVING']}</span>
                        <div class="agile-detail">
                            <span class="badge priority-{$items[j][{$priorityfield}]}">
                                <i class="fa fa-clock-o"></i> {$items[j]['PRIORITY_NAME']}
                            </span>
                        </div>
                    </li>
                {/if}
                {/section}
                </ul>
            </div>
        </div>
    </div>
{/section}
</div>
