{strip}
{config_load file="globals.conf"}
{include file="header.tpl.php"}

<div class="container">

    <nav class="navbar navbar-static-topx" role="navigation" style="margin-bottom: 0">
        <ul class="nav navbar-top-links navbar-right">
            {include file="featuresection.tpl.php"}
        </ul>
    </nav>

    <div class="client-logo text-center"><span>
        <h2 class="client-title">&mdash; {$client->NAME} &mdash; </h2>
    </span></div>

    {if $maintenance|@count > 0}
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-warning">
                <div class="panel-heading">
                    <i class="fa fa-warning"></i> {lang maintenance}
                </div>
                <div class="panel-body">
                {include file="maintenance.tpl.php"}
                </div>
            </div>
        </div>
    </div>
    {/if}

    <div class="appslist">
        <div class="row">
            <div class="col-sm-12 col-md-12 text-center">
                <h1 class="welcome-to">{lang welcome_to} {lang polaris_main}</h1>
            </div>
        </div>

        {include file="_appsmenu.tpl.php"}
    </div>
</div>

{include file="footer.tpl.php"}
{/strip}