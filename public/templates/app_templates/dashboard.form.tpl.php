{config_load file="globals.conf"}
{include file="header.tpl.php" prototype_original=true}
{include file="error_message.tpl.php"}
{if $module}
	{$module->Show()}
{elseif $currentaction eq 'impexp'}
	{include file="import_export.tpl.php"}
{else}
	{include file="actions.tpl.php"}
	{$cons->Show()}
{/if}
{include file="footer.tpl.php"}