{section name=i loop=$webparams}
{if $smarty.section.i.first}
<div class="row">
<span class="left">{lang click_and_edit_item}</span><span class="right">
{php}
  global $currentconstruct, $BASEURL;
  if (($currentconstruct->masterform->GetPermissionType() & INSERT) == INSERT ) {
    {/php}<a href='{$callerpage}?const={$currentconstructid}&action=insert'>{php}
    echo lang_get('add_item_to_list', $currentconstruct->masterform->ItemName());
    echo "</a>";
  }
{/php}
&nbsp;</span>
</div>
<table class="data striped" cellspacing="0" cellpadding="0" border="0">
<thead><tr><th width="1%">{lang webparam_name}</th><th>{lang value}</th><th>{lang languages}</th><th class="center">{lang delete}</th></tr></thead>
{/if}

<tr>
<td>{$webparams[i].paramname}:</td>
{* <td class="action"><a href="?const={$currentconstructid}&action={#action_edit#}&rec={$webparams[i].recordid}">[{lang edit}]</a> *}
<td><input type="hidden" name="OLDVALUE_{$webparams[i].recordid}" value="{$webparams[i].paramvalue}" />
<input type="hidden" name="_hdnRecordID[]" value="{$webparams[i].recordid}" />
<input type="text" name="PARAMVALUE:{$webparams[i].recordid}" size="40" value="{$webparams[i].paramvalue}" /></td>
<td>{$webparams[i].language}</td>
<td class="center"><input name="rec:{$webparams[i].recordid}" type="checkbox"></td>
</tr>

{if $smarty.section.i.last}
<tfoot>
<td></td><td><input type="submit" xclass="protect" value="Opslaan" onclick="getElem('dataform')._hdnAction.value='save'" /></td><td></td>
<td class="center"><input type="submit" class="protect" value="Verwijder" onclick="return CheckDel('dataform');" /></td></tfoot>
</table>

{/if}
{sectionelse}
<p>{lang no_webpage_exists}</p>
<ul class="startitems">
{section name=j loop=$actions}
<li class="normalmenu"><a href="{$actions[j].location}"><img src="{#default_icon_url#}{$actions[j].icon}" height="{#large_icon_height#}" width="{#large_icon_width#}" alt="" class="im" />{$actions[j].caption}</a></li>
{/section}
</ul>
{/section}
