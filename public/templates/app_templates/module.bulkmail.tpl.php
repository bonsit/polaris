<input type="hidden" name="_hdnProcessedByModule" value="true" />

{if $processed}
<h1>Email status</h1>
<p>De email-berichten zijn correct verstuurd.</p>
{else}
<form action="" method="post">
<ol>
<li><h1>Email instellingen</h1></li>
<p class="formlayout">
<label for="from"><strong>"Beantwoord aan":</strong></label>
<input id="from" name="fromaddress" accesskey="v" class="text" type="text" size="60" value="{$fromaddress|escape}" /><br />

<small>Dit is het emailadres waarnaar antwoorden worden gestuurd</small>

<p class="formlayout">
<label for="sub" ><strong>Onderwerp:</strong></label>
<input id="sub" type="text" name="subject" accesskey="o" class="text" size="60" value="{$subject}" /></p>

<li><h1>Selecteer de geadresseerde</h1></li>
<p>Kruis de emailadressen aan waarnaar u de email wilt sturen.</p>
{*<textarea name="emailaddresses" cols="80" class="" rows="20">*}
<div id="emailselectbox">
{section name=i loop=$addresses}
{if $addresses[i][$addresscolumnname]|strip ne ''}
<input type="checkbox" name="emailaddresses[]" value="{$addresses[i][$addresscolumnname]}" checked="checked" /> {$addresses[i][$addresscolumnname]}<br />
{/if}
{/section}
</div>
<p>Totaal aantal emailadressen: {$addresses|@count}  &nbsp;<input type="button" onclick="var e = $('emailselectbox').getElementsByTagName('INPUT'); for (i=0;i<=e.length;i++ ) {ldelim}e[i].checked=true{rdelim};" value="Selecteer alles">
&nbsp;<input type="button" onclick="var e = $('emailselectbox').getElementsByTagName('INPUT'); for (i=0;i<=e.length;i++ ) {ldelim}e[i].checked=false{rdelim};" value="Selecteer niets">
</p>
<br />
<li><h1>Typ uw email tekst</h1></li>
<p>Plak of typ hier de tekst die u naar de geadresseerde wilt sturen.</p>
<textarea name="bodytext" cols="80" class="" rows="20">
{$bodytext}
</textarea>
<br /><input id="keep" type="checkbox" name="keeptext" value="yes" checked="checked" /><label for="keep"> Bewaar wijzigingen in deze tekst voor de volgende keer.</label>
<hr class="cleaner" />

<br /><input type="submit" name="submit" value="Verstuur emails nu!" />
</form>
{/if}