{strip}
{if $isgroup == true}
    <div class="groupname">
        {$menuitem.CONSTRUCTNAME|get_resource:$lang|truncate:28:"...":true:true}
        {if $menuitem.PARAMETERS == 'nieuw=true'} <label class="label label-success">nieuw</label>{/if}
        <span class="toggler"></span>
    </div>
{else}

    {if ($currentconstructid == $menuitem.CONSTRUCTID)
    or ($menuitem.groupitems and $currentconstructid|in_array:$menuitem.groupitems_id_array
    )}
        {assign var=menutype value='active '}
    {else}
        {assign var=menutype value='normalmenu '}
    {/if}

    <div class='menuitem {$menutype} {if $menuitem.groupitems}dropdown{/if}'>

        <a class="feature dropdown-toggle" {if $menuitem.groupitems}data-toggle="dropdown"{/if} tabindex="-1" href="{$app->applink}const/{$menuitem.METANAME}/{if $menuitem.OPTIONS|strpos:'show-insert-form' !== false}insert/{/if}" {if $menuitem.ACCESSKEY !='' } accesskey="{$menuitem.ACCESSKEY}" {/if}>
            {icon name=$menuitem.IMAGEURL}
            <span>
                {$menuitem.CONSTRUCTNAME|get_resource:$lang}
                {if $menuitem.groupitems}<b>...</b>{/if}
            </span>
            {if $menuitem.PARAMETERS == 'nieuw=true'} <label class="label label-success">nieuw</label>{/if}
            {if $menuitem.RECORDCOUNTER > 0}
                <span class="badge badge-primary pull-right">{$menuitem.RECORDCOUNTER}</span>
            {/if}
        </a>

        {if $menuitem.groupitems}
            <ul class="menuitem-submenu dropdown-menu dropdown-menu-right" role="menu">
            {section name=k loop=$menuitem.groupitems}
                <li><a tabindex="-1" href="{$app->applink}const/{$menuitem.groupitems[k].METANAME}/">{$menuitem.groupitems[k].CONSTRUCTNAME|get_resource:$lang}</a></li>
            {/section}
            </ul>
        {/if}

    </div>

{/if}
{/strip}