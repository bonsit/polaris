<div id="page-wrapperx" class="page-wrapper gray-bg xdashbard-1">
  <div class="row wrapper border-bottom white-bg page-heading">
    <div class="service-section pull-right">
    {include file="featuresection.tpl.php"}
    </div>
    {include file="breadcrumbs.tpl.php"}
  </div>

  <div class="wrapper wrapper-content">
    <div class="row">

    {if !$showmaincontentdiv}
    <div id="appsmenu" class="appsmenu arrow_box" style="display:none">
    {include file="_appsmenusmall.tpl.php"}
    </div>
    {/if}
