{strip}
<nav id="sidebar" class="navbar-default navbar-static-side" role="navigation">

    <div class="sidebar-collapse">
        <div class="sidebar-toggler navbar-minimalize"></div>
    {if isset($smarty.get.sitesettings)}
    <div class="dropdown application-group">
            <div id="applicationtitle" class="applicationtitle">
                Organisatie Instellingen
            </div>
        </div>
        <div><h4><a href="#"><-- Terug naar applicatie</a></h4></div>
    {elseif ($client->NAME neq '')}
            <a class="dropdown-toggle" style="" type="button" id="appslist" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <div class="dropdown application-group">
                    <div class="applicationicon">
                        <img src="/assets/images/maxia icoon_white.png" class="polaris-appslist-icon" />
                    </div>
                    <div id="applicationtitle" class="applicationtitle">
                        {$app->record->APPLICATIONNAME|get_resource}
                    </div>
                </div>
            </a>
            {include file="_appsmenusmall.tpl.php"}
    {/if}

    <div id="constructs" class="construct-group nav2">

    {if $constructs and $constructs|@count > 0}
        {section name=i loop=$constructs}
            {if $constructs[i].CONSTRUCTTYPE == 'CONTENT' or $constructs[i].CONSTRUCTTYPE == 'GROUP'}
                {if ($currentconstructid == $constructs[i].CONSTRUCTID)}
                    {assign var=menutype value='active '}
                {else}
                    {assign var=menutype value='normalmenu '}
                {/if}

                {if $constructs[i].CONSTRUCTTYPE == 'GROUP'}

                    <div class="menugroup" data-groupid="{$constructs[i].PLR__RECORDID}" data-initialgroupstate="{if $constructs[i].INITIALGROUPSTATE == 'C'}closed{else}opened{/if}">

                        {include file="constructs_menuitem.tpl.php" isgroup=true menuitem=$constructs[i] nocache}

                {else}
                    {include file="constructs_menuitem.tpl.php" menuitem=$constructs[i] nocache}
                {/if}

                {if $constructs[i].CONSTRUCTTYPE == 'GROUP'}

                    {section name=j loop=$constructs[i].groupitems}

                        {if $constructs[i].groupitems[j].CONSTRUCTTYPE == 'CONTENT'}

                            {include file="constructs_menuitem.tpl.php" menuitem=$constructs[i].groupitems[j] nocache}

                        {elseif $constructs[i].groupitems[j].CONSTRUCTTYPE == 'GROUP'}

                            {include file="constructs_menuitem.tpl.php" menuitem=$constructs[i].groupitems[j] nocache}

                        {elseif $constructs[i].groupitems[j].CONSTRUCTTYPE == 'SEPARATOR'
                            and $constructs[i].groupitems[j.index_prev].CONSTRUCTTYPE eq 'CONTENT'
                            and $constructs[i].groupitems[j.index_next].CONSTRUCTTYPE neq ''}
                            <div class='separator'></div>
                        {/if}
                    {/section}

                    {if $constructs[i].CONSTRUCTTYPE == 'GROUP'}
                        </div>
                    {/if}
                {/if}

            {* show separator only when it makes sense: no two separators in a row or at the end*}
            {elseif $constructs[i].CONSTRUCTTYPE == 'SEPARATOR'
                and $constructs[i.index_prev].CONSTRUCTTYPE eq 'CONTENT'
                and $constructs[i.index_next].CONSTRUCTTYPE neq ''}
                <div class='separator'></div>
            {/if}
        {/section}
    {/if}
    </div>
</nav>
{/strip}