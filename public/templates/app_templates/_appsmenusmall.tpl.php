<div class="dropdown-menu apps-dropdown" aria-labelledby="appslist">
    <li class="dropdown-divider">&nbsp;</li>
    <li class="dropdown-header"><h3>{lang polaris} applicaties</h3></li>
    {section name=i loop=$apps}
    <li>
        <a class="dropdown-item" href="{$apps[i]->applink}">{icon name={$apps[i]->record->IMAGE}} &nbsp; {$apps[i]->record->APPLICATIONNAME}
            {if $apps[i]->record->PARAMETERS|strpos:"nieuw=true" !== false}
            <label class="label label-success">nieuw</label>
            {/if}
        </a>
    </li>
    {/section}
</div>
