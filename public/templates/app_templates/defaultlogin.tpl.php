{if $hasbrandglobals}{config_load file="brandglobals.conf"}{/if}
{include file="defaultheader.tpl.php" title="Content Management Systeem" background="gray-bg"}

<div class="middle-box text-center loginscreen xanimated fadeInDownBig">
    <div>
        <div>
            <h1 class="logo-name">{$config.polarisname}</h1>

            {if $loginerror}
            <span style="color:#F1ABAB" class="loginerror">{$loginerror}</span>
            {*<a href="/polaris/?method=dropsessions">Andere gebruiker geforceerd uitloggen</a>*}
            {/if}

            <form id="loginform" method="post" name="loginform" action="{$serverroot}">
                <input type="hidden" name="_hdnAction" value="login" />
                <input type="hidden" name="_hdnPlrSes" value="{$polarissession}" />

                <h3>{lang welcome_to} {lang polaris}</h3>
                <p id="login_text">{if !$loginerror}{lang login_text}{/if}</p>
                <div class="form-group">
                    <input type="text" class="form-control" name="username" id="fldUserName" placeholder="{lang username}" required="" value="{$username}">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" name="password" placeholder="{lang password}" required="">
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">Login</button>

                <a href="#" id="fldForgotPassword"><small>{lang forgot_password}</small></a>
                <p class="text-muted text-center"><small>{lang no_account}</small></p>
                <a class="btn btn-sm btn-white btn-block" href="/createaccount/">{lang create_account}</a>

                {if $multipleinstances}
                <br /><br /><br />
                <label for="plrinstance">{lang polarisinstance}:&nbsp;</label> {$instancesselect}
                {/if}

            </form>
        </div>
    </div>
</div>

{include file="defaultfooter.tpl.php"}
