{$displaytimers}
{if $isclientadmin and $app}{$app->polaris()->GetAdvancedForm()}{/if}
{$shareform}
{literal}
<script type="text/javascript">
    // To prevent flickering of the screen, this needs to be one of the first scripts to run (so before main.js)

    // Loop through all menu groups and set the state
    var menustates = JSON.parse(localStorage.getItem("menustates")) || {};
    $(".menugroup").each(function () {
      const group = $(this);
      const menuitems = group.find(".menuitem,.separator");
      const groupid = group.data("groupid");
      const initialGroupState = group.data("initialgroupstate");
      const isClosed = menustates[groupid] === "closed" || (menustates[groupid] === undefined && initialGroupState === "closed");
      group.toggleClass("closed", isClosed);
      menuitems.toggle(!isClosed);
    });

    // Store the position of the mainmenu scrollbar
    if (sessionStorage.getItem("tempScrollTop") && document.getElementById('constructs'))
        document.getElementById('constructs').scrollTo({top: sessionStorage.getItem("tempScrollTop")})
</script>
{/literal}

<script src="{$serverpath}/assets/dist/main.js?v={#ReleaseVersion#}"></script>

{if $app}{$app->polaris()->GetIncludedJavascripts()}{/if}
{if ($currentform and $currentform->HasOption('sortable-table'))
or ($currentdetailform and $currentdetailform->HasOption('sortable-table'))}
<script src="{$serverpath}/assets/dist/Sortable.min.js"></script>
{/if}

{if ($showformscripts == true) and ($lang|lower != 'nl') and ($lang|lower != 'en')}
<script type="text/javascript" src="{$serverpath}/javascript/jquery-validate/localization/messages_{$lang|lower}.js"></script>
{/if}

{if $autoclosewindow}
<script type="text/javascript">
    Polaris.Form.closePopup();
</script>
{/if}

{if $resetpolarissession == true}
<script type="text/javascript">
    sessionStorage.plrses = '{$polarissession}';
</script>
{/if}

{include file="ajax_feedback.tpl.php"}
<div id="tooltipcontent" class="shadowbox" style="display:none">N.v.t.</div>

{if $currentform}{$currentform->ShowSearchSaveModal()}{/if}

<div class="jqmWindow shadowbox" id="ajaxdialog" style="display:none;">Even geduld a.u.b...</div>
<div id="blockUImessage" style="display:none;cursor:default">
    <p class="prettymessage">De melding</p>
    <textarea style="width:400px;height:200px;font-size:8pt;display:none;" readonly="readonly" class="detailed_message">..uitgebreide melding..</textarea>
    <div class="buttons" style="font-size:0.8em;">
        <button type="button" class="btn btn-success savebutton">{lang but_close}</button>
        <button type="button" class="btn btn-warning" onclick="$('#blockUImessage .detailed_message').toggle();">Details</button>
        {*<button type="button" class="nobutton">{lang no}</button>*}
    </div>
</div>
<div id="LSResult" style="display:none;">
    <div id="LSShadow"></div>
</div>
<span tabindex="0" onfocus="javascript:document.getElementById('prevent-outside-tab').focus();"></span>
</body>
</html>