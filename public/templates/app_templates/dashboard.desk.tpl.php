{include file="dashboard.header.tpl.php"}
<script type="text/javascript">
var win = null;
{section name=i loop=$windows}
win = Dashboard.addWindow('{$windows[i].ID}',
    {ldelim} title: "{$windows[i].TITLE}"
    , top:{$windows[i].TOP|default:200}, left:{$windows[i].LEFT|default:100}, width:{$windows[i].WIDTH|default:400}, height:{$windows[i].HEIGHT|default:300} 
    {if $windows[i].TYPE ne 'RSS'}, url:'{$windows[i].URL}'{/if}
    , content:'{$windows[i].RSS}'
    , windowmode:'{$windows[i].WINDOWMODE}' {rdelim}
);
if (win)
    win.show();
{/section}
</script>

<div id="addcontentform">
    <div id="addcontentbody">
        <div class="contentblock" id="dash_polaris"><div class="body">
            <h2>Applicatie functies</h2>
            <select id="PLRlink">
                {section name=i loop=$plruserconstructs}
                {if $plruserconstructs[i].applicationname ne $plruserconstructs[i.index_prev].applicationname}
                <optgroup label="{$plruserconstructs[i].applicationname}">
                {/if}
                <option value="/app/{$plruserconstructs[i].appmetaname}/const/{$plruserconstructs[i].constmetaname}/">{$plruserconstructs[i].constructname}</option>
                {/section}
            </select><br />
            <input type="button" id="PLR_addcontent" value="Toevoegen"/>
        </div></div>

        <div class="contentblock" id="dash_webpage"><div class="body">
            <h2>Webpaginas</h2>
            <input type="text" id="URLlink" value="&lt;typ een URL&gt;" size="20" /><br />
            <input type="button" id="URL_addcontent" value="Toevoegen"/>
        </div></div>

        <div class="contentblock" id="dash_feed"><div class="body">
            <h2>Nieuws feeds</h2>
            <input type="text" id="RSSlink" value="&lt;typ een RSS link&gt;" size="20" /><br />
            <input type="button" id="RSS_addcontent" value="Toevoegen"/>
        </div></div>

        <div class="actionblock" id="dash_actions"><div class="body">
            <a href="?method=logout">Logout</a>
        </div></div>
	</div>
</div>
<div id="addcontenttoggle" class="button">&nbsp;</div>
{include file="dashboard.footer.tpl.php"}