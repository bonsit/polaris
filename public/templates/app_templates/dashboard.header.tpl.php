<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="nl" lang="nl">
<head>
<title>{$title|default:"Dashboard"}</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" type="text/css" media='all' href="{$serverroot}/css/plrdashboard.css" />
<link rel="stylesheet" type="text/css" href="{$serverroot}/css/javawin/themes/default.css"></link>
<link rel="stylesheet" type="text/css" href="{$serverroot}/css/javawin/themes/mac_os_x.css"></link>
<link rel="stylesheet" type="text/css" href="{$serverroot}/css/javawin/themes/alphacube.css"></link>

<script type="">
var _loadevents_ = [];
var _serverroot = "{php}global $_GVARS; echo $_GVARS['serverroot']{/php}";
</script>

</head>
<body>