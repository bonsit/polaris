{config_load file="globals.conf"}{include file="header.tpl.php" title="Choose image"}
<script type="text/javascript" language="javascript">
var guifield = '{$guifield}';
{literal}
<!--

_loadevents_[_loadevents_.length] = function() {
    $("#filelist a").click(selectPic);
    $("#select").click( function(){ pick('placeholder') } );
    $('.cancel').click( function(){ window.close() });
    var h = ($(window).height() - $('#filelist').height()) - 50 + 'px';
    $("#filelist").css({height:h});
}

function selectPic (e) {
    $('#filelist').find('a.current').removeClass();
    
 	var elem = $(e.target);
 	elem.addClass('current');
    $('#placeholder').attr('src','{/literal}{$serverroot}/thumb/400/400{$smarty.get.dir}{literal}/'+elem.attr('imagename'));
    $('#placeholder').attr('imagename', elem.attr('imagename'));
    /* show the original pic if the thumbnail is not available */
//    $('#placeholder').attr('onError', "$('#placeholder').src = '{/literal}{$smarty.get.url}{literal}/"+whichpic.getAttribute('imagename')+"'");
    if (elem.attr('imagename')) {
      $('#desc').val( elem.attr('imagename') );
    } else {
      $('#desc').val( e.target.childNodes[0].nodeValue );
    }
    return false;
}

function pick(image) {
  if (window.opener && !window.opener.closed) {
      var destfield = window.opener.document.getElementById(guifield);
      var sourcefield = $("#"+image);
      if (destfield) {
          destfield.value = sourcefield.attr('imagename');
      }
  }
  window.close();
}

// -->
</script>
{/literal}
<br />
<div id="pagetabcontainer">
    <ul id="pagetabs">
	    <li><a href="#contentblock1">{lang choose_image}</a></li>
	    <li><a href="#contentblock2">{lang upload_images}</a></li>
	</ul>
</div>

<div id="contentblock1" class="pagecontents" style="height:auto;display:none;position:relative;">
    <a name="target1"></a>
    <div id="preview">
        <p>{lang selected_image}: <br /><input type="text" size="40" readonly="readonly" class="readonly" id="desc" value="{$currentfilename|default:"None. Click on an image to view it."}" /></p>
        {lang image_preview}:<br />
        {if $currentfilename}
        <img id="placeholder" src="{$serverroot}/thumb/400/400/{$imagepath}{$currentfilename}" imagename="{$currentfilename}" alt="Voorbeeld"  />
        {else}
        <img id="placeholder" src="{$serverroot}/images/blank.gif" alt="Voorbeeld"  />
        {/if}
    </div>

    <div id="imagepick" >
        <h3>{lang pick_image}</h3>
		{if $uploaded eq true}
		<p style="clear:both">Aantal uploads: {$uploadcount}</p>
		{/if}
        <div id="filelist">
        {section name=i loop=$files}
        <a {if $currentfilename eq $files[i]}class="current"{/if} href="{$imageurl}{$files[i]}">
        <img src="{$serverroot}/thumb/100/100/{$imagepath}/{$files[i]}" width="100px" imagename="{$files[i]}"/>
        </a>
        {sectionelse}
        {lang no_images} 
        {/section} 
        </div>
        <button id="select">{lang but_select}</button> <button class="cancel">{lang but_cancel}</button>
    </div id="imagepick">
</div>
	
<div id="contentblock2" class="pagecontents" style="display:none">
    <a name="target2"></a>
    <div id="upload">
        <p>U kunt afbeeldingen uploaden in het formaat PNG, GIF en JPG.</p>
        <form action="{$smarty.server.PHP_SELF}?{$smarty.server.QUERY_STRING}" method="post" enctype="multipart/form-data">
            <input type="hidden" name="MAX_FILE_SIZE" value="5000000" />
            <input type="file" name="imagefile1" size="45"><br />
            {php}
            $maxinputs = $_GET['uploadcount'];
            if (!isset($maxinputs)) $maxinputs = 1;
            for ($i=1;$i<$maxinputs;$i++) {
              echo "<input type=\"file\" name=\"imagefile${i}\" size=\"45\"><br />";
            }
            {/php}    
            <input type="submit" name="upload" value="{lang but_upload}" />
            <button class="cancel">{lang but_cancel}</button>
        </form>
    </div>
</div>
{include file="footer.tpl.php"}
