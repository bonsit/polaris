{strip}
<nav id="sidebar" class="navbar-default navbar-static-side" role="navigation">
<div class="sidebar-collapse">
<ul id="constructs" class="nav">

  {if ($client->NAME neq '')}
  <li class="nav-header">
      <div class="xdropdown profile-element">

          <h2 id="applicationtitle" class="applicationtitle" style="position:relative">
          <a href="{$serverroot}">{$app->record->APPLICATIONNAME|get_resource}</a>
            {*<a data-toggle="dropdown" class="dropdown-toggle" href="{$serverroot}">
              {$app->record->APPLICATIONNAME|get_resource} <b class="caret"></b>
              {include file="_appsmenusmall.tpl.php"}
            </a>*}
          </h2>
          <div class="powered-by pull-right">Powered by Maxia</div>
      </div>
  </li>
  {/if}

{if $constructs and $constructs|@count > 0}
{section name=i loop=$constructs}
{if $constructs[i].CONSTRUCTTYPE == 'CONTENT' or $constructs[i].CONSTRUCTTYPE == 'GROUP'}
  {if ($currentconstructid == $constructs[i].CONSTRUCTID)}
	{assign var=menutype value='active '}
  {elseif $constructs[i].METANAME == 'website'}
	{assign var=menutype value='special '}
  {else}
	{assign var=menutype value='normalmenu '}
  {/if}
  <li class='xdesaturate {$menutype}
  {if ($constructs[i].ACTIVE == true) or ($constructs[i].CONSTRUCTTYPE == 'GROUP' and $constructs[i].INITIALGROUPSTATE == 'O')} active{else} {/if}
  '>
  {if $constructs[i].RECORDCOUNTER !== false}
  <span class="badge">{$constructs[i].RECORDCOUNTER}</span>
  {/if}
  {if $constructs[i].CONSTRUCTTYPE == 'GROUP'}
       <a tabindex="-1" href="javascript:void(0)" class="xsubmenu" {if $navigationstyle eq 'horizontal'}style="background-image:url({$serverpath}{$constructs[i].IMAGEURL})"{/if}>
  {else}
	   <a tabindex="-1" href="{$app->applink}const/{$constructs[i].METANAME}/"
    	{if $constructs[i].ACCESSKEY != ''}accesskey="{$constructs[i].ACCESSKEY}"{/if}>
  {/if}
      {if $constructs[i].IMAGEURL neq '' and $constructs[i].IMAGEURL neq '0'}
        {if $constructs[i].IMAGEURL|substr:0:3 eq 'fa-'}
        <i class="fa-regular {$constructs[i].IMAGEURL} fa-lg fa-fw"></i>
        {elseif $constructs[i].IMAGEURL|substr:0:9 eq 'glyphicon'}
        <i class="glyphicons {$constructs[i].IMAGEURL}" aria-hidden="true"></i>
        {elseif $constructs[i].IMAGEURL|substr:0:5 eq 'icon-' or $constructs[i].IMAGEURL|substr:0:6 eq 'ficon-'}
        <i class="icon {$constructs[i].IMAGEURL}" aria-hidden="true"></i>&nbsp;
        {else}
          {if $navigationstyle eq 'vertical'}
          <img src="{$serverpath}{$constructs[i].IMAGEURL}" class="im" alt="" height="{#small_icon_height#}" width="{#small_icon_width#}" />&nbsp;
          {/if}
        {/if}
      {/if}
  <span class="nav-label">
  {$constructs[i].CONSTRUCTNAME|get_resource:$lang|truncate:28:"...":true:true}
  </span>
  {if $constructs[i].CONSTRUCTTYPE == 'GROUP'}
  <span class="fa arrow"></span>
  {/if}
</a>

  {if $constructs[i].CONSTRUCTTYPE == 'GROUP'}
			<ul class="nav nav-second-level {if in_array($currentconstructid, array_column($constructs[i].groupitems, 'CONSTRUCTID'))}{else}collapse{/if}">
			    {section name=j loop=$constructs[i].groupitems}
                {if $constructs[i].groupitems[j].CONSTRUCTTYPE == 'CONTENT' or $constructs[i].groupitems[j].CONSTRUCTTYPE == 'GROUP'}
                  {if ($currentconstructid == $constructs[i].groupitems[j].CONSTRUCTID)}
                    {assign var=menutype value='active '}
                  {elseif $constructs[i].groupitems[j].METANAME == 'website'}
                    {assign var=menutype value='special '}
                  {else}
                    {assign var=menutype value='normalmenu '}
                  {/if}

                  <li class='{$menutype}'>
                  <a tabindex="-1" href="{$app->applink}const/{$constructs[i].groupitems[j].METANAME}/{if $constructs[i].groupitems[j].OPTIONS|strpos:'show-insert-form' !== false}insert/{/if}"

                    {if $constructs[i].groupitems[j].ACCESSKEY != ''}
                        accesskey="{$constructs[i].groupitems[j].ACCESSKEY}"
                    {/if}
                    >

                  {*if $constructs[i].groupitems[j].IMAGEURL neq '' and $constructs[i].IMAGEURL neq '0'}
                    {if $constructs[i].groupitems[j].IMAGEURL|substr:0:3 eq 'fa-'}
                    <i class="fa {$constructs[i].groupitems[j].IMAGEURL} fa-lg fa-fw"></i>&nbsp;
                    {elseif $constructs[i].groupitems[j].IMAGEURL|substr:0:9 eq 'glyphicon'}
                    <i class="glyphicons {$constructs[i].groupitems[j].IMAGEURL}" aria-hidden="true"></i>&nbsp;
                    {elseif $constructs[i].groupitems[j].IMAGEURL|substr:0:5 eq 'icon-' or $constructs[i].groupitems[j].IMAGEURL|substr:0:6 eq 'ficon-'}
                    <i class="icon {$constructs[i].groupitems[j].IMAGEURL}" aria-hidden="true"></i>&nbsp;
                    {else}
                    <img src="{$serverpath}{$constructs[i].groupitems[j].IMAGEURL}" class="im" alt="" />&nbsp;
                    {/if}
                  {/if*}

                  {$constructs[i].groupitems[j].CONSTRUCTNAME|get_resource:$lang}
                  {if $constructs[i].groupitems[j].RECORDCOUNTER !== false}
                    <span class="badge badge-primary pull-right">{$constructs[i].groupitems[j].RECORDCOUNTER}</span>
                  {/if}

                </a>
                  </li>
                {elseif $constructs[i].groupitems[j].CONSTRUCTTYPE == 'SEPARATOR'
                 and $constructs[i].groupitems[j.index_prev].CONSTRUCTTYPE eq 'CONTENT'
                 and $constructs[i].groupitems[j.index_next].CONSTRUCTTYPE neq ''}
                  <li class='separator'></li>
                {/if}
				{/section}
			</ul>
	{/if}
	</li>
{* show separator only when it makes sense: no two separators in a row or at the end*}
{elseif $constructs[i].CONSTRUCTTYPE == 'SEPARATOR'
 and $constructs[i.index_prev].CONSTRUCTTYPE eq 'CONTENT'
 and $constructs[i.index_next].CONSTRUCTTYPE neq ''}
  <li class='separator'></li>
{/if}
{/section}
{/if}
</ul>

</div>
</nav>
{/strip}
