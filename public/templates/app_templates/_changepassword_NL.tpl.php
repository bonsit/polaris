<h2>Wat is een sterk wachtwoord?</h2>
<p style="font-size: 1.2em;">
    Een sterk wachtwoord is minimaal 8 tekens lang en bevat een combinatie van cijfers, letters en eventueel speciale tekens.
</p>

<h3 style="color: red;">SLECHTE wachtwoorden</h3>
<ul style="color: red;">
    <li>12345678</li>
    <li>wachtwoord</li>
    <li>cc1234</li>
    <li>tafel</li>
</ul>

<h3 style="color: green;">GOEDE wachtwoorden <span style="font-weight: normal;">(onderstaande niet gebruiken!)</span></h3>
<ul style="color: green;">
    <li>$Uperm@n-2003</li>
    <li>ZonnigeBezemVliegtuigPaard-8181</li>
    <li>NGIAM!1982 <span style="color: black;">=> (Gebruik een afkorting: <u><b>N</b></u>iet <u><b>G</b></u>eschoten <u><b>I</b></u>s <u><b>A</b></u>ltijd <u><b>M</b></u>is)</span></li>
</ul>

<h3 style="color: green;">BESTE wachtwoorden:</h3>
<p style="color: green;">
    Dat zijn wachtwoorden die je niet kunt onthouden. Gebruik daarom een wachtwoordmanager!
</p>
