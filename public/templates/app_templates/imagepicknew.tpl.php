{config_load file="globals.conf"}
{include file="header.tpl.php" title="Choose image"}
<script type="text/javascript" src="javascript/domtab.js"></script>
<script type="text/javascript" language="javascript">
var guifield = '{$guifield}';
{literal}
<!--
// Fired when the user press the OK button
function ok(imageSource) {
	window.returnValue = imageSource.alt;
	window.close() ;
}

function showPic (whichpic) {
 	a = document.getElementsByClassNameExt($('filelist'),'current','a');
	for (i=0;i<a.length;i++) 
		a[i].className = '';
 	whichpic.className = 'current';
  $('placeholder').src = '{/literal}{$smarty.get.url}{literal}/_THUMB_'+whichpic.getAttribute('imagename');
  $('placeholder').setAttribute('imagename', whichpic.getAttribute('imagename'));
  /* show the original pic if the thumbnail is not available */
  $('placeholder').setAttribute('onError', "$('placeholder').src = '{/literal}{$smarty.get.url}{literal}/"+whichpic.getAttribute('imagename')+"'");
  if (whichpic.title) {
   $('desc').value = whichpic.title;
  } else {
   $('desc').value = whichpic.childNodes[0].nodeValue;
  }
  return false;
}

function pick(image) {
  if (window.opener && !window.opener.closed) {
		var destfield = window.opener.document.getElementById(guifield);
		var sourcefield = $(image);
		if (destfield) {
			destfield.value = sourcefield.getAttribute('imagename');
		}
	}
  window.close();
}

// -->
</script>
{/literal}
	<br />
	<div id="pagetabcontainer"><ul id="pagetabs">
	<li><a href="#contentblock1">{lang choose_image}</a></li>
	<li><a href="#contentblock2">{lang upload_images}</a></li>
	</ul></div>

	<div id="contentblock1" class="pagecontents" style="height:auto;display:none">
		<a name="target1"></a>
    <div id="imagepick" style="clear:both;height:auto;">
    <h3>{lang pick_image}</h3>
		{if $uploaded eq true}
		<p style="clear:both">Aantal uploads: {$uploadcount}</p>
		{/if}
    <div id="filelist">
    {section name=i loop=$files}
    <a {if $currentfile eq $files[i]}class="current"{/if} onmouseover="return showPic(this);" href="{$imageurl}{$files[i]}" {if $currentfile eq $files[i]}class="selected"{/if} imagename="{$files[i]}">{$files[i]}</a>
    <br />
    {sectionelse}
    {lang no_images} 
    {/section} 
    </div>
    <button onclick="javascript:pick('placeholder')">{lang but_select}</button> <button onclick="javascript:window.close();">{lang but_cancel}</button>
    </div id="imagepick">
    
    <div id="preview">
    {lang image_preview}:<br />
    <img id="placeholder" src="{$currentfile}" alt=""  /><br />
    <p>{lang selected_image}: <br /><input type="text" size="40" readonly="readonly" id="desc" value="{$currentfile|default:"None. Click on an image to view it."}" /></p>
    </div>
	</div>
	
	<div id="contentblock2" class="pagecontents" style="display:none">
		<a name="target2"></a>
    <div id="upload">
{*    <p>{lang image_upload_text}</p> *}
    <p>U kunt afbeeldingen uploaden in het formaat PNG, GIF en JPG.</p>
{*    <p>Afbeeldingen mogen <b>maximaal</b> 1024 x 768 pixels zijn.</p>
    <form action="{$smarty.server.PHP_SELF}?{$smarty.server.QUERY_STRING}" method="post">
    Hoeveel afbeeldingen wilt u in een keer uploaden? <select name="uploadcount" size="1" onchange="submit();"><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option></select>
    </form>
*}
    <form action="{$smarty.server.PHP_SELF}?{$smarty.server.QUERY_STRING}" method="post" enctype="multipart/form-data">
    <input type="hidden" name="MAX_FILE_SIZE" value="500000" />
    <input type="file" name="imagefile1" size="45"><br />
    {php}
    $maxinputs = $_GET['uploadcount'];
    if (!isset($maxinputs)) $maxinputs = 1;
    for ($i=1;$i<$maxinputs;$i++) {
      echo "<input type=\"file\" name=\"imagefile${i}\" size=\"45\"><br />";
    }
    {/php}    
    <input type="submit" name="upload" value="{lang but_upload}" />
		<input type="button" button onclick="javascript:window.close();" value="{lang but_cancel}" />
    </form>
    </div>
	</div>
<script type="text/javascript">if (window.initPageTabs) initPageTabs();</script>
</body>
</html>
