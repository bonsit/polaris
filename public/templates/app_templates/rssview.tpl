<?xml version="1.0" encoding="utf-8"?>
<?xml-stylesheet type="text/css" href="{$cssfile}" ?>
<rss version="2.0">
<channel>
<title>{$rsstitle}</title>
<link>{$mainlink}</link>
<description>{$description}</description>
<language>{$language}</language>
<copyright>{$copyright}</copyright>
<webMaster>Polaris</webMaster>
<pubDate>{$publicationdate}</pubDate>
<lastBuildDate>{$lastbuilddate}</lastBuildDate>
<category>-</category>
<generator>Polaris</generator>
<docs>http://blogs.law.harvard.edu/tech/rss/</docs>

{section name=i loop=$items}
<item>
<title>{$items[i].$_title_column}</title>
<link>{$items[i].$_link_column}</link>
<description>{$items[i].$_description_column|escape:"htmlall"}</description>
<pubDate>{$items[i].$_publicationdate_column}</pubDate>
</item>
{/section}

</channel>
</rss>
