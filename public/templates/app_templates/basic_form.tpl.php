{if $currentform}
{$currentform->ShowMaster($currentaction, $outputformat)}

{if (isset($currentform->moduleobj) and $currentform->moduleobj->showDefaultButtons == true)}
<div class="buttons">
    <button id="btnSAVE" type="button" class="jqmAccept btn btn-primary positive {if isset($error)}disabled{/if}" accesskey="F9">{lang but_save}</button>
    <button id="btnCANCEL" type="button" class="jqmClose btn btn-default negative" accesskey="F2">{lang but_cancel}</button>
</div>
{/if}

{/if}