{if $error_message}
<div id="errormessage">
<div class="buttons small" style="float:right;"><button id="error_showdetails">Toon details</button></div>
{$pretty_error_message}
<br />
<span id="error_originalmessage" class="original" style="display:none;">{$error_message|nl2br} <br />Errornr: {$error_no}</span>
{if $showbackbutton}<span style="font-size:0.9em;display:block"><a href="javascript:void(0)" onclick="history.go((navigator.appName.indexOf('Internet Explorer') != -1) ? -1 :-2)">&laquo; {lang go_back_after_insert_error}</a></span>{/if}
</div>
{/if}
{if $debug_message}
<div id="debugmessage">{$debug_message}<br /></div>
{/if}
