{strip}
{config_load file="globals.conf"}
{if isset($smarty.get.output) and $smarty.get.output eq 'xml'}
{$cons->Show($smarty.get.output)}
{else}
{include file="header.tpl.php"}
<div id="wrapper">

    {if $maximizeform != 'true'}
        {if $navigationstyle == 'vertical'}
        {include file="constructs_vertical3.tpl.php"}
        {/if}
    {/if}

    <div id="page-wrapper" class="gray-bg">
        {if $maximizeform neq 'true'}
            {if $navigationstyle == 'horizontal'}
            <div class="row">
            {include file="constructs_horizontal.tpl.php"}
            </div>
            {/if}
        {/if}

        {if $currentform == false or ($currentform and !$currentform->HasOption('no-page-header'))}
        <div id="pageheader" class="wrapper white-bg border-bottom page-heading d-print-none">
            <div class="service-section pull-right">
                {include file="featuresection.tpl.php"}
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-7 no-padding">
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <a class="constimg" href="/">{if $cons}{icon name=$cons->record->IMAGEURL size="3x"}{/if}</a>
                            <div class="construct-name">{if $currentgroupname}<span class="construct-name-group">{$currentgroupname} &nbsp;</span>{/if}
                            {$cons->record->CONSTRUCTNAME|get_resource}&nbsp; {if $config.debugmode == true}{$currentform->record->FORMID}{/if}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            {include file="breadcrumbs.tpl.php"}
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-5 no-padding">
                    &nbsp;
                    {if $searchstyle == 'horizontal' and $currentform}{$currentform->ShowGroupFields()}{/if}
                </div>
            </div>
        </div>
        {/if}

        <div class="fh-breadcrumb">
            {if $searchstyle == 'vertical' and $currentform and $maximizeform neq 'true' and !$currentform->HasOption('no-searchbar')}
            <div class="fh-column">
                <div class="slimScrollDiv" style="overflow: scroll">
                    <div class="full-height-scroll">
                        {if $currentform and $maximizeform neq 'true'}
                        {$currentform->ShowSearchPanel($currentaction, $permission, $currentform->detail, false)}
                        {/if}
                    </div>
                </div>
            </div>
            {/if}
            {if $currentform->record->DEFAULTVIEWTYPE == 'CUSTOMVIEW'}
                {$cons->Show($currentaction, $currentdetailaction, $currentdetailform->record->METANAME, $outputformat)}
            {else}
                {if $searchstyle == 'horizontal' and $currentform and $maximizeform neq 'true'
                and !$currentform->HasOption('no-searchbar')}
                {$currentform->ShowHorizontalSearchPanel($currentaction, $permission, $currentform->detail, false)}
                {/if}

                {if $currentform}
                {assign var=withinLimits value=$currentform->clientWithinLimitForTableResources($currentaction, true)}
                {else}
                {assign var=withinLimits value=true}
                {/if}

                <div class="{if !$currentform or ($currentform and !$currentform->HasOption('no-form-autosize'))}full-height{/if}">
                    <div class="slimScrollDiv" style="overflow: scroll">
                        <div class="full-height-scroll gray-bg {if $searchstyle == 'vertical' and $currentform and $maximizeform neq 'true'}border-left{/if}" style="overflow: scroll; width: auto; height: 100%;">

                            {include file="error_message.tpl.php"}

                            {if ($currentconstructid eq 0) and (!$module) and (!$currentform)}
                                {include file="introduction.tpl.php"}
                            {else}
                                {if $currentform}
                                    {if $currentaction == 'insert' and !$withinLimits}
                                        <div class='element-detail-box'>
                                            <div class='containe widget red-bg p-lg text-center'>
                                                <div class='m-b-md'>
                                                    <i class='fa fa-bell fa-4x'></i>
                                                    <h1 class='m-xs'>U heeft het maximum aantal records voor dit formulier bereikt.</h1>
                                                    <h3 class='font-bold no-margins'>
                                                    Huidig aantal: {$currentform->resourcesInUse()} &nbsp; &nbsp; Maximum aantal: {$currentform->clientTableResourceLimit()}
                                                    </h3>
                                                    <br>
                                                    <p>Neem contact op met uw account beheerder om meer resources aan te vragen.</p>
                                                </div>
                                            </div>
                                        </div>
                                    {else}
                                        {if $module}
                                            {$module->Show()}
                                        {elseif $cons}
                                            {$cons->Show($currentaction, $currentdetailaction, $currentdetailform->record->METANAME, $outputformat)}
                                        {elseif $currentform}
                                            {$currentform->ShowMaster($currentaction, $outputformat)}
                                        {/if}
                                    {/if}
                                {/if}
                            {/if}

                        </div>
                    </div>
                </div>
            {/if}

            {if $currentform  and $withinLimits}
            <div id="pagefooter" class="footer d-print-none">
                <div class="row">
                    <div class="col-sm-12 col-md-8">
                    {$currentform->ShowButtons($currentaction, $permission)}
                    {if (!$currentform->detail)}
                        {$currentform->ShowLinkedConstructs($cons->masterform->record->TABLENAME)}
                    {/if}
                    {if ($currentform->ShowInitialDataset($currentaction))}
                        {$currentform->ShowListViewControls($currentaction, $permission)}
                    {/if}
                    </div>
                    <div class="col-sm-12 col-md-4 text-right">
                    {if (($currentaction !== 'edit' and $currentaction !== 'view' )
                        or (
                            $currentdetailaction == 'detail_allitems'
                            and $smarty.get.detailform !== 'overzicht'
                            and $smarty.get.detailform
                        )
                        )
                        and !$currentform->HasOption('no-navigator')}
                        {$currentform->ShowNavigator(false, $currentpermission, $currentaction)}
                    {/if}
                    </div>
                </div>
            </div>
            {/if}

        </form>
    </div>

    <ul id="generic-record-menu" class="dropdown-menu" aria-labelledby="dropdownMenu2">
        {if ($permission & 8) eq 8}
        <li class="dropdown-item"><a href="#" id="generic-edit-link">{icon name="md-edit_square"}&nbsp; {lang edit}...</a></li>
        {/if}
        {if $currentform}
        {$currentform->actions->ShowActionAsDropdownItem($currentaction, false, 'RECORD', 'medium')}
        {/if}
        {if ($permission & 16) eq 16 or ($detailpermission & 16) eq 16}
        <li role="separator" class="divider"></li>
        <li class="dropdown-item text-danger"><a href="#" id="generic-delete-link">{icon name="md-delete"}&nbsp; {lang delete}</a></li>
        {/if}
    </ul>

    <div class="off-canvas-overlay"></div>
    <div class="off-canvas"></div>

</div>

    {if $currentform}
        {if $currentform->moduleobj}{$currentform->moduleobj->ShowModalForms()}{/if}
        {$currentform->ShowCustomScripts()}
    {/if}
    {include file="footer.tpl.php"}
{/if}
{/strip}