<?php
require_once 'includes/app_includes.inc.php';
error_reporting(E_ALL & ~E_DEPRECATED & ~E_STRICT & ~E_WARNING);

/**
 * Create the Polaris object
 */
try {
    $polaris = new base_Polaris();
    $polaris->start();
} catch (Exception $e) {
    if ($_ENV['ENVIRONMENT'] == 'dev') {
        echo "<pre>{$e->getTraceAsString()} <br><br> {$e->getMessage()}</pre>";
    } else {
        $polaris->logQuick('Polaris failed to start: ' . $e->getMessage());
        $polaris->showGenericErrorScreen('Polaris failed to start: ' . $e->getMessage());
    }
}
