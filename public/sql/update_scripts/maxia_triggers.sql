DELIMITER ;;

DROP FUNCTION `GetDayPartFromValue`;;

CREATE FUNCTION `GetDayPartFromValue`(daypart VARCHAR(25)) RETURNS varchar(25) CHARSET utf8mb4 COLLATE utf8mb4_general_ci
    DETERMINISTIC
BEGIN
    DECLARE pretty_daypart VARCHAR(25);
    CASE daypart
      WHEN 'ochtend' THEN SET pretty_daypart = 'in de ochtend';
      WHEN 'middag' THEN SET pretty_daypart = 'in de middag';
      WHEN 'heledag' THEN SET pretty_daypart = 'gedurende de hele dag';
      WHEN 'tweedagen' THEN SET pretty_daypart = 'gedurende twee dagen';
      WHEN 'driedagen' THEN SET pretty_daypart = 'gedurende drie dagen';
      WHEN 'vierdagen' THEN SET pretty_daypart = 'gedurende vier dagen';
      WHEN 'vijfdagen' THEN SET pretty_daypart = 'gedurende vijf dagen';
      WHEN 'eenweek' THEN SET pretty_daypart = 'gedurende de hele week';
      WHEN 'tweeweken' THEN SET pretty_daypart = 'gedurende twee weken';
      WHEN 'drieweken' THEN SET pretty_daypart = 'gedurende drie weken';
      WHEN 'vierweken' THEN SET pretty_daypart = 'gedurende vier weken';
      ELSE SET pretty_daypart = 'gedurende de hele dag';
    END CASE;

  RETURN pretty_daypart;
END ;;
DELIMITER ;



DELIMITER ;;

DROP FUNCTION `GetMonthsFromValue`;;

CREATE FUNCTION `GetMonthsFromValue`(months INT) RETURNS varchar(255) CHARSET utf8mb3
    DETERMINISTIC
BEGIN
  DECLARE month_names VARCHAR(255);
  DECLARE month_index INT DEFAULT 0;

  SET month_names = '';

  WHILE months > 0 AND month_index <= 12 DO
    IF months & (1 << month_index) > 0 THEN
      SET month_names = CONCAT(month_names, IF(month_names = '', '', ', '),
                               DATE_FORMAT(
                                 DATE_FORMAT(
                                   CONCAT('2021-',month_index+1,'-01')
                                   , '%Y-%m-%d'
                                 ), '%b'
                               ));
    END IF;

    SET month_index = month_index + 1;
  END WHILE;

  RETURN month_names;
END ;;
DELIMITER ;



DELIMITER ;;
DROP FUNCTION `GetWeekdaysFromValue`;;

CREATE FUNCTION `GetWeekdaysFromValue`(weekdays INT) RETURNS varchar(255) CHARSET utf8mb3
    DETERMINISTIC
BEGIN
  DECLARE weekday_names VARCHAR(255);
  DECLARE weekday_index INT DEFAULT 0;

  SET weekday_names = '';

  WHILE weekdays > 0 AND weekday_index < 7 DO
    IF weekdays & (1 << weekday_index) > 0 THEN
      SET weekday_names = CONCAT(weekday_names, IF(weekday_names = '', '', ', '),
                               DATE_FORMAT(
                                 DATE_FORMAT(
                                   CONCAT('2023-05-',weekday_index+1)
                                   , '%Y-%m-%d'
                                 ), '%a'
                               ));
    END IF;

    SET weekday_index = weekday_index + 1;
  END WHILE;

  RETURN weekday_names;
END ;;
DELIMITER ;




DELIMITER ;;
DROP TRIGGER `fb_geplandonderhoud_pretty_planning_insert`;;

CREATE TRIGGER `fb_geplandonderhoud_pretty_planning_insert` BEFORE INSERT ON `fb_geplandonderhoud` FOR EACH ROW BEGIN
  CASE WHEN NEW.schema_type = 'D' THEN
    SET NEW.PRETTYPLANNING = CONCAT('Dagelijks ', GetDayPartFromValue(NEW.schema_start_tijd), ', vanaf ', DATE_FORMAT(NEW.schema_dagelijks_start_datum, '%d-%m-%Y'));
  WHEN NEW.schema_type = 'W'
    AND NEW.schema_week_herhaling = 127 THEN
    SET NEW.PRETTYPLANNING = CONCAT('Elke dag om ', NEW.schema_start_tijd, ', vanaf ', DATE_FORMAT(NEW.schema_week_start_datum, '%d-%m-%Y'));
  WHEN NEW.schema_type = 'W'
    AND NEW.schema_week_herhaling < 127 THEN
      CASE
      WHEN NEW.schema_periodiek = '1' THEN
        SET NEW.PRETTYPLANNING = CONCAT('Elke ', GetWeekdaysFromValue(NEW.schema_week_herhaling),' ', GetDayPartFromValue(NEW.schema_start_tijd), ', vanaf ', DATE_FORMAT(NEW.schema_week_start_datum, '%d-%m-%Y'));
      WHEN NEW.schema_periodiek = '2' THEN
        SET NEW.PRETTYPLANNING = CONCAT('Elke twee weken op ', GetWeekdaysFromValue(NEW.schema_week_herhaling),' ', GetDayPartFromValue(NEW.schema_start_tijd), ', vanaf ', DATE_FORMAT(NEW.schema_week_start_datum, '%d-%m-%Y'));
      WHEN NEW.schema_periodiek = '3' THEN
        SET NEW.PRETTYPLANNING = CONCAT('Elke drie weken op ', GetWeekdaysFromValue(NEW.schema_week_herhaling),' ', GetDayPartFromValue(NEW.schema_start_tijd), ', vanaf ', DATE_FORMAT(NEW.schema_week_start_datum, '%d-%m-%Y'));
      END CASE;
  WHEN NEW.schema_type = 'M'
    AND NEW.schema_maand_herhaling = 4095 THEN
    SET NEW.PRETTYPLANNING = CONCAT('Elke ', NEW.schema_maand_start_dag, 'e dag van de maand ', GetDayPartFromValue(NEW.schema_start_tijd), ', vanaf ', DATE_FORMAT(NEW.schema_week_start_datum, '%d-%m-%Y'));
  WHEN NEW.schema_type = 'M'
    AND NEW.schema_maand_herhaling < 4095 THEN
    SET NEW.PRETTYPLANNING = CONCAT('Elke ', NEW.schema_maand_start_dag, 'e dag in ', GetMonthsFromValue(NEW.schema_maand_herhaling), ' ', GetDayPartFromValue(NEW.schema_start_tijd), ', vanaf ', DATE_FORMAT(NEW.schema_week_start_datum, '%d-%m-%Y'));
  WHEN NEW.schema_type = 'E' THEN
    SET NEW.PRETTYPLANNING = CONCAT('Op ', DATE_FORMAT(NEW.schema_eenmalig_start_datum, '%d-%m-%Y'), ' ', GetDayPartFromValue(NEW.schema_start_tijd));
  ELSE
    SET NEW.PRETTYPLANNING = "Fout in planning";
  END CASE;
END ;;


DELIMITER ;;

DROP TRIGGER `fb_geplandonderhoud_pretty_planning_update`;;

CREATE TRIGGER `fb_geplandonderhoud_pretty_planning_update` BEFORE UPDATE ON `fb_geplandonderhoud` FOR EACH ROW BEGIN
  CASE WHEN NEW.schema_type = 'D' THEN
    SET NEW.PRETTYPLANNING = CONCAT('Dagelijks ', GetDayPartFromValue(NEW.schema_start_tijd), ', vanaf ', DATE_FORMAT(NEW.schema_dagelijks_start_datum, '%d-%m-%Y'));
  WHEN NEW.schema_type = 'W'
    AND NEW.schema_week_herhaling = 127 THEN
    SET NEW.PRETTYPLANNING = CONCAT('Elke dag om ', NEW.schema_start_tijd, ', vanaf ', DATE_FORMAT(NEW.schema_week_start_datum, '%d-%m-%Y'));
  WHEN NEW.schema_type = 'W'
    AND NEW.schema_week_herhaling < 127 THEN
      CASE
      WHEN NEW.schema_periodiek = '1' THEN
        SET NEW.PRETTYPLANNING = CONCAT('Elke ', GetWeekdaysFromValue(NEW.schema_week_herhaling),' ', GetDayPartFromValue(NEW.schema_start_tijd), ', vanaf ', DATE_FORMAT(NEW.schema_week_start_datum, '%d-%m-%Y'));
      WHEN NEW.schema_periodiek = '2' THEN
        SET NEW.PRETTYPLANNING = CONCAT('Elke twee weken op ', GetWeekdaysFromValue(NEW.schema_week_herhaling),' ', GetDayPartFromValue(NEW.schema_start_tijd), ', vanaf ', DATE_FORMAT(NEW.schema_week_start_datum, '%d-%m-%Y'));
      WHEN NEW.schema_periodiek = '3' THEN
        SET NEW.PRETTYPLANNING = CONCAT('Elke drie weken op ', GetWeekdaysFromValue(NEW.schema_week_herhaling),' ', GetDayPartFromValue(NEW.schema_start_tijd), ', vanaf ', DATE_FORMAT(NEW.schema_week_start_datum, '%d-%m-%Y'));
      END CASE;
  WHEN NEW.schema_type = 'M'
    AND NEW.schema_maand_herhaling = 4095 THEN
    SET NEW.PRETTYPLANNING = CONCAT('Elke ', NEW.schema_maand_start_dag, 'e dag van de maand ', GetDayPartFromValue(NEW.schema_start_tijd), ', vanaf ', DATE_FORMAT(NEW.schema_week_start_datum, '%d-%m-%Y'));
  WHEN NEW.schema_type = 'M'
    AND NEW.schema_maand_herhaling < 4095 THEN
    SET NEW.PRETTYPLANNING = CONCAT('Elke ', NEW.schema_maand_start_dag, 'e dag in ', GetMonthsFromValue(NEW.schema_maand_herhaling), ' ', GetDayPartFromValue(NEW.schema_start_tijd), ', vanaf ', DATE_FORMAT(NEW.schema_week_start_datum, '%d-%m-%Y'));
  WHEN NEW.schema_type = 'E' THEN
    SET NEW.PRETTYPLANNING = CONCAT('Op ', DATE_FORMAT(NEW.schema_eenmalig_start_datum, '%d-%m-%Y'), ' ', GetDayPartFromValue(NEW.schema_start_tijd));
  ELSE
    SET NEW.PRETTYPLANNING = "Fout in planning";
  END CASE;
END ;;



DELIMITER ;


DELIMITER ;;

DROP TRIGGER `fb_reserveringen_intern_logger_bu`;;

CREATE TRIGGER `fb_reserveringen_intern_logger_bu` BEFORE UPDATE ON `fb_reserveringen_intern` FOR EACH ROW BEGIN

	IF OLD.status <> NEW.status THEN
	    INSERT INTO fb_reserveringen_intern_log (aanvraag_id, aanvrager_naam, client_hash, status_old, status_new)
	    VALUES (OLD.aanvraag_id, COALESCE(OLD.aanvrager_naam, 'N/A'), OLD.client_hash, COALESCE(OLD.status, 'N/A'), NEW.status);
	 END IF;

    -- If certain columns are changed, resend the confirmation email
    IF OLD.SCHEMA_TERUGKEREND <> NEW.SCHEMA_TERUGKEREND
    OR OLD.CONTACTPERSOON_EMAIL <> NEW.CONTACTPERSOON_EMAIL
    OR OLD.GEWENSTE_TIJD_START <> NEW.GEWENSTE_TIJD_START
    OR OLD.GEWENSTE_TIJD_EIND <> NEW.GEWENSTE_TIJD_EIND  THEN
        SET NEW.__ALERTSTATUSBEVESTIGING := null;
    END IF;

END ;;

DELIMITER ;;

DROP TRIGGER `fb_meldingen_bu`;;

CREATE TRIGGER `fb_meldingen_bu` BEFORE UPDATE ON `fb_meldingen` FOR EACH ROW BEGIN

  IF OLD.__ALERTSTATUSOPMERKINGEN <> NEW.__ALERTSTATUSOPMERKINGEN THEN
      SET NEW.__OPMERKINGFIELDCHANGED := 'N';
  END IF;

	IF OLD.OPMERKINGEN <> NEW.OPMERKINGEN THEN
      SET NEW.__OPMERKINGFIELDCHANGED := 'Y';
  END IF;

	IF OLD.status <> NEW.status THEN
	    INSERT INTO fb_meldingen_log (melding_id, aanvrager_naam, client_hash, status_old, status_new)
	    VALUES (OLD.melding_id, COALESCE(OLD.aanvrager_naam, 'N/A'), OLD.client_hash, COALESCE(OLD.status, 'N/A'), NEW.status);
	END IF;

END ;;


DROP TRIGGER `fb_reserveringen_intern_logger_ai`;;

CREATE TRIGGER `fb_reserveringen_intern_logger_ai` AFTER INSERT ON `fb_reserveringen_intern` FOR EACH ROW BEGIN

    INSERT INTO fb_reserveringen_intern_log (aanvraag_id, aanvrager_naam, client_hash, status_old, status_new)
    VALUES (NEW.aanvraag_id, COALESCE(NEW.aanvrager_naam, 'N/A'), NEW.client_hash, '', NEW.status);

END ;;


DROP TRIGGER `fb_reserveringen_intern_logger_au`;;

CREATE TRIGGER `fb_reserveringen_intern_logger_au` AFTER UPDATE ON `fb_reserveringen_intern` FOR EACH ROW BEGIN

    -- If the record is deleted, we can delete the rrule time records
    IF OLD.__DELETED <> NEW.__DELETED AND NEW.__DELETED = 1 THEN
        DELETE FROM fb_reserveringen_intern_rrule
        WHERE aanvraag = NEW.aanvraag_id;
    END IF;

END ;;


DROP TRIGGER `do_persoonsbeeld_bi`;;

CREATE TRIGGER do_persoonsbeeld_bi
BEFORE INSERT ON do_persoonsbeeld
FOR EACH ROW
BEGIN
    DECLARE max_versie INT;

    -- Haal de maximale versie van de deelnemer op
    SELECT IFNULL(MAX(versie), 0) INTO max_versie
    FROM do_persoonsbeeld
    WHERE deelnemer = NEW.deelnemer;

    -- Zet de nieuwe versie in als max_versie + 1
    SET NEW.versie = max_versie + 1;
END;;

DELIMITER ;



DELIMITER ;;

DROP TRIGGER `do_deelnemers_au`;;

CREATE TRIGGER do_deelnemers_au
AFTER UPDATE ON do_deelnemers
FOR EACH ROW
BEGIN
    IF OLD.traject != NEW.traject THEN
        INSERT INTO do_deelnemertrajecten (
            client_hash,
            deelnemer,
            traject,
            datum_vanaf
        ) VALUES (
            NEW.client_hash,
            NEW.deelnemer_id,
            NEW.traject,
            CURDATE()
        );
    END IF;
END ;;

DELIMITER ;


DELIMITER ;;

DROP PROCEDURE IF EXISTS wp_posts_pivot;;
CREATE DEFINER=`root`@`%` PROCEDURE `wp_posts_pivot`(
	IN post_type_filter VARCHAR(50)
)
BEGIN

    /* allow longer concat */
    DECLARE max_len_original INT UNSIGNED DEFAULT 0;
    SET max_len_original = @@group_concat_max_len;
    SET @@group_concat_max_len = 700000;

    SET @sql = NULL;
    SELECT
       REPLACE(
        GROUP_CONCAT(DISTINCT CONCAT('MAX(IF(pm.meta_key = ''',
                    meta_key,
                    ''', pm.meta_value, NULL)) AS `',
                    meta_key,
                    '`'))
		, 'positivehealth_questions_', 'ph_q_')

    INTO @sql FROM
        athos_import.wp_posts p
            INNER JOIN
        athos_import.wp_postmeta AS pm ON p.id = pm.post_id
    WHERE SUBSTRING(meta_key, 1, 1) <> '_' AND
        p.post_type COLLATE utf8mb4_unicode_ci = post_type_filter COLLATE utf8mb4_unicode_ci;

    SET @sql = CONCAT('CREATE TABLE aa_',
    	REPLACE(
	    		post_type_filter, '-', '_'
    	),
    	' AS SELECT p.id
                        , p.post_title
                        , p.post_date
                        , p.post_modified
                        , p.post_author
                        , p.post_status
                        , u.user_login
                        , ', @sql, '
                       FROM athos_import.wp_posts p
			            LEFT JOIN
				        athos_import.`wp_users` AS u ON p.`post_author` = u.`ID`
                       LEFT JOIN athos_import.wp_postmeta AS pm
                        ON p.id = pm.post_id
                        WHERE p.post_type COLLATE utf8mb4_unicode_ci = \'', post_type_filter COLLATE utf8mb4_unicode_ci, '\'
                       GROUP BY p.id, p.post_title, p.post_status');

    /* reset the default concat */
    SET @@group_concat_max_len = max_len_original;

    /*
    SELECT @sql;
    */

    PREPARE stmt FROM @sql;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;

END;;
DELIMITER ;




DELIMITER ;;
DROP PROCEDURE wp_2_posts_pivot;;
CREATE DEFINER=`root`@`%` PROCEDURE `wp_2_posts_pivot`(
	IN post_type_filter VARCHAR(50)
)
BEGIN

    /* allow longer concat */
    DECLARE max_len_original INT UNSIGNED DEFAULT 0;
    SET max_len_original = @@group_concat_max_len;
    SET @@group_concat_max_len = 700000;

    SET @sql = NULL;
    SELECT
       REPLACE(
        GROUP_CONCAT(DISTINCT CONCAT('MAX(IF(pm.meta_key = ''',
                    meta_key,
                    ''', pm.meta_value, NULL)) AS `',
                    meta_key,
                    '`'))
		, 'positivehealth_questions_', 'ph_q_')

    INTO @sql FROM
        athos_import.wp_2_posts p
            INNER JOIN
        athos_import.wp_2_postmeta AS pm ON p.id = pm.post_id
    WHERE SUBSTRING(meta_key, 1, 1) <> '_' AND
        p.post_type COLLATE utf8mb4_unicode_ci = post_type_filter COLLATE utf8mb4_unicode_ci;


    SET @row_count = 0;
    SELECT COUNT(*) INTO @row_count
    FROM athos_import.wp_2_posts p
    WHERE p.post_type COLLATE utf8mb4_unicode_ci = post_type_filter COLLATE utf8mb4_unicode_ci;

    IF @row_count > 0 THEN

      SET @sql = CONCAT('CREATE TABLE IF NOT EXISTS aa_',
        REPLACE(
            post_type_filter, '-', '_'
        ), '_2',
        ' AS SELECT p.id
                          , p.post_title
                          , p.post_date
                          , p.post_modified
                          , p.post_author
                          , p.post_status
                          , u.user_login
                          , ', @sql, '
                        FROM athos_import.wp_2_posts p
                    LEFT JOIN
                  athos_import.`wp_users` AS u ON p.`post_author` = u.`ID`
                        LEFT JOIN athos_import.wp_2_postmeta AS pm
                          ON p.id = pm.post_id
                          WHERE p.post_type COLLATE utf8mb4_unicode_ci = \'', post_type_filter COLLATE utf8mb4_unicode_ci, '\'
                        GROUP BY p.id, p.post_title, p.post_status');

      /* reset the default concat */
      SET @@group_concat_max_len = max_len_original;

      PREPARE stmt FROM @sql;
      EXECUTE stmt;
      DEALLOCATE PREPARE stmt;

    ELSE
        -- Handle the case when no rows are found
        SELECT CONCAT('No rows found for post_type: ', post_type_filter) AS message;
    END IF;

END;;
DELIMITER ;






DELIMITER ;;
DROP PROCEDURE wp_3_posts_pivot;;

CREATE DEFINER=`root`@`%` PROCEDURE `wp_3_posts_pivot`(
	IN post_type_filter VARCHAR(50)
)
BEGIN

    /* allow longer concat */
    DECLARE max_len_original INT UNSIGNED DEFAULT 0;
    SET max_len_original = @@group_concat_max_len;
    SET @@group_concat_max_len = 700000;

    SET @sql = NULL;
    SELECT
       REPLACE(
        GROUP_CONCAT(DISTINCT CONCAT('MAX(IF(pm.meta_key = ''',
                    meta_key,
                    ''', pm.meta_value, NULL)) AS `',
                    meta_key,
                    '`'))
		, 'positivehealth_questions_', 'ph_q_')

    INTO @sql FROM
        athos_import.wp_3_posts p
            INNER JOIN
        athos_import.wp_3_postmeta AS pm ON p.id = pm.post_id
    WHERE SUBSTRING(meta_key, 1, 1) <> '_' AND
        p.post_type COLLATE utf8mb4_unicode_ci = post_type_filter COLLATE utf8mb4_unicode_ci;

    SET @row_count = 0;
    SELECT COUNT(*) INTO @row_count
    FROM athos_import.wp_3_posts p
    WHERE p.post_type COLLATE utf8mb4_unicode_ci = post_type_filter COLLATE utf8mb4_unicode_ci;

    IF @row_count > 0 THEN

        SET @sql = CONCAT('CREATE TABLE IF NOT EXISTS aa_',
          REPLACE(
              post_type_filter, '-', '_'
          ), '_3',
          ' AS SELECT p.id
                            , p.post_title
                            , p.post_date
                            , p.post_modified
                            , p.post_author
                            , p.post_status
                            , u.user_login
                            , ', @sql, '
                          FROM athos_import.wp_3_posts p
                      LEFT JOIN
                    athos_import.`wp_users` AS u ON p.`post_author` = u.`ID`
                          LEFT JOIN athos_import.wp_3_postmeta AS pm
                            ON p.id = pm.post_id
                            WHERE p.post_type COLLATE utf8mb4_unicode_ci = \'', post_type_filter COLLATE utf8mb4_unicode_ci, '\'
                          GROUP BY p.id, p.post_title, p.post_status');

        /* reset the default concat */
        SET @@group_concat_max_len = max_len_original;

        /*
        SELECT @sql;
        */

        PREPARE stmt FROM @sql;
        EXECUTE stmt;
        DEALLOCATE PREPARE stmt;
    ELSE
        -- Handle the case when no rows are found
        SELECT CONCAT('No rows found for post_type: ', post_type_filter) AS message;
    END IF;

END;;
DELIMITER ;

