<?php
$polarisscramble = $_ENV['POLARIS_HASH'];
$scramble = $_ENV['POLARIS_HASH'];
// update plr_database set `ROOTPASSWORD` = AES_ENCRYPT('...password...', <env.POLARIS_HASH>) where databaseid = 1 and clientid = 12

$ClientHash           = DEFAULT_DB_HASH_FUNCTION.'(CONCAT(CLIENTID, "' . $scramble . '")) ';
$SpecClientHash       = DEFAULT_DB_HASH_FUNCTION.'(CONCAT(C.CLIENTID, "' . $scramble . '")) ';
$FormClientHash       = DEFAULT_DB_HASH_FUNCTION.'(CONCAT(F.CLIENTID, "' . $scramble . '")) ';
$AppClientHash        = DEFAULT_DB_HASH_FUNCTION.'(CONCAT(A.CLIENTID, "' . $scramble . '")) ';
$AppHash              = DEFAULT_DB_HASH_FUNCTION.'(CONCAT(A.CLIENTID, "_", A.APPLICATIONID, "' . $scramble . '")) ';
$AppHash_clean        = DEFAULT_DB_HASH_FUNCTION.'(CONCAT(CLIENTID, "_", APPLICATIONID, "' . $scramble . '")) ';
$FormAppHash          = DEFAULT_DB_HASH_FUNCTION.'(CONCAT(F.CLIENTID, "_", "' . $scramble . '")) ';
$UserGroupHash        = DEFAULT_DB_HASH_FUNCTION.'(CONCAT(U.CLIENTID, "_", U.USERGROUPID, "' . $scramble . '")) ';
$UserGroupHash_clean  = DEFAULT_DB_HASH_FUNCTION.'(CONCAT(CLIENTID, "_", USERGROUPID, "' . $scramble . '")) ';
$GroupHash            = DEFAULT_DB_HASH_FUNCTION.'(CONCAT(CLIENTID, "_", GROUPID, "' . $scramble . '")) ';
$GroupMemberHash      = DEFAULT_DB_HASH_FUNCTION.'(CONCAT(M.CLIENTID, "_", M.GROUPID, "' . $scramble . '")) ';
$FormHash             = DEFAULT_DB_HASH_FUNCTION.'(CONCAT(F.CLIENTID, "_", F.FORMID, "' . $scramble . '")) ';
$ReplyruleHash        = DEFAULT_DB_HASH_FUNCTION.'(CONCAT(CLIENTID, "_", FORMID, "_", REPLYRULEID, "' . $scramble . '")) ';
$AlertHash            = DEFAULT_DB_HASH_FUNCTION.'(CONCAT(CLIENTID, "_", APPLICATIONID, "_", FORMID, "_", ALERTID, "' . $scramble . '")) ';
$TagHash              = DEFAULT_DB_HASH_FUNCTION.'(CONCAT(TAGID, "' . $scramble . '")) ';
$ActionHash           = DEFAULT_DB_HASH_FUNCTION.'(CONCAT(CLIENTID, "_", FORMID, "_", ACTIONID, "' . $scramble . '")) ';
$FormTagHash          = DEFAULT_DB_HASH_FUNCTION.'(CONCAT(CLIENTID, "_", FORMID, "_", TAGID, "' . $scramble . '")) ';
$FormpermHash         = DEFAULT_DB_HASH_FUNCTION.'(CONCAT(CLIENTID, "_", FORMID, "_", USERGROUPID, "' . $scramble . '")) ';

$DatabaseHash         = DEFAULT_DB_HASH_FUNCTION.'(CONCAT("' . $scramble . '", "_", CLIENTID, "_", DATABASEID)) ';
$DatabaseHash_column  = DEFAULT_DB_HASH_FUNCTION.'(CONCAT("' . $scramble . '", "_", c.CLIENTID, "_", c.DATABASEID)) ';
$WebPageHash          = DEFAULT_DB_HASH_FUNCTION.'(CONCAT("' . $scramble . '", "_", pageid)) ';
$WebParamHash         = DEFAULT_DB_HASH_FUNCTION.'(CONCAT("' . $scramble . '", "_", paramid)) ';
$WebMenuHash          = DEFAULT_DB_HASH_FUNCTION.'(CONCAT("' . $scramble . '", "_", menuid)) ';
$WebParentMenuHash    = DEFAULT_DB_HASH_FUNCTION.'(CONCAT("' . $scramble . '", "_", parentmenuid)) ';

$DomainHash           = DEFAULT_DB_HASH_FUNCTION.'(CONCAT("' . $scramble . '", "_", f.CLIENTID, "_", f.DOMAINID)) ';
$DomainHash_ph        = DEFAULT_DB_HASH_FUNCTION.'(CONCAT("' . $scramble . '", "_", ph.CLIENTID, "_", ph.DOMAINID)) ';
$UIHash               = DEFAULT_DB_HASH_FUNCTION.'(CONCAT("' . $scramble . '", "_", u.CLIENTID, "_", u.DOMAINID, "_", u.UIID)) ';

$UserGroupV2Hash      = DEFAULT_DB_HASH_FUNCTION.'(CONCAT("' . $scramble . '", "_", u.CLIENTID, "_", u.USERGROUPID)) ';

$_sqlApplicationListBASE = '
    SELECT DISTINCT A.CLIENTID, A.APPLICATIONID, APPLICATIONNAME, DESCRIPTION
    , REGISTERED, VERSION, IMAGE, METANAME, PARAMETERS
    , APPLICATIONPICTURE, AP.DEFAULTAPPLICATION, ' . $AppHash . ' as RECORDID
		, C.NAME, A.CUSTOMCSS, SEARCHOPTIONS, NAVIGATIONSTYLE, SEARCHSTYLE, A.DATEFORMAT, A.TIMEFORMAT, A.ISPOLARISDESIGNER
        , A.APPTILECOLOR, A.ORDERINDEX, A.EXTERNALURL, A.SELECTDATABASE, A.SELECTVIEW, A.SELECTCOLUMNNAME, A.SELECTSHOWCOLUMNS
	FROM plr_application A
	LEFT JOIN plr_applicationpermission AP
	ON A.CLIENTID = AP.CLIENTID
	AND A.APPLICATIONID = AP.APPLICATIONID
	LEFT JOIN plr_applicationpermission AP2
	ON A.CLIENTID = AP2.CLIENTID
	AND A.APPLICATIONID = AP2.APPLICATIONID
	LEFT JOIN plr_client C
	ON A.CLIENTID = C.CLIENTID
  LEFT JOIN plr_membership M
  ON M.CLIENTID = C.CLIENTID
  AND M.USERGROUPID = AP.USERGROUPID
';

$_sqlClientApplicationList = $_sqlApplicationListBASE . '
	WHERE A.CLIENTID = ?
  AND AP.USERGROUPID in (?)
  AND AP.`PERMISSIONTYPE` > 1
	ORDER BY A.ORDERINDEX
';

$_sqlCompleteApplicationList = '
  SELECT DISTINCT A.CLIENTID, A.APPLICATIONID, APPLICATIONNAME, DESCRIPTION
    , REGISTERED, VERSION, IMAGE, METANAME
    , APPLICATIONPICTURE, AP.DEFAULTAPPLICATION, ' . $AppHash . ' as RECORDID
		, C.NAME, ' . $AppClientHash . ' as CLIENTHASHID
    , SEARCHOPTIONS, NAVIGATIONSTYLE, SEARCHSTYLE, A.DATEFORMAT, A.TIMEFORMAT, A.ISPOLARISDESIGNER
    , A.APPTILECOLOR, A.ORDERINDEX
	FROM plr_application A
	LEFT JOIN plr_applicationpermission AP
	ON A.CLIENTID = AP.CLIENTID
	AND A.APPLICATIONID = AP.APPLICATIONID
	LEFT JOIN plr_client C
	ON A.CLIENTID = C.CLIENTID
  GROUP BY CLIENTID, APPLICATIONID
	ORDER BY CLIENTID, APPLICATIONNAME
';

$_sqlDSGClients = '
	SELECT CLIENTID, NAME, ADDRESS, ZIPCODE, CITY, TELEPHONE, FAX, POSTADDRESS, POSTZIPCODE
	, POSTCITY, COUNTRY, DEFAULTLANGUAGE, ' . $ClientHash . ' as RECORDID
	, ' . $ClientHash . ' AS CLIENTHASHID
	FROM plr_client C
';

$_sqlDSGClient = $_sqlDSGClients . '
	WHERE ' . $ClientHash . ' = ?
';

$_sqlDSGGetHashClient = $_sqlDSGClients . '
	WHERE C.clientid = ?
';

$_sqlDSGApplications_base = '
	SELECT A.CLIENTID as CLIENTID, A.APPLICATIONID, A.APPLICATIONNAME, A.VERSION
	, A.DESCRIPTION, A.REGISTERED, A.IMAGE, C.NAME, A.METANAME
	, ' . $AppHash . ' as RECORDID
	FROM plr_application A
   LEFT JOIN plr_client C
   ON A.clientid = C.clientid
';

$_sqlDSGApplications = $_sqlDSGApplications_base . '
	ORDER BY C.NAME
';

$_sqlDSGApplication = $_sqlDSGApplications_base . '
  WHERE ' . $AppHash . ' = ?
';

$_sqlDSGClientApplications = $_sqlDSGApplications_base . '
  WHERE ' . $SpecClientHash . ' = ?
';


$_sqlDSGClientForms = '
	SELECT F.*, ' . $FormHash . ' as RECORDID
	FROM plr_form F
';
//	WHERE '.$FormAppHash.' = ?

$_sqlDSGClientForms = '
	SELECT F.*, ' . $FormHash . ' as RECORDID, count(P.pageid) as PAGECOUNT
	FROM plr_form F
	LEFT JOIN plr_page P
	ON F.clientid = P.clientid
	AND F.formid = P.formid
	WHERE ' . $FormAppHash . ' = ?
	GROUP BY F.formid
';

$_sqlDSGClientForm = '
	SELECT F.*
	FROM plr_form F
	WHERE ' . $FormHash . ' = ?
';

$_sqlDSGAuthorization = '
  SELECT P.clientid,  P.applicationid, P.permissiontype, P.defaultapplication
  , P.usergroupid, U.usergroupname
  FROM plr_applicationpermission P
  LEFT JOIN plr_application A
  ON P.clientid = A.clientid
  AND P.applicationid = A.applicationid
  LEFT JOIN plr_usergroup U
  ON P.clientid = U.clientid
  AND P.usergroupid = U.usergroupid
  WHERE ' . $AppHash . ' = ?
';

$_sqlDSG_SG_List = '
    SELECT websiteid, clientid, name, sitename, url, docroot, owner, buildnumber, meta_description,
    meta_keywords, locale, encoding, destination, ftphost, ftpuser, ftppassw, defaultpageprocessor,
    databasehost, databasename, databaseuser, databasepassword
    FROM sitegenerator.web_site
';

$_sqlDSG_SG_Clientlist = $_sqlDSG_SG_List . '
    WHERE clientid = ?
';

$_sqlReplyRulesBASE = '
	SELECT CLIENTID, FORMID, REPLYRULEID, FROMADDRESS, SUBJECT, BODYTEXT, MAILTAG
	, ' . $ReplyruleHash . ' as RECORDID
	FROM plr_replyrule F
';

$_sqlReplyRules = $_sqlReplyRulesBASE . '
	WHERE ' . $FormHash . ' = ?
';

$_sqlReplyRule = $_sqlReplyRulesBASE . '
  WHERE ' . $ReplyruleHash . ' = ?
';

$_sqlSingleReplyRule = $_sqlReplyRulesBASE . '
    WHERE ' . $FormHash . ' = ? AND replyruleid = ?
';

$_sqlReplyRuleWithHash = "
    SELECT F.*, $FormHash AS FORMHASH FROM plr_replyrule F WHERE MAILTAG = ?
";

$_sqlAlertBASE = '
    SELECT CLIENTID, APPLICATIONID, FORMID, ALERTID, IMAGEINDEX, ORDERINDEX, VISIBLE, FILTER, STATUSCOLUMN
    , ALERTTYPE, SUBJECT, BODYTEXT, ADDRESSEE, ADDRESSEETYPE, TEMPLATEID
    , ' . $AlertHash . ' as RECORDID
    FROM plr_alert F
';

$_sqlAlerts = $_sqlAlertBASE . '
    WHERE VISIBLE = \'Y\'
    ORDER BY orderindex
';

$_sqlMemberships = '
	SELECT U.CLIENTID, U.USERGROUPID, U.USERGROUPNAME, U.FULLNAME, U.SERVERMAPPING, U.DESCRIPTION
	, ' . $UserGroupHash . ' as RECORDID
	FROM plr_usergroup U
	WHERE U.CLIENTID = ?
	AND U.USERGROUPID IN (#subselect#)
';

$_sqlUnusedClientUsers = '
	SELECT U.CLIENTID, U.USERGROUPID, U.USERGROUPNAME, U.FULLNAME, U.DESCRIPTION
	, ' . $UserGroupHash . ' as RECORDID
	FROM plr_usergroup U
	WHERE ' . $ClientHash . ' = ?
	AND U.USERGROUPID NOT IN (#subselect#)
    ORDER BY USERORGROUP, USERGROUPNAME
';

$_sqlDeleteMembership = '
	DELETE FROM plr_membership
	WHERE ' . $UserGroupHash_clean . ' = ?
	AND ' . $GroupHash . ' = ?
';

$_sqlInsertMembership = '
  INSERT INTO plr_membership (clientid, usergroupid, groupid)
  SELECT clientid, ?, usergroupid
  FROM plr_usergroup
  WHERE ' . $UserGroupHash_clean . ' in (#subselect#)
';

$_sqlInsertMembers = '
  INSERT INTO plr_membership (clientid, usergroupid, groupid)
  SELECT clientid, usergroupid, ?
  FROM plr_usergroup
  WHERE ' . $UserGroupHash_clean . ' in (#subselect#)
';

$_sqlMembers = '
	SELECT U.CLIENTID, U.USERGROUPID, U.USERGROUPNAME, U.DESCRIPTION, U.FULLNAME, U.SERVERMAPPING
	, ' . $UserGroupHash . ' as  RECORDID
	FROM plr_usergroup U
  LEFT JOIN plr_membership M
  ON U.clientid = M.clientid
  and U.usergroupid = M.usergroupid
	WHERE U.clientid = ?
	AND ' . $GroupMemberHash . ' = ?
';

$_sqlGetUserGroupName = '
	SELECT usergroupname
	FROM plr_usergroup
	WHERE clientid = ?
	AND usergroupid = ?
';

$_sqlGroupBASE = '
    SELECT M1.USERGROUPID AS LVL1, M1.GROUPID AS LVL2, M2.GROUPID AS LVL3, M3.GROUPID AS LVL4, M4.GROUPID AS LVL5, M5.GROUPID AS LVL6, M6.GROUPID AS LVL7
    FROM plr_membership M1
    LEFT JOIN plr_membership M2 ON M1.`CLIENTID` = M2.`CLIENTID` AND M1.`GROUPID` = M2.`USERGROUPID`
    LEFT JOIN plr_membership M3 ON M2.`CLIENTID` = M3.`CLIENTID` AND M2.`GROUPID` = M3.`USERGROUPID`
    LEFT JOIN plr_membership M4 ON M3.`CLIENTID` = M4.`CLIENTID` AND M3.`GROUPID` = M4.`USERGROUPID`
    LEFT JOIN plr_membership M5 ON M4.`CLIENTID` = M5.`CLIENTID` AND M4.`GROUPID` = M5.`USERGROUPID`
    LEFT JOIN plr_membership M6 ON M5.`CLIENTID` = M6.`CLIENTID` AND M5.`GROUPID` = M6.`USERGROUPID`
';

$_sqlGroup = $_sqlGroupBASE . '
	WHERE M1.CLIENTID = ?
	AND M1.USERGROUPID = ?
    ORDER BY LENGTH(CONCAT_WS(LVL1, LVL2, LVL3, LVL4, LVL5, LVL6, LVL7)) DESC
';

$_sqlUsersGroups_base = '
	SELECT U.CLIENTID AS CLIENT, USERGROUPID, ROOTADMIN, CLIENTADMIN, SERVERMAPPING
	, USERGROUPNAME, DESCRIPTION, FULLNAME, LASTLOGINDATE, USERORGROUP, ACCOUNTDISABLED
	, IF(USERORGROUP="USER",LOGINCOUNT,"") AS LOGINCOUNT, ' . $UserGroupHash . ' as RECORDID
	, C.NAME as CLIENTNAME
	FROM plr_usergroup U
	LEFT JOIN plr_client C
	ON U.clientid = C.clientid
';

$_sqlUsersGroups = $_sqlUsersGroups_base . '
	ORDER BY CLIENT, USERGROUPNAME
';

$_sqlClientUsersGroups = $_sqlUsersGroups_base . '
	WHERE ' . $SpecClientHash . ' = ?
	ORDER BY USERORGROUP, USERGROUPNAME
';

$_sqlUnusedClientGroups = $_sqlUsersGroups_base . '
	WHERE ' . $SpecClientHash . ' = ?
  AND USERORGROUP = "GROUP"
	AND U.USERGROUPID NOT IN (#subselect#)
';

$_sqlUserGroup = '
	SELECT U.*
	FROM plr_usergroup U
	WHERE ' . $UserGroupHash . ' = ?
';

$_sqlUserGroupV2 = '
	SELECT U.*, C.IMAGESGROUP_HASH, C.CUSTOMCSS, C.NAME as CLIENTNAME
	FROM plr_usergroup U
  LEFT JOIN plr_client C
  ON U.clientid = C.clientid
	WHERE ' . $UserGroupV2Hash . ' = ?
';

$_sqlUserGroup2 = '
	SELECT U.*, ' . $UserGroupHash . ' as RECORDID
	FROM plr_usergroup U
	WHERE ' . $ClientHash . ' = ?
  AND usergroupid = ?
';

$_sqlUpdateUserGroup = '
  UPDATE plr_usergroup
  SET fullname = ?
    , description = ?
    , emailaddress = ?
    , photourl = ?
  WHERE ' . $UserGroupHash_clean . ' = ?
';

$_sqlUpdateUserPassword = '
  UPDATE plr_usergroup
  SET userpassword = ?
  WHERE ' . $UserGroupHash_clean . ' = ?
';

$_sqlConfirmAccount = '
  UPDATE plr_usergroup
  SET accountconfirmed = "Y"
  WHERE ' . $UserGroupHash_clean . ' = ?
';

$_sqlClientBASE = '
	SELECT CLIENTID, NAME, DEFAULTLANGUAGE, POSTMARK_API_TOKEN, CUSTOMCSS, PARAMETERTABLENAME
  , ADDRESS, ZIPCODE, CITY, TELEPHONE, FAX, POSTADDRESS, POSTZIPCODE, POSTCITY, COUNTRY
  , ' . $ClientHash . ' AS CLIENTHASHID
	FROM plr_client
';

$_sqlClients = $_sqlClientBASE;

$_sqlClient = $_sqlClientBASE . '
	WHERE ClientID = ?
';

$_sqlClientMETA = $_sqlClientBASE . '
	WHERE ' . $ClientHash . ' = ?
';

$_sqlClientApiKeys = '
	SELECT APIKEY, APISECRET
	FROM plr_client
  WHERE ' . $ClientHash . ' = ?
  AND APIKEYACTIVE = "Y"
';

$_sqlApplicationsBASE = '
	SELECT distinct A.*, FTPSERVER, FTPUSER, AES_DECRYPT(FTPPASSWORD, "' . $polarisscramble . '") as FTPPW
	, ' . $AppHash . ' as RECORDID, METANAME, ' . $AppClientHash . ' as CLIENTHASHID
	FROM plr_application A
	LEFT JOIN plr_applicationpermission AP
	ON A.CLIENTID = AP.CLIENTID
	AND A.APPLICATIONID = AP.APPLICATIONID
';

$_sqlApplications = $_sqlApplicationsBASE . '
	WHERE A.CLIENTID = ?
';

$_sqlApplication = $_sqlApplicationsBASE . '
  WHERE ' . $AppHash . ' = ?
';

$_sqlApplicationAdmin = $_sqlApplicationsBASE . '
  WHERE ' . $AppClientHash . ' = ?
  AND metaname = ?
';

$_sqlApplicationMeta = $_sqlApplicationsBASE . '
  WHERE ' . $AppClientHash . ' = ?
  AND metaname = ?
';

$_sqlApplication_two = $_sqlApplicationsBASE . '
  WHERE ' . $AppClientHash . ' = ?
  AND A.APPLICATIONID = ?
';

$_sqlMaxApplicationId = '
	SELECT MAX(A.APPLICATIONID) AS MAXID
	FROM plr_application A
	WHERE clientid = ?
';

$_sqlConstructs_OLD = '
	SELECT CONSTRUCTID, CONSTRUCTNAME, IMAGEURL
	FROM plr_construct
	WHERE CLIENTID = ?
	AND APPLICATIONID = ?
	AND VISIBLE = "Y"
	ORDER BY ORDERINDEX
';

$_sqlConstructsBASE = '
	SELECT distinct C.CONSTRUCTID, C.CONSTRUCTNAME, C.IMAGEURL, C.CONSTRUCTTYPE, FP.DEFAULTFORM, C.METANAME
	, C.INITIALGROUPSTATE, F.OPTIONS, C.SHOWCOUNTBADGE, C.RECORDCOUNTER, C.BADGEUPDATEPATTERN, C.LASTRECORDCOUNT, C.BADGEFILTER
  , C.ORDERINDEX, C.MAXIMIZED, C.OVERRULEPERMISSIONTYPE, C.PARAMETERS
  , '.DEFAULT_DB_HASH_FUNCTION.'(CONCAT(C.APPLICATIONID,"_",C.CONSTRUCTID)) AS PLR__RECORDID
	FROM plr_construct C
  LEFT JOIN plr_constructtree T
  ON C.clientid = T.clientid
  AND C.applicationid = T.applicationid
  AND C.constructid = T.constructid
  LEFT JOIN plr_form F
  ON C.clientid = F.clientid
  AND C.masterformid = F.formid
  LEFT JOIN plr_formpermission FP
  ON F.clientid = FP.clientid
  AND F.formid = FP.formid
  WHERE C.VISIBLE = \'Y\'
  AND T.SUPERCONSTRUCTID = ?
';

$_sqlConstructs = $_sqlConstructsBASE . '
    AND C.CLIENTID = ?
    AND C.APPLICATIONID = ?
    AND (FP.USERGROUPID in (#subselect#)
        OR
        (USERGROUPID IS NULL AND (CONSTRUCTTYPE = \'GROUP\' OR CONSTRUCTTYPE = \'SEPARATOR\')))
';

$_sqlConstructsADMIN = $_sqlConstructsBASE . '
    AND ' . $SpecClientHash . ' = ?
    AND C.APPLICATIONID = ?
';
//    AND (USERGROUPID IS NULL AND (CONSTRUCTTYPE = \'GROUP\' OR CONSTRUCTTYPE = \'SEPARATOR\'))

$_sqlQuickNavConstructs = '
  SELECT c.CONSTRUCTID, c.CONSTRUCTNAME, c.IMAGEURL
  FROM plr_quicknavigation q
  LEFT JOIN plr_construct c
  ON q.clientid = c.clientid
  AND q.applicationid = c.applicationid
  AND q.constructid = c.constructid
  WHERE q.clientid = ?
  AND q.applicationid = ?
  AND c.visible = \'Y\'
  AND q.usergroupid = ?
  ORDER BY frequency desc
  LIMIT 4
';

$_sqlApplicationAdvertorials = '
  SELECT a.adtext
  FROM plr_advertorial a
  WHERE a.clientid = ?
  AND a.applicationid = ?
  AND a.visible = \'Y\'
  ORDER BY orderindex
';

$_sqlConstructBASE = '
	SELECT C.CLIENTID, APPLICATIONID, CONSTRUCTID, CONSTRUCTNAME, CONSTRUCTTYPE, IMAGEURL, ORDERINDEX
	, INCLUDEINNEWMENU, NEWMENUITEMNAME, MASTERFORMID, DETAILFORMID, DASHBOARDID, PANELHELPTEXT
	, MASTERFILTER, DETAILFILTER, MASTERORDERBY, DETAILORDERBY, DESCRIPTION, MASTERFORMTYPE, DETAILFORMTYPE, SHOWSEARCHPANEL, WEBPAGELINK
	, SHOWDETAILASLINKS, PARAMETERS, METANAME, C.OVERRULEPERMISSIONTYPE
	, C.SHOWCOUNTBADGE, C.RECORDCOUNTER, C.BADGEUPDATEPATTERN, C.LASTRECORDCOUNT, C.BADGEFILTER, C.MAXIMIZED
	FROM plr_construct C
	LEFT JOIN plr_formpermission FP ON C.CLIENTID = FP.CLIENTID AND C.MASTERFORMID = FP.FORMID
	WHERE C.CLIENTID = ?
	AND APPLICATIONID = ?
	AND VISIBLE = "Y"
	AND PERMISSIONTYPE > 0
';

$_sqlConstruct = $_sqlConstructBASE . '
	AND CONSTRUCTID = ?
';

$_sqlConstructMeta = $_sqlConstructBASE . '
	AND METANAME = ?
';

$_sqlDashboard = '

  SELECT * FROM plr_dashboard
  WHERE clientid = ?
  AND applicationid = ?
  AND dashboardid = ?
';



$_sqlDashboardItems = '

 SELECT ID, TYPE, CLIENTID, USERGROUPID, TITLE, URL, APPLICATIONID, CONSTRUCTID
	, HEIGHT, WIDTH, TOP, BOTTOM, `LEFT`, `RIGHT`, VISIBLE, PERSISTENT, WINDOWMODE
  FROM plr_dashboard d
	WHERE d.clientid = ? AND d.usergroupid in (?)
    and id not in (SELECT d2.original from plr_dashboard d2
	               WHERE d2.clientid = d.clientid AND d2.usergroupid in (?)
	               AND d2.original is not null)
  ORDER BY WINDOWMODE ASC
';

$_sqlDashboardItem = '

 SELECT ID, TYPE, CLIENTID, USERGROUPID, TITLE, URL, APPLICATIONID, CONSTRUCTID
	, HEIGHT, WIDTH, TOP, BOTTOM, `LEFT`, `RIGHT`, VISIBLE, PERSISTENT
  FROM plr_dashboard d
	WHERE d.id = ? and d.clientid = ? AND d.usergroupid = ?
';

$_sqlCloneDashboardItem = '
INSERT INTO plr_dashboard (TYPE, CLIENTID, USERGROUPID, TITLE, URL, APPLICATIONID, CONSTRUCTID
	, TOP, BOTTOM, `LEFT`, `RIGHT`, HEIGHT, WIDTH, VISIBLE, PERSISTENT, ORIGINAL, WINDOWMODE)
SELECT TYPE, CLIENTID, ?, TITLE, URL, APPLICATIONID, CONSTRUCTID
	, ?, ?, ?, ?, ?, ?, VISIBLE, PERSISTENT, ID, WINDOWMODE FROM plr_dashboard WHERE ID = ?
';

$_sqlUpdateDashboardItem = '
	UPDATE plr_dashboard SET `top`=?, `bottom`=?, `left`=?, `right`=?, `width`=?, `height`=?
	WHERE id = ? and clientid = ? and usergroupid = ?
';

$_sqlDeleteDashboardItem = '

 DELETE FROM plr_dashboard
	WHERE id = ? and clientid = ? and usergroupid = ? AND persistent = \'N\'
';

$_sqlAddDashboardForm = '
	INSERT INTO plr_dashboard (type, clientid, usergroupid, title, url, applicationid, constructid, height, width, top, `left`, visible)
	VALUES (?,?,?,?,?,?,?,?,?,?,?,?)
';

$_sqlFormTable = '
	SELECT tablename, updatetablename
	FROM plr_form f
	WHERE F.CLIENTID = ?
	AND F.FORMID = ?
';

$_sqlFormBASE = '
  SELECT DISTINCT F.CLIENTID, F.FORMID, F.FORMNAME, F.METANAME, F.HELPNR, F.PAGECOLUMNCOUNT, F.WEBPAGEURL, F.DATABASEID

  , F.TABLENAME, F.UPDATETABLENAME, F.FILTER, F.MODULE, F.MODULEID, F.DEFAULTVIEWTYPE, F.VIEWTYPEPARAMS, F.SHOWINFRAME, F.ITEMNAME, F.WEBPAGENAME

  , F.WEBPAGECONSTRUCT, F.SHOWRECORDSASCONSTRUCTS, F.CAPTIONCOLUMNNAME, F.SUBCAPTIONCOLUMNNAME, F.SORTCOLUMNNAME, F.SEARCHCOLUMNNAME

  , F.PANELHELPTEXT, F.PRELOAD, F.HISTORYTYPE, F.SELECTVIEW, F.SELECTCOLUMNNAMES, F.SELECTSHOWCOLUMNS

  , F.SELECTQUERY, F.OPTIONS, F.SENTENCEPATTERN, BIT_OR(FP.permissiontype) AS PERMISSIONTYPE
  , F.REFRESHFORM, F.DEFAULTREFRESHINTERVAL, F.CSSIDENTIFIERCOLUMN, F.RSSCOLUMNS, F.RECORDLIMIT
  , F.ROWREADONLYONVALUE
  , T.ISVIEW, F.CUSTOMSCRIPTS
  , ' . $FormClientHash . ' as CLIENTHASH
  , ' . $FormHash . ' as RECORDID
  FROM (((plr_form F)
  LEFT JOIN plr_formpermission FP ON F.clientid = FP.clientid AND F.formid = FP.formid)
  LEFT JOIN plr_table T ON F.clientid = T.clientid AND F.databaseid = T.databaseid AND F.tablename = T.tablename)
  WHERE F.CLIENTID = ?
';

$_sqlFormWithoutUser = $_sqlFormBASE . '
	AND F.FORMID = ?
';

$_sqlForm = $_sqlFormBASE . '
    AND FP.USERGROUPID in (#subselect#)
	AND F.FORMID = ?
';

$_sqlFormMeta = $_sqlFormBASE . '
	AND F.METANAME = ?
';

$_sqlFormHashedBASE = '
	SELECT F.CLIENTID, F.FORMID, F.FORMNAME, F.METANAME, F.HELPNR, F.PAGECOLUMNCOUNT, F.WEBPAGEURL, F.DATABASEID

	, F.TABLENAME, F.UPDATETABLENAME, F.FILTER, F.MODULE, F.MODULEID, F.DEFAULTVIEWTYPE, F.VIEWTYPEPARAMS
	, F.SHOWINFRAME, F.ITEMNAME, F.WEBPAGENAME
	, F.WEBPAGECONSTRUCT, F.SHOWRECORDSASCONSTRUCTS, F.CAPTIONCOLUMNNAME, F.SUBCAPTIONCOLUMNNAME, F.SORTCOLUMNNAME, F.SEARCHCOLUMNNAME

	, F.PANELHELPTEXT, F.PRELOAD, F.HISTORYTYPE, F.SELECTVIEW, F.SELECTCOLUMNNAMES, F.SELECTSHOWCOLUMNS

	, F.SELECTQUERY, F.OPTIONS, F.SENTENCEPATTERN, F.REFRESHFORM, F.DEFAULTREFRESHINTERVAL, F.CSSIDENTIFIERCOLUMN
    , F.ROWREADONLYONVALUE
  , ' . $FormClientHash . ' as CLIENTHASH
  , ' . $FormHash . ' as RECORDID
  , T.ISVIEW, T.INSERTPROCEDURE, T.UPDATEPROCEDURE, T.DELETEPROCEDURE
    FROM plr_form F
    LEFT JOIN plr_table T ON F.clientid = T.clientid AND F.databaseid = T.databaseid AND F.tablename = T.tablename

';
$_sqlFormHashed = $_sqlFormHashedBASE . '
	WHERE ' . $FormHash . ' = ?
';

$_sqlPages = '
	SELECT CLIENTID, FORMID, PAGEID, PAGENAME, DETAILFORM, PAGETYPE, PAGECOLUMNCOUNT,	IMAGEINDEX, ACTIONID, JASPERREPORTNAME, JASPERREPORTPARAMS
	FROM plr_page
	WHERE CLIENTID = ?
	AND FORMID = ?
    ORDER BY ORDERINDEX ASC
';

$_sqlActions_BASE = '
	SELECT ACTIONNAME, ACTIONURL, MODULE, MODULEPARAM, CUSTOMPARAMFORM, EVENTS, BUTTONSTYLE, STOREDPROCEDURE, PARAM1, PARAM2, PARAM3, PARAM4, PARAM5
	, CLIENTID, DATABASEID, TITLE, TEXT, OKMESSAGE, REFRESH, SELECTEDROWS, JASPERREPORTNAME, JASPERREPORTPARAMS, BEFOREPRINT, AFTERPRINT, USERGROUPID
	, SHOWINCONSTRUCTS, SHOWINFORMVIEW, `GROUP`, BUTTONICON
	,' . $ActionHash . ' AS RECORDID
	FROM plr_action
';

$_sqlAction = $_sqlActions_BASE . '
	WHERE ' . $ActionHash . ' = ?
';

$_sqlActions = $_sqlActions_BASE . '
	WHERE CLIENTID = ?
	AND FORMID = ?
	AND (USERGROUPID IS NULL OR USERGROUPID IN (#subselect#))
	ORDER BY ORDERINDEX
';

$_sqlFormTags_BASE = '
    SELECT CLIENTID, FORMID, TAGID, METANAME, TAGNAME, FILTER, GROUPID, COLOR, ACTIVE, DEFAULTTAG
    FROM plr_formtag
';

$_sqlFormTag = $_sqlFormTags_BASE . '
    WHERE ' . $FormTagHash . ' = ?
';

$_sqlFormTags = $_sqlFormTags_BASE . '
    WHERE CLIENTID = ?
    AND FORMID = ?
    ORDER BY TAGID
';

$_sqlGroupLines = '
	SELECT L.COLUMNNAME, L.LINEDESCRIPTION, L.GROUPSOURCE, L.LINEDEFAULTVALUE
	FROM plr_line L
	LEFT JOIN plr_column C
	ON L.CLIENTID= C.CLIENTID
	AND L.DATABASEID = C.DATABASEID
	AND L.TABLENAME = C.TABLENAME
	AND L.COLUMNNAME = C.COLUMNNAME
	WHERE isgroupfield = "Y"
	AND L.CLIENTID = ?
	AND FORMID = ?
	ORDER BY L.POSITIONINDEX
';

$_sqlLines = '
	SELECT L.CLIENTID, L.DATABASEID , L.TABLENAME , L.COLUMNNAME , KEYCOLUMN , DATATYPE , TOTALLENGTH

	, DECIMALCOUNT , SIGNED , COLUMNCHECK , FORMULA, MASK, REQUIRED , FULLNAME , DESCRIPTION, GROUPID, COLUMNGROUPINDEX

	, SUPERSHOWCOLUMNNAME , LISTVALUES , SUPERSHOWBOTH , DEFAULTVALUE , POSITION

	, OPTIONGROUP , COLUMNVISIBLE , DEFAULTCOLUMN, FORMID, PAGEID, LINEID, LINEDESCRIPTION
	, LINEDESCRIPTION_SHORT, HELPNR, LINETYPE, READONLY, READONLYONVALUE, LISTVALUES, BINARYVALUES
	, LINEDEFAULTVALUE, LINEREQUIRED, FORMAT, FORMATPARAMS, UNIT, IMAGEDIR, IMAGEURL

	, POSITIONTYPE, XPOSITION, YPOSITION, VISIBLE, VISIBLEINLIST, POSITIONINDEX

	, PREVIEWURL, ISSELECTFIELD, SELECTSOURCE, ISGROUPFIELD, GROUPSOURCE, EVENTS, CSSIDENTIFIER
	, UPDATECOLUMN, CUTOFFTHRESHOLD, CHECKBOXGROUP, FORCEDEFAULTONUPDATE, SUPERCOLUMNNAME, S.SUBSETID
  , S.LINKEDFORM, S.SUPERFORM, S.CASCADEFORM, S.CASCADEPARENTFIELD
	, AUTOFOCUS
	FROM plr_line L
	LEFT JOIN plr_column C
	ON L.CLIENTID= C.CLIENTID
	AND L.DATABASEID = C.DATABASEID
	AND L.TABLENAME = C.TABLENAME
	AND L.COLUMNNAME = C.COLUMNNAME
	LEFT JOIN plr_subsetitem SI
	ON C.CLIENTID= SI.CLIENTID
	AND C.DATABASEID = SI.DATABASEID
	AND C.TABLENAME = SI.TABLENAME
	AND C.COLUMNNAME = SI.COLUMNNAME
	LEFT JOIN plr_subset S
	ON SI.CLIENTID= S.CLIENTID
	AND SI.DATABASEID = S.DATABASEID
	AND SI.SUBSETID = S.SUBSETID
	WHERE L.CLIENTID = ?
	AND FORMID = ?
	AND PAGEID = ?
/*	AND (S.USEINPINUP = \'N\' OR S.USEINPINUP IS NULL) */
    GROUP BY FORMID, PAGEID, LINEID
    ORDER BY L.COLUMNGROUPINDEX, L.POSITIONINDEX
';

$_sqlDatabaseBase = '
	SELECT D.CLIENTID, DATABASEID, DESCRIPTION, HOST, DATABASETYPE, DATABASENAME, ROOTUSERNAME, ROOTPASSWORD

	, CONVERT(AES_DECRYPT(D.ROOTPASSWORD, "' . $polarisscramble . '"), CHAR(110)) as DECROOTPASSWORD, USEDBASPREFIX
  , ' . $DatabaseHash . ' as RECORDID
  , CONCAT(DESCRIPTION, \' (\', ROOTUSERNAME, \'@\', HOST, \':\', DATABASENAME, \')\') AS DB_DESC
  FROM plr_database D
';

$_sqlDatabases = $_sqlDatabaseBase;

$_sqlDatabase = $_sqlDatabaseBase . '
	WHERE D.CLIENTID = ?
	AND D.DATABASEID = ?
';

$_sqlDatabaseHashed = $_sqlDatabaseBase . '
	WHERE ' . $DatabaseHash . ' = ?
';

$_sqlClientDatabases = $_sqlDatabaseBase . '
  WHERE ' . $ClientHash . ' = ?
';

$_sqlTable = '
    SELECT CLIENTID,DATABASEID,TABLENAME,PRELOAD,INSERTPROCEDURE,UPDATEPROCEDURE,DELETEPROCEDURE
    ,SEQUENCE,SEQUENCECOLUMNNAME,SENTENCEPATTERN,USEINKK,ISVIEW
    FROM plr_table
    WHERE clientid = ? AND databaseid = ?
    AND tablename = ?
';

$_sqlTables = '
	SELECT *
	FROM plr_table
	WHERE ' . $DatabaseHash . ' = ?
	AND useinkk = \'Y\'
';

$_sqlColumnsBase = '
	SELECT c.*, i.SUPERTABLENAME, i.SUPERCOLUMNNAME, i.SUBSETID
	FROM plr_column c
	LEFT JOIN plr_subsetitem i
	ON c.clientid = i.clientid
	AND c.databaseid = i.databaseid
	AND c.tablename = i.tablename
	AND c.columnname = i.columnname
	WHERE ' . $DatabaseHash_column . ' = ?
	AND c.tablename = ?
';

$_sqlColumns = $_sqlColumnsBase . '
	ORDER BY POSITION
';

$_sqlModelerColumns = $_sqlColumnsBase . '
	ORDER BY KEYCOLUMN DESC, POSITION
';

$_sqlKeyColumns = $_sqlColumnsBase . '
  AND keycolumn = \'Y\'
  ORDER BY POSITION
';

$_sqlSubsetItemsBase = '
  select s.tablename, s.subsetid, s.filter, si.columnname, s.jointype
  , si.supercolumnname, si.supertablename, s.distinctselect
  , c.supershowcolumnname, s.showdropdownbutton, s.cascadeform, s.cascadeparentfield
  , c.datatype, c.formula
  from plr_subset s
  left join plr_subsetitem si
  on s.clientid = si.clientid
  and s.databaseid = si.databaseid
  and s.subsetid = si.subsetid
  left join plr_column c
  on s.clientid = c.clientid
  and s.databaseid = c.databaseid
  and s.tablename = c.tablename
  and si.columnname = c.columnname
  where s.distinctselect = "N"
  and si.clientid = ?
  and si.databaseid = ?
  and si.tablename = ?
';

$_sqlSubsetItems = $_sqlSubsetItemsBase . '
  and UPPER(si.columnname) = UPPER(?)
';

$_sqlSubsetItemsNormal = $_sqlSubsetItemsBase . '
  and ((upper(si.supercolumnname) <> supershowcolumnname and supershowcolumnname <> \'\') OR s.useinpinup = \'Y\')
  order by s.subsetid, si.orderindex
';

$_sqlSubsetItemsJoins = $_sqlSubsetItemsBase . '
  and si.supertablename = ?
  and (s.useinpinup = ? OR s.useinpinup = \'N\')
  order by s.subsetid, si.orderindex
';

$_sqlAllSearchColumns = '
  SELECT l.tablename, l.columnname, l.linedescription
  FROM plr_line l
  WHERE l.clientid = ?
  AND l.formid = ?
';

$_sqlNavigationFreq = '
	SELECT frequency
	FROM plr_quicknavigation
	WHERE clientid = ?
	AND applicationid = ?
	AND constructid = ?
	AND usergroupid = ?
';

$_sqlUpdateNavFreq = '
	UPDATE plr_quicknavigation
	SET frequency = frequency + 1
	WHERE clientid = ?
	AND applicationid = ?
	AND constructid = ?
	AND usergroupid = ?
';

$_sqlNewManualItems = '
	SELECT *
	FROM manual
	WHERE includeinpolaris = "Y"
	AND language = ?
	ORDER BY last_updated DESC
';

$_sqlCountryNames = '
  SELECT LocalName
	FROM plr_country
	WHERE active = \'Y\'
';

$_sqlUserWebPagesBASE = '
  SELECT p.*, \'db\' as type, ' . $WebPageHash . ' as recordid FROM web_page p
';

$_sqlUserWebPages = $_sqlUserWebPagesBASE . '
  WHERE editable = "Y"
  ORDER BY pagename, language
';

$_sqlUserWebPage = $_sqlUserWebPagesBASE . '
  WHERE ' . $WebPageHash . ' = ?
';

$_sqlUserWebPageLanguages = $_sqlUserWebPagesBASE . '
  WHERE pagename = ?
';

$_sqlWebLanguagesBASE = '
  SELECT * FROM web_language
';

$_sqlWebLanguages = $_sqlWebLanguagesBASE . '
  WHERE selected = "Y"
';

$_sqlUnusedWebLanguages = $_sqlWebLanguages . '
  AND languagecode not in
';

$_sqlUserWebParams = '
  SELECT p.* , ' . $WebParamHash . ' as recordid
  FROM web_parameter p
  WHERE userparam = "Y"
  ORDER BY paramname, language
';

$_sqlWebMenuBASE = '
  SELECT menuid, caption_rsc_, url, orderindex, m.pagename, seokeywords_nl, seokeywords_de
  , app, construct
  , target, visible, parentmenuid, r.language, text as menuname
  , ' . $WebMenuHash . ' as recordid
  , ' . $WebPageHash . ' as page_recordid
  , p.lastedited as page_lastedited
  FROM ((web_menu m
  LEFT JOIN web_page p
  ON m.pagename = p.pagename)
  LEFT JOIN web_resource r
  ON m.caption_rsc_ = r.resourceid)
';

$_sqlWebMenuGroups = $_sqlWebMenuBASE . '
  WHERE menuid < 0
  GROUP BY r.resourceid
  ORDER BY menuid desc
';

$_sqlWebMenu = $_sqlWebMenuBASE . '
  WHERE parentmenuid = ?
  AND r.language = ?
  GROUP BY r.resourceid
  ORDER BY orderindex
';

$_sqlUnlinkedPages = '
  SELECT pagename as menuname, pagename
  , ' . $WebPageHash . ' as page_recordid
  , lastedited as page_lastedited
  FROM (web_page p)
  WHERE p.pagename NOT IN (SELECT pagename FROM web_menu)

  AND language = ?
  AND editable = "Y"
';

$_sqlWebMenuItem = $_sqlWebMenuBASE . '
  WHERE ' . $WebMenuHash . ' = ?
';
//  GROUP BY r.resourceid

$_sqlWebMenuItemsWithParent = $_sqlWebMenuBASE . '
  WHERE ' . $WebParentMenuHash . ' = ?
  GROUP BY r.resourceid
';


$_sqlLastWebMenuItem = '
	SELECT menuid, orderindex
	FROM web_menu
	WHERE parentmenuid = ?
  ORDER BY orderindex desc
  LIMIT 0,1
';

$_sqlMaxWebMenuItem = '
	SELECT max(menuid) as maxmenuid
	FROM web_menu
';

$_sqlMinWebMenuItem = '
	SELECT min(menuid) as minmenuid
	FROM web_menu
	WHERE menuid <= 0
';

$_sqlMenuItemsOrderIndexUpdate = '
	SELECT menuid, orderindex
	FROM web_menu
	WHERE parentmenuid = ?
	AND orderindex >= ?
';

$_sqlInsertMenuItem = '
  INSERT INTO web_menu (menuid, caption_rsc_, orderindex, visible, parentmenuid, pagename)
	VALUES (?, ?, ?, ?, ?, ?)
';

$_sqlDeleteResources = '
	DELETE FROM web_resource
	WHERE resourceid IN (?)
';

$_sqlInsertNewResource = '
  INSERT INTO web_resource (language, text)
	VALUES (?, ?)
';

$_sqlInsertResource = '
  INSERT INTO web_resource (resourceid, language, text)
	VALUES (?, ?, ?)
';

$_sqlDeleteResource = '
	DELETE FROM web_resource WHERE resourceid = ?
';

$_sqlAllUserWebPages = '
  SELECT pageid, pagename, editable, language
  FROM web_page
';

$_sqlBulkMail = '
  SELECT * FROM plr_bulkmail
  WHERE ' . $AppHash_clean . ' = ?
  AND bulkmailid = ?
';

$_sqlUpdateBulkMail = '
  UPDATE plr_bulkmail
  SET fromaddress = ?, subject = ?
  WHERE ' . $AppHash_clean . ' = ?
  AND bulkmailid = ?
';

$_sqlUpdateBulkMailText = '
  UPDATE plr_bulkmail
  SET bodytext = ?
  WHERE ' . $AppHash_clean . ' = ?
  AND bulkmailid = ?
';

$_sqlCheckMaintenance = '
  SELECT m.startdate, date_format(m.starttime, \'%H.%i\') as starttime, m.status, m.remark
  , IF(
  	now() <= ADDTIME(m.startdate, m.starttime)
  	, m.duration * 60
  	, m.duration * 60 - TIME_TO_SEC(TIMEDIFF(curtime(), m.starttime))
  ) duration
  , m.announcetime
  , curtime()
  , TIME_TO_SEC(timediff(m.starttime, curtime())) secondstostart
  from plr_maintenance m
  where startdate = curdate()
  and now() <= DATE_ADD(ADDTIME(m.startdate, m.starttime), INTERVAL m.duration MINUTE)
  -- and curtime() >= SEC_TO_TIME(TIME_TO_SEC(m.starttime) - m.announcetime * 60)
  and m.active = \'Y\'
';

$_sqlLiveSearchConstructsBASE = '
	SELECT distinct A.constructid, A.constructname, A.imageurl, A.constructtype
	, f.panelhelptext, f.formid
	FROM plr_construct A
  LEFT JOIN plr_constructtree t
  ON A.clientid = t.clientid
  AND A.applicationid = t.applicationid
  AND A.constructid = t.constructid
  LEFT JOIN plr_form f
  ON A.clientid = f.clientid
  AND A.applicationid = f.applicationid
  AND A.masterformid = f.formid
  LEFT JOIN plr_formpermission fp
  ON f.clientid = fp.clientid
  AND f.formid = fp.formid
  WHERE ' . $AppHash . ' = ?
  AND fp.USERGROUPID in (#subselect#)
  AND VISIBLE = \'Y\'
';

$_sqlAllConstructs = $_sqlLiveSearchConstructsBASE . '
';

$_sqlLiveSearchConstructs = $_sqlLiveSearchConstructsBASE . '
  AND (constructname LIKE ?
    OR f.panelhelptext LIKE ?)
';

$_sqlAllApplicationConstructs = '
	SELECT distinct a.clientid, a.applicationid, applicationname, c.constructid, constructname
	, a.metaname as appmetaname, c.metaname as constmetaname
	FROM plr_application a
	LEFT JOIN plr_construct c
  ON c.clientid = a.clientid
  AND c.applicationid = a.applicationid
  LEFT JOIN plr_constructtree t
  ON c.clientid = t.clientid
  AND c.applicationid = t.applicationid
  AND c.constructid = t.constructid
  LEFT JOIN plr_form f
  ON c.clientid = f.clientid
  AND c.masterformid = f.formid
  LEFT JOIN plr_formpermission fp
  ON f.clientid = fp.clientid
  AND f.formid = fp.formid
  WHERE fp.USERGROUPID in (?)
  AND VISIBLE = \'Y\'
';

$_sqlAllTagsBASE = '
    SELECT tagid, app, const, form, record, tag, recordcaption
    , ' . $TagHash . ' as recordid
    FROM plr_tag
    WHERE clientid=? AND userid=? AND app=? AND const=? AND form=?
';
$_sqlAllRecordTags = $_sqlAllTagsBASE . '
    AND record=?
';
$_sqlAllSearchResultTags = $_sqlAllTagsBASE . '
    AND search=?
';
$_sqlDeleteTag = '
    DELETE FROM plr_tag WHERE clientid=? AND userid=? AND ' . $TagHash . ' = ?
';

$_sqlDataScopeForTable = '
    SELECT columnname, operand, filtervalue, applicationid, constructid
    FROM plr_datascope
    WHERE clientid=?
    AND (usergroupid in (?) or usergroupid = 0)
    AND (databaseid = ?)
    AND (tablename = ? OR tablename = "*")
    AND (USEFORSTORAGE = ? OR USEFORSTORAGE = "Y")
';

$_sqlDropOtherSessions = '
    DELETE FROM plr_session
    WHERE EXPIREREF <> ? AND DATA LIKE ?
';

$_sqlFactTypes = '
    SELECT FACTTYPEID, FACTTYPENAME, SENTENCEPATTERN, COMMENT, METHODNAME, TRIGGERTYPE, TRIGGEREVENT, DESCRIPTION
    FROM mod_facttype f
LEFT JOIN mod_method m
ON f.FACTTYPENAME = m.TRIGGERTABLE
	WHERE ' . $DomainHash . ' = ?
';



$_sqlFactTypesTriggers = '
    SELECT FACTTYPEID, FACTTYPENAME, SENTENCEPATTERN, COMMENT, METHODNAME, TRIGGERTYPE, TRIGGEREVENT, DESCRIPTION
    , CASE TRIGGERTYPE WHEN "BEFORE EACH ROW" THEN 1 WHEN "BEFORE EACH STATEMENT" THEN 2 WHEN "AFTER EACH ROW" THEN 3 WHEN "AFTER EACH STATEMENT" THEN 4 ELSE 5 END AS ORDERINDEX
    FROM mod_facttype f
LEFT JOIN mod_method m
ON f.FACTTYPENAME = m.TRIGGERTABLE
	WHERE ' . $DomainHash . ' = ?
	AND f.FACTTYPENAME = ?


	ORDER BY FACTTYPENAME, ORDERINDEX
';



$_sqlFactTypesUi = '
    SELECT UIID, NAME, FACTTYPE, ' . $UIHash . ' AS PLR__RECORDID
    FROM mod_facttype f
LEFT JOIN mod_ui u
ON f.FACTTYPENAME = u.FACTTYPE
	WHERE ' . $DomainHash . ' = ?
	AND f.FACTTYPENAME = ?


';



/*
$_sqlPlaceHoldersBase = '

	SELECT c.*, i.SUPERTABLENAME, i.SUPERCOLUMNNAME, i.SUBSETID
	FROM plr_column c
	LEFT JOIN plr_subsetitem i
	ON c.clientid = i.clientid
	AND c.databaseid = i.databaseid
	AND c.tablename = i.tablename
	AND c.columnname = i.columnname
	WHERE '.$DomainHash_ph.' = ?

	AND c.tablename = ?
';


*/

$_sqlPlaceHoldersBase = '

	SELECT ph.*
	FROM mod_facttype_placeholder ph
	WHERE ' . $DomainHash_ph . ' = ?

	AND ph.FACTTYPENAME = ?
';



$_sqlPlaceHolders = $_sqlPlaceHoldersBase . '

	ORDER BY POSITION

';



$_sqlModelerPlaceHolders = $_sqlPlaceHoldersBase . '

	ORDER BY KEYCOLUMN DESC, POSITION

';

$_sqlKeyPlaceHolders = $_sqlPlaceHoldersBase . '
  AND KEYCOLUMN = \'Y\'

  ORDER BY POSITION
';

$_sqlSearchColumns = "
    SELECT s.clientid, s.formid, s.databaseid, s.tablename, s.columnname, s.supersearchcolumn, s.position, s.processing, c.mask
    , c.datatype, c.totallength, c.decimalcount, c.signed, c.fullname, c.supershowcolumnname
    , c.listvalues, c.searchvalueprocessor, l.linedescription, l.linedescription_short, s.searchquery
    FROM plr_search s
    LEFT JOIN plr_column c
    ON s.clientid = c.clientid AND s.databaseid = c.databaseid AND s.tablename = c.tablename AND s.columnname = c.columnname
    LEFT JOIN plr_line l
    ON s.clientid = l.clientid AND s.formid = l.formid AND s.tablename = l.tablename AND s.columnname = l.columnname
    WHERE c.columnvisible = 'Y' AND s.clientid = ? AND s.formid = ?
    ORDER BY s.position
";

$_sqlLinkedForm = "
    select distinct si.columnname, si.tablename, si.supertablename, si.supercolumnname
    , s.linkedform, f.metaname, f.formname
    from plr_subsetitem si
    left join plr_subset s on si.clientid = s.clientid and si.databaseid = s.databaseid and si.subsetid = s.subsetid
    left join plr_form f on s.clientid = f.clientid and s.databaseid = f.databaseid AND s.linkedform = f.formid
    where si.clientid = ?
    and si.databaseid = ?
    and si.supertablename = ?
    and s.linkedform IS NOT NULL
    and f.metaname IS NOT NULL
    group by s.linkedform
    order by s.orderindex
";

$_sqlLinkedFormNew = "
    select distinct si.columnname, si.tablename, si.supertablename, si.supercolumnname
    , s.linkedform, c.metaname, f.formname, c.imageurl, c.constructname
    from plr_subsetitem si
    left join plr_subset s on si.clientid = s.clientid and si.databaseid = s.databaseid and si.subsetid = s.subsetid
    left join plr_form f on s.clientid = f.clientid and s.databaseid = f.databaseid AND s.linkedform = f.formid
    left join plr_construct c on s.clientid = c.clientid and f.formid = c.masterformid
    where si.clientid = ?
    and si.databaseid = ?
    and si.supertablename = ?
    and s.linkedform IS NOT NULL
    and c.metaname IS NOT NULL
    group by s.linkedform
    order by s.orderindex
";

$_sqlUserSettings = "
    SELECT SETTING, VALUE
    FROM plr_usersetting
    WHERE clientid = ? AND applicationid = ? AND usergroupid = ?
";

$_sqlRecordCountCons = "
	SELECT distinct C.CLIENTID, C.APPLICATIONID, C.CONSTRUCTID, C.CONSTRUCTNAME, C.MASTERFORMID, C.MASTERFILTER
	, C.SHOWCOUNTBADGE, C.RECORDCOUNTER, C.BADGEUPDATEPATTERN, C.LASTRECORDCOUNT, C.BADGEFILTER
	FROM plr_construct C
    LEFT JOIN plr_form F
    ON C.clientid = F.clientid
    AND C.masterformid = F.formid
    WHERE C.SHOWCOUNTBADGE = 'Y' AND DATABASEID > 0
";

$_sqlUpdateRecordCountConstruct = "
    UPDATE plr_construct SET RECORDCOUNTER = ?, LASTRECORDCOUNT = now()
    WHERE clientid = ? AND applicationid = ? AND constructid = ?
";

$_sqlDSHContainers = $_sqlRecordCountCons . "
    AND C.APPLICATIONID = ?
    ORDER BY CONSTRUCTID
";

$_sqlInsertSaveSearch = "
    INSERT INTO plr_searchhistory (`CLIENTID`, `USERGROUPID`, `FORMID`, `SEARCHID`, `DATABASEID`, `SEARCHNAME`, `SEARCHQUERY`)
    VALUES (?,?,?,?,?,?,?)
";

$_sqlMaxIDSaveSearch = "
    SELECT IFNULL(MAX(SEARCHID), 1) + 1 FROM plr_searchhistory WHERE `CLIENTID` = ? AND `USERGROUPID` = ? AND `FORMID` = ?
";

$_sqlSearchHistory = "
    SELECT * FROM plr_searchhistory WHERE `CLIENTID` = ? AND `USERGROUPID` = ? AND `FORMID` = ?
";


// Database error codes

define('DB_ERROR_DUPLICATE_KEY', 512);
