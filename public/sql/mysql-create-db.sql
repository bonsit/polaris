# MySQL-Front Dump 2.1
#
# Host: 192.168.1.12   Database: polaris
#--------------------------------------------------------
# Server version 3.23.52-nt


#
# Table structure for table 'plr_action'
#

CREATE TABLE `plr_action` (
  `CLIENTID` int(5) NOT NULL default '0',
  `ACTIONID` int(5) NOT NULL default '0',
  `ACTIONURL` varchar(150) default NULL,
  `DATABASEID` int(5) default NULL,
  `STOREDPROCEDURE` text,
  `TITLE` varchar(50) default NULL,
  `TEXT` varchar(150) default NULL,
  `OKMESSAGE` text,
  `STARTMESSAGEIMAGEINDEX` int(5) default NULL,
  `STARTMESSAGE` text,
  `REFRESH` char(1) default 'R',
  PRIMARY KEY  (`CLIENTID`,`ACTIONID`)
) TYPE=MyISAM;



#
# Table structure for table 'plr_alert'
#

CREATE TABLE `plr_alert` (
  `CLIENTID` int(5) NOT NULL default '0',
  `APPLICATIONID` varchar(50) NOT NULL default '',
  `ALERTID` int(5) NOT NULL default '0',
  `IMAGEINDEX` int(5) NOT NULL default '0',
  `ORDERINDEX` int(5) NOT NULL default '0',
  `VISIBLE` char(1) NOT NULL default '',
  `FORMID` int(5) default NULL,
  `FILTER` varchar(255) default NULL,
  PRIMARY KEY  (`CLIENTID`,`APPLICATIONID`,`ALERTID`)
) TYPE=MyISAM;



#
# Table structure for table 'plr_application'
#

CREATE TABLE `plr_application` (
  `CLIENTID` int(5) NOT NULL default '0',
  `APPLICATIONID` int(5) NOT NULL auto_increment,
  `APPLICATIONNAME` varchar(50) NOT NULL default '',
  `DESCRIPTION` varchar(255) NOT NULL default '',
  `ORDERINDEX` int(5) NOT NULL default '0',
  `APPLICATIONPICTURE` varchar(100) default NULL,
  `VERSION` varchar(10) NOT NULL default '',
  `REGISTERED` varchar(35) NOT NULL default '',
  `IMAGE` varchar(255) default NULL,
  `STARTUPMESSAGE` varchar(255) default NULL,
  `STARTUPMESSAGEFORMCOLOR` int(11) default '16744994',
  `HELPNR` int(7) default NULL,
  `HELPFILE` varchar(25) default NULL,
  `SEARCHOPTIONS` varchar(5) default NULL,
  `WEBSITE` varchar(200) default NULL,
  `FTPSERVER` varchar(255) default NULL,
  `FTPUSER` varchar(100) default NULL,
  `FTPPASSWORD` varchar(50) default NULL,
  PRIMARY KEY  (`CLIENTID`,`APPLICATIONID`)
) TYPE=MyISAM;



#
# Table structure for table 'plr_applicationlogo'
#

CREATE TABLE `plr_applicationlogo` (
  `CLIENTID` int(5) NOT NULL default '0',
  `APPLICATIONID` varchar(50) NOT NULL default '',
  `APPLICATIONLOGO` varchar(100) default NULL,
  PRIMARY KEY  (`CLIENTID`,`APPLICATIONID`)
) TYPE=MyISAM;



#
# Table structure for table 'plr_applicationpermission'
#

CREATE TABLE `plr_applicationpermission` (
  `CLIENTID` int(5) NOT NULL default '0',
  `APPLICATIONID` varchar(50) NOT NULL default '',
  `USERGROUPID` int(5) NOT NULL default '0',
  `PERMISSIONTYPE` int(2) NOT NULL default '0',
  `DEFAULTAPPLICATION` char(1) NOT NULL default 'N',
  PRIMARY KEY  (`CLIENTID`,`APPLICATIONID`,`USERGROUPID`)
) TYPE=MyISAM;



#
# Table structure for table 'plr_bugs'
#

CREATE TABLE `plr_bugs` (
  `bugid` int(4) NOT NULL auto_increment,
  `date_entered` date NOT NULL default '0000-00-00',
  `type` enum('UI','Functionality','Typo','Performance','Design','Security') NOT NULL default 'UI',
  `description` text NOT NULL,
  `environment` enum('Production','Test','Development') NOT NULL default 'Production',
  `priority` enum('Must fix','Fix ASAP','Enhancement') NOT NULL default 'Must fix',
  `date_fixed` date NOT NULL default '0000-00-00',
  `solution` enum('Fix in current design','Fix in new design','Fix in next version') NOT NULL default 'Fix in current design',
  `comments` text NOT NULL,
  `QA_resolution` enum('Fixed','Closed','Not fixed/No info','Not fixed/New info','Fixed/New bugs found') NOT NULL default 'Fixed',
  PRIMARY KEY  (`bugid`)
) TYPE=MyISAM;



#
# Table structure for table 'plr_client'
#

CREATE TABLE `plr_client` (
  `CLIENTID` int(5) unsigned NOT NULL auto_increment,
  `NAME` varchar(30) NOT NULL default '',
  `ADDRESS` varchar(30) default NULL,
  `ZIPCODE` varchar(6) default NULL,
  `CITY` varchar(30) default NULL,
  `TELEPHONE` varchar(12) default NULL,
  `FAX` varchar(12) default NULL,
  `POSTADDRESS` varchar(30) default NULL,
  `POSTZIPCODE` varchar(6) default NULL,
  `POSTCITY` varchar(30) default NULL,
  `PASSWORD` varchar(10) default NULL,
  `COUNTRY` char(3) NOT NULL default 'NL',
  `DEFAULTLANGUAGE` char(2) NOT NULL default 'NL',
  PRIMARY KEY  (`CLIENTID`),
  UNIQUE KEY `CLIENTID` (`CLIENTID`)
) TYPE=MyISAM;



#
# Table structure for table 'plr_column'
#

CREATE TABLE `plr_column` (
  `DATABASEID` int(5) NOT NULL default '0',
  `TABLENAME` varchar(100) NOT NULL default '',
  `COLUMNNAME` varchar(50) NOT NULL default '',
  `KEYCOLUMN` char(1) NOT NULL default 'N',
  `DATATYPE` char(1) NOT NULL default '',
  `TOTALLENGTH` int(5) NOT NULL default '0',
  `DECIMALCOUNT` int(1) default NULL,
  `COLUMNCHECK` varchar(50) default NULL,
  `FORMULA` varchar(255) default NULL,
  `REQUIRED` char(1) NOT NULL default 'N',
  `FULLNAME` varchar(50) NOT NULL default '',
  `DESCRIPTION` varchar(255) default NULL,
  `SUPERSHOWCOLUMNNAME` varchar(50) default NULL,
  `LISTVALUES` varchar(255) default NULL,
  `SUPERSHOWBOTH` char(1) default NULL,
  `DEFAULTVALUE` varchar(64) default NULL,
  `POSITION` int(4) default NULL,
  `OPTIONGROUP` char(1) NOT NULL default 'N',
  `COLUMNVISIBLE` char(1) NOT NULL default 'Y',
  `DEFAULTCOLUMN` char(1) NOT NULL default 'N',
  PRIMARY KEY  (`DATABASEID`,`TABLENAME`,`COLUMNNAME`)
) TYPE=MyISAM;



#
# Table structure for table 'plr_construct'
#

CREATE TABLE `plr_construct` (
  `CLIENTID` int(5) NOT NULL default '0',
  `APPLICATIONID` varchar(50) NOT NULL default '',
  `CONSTRUCTID` int(5) NOT NULL default '0',
  `CONSTRUCTNAME` varchar(50) NOT NULL default '',
  `CONSTRUCTTYPE` varchar(15) NOT NULL default '',
  `IMAGEURL` varchar(255) default '0',
  `ORDERINDEX` int(5) NOT NULL default '0',
  `INCLUDEINNEWMENU` char(1) NOT NULL default '',
  `NEWMENUITEMNAME` varchar(40) default NULL,
  `VISIBLE` char(1) NOT NULL default 'Y',
  `MASTERFORMID` int(5) default NULL,
  `DETAILFORMID` int(5) default '0',
  `MASTERFILTER` varchar(255) default NULL,
  `DETAILFILTER` varchar(255) default NULL,
  `DESCRIPTION` text,
  `MASTERFORMTYPE` varchar(10) default 'LISTVIEW',
  `DETAILFORMTYPE` varchar(10) default 'LISTVIEW',
  `SHOWSEARCHPANEL` char(1) NOT NULL default 'N',
  `WEBPAGELINK` varchar(200) default NULL,
  PRIMARY KEY  (`CLIENTID`,`APPLICATIONID`,`CONSTRUCTID`)
) TYPE=MyISAM;



#
# Table structure for table 'plr_constructtree'
#

CREATE TABLE `plr_constructtree` (
  `CLIENTID` int(5) NOT NULL default '0',
  `APPLICATIONID` varchar(50) NOT NULL default '',
  `CONSTRUCTID` int(5) NOT NULL default '0',
  `SUPERCONSTRUCTID` int(5) NOT NULL default '0',
  PRIMARY KEY  (`CLIENTID`,`APPLICATIONID`,`CONSTRUCTID`,`SUPERCONSTRUCTID`)
) TYPE=MyISAM;



#
# Table structure for table 'plr_database'
#

CREATE TABLE `plr_database` (
  `DATABASEID` int(5) NOT NULL auto_increment,
  `DESCRIPTION` varchar(50) NOT NULL default '',
  `HOST` varchar(50) NOT NULL default '',
  `DATABASETYPE` enum('access','ado','ado_access','ado_mssql','db2','vfp','fbsql','ibase','firebird','borland_ibase','informix72','informix','mssql','mssqlpo','mysql','mysqlt','oci8','oci805','oci8po','odbc','odbc_mssql','odbc_oracle','oracle','postgres','postgres64','postgres7','sqlanywhere','sybase','ldap') NOT NULL default 'mysql',
  `DATABASENAME` varchar(50) NOT NULL default '',
  `ROOTUSERNAME` varchar(20) NOT NULL default '',
  `ROOTPASSWORD` varchar(20) NOT NULL default '',
  `ERRORTABLENAME` varchar(100) default NULL,
  `LOGINWITHUSERACCOUNT` char(1) NOT NULL default '',
  `ERRORCODECOLUMNNAME` varchar(50) default NULL,
  `ERRORMESSAGECOLUMNNAME` varchar(50) default NULL,
  PRIMARY KEY  (`DATABASEID`),
  UNIQUE KEY `DESCRIPTION` (`DESCRIPTION`)
) TYPE=MyISAM;



#
# Table structure for table 'plr_databasetype'
#

CREATE TABLE `plr_databasetype` (
  `DATABASETYPE` varchar(20) NOT NULL default '',
  `DESCRIPTION` varchar(250) default NULL,
  PRIMARY KEY  (`DATABASETYPE`)
) TYPE=MyISAM;



#
# Table structure for table 'plr_errormessages'
#

CREATE TABLE `plr_errormessages` (
  `ERRORCODE` varchar(32) NOT NULL default '',
  `ERROROMSCHRIJVING` varchar(255) NOT NULL default ''
) TYPE=MyISAM;



#
# Table structure for table 'plr_form'
#

CREATE TABLE `plr_form` (
  `CLIENTID` int(5) NOT NULL default '0',
  `APPLICATIONID` int(5) NOT NULL default '0',
  `FORMID` int(5) NOT NULL default '0',
  `FORMNAME` varchar(50) NOT NULL default '',
  `HELPNR` int(5) default NULL,
  `PAGECOLUMNCOUNT` int(1) NOT NULL default '0',
  `WEBPAGEURL` varchar(200) default NULL,
  `DATABASEID` int(5) default NULL,
  `TABLENAME` varchar(100) default NULL,
  `FILTER` varchar(255) default NULL,
  `DEFAULTVIEWTYPE` varchar(10) NOT NULL default 'LISTVIEW',
  `SHOWRECORDSASCONSTRUCTS` char(1) NOT NULL default 'N',
  `CAPTIONCOLUMNNAME` varchar(50) default NULL,
  `SORTCOLUMNNAME` varchar(50) default NULL,
  `SEARCHCOLUMNNAME` varchar(50) default NULL,
  `PANELHELPTEXT` varchar(250) default NULL,
  `PRELOAD` char(1) default 'N',
  `NEWRECORDINFORMVIEW` char(1) NOT NULL default 'N',
  `SELECTVIEW` varchar(50) default NULL,
  `SELECTCOLUMNNAMES` varchar(50) default NULL,
  `SELECTSHOWCOLUMNS` varchar(250) default NULL,
  `SELECTQUERY` text,
  PRIMARY KEY  (`CLIENTID`,`APPLICATIONID`,`FORMID`)
) TYPE=MyISAM;



#
# Table structure for table 'plr_formpermission'
#

CREATE TABLE `plr_formpermission` (
  `CLIENTID` int(5) NOT NULL default '0',
  `APPLICATIONID` int(5) NOT NULL default '0',
  `FORMID` int(5) NOT NULL default '0',
  `USERGROUPID` int(5) NOT NULL default '0',
  `PERMISSIONTYPE` int(2) NOT NULL default '0',
  PRIMARY KEY  (`CLIENTID`,`APPLICATIONID`,`FORMID`,`USERGROUPID`)
) TYPE=MyISAM;



#
# Table structure for table 'plr_images'
#

CREATE TABLE `plr_images` (
  `IMAGEID` int(5) NOT NULL default '0',
  `IMAGE` varchar(100) NOT NULL default '',
  `DESCRIPTION` varchar(50) default NULL,
  PRIMARY KEY  (`IMAGEID`)
) TYPE=MyISAM;



#
# Table structure for table 'plr_line'
#

CREATE TABLE `plr_line` (
  `CLIENTID` int(5) NOT NULL default '0',
  `APPLICATIONID` varchar(50) NOT NULL default '',
  `FORMID` int(5) NOT NULL default '0',
  `PAGEID` int(5) NOT NULL default '0',
  `LINEID` int(5) NOT NULL default '0',
  `LINEDESCRIPTION` varchar(100) NOT NULL default '',
  `HELPNR` int(5) default NULL,
  `LINETYPE` varchar(10) NOT NULL default '',
  `DATABASEID` int(5) default NULL,
  `TABLENAME` varchar(100) default NULL,
  `COLUMNNAME` varchar(50) default NULL,
  `READONLY` char(1) NOT NULL default 'N',
  `FORMAT` varchar(25) default NULL,
  `IMAGEDIR` varchar(255) default NULL,
  `IMAGEURL` varchar(255) default NULL,
  `POSITIONTYPE` varchar(10) NOT NULL default 'AUTO',
  `XPOSITION` int(5) default NULL,
  `YPOSITION` int(5) default NULL,
  `VISIBLE` char(1) NOT NULL default 'Y',
  `VISIBLEINLIST` char(1) NOT NULL default 'Y',
  `POSITIONINDEX` int(5) unsigned default NULL,
  `PREVIEWURL` varchar(255) default NULL,
  `ISGROUPFIELD` char(1) NOT NULL default 'N',
  `GROUPSOURCE` varchar(255) default NULL,
  PRIMARY KEY  (`CLIENTID`,`APPLICATIONID`,`FORMID`,`PAGEID`,`LINEID`)
) TYPE=MyISAM;



#
# Table structure for table 'plr_logbase'
#

CREATE TABLE `plr_logbase` (
  `logid` bigint(20) unsigned NOT NULL auto_increment,
  `status` varchar(20) default NULL,
  `session` varchar(255) default '0',
  `sessionstart` datetime default NULL,
  `sessionend` datetime default NULL,
  `username` varchar(100) default NULL,
  `server` varchar(200) default NULL,
  `useripaddress` varchar(50) default NULL,
  PRIMARY KEY  (`logid`)
) TYPE=MyISAM;



#
# Table structure for table 'plr_membership'
#

CREATE TABLE `plr_membership` (
  `CLIENTID` int(5) NOT NULL default '0',
  `USERGROUPID` int(5) NOT NULL default '0',
  `GROUPID` int(5) NOT NULL default '0',
  PRIMARY KEY  (`CLIENTID`,`USERGROUPID`,`GROUPID`)
) TYPE=MyISAM;



#
# Table structure for table 'plr_menuitem'
#

CREATE TABLE `plr_menuitem` (
  `CLIENTID` int(5) NOT NULL default '0',
  `APPLICATIONID` int(5) NOT NULL default '0',
  `MENUITEMID` int(5) NOT NULL default '0',
  `MENUCAPTION` varchar(50) NOT NULL default '',
  `MENUTYPE` varchar(15) NOT NULL default '',
  `MAINMENUGROUPINDEX` int(5) default NULL,
  `IMAGEINDEX` int(5) default NULL,
  `ORDERINDEX` int(5) NOT NULL default '0',
  `GROUPINDEX` int(5) NOT NULL default '0',
  `VISIBLE` char(1) NOT NULL default '',
  `CHAINEDCONSTRUCTID` int(5) default NULL,
  `PARENTMENUITEMID` int(5) default NULL,
  `SUBGROUPINDEX` int(11) default NULL,
  `SHOWASBUTTON` char(1) NOT NULL default 'N',
  `ACTIONID` int(5) default NULL,
  PRIMARY KEY  (`CLIENTID`,`APPLICATIONID`,`MENUITEMID`)
) TYPE=MyISAM;



#
# Table structure for table 'plr_news'
#

CREATE TABLE `plr_news` (
  `CLIENTID` int(5) unsigned NOT NULL default '0',
  `NEWSID` int(3) NOT NULL auto_increment,
  `STARTDATE` date default NULL,
  `ENDDATE` date default NULL,
  `MESSAGE` text NOT NULL,
  `DATEMESSAGE` date NOT NULL default '2002-01-01',
  PRIMARY KEY  (`CLIENTID`,`NEWSID`),
  KEY `NEWSID_2` (`NEWSID`),
  KEY `NEWSID` (`NEWSID`)
) TYPE=MyISAM;



#
# Table structure for table 'plr_page'
#

CREATE TABLE `plr_page` (
  `CLIENTID` int(5) NOT NULL default '0',
  `APPLICATIONID` int(5) NOT NULL default '0',
  `FORMID` int(5) NOT NULL default '0',
  `PAGEID` int(5) NOT NULL default '0',
  `PAGENAME` varchar(50) NOT NULL default '',
  `HELPNR` int(5) default NULL,
  `PAGECOLUMNCOUNT` int(1) NOT NULL default '0',
  `LIBRARYNAME` varchar(8) default NULL,
  `PROCEDURENAME` varchar(50) default NULL,
  `IMAGEINDEX` int(5) default '-1',
  `CRYSTALREPORTNAME` varchar(100) default NULL,
  `ACTIONID` int(5) default NULL,
  `BEFORECRYSTALREPORTACTION` int(5) default NULL,
  PRIMARY KEY  (`CLIENTID`,`APPLICATIONID`,`FORMID`,`PAGEID`)
) TYPE=MyISAM;



#
# Table structure for table 'plr_permissionnames'
#

CREATE TABLE `plr_permissionnames` (
  `PERMISSIONTYPE` int(2) NOT NULL default '0',
  `PERMISSIONNAME` varchar(50) NOT NULL default '',
  PRIMARY KEY  (`PERMISSIONTYPE`),
  UNIQUE KEY `PERMISSIONNAME` (`PERMISSIONNAME`)
) TYPE=MyISAM;



#
# Table structure for table 'plr_search'
#

CREATE TABLE `plr_search` (
  `CLIENTID` int(5) NOT NULL default '0',
  `APPLICATIONID` int(5) NOT NULL default '0',
  `FORMID` int(5) NOT NULL default '0',
  `DATABASEID` int(5) NOT NULL default '0',
  `TABLENAME` varchar(100) NOT NULL default '',
  `COLUMNNAME` varchar(50) NOT NULL default '',
  `POSITION` int(5) NOT NULL default '0',
  `PATHCOLUMNNAME` varchar(50) default NULL,
  PRIMARY KEY  (`CLIENTID`,`APPLICATIONID`,`FORMID`,`TABLENAME`,`COLUMNNAME`)
) TYPE=MyISAM;



#
# Table structure for table 'plr_shortcut'
#

CREATE TABLE `plr_shortcut` (
  `CLIENTID` int(5) NOT NULL default '0',
  `APPLICATIONID` int(5) NOT NULL default '0',
  `SHORTCUTID` int(5) NOT NULL default '0',
  `SHORTCUTSHEETID` int(5) NOT NULL default '0',
  `SHORTCUTNAME` varchar(50) NOT NULL default '',
  `CONSTRUCTID` int(5) NOT NULL default '0',
  `ORDERINDEX` int(5) NOT NULL default '0',
  `USERGROUPID` int(5) default NULL,
  PRIMARY KEY  (`CLIENTID`,`APPLICATIONID`,`SHORTCUTID`)
) TYPE=MyISAM;



#
# Table structure for table 'plr_shortcutsheet'
#

CREATE TABLE `plr_shortcutsheet` (
  `CLIENTID` int(5) NOT NULL default '0',
  `APPLICATIONID` int(5) NOT NULL default '0',
  `SHORTCUTSHEETID` int(5) NOT NULL default '0',
  `SHEETNAME` varchar(50) NOT NULL default '',
  `ORDERINDEX` int(5) NOT NULL default '0',
  `USERGROUPID` int(5) default NULL,
  PRIMARY KEY  (`CLIENTID`,`APPLICATIONID`,`SHORTCUTSHEETID`)
) TYPE=MyISAM;



#
# Table structure for table 'plr_subset'
#

CREATE TABLE `plr_subset` (
  `DATABASEID` int(5) NOT NULL default '0',
  `SUBSETNAME` varchar(30) NOT NULL default '',
  `TABLENAME` varchar(100) NOT NULL default '',
  `SHOWSELECTITEMBUTTON` char(1) NOT NULL default 'N',
  `LINKEDFORM` int(5) default NULL,
  `WEIGHT` int(1) NOT NULL default '1',
  `SHOWDROPDOWNBUTTON` char(1) NOT NULL default 'Y',
  `USEINPINUP` char(1) default 'N',
  `FILTER` varchar(255) default NULL,
  PRIMARY KEY  (`DATABASEID`,`SUBSETNAME`)
) TYPE=MyISAM;



#
# Table structure for table 'plr_subsetitem'
#

CREATE TABLE `plr_subsetitem` (
  `DATABASEID` int(5) NOT NULL default '0',
  `SUBSETNAME` varchar(30) NOT NULL default '',
  `COLUMNNAME` varchar(50) NOT NULL default '',
  `TABLENAME` varchar(100) NOT NULL default '',
  `SUPERTABLENAME` varchar(100) NOT NULL default '',
  `SUPERCOLUMNNAME` varchar(50) NOT NULL default '',
  `ORDERINDEX` int(5) NOT NULL default '0',
  PRIMARY KEY  (`DATABASEID`,`SUBSETNAME`,`COLUMNNAME`)
) TYPE=MyISAM;



#
# Table structure for table 'plr_table'
#

CREATE TABLE `plr_table` (
  `DATABASEID` int(5) NOT NULL default '0',
  `TABLENAME` varchar(100) NOT NULL default '',
  `PRELOAD` char(1) default 'N',
  `INSERTPROCEDURE` varchar(150) default NULL,
  `UPDATEPROCEDURE` varchar(150) default NULL,
  `DELETEPROCEDURE` varchar(150) default NULL,
  `SEQUENCE` varchar(50) default NULL,
  `SEQUENCECOLUMNNAME` varchar(50) default NULL,
  PRIMARY KEY  (`DATABASEID`,`TABLENAME`)
) TYPE=MyISAM;



#
# Table structure for table 'plr_usergroup'
#

CREATE TABLE `plr_usergroup` (
  `CLIENTID` int(5) NOT NULL default '0',
  `USERGROUPID` int(5) NOT NULL auto_increment,
  `USERGROUPNAME` varchar(100) NOT NULL default '',
  `USERORGROUP` varchar(5) NOT NULL default '',
  `FULLNAME` varchar(50) default NULL,
  `DESCRIPTION` varchar(255) default NULL,
  `USERPASSWORD` varchar(32) default NULL,
  `EMAILADDRESS` varchar(100) default NULL,
  `CHANGEPASSWORDNEXTLOGON` char(1) NOT NULL default 'N',
  `USERCANNOTCHANGEPASSWORD` char(1) NOT NULL default 'N',
  `PASSWORDNEVEREXPIRES` char(1) NOT NULL default 'N',
  `ACCOUNTDISABLED` char(1) NOT NULL default 'N',
  `LASTLOGINDATE` date default NULL,
  `LASTPASSWORDCHANGEDATE` date default NULL,
  `PASSWORDCHANGEDONDATE` timestamp(14) NOT NULL,
  `ROLE` varchar(64) default NULL,
  `CLIENTADMIN` char(1) NOT NULL default 'N',
  `ROOTADMIN` char(1) NOT NULL default 'N',
  `LOGINCOUNT` int(10) NOT NULL default '0',
  PRIMARY KEY  (`CLIENTID`,`USERGROUPID`),
  UNIQUE KEY `USERGROUPNAME` (`USERGROUPNAME`)
) TYPE=MyISAM;



#
# Table structure for table 'plr_usersettingsconstruct'
#

CREATE TABLE `plr_usersettingsconstruct` (
  `CLIENTID` int(5) NOT NULL default '0',
  `APPLICATIONID` varchar(50) NOT NULL default '',
  `CONSTRUCTID` int(5) NOT NULL default '0',
  `USERGROUPID` int(5) NOT NULL default '0',
  `MASTERFORMTYPE` varchar(10) default NULL,
  `DETAILFORMTYPE` varchar(10) default NULL,
  PRIMARY KEY  (`CLIENTID`,`APPLICATIONID`,`CONSTRUCTID`,`USERGROUPID`)
) TYPE=MyISAM;



#
# Table structure for table 'plr_workflowjump'
#

CREATE TABLE `plr_workflowjump` (
  `DATABASEID` int(5) NOT NULL default '0',
  `WORKFLOWJUMPID` varchar(10) NOT NULL default '',
  `APPLICATIONID` varchar(50) NOT NULL default '',
  `CONSTRUCTID` int(5) NOT NULL default '0',
  `MESSAGE` text,
  PRIMARY KEY  (`DATABASEID`,`WORKFLOWJUMPID`)
) TYPE=MyISAM;



#
# Table structure for table 'plr_yesnovalues'
#

CREATE TABLE `plr_yesnovalues` (
  `YESNOVALUE` char(1) NOT NULL default '',
  `DESCRIPTION` varchar(5) NOT NULL default '',
  PRIMARY KEY  (`YESNOVALUE`),
  UNIQUE KEY `DESCRIPTION` (`DESCRIPTION`)
) TYPE=MyISAM;

