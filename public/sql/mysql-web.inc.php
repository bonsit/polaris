<?php

$_sqlBlocks = '
  SELECT m.*, p.pagename, r.text as caption
	FROM web_page p
	LEFT JOIN web_menu m 
	ON p.pagename = m.pagename
	LEFT JOIN web_resource r
	ON caption_rsc_ = r.resourceid
	WHERE parentmenuid = ?
	AND r.LANGUAGE = ? 
	AND p.LANGUAGE = r.LANGUAGE 
	AND visible = "Y"
	ORDER BY orderindex
';
		
$_sqlMainMenu = '
  SELECT *, r.text as caption 
	FROM web_menu
	LEFT JOIN web_resource r
	ON caption_rsc_ = r.resourceid
	WHERE parentmenuid = -1
	AND LANGUAGE = ? 
	AND visible = "Y"
	ORDER BY orderindex
';
$_sqlSubMenu = '
  SELECT *, r.text as caption 
	FROM web_menu
	LEFT JOIN web_resource r
	ON caption_rsc_ = r.resourceid
	WHERE parentmenuid = ?
	AND LANGUAGE = ? 
	AND visible = "Y"
	ORDER BY orderindex
';
$_sqlStandardSubMenu = '
  SELECT *, r.text as caption 
	FROM web_menu
	LEFT JOIN web_resource r
	ON caption_rsc_ = r.resourceid
	WHERE parentmenuid = 0
	AND LANGUAGE = ? 
	AND visible = "Y"
	ORDER BY orderindex
';

$_sqlFirstPage = '
  SELECT p.pagename, p.sqlsource, p.encoding, p.locale, p.stylesheet, m.menuid
	FROM web_page p
	LEFT JOIN web_menu m
	ON m.pagename = p.pagename
	WHERE (parentmenuid = ? OR menuid = ?)
	AND LANGUAGE = ? 
	AND p.LANGUAGE = LANGUAGE 
	ORDER BY orderindex
';

$_sqlPage = '
  SELECT p.pagename, p.sqlsource, p.encoding, p.locale, p.stylesheet, m.parentmenuid, m.menuid
	FROM web_page p
	LEFT JOIN web_menu m
	ON m.pagename = p.pagename
	WHERE upper(p.pagename) = ?
	AND p.language = ?
';

$_sqlParam = '
  SELECT p.*, field(language, \'FR\', \'NL\' , \'AA\') as orderindex 
	FROM web_parameter p
	WHERE language = ?
	OR language = "AA"
	ORDER BY orderindex
';

?>