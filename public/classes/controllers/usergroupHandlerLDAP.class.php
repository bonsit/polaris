<?php
use LdapRecord\Connection;

class controllers_usergroupHandlerLDAP extends controllers_usergroupHandlerBase
{
    var $ldap;

    function __construct($type=false) {
        global $_CONFIG;

        $this->ldap = new Connection([
            'hosts' => $_CONFIG['LDAP.domain_controllers'],
            'port' => 389,
            'base_dn' => 'dc=local,dc=com',
            'username' => $_CONFIG['LDAP.ad_username'],
            'password' => $_CONFIG['LDAP.ad_password'],
        ]);

        $this->ldap = new base_adLDAP();

        // // You will need to edit these variables to suit your installation
        // $this->ldap->_account_suffix = $_CONFIG['LDAP.account_suffix'];
        // $this->ldap->_base_dn = $_CONFIG['LDAP.base_dn'];

        // // An array of domain controllers. Specify multiple controllers if you
        // // would like the class to balance the LDAP queries amongst multiple servers
        // $this->ldap->_domain_controllers = $_CONFIG['LDAP.domain_controllers'];

        // // optional account with higher privileges for searching
        // $this->ldap->_ad_username = $_CONFIG['LDAP.ad_username'];
        // $this->ldap->_ad_password = $_CONFIG['LDAP.ad_password'];

        // // AD does not return the primary group. http://support.microsoft.com/?kbid=321360
        // // This tweak will resolve the real primary group, but may be resource intensive.
        // // Setting to false will fudge "Domain Users" and is much faster. Keep in mind though that if
        // // someone's primary group is NOT domain users, this is obviously going to bollocks the results
        // $this->ldap->_real_primarygroup = $_CONFIG['LDAP.real_primarygroup'];

        // // When querying group memberships, do it recursively
        // // eg. User Fred is a member of Group A, which is a member of Group B, which is a member of Group C
        // // user_ingroup("Fred","C") will returns true with this option turned on, false if turned off
        // $this->ldap->_recursive_groups = $_CONFIG['LDAP.recursive_groups'];

    }

    /**
    *	Authenticate to the directory with a specific username and password
    */
    function authenticate($username,$password) {
        global $polaris;
        global $_CONFIG;

        $this->ldap->connect();
        $result = $this->ldap->authenticate($_CONFIG['LDAP.account_prefix'].$username, $password) == true;

        if ($result) {
            // logging
            $polaris->logUserAction('success', session_id(), $username);

            $_userObject = new base_plrUserGroup($polaris);
            $_userObject->LoadRecordByName($username);

            $outparams['clientid'] = $_userObject->record['CLIENTID'];
            $outparams['userid'] = $_userObject->record['USERGROUPID'];
            $outparams['changepassword'] = 'N'; // password changes are done via the LDAP software
            $usertype = $_userObject->record['USERTYPE'];
            if ($_userObject->record['CLIENTADMIN'] == 'Y')
                $usertype = 'client';
            if ($_userObject->record['ROOTADMIN'] == 'Y')
                $usertype = 'root';
            $outparams['usertype'] = $usertype;
            $outparams['language'] = $_userObject->record['LANGUAGE'];
            $outparams['usergroupname'] = $_userObject->record['USERGROUPNAME'];
            $outparams['fullname'] = $_userObject->record['FULLNAME'];
            $outparams['userphoto'] = $_userObject->record['PHOTOURL'];
            $outparams['plrinstance'] = $_userObject->record['PLRINSTANCE'];
            $outparams['options'] = $_userObject->record['OPTIONS'];
            $outparams['usercss'] = $_userObject->record['CUSTOMCSS'];
            $outparams['userbackground'] = $_userObject->record['USER_BACKGROUND'];
        } else {
            $polaris->logUserAction('LDAP logon failed', session_id(), $username);
            if ($polaris->tpl) {
                $polaris->tpl->assign('loginerror', lang_get('loginerror_1'));
            }
        }

        return $result;
    }

    /**
    *   Returns an array of information for a specific user
    */
    function getUserInfo($user,$fields=NULL) {
        return $this->ldap->user_info($user,$fields);
    }

    /**
    *   Returns an array of information for a specific group
    */
    function getGroupInfo($user,$fields=NULL) {
        return $this->ldap->group_info($group);
    }

    /**
    *   Returns an array of groups that a user is a member off
    */
    function getUserGroups($user,$fields=NULL) {
        return $this->ldap->user_groups($user,$fields);
    }

    /**
	*   Returns true if the user is a member of the group
    */
    function userIsMemberOf($user,$group,$recursive=NULL) {
        return $this->ldap->user_ingroup($user,$group,$recursive);
    }

	/**
	*	Returns an array of information for a specific group
	*/
    function getAllUsers($include_description = false, $sorted = true) {
        return $this->ldap->all_users($include_description, $search = "*", $sorted);
    }

	/**
    *	Returns all AD groups (expensive on resources)
    */
    function getAllGroups($include_description = false, $sorted = true) {
        return $this->ldap->all_groups($include_description, $search = "*", $sorted);
    }

}
