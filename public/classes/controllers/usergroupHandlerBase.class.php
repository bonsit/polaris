<?php

class controllers_usergroupHandlerBase {
    var $type;

    function __construct($type=false) {
        global $_CONFIG;

        $this->type = $type ? $type : $_CONFIG['usergrouphandler_type'];
    }

    function logon($username, $password, $salt=false) {
        global $polaris;
        global $_CONFIG;
        global $PLRLANGUAGES;

        $polaris->currentuser = $this->authenticate($username, $password);
        if ( $polaris->currentuser === FALSE || $polaris->currentuser->outparams['usertype'] === FALSE ) {
            if ($polaris->tpl and $polaris->currentuser->outparams['err']) {
                $polaris->tpl->assign('username', $username);
                $polaris->tpl->assign('loginerror', lang_get('loginerror_'.$polaris->currentuser->outparams['err']));
            }
            return false;
        }

        if (!empty($polaris->currentuser->outparams['plrinstance']) && $_SERVER['HTTP_HOST'] != $polaris->currentuser->outparams['plrinstance'] ) {
            $key = md5($polaris->currentuser->outparams['clientid'].'_'.strtoupper($username).'_'.$_SERVER['SERVER_NAME']);
            header('location: http://'.$polaris->currentuser->outparams['plrinstance'].'?method=plr_redirect&k='.$key);
        } else {
            // set the language of this user (language of plr_usergroup or defaultlanguage from plr_client)
            setlocale(LC_ALL, $PLRLANGUAGES[$polaris->currentuser->outparams['language']]);
            $polaris->setLanguage($polaris->currentuser->outparams['language']);
            $polaris->SetLoginSessionVars($polaris->currentuser->outparams);

            if (function_exists('adodb_session_regenerate_id')) {
                adodb_session_regenerate_id();
            }
            if ($polaris->currentuser->outparams['2fa'] == true and !empty($polaris->currentuser->outparams['phonenumber'])) {
                if (!$_CONFIG['2fa_debug']) {
                    $_twofacode = $polaris->generate2FACode($username);
                    if ($_twofacode) {
                        $polaris->send2FACheck($polaris->currentuser->outparams['phonenumber'], $_twofacode);
                    } else {
                        die('Could not send 2FA code');
                    }
                }
                $_SESSION['2falogonrequired'] = true;
                $_SESSION['2fachecked'] = false;
            } else {
                $_SESSION['2falogonrequired'] = false;
            }
        }
        return $polaris->currentuser->outparams['usertype'];
    }

    /**
    *	Authenticate to the directory with a specific username and password
    */
    function authenticate($username, $password) {
        return false;
    }

    /**
    *   Returns an array of information for a specific user
    */
    function getUserInfo($user,$fields=NULL) {
        return false;
    }

    /**
    *   Returns an array of information for a specific group
    */
    function getGroupInfo($user,$fields=NULL) {
        return false;
    }

    /**
    *   Returns an array of groups that a user is a member off
    */
    function getUserGroups($user,$fields=NULL) {
        return false;
    }

    /**
	*   Returns true if the user is a member of the group
    */
    function userIsMemberOf($user,$group,$recursive=NULL) {
        return false;
    }

	/**
	*	Returns an array of information for a specific group
	*/
    function getAllUsers($include_description = false, $sorted = true) {
        return false;
    }

	/**
    *	Returns all AD groups (expensive on resources)
    */
    function getAllGroups($include_description = false, $sorted = true) {
        return false;
    }

}
