<?php
/***
    Events that can take place in the Dashboard (Ajax) environment
**/
// require_once('handler.dash_addcontent.class.php');
// require_once('handler.dash_removecontent.class.php');
// require_once('handler.dash_updatewindow.class.php');

class controllers_dispatcherDashboardAjax extends controllers_dispatcherBase {

	/**
	*	PHP 5 constructor
	*/
    function __construct($eventname) {
        parent::__construct($eventname);
    }

}
