<?php

class controllers_logout extends controllers_handlerBase {

    function __construct($eventname) {
        parent::__construct($eventname);
    }

    function handleEvent() {
        global $_GVARS;
        global $polaris;

        // remember the brand environment Polaris is in
        $branding = $_SESSION['branding'];
        $brand = $_SESSION['brand'];

        $polaris->logout($_SESSION['name']);
        session_write_close();

        // and store the brand environment back into Polaris
        if ($branding)
            header("Location: {$_GVARS['serverroot']}?brand=$brand&lang=".$polaris->language);
        else
            header("Location: {$_GVARS['serverroot']}?lang=".$polaris->language);
    }

}
