<?php

class controllers_usergroupHandlerPLR extends controllers_usergroupHandlerBase {

    var $type;

    function __construct($type=false) {
        parent::__construct($type);
    }

    /**
    *	Authenticate to the directory with a specific username and password
    */
    function authenticate($username, $password) {
        global $polaris;

        $_userObject = new base_plrUserGroup($polaris);
        $_userObject->login($username, $password);
        return $_userObject;
    }

    /**
    *   Returns an array of information for a specific user
    */
    function getUserInfo($user,$fields=NULL) {
    }

    /**
    *   Returns an array of information for a specific group
    */
    function getGroupInfo($user,$fields=NULL) {
    }

    /**
    *   Returns an array of groups that a user is a member off
    */
    function getUserGroups($user,$fields=NULL) {
    }

    /**
	*   Returns true if the user is a member of the group
    */
    function userIsMemberOf($user,$group,$recursive=NULL) {
    }

	/**
	*	Returns an array of information for a specific group
	*/
    function getAllUsers($include_description = false, $sorted = true) {
    }

	/**
    *	Returns all PLR groups
    */
    function getAllGroups($include_description = false, $sorted = true) {
    }

}
