<?php

class controllers_plr_newsession extends controllers_handlerBase {

    function __construct($eventname) {
        parent::__construct($eventname);
    }

    function handleEvent() {
        global $polaris;

        $polaris->tpl->assign('resetpolarissession', false);
        if (isset($_SESSION['plrauth']) and $_SESSION['plrauth']) {
            $polaris->setPolarisSession($polaris->generateSessionId());
            $polaris->tpl->assign('resetpolarissession', true);
        }
    }
}
