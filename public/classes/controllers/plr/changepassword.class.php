<?php

class controllers_plr_changepassword extends controllers_handlerBase {

    function __construct($eventname) {
        parent::__construct($eventname);
    }

    function handleEvent() {
        global $g_lang_strings;
        global $polaris;

        $_result = false;
        if (isset($_SESSION['plrauth']) and $_SESSION['plrauth']) {
            $_userObject = new base_plrUserGroup($polaris);
            $_userObject->LoadRecordByName($_SESSION['name']);
            $_result = $_userObject->ChangePassword($_POST['newpassword'], $_POST['retypepassword']);
            $_SESSION['changepassword'] = 'N';
        }
        return $_result == PASSWORD_STORED;
    }
}
