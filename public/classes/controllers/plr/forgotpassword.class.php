<?php
require_once 'includes/miscfunc.inc.php';

class controllers_plr_forgotpassword extends controllers_handlerBase {

    function __construct($eventname) {
        parent::__construct($eventname);
    }

    function handleEvent() {
        global $polaris;
        global $g_lang_strings;
        global $_GVARS;

        $_result = false;
        if (isset($_POST['emailadres']) and trim($_POST['emailadres']) != '') {
            $_emailadres = $_POST['emailadres'];
            if (validateEmail($_emailadres) !== FALSE) {
                $values['FORMHASH'] = 'e793390ad68aa8d521f3d15097627f10';
                $resetlink = $polaris->createResetPasswordLink($_emailadres);
                if ($resetlink !== false) {
                    $values['RESETLINK'] = $resetlink;
                    $polaris->AddToQueue('send_reset_email', array('email' => $_emailadres, 'insertValues' => $values));
                    $_result = true;
                } else {
                    // not throwing an exception here, because we don't want to give away if an email address is in use
                    // throw new Exception($g_lang_strings['only_licenced_users_message']);

                    $_result = true;
                }
            } else {
                throw new Exception($g_lang_strings['invalid_email']);
            }
        }
        return $_result;
    }
}
