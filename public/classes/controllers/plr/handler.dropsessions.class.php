<?php

class controllers_dropsessions extends controllers_handlerBase {

    function __construct($eventname) {
        parent::__construct($eventname);
    }

    function handleEvent() {
        global $polaris;

        $polaris->dropOtherUsers();
	}

}
