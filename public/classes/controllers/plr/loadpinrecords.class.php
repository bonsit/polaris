<?php

class controllers_plr_loadPinRecords extends controllers_handlerBase
{
    function __construct($eventname) {
        parent::__construct($eventname);
    }

	function customUrlEncode($url) {
        return str_replace(array('+','/'), array('^','$'), $url);
	}

    function handleEvent() {
        global $_GVARS;
        global $polaris;

        $result = "";
        // $polaris->polarissession = $_GET['_hdnPlrSes'];
        // pr($polaris->polarissession, $_SESSION);
        $_pinnedTags = isset($_GET['pinnedtags']);

        if (isset($_SESSION['pinrecords'][$polaris->polarissession]) and count($_SESSION['pinrecords'][$polaris->polarissession]) > 0) {

            $_widgetOrTag = $_pinnedTags ? "tags" : "widget";
            foreach($_SESSION['pinrecords'][$polaris->polarissession] as $_tablename => $_pintable) {
                if (count($_pintable) > 0)
                foreach($_pintable as $_pin) {
                    if ($_pin and !isset($_pin['VISIBLE']) or (isset($_pin['VISIBLE']) and $_pin['VISIBLE'] !== false)) {
                        $_keys = array_keys($_pin);
                        $_plrrecordid = $this->customUrlEncode($_pin['PLR__RECORDID']);
                        if ($_plrrecordid == '') {
                            $_plrrecordid = $this->customUrlEncode($_pin['ROWIDTOCHAR(TTT.ROWID)']);
                            if ($_plrrecordid == '') {
                                foreach($_keys as $key => $_value){
                                    if(strpos($_value, 'MD5(CONCAT(') !== false){
                                        $_plrrecordid = $_pin[$_keys[$key]];
                                         break;
                                    }
                                }
                 //               $_pin['ROWIDTOCHAR(TTT.ROWID)']
                            }
                        }

                        $_keyvalue = $_pin[$_keys[0]];
                        $_showvalueTmp = $_pin['SHOWVALUE'] ?? '';
                        $_showvalue = ($_keyvalue !== $_showvalueTmp) ? ' '.$_showvalueTmp : '';
                        $_link = $_GVARS['serverpath'].'/app/'.$_pin['APP'].'/const/'.$_pin['CONST'].'/?qc[]='.$_keys[0].'&qv[]=\''.$_keyvalue.'\'';
                        if ($_pinnedTags) {
                            $_pintypename = '<span>'.$polaris->ReplaceDynamicVariables($_pin['FORMNAME']). '</span> ';
                            $_linktext = $_pintypename;
                            if ($_pin['EXTRAINFO'] != '') {
                                $_extrainfo = implode('<br/>',$_pin['EXTRAINFO']);
                                $_extrainfo = str_replace("<br/><br/>", '<br/>', $_extrainfo);
                                $_linktext .= $_extrainfo;
                            } else {
                                $_linktext .=  $_keyvalue.$_showvalue;
                            }

                            $result .= "<div class='btn-group' role='group' data-toggle='tooltip' title='Gepinde {$_pintypename}'>
                                <button type='button' class='reclink btn btn-success btn-outline' href='{$_link}' id='{$_plrrecordid}'>{$_linktext}</button>
                                <button type='button' class='btn btn-success btn-outline deletepin' data-pin='{$_tablename}:{$_plrrecordid}'><i class='icon icon-883' style='font-size:0.9rem;'></i></button>
                            </div>
                            ";

                        } else {
                            $_linktext = '<span>'.$polaris->ReplaceDynamicVariables($_pin['FORMNAME']). '</span> '. $_keyvalue.$_showvalue;
                            $_activeclass = ($_pin['ACTIVE'] == true)?' class="active"':'';
                            $result .= "<li$_activeclass>";
                            $result .= "<div class=\"$_widgetOrTag\">";
                            $result .= "<label data-pin=\"{$_tablename}:{$_plrrecordid}\" title=\"".lang_get('delete_pin')."\"><i class=\"delete-pin icon icon-757 fa-lg\"></i></label>";
                            $result .= "<a class=\"reclink\" href=\"{$_link}\" id=\"{$_plrrecordid}\">".$_linktext;
                            $result .= "</a>";
                            if ($_pin['EXTRAINFO'] != '') {
                                $_extrainfo = implode('<br/>',$_pin['EXTRAINFO']);
                                $_extrainfo = str_replace("<br/><br/>", '<br/>', $_extrainfo);
                                $result .= "<p>$_extrainfo</p>";
                            }
                            $result .= "</div>";
                            $result .= "</li>";
                        }
                    }
                }
            }
        } else {
            if (!$_pinnedTags)
                $result .= "<li>&ndash; ".lang_get('no_pinned_items')." &ndash;</li>";
        }

        return $result;
    }

}
