<?php

class controllers_plr_confirmuser extends controllers_handlerBase
{
    function __construct($eventname) {
        parent::__construct($eventname);
    }

    function handleEvent() {
        global $_GVARS;
        global $plrdb;
        global $_sqlConfirmAccount;

        if ( isset($_GET['a']) ) {
            $accounthash = $_GET['a'];
            $plrdb->Execute($_sqlConfirmAccount, array($accounthash) ) or die ('Account confirmation failed.');
            if ( isset($_GET['page']) ) {
                header( 'location: '. urldecode($_GET['page']) );
            } else {
                header( 'location: '. $_SERVER['REQUEST_URI'] );
            }
        }
    }
}
