<?php

class controllers_plr_importexport extends controllers_handlerBase {

    function __construct($eventname) {
        parent::__construct($eventname);
    }

    function handleEvent() {
        if (isset($_SESSION['plrauth']) and $_SESSION['plrauth']) {
            if ($_GET['action'] == 'impexp' and $_GET['result'] == 'true') {
                require_once('classes/importexport.class.php');
                var_dump($_POST);
            }
        }
    }
}
