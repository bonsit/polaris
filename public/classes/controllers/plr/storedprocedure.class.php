<?php

class controllers_plr_storedprocedure extends controllers_handlerBase {

    function __construct($eventname) {
        parent::__construct($eventname);
    }

    function autoQuote($value) {
        if (!is_numeric($value))
            return '"'.$value.'"';
        else
            return $value;
    }

    function handleEvent() {
        global $_GVARS, $polaris;

        if (isset($_SESSION['plrauth']) and $_SESSION['plrauth']) {
            if (isset($_REQUEST['databaseid']) and isset($_REQUEST['sp'])) {
                $databasehash = $_REQUEST['databaseid'];
                $params = Array();
                if (isset($_REQUEST['parameter1']))
                    $params[] = $this->autoQuote($_REQUEST['parameter1']);
                if (isset($_REQUEST['parameter2']))
                    $params[] = $this->autoQuote($_REQUEST['parameter2']);
                if (isset($_REQUEST['parameter3']))
                    $params[] = $this->autoQuote($_REQUEST['parameter3']);
                if (isset($_REQUEST['parameter4']))
                    $params[] = $this->autoQuote($_REQUEST['parameter4']);
                if (isset($_REQUEST['parameter5']))
                    $params[] = $this->autoQuote($_REQUEST['parameter5']);
                if (isset($_REQUEST['parameter6']))
                    $params[] = $this->autoQuote($_REQUEST['parameter6']);
                if (isset($_REQUEST['parameter7']))
                    $params[] = $this->autoQuote($_REQUEST['parameter7']);
                if (isset($_REQUEST['parameter8']))
                    $params[] = $this->autoQuote($_REQUEST['parameter8']);
                if (isset($_REQUEST['parameter9']))
                    $params[] = $this->autoQuote($_REQUEST['parameter9']);
                if (isset($_REQUEST['parameter10']))
                    $params[] = $this->autoQuote($_REQUEST['parameter10']);
                if (isset($_REQUEST['parameter11']))
                    $params[] = $this->autoQuote($_REQUEST['parameter11']);
                if (isset($_REQUEST['parameter12']))
                    $params[] = $this->autoQuote($_REQUEST['parameter12']);
                if (count($params) > 0)
                    $parameters = implode(',',$params);
                else
                    $parameters = '';
                $sp = $_REQUEST['sp']."($parameters)";
                $sql = "CALL $sp;";

                define(CLIENT_MULTI_RESULTS, 131072);

                $db = new base_plrDatabase($polaris);
                $db->LoadRecordHashed($databaseid);
                $db->connectUserDB();

                $db->debug=true;
                $db->Execute($sql);// or trigger_error($db->ErrorMsg().' '.$db->ErrorNo(), E_USER_WARNING);
            }
            if (isset($_REQUEST['redirecturl']))
                $gotourl = urldecode($_REQUEST['redirecturl']);
            else
                $gotourl = $_SERVER['REDIRECT_URL'];
            exit;
//            header("Location: $gotourl");
        }
    }
}
