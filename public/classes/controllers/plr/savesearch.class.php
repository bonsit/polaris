<?php

class controllers_plr_savesearch extends controllers_handlerBase {

    function __construct($eventname) {
        parent::__construct($eventname);
    }

    function handleEvent() {
        global $_sqlInsertSaveSearch;
        global $_sqlMaxIDSaveSearch;
        global $polaris;

        if (isset($_SESSION['plrauth']) and $_SESSION['plrauth'] === 'true') {
            if ( isset($_POST['SEARCHNAME']) ) {
                $_clientid = $_SESSION['clientid'];
                $_usergroupid = $_SESSION['userid'];
                $_formid = $_POST['_hdnForm'];
                $_databaseid = $_POST['_hdnDatabase'];
                $_searchname = $_POST['SEARCHNAME'];
                $_searchquery = base64_decode($_POST['SEARCHQUERY']);
                $_newSearchID = $polaris->instance->GetOne($_sqlMaxIDSaveSearch, array($_clientid, $_usergroupid, $_formid) );
                try {
                    $_result = $polaris->instance->Execute($_sqlInsertSaveSearch, array($_clientid, $_usergroupid, $_formid, $_newSearchID, $_databaseid, $_searchname, $_searchquery) );
                } catch (Exception $E) {
                    return false;
                }

                return true;
            }
        }
    }
}
