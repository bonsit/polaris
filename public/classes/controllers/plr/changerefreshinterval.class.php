<?php

class controllers_plr_changerefreshinterval extends controllers_handlerBase {

    function __construct($eventname) {
        parent::__construct($eventname);
    }

    function handleEvent() {
        if (isset($_SESSION['plrauth']) and $_SESSION['plrauth']) {
            $_SESSION['refresh_interval'] = $_POST['refresh_interval'];
            header('Location: '.$_SERVER['REQUEST_URI']);
        }
    }

}
