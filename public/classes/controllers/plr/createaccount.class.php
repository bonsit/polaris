<?php

class FileReadException extends Exception { }

class controllers_plr_createaccount extends controllers_handlerBase {

    function __construct($eventname) {
        parent::__construct($eventname);
    }

    function handleEvent() {
        global $g_lang_strings;
        global $polaris;
        global $_GVARS;

        $_resultsArray = null;
        if (trim($_POST['USERPASSWORD']) != '' and $_POST['USERPASSWORD'] == $_POST['RETYPEUSERPASSWORD']) {
            $_clientHash = $_POST['CLIENTHASH'];
            $_POST['PHONENUMBER'] = str_replace('+', '', $_POST['PHONENUMBER']);
            $_phoneNumber = $_POST['PHONENUMBER'];
            $_result = $polaris->addUserAccount(
                $_clientHash,
                $_POST['USERNAME'],
                $_POST['FIRSTNAME'].' '.$_POST['LASTNAME'],
                '', // description
                $_POST['USERNAME'],
                $_POST['USERPASSWORD'],
                $_resultsArray,
                'N', // clientadmin
                'N', // confirm account
                false,
                $_phoneNumber
            );

            if ($_result === true) {
                // call the client specific CreateAccount method (if it exists)
                // automatically login
                //$_loginController = new controllers_login(NULL);
                //$_loginController->processLogin($_POST['USERNAME'], $_POST['USERPASSWORD']);

                $_client = new base_plrClient($polaris, $_POST['CLIENTHASH'], true);
                $_POST['USERGROUPID'] = $_resultsArray['usergroupid'];
                $_client->ExecuteCustomClientHandler('CreateAccount', $_POST);

                $polaris->logout($_POST['USERNAME']);
                header("location: {$_GVARS['serverroot']}?prevaccount=".$_POST['USERNAME']);
            } else {
                if ($_resultsArray['errorid'] == 1) {
                    throw new Exception($g_lang_strings['user_exists']. "<br>Misschien wilt u gewoon <a href='/'>inloggen</a>?");
                }
            }
        } else {
            throw new Exception($g_lang_strings['password_not_correct']);
        }
    }
}
