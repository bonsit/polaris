<?php

class controllers_plr_deletetag extends controllers_handlerBase {

    function __construct($eventname) {
        parent::__construct($eventname);
    }

    function handleEvent() {
        global $polaris;
        global $_sqlDeleteTag;

        if (isset($_SESSION['plrauth']) and $_SESSION['plrauth']) {
            $clientid = $_SESSION['clientid'];
            $userid = $_SESSION['userid'];
            $recordid = $_GET['recordid'];

            $keys = array($clientid, $userid, $recordid);
            $polaris->instance->Execute($_sqlDeleteTag, $keys);
            $response = ($polaris->instance->Affected_Rows() > 0);
            return $response;
        } else {
            return false;
        }
    }
}
