<?php

class controllers_plr_addtag extends controllers_baseHandler {

    function __construct($eventname) {
        parent::__construct($eventname);
    }

    function handleEvent() {
        global $polaris;
        global $scramble;
        global $g_lang_strings;

        if (isset($_SESSION['plrauth']) and $_SESSION['plrauth']) {

            $clientid = $_SESSION['clientid'];
            $userid = $_SESSION['userid'];
            $app = $_REQUEST['app'];
            $const = $_REQUEST['const'];
            $form = $_REQUEST['form'];
            $record = $_REQUEST['record'];
            $recordcaption = $_REQUEST['recordcaption'];
            $tag = trim($_REQUEST['tag']);
            $searchquery = $_REQUEST['search'];

            $response = array('result'=>false);

            if ($tag != '') {
                if (isset($searchquery) and $searchquery != '') {
                    $search = explode('%2B',$searchquery); // split the query values (+ sign)
                    sort($search);
                    $search = implode('%2B', $search);
                }
                $sql = 'SELECT TAGID, TAG FROM plr_tag WHERE clientid=? AND userid=? AND app=? AND const=? AND form=? AND tag=?';
                if ((!isset($record) or ($record == '')) and isset($search)) {
                    $sql .= ' AND search=?';
                    $keys = array($clientid, $userid, $app, $const, $form, $tag, $search);
                    $record = '';
                } else {
                    $sql .= ' AND record=?';
                    $keys = array($clientid, $userid, $app, $const, $form, $tag, $record);
                }

                $rs = $polaris->instance->Execute($sql, $keys);
                if ($rs->RecordCount() > 0) {
                    $response = array('result'=>false,'error' => $g_lang_strings['tag_already_exists']);
                } else {
                    $url = $_SERVER['HTTP_REFERER'];
                    $polaris->instance->Execute('INSERT INTO plr_tag (CLIENTID, USERID, APP, CONST, FORM, RECORD, RECORDCAPTION, SEARCH, TAG, URL, TAGGEDON) VALUES (?,?,?,?,?,?,?,?,?,?,now())'
                     , array($clientid, $userid, $app, $const, $form, $record, $recordcaption, $search, $tag, $url) );
                    $tagid = $polaris->instance->Insert_ID();
                    $response = array('result'=>true,'app'=>$app,'tag'=>$tag,'recorid'=>MD5($tagid.$scramble));
                }
            }
            return $response;
        }
    }
}
