<?php

class controllers_plr_recordlimit extends controllers_handlerBase {

    function __construct($eventname) {
        parent::__construct($eventname);
    }

    function handleEvent() {
        global $polaris;

        if (isset($_SESSION['plrauth']) and $_SESSION['plrauth']) {
            if (isset($_POST['_hdnRecordLimit']))
                $_SESSION['record_limit'][$_POST['_hdnFormName']] = $_POST['_hdnRecordLimit'];
        }
    }
}
