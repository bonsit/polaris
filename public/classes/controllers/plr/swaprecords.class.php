<?php

class controllers_plr_swaprecords extends controllers_handlerBase {

    function __construct($eventname) {
        parent::__construct($eventname);
    }

    function handleEvent() {
        if (isset($_SESSION['plrauth']) and $_SESSION['plrauth']) {
            $this->swapRecords($_POST);
            if ( !$GLOBALS['errorhasoccured'] ) {
                header('Location: '.$_SERVER['REQUEST_URI']);
                exit();
            }
        }
    }

    function swapRecords($RecordArray) {
        global $scramble;
        global $polaris;

        // convert url-safe ROWID back to real Oracle ROWID
        $recordid1 = $plrdatabase->customUrlDecode($RecordArray['_hdnSwapRecord1']);
        $recordid2 = $plrdatabase->customUrlDecode($RecordArray['_hdnSwapRecord2']);
        if (isset($recordid1) and isset($recordid2)) {
            $databaseid = $RecordArray['_hdnDatabase'];
            $plrdatabase = new plrDatabase($polaris);
            $plrdatabase->LoadRecordHashed($databaseid);
            $plrdatabase->connectUserDB();

            $tablename = $RecordArray['_hdnTable'];

            $keyfieldselect1 = $plrdatabase->makeEncodedKeySelect($tablename, $recordid1);
            $keyfieldselect2 = $plrdatabase->makeEncodedKeySelect($tablename, $recordid2);
            $volgorde1 = this->swapSelectOrder($plrdatabase->userdb, $tablename, $keyfieldselect1, $RecordArray['_hdnSwapColumn']);
            $volgorde2 = this->swapSelectOrder($plrdatabase->userdb, $tablename, $keyfieldselect2, $RecordArray['_hdnSwapColumn']);

            if (($volgorde1 != false) and ($volgorde2 != false)) {
                $fields1 = array($RecordArray['_hdnSwapColumn'] => $volgorde2);
                $fields2 = array($RecordArray['_hdnSwapColumn'] => $volgorde1);

                $keyfieldselect1 = $plrdatabase->makeEncodedKeySelect($tablename, $recordid1);
                $keyfieldselect2 = $plrdatabase->makeEncodedKeySelect($tablename, $recordid2);

                $plrdatabase->updateRecord($tablename, $keyfieldselect1, $fields1);
                $plrdatabase->updateRecord($tablename, $keyfieldselect2, $fields2);
            }
        }
    }

    function swapSelectOrder($userdb, $tablename, $where, $columnname) {
        $swapselect = 'SELECT ' . $columnname . ' FROM ' . $tablename . ' WHERE ' . $where;

        $rs = $userdb->Execute($swapselect);
        if ($rec = $rs->FetchNextObject(true))
            return $rec->$columnname;
        else
            return false;
    }
}
