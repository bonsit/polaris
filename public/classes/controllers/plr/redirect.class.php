<?php

class controllers_plr_redirect extends controllers_handlerBase {

    function __construct($eventname) {
        $this->__construct($eventname);
    }

    function handleEvent() {
        global $polaris;
        global $_CONFIG;
        global $_GVARS;

        $key = $_GET['k'];
        $_userObject = new base_plrUserGroup($polaris, $key);

        $parts = parse_url($_SERVER['HTTP_REFERER']);
        $servername = $parts['host'];

        list($result, $outparams) = $_userObject->checkInterInstanceLogin($servername, $key);

        if ($result) {
            $polaris->SetLoginSessionVars($outparams);
            header('location: '.$_GVARS['serverroot']);
        } else {
            if ($polaris->tpl)
                $polaris->tpl->assign('reponse',$g_lang_strings['login_not_correct']);
        }
    }
}
