<?php



class controllers_plr_changepasswordreset extends controllers_handlerBase {

    function __construct($eventname) {
        parent::__construct($eventname);
    }

    function handleEvent() {
        global $g_lang_strings;
        global $polaris;
        global $_GVARS;

        $_result = 0;
        $_userhash = $polaris->resetHashExists($_POST['_hdnResetHash']);
        if ($_userhash) {
            $_userObject = new base_plrUserGroup($polaris, $_userhash);
            $_result = $_userObject->ChangePassword($_POST['newpassword'], $_POST['retypepassword']);
            $_userObject->updateUser(array('RESETHASH' => ''));
            $_SESSION['changepassword'] = 'N';
        }
        return ($_result == 0);
    }
}
