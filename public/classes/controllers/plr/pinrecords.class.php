<?php

class controllers_plr_pinrecords extends controllers_handlerBase {

    function __construct($eventname=false) {
        parent::__construct($eventname);
    }

    function handleEvent() {
        if (isset($_SESSION['plrauth']) and $_SESSION['plrauth']) {
            $pbm = $_POST['_hdnProcessedByModule'] ?? 'false';
            if ($pbm !== 'true') {
                require_once('includes/mainactions.inc.php');
                return $this->pinRecords($_POST, $_POST['_hdnRecordID']);
            }
        }
    }

    function CombinePinnedRecords($tablearray, $pinarray) {
        $_recordfound = false;

        if (empty($tablearray)) {
            $tablearray = $pinarray;
        } else {
            foreach($pinarray as $pin) {
                foreach($tablearray as $pinrecord) {
                    if ($pinrecord == $pin) {
                        $_recordfound = true;
                        break;
                    }
                }
                if (!$_recordfound) {
                    $tablearray[] = $pin;
                }
            }
        }
        asort($tablearray);
        return $tablearray;
    }

    function pinRecords($RecordArray, $p_recordid=false, $add=false) {
        global $polaris;
        global $_GVARS;
        global $_CONFIG;
        global $TABLEPREFIX;

        $plrdatabase = new base_plrDatabase($polaris);
        $plrdatabase->LoadRecordHashed($RecordArray['_hdnDatabase']);
        $plrdatabase->connectUserDB();

        if (isset($RecordArray['_hdnPlrSes'])) {
            $polaris->polarissession = $RecordArray['_hdnPlrSes'];
        } else {
            $polaris->polarissession = '';
        }

        // get the masterform from the construct that is passed as parameter
        // or get the form from the formhash that is passed as parameter
        if (!empty($RecordArray['_hdnPinConstruct'])) {
            $plrconst = $polaris->currentApp->GetConstruct($RecordArray['_hdnPinConstruct'], $metaname = true);
            $plrform = $plrconst->masterform;
        } else {
            $plrform = new base_plrForm($polaris);
            $plrform->LoadRecordHashed($RecordArray['_hdnForm']);
        }

        $_tablename = $RecordArray['_hdnTable'];
        $_justtablename = explode('.',$_tablename);
        if (isset($_justtablename[1]))
            $_justtablename = $_justtablename[1];
        else
            $_justtablename = $_justtablename[0];

        $_primkeys = $plrdatabase->metaPrimaryKeys($_justtablename);

        if (!empty($_SESSION['pinrecords']) and !empty($_SESSION['pinrecords'][$polaris->polarissession]))
            $_currentpinitems = $_SESSION['pinrecords'][$polaris->polarissession][$_tablename];
        // remove pins from all other tables
        unset($_SESSION['pinrecords'][$polaris->polarissession]);

        $recordids = explode('|', urldecode($p_recordid));
        if (!empty($_CONFIG['maxpinrecords'])) {
            // limit the number of $recordids to the maxpinrecords
            if (count($recordids) > $_CONFIG['maxpinrecords']) {
                $recordids = array_slice($recordids, 0, $_CONFIG['maxpinrecords']);
            }
        }

        if (isset($recordids)) {
            foreach($recordids as $recid) {
                $recordid[] = $plrdatabase->customUrlDecode($recid);
            }
            $_keyfieldselect = $plrdatabase->makeEncodedKeySelect($_tablename, $recordid, $TABLEPREFIX);
        }
        /* else {
            foreach($RecordArray as $recid => $onoff) {
                if (substr($recid, 0, 4) == 'rec@') {
                    $recordid[] = $plrdatabase->customUrlDecode(substr($recid, 4, 32));
                }
            }
            $_keyfieldselect = $plrdatabase->makeEncodedKeySelect($_tablename, $recordid);
        }
        */
        if ($_primkeys) {
            $_primkeyscolumns = implode(',',$_primkeys);
            $_primkeyscolumns .= ' ,';
        }

        $showcolumns = '';
        if ($plrform->record->SELECTSHOWCOLUMNS != '') {
            $_showcolumns = $plrform->record->SELECTSHOWCOLUMNS;
            $_showcolumns = explode(',', $_showcolumns);
            $_showcolumns = implode(',\' \',',$_showcolumns);
            $_showcolumns = explode(',', $_showcolumns);
            $showcolumns = implode($plrdatabase->userdb->concat_operator, $_showcolumns).' AS SHOWVALUE, ';
        }
        $_rowidcolumn = $plrdatabase->makeRecordIDColumn($_tablename, $TABLEPREFIX);
        $_sql = "SELECT {$_primkeyscolumns} {$showcolumns} {$_rowidcolumn} AS PLR__RECORDID FROM $_tablename $TABLEPREFIX WHERE $_keyfieldselect";

        $plrdatabase->userdb->SetFetchMode(ADODB_FETCH_ASSOC);
        $_rs = $plrdatabase->userdb->GetAll($_sql);

        if ($plrform->record->SELECTQUERY != '') {
            if (!$p_recordid) {
                $_keyfieldselect = $plrdatabase->makeEncodedKeySelect($_tablename, $recordid, 'ttt'); /* main table should always have ttt alias*/
            }
            $plrdatabase->userdb->SetFetchMode(ADODB_FETCH_ASSOC);
            $_extrainfo = $plrdatabase->userdb->GetAll(sqlCombineWhere($plrform->record->SELECTQUERY, "(".$_keyfieldselect.")"));
        }

        // add the original location of the pinned records, and the extra info for the records
        foreach($_rs as $_key => $_record) {
            $_rs[$_key]['APP'] = $_GET['app'];
            $_rs[$_key]['CONST'] = $RecordArray['_hdnPinConstruct'] ?? $_GET['const'];
            $_rs[$_key]['EXTRAINFO'] = $_extrainfo[$_key] ?? '';
            $_pinformname = explode(';',$plrform->record->ITEMNAME)[0];
            if (empty($_pinformname))
                $_pinformname = $plrform->record->FORMNAME;
            $_rs[$_key]['FORMNAME'] = $polaris->i18n($_pinformname);
        }

        if ($RecordArray['_hdnKeyPressed'] == 'ctrl' or $add)
            $_SESSION['pinrecords'][$polaris->polarissession][$_tablename] = $this->CombinePinnedRecords($_currentpinitems, $_rs);
        else
            $_SESSION['pinrecords'][$polaris->polarissession][$_tablename] = $_rs;
        return true;
    }

    function pinOneRecord($RecordArray, $recordid=false, $add=false) {
        $result = $this->pinRecords($RecordArray, $recordid, $add);
        return $result;
    }

}
