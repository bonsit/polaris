<?php

class controllers_plr_save extends controllers_handlerBase {

    function __construct($eventname) {
        parent::__construct($eventname);
    }

    function handleEvent() {
        global $_CONFIG;
        global $polaris;

        if (isset($_SESSION['plrauth']) and $_SESSION['plrauth']) {
            unset($_SESSION['prevsaveresult']);

            if (isset($_POST['_hdnProcessedByModule']) and $_POST['_hdnProcessedByModule'] == 'true') return false;
            require_once('includes/mainactions.inc.php');
            $saveresult = SaveRecord($_POST);
            if ($saveresult) {
                switch ($_POST['_hdnState']) {
                case 'insert':
                    $_SESSION['prevaction'] = lang_get('item_added');
                    $_SESSION['prevsaveresult'] = $saveresult;
                    break;
                case 'edit':
                    $_SESSION['prevaction'] = lang_get('changes_saved');
                    /**
                    * Keep the saved row in SESSION
                    * so we can yellowfade the changed row;
                    */
                    $_SESSION['changedrowid'] = 'row@'.$_POST['_hdnRecordID'];
                    break;
                }
            }

            $gotourl = $_SERVER['REQUEST_URI'];
            $gotourl = getFragmentFromQuery($gotourl);

            if ($_REQUEST['output'] == 'json') {
                // if ajax call, then don't jump to other URL. Let Javascript handle that.
                return $saveresult;
            } else {
                // if the record is insert in 'SELECT' mode then jump (or search) to the newly inserted record
                if ( isset($_POST['_hdnSelectField']) ) {
                    $gotourl = remove_query_arg($gotourl, 'q\[\]');
                    $gotourl = remove_query_arg($gotourl, 'qc\[\]');
                    $fields = explode(',', $_POST['_hdnSelectField']);

                    foreach($fields as $fld) {
                        $gotourl = add_query_arg($gotourl, ['qc[]','qv[]'], [$fld, $_POST[$fld]]);
                    }
                }
                // goto the added record as /edit/
                if ( $_POST['_hdnKeepOpen'] or $_POST['_hdnAlwaysKeepOpen'] ) {
                    $databaseid = $_POST['_hdnDatabase'];
                    $plrdatabase = new base_plrDatabase($polaris);
                    $plrdatabase->LoadRecordHashed($databaseid);
                    $plrdatabase->connectUserDB();

                    // locate ROWID
                    if ($_POST['_hdnState'] == 'edit' and isset($_POST['_hdnRecordID']) and $_POST['_hdnAlwaysKeepOpen']) {
                        $urlparts = parse_url($gotourl);
                        $_recordID = $_POST['_hdnRecordID'];
                        $_recordID = $plrdatabase->customUrlEncode($_recordID);
                        $gotourl = $urlparts['path']."edit/$_recordID/?".$urlparts['query'];
                    }
                    elseif ( $_POST['_hdnKeepOpen'] and $_POST['_hdnState'] == 'insert') {
                        if (is_array($saveresult)) {
                            $_keys = array_keys($saveresult);
                            $_sql = "SELECT ROWIDTOCHAR(ROWID) FROM ".$_POST['_hdnTable']." WHERE {$_keys[0]} = '{$saveresult[$_keys[0]]}'";
                            $_recordID = $plrdatabase->userdb->GetOne($_sql);
                            $_recordID = $plrdatabase->customUrlDecode($_recordID);
                            $urlparts = parse_url($gotourl);
                            $gotourl = $urlparts['path']."edit/$_recordID/?".$urlparts['query'];
                        }
                    }
                }
                // keep insert form active (or not)
                if ( $_POST['_hdnNewAction'] )
                    $gotourl = add_query_arg($gotourl, ['action','stay'], ['insert','true']);
                // open the edit form again so the user can insert detailrecords
                if ( $_POST['_hdnNewDetailAction'] ) {
                    $gotourl = add_query_arg($gotourl, ['action','stay','rec','detailaction'], ['edit','true',$_GET['rec'],'insert']);
                    if ($_GET['detailrec'])
                        $gotourl = add_query_arg($gotourl, ['detailrec'], [$_GET['detailrec']]);
                }
                if ( !$GLOBALS['errorhasoccured'] and !$GLOBALS['debuggingview'] ) {
                    header('Location: '.$gotourl);
                }
            }
        }
    }
}
