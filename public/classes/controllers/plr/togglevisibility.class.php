<?php

class controllers_plr_togglevisibility extends controllers_handlerBase {

    function __construct($eventname) {
        parent::__construct($eventname);
    }

    function handleEvent() {
        if (isset($_SESSION['plrauth']) and $_SESSION['plrauth']) {
            require_once('includes/mainactions.inc.php');
            $this->updateVisibleState($_POST);
            header('Location: '.$_SERVER['REQUEST_URI']);
        }
    }

    function updateVisibleState($RecordArray) {
        global $polaris;

        $plrdatabase = new base_plrDatabase($polaris);
        $plrdatabase->LoadRecordHashed($RecordArray['_hdnDatabase']);
        $plrdatabase->connectUserDB();

        $tablename = $RecordArray['_hdnTable'];
        // convert url-safe ROWID back to real Oracle ROWID
        $recordid = $plrdatabase->customUrlDecode($RecordArray['_hdnVisibleRecord']);

        $keyfieldselect = $plrdatabase->makeEncodedKeySelect($tablename, $recordid);
        $fields = array($RecordArray['_hdnVisibleField'] => $RecordArray['_hdnVisibleValue']);
        $plrdatabase->updateRecord($tablename, $keyfieldselect, $fields);
    }
}
