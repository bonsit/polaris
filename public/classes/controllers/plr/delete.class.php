<?php

class controllers_plr_delete extends controllers_handlerBase
{
    function __construct($eventname) {
        parent::__construct($eventname);
    }

    function handleEvent() {
        global $_CONFIG;

        if (isset($_SESSION['plrauth']) and $_SESSION['plrauth']) {
            if (!isset($_POST['_hdnProcessedByModule']) or (isset($_POST['_hdnProcessedByModule']) and !($_POST['_hdnProcessedByModule'] == 'true'))) {
                require_once 'includes/mainactions.inc.php';
                if (isset($_POST['_hdnRecordID'])) {
                    $affectedRows = $this->deleteRecordsAjax($_POST);
                    return $affectedRows;
                }
            }
        }
    }

    function deleteRecordsAjax($RecordArray) {
        global $polaris;

        $databaseid = $RecordArray['_hdnDatabase'];
        $tablename = $RecordArray['_hdnTable'];
        $recordid = $RecordArray['_hdnRecordID'];

        $plrdatabase = new base_plrDatabase($polaris);
        $plrdatabase->LoadRecordHashed($databaseid);
        $plrdatabase->connectUserDB();

        // convert url-safe ROWID back to real Oracle ROWID
        $recordid = $plrdatabase->customUrlDecode($recordid);
        $recordids = explode('|', $recordid);
        $keyfieldselect = $plrdatabase->makeEncodedKeySelect($tablename, $recordids);
        $affectedRows = $plrdatabase->deleteRecord($tablename, $keyfieldselect);
        return $affectedRows;
    }

}
