<?php

class controllers_plr_hotupdate extends controllers_handlerBase {

    function __construct($eventname) {
        parent::__construct($eventname);
    }

    function handleEvent() {
        global $polaris;

        if (isset($_SESSION['plrauth']) and $_SESSION['plrauth']) {
            require_once('includes/mainactions.inc.php');

            return $this->hotUpdate($_POST);
        }
    }

    function hotUpdate($RecordArray) {
        global $polaris;
        global $ADODB_FETCH_MODE;

        $databaseid = $RecordArray['_hdnDatabase'];
        $tablename = $RecordArray['_hdnTable'];
        $formhash = $RecordArray['_hdnForm'];

        $plrdatabase = new base_plrDatabase($polaris);
        $plrdatabase->LoadRecordHashed($databaseid);
        $plrdatabase->connectUserDB();
        $_prevfetchmode = $ADODB_FETCH_MODE;
        $plrdatabase->userdb->SetFetchMode(ADODB_FETCH_ASSOC);

        $recordids = explode(';', $plrdatabase->customUrlDecode($RecordArray['_hdnHotUpdateRecord']));
        $fields = explode(';', $RecordArray['_hdnHotUpdateFields']);
        $values = explode(';', $RecordArray['_hdnHotUpdateValues']);
        foreach ($recordids as $_key => $recordid) {
            $keyfieldselect = $plrdatabase->makeEncodedKeySelect($tablename, $recordid);

            if (count($fields) == 1 and count($values) > 1) {
                // Only one field was given, so we assume
                // that we are changing a number of records, with the same column
                // name and different values
                $fieldvalues[$fields[0]] = $values[$_key];
            } else {
                // Field and values arrays are of same length, so we assume
                // that we are changing multiple fields in the same record
                $fieldvalues = array_combine($fields, $values);
            }

            // convert url-safe ROWID back to real Oracle ROWID
            $fieldvalues = $polaris->ConvertFloatValues($fieldvalues);
            $_updated[] = $plrdatabase->updateRecord($tablename, $keyfieldselect, $fieldvalues);
            $_newvalues[] = $plrdatabase->userdb->GetRow("SELECT * FROM $tablename WHERE $keyfieldselect");
        }
        $plrdatabase->userdb->SetFetchMode($_prevfetchmode);
        return array('result'=>in_array(true, $_updated), 'new_values'=>$_newvalues);
    }
}
