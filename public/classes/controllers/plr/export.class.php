<?php

class controllers_plr_export extends controllers_handlerBase {

    function __construct($eventname) {
        parent::__construct($eventname);
    }

    function handleEvent() {
        global $_GVARS;

        if (isset($_SESSION['plrauth']) and $_SESSION['plrauth']) {

            if ($_REQUEST['_hdnProcessedByModule'] == 'true') return false;

            $app = $_REQUEST['app'];
            $const = $_REQUEST['const'];

            switch($_REQUEST['exportresult']) {
            case 'export_all':
                $limit = false;
            break;
            case 'export_limit':
                $limit = $_REQUEST['exportlimit'];
            break;
            case 'export_search':
                $q = $_REQUEST['q'];
                $tag = $_REQUEST['tag'];
            break;
            }

            $offset = false;
            if (isset($_REQUEST['exportoffset'])) {
                $offset = $_REQUEST['exportoffset'];
            }

            switch($_REQUEST['exporttype']) {
            case 'export_csv_tab':
                $output = 'csv';
                $subformat = 'tab';
            break;
            case 'export_csv_comma':
                $output = 'csv';
                $subformat = 'comma';
            break;
            case 'export_xls':
                $output = 'xls';
                $subformat = false;
            break;
            case 'export_xml':
                $output = 'xml';
                $subformat = false;
            break;
            case 'export_msxml':
                $output = 'xml';
                $subformat = 'ms-xml';
            break;
            case 'export_html':
                $output = 'xhtml';
            break;
            }

            $exporturl = $_GVARS['serverroot'].'/services/'.$output.'/app/'.$app.'/const/'.$const.'/?dest=tofile';
            if ($subformat) {
                $exporturl .= "&subformat={$subformat}";
            }
            if ($limit) $exporturl .= '&limit='.$limit;
            if ($offset) $exporturl .= '&offset='.$offset;
            if ($tag) $exporturl .= '&tag='.$tag;
            if (isset($q)) $exporturl .= '&q='.$q;
            header('Location: '.$exporturl);
        }
    }
}
