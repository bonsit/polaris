<?php

class controllers_plr_deletepin extends controllers_handlerBase {

    function __construct($eventname) {
        parent::__construct($eventname);
    }

    function handleEvent() {
        global $polaris;

        if (isset($_SESSION['plrauth']) and $_SESSION['plrauth']) {
            if (isset($_POST['_hdnDatabase']) and isset($_POST['_hdnTable']) and isset($_POST['_hdnRecordID'])) {
                $result = $this->deletePin($_POST['_hdnDatabase'], $_POST['_hdnTable'], $_POST['_hdnRecordID']);
                return true;
            } else {
                return false;
            }
        }
    }

    function deletePin($databaseid, $tablename, $recordid) {
        global $polaris;

        if ($tablename == 'ALL' and $recordid = 'ALL') {
            unset($_SESSION['pinrecords'][$polaris->polarissession]);
        } else {
            $plrdatabase = new base_plrDatabase($polaris);
            $plrdatabase->LoadRecordHashed($databaseid);
            $plrdatabase->connectUserDB();

            $recordid = $plrdatabase->customUrlDecode($recordid);

            foreach($_SESSION['pinrecords'][$polaris->polarissession][$tablename] as $_key => $_pin) {

                foreach($_pin as $_key2 => $_value){
                    if(strpos($_key2, 'MD5(CONCAT(') !== false){
                        $recordidKey = $_value;
                        break;
                    }
                }

                if ($_pin['PLR__RECORDID'] == $recordid or ($recordidKey ?? false) == $recordid) {
                    unset($_SESSION['pinrecords'][$polaris->polarissession][$tablename][$_key]);
                }
            }
            if (count($_SESSION['pinrecords'][$polaris->polarissession][$tablename]) == 0) {
                unset($_SESSION['pinrecords'][$polaris->polarissession][$tablename]);
            }
        }

        if (count($_SESSION['pinrecords'][$polaris->polarissession]) == 0) {
            unset($_SESSION['pinrecords'][$polaris->polarissession]);
        }
        return true;
    }
}
