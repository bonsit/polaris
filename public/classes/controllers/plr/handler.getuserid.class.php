<?php

class controllers_getuserid extends controllers_handlerBase {

    function __construct($eventname) {
        parent::__construct($eventname);
    }

    function handleEvent() {
        return array('result'=>$_SESSION['userid']);
    }

}

?>