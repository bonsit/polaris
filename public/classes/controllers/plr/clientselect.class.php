<?php

class controllers_plr_clientselect extends controllers_handlerBase {

    function __construct($eventname) {
        parent::__construct($eventname);
    }

    function handleEvent() {
        if (isset($_SESSION['plrauth']) and $_SESSION['plrauth']) {
            if (!($_POST['_hdnProcessedByModule'] == 'true')) {

                if (isset($_REQUEST['clientselect'])) {
                    $_SESSION['clientselect'] = $_REQUEST['clientselect'];
                }
            }
        }
    }
}
