<?php

class controllers_plr_recordcount extends controllers_handlerBase {

    var $currentApp;
    var $currentconstruct;
    var $currentform;

    function __construct($eventname) {
        parent::__construct($eventname);
    }

    function handleEvent() {
        global $polaris;

        if (isset($_SESSION['plrauth']) and $_SESSION['plrauth']) {

            if ($_POST['_hdnProcessedByModule'] == 'true') return false;

            $_appId = isset($_GET['app'])?$_GET['app']:false;
            if ($_appId) {
                $this->currentApp =& $polaris->plrClient->LoadApplication($_appId, $metaname=true);
            } else {
                $this->currentApp =& $polaris->plrClient->applications->DefaultApplication();
            }

            $this->currentconstruct = false;
            $this->currentform = false;

            if (isset($_GET['const']) or isset($_GET['form']) ) {
                if (isset($_GET['const'])) {
                    $this->currentconstruct =& $this->currentApp->GetConstruct($_GET['const'], $metaname=true);
                    $this->currentform = $this->currentconstruct->masterform;
                } else {
                    $this->currentform =& $this->currentApp->GetForm($_GET['form'], $metaname=true);
                }
            } else {
                $this->currentconstruct =& $this->currentApp->GetDefaultConstruct($metaname=true);
                $this->currentform = $this->currentconstruct->masterform;
            }

            $this->currentform->database->connectUserDB();
            echo $this->currentform->GetRecordCount('','', $persistentcount=true);
            exit;
        }
    }
}
