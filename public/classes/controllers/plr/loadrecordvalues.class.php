<?php

class controllers_plr_loadrecordvalues extends controllers_handlerBase {

    function __construct($eventname) {
        parent::__construct($eventname);
    }

    function handleEvent() {
        global $polaris;

        $_result = '';
        if (isset($_SESSION['plrauth']) and $_SESSION['plrauth']) {
            $_databaseHash = $_REQUEST['_hdnDatabase'];
            $_tableName = $_REQUEST['_hdnTable'];
            $_recordIDs = explode(',',$_REQUEST['_hdnRecordIDs']);

            $database = new base_plrDatabase($polaris);
            $database->LoadRecordHashed($_databaseHash);
            $userdb = $database->connectUserDB();

            $_rowid = $database->makeRecordIDColumn($_tableName, 'ttt');
            $_recordIDs = $database->customUrlDecode($_recordIDs);
            $_where = $database->makeEncodedKeySelect($_tableName, $_recordIDs);
            $_sql = "SELECT {$_rowid} AS PLR__RECORDID, ttt.* FROM {$_tableName} ttt WHERE {$_where}";
            $userdb->SetFetchMode(ADODB_FETCH_ASSOC);
            $_rs = $userdb->GetAll($_sql);
            return array('result'=>true, 'new_values'=>$_rs);
        }
    }
}
