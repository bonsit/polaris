<?php

class controllers_check2fa extends controllers_handlerBase {

    function __construct($eventname) {
        parent::__construct($eventname);
    }

    function handleEvent() {
        global $polaris;

        $_twofacodecheck = trim($_POST['twofacodecheck']);
        $_SESSION['2fachecked'] = $polaris->check2FACode($_twofacodecheck);
    }

}
