<?php

class controllers_login extends controllers_handlerBase {

    function __construct($eventname) {
        parent::__construct($eventname);
    }

    function processLogin($name, $password) {
        global $polaris;

        $_userObject = new base_plrUserGroup($polaris);
        $_userObject->LoadRecordByName($name);
        $_authType = $_userObject->GetAuthorisationType();
        $classname = "controllers_usergroupHandler{$_authType}";

        $ugHandler = new $classname();
        $ugHandler->logon($name, $password, $_salt=false);
    }

    function handleEvent() {
        global $polaris;
        global $_CONFIG;
        global $_GVARS;

        // allways register the name so the next LoginForm shows the name of the previous try
        $_name = trim($_POST['username']);
        $_password = $_POST['password'];
        $_SESSION['name'] = $_name;

        if (isset($_POST['plrinstance'])) {
            safeSetCookie('plrinstance', $_POST['plrinstance']);
            $polaris->initialize($forceReconnect=true);
        }
        $this->processLogin($_name, $_password);
    }

}
