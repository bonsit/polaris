<?php

class controllers_Dash_AddContent extends controllers_handlerBase
{
    function __construct($eventname) {
        parent::__construct($eventname);
    }

    function handleEvent() {
        if (isset($_GET['type']) and isset($_SESSION['plrauth'])) {
            $dashboard = new base_plrDashboard(null);

            switch($_GET['type']) {
            case 'PLR': return $dashboard->addPLRForm($_GET['title'],$_GET['url']);
            break;
            case 'URL': return $dashboard->addURLForm($_GET['title'],$_GET['url']);
            break;
            case 'RSS': return $dashboard->addRSSForm($_GET['title'],$_GET['url']);
            break;
            }
        } else {
            return false;
        }
    }
}
