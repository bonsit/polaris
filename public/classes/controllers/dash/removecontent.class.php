<?php

class controllers_Dash_RemoveContent extends controllers_handlerBase {

    function __construct($eventname) {
        parent::__construct($eventname);
    }

    function handleEvent() {
        if (isset($_GET['id']) and isset($_SESSION['plrauth'])) {
            $dashboard = new base_plrDashboard(null);
            return $dashboard->deleteRecord($_GET['id']);
        }
	}

}
