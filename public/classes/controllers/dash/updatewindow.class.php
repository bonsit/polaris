<?php

class controllers_Dash_UpdateWindow extends controllers_handlerBase
{
    function __construct($eventname) {
        parent::__construct($eventname);
    }

    function handleEvent() {
        if (isset($_GET['id']) and isset($_SESSION['plrauth'])) {
            $dashboard = new base_plrDashboard(null);
            return $dashboard->updateWindow($_GET['id'], $_GET['top'], $_GET['bottom'], $_GET['left'], $_GET['right'], $_GET['width'], $_GET['height']);
        } else {
            return false;
        }
    }

}
