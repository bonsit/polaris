<?php
class controllers_dispatcherBase {
    private $event;

    public function __construct(?string $eventname = null) {
        $this->event = $eventname;
    }

    /**
     * Process the event and handle it using the appropriate controller.
     *
     * @return mixed The response from the controller's event handler.
     */
    function processEvent() {
        if ($this->event !== null) {
            $classname = "controllers_".$this->event;
            if (class_exists($classname, true)) {
                $handlerObj = new $classname($this->event);
                if ($handlerObj instanceof controllers_handlerBase) {
                    $_result = $handlerObj->handleEvent();
                    return $_result;
                } else {
                    trigger_error("The event handler does not extend ControllersBase: ".$this->event, E_USER_WARNING);
                }
            } else {
                trigger_error("Cannot handle the event: ".$this->event, E_USER_WARNING);
            }
        }
        return null;
    }
}
