<?php

// require_once('eventdispatcher._base.class.php');
require_once PLR_DIR."/classes/controllers/logout.class.php";
/***
    Events that can take place in the Dashboard environment
**/

class controllers_dispatcherDashboard extends controllers_dispatcherBase {

    function __construct($eventname) {
        parent::__construct($eventname);
    }
}
