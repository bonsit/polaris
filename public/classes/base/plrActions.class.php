<?php

class base_plrActions extends base_plrRecords {
    var $actions;

    function __construct(&$aowner, $group='BOTTOM') {
        parent::__construct($aowner);

        $this->keyfields = array('CLIENTID', 'FORMID', 'ACTIONID');
        $this->LoadRecords($group);
    }

    function LoadRecords($group) {
        global $_sqlActions;

        if (empty($_SESSION['userid'])) return;

        $_po = $this->polaris();
        $usergroups = $_po->GetGroupValues($_SESSION['userid']);

        $_sqlActions = str_replace('#subselect#', $usergroups, $_sqlActions);
        $rs = $this->dbExecute($_sqlActions, array($_SESSION['clientid'], $this->owner->record->FORMID)) or Die('query failed actions: '.$this->dbErrorMsg());
        $this->actions = array();
        while ($rec = $rs->FetchNextObject(true)) {
            /***
             * Check if current user has access to this Action
             */
            $plrAction = new base_plrAction($this);
            $plrAction->record = $rec;
            $this->actions[] = $plrAction;
        }
    }

    function ShowActions($state='list', $group='BOTTOM', $size='medium') {
        return;
        if (count($this->actions) > 0) {
            echo '<ul id="formactionlist" xstyle="display:none">';
            foreach(array_keys($this->actions) as $index ) {
                $action =& $this->actions[$index];
                if ($action->NotConstrainedByConstruct() && $action->PartOfGroup($group)) {
                    echo "<li>";
                    $action->Show($index, $state, $size);
                    echo "</li>";
                }
            }
            echo '</ul>';
        }
    }

    function ShowActionAsDropdownItem($state='list', $detailstate=false, $group='BOTTOM', $size='medium') {
        if (count($this->actions) > 0) {
            foreach(array_keys($this->actions) as $index ) {
                $action =& $this->actions[$index];
                if ($action->NotConstrainedByConstruct() && $action->PartOfGroup($group)) {
                    $action->Show($index, $state, $detailstate, $size, true);
                }
            }
        }
    }

    function ShowActionButtons($state='list', $detailstate=false, $group='BOTTOM', $size='medium') {
        if (count($this->actions) > 0) {
            foreach(array_keys($this->actions) as $index ) {
                $action =& $this->actions[$index];
                if ($action->NotConstrainedByConstruct() && $action->PartOfGroup($group)) {
                    $action->Show($index, $state, $detailstate, $size);
                }
            }
        }
    }

    function GetGeneralActionButtons($state='list', $group='BOTTOM', $size='medium') {
        $_result = '';
        if (count($this->actions) > 0) {
            foreach(array_keys($this->actions) as $index ) {
                $action =& $this->actions[$index];
                if ($action->NotConstrainedByConstruct() && $action->PartOfGroup($group)) {
                    $_result .= $action->GetAction($index, $state, $size);
                }
            }
        }
        return $_result;
    }
}
