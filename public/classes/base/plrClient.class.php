<?php

class base_plrClient extends base_plrRecord {
  var $clientid;
  var $applications;
  var $meta;
  var $clientparameters;
  var $_cachedatascope = Array();

  function __construct($aowner, $clientid=false, $meta=false) {
      parent::__construct($aowner);
      $this->__polaris = $aowner;

      $this->meta = false;
      if ($clientid) {
          $this->clientid = $clientid;
      } else {
          if (isset($_GET['cln'])) {
              $this->clientid = $_GET['cln'];
              $this->meta = true;
          } elseif (isset($_SESSION['clientid'])) {
              $this->clientid = $_SESSION['clientid'];
          }
      }
      if ($this->clientid) {
          $this->LoadRecord($meta);
          $this->applications = new base_plrApplications($this);
      }
  }

  function LoadRecord($meta=false) {
      global $_sqlClient;
      global $_sqlClientMETA;

      if (($_SESSION['usertype'] ?? null) == 'root' or ($this->meta or $meta) ) {
          $rs = $this->dbExecute($_sqlClientMETA, array($this->clientid));
      } else {
          $rs = $this->dbExecute($_sqlClient, array($this->clientid));
      }
      if ($rs) {
          $this->record = $rs->FetchNextObject(true);
      }

      $this->ApplyUserSessionVariables();
  }

  function &LoadApplication($appid, $metaname=false) {
      global $_sqlApplication;
      global $_sqlApplicationMeta;
      global $_sqlApplicationAdmin;

      $aApplication = new base_plrApplication($this);
      $aApplication->LoadRecord($key = array($this->record->CLIENTHASHID, $appid), $encoded = false, $metaname);
      $aApplication->createApplicationLink();
      return $aApplication;
  }

  function getGlobalScrambler() {
    global $scramble;
    return $scramble;
  }

  function ApplyUserSessionVariables() {
      $sessiondatascope = $this->GetDataScope(null, '%session%');
      if ($sessiondatascope)
      foreach ($sessiondatascope as $sesvar => $sesvalues) {
          if ($sesvalues['operand'] == '=') {
              $_SESSION[$sesvar] = $sesvalues['filtervalue'];
          }
      }
  }

  function LoadClientParameters() {
    global $polaris;
    global $scramble;

    if (!empty($this->record->PARAMETERTABLENAME)) {
        if (empty($_SESSION['clientparameters'])) {
            $_paramTable = $this->record->PARAMETERTABLENAME;
            // assume (wrongly, for now) that the databaseid is always 1
            $_databaseid = 1;
            $_sql = "SELECT PARAMETERCODE, WAARDE FROM {$_paramTable} ttt WHERE CLIENT_HASH = '{$this->record->CLIENTHASHID}' AND __DELETED = 0";
            $_tempDb = new base_plrDatabase($polaris);
            $_tempDb->LoadRecord([$this->record->CLIENTID, $_databaseid]);
            if (isset($_tempDb->record)) {
                $_tempDb->connectUserDB();
                $_tempDb->userdb->setFetchMode(ADODB_FETCH_ASSOC);
                $_rs = $_tempDb->userdb->GetAssoc($_sql);
            }
            $_SESSION['clientparameters'] = $_rs;
        }
        $this->clientparameters = $_SESSION['clientparameters'];
    }
  }

  function getNewFormId() {
    global $polaris;

    $_formid = $this->dbGetOne("SELECT MAX(formid) FROM plr_form WHERE clientid = ".$this->record->CLIENTID);

    if (!isset($_formid))
        $_formid = 1;
    else
        $_formid++;
    return $_formid;
  }

  function &GetSharedModule($module, $moduleid, $moduleparam) {
     global $_GVARS;

     $_clientname = '_shared';

     $modulefile = PLR_DIR.DIRECTORY_SEPARATOR.$_GVARS['module_dir'].DIRECTORY_SEPARATOR.$_clientname.DIRECTORY_SEPARATOR.'module.'.strtolower($module).'.php';
     if (isset($module) and (file_exists($modulefile))) {
       require($modulefile);
       $moduleobj = new Module($moduleid, $module, $_clientname);
       $moduleobj->params = $moduleparam;
       return $moduleobj;
     } else {
       echo "$module does not exists ($modulefile)";
     }
  }

  function &GetModule($module, $moduleid, $moduleparam, $clientname=false) {
     global $_GVARS;

     if ($clientname)
         $_clientname = $clientname;
     else {
        $_clientname = (substr($module,0,3) == 'plr')?'_shared':false; // assume we are using the _shared module when directly calling them from the url
        if (!$_clientname) {
            $_clientname = str_replace(' ', '_', strtolower($this->record->NAME));
        }
     }
     $modulefile = PLR_DIR.DIRECTORY_SEPARATOR.$_GVARS['module_dir'].DIRECTORY_SEPARATOR.$_clientname.DIRECTORY_SEPARATOR.'module.'.strtolower($module).'.php';
     if (isset($module) and (file_exists($modulefile))) {
       require_once($modulefile);
       $className = 'Module'.$module;
       $moduleobj = new $className(...[$moduleid, $module, $_clientname]);
       $moduleobj->params = $moduleparam;
       return $moduleobj;
     } else {
       echo "$module does not exists ($modulefile: $module)";
     }
  }

  function ExecuteCustomClientHandler($method, $post) {
        global $polaris;

        $_client = new base_plrClient($polaris, $post['CLIENTHASH'], true);

        $_clientSharedModule = $this->GetModule($module='shared', $moduleid=null, $moduleparam=null);
        $_clientSharedModule->client = $_client;
        if (method_exists($_clientSharedModule, $method)) {
            return $_clientSharedModule->$method($post);
      }

      return false;
  }

  function GetDataScope($databaseid, $tablename, $onlystoragescope=false) {
      global $_sqlDataScopeForTable;

      $result = false;
      if (isset($this->_cachedatascope[$tablename]) or isset($this->_cachedatascope['*'])) {
            $result = $this->_cachedatascope[$tablename];
      } else {
            $_sessionUserID = $_SESSION['userid'] ?? null;
            $groupvalues = $this->owner->GetGroupValues($_sessionUserID);
            if (!empty($groupvalues))
                $groupvalues .= '@!integer';
            $_onlystoragescope = $onlystoragescope?'Y':'N';

            $rs = $this->dbGetAll($_sqlDataScopeForTable, [$this->clientid, $groupvalues, $databaseid, $tablename, $_onlystoragescope]);
            $result = [];
            if ($rs) {
                $po = $this->polaris();
                foreach($rs as $filter) {
                    $filter['filtervalue'] = $po->ReplaceDynamicVariables($filter['filtervalue']);
                    $_columnName = strtoupper($filter['columnname']);
                    if (isset($result[$_columnName])) {
                        $result[strtoupper($filter['columnname'])] = [
                            'operand' => 'IN'
                            , 'filtervalue' => $result[strtoupper($filter['columnname'])]['filtervalue'] . ', ' . $filter['filtervalue']
                            , 'applicationid'=> $filter['applicationid']
                            , 'constructid'=> $filter['constructid']
                        ];
                    } else {
                        $result[strtoupper($filter['columnname'])] = [
                            'operand' => $filter['operand']
                            , 'filtervalue' => $filter['filtervalue']
                            , 'applicationid'=> $filter['applicationid']
                            , 'constructid'=> $filter['constructid']
                        ];
                    }
                }
                $this->_cachedatascope[$tablename] = $result;
            }
      }
      return $result;
  }

  function GetDataScopeFilter($databaseid, $tablename, $tableprefix = 'ttt') {
      global $polaris;

      $scopevalues = $this->GetDataScope($databaseid, $tablename);
      if ($tableprefix != '') $tableprefix = $tableprefix.'.';
      $result = '';
      if (count($scopevalues) > 0) {
          foreach($scopevalues as $columnname => $value) {
              if (
                (!isset($value['applicationid'])
                or (isset($value['applicationid']) and $value['applicationid'] == $polaris->currentApp->record->APPLICATIONID))
                and
                (!isset($value['constructid'])
                or (isset($value['constructid']) and $value['constructid'] == $polaris->currentconstruct->record->CONSTRUCTID))
              ) {
                $result .= '('.$tableprefix.$columnname.' '.$value['operand'].' ';
                if ($value['operand'] == '=') $result .= '\'';
                if ($value['operand'] == 'IN') $result .= '(';
                $result .= $value['filtervalue'];
                if ($value['operand'] == '=') $result .= '\'';
                if ($value['operand'] == 'IN') $result .= ')';
                $result .= ') AND ';
              }
          }
          $result = substr($result, 0, -4);
      }
      return $result;
  }

  function getTaggedObjects($userid, $app, $tag) {

      $sql = '
      SELECT tag, t.clientid, userid, app, const, taggedon, form, record, recordcaption, search, url
      , a.applicationname, a.applicationid
      , c.constructname, c.constructid, u.usergroupname, count(u.usergroupname) as usercount
      FROM ((plr_tag t
      LEFT JOIN plr_application a
      ON t.clientid = a.clientid AND t.app = a.metaname)
      LEFT JOIN plr_construct c
      ON t.clientid = c.clientid AND a.applicationid = c.applicationid AND t.const = c.metaname)
      LEFT JOIN plr_usergroup u
      ON t.clientid = u.clientid AND t.userid = u.usergroupid
      WHERE t.clientid = ?
      AND t.tag = ?
      ';

      $keys = array($this->clientid, $tag);
      if ($userid) {
          $sql .= ' AND userid = ?';
          $keys[] = $userid;
      }
      if ($app) {
          $sql .= ' AND a.metaname = ?';
          $keys[] = $app;
      }

      $sql .= '
      GROUP BY t.record, t.search
      ORDER BY a.applicationname, c.constructname, taggedon desc
      ';
      include('includes/fuzzy_datetime.inc.php');
      $tagrs = $this->dbGetAll($sql, $keys);
      foreach($tagrs as $key => $tag) {
          if ($tagrs[$key]['taggedon'] != '')
              $tagrs[$key]['fuzzydate'] = FuzzyTime(strtotime($tagrs[$key]['taggedon']));
      }

      return $tagrs;
  }

  function getTags($userid=null) {
      $keys = array($this->clientid);
      $sql = '
      SELECT tag, clientid, userid, app, const, taggedon, form, record, url
      , count(tag) as tagcount
      FROM plr_tag
      WHERE clientid = ?
      ';
      if (isset($userid)) {
          $sql .= ' AND userid = ? ';
          $keys[] = $userid;
      }
      $sql .= '
      GROUP BY tag
      ORDER BY tag';

      $rs = $this->dbGetAll($sql, $keys);

      $countmin = 999;
      $countmax = 1;
      foreach($rs as $tagkey => $tagvalue) {
          if ($tagvalue['tagcount'] < $countmin) $countmin = $tagvalue['tagcount'];
          if ($tagvalue['tagcount'] > $countmax) $countmax = $tagvalue['tagcount'];
      }
      $min = sqrt($countmin);
      $max = sqrt($countmax);
      $factor = 0;

      if (($max - $min) == 0) {
          $min -= 24;
          $factor = 1;
      } else {
          $factor = 24 / ($max - $min);
      }

      foreach($rs as $tagkey => $tagvalue) {
          $rs[$tagkey]['level'] = (int)((sqrt($tagvalue['tagcount']) - $min) * $factor);
      }

      return $rs;
  }

}