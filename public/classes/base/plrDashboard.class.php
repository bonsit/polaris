<?php

class base_plrDashboard extends base_plrRecord {
    var $items;
    var $userdb;

    function __construct($aowner, $key=null) {
        parent::__construct($aowner, $key);
    }

    function LoadRecords() {
        global $_sqlDashboardItems;
        global $_GVARS;
        global $polaris;

        $currentlevel = 1;
        $groupvalues = $polaris->GetGroupValues($_SESSION['userid']).'@!integer';
        $in = $this->plrInstance();
        $this->items = $in->GetAll($_sqlDashboardItems, array($_SESSION['clientid'], $groupvalues, $groupvalues));
        if ($this->items)
            foreach($this->items as $key => $window) {
                if ($window['TYPE'] == 'RSS') {
                    $this->items[$key]['RSS'] = showRSSFeed($window['URL'], $maxitems=5);
                }
                if ($window['TYPE'] == 'PLR') {
                    $this->items[$key]['URL'] = $_GVARS['serverroot'] . $window['URL'];
                }
            }
    }

    function deleteRecord($id) {
        global $_sqlDeleteDashboardItem;

        $this->plrInstance()->Execute($_sqlDeleteDashboardItem, array($id, $_SESSION['clientid'], $_SESSION['userid']));
        return array('result'=>($this->plrInstance()->Affected_Rows() == 1), 'formid'=>$id);
    }

    function updateWindow($id, $top=null, $bottom=null, $left=null, $right=null, $width=null, $height=null) {
        global $_sqlUpdateDashboardItem, $_sqlCloneDashboardItem;

        if ($this->dashboardFormExists($id, $_SESSION['clientid'], $_SESSION['userid'])) {
            $this->plrInstance()->Execute($_sqlUpdateDashboardItem, array($top, $bottom, $left, $right, $width, $height, $id, $_SESSION['clientid'], $_SESSION['userid']));
            $result = $this->plrInstance()->Affected_Rows() == 1;
        } else {
            // No update because the form belongs to a group
            // Add another form for the current user based on the group form (clone it)
            $this->plrInstance()->Execute($_sqlCloneDashboardItem, array($_SESSION['userid'], $top, $bottom, $left, $right, $width, $height, $id) );
            $id = $this->plrInstance()->Insert_ID();
            $result = true;
        }
        return array('result'=>$result, 'formid'=>$id);
    }

    function getDashboardForm() {
        if ($this->record) {
            $this->items = new plrDashboardForm($this, $this->key);
            $this->items->LoadRecords();
            return $this->items;
        }
    }

    function dashboardFormExists($id, $clientid, $usergroupid) {
        global $_sqlDashboardItem;
        $rs = $this->plrInstance()->Execute($_sqlDashboardItem, array($id, $clientid, $usergroupid));
        return ($rs->RecordCount() == 1);
    }

    function getAllConstruct() {
        global $_sqlAllApplicationConstructs;
        global $polaris;

        $currentlevel = 1;
        $groupvalues = $polaris->GetGroupValues($_SESSION['userid']).'@!integer';

        $_instance = $this->plrInstance();
        $rs = $_instance->Execute($_sqlAllApplicationConstructs, array($groupvalues));
        $constructs = array();
        if ($rs) {
            while (!$rs->EOF) {
                $constructs[] = array(
                    'clientid'          => $rs->fields['clientid'],
                    'applicationid'     => $rs->fields['applicationid'],
                    'constructid'       => $rs->fields['constructid'],
                    'applicationname'   => $rs->fields['applicationname'],
                    'constructname'     => $rs->fields['constructname'],
                    'appmetaname'       => $rs->fields['appmetaname'],
                    'constmetaname'     => $rs->fields['constmetaname']
                );
                $rs->MoveNext();
            }
        }
        return $constructs;
    }

    function _addDashboardItem($type, $clientid, $userid, $title, $url=null, $applicationid=null, $constructid=null, $height=null, $width=null, $top=null, $left=null, $visible='Y') {
        global $_sqlAddDashboardForm;

        $this->plrInstance()->Execute($_sqlAddDashboardForm, array($type, $clientid, $userid, $title, $url, $applicationid, $constructid, $height, $width, $top, $left, $visible));
        return ($this->plrInstance()->Affected_Rows() == 1) ? $this->plrInstance()->Insert_ID() : false;
    }

    function addURLForm($title, $url) {
        $id = $this->_addDashboardItem($type='URL', $clientid=$_SESSION['clientid'], $userid=$_SESSION['userid'], $title, $url);
        return array('formid'=>$id, 'title'=>$title, 'url'=>$url);
    }

    function addRSSForm($title, $url) {
        $id = $this->_addDashboardItem($type='RSS', $clientid=$_SESSION['clientid'], $userid=$_SESSION['userid'], $title, $url);
        return array('formid'=>$id, 'title'=>$url, 'url'=>$url, 'rss'=>showRSSFeed($url, $maxitems=5));
    }

    function addPLRForm($title, $url) {
        $returnurl = '/services/rest'.$url;
        $id = $this->_addDashboardItem($type='URL', $clientid=$_SESSION['clientid'], $userid=$_SESSION['userid'], $title, $returnurl);
        return array('formid'=>$id, 'title'=>$title, 'url'=>$returnurl);
    }

}
