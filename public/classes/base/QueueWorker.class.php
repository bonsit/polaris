<?php

class base_QueueWorker
{
    protected $config;
    protected $db;
    protected $client;

    public function __construct() {
    }

    public function send_reset_email($data) {
        global $polaris;

        $_emailaddress = $data['email'];
        $_insertValues = $data['insertValues'];
        $_formhash = $_insertValues['FORMHASH'];
        $_replyruleid = 1;

        $_result = $polaris->ProcessSingleReplyRule($_formhash, $_replyruleid, $_emailaddress, $_insertValues);

        return true;
    }

    public function send_invite_mail($data) {
        global $polaris;

        $_formhash = $data['insertValues']['FORMHASH'];
        $_emailaddress = $data['email'];
        $_insertValues = $data['insertValues'];
        $_ID = $data['ID'];

        $_form = new base_plrForm($polaris);
        $_form->LoadRecordHashed($_formhash);
        $_form->database->connectUserDB();

        $_result = $_form->ProcessReplyRules($_emailaddress, $_insertValues);
        if ($_result->errorcode == 0 and !empty($_ID)) {
            $_record["STATUS"] = "UITGENODIGD";
            $_record["MESSAGEID"] = $_result->messageid;
            $_form->database->userdb->AutoExecute($_form->GetDataDestination(), $_record, 'UPDATE', 'ID = '.$_ID);
        }
        return true;
    }

    public function send_deelnemeruitnodiging($data) {
        global $polaris;
        global $scramble;

        try {
            $_emailaddress = $data['email'];
            $_insertValues = $data['insertValues'];
            $_formhash = $_insertValues['FORMHASH'];
            $_PZN_ID = $_insertValues['PZN'];

            $_form = new base_plrForm($polaris);
            $_form->LoadRecordHashed($_formhash);
            if ($_form->database) {
                $_form->database->connectUserDB();

                $_result = $_form->ProcessReplyRules($_emailaddress, $_insertValues);

                if ($_result->errorcode == 0 and !empty($_PZN_ID)) {
                    $_record["DATUM_UITGENODIGD"] = date("Y-m-d H:i:s");
                    $_record["STATUS"] = "VERSTUURD";
                    $_record["MESSAGEID"] = $_result->messageid;

                    $_recordSelector = $_form->database->makeRecordIDColumn('PZN_NETWERKDEELNEMER');

                    $_form->database->userdb->AutoExecute($_form->GetDataDestination(), $_record, 'UPDATE', $_recordSelector . " = '".$_insertValues['PLR__RECORDID']."'");
                }
            }
        } catch (Exception $e) {
            // rethrow
            throw $e;
        }
        return true;
    }

    public function send_coordinatorwissel_mail($data) {
        global $polaris;
        global $scramble;

        $_emailaddress = $data['email'];
        $_insertValues = $data['insertValues'];
        $_formhash = $_insertValues['FORMHASH'];
        $_PZN_ID = $_insertValues['PZN'];
        $_volgnummer = $_insertValues['ID'];

        $_form = new base_plrForm($polaris);
        $_form->LoadRecordHashed($_formhash);
        $_form->database->connectUserDB();
        $_result = $_form->ProcessReplyRules($_emailaddress, $_insertValues);

        if ($_result->errorcode == 0 and !empty($_PZN_ID)) {
            $_record["STATUS"] = "VERSTUURD";
            $_record["MESSAGEID"] = $_result->messageid;

            $_recordSelector = $_form->database->makeRecordIDColumn();
            $_form->database->userdb->AutoExecute($_form->GetDataDestination(), $_record, 'UPDATE', $_recordSelector . " = '".$_insertValues['PLR__RECORDID']."'");
        }
        return true;
    }

    public function send_cirkelmodelafgerond_mail($data) {
        global $polaris;

        $_emailaddress = $data['email'];
        $_insertValues = $data['insertValues'];
        $_formhash = $_insertValues['FORMHASH'];
        $_replyruleid = 2;
        $_PZN_ID = $_insertValues['PZN'];
        $_volgnummer = $_insertValues['ID'];

        $_result = $polaris->ProcessSingleReplyRule($_formhash, $_replyruleid, $_emailaddress, $_insertValues);

        return true;
    }

    public function send_pzningediend_mail($data) {
        global $polaris;

        $_emailaddress = $data['email'];
        $_insertValues = $data['insertValues'];
        $_formhash = $_insertValues['FORMHASH'];
        $_replyruleid = 3;
        $_PZN_ID = $_insertValues['PZN'];
        $_volgnummer = $_insertValues['ID'];

        $_result = $polaris->ProcessSingleReplyRule($_formhash, $_replyruleid, $_emailaddress, $_insertValues);

        return true;
    }

    public function send_rtogestart_mail($data) {
        global $polaris;

        $_emailaddress = $data['email'];
        $_insertValues = $data['insertValues'];
        $_formhash = $_insertValues['FORMHASH'];
        $_replyruleid = 2;
        $_PZN_ID = $_insertValues['PZN'];

        $_result = $polaris->ProcessSingleReplyRule($_formhash, $_replyruleid, $_emailaddress, $_insertValues);

        return true;
    }

    public function queue_mailsend_96dff811c798ba5b8f38e5c788bf4fff($data) {
        global $polaris;

        if (!is_array($data)) {
            return false;
        }

        $_formhash = $data['FORMHASH'];
        $_id = $data['insertid'];

        $_form = new base_plrForm($polaris);
        $_form->LoadRecordHashed($_formhash);

        if ($_form->database) {
            $_form->database->connectUserDB();

            $_result = $_form->ProcessReplyRules($_emailaddress, $_insertValues);

            if ($_result->errorcode == 0 and !empty($_PZN_ID)) {
                $_record["STATUS"] = "VERSTUURD";
                $_record["MESSAGEID"] = $_result->messageid;

                $_recordSelector = $_form->database->makeRecordIDColumn();
                $_form->database->userdb->AutoExecute($_form->GetDataDestination(), $_record, 'UPDATE', $_recordSelector . " = '".$_id."'");
            }

            return true;
        } else {
            return false;
        }
    }

}