<?php
    # Polaris - a weblapplication generator

    # Copyright (C) 2000 - 2005  Debster - bart Bons

    # This is the Import/Export module to copy Polaris applications

    # back and forth to different Polaris installations.

    # this module is accessable as a php class
    # and through query parameters (via plrimpexp.php):
    #   plr_impexp.php?action=export&clientid=n&applicationid=m
    #   >> result: Polaris export script (semi sql insert script)
    #
    #   plr_impexp.php?action=import&file=<filename>&clientid=n&applicationid=m
    #   >> result: imports a Polaris application into client n and application m

//require_once('/includes/dbsession.inc.php');
require_once('includes/global.inc.php');
require_once($_GVARS['docroot'].'/classes/base/plr_base.class.php');
require_once($_GVARS['docroot'].'/includes/basefunctions.inc.php');
require_once($_GVARS['docroot'].'/includes/dbroutines.inc.php');

define( 'CLIENTIDPLACEHOLDER', '##CLIENTID##' );
define( 'APPLICATIONIDPLACEHOLDER', '##APPLICATIONID##' );
define( 'DATABASEIDPLACEHOLDER', '##DATABASEID##' );

class base_plrImpExp extends base_plrRecord {

    var $exp_tables = array (
        'plr_action',
        'plr_form',
        'plr_formpermission',
        'plr_line',
        'plr_page',
        'plr_replyrule',
    );

    var $exp_app_tables = array (
        'plr_advertorial',
        'plr_alert',
        'plr_application',
        'plr_applicationpermission',
        'plr_bulkmail',
        'plr_construct',
        'plr_constructtree',
        'plr_menuitem',
        'plr_shortcut',
        'plr_shortcutsheet'
    );

    var $exp_db_tables = array (
       'plr_database',
       'plr_table',
       'plr_column',
       'plr_subset',
       'plr_subsetitem'
    );


    function __construct($aowner, $key = false) {
        parent::__construct($aowner, $key);
    }

    function _exportTables($tablenames, $type, $params) {

    foreach( $tablenames as $exp_table ) {
      $sql = 'SELECT * FROM ' . $exp_table;

      if ( $type == 'normal' ) {
        $sql .= ' WHERE clientid = ' . $params[0] ; //.' AND  = ' . $params[1];
      } elseif ( $type == 'app' ) {
        $sql .= ' WHERE clientid = ' . $params[0] .' AND applicationid = ' . $params[1];
      } else {
        $sql .= ' WHERE clientid = ' . $params[0] .' AND databaseid = ' . $params[1];
      }
//      echo $sql.'<br />';
//$this->plrInstance()->debug=true;
      $db = $this->plrInstance();

      $rs = $db->Execute($sql);
      if (!$rs) die('Fout opgetreden bij ' . $exp_table. '  '. $db->ErrorMsg());
      while( $rec = $rs->FetchNextObject (true) ) {
        $exportline = 'INSERT INTO ' . $exp_table . ' (';

        if ( $type == 'normal' ) {
          ## feed the CLIENTID and APPLICATIONID to this line
          ## these columns are parameterized and filled by the Import function
          $columns = 'CLIENTID, ';
          $values = '##CLIENTID##, ';
        } elseif ( $type == 'app' ) {
          ## feed the CLIENTID and APPLICATIONID to this line
          ## these columns are parameterized and filled by the Import function
          $columns = 'CLIENTID, APPLICATIONID, ';
          $values = '##CLIENTID##, ##APPID##, ';
        } else {
          ## feed the CLIENTID and DATABASEID to this line
          ## these columns are parameterized and filled by the Import function
//          $columns = 'CLIENTID, DATABASEID, ';
//          $values = '##CLIENTID##, ##DATABASEID##, ';
          $columns = 'CLIENTID, ';
          $values = '##CLIENTID##, ';
        }

        ## feed the column names to this line
        foreach($rec as $column => $value) {

          if ( $type == 'normal' ) {
            ## feed the CLIENTID and APPLICATIONID to this line
            ## these columns are parameterized and filled by the Import function
            if ( $column == 'CLIENTID' )
              $value = '##CLIENTID##, ';
          } elseif ( $type == 'app' ) {
            ## feed the CLIENTID and APPLICATIONID to this line
            ## these columns are parameterized and filled by the Import function
            if ( $column == 'CLIENTID' )
              $value = '##CLIENTID##, ';
            if ( $column == 'APPLICATIONID' )
              $value = '##APPLICATIONID##, ';
          } else {
            ## feed the CLIENTID and DATABASEID to this line
            ## these columns are parameterized and filled by the Import function

            if ( $column == 'CLIENTID' )
              $value = '##CLIENTID##, ';
/*          if ( $column == 'DATABASEID' )
              $value = '##DATABASEID##, ';*/
          }



          if (
            ( $type == 'normal' and $column != 'CLIENTID' )
            or
            ( $type == 'app' and $column != 'CLIENTID' and $column != 'APPLICATIONID' )
            or
            ( $type == 'db' and $column != 'CLIENTID' /* and $column != 'DATABASEID' */)
          ) {
            $columns .= $column . ', ';
            $values .= '"'. addslashes( $value ) . '", ';
          }
        }
        ## trim the last comma from the strings
        $columns = substr($columns, 0, -2);
        $values = substr($values, 0, -2);

        ## feed the values to this line
        $exportline .= $columns . ') VALUES ( ' . $values . " );\r\n";

        $result .= $exportline;
      }
    }
    return $result;
  }

	function Import($pclientid, $papplicationid, $pfilename) {
	   $content = file_get_contents( $pfilename );

	   str_replace( CLIENTIDPLACEHOLDER, $pclientid, $content );
	}

  function Export($pclientid, $papplicationid, $pfilename) {
    $db = $this->plrInstance();

    $export = $this->_exportTables( $this->exp_tables, $type='normal', array($pclientid) );

    $export .= $this->_exportTables( $this->exp_app_tables, $type='app', array($pclientid, $papplicationid ) );

    $sqlGetDatabases = 'select distinct databaseid from plr_form where clientid = ? and databaseid IS NOT NULL';

    $dbs = $db->Execute( $sqlGetDatabases, array($pclientid) );

    while($dbrec = $dbs->FetchNextObject(true)) {
      $pdatabaseid = $dbrec->DATABASEID;

      $export .= $this->_exportTables( $this->exp_db_tables, $type='db', array($pclientid, $pdatabaseid) );
    }

    $sqlGetAppName = 'select applicationname from plr_application where clientid = ? and applicationid = ?';
    $appname = $db->GetOne( $sqlGetAppName, array($pclientid, $papplicationid) );

    echo 'This Polaris database instance was used:<br />';
    echo 'Type: '.$db->databaseType.'<br />';
    echo 'Database: '.$db->database.'<br />';
    echo 'Host: '.$db->host.'<br />';
    echo 'User: '.$db->user.'<br />';
    $r = file_put_contents( $pfilename, $export );
    if ( $r ) {
      echo '<br />Export of <b>'.$appname.'</b> completed...!';
      echo '<br /> Result was written to file \''.$pfilename.'\'.<br />';
    } else
      echo 'Error exporting application to '.$pfilename.': ' .$r .'<br />Here is the content of the file:<br />';
    echo '<textarea style="width:100%;height:500px">'  ;
    echo $export;
    echo '</textarea>'  ;
	}

}

?>