<?php

class base_plrConstruct extends base_plrRecord {
    var $masterform;
    var $detailform;
    var $masterform_insert;
    var $constructid;
    var $outputformat;
    var $masteraction;
    var $detailaction;

    function __construct(&$aowner) {
        parent::__construct($aowner);
    }

    function LoadRecord($params, $metaname = false) {
        global $_sqlConstruct, $_sqlConstructMeta;

        $theQuery = ($metaname)?$_sqlConstructMeta:$_sqlConstruct;
        $rs = $this->dbExecute($theQuery, $params) or Die('query failed construct: '.$this->dbErrorMsg());
        $this->record = $rs->FetchNextObject(true);
        if ($this->record) {
            $clientid = $params[0];
            $applicationid = $params[1];
            $this->constructid = $this->record->CONSTRUCTID;
            $this->masterform = new base_plrForm($this);
            $this->masterform->LoadRecord(array($clientid, $this->record->MASTERFORMID));
            if ($this->record->DETAILFORMID != 0) {
                $this->detailform = new base_plrForm($this);
                $this->detailform->LoadRecord(array($clientid, $this->record->DETAILFORMID));
            }
            if (!empty($this->record->NEWMENUITEMNAME)) {
                $this->masterform_insert = new base_plrForm($this);
                $this->masterform_insert->LoadRecord(array($clientid, $this->record->NEWMENUITEMNAME));
            }
        }
    }

    function IsMaximized() {
        return (isset($_GET['maximize']) or ($this->record->MAXIMIZED == 'Y'));
    }

    function ShowConstructIcon() {
        global $_GVARS;

        if ($this->record->IMAGEURL != '') {
            if (substr($this->record->IMAGEURL, 0, 3) == 'fa-') {
                echo "<i class=\"constimg fa-light {$this->record->IMAGEURL} fa-3x\" title=\"".$this->record->MASTERFORMID."\"></i>";
            } elseif (substr($this->record->IMAGEURL, 0, 5) == 'icon-') {
                echo "<i class=\"constimg icon {$this->record->IMAGEURL}\"></i>";
            } elseif (substr($this->record->IMAGEURL, 0, 6) == 'ficon-') {
                echo "<i class=\"constimg icon {$this->record->IMAGEURL}\"></i>";
            } else {
                echo "<img class=\"constimg construct-icon\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"".$this->record->MASTERFORMID."\" src='".$_GVARS['serverpath'].$this->record->IMAGEURL."' />";
            }
        }
    }

    function Show($masteraction, $detailaction, $currentdetailform=false, $outputformat='xhtml') {
        global $_sqlAllSearchColumns;
        global $_sqlSearchColumns;
        global $_CONFIG;

        $this->masteraction = $masteraction;
        $this->detailaction = $detailaction;

        $this->outputformat = $outputformat;
        if ($this->record->CONSTRUCTTYPE == 'GROUP') {
            echo '<h1>'.get_resource($this->record->CONSTRUCTNAME, $this->__polaris->language).'</h1>';

            $subconstructs = & $this->owner->GetConstructs($this->record->CONSTRUCTID);
            echo '<div id="2tabs-content"><ul class="xtabs">';
            foreach (array_keys($subconstructs) as $index ) {
                $sub = & $subconstructs[$index];
                echo '<li><a href="?freq=y&amp;const='.$sub['CONSTRUCTID'].'"><img height="21" width="21" src="'.$sub['IMAGEURL'].'" alt="" />'.$sub['CONSTRUCTNAME'].'</a></li>';
            }
            echo '</ul></div>';
        } else {
            /**
            * Show the Master (and Detail) form
            *
            */
            if ($this->record->DESCRIPTION != '') echo ' - '.$this->record->DESCRIPTION;

            if (isset($this->masterform)) {
                if ($masteraction == 'detail') {
                    $this->masterform->ShowFormHeader($masteraction);
                } else {
                    if (isset($this->masterform_insert) and $masteraction == 'insert') {
                        $this->masterform_insert->ShowMaster($masteraction, $this->outputformat);
                    } else {
                        $this->masterform->ShowMaster($masteraction, $this->outputformat);
                    }
                }
            }
            if (($masteraction == 'detail' or $masteraction == 'edit' or $masteraction == 'view' /* or $masteraction == 'clone' -> no details when cloning */)) {
                // $this->masterform->ShowDetails($detailaction, $currentdetailform, $this->outputformat);
            }
            if ($_CONFIG['showajaxdetails'] == true) {
                if ($GLOBALS['showdetaillist']) {
                    if ($this->record->DETAILFORMID > 0 and $this->record->SHOWDETAILASLINKS == 'N' and ($masteraction != 'edit' and $masteraction != 'clone' and $masteraction != 'detail')) {
                        $this->detailform->ShowAjaxDetail($detailaction, $this->outputformat);
                    }
                }
            }
        }
    }

    function StoreNavFreq($userid) {
        global $_sqlNavigationFreq, $_sqlUpdateNavFreq;

        $clientid = $this->record->CLIENTID;
        $applicationid = $this->record->APPLICATIONID;
        $constructid = $this->record->CONSTRUCTID;

        $rs = $this->dbExecute($_sqlNavigationFreq, array($clientid, $applicationid, $constructid, $userid));
        if (!$rs)
            $freq = 1;
        else {
            $rec = $rs->FetchNextObject(false);
            $freq = $rec->frequency + 1;
        }
        $fields = array('clientid' => $clientid, 'applicationid' => $applicationid, 'constructid' => $constructid, 'usergroupid' => $userid, 'frequency' => $freq);
        replaceRecord($this->plrInstance(), $tablename = 'plr_quicknavigation', array('clientid', 'applicationid', 'constructid', 'usergroupid'), $fields, $autosupervalues);
    }

    function AddMasterFilter($sql) {
        return sqlCombineWhere($sql, $this->record->MASTERFILTER);
    }

    function AddDetailFilter($sql) {
        return sqlCombineWhere($sql, $this->record->DETAILFILTER);
    }

    function AddConstructFilter($sql, $isdetail) {
        if ($isdetail)
            return $this->AddDetailFilter($sql);
        else
            return $this->AddMasterFilter($sql);
    }

    function GetMasterOrderBy() {
        if ($this->record->MASTERORDERBY != '')
            return ' ORDER BY '.$this->record->MASTERORDERBY;
        else
            return FALSE;
    }

    function GetDetailOrderBy() {
        if ($this->record->DETAILORDERBY != '')
            return ' ORDER BY '.$this->record->DETAILORDERBY;
        else
            return FALSE;
    }

    function GetConstructOrderBy($isdetail) {
        if ($isdetail)
            return $this->GetDetailOrderBy();
        else
            return $this->GetMasterOrderBy();
    }

    function HasParameter($param) {
        return (strpos($this->record->PARAMETERS, $param) !== false );
    }

    function GetParameters() {
        if ($this->record->PARAMETERS != '') {
            $param = array();
            $paramarray = explode(';', $this->record->PARAMETERS);
            foreach($paramarray as $param) {
                $paramkeyvalues = explode('=', $param);
                $params[$paramkeyvalues[0]] = $paramkeyvalues[1];
            }
            return $params;
        } else {
            return false;
        }
    }
}
