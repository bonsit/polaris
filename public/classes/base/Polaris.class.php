<?php
require "adodb.inc.php";
require "adodb-plr-exceptions.inc.php";

use Pheanstalk\Pheanstalk;
use Pheanstalk\Values\TubeName;
use SpryngApiHttpPhp\Client;

function SessionNotifyFn($expireref, $sesskey) {
    global $ADODB_SESS_CONN; # the session connection object

    $user = $ADODB_SESS_CONN->qstr($expireref);
    $ADODB_SESS_CONN->Execute("delete from plr_session2 where expireref=$user");
}

class base_Polaris extends base_plrRecord {
    private $queue = false;
    public $queue_tube = null;

    const _saltReset = '2cf4c824ef72c56529716d027692adfe';

    var $instance;
    var $instanceid;
    var $tpl;
    var $language;
    var $polarissession;
    var $plrClient;
    var $groupvalues = false;
    var $timer;
    var $inDesignerMode = false;
    var $currentuser = null;
    var $currentApp;
    var $currentconstruct;
    var $currentform;
    var $currentdetailform;
    var $_baseUserQuery = '
            SELECT u.clientid, usergroupid, usergroupname, rootadmin, clientadmin, userpassword
            , accountdisabled, passwordneverexpires, lastpasswordchangedate
            , fullname, photourl
            , usercannotchangepassword, changepasswordnextlogon
            , if(ifnull(language, \'\') = \'\', defaultlanguage, language) as language
            , options, plrinstance, u.customcss, u.user_background, u.2fa, u.2facode, u.phonenumber
            , c.name, c.imagesgroup_hash
            FROM plr_usergroup u
            LEFT JOIN plr_client c
            ON u.clientid = c.clientid
        ';

    public function __construct($aowner=null) {
        parent::__construct($aowner);

        $this->__polaris = $this;
        // initialize the timer object
        include_once("pageTimer.class.php");
        $this->timer = new pageTimer();
    }

    public function initialize($forceReconnect=false) {
        global $PLRLANGUAGES;

        $this->instanceid = $this->getInstance();
        $this->connectInstance($forceReconnect, false);
        // For now we don't store sessions in the plr_session2 table,
        // because they result in 'Server has gone away' errors
        // $this->setupSession();
        $this->setupSimpleSession();

        if (isset($_GET['lang']))
            $this->setLanguage($_GET['lang']);
        elseif (isset($_SESSION['lang']))
            $this->setLanguage($_SESSION['lang']);
        else
            $this->setLanguage("NL");

        $this->inDesignerMode = isset($_GET['designer']) and $_GET['designer'] == 'true';

        /**
         * Create the Application Templating (Smarty) object
         */
        if ($this->inDesignerMode) {
            $this->tpl = new tempengine_dsgSmarty($this->language);
        } else {
            $this->tpl = new tempengine_appSmarty($this->language);
        }

        if (isset($this->tpl)) {
            $this->tpl->assignByRef('polaris', $this);
        }

        if (!isset($this->plrClient)) {
            $_clientid = isset($_SESSION['clientid'])?$_SESSION['clientid']:'';
            $this->plrClient = new base_plrClient($this, $_clientid);
        }

        // if (!empty($_REQUEST['_hdnPlrSes']))
        //     $this->setPolarisSession($_REQUEST['_hdnPlrSes']);
    }

    public function start() {
        $this->timer->startTimer('page');

        $this->initialize();
        $this->setPolarisSession();

        /**
         * What is the event (Save record, Delete record, etc) ?
         */
        $_event = isset($_GET['method'])?$_GET['method']:false;
        if (!$_event) $_event = isset($_REQUEST['_hdnAction'])?$_REQUEST['_hdnAction']:false;

        /**
         * Dispatch the event to the right event handler (if not handled by the custom module)
         */
        $_processedByModule = (isset($_REQUEST['_hdnProcessedByModule']) and ($_REQUEST['_hdnProcessedByModule'] == 'true'));
        if ($_event and !$_processedByModule) {
            $dispatcher = new controllers_dispatcherPolaris($_event);
            $dispatcher->processEvent();
        }

        $_apiCall = $this->isApiCall();
        $_loggedOn = $this->isLoggedOn();
        if ($_apiCall == true && !$_loggedOn) {
            /**
             * If it's a Service call (login via URL), then authenticate the Service call
             */
            $_loggedOn = $this->authenticateServiceCall(
                $_GET['api_token']
                , $_GET['api_user']
                , $_GET['app'].$_GET['const']
            );
        }

        if ($_loggedOn) {
            $this->ShowApplication();
        } else {

            if ($_GET['forgotpassword'] == 'true')  {
                $this->showForgotPassword();
                return;
            } elseif (!empty($_GET['resethash'])) {

                if ($this->resetHashExists($_GET['resethash'])) {
                    $this->showChangePassword("plr_changepasswordreset");
                    return;
                }
            } else {
                $this->showLoginForm();
            }
        }
    }

    public function setPolarisSession($id=false) {
        if (!empty($id)) {
            $_SESSION['polarissession'] = $id;
        } else {
            $id = $_SESSION['polarissession'];
        }

        $this->polarissession = $id;
}

    public function setLanguage($_language) {
        global $PLRLANGUAGES;

        // Load the GETted language resource and store is as a session variable
        if (isset($_language))
            $this->language = $_language;
        // Load the SESSION language resource.
        // If none is supplied, then language is defaulted to NL
        if (!isset($this->language)) {
            // set the default language or the even more default language
            $this->language = (@constant('PLRDEFAULTLANG') != '') ? constant('PLRDEFAULTLANG') : 'NL';
        }
        $_SESSION['lang'] = $this->language;
        lang_load($this->language);

        // WERKT NOG NIET: MAX-348
        setlocale(LC_TIME, $PLRLANGUAGES[$this->language]);
    }

    public function getInstance() {
        global $_CONFIG;

        if (isset($_COOKIE['plrinstance'])) {
            $i = $this->instanceHashToId($_COOKIE['plrinstance']);
        } else {
            $i = $_CONFIG['currentinstance'];
        }
        return $i;
    }

    public function showInstanceInfo() {
        global $_PLRINSTANCE;

        return $_PLRINSTANCE[$this->getInstance()]['username'].'@'.$_PLRINSTANCE[$this->getInstance()]['host'].'/'.$_PLRINSTANCE[$this->getInstance()]['database'];
    }

    public function instanceHashToId($instancehash) {
        global $_PLRINSTANCE;

        foreach($_PLRINSTANCE as $key => $instance) {
            if ($instancehash == md5($instance['host'].$instance['port'].$instance['database']) ) {
                return $key;
            }
        }
    }

    public function &connectInstance($forceReconnect=false,$debug=false) {
        global $_CONFIG;
        global $ADODB_CACHE_DIR;
        global $ADODB_COUNTRECS;
        global $_PLRINSTANCE;
        global $ADODB_COUNTRECS;

        if (!$this->instance or $forceReconnect) {
            $this->timer->startTimer('plrdatabase_connect');

            $i = $this->instanceid;
            $this->instance = NewADOConnection($_PLRINSTANCE[$i]['dbtype']);
            $this->instance->SetFetchMode(ADODB_FETCH_ASSOC);

            $plrhost = $_PLRINSTANCE[$i]['host'];
            if ($_PLRINSTANCE[$i]['port'] > 0) $this->instance->port = $_PLRINSTANCE[$i]['port'];
            try {
                $this->instance->Connect($plrhost, $_PLRINSTANCE[$i]['username'], $_PLRINSTANCE[$i]['password'], $_PLRINSTANCE[$i]['database']);
            } catch (Exception $E) {
                echo "<pre>Could not connect to Polaris instance.";
                echo "<br>System halted.</pre>";
                echo "<br>Exception: ".$E->getMessage();
                exit();
            }
            if ( $_CONFIG['logperformance'] ) {
                $this->instance->LogSQL(); // log all sql queries
            }

            if ( isset($_GET['purgecache']) )
                $this->instance->CacheFlush();
            if ( $_CONFIG['usememcached'] ) {
                $this->instance->memCache = true;
                $this->instance->memCacheHost = $_CONFIG['memcached_servers'];
                $this->instance->memCachePort = $_CONFIG['memcached_port']; /// this is default memCache port
                $this->instance->memCacheCompress = false; /// Use 'true' to store the item compressed (uses zlib)
            }
            if ( $_CONFIG['cachequeries'] ) {
                $ADODB_CACHE_DIR = $_CONFIG['cachequeries_folder'];
                $this->instance->cacheSecs = $_CONFIG['cachequeries_timeout'];
            }
            if ($_PLRINSTANCE[$_CONFIG['currentinstance']]['dbtype'] == 'mysqli')
                mysqli_set_charset($this->instance->_connectionID, 'utf8');
            else
                mysql_set_charset('utf8', $this->instance->_connectionID);
            mb_internal_encoding('UTF-8');

            $this->instance->Execute("SET time_zone = '+02:00'");

            // FALSE: don't buffer the records for record counting, it slows down Polaris.
            // TRUE: because we get 'Commands out of sync; you can't run this command now', we must set this to True
            $ADODB_COUNTRECS = true;

            $this->timer->endTimer('plrdatabase_connect');
        }
        $this->instance->debug = $debug;
        return $this->instance;
    }

    public function showGenericErrorScreen($message) {
        $_errorpage = file_get_contents('admin/500.php');
        $_errorpage = str_replace('{{message}}', $message, $_errorpage);
        echo $_errorpage;
    }

    public function instancesAsSelectList($instancearray) {
        global $_CONFIG;

        $r = '<select name="plrinstance" id="plrinstance">';
        foreach($instancearray as $key => $instance) {
            $instancehash = md5($instance['host'].$instance['port'].$instance['database']);
            if ($this->instanceid == $key) $selected = 'selected="selected"'; else $selected = '';
            $r .= '<option value="'.$instancehash.'" '.$selected.'>'.$instance['name'].'</option>';
        }
        $r .= '</select>';
        return $r;
    }

    function GetCurrentClient($defaultclient=null) {
        // return the subdomain of the $_SERVER['HTTP_HOST']
        $_clientname = strtolower(explode('.', $_SERVER['HTTP_HOST'])[0]);
        if (!isset($_clientname) or $_clientname == 'main') {
            $_clientname = $defaultclient;
        }
        $_sql = 'SELECT clientid FROM plr_client WHERE lower(domainname) = ?';
        $_clientid = $this->instance->GetOne($_sql, array($_clientname));
        if (empty($_clientid))
            trigger_error('Clientname is not defined. Please contact administrator.', E_USER_ERROR);
        return $_clientid;
    }

    public function generateSessionId() {
        $tv = gettimeofday();
        $buf = sprintf("%.15s%ld%ld%0.8f", $_SERVER['REMOTE_ADDR'], $tv['sec'], $tv['usec'], php_combined_lcg() * 10);
        return md5($buf);
    }

    public function ConvertFloatValue($value) {
        global $_CONFIG;

        if (isset($_CONFIG['floatdecimalchar'])) {
            $value = trim($value);
            $value = str_replace('.','',$value); // haal thousand separator weg
            $result = str_replace(',', $_CONFIG['floatdecimalchar'], $value);
        }
        return $result;
    }

    public function ConvertFloatValues($fields) {
        global $_CONFIG;

        foreach($fields as $field => $value) {
            if (strpos($field, FLOATCONVERT) !== FALSE) {
                $field = str_replace(FLOATCONVERT,'',$field);
                if (isset($_CONFIG['floatdecimalchar'])) {
                    $result[$field] = $this->ConvertFloatValue($value);
                }
            } else
                $result[$field] = $value;
        }
        return $result;
    }

    public function KeepDefaultColumns($constructname, $tablename, $fields) {
        global $_CONFIG;

        $result = $fields;
        if (isset($constructname)) {
            foreach($fields as $field => $value) {
                if (strpos($field, DEFAULTCOLUMN) !== FALSE) {
                    $field = str_replace(DEFAULTCOLUMN,'',$field);
                    $_SESSION['DEFAULTCOLUMNS'][$constructname][$field] = $value;
                }
                $result[$field] = $value;
            }
        }
        return $result;
    }

    public function ConvertJSONMultiField($fields) {
        $result = $fields;
        foreach($fields as $field => $value) {
            if (strpos($field, JSONMULTIFIELD) !== FALSE) {
                unset($result[$field]);
                $newfield = str_replace(JSONMULTIFIELD, '', $field);
                $result[$newfield] = json_encode($value);
            }
        }
        return $result;
    }

    public function ReplaceDynamicVariables($subject, $nullvalue='', $currentlinerecord=false, $callerobject=false) {
        if (is_array($subject)) {
            foreach($subject as $k => $text) {
                $subject[$k] = $this->_ReplaceDynamicVariables($text, $nullvalue, $currentlinerecord, $callerobject);
            }
        } else {
            $subject = $this->_ReplaceDynamicVariables($subject, $nullvalue, $currentlinerecord, $callerobject);
        }
        return $subject;
    }

    function _ReplaceDynamicVariablesArray($array, $text, $nullvalue, $arrayname='prefix') {
        $varprefix = "%{$arrayname}[";
        $pos = strpos($text, $varprefix);
        while ($pos !== false) {
            $endpos = strpos($text, ']%', $pos+1);
            $varname = substr($text, $pos + strlen($varprefix), $endpos - ($pos + strlen($varprefix)));

            if (!empty($array[$varname])) {
                // if found then replace with real value
                $text = str_replace($varprefix.$varname.']%', $array[$varname], $text);
            } else {
                // if not found then replace with NULL (so the query will work properly)
                $text = str_replace($varprefix.$varname.']%', $nullvalue, $text);
            }
            // get the next dynamic session variable
            if (!empty($text)) {
                $pos = @strpos($text, $varprefix, $pos + 1);
            } else {
                $pos = false;
            }
        }
        return $text;
    }

    public function _ReplaceDynamicVariables($text, $nullvalue='', $currentlinerecord=false, $database=false) {
        global $_GVARS;
        global $scramble;

        if ($text != '') {
            $text = str_replace('%clientid%', $_SESSION['clientid'] ?? '', $text);
            $text = str_replace('%clienthash%', $_SESSION['clienthash'] ?? '', $text);
            $text = str_replace('%clientname%', $_SESSION['clientname'] ?? '', $text);
            $text = str_replace('%userid%', $_SESSION['userid'] ?? '', $text);
            $text = str_replace('%username%', $_SESSION['name'] ?? '', $text);
            $text = str_replace('%fullname%', $_SESSION['fullname'] ?? '', $text);
            $text = str_replace('%serverroot%', $_GVARS['serverroot'] ?? '', $text);
            $text = str_replace('%domainname%', get_domain($_SERVER['SERVER_NAME'] ?? ''), $text);
            $text = str_replace('%servername%', $_SERVER['SERVER_NAME'] ?? '', $text);
            $text = str_replace('%host%', $_GVARS['host'] ?? '', $text);
            $text = str_replace('%scramble%', $scramble ?? '', $text);
            $text = $this->_ReplaceDynamicVariablesArray($_SESSION, $text, $nullvalue, arrayname:'session');
            if ($this->plrClient) {
                $text = $this->_ReplaceDynamicVariablesArray($this->plrClient->clientparameters, $text, $nullvalue, arrayname:'clientparameters');

                $datascopevarprefix = '%datascope[';
                $pos = strpos($text, $datascopevarprefix);
                while ($pos !== false) {
                    $endpos = strpos($text, ']%', $pos+2);
                    $tablename = substr($text, $pos + strlen($datascopevarprefix), $endpos - ($pos + strlen($datascopevarprefix)));

                    $ds = $this->plrClient->GetDataScopeFilter(1, $tablename);
                    if (isset($ds) and ($ds != ''))
                        // if found then replace with real value
                        $text = str_replace($datascopevarprefix.$tablename.']%', $ds, $text);
                    else
                        // if not found then replace with NULL (so the query will work properly)
                        $text = str_replace($datascopevarprefix.$tablename.']%', $nullvalue, $text);
                    // get the next dynamic session variable
                    $pos = strpos($text, $datascopevarprefix, $pos + 1);
                }

            }

            if ($currentlinerecord) {
                $ROWID = $_GET['ROWID']??null;
                if (!isset($ROWID)) $ROWID = $_GET['rec']??null;
                if ($database) $ROWID = $database->customUrlDecode($ROWID);
                $fieldvarprefix = '%field[';
                $pos = strpos($text, $fieldvarprefix);
                while ($pos !== false) {
                    $endpos = strpos($text, ']%', $pos+2);
                    $varname = substr($text, $pos + strlen($fieldvarprefix), $endpos - ($pos + strlen($fieldvarprefix)));
                    if ($varname == 'ROWID') {
                        // if found then replace with ROWID value
                        if (strpos($ROWID, ',') !== FALSE) {
                            $ROWID = str_replace(',','\',\'',$ROWID);
                        }
                        $text = str_replace($fieldvarprefix.$varname.']%', "'$ROWID'", $text);
                    } elseif (is_object($currentlinerecord) and isset($currentlinerecord->$varname)) {
                        // if found then replace with real value
                        $text = str_replace($fieldvarprefix.$varname.']%', $currentlinerecord->$varname, $text);
                    } elseif (is_array($currentlinerecord) and isset($currentlinerecord[$varname])) {
                        // if found then replace with real value
                        $text = str_replace($fieldvarprefix.$varname.']%', $currentlinerecord[$varname], $text);
                    } else {
                        // if not found then replace with NULL (so the query will work properly)
                        $text = str_replace($fieldvarprefix.$varname.']%', 'NULL', $text);
                    }
                    // get the next dynamic session variable
                    $pos = @strpos($text, $fieldvarprefix, $pos + 1);
                }
            }
        }
        return $text;
    }

    public function GetGroupValues($startgroupvalue=false) {
        if (!$this->groupvalues) {
            $this->groupvalues = GetGroupValues($this->instance, $_SESSION['clientid']??'', $startgroupvalue);
        }
        return $this->groupvalues;
    }

    public function userSettings($setting) {
        global $_CONFIG;

        $customsettings = $this->instance->GetOne(
            "SELECT VALUE FROM plr_usersetting WHERE clientid = ? AND applicationid = ?
            AND usergroupid in (?) and SETTING = ?"
            , array($_SESSION['clientid'], 1, $this->GetGroupValues($_SESSION['userid']), strtoupper($setting)));
        return (!empty($customsettings) ? $customsettings : ($_CONFIG[strtolower($setting)] ?? ''));
    }

    public function IsUserMemberOf($groupname) {
        $groupname = strtoupper($groupname);
        $group = $this->instance->GetOne(
            "SELECT usergroupid FROM plr_usergroup WHERE clientid = ? AND UPPER(usergroupname) = ?"
            , array($_SESSION['clientid'], $groupname));
        $groupvalues = explode(',', $this->GetGroupValues());
        return in_array($group, $groupvalues);
    }

    public function createBreadcrumbs($currentApp, $currentAction, $_currentDetailAction) {
        global $g_lang_strings;
        global $_GVARS;
        global $_CONFIG;

        $_appRef = "{$_GVARS['serverroot']}/app/{$currentApp->record->METANAME}/";

        /*
        if (count($this->plrClient->applications->applications) > 1) {
            $_breadcrumbs = array (array('label' => strtolower($g_lang_strings['all_applications']), 'link' => $_GVARS['serverroot'].'/allapps/'));
            $_breadcrumbs[] = array('label' => $this->plrClient->applications->applications, 'link' => $_appRef, 'type' => 'select');
        } else
        */
        if (count($this->plrClient->applications->applications) > 1) {
            // $g_lang_strings['all_applications']
            $_breadcrumbs = array (array('label' => '<i class="material-symbols-outlined">home</i>', 'link' => $_GVARS['serverroot'].'/allapps/'));
            $_breadcrumbs[] = array('label' => get_resource($currentApp->record->APPLICATIONNAME), 'link' => $_appRef, 'type' => '');
            if ($this->currentconstruct and $this->currentconstruct->record) {
                $_constRef = "{$_appRef}const/{$this->currentconstruct->record->METANAME}/";
                $_breadcrumbs[] = array('label' => $this->currentconstruct->record->CONSTRUCTNAME, 'link' => $_constRef);
            }
        }
        if ($this->currentform and $currentAction == 'edit' and (!isset($_GET['donly']) or (isset($_GET['donly']) and $_GET['donly'] != 'true'))  ) {
            if (($this->currentform->GetPermissionType() & UPDATE) == UPDATE)
                $actiontype = lang_get('edit_item', $this->currentform->ItemName() );
            else
                $actiontype = lang_get('view_item', $this->currentform->ItemName() );
            $_breadcrumbs[] = array('label' => $actiontype, 'link' => '');
        }
        if (isset($GLOBALS['detailonly'])) {
            if (isset($this->currentdetailform)) {
                $_breadcrumbs[] = array('label' => $this->currentform->ItemName() . ' item', 'link' => $_constRef.'edit/'.$_GET['rec'].'/');
                if ($_currentDetailAction == 'detail_edit') {
                    $_breadcrumbs[] = array('label' => $this->currentdetailform->ItemsName().' wijzigen', 'link' => '');
                } else {
                    $_breadcrumbs[] = array('label' => $this->currentdetailform->ItemsName(), 'link' => '');
                }
            }
        }
        if ($currentAction == 'insert') {
            if ($this->currentform)
                $_breadcrumbs[] = array('label' => lang_get('add_an_item', $this->currentform->ItemName() ), 'link' => '');
        }
        if ($currentAction == 'search') {
            $_breadcrumbs[] = array('label' => lang_get('search_result'), 'link' => '');
        }
        if ($currentAction == 'impexp')
            $_breadcrumbs[] = array('label' => lang_get('import_export'), 'link' => '');
        return $_breadcrumbs;
    }

    public function DisplayApplicationList() {
        global $g_lang_strings;

        $_breadcrumbs = array (
            array('label' => mb_convert_case($g_lang_strings['application_list'], MB_CASE_LOWER, "UTF-8"), 'link' => '')
        );

        if ($this->inDesignerMode) {
            if (isset($_GET['module'])) {
                $_module = $this->plrClient->GetSharedModule('plr_'.$_GET['module'], 1, $moduleparam, $clientname='_shared');
                $this->tpl->assign('module', $_module);
            }
            $this->tpl->assign('module', $_module);
            $this->tpl->display('dsg_designer.tpl.php');
        } else {
            $this->tpl->assign('breadcrumbs', $_breadcrumbs);
            $this->tpl->assign('maintenance', $this->getMaintenance());
            $this->tpl->assignByRef('apps', $this->plrClient->applications->applications);
            $this->tpl->display('apps.tpl.php');
        }
    }

    public function DisplayTagCloud($currentaction) {
        global $_GVARS;
        global $g_lang_strings;

        $_breadcrumbs = array (
            array('label' => strtolower($g_lang_strings['all_applications']), 'link' => $_GVARS['serverroot'].'/allapps/'),
            array('label' => 'tagcloud', 'link' => '')
        );
        if ($_GET['tag'] != '') {
            $this->tpl->assign('currenttag', $_GET['tag']);
            $this->tpl->assign('taggedobjects', $this->plrClient->getTaggedObjects(($currentaction == 'tags')?$_SESSION['userid']:null, $app=null, $_GET['tag']));
        } else {
            $this->tpl->assign('tags', $this->plrClient->getTags(($currentaction == 'tags')?$_SESSION['userid']:null));
        }
        $this->tpl->assign('breadcrumbs', $_breadcrumbs);
        $this->tpl->assign('navigationstyle', 'horizontal');
        $this->tpl->display('tagcloud.tpl.php');
    }

    public function GetDashboardContainers($appid) {
        global $_sqlDSHContainers;

        $_rs = $this->instance->Execute($_sqlDSHContainers, array($appid));
        return $_rs->GetAll();
    }

    public function ShowModule($module, $moduleId=0, $outputformat='xhtml') {
        $clientname = (substr($module,0,3) == 'plr' or $this->inDesignerMode)?'_shared':false; // assume we are using the _shared module when directly calling them from the url

        if ($this->inDesignerMode and substr($module,0,3) !== 'plr') {
            $moduleId = $module;
            $module = 'plr_designer';
        }

        $moduleparam = false;
        $module =& $this->plrClient->GetModule($module, $moduleId, $moduleparam, $clientname);
        /* if databaseid is provided then connect to database and set as userdb in module object */
        if (isset($_GET['databaseid'])) {
            $databaseid = $_GET['databaseid'];
            $plrdatabase = new base_plrDatabase($this);
            $plrdatabase->LoadRecordHashed($databaseid);
            if (isset($plrdatabase->record)) {
                $plrdatabase->connectUserDB();
                $module->userdatabase =& $plrdatabase->userdb;
            }
        }
        if ($module) {
            $this->tpl->assign('module', $module);
            $module->Show($outputformat);
        }
    }

    public function IsRootAdmin() {
        return $_SESSION['usertype'] == 'root';
    }

    public function IsClientAdmin() {
        return $_SESSION['usertype'] == 'client';
    }

    public function ShowApplication() {
        global $g_lang_strings;
        global $_sqlDSGClients;
        global $_GVARS;
        global $_CONFIG;

        if ($_SESSION['changepassword'] == 'Y') {
            $this->showChangePassword();
            return;
        }

        $_isRestService = !empty($_GET['service']);

        if (isset($_GET['cln']) and $this->IsRootAdmin()) {
            $this->plrClient = new base_plrClient($this, $_GET['cln'], true);
        } else {
            if (empty($_SESSION['clientid']))  {
                session_destroy();
                setcookie('PHPSESSID', '', -3600, '/');
                $_SESSION = array();
            } else {
                if (!$this->plrClient->record->CLIENTID) {
                    $this->plrClient = new base_plrClient($this, $_SESSION['clientid']);
                }

                $this->plrClient->applications->LoadRecords();

                $this->plrClient->LoadClientParameters();
            }
        }

        /***
        *  Global Polaris Template assignments
        **/
        $this->tpl->assign('isoracle', 'false'); // default value
        $this->tpl->assign('service', $_isRestService ? 'REST' : false);
        $this->tpl->assign('username', $_SESSION['fullname']);
        $this->tpl->assign('isclientadmin', $this->IsClientAdmin());
        $this->tpl->assign('clientstylesheet', $this->plrClient->record->CUSTOMCSS);
        $this->tpl->assign('clientparameters', $this->plrClient->clientparameters);
        $this->tpl->assign('userstylesheet', $_SESSION['usercss']);
        $this->tpl->assign('userbackground', $_SESSION['userbackground']);
        $this->tpl->assign('headercolor', $this->userSettings('headercolor'));
        $this->tpl->assignByRef('client', $this->plrClient->record);
        if ($this->plrClient->applications)
            $this->tpl->assignByRef('apps', $this->plrClient->applications->applications);

        /***
        *  Global Variables (clean up a.s.a.p.)
        **/
        $GLOBALS['select'] = (isset($_GET['select']) and ($_GET['select'] == 'true'));

        unset($this->currentApp);
        $_appId = isset($_GET['app'])?$_GET['app']:false;
        if ($_appId) {
            $this->currentApp =& $this->plrClient->LoadApplication($_appId, $metaname=true);
        } else {
            $this->currentApp =& $this->plrClient->applications->DefaultApplication();
        }
        $_currentAction = isset($_GET['action'])?$_GET['action']:'allitems';
        /* Special case of searching in a detail form */

        if ((isset($_GET['q']) and $_GET['q'] !== '' and $_GET['action'] == '') or
        (isset($_GET['qv']))) $_currentAction = 'search';

        $_currentDetailAction = isset($_GET['detailaction'])?'detail_'.$_GET['detailaction']:'detail_allitems';
        /* If there is a recordId and Insert action, then assume the record needs to be cloned */
        if (isset($_GET['rec']) and $_currentAction == 'insert') $_currentAction = 'clone';
        if (isset($_GET['detailrec']) and $_currentDetailAction == 'detail_insert') $_currentDetailAction = 'detail_clone';
        if (isset($_GET['module']) and !isset($_GET['const']) and !isset($_GET['form'])) {
            /***
            *
            * Display the Module
            *
            */
            $outputformat = !empty($_POST) ? 'json' : 'xhtml';

            $this->ShowModule($_GET['module'], $_GET['moduleid'], $outputformat);
        } elseif (!isset($this->currentApp) and ($_currentAction == 'tags' or $_currentAction == 'alltags')) {
            /***
            *
            * Display the Tag Cloud for all applications
            *
            */
            $this->DisplayTagCloud($_currentAction);

        } elseif (!isset($this->currentApp) or $_currentAction == 'allapps') {
            /***
            *
            * Display the applications that the user is authorized for
            *
            */
            $this->DisplayApplicationList();
        } else {
            /***
            *
            * Display the selected application
            *
            */
            $constructs =& $this->currentApp->GetConstructs();
            $this->tpl->SetApplication($this->currentApp); // moet deze ook bij de overige if's
            $this->currentconstruct = false;
            $this->currentform = false;
            $_currentformtype = 'master';

            if (isset($_GET['const']) or isset($_GET['form']) or isset($_GET['basicform']) ) {
                if (isset($_GET['const'])) {
                    $this->currentconstruct =& $this->currentApp->GetConstruct($_GET['const'], $metaname=true);
                    $this->currentform = $this->currentconstruct->masterform;
                } else {
                    $this->currentform =& $this->currentApp->GetForm($_GET['form']??$_GET['basicform'], $metaname=true);
                }
            } else {
                $this->currentconstruct =& $this->currentApp->GetDefaultConstruct($metaname=true);
                if ($this->currentconstruct)
                    $this->currentform = $this->currentconstruct->masterform;
            }

            if ((substr($_currentDetailAction,0,7) == 'detail_')) {
                if (isset($_GET['detailform'])) {
                    $this->currentdetailform =& $this->currentApp->GetForm($_GET['detailform'], $metaname=true);
                    $this->currentdetailform->owner = $this->currentconstruct;
                } else {
                    if ($this->currentconstruct)
                        $this->currentdetailform = $this->currentconstruct->detailform;
                }
            }

            $_breadcrumbs = $this->createBreadcrumbs($this->currentApp, $_currentAction, $_currentDetailAction);
            $this->tpl->assign('breadcrumbs', $_breadcrumbs);
            $this->tpl->assign('appstylesheet', $this->currentApp->record->CUSTOMCSS);
            $this->tpl->assign('applicationname', get_resource($this->currentApp->record->APPLICATIONNAME));
            if ($this->currentconstruct)
                $this->tpl->assign('constructname', $this->currentconstruct->record->CONSTRUCTNAME);
            $this->tpl->assign('version', $this->currentApp->record->VERSION);
            $this->tpl->assign('navigationstyle', $this->currentApp->record->NAVIGATIONSTYLE);
            $this->tpl->assign('searchstyle', $this->currentApp->record->SEARCHSTYLE);

            $this->tpl->assign('maintenance', $this->getMaintenance());
            if ($this->currentApp->record->ISPOLARISDESIGNER == 'Y') {
                $_clients = $this->instance->Execute($_sqlDSGClients);
                $currentclientid = 1;
                if ($_clients->RecordCount() == 1) {
                    $_SESSION['clientselect'] = $currentclientid;
                }
                $_clientselect = RecordSetAsSelectList($_clients, 'CLIENTID', 'NAME', $currentvalue, $id='clientselect', $selectname='', $attr='', $required=true);
                $this->tpl->assign('clientselect', $_clientselect);
            }

            $this->tpl->assign('autoclosewindow', (isset($_REQUEST['autoclosewindow']) and $_REQUEST['autoclosewindow'] == 'true'));
            if ($_currentAction == 'allapps') $_currentAction = 'allitems';
            $this->tpl->assign('currentaction', $_currentAction);
            $this->tpl->assign('currentform', $this->currentform);
            $this->tpl->assign('currentformtype', $_currentformtype);
            $this->tpl->assign('currentdetailform', $this->currentdetailform);
            $this->tpl->assign('currentdetailformtype', $_currentformtype);
            $this->tpl->assign('currentdetailaction', $_currentDetailAction);
            if ($this->currentform) {
                $this->tpl->assign('currentformid', $this->currentform->record->METANAME);
                $this->tpl->assign('itemname', $this->currentform->ItemName());
                $this->tpl->assign('currentdb', $this->currentform->record->DATABASEID);
                $this->tpl->assign('add_item_text', lang_get('add_item'));
                $this->tpl->assign('formhasactivepins', $this->currentform->FormHasActivePins());
                $this->tpl->assign('groupvalues', $this->groupvalues);
            }
            $this->tpl->assign('currentapplicationid', $this->currentApp->record->RECORDID);
            $this->tpl->assign('currentapplicationmeta', $this->currentApp->record->METANAME);
            if ($this->currentconstruct) {
                $this->tpl->assign('currentconstructid', $this->currentconstruct->record->CONSTRUCTID);
                $this->tpl->assign('currentconstructmeta', $this->currentconstruct->record->METANAME);

            }
            $this->tpl->assignByRef('constructs', $constructs);

            if (isset($this->currentform) and $this->currentform) {
                $this->tpl->assign('baseurl', $this->currentform->MakeUrl());
                $this->tpl->assign('basequery', $this->currentform->MakeQuery(array('q','qc','sort','dir','offset','maximized')));
                $this->tpl->assign('baseurlquery', $this->currentform->MakeUrlQuery(array('q','qc','sort','dir','offset','maximized')));
                $this->tpl->assign('nosearchquery', $this->currentform->MakeQuery(array('sort','dir','offset','maximized')));
                if (isset($this->currentform->database)) {
                    $this->tpl->assign('isoracle', $this->currentform->database->IsOracle()?'true':'false');
                    $this->tpl->assign('currentuserdbinstance', $this->currentform->database->GetUserDBInstance());
                }
                if (isset($this->currentconstruct->record->NEWMENUITEMNAME)) {
                    $this->tpl->assign('baseurl_custominsert', $this->currentconstruct->record->NEWMENUITEMNAME);
                } else {
                    $this->tpl->assign('baseurl_custominsert', $this->currentform->MakeUrl());
                }
            }

            if (isset($_GET['select']) and $_GET['select'] == 'true') {
                $this->tpl->assign('selectmode', 'true');
                $this->tpl->assign('permission', 258); // don't change data when selecting a record in a listview
            } elseif ($this->currentform) {
                $this->tpl->assign('permission', $this->currentform->GetPermissionType());
            }
            if (isset($this->currentdetailform)) {
                $this->tpl->assign('detailpermission', $this->currentdetailform->GetPermissionType());
            }
            $outputformat = $_REQUEST['output'] ?? 'xhtml';
            $this->tpl->assign('outputformat', $outputformat);
            $this->tpl->assignByRef('app', $this->currentApp);
            $this->tpl->assignByRef('cons', $this->currentconstruct);
            $showformscripts = /* masterform */
                ((($_currentAction == 'detail' or $_currentAction == 'edit' or $_currentAction == 'view' or $_currentAction == 'insert' or $_currentAction == 'clone')) // and (!isset($_GET['donly']) or (isset($_GET['donly']) and $_GET['donly'] != 'true')))
                /* detailform with invisible masterform */
                || ($_currentAction == 'detail' and $_currentDetailAction != 'detail_allitems')
                /* module */
                || isset($module)
                /* hotlistview EVEN NIET: TE TRAAG */
                //        || ($this->currentform->record->DEFAULTVIEWTYPE == 'HOTLISTVIEW')
                );
            $this->tpl->assign('showformscripts', $showformscripts);
            $this->tpl->assign("maximizeform", (($this->currentconstruct and $this->currentconstruct->IsMaximized()) or (!$this->currentconstruct and $this->currentform)));

            if (isset($_GET['module'])) {
                $module = $this->GetModule($_GET['module'], $_GET['moduleid']);
                $this->tpl->assignByRef('module', $module);
            }

            if (!$this->currentconstruct and !isset($module) and !$this->currentform) {
                $this->tpl->assign('dashboard_containers', $this->GetDashboardContainers($this->currentApp->record->APPLICATIONID));
            }

            if ($_CONFIG['displaytimers']) {
                $this->tpl->assign('displaytimers', $this->timer->displayTimeTaken());
            }

            if ($_isRestService) {
                $_contentType = $this->determineContentType($outputformat);
                if ($_contentType)
                    header("Content-type: text/{$_contentType}\n");
                $outputfile = 'host '.$_SERVER['SERVER_NAME'].' - '. get_resource($this->currentApp->record->APPLICATIONNAME).' - '.get_resource($this->currentconstruct->record->CONSTRUCTNAME);
                $this->tpl->assign('outputfile', $outputfile);
                $this->tpl->display('service.'.$outputformat.'.form.tpl.php');
            } elseif (!empty($_GET['basicform'])) {
                $this->tpl->display('basic_form.tpl.php');
            } else {

                $this->tpl->display('application.tpl.php');
            }
        }
        unset($_SESSION['changedrowid']);

    }

    function getAdvancedForm() {
        global $_sqlClientApiKeys;

        $_rec = $this->instance->GetRow($_sqlClientApiKeys, [$_SESSION['clienthash']]);
        $_signature = $this->calculateSig($_rec['APIKEY'], $_rec['APISECRET']);
	    $result =  "<div id=\"advancedinfo\" style=\"display:none\">
            <h3>Advanced settings</h3>
            <p>Use the following URL to access the form:</p>
            <input type='text' value='{$_signature}' style='width:100%;' readonly>
            <a target='_blank' href='{$_signature}'>Share form</a>
        </div>";
	    return $result;
    }

    function determineContentType($servicetype=false) {
        switch ($servicetype) {
            case 'php':
                return 'php';
                break;
            case 'list':
                return 'plain';
                break;
            case 'rss':
                return 'xml';
                break;
            case 'json2':
                return 'javascript';
                break;

            default:
                return false;
                break;
        }
    }

    public function GetIncludedJavascriptsOLD() {
        global $_GVARS;

        $_result = '';

        if (is_array($_GVARS['_includeJavascript'])) {
            $_result = '<script>';

            // Load the first script, before all other scripts
            $_result .= 'loadScript("'.$_GVARS['_includeJavascript'][0].'")';
            array_shift($_GVARS['_includeJavascript']);
            foreach($_GVARS['_includeJavascript'] as $_jsfile) {
                $_result .= '.then(function () {
                    // First script is loaded, now load other scripts
                    return loadScript("'.$_jsfile.'");
                  })';
            }
            $_result .= '</script>';
        }
        return $_result;
    }

    public function GetIncludedJavascripts() {
        global $_GVARS;

        $_result = '';

        if (is_array($_GVARS['_includeJavascript'])) {
            // Load the first script, before all other scripts
            // $_result .= 'loadScript("'.$_GVARS['_includeJavascript'][0].'")';
            // array_shift($_GVARS['_includeJavascript']);
            foreach($_GVARS['_includeJavascript'] as $_jsfile) {
                $_result .= '<script src="'.$_jsfile.'" defer></script>';
            }
        }
        return $_result;
    }

    public function ContainsPHPFunction($string) {
        $_allowed_funcs_in_default_values = ['date()'];
        $_cleanstring = preg_replace("/\([^)]+\)/", "", $string);

        return stripos(json_encode($_allowed_funcs_in_default_values), $_cleanstring.'()') !== false;
    }

    public function GetModule($module, $moduleid) {
        if (isset($_GET['databaseid'])) {
            $databaseid = $_GET['databaseid'];
            $plrdatabase = new base_plrDatabase($this);
            $plrdatabase->LoadRecordHashed($databaseid);
            $this->currentform->database =& $plrdatabase;
            $this->currentform->userdb =& $plrdatabase->userdb;
        } else {
            $this->currentform->LoadDatabaseObject();
            $this->currentform->userdb =& $this->currentform->database->userdb;
        }
        return $this->currentform->GetModule($this->currentform->GetPermissionType(), $state=false, $module, $moduleid, $moduleparam, $masterrecord = false);
    }

    public function ExtractPrettyMessage($msg) {
        $_errorstr = false;
        $_oraErrorNumbers = array('ORA-20000','ORA-00001');
        foreach($_oraErrorNumbers as $_oraErrorNumber) {
            $_oraErrorNumber = $_oraErrorNumber.':';
            $_posOraErrorNumber = strpos($msg, $_oraErrorNumber);
            if ($_posOraErrorNumber !== false) {
                $_errorstr = substr($msg, $_posOraErrorNumber + strlen($_oraErrorNumber), strpos($msg, 'ORA', $_posOraErrorNumber + 1) - $_posOraErrorNumber - strlen($_oraErrorNumber));
                $parts = explode(' : ', $_errorstr);
                if ($parts[1] != '') {
                    $_errorstr = $parts[1];
                    break;
                }
            } else {
                $_errorstr = $msg;
            }
        }
        return $_errorstr;
    }

    public function ExtractCustomError($msg) {
        $_errorstr = false;
        $_oraErrorNumber = 'ORA-20000: ';
        $_posOraErrorNumber = strpos($msg, $_oraErrorNumber);
        if ($_posOraErrorNumber !== false) {
            $_errorstr = trim(substr($msg
                , $_posOraErrorNumber + strlen($_oraErrorNumber)
                , strpos($msg, ':', $_posOraErrorNumber + strlen($_oraErrorNumber) + 2) - ($_posOraErrorNumber + strlen($_oraErrorNumber))));
        }
        if ($_errorstr === false) {
            $_oraErrorNumber = ' (';
            $_posOraErrorNumber = strpos($msg, $_oraErrorNumber);
            if ($_posOraErrorNumber !== false) {
                $_errorstr = trim(substr($msg
                    , $_posOraErrorNumber + strlen($_oraErrorNumber)
                    , strpos($msg, ')', $_posOraErrorNumber + strlen($_oraErrorNumber) + 2) - ($_posOraErrorNumber + strlen($_oraErrorNumber))));
            }
            if (strpos($_errorstr, '.') !== false) {
                $_errorstr = explode('.', $_errorstr);
                $_errorstr = $_errorstr[1];
            }

        }
        return $_errorstr;
    }

    public function getPrettyErrorMessage($errornr, $errormessage) {
        global $_GVARS;

        $_lang = isset($_GVARS['lang']) ? $_GVARS['lang'] : 'NL';
        $result = $errormessage;

        $_errornr = $errornr;

        // First, search for custom errornr (like DV_2042)
        // $_errornr = $this->ExtractCustomError($errormessage);

        if ($_errornr) {
            $_errornr = str_replace('NHA.','',$_errornr);
            $prettymessage = $this->instance->GetOne('SELECT erroromschrijving_'.$_lang.' FROM plr_errormessages WHERE errorcode = ?', array($_errornr));
        }
        if (!$_errornr || !$prettymessage) {
            $prettymessage = $this->ExtractPrettyMessage($errornr);
            $_errornr = false;
        }
        if ($prettymessage) $result = $prettymessage;
        return $result;
    }

    public function showChangePassword($action=false) {
        if ($action !== false) {
            $this->tpl->assign("action", $action);
        }
        $this->tpl->display('changepassword.tpl.php');
    }

    public function showForgotPassword() {
        $this->tpl->display('forgotpassword.tpl.php');
    }

    public function GetCurrentBrand() {
        global $_GVARS;
        global $_CONFIG;

        $brand = false;
        /**
         * Check if the domain name contains a subdomain, and select that subdomain as branded start page
         */
        $_brandingpath = $_GVARS['docroot'].'/branding/';
        $_hostParts = explode(".",$_SERVER['HTTP_HOST']);

        if (count($_hostParts) > 1) {
            $_exploded = explode(".", $_SERVER['HTTP_HOST']);
            $_subdomain = array_shift($_exploded);

            if ($_subdomain != '' and $_subdomain != 'www' and file_exists($_brandingpath.$_subdomain)) {
                $_SESSION['brand'] = $_subdomain;
                $_SESSION['branding'] = true;
            }
        }

        if ((isset($_SESSION['branding']) and $_SESSION['branding'] == true) or isset($_CONFIG['brand'])) {
            /***
            *  Determine the branded forms
            */
            if (isset($_SESSION['branding']) and $_SESSION['branding'] == true)
                $brand = strtolower($_SESSION['brand']);
            else {
                $_CONFIG['brand'] = $this->ReplaceDynamicVariables($_CONFIG['brand']);
                $brand = strtolower($_CONFIG['brand']);
            }
        }
        return $brand;
    }

    public function showLoginForm() {
        global $_CONFIG;
        global $_PLRINSTANCE;

        $this->setPolarisSession($this->generateSessionId());
        safeSetCookie('plrinstance', ''); // clear the instance

        $_brandingpath = $this->GetBrandPath();
        $_brand = $this->GetCurrentBrand();

        if ($_CONFIG['chooseinstance']) {
            $this->tpl->assign('multipleinstances', count($_PLRINSTANCE) > 1);
            if (count($_PLRINSTANCE) > 1)
               $this->tpl->assign('instancesselect', $this->instancesAsSelectList($_PLRINSTANCE));
        }
        if (file_exists($_brandingpath.$_brand.'/brandglobals.conf')) {
            $this->tpl->config_dir = $_brandingpath.$_brand;
            $this->tpl->assign('hasbrandglobals', true);
        }
        $this->tpl->assign('brand', $_brand);
        // why? NOT SAFE
        // if (isset($_COOKIE['plr_name'])) {
        //     $this->tpl->assign('username', $_COOKIE['plr_name']);
        // }

        $displayloginpage = 'defaultlogin.tpl.php';
        if (file_exists($_brandingpath.$_brand.'/login.tpl.php')) {
            $displayloginpage = $_brandingpath.$_brand.'/login.tpl.php';
        }

        if ($this->is2FARequired())
            $this->tpl->display('2falogin.tpl.php');
        else
            $this->tpl->display($displayloginpage);
    }

    public function showCreateAccountForm($error) {
        global $_CONFIG;
        global $scramble;
        global $ClientHash;

        $this->setPolarisSession($this->generateSessionId());

        $_brandingpath = $this->GetBrandPath();
        $_brand = $this->GetCurrentBrand();
        $this->tpl->assign('brand', $_brand);
        if (file_exists($_brandingpath.$_brand.'/brandglobals.conf')) {
            $this->tpl->config_dir = $_brandingpath.$_brand;
            $this->tpl->assign('hasbrandglobals', true);
        }

        /**
         * Create the form (base on the FORMHASH constructed by the previous SQL),
         * then connect to the client database from that form
         */
        if (!empty($_GET['formhash'])) {
            unset($_SESSION['plrauth']);
            unset($_SESSION['userid']);

            $_form = new base_plrForm($this);
            $_form->LoadRecordHashed($_GET['formhash']);
            if (!empty($_form->record->CLIENTHASH)) {
                $_form->database->connectUserDB();
                // assume the first column of DataDestination is the key column (ID)
                $_row = $_form->database->userdb->GetRow("SELECT ttt.* FROM {$_form->GetDataDestination()} ttt WHERE MD5(CONCAT(ID, '$scramble')) = '".$_GET['linkhash']."'");
                $_client = new base_plrClient($this, $_form->record->CLIENTHASH, true);
                $this->tpl->assign('LINKHASH', $_GET['linkhash']);
                $this->tpl->assign('CLIENTHASH', $_form->record->CLIENTHASH);
                $this->tpl->assign('CLIENTNAME', $_client->record->NAME);
                $this->tpl->assign('clientstylesheet', $_client->record->CUSTOMCSS);
                $this->tpl->assign('errormessage', $error);
                $this->tpl->assign('item', $_row);
            }
        }
        $displayloginpage = 'createaccount.tpl.php';
        if (file_exists($_brandingpath.$_brand.'/'.$displayloginpage)) {
            $displayloginpage = $_brandingpath.$_brand.'/'.$displayloginpage;
        }
        $this->tpl->display($displayloginpage);
    }

    public function GetBrandPath() {
        global $_GVARS;

        return $_GVARS['docroot'].'/branding/';
    }

    public function authenticateServiceCall($api_token, $api_user, $api_call) {
        global $PLRLANGUAGES;

        $this->currentuser = new base_plrUserGroup($this);
        $this->currentuser->loadRecordByName($api_user);

        $outparams = $this->currentuser->authenticateServiceCall($api_token, $api_call);
        if ($outparams) {
            setlocale(LC_ALL, $PLRLANGUAGES[$outparams['language']]);
            $outparams['tokencall'] = "{$_GET['app']}{$_GET['const']}";
            $this->setLanguage($outparams['language']);
            $this->SetLoginSessionVars($outparams);
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function calculateSig($apikey, $apisecret) {
        global $_GVARS;

        if (empty($this->currentuser))
            $this->currentuser = new base_plrUserGroup($this, $_SESSION['userhash']);

        if ($this->currentuser) {
            $hashpassword = $this->currentuser->getUserPassword();
            $_signature = "api_call{$_GET['app']}{$_GET['const']}api_key{$apikey}api_pwd{$hashpassword}";
            $_signaturehash = hash_hmac('sha256', $_signature, $apisecret);

            $_apicall = "{$_GVARS['serverroot']}/services/xhtml/app/{$_GET['app']}/const/{$_GET['const']}/?api_token={$_signaturehash}&api_user={$_SESSION['name']}";
            return $_apicall;
        } else {
            return false;
        }
    }

    public function setupSimpleSession() {
        global $_CONFIG;
        session_start();

        if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > $_CONFIG['LAST_ACTIVITY_TIMEOUT'])) {
            // last request was more than 4 hours ago
            $this->unsetAuthentication();
            session_unset();     // unset $_SESSION variable for the run-time
            session_destroy();   // destroy session data in storage
        }
        $_SESSION['LAST_ACTIVITY'] = time(); // update last activity time stamp

        if (!isset($_SESSION['SESSION_CREATED'])) {
            $_SESSION['SESSION_CREATED'] = time();
        } else if (time() - $_SESSION['SESSION_CREATED'] > 1800) { //1800
            // session started more than 30 minutes ago
            session_regenerate_id(true);    // change session ID for the current session and invalidate old session ID
            $_SESSION['SESSION_CREATED'] = time();  // update creation time
        }
    }

    public function setupSession() {
        global $_PLRINSTANCE;

        $i = $this->instanceid;
        require_once 'session/adodb-session2.php';
        $options['table'] = 'plr_session2';
        ADOdb_Session::config(
            $driver=$_PLRINSTANCE[$i]['dbtype'],
            $host=$_PLRINSTANCE[$i]['host'],
            $user=$_PLRINSTANCE[$i]['username'],
            $password=$_PLRINSTANCE[$i]['password'],
            $database=$_PLRINSTANCE[$i]['database'],
            $options);

        session_start();
    }

    public function logout($username=false) {
        try {
            if (!empty($username)) {
                $updatesql = "UPDATE plr_logbase SET sessionend = '".date('Y-m-d H:i:s')."' WHERE session = '".session_id()."' and username = '".$username."' and sessionend is null";
                $this->connectInstance();
                @$this->instance->Execute($updatesql);
            }
        } finally {
            session_destroy();
            setcookie('PHPSESSID', '', -3600, '/');
            $_SESSION = array();
        }
    }

    public function logToFile($filename, $content) {
        file_put_contents('/tmp/'.$filename, $content);
    }

    public function logQuick($message=null) {
        if (is_array($message))
            $message = var_export($message, true);
        $updatesql = '
            INSERT INTO plr_logbase (status, session, sessionstart, username, server, useripaddress, message)
            VALUES (?,?,?,?,?,?,?)';

        $_serverlocation = @substr($this->instance->database.'@'.$_SERVER["SERVER_ADDR"].' '.$_SERVER['HTTP_REFERER'].' '.$_SERVER['HTTPS'], 0, 200);
        $this->connectInstance();
        $this->instance->Execute($updatesql, array($status='quicklog', $sessid=session_id(), date('Y-m-d H:i:s'), $username=$_SESSION['name'], $_serverlocation, $_SERVER['REMOTE_ADDR']??'no remote_addr', $message));
    }

    public function logUserAction($status, $sessid, $username, $message=null) {
        $updatesql = '
            INSERT INTO plr_logbase (status, session, sessionstart, username, server, useripaddress, message)
            VALUES (?,?,?,?,?,?,?)';

        $_serverlocation = substr($this->instance->database.'@'.$_SERVER["SERVER_ADDR"].' '.($_SERVER['HTTP_REFERER']??'no-referer'), 0, 200);
        $this->connectInstance();
        $this->instance->Execute($updatesql, array($status, $sessid, date('Y-m-d H:i:s'), $username, $_serverlocation, $_SERVER['REMOTE_ADDR'], $message));
    }

    public function setLoginSessionVars($sesparams) {
        global $scramble;

        $_SESSION['plrauth'] = 'true';
        $_SESSION['lang'] = $sesparams['language'];
        $_SESSION['clientid'] = $sesparams['clientid'];
        $clienthash = getClientHash($this->instance, $sesparams['clientid'] );
        $_SESSION['clienthash'] = $clienthash;
        $_SESSION['imagesgroup_hash'] = $sesparams['imagesgroup_hash'];
        $_SESSION['clientname'] = $sesparams['clientname'];
        $_SESSION['usertype'] = $sesparams['usertype'];
        if (isset($sesparams['pwd']))
            $_SESSION['pwd'] = $sesparams['pwd'];
        $_SESSION['changepassword'] = $sesparams['changepassword'];
        // allways register the name so the next LoginForm shows the name of the previous try
        $_SESSION['userid'] = $sesparams['userid'];
        $_SESSION['fullname'] = $sesparams['fullname'];
        $_SESSION['userphoto'] = $sesparams['userphoto'];
        $_SESSION['usercss'] = $sesparams['usercss'];
        $_SESSION['userbackground'] = $sesparams['userbackground'];
        // Sets the Expiry column in the session table (via adodb)
        $_SESSION['userhash'] = $sesparams['userhash'];
    }

    public function dropOtherUsers() {
        global $_sqlDropOtherSessions;
        if ($_SESSION['loggedinuser']) {
            $expireref = $_SESSION['loggedinuser'];
            $name = "name\%7Cs\%3A11\%3A\%22".$_SESSION['name']."\%22%";
            $this->instance->Execute($_sqlDropOtherSessions, array($expireref, $name));
        }
    }

    public function rs2format($cur, $format, $subformat=false, $debug=false) {
        if ($cur === false) return;
        switch ($format) {
            case 'json':
                if (!$debug)
                    header("Content-type: text/json; charset=utf-8");
                    if (!is_array($cur))
                    $rs = $cur->GetAssoc();
                else
                    $rs = $cur;
                $result = json_encode($rs);
                break;

            case 'php':
                if (!is_array($cur))
                    $rs = $cur->GetAll();
                else
                    $rs = $cur;
                $result = serialize($rs);
                break;

            case 'csv':
                include_once('toexport.inc.php');
                if ($subformat == 'tab')
                    $result = rs2tab($cur);
                else
                    $result = rs2csv($cur);
                break;
        }

        return $result;
    }

    public function changePasswordNextLogon($usergroupname, $yesorno) {
        $_sql = 'UPDATE plr_usergroup SET CHANGEPASSWORDNEXTLOGON = ? WHERE LOWER(USERGROUPNAME) = ?';
        try {
            $this->instance->Execute($_sql, array($yesorno, $usergroupname));
        } catch (ADODB_Exception $e) {
            var_dump($e);
            return false;
        }
    }

    public function getOrganisationRole($clientid) {
        global $scramble;

        try {
            $rs = $this->instance->GetOne("
                SELECT
                ".DEFAULT_DB_HASH_FUNCTION."(CONCAT('{$scramble}', '_', ug.CLIENTID, '_', ug.USERGROUPID)) AS USERGROUPHASH
                FROM plr_usergroup ug
                WHERE ug.CLIENTID = ?
                AND ug.ROLE = ?
            ", [$clientid, 'ORGADMINS']);
        } catch (ADODB_Exception $e) {
            return false;
        }

        return $rs;
    }

    public function getApplicationRole($clientid, $applicationhash, $role) {
        global $scramble;

        // check that $role is either USERS or ADMINS
        if (!in_array($role, ['USERS', 'ADMINS', 'ORGADMINS'])) {
            return false;
        }

        try {
            $rs = $this->instance->GetRow("
                SELECT c.clientid, c.usergroupid
                FROM plr_applicationpermission c
                LEFT JOIN plr_usergroup ug ON c.clientid = ug.clientid AND c.usergroupid = ug.usergroupid
                WHERE c.CLIENTID = ?
                AND ".DEFAULT_DB_HASH_FUNCTION."(CONCAT('{$scramble}', '_', c.CLIENTID, '_', c.APPLICATIONID)) = ?
                AND ug.ROLE = ?
            ", [$clientid, $applicationhash, $role]);
        } catch (ADODB_Exception $e) {
            return false;
        }

        return $rs['usergroupid'];
    }

    public function getApplicationUserRole($clientid, $applicationhash) {
        return $this->getApplicationRole($clientid, $applicationhash, 'USERS');
    }

    public function getApplicationAdminRole($clientid, $applicationhash) {
        return $this->getApplicationRole($clientid, $applicationhash, 'ADMINS');
    }

    function getApplicationGroupObject($clientid, $grouphash) {
        $rs = $this->instance->GetRow('SELECT * FROM plr_usergroup WHERE clientid = ? AND usergroupid = ?', [$clientid, $grouphash]);
        return $rs;
    }

    /**
     * Check if the reset hash exists
     */
    public function resetHashExists($hash) {
        global $UserGroupV2Hash;

        if (empty(trim($hash))) return false;
        if (strlen($hash) != 32) return false;

        $_updatesql = "SELECT {$UserGroupV2Hash} AS USERGROUPHASH FROM plr_usergroup u WHERE RESETHASH = ?";
        $_result = $this->instance->GetOne($_updatesql, array($hash));
        if ($_result) {
            return $_result;
        } else {
            return false;
        }
    }

    /**
     * Reset the userpassword and provide a reset link
     */
    public function createResetPasswordLink($emailaddress) {
        global $_GVARS;

        $_result = false;
        $emailaddress = strtoupper($emailaddress);
        $_user = new base_plrUserGroup($this);
        $_user->loadRecordByEmailAddress($emailaddress);

        if ($_user->record) {
            $_resetHash = MD5(date('U').$emailaddress.self::_saltReset);
            $_user->updateUser(['RESETHASH' => $_resetHash]);
            $_result = $_GVARS['serverroot']."/../resetpassword/$_resetHash/";
        }
        return $_result;
    }

    // public function getUserGroupID($usergrouphash) {
    //     global $_sqlUserGroupV2;

    //     $result = $this->instance->GetRow($_sqlUserGroupV2, array($usergrouphash));
    //     return $result;
    // }

    function unsetAuthentication() {
        unset($_SESSION['plrauth']);
    }

    function checkSessionHijack() {
        if (isset($_SESSION['HTTP_USER_AGENT'])) {
            if ($_SESSION['HTTP_USER_AGENT'] != md5($_SERVER['HTTP_USER_AGENT'])) {
                $this->unsetAuthentication();
            }
        } else {
            $_SESSION['HTTP_USER_AGENT'] = md5($_SERVER['HTTP_USER_AGENT']);
        }
    }
    public function isLoggedOn() {
        /**
         * Session Hijack check
         */
        $this->checkSessionHijack();

        /**
         * Is logon authorized?
         */
        $_isLoggedOn = (isset($_SESSION['plrauth']) && $_SESSION['plrauth'] && $this->is2FAChecked());

        return $_isLoggedOn;
    }

    public function isApiCall() {
        $_result = isset($_GET['api_token']) && isset($_GET['service']);
        return $_result;
    }

    public function is2FARequired() {
        /**
         * Is 2FA required
         */
        return isset($_SESSION['2falogonrequired']) and ($_SESSION['2falogonrequired'] == true);
    }

    public function is2FAChecked() {
        /**
         * Is 2FA required
         */
        $_2farequired = $this->is2FARequired();
        return !$_2farequired or ($_2farequired and $_SESSION['2fachecked'] == true);
    }

    public function throttleLogins() {
        $throttle = array(10 => 1, 20 => 2, 30 => 5);

        // retrieve the latest failed login attempts
        $sql = "SELECT MAX(sessionstart) as ATTEMPTED FROM plr_logbase WHERE status = 'INVALID_LOGIN'";

        $this->connectInstance();
        $result = $this->instance->Execute($sql);
        if ($result->RecordCount() > 0) {
            $rs = $result->FetchNextObject(true);

            $latest_attempt = (int) date('U', strtotime($rs->ATTEMPTED));
            // get the number of failed attempts
            $sql = "SELECT COUNT(1) AS failed
                    FROM plr_logbase
                    WHERE STATUS = 'INVALID_LOGIN'
                    AND SESSIONSTART > DATE_SUB(NOW(), INTERVAL 15 MINUTE)";
            $result = $this->instance->GetOne($sql);
            if ($result > 0) {
                $failed_attempts = (int) $result;
                krsort($throttle);
                foreach ($throttle as $attempts => $delay) {
                    if ($failed_attempts > $attempts) {
                        // we need to throttle based on delay
                        $remaining_delay = $latest_attempt + ($delay * 60) - time();
                        $remaining_delay_in_min = ceil($remaining_delay / 60);
                        // output remaining delay
                        if ($remaining_delay <= 0) {
                            throw new Exception('U moet ' . abs($remaining_delay_in_min) . ' minuten wachten voordat u weer kunt inloggen.');
                        }
                    }
                }
            }
        }
        return false;
    }

    public function generate2FACode($username) {
        $_2facode = rand(1000, 9999);

        $_sql = "UPDATE plr_usergroup SET `2facode` = ? WHERE usergroupname = ?";
        $_result = $this->instance->Execute($_sql, array($_2facode, $username));

        if ($_result)
            return $_2facode;
        else
            return false;
    }

    public function send2FACheck($phonenumber, $twofacode) {
        global $_CONFIG;

        unset($_SESSION['2falogon']);
        $_message = $twofacode;
        $this->logUserAction('2FA code send', session_id(), $username, $phonenumber.' '.$twofacode);
        $spryng = new Client($_CONFIG['spryng_username'], $_CONFIG['spryng_password'], $_CONFIG['spryng_companyname']);
        $spryng->sms->send($phonenumber, $_message, array(
          'route'     => 'Business',
          'allowlong' => true
        ));
    }

    public function check2FACode($code) {
        $username = $_SESSION['name'];
        if (empty($username)) $username = $_SESSION['groupname'];
        $_sql = "SELECT `2facode` FROM plr_usergroup WHERE usergroupname = ?";
        $_thecode = $this->instance->GetOne($_sql, array($username));
        return $_thecode == $code;
    }

    public function processWebForm($post) {
        global $_sqlFormHashed;
        global $_sqlReplyRules;
        global $_GVARS;

        require_once(PLR_DIR.'/includes/dbroutines.inc.php');
        // require_once PLR_DIR.'/classes/temp_engine/ext_smarty.class.php';
        require_once(PLR_DIR.'/includes/func_email.inc.php');

        $mailresult = false;
        $ignoreitems = 'form;emailfield;namefield;sendto;redirect;ignore;submit;reset;required;s3capcha;flytrapfield;interesses';

        /* Check required fields */
        if (isset($post['required'])) {
            $requiredfields = explode(',',$post['required']);
            $error = '';
            foreach($requiredfields as $reqfield) {
                if ($post[$reqfield] == '') {
                    $error .= "<p>Het veld \"$reqfield\" dient ingevuld te worden.</p>\r\n";
                }
            }
            if ($error != '') {
                echo "<html>$error <p><a href=\"javascript: history.go(-1)\">Ga terug naar formulier</a><p></html>";
                die();
            }
            // $redirect = $_SERVER['HTTP_REFERER'];
        }

        $has_link = stristr($post['naam'], 'http://') ?: stristr($post['naam'], 'https://');
        // if ($post['interesses'] !== '' or $has_link or strpos($_SERVER['HTTP_REFERER'], 'https://') === FALSE) {
        //     echo "Het lijkt erop alsof er een spambot actief is...";
        //     $this->logQuick('Unprocessed mail: ' . var_export($post, true). "\n\n".var_export($_SERVER, true));
        //     die();
        // } else {
        //     $this->logQuick('Processed mail: ' . var_export($post, true). "\n\n".var_export($_SERVER, true));
        // }

        //$this->logQuick('Processed mail: ' . var_export($post, true). "\n\n".var_export($_SERVER, true));

        $subject = 'Bericht ontvangen via '.$_SERVER['HTTP_REFERER'];
        $emailfield = $post['emailfield'];
        $namefield = $post['namefield'];

        $emailaddress = $post[$emailfield];
        $emailaddress = preg_replace("/\r/", "", $emailaddress);
        $emailaddress = preg_replace("/\n/", "", $emailaddress);
        if (isset($post[$namefield])) {
            $emailaddress = $post[$namefield].' <'.$emailaddress.'>';
        }

        $sendto = $post['sendto'];
        $sendto = preg_replace("/\r/", "", $sendto);
        $sendto = preg_replace("/\n/", "", $sendto);

        if (isset($post['redirect']))
            $redirect = $post['redirect'];
        else {
            $urlarray = parse_url ( $_SERVER['HTTP_REFERER'] );
            $redirect = $urlarray['scheme'].'://'.$urlarray['host'];
        };

        if ((trim($emailaddress) == '') or !ValidEmail($emailaddress))
            $emailaddress = 'no-reply@bonsit.nl';

        $message = 'Onderstaand bericht is binnengekomen via '.$_SERVER['HTTP_REFERER']."\r\n";
        $message .= 'op '.date("d-m-Y").' om '.date("H:i")."\r\n\r\n";

        reset($post);
        $insertfields = '';
        $insertvalues = '';
        while (list($key, $val) = each($post)) {
            if (strpos($ignoreitems, $key) === false) {
                $values = '';
                if (trim($val) == 'Array') {
                    while (list($var, $val2) = each($val)) {
                        if ($values != '')
                            $values .= ' en ';
                        $values .= $val2;
                    }
                } else {
                    $values = $val;
                }
                // WVH SHOULD FIX
                // $ivalues = mysql_real_escape_string($values);
                $ivalues = $values;
                $insertfields .= "$key, ";
                $insertvalues .= "'$ivalues', ";
                $insertfieldarray[$key] = $values;
                $message .= UcFirst($key).': '.$values."\r\n";
            }
        }

        $message .= "\r\n\r\n (verstuurd naar ".$sendto.")";
        $mailresult = sendmail($resp, $sendto, $subject, $sendto, $message);
        if (isset($post['form'])) {
            require_once(PLR_DIR.'/classes/base/plrBase.class.php');
            require_once(PLR_DIR.'/classes/base/plrForm.class.php');
            $this->initialize();
            $plrform = new base_plrForm($this);
            if ($plrform->LoadRecordHashed($post['form'])) {
                $plrform->database->connectUserDB();
                if ($plrform->database->userdb) {
                    $_message = json_decode(json_encode($mailresult), true);
                    // WVH SHOULD FIX
                    // $_message = mysql_real_escape_string($_message['Message']);
                    $_message = $_message['Message'];
                    $_sqlInsert = 'INSERT INTO '.$plrform->GetDataDestination().' ('.$insertfields.' sendto, datetimesend, ipaddress, transmissionok, errormessage) ';
                    $_sqlInsert .= "VALUES ($insertvalues '$sendto', '".date("Y-m-d H:i")."', '".$_SERVER['REMOTE_ADDR']."', 'N', '".$_message."')";
                    $result = $plrform->database->userdb->Execute($_sqlInsert);
                    if (!$result)
                        $this->logUserAction('form failed', session_id(), $username, $plrform->database->userdb->ErrorMsg()."\r\n".$_sqlInsert."\r\n".$insertvalues);

                    # And send a `mailsend` to the queue, so client modules
                    # can do some processing, if needed
                    $_message = array(
                        'insertid' => $plrform->database->userdb->Insert_ID()
                    );
                    $this->AddToQueue('queue_mailsend_'.$post['form'], $_message);
                } else {
                    $this->logQuick('Mail processing: could not connect to database');
                }
            } else {
                $this->logQuick('Mail processing: formhash not correct: '.$post['form']);
            }

            if ($mailresult and $plrform->GetDataDestination()) {
                $_sqlUpdateOK = 'UPDATE '.$plrform->GetDataDestination().' SET transmissionok = "Y" WHERE messageid = "'.$plrform->database->userdb->Insert_ID().'"';
                @$plrform->database->userdb->Execute($_sqlUpdateOK);

                $plrform->ProcessReplyRules($emailaddress, $insertfieldarray);
            }
        } else {
            $this->logQuick('Mail processing: formhash not provided');
        }
        return $redirect;
    }

    public function UpdateAlertRecord($db, $tablename, $alertrecord, $userrecord, $keycolumn, $mailid) {
        $_updateSQL = 'UPDATE '.$tablename;
        $_updateSQL .= ' SET '.$alertrecord['STATUSCOLUMN'].' = ifNULL('.$alertrecord['STATUSCOLUMN'].', 0) + 1 WHERE '.$keycolumn.' = \''.$userrecord['PLR__RECORDID'].'\'';
        $db->Execute($_updateSQL);

        try {
            $_updateSQL = 'UPDATE '.$tablename;
            $_updateSQL .= ' SET __MESSAGEID = \''.$mailid.'\' WHERE '.$keycolumn.' = \''.$userrecord['PLR__RECORDID'].'\'';
            $db->Execute($_updateSQL);
        } catch (ADODB_Exception $e) {
            $this->logQuick("De tabel {$tablename} heeft geen __MESSAGEID kolom");
        }
    }

    public function GetTemplateModel($alertrecord, $userrecord, $appobject, $clientname) {
        // Keep the PLR__RECORD because we need it in the mails for id'ing
        $plrrecord = $userrecord['PLR__RECORDID'];
        $_templatemodel = StripHiddenFields($userrecord);
        $_templatemodel['PLR__RECORDID'] = $plrrecord;
        $_templatemodel = array_change_key_case($_templatemodel, CASE_UPPER);
        $_templatemodel['PRODUCT_NAAM'] = "{$appobject->record->REGISTERED} {$appobject->record->APPLICATIONNAME}";
        $_templatemodel['BEDRIJF_NAAM'] = $this->plrClient->record->NAME;
        $_templatemodel['BEDRIJF_ADRES'] = $this->plrClient->record->ADDRESS;
        $_templatemodel['BEDRIJF_POSTCODE'] = $this->plrClient->record->ZIPCODE;
        $_templatemodel['BEDRIJF_PLAATSNAAM'] = $this->plrClient->record->CITY;
        $_templatemodel['SUBJECT'] = $this->ReplaceDynamicVariables($alertrecord['SUBJECT']);
        $_templatemodel['CLIENTNAME'] = $clientname;

        return $_templatemodel;
    }

    public function SendAlert($alertrecord, $userrecord, $appobject, $recordindex, $recordcount) {
        global $_CONFIG;

        require_once(PLR_DIR.'/includes/func_email.inc.php');

        $result = true;
        $_from = $_CONFIG['alerter_fromaddress'];
        $_subject = $this->ReplaceDynamicVariables($alertrecord['SUBJECT']);

        $_sendto = $alertrecord['ADDRESSEE'];
        $_sendto = preg_replace("/\r/", "", $_sendto);
        $_sendto = preg_replace("/\n/", "", $_sendto);
        $_sendto = $this->ReplaceDynamicVariables($_sendto, $nullvalue='', $userrecord);

        if ($_sendto == '') {
            echo "No email address found for alert record. Skipping alert...\r\n";
            $this->logQuick("No email address found ({$_sendto}) for alert record. Skipping alert...". var_export($alertrecord, true));
            return false;
        }

        echo "\r\n  Sending alert to {$_sendto} \r\n\r\n";
        $_templateid = $alertrecord['TEMPLATEID'];

        if ($recordindex <= $_CONFIG['alerter_sendmaximum']) {
            if (!empty($_templateid)) {
                $_api_token = $this->plrClient->record->POSTMARK_API_TOKEN;
                $_clientname = $this->plrClient->record->NAME;
                $_templatemodel = $this->GetTemplateModel($alertrecord, $userrecord, $appobject, $_clientname);
                $mailresult = sendmailWithTemplate($_from, $_sendto, $_templateid, $_templatemodel, $_api_token);
                if ($mailresult) {
                    echo "Alert send to {$_sendto} ({$_subject}) with template {$_templateid} (messageid: {$mailresult->messageid})";
                    $result = $mailresult->messageid;
                } else {
                    $result = false;
                    echo "\r\nERROR: Could not send alert to {$_sendto} ({$_subject}) with template {$_templateid}";
                }
            } else {
                $_bodytext = $this->ReplaceDynamicVariables($alertrecord['BODYTEXT'], $nullvalue='', $userrecord);
                if ($recordindex == $_CONFIG['alerter_sendmaximum']) {
                    $_bodytext .= "\r\n\r\n Er zijn nog meer gevallen bekend maar deze worden niet ge-emailed.";
                }
                $mailresult = sendmail($resp, $_from, $_subject, $_sendto, $_bodytext, 'Alerter');
                echo "Alert send to {$_sendto} ({$_subject})";
                $result = $mailresult->messageid;
            }
        }

        return $result;
    }

    public function ProcessAlerts() {
        global $_sqlFormHashed;
        global $_sqlAlerts;

        $rs = $this->instance->GetArray($_sqlAlerts);
        if ($rs) {
            // Loop through the alerts
            foreach($rs as $alertrec) {
                $this->plrClient = new base_plrClient($this, $alertrec['CLIENTID']);
                $this->plrClient->LoadRecord();
                unset($_SESSION['clientparameters']);
                $this->plrClient->LoadClientParameters();

                // because the alerts are processed with user session
                // we store the clienthash in the session, so the rest of the code
                // works as expected
                if (!isset($this->plrClient->record->CLIENTHASHID)) {
                    echo "\r\n{$alertrec['CLIENTID']} is not a valid clientid. Skipping alert...\r\n";
                    continue;
                }
                $_SESSION['clienthash'] = $this->plrClient->record->CLIENTHASHID;

                $plrapp = new base_plrApplication($this->plrClient);
                $plrapp->LoadRecord(array($_SESSION['clienthash'], $alertrec['APPLICATIONID']), encoded:false);

                if ($plrapp->record == false) {
                    echo "Application {$alertrec['APPLICATIONID']} not found. Skipping alert...\r\n";
                    continue;
                }

                $plrform = new base_plrForm($plrapp);
                if ($plrform->LoadRecord(array($alertrec['CLIENTID'], $alertrec['FORMID']))) {
                    $plrform->database->connectUserDB();

                    if ($plrform->database->userdb) {
                        $_keycolumn = $plrform->database->makeRecordIDColumn();
                        if ($plrform->DataSourceIsSQL()) {
                            $_alert = $plrform->GetDataSource();
                            if (strpos($_alert, 'GROUP BY') > 0) {
                                $_alert .= ' HAVING '.$alertrec['FILTER'];
                            } else {
                                $_alert .= ' WHERE '.$alertrec['FILTER'];
                            }
                        } else {
                            $_alert = $plrform->MakeSQL(edit:false, countselect:false);
                            $_alert = sqlCombineWhere($_alert, $alertrec['FILTER']);
                        }

                        $datascope = $this->plrClient->GetDataScopeFilter($plrform->database->record->DATABASEID, $plrform->GetDataDestination());
                        $_alert = sqlCombineWhere($_alert, $datascope);
                        $_alert = $this->ReplaceDynamicVariables($_alert);

                        try {
                            $plrform->database->userdb->setFetchMode(ADODB_FETCH_ASSOC);
                            $_rs = $plrform->database->userdb->GetArray($_alert);
                        } catch (Exception $e) {
                            echo "Error processing alert: {$_alert}\r\n";
                            echo "continuing with next alert...\r\n";
                            continue;
                        }

                        $_recindex = 1;
                        foreach($_rs as $userrec) {
                            $_processrecord = TRUE;
                            if ($alertrec['ALERTTYPE'] !== 'ALL') {
                                $_processrecord = ($userrec[0] == $alertrec['ALERTVALUE']);
                            }
                            if ($_processrecord) {
                                $_recordcount = 1;
                                $_mailid = $this->SendAlert($alertrec, $userrec, $plrapp, $_recindex, $_recordcount);
                                if ($_mailid) {
                                    $this->UpdateAlertRecord($plrform->database->userdb, $plrform->GetDataDestination(), $alertrec, $userrec, $_keycolumn, $_mailid);
                                } else {
                                    // echo "Error sending alert to {$alertrec['ADDRESSEE']}. No mailid returned.\r\n";
                                }
                            }
                            $_recindex++;
                        }
                    }
                }
            }

        }
        return true;
    }

    public function ProcessReplyRules($formhash, $emailaddress, $insertValues) {
        global $_sqlReplyRules;

        require_once PLR_DIR.'/includes/func_email.inc.php';
        // require_once PLR_DIR.'/classes/temp_engine/mail_smarty.class.php';

        // locate and apply the reply rules for this form
        $rs = $this->plrInstance()->GetAssoc($_sqlReplyRules, array($formhash));
        if (!empty($rs)) {
            $smarty = new tempengine_mailSmarty($this->instance);
            foreach($rs as $rule) {
                $smarty->assign($insertValues);
                $processedbodytext = $this->ReplaceDynamicVariables($smarty->fetch('mail:'.$rule['RECORDID']));
                $processedsubjecttext = $smarty->fetch('mailsubject:'.$rule['RECORDID']);

                $mailresult = sendmail($resp, $rule['FROMADDRESS'], $processedsubjecttext, $emailaddress, $processedbodytext, $rule['MAILTAG']);
                // Don't because of  DynamicResponseModel:   $_message = json_decode(json_encode($mailresult), true);
                $result = $mailresult->messageid;

                $this->logUserAction('replied rule send ', session_id(), $username, $result);
                return $mailresult;
            }
        }
    }

    public function ProcessSingleReplyRule($formhash, $replyid, $emailaddress, $insertValues) {
        global $_sqlSingleReplyRule;

        require_once PLR_DIR.'/includes/func_email.inc.php';
        // require_once PLR_DIR.'/classes/temp_engine/mail_smarty.class.php';

        // locate and apply the reply rules for this form
        $rs = $this->plrInstance()->GetRow($_sqlSingleReplyRule, array($formhash, $replyid));
        if (!empty($rs)) {
            $rule = $rs;
            $smarty = new tempengine_mailSmarty();
            $smarty->assign($insertValues);
            $processedbodytext = $this->ReplaceDynamicVariables($smarty->fetch('mail:'.$rule['RECORDID']));
            $processedsubjecttext = $smarty->fetch('mailsubject:'.$rule['RECORDID']);
            $mailresult = sendmail($resp, $rule['FROMADDRESS'], $processedsubjecttext, $emailaddress, $processedbodytext, $rule['MAILTAG']);
            $this->logUserAction('replied rule send ', session_id(), $username, var_export(array($resp, $rule['FROMADDRESS'], $processedsubjecttext, $emailaddress, $processedbodytext, $rule['MAILTAG']), true));
            return $mailresult;
        }
    }

    public function &LoadAllRecordCountConstructs() {
        global $_sqlRecordCountCons;

        $rs = $this->instance->GetAll($_sqlRecordCountCons);

        $_constructs = array();
        foreach($rs as $rec) {
            $_construct = new base_plrConstruct($this);
            $_construct->LoadRecord(array($rec['CLIENTID'], $rec['APPLICATIONID'], $rec['CONSTRUCTID']));
            $_constructs[] = $_construct;
        }
        return $_constructs;
    }

    public function UpdateConstructRecordCount($clientid, $applicationid, $constructid, $count) {
        global $_sqlUpdateRecordCountConstruct;

        $this->instance->Execute($_sqlUpdateRecordCountConstruct, array($count, $clientid, $applicationid, $constructid));
    }

    public function cleanStringFromPasswords($str) {
        global $scramble;

        return str_replace($scramble, '&lt;SECRET POLARIS HASH&gt;', $str);
    }

    /**
     * Maintenance functions
     */
    public function getMaintenance() {
        global $_sqlCheckMaintenance;

        $rs = $this->instance->GetAll($_sqlCheckMaintenance);
        return $rs;
    }

    /**
    * Show recordset as list (for prails)
    */
    public function showAsList($sql, $params=false) {
        include("toexport.inc.php");
        $_rs = $this->instance->Execute($sql, $params);
        rs2tabout($_rs);
    }

    public function deleteApp($clientid, $appid) {
        $_app = new base_plrApplication($this);
        $_app->LoadRecord(array($clientid, $appid), $encoded = false);
        $_app->DeleteAppRecords($database, $deleteapprecord=true, $includeformrecords=true);
    }

    private function InitQueue($name='default') {
        global $_CONFIG;

        $this->queue = Pheanstalk::create($_CONFIG['beanstalkd_host'], $_CONFIG['beanstalkd_port']);
        $this->queue_tube = new TubeName($name);

        // we want jobs from 'default' only.
        $this->queue->watch($this->queue_tube);
    }

    public function GetQueue($name='default') {
        if ($this->queue == false) $this->InitQueue($name);

        return $this->queue;
    }

    public function AddToQueue($action, $data, $name='default') {
        if ($this->queue == false) $this->InitQueue($name);

        $job = array("action" => $action, "data" => $data);
        $this->queue->put(json_encode($job));
    }
}
