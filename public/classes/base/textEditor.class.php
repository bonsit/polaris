<?php

class textEditor {
    /**
    * The Real Editor
    */
    var $editor;

    /**
    * The value (text) in the editor box
    */
    var $value;

    /**
    * What editor is being used for display
    */
    var $style;

    /**
    * The editor name is the value of the NAME property of Input element
    */
    var $name;

    /**
    * What type of the editor do we use. Standard, Extended, .... (depends on the EditorStyle)
    */
    var $type;

    /**
    * The width of the editr (CSS style)
    */
    var $width;

    /**
    * The height of the editr (CSS style)
    */
    var $height;

    /**
    * Is the editor field required of not
    */
    var $required;

    /**
    * What is the length of editor field
    */
    var $maxlength;

    /**
    * Show the special editor or just a textarea tag
    */
    var $showDefaultEditor;

    /**
    * Any special tags that should be added to the Textarea tag
    */
    var $specialTags;

    /**
    * Set thefocus on the field?
    */
    var $autofocus;

    /**
    * constructor function
    */
    function __construct($editorType, $editorname='textEditor1', $editorStyle=false, $width=false, $height=false, $required=false, $readonly=false, $maxlength=false, $autofocus=false) {

	    $this->type = $editorType;
	    $this->name = $editorname;
	    $this->style = $editorStyle;
	    $this->width = $width;
	    $this->height = $height;
	    $this->maxlength = $maxlength;
	    if ($required) $this->required = 'required';
	    if ($readonly) {
	        $this->readonlytag = 'readonly="readonly"';
	        $this->readonlyclass = 'readonly';
	    }

	    if ($autofocus and $autofocus != '') {
	        $this->autofocus = 'autofocus="autofocus"';
	    }
    }

    /**
    * Show editor on screen
    */
    function setValue($value) {
        $this->value = $value;
    }

    /**
    * Special tags (like readonly, label, required)
    */
    function SetSpecialTags($specialtags) {
      $this->specialTags = $specialtags;
    }

    /**
    * Show editor on screen
    */
    function Render() {
        echo $this->Create();
    }

    /**
    * Create the editor HTML code
    */
    function Create() {
	    if (!$this->width) $this->width = '100%';
	    if (!$this->height) $this->height = '200px';

        $_showEditor = false;
        switch(strtoupper(string: $this->type)) {
	        case 'SUMMERNOTE':

                $html = "<textarea style=\"width:{$this->width};height:$this->height\" {$this->autofocus} {$this->maxlength} name=\"{$this->name}\" id=\"_fld{$this->name}\" {$this->specialtags} {$this->readonlytag} class=\"text {$this->required} {$this->readonlyclass}\">$this->value</textarea>";
                $html .= "
                        <script type='text/javascript'>
                            $(document).ready(function() {
                                $('#_fld{$this->name}').summernote({
                                    lang: 'nl-NL',
                                    minHeight: 300,
                                });
                            });
                        </script>";
                $showDefaultEditor = true;
                break;
            case 'MARKDOWN':
                $GLOBALS['includeMarkdownEditor'] = true;
                if ($_GET['downify']) {
                    include('guicomponents/wmarkdown/markdownify_extra.php');
                    $md = new Markdownify();
                    $this->value = $md->parseString($this->value);
                }
                $html .= "<textarea data-provide=\"markdown\" data-hidden-buttons=\"cmdUrl cmdImage\"  data-iconlibrary=\"fa\" id=\"_fld{$this->name}\" style=\"white-space: pre-wrap;width:{$this->width};height:{$this->height}\" name=\"{$this->name}\" value=\"{$this->name}\" {$this->readonlytag} class=\"markdownpreview {$this->required} {$this->readonlyclass}\">{$this->value}</textarea>";

                $showDefaultEditor = true;
                break;
            default:
                $showDefaultEditor = false;
                break;
        }

        if (!$showDefaultEditor) {
            $html = "<textarea style=\"width:{$this->width};height:$this->height\" {$this->autofocus} {$this->maxlength} name=\"{$this->name}\" id=\"_fld{$this->name}\" {$this->specialtags} $labeltag {$this->readonlytag} class=\"text {$this->required} {$this->readonlyclass}\">$this->value</textarea>";
        }
        return $html;
	}

}
