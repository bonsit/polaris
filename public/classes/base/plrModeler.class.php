<?php
# Polaris - a webapplication generator

# Copyright (C) 2000 - 2008  Debster - Bart Bons

# This is the 'Modeler' module to create FTD, cogNIAM and (future other diagrams).

# this module is accessable as a php class
# and through query parameters (via plrmodeler.php):
#   http://polaris/modeler/ftd/draw/<db>/<tablename>/
#    >> result: FactTypeDiagram of table <tablename> from database identified by databasehash <db>

use Michelf\Markdown;

class base_plrModeler extends base_plrRecord {
    var $schema;

    function __construct($aowner, $key = false) {
        parent::__construct($aowner, $key);
    }

    function addConceptsToString($string, $concepts) {
        if ($concepts) {
            $_words = explode(' ',$string);
            foreach($_words as $k => $_word) {
                foreach($concepts as $_concept) {
                    if (strcasecmp($_word, $_concept['CONCEPT']) == 0
                    or strcasecmp($_word, '&lt;'.$_concept['CONCEPT'].'&gt;') == 0) {
                        $_words[$k] = '<abbr title="'.$_concept['DEFINITION'].'">'.$_word.'</abbr>';
                    }
                }
            }
            $string = implode(' ', $_words);
        }
        return $string;
    }

    function getSchema($domainhashid, $showentities=false, $skipprefix=false) {
        global $_sqlFactTypes;
        global $_sqlModelerPlaceHolders;
        global $_sqlKeyPlaceHolders;

        $this->schema = array();
        $rs = $this->plrInstance()->Execute( $_sqlFactTypes, array($domainhashid) );

        if (!$rs) Die('No domain found: '.$domainhashid);
        $i = 0;
        while ( $rec = $rs->FetchRow(true) ) {
            $realTableName = $rec['FACTTYPENAME'];
            $rec['FTDNAME'] = $rec['FACTTYPENAME'];
            if ( $skipprefix ) $rec['FTDNAME'] = str_replace( $skipprefix, '', $rec['FTDNAME'] );
            $this->schema[$i] = $rec;

            /*
            if ($showentities)
              $sql = $_sqlKeyPlaceHolders;
            else
              $sql = $_sqlModelerPlaceHolders;

            $this->plrInstance()->SetFetchMode(ADODB_FETCH_ASSOC);
            $rsc = $this->plrInstance()->Execute( $sql, array($domainhashid, $realTableName) );
            while ( $col = $rsc->FetchRow(true) ) {
              $col['DEFAULTVALUE'] = trim($col['DEFAULTVALUE']);
              $this->schema[$i]['columns'][] = $col;
            }
            */
            $i++;
        }
        return $this->schema;
	}

	function getFactType($domainhashid, $currentfacttype, $concepts=false, $showentities=false) {
        global $_sqlModelerPlaceHolders;
        global $_sqlKeyPlaceHolders;
        global $_sqlFactTypesTriggers;
        global $_sqlFactTypesUi;

	    if ($currentfacttype and ($currentfacttype != '')) {
	        $currentfacttype = strtolower($currentfacttype);
            foreach($this->schema as $_rec) {
                if (strtolower($_rec['FACTTYPENAME']) == $currentfacttype) {
                    $_rec['SENTENCEPATTERN'] = htmlentities($_rec['SENTENCEPATTERN'], ENT_QUOTES, "UTF-8");
                    $_rec['SENTENCEPATTERN'] = Markdown::defaultTransform($_rec['SENTENCEPATTERN']);

                    /**
                    * Process sentencepattern to include links to concepts
                    */
                    $_rec['SENTENCEPATTERN'] = $this->addConceptsToString($_rec['SENTENCEPATTERN'], $concepts);

                    /**
                    * Get the placeholders for this facttype and add them to the array
                    */
                    if ($showentities)
                      $sql = $_sqlKeyPlaceHolders;
                    else
                      $sql = $_sqlModelerPlaceHolders;
                    $this->plrInstance()->SetFetchMode(ADODB_FETCH_ASSOC);
                    $rsc = $this->plrInstance()->Execute( $sql, array($domainhashid, $_rec['FACTTYPENAME']) );
                    while ( $col = $rsc->FetchRow(true) ) {
                      $col['DEFAULTVALUE'] = trim($col['DEFAULTVALUE']);
                      $_rec['columns'][] = $col;
                    }

                    $rsc = $this->plrInstance()->Execute( $_sqlFactTypesTriggers, array($domainhashid, $_rec['FACTTYPENAME']) );
                    while ( $trigger = $rsc->FetchRow(true) ) {
                      $_rec['triggers'][] = $trigger;
                    }

                    $rsc = $this->plrInstance()->Execute( $_sqlFactTypesUi, array($domainhashid, $_rec['FACTTYPENAME']) );
                    while ( $trigger = $rsc->FetchRow(true) ) {
                      $_rec['ui'][] = $trigger;
                    }

                    return $_rec;
                }
            }
        }
        return false;
    }

}
