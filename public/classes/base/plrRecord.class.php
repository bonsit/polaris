<?php

class base_plrRecord extends base_plrBase {
    var $record = false;
    var $owner;
    var $key;
    var $tablename;

    function __construct(&$aowner, $key=false) {
        parent::__construct();

        $this->owner =& $aowner;
        if ($key)
            $this->key = $key;
    }

    function &AppObject() {
        $parent = $this->owner;

        while (isset($parent)) {
            if (strcasecmp(get_class($parent), 'base_plrapplication') == 0) {
                return $parent;
            }
            $parent = $parent->owner;
        }
    }

}
