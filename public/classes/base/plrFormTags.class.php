<?php

class base_plrFormTags extends base_plrRecords {
    var $tags;

    function __construct(&$aowner, $group='BOTTOM') {
        parent::__construct($aowner);

        $this->keyfields = array('CLIENTID', 'FORMID', 'TAGID');
        $this->LoadRecords($group);
    }

    function LoadRecords($group) {
        global $_sqlFormTags;

        $_po = $this->polaris();

        $rs = $this->dbExecute($_sqlFormTags, array($_SESSION['clientid'] ?? '', $this->owner->record->FORMID));
        $this->tags = array();
        while ($rec = $rs->FetchNextObject(true)) {
            $plrFormTag = new base_plrFormTag($this);
            $plrFormTag->record = $rec;
            $this->tags[] = $plrFormTag;
        }
    }

    function showTags($style = false) {
        if (count($this->tags) > 0) {
            $_activeTag = $this->getSelectedTag();
            echo "<div class='btn-toolbar'>";

            $prevGroup = null;

            foreach ($this->tags as $tagObject) {
                if ($tagObject->record->ACTIVE === 'Y') {
                    if ($tagObject->record->GROUPID !== $prevGroup) {
                        // Close previous group if not the first iteration
                        if ($prevGroup !== null) {
                            echo "</div>";
                        }
                        echo "<div class='btn-group'>";
                    }

                    $selected = ($tagObject === $_activeTag);
                    $tagObject->show($style, $selected);

                    // Update $prevGroup
                    $prevGroup = $tagObject->record->GROUPID;
                }
            }

            // Close the last group if any tags are present
            if ($prevGroup !== null) {
                echo "</div>";
            }

            echo "</div>";
        }
    }

    function hasActiveTags() {
        foreach ($this->tags as $tagObject) {
            if ($tagObject->record->ACTIVE === 'Y') {
                return true;
            }
        }

        return false;
    }

    function getSelectedTag() {
        $tagName = !empty($_GET['tag']) ? $_GET['tag'] : null;

        foreach ($this->tags as $tagObject) {
            if ($tagObject->record->METANAME === $tagName) {
                return $tagObject;
            } elseif ($tagObject->record->DEFAULTTAG === 'Y' && !$tagName) {
                return $tagObject;
            }
        }

        return false;
    }

}
