<?php

class base_plrApplications extends base_plrRecords {
    var $applications;
    protected $groupvalues;

    function __construct(&$aowner) {
        parent::__construct($aowner);
        $this->LoadRecords();
    }

    function LoadRecords() {
        global $_GVARS;
        global $_sqlCompleteApplicationList;
        global $_sqlClientApplicationList;
        global $_sqlGetUserGroupName;

        if (isset($_SESSION['userid'])) {
            $this->groupvalues = $this->owner->owner->GetGroupValues($_SESSION['userid']);
        } else {
            $this->groupvalues = '';
        }
        // store the first group of the current user in a session var
        $firstgroup = explode(',', $this->groupvalues);
        if (count($firstgroup) > 1) {
            $_SESSION['usergroup'] = $firstgroup[1];
            if (!isset($_SESSION['groupname'])) {
                $usergroupname = $this->dbGetOne($_sqlGetUserGroupName, array($this->owner->record->CLIENTID, $firstgroup[1])) or die($this->dbErrorMsg() );
                $_SESSION['groupname'] = $usergroupname;
            }
        }
        if (($_SESSION['usertype'] ?? null) == 'root' ) {
            $rs = $this->dbExecute($_sqlCompleteApplicationList);
        } else {
            if ($this->groupvalues) {
                $rs = $this->dbExecute($_sqlClientApplicationList, [$this->owner->record->CLIENTID, $this->groupvalues.'@!integer']);// or die($this->dbErrorMsg() );
            }
        }
        $this->applications = [];
        if ($rs ?? null) {
            while ($rec = $rs->FetchNextObject(true)) {
                $plrApplication = new base_plrApplication($this);
                $plrApplication->record = $rec;
                $plrApplication->groupvalues = $this->groupvalues;
                $plrApplication->createApplicationLink();
                $this->applications[] = $plrApplication;
            }
        }
    }

    function ShowApps() {
        global $BASEURL;
        foreach (array_keys($this->applications) as $index ) {
            $application = $this->applications[$index];
            echo '<div class="application"><b>&rsaquo; <a href="'.$BASEURL.'&amp;app='.$application->EncodedID().'">'.get_resource($application->record->APPLICATIONNAME).'</a> - '.$application->record->DESCRIPTION.'</b></div>';
        }
    }

    function ShowSelectList($currentappid, $attr = '') {
        echo ObjectSetAsSelectList($this->applications, $keycolumn = 'APPLICATIONID', $namecolumn = 'APPLICATIONNAME', $currentvalue = $currentappid, $id = 'appselect', $selectname = 'APPLICATIONID', $attr);
    }

    function &FindApplication($encodedid) {
        $result = null;
        foreach (array_keys($this->applications) as $index ) {
            $application = $this->applications[$index];
            if ($application->EncodedID() == $encodedid) {
                return $application;
            }
        }
    }

    function &DefaultApplication() {
        $result = null;
        if (count($this->applications) == 1) {
            $result =& $this->applications[0];
        } else {
            foreach (array_keys($this->applications) as $index ) {
                $application = $this->applications[$index];
                if ($application->record->DEFAULTAPPLICATION == 'Y') {
                    $result =& $application;
                    break;
                }
            }
        }
        return $result;
    }

}
