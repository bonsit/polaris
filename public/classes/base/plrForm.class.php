<?php
use PHPStan\Reflection\FunctionReflection;
use PHPStan\Type\Php\FilterFunctionReturnTypeHelper;
use Michelf\Markdown;

class formOptions {
    var $showbuttonbar = true;
}

class base_plrForm extends base_plrRecord {
    var $pages;
    var $actions;
    var $formtags;
    var $searchcolumns;
    var $userdb;
    var $recordcount;
    var $startno;
    var $limit;
    var $refresh_interval;
    var $SQL;
    var $publishform;
    var $masterfields;
    var $detailfields;
    var $groupfilters;
    var $servicecall = false;
    var $moduleobj;
    var $datafiltered;
    var $rsdata; // the records for the listview
    var $database;
    var $currentGroupFilterValue;
    var $basehref;
    var $detail;
    var $detailform;
    var $currentdetailformname;
    var $databasehash;
    var $outputformat;
    var $resourcesinuse = null;
    var $options = null;

    var $masterrecord = null;

    function __construct(&$aowner) {
        parent::__construct($aowner);

        $this->options = new formOptions();

        if (strcasecmp(get_class($aowner), 'Polaris') == 0)
            $this->__polaris = $aowner;

        $this->servicecall = (isset($_GET['service']) and $_GET['service']);
    }

    function LoadRecord($params, $metaname=false) {
        global $_sqlFormMeta;
        global $_sqlForm;
        global $_sqlFormWithoutUser;

        $this->key = $params;
        $groupvalues = GetGroupValues($this->plrInstance(), $params[0], $_SESSION['userid'] ?? '');
        if (($_SESSION['usertype'] ?? '') == 'root') {
            $theQuery = $_sqlFormWithoutUser;
        } elseif ($groupvalues) {
            $theQuery = ($metaname) ? $_sqlFormMeta : $_sqlForm;
            $theQuery = str_replace('#subselect#', $groupvalues, $theQuery);
        } else {
            $theQuery = $_sqlFormWithoutUser;
        }
        $rs = $this->dbExecute($theQuery, $params) or Die('query failed form: '.$this->dbErrorMsg());
        $this->record = $rs->FetchNextObject(true);
        $this->LoadSubRecords();
        return true;
    }

    function LoadRecordHashed($formidhashed) {
        global $_sqlFormHashed;

        if ($rs = $this->dbGetAll($_sqlFormHashed, array($formidhashed))) {
            $this->LoadRecord([$rs[0]['CLIENTID'], $rs[0]['FORMID']]);
            return true;
        } else {
            return false;
        }
    }

    function LoadSubRecords() {
        if ($this->record) {
            $this->pages = new base_plrPages($this);
            $this->actions = new base_plrActions($this);
            $this->formtags = new base_plrFormTags($this);
            $this->searchcolumns = new base_plrSearchColumns($this);
            $this->database = new base_plrDatabase($this);
            $this->database->LoadRecord([$this->record->CLIENTID, $this->record->DATABASEID]);
        }
    }

    function LoadDatabaseObject() {
        if ($this->record and !isset($this->database)) {
            $this->database = new base_plrDatabase($this);
            $this->database->LoadRecord([$this->record->CLIENTID, $this->record->DATABASEID]);
        }
    }

    function SetTableName($tablename) {
        $this->SQL = 'SELECT * FROM ' . strtolower($tablename);
    }

    function GetViewTypeParameters() {
      if ($this->record->VIEWTYPEPARAMS != '') {
          $param = array();
          $paramarray = explode(';',$this->record->VIEWTYPEPARAMS);
          foreach($paramarray as $param) {
            $paramkeyvalues = explode('=', $param);
            $params[$paramkeyvalues[0]] = $paramkeyvalues[1];
          }
          return $params;
      } else {
          return false;
      }
    }

    function GetViewTypeParameter($key) {
        $params = $this->GetViewTypeParameters();
            return $params[$key];
    }

    function IsLineReadOnly($rec) {
        $_result = FALSE;
        $_lineReadonly = $this->record->ROWREADONLYONVALUE;
        if ($_lineReadonly !== '') {
            $_readOnlyCombo = explode(':', $_lineReadonly);
            $_readOnlyColumnName = $_readOnlyCombo[0];
            $_readOnlyValues = $_readOnlyCombo[1];
            if (isset($rec->$_readOnlyColumnName) and strpos($_readOnlyValues.',', $rec->$_readOnlyColumnName.',') !== FALSE ) {
                $_result = TRUE;
            }
        }
        return $_result;
    }

    function RSSLink() {
        return $this->MakeURL(false,false,true).'?output=rss';
    }

    function ShowLinkedConstructs($tablename) {
        global $_sqlLinkedFormNew;
        global $_GVARS;

        $rs = $this->dbExecute($_sqlLinkedFormNew, array($this->record->CLIENTID, $this->record->DATABASEID, $tablename)) or Die('query failed join2: '.$_sqlLinkedForm.$this->dbErrorMsg());
        if ($rs->RecordCount() > 0)
            echo "<div id=\"linkedconstructs\"><ul>";
        while (!$rs->EOF) {
            $rec = $rs->FetchObject(true);
            $_basehref = $this->MakeURLQuery();
            $_link = str_replace($this->owner->record->METANAME,$rec->METANAME,$_basehref);
            $_link = add_query_arg($_link, 'qo[]', 'IN' );
            $_link = add_query_arg($_link, 'qc[]', $rec->COLUMNNAME );
            $_link = add_query_arg($_link, 'qv[]', '__ids__' );
            $_constructname = get_resource($rec->CONSTRUCTNAME);
            echo "<li><a href=\"{$_link}\" title=\"{$_constructname}\"><img height=\"16\" widht=\"16\" src='{$_GVARS['serverpath']}$rec->IMAGEURL' /><span>{$_constructname}</span></a></li>";
            $rs->MoveNext();
        }
        if ($rs->RecordCount() > 0)
            echo "</ul></div>";

    }

    function MakeJoinClauseFromTable($tablename, $type = 'SUPER', &$tableprefixcounter, &$firstdetailkey, &$joinarray) {
      global $_sqlSubsetItemsJoins;
      global $_sqlSubsetItemsNormal;
      global $TABLEPREFIX;
      global $_CONFIG;

      if (!isset($joinarray))
          $joinarray = array();

      $firstdetailkey = '';
      $tableprefixcounter = $tableprefixcounter + 1;
      $debug = false;
      switch ($type) {
      case 'SUPER':
        $sstablename = 'SUPERTABLENAME';
        $rs = $this->dbExecute($_sqlSubsetItemsNormal, array($this->record->CLIENTID, $this->record->DATABASEID, $tablename), $debug) or Die('query failed join2: '.$this->dbErrorMsg());
      break;
      case 'NORMAL':
        $sstablename = 'TABLENAME';
        $rs = $this->dbExecute($_sqlSubsetItemsJoins, array($this->record->CLIENTID, $this->record->DATABASEID, $tablename, $this->record->TABLENAME, $useinpin=false), $debug) or Die('query failed join4: '.$this->dbErrorMsg());
      case 'SUB':
        $sstablename = 'SUPERTABLENAME';
        $rs = $this->dbExecute($_sqlSubsetItemsJoins, array($this->record->CLIENTID, $this->record->DATABASEID, $this->record->TABLENAME, $tablename, $useinpin=false), $debug) or Die('query failed join4: '.$this->dbErrorMsg());
      }
      $aSQL = '';
      $subsetid = '';
      while (!$rs->EOF) {
            $rec = $rs->FetchObject(true);
            //$suptable = strtolower($rec->$sstablename); // WHY strtolower?
            $suptable = $rec->$sstablename;
            if ($this->database->record->USEDBASPREFIX == 'Y' and strpos($suptable, '.') === FALSE) {
                $suptable = $this->database->record->DATABASENAME .'.'. $suptable;
            }

            if ($rec->SUBSETID != $subsetid) {
                $tableprefixcounter = $rec->SUBSETID;
                $jointype = $rec->JOINTYPE ?? 'LEFT';
                if ( $this->database->record->DATABASETYPE == 'oci8plr') {
                    $aSQL .= " , $suptable sss$tableprefixcounter ";
                } else {
                    $aSQL .= " {$jointype} JOIN $suptable sss$tableprefixcounter ON ";
                }
            } else {
                if ( $this->database->record->DATABASETYPE != 'oci8plr')
                    $aSQL .= " AND ";
            }

            switch ($type) {
            case 'NORMAL':
                $supercolumn = "sss$tableprefixcounter.$rec->COLUMNNAME";
                if ( $this->database->record->DATABASETYPE != 'oci8plr')
                    $aSQL .= " $TABLEPREFIX.$rec->SUPERCOLUMNNAME=$supercolumn";
                $joinarray[$TABLEPREFIX.'.'.strtoupper($rec->SUPERCOLUMNNAME)] = array('COLUMN'=>$supercolumn, 'TYPE'=>$type, 'SUPERTABLE'=>$suptable);
                if ($firstdetailkey == '') $firstdetailkey = $supercolumn;
                break;
            case 'SUB':
            case 'SUPER':
                $supercolumn = "sss$tableprefixcounter.".strtoupper($rec->SUPERCOLUMNNAME);
                if ( $this->database->record->DATABASETYPE != 'oci8plr')
                $aSQL .= " $TABLEPREFIX.$rec->COLUMNNAME=$supercolumn";

                if (!isset($joinarray[$TABLEPREFIX.'.'.strtoupper($rec->COLUMNNAME)]))
                    $joinarray[$TABLEPREFIX.'.'.strtoupper($rec->COLUMNNAME)] = array('COLUMN'=>$supercolumn, 'TYPE'=>$type, 'SUPERTABLE'=>$suptable);
                if ($firstdetailkey == '') $firstdetailkey = $supercolumn;

                if (!empty($rec->FILTER)) {
                    $aSQL .= " AND {$rec->FILTER}";
                    // replace {prefix} with actual tableprefix
                    $aSQL = str_replace('{prefix}', "sss{$tableprefixcounter}", $aSQL);
                }
                break;
            }

            $rs->MoveNext();
            if ($rec->SUBSETID == $subsetid)
                $tableprefixcounter = $rec->SUBSETID;
            $subsetid = $rec->SUBSETID;
        }
        return $aSQL;
    }

    function MakeJoinClause(&$tableprefixcounter, &$joinarray) {
        $firstdetailkey = '';
        $join = $this->MakeJoinClauseFromTable($this->GetDataDestination(), 'SUPER', $tableprefixcounter, $firstdetailkey, $joinarray);
        return $join;
    }

    function MakeMasterWhereClause($masterrecord, $type="detailcolumns") {
        $where = '';
        foreach($this->keyfields as $keyfield) {
            $keyfound = array_search($keyfield, $this->detailfields);

            if ($keyfound !== false) {
                $masterkeyfield = $this->masterfields[$keyfound];
                if (isset($masterrecord->$masterkeyfield)) {
                    if ($type == "mastercolumns") $keyfield = $this->masterfields[$keyfound];
                    $where .= "$keyfield = '{$masterrecord->$masterkeyfield}' AND ";
                }
            }
        }
        $where = substr($where, 0, -5);
        return $where;
    }

    function MakeDetailWhereClause($masterrecord, $masterfields, $detailfields) {
        global $TABLEPREFIX;

        if (is_array($detailfields)) {
            $usetableprefix = !$this->DataSourceIsSQL();
            $aSQL = '';
            $cutoff = false;
            foreach($detailfields as $detailindex => $detailfield) {
                if ($usetableprefix)
                    $aSQL .= $TABLEPREFIX.'.';
                $masterfield = $masterfields[$detailindex];
                $aSQL .= "$detailfield = '{$masterrecord->$masterfield}' AND ";
                $cutoff = true;
            }
            if ($cutoff) $aSQL = substr($aSQL, 0, -5);
        }

        return $aSQL;
    }

    function GetMasterFields($supertablename='', $useinpin=false) {
        global $_sqlSubsetItemsJoins;

        $result = [];
        if ($supertablename == '') $supertablename = $this->owner->masterform->GetDataDestination($onlytablename=true);
        $rs = $this->dbExecute($_sqlSubsetItemsJoins, array($this->record->CLIENTID, $this->record->DATABASEID, $this->GetDataDestination($onlytablename=true), $supertablename, $useinpin?'Y':'N')) or Die('query failed master2: '.$this->dbErrorMsg());
        while ($rec = $rs->FetchNextObject(true)) {
            $result[] = strtoupper($rec->SUPERCOLUMNNAME);
        }
        return $result;
    }

    function GetDetailFields($supertablename='', $useinpin=false) {
        global $_sqlSubsetItemsJoins;

        $result = [];
        if ($supertablename == '') $supertablename = $this->owner->masterform->GetDataDestination($onlytablename=true);
        $rs = $this->dbExecute($_sqlSubsetItemsJoins, array($this->record->CLIENTID, $this->record->DATABASEID, $this->GetDataDestination($onlytablename=true), $supertablename, $useinpin?'Y':'N')) or Die('query failed detail2: '.$this->dbErrorMsg());
        while ($rec = $rs->FetchNextObject(true)) {
            $result[] = strtoupper($rec->COLUMNNAME);
        }
        return $result;
    }

    function GetKeyFields() {
        global $_sqlKeyColumns;

        $this->keyfields = array();
        $databasehash = encodeString($this->record->CLIENTID.'_'.$this->record->DATABASEID);
        $rs = $this->dbExecute($_sqlKeyColumns, array($databasehash, $this->GetDataDestination($onlytablename=true))) or Die('query failed master2: '.$this->dbErrorMsg());
        while ($rec = $rs->FetchNextObject(true)) {
          $this->keyfields[] = strtoupper($rec->COLUMNNAME);
        }
        return $this->keyfields;
    }

    function GetItemName($param = 0) {
      global $_GVARS;

      $result = '';
      $items = $this->i18n($this->record->ITEMNAME);

      $names = explode(';', $items);

      if ($items != '') {
        if ($names[$param] != '')
          $result = $names[$param];
      } else {
        if ($param == 0)
          $result = lang_get('item');
        else
          $result = lang_get('items');
      }
      return $result;
    }

    function ItemName() {
      return $this->GetItemName(0);
    }

    function ItemsName() {
        if ($this->formtags->hasActiveTags())
            return $this->i18n($this->formtags->getSelectedTag()->record->TAGNAME);
        else {
            return $this->GetItemName(1);
        }
    }

    function HasOption($option) {
      return (strpos($this->record->OPTIONS, $option) !== false );
    }

    function ClearGroupFilter() {
      foreach($_SESSION as $key => $value) {
        if ((substr($key, 0, strpos($key, '.')) == $this->owner->record->CONSTRUCTID)
          and (substr($key, strpos($key, '.') + 1, strlen(GROUP_PREFIX)) == GROUP_PREFIX) ) {
          unset($_SESSION[$key]);
        }
      }
    }

    function AddGroupFilter($where) {
        global $TABLEPREFIX;

        $r = '';
        $result = $where;
        if (is_array($this->groupfilters)) {

            foreach($this->groupfilters as $key => $groupfilter) {
                if (($groupfilter['currentvalue'] == '')) {
                    $_currentValue = $this->currentGroupFilterValue;
                } else {
                    $_currentValue = $groupfilter['currentvalue'];
                }

                if (!empty($_currentValue)) {
                    $r .= $TABLEPREFIX.'.'.substr($key, strlen(GROUP_PREFIX))." = '$_currentValue'";
                    $r .= ' and ';
                }
            }
            $r = substr($r, 0, -5);
            if ($r != '' ) {
              $r = "($r)";

              if ($result != '' )
                $result .= ' AND ';
              $result .= $r;
            }
        }
        return $result;
    }

    function AddSearchFilter($where, $thesearchvalue = false, $includetableprefix = true, &$joinarray, &$joinedtables=false) {
        global $TABLEPREFIX;
        global $polaris;

        $qvalue = false;

        if ($includetableprefix)
            $prefix = $TABLEPREFIX.'.';
        else
            $prefix = '';

        if ($thesearchvalue)
            $qvalue = $thesearchvalue;
        elseif (isset($_GET['q']) and $_GET['q'] !== '')
            $qvalue = $_GET['q'];

        // Add support for 'qd[]' search value for detail forms
        if ($this->detail && isset($_GET['qd']) && $_GET['qd'] !== '') {
            $qvalue = $_GET['qd'];
        }

        if (is_array($qvalue)) {
            $qvalue = array_unique($qvalue);
            // sanitize the search values (strip_tags and other stuff)
            foreach($qvalue as $key => $value) {
                $qvalue[$key] = strip_tags($value);
            }
        }

        $qcolumns = $_GET['qc'] ?? null;
        $_operators = $_GET['qo'] ?? null;

        $qcompareoperator = $_GET['qcomp'] ?? 'AND';
        $searchwhere = '';
        $result = $where;
        $columnfieldlist = $this->GetColumnList($prefix, useinsearchfilter:true, joinarray:$joinarray);
        if ($columnfieldlist == '*') return false;
        $columnfields = explode(' , ', $columnfieldlist);

        if ( !isset($qcolumns) and isset($qvalue) and $qvalue !== '' ) {
            /***
            / Simple search
            */

            $searcharray = trim_array($qvalue);
            //$searcharray = explode('+',$search);
            if (isset($columnfields) and isset($searcharray)) {
                foreach($searcharray as $_index => $searchstring) {
                    $exactSearch = ($searchstring[0] == '\'' or $searchstring[0] == '"')
                      and ($searchstring[strlen($searchstring)] == '\'' or $searchstring[strlen($searchstring)] == '"');
                    if ($exactSearch) $searchstring = substr($searchstring, 1, -1);
                    $selectmode = false;//($_GET['select'] != '' or $exactSearch);

                    if ((strpos($searchstring,'%') === false) and !$selectmode) $searchstring = '%'.$searchstring.'%';

                    if (isset($_operators[$_index]))
                        $compare_operator = ' '.$_operators[$_index].' ';
                    else {
                        $compare_operator = ($selectmode)?' = ':' LIKE ';
                    }
                    $searchwhere .= ' (';
                    foreach($columnfields as $index => $field) {
                        if ($field != '') {
    //                        if (strpos($searchstring,'%') === false) $searchstring = '%'.$searchstring.'%';
                            $searchwhere .= $field.' '.$compare_operator.' \''.$searchstring.'\'';
                            $searchwhere .= ' OR ';

                            if ($this->database->IsOracle()) {
                                // if searchall then also look for search-string in the middle of text, but only for the second, third... field
                                if ($index > 0 and $_GET['searchall'] == 'true' and ($searchstring[0] != '%')) {
                                    $searchwhere .= $field.' '.$compare_operator.' \'%'.$searchstring.'\'';
                                    $searchwhere .= ' OR ';
                                }

                                $searchwhere .= $field.' '.$compare_operator.' \''.strtoupper($searchstring).'\'';
                                $searchwhere .= ' OR ';

                                // if searchall then also look for search-string in the middle of text, but only for the second, third... field
                                if ($index > 0 and $_GET['searchall'] == 'true' and ($searchstring[0] != '%')) {
                                    $searchwhere .= $field.' '.$compare_operator.' \'%'.strtoupper($searchstring).'\'';
                                    $searchwhere .= ' OR ';
                                    $searchwhere .= $field.' '.$compare_operator.' \'%'.nameize($searchstring).'\'';
                                    $searchwhere .= ' OR ';
                                }

                                // zoek ook op zoekcriterium met hoofdletter als eerste letter
                                if ($searchstring[0] == '%')
                                    $searchstringfirstcapital = substr($searchstring,0,1).strtoupper($searchstring[1]).substr($searchstring,2);
                                else
                                    $searchstringfirstcapital = strtoupper($searchstring[0]).substr($searchstring,1);
                                $searchwhere .= $field.' '.$compare_operator.' \''.$searchstringfirstcapital.'\'';
                                $searchwhere .= ' OR ';

                                // if searchall then also look for search-string in the middle of text, but only for the second, third... field
                                if ($index > 0 and $_GET['searchall'] == 'true' and ($searchstringfirstcapital[0] != '%')) {
                                    $searchwhere .= $field.' '.$compare_operator.' \'%'.strtoupper($searchstringfirstcapital).'\'';
                                    $searchwhere .= ' OR ';
                                }
                            }
                        }
                    }
                    $searchwhere = substr($searchwhere, 0, -4);
                    $searchwhere .= ') OR ';
                }
            }
            $searchwhere = substr($searchwhere, 0, -4);
        } elseif (isset($qcolumns)) {
            $qvalue = $_GET['qv'];
            /***
            * Extended search
            */
            $columnfieldlist = strtoupper($columnfieldlist);
            if (count($qcolumns) != count($qvalue)) {
                // equalize the amount of columns and values
                // this comes in handy when you want to search for several values in the same column(s).
                /*
                    qc[] = 'COLUMNX'
                    qv[] = 'value A'
                    qv[] = 'value B'

                    =>

                    qc[] = 'COLUMNX'
                    qc[] = 'COLUMNX'
                    qv[] = 'value A'
                    qv[] = 'value B'
                */
                if (count($qvalue) > count($qcolumns)) {
                    // when there are more values than columns, add the missing columnnames based on the first columnname of the array
                    $qcolumns = array_merge($qcolumns, array_fill(count($qcolumns), count($qvalue) - count($qcolumns), $qcolumns[0]));
                }
            }

            if (isset($qvalue))
                $searchvalues = array_combine( $qcolumns, $qvalue );
            else
                $searchvalues = $qcolumns;

            $_index = 0;
            if (isset($columnfields)) {
                foreach($searchvalues as  $searchfields => $_searchvalue) {
                    if ($searchfields == '') continue;
                    $searchfieldarray = explode('^',$searchfields);
                    $searchfield = $searchfieldarray[0];

                    $searchvaluearray = explode('^',$_searchvalue);
                    if (count($searchfieldarray) > count($searchvaluearray)) {
                        $searchvaluearray[] = $searchvaluearray[0];
                    }
                    $searchvalue = $searchvaluearray[0];

                    // floatvalue? then convert comma to point
                    if (strpos($searchvalue, ',') !== false and ctype_digit(str_replace(array(',','\''),'',$searchvalue))) {
                        $searchvalue = $polaris->ConvertFloatValue($searchvalue);
                    }

                    if ($includetableprefix)
                        $tmpsearchfield = '.'.$searchfield.' ';
                    else
                        $tmpsearchfield = ''.$searchfield.'';

                    $linkedtable = $_GET['qtable'][array_search($searchfields,$qcolumns)];
                    if ( ($searchvalue != '')
                    and ((stripos($columnfieldlist, $tmpsearchfield) !== false)
                        or (isset($searchfieldarray[1]) and (stripos($columnfieldlist, ' '.$searchfieldarray[1].' ') !== false)))
                        or ($linkedtable != '' and $searchvalue != '') // search in linked tables (like Inschrijving->Relatie.postcode)
                       ) {
                        // PREFIX is TTT. or the Linked table (like RELATIE.)
                        // Needs to be fixed??!!!
                        if ($linkedtable != '' and 1==2) {
                            $joinedtables = $this->MakeJoinClauseFromTable($linkedtable, 'SUB', $tableprefixcounter, $firstdetailkey, $joinarray);
//                            $tmpprefix =  ''; //$linkedtable.'.'; // Linkedtable like RELATIE. does not work (in Oracle) because of alias like sss8002
                            $tmpprefix = 'sss'.$tableprefixcounter.'.';
                            $firstdetailkey = '';
                        } else {
                            $tmpprefix = $prefix;
                        }
                        if (isset($_operators[$_index])) {
                            switch ( $_operators[$_index] ) {
                            case 'BT':
                                $searchwhere .= '('.$tmpprefix.$searchfield.' & '.$searchvalue . ' > 0 ';
                            break;
                            case 'IN':
                                $searchwhere .= '('.$tmpprefix.$searchfield.' IN ('.$searchvalue.')';
                            break;
                            case 'EQ':
                                $searchwhere .= '('.$tmpprefix.$searchfield.' = '.$searchvalue;
                            break;
                            case 'OJ':
                                $searchwhere .= '('.$tmpprefix.$searchfield.' (+) = '.$searchvalue;
                            break;
                            }
                            $compare_operator = ' '.$_operators[$_index].' ';
                        } else {
                            if ($searchvalue == '\'ISNULL\'') {
                                $compare_operator = ' IS ';
                                $searchvalue = 'NULL';
                            } elseif ($searchvalue == '\'ISNOTNULL\'') {
                                $compare_operator = ' IS NOT ';
                                $searchvalue = 'NULL';

                            } else {
                                $exactSearch = ($searchvalue[0] == '\'' or $searchvalue[0] == '"')
                                  and ($searchvalue[strlen($searchvalue)] == '\'' or $searchvalue[strlen($searchvalue)] == '"');
                                if ($exactSearch)
                                    $realsearchvalue = substr($searchvalue, 1, -1);
                                else
                                    $realsearchvalue = $searchvalue;

                                $selectmode = ($_GET['select'] != '' or $exactSearch);

                                /***
                                * check if the search value contains operator characters
                                */
                                $_searchops = array('<','>','=');
                                if (in_array($realsearchvalue[0], $_searchops)) {
                                    $_compare_operator = $realsearchvalue[0];
                                    $realsearchvalue = substr($realsearchvalue, 1);
                                    if (in_array($realsearchvalue[0], $_searchops)) {
                                        $_compare_operator .= $realsearchvalue[0];
                                        $realsearchvalue = substr($realsearchvalue, 1);
                                    }
                                } else {
                                    $_compare_operator = ' = ';
                                }
                                $searchvalue = $realsearchvalue;
                                $searchvalue_alt = nameize($realsearchvalue);
                                if (strpos($realsearchvalue,'%') === false and !$selectmode) {
                                    $searchvalue = '%'.$searchvalue.'%';
                                    $searchvalue_alt = '%'.$searchvalue_alt.'%';
                                }

                                $compare_operator = ($selectmode)?$_compare_operator:' LIKE ';
                                $searchvalue = '\''.$searchvalue.'\'';
                                $searchvalue_alt = '\''.$searchvalue_alt.'\'';
                            }
                            if ($searchvalue != 'NULL') {
                                $searchwhere .= '('.$tmpprefix.$searchfield.$compare_operator.$searchvalue;
                                // zoek ook op zoektermen met de eerste letter als hoofdletter (als er verschil is)
                                if ($searchvalue != $searchvalue_alt)
                                    $searchwhere .= ' OR '.$tmpprefix.$searchfield.$compare_operator.$searchvalue_alt;
                            } else {
                                $searchwhere .= $tmpprefix.$searchfield.$compare_operator.$searchvalue;
                            }
                        }
                        if ( count($searchfieldarray) > 1) {
                            array_shift($searchvaluearray);
                            foreach($searchvaluearray as $_index => $searchvalue) {
                                $_index++; // because of array_shift, we need to inc the index
                                if ($searchvalue[1] != '') {
                                    if ($searchvalue == 'ISNULL') {
                                        $compare_operator = ' IS ';
                                        $searchvalue = 'NULL';
                                    } elseif ($searchvalue == 'ISNOTNULL') {
                                        $compare_operator = ' IS NOT ';
                                        $searchvalue = 'NULL';
                                    } else {
                                        $exactSearch = ($searchvalue[0] == '\'' or $searchvalue[0] == '"');
                                        if ($exactSearch) $searchvalue = substr($searchvalue, 1, -1);
                                        $selectmode = ($_GET['select'] != '' or $exactSearch);
                                        if (strpos($searchvalue,'%') === false and !$selectmode) $searchvalue = '%'.$searchvalue.'%';

                                        // second search value, example: A,B='1','2'
                                        $sec_exactSearch = ($searchvalue[0] == '\'' or $searchvalue[0] == '"');
                                        if ($sec_exactSearch) $searchvalue = substr($searchvalue, 1, -1);

                                        $searchvalue = '\''.$searchvalue.'\'';
                                    }
                                    if ($searchvalue != 'NULL') {
                                        $_searchfield = isset($searchfieldarray[$_index])?$searchfieldarray[$_index]:$searchfieldarray[0];
                                        $searchwhere .= ' OR '.$tmpprefix.$_searchfield.$compare_operator.$searchvalue;
                                        $searchwhere .= ' OR '.$tmpprefix.$_searchfield.$compare_operator.$searchvalue_alt;
                                    } else {
                                        $searchwhere .= $tmpprefix.$searchfield.$compare_operator.$searchvalue;
                                    }
                                }
                            }
                        }
                        if ($searchvalue != 'NULL') {
                            $combiningoperator = ") $qcompareoperator ";
                            $searchwhere .= $combiningoperator;
                        }
                    }
                    $_index++;
                }
                if ($combiningoperator)
                    $searchwhere = substr($searchwhere, 0, -(strlen($combiningoperator)-1));
            }
        }

        if ($searchwhere != '') $searchwhere = "($searchwhere)";
        if ($result != '' and $searchwhere != '') $result .= ' AND ';
        $result .= $searchwhere;
        return $result;
    }

    function AddCustomFilter($where, $customfilter) {
        global $polaris;

        $result = $where;
        if (trim($customfilter) != '') {
            $customfilter = $polaris->ReplaceDynamicVariables($customfilter);

            if ($result != '') $result .= ' AND ';
            $result .= $customfilter;
        }
        return $result;
    }

    function AddFormFilter($where) {
        return $this->AddCustomFilter($where, $this->record->FILTER);
    }

    function AddTagFilter($where='') {
        /*
        return $where;
        */
        $_tagObject = $this->formtags->GetSelectedTag();
        if ($_tagObject) {
            $result = $this->AddCustomFilter($where, $_tagObject->record->FILTER);
        } else {
            $result = $where;
        }
        return $result;
    }

    function AddDataScopeFilter($where='', $tableprefix='ttt', $tablename=false) {
        $ao = $this->AppObject();
        $result = '';
        if (!$tablename) $tablename = $this->GetDataDestination();
        if ($ao) {
            $co = $ao->ClientObject();
            $result .= $this->AddCustomFilter($where, $co->GetDataScopeFilter($this->database->record->DATABASEID, $tablename, $tableprefix));
        } else {
            $result = $where;
        }
        return $result;
    }

    function GetAllPinColumns() {
        global $polaris;
        global $TABLEPREFIX;

        $_pinfilter = false;
        if (is_array($_SESSION['pinrecords'][$polaris->polarissession]) and count($_SESSION['pinrecords'][$polaris->polarissession]) > 0) {
            $_columnfieldlist = $this->GetColumnList($TABLEPREFIX);
            $_columnfieldlist = explode(' , ',$_columnfieldlist);

            foreach($_SESSION['pinrecords'][$polaris->polarissession] as $_table => $_pintable) {
                if ($this->GetDataDestination() !== $_table) {
                    foreach($_pintable as $_pinkey => $_pin) {
                        $_keys = array_keys($_pin);
                        // if column already found then don't add it again
                        // : because of RELATIENR and FACTUURRELATIE (NHA)
                        if (!isset($_pinfilter[$_keys[0]]))
                            $_pinfilter[$_keys[0]] = $_pin[$_keys[0]];
                    }
                }
            }
        }

        return $_pinfilter;
    }

    function GetPinColumns() {
        global $polaris;

        $_pinfilter = false;
        if (!empty($_SESSION['pinrecords'][$polaris->polarissession])) {
            $_columnfieldlist = $this->GetColumnList($prefix, $useinsearchfilter = false, $joinarray);
            $_columnfieldlist = explode(' , ',$_columnfieldlist);

            foreach($_SESSION['pinrecords'][$polaris->polarissession] as $_table => $_pintable) {
                if ($this->GetDataDestination() !== $_table) {
                    $_justtable = (strpos($_table, '.') !== FALSE) ? substr($_table, -(strlen($_table)-strpos($_table, '.')-1)):$_table;

                    $_masterfields = $this->GetMasterFields($_justtable, $useinpin=true);
                    $_detailfields = $this->GetDetailFields($_justtable, $useinpin=true);
                    foreach($_pintable as $_pinkey => $_pin) {
                        $_keys = array_keys($_pin);
                        /* find the corresponding subset master fieldname */
                        $keyfound = array_search($_keys[0], $_masterfields);
                        if ($keyfound !== false) {
                            $_key = $_detailfields[$keyfound];
                            if (array_search($_key, $_columnfieldlist) !== FALSE) {
                                $_pinfilter[$_key] = $_pin[$_keys[0]];
                            }
                        }
                    }
                }
            }
        }
        return $_pinfilter;
    }

    function AddPinFilter($where='', $currenttable=false, $searchactive=false) {
        global $_GVARS;
        global $polaris;
        global $TABLEPREFIX;

        $_pinfilter = '';
        $this->datafiltered = false;

        if (($_GET['skippinfilter'] ?? 'false') == 'true') return $where;

        $polaris->polarissession = '';
        if (isset($_SESSION['pinrecords'][$polaris->polarissession])) {
            $joinarray = array();
            $_columnfieldlist = $this->GetColumnList(false, $useinsearchfilter = false, $joinarray);
            $_columnfieldlist = explode(' , ', $_columnfieldlist);
            if ($this->owner->masterform)
                $supertablename = $this->owner->masterform->GetDataDestination($onlytablename=true);
            $_sametablekeys = $this->GetKeyFields();
            $_previousKey = false;
            foreach($_SESSION['pinrecords'][$polaris->polarissession] as $_table => $_pintable) {
                foreach($_pintable as $_pinkey => $_pin) {
                    $_SESSION['pinrecords'][$polaris->polarissession][$_table][$_pinkey]['ACTIVE'] = false;
                }
                $_justtable = (strpos($_table, '.') !== FALSE) ? substr($_table, -(strlen($_table)-strpos($_table, '.')-1)):$_table;

                // don't apply pins when user is searching in the same form as the pins are set
                if (!$searchactive && $currenttable !== $_justtable) {
                    $_masterfields = $this->GetMasterFields($_justtable, $useinpin=true);
                    $_detailfields = $this->GetDetailFields($_justtable, $useinpin=true);
                    foreach($_pintable as $_pinkey => $_pin) {
                        // $_previousKeys = is_array($_pintable[$_pinkey-1])?array_keys($_pintable[$_pinkey-1]):false;
                        $_keys = array_keys($_pin);
                        $applyfilter = false;
                        if (isset($_pin['VISIBLE']) && ($_pin['VISIBLE'] == FALSE)) {
                        } else {
                            $applyfilter = true;
                        }
                        if ($applyfilter) {
                            $_newGroup = ($_previousKey !== false and ($_keys[0] != $_previousKey));
                            /* find the corresponding subset master fieldname */
                            $keyfound = array_search($_keys[0], $_masterfields);

                            if ($supertablename == $_justtable) {
                                if ($_newGroup) {
                                   if ($_pinfilter != '') $_pinfilter = substr($_pinfilter, 0, -4);
//                                   $_pinfilter .= $_sametablekeys[0].' = \''.$_pin[$_keys[0]].'\'';
                                   $_pinfilter = $_pinfilter . ' ) AND ('.$_sametablekeys[0].' = \''.$_pin[$_keys[0]].'\' OR ';
                                } else {
                                    if (count($_sametablekeys) == 0) {
                                        trigger_error("Check keycolumn in plr_column for table: ".$_justtable );
                                        die();
                                    }
                                   $_pinfilter .= $TABLEPREFIX.'.'.$_sametablekeys[0].' = \''.$_pin[$_keys[0]].'\' OR ';
                                }
                            } elseif ($keyfound !== false) {
                                $_key = $_detailfields[$keyfound];

                                if ($this->IsPartOfArrayString($_key.'_JSON_SEARCH', $_columnfieldlist)) {
                                    $_pinfilter .= "JSON_CONTAINS({$TABLEPREFIX}.{$_key}, '{$_pin[$_keys[0]]}') OR ";
                                } elseif (array_search($_key, $_columnfieldlist) !== FALSE) {
                                    if ($_newGroup) {
                                        if ($_pinfilter != '') $_pinfilter = substr($_pinfilter, 0, -4);
                                        $_pinfilter = '('.$_pinfilter . ' ) AND ('.$TABLEPREFIX.'.'.$_key.' = \''.$_pin[$_keys[0]].'\'';
                                        $_addclosingpar = true;
                                        $_pinfilter .= ' OR ';
                                    } else {
                                        if (strpos($_pinfilter, $TABLEPREFIX.'.'.$_key.' = \''.$_pin[$_keys[0]] . '\'') === false) {
                                            $_pinfilter = $_pinfilter . $TABLEPREFIX.'.'.$_key.' = \''.$_pin[$_keys[0]] . '\' OR ';
                                        }
                                    }

                                    $_SESSION['pinrecords'][$polaris->polarissession][$_table][$_pinkey]['ACTIVE'] = true;
                                    $this->datafiltered = true;
                                }
                            }
                            $_previousKey = $_keys[0];
                        }
                    }
                }
            }
            if ($_pinfilter != '') {
                if (substr($_pinfilter, -4) == ' OR ')
                    $_pinfilter = substr($_pinfilter, 0, -4);
                if ($_addclosingpar)
                    $_pinfilter .= ' ) ';
                $_pinfilter = '('.$_pinfilter.') ';
            }
        }
        return $this->AddCustomFilter($where, $_pinfilter);
    }

    function IsPartOfArrayString($fieldname, $array) {
        $result = array_filter(
            $array,
            function( $columnfield ) use( $fieldname ) {
                return (strpos( $columnfield, $fieldname ) !== FALSE);
            }
        );
        return count($result) > 0;
    }

    function FormHasActivePins() {
        global $polaris;
        global $TABLEPREFIX;

        $_pinfilter = '';
        $this->datafiltered = false;

        if (isset($_SESSION['pinrecords'][$polaris->polarissession])) {
            $joinarray = array();
            $_columnfieldlist = $this->GetColumnList($TABLEPREFIX, $useinsearchfilter = false, $joinarray);
            $_columnfieldlist = explode(' , ',$_columnfieldlist);

            foreach($_SESSION['pinrecords'][$polaris->polarissession] as $_table => $_pintable) {
                $_justtable = (strpos($_table, '.') !== FALSE) ? substr($_table, -(strlen($_table)-strpos($_table, '.')-1)):$_table;

                $_masterfields = $this->GetMasterFields($_justtable, $useinpin=true);
                $_detailfields = $this->GetDetailFields($_justtable, $useinpin=true);
                foreach($_pintable as $_pinkey => $_pin) {
                    $_keys = array_keys($_pin);
                    /* find the corresponding subset master fieldname */

                    $keyfound = array_search($_keys[0], $_masterfields);
                    if ($keyfound !== false) {
                        $_key = $_detailfields[$keyfound];
                        if (array_search($_key, $_columnfieldlist) !== FALSE) {
                            $this->datafiltered = true;
                        }
                    }
                }
            }
        }
        return $this->datafiltered;
    }

    function _sortreorder($sort) {
        $result = array();
        $nr = 1;
        foreach($sort as $key => $values) {
            $result[$key] = array('nr'=>$nr,'dir'=>$values['dir']);
            $nr++;
        }
        return $result;
    }

    function GetSortArray() {
        $sort = array();
        $nr = 1;
        foreach($_GET as $getitem => $getvalue) {
            if (substr($getitem,0,5) == 'sort_') {
                $sort[$getvalue] = array('nr'=>substr($getitem,5), 'dir'=>$_GET['dir_'.substr($getitem,5)]);
                $nr++;
            }
        }
        uasort($sort, '_sortcmp');
        return $this->_sortreorder($sort);
    }

    function GetSortArrayOrderBy() {
        $sort = array();
        foreach($_GET as $getitem => $getvalue) {
            if (substr($getitem,0,5) == 'sort_')
                $sort[substr($getitem,5)] = array('sort'=>$getvalue, 'dir'=>$_GET['dir_'.substr($getitem,5)]);
        }
        ksort($sort);
        return $sort;
    }

    function AddUrlSortValues($href, $sortcolumns) {
        $result = $href;
        foreach($sortcolumns as $sort => $value) {
            $result = add_query_arg($result, 'sort_'.$value['nr'], $sort );
            $result = add_query_arg($result, 'dir_'.$value['nr'], $value['dir'] );
        }
        return $result;
    }

    function AddOrderBy($useprefix) {
        global $TABLEPREFIX;

        $orderby = '';

        if ($useprefix) $prefix = $TABLEPREFIX.'.'; else $prefix = '';
        $cl = $this->GetColumnList($prefix);

        $sortarray = $this->GetSortArrayOrderBy();
        foreach($sortarray as $k => $v) {
            $dir = ($v['dir'] == 'down')?' DESC':' ASC';
            if ((strpos($cl, $v['sort'].' ' ) !== false)) {
                $orderby .= $prefix.$v['sort'].$dir.',';
            } elseif (substr( $v['sort'], -12) == '_DETAILCOUNT') {
                $orderby .= $v['sort'].$dir.',';
            }
        }

        if ($orderby == '' and trim($this->record->SORTCOLUMNNAME) != '' ) {
            if (strpos($this->record->SORTCOLUMNNAME, '.') !== false OR strpos($this->record->SORTCOLUMNNAME, ',') !== false) {
                $orderby = $this->record->SORTCOLUMNNAME;
            } else {
                $orderby = $prefix.$this->record->SORTCOLUMNNAME;
            }
        }
        if (substr($orderby,-1) == ',') $orderby = substr($orderby,0,-1);
        if ($orderby != '')
            return ' ORDER BY '.$orderby;
        else
            return '';
    }

    function GetFormGroupFields() {
      $r = '';
      foreach($_GET as $key => $value) { //BART-POST
        if (substr($key, strpos($key, '.'), strlen(GROUP_PREFIX)) == GROUP_PREFIX ) {
          $r .= "<input type='hidden' name='".substr($key, strlen(GROUP_PREFIX))."' value='$value' />\r\n";
        }
      }
      return $r;
    }

    function StoreFormGroupFields() {
      if ($_GET['cancelfilter'] == 'true') { //BART-POST
        $this->ClearGroupFilter();
      }
      /*
      foreach($_GET as $key => $value) { //BART-POST
        // just for the _GRP_ variables
        if (substr($key, strpos($key, '.'), strlen(GROUP_PREFIX)) == GROUP_PREFIX ) {
          if (($value == '') or ($_GET['cancelfilter'] == 'true') ) {
            unset($_SESSION[$this->owner->record->CONSTRUCTID.'.'.$key]);
            unset($_GET[$key]); //BART-POST
          } else {
            $_SESSION[$this->owner->record->CONSTRUCTID.'.'.$key] = $value;
          }
        }
      }
      */
    }

    function GetColumnList($tblprefix, $useinsearchfilter = false, $joinarray = false, $forgroupby = false) {
        if ($tblprefix != '' and $tblprefix[strlen($tblprefix) - 1] != '.')
            $tblprefix .= '.';

        $return = '';
        if (isset($this->pages) and count($this->pages->pages) > 0) {
            foreach( array_keys($this->pages->pages) as $index ) {
                $page =& $this->pages->pages[$index];
                if (!isset($page->lines)) $page->LoadRecord();
                foreach( array_keys($page->lines->lines) as $lineindex ) {
                    $line =& $page->lines->lines[$lineindex];
                    $return .= $line->getColumns($tblprefix, $useinsearchfilter, $joinarray, $forgroupby);
                }
            }
            $return = substr($return, 0, -2);
            $_uniques = array_unique(explode(' , ',$return));
            $return = implode(' , ', $_uniques);
        } else {
            if (isset($this->record->MODULE)) {
                $_sql = "SELECT COLUMNNAME FROM plr_column WHERE CLIENTID = ? AND DATABASEID = ? AND TABLENAME = ?";
                $_allColumns = $this->dbGetCol($_sql, array($this->record->CLIENTID, $this->record->DATABASEID, $this->record->TABLENAME), true);
                $return = implode(' , ', array_map(function($col) use ($tblprefix) {
                    return $tblprefix . $col;
                }, $_allColumns));
            }
        }
        if (!$return) $return = "$tblprefix*";
        return trim($return);
    }

    function ReplaceDynamicVariables($text, $nullvalue='', $currentlinerecord=false) {
        $po = $this->polaris();
        $text = $po->ReplaceDynamicVariables($text, $nullvalue='', $currentlinerecord);
        if (strcasecmp(get_class($this->owner), 'base_plrconstruct') == 0) {
            $text = str_ireplace('%constructname%',$this->owner->record->CONSTRUCTNAME, $text);
        }
        $text = str_ireplace('%dbname%',$this->database->record->DATABASENAME, $text);
        return $text;
    }

    function removeOrderBy($sql) {
        $_pos_orderby = strpos($sql, 'ORDER ');
        if ($_pos_orderby !== FALSE) {
            if (strpos($sql, ')', $_pos_orderby) == FALSE)
                return substr($sql, 0, strpos($sql, 'ORDER '));
        }
        return $sql;
    }

    function AddJoinColumns($where, $joinarray) {
        if ( $this->database->record->DATABASETYPE == 'oci8plr') {
            if ($where != '')
                $where .= ' AND ';
            foreach($joinarray as $_column => $_supercolumn) {
                $_supercolumnname = $_supercolumn['COLUMN'];
                $_supercolumntype = $_supercolumn['TYPE'];
                //$_joinoption = $_supercolumntype == 'SUB' ? '' : '(+)';
                $_joinoption = '(+)';
                $where .= "$_column = $_supercolumnname $_joinoption AND ";
            }
            $where = substr($where, 0, -4);
        }

        return $where;
    }

    function MakeSQL($edit = false, $countselect = false, $thesearchvalue = false, $masterfields = false, $countdetails = false, $detailtable = false, $customfilter = false, $skipsearchpart = false, $skipallextrafiltering = false) {
      global $SALT;
      global $TABLEPREFIX;
      global $scramble;

      $clientid = $this->record->CLIENTID;
      $databaseid = $this->record->DATABASEID;
      // get the correct recorid in case of Master or Detail state
      if ( $masterfields and (isset($_GET['detailrec'])) ) { // or ($GLOBALS['detailaction'] == 'insert')
        $recordid = $this->database->customUrlDecode($_GET['detailrec']);
      } elseif (!$masterfields and isset($_GET['rec'])) {
        $recordid = $this->database->customUrlDecode($_GET['rec']);
      } else {
        $recordid = false;
      }

      if ( $this->DataSourceIsSQL() ) {
        /**
        * create SQL based on a custom SQL query
        */
        $sql = $this->GetDataSource();
        $table = $this->GetDataDestination();
        if (strtolower(get_class($this->owner)) == 'base_plrconstruct')
            $sql = $this->owner->AddConstructFilter($sql, $masterfields != false);
        $sql = $this->ReplaceDynamicVariables($sql, $nullvalue='NULL');
        if ($countselect == true) {
            $sql = preg_replace('@/\*_SELECTSTART_\*/.*?/\*_SELECTEND_\*/@s', 'count(*) as RECORDCOUNT', $sql); //Geeft deze problemen?
            $where = '';
            if (!$skipsearchpart and !$skipallextrafiltering)
                $where = $this->AddSearchFilter($where, $thesearchvalue, true, $joinarray);
            $where = $this->AddFormFilter($where);
            $where = $this->AddDataScopeFilter($where); //, $tableprefix=''
            if (!$skipallextrafiltering) {
                $where = $this->AddPinFilter($where);
                $where = $this->AddTagFilter($where);
            }
            $where = $this->AddCustomFilter($where, $customfilter);
            $sql = sqlCombineWhere($sql, $where, $this->database->IsOracle()?"WHERE":null);
            $sql = $this->removeOrderBy($sql);

        } else {
            if ($edit == false) {
                $where = '';
                if (!$skipsearchpart) {
                    $where = $this->AddSearchFilter($where, $thesearchvalue, true, $joinarray);
                }
                $where = $this->AddFormFilter($where);
                $where = $this->AddDataScopeFilter($where); //, $tabldeprefix=''
                $where = $this->AddPinFilter($where);
                $where = $this->AddTagFilter($where);
                $where = $this->AddCustomFilter($where, $customfilter);
                $sql = sqlCombineWhere($sql, $where, $this->database->IsOracle()?"WHERE":null);
                $_orderbyadded = false;
                if (!$masterfields) {
                    if (strtolower(get_class($this->owner)) == 'base_plrconstruct') {
                        $constructorderby = $this->owner->GetConstructOrderBy($masterfields != false);
                        if ($constructorderby) {
                            $sql .= $constructorderby;
                            $_orderbyadded = true;
                        }
                    }
                    if (!$_orderbyadded) {
                        $sql .= $this->AddOrderBy(true);
                    }
                }
            } else {
                if ($recordid) {
                    if ($table == '') {
                        echo "Please provide a UPDATETABLENAME in plr_form";
                    } else {
                        $where = $this->database->makeEncodedKeySelect($table, $recordid, $TABLEPREFIX);
                    }
                    $sql = sqlCombineWhere($sql, $where, 'HAVING');
                } else {
                    $sql = sqlCombineWhere($sql, "1=0", 'HAVING');
                }
            }
        }
      } else {
        /**
        * create SQL based on a tablename
        */
        $table = $this->GetDataDestination();
        $fromtable = $this->GetDataSource();
        if ($fromtable == '') return false;

        if ($countselect == true) {
            $sql = ' SELECT count(*) as RECORDCOUNT FROM '.$fromtable.$TABLEPREFIX;
            // Make LEFT JOIN with other table for including Lookup-values
            $tableprefixcount = 1;
            $join = $this->MakeJoinClause($tableprefixcount, $joinarray);  // WAAROM? Oracle is traag hierdoor
            if (!$this->database->IsOracle()) {
                $sql .= $join;
            }
            $where = '';

            if (strtolower(get_class($this->owner)) == 'base_plrconstruct')
                $where = $this->owner->AddConstructFilter($where, $masterfields != false);
            $where = $this->AddFormFilter($where);
            $where = $this->AddDataScopeFilter($where);
            if (!$skipallextrafiltering) {
                $where = $this->AddTagFilter($where);
                $where = $this->AddCustomFilter($where, $customfilter);
            }

            $where = $this->ReplaceDynamicVariables($where, $nullvalue='NULL');
            if ( !$masterfields or ($masterfields and $GLOBALS['detailonly'])) {
              $this->GetGroupFields();
              $where = $this->AddGroupFilter($where);
              if (!$skipsearchpart && !$skipallextrafiltering) {
                  $wheresearch = $this->AddSearchFilter($where, $thesearchvalue, $includetableprefix = true, $joinarray, $joinsearch);
                  $skippinfilter = $wheresearch != $where;
                  $where = $wheresearch;
//                  if (!$skippinfilter)

                  $where = $this->AddPinFilter($where, $fromtable, $skippinfilter);
              }
            }

            if (!$this->database->IsOracle() or ($this->database->IsOracle() and $joinsearch != '')) {
                $where = $this->AddJoinColumns($where, $joinarray);
                if ($join == '' or ($join != '' and strpos($join, $joinsearch) !== FALSE)) {
                    // $sql .= $join; //RAAR PROBLEEM met Inschrijving->Relatie.POSTCODE, was eerst $joinsearch
//                    $sql .= $joinsearch;  // Deze is nodig voor zoeken in AANVRAAG.POSTCODE en HUISNUMMER
                }
            }
            $sql = sqlCombineWhere($sql, $where);
        } else {
          if ($edit == false) {
            $tableprefixcount = 1;
            // Listview SQL
            // Make LEFT JOIN with other table for including Lookup-values
            $join = $this->MakeJoinClause($tableprefixcount, $joinarray);
            if ( $countdetails and !$this->database->IsOracle()) {
              // Make LEFT JOIN with detail table for including count(...) values
                $firstdetailkey = '';
                $join .= $this->MakeJoinClauseFromTable($detailtable, 'NORMAL', $tableprefixcount, $firstdetailkey, $joinarray_detail);
                $joinarray = array_merge($joinarray_detail, $joinarray);
            }
            $_recordselector = $this->database->makeRecordIDColumn($table, $TABLEPREFIX, false, $this->record->ISVIEW=='Y') . " AS "._RECORDID;
            $_allcolumns = $this->GetColumnList($TABLEPREFIX, $useinsearchfilter = false, $joinarray);
            $sql = ' SELECT '.$_allcolumns; // BART: DISTINCT uitzoeken
            $sql .= ', '.$_recordselector; //.', '.$_prevnext_recordselector;
            if ( $countdetails and !$this->database->IsOracle()) {
              if ($firstdetailkey != '') {
                 $detailtable = $table;
                 $sql .= ', COUNT('.$firstdetailkey.') as '.strtoupper($detailtable).'_DETAILCOUNT';
              }
//              else
//                  echo 'De COUNT(...) van de detail tabel is niet opgenomen in de query. Controleer uw plr_subset-records.';
            }

            $sql .= " FROM $fromtable$TABLEPREFIX";
            $sql .= $join;
            $where = '';
            if (strtolower(get_class($this->owner)) == 'base_plrconstruct')
                $where = $this->owner->AddConstructFilter($where, $masterfields != false);
            $where = $this->AddFormFilter($where);
            $where = $this->AddDataScopeFilter($where);
            $where = $this->AddTagFilter($where);
            $where = $this->AddCustomFilter($where, $customfilter);

            $where = $this->ReplaceDynamicVariables($where, $nullvalue='NULL');
            $skippinfilter = false;
            if ( !$masterfields or ($masterfields )) {
                $where = $this->AddGroupFilter($where);
                if (!$skipsearchpart) {
                    $wheresearch = $this->AddSearchFilter($where, $thesearchvalue, $includetableprefix = true, $joinarray, $joinsearch);
                    $skippinfilter = $wheresearch !== $where;
                    $where = $wheresearch;

                    // Alleen toevoegen als deze nog niet in de Join string zit.
                    if ($this->JoinExistsInSQL($joinsearch, $join) === FALSE) {
                        $sql .= $joinsearch; // Deze is nodig voor zoeken in AANVRAAG.POSTCODE en HUISNUMMER
                    }
                    $where = $this->AddPinFilter($where, $fromtable, $skippinfilter);
                }
            }
            $where = $this->AddJoinColumns($where, $joinarray);

            $sql = sqlCombineWhere($sql, $where);
            if ( $countdetails ) {
// WAAROM GROUP BY (want bij Oracle is het veel te traag; MySQL gaat prima)?
// Omdat je geen COUNT kunt hebben zonder de groepen op de overige kolommen
// Dus de COUNTDETAIL maar uitschakelen...
                if ( $this->database->record->DATABASETYPE == 'oci8plr') {
//                    $_allgroupcolumns = $this->GetColumnList($TABLEPREFIX, $useinsearchfilter = false, $masterfields, $joinarray, $forgroupby=true);
//                    $sql .= ' GROUP BY ' . $_allgroupcolumns . ' , ttt.ROWID ';// 20 juni 2009 NHA Oracle => was eerst $keyfields;
                } else {
                    $primkeys = $this->database->metaPrimaryKeys($table, $TABLEPREFIX);
                    $keyfields = implode( ',', $primkeys );
                    $sql .= ' GROUP BY ' . $keyfields;
                }
            }

            if (strtolower(get_class($this->owner)) == 'base_plrconstruct') {
                $constructorderby = $this->owner->GetConstructOrderBy($masterfields != false);
            } else {
                $constructorderby = false;
            }


            if ($constructorderby)
                $_orderBy = $constructorderby;
            else
                $_orderBy = $this->AddOrderBy(true);
            $sql .= $_orderBy;
            $sql = str_replace('%ORDERBY%',$_orderBy,$sql);
          } else {
            // EDIT is true
            $join = $this->MakeJoinClause($tableprefixcount, $joinarray);

            $sql = 'SELECT '.$this->GetColumnList($TABLEPREFIX, $useinsearchfilter = false, $joinarray);
            $sql .= ', '.$this->database->makeRecordIDColumn($table, $TABLEPREFIX, false, $this->record->ISVIEW=='Y') . " AS "._RECORDID;
            $sql .= ' FROM '.$fromtable.$TABLEPREFIX;
            // Make LEFT JOIN with other table for including Lookup-values
            $sql .= $join;
            $firstdetailkey = '';

            if (isset($_GET['recoffset'])) {
                $sql = $this->GetOffsetSQL($filteroffset);
                // $mrs = $this->database->userdb->SelectLimit($_sql, 1, $filteroffset);
            } else {
                if ($recordid) {
                    $where = $this->database->makeEncodedKeySelect($table, $recordid, $tableprefix = $TABLEPREFIX, $this->record->ISVIEW=='Y');
                    $where = $this->AddJoinColumns($where, $joinarray);
                    $sql = sqlCombineWhere($sql, $where);
                } else {
                    $sql = sqlCombineWhere($sql, "1=0");
                }
            }
          }
        }
      }

      /*******/
      if (isset($_GET['showsql']) and $_GET['showsql'] == 'true') {
        echo "<div class=\"message\">";
        echo $sql;
        echo "</div>";
      }
      /*******/

      return $sql;
    }

    function RecordSelector($tableprefix=false) {
        global $TABLEPREFIX;

        if ($tableprefix === false) $tableprefix = $TABLEPREFIX;
        $_recordselector = $this->database->makeRecordIDColumn($this->GetDataDestination(), $tableprefix, false, $this->record->ISVIEW=='Y');
        return $_recordselector;
    }

    function JoinExistsInSQL($needle, $haystack) {
        $_result = FALSE;
        if ($needle != '') {
            $needle = str_replace(' ', '', $needle);
            $haystack = str_replace(' ', '', $haystack);
            foreach(explode(',',$needle) as $jstring) {
                if (trim($jstring) != '') {
                    if (strpos($haystack, $jstring) !== FALSE) {
                        $_result = TRUE;
                    } else {
                        $_result = FALSE;
                        break;
                    }
                }
            }
        }
        return $_result;
    }

    function MakeDetailSQL($edit = false, $masterrecord, $countselect = false, $mastertable=false, $customfilter=false) {
        $this->masterfields = $this->GetMasterFields($mastertable);
        $this->detailfields = $this->GetDetailFields($mastertable);
        $sql = $this->MakeSQL($edit, $countselect, $thesearchvalue=false, $this->masterfields, $countdetails=false, $detailtable=false, $customfilter);
        // Only add the Where clause when we want all the detailrecords.
        // If a detailrecord is editted we can skip the Where clause, because
        // it's added by the MakeSQL function
        if (!$edit) {
            $orderbyindex = 0;
            $sql = str_replace("\r", " ", $sql);
            $sql = str_replace("\n", " ", $sql);
            $tmpsql = strtoupper($sql);
            $lastfrom = strripos($tmpsql, ' FROM ');
            if ($lastfrom > 0) {
                $orderbyindex = strpos($tmpsql, 'ORDER BY', $lastfrom); //, $lastfrom
            }
            $where = $this->MakeDetailWhereClause($masterrecord, $this->masterfields, $this->detailfields);
            $sql = sqlCombineWhere($sql, $where);
            if ($orderbyindex == 0)
                $sql .= $this->AddOrderBy(true);
        }
        /*******/
        if ($_GET['showdetailsql'] == 'true')
            pr($sql, $where);
        /*******/
        return $sql;
    }

    function DataSourceIsSQL() {
      return strtoupper(substr($this->record->TABLENAME, 0, 5)) == 'SQL::';
    }

    function GetDataSource() {
      if ( $this->DataSourceIsSQL() ) {
        $source = str_replace('SQL::','',$this->record->TABLENAME);
      } else {
        $source = $this->record->TABLENAME;
    }
      return $source;
    }

    function GetDataDestination($onlytablename=false) {
        if ( trim($this->record->UPDATETABLENAME) != '' )
            $dest = $this->record->UPDATETABLENAME;
        else
            $dest = $this->record->TABLENAME;
        if ($this->database->record->USEDBASPREFIX == 'Y' and !$onlytablename and $dest != '') {
            $dest = $this->database->record->DATABASENAME .'.'. $dest;
        }
        return $dest;
    }

    function ShowRecordCount() {
        global $_GVARS;
        global $polaris;

        if ($polaris->currentdetailform and isset($polaris->currentdetailform->rsdata) and $polaris->currentdetailform->rsdata !== FALSE) {
            if (!is_array($polaris->currentdetailform->rsdata)) {
                $this->recordcount = $polaris->currentdetailform->rsdata->RecordCount();
            } else {
                $this->recordcount = count($polaris->currentdetailform->rsdata);
            }
        } elseif (isset($this->rsdata) and $this->rsdata !== FALSE) {
            if ($this->recordcount == -1) {
                $this->recordcount = $this->rsdata->RecordCount();
            }
        }

        $this->recordcount = intval($this->recordcount);
        $showqueryrecordcount = ($this->HasOption('skip-recordcount') or !$this->ShowInitialDataset($state)) and ($this->recordcount == -1);

        // only show the navigator when there are more items available than can be shown
        if ($this->limit) {
            $_delta = $this->recordcount < $this->limit ? $this->recordcount : $this->limit;
            $endno = min($this->recordcount, (($this->startno - 1) + $_delta));
        } else {
            $endno = $this->recordcount;
        }
        if ($showqueryrecordcount and $this->recordcount >= $this->limit) {
            $_tmprecordcount = '<span id="recordcount_unknown">'.$this->i18n_trans('unknown').' <i id="countrecords" class="fa fa-info-circle fa-lg" title="'.$this->i18n_trans('show_record_number').'..." alt="?"></i></span>';
        } else {
            $_tmprecordcount = $this->recordcount;
        }

        if ($this->recordcount == -1)
            $startno = $this->startno;
        elseif ($this->recordcount > 0)
            $startno = $this->startno;
        else
            $startno = '0';

        $_result = '<span class="recordcount">'.ucwords(lang_get('items')).' '.$startno.' '.lang_get('until').' '.$endno;

        if ($endno !== $_tmprecordcount)
            $_result .= ' '.lang_get('of').' '.$_tmprecordcount;

        if ( $this->datafiltered ) {
          $_result .= ' (<abbr title="'.lang_get('filtered_hint_pin').'">'.lang_get('filtered_pin').'</abbr>) ';
        }
        if ( (isset($_GET['q']) and $_GET['q'] !== '' and !$this->detail) ) {
          $_result .= ' (<abbr title="'.lang_get('filtered_hint', $this->ItemsName()).'">'.lang_get('filtered').'</abbr>) ';
        }
        $_result .= '</span>';

        return $_result;
    }

    function ShowHelpPanel() {

        if ($this->record->PANELHELPTEXT != '' or $this->owner->record->PANELHELPTEXT != '') {
            //        if (($GLOBALS['action'] == '') or ($GLOBALS['action'] != 'search' and $GLOBALS['detailonly'])) {
            if ($this->record->PANELHELPTEXT != '')
                $panelhelp = $this->record->PANELHELPTEXT;
            else
                $panelhelp = $this->owner->record->PANELHELPTEXT;

            $panelhelp = $this->ReplaceDynamicVariables($panelhelp);
            $_toggle = strpos($panelhelp, "<--->\n") !== false;
            $panelhelp = Markdown::defaultTransform($panelhelp);

            if ($_toggle) {
                $_parts = explode("<--->", $panelhelp);
                echo '<div id="panelhelp1" class="ibox alert-info collapsed">
                        <div class="ibox-title">
                            <p>'.$_parts[0].'</p>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="material-symbols-outlined md-1x">keyboard_arrow_down</i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content" style="display: none;">
                            '.$_parts[1].'
                        </div>
                    </div>';

            } else {
                echo '<div id="panelhelp1" class="ibox alert-info">
                        <div class="ibox-title"><p>'.$panelhelp.'</p></div>
                    </div>';
            }
        }
    }

    function IsGeneratedForm() {
        $_formview = $this->GetFormView();
        return (($this->record->MODULE == '') and ($_formview != 'RSSVIEW') and ($_formview != 'CUSTOMVIEW'));
    }

    function ShowFormHeader($state, $permissiontype=SELECT) {
        global $polaris;

        echo '<div id="constructtitle">';

        $generatedForm = $this->IsGeneratedForm();

        // Should we display the previous save value?
        if ($this->HasOption('show_saveresult')) {
            /**
            * Display the form title and current action label
            */
            $this->ShowSaveResult($state, $permissiontype);
        }

        echo "</div>";
    }

    function ShowFormTitle($state, $permissiontype=SELECT) {
        /**
        * Form title with current action (insert, edit, view, ...)
        */
        echo '<span class="constructname">';
        if ($state == '' or $state == 'detail' or $state == 'search' or $state == 'allitems' or $state == 'detail_allitems') {
            $formname = $this->ReplaceDynamicVariables($this->record->FORMNAME);
            if ($state == 'search')
                echo get_resource($formname).' '.strtolower(lang_get('search'));
            else
                echo get_resource($formname);
        } else {
            if ($state == 'edit' or $state == 'view' or $state == 'detail_edit' or $state == 'detail_view') {
                if ((($permissiontype & UPDATE) == UPDATE) and ($state == 'edit' or $state == 'detail_edit'))
                    echo lang_get('edit_item', ucwords($this->ItemName()) );
                else
                    echo lang_get('view_item', ucwords($this->ItemName()) );
            } elseif ($state == 'impexp') {
                echo lang_get('import_export_items', ucwords($this->ItemsName()) );
            } else {
                if ( (!$this->detail and isset($_GET['rec'])) or ($this->detail and isset($_GET['detailrec'])) )
                    echo lang_get('clone_an_item', ucwords($this->ItemName()) );
                else
                    echo lang_get('add_new_item', ucwords($this->ItemName()) );
            }
        }
        echo '</span>';
    }

    function ShowSaveResult($state, $permissiontype) {
        if (is_array($_SESSION['prevsaveresult'])) {
            echo "<span class=\"saveresult\">";
            $k = array_keys($_SESSION['prevsaveresult']);
            $v = array_values($_SESSION['prevsaveresult']);
            echo "(laatst toegevoegd {$k[0]}: <b>{$v[0]}</b>)";
            echo "</span>";
        }
    }

    function ShowNoRecordsInfo($permissiontype) {
        echo "<div class='row view-detail-box'><div class='col-xs-12 col-sm-6 col-sm-offset-3' xstyle='text-align:center'>";
        if ( isset($_GET['q']) or $this->datafiltered ) {
            if ($this->datafiltered) {
                echo "<p>".lang_get('no_records_found_pin')."</p>";
                echo "<p>".lang_get('alter_pin')."</p>";
            } else {
                // In case of an empty search result
                echo "<p>".lang_get('no_records_found')."</p>";
                echo "<p>".lang_get('alter_search', $this->ItemsName())."</p>";
            }
        } else {
            // In case of an empty table
            echo "<div class='row view-detail-box'><div class='col-xs-12 col-sm-6 col-sm-offset-3' style='text-align:center'>";
            echo "<img src='/assets/images/no_items_boy.png' style='margin-top:30px;width:150px' />";
            echo "<h1>".lang_get('list_is_empty', $this->ItemsName() )."</h1>";
            if (($permissiontype & INSERT) == INSERT) {
                $insertquery = $this->MakeURL(false, $this->detail).'insert/'.$this->MakeQuery();
                echo '<h2>'.lang_get('you_can_add_to_list', "<a href=\"$insertquery\">".$this->ItemName().'</a>' ).'</h2>';
            }
            echo "</div></div>";
        }
        echo "</div></div>";
    }

    function ShowMaster($state, $outputformat='xhtml') {
        /**
        * Show the master formview/listview
        */
        $this->Show($state, $detail=false, $outputformat);
    }

    function GetOffsetSQL(&$filteroffset) {
        $_offset = isset($_GET['offset'])?$_GET['offset']:0;
        $_offset = intval($_offset);
        $_recoffset = intval($_GET['recoffset']);
        $_filteroffset = $_offset + $_recoffset;
        $_sql = $this->MakeSQL($edit=false, $countselect = false, $thesearchvalue = false, $masterfields = false, $countdetails=false, $detailtable=false, $customfilter=false);
        return $_sql;
    }

    function GetMasterRecord($masterrecordid=false) {

        if (isset($_GET['recoffset'])) {
            $_sql = $this->GetOffsetSQL($filteroffset);
            $mrs = $this->database->userdb->SelectLimit($_sql, 1, $filteroffset);
        } else {
            if (!$masterrecordid and isset($_GET['rec']))
                $masterrecordid = $_GET['rec'];

            if (isset($this->masterrecord) and $this->masterrecordid == $masterrecordid) return $this->masterrecord;

            if ($this->owner->masterform and $masterrecordid) {
                $mastertablename = $this->owner->masterform->GetDataSource();
                $masterdestination = $this->owner->masterform->GetDataDestination();
                if ($mastertablename) {
                    $this->masterrecordid = $masterrecordid;
                    $this->database->connectUserDB();
                    $recordID = $this->database->makeRecordIDColumn($masterdestination, 'msql');
                    $mastersql = "SELECT msql.*, $recordID AS PLR__RECORDID FROM $masterdestination msql";
                    $masterrecordid = $this->database->customUrlDecode($masterrecordid);
                    $mastersql .= ' WHERE '. $this->database->makeEncodedKeySelect($masterdestination, $masterrecordid);
                    $mrs = $this->database->userdb->Execute($mastersql) or die('ShowDetail mastersql failed: '.$this->database->userdb->ErrorMsg());
                } else {
                    return false;
                }
            }
        }

        if ($mrs) {
            $this->masterrecord = $mrs->FetchNextObject(true);
            return $this->masterrecord;
        } else {
            return false;
        }
    }

    function ShowAjaxDetail($state, $outputformat='xhtml') {
        echo "<div id=\"datadetailform\"><h3>".$this->i18n($this->record->FORMNAME)."</h3>";
        echo "<div id=\"detailajax\"></div>";
        echo "</div>";
    }

    function ShowDetail($state, $outputformat='xhtml', $masterrecordid = false) {
        /**
        * Get the master-record and store it, when the user clicks a 'view details of masterrecord' link
        */
        if ($GLOBALS['detailonly']) {
            $masterrecord = $this->GetMasterRecord();
            $_SESSION['currentmasterrecord'] = serialize($masterrecord);
//            $GLOBALS['masterrecord'] = $masterrecord;
        }

        /**
        * Show the detail formview/listview
        */
        $this->Show($state, $detail=$this->record->METANAME, $outputformat);
    }

    function ShowDetails($state, $currentform=false, $outputformat='xhtml') {
        global $_sqlLinkedForm;
        global $_GVARS;

        $masterrecord = $this->GetMasterRecord();
        $this->ShowMasterRecordLink($masterrecord, $this->owner->masteraction == 'detail', $skipfrom=false);

        $rs = $this->dbExecute($_sqlLinkedForm, array($this->record->CLIENTID, $this->record->DATABASEID, $this->GetDataDestination($onlytablename=true))) or Die('query failed join2: '.$_sqlLinkedForm.$this->dbErrorMsg());
        $rs->MoveFirst();
        $rec = $rs->FetchObject(true);

        if ($currentform == false) {
            $rec = $rs->FetchObject(true);
            $_currentform = $rec->METANAME;
        } else {
            $_currentform = $currentform;
        }

        if ($rs->RecordCount() > 0)
            echo "<br /><div class=\"pagetabcontainer\"><ul id=\"pagetabs\">";

        $_basehref = $this->MakeURL().$this->owner->masteraction."/".$_GET['rec']."/";
        while (!$rs->EOF) {
            $rec = $rs->FetchObject(true);
            $_link = $_basehref.$rec->METANAME.'/'.$this->MakeQuery();
            if ($_currentform == $rec->METANAME) {
                $_linknewwindow =  str_replace('/edit/','/detail/',$_link);
                $popbox = $this->owner->masteraction == 'detail'?'':'popbox';
                $spantag = "<span class=\"togglemaster\" link=\"$_linknewwindow\">&nbsp;</span>";
                $spantag = "<i class=\"fa fa-expand togglemaster\" link=\"$_linknewwindow\"></i>";
                $selected = 'selected';
            } else {
                $spantag = '';
                $selected = '';
            }
            echo "<li><a class='$selected' href=\"{$_link}\"><span>".$this->i18n($rec->FORMNAME)."</span> $spantag</a></li>";
            $rs->MoveNext();
        }
        if ($rs->RecordCount() > 0)
            echo "</ul></div>";

    }

    function GetLimit() {
        global $_CONFIG;

        if (isset($_SESSION['record_limit'][$this->record->METANAME]))
            $limit = $_SESSION['record_limit'][$this->record->METANAME];
        elseif (!empty($_SESSION['clientparameters']['DRLIM']))
            $limit = $_SESSION['clientparameters']['DRLIM'];
        elseif (isset($this->record->RECORDLIMIT))
            $limit = $this->record->RECORDLIMIT;
        else
            $limit = $_CONFIG['default_record_limit'];

        if ($limit == -1) $limit = false;
        return $limit;
    }

    function GetFormView() {
        // Check if 'formview' is set in the GET parameters and return immediately if true
        if (isset($_GET['formview'])) {
            return $_GET['formview'];
        }

        // If not in detail view, determine form view based on DEFAULTVIEWTYPE or MASTERFORMTYPE
        if (!$this->detail) {
            return !empty($this->record->DEFAULTVIEWTYPE) ? $this->record->DEFAULTVIEWTYPE : $this->owner->record->MASTERFORMTYPE;
        }

        // In detail view, determine form view based on DEFAULTVIEWTYPE or DETAILFORMTYPE
        return !empty($this->record->DEFAULTVIEWTYPE) ? $this->record->DEFAULTVIEWTYPE : $this->owner->record->DETAILFORMTYPE;
    }

    function ShowInitialDataset($state) {
        $result = true;

        if (!isset($this->detail)
        and (isset($this->formtags) and !$this->formtags->hasActiveTags())
        // and $this->GetViewTypeParameter('searchfirst') == 'true'
        and $state != 'search'
        and !$this->FormHasActivePins()
        ) {
            $result = false;
        }

        return $result;
    }

    function GetPermissionType() {
        $permissiontype = $this->record->PERMISSIONTYPE;
        if (isset($this->owner->record->OVERRULEPERMISSIONTYPE)) {
            $permissiontype = $this->owner->record->OVERRULEPERMISSIONTYPE;
        }
        return $permissiontype;
    }

    function Show($state, $detail=false, $outputformat='xhtml') {
        global $_GVARS;
        $permissiontype = $this->GetPermissionType();
        if ($permissiontype == 0) {
            echo lang_get('insufficient_rights_cannot_update');
            return;
        }

        $this->detail = $detail;
        $this->basehref = $this->MakeURLQuery( array('q','qc','qtable','sort','dir','offset',/*module.webmenu*/'group'), $this->detail );
        $this->databasehash = encodeString($this->record->CLIENTID.'_'.$this->record->DATABASEID);
        $this->outputformat = $outputformat;
        if ($this->outputformat == 'xhtml') {
            if (!$this->HasOption('no-form-actions')) {
                $this->polaris()->tpl->display("actions.tpl.php");
            }
        }

        if ($this->pages) {
            $this->database->connectUserDB();

            $exportlimit = 30000;
            $limit=($_GET['limit'])?$_GET['limit']:$exportlimit;

            $masterrecord = $detail ? $this->GetMasterRecord() : false;
            if ($this->outputformat != 'xhtml') {
                /**
                * When the data is being exported (and not shown via a form)
                */
                if ($this->record->MODULE) { //  and !$this->servicecall
                    /**
                    * Show the Module export data
                    */
                    $this->ShowModule($permissiontype, $state, $this->record->MODULE, $this->record->MODULEID, $this->record->VIEWTYPEPARAMS, $_GET['rec'], $this->GetMasterRecord());
                } else {
                    /**
                    * Show the generated export data
                    */
                    echo $this->GetFormattedViewContent($this->outputformat, $detail, $this->GetMasterRecord(), $limit);
                }
            } else {
                if (!$this->clientWithinLimitForTableResources($state, false)) {
                    echo '<div class="m-l-sm m-r-sm alert alert-danger"><b>Er zijn '.$this->resourcesInUse().' '.$this->GetItemName(1).' beschikbaar, maar u kunt er nu maximaal '.$this->clientTableResourceLimit().' bekijken.</b> <br>Wilt u alle records zien en volledige toegang krijgen? Ontgrendel deze door een upgrade of neem contact op met uw Maxia accountbeheerder.</div>';
                }

                $_formview = $this->GetFormView();

                if ($this->ShowInitialDataset($state)
                and (in_array($_formview, ['LISTVIEW', 'SQLVIEW', 'WIZARDVIEW', 'TASKVIEW', 'DETAILSVIEW'])
                and (!in_array($state, ['insert', 'clone', 'edit'])))) {
                    if ($this->record->MODULE) {
                        $module =& $this->GetModule($permissiontype, $state, $this->record->MODULE, $this->record->MODULEID, $this->record->MODULEPARAM);
                        if ($module) {
                            $customfilter = $module->__GetCustomFilter();
                        }
                    } else {
                        $this->LoadViewContent($detail, $masterrecord, $limit=false);
                    }
                }

                if ($state != 'insert') {
                    $this->GetRecordCount($permissiontype, $state);
                }

                $this->ShowFormSettings($state);
                if ($_formview == 'RSSVIEW') {
                    /**
                    * Show RSS feed via URL
                    */
                    $this->ShowRSS($this->record->WEBPAGEURL, $this->record->VIEWTYPEPARAMS );

                } elseif ($_formview == 'CUSTOMVIEW') {
                    /**
                    * Show custom external form via URL
                    */
                    // Replace dynamic variables in case there are vars in the SQL
                    $url = $this->ReplaceDynamicVariables($this->record->WEBPAGEURL);
                    $url = ProcessSQL($this->database->userdb, $url);
                    // Again in case there is a dynamic variable in the value
                    $url = $this->ReplaceDynamicVariables($url);

                    $this->ShowCustomForm($url);
                } elseif (!empty($this->record->MODULE)) {
                    /**
                    * Show the Module (programmed form)
                    */
                    $this->ShowPostForms($_formview, $state);
                    $this->ShowModule($permissiontype, $state, $this->record->MODULE, $this->record->MODULEID, $this->record->VIEWTYPEPARAMS, $_GET['rec'], $this->GetMasterRecord());
                } else {

                    $this->ShowPostForms($_formview, $state);
                    if ($state == 'impexp') {
                        /**
                        * Import Export data
                        */
                        $this->polaris()->tpl->display('import_export.tpl.php');

                    } elseif ($_formview == 'TASKVIEW') {
                        /**
                        * FormView
                        */
                        if ($state == 'allitems') {
                            $this->ShowTaskView($state, $permissiontype);
                        } else {
                            $this->ShowFormView($state, $permissiontype, $this->detail, $this->GetMasterRecord());
                        }
                    } elseif ($_formview == 'FORMVIEW') {
                        /**
                        * FormView
                        */
                        $this->ShowFormView($state, $permissiontype, $this->detail, $this->GetMasterRecord());

                    } elseif ($_formview == 'LIVEVIEW') {

                        $this->ShowLiveView($permissiontype, $this->detail);

                    } elseif ($_formview == 'LISTVIEW' or $_formview == 'WIZARDVIEW') {

                        if (!empty($_GET['detailform']) and $state == 'detail_allitems') {
                            $this->ShowListView($state, $permissiontype);
                        } else {
                            if (( in_array($state, ['allitems', 'detail_allitems', 'search', '']))) {
                                /**
                                * ListView
                                */
                                if (!$this->ShowInitialDataset($state)) {
                                    echo "<br /><p style=\"font-size:1.5em;font-weight:100;\" class=\"alert alert-info\"><span style=\"font-size:1.2em;\">&larr;</span> &nbsp; ".lang_get('search_first_text')."</p>";
                                } else {
                                    if ($this->recordcount != 0) {
                                        $this->ShowListView($state, $permissiontype);
                                    }
                                }
                            } elseif ($state != 'allitems') {
                                /**
                                * FormView/WizardView
                                */
                                $this->ShowFormView($state, $permissiontype, $this->detail, $this->GetMasterRecord());
                            }
                        }
                    } elseif ($_formview == 'DETAILSVIEW') {
                        /**
                        * DetailsView
                        */
                        $this->ShowDetailsView($state, $permissiontype, true, $this->GetMasterRecord());
                    } elseif ($_formview == 'SQLVIEW') {
                        /**
                        * SQLView
                        */
                        $this->ShowSQLView($state, $permissiontype, $this->detail, $this->GetMasterRecord());
                    }
                    if ($this->recordcount == 0 and ($state == 'allitems' or $state == 'search')) {
                        /**
                        * In case there are no records in the dataset, show some practical info
                        */
                        $this->ShowNoRecordsInfo($permissiontype);
                    }
                }

                echo '</div>'; // masterform/detailform div
             }
          }
    }

    function ShowPostForms($formview, $state) {
        global $polaris;

        if ($this->moduleobj) {
            $state = $this->moduleobj->GetCurrentState() ? : $state;
        }
        $isBasicForm = !empty($_GET['basicform']);
        $tablename = $this->GetDataDestination();
        $detailState = $this->owner->detailaction;

        if (in_array($formview, ['LISTVIEW','TASKVIEW','WIZARDVIEW'])
            and !$isBasicForm
            and !($detailState == 'detail_insert')
        ) {
            if ($this->detail) {
                // $formnameprefix = $tablename.'_';
                // Geen prefix voor detailformulieren, omdat we toch geen situatie meer hebben
                // dat we master en detailformulieren tegelijkertijd tonen
            }
            echo '<form name="'.$formnameprefix.'frmextra" id="'.$formnameprefix.'frmextra" method="post" action="'.$this->basehref.'">';
            echo '<input type="hidden" name="_hdnAction" value="plr_togglevisibility" />';
            echo '<input type="hidden" name="_hdnPublishField" value="" />';
            echo '<input type="hidden" name="_hdnVisibleField" value="" />';
            echo '<input type="hidden" name="_hdnVisibleRecord" value="" />';
            echo '<input type="hidden" name="_hdnVisibleValue" value="" />';
            echo '<input type="hidden" name="_hdnDatabase" value="'.$this->databasehash.'" />';
            echo '<input type="hidden" name="_hdnTable" value="'.$tablename.'" />';
            echo '</form>';

            echo '<form name="'.$formnameprefix.'frmhotupdate" id="'.$formnameprefix.'frmhotupdate" method="post" action="'.$this->basehref.'">';
            echo '<input type="hidden" name="_hdnAction" value="plr_hotupdate" />';
            echo '<input type="hidden" name="_hdnHotUpdateFields" value="" />';
            echo '<input type="hidden" name="_hdnHotUpdateRecord" value="" />';
            echo '<input type="hidden" name="_hdnHotUpdateValues" value="" />';
            echo '<input type="hidden" name="_hdnDatabase" value="'.$this->databasehash.'" />';
            echo '<input type="hidden" name="_hdnTable" value="'.$tablename.'" />';
            echo '</form>';

            echo '<form name="'.$formnameprefix.'frmswap" id="'.$formnameprefix.'frmswap" method="post" action="'.$this->basehref.'">';
            echo '<input type="hidden" name="_hdnAction" value="plr_swaprecords" />';
            echo '<input type="hidden" name="_hdnSwapRecord1" value="" />';
            echo '<input type="hidden" name="_hdnSwapRecord2" value="" />';
            echo '<input type="hidden" name="_hdnSwapColumn" value="" />';
            echo '<input type="hidden" name="_hdnDatabase" value="'.$this->databasehash.'" />';
            echo '<input type="hidden" name="_hdnTable" value="'.$tablename.'" />';
            echo '</form>';
        }

        if (($state == 'edit') or ($state == 'insert') or ($state == 'clone')
        or ($state == 'detail_edit') or ($state == 'detail_insert')) {
            $formstyle = 'formview';
            $validateform = 'validate';
        } else {
            $formstyle = 'listview';
            $validateform = '';
        }

        if (!($formview == 'DETAILSVIEW' and !$this->detail and $state !== 'insert')
            or ($formview == 'DETAILSVIEW' and $state == 'edit')
        ) {
            $customsaverecordhref = $this->basehref;
            echo '<form autocomplete="off" name="'.$formnameprefix.'dataform" id="'.$formnameprefix.'dataform" method="post" action="'.$customsaverecordhref.'" class="form-horizontalx dataform '.$sortableTable.' '.$validateform.' '.$formstyle.'" enctype="multipart/form-data">';

            if (($state == 'edit') or ($state == 'view') or ($state == 'insert') or ($state == 'clone')
            or ($state == 'detail_edit') or ($state == 'detail_view') or ($state == 'detail_insert') or ($state == 'detail_clone')) {
                $action = 'plr_save';
                $state = str_replace('detail_','',$state);
            } elseif ($state == 'searchapp') {
                $action = 'plr_searchapp';
            } elseif ($state == 'impexp') {
                $action = 'plr_export';
            } else {
                $action = '_nothing_';
            }

            echo '<input type="hidden" name="_hdnAction" value="'.$action.'" />';
            echo '<input type="hidden" name="_hdnKeyPressed" value="" />';
            echo '<input type="hidden" name="_hdnDatabase" value="'.$this->databasehash.'" />';
            echo '<input type="hidden" name="_hdnTable" value="'.$tablename.'" />';
            echo '<input type="hidden" name="_hdnState" value="'.$state.'" />';
            echo '<input type="hidden" name="_hdnOffset" value="'.$this->startno.'" />';
            echo '<input type="hidden" name="_hdnConstruct" value="'.$this->owner->record->METANAME.'" />';
            echo '<input type="hidden" name="_hdnForm" value="'.$this->record->RECORDID.'" />';
            echo '<input type="hidden" name="_hdnPlrSes" value="'.$polaris->polarissession.'" />';
            echo '<input type="hidden" name="_hdnFormName" value="'.$this->record->METANAME.'" />';
            echo '<input type="hidden" name="_hdnRecordLimit" value="_nothing_" />';

            if ($_GET['select'] == 'true')
                echo '<input type="hidden" name="_hdnSelectField" value="'.str_replace('_fld','',$_GET['selectshowfield']).'" />';
            if ($this->HasOption('pin-on-insert')) {
                if ($state == 'insert')
                    echo '<input type="hidden" name="_hdnPinAfterInsert" value="true" />';
            }
            if ($this->HasOption('insert-and-new')) {
                echo '<input type="hidden" name="_hdnNewAction" value="true" />';
            }
            if ($this->HasOption('insert-and-open')) {
                echo '<input type="hidden" name="_hdnKeepOpen" value="true" />';
            }
            if ($this->HasOption('save-and-open')) {
                echo '<input type="hidden" name="_hdnAlwaysKeepOpen" value="true" />';
            }
        }

        /*
        if ($this->record->HISTORYTYPE != '') {
            if ($state == 'insert' and $this->record->HISTORYTYPE == 'AFTERINSERT')
                echo '<input type="hidden" name="_hdnKeepInHistory" value="true" />';
        }
        */
    }

    function ShowPinnedRecords() {
        global $polaris;

        $deletelalllink = "<span class=\"deleteallpins\"><label title=\"".lang_get('delete_pinned_items')."\" for=\"ALL:ALL\">&nbsp;<a href=\"javascript:void(0)\"><i class=\"icon icon-757\" style=\"font-size:1.2rem;\"></i></a></label></span>";
        echo "<h4 class=\"search pinneditems\">".lang_get('pinned_items_title')." $deletelalllink</h4>";
        echo "<ul id=\"pinnedrecords\" class=\"pinnedrecords\"></ul>";
    }

    function ShowHorizontalPinnedRecords() {
        global $polaris;

        echo "<div id=\"pinnedrecords\" class=\"pinnedtags\"></div>";
    }

    function ShowSearchPanel($state, $permissiontype, $detailform, $masterrecord) {
        global $_GVARS;
        global $polaris;

        $defaultoff = $this->GetViewTypeParameter('searchpanel');
        if ($defaultoff) $defaultoffclass = ' switchedoff';
        $docked = ($_COOKIE['searchpanel'] != 'off' and !$defaultoff)?'docked':'';

        echo "<div id=\"searchpnl\" class=\"{$defaultoffclass} {$docked}\">";

        echo '<div class="xibox-tools search-tools">
            <a class="advanced-link" data-target=".search-options" data-toggle="tooltip" data-placement="top" title="'.lang_get('search_advanced_options').'">
                <i class="fa fa-cog"></i>
            </a>
            <a class="switch-link" data-target="#searchpanel" data-toggle="tooltip" data-placement="top" title="'.lang_get('toggle_search_panel').'">
                <i class="fa fa-chevron-right"></i>
            </a>
        </div>';
        echo "<div id=\"searchpanel\">";
        echo "<div id=\"searchpanelinner\">";

        echo "<h4 class=\"search\">".lang_get('searchform_title')."</h4>";
        $_basehref = $this->MakeURLQuery( array('q','qc','qtable','sort','dir','offset',/*module.webmenu*/'group') );

        echo '<div class="row section search-options" style="display:none">';

        echo '<label class="col-xs-12"> <input type="checkbox" id="_fldCASESENSITIVE" disabled="disabled" checked="checked" /> '.lang_get('case_sensitive').'</label>';
        echo '<label class="col-xs-12"> <input type="checkbox" id="_fldSEARCHFULLWORD" name="_hdnSEARCHFULLWORD" /> '.lang_get('search_fullword').'</label>';
        echo '<label class="col-xs-12"> <input type="checkbox" id="_fldSEARCHFROMSTART" name="_hdnSEARCHFROMSTART" /> '.lang_get('search_fromstart').'</label>';

        echo "</div>";

        echo "<form autocomplete=\"off\" name=\"search_dataform\" id=\"search_dataform\" method=\"get\" action=\"$_basehref\">";
        echo "<input type=\"hidden\" name=\"_hdnPlrSes\" value=\"{$polaris->polarissession}\" />";
//        echo "<input type=\"hidden\" name=\"_hdnSkipPins\" value=\"{$polaris->polarissession}\" />";
        $fieldcount = 0;
        if ($detailform) {
            $fieldcount = $this->owner->masterform->ShowSearchForm($state, $permissiontype, $detailform=false, $masterrecord);
        } else {
            $fieldcount = $this->ShowSearchForm($state, $permissiontype, $detailform, $masterrecord);
        }

        if ($fieldcount > 0) {
            $_q = !is_array($_GET['q']) ? $_GET['q'] : '';
            echo "<p class=\"search\">".lang_get('searchall_title')."</p>";
            echo '<div class="">
            <div class="col-sm-12 search-main-control">
            <input type="search" placeholder="" incremental="incremental" class="form-control" autosave="__plrsearch" xresults="5" id="searchquery" name="q" value="'.$_q.'">
            </div>
            </div>';
//            echo "<button id=\"_fldNEWSEARCH\" tabindex=\"-1\" title=\"".lang_get('new_search_hint')."\" type=\"button\" class=\"btn btn-primary btn-xs\" style=\"float:right;\">".lang_get('new_search')."</button>";
            echo "<div class=\"m-b-sm\">
            <button id=\"_fldSEARCHNOW\" class=\"search-main btn btn-primary btn-smx\">".lang_get('search_now')."</button>
            <button id=\"_fldNEWSEARCH\" class=\"search-reset btn btn-smx\" title=\"".lang_get('new_search_hint')."\"><i class=\"fa fa-undo\"></i></button>
            </div>";
        } else {
            echo "<p>&ndash; ".lang_get('no_search_available')." &ndash;</p>";
        }
        echo "</form>";

        if (!$this->AppObject()->HasSearchOption('no-pinning')) {
            echo "<hr class=\"fix\" /><br />";
            $this->ShowPinnedRecords();
        }

        echo "</div></div></div>";
    }

    function ProcessSearchValue($value) {
        $result = !is_array($value) ? $value : '';
        $result = htmlspecialchars(str_replace(array("'","%"), "", $result));
        return $result;
    }

    function ShowHorizontalSearchPanel($state, $permissiontype, $detailform, $masterrecord) {
        global $_GVARS;
        global $polaris;

        if ($this->GetViewTypeParameter('searchpanel') == 'false') return;
        $_basehref = $this->MakeURLQuery( array('q','qc','qtable','sort','dir','offset',/*module.webmenu*/'group'));

        $_q = $this->ProcessSearchValue($_GET['q']);

        $searcharray = $_GET['q'];
        if (is_array($searcharray)) $searcharray = array_unique($searcharray);

        echo '<div id="searchpanel_horizontal" class="gray-bg bigsearch white-bg">';
        echo '<form autocomplete="off" name="search_dataform" id="search_dataform" method="get" action="'.$_basehref.'">';
        echo '<div class="searchbox border-bottom">';
        echo '<div class="globalsearch">';
        $this->ShowHorizontalPinnedRecords();
        echo $this->DisplayResultsBox($searcharray);
        echo '<input id="_hdnGlobalSearch" type="search" name="q[]" value="'.$_q.'" accesskey="CTRL+f"  placeholder="'.lang_get('bigsearch_placeholder').'" />';
        echo $this->DisplaySearchActions($searcharray);
        echo '</div>';
        echo '</div>';
        echo '</form>';
        echo '</div>';
    }

    function DisplayResultsBox($searcharray) {
        $_result = '';
        if (is_array($searcharray) and count($searcharray) > 0) {
            $_result .= '<div id="search_resultbox" class="resultsbox">';
            foreach($searcharray as $_item) {
                foreach(explode('+', $_item) as $_subitem) {
                    if ($_subitem !== '') {
                        // sanitize the search value
                        $_subitem = strip_tags($_subitem);

                        $_subitem = str_replace(array("'","%"), "", $_subitem);
                        $_value = str_replace(' ', '+', $_subitem);
                        $_linkDelete = $this->MakeURLQuery( array('q','qc','qtable','sort','dir','offset') );
                        $_linkDelete = str_replace('q[]=%27'.$_value.'%27', '', $_linkDelete);
                        $_linkDelete = str_replace('q[]=%25'.$_value.'%25', '', $_linkDelete);
                        $_linkDelete = str_replace('q[]='.$_value, '', $_linkDelete);
                        $_result .= "<div class=\"searchtag btn-group\" role=\"group\">
                            <button type=\"button\" class=\"reclink btn btn-default btn-outline \" id=\"{$_plrrecordid}\"><input type=\"hidden\" name=\"q[]\" value=\"{$_value}\" >{$_subitem}</button>
                            <a type=\"button\" class=\"btn btn-default btn-outline   kill-filter\" href=\"{$_linkDelete}\"><i class=\"icon icon-883\" style=\"font-size:0.9rem;\"></i></a>
                        </div>
                        ";
                    }
                }
            }
            $_result .= '</div>';
        }
        return $_result;
    }

    function DisplaySearchActions($searcharray) {
        $_result = '';

        $_searchHistory = $this->GetSearchHistory();
        $_result .= '
        <div class="search-drop-down">
            <button data-toggle="dropdown" type="button" class="btn btn-clean dropdown-toggle" aria-expanded="false"><i class="material-symbols-outlined md-2x">more_horiz</i></button>
            <div class="dropdown-menu pull-right">';
        foreach($_searchHistory as $_searchResult) {
            $_searchQuery = $_searchResult['SEARCHQUERY'];
            $_searchQuery = http_build_query(json_decode($_searchQuery));
            $_link = $this->basehref.'?'.$_searchQuery;
            $_result .= "<li><a href=\"{$_link}\"><i class='material-symbols-outlined md-1x'>bookmark</i> {$_searchResult['SEARCHNAME']}</a><li>";
        }
        $_queryActionDisabled = empty($searcharray) ? 'disabled' : '';
        $_result .= '<li class="divider"></li>';
        $_result .= '<li class="'.$_queryActionDisabled.'"><a id="saveSearchAction" data-toggle="modal" data-target="#modSaveSearch" class="dropdown-item"><i class="material-symbols-outlined md-1x">bookmark_add</i> Zoekresultaat opslaan</a></li>';
        $_linkDeleteAll = $this->MakeURLQuery( array('sort','dir','offset') );
        $_result .= '<li class="'.$_queryActionDisabled.'"><a href="'.$_linkDeleteAll.'" class="dropdown-item kill-all-filters"><i class="material-symbols-outlined md-1x">cancel</i> Annuleer zoekactie</a></li>';
        $_result .= '<li><a href="#" class="dropdown-item" id="advancedSearch"><i class="material-symbols-outlined md-1x">tune</i> Advanced search...</a></li>';
        $_result .= '</div>';
        $_result .= '</div>';
        return $_result;
    }

    function GetSearchHistory() {
        global $_sqlSearchHistory;

        $_rs = $this->dbGetAll($_sqlSearchHistory, array($this->record->CLIENTID, $_SESSION['userid'], $this->record->FORMID));
        return $_rs;
    }

    function ShowSearchSaveModal() {
        echo '
        <div class="modal inmodal in" id="modSaveSearch" tabindex="-1" role="dialog" aria-hidden="true" style="display:none;">
            <div class="modal-dialog modal-sm">
                <div class="modal-content animated fadeIn">
                    <form action="." method="POST" id="frmSaveSearch">
                        <input type="hidden" name="_hdnAction" value="plr_savesearch" />
                        <input type="hidden" name="_hdnForm" value="'.$this->record->FORMID.'" />
                        <input type="hidden" name="_hdnDatabase" value="'.$this->record->DATABASEID.'" />
                        <input type="hidden" name="SEARCHQUERY" value="'.base64_encode(json_encode(array('q'=>$_GET['q'], 'qc'=>$_GET['qc'], 'qtable'=>$_GET['qtable']))).'" />
                        <div class="modal-header">
                            <h2 class="">Zoekresultaat bewaren</h2>
                        </div>
                        <div class="modal-body">
                            <p>Geef het zoekresultaat een naam:</p>
                            <div class="form-group"><input type="text" name="SEARCHNAME" autofocus required="required" placeholder="Typ naam in" class="form-control"></div>
                            <div id="modSaveSearchError" class="text-danger"></div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">'.lang_get('but_cancel').'</button>
                            <button type="button" id="btnSaveSearchSave" class="btn btn-primary">'.lang_get('but_save').'</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        ';
    }

    function HasActiveSorting() {
        return count($this->GetSortArray()) > 0;
    }

    function ShowSortingActions() {
        if ($this->HasActiveSorting()) {
            $href = $this->MakeURLQuery( array('offset','q','qc','qtable'), $this->detail );
            echo '<a href="'.$href.'" class="remove_ordercols">'.lang_get('cancel_sorting').'</a>';
        }
    }

    function ShowFormSettings($state) {
        $isBasicForm = !empty($_GET['basicform']);

        if (!$this->detail
            and (isset($this->moduleobj) and $this->moduleobj->showControls == true)
        or (!isset($this->moduleobj) and !$isBasicForm and $this->recordcount)) {
            echo '
            <div class="dropdownx pull-right">
                <a href="#" class="advanced-link dropdown-togglex" xdata-toggle="dropdown"
                data-placement="top" title="'.lang_get('search_advanced_options').'"
                >
                    <i class="glyphicons glyphicons-cogwheel"></i>
                </a>
                <ul class="dropdown-menu"><li><a href="#">';
            echo $this->DisplayLimit();
            echo '</a></li>';
            echo '</ul></div>';

            // Show the 'Check webpage' button
            $appobj =& $this->AppObject();
            if ($appobj->record->WEBSITE && $this->owner->record->WEBPAGELINK) {

                if ($state == 'edit' or $state == 'view')
                    $replacement = '\' + $(\'%PARAM%\').val()';
                else
                    $replacement = '\'';

                $linkUrl = $this->owner->record->WEBPAGELINK;
                $first = strpos($linkUrl, '%');
                if ($first === false) {
                    $linkUrl = $linkUrl. '\'';
                } else {
                    $next = strpos($linkUrl, '%', $first + 1);
                    $paramname = substr($linkUrl, $first + 1, $next - $first - 1);
                    $replacement = str_replace('%PARAM%', $paramname, $replacement);
                    $linkUrl = str_replace('%'.$paramname.'%', $replacement, $linkUrl);
                }
                $linkUrl = '\''.$appobj->record->WEBSITE .'/'.$linkUrl;
                echo '<div id="checkwebpage"><a href="#" onclick="window.location.href='.$linkUrl.'" title="'.lang_get('check_webpage_hint').'" tabindex="3">'.lang_get('check_webpage').'</a> <a style="font-size:0.8em" href="#" onclick="window.open('.$linkUrl.');return false;" >('.lang_get('seperate_window').')</a></div>';
            }
        }
        if ($this->record->REFRESHFORM == 'Y' and ($state == 'allitems' or $state == 'detail_allitems')) {
            echo lang_get('refreshinterval_label').": ".$this->RefreshInterval();
        }
    }

    function ShowNavigationLinks($state, $permissiontype, $detail) {
      if (!$detail and ($state == 'edit' or $state == 'view') and $this->owner->record->SHOWDETAILASLINKS == 'Y') {
        $url = $this->MakeURL();
        $url .= 'edit/'.$_GET['rec'].'/'.$this->record->METANAME.'/';
        $url .= $this->MakeQuery();
        echo '<hr class="fix" />';
        echo '<ul class="formnavigatebox"><li><a href="'.$url.'" class="formnavigate">Bekijk de '.$this->owner->detailform->ItemsName().' van dit '.$this->ItemName().'-record &raquo;</a></li></ul>';
        echo '<hr class="fix" /><br />';
      }
    }

    function GetFormAsRSS(&$rs) {
        require 'classes/rss.class.php';

        $rsscolumns = explode(';',$this->record->RSSCOLUMNS);

        $rss = new Services_RSS();
        $rss->usecaching = false;
        $rs = $rs->GetAll();
        return $rss->Create($this, $rs, $rsscolumns);
    }

    function DisplayLimit() {
        $values = array('50 '.lang_get('n_items') => 50, '100 '.lang_get('n_items') => 100, '200 '.lang_get('n_items') => 200); // , lang_get('all_items') => -1
        $defaultfound = false;
        $result = '<form method="post" action="'.$this->basehref.'">';
//        $result .= '<input type="hidden" name="_hdnAction" value="recordlimit" />';
        $result .= lang_get('recordlimit_label').": ";
        $result .= "<select name=\"record_limit\" id=\"recordLimitSelect\">";
        foreach($values as $label => $value) {
            $result .= "<option value=\"$value\"";
            if ($value == $this->limit) {
              $result .= " selected=\"selected\"";
              $defaultfound = true;
            }
            $result .= ">$label</option>";
        }
        $_limit = $this->limit." ".lang_get('n_items');
        if (!$defaultfound) {
            if (!$this->limit) $_limit = lang_get('all_items');
            $result .= "<option selected='selected' value='".$this->limit."'>".$_limit."</option>";
        }
        $result .= "</select>";
        $result .= '</form>';
        echo $result;
    }

    function RefreshIntervalValue() {
      if ( $_SESSION['refresh_interval'] != '')
        $currentrefreshinterval = $_SESSION['refresh_interval'];
      elseif ( $this->record->DEFAULTREFRESHINTERVAL > 0 )
        $currentrefreshinterval = $this->record->DEFAULTREFRESHINTERVAL;
      else
        $currentrefreshinterval = $_CONFIG['default_refresh_interval'];
        return $currentrefreshinterval;
    }

    function RefreshInterval() {
        global $_CONFIG;

      $basehref = $this->MakeURLQuery( array('q','qc','qtable','sort','dir','offset') );

      $values = array(
        lang_get('n_seconds', 10) => 10,
        lang_get('n_seconds', 20) => 20,
        lang_get('n_seconds', 30) => 30,
        lang_get('n_minutes', 1) => 60,
        lang_get('n_minutes', 2) => 120,
        lang_get('n_minutes', 5) => 300
      );
      $currentrefreshinterval = $this->RefreshIntervalValue();

      $defaultfound = false;
      $result = '<form method="post" action="'.$basehref.'">';
      $result .= "<input type=\"hidden\" name=\"_hdnAction\" value=\"changerefreshinterval\" >";
      $result .= "<select name=\"refresh_interval\" id=\"refresh_interval\" onchange=\"submit();\" >";
      foreach($values as $label => $value) {
        $result .= "<option value=\"$value\"";
        if ($value == $currentrefreshinterval) {
          $result .= " selected=\"selected\"";
          $defaultfound = true;
        }
        $result .= ">$label</option>";
      }
      if (!$defaultfound) $result .= "<option selected='selected' value='".$currentrefreshinterval."'>".lang_get('n_seconds', $currentrefreshinterval)."</option>";
      $result .= "</select>";
      $result .= '</form>';

      return $result;
    }

    function MakeBaseURL($servicecall=false) {
        global $_GVARS;

        $basehref = $this->AppObject()->applicationLink($servicecall);
        return $basehref;
    }

    function MakeURL($vars=false, $detailform=false, $servicecall=false, $action=false) {
        global $_GVARS;

        $basehref = $this->MakeBaseURL($servicecall);
        $cfm = null;

        if (isset($_GET['const']))
            $cfm = 'const';
        if (isset($_GET['form']))
            $cfm = 'form';
        if (isset($_GET['module']) and !isset($_GET['const']) and !isset($_GET['form']))
            $cfm = 'module';

        if (isset($cfm)) {
            $cfmid  = $_GET[$cfm];
        } else {
            /**
             * If no construct/form/module is provided, then select the default construct for this application
             */
            $cfm = 'const';
            $cfmid = $this->AppObject()->GetCurrentConstructMetaName();
        }

        if ($action == false) {
            $action = $_GET['action'];
        }

        $basehref .= $cfm.'/'.$cfmid.'/';

        if ($detailform) {
            $basehref .= $action.'/'.urlencode($_GET['rec']).'/'.$detailform.'/';
        } else {
            if ( is_array($vars) !== false ) {
                $key = array_search('action', $vars);

                if ($key !== false and isset($action)) {
                    $basehref .= ''.urlencode($action).'/';
                    unset($vars[$key]);
                }
                $key = array_search('rec', $vars);
                if ($key !== false and isset($_GET['rec'])) {
                    $basehref .= ''.urlencode($_GET['rec']).'/';
                    unset($vars[$key]);
                }
            }
        }
        return $basehref;
    }

    function MakeQuery($vars = false) {
        global $polaris;

        $query = '?';

        if ( is_array($vars) !== false ) {
            foreach($vars as $var) {
                if (!array_search('action', $vars) and !array_search('rec', $vars)) {
                    $found = false;
                    if ($var == 'sort') {
                        for ($i=1;$i<=9;$i++) {
                            if ( isset($_GET['sort_'.$i]) ) {
                                $query = add_query_arg( $query, 'sort_'.$i, urlencode($_GET['sort_'.$i]) );
                                $query = add_query_arg( $query, 'dir_'.$i, urlencode($_GET['dir_'.$i]) );
                            }
                        }
                    } else {
                        if ( isset($_GET[$var]) ) {
                            if ( !is_array($_GET[$var]) ) {
                                $query = add_query_arg( $query, $var, urlencode($_GET[$var]) );
                            } else {
                                foreach( $_GET[$var] as $key => $value) {
                                    if ( !empty($value) ) {
                                        $query = add_query_arg( $query, $var.'[]', urlencode($value) );
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        if (strcasecmp(get_class($this->owner), 'base_plrconstruct') == 0 and $this->owner->IsMaximized())
            $query = add_query_arg($query, 'maximize', 'true' );
        if (isset($_GET['tag']))
            $query = add_query_arg($query, 'tag', $_GET['tag'] );
        if (isset($_GET['doautoclose']) and $_GET['doautoclose'] == 'true')
            $query = add_query_arg($query, 'autoclosewindow', 'true' );
        if (isset($_GET['select']) and $_GET['select'] == 'true') {
            $query = add_query_arg($query, 'select', 'true' );
            $query = add_query_arg($query, 'selectfield', $_GET['selectfield'] );
            $query = add_query_arg($query, 'selectmasterfield', $_GET['selectmasterfield'] );
            $query = add_query_arg($query, 'selectshowfield', $_GET['selectshowfield'] );
        }
        return $query;
    }

    function MakeURLQuery($vars = false, $detail = false, $servicecall=false) {
        $result = $this->MakeURL($vars, $detail, $servicecall).$this->MakeQuery($vars);
        return $result;
    }

    function ShowCustomScripts() {
        global $_GVARS;

        if (!empty($this->record->CUSTOMSCRIPTS)) {
            echo '<script type="text/javascript">';
            echo $this->record->CUSTOMSCRIPTS;
            echo '</script>';
        }
    }

    function SetGroupField($columnname, $value) {
        // $_SESSION[$constructid.'.'.GROUP_PREFIX.$columnname] = $value;
        $_SESSION[strtoupper($columnname)] = $value;
    }

    function GetGroupField($columnname) {
        return $_SESSION[GROUP_PREFIX.strtoupper($columnname)];
    }

    function GetGroupFields($constructid=false) {
        global $polaris;
        global $_sqlGroupLines;

        $rs = $this->dbExecute($_sqlGroupLines, array($this->record->CLIENTID, $this->record->FORMID));
        $this->groupfilters = array();
        while ($rec = $rs->FetchNextObject(true)) {
            $_selectname = GROUP_PREFIX.$rec->COLUMNNAME;
            $_description = ProcessSQL($this->database->userdb, $rec->LINEDESCRIPTION);
            $_defvalsql = $polaris->ReplaceDynamicVariables($rec->LINEDEFAULTVALUE);
            $_defval = ProcessSQL($this->database->userdb, $_defvalsql);
            $currentvalue = isset($_GET[$_selectname]) ? $_GET[$_selectname] : ($_SESSION[$_selectname] ?? '');
            $this->SetGroupField(GROUP_PREFIX.$rec->COLUMNNAME, $currentvalue);
            $this->groupfilters[$_selectname] = array('currentvalue' => $currentvalue, 'description' => $_description, 'groupsource' => $rec->GROUPSOURCE, 'defaultvalue' => $_defval);
        }
    }

    function ShowGroupFields() {
        global $_GVARS;
        global $_sqlGroupLines;
        global $polaris;

        $this->currentGroupFilterValue = false;

        $this->database->connectUserDB();

        if ($this->groupfilters == NULL or count($this->groupfilters) == 0) {
            $this->GetGroupFields();
        }

        if (count($this->groupfilters) > 0) {
            echo '<div id="filtergroup" class="row">' ;
            foreach($this->groupfilters as $selectname => $groupfilter) {
                $_currentvalue = (!empty($groupfilter['currentvalue']) || $groupfilter['currentvalue'] == '0') ? $groupfilter['currentvalue'] : $groupfilter['defaultvalue'];
                echo '<div class="col-sm-5 text-right"><h4>'.$groupfilter['description'].'<h4></div> ';
                echo '<div class="col-sm-7"  data-toggle="tooltip" data-placement="bottom" title="Invullen!">';
                $_sql = $polaris->ReplaceDynamicVariables($groupfilter['groupsource']);
                echo SQLAsSelectList($this->database->userdb, $_sql, $keycolumn = 0, $namecolumn = 1, $_currentvalue, $id = '_fld'.$selectname, $selectname, $extra = 'onchange="javascript:Polaris.Form.groupFilter(this)"', $required=false);
                if (!empty($_currentvalue)) {
                    $this->SetGroupField($selectname, $_currentvalue);
                }
                echo '</div>';
            }
            echo '</div>';
        }
    }

    function IsOverviewDetailform() {
        global $polaris;

        return !isset($polaris->currentdetailform->record->METANAME);
    }

    function ShowDefaultFormButtons($state) {
        if ($this->HasOption('pin-on-insert') and $state == 'insert')
            $_toggletextSave = lang_get('but_save_and_pin');
        elseif ($this->HasOption('insert-and-new'))
            $_toggletextSave = lang_get('but_save_and_new');
        else
            $_toggletextSave = lang_get('but_save');

        if ($state == 'view') {
            if ($this->IsOverviewDetailform()) {
                echo '<a href="'.$this->basehref.'" class="btn btn-default" title="'.lang_get('but_back_hint').'"><i class="material-symbols-outlined">arrow_back</i>';
                echo lang_get('but_back');
                echo '</a> ';

                echo '<button type="button" id="btnEditForm" tabindex="1000" accesskey="F9" action="edit" name="_hdnEditButton" value="" role="button" class="btn btn-primary" title="'.lang_get('but_edit_hint').'">';
                echo lang_get('but_edit');
                echo '</button> ';
            }
        } else {
            if ($this->detail) {
                $_url = $this->MakeURLQuery($vars=["offset"], $_GET['detailform']);
            } else {
                if ($this->GetFormView() == 'DETAILSVIEW') {
                    // take the current url and replace 'edit' with 'view'
                    $_url = str_replace('/edit/','/view/',$_SERVER['REQUEST_URI']);
                } else {
                    $_url = $this->basehref;
                }
            }

            echo '<button type="button" id="btnSaveForm" tabindex="1000" accesskey="F9" action="save" name="_hdnSaveButton" value="" role="button" class="btn btn-primary" title="'.lang_get('but_save_hint').'">';
            echo $_toggletextSave;
            echo '</button> ';

            echo '<button type="button" tabindex="1010" accesskey="F2" role="button" class="btn btn-default xbtn-outline xnegative cancelbutton" onclick="location.href=\''.$_url.'\'" title="'.lang_get('but_cancel_hint').'">
            '.lang_get('but_cancel').'</button>';
        }
    }

    function ShowWizardButtons() {
        // somewhere in the code the basehref is set to null, so reassigning
        $this->basehref = $this->MakeURLQuery( array('q','qc','qtable','sort','dir','offset',/*module.webmenu*/'group'), $this->detail );
        echo '
        <button class="btn btn-success btn-lg" id="wizard-prev-btn" type="button" style="text-transform: capitalize;">'.lang_get('navigate_prev').'</button>
        <button class="btn btn-success btn-lg" id="wizard-next-btn" type="button" style="text-transform: capitalize;">'.lang_get('navigate_next').'</button>
        <button class="btn btn-primary btn-lg" id="wizard-finish-btn" type="button" accesskey="F9">'.lang_get('but_save').'</button>
        <button type="button" tabindex="1010" accesskey="F2" role="button" class="btn btn-default btn-lg cancelbutton" onclick="location.href=\''.$this->basehref.'\'" title="'.lang_get('but_cancel_hint').'">
        '.lang_get('but_cancel').'</button>
      ';
    }

    function ShowPublishButtons() {
        echo '<input type="submit" class="publish positive publishbutton" action="save" name="_hdnPublishButton" value="'.lang_get('save_publish').'" onclick="document.getElementById(\'frmextra\')._hdnPublishField.value=\''.$recordid.'\';return true" tabindex="0" />';
        echo '<input type="submit" class="savedraft savedraftbutton" action="savedraft" class="sbttn protect publish" onclick="submit()" name="_hdnSaveButton" value="'.lang_get('but_save').'" title="'.lang_get('but_save_hint').'" tabindex="1" />';
    }

    function ShowButtons($state, $permissiontype) {
        if ($this->options->showbuttonbar == false) return;

        if ($this->moduleobj) {
            $state = $this->moduleobj->GetCurrentState() ? : $state;
        }
        if (($state == 'edit') or ($state == 'insert') or ($state == 'clone') or ($state == 'view')
        or ($state == 'detail_search') or ($state == 'detail_edit') or ($state == 'detail_view') or ($state == 'detail_insert') or ($state == 'detail_clone')) {

            echo '<div class="buttons">';
            if (
               (($permissiontype & UPDATE) == UPDATE)
            or ((($permissiontype & INSERT) == INSERT) and $state == 'insert')
            ) {

                if ($this->publishform) {
                    $this->ShowPublishButtons();
                } elseif ($this->owner->masterform_insert->record->DEFAULTVIEWTYPE == 'WIZARDVIEW' and $state == 'insert') {

                    $this->ShowWizardButtons();
                } else {
                    $this->ShowDefaultFormButtons($state);
                }

            } else {
                echo '<button class="btn btn-primary protect" onclick="javascript:window.location.href=\''.$this->basehref.'\';return false;" title="'.lang_get('but_close_hint').'" tabindex="2"><i class="fa fa-close"></i> '.lang_get('but_close').'</button>';
            }

            $this->actions->ShowActionButtons($state, false, 'BOTTOM', 'medium');

            echo '</div>';
        }
    }

    function GetMasterCaption($masterrecord) {
        if ($masterrecord and isset($this->owner->masterform) and $this->owner->masterform->record->CAPTIONCOLUMNNAME != '') {
            $captioncols = explode(';',$this->owner->masterform->record->CAPTIONCOLUMNNAME);
            foreach($captioncols as $captioncol) {
                $mastercaption .= $masterrecord->$captioncol . ' - ';
            }
            $mastercaption = substr($mastercaption, 0, -3);
       } else {
           $mastercaption = '';
       }
       return $mastercaption;
    }

    function ShowMasterRecordLink($masterrecord, $initialvisible=true, $skipfrom=false) {
        if ($masterrecord and isset($this->owner->masterform) and $this->owner->masterform->record->CAPTIONCOLUMNNAME != '') {
            if (!$initialvisible) $hidden = "style=\"display:none\"";
            $mastercaption = $this->GetMasterCaption($masterrecord);
            $masterformname = $this->owner->masterform->GetItemName();
            $masterurl = $this->MakeURLQuery( array('action','rec','offset') );
            $masterurl = str_replace('/detail/','/edit/',$masterurl);
            $from = $skipfrom?'':'van '.$masterformname;
            echo '<span '.$hidden.' id="masterrecordlink" class="detailcaption">&ndash; '.$from.' <span class="detailcaptionname">\'<a href="'.$masterurl.'">'.$mastercaption.'</a>\'</span></span>';
        }
    }

    function ShowFormInfo($permissiontype, $recordcount) {
      $basehref = $this->MakeURLQuery();

      echo "<div id=\"forminfo\" class=\"row\">";
      if ($recordcount > 0) {
        echo "<span class=\"left\">";
        if (($permissiontype & UPDATE ) == UPDATE )
          echo lang_get('click_and_edit_item', $this->ItemName());
        else
          echo lang_get('click_and_view_item', $this->ItemName());
        echo "</span>";
      }
      echo "<span class=\"right\">";
      if (($permissiontype & INSERT ) == INSERT ) {
          $insertquery = $this->MakeURL(false, $this->detail).'insert/'.$this->MakeQuery();
          echo "<a href=\"{$insertquery}\">".lang_get('add_item_to_list', $this->ItemName() )."</a>";
      }
      echo "&nbsp;</span></div>";
    }

    function ShowEditWebPageLink() {
      global $scramble;

      if ($this->record->WEBPAGENAME != '') {
        $pagearray = explode(':', $this->record->WEBPAGENAME);
        $webpagehash = MD5($scramble.'_'.$pagearray[1]);
        $webpageconstruct = $this->record->WEBPAGECONSTRUCT;
        echo "<span class=\"editwebpage center\"><a href=\"{$_GVARS['serverroot']}/app/{$_GET['app']}/const/$webpageconstruct/?action=edit&amp;type=$pagearray[0]&amp;rec=$webpagehash\">wijzig webpagina <img src=\"{$_GVARS['serverroot']}/images/edit_webpage2.png\" style=\"vertical-align:middle\" height=\"16\" width=\"16\" /></a></span>";
      }
    }

    function &GetFormattedViewContent($outputformat, $detail=false, $masterrecord=false, $limit=false, $customfilter=false) {
        global $_CONFIG;

        $result = '';
        switch($outputformat) {
        case 'xml':
            header("Content-type: text/xml\n");
            if ($_GET['dest'] == 'tofile') {
                $plr = $this->polaris();
                header("Content-Disposition: attachment; filename=".$plr->tpl->getTemplateVars('outputfile').".xml");
            }
            if ($_GET['subformat'] == 'ms-xml') {
                include_once('adodb-xml_data.inc.php');
                ini_set("memory_limit","20M");
                $rs = $this->GetViewContent($detail, $masterrecord, $limit);
                $o = new adodb_xml_data($rs);
                $result .= "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
                $result .= $o->_decodeChars($o->exportAsXML());
            } else {
                $this->database->userdb->SetFetchMode(ADODB_FETCH_ASSOC);
                $rs = $this->GetViewContent($detail, $masterrecord, $limit);
                require_once('includes/adodb_toxml.inc.php');
                $result .= rs2xml($rs, $this->GetDataDestination());
            }
        break;
        case 'php':
            $this->database->userdb->SetFetchMode(ADODB_FETCH_ASSOC);
            $rs = $this->GetViewContent($detail, $masterrecord, $limit);
            $result = $this->polaris()->rs2format($rs, $outputformat);
        break;
        case 'json':
            $this->database->userdb->SetFetchMode(ADODB_FETCH_ASSOC);
            if ($_GET['headers'] == 'true') {
                $rs = $this->pages->GetListViewHeader($this->database->userdb, $permissiontype, $repeatedheader = false, $this->masterfields);
            } else {
                $rs = $this->GetViewContent($detail, $masterrecord, $limit, $customfilter);
            }
            $result = $this->polaris()->rs2format($rs, $outputformat, false, $_CONFIG['debug']);
        break;
        case 'json2':
            $this->database->userdb->SetFetchMode(ADODB_FETCH_ASSOC);
            $_rs = $this->GetViewContent($detail, $masterrecord, $limit, $customfilter);
            $rs['result'] = $_rs;
            $rs['recordcount'] = $_rs->RecordCount();
            $result .= json_encode($rs);
        break;
        case 'xls':
            require_once('includes/excel.inc.php');
            $this->database->userdb->SetFetchMode(ADODB_FETCH_ASSOC);
            $rs = $this->GetViewContent($detail, $masterrecord, $limit, $customfilter);
            $plr = $this->polaris();
            $filename = sanitize_filename($plr->tpl->getTemplateVars('outputfile'), 'xls');
            $excel = new ExcelExport($rs, $filename);
            $excel->download_with_dialog($filename);

            break;
        case 'csv':
            if ($this->servicecall) {
                // header("Content-type: text/x-csv\n");
                if ($_GET['dest'] == 'tofile') {
                    $plr = $this->polaris();
                    header("Content-Disposition: attachment; filename=".$plr->tpl->getTemplateVars('outputfile').".csv");
                    header('Content-Type: application/csv');
                }
            } else {
                $result .= "<textarea cols='130' rows='30'>";
            }
            $this->database->userdb->SetFetchMode(ADODB_FETCH_ASSOC);
            $rs = $this->GetViewContent($detail, $masterrecord, $limit, $customfilter, as_array:false);
            $result = $this->polaris()->rs2format($rs, $outputformat, $_GET['subformat']);
            if ($this->servicecall) {
            } else {
                $result .= "</textarea>";
            }
        break;
        case 'rss':
            header("Content-type: text/xml; charset=utf-8");
            $this->database->userdb->SetFetchMode(ADODB_FETCH_ASSOC);
            $rs = $this->GetViewContent($detail, $masterrecord, $limit, $customfilter);
            $this->GetFormAsRSS($rs);
        break;
        case 'list':
            $this->database->userdb->SetFetchMode(ADODB_FETCH_NUM);
            $rs = $this->GetViewContent($detail, $masterrecord, $limit, false, false);
            // $rs = $rs->GetAll();
            $result = '';
            foreach($rs as $record) {
                if ($record[1] != '')
                    $result .= $record[0].' '.$record[1].'|'.$record[0]."\n";
                else
                    $result .= $record[0].'|'.$record[0]."\n";
            }
        break;
        }
        return $result;
    }

    function GetViewSQL($detail, $masterrecord, $limit=false, $customfilter=false) {
        if ($detail or $GLOBALS['detailonly']) {
            if (isset($this->owner->masterform))
                $mastertable = $this->owner->masterform->GetDataDestination(true);
            $_sql = $this->MakeDetailSQL($edit=false, $masterrecord, $countselect=false, $mastertable, $customfilter);
        } else {
            $includedetailtables = $this->owner->record->DETAILFORMID > 0;
            if ($includedetailtables) {
                $detailtable = $this->owner->detailform->GetDataDestination();
                $countdetails = true;
            }
            $edit = $_GET['action'];
            $_sql = $this->MakeSQL($edit, $countselect = false, $thesearchvalue = false, $masterfields = false, $countdetails, $detailtable, $customfilter);
        }
        return $_sql;
    }

    function LoadViewContent($detail, $masterrecord, $limit=false, $customfilter=false) {
        $this->rsdata = $this->GetViewContent($detail, $masterrecord, $limit, $customfilter);
    }

    function GetViewContent($detail, $masterrecord, $limit=false, $customfilter=false, $customsql=false, $as_array=true) {
        global $_CONFIG;

        $po =& $this->polaris();
        $po->timer->startTimer('User_SQL');

        if ($customsql) {
            $_sql = $customsql;
        } else {
            $_sql = $this->GetViewSQL($detail, $masterrecord, $limit, $customfilter);
        }

        $this->DetermineLimits();
        $this->limit = $this->GetLimit();

        if ($_GET['debuguserdb'])
            $this->database->userdb->debug = $_GET['debuguserdb'];
        // execute SQL to get recordset for display
        $tmpsql = strtoupper($_sql);
        $selectend = strpos($tmpsql, '/*_SELECTEND_*/');
        if (strpos($tmpsql, ' LIMIT ', $selectend) !== FALSE OR !$this->limit) {
            // Don't add a LIMIT when the SQL already contains a LIMIT clause OR all records should be read
            $result = $this->database->userdb->Execute($_sql) or die ('Error in generated SQL Statement: '.$_sql.'<br /> >> '.$this->database->userdb->ErrorMsg());
            if ($as_array) {
                $result = $result->GetAll();
            }
        } else if ($_sql) {
            if ($this->clientTableResourceLimit() > 0) {
                $resultlimit = $this->clientTableResourceLimit();
            } elseif ($limit) {
                $resultlimit = $limit;
            } elseif ($detail) {
                $resultlimit = 1000;
            } elseif ($this->limit and $this->limit > 0) {
                $resultlimit = $this->limit;
            }
            $startno = $this->startno - 1;
            try {
                if (!$this->database->userdb->IsConnected() ) {
                    throw new ADODB_PLR_Exception(false, false, -1, 'Userdatabase is niet verbonden.', false, false, $this->database);
                }
                if ($this->FormHasActivePins() or $this->HasOption('no-first-rows')) $this->database->userdb->firstrows = false; // bij kleine datasets (zoals bij gepinde dataset), firstrows uitzetten
                $result = $this->database->userdb->selectLimit($_sql, $resultlimit, $startno) or die ('Error in generated SQL Statement: '.$_sql.'<br /> >> '.$this->database->userdb->ErrorMsg());

                if ($as_array) {
                    $result = $result->GetAll();
                }
                if ($this->FormHasActivePins() or $this->HasOption('no-first-rows')) $this->database->userdb->firstrows = true;
            } catch (exception $E) {

                if ($_CONFIG['debug']) {
                    echo "<div id=\"errormessagex\" class=\"error\" style=\"-khtml-user-select:auto\">{$E->msg}<br /><span class=\"original\">({$E->msg}: ".$E->getCode().")</span>";
                    echo "<br /><br />".$_sql;
                    echo "</div>";
                } else {
                    echo "<div id=\"errormessagex\" class=\"error\" style=\"-khtml-user-select:auto\">An error occured during the execution of the SQL statement.</div>";
                }

                $result = false;
            }
        }
        $token = 'User_SQL';
        $po->timer->endTimer($token);
        $timeTaken = $po->timer->getEndTime($token) - $po->timer->getStartTime($token);
        if ($timeTaken > 1.5) {
            $po->logUserAction('Benchmark', session_id(), $_SESSION['name'], 'Tijd: '.$timeTaken.' '.var_export($_GET, true));
        }

        return $result;
    }

    function FormHasSearchResult() {
        $result = false;
        if (isset($_GET['q'])) {
            $result = !empty($q);
        }
        if (isset($_GET['qv'])) {
            $q = implode('', $_GET['qv']);
            $result = !empty($q);
        }

        return $result;
    }

    function ShowListViewTableHeader($id, $ajaxdataurl=false) {
        if (!$this->detail or $GLOBALS['detailonly']) {
            $searchresultclass = ($this->FormHasSearchResult())?'searchresult':'';
        }
        if ($this->HasOption('group-table') ) $grouptableclass = 'group-table right';

        $GLOBALS['showdetaillist'] = true;
        if (!$this->detail and $this->owner->detailform->record->METANAME) {
            $showdetaillistclass = 'showdetaillist';
            $detailformref  = 'ref="'.$this->owner->detailform->record->METANAME.'"';
        }

        echo '<table id="'.$id.'" class="table table-striped table-borderedx table-hover dataTable '.$liveview.' '.$grouptableclass.' '.$searchresultclass.' '.$showdetaillistclass.'" '.$detailformref.' cellspacing="0" cellpadding="0">';
    }

    function ShowListViewTableFooter($state, $permissiontype) {
        echo '</table>';
    }

    function ShowListViewControls($state, $permissiontype) {
        global $_GVARS;
        global $polaris;

        if (($state == 'allitems' or $state == 'detail_allitems' or $state == 'search') and ($_GET['select'] != 'true')) {

            echo "<div id=\"formcontrols\" class=\"footerscroll\" style=\"clear:both\">";

            if ((isset($this->moduleobj) and $this->moduleobj->showDefaultButtons == true) or (!isset($this->moduleobj)) and $this->recordcount) {

                echo "<div class=\"buttons smaller\" style=\"float:left;\">";
                if (($permissiontype & DELETE) == DELETE or $this->detail == '' or !empty($this->actions)) {
                    if ($this->detail == '' and $this->HasOption('show-pinning')) {
                        echo '<button type="button" id="_hdnPinButton" data-colorclass="info" class="btn btn-smx btn-default disabled" data-toggle="tooltip" oritext="'.lang_get('but_pinrecord').'" toggled="Voeg pin toe" title="'.lang_get('but_pinrecord_hint').'" tabindex="2">
                        <i class="material-symbols-outlined md-1x">bookmark</i>&nbsp;
                        '.lang_get('but_pinrecord').'</button>&nbsp;';
                    }

                    if (($permissiontype & DELETE) == DELETE )
                        echo '<button type="button" id="_hdnDeleteButton" data-colorclass="danger" class="btn btn-smx btn-default deletebutton disabled" title="'.lang_get('confirmation').'" tabindex="3"> <i class="fa fa-trash-o"></i>&nbsp;
                        '.lang_get('but_delete').'</button>&nbsp;';
                }
                echo "</div>"; // buttons
            }
            if ($this->detail == '') {
                echo "<div class=\"buttons\" style=\"float:left\">";
                $this->actions->ShowActionButtons($state, false, 'BOTTOM', 'medium');
                echo "</div>"; // buttons
            }

            echo "</div>"; // formcontrols

        }
    }

    function ShowDetailsHeader() {
        // Use null coalescing operator to safely access values
        if (isset($this->owner->masterform)) {
            $master = $this->owner->masterform->record;
        }
        $captionColumnName = $master->CAPTIONCOLUMNNAME;
        $masterRecordCaption = isset($this->masterrecord->$captionColumnName)
                                ? htmlspecialchars($this->masterrecord->$captionColumnName)
                                : '';
        $subCaptionColumnName = $master->SUBCAPTIONCOLUMNNAME;
        $masterRecordSubCaption = isset($this->masterrecord->$subCaptionColumnName)
                                   ? htmlspecialchars($this->masterrecord->$subCaptionColumnName)
                                   : '';

        // Output HTML
        echo '
            <div class="detailview-header">
                <div>
                    <h3 class="copy-clipboard" id="masterrecord-caption">' . $masterRecordCaption . '</h3>
                    <h6 class="copy-clipboard">' . $masterRecordSubCaption . '</h6>
                </div>
            </div>
        ';
    }

    function ShowDetailformsAsMenu($currentdetailform) {
        $this->ShowDetailsHeader();
        $this->pages->ShowDetailformsMenu($currentdetailform);
    }

    function ShowTaskViewPanels($statuscolumn, $recordset) {
        $plr = $this->polaris();

        $_statusLine = $this->pages->FindLineRecord($statuscolumn);

        $_tmpstat = explode(';',$_statusLine->record->LISTVALUES);
        foreach($_tmpstat as $status) {
            $_values = explode('=',$status);
            $_statussen[] = $_values;
        }

        $_priorityField = 'PRIORITEIT_sss40';
        $_standardPriorities = ['L' => 'Low', 'M' => 'Medium', 'H' => 'High'];
        // Enrich the recordset with priority data
        foreach($recordset as $key => $record) {
            $recordset[$key]['PRIORITY_NAME'] = $_standardPriorities[$record[$_priorityField]];
        }
        $plr->tpl->assign('panes', $_statussen);
        $plr->tpl->assign('items', $recordset);
        $plr->tpl->assign('statusfield', $_statusLine->record->COLUMNNAME);
        $plr->tpl->assign('sortfield', $this->record->SORTCOLUMNNAME);
        $plr->tpl->assign('priorityfield', $_priorityField);

        $plr->tpl->assign('panewidth', floor(12/count($_statussen)));
        echo $plr->tpl->fetch('taskview.tpl.php');
    }

    function ShowTaskView($state='allitems', $permissiontype=SELECT) {
        $this->ShowTaskViewPanels($this->record->CAPTIONCOLUMNNAME, $this->rsdata);
    }

    function ShowListView($state='allitems', $permissiontype=SELECT) {
        echo '<div class="element-detail-box view-detail-box">';

        $this->ShowHelpPanel();

        $this->ShowEditWebPageLink();

        $this->ShowListViewTableHeader('datalistview');
        $fieldcount = $this->pages->ShowListViewHeader($this->database->userdb, $permissiontype, $repeatedheader = false, $this->masterfields);

        $this->pages->ShowListViewData($this->database->userdb, $this->rsdata, $permissiontype, $this->masterfields);
        $this->pages->ShowListViewFormulas($this->database->userdb, $permissiontype, $this->masterfields);
        $this->pages->ShowListViewFooter($fieldcount, $permissiontype, $this->masterfields);
        $this->ShowListViewTableFooter($state, $permissiontype);

        echo '</div>';
    }

    function ShowSQLView($state, $permissiontype, $detail=false, $masterrecord=false) {

        if (isset($_POST['customsql'])) {
            $_sql = $_POST['customsql'];
            /* Store the SQL for later */
            $inst =& $this->plrInstance();
            $inst->Execute('UPDATE plr_form SET tablename = ? WHERE MD5(CONCAT(CLIENTID,"_",FORMID)) = ?', array('SQL::'.$_sql, md5($this->record->CLIENTID.'_'.$this->record->FORMID)));
        } else {
            $_sql = $this->GetViewSQL($detail, $masterrecord=false);
        }
        $_strippedSQL = str_replace('SQL::', '', $_sql);
        $_strippedSQL = str_replace('\\', '', $_strippedSQL);
        echo "<form action='.' method='POST'><input type='hidden' name='_hdnProcessedByModule' value='true'><textarea rows='3' cols='60' name='customsql'>$_strippedSQL</textarea><br /><input type='submit' value='Go'></form>
        ";

        include('tohtml.inc.php');
        rs2html($this->rsdata, 'id="datalistview" class="data striped nowrap group-table right" cellspacing="0" cellpadding="0"');
    }

    function ShowLiveView($permissiontype, $detail) {
        global $_GVARS;
        $masterajaxurl = $_GVARS['serverpath'].$this->MakeURLQuery(false, $detail, $servicecall=true);
        $masterajaxurl .= '?limit=20';
        $this->ShowListViewTableHeader('dataheaderview', $masterajaxurl);
        $fieldcount = $this->pages->ShowListViewHeader($this->database->userdb, $permissiontype, $repeatedheader = false, $this->masterfields);
        $this->pages->ShowListViewDataRowTemplate($permissiontype, $rec=false, $rs=false);
        $this->pages->ShowListViewFooter($fieldcount, $permissiontype, $this->masterfields);
        $this->ShowListViewTableFooter($state, $permissiontype);
    }

    function ShowFormView($state='edit', $permissiontype=SELECT, $detail=false, $masterrecord=false) {
        global $polaris;

        echo '<div class="element-detail-box view-detail-box">';

        $_formview = $this->GetFormView();
        $_isWizard = ($_formview == 'WIZARDVIEW' and $state == 'insert');

        if ($detail) {
            $_recordid = $_GET['detailrec'];
            $_query = $this->MakeDetailSQL(edit:true, masterrecord:$masterrecord, countselect:false, mastertable:$this->owner->masterform->GetDataDestination(true));
        } else {
            $_recordid = $_GET['rec'];
            $_query = $this->MakeSQL($edit = true);
        }

        $this->rsdata = $this->database->userdb->SelectLimit($_query, 1, 0);
        if ($this->rsdata) {
            $_rec = $this->rsdata->FetchNextObject(true);
            if (!$this->detail and $this->owner->detailform) {
                $_SESSION['currentmasterrecord'] = serialize($_rec);
            } else {
                // Detailformview in a masterdetail construct?
                $currentmasterrecord = unserialize($_SESSION['currentmasterrecord']);
            }
        } else {
            echo '<div id="debugmessage">SQL Error in ' . $_query . '<br /> ' . $this->database->userdb->ErrorMsg() . '<br /></div>';
        }

        $_pinvalues = $this->GetAllPinColumns();
        $_wizardClass = $_isWizard ? 'wizard' : '';

        $this->pages->ShowGenericPage($this->database->userdb, $_rec, $_recordid, $state, $permissiontype, $masterrecord, $_pinvalues);

        echo "<div class=\"tabs-container {$_wizardClass}\">";
        $this->pages->ShowPageTabs($_isWizard);
        $this->pages->ShowFormViewData($this->database->userdb, $_rec, $_recordid, $state, $permissiontype, $masterrecord, $_pinvalues);
        echo '</div>';
        echo '</div>';
    }

    function ShowDetailsView($state='edit', $permissiontype=SELECT, $detail=false, $masterrecord=false) {
        global $polaris;

        if ($state == 'insert') {
            $this->ShowFormView($state, $permissiontype, $detail, $masterrecord);
        } elseif ((($state == 'edit' or $state == 'view') and $detail) or $this->owner->detailaction == 'detail_edit') {
            if (!isset($polaris->currentdetailform->record->METANAME)) {
                $_overviewCaption = 'Overzicht';
                $_overviewSubCaption = 'Algemene info';
            } else {
                $_overviewCaption = $polaris->currentdetailform->ItemsName();
                if ($this->owner->detailaction == 'detail_allitems') {
                    $_overviewSubCaption = lang_get('view_details').' '.$polaris->currentdetailform->ItemsName();
                } else {
                    $_overviewSubCaption = lang_get('add_an_item', ucfirst($polaris->currentdetailform->ItemName()));
                }
                $polaris->tpl->assign('detailbasehref', $polaris->currentdetailform->MakeURL(false, $polaris->currentdetailform->record->METANAME));
                $polaris->tpl->assign('addanitemlabel', lang_get('add_item', $polaris->currentdetailform->ItemName() ));
                $detailpermission = $polaris->currentdetailform->GetPermissionType();
                $polaris->tpl->assign('detailpermission', $detailpermission);
                $polaris->tpl->assign('hasinsertpermission', ($detailpermission & INSERT) == INSERT);
            }

            $polaris->tpl->assign('currentdetailaction', $this->owner->detailaction);
            $polaris->tpl->assign('currentdetailformname', $polaris->currentdetailform->record->METANAME);
            $polaris->tpl->assign('overviewcaption', $_overviewCaption);
            $polaris->tpl->assign('overviewsubcaption', $_overviewSubCaption);
            $polaris->tpl->assign('detailformobject', $polaris->currentdetailform);
            $polaris->tpl->assign('masterrecord', $masterrecord);
            $polaris->tpl->assign('permissiontype', $permissiontype);
            $qvalue = $_GET['qd'][0];
            $qdetail = $this->ProcessSearchValue($qvalue);
            $polaris->tpl->assign('detailsearchvalue', $qdetail);
            $polaris->tpl->display('detailsview.tpl.php');
        } else {
            $this->ShowListView($state, $permissiontype);
        }
    }

    function ShowRSS($rss_url , $params ) {
      $paramlist = extractParams($params);
      if (!$paramlist['count'] ) $paramlist['count'] = 5;
      echo showRSSFeed($rss_url, $paramlist['count'], false);
    }

    function DisplayDetailTabContent($state, $permissiontype, $masterrecord) {
        global $polaris;

        // $_recordid = $_GET['detailrec'];
        // $masterrecord = $this->masterrecord;
        $_query = $this->MakeDetailSQL(edit:false, masterrecord:$masterrecord, countselect:false, mastertable:$this->owner->masterform->GetDataDestination(true));
        $this->rsdata = $this->database->userdb->SelectLimit($_query, 1, 0);
        if ($this->rsdata) {
            $_rec = $this->rsdata->FetchNextObject(true);
            if (!$this->detail and $this->owner->detailform) {
                $_SESSION['currentmasterrecord'] = serialize($_rec);
            } else {
                // Detailformview in a masterdetail construct?
                $currentmasterrecord = unserialize($_SESSION['currentmasterrecord']);
            }
        } else {
            echo '<div id="debugmessage">SQL Error in ' . $_query . '<br /> ' . $this->database->userdb->ErrorMsg() . '<br /></div>';
        }

        echo "<div class='pagecontents'>";
        $this->Show($state, $this->record->METANAME);
        echo "<br /></div>";
    }

    function resourcesInUse() {
        $this->resourcesinuse = 0;
        if (!isset($this->resourcesinuse) and isset($this->database)) {
            $sql = $this->MakeSQL(edit:false, countselect:true, customfilter:false, skipallextrafiltering:true);
            $this->resourcesinuse = $this->database->userdb->GetOne($sql);
        }
        return $this->resourcesinuse;
    }

    function clientTableResourceLimit($tableName=false) {
        if ($tableName == false)
            $tableName = $this->GetDataDestination();
        $maxValue = $_SESSION['clientparameters']["TABLE.{$tableName}"];
        $result = is_numeric($maxValue) ? intval($maxValue) : 0;
        return $result;
    }

    function clientWithinLimitForTableResources($state, $showmessage=false) {
        global $TABLEPREFIX;

        $result = true;
        if (($this->resourcesInUse() > $this->clientTableResourceLimit()) and $this->clientTableResourceLimit() !== 0) {
            $result = false;
        }
        return $result;
    }

    function &GetModule($permissiontype, $state, $module, $moduleid, $moduleparam, $masterrecord = false) {
        global $_GVARS;
        global $polaris;

        if (!isset($this->moduleobj)) {
            $_clientObject =& $this->AppObject()->ClientObject();
            $this->moduleobj =& $_clientObject->GetModule($module, $moduleid, $moduleparam);
            if (isset($this->moduleobj)) {
                $this->moduleobj->form =& $this;
                $this->moduleobj->permission = $permissiontype;
                $this->moduleobj->state = $state;
                $this->moduleobj->basehref = $this->basehref;
                $this->moduleobj->userdatabase =& $this->database->userdb;
                $this->moduleobj->databaseid = $this->databasehash;
                $this->moduleobj->masterrecord =& $masterrecord;
            } else {
                echo "<p>De module '{$this->record->MODULE}' is niet bekend.</p>";
            }
        }
        return $this->moduleobj;
    }

    function ShowModule($permissiontype, $state, $module, $moduleid, $moduleparam, $rec=false, $masterrecord=false) {
        $_moduleobj =& $this->GetModule($permissiontype, $state, $module, $moduleid, $moduleparam, $masterrecord);
        if ($_moduleobj) {
            echo $_moduleobj->Show($this->outputformat, $rec);
        }
    }

    function ShowTagBar($type, $detail) {
        global $_GVARS, $_CONFIG;

        $plr = $this->polaris();
        if ($_CONFIG['includetags'] == 'true') {
            if ($type == 'searchresult') {
                $keyvalue = $_GET['q'];
            } else {
                if ($detail) {
                    $keyvalue = $_GET['detailrec'];
                    $sql = $this->MakeDetailSQL($edit = true, $masterrecord);
                } else {
                    $keyvalue = $_GET['rec'];
                    $sql = $this->MakeSQL($edit = true);
                }
                /***
                    Find the Caption of the record that is being tagged
                */
                if ($sql) {
                    $this->database->connectUserDB();
                    $this->rs = $this->database->userdb->SelectLimit($sql, 1, 0) or Die('SQL Error in '.$query.'<br /> '.$this->database->userdb->ErrorMsg());
                    $rec = $this->rs->FetchNextObject(true);

                    if ($rec and isset($this->owner->masterform) and $this->record->CAPTIONCOLUMNNAME != '') {
                        $captioncols = explode(';',$this->record->CAPTIONCOLUMNNAME);
                        foreach($captioncols as $captioncol) {
                            $reccaption .= $rec->$captioncol . ' - ';
                        }
                        $reccaption = substr($reccaption,0,-3);
                    }
                    $plr->tpl->assign('recordcaption', $reccaption);
                }
            }

            require_once('includes/func_tags.inc.php');
            $appobj =& $this->AppObject();
            $plr->tpl->assign('tags', $this->getTags($this->record->CLIENTID, $_SESSION['userid'], $appobj->record->METANAME, $this->owner->record->METANAME, $this->record->METANAME, $type, $keyvalue));

            echo $plr->tpl->fetch('tagbar.tpl.php');
        }
    }

    function getTags($userid, $type, $keyvalue) {
        global $_sqlAllRecordTags, $_sqlAllSearchResultTags;

        if ($type == 'record') {
            $sql = $_sqlAllRecordTags;
        } else {
            $sql = $_sqlAllSearchResultTags;
        }

        $appobj =& $this->AppObject();
        $tagrs = $this->dbGetAll($sql
        , array($this->record->CLIENTID, $userid, $appobj->record->METANAME, $this->owner->record->METANAME, $this->record->METANAME, $keyvalue)
        );
        foreach($tagrs as $key => $tag) {
            $tagrs[$key]['tagtarget'] = 'app/'.$tag['app'].'/const/'.$tag['const'].'/edit/'.$tag['record'].'/';
        }

        return $tagrs;
    }

    function GetQuickAddScripts() {
        global $_GVARS;
        $html = '<script type="text/javascript" src="javascript/validation/wforms.js"></script>';
        $html .= '<script type="text/javascript" src="javascript/validation/localization-'.strtolower($_GVARS['lang']).'.js"></script>';
        return $html;
    }

    function GetQuickAddBox() {
        $pages =& $this->pages->pages;
        for($i = 0; $i < count($pages); $i++)
            $pages[$i]->LoadRecord();

        $html = $this->GetQuickAddScripts();
        $html .= "<tbody id=\"quickinsert\" style=\"display:none\"><tr>";
        foreach ( array_keys($pages) as $index ) {
            $page =& $pages[$index];

            foreach ( array_keys($page->lines->lines) as $lineindex ) {
                $line =& $page->lines->lines[$lineindex];

                if ( ($line->record->LINETYPE == 'COLUMN' or $line->record->LINETYPE == 'COMPONENT')
                            and $line->record->COLUMNVISIBLE == 'Y'
                         and !isset($_SESSION[$this->owner->record->CONSTRUCTID.'.'.GROUP_PREFIX.$line->record->COLUMNNAME])
                         and ( !$masterfields or ($masterfields and !in_array($line->record->COLUMNNAME, $masterfields)))
                ) {
                    $gui = $line->GetGuiElement($this->database->userdb, $inlistview = true, $recordid, $state='insert', $originalfieldvalue='', $line->record->LINEREQUIRED == 'Y' or $line->record->REQUIRED == 'Y', $line->record->READONLY == 'Y', $this->permission=254);
                    if ($gui != '')
                        $html .= "<td>$gui</td>";
                    $fldidx++;
                }
            }
        }
        $html .= "<td></td>";
        $html .= '</tr>';
        $html .= '<tr style=""><td colspan="0" style="font-size:0.9em"> <input type="submit" onclick="this.form._hdnAction.value=\'save\';" value="Opslaan" /> <input type="button" value="Annuleren" onclick="javascript:quickEntryToggle(false)" /></td></tr>';
        $html .= '</tbody>';
        return $html;
    }

    function ShowQuickAddBox() {
        echo $this->GetQuickAddBox();
    }

    function ShowSearchForm($state, $permissiontype, $detail, $masterrecord) {
        if ($this->owner->IsMaximized())
            echo '<input type="hidden" name="maximize" value="true">';
        if ($detail) {
            echo '<input type="hidden" name="rec" value="'.$_GET['rec'].'">';
            echo '<input type="hidden" name="donly" value="true">';
            echo '<input type="hidden" name="action" value="edit">';
        }
        $searchvalues = array('');
        if (is_array($_GET['qc']) and is_array($_GET['qv']))
            $searchvalues = array_combine($_GET['qc'], $_GET['qv']);

        $fieldcount = 0;
        if (!empty($this->searchcolumns->columns)) {
            $fieldcount = count($this->searchcolumns->columns);
            $searchvalues = $this->searchcolumns->Show($searchvalues);
        }

        $allcolumns = $this->pages->GetSearchFormFields();
        // var_dump($allcolumns);
        // exit();
        $allcolumns_count = is_array($allcolumns) ?? 0;
        if ($allcolumns_count != $fieldcount) {
            $fieldcount = $allcolumns_count;

            if (empty($this->searchcolumns->columns)) {
                echo '<p class="search">Specifieke kolom </p>';
            }

            foreach($searchvalues as $searchcolumn => $searchvalue) {
                if ($searchcolumn !== '') {
                    $options = '';
                    $selectedvalue = '';
                    foreach($allcolumns as $c) {
                        $selected = ($c[0] === $searchcolumn && $searchvalue !== '')?'selected="selected"':'';
                        if ($selected) {
                            $selectedvalue = $searchvalue;
                            /* Remove the processing characters */
                            //$selectedvalue = str_replace(array("'","%"), "", $selectedvalue);
                            $selectedvalue = str_replace(array("'"), "", $selectedvalue);
                        }
                        $_labelfield = $this->i18n($c[1]);
                        $options .= "<option value=\"{$c[0]}\" $selected>{$_labelfield}</option>";
                    }
                    $label = "<select class=\"searchallcolumns_column\" ";
                    if ($selectedvalue) // make sure changing the selectvalue does not create another selectbox
                        $label .= "data-done=\"true\" ";
                    $label .= " name='qc[]'><option value=\"\">...</option>";
                    $label .= $options ."</select>";
                    echo '<div class="row form-group form-group-sm search-generic">';
                    echo "<label class=\"control-label col-xs-7\">".$label."</label>";
                    echo '<div class="col-sm-5">';
                    echo "<input type=\"search\" class=\"form-control searchallcolumns_value\" size=\"10\" name=\"qv[]\" value=\"$selectedvalue\" />";
                    echo "</div>";
                    echo "</div>";
                }
            }
        }

        return $fieldcount;
    }

    function ShowHorizontalSearchForm($state, $permissiontype, $detail, $masterrecord) {
        if ($this->owner->IsMaximized())
            echo '<input type="hidden" name="maximize" value="true">';
        if ($detail) {
            echo '<input type="hidden" name="rec" value="'.$_GET['rec'].'">';
            echo '<input type="hidden" name="donly" value="true">';
            echo '<input type="hidden" name="action" value="edit">';
        }
        $searchvalues = array('');

        if (is_array($_GET['qc']) and is_array($_GET['qv']) and count($_GET['qc']) === count($_GET['qv']))
            $searchvalues = array_combine($_GET['qc'], $_GET['qv']);

        $fieldcount = 0;
        if (!empty($this->searchcolumns->columns)) {
            $fieldcount = count($this->searchcolumns->columns);
            $searchvalues = $this->searchcolumns->ShowHorizontal($searchvalues);
        }

        return $fieldcount;
    }

    function PostToGet($array) {
      if (is_array($array) ) {
        foreach($array as $key => $value) {
          $r .= $key .'='. urlencode($value).'&amp;';
        }
      }
      else
        $r = '';
      return $r;
    }

    function ShowCustomForm($URL) {
        global $_GVARS;

        if ($URL == "") return false;
        ## encode the calling php file and add it to the query
        if (substr(strtoupper($URL), 0, 7) == 'HTTP://' or substr(strtoupper($URL), 0, 8) == 'HTTPS://') {
            $parts = parse_url($URL);
            $query = '?';
            if (isset($parts['query']))
                $query = '?'.$parts['query'].'&amp;';
            $query .= 'url='.urlencode($_SERVER['PHP_SELF'].'?'.$_SERVER['QUERY_STRING']);

            ## add the action var to the query
            if (isset($_GET['action']) )
                $query .= '&amp;action='.$_GET['action'];
            ## add the Post vars to the query by translating the POST vars to GET vars
            $query .= '&amp;'.$this->PostToGet($_POST);
        }
        ## call the external form with the complete url (script + query)
        $URLComplete = $parts['scheme'].'://'.$parts['host'].($parts['port']?':'.$parts['port']:'').$parts['path'];
        if (isset($query))
            $URLComplete .= $query;
        if (isset($parts['fragment']))
            $URLComplete .= '#'.$parts['fragment'];

        if ($this->record->SHOWINFRAME == 'Y') {
            $autosize = 'autosize="true"';
            echo "<iframe target='_top' onload='Polaris.Window.resizeCustomForm(this)' class=\"customview\" id=\"customform_iframe\" $autosize src=\"$URLComplete\" frameborder=\"no\"></iframe>";
        } else {
            $_params = explode(':', $this->record->VIEWTYPEPARAMS);
            $_magnifierClass = $_params[0] == "MAGNIFIER" ? 'magnifier' : '';
            $_isMagnifierSize = $_params[1] ?? '300';
            $_isMagnifierScale = $_params[2] ?? '2';
            echo "<img id='{$this->record->METANAME}_img' class='custom-form-img' data-toggle='{$_magnifierClass}' data-magnifier_size='{$_isMagnifierSize}' data-magnifier_scale='{$_isMagnifierScale}' src='{$URLComplete}' style='height:87vh' />";
        }
    }

    function GetRecordCount($permissiontype='', $state='', $persistentcount=false) {
        global $_CONFIG;

        $result = false;

        // cache the recordcount so we don't execute too much queries.
        if (isset($this->recordcount)) {
            return $this->recordcount;
        }

        if (!$persistentcount and ($this->HasOption('skip-recordcount') or !$this->ShowInitialDataset($state))) {
            $result = -1;
        } else {
            if (isset($this->database->userdb) and $this->database->userdb->IsConnected() ) {
              // get custom filter from module
                if ($this->record->MODULE) {
                    $module =& $this->GetModule($permissiontype, $state, $this->record->MODULE, $this->record->MODULEID, $this->record->MODULEPARAM);
                    if ($module) {
                        $customfilter = $module->__GetCustomFilter();
                    }
                }

                // execute SQL to determine recordcount
                if ( isset($this->detail) and $this->detail ) {
                    $theSQL = $this->MakeDetailSQL($edit = false, $this->GetMasterRecord(), $countselect = true, $mastertable=false, $customfilter);
                } else {
                    $theSQL = $this->MakeSQL($edit = false, $countselect = true, $thesearchvalue = false, $masterfields = false, $countdetails = false, $detailtable = false, $customfilter);
                }
                if ($theSQL) {
                    $po =& $this->polaris();
                    $po->timer->startTimer('recordcount');

                    $this->database->userdb->debug = $_GET['debuguserdb'];
                    try {
                        $countrs = $this->database->userdb->execute( $theSQL );
                    } catch (exception $E) {
                        if ($_CONFIG['debug']) {
                            echo "<div id=\"errormessagex\" class=\"error\" style=\"-khtml-user-select:auto\">{$E->getMessage()}<br /><span class=\"original\">({$E->getMessage()}: ".$E->getCode().")</span>";
                            echo "<br /><br />".$theSQL;
                            echo "</div>";
                        } else {
                            echo "<div id=\"errormessagex\" class=\"error\" style=\"-khtml-user-select:auto\">An error occured during the execution of the SQL statement.</div>";
                        }
                    }
                    $po->timer->endTimer('recordcount');

                    if ($countrs) {
                        if (
                            (isset($countrs->fields['RECORDCOUNT'])
                                or isset($countrs->fields['recordcount'])
                            )
                            and strpos(strtoupper($theSQL), 'GROUP BY') === false
                        ) {
                            $result = $countrs->fields['RECORDCOUNT'];
                        } else {
                            $result = $countrs->RecordCount();
                            if ($result == -1) {
                                $result = count($countrs->GetAll());
                            }
                        }
                    }
               }
            }
        }
        $this->recordcount = $result;
        return $result;
    }

    function ShowPagination($currentposition, $limit, $total_pages) {
        // How many adjacent pages should be shown on each side?
        $adjacents = 3;

        // Visible maximum of pages
        $max = 1;

        $page = $currentposition;
        if($page)
            $start = ($page - 1) * $limit;          //first item to display on this page
        else
            $start = 1;                             //if no page var is given, set start to 1

        /* Setup page vars for display. */
        if ($page == 0) $page = 1;                  //if no page var is given, default to 1.
        $prev = $page - 1;                          //previous page is page - 1
        $next = $page + 1;                          //next page is page + 1
        $lastpage = ceil($total_pages/$limit);//lastpage is = total pages / items per page, rounded up.
        $lpm1 = $lastpage - 1;                      //last page minus 1


        $targetpage = $this->MakeURLQuery( array('q','qc','qtable','sort','dir'), $this->detail );

        $prev = add_query_arg( $basehref, 'offset', $currentposition - $limit );
        $next = add_query_arg( $basehref, 'offset', $currentposition + $limit );

        /*
            Now we apply our rules and draw the pagination object.
            We're actually saving the code to a variable in case we want to draw it more than once.
        */
        $pagination = "";
        if($lastpage > 1)
        {
            //previous button
            if ($page > 1)
                $pagination.= "<a href=\"$prev\"><< vorige</a>";
            else
                $pagination.= "<span class=\"dis abled\"><< previous</span>";

            //pages
            if ($lastpage < 7 + ($adjacents * 2))   //not enough pages to bother breaking it up
            {
                for ($counter = 1; $counter <= $lastpage; $counter++)
                {
                    $offset = ($counter - 1) * $limit + 1;
                    if ($counter == $page)
                        $pagination.= "<span class=\"current\">$counter</span>";
                    else
                        $pagination.= "<a href=\"$targetpage?offset=$offset\">$counter</a>";
                }
            }
            elseif($lastpage > 5 + ($adjacents * 2))    //enough pages to hide some
            {
                //close to beginning; only hide later pages
                if($page < 1 + ($adjacents * 2))
                {
                    for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
                    {
                        $offset = ($counter - 1) * $limit + 1;
                        if ($counter == $page)
                            $pagination.= "<span class=\"current\">$counter</span>";
                        else
                            $pagination.= "<a href=\"$targetpage?offset=$offset\">$counter</a>";
                    }
                    $pagination.= "...";
                    $pagination.= "<a href=\"$targetpage?offset=$lpm1\">$lpm1</a>";
                    $pagination.= "<a href=\"$targetpage?offset=$lastpage\">$lastpage</a>";
                }
                //in middle; hide some front and some back
                elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
                {
                    $pagination.= "<a href=\"$targetpage?page=1\">1</a>";
                    $pagination.= "<a href=\"$targetpage?page=2\">2</a>";
                    $pagination.= "...";
                    for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
                    {
                        $offset = ($counter - 1) * $limit + 1;
                        if ($counter == $page)
                            $pagination.= "<span class=\"current\">$counter</span>";
                        else
                            $pagination.= "<a href=\"$targetpage?offset=$offset\">$counter</a>";
                    }
                    $pagination.= "...";
                    $pagination.= "<a href=\"$targetpage?offset=$lpm1\">$lpm1</a>";
                    $pagination.= "<a href=\"$targetpage?offset=$lastpage\">$lastpage</a>";
                }
                //close to end; only hide early pages
                else
                {
                    $pagination.= "<a href=\"$targetpage\">1</a>";
                    $pagination.= "<a href=\"$targetpage?offset=21\">2</a>";
                    $pagination.= "...";
                    for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
                    {
                        $offset = ($counter - 1) * $limit + 1;
                        if ($counter == $page)
                            $pagination.= "<span class=\"current\">$counter</span>";
                        else
                            $pagination.= "<a href=\"$targetpage?offset=$offset\">$counter</a>";
                    }
                }
            }

            //next button
            if ($page < $counter - 1)
                $pagination.= "<a href=\"$targetpage?offset=$next\">next >></a>";
            else
                $pagination.= "<span class=\"dis abled\">next >></span>";
        }
        return $pagination;
    }

    function DetermineLimits() {
        $this->database->connectUserDB();

        $this->limit = $this->GetLimit();
        $this->GetRecordCount($permissiontype, $state);
        if (isset($_GET['offset']) and $_GET['offset'] != '' and (!$this->detail or ($this->detail and $GLOBALS['detailonly'])) )
            $this->startno = intval($_GET['offset']);
        else
            $this->startno = 1;
    }

    function ShowNavigator($textonly = false, $permissiontype, $state) {
        global $_CONFIG;
        global $polaris;

        if ($polaris->currentdetailform) {
            $recordcount = $polaris->currentdetailform->recordcount;
        } else {
            $recordcount = $this->recordcount;
        }

        $sep = '&nbsp;';
        $shownavigator = ($this->HasOption('skip-recordcount')
            or $this->ShowInitialDataset($state)
            or ($recordcount == -1))
            or in_array($state, ['list']);

            // only show the navigator when there are more items available than can be shown
        if (($this->limit and ($recordcount > $this->limit)) or $shownavigator) {

            $basehref = $this->MakeURLQuery( array('q','qc','qtable','sort','dir'), $this->detail );
            if ($this->startno > 1) {
                $first = add_query_arg( $basehref, 'offset', 1 );
                $prev  = add_query_arg( $basehref, 'offset', max($this->startno - $this->limit, 1) );
                $disabled = '';
            } else {
                $first = 'javascript:void()';
                $prev = 'javascript:void()';
                $disabled = 'pagination-disabled';
            }
            $showasbuttons = ($_CONFIG['navigator_button_style'] and !$textonly);
            if ($showasbuttons)
                $extraclass = 'navbuttons';

            $result = '<ul class="pagination" style="margin:0;padding:0">';
            if ($showasbuttons) {
//                $result .= '<a class="first '.$disabled.'" href="'.$first.'" title="'.lang_get('navigate_first_hint').'"><span>'.lang_get('navigate_first_hint').'</span></a>';
//                $result .= '<a class="prev '.$disabled.'" href="'.$prev.'" title="'.lang_get('navigate_prev_hint').'"><span>'.lang_get('navigate_prev_hint').'</span></a>';
                $result .= '<li><a href="'.$first.'" class="'.$disabled.'" title="'.lang_get('navigate_first_hint').'"><i class="material-symbols-filled md-1x">skip_previous</i></a></li>';
                $result .= '<li><a href="'.$prev.'" class="'.$disabled.'" title="'.lang_get('navigate_prev_hint').'"><i class="material-symbols-filled md-1x">fast_rewind</i></a></li>';
            } else {
              $result .= '[&nbsp;';
              if ($disabled == '') $result .= '<a href="'.$first.'">';
              $result .= lang_get('navigate_first');
              if ($disabled == '') $result .= '</a>'.$sep.'<a href="'.$prev.'">';
              else $result .= $sep;
              $result .= lang_get('navigate_prev');
              if ($disabled == '') $result .= '</a>';
              $result .= $sep;
            }

            if ($this->startno==1) {
                if (($shownavigator and ($this->limit <= $recordcount)) or $recordcount == -1) {
                    $nextdisabled = '';
                    $lastdisabled = '';
                    $next = add_query_arg( $basehref, 'offset', $this->startno + $this->limit );
                    $last  = add_query_arg( $basehref, 'offset', $recordcount - ($this->limit - 1) );
                } else {
                    $nextdisabled = 'pagination-disabled';
                    $lastdisabled = 'pagination-disabled';
                    $next = 'javascript:void(0)';
                    $last  = 'javascript:void(0)';
                }
            } elseif (($this->startno + $this->limit) <= $recordcount) {
                $next = add_query_arg( $basehref, 'offset', $this->startno + $this->limit );
                $last  = add_query_arg( $basehref, 'offset', $recordcount - ($this->limit - 1) );
                $nextdisabled = $lastdisabled = '';
            } else {
                $next = 'javascript:void(0)';
                $last = 'javascript:void(0)';
                $nextdisabled = $lastdisabled = 'pagination-disabled';
            }

            $result .= '<li><a href="javascript:void()">'.$this->ShowRecordCount().'</a></li>';

            if ($showasbuttons) {
                $result .= '<li><a href="'.$next.'" class="'.$nextdisabled.'" title="'.lang_get('navigate_next_hint').'"><i class="material-symbols-filled md-1x">fast_forward</i></a></li>';
                $result .= '<li><a class="last '.$lastdisabled.'" href="'.$last.'" title="'.lang_get('navigate_last_hint').'"><i class="material-symbols-filled md-1x">skip_next</i></a></li>';
            } else {
                if ($disabled == '') $result .= '<li><a href="'.$next.'">';
                $result .= lang_get('navigate_next');
                if ($disabled == '') $result .= '</a>'.$sep.'<a href="'.$last.'">';
                else $result .= $sep;
                $result .= lang_get('navigate_last');
                if ($disabled == '') $result .= '</a></li>';
                $result .= '&nbsp;]';
            }
            /*
            //$gotopage = $basehref.'&amp;offset=\''+$("pageselect").value+'\'';
            $result .= ' '.lang_get('page').': <select onchange="javascript:window.location.href=\''.$gotopage.'\';return false;">';
            $pageindex = 1;
            for ($i=0;$i<$this->recordcount;$i=$i+20) {
            $result .= "<option value='$i'>$pageindex</option>";
            $pageindex++;
            }
            $result .= '</select>';
            */
            $result .= '</ul>';

            echo $result;
        }

// NIET MEER NODIG? Wordt nu gedaan met recordcount = -1        if ($this->HasOption('skip-recordcount'))
//            $this->recordcount = 2147483647; // max int value

    }

    public function ProcessReplyRules($emailaddress, $insertValues) {
        global $polaris;

        return $polaris->ProcessReplyRules($this->record->RECORDID, $emailaddress, $insertValues);
    }

}
