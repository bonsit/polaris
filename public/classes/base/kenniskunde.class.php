<?php
# Polaris - a weblapplication generator

# Copyright (C) 2000 - 2008  Debster - Bart Bons

# This is the 'KennisKunde' module to create IGD.

# this module is accessable as a php class
# and through query parameters (via plrkk.php):
#   plrkk.php?show=schema&db=<...>
#   >> result: Kenniskunde schema

require_once('classes/base/plr_base.class.php');

class plrKennisKunde extends plrRecord {

    function __construct($aowner, $key = false) {
        parent::__construct($aowner, $key);
    }

    function GetSchema($databasehashid, $showentities=false, $skipprefix=false) {
        global $_sqlTables, $_sqlColumns, $_sqlKeyColumns;

        $schema = array();
        $rs = $this->plrInstance()->Execute( $_sqlTables, array($databasehashid) );

        if (!$rs) Die('No database found: '.$databasehashid);
        $i = 0;
        while ( $rec = $rs->FetchRow(true) ) {
            $rec['SENTENCEPATTERN'] = htmlentities(addslashes ($rec['SENTENCEPATTERN']));
            $rec['SENTENCEPATTERN'] = str_replace( "\n\r", '<br />', $rec['SENTENCEPATTERN'] );
            $rec['SENTENCEPATTERN'] = str_replace( "\r", '', $rec['SENTENCEPATTERN'] );
            $rec['SENTENCEPATTERN'] = str_replace( "\n", '', $rec['SENTENCEPATTERN'] );
            $rec['SENTENCEPATTERN'] = preg_replace("/(&lt;)(.*?)(&gt;)/","<b>\\0</b>", $rec['SENTENCEPATTERN']);

            unset( $rec[9] ); // remove the sentencepattern in the other key (9)

            $realTableName = $rec['TABLENAME'];
            $rec['TABLENAME'] = str_replace('.','_',$rec['TABLENAME']);
            $rec['FTDNAME'] = $rec['TABLENAME'];
            if ( $skipprefix ) $rec['FTDNAME'] = str_replace( $skipprefix, '', $rec['FTDNAME'] );
            $schema[$i] = $rec;

            if ($showentities)
                $sql = $_sqlKeyColumns;
            else
                $sql = $_sqlColumns;

            $this->plrInstance()->SetFetchMode(ADODB_FETCH_ASSOC);
            $rsc = $this->plrInstance()->Execute( $sql, array($databasehashid, $realTableName) );
            while ( $col = $rsc->FetchRow(true) ) {
                $col['DEFAULTVALUE'] = trim($col['DEFAULTVALUE']);
                $schema[$i]['columns'][] = $col;
            }

            $i++;
        }
        return $schema;
    }
}
