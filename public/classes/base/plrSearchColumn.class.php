<?php

class base_plrSearchColumn extends base_plrRecord {

    function __construct(&$aowner) {
        parent::__construct($aowner);
    }

    function LoadRecord() {
    }

    function Show($searchvalues) {
        global $_CONFIG;

        $_record = &$this->record;

        $columnname = $_record->COLUMNNAME;
        if (($_record->SUPERSHOWCOLUMNNAME != '') and ($_record->SUPERSHOWCOLUMNNAME != strtoupper($_record->SUPERCOLUMNNAME))) {
// NOG UITZOEKEN (CURSUS->CURSUSGROEP            $columnname .= ','.$_record->SUPERSHOWCOLUMNNAME;
        }
        if (($_record->SUPERSEARCHCOLUMN != '')) {
            $columnname = $_record->SUPERSEARCHCOLUMN;
        }
        $inputtype = 'search';
        $maxlengthcode = '100';
//            $label = ProcessSQL($userdb, $_record->LINEDESCRIPTION);
        $_label = $this->i18n($_record->LINEDESCRIPTION_SHORT?$_record->LINEDESCRIPTION_SHORT:$_record->FULLNAME);

        echo '<div class="row form-group form-group-sm">';
        echo "<label class=\"control-label col-xs-7\" for=\"_fld$_record->COLUMNNAME\">".$_label."</label>";
//        echo "<div class=\"xformcolumn\">";
        echo "<input type='hidden' name='qc[]' value='$columnname' />";
        if (($_record->SUPERSEARCHCOLUMN != '')) {
//            echo "<input type='hidden' name='qc[]' value='{$_record->SUPERSEARCHCOLUMN}' />";
        }
        $testtablename = $_record->TABLENAME;
        if ($this->owner->owner->database->record->DATABASENAME != '')
            $testtablename = $this->owner->owner->database->record->DATABASENAME.'.'.$testtablename;
        $tablename = ($testtablename != $this->owner->owner->GetDataDestination())?$_record->TABLENAME:'';
        echo "<input type='hidden' name='qtable[]' value='{$tablename}' />";

        // why only if no pins are active?
        // if (!$this->owner->owner->FormHasActivePins()) {
        // }
        /* Remove the processing quotes */
        $_value = str_replace(array("'","%"), "", $searchvalues[$columnname]);
        if ($_value != '') unset($searchvalues[$columnname]);

        $mask = '';
        if ($_record->MASK != '') {
            switch ($_record->MASK) {
            case 'UPPER':
                $maskclass = 'validation_uppercase';
                break;
            default:
                $mask = $_record->MASK;
            }
        }
        if ($_record->SEARCHVALUEPROCESSOR != '') {
            switch ($_record->SEARCHVALUEPROCESSOR) {
            case 'UPPER':
                $maskclass = 'validation_uppercase';
                break;
            default:
                $mask = $_record->MASK;
            }
        }
        if ($_record->DATATYPE == 'D')
            $placeholder = "placeholder='".$_CONFIG['date_format_placeholder']."'";

        echo '<div class="col-sm-5">';
        echo "<input type='$inputtype' autocomplete='off' label='$_label' $placeholder alt='$mask' class='form-control $maskclass' rel='{$_record->PROCESSING}' id='_fld$_record->COLUMNNAME' name='qv[]' maxlength='$maxlengthcode' value='$_value' />";
        echo '</div>';
        //echo "</div>";
        /*
        // if the COLUMNNAME is not a proper columnname (ie. CONCAT(...,...))
        // then don't show the name columnname
        if (ereg('^[a-zA-Z0-9_]{1,}$', $record->COLUMNNAME)) {
        }
        */
        echo '</div>';

        return $searchvalues;
    }

    function ShowHorizontal($searchvalues) {
        global $_CONFIG;

        $_record = &$this->record;

        $columnname = $_record->COLUMNNAME;
        if (($_record->SUPERSHOWCOLUMNNAME != '') and ($_record->SUPERSHOWCOLUMNNAME != strtoupper($_record->SUPERCOLUMNNAME))) {
// NOG UITZOEKEN (CURSUS->CURSUSGROEP            $columnname .= ','.$_record->SUPERSHOWCOLUMNNAME;
        }
        if (($_record->SUPERSEARCHCOLUMN != '')) {
            $columnname = $_record->SUPERSEARCHCOLUMN;
        }
        $inputtype = 'search';
        $maxlengthcode = '100';
//            $label = ProcessSQL($userdb, $_record->LINEDESCRIPTION);
        $_label = $this->i18n($_record->LINEDESCRIPTION_SHORT?$_record->LINEDESCRIPTION_SHORT:$_record->FULLNAME);
        $testtablename = $_record->TABLENAME;
        if ($this->owner->owner->database->record->DATABASENAME != '')
            $testtablename = $this->owner->owner->database->record->DATABASENAME.'.'.$testtablename;
        $tablename = ($testtablename != $this->owner->owner->GetDataDestination())?$_record->TABLENAME:'';
        $_value = str_replace(array("'","%"), "", $searchvalues[$columnname]);
        if ($_value != '') unset($searchvalues[$columnname]);

        echo '
        <div class="btn-group">
            <input type="hidden" name="qc[]" value="'.$columnname.'" />
            <input type="hidden" name="qtable[]"" value="'.$tablename.'" />
            <button data-toggle="dropdown" type="button" class="btn btn-clean dropdown-toggle" aria-expanded="false">'.$_label.' &nbsp; <span class="fa fa-angle-down"></span></button>
            <ul class="dropdown-menu dropdown-menu-right">';

        $mask = '';
        if ($_record->MASK != '') {
            switch ($_record->MASK) {
            case 'UPPER':
                $maskclass = 'validation_uppercase';
                break;
            default:
                $mask = $_record->MASK;
            }
        }

        if ($_record->SEARCHVALUEPROCESSOR != '') {
            switch ($_record->SEARCHVALUEPROCESSOR) {
            case 'UPPER':
                $maskclass = 'validation_uppercase';
                break;
            default:
                $mask = $_record->MASK;
            }
        }
        if ($_record->DATATYPE == 'D')
            $placeholder = "placeholder='".$_CONFIG['date_format_placeholder']."'";
        $this->owner->owner->database->connectUserDB();

        if (!empty($_record->SEARCHQUERY)) {
            $_sql = $_record->SEARCHQUERY;
            if (strpos($_sql, ' LIMIT ') === 0) {
                 $_sql .= "LIMIT 0,20";
            }
        } else {
            $_sql = "SELECT DISTINCT $columnname FROM $tablename LIMIT 0,20";
        }
        $_rs = $this->owner->owner->database->userdb->GetAssoc($_sql);

        if ($_rs !== false and count($_rs) > 0) {
            if (count($_rs) > 5) {
                $_subset = array_slice($_rs, 0, 5);
                foreach($_subset as $_key => $_rec) {
                    $_checked = '';
                    if (!empty($_GET['q'])) {
                        foreach($_GET['q'] as $_qValue) {
                            if ($_qValue == $_key) {
                                $_checked = ' checked';
                            }
                        }
                    }

                    echo '<li><a href="#">
                    <input type="checkbox" name="q[]" onclick="submit()" value="'.$_key.'" '.$_checked.' /> '.$_rec.'</a></li>';
                }
                echo '<li class="divider"></li>';
            }
            echo '<li>'.AssocSetAsSelectList($_rs, $currentvalue, $id='idselect', $selectname='q[]').'</li>';
        } else {
            echo "<li><input type='$inputtype' autocomplete='off' label='$_label' $placeholder alt='$mask' class='$maskclass' rel='{$_record->PROCESSING}' id='_fld$_record->COLUMNNAME' name='qv[]' maxlength='$maxlengthcode' value='$_value' /></li>";
        }

        echo '
        </ul></div>
        ';

        /* Remove the processing quotes */
        $_value = str_replace(array("'","%"), "", $searchvalues[$columnname]);
        if ($_value != '') unset($searchvalues[$columnname]);

        return $searchvalues;

    }
}
