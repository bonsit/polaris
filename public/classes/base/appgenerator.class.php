<?php
require_once($_GVARS['docroot'].'/classes/base/plrRecord.class.php');
require_once($_GVARS['docroot'].'/classes/base/plrDatabase.class.php');

class plrAdmin extends plrRecord {
	var $databases;

	function __construct($aowner) {
		parent::__construct($aowner);
        $this->__polaris = $aowner;
		$this->databases = new plrDatabases($this);
	}

}
