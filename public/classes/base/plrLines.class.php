<?php

class base_plrLines extends base_plrRecords {
    var $lines;

    function __construct(&$aowner) {
        parent::__construct($aowner);
    }

    function LoadRecords() {
        global $_sqlLines;
        $page =& $this->owner->record;
        $rs = $this->dbExecute($_sqlLines, array($page->CLIENTID, $page->FORMID, $page->PAGEID)) or Die('query failed lines: '.$this->dbErrorMsg());
        $this->lines = array();
        while ($rec = $rs->FetchNextObject(true)) {
            $plrLine = new base_plrLine($this);
            $plrLine->record = $rec;
            $this->lines[] = $plrLine;
        }
    }

    function FindLineRecord($columnname) {
        foreach($this->lines as $line) {
            if ($line->record->COLUMNNAME == $columnname) {
                return $line;
            }
        }

    }
}
