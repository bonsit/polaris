<?php
use Coduo\PHPHumanizer\DateTimeHumanizer;

class base_plrPages extends base_plrRecords
{
    var $pages;
    var $rs;

    function __construct(&$aowner) {
        parent::__construct($aowner);

        $this->keyfields = ['CLIENTID', 'FORMID', 'PAGEID'];
        $this->LoadRecords();
    }

    function LoadRecords() {
        global $_sqlPages;

        $form =& $this->owner->record;
        $rs = $this->dbExecute($_sqlPages, [$form->CLIENTID, $form->FORMID]) or Die('query failed: ' . $this->dbErrorMsg());
        $this->pages = [];
        while ($rec = $rs->FetchNextObject(true)) {
            $plrPage = new base_plrPage($this);
            $plrPage->record = $rec;
            $this->pages[] = $plrPage;
        }
    }

    function ShowPageTabs($asWizard=false) {
        $_navType = $asWizard ? 'nav-progress' : 'nav-tabs';
        if (count($this->pages) > 1) {
            echo '<ul class="nav '.$_navType.'" role="tablist">';
            $_realIndex = 0;
            foreach (array_keys($this->pages) as $index) {
                $page =& $this->pages[$index];
                // $page->LoadRecord();
                if ($page->record->PAGETYPE == 'TAB') {
                    $pageurl = $page->GetPageTabName();
                    if (!$asWizard)
                        $_active = $_realIndex == 0 ? 'active' : '';
                    else
                        $_active = '';
                    echo '<li class="' . $_active . '"><a id="tab_' . $pageurl . '" class="nav-link" data-toggle="tab" href="#' . $pageurl . '">';
                    if (isset($page->record->IMAGEINDEX))
                        echo '<i class="material-symbols-outlined">'.$page->record->IMAGEINDEX.'</i> ';
                    if ($asWizard)
                        echo '<span class="num">'.($index+1).'</span>';
                    echo $this->i18n($page->record->PAGENAME) . '</a>';
                    echo '</li>';
                    $_realIndex++;
                }
            }
            echo '</ul>';
        }
    }

    function ShowDetailformsMenu($currentmenu) {

        if (count($this->pages) > 1) {
            $active = !isset($currentmenu) ? 'active' : '';
            $basehref = $this->owner->MakeURL(false, 'overzicht');
            echo '<ul class="nav nav-details-view" role="tablist">';
            echo "<li class=\"nav-item {$active}\">";
            echo "<a class=\"nav-link\" href=\"{$basehref}\">";
            echo 'Overzicht</a>';
            echo '</li>';

            $_realIndex = 0;
            foreach (array_keys($this->pages) as $index) {
                $page =& $this->pages[$index];
                if ($page->record->PAGETYPE == 'DETAILFORM') {
                    $basehref = $this->owner->MakeURL(vars:false, detailform:$page->record->DETAILFORM, action:'view');
                    $active = $currentmenu == $page->record->DETAILFORM ? 'active' : '';
                    echo "<li class=\"nav-item {$active}\">";
                    echo "<a class=\"nav-link\" href=\"{$basehref}\">";
                    echo $this->i18n($page->record->PAGENAME) . '</a>';
                    echo '</li>';
                    $_realIndex++;
                }
            }
            echo '</ul>';
        }
    }

    function HasGenericPages() {
        foreach ($this->pages as $_page) {
            $_page->LoadRecord();
            if ($_page->record->PAGETYPE == 'GENERIC') {
                return true;
            }
        }
        return false;
    }

    function HasDetailformPages() {
        foreach ($this->pages as $_page) {
            $_page->LoadRecord();
            if ($_page->record->PAGETYPE == 'DETAILFORM') {
                return true;
            }
        }
        return false;
    }

    function ShowGenericPage($userdb, $rec, $recordid, $state, $permissiontype, $masterrecord, $pinvalues) {

        if ($state == 'edit' or $state == 'detail_edit') {
            echo '<input type="hidden" name="_hdnRecordID" value="' . $recordid . '" />';
        }

        if ($this->HasGenericPages()) {
            echo '<div class="generic-page">';
            foreach ($this->pages as $_page) {
                $_page->LoadRecord();
                if ($_page->record->PAGETYPE == 'GENERIC') {
                    $_page->Show($userdb, $rec, $recordid, $state, $permissiontype, $masterrecord, $pinvalues);
                }
            }
            echo '</div>';
        }
    }

    function ShowFormViewData($userdb, $rec, $recordid, $state, $permissiontype, $masterrecord, $pinvalues) {
        echo '<div class="tab-content">';
        $_realIndex = 0;
        foreach (array_keys($this->pages) as $index) {
            $_page =& $this->pages[$index];
            $_page->LoadRecord();
            if ($_page->record->PAGETYPE == 'TAB') {
                if (count($this->pages) > 1 or ($state == 'edit' or $state == 'insert' or $state == 'view')) {
                    $pageClass= 'panel-body';
                } else {
                    $pageClass= '';
                }

                $pageurlid = $_page->GetPageTabName();
                $_active = $_realIndex == 0 ? 'active' : '';
                echo "<div role=\"tabpanel\" id=\"$pageurlid\" class=\"tab-pane $_active\">";
                echo "<div class=\"$pageClass\">";
                $_page->Show($userdb, $rec, $recordid, $state, $permissiontype, $masterrecord, $pinvalues);
                echo '</div>';
                echo '</div>';
                $_realIndex++;
            } else {
                // $_page->Show($userdb, $rec, $recordid, $state, $permissiontype, $masterrecord, $pinvalues);
            }
        }
        echo '</div>';
    }

    function GetOrderHref($basehref_order, $lastordernr, $sortcolumns, $columnname, $datatype, &$currentsortdir, &$sortnr)
    {
        if (isset($sortcolumns[$columnname])) {
            $currentsortdir = ($sortcolumns[$columnname]['dir'] == 'down') ? 'up' : 'down';
            $sortnr = $sortcolumns[$columnname]['nr'];
        } else {
            $currentsortdir = 'up';
            $sortnr = $lastordernr;
        }

        $orderhref = remove_query_arg($basehref_order, 'sort_' . $sortnr);
        $orderhref = remove_query_arg($orderhref, 'dir_' . $sortnr);
        $orderhref = add_query_arg($orderhref, 'sort_' . $sortnr, $columnname);
        $orderhref = add_query_arg($orderhref, 'dir_' . $sortnr, $currentsortdir);
        return $orderhref;
    }

    function GetSearchFormFields()
    {
        foreach (array_keys($this->pages) as $index) {
            $page =& $this->pages[$index];
            $page->LoadRecord();
            foreach (array_keys($page->lines->lines) as $index) {
                $line =& $page->lines->lines[$index];
                if ($line->record->LINETYPE == 'COLUMN') {
                    $result[] = $line->GetSearchField($this->owner->database->userdb);
                }
            }
        }
        if (is_array($result))
            usort($result, 'search_sort');
        return $result;
    }

    function ShowSearchFormFields($masterrecord)
    {
        if ($this->record->CAPTIONCOLUMNNAME != '') {
            $captioncols = explode(';', $this->record->CAPTIONCOLUMNNAME);
            foreach ($captioncols as $captioncol) {
                $mastercaption .= $masterrecord->$captioncol . ' - ';
            }
            echo '<span class="constructname">' . $mastercaption . '</span>';
        }
        echo '<div id="searchview" class="searchview">';
        $fieldcount = 0;
        foreach (array_keys($this->pages) as $index) {
            $page =& $this->pages[$index];
            $page->LoadRecord();
            $fieldcount = $fieldcount + $page->ShowSearchFields($this->owner->database->userdb, $rec, $recordid, $permissiontype);
        }
        echo '</div>';
        return $fieldcount;
    }

    function GetListViewHeader($userdb, $masterfields = false, $hotedit = false)
    {
        global $_CONFIG;

        $fieldnames = [];
        $fldidx = 1;
        foreach (array_keys($this->pages) as $index) {
            $page =& $this->pages[$index];
            if (!isset($page->lines))
                $page->LoadRecord();
            foreach (array_keys($page->lines->lines) as $lineindex) {
                $line =& $page->lines->lines[$lineindex];
                if (
                    (($line->record->LINETYPE == 'COLUMN' or $line->record->LINETYPE == 'COMPONENT') and $line->record->COLUMNVISIBLE == 'Y' and $line->record->VISIBLEINLIST == 'Y')
                    and (!isset($_SESSION[$this->owner->owner->record->CONSTRUCTID . '.' . GROUP_PREFIX . $line->record->COLUMNNAME]))
                ) {

                    if (!$masterfields or ($masterfields and !in_array($line->record->COLUMNNAME, $masterfields))) {

                        $align = $line->GetAlign($fldidx);

                        $validationformat = $line->GetValidationFormat();
                        $numericmask = $line->GetNumericMaskFormat($validationformat);
                        $fieldnames[$fldidx] = array(
                            'COLUMNNAME' => $line->record->COLUMNNAME
                            ,
                            'DISPLAYNAME' => str_replace(' ', '&nbsp;', ProcessSQL($userdb, $this->i18n($line->record->LINEDESCRIPTION_SHORT)))
                            ,
                            'ALIGN' => $align
                            ,
                            'NUMERICMASK' => $numericmask
                        );
                    }
                }
                $fldidx++;
            }
        }
        return $fieldnames;
    }

    function ShowListViewHeader($userdb, $permissiontype, $repeatedheader = false, $masterfields = false, $hotedit = false)
    {
        global $_CONFIG;

        $basehref = $this->owner->MakeURLQuery(array('q', 'qc', 'qtable', 'offset'), $this->owner->detail);

        $vis_highclass = $repeatedheader ? 'repeated-header' : '';
        echo "<thead class='$vis_highclass'><tr>";
        //      echo "<th class=\"spacer\"></th>";
        $constructid = $this->owner->owner->record->CONSTRUCTID;

        $currentsortcolumns = $this->owner->GetSortArray($defaultsort = $this->owner->record->SORTCOLUMNNAME);
        if ($currentsortcolumns == '')
            $currentsortcolumns = $this->owner->record->SORTCOLUMNNAME;
        if ($_CONFIG['multicolumnsort'] == true)
            $lastordernr = count($currentsortcolumns) + 1;
        else
            $lastordernr = 1;

        $basehref_order = $this->owner->AddUrlSortValues($basehref, $currentsortcolumns);
        // When the table is quite long with few columns then we add an extra column to make the table centered
        if ($this->owner->HasOption('no-wordwrap'))
            $nowrapclass = 'nowrap';
        else
            $nowrapclass = '';

        if ($_GET['select'] != 'true') {
            echo "<th class=\"action\"><input type='checkbox' tabindex='-1' class='checkbox deletebox' name='_hdnAllBox' id='_hdnAllBox' /></th>";
        }
        $fldidx = 1;
        foreach (array_keys($this->pages) as $index) {
            $page =& $this->pages[$index];
            if (!isset($page->lines))
                $page->LoadRecord();
            foreach (array_keys($page->lines->lines) as $lineindex) {
                $line =& $page->lines->lines[$lineindex];
                if (
                    (($line->record->LINETYPE == 'COLUMN' or $line->record->LINETYPE == 'COMPONENT')
                    and $line->record->COLUMNVISIBLE == 'Y' and $line->ShouldDisplayFieldInListView())
                    and (!isset($_SESSION[$this->owner->owner->record->CONSTRUCTID . '.' . GROUP_PREFIX . $line->record->COLUMNNAME]))
                ) {
                    if (!$masterfields or ($masterfields and !in_array($line->record->COLUMNNAME, $masterfields))) {

                        $align = $line->GetAlign($lineindex);
                        $fieldname = $line->record->COLUMNNAME;
                        $sortfieldname = $line->record->COLUMNNAME;

                        if ($line->record->READONLY == 'N' and $line->record->SUPERSHOWCOLUMNNAME != '') {
                            $fieldname = $fieldname . '_SSS' . $line->record->SUBSETID;
                            require_once 'classes/base/guielements.class.php';
                            $_lookupfield = new PLR_LookupPicklist($userdb, $line, inlistview:true, autofocus:false, optimizedselect:false);
                            echo $_lookupfield->GetLookupListJavascript('json', $line->record->REQUIRED == 'Y');
                        }

                        if ($line->record->LISTVALUES != '') {
                            echo '<script type="text/javascript"> // <![CDATA[
                                var ' . $line->record->COLUMNNAME . '_lookuppicklist = ';
                            $setarray = explode(';', trim($line->record->LISTVALUES));
                            foreach ($setarray as $k => $set) {
                                $setarray[$k] = $this->i18n($set);
                            }
                            echo SetAsJson($setarray);
                            echo "// ]]></script>";
                        }

                        $validationformat = $line->GetValidationFormat();
                        $numericmask = $line->GetNumericMaskFormat($validationformat);
                        $ordered = isset($currentsortcolumns[$sortfieldname]) ? 'sorted' : '';

                        echo "<th class=\"$ordered $align $nowrapclass $numericmask\" mask=\"$validationformat\">";

                        /*
                          No sort on detail tables, except if only the detailsrecords are shown
                        */
                        if (!$this->owner->detail or ($this->owner->detail and $this->owner->owner->record->SHOWDETAILASLINKS == 'Y')) {
                            $orderhref = $this->GetOrderHref($basehref_order, $lastordernr, $currentsortcolumns, $sortfieldname, $line->record->DATATYPE, $currentsortdir, $sortnr);
                            echo "<a class=\"sortcolumnlink\" href=\"$orderhref\">";
                        }

                        if ($ordered != '') {
                            echo ' <i class="material-symbols-outlined">'.($currentsortdir == 'up' ? 'keyboard_control_key' : 'keyboard_arrow_down' ).'</i>&nbsp;';
                        }
                        echo '&nbsp;' . str_replace(' ', '&nbsp;', $this->i18n(ProcessSQL($userdb, $line->record->LINEDESCRIPTION_SHORT))) . '&nbsp;';
                        if (!$this->owner->detail or ($this->owner->detail and $this->owner->owner->record->SHOWDETAILASLINKS == 'Y'))
                            echo '</a>';
                        echo '</th>';
                        if ($fldidx == 1) {
                            if (!$this->owner->HasOption('no-editlink'))
                                echo "<th class=\"editlink\"></th>";

                            /*
                            if (!$masterfields and $this->owner->owner->record->SHOWDETAILASLINKS == 'Y') {
                                $orderhref = $this->GetOrderHref($basehref_order, $lastordernr, $currentsortcolumns, strtoupper($line->record->TABLENAME).'_DETAILCOUNT', 'N', $currentsortdir, $sortnr);
                                $ordered = isset($currentsortcolumns[strtoupper($line->record->TABLENAME).'_DETAILCOUNT'])?'sorted':'';
                                echo "<th class=\"left $ordered $nowrapclass formnavigate\">";
                                echo "<a class=\"sortcolumnlink\" href=\"$orderhref\">";
                                if ($ordered != '') {
                //                                  echo '<div id="scaction_'.$nr.'" class="remove_ordercolumn" onclick="window.location.href=removeQueryVariable(\'sort_'.$sortnr.'\');">Verwijder deze sortering</div>';
                                  echo ' <i class="fa fa-caret-'.$currentsortdir.'"></i>&nbsp;';
                                }
                                echo "Navigeer</a></th>";
                            }*/
                        }
                        $fldidx++;
                    }
                }
            }
        }

        // Add an extra header column because the horizontal scrolling goes one scrollbar-width to far
        echo "<th class='headerfiller'>&nbsp;</th>";
        // When the table is quite long with few columns then we add an extra column to make the table centered
        // if ($this->owner->HasOption('group-table') ) echo "<th class=\"group-table right\">&nbsp;</th>";
        echo "</tr></thead>";
        return $fldidx;
    }

    function DetermineCSSValue($rec, $fieldname, $CSSIdentifiedColumn) {
        $result = '';
        if ($CSSIdentifiedColumn != '') {
            $csscolumns = explode(';', $CSSIdentifiedColumn);
            foreach ($csscolumns as $csscolumn) {
                if ($fieldname == $csscolumn) {
                    if (strpos($csscolumn, 'ISNULL(') !== FALSE) {
                        $csscolumn = str_replace(array("ISNULL(", ")"), array(""), $csscolumn);
                        if ($rec[$csscolumn] == '')
                            $result .= 'fval-global fval_' . $csscolumn . 'ISNULL';
                    } elseif (strpos($csscolumn, 'ISNOTNULL(') !== FALSE) {
                        $csscolumn = str_replace(array("ISNOTNULL(", ")"), array(""), $csscolumn);
                        if ($rec[$csscolumn] != '')
                            $result .= 'fval-global fval_' . $csscolumn . 'ISNOTNULL';
                    } else {
                        if ($rec[$csscolumn] != '') {
                            $_value = $rec[$csscolumn];
                            // take the first part up until to the first space
                            $_value = explode(' ', $_value);
                            $_value = $_value[0];
                            $result .= 'fval-global fval_' . $csscolumn . '_' . str_replace(' ', '_', $_value);
                        }
                    }
                }
            }
        }
        return $result;
    }

    function ShowStatusLogs($logs, $cssidcolumn, $recordid ) {
        $_result = '';

        if (!empty($logs)) {
            $_result .= '<div class="hoverstatus-content" id="statushover__'.$recordid.'">';
            $_result .= '<div class="hoverstatus-content-inner">';
            $_logrows = explode('|',$logs);
            foreach ($_logrows as $_index => $_log) {
                $_logitems = explode(';',$_log);
                $_logcssvalue = 'fval-global fval_'.$cssidcolumn.'_'.$_logitems[0];
                $_result .= '<div class="hoverstatus-content-item xrow xno-gutter">';
                $_result .= '<div><span class="'.$_logcssvalue.' hoverstatus-content-item-status">';
                $_result .= $_logitems[0];
                $_result .= '</span></div>';
                $_result .= '<div class="hoverstatus-content-item-person">';
                $_result .= $_logitems[2];
                $_result .= '</div>';
                $_result .= '<div class="hoverstatus-content-item-date">';
                $_result .= $_logitems[1];
                $_result .= '</div>';
                $_result .= '</div>';
                if ($_index >= 10 ) {
                    $_result .= '...'.(count($_logrows) - $_index - 1).' items worden niet getoond.';
                    break;
                }
            }
            $_result .= '</div></div>';
        }
        return $_result;
    }

    function ShowListViewDataRowTemplate($permissiontype, $rec, $rs)
    {
        echo "<tbody>";
        echo "<tr class=\"record\">
        <td></td>
        <td class=\"MODULECODE\"></td>
        <td></td>
        <td class=\"NAAM\"></td>
        <td class=\"PLR__RECORDID\"></td>
        </tr>
        ";
        echo "</tbody>";
    }

    function ShowListViewDataNew($userdb, $rs, $permissiontype, $masterfields = false, $hotedit = false)
    {
        global $_CONFIG, $_GVARS, $polaris;

        $basehref = $this->owner->MakeURLQuery(['q', 'qc', 'qtable', 'sort', 'dir', 'offset']);
        $_po = $this->polaris();
        $nowrapclass = $this->owner->HasOption('no-wordwrap') ? ' nowrap' : '';
        $formnameprefix = $this->getFormNamePrefix($masterfields);
        $primkeys = $this->owner->database->metaPrimaryKeys(strtolower($this->owner->GetDataDestination(TRUE)), $hashed = false);
        $currentsortcolumn = $_GET['sort'] ? $_GET['sort'] : $this->owner->record->SORTCOLUMNNAME;
        $sortableTable = $this->owner->HasOption('sortable-table') ? 'sortable-table' : '';
        $sortableColumnName = $this->owner->record->SORTCOLUMNNAME;

        echo '<tbody class="'.$sortableTable.'" data-column="'.$sortableColumnName.'">';

        if ($rs) {
            $rowidx = 1;
            foreach ($rs as $_index => $rec) {
                $this->renderTableRow($userdb, $rec, $permissiontype, $masterfields, $hotedit, $rowidx, $formnameprefix, $nowrapclass, $_po, $basehref);
                $rowidx++;
            }
        }

        echo '</tbody>';
        return $fldidx;
    }

    private function getFormNamePrefix($masterfields)
    {
        if ($masterfields) {
            return $this->owner->GetDataDestination() . '_';
        } elseif ($this->owner->owner->detailform) {
            // WHY?? $this->owner->owner->detailform->GetMasterFields();
        }
        return '';
    }

    private function renderTableRow($userdb, $rec, $permissiontype, $masterfields, $hotedit, $rowidx, $formnameprefix, $nowrapclass, $_po, $basehref)
    {
        $fldidx = 1;
        $_recidname = _RECORDID;
        $recordid = $this->owner->database->customUrlEncode($rec[$_recidname]);

        echo "<tr class=\"dr \" id=\"row@$recordid\">";

        $cellstatus = (($permissiontype & UPDATE) != UPDATE) ? ' cell_disabled' : '';

        if ($_GET['select'] != 'true') {
            $this->renderActionCell($recordid, $cellstatus);
        }

        foreach (array_keys($this->pages) as $index) {
            $page =& $this->pages[$index];
            foreach (array_keys($page->lines->lines) as $lineindex) {
                $line =& $page->lines->lines[$lineindex];
                $this->renderTableCell($userdb, $rec, $line, $permissiontype, $masterfields, $hotedit, $recordid, $fldidx, $formnameprefix, $nowrapclass, $_po, $basehref);
            }
        }

        echo "<td class='datafiller'>&nbsp;</td>";
        echo "</tr>";
    }

    private function renderActionCell($recordid, $cellstatus)
    {
        echo "<td class=\"action$cellstatus\">";
        if (!$this->owner->HasOption('no-select-checkboxes')) {
            echo "<input id=\"rec@$recordid\" name=\"rec@$recordid\" tabindex=\"-1\" type=\"checkbox\" class=\"xcheckbox deletebox rowselect\" />";
        }
        if ($this->owner->HasOption('show-pinning')) {
            echo "<a href=\"javascript:void(0)\" class=\"pinrecord\" title=\"" . $this->i18n_trans('pin_this_record') . "\">" .
                "<i class=\"material-symbols-outlined\">bookmark</i>" .
                "</a>";
        }
        echo "</td>";
    }

    private function renderTableCell($userdb, $rec, $line, $permissiontype, $masterfields, $hotedit, $recordid, &$fldidx, $formnameprefix, $nowrapclass, $_po, $basehref)
    {
        $hiddenfieldcache = '';
        if (
            ($line->record->LINETYPE == 'COLUMN' or $line->record->LINETYPE == 'COMPONENT')
            and $line->record->COLUMNVISIBLE == 'Y'
            and !isset($_SESSION[$this->owner->owner->record->CONSTRUCTID . '.' . GROUP_PREFIX . $line->record->COLUMNNAME])
            and (!$masterfields or ($masterfields and !in_array($line->record->COLUMNNAME, $masterfields)))
        ) {
            $fieldname = $line->record->COLUMNNAME;
            $originalfieldname = $fieldname;
            $namefieldname = $originalfieldname;
            if ($line->GetValidationFormat() == 'validation_float') {
                $namefieldname = $namefieldname . FLOATCONVERT;
            }
            if (
                $line->record->SUPERSHOWCOLUMNNAME != ''
                and $line->record->SUPERSHOWCOLUMNNAME != strtoupper($line->record->SUPERCOLUMNNAME)
            ) {
                $fieldname = $fieldname . '_sss' . $line->record->SUBSETID;
            }

            $hoverpop = ($line->record->DATATYPE == 'X') ? ' hoverpop' : '';
            $originalfieldvalue = $rec[$originalfieldname];

            if ($line->record->DATATYPE == 'P') {
                $originalfieldvalue = '*************';
                $fieldvalue = $originalfieldvalue;
            } else {
                $fieldvalue = $line->ProcessFieldValue($rec, $rec[$fieldname]);
            }

            $_edittype = $this->determineEditType($line, $permissiontype, $fieldvalue, $rec);

            $cellstatus = $this->determineCellStatus($line, $permissiontype, $fieldvalue, $rec);

            $cssidentifier = $line->record->CSSIDENTIFIER == 'Y' ? ' fval_' . str_replace(' ', '_', $originalfieldvalue) : '';
            $csshoverstatus = $this->owner->record->CSSIDENTIFIERCOLUMN == $namefieldname ? 'hoverstatus' : '';

            if ($line->ShouldDisplayFieldInListView()) {
                $align = $line->GetAlign($fldidx);
                echo "<td class=\"$align$nowrapclass$cssidentifier$cellstatus$hoverpop\">";
            }
            $editfield_noname = "<input type=\"hidden\" class=\"_fld{$originalfieldname}\" name=\"{$namefieldname}\" id=\"_fld{$originalfieldname}@$recordid\" value=\"$originalfieldvalue\" />";

            if ($hotedit) {
                $this->renderHotEditField($masterfields, $line, $userdb, $recordid, $fldidx, $originalfieldvalue, $permissiontype);
            } else {
                $this->renderNonHotEditField($masterfields, $line, $rec, $recordid, $fldidx, $fieldvalue, $originalfieldvalue, $editfield_noname, $formnameprefix, $permissiontype, $_po, $basehref);
            }

            echo '</td>';
            $fldidx++;
        }
    }

    private function determineEditType($line, $permissiontype, $fieldvalue, $rec)
    {
        if (
            (($permissiontype & UPDATE) == UPDATE)
            and !$line->IsReadOnly($fieldvalue)
            and !$this->owner->IsLineReadOnly($rec)
        ) {
            $_edittype = 'edit';
        } else {
            $_edittype = 'disabled';
        }
        if (
            $line->record->SUPERSHOWCOLUMNNAME != ''
            //and $line->record->SUPERSHOWCOLUMNNAME != strtoupper($line->record->SUPERCOLUMNNAME)
        ) {
            $_edittype .= ' select';
        } elseif ($line->record->LISTVALUES != '') {
            $_edittype .= ' select';
        }
        return $_edittype;
    }

    private function determineCellStatus($line, $permissiontype, $fieldvalue, $rec)
    {
        if (
            (($permissiontype & UPDATE) != UPDATE)
            or ($line->IsReadOnly($fieldvalue))
            or $this->owner->IsLineReadOnly($rec)
        ) {
            return ' cell_disabled';
        } elseif ($line->record->LINEREQUIRED == 'Y' or $line->record->REQUIRED == 'Y') {
            return ' cell_required';
        }
        return '';
    }

    private function renderHotEditField($masterfields, $line, $userdb, $recordid, &$fldidx, $originalfieldvalue, $permissiontype)
    {
        if ($recordid and ($fldidx == 1)) {
            echo '<input type="hidden" name="_hdnRecordID[]" value="' . $recordid . '" />';
        }
        $line->ShowGuiElement($userdb, $inlistview = true, $recordid, $state = 'insert', $originalfieldvalue, $line->record->LINEREQUIRED == 'Y' or $line->record->REQUIRED == 'Y', $line->record->READONLY == 'Y', $permissiontype, $optimizedselect = true);
        if ($recordid and ($fldidx == 1)) {
            echo "</td><td>";
        }
        $fldidx++;
    }

    private function renderNonHotEditField($masterfields, $line, $rec, $recordid, &$fldidx, $fieldvalue, $originalfieldvalue, $editfield_noname, $formnameprefix, $permissiontype, $_po, $basehref)
    {
        if (!$line->ShouldDisplayFieldInListView()) {
            $hiddenfieldcache .= $editfield_noname;
        } else {
            if ($recordid and ($fldidx == 1)) {
                $this->renderFirstField($masterfields, $line, $rec, $recordid, $fieldvalue, $originalfieldvalue, $editfield_noname, $formnameprefix, $permissiontype, $_po, $basehref);
            } else {
                $this->renderOtherFields($masterfields, $line, $rec, $recordid, $fieldvalue, $originalfieldvalue, $editfield_noname, $formnameprefix, $permissiontype, $_po, $basehref);
            }
        }
    }

    private function renderFirstField($masterfields, $line, $rec, $recordid, $fieldvalue, $originalfieldvalue, $editfield_noname, $formnameprefix, $permissiontype, $_po, $basehref)
    {
        if ($line->record->FORMAT == 'SMALLPIC') {
            $appl = $this->owner->owner->owner->record;
            $imgesc = "return escape('<img src=\'" . $line->record->IMAGEURL . "/_THUMB_" . $originalfieldvalue . "\'>')";
            echo '<img src="' . $line->record->IMAGEURL . '/_THUMB_' . $originalfieldvalue . '" height="20" width="20" onmouseover="this.T_DELAY=0;' . $imgesc . '" />&nbsp;&nbsp;';
            $GLOBALS['showtooltips'] = true;
        }
        if ($GLOBALS['select'] == true) {
            $destinationfield = $_GET['selectfield'];
            $destinationmasterfield = $_GET['selectmasterfield'];
            $selectatag = '<button type="button" title="' . lang_get('select') . '" href="javascript:void(0);" class="selectrow" recordid="' . $recordid . '" autosuggestfield="' . $destinationfield . '" autosuggestmasterfield="' . $destinationmasterfield . '">';
            echo $selectatag . '<i class="fa fa-chevron-circle-left fa-lg"></i></button> &nbsp;';
        }

        $listviewstate = $this->owner->HasOption('initial-view-state') ? 'view' : 'edit';
        if (!$masterfields) {
            $customhref = $this->owner->MakeURL() . "{$listviewstate}/{$recordid}/" . $this->owner->MakeQuery(['q', 'qc', 'qtable', 'sort', 'dir', 'offset']);
        } else {
            $customhref = $this->owner->MakeURL() . "view/{$_GET['rec']}/{$this->owner->record->METANAME}/edit/{$recordid}/" . $this->owner->MakeQuery(array('q', 'qc', 'qtable', 'sort', 'dir', 'offset'));
        }
        $fieldvalue = html_entity_decode(strip_tags($fieldvalue), ENT_COMPAT | ENT_HTML401, 'UTF-8');

        $labelClass = $line->record->FORMAT == 'LABEL' ? 'label' : '';

        if ($line->record->SUPERSHOWCOLUMNNAME != '' and $line->record->SUPERSHOWBOTH == 'Y') {
            if ($fieldvalue) {
                $fieldvalue = $originalfieldvalue . " - " . $fieldvalue;
            } else {
                $fieldvalue = $originalfieldvalue;
            }
        }
        echo $editfield_noname . '<span class="' . $labelClass . ' ' . $_edittype . '">' . $fieldvalue . '</span>';
    }

    private function renderOtherFields($masterfields, $line, $rec, $recordid, $fieldvalue, $originalfieldvalue, $editfield_noname, $formnameprefix, $permissiontype, $_po, $basehref) {
        global $_GVARS;

        switch ($line->record->FORMAT) {
            case 'POSITION':
                if (($permissiontype & UPDATE) == UPDATE) {
                    echo '<center><i class="sortable-handle material-symbols-outlined md-1x">drag_indicator</i></center>';
                }
                break;

            case 'ICONSELECT':
                echo '<i class="iconselect material-symbols-outlined md-1x">';
                echo $fieldvalue;
                echo '</i>';
                break;

            case 'BUTTON':
                $formatparams = explode(':', $line->record->FORMATPARAMS);
                $hotbuttonname = $formatparams[0];
                $hotcolumnnames = $formatparams[1];
                $hotcolumnvalues = $_po->ReplaceDynamicVariables($formatparams[2]);
                echo '<input type="button" class="hotbutton" name="_hdnHotUpdateButton_' . $recordid . '" id="_hdnHotUpdateButton_' . $recordid . '" onclick="Polaris.Dataview.hotRecordUpdate(\'' . $formnameprefix . 'frmhotupdate\', \'' . $recordid . '\', \'' . $hotcolumnnames . '\', \'' . $hotcolumnvalues . '\')" value="' . $hotbuttonname . '" />';
                break;

            case 'URL':
                $formatparams = explode(':', $line->record->FORMATPARAMS);
                $urlbuttonname = $_po->ReplaceDynamicVariables($formatparams[0], '', $rec);
                $urlbuttonurl = $formatparams[1];
                $urlbuttonurl = $_po->ReplaceDynamicVariables($urlbuttonurl, '', $rec);
                echo '<a href="' . $urlbuttonurl . '"><input type="button" class="button"  value="' . $urlbuttonname . '" target="_blank" /></a>';
                break;

            case 'COLOR':
                echo "<input type=\"color\" class=\"colorpicker\" data-target=\"{$formnameprefix}frmhotupdate\" name=\"_hdnColorButton_{$recordid}\" column=\"{$fieldname}\" value=\"{$fieldvalue}\" />";
                break;

            case 'EMAIL':
                echo "<img src=\"{$_GVARS['serverroot']}/images/email2_small.png\" style=\"vertical-align:text-top\" alt=\"Send mail\" /> <a class=\"replyemail\" title=\"" . lang_get('click_to_reply') . "\" href=\"mailto:$fieldvalue\">$fieldvalue</a>";
                break;

            case 'VISIBLE':
                if (($permissiontype & UPDATE) == UPDATE) {
                    $yes_down = $originalfieldvalue == 'Y' ? 'checked="checked"' : '';
                    $no_down = $originalfieldvalue != 'Y' ? 'checked="checked"' : '';
                    echo '<div class="checkbox-slider--b-flat checkbox-slider-info">
                        <label>
                            <input type="checkbox" class="togglevisible" data-target="' . $formnameprefix . 'frmextra" name="_hdnVisibleButton_' . $recordid . '" ' . $yes_down . ' data-values="N|Y" column="' . $fieldname . '"><span></span>
                        </label>
                    </div>';
                }
                break;

            case 'PUBLISH':
                if ($originalfieldvalue == 'Y') {
                    echo lang_get('published');
                } else {
                    echo '<input type="button" name="_hdnPublishButton" class="publish" value="' . lang_get('publish') . '" onclick="document.getElementById(\'frmextra\')._hdnPublishField.value=\'' . $recordid . '\';$(\'frmextra\').submit();return true" />';
                }
                break;

            default:
                if ($line->record->SUPERSHOWCOLUMNNAME != '' and $line->record->SUPERSHOWBOTH == 'Y') {
                    if ($fieldvalue) {
                        $fieldvalue = $originalfieldvalue . " - " . $fieldvalue;
                    } else {
                        $fieldvalue = $originalfieldvalue;
                    }
                }
                $fieldvalue = $line->ApplyMask($fieldvalue, $rec);
                if ($line->record->FORMAT == 'LOGGER') {
                    $_loggerparams = explode(':', $line->record->FORMATPARAMS);
                    $_loggersourcefield = $_loggerparams[0];
                    $_loggerwidth = $_loggerparams[1];
                    $csshoverstatus = !empty($rec[$_loggersourcefield]) ? 'hoverstatus' : '';
                } else {
                    $csshoverstatus =  '';
                }

                $cssvalue = $this->DetermineCSSValue($rec, $fieldname, $this->owner->record->CSSIDENTIFIERCOLUMN);

                if ($line->record->FORMAT == 'humanize_date') {
                    if (!empty($originalfieldvalue)) {
                        $fieldvalue = DateTimeHumanizer::difference(new \DateTime(), new \DateTime($originalfieldvalue), 'nl');
                    }
                }

                $fieldvalue = html_entity_decode(strip_tags($fieldvalue, ['mark']), ENT_COMPAT | ENT_HTML401, 'UTF-8');
                $unit = $line->record->UNIT;

                echo $editfield_noname . '<span class="'.$cssvalue.' '.$csshoverstatus.' '.$_edittype . '" data-hoverwidth="'.$_loggerwidth.'" data-hoverid="'.$recordid.'">' . $unit . ' ' . $fieldvalue . '</span>';
                if (!empty($csshoverstatus)) {
                    echo $this->ShowStatusLogs($rec[$_loggersourcefield], $fieldname, $recordid);
                }
                break;
        }
    }





    function ShowListViewData($userdb, $rs, $permissiontype, $masterfields = false, $hotedit = false)
    {
        global $_CONFIG;
        global $_GVARS;
        global $polaris;

        $basehref = $this->owner->MakeURLQuery(['q', 'qc', 'qtable', 'sort', 'dir', 'offset']);
        $_po = $this->polaris();
        /**
         * If wordwrap is not allowed in a table (fullwidth) then add the nowrap css class;
         */
        if ($this->owner->HasOption('no-wordwrap'))
            $nowrapclass = ' nowrap';
        else
            $nowrapclass = '';

        if ($masterfields) {
            /**
             * if it's a detailform add a prefix to the formname
             */
            $formnameprefix = $this->owner->GetDataDestination() . '_';
        } elseif ($this->owner->owner->detailform) {
            /**
             * if it's a masterform load the masterfields for the detailform
             */
            // WHY?? $this->owner->owner->detailform->GetMasterFields();
        }
        $primkeys = $this->owner->database->metaPrimaryKeys(strtolower($this->owner->GetDataDestination(TRUE)), $hashed = false);
        $rowidx = 1;
        $currentsortcolumn = $_GET['sort'] ? $_GET['sort'] : $this->owner->record->SORTCOLUMNNAME;
        if ($this->owner->HasOption('sortable-table')) {
            $sortableTable = 'sortable-table';
            $sortableColumnName = $this->owner->record->SORTCOLUMNNAME;
        }

        echo '<tbody class="'.$sortableTable.'" data-column="'.$sortableColumnName.'">';

        if ($rs)
            foreach ($rs as $_index => $rec) {
                $fldidx = 1;
                $_recidname = _RECORDID;
                $recordid = $this->owner->database->customUrlEncode($rec[$_recidname]);

                echo "<tr class=\"dr \" id=\"row@$recordid\">";

                $cellstatus = '';
                if ((($permissiontype & UPDATE) != UPDATE)) {
                    $cellstatus = ' cell_disabled';
                }

                if ($_GET['select'] != 'true') {
                    echo "<td class=\"action$cellstatus\">";
                    if (!$this->owner->HasOption('no-select-checkboxes')) {
                        echo "<input id=\"rec@$recordid\" name=\"rec@$recordid\" tabindex=\"-1\" type=\"checkbox\" class=\"xcheckbox deletebox rowselect\" />";
                    }
                    if ($this->owner->HasOption('show-pinning')) {
                        echo "<a href=\"javascript:void(0)\" class=\"pinrecord\" title=\"" . $this->i18n_trans('pin_this_record') . "\">" .
                            "<i class=\"material-symbols-outlined\">bookmark</i>" .
                            "</a>";
                    }

                    echo "</td>";
                }

                foreach (array_keys($this->pages) as $index) {
                    $page =& $this->pages[$index];
                    foreach (array_keys($page->lines->lines) as $lineindex) {
                        $line =& $page->lines->lines[$lineindex];
                        $hiddenfieldcache = '';
                        if (
                            ($line->record->LINETYPE == 'COLUMN' or $line->record->LINETYPE == 'COMPONENT')
                            and $line->record->COLUMNVISIBLE == 'Y'
                            and !isset($_SESSION[$this->owner->owner->record->CONSTRUCTID . '.' . GROUP_PREFIX . $line->record->COLUMNNAME])
                            and (!$masterfields or ($masterfields and !in_array($line->record->COLUMNNAME, $masterfields)))
                        ) {
                            $fieldname = $line->record->COLUMNNAME;
                            $originalfieldname = $fieldname;
                            $namefieldname = $originalfieldname;
                            if ($line->GetValidationFormat() == 'validation_float') {
                                $namefieldname = $namefieldname . FLOATCONVERT;
                            }
                            if (
                                $line->record->SUPERSHOWCOLUMNNAME != ''
                                and $line->record->SUPERSHOWCOLUMNNAME != strtoupper($line->record->SUPERCOLUMNNAME)
                            ) {
                                $fieldname = $fieldname . '_sss' . $line->record->SUBSETID;
                            }

                            $hoverpop = ($line->record->DATATYPE == 'X') ? ' hoverpop' : '';
                            // make sure the special chars (äèà...) get converted first, then apply htmlentities
                            $originalfieldvalue = $rec[$originalfieldname];

                            if ($line->record->DATATYPE == 'P') {
                                // Hide the password fields with asterics values
                                $originalfieldvalue = '*************';
                                $fieldvalue = $originalfieldvalue;
                            } else {
                                $fieldvalue = $line->ProcessFieldValue($rec, $rec[$fieldname]);
                            }

                            if (
                                (($permissiontype & UPDATE) == UPDATE)
                                and !$line->IsReadOnly($fieldvalue)
                                and !$this->owner->IsLineReadOnly($rec)
                            ) {
                                $_edittype = 'edit';
                            } else {
                                $_edittype = 'disabled';
                            }
                            if (
                                $line->record->SUPERSHOWCOLUMNNAME != ''
                                //and $line->record->SUPERSHOWCOLUMNNAME != strtoupper($line->record->SUPERCOLUMNNAME)
                            ) {
                                $_edittype .= ' select';
                            } elseif ($line->record->LISTVALUES != '') {
                                $_edittype .= ' select';
                            }

                            $cellstatus = '';
                            if (
                                (($permissiontype & UPDATE) != UPDATE)
                                or ($line->IsReadOnly($fieldvalue))
                                or $this->owner->IsLineReadOnly($rec)
                            ) {
                                $cellstatus = ' cell_disabled';
                            } elseif ($line->record->LINEREQUIRED == 'Y' or $line->record->REQUIRED == 'Y') {
                                $cellstatus = ' cell_required';
                            }
                            $csshoverstatus = '';
                            $cssidentifier = '';
                            if ($line->record->CSSIDENTIFIER == 'Y') {
                                $cssidentifier = ' fval_' . str_replace(' ', '_', $originalfieldvalue);
                            }
                            if ($this->owner->record->CSSIDENTIFIERCOLUMN == $namefieldname) {
                                $csshoverstatus = 'hoverstatus';
                            }

                            if ($line->ShouldDisplayFieldInListView()) {
                                $align = $line->GetAlign($fldidx);
                                echo "<td class=\"$align$nowrapclass$cssidentifier$cellstatus$hoverpop\">";
                            }
                            $editfield_noname = "<input type=\"hidden\" class=\"_fld{$originalfieldname}\" name=\"{$namefieldname}\" id=\"_fld{$originalfieldname}@$recordid\" value=\"$originalfieldvalue\" />";

                            if ($hotedit) {
                                if ($recordid and ($fldidx == 1)) {
                                    echo '<input type="hidden" name="_hdnRecordID[]" value="' . $recordid . '" />';
                                }
                                $line->ShowGuiElement($userdb, $inlistview = true, $recordid, $state = 'insert', $originalfieldvalue, $line->record->LINEREQUIRED == 'Y' or $line->record->REQUIRED == 'Y', $line->record->READONLY == 'Y', $permissiontype, $optimizedselect = true);
                                if ($recordid and ($fldidx == 1)) {
                                    echo "</td><td>";
                                }
                                $fldidx++;
                            } else {
                                if (!$line->ShouldDisplayFieldInListView()) {
                                    /***
                                     * Not visible? Then only show the hidden field... without the fieldvalue
                                     */

                                    $hiddenfieldcache .= $editfield_noname;
                                } else {
                                    if ($recordid and ($fldidx == 1)) {
                                        if ($line->record->FORMAT == 'SMALLPIC') {
                                            $appl = $this->owner->owner->owner->record;
                                            $imgesc = "return escape('<img src=\'" . $line->record->IMAGEURL . "/_THUMB_" . $originalfieldvalue . "\'>')";
                                            echo '<img src="' . $line->record->IMAGEURL . '/_THUMB_' . $originalfieldvalue . '" height="20" width="20" onmouseover="this.T_DELAY=0;' . $imgesc . '" />&nbsp;&nbsp;';
                                            $GLOBALS['showtooltips'] = true;
                                        }
                                        if ($GLOBALS['select'] == true) {
                                            $destinationfield = $_GET['selectfield'];
                                            $destinationmasterfield = $_GET['selectmasterfield'];
                                            //                                            $selectatag = $editfield_noname.'<a title="Selecteer dit record" href="javascript:void(0);" class="selectrow" recordid="'.$recordid.'" autosuggestfield="'.$destinationfield.'" autosuggestmasterfield="'.$destinationmasterfield.'">';
                                            $selectatag = '<button type="button" title="' . lang_get('select') . '" href="javascript:void(0);" class="selectrow" recordid="' . $recordid . '" autosuggestfield="' . $destinationfield . '" autosuggestmasterfield="' . $destinationmasterfield . '">';
                                            echo $selectatag . '<i class="fa fa-chevron-circle-left fa-lg"></i></button> &nbsp;';
                                        }

                                        $listviewstate = $this->owner->HasOption('initial-view-state')?'view':'edit';
                                        if (!$masterfields) {
                                            // if it's a masterform the create an edit url
                                            $customhref = $this->owner->MakeURL() . "{$listviewstate}/{$recordid}/" . $this->owner->MakeQuery(['q', 'qc', 'qtable', 'sort', 'dir', 'offset']);
                                        } else {
                                            // if it's a detailform the create an edit url for the detailrecord;
                                            $customhref = $this->owner->MakeURL() . "view/{$_GET['rec']}/{$this->owner->record->METANAME}/edit/{$recordid}/" . $this->owner->MakeQuery(array('q', 'qc', 'qtable', 'sort', 'dir', 'offset'));
                                            //                                            $customhref = $this->owner->MakeURL()."{$basehref}detail/{$_GET['rec']}/detailtabel/edit/{$recordid}/";
//                                            $customhref = $basehref.'&amp;action=edit&amp;rec='.$_GET['rec'].'&amp;detailaction=edit&amp;detailrec='.$recordid;
//                                            if ($GLOBALS['detailonly'])
//                                                $customhref .= '&amp;donly=true';
                                        }
                                        $fieldvalue = html_entity_decode(strip_tags($fieldvalue), ENT_COMPAT | ENT_HTML401, 'UTF-8');

                                        $labelClass = '';
                                        if ($line->record->FORMAT == 'LABEL') {
                                            $labelClass = 'label';
                                        }

                                        if ($line->record->SUPERSHOWCOLUMNNAME != '' and $line->record->SUPERSHOWBOTH == 'Y') {
                                            if ($fieldvalue) {
                                                $fieldvalue = $originalfieldvalue . " - " . $fieldvalue;
                                            } else {
                                                $fieldvalue = $originalfieldvalue;
                                            }
                                        }
                                        echo $editfield_noname . '<span class="' . $labelClass . ' ' . $_edittype . '">' . $fieldvalue . '</span>';

                                    } else {
                                        if ($line->record->FORMAT == 'POSITION' and ($permissiontype & UPDATE) == UPDATE) {
                                            echo '<center><i class="sortable-handle material-symbols-outlined md-1x">drag_indicator</i></center>';
                                        } elseif ($line->record->FORMAT == 'ICONSELECT') {
                                            echo '<i class="iconselect material-symbols-outlined md-1x">';
                                            echo $fieldvalue;
                                            echo '</i>';
                                        } elseif ($line->record->FORMAT == 'LINKBUTTON') {
                                            $links = explode(';', $line->record->FORMATPARAMS);
                                            echo '<div class="rowaction">';
                                            foreach ($links as $link) {
                                                $formatparams = explode('|', $link);
                                                $linkname = $formatparams[0];
                                                $linkhref = $formatparams[1];
                                                if (substr($linkhref, 0, 7) != 'http://')
                                                    $linkhref = $_GVARS['serverroot'] . $linkhref;
                                                $linkhref = $_po->ReplaceDynamicVariables($linkhref, '', $rec);
                                                echo '<a href="' . $linkhref . '" />[' . $linkname . ']</a>';
                                            }
                                            echo '</div>';
                                        } elseif ($line->record->FORMAT == 'BUTTON') {
                                            $formatparams = explode(':', $line->record->FORMATPARAMS);
                                            $hotbuttonname = $formatparams[0];
                                            $hotcolumnnames = $formatparams[1];
                                            $hotcolumnvalues = $_po->ReplaceDynamicVariables($formatparams[2]);
                                            echo '<input type="button" class="hotbutton" name="_hdnHotUpdateButton_' . $recordid . '" id="_hdnHotUpdateButton_' . $recordid . '" onclick="Polaris.Dataview.hotRecordUpdate(\'' . $formnameprefix . 'frmhotupdate\', \'' . $recordid . '\', \'' . $hotcolumnnames . '\', \'' . $hotcolumnvalues . '\')" value="' . $hotbuttonname . '" />';
                                        } elseif ($line->record->FORMAT == 'URL') {
                                            $formatparams = explode(':', $line->record->FORMATPARAMS);
                                            $urlbuttonname = $_po->ReplaceDynamicVariables($formatparams[0], '', $rec);
                                            $urlbuttonurl = $formatparams[1];
                                            $urlbuttonurl = $_po->ReplaceDynamicVariables($urlbuttonurl, '', $rec);
                                            echo '<a href="' . $urlbuttonurl . '"><input type="button" class="button"  value="' . $urlbuttonname . '" target="_blank" /></a>';
                                        } elseif ($line->record->FORMAT == 'COLOR') {
                                            echo "<input type=\"color\" class=\"colorpicker\" data-target=\"{$formnameprefix}frmhotupdate\" name=\"_hdnColorButton_{$recordid}\" column=\"{$fieldname}\" value=\"{$fieldvalue}\" />";
                                        } elseif ($line->record->FORMAT == 'EMAIL') {
                                            echo "<img src=\"{$_GVARS['serverroot']}/images/email2_small.png\" style=\"vertical-align:text-top\" alt=\"Send mail\" /> <a class=\"replyemail\" title=\"" . lang_get('click_to_reply') . "\" href=\"mailto:$fieldvalue\">$fieldvalue</a>";
                                        } elseif ($line->record->FORMAT == 'VISIBLE' and ($permissiontype & UPDATE) == UPDATE) {
                                            if ($originalfieldvalue == 'Y') {
                                                $yes_down = 'checked="checked"';
                                                $no_down = '';
                                            } else {
                                                $yes_down = '';
                                                $no_down = 'checked="checked"';
                                            }
                                            echo '<div class="checkbox-slider--b-flat checkbox-slider-info">
                                                <label>
                                                    <input type="checkbox" class="togglevisible" data-target="' . $formnameprefix . 'frmextra" name="_hdnVisibleButton_' . $recordid . '" ' . $yes_down . ' data-values="N|Y" column="' . $fieldname . '"><span></span>
                                                </label>
                                            </div>';
                                        } elseif ($line->record->FORMAT == 'PUBLISH') {
                                            if ($originalfieldvalue == 'Y')
                                                echo lang_get('published');
                                            else {
                                                echo '<input type="button" name="_hdnPublishButton" class="publish" value="' . lang_get('publish') . '" onclick="document.getElementById(\'frmextra\')._hdnPublishField.value=\'' . $recordid . '\';$(\'frmextra\').submit();return true" />';
                                            }
                                        } else {
                                            if ($line->record->SUPERSHOWCOLUMNNAME != '' and $line->record->SUPERSHOWBOTH == 'Y') {
                                                if ($fieldvalue)
                                                    $fieldvalue = $originalfieldvalue . " - " . $fieldvalue;
                                                else
                                                    $fieldvalue = $originalfieldvalue;
                                            }
                                            $fieldvalue = $line->ApplyMask($fieldvalue, $rec);
                                            if ($line->record->FORMAT == 'LOGGER') {
                                                $_loggerparams = explode(':', $line->record->FORMATPARAMS);
                                                $_loggersourcefield = $_loggerparams[0];
                                                $_loggerwidth = $_loggerparams[1];
                                                $csshoverstatus = !empty($rec[$_loggersourcefield]) ? 'hoverstatus' : '';
                                            } else {
                                                $csshoverstatus =  '';
                                            }

                                            $cssvalue = $this->DetermineCSSValue($rec, $fieldname, $this->owner->record->CSSIDENTIFIERCOLUMN);

                                            if ($line->record->FORMAT == 'humanize_date') {
                                                if (!empty($originalfieldvalue)) {
                                                    $fieldvalue = DateTimeHumanizer::difference(new \DateTime(), new \DateTime($originalfieldvalue), 'nl');
                                                }
                                            }

                                            $fieldvalue = html_entity_decode(strip_tags($fieldvalue, ['mark']), ENT_COMPAT | ENT_HTML401, 'UTF-8');
                                            $unit = $line->record->UNIT;

                                            echo $editfield_noname . '<span class="'.$cssvalue.' '.$csshoverstatus.' '.$_edittype . '" data-hoverwidth="'.$_loggerwidth.'" data-hoverid="'.$recordid.'">' . $unit . ' ' . $fieldvalue . '</span>';
                                            if (!empty($csshoverstatus)) {
                                                echo $this->ShowStatusLogs($rec[$_loggersourcefield], $fieldname, $recordid);
                                            }
                                        }
                                    }

                                    echo '</td>';
                                    if (isset($recordid) and $fldidx == 1) {
                                        $customhref .= '&baseoffset=' . $rowidx;
                                        $_tmpEcho = "<td class=\"dropdown $cellstatus\">";

                                        if (($recordid != false) and ($recordid != "")) {
                                            $_tmpEcho .= '<a class="edit_mnu jump" data-toggle="dropdown" href="'.$customhref.'">
                                            <i class="material-symbols-outlined edit_mnu_i">more_vert</i>
                                            </a>';
                                        }

                                        if (($permissiontype & DOCLONE) == DOCLONE and ($recordid != false) and ($recordid != '')) {
                                            $clone_linktag = '<a href="' . $customhref . '" accesskey="r" class="xclone_but jump" title="' . lang_get('clone') . '" >';
                                            $clone_linktag = str_replace('/edit/', '/clone/', $clone_linktag);
                                            $_tmpEcho .= '&nbsp;' . $clone_linktag . '<i class="fa fa-files-o"></i></a>';
                                        }

                                        $_tmpEcho .= '</td>';
                                        echo $_tmpEcho;

                                        /*
                                        if (!$masterfields and $this->owner->owner->record->SHOWDETAILASLINKS == 'Y') {
                                            $navigatebasehref = $this->owner->MakeURLQuery();
                                            $nav_url = $this->owner->MakeURL()."detail/{$recordid}/".$this->owner->record->METANAME."/".$this->owner->MakeQuery();
                                            $detailcountcolumnname = strtoupper($this->owner->GetDataDestination()).'_DETAILCOUNT';
                                            if ($rec->$detailcountcolumnname != '')
                                                $detailcount = "({$rec->$detailcountcolumnname})";
                                            else
                                                $detailcount = '';
                                            echo "<td class=\"formnavigate$nowrapclass\"><a href=\"$nav_url\" class='jump formnavigate'>".lang_get('view_details')." ".$this->owner->owner->detailform->ItemsName()." $detailcount &raquo;</a></td>";
                                        }
                                        */
                                    }
                                    $fldidx++;
                                }
                            }
                        }
                    }
                }

                // Add an extra data filler column to layout the columns better
                echo "<td class='datafiller'>&nbsp;</td>";

                // When the table is quite long with few columns then we add an extra column to make the table centered
//                if ($this->owner->HasOption('group-table') ) echo "<td class=\"group-table right\">&nbsp;</td>";
                if ($hiddenfieldcache != '')
                    echo "<td style=\"display:none;\">$hiddenfieldcache</td>";
                /*
                $sentence = $this->owner->record->SENTENCEPATTERN;
                $sentence = str_replace ('<nr>', $rec->NR, $sentence);
                $sentence = str_replace ('<status>', $rec->STATUS, $sentence);

                echo '<td><a href="#" title="'.$sentence.'">#</a></td>';
                */
                echo "</tr>";
                $rowidx++;
                $prevrec = $rec;

                //if ($bodygroup != $cssvalue)
                $bodygroup = $cssvalue;

                if ($this->owner->HasOption('header-repeat') and fmod($rowidx, $GLOBALS['REPEATHEADERAT']) == 0)
                    $this->ShowListViewHeader($userdb, $permissiontype, $repeatedheader = true);
            }
        echo '</tbody>';
        return $fldidx;
    }

    function ShowListViewFormulas($userdb, $permissiontype, $repeatedheader = false, $masterfields = false, $hotedit = false)
    {
        global $_CONFIG;

        $basehref = $this->owner->MakeURLQuery(array('q', 'qc', 'qtable', 'offset'), $this->owner->detail);

        echo "<tfoot class='formulas'><tr>";
        $fldidx = 1;
        foreach (array_keys($this->pages) as $index) {
            $page =& $this->pages[$index];
            if (!isset($page->lines))
                $page->LoadRecord();
            foreach (array_keys($page->lines->lines) as $lineindex) {
                $line =& $page->lines->lines[$lineindex];
                if (
                    (($line->record->LINETYPE == 'COLUMN') and $line->record->COLUMNVISIBLE == 'Y' and $line->record->VISIBLEINLIST == 'Y')
                    and (!isset($_SESSION[$this->owner->owner->record->CONSTRUCTID . '.' . GROUP_PREFIX . $line->record->COLUMNNAME]))
                ) {

                    if (!$masterfields or ($masterfields and !in_array($line->record->COLUMNNAME, $masterfields))) {
                        $fieldname = $line->record->COLUMNNAME;

                        // if FORMULA in SUM, AVG, COUNT, MIN, MAX, then show the formula
                        if (in_array($line->record->FORMULA, ['SUM', 'AVG', 'COUNT', 'MIN', 'MAX'])) {
                            echo "<td class=\"formula numeric\" data-formula=\"{$line->record->FORMULA}\" data-formulafield=\"$fieldname\"><b><span></span></b></td>";
                        } else {
                            echo "<td></td>";
                        }
                        if ($fldidx == 1) {
                            if (!$this->owner->HasOption('no-editlink'))
                                echo "<td></td>";
                            echo "<td></td>";
                        }

                        $fldidx++;
                    }
                }
            }
        }
        echo "</tr></tfoot>";
        return $fldidx;
    }

    function ShowListViewFooter($fieldcount, $permissiontype, $masterfields, $hotedit = false)
    {
        $colspan = $fieldcount + 1;

        if ($this->owner->HasOption('no-editlink'))
            $colspan = $colspan - 1;
        echo "<tfoot><tr>";
        //      echo "<td class=\"spacer\"></td>";

        //        echo '<td class="action"></td>';

        // When the table is quite long with few columns then we add an extra column to make the table centered
//      if ($this->owner->HasOption('group-table') ) echo "<td class=\"group-table left\">&nbsp;</td>";
        echo "<td colspan='$colspan'>";
        echo "</td>";

        //      if (!$masterfields and $this->owner->owner->record->SHOWDETAILASLINKS == 'Y') {
//        echo "<td class=\"formnavigate\">what is this?</td>";
//      }

        // When the table is quite long with few columns then we add an extra column to make the table centered
        if ($this->owner->HasOption('group-table'))
            echo "<td class=\"group-table right\"></td>";
        echo "</tr></tfoot>";
    }

    function ShowPage($pageid)
    {
        $form =& $this->owner->record;
        $keys = array($form->CLIENTID, $form->FORMID, 1);
        $currentpage =& $this->FindRecord($this->pages, $keys);
        $currentpage->LoadRecord();
        $currentpage->Show();
    }

    function FindLineRecord($columnname) {
        foreach($this->pages as $_page) {
            $_page->LoadRecord();
            $_statusLine = $_page->lines->FindLineRecord($columnname);
            if ($_statusLine) {
                return $_statusLine;
            }
        }
    }

}