<?php

class base_plrSearchColumns extends base_plrRecords {
    var $columns;

    function __construct(&$aowner) {
        parent::__construct($aowner);
        $this->keyfields = array('CLIENTID', 'FORMID',  'TABLENAME', 'COLUMNNAME');
        $this->LoadRecords();
    }

    function LoadRecords() {
        global $_sqlSearchColumns;
        $_owner = & $this->owner->record;

        $rs = $this->dbExecute($_sqlSearchColumns, array($_owner->CLIENTID, $_owner->FORMID)) or Die('query failed lines in plr_search: '.$this->dbErrorMsg());
        $this->columns = array();
        while ($rec = $rs->FetchNextObject(true)) {
            $plrSearchColumn = new base_plrSearchColumn($this);
            $plrSearchColumn->record = $rec;
            $this->columns[] = $plrSearchColumn;
        }
    }

    function Show($searchvalues) {
        return $this->_Show('Show', $searchvalues);
    }

    function ShowHorizontal($searchvalues) {
        return $this->_Show('ShowHorizontal', $searchvalues);
    }

    function _Show($function, $searchvalues) {
        foreach(array_keys($this->columns) as $index ) {
            $_column =& $this->columns[$index];
            $searchvalues = $_column->$function($searchvalues);
        }
        return $searchvalues;
    }
}
