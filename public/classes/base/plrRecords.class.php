<?php

class base_plrRecords extends base_plrBase {
    var $owner;
    var $keyfields;

    function __construct(&$aowner) {
        parent::__construct();

        $this->owner =& $aowner;
    }

    function &FindRecord($array, $keyvalues) {
        $foundobject = null;
        foreach ($array as $object) {
            $currentrecord = $object->record;
            $result = true;
            $i = 0;
            foreach($this->keyfields as $keyfield) {
                if ($currentrecord->$keyfield != $keyvalues[$i]) {
                        $result = false;
                    break;
                }
                $i++;
            }
            if ($result) {
                $foundobject = $object;
                return $foundobject;
            }
        }
    }

}
