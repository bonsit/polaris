<?php

require('includes/cloudflare_images.php');

class PLR_GuiElement {
    var $owner;
    var $isprocessed;
    var $selectrs;
    var $superrecord;
    var $userdb;
    var $lineobject;
    var $inlistview;
    var $size;
    var $maxlength;
    var $readonly;
    var $disabled;
    var $inputname;
    var $readonlyclass;
    var $requiredclass;
    var $events;

    function __construct($userdb, $lineobject, $inlistview, $autofocus=false) {
        $this->isprocessed = false;
        $this->userdb =& $userdb;
        $this->owner =& $lineobject;
        $this->inlistview = $inlistview;
        $this->autofocus = $autofocus;

        $this->size = Max($this->owner->record->TOTALLENGTH, 50);
        if ($this->owner->record->TOTALLENGTH != 0)
            $this->maxlength = "maxlength='".trim($this->owner->record->TOTALLENGTH)."'";
        else
            $this->maxlength = "";
    }

    function GetLabelTag() {
        return 'label="'.ProcessSQL($this->userdb, $this->owner->record->LINEDESCRIPTION).'"';
    }

    function ProcessDisplayValue($fieldvalue) {
        if (($this->owner->record->DATATYPE == 'X') or ($this->owner->record->DATATYPE == 'C')) {
            if (strpos($this->owner->record->FORMATPARAMS, ':showhtml' ) === false ) {
                $fieldvalue = htmlspecialchars($fieldvalue);
                $fieldvalue = strip_tags($fieldvalue);
            }
            if ( ($this->owner->record->CUTOFFTHRESHOLD > 0) and (!$this->owner->owner->owner->owner->owner->HasOption('no-cutoff')) ) {
                if (strlen($fieldvalue) > $this->owner->record->CUTOFFTHRESHOLD) {
                    $fieldvalue = substr($fieldvalue, 0, $this->owner->record->CUTOFFTHRESHOLD) . '...';

                }
            }
        }
        return $fieldvalue;
    }

    function DefaultValue() {
        global $polaris;

        $thedefaultvalue = '';
        if (($this->owner->record->DEFAULTVALUE != '' or $this->owner->record->LINEDEFAULTVALUE != '')) {
            if ($this->owner->record->LINEDEFAULTVALUE != '')
                $thedefaultvalue = $this->owner->record->LINEDEFAULTVALUE;
            else
                $thedefaultvalue = $this->owner->record->DEFAULTVALUE;
        }
        $thedefaultvalue = $polaris->ReplaceDynamicVariables($thedefaultvalue, $nullvalue = 'NULL');
        $thedefaultvalue = ProcessSQL($this->userdb, $thedefaultvalue);
        return $thedefaultvalue;
    }

    function GetAllListItems($optimizedselect=false) {
        global $polaris;
        global $_sqlSubsetItems;

        $record =& $this->owner->record;

        if (!$this->selectrs) {
            $_tableName = $this->owner->owner->owner->owner->owner->GetDataDestination();
            $rs = $this->owner->dbExecute($_sqlSubsetItems, array($record->CLIENTID, $record->DATABASEID, $_tableName, $record->COLUMNNAME)) or Die('subset query failed: '.$this->owner->dbErrorMsg());
            if ($rs and !$rs->EOF) {
                $this->superrecord = $rs->FetchNextObject(true);
                $_tablePrefix = "sss{$this->superrecord->SUBSETID}";
                $sql = "SELECT * FROM {$this->superrecord->SUPERTABLENAME} {$_tablePrefix}";
                $_filter = $polaris->plrClient->GetDataScopeFilter($record->DATABASEID, $this->superrecord->SUPERTABLENAME, '');
                if (!empty($_filter)) {
                    $sql .= ' WHERE '.$_filter;
                }
                if ($this->superrecord->FILTER) {
                    if (!empty($_filter)) {
                        $sql .= ' AND ';
                    } else {
                        $sql .= ' WHERE ';
                    }
                    $sql .= $this->superrecord->FILTER;
                    $sql = str_replace('{prefix}', $_tablePrefix, $sql);

                    $sql = $polaris->ReplaceDynamicVariables($sql, $nullvalue = 'NULL');
                }
                $this->selectrs = $this->userdb->Execute($sql);
                $this->isprocessed = true;

                if (($optimizedselect or !empty($this->superrecord->CASCADEFORM))) {
                    $this->optimizedselect = true;
                    $this->GetLookupListJavascript($this->inlistview?'json':'json_extended', required:false);
                }
            }
        }

        if (!$this->selectrs) {
            echo 'Probleem met de lookuppicklist:';
            echo "<pre>{$sql}</pre>";
            // pr($_sqlSubsetItems, array($record->CLIENTID, $record->DATABASEID, $_tableName, $record->COLUMNNAME));
        }

        return $this->selectrs;
    }

    function Show($state, $currentvalue, $rec, $required, $readonly, $recordid, $permissiontype) {
        if ($readonly or ($state == 'view') or ($this->owner->record->PASSWORD == 'Y')
        or (($permissiontype & UPDATE) != UPDATE) ) {
            $this->readonlytag = 'readonly="readonly"';
            $this->disabled = 'disabled="disabled"';
            $this->readonlyclass = 'readonly';
        } else {
            $this->readonlytag = '';
            $this->disabled = '';
            $this->readonlyclass = '';
        }
        if ($required) {
            $this->requiredclass = 'required';
        } else {
            $this->requiredclass = '';
        }
        if ( (($permissiontype & INSERT) == INSERT) or (($permissiontype & UPDATE) == UPDATE) )
        $this->events = createEvents($this->owner->record->EVENTS, $recordid);
    }
}

class PLR_LookupPicklist extends PLR_GuiElement {
    var $superrecord;
    var $html;
    var $basehtml;
    var	$displaycount;
    var $optimizedselect;
    var $multiselect;
    var $selectrs;

    function __construct($userdb, $lineobject, $inlistview, $autofocus=false, $optimizedselect=false, $multiselect=false) {
        parent::__construct($userdb, $lineobject, $inlistview, $autofocus);

        $this->basehtml = '';
        $this->displaycount = 0;
        $this->optimizedselect = $optimizedselect;
        $this->multiselect = $multiselect;
        $this->GetAllListItems($this->optimizedselect);
    }

    function GetLookupListJavascript($type='array', $required) {
    	global $ADODB_EXTENSION;

        $record =& $this->owner->record;

        $this->basehtml = '<script type="text/javascript">
        var '.$record->COLUMNNAME.'_lookuppicklist = ';
        if ($type == 'array') {
            $this->basehtml .= 'new Array( ';
        } elseif ($type=="json") {
            $this->basehtml .= '{';
        } elseif ($type=="json_extended") {
            $this->basehtml .= '[';
        }
        $supershowcolumnname = $record->SUPERSHOWCOLUMNNAME;
        if (strpos($supershowcolumnname, ',') !== false)
            $supershowcolumnname = 0;
        elseif (strpos($supershowcolumnname, '.') !== false)
            $supershowcolumnname = substr($supershowcolumnname, strpos($supershowcolumnname, '.') + 1);
        $supershowcolumnname = strtoupper($supershowcolumnname);
        $supercolumn = strtoupper($this->superrecord->SUPERCOLUMNNAME);

        $_first = true;
        if ($this->selectrs) {
            while (!$this->selectrs->EOF) {
                $rec = $this->selectrs->fetchObject();
                $vals = array_values(get_object_vars($rec));

                $key = json_encode($rec->$supercolumn);
                if ($rec->$supershowcolumnname == NULL) {
                    $value = json_encode($vals[$supershowcolumnname]);
                } else {
                    $value = json_encode($rec->$supershowcolumnname);
                }
                if ($type == 'array') {
                    if (!$required and $_first) {
                        $this->basehtml .= 'new Array("",""), ';
                    }
                    $this->basehtml .= 'new Array("'.$key.'","'.$value.'")';
                } elseif ($type=="json") {
                    if (!$required and $_first) {
                        $this->basehtml .= '"":"" ,';
                    }
                    $this->basehtml .= $key.':'.$value;
                } elseif ($type=="json_extended") {
                    if (!$required and $_first) {
                        $this->basehtml .= '{"":""} ,';
                    }
                    if (!empty($record->CASCADEPARENTFIELD)) {
                        $cascadevalue = json_encode($rec->{$record->CASCADEPARENTFIELD});
                        $this->basehtml .= "{ $supercolumn: $key, $supershowcolumnname: $value, $record->CASCADEPARENTFIELD: {$cascadevalue} }";
                    } else {
                        $this->basehtml .= "{ $supercolumn: $key, $supershowcolumnname: $value }";
                    }
                }
                $this->basehtml .= ',';

                if ($ADODB_EXTENSION) {
                    adodb_movenext($this->selectrs);
                } else {
                    $this->selectrs->MoveNext();
                }
                $_first = false;
            }
        };
        if (substr($this->basehtml, -1) == ',')
            $this->basehtml = substr($this->basehtml, 0, -1);

        if ($type == 'array') {
            $this->basehtml .= ');';
        } elseif ($type=="json") {
            $this->basehtml .= '};';
        } elseif ($type=="json_extended") {
            $this->basehtml .= '];';
        }
        $this->basehtml .= '</script>';

        return $this->basehtml;
    }

    function Show($state, $currentvalue, $rec, $required, $readonly, $recordid, $permissiontype)	{
        global $_GVARS;

        parent::Show($state, $currentvalue, $rec, $required, $readonly, $recordid, $permissiontype);
        $record =& $this->owner->record;
        $column = $record->COLUMNNAME;

        if ($this->inlistview and isset($recordid)) $column = $column.':'.$recordid;
        $labeltag = $this->GetLabelTag();
        if ($this->inlistview) $classes = 'cancelbubble';
        if ($classes != '') $classes = 'class="'.$classes.'"';
        $this->html = '';
        if ($this->isprocessed) {
            if (!$readonly && $state != 'view') {
                $data = [];
                if ($record->ISGROUPFIELD == 'Y') {
                    $sessionvar = $this->owner->owner->owner->owner->owner->record->CONSTRUCTID.'.'.GROUP_PREFIX.strtoupper($record->COLUMNNAME);
                    if ($_SESSION[$sessionvar] != '')
                        $currentvalue = $_SESSION[$sessionvar];
                }
                $supershowcolumnname = $record->SUPERSHOWCOLUMNNAME;
                if (strpos($supershowcolumnname, '.') !== false)
                    $supershowcolumnname = substr($supershowcolumnname, strpos($supershowcolumnname, '.') + 1);

                $supercolumn = $this->superrecord->SUPERCOLUMNNAME;
                if ($this->superrecord->DISTINCTSELECT == 'Y')
                    $supercolumn = 'DISTINCT '.$supercolumn;

                $superformurl = false;
                if ($record->SUPERFORM) {
                    $appobject =& $this->owner->owner->owner->owner->owner->AppObject();
                    $superformurl = $_GVARS['serverpath'].'/app/'.$appobject->record->METANAME.'/const/'.$record->SUPERFORM.'/insert/';
                    $data['url'] = $superformurl;
                }

                if ($record->CASCADEFORM) {
                    $appobject =& $this->owner->owner->owner->owner->owner->AppObject();
                    $data['cascade-parent'] = '_fld'.strtoupper($record->CASCADEPARENTFIELD);
                    $data['cascade-parent-field'] = strtoupper($record->CASCADEPARENTFIELD);
                    $data['cascade-idcolumn'] = strtoupper($record->SUPERCOLUMNNAME);
                    $data['cascade-showcolumn'] = strtoupper($record->SUPERSHOWCOLUMNNAME);
                    $data['cascade-valuepicklist'] = $record->COLUMNNAME.'_lookuppicklist';
                }
                $data['select_searchable'] = '20'; // minimum number of options before search is enabled

                $_idField = '_fld'.$column;
                $selectname = $this->owner->GetColumnName();
                $data['currentvalue'] = $currentvalue;
                $extra = $disabled.' '.$labeltag.' '.$classes.' '.$this->events;

                if ($this->optimizedselect) {
                    if ($this->displaycount == 0) {
                        $this->html .= $this->basehtml;
                    }
                    $this->displaycount++;
                    $nothing = false;
                    $this->html .= RecordSetAsSelectList($nothing, $supercolumn, $supershowcolumnname, $currentvalue, $_idField, $selectname, $extra, $required, $data);
                } else {
                    if ($this->multiselect) {
                        $extra .= " multiple='multiple'";
                        $data['placeholdertext'] = strtolower("een of meerdere {$selectname}");
                        $data['select_width'] = '90%'; // leave room for 'option' label
                        $selectname = $selectname.JSONMULTIFIELD.'[]';
                    } else {
                        $data['select_width'] = '300px';
                    }
                    if ($record->CHECKBOXGROUP == 'Y') {
                        $this->html .= RecordSetAsCheckboxGroup($this->selectrs, $supercolumn, $supershowcolumnname, $currentvalue, $_idField, $selectname, $required, $classes = '', $extra);
                    } else {
                        $this->html .= RecordSetAsSelectList($this->selectrs, $supercolumn, $supershowcolumnname, $currentvalue, $_idField, $selectname, $extra, $required, $data, $this->multiselect);
                    }
                }
            } else {
                /* Show a NORMAL edit field if the line is readonly */
                $database = $this->owner->owner->owner->owner->owner->database;
                $arrayOfSuperValues = GetRecordAsArray($database, $tablename = $this->superrecord->SUPERTABLENAME, [$this->superrecord->SUPERCOLUMNNAME => $currentvalue], datatype:$record->DATATYPE);
                $valuecolumnname = strtoupper($record->SUPERSHOWCOLUMNNAME);
                // Hidden input field
                $this->html .= "<input type='hidden' id='_fld{$column}' name='".$this->owner->GetColumnName()."' value='".$currentvalue."' />";

                // Use array_map to collect values and implode to join them with commas
                $superValuesArray = array_map(function($supervalue) use ($valuecolumnname) {
                    return $supervalue[$valuecolumnname];
                }, $arrayOfSuperValues);

                $this->html .= implode(', &nbsp;', $superValuesArray);
            }
        }

        return $this->html;
    }
}

class PLR_OptionGroup extends PLR_GuiElement {
    var $items;

    function __construct($userdb, $lineobject, $inlistview, $autofocus=false) {

        parent::__construct($userdb, $lineobject, $inlistview, $autofocus);

        if (!empty($this->owner->record->LISTVALUES)) {
            $this->items = explode(';', trim($this->owner->record->LISTVALUES));
        } else {
            $this->userdb->SetFetchMode(ADODB_FETCH_ASSOC);
            $this->items = $this->GetAllListItems();
        }
        $this->isprocessed = true;
    }

    function Show($state, $currentvalue, $rec, $required, $readonly, $recordid, $permissiontype) {
        parent::Show($state, $currentvalue, $rec, $required, $readonly, $recordid, $permissiontype);

        $record =& $this->owner->record;
        $validationformat = $this->owner->GetValidationFormat();
        $column = $record->COLUMNNAME;
        if ($this->inlistview and isset($recordid)) {
            $column = $column.':'.$recordid;
            $classes = 'cancelbubble pause_countdown';
        }
        $html = '';
        $labeltag = $this->GetLabelTag();
        $_idField = '_fld'.$column;
        $selectname = $this->owner->GetColumnName();
        if ($state == 'view') {
            $html = $this->owner->GetListValue($currentvalue);
            if ($this->owner->record->CSSIDENTIFIER == 'Y') {
                $html = "<div class='badge badge-success'>".$html."</div>";
            }
        } elseif ($readonly) {
            $html = "<input type='hidden' $labeltag {$this->readonlytag} class='text validate {$required} {$this->readonlyclass} $validationformat $cancelbubble' id='_fld".$column."' name='".$this->owner->GetColumnName()."' size='$size' $maxlengthcode value='$currentvalue' $events />";
            $currentvalue = $this->owner->GetListValue($currentvalue);
            $html .= "<input type='text' $labeltag {$this->readonlytag} class='form-control textx validate {$required} {$this->readonlyclass} $validationformat $cancelbubble' id='_fld".$this->owner->GetColumnName()."_lookup' size='$size' $maxlengthcode value='$currentvalue' $events />";
        } else {
            /* Show a LISTVALUES dropdown box OR radio buttons */
            if ($record->OPTIONGROUP == 'Y') {
                if (is_array($this->items)) {
                    $html = SetAsOptionGroup($this->items, $record->FULLNAME, $currentvalue, $_idField, $selectname, $required, $classes, $extra = $labeltag.' '.$this->events);
                } else {
                    $supershowcolumnname = $record->SUPERSHOWCOLUMNNAME;
                    if (strpos($supershowcolumnname, '.') !== false)
                        $supershowcolumnname = substr($supershowcolumnname, strpos($supershowcolumnname, '.') + 1);

                    $supercolumn = $this->superrecord->SUPERCOLUMNNAME;
                    if ($this->superrecord->DISTINCTSELECT == 'Y')
                        $supercolumn = 'DISTINCT '.$supercolumn;

                    $html = RecordSetAsOptionGroup($this->items, $supercolumn, $supershowcolumnname, $currentvalue, $_idField, $selectname, extra:'', required:$required);
                }

            } elseif ($record->CHECKBOXGROUP == 'Y') {
                $orientation = strpos($record->FORMATPARAMS, 'binarygroup-vertical' ) === false?'horizontal':'vertical';
                $html = SetAsCheckboxGroup($this->items, $record->FULLNAME, $currentvalue, $id = '_fld'.$column, $selectname = $column, $required, $classes, $extra = $labeltag.' '.$this->events, $orientation);
            } else {
                $html = SetAsSelectList($this->items, $record->FULLNAME, $currentvalue, $id = '_fld'.$column, $selectname = $column, $required, $classes);
            }
        }
        return $html;
    }

}

class PLR_ComponentField extends PLR_GuiElement {

    function __construct($userdb, $lineobject, $inlistview, $autofocus=false) {
        parent::__construct($userdb, $lineobject, $inlistview, $autofocus, $optimizedselect=false);

        $this->isprocessed = true;
    }

    function Show($state, $currentvalue, $rec, $required, $readonly, $recordid, $permissiontype) {
        parent::Show($state, $currentvalue, $rec, $required, $readonly, $recordid, $permissiontype);

        $record =& $this->owner->record;
        if ($record->FORMAT != '') {
            $componentname  = strtolower($record->FORMAT);
            require_once('components/component.'.$componentname.'.php');
            $classname = $componentname.'Component()';
            eval("\$component = new $classname;");
            $html = $component->GenerateXHTML(array($this->owner->owner->owner->owner->owner, $rec, $record->COLUMNNAME, $this->userdb, $record->FORMATPARAMS));
        }

        return $html;
    }

}

class PLR_EditField extends PLR_GuiElement {
    var $setarray;

    function __construct($userdb, $lineobject, $inlistview, $autofocus = false) {
        parent::__construct($userdb, $lineobject, $inlistview, $autofocus, $optimizedselect = false);
        $this->isprocessed = true;
    }

    function Show($state, $currentvalue, $rec, $required, $readonly, $recordid, $permissiontype) {
        parent::Show($state, $currentvalue, $rec, $required, $readonly, $recordid, $permissiontype);

        $record = &$this->owner->record;
        $validationformat = $this->owner->GetValidationFormat();
        $labeltag = $this->GetLabelTag();
        $column = $record->COLUMNNAME;

        if ($this->inlistview && isset($recordid)) {
            $column .= ":$recordid";
            $cancelbubble = 'cancelbubble';
        }

        // Determine input type
        $inputtype = $this->determineInputType($record);

        // Determine max size and length attributes
        [$maxsizingclass, $sizecode, $maxlengthcode] = $this->determineSizingAttributes($record);

        // Handle default values
        $thedefaultvalue = $this->handleDefaultValue($record, $state, $currentvalue, $permissiontype);

        // Display value
        $displayvalue = $this->ProcessDisplayValue($currentvalue);
        if ($inputtype == 'color') {
            $formcontrol = '';
        } else {
            $formcontrol = 'form-control';
        }

        $editfield = "<input type='$inputtype' $labeltag {$this->readonlytag} class='{$formcontrol} text validate $maxsizingclass {$this->requiredclass} {$this->readonlyclass} $validationformat' id='_fld$column' name='{$this->owner->GetColumnName()}' $sizecode $maxlengthcode value='$currentvalue' {$this->events} />";
        $editfield_noname = "<input type='hidden' id='_fld$column' value='$currentvalue' />";

        // Render HTML based on state
        $html = $this->renderHtml($state, $readonly, $currentvalue, $thedefaultvalue, $displayvalue, $editfield, $editfield_noname, $record);

        return $html;
    }

    private function determineInputType($record) {
        if ($record->DATATYPE == 'P') {
            return 'password';
        } elseif ($record->FORMAT == 'COLOR') {
            return 'color';
        } elseif (($record->VISIBLE == 'Y' && !$this->inlistview) || ($record->VISIBLEINLIST == 'Y' && $this->inlistview)) {
            return 'text';
        } else {
            return 'hidden';
        }
    }

    private function determineSizingAttributes($record) {
        if ($this->inlistview) $realmaxlength = 20; else $realmaxlength = 50;

        if ($record->TOTALLENGTH > $realmaxlength) {
            $maxsizingclass = 'formcontrol-max-size';
            $sizecode = '';
        } else {
            $size = min($record->TOTALLENGTH, $realmaxlength);
            $maxsizingclass = '';
            $sizecode = "size='$size'";
        }

        $maxlengthcode = $record->TOTALLENGTH != 0 ? "maxlength='" . trim($record->TOTALLENGTH) . "'" : '';

        return [$maxsizingclass, $sizecode, $maxlengthcode];
    }

    private function handleDefaultValue($record, $state, &$currentvalue, $permissiontype) {
        $thedefaultvalue = $this->DefaultValue();

        if ($thedefaultvalue !== null && $this->inlistview &&
            (($permissiontype & INSERT) == INSERT || ($permissiontype & UPDATE) == UPDATE)) {

            if ($thedefaultvalue[0] == '%' && $thedefaultvalue[strlen($thedefaultvalue) - 1] == '%') {
                $sessionfield = substr($thedefaultvalue, 1, -1);
                $currentvalue = $_SESSION[$sessionfield];
            } elseif (substr($thedefaultvalue, 0, 2) == '__' && substr($thedefaultvalue, -2) == '__') {
                $currentvalue = $thedefaultvalue;
            }
        }

        return $thedefaultvalue;
    }

    private function renderHtml($state, $readonly, $currentvalue, $thedefaultvalue, $displayvalue, $editfield, $editfield_noname, $record) {
        if ($record->COLUMNVISIBLE == 'Y') {
            switch ($state) {
                case 'insert':
                case 'detail_insert':
                    return $readonly && $thedefaultvalue === ''
                        ? $this->renderReadonlyLabel($currentvalue, $displayvalue, $editfield_noname)
                        : $editfield;
                case 'edit':
                case 'detail_edit':
                        return $readonly && $record->FORCEDEFAULTONUPDATE == 'N'
                        ? $editfield
                        : $editfield;
                case 'view':
                    return $displayvalue;
                default:
                    return $displayvalue;;
            }
        }
        return '';
    }

    private function renderReadonlyLabel($currentvalue, $displayvalue, $editfield_noname) {
        $html = $editfield_noname;
        if ($currentvalue != $displayvalue) {
            $html .= '<label class="hint" title="' . $currentvalue . '">';
            $html .= $displayvalue;
            $html .= '</label>';
        } else {
            $html .= $displayvalue;
        }
        return $html;
    }
}

class PLR_EmailField extends PLR_EditField {

    function Show($state, $currentvalue, $rec, $required, $readonly, $recordid, $permissiontype) {
        global $_GVARS;

        # Show the normal editfield
        $html = parent::Show($state, $currentvalue, $rec, $required, $readonly, $recordid, $permissiontype);

        # Show the mailto link
        // For now, no email link... should be something more intelligent
        // $html .= " <a title=\"".lang_get('click_to_reply')."\" href=\"mailto:$currentvalue\">".lang_get('send_email')."</a> <i class=\"fa fa-letter\"></i>";

        return $html;
    }
}

class PLR_ImageUploader extends PLR_GuiElement {

    function __construct($userdb, $lineobject, $inlistview, $autofocus=false) {
        parent::__construct($userdb, $lineobject, $inlistview, $autofocus, $optimizedselect=false);

        $this->isprocessed = true;
    }

    function Show($state, $currentvalue, $rec, $required, $readonly, $recordid, $permissiontype) {
        parent::Show($state, $currentvalue, $rec, $required, $readonly, $recordid, $permissiontype);

        $record =& $this->owner->record;
        if (isset($record->FORMATPARAMS)) $formatparams = explode(':', $record->FORMATPARAMS);
        $html = '<script type="text/javascript" src="/assets/dist/dropzone-min.js"></script>';
        $html .= '<link rel="stylesheet" type="text/css" media="all" href="/assets/dist/dropzone.css" />';
        if (empty($currentvalue)) {
            $currentvalue = 'data:image/svg+xml;base64,PHN2ZyBoZWlnaHQ9IjI0My42MDQiIHZpZXdCb3g9IjAgMCAzMjkuNzc4IDQwNi4wMDciIHdpZHRoPSIxOTcuODY3IiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIj48bGluZWFyR3JhZGllbnQgaWQ9ImEiIGdyYWRpZW50VHJhbnNmb3JtPSJtYXRyaXgoLjk2IC4yODEgLS4yODEgLjk2IC00MjkuMTYxIC0xMDIuNzk5KSIgZ3JhZGllbnRVbml0cz0idXNlclNwYWNlT25Vc2UiIHgxPSI2NjAuMDY3IiB4Mj0iNjU4LjQ1OCIgeTE9IjYuNiIgeTI9IjEwOC43NzYiPjxzdG9wIG9mZnNldD0iMCIgc3RvcC1jb2xvcj0iI2MyYzJjMiIvPjxzdG9wIG9mZnNldD0iMSIgc3RvcC1jb2xvcj0iIzlmOWY5ZiIvPjwvbGluZWFyR3JhZGllbnQ+PHBhdGggZD0ibTcuMDE0IDBoMzE1Ljc1YzMuODc0IDAgNy4wMTQgMy4xNCA3LjAxNCA3LjAxNHYzOTEuOThjMCAzLjg3My0zLjE0IDcuMDEzLTcuMDE0IDcuMDEzaC0zMTUuNzVjLTMuODc0IDAtNy4wMTQtMy4xNC03LjAxNC03LjAxM3YtMzkxLjk4YzAtMy44NzQgMy4xNC03LjAxNCA3LjAxNC03LjAxNHoiIGZpbGw9IiNlZWUiLz48cGF0aCBkPSJtODUuNjQ1IDEyNS4yMDkgNjMuMDQxLTE4LjQ4NmM0LjQ1LTEuMzA1IDkuMTE2IDEuMjQ1IDEwLjQyMSA1LjY5NWwyMC43MzEgNzAuNjk5YzEuMzA1IDQuNDUtMS4yNDUgOS4xMTYtNS42OTUgMTAuNDIxbC02My4wNDIgMTguNDg2Yy00LjQ1IDEuMzA0LTkuMTE1LTEuMjQ1LTEwLjQyLTUuNjk1bC0yMC43MzEtNzAuNjk5Yy0xLjMwNS00LjQ1MSAxLjI0NC05LjExNiA1LjY5NS0xMC40MjF6IiBmaWxsPSIjOWY5ZjlmIi8+PHBhdGggZD0ibTE0My40NTQgNjYuMjE5IDk0LjY2NyAyNy43MTljMi40MzYuNzEzIDMuODMzIDMuMjY2IDMuMTE5IDUuNzAzbC0zMC4wMDQgMTAyLjQ3Yy0uNzEzIDIuNDM2LTMuMjY3IDMuODMzLTUuNzAzIDMuMTJsLTk0LjY2Ni0yNy43MmMtMi40MzYtLjcxMy0zLjgzMy0zLjI2Ni0zLjEyLTUuNzAybDMwLjAwNS0xMDIuNDcxYy43MTMtMi40MzYgMy4yNjYtMy44MzMgNS43MDItMy4xMTl6IiBmaWxsPSJ1cmwoI2EpIi8+PHBhdGggZD0ibTE0My40NTQgNjYuMjE5IDk0LjY2NyAyNy43MTljMi40MzYuNzEzIDMuODMzIDMuMjY2IDMuMTE5IDUuNzAzbC0zMC4wMDQgMTAyLjQ3Yy0uNzEzIDIuNDM2LTMuMjY3IDMuODMzLTUuNzAzIDMuMTJsLTk0LjY2Ni0yNy43MmMtMi40MzYtLjcxMy0zLjgzMy0zLjI2Ni0zLjEyLTUuNzAybDMwLjAwNS0xMDIuNDcxYy43MTMtMi40MzYgMy4yNjYtMy44MzMgNS43MDItMy4xMTl6IiBmaWxsPSJub25lIiBzdHJva2U9IiNmZmYiIHN0cm9rZS1saW5lY2FwPSJyb3VuZCIgc3Ryb2tlLWxpbmVqb2luPSJyb3VuZCIgc3Ryb2tlLXdpZHRoPSI1LjM5OCIvPjxwYXRoIGQ9Im0xNDcuNTIgNzguNDEgODEuMzc1IDIzLjgyNy0yMS44MzMgNzQuNTY0LTgxLjM3NS0yMy44Mjh6IiBmaWxsPSIjY2NjIi8+PHBhdGggZD0ibTE0Ny41MiA3OC40MSA4MS4zNzUgMjMuODI3LTIxLjgzMyA3NC41NjQtODEuMzc1LTIzLjgyOHoiIGZpbGw9Im5vbmUiIHN0cm9rZT0iI2ZmZiIgc3Ryb2tlLWxpbmVjYXA9InJvdW5kIiBzdHJva2UtbGluZWpvaW49InJvdW5kIiBzdHJva2Utd2lkdGg9IjIuMjU3Ii8+PHBhdGggZD0ibTE0Ny41MiA3OC40MSA4MS4zNzUgMjMuODI3LTEwLjE4OCAzNC43OTctODEuMzc1LTIzLjgyOHoiIGZpbGw9IiNmZmYiLz48cGF0aCBkPSJtMTQ3LjUyIDc4LjQxIDgxLjM3NSAyMy44MjctMTAuMTg4IDM0Ljc5Ny04MS4zNzUtMjMuODI4eiIgZmlsbD0ibm9uZSIgc3Ryb2tlPSIjZmZmIiBzdHJva2UtbGluZWNhcD0icm91bmQiIHN0cm9rZS1saW5lam9pbj0icm91bmQiIHN0cm9rZS13aWR0aD0iMi4yNTciLz48cGF0aCBkPSJtMTQyLjMwMiAxMTQuNjYyIDguOTcxLTcuMDA1IDEyLjIyMS00LjU0NyAxLjczIDMuODQ5IDQuMTMzLTUuNzUgMS41MDQtLjA5NiAzLjU3NC00LjAyOCAyMS4wOTIgMzguNTU5IiBmaWxsPSIjY2NjIi8+PHBhdGggZD0ibTE0Mi4zMDIgMTE0LjY2MiA4Ljk3MS03LjAwNSAxMi4yMjEtNC41NDcgMS43MyAzLjg0OSA0LjEzMy01Ljc1IDEuNTA0LS4wOTYgMy41NzQtNC4wMjggMjEuMDkyIDM4LjU1OSIgZmlsbD0ibm9uZSIgc3Ryb2tlPSIjZmZmIiBzdHJva2UtbGluZWNhcD0icm91bmQiIHN0cm9rZS1saW5lam9pbj0icm91bmQiIHN0cm9rZS13aWR0aD0iMi4yNTciLz48cGF0aCBkPSJtMTc0LjQzNSA5Ny4wODVjLS4xMjEgMi45MjgtLjMyNyA1Ljg1My0uNjE2IDguNzctLjIxOSAyLjIxMy0uNDg3IDQuNDIyLS44MDMgNi42MjRsLTMuMzA5LTIuMjU3LTYuMjEzLTcuMTEyLTEuMzcgNy45NTEtMS40NzEgNC4wMjkuMTkyIDUuNTIxIDEyLjY5NyAxLjYwMyAxNC45NTQuNTc3LTQuMTYzLTE1LjE1LTIuMzMyIDIuMDA0LTMuNDIxLTQuOTgyeiIgZmlsbD0iI2IzYjNiMyIvPjxwYXRoIGQ9Im0xNzQuNDM1IDk3LjA4NWMtLjEyMSAyLjkyOC0uMzI3IDUuODUzLS42MTYgOC43Ny0uMjE5IDIuMjEzLS40ODcgNC40MjItLjgwMyA2LjYyNGwtMy4zMDktMi4yNTctNi4yMTMtNy4xMTItMS4zNyA3Ljk1MS0xLjQ3MSA0LjAyOS4xOTIgNS41MjEgMTIuNjk3IDEuNjAzIDE0Ljk1NC41NzctNC4xNjMtMTUuMTUtMi4zMzIgMi4wMDQtMy40MjEtNC45ODJ6IiBmaWxsPSJub25lIiBzdHJva2U9IiNmZmYiIHN0cm9rZS1saW5lY2FwPSJyb3VuZCIgc3Ryb2tlLWxpbmVqb2luPSJyb3VuZCIgc3Ryb2tlLXdpZHRoPSIyLjI1NyIvPjxwYXRoIGQ9Im0xNjAuNzMxIDEyNS40NTUgMTcuMTgxLTguMzU5IDMuNTM2LS43MDcgNi4wNjMtNi4xNzQgNS4zNTItMi4zMzcgMjEuMDkyIDM4LjU1OSIgZmlsbD0iI2NjYyIvPjxwYXRoIGQ9Im0xNjAuNzMxIDEyNS40NTUgMTcuMTgxLTguMzU5IDMuNTM2LS43MDcgNi4wNjMtNi4xNzQgNS4zNTItMi4zMzcgMjEuMDkyIDM4LjU1OSIgZmlsbD0ibm9uZSIgc3Ryb2tlPSIjZmZmIiBzdHJva2UtbGluZWNhcD0icm91bmQiIHN0cm9rZS1saW5lam9pbj0icm91bmQiIHN0cm9rZS13aWR0aD0iMi4yNTciLz48cGF0aCBkPSJtMTkyLjg2MyAxMDcuODc4IDIuMDI5IDE1LjE1MS0xLjQ2MSA0Ljk4OS0xLjQ2MSA0Ljk4OSAyLjc3NyA0LjMxNWMxLjM5NC0uNzUyIDIuOTE2LTEuMjY2IDQuNDgxLTEuNTEzIDIuNzk2LS40NDEgNS42NDctLjAzIDguNDc3LjAzMiAxLjE0OS4wMjUgMi4zMjYtLjAxMyAzLjQtLjQyMS4yNzQtLjEwNC41MzgtLjIzMi43OS0uMzgxbC00Ljk3MS0xLjQ1NS00Ljc0Ni04LjY4NS0yLjU3NC05LjU1NS0yLjEyOC45Njd6IiBmaWxsPSIjYjNiM2IzIi8+PHBhdGggZD0ibTE5Mi44NjMgMTA3Ljg3OCAyLjAyOSAxNS4xNTEtMS40NjEgNC45ODktMS40NjEgNC45ODkgMi43NzcgNC4zMTVjMS4zOTQtLjc1MiAyLjkxNi0xLjI2NiA0LjQ4MS0xLjUxMyAyLjc5Ni0uNDQxIDUuNjQ3LS4wMyA4LjQ3Ny4wMzIgMS4xNDkuMDI1IDIuMzI2LS4wMTMgMy40LS40MjEuMjc0LS4xMDQuNTM4LS4yMzIuNzktLjM4MWwtNC45NzEtMS40NTUtNC43NDYtOC42ODUtMi41NzQtOS41NTUtMi4xMjguOTY3eiIgZmlsbD0ibm9uZSIgc3Ryb2tlPSIjZmZmIiBzdHJva2UtbGluZWNhcD0icm91bmQiIHN0cm9rZS1saW5lam9pbj0icm91bmQiIHN0cm9rZS13aWR0aD0iMi4yNTciLz48cGF0aCBkPSJtMTUwLjU0MiAxNjAuMjUxYy4zOTctMS42MTEtLjYwNy0zLjM0NSA0LjMyNS00LjI1IDYuMDUyLTEuMTExIDIyLjYwMi4xNjEgMjguMTIyLTIuNTU4IDQuMDg0LTIuMDExLTkuODQtOS42NTgtNi4yNS0xMi40NTcgMS42MjctMS4yNyAxMC43NTEtLjQ0NSAxNS4wOC0xLjAzMSA0LjEwNC0uNTU2IDMuNDctMi41NDcgNi42NTgtMy40NDMgMy4wOC0uODY2IDEyLjI4NS4wNCAxMy40MTgtMS40NzMtMi4yMzYgMS4yMzgtMTIuMzk1LjA0MS0xNi4zNjguNjA1LTQuMTkzLjU5NC00LjUyNCAyLjI2LTkuNjIxIDIuNDg3LTUuNDkxLjI0NC0xNS43NjgtLjkyLTE4LjQ5OS4wNzMtNS43MjEgMi4wNzkgNS42OTggOS4zMzItLjIyNiAxMC43MjgtOC4wMjMgMS44OS0yNi44NDYtLjA3Mi0zNS4wNjcuNTI2LTUuNjgxLjQxNC01LjU2OSAxLjU2My02LjQyNyAzLjUxNiIgZmlsbD0iI2ZmZiIvPjxwYXRoIGQ9Im0xNTAuNTQyIDE2MC4yNTFjLjM5Ny0xLjYxMS0uNjA3LTMuMzQ1IDQuMzI1LTQuMjUgNi4wNTItMS4xMTEgMjIuNjAyLjE2MSAyOC4xMjItMi41NTggNC4wODQtMi4wMTEtOS44NC05LjY1OC02LjI1LTEyLjQ1NyAxLjYyNy0xLjI3IDEwLjc1MS0uNDQ1IDE1LjA4LTEuMDMxIDQuMTA0LS41NTYgMy40Ny0yLjU0NyA2LjY1OC0zLjQ0MyAzLjA4LS44NjYgMTIuMjg1LjA0IDEzLjQxOC0xLjQ3My0yLjIzNiAxLjIzOC0xMi4zOTUuMDQxLTE2LjM2OC42MDUtNC4xOTMuNTk0LTQuNTI0IDIuMjYtOS42MjEgMi40ODctNS40OTEuMjQ0LTE1Ljc2OC0uOTItMTguNDk5LjA3My01LjcyMSAyLjA3OSA1LjY5OCA5LjMzMi0uMjI2IDEwLjcyOC04LjAyMyAxLjg5LTI2Ljg0Ni0uMDcyLTM1LjA2Ny41MjYtNS42ODEuNDE0LTUuNTY5IDEuNTYzLTYuNDI3IDMuNTE2IiBmaWxsPSJub25lIiBzdHJva2U9IiNmZmYiIHN0cm9rZS1saW5lY2FwPSJyb3VuZCIgc3Ryb2tlLWxpbmVqb2luPSJyb3VuZCIgc3Ryb2tlLXdpZHRoPSIyLjI1NyIvPjx0ZXh0IHRyYW5zZm9ybT0idHJhbnNsYXRlKDE2Mi44NzIgMjc4LjMzNSkiPjx0c3BhbiBmaWxsPSIjNzY3Njc2IiBmb250LWZhbWlseT0iSGVsdmV0aWNhIiBmb250LXNpemU9IjMyIiB4PSItODAuMDA4IiB5PSIxMSI+Tk8gSU1BR0U8L3RzcGFuPjwvdGV4dD48dGV4dCB0cmFuc2Zvcm09InRyYW5zbGF0ZSgxNjIuODcyIDMxOC4zMzUpIj48dHNwYW4gZmlsbD0iIzc2NzY3NiIgZm9udC1mYW1pbHk9IkhlbHZldGljYSIgZm9udC1zaXplPSIzMiIgeD0iLTgzLjkxNCIgeT0iMTEiPkFWQUlMQUJMRTwvdHNwYW4+PC90ZXh0Pjwvc3ZnPg==';
        } else {
            $currentvalue = 'data:image/jpeg;base64,'.$currentvalue;
        }
        $html .= '
        <div class="row">
            <div class="col-md-6">
                <img id="FOTOPREVIEW" style="max-height:300px" src="'.$currentvalue.'"/>
            </div>
            <div class="col-md-6">
                <div id="dropzone" class="dropzone">
                    <input type="hidden" id="_hdnFOTO" name="FOTO">
                </div>
            </div>
        </div>
        ';

        return $html;
    }

}

class PLR_ImageUploaderV2 extends PLR_GuiElement {

    function __construct($userdb, $lineobject, $inlistview, $autofocus=false) {
        parent::__construct($userdb, $lineobject, $inlistview, $autofocus, $optimizedselect=false);

        $this->isprocessed = true;
    }

    function Show($state, $currentvalue, $rec, $required, $readonly, $recordid, $permissiontype) {
        global $polaris;

        parent::Show($state, $currentvalue, $rec, $required, $readonly, $recordid, $permissiontype);

        $record =& $this->owner->record;

        $_imagelist = explode(',', $currentvalue);
        $_form = $this->owner->owner->owner->owner->owner;

        if (count($_imagelist) > 0 and !empty($currentvalue)) {
            $_implodedList = implode("','", $_imagelist);
            $_recordid = $_form->database->makeRecordIDColumn('MX_MEDIABEHEER', true);
            $_sql = "
            SELECT {$_recordid} AS PLR__RECORDID, CLOUD_IMAGE_ID, BESTANDSNAAM, MEDIA_ID
            FROM MX_MEDIABEHEER ttt
            WHERE {$_recordid} IN ('{$_implodedList}')
            AND {$_form->AddDataScopeFilter()}
            ";
            $_RSimages = $this->userdb->GetAssoc($_sql);
        }

        $cfi = new bartb\CloudFlare\Images($_ENV['CLOUDFLARE_IMAGES_ACCOUNT'], $_ENV['CLOUDFLARE_IMAGES_TOKEN'], $_ENV['CLOUDFLARE_ACCOUNT_HASH'], $_ENV['CLOUDFLARE_IMAGES_DELIVERY_URL']);

        $_columnname = $this->owner->record->COLUMNNAME;
        $_html = "<input type='hidden' style='width:100%;' id='_fld{$_columnname}' name='{$_columnname}' value='{$currentvalue}' />";
        $_html .= '<button type="button" class="image-uploader-add btn btn-sm btn-success">Voeg afbeelding toe...</button>';
        // $_html .= '<hr class="cleaner">';
        $_html .= '<div id="image-upload" class="sortable-media shared-media">';
        foreach($_imagelist as $image_id) {
            if (!empty($image_id)) {
                if (!empty($_RSimages[$image_id]['CLOUD_IMAGE_ID'])) {
                    $imageURL = $cfi->getImageUrl($_RSimages[$image_id]['CLOUD_IMAGE_ID'], 'thumbnail');
                } else {
                    // deleted image so show placeholder
                    $imageURL = '/assets/images/deleted_file_placeholder.png';
                }
                $_html .= '<div class="sortable-draggable" id="'.$image_id.'">
                        <div class="media-image sortable-handle">
                            <img src="'.$imageURL.'" class="rounded img-thumbnail img-responsive" />
                            <p class="bestandsnaam">'.$_RSimages[$image_id]['BESTANDSNAAM'].'</p>
                        </div>
                    </div>';
}
        }
        $_html .= '</div>';
        $_html .= '<script type="text/javascript" src="/assets/dist/Sortable.min.js"></script>';
        $_html .= "<script type='text/javascript'>
        jQuery(function() {
            Polaris.Dataview.initializeSortableMedia('image-upload', {fieldid:'_fld{$_columnname}'});
            $('.image-uploader-add').on('click', function() {
                Polaris.Form.showImageUploader();
            });

            var _deleteImage = function(id) {
                var currentvalue = $('#_fld{$_columnname}').val();
                var valuearray = currentvalue.split(',');
                // remove id from array
                valuearray = valuearray.filter(function(item) {
                    return item !== id;
                });
                var newvalue = valuearray.join(',');
                $('#_fld{$_columnname}').val(newvalue);
                $('#'+id).remove();
            }

            $('.image-uploader-del').on('click', function() {
                var id = $('.sortable-draggable.selected').prop('id');
                _deleteImage(id);
            });
            $('.sortable-media .media-image').on('click', function() {
                $('.image-uploader-del').toggleClass('disabled', $('.sortable-draggable.selected').length == 0);
            });
            // if a key is pressed, check if it's the delete key and if so delete the selected image
            $(document).on('keydown', function(e) {
                // Backspace or Delete
                if (e.keyCode == 8 || e.keyCode == 46) {
                    var id = $('.sortable-draggable.selected').prop('id');
                    _deleteImage(id);
                }
            });
        });
        </script>";

        return $_html;
    }

}

class PLR_IconSelect extends PLR_GuiElement {

    function __construct($userdb, $lineobject, $inlistview, $autofocus=false) {
        global $_sqlSubsetItems;

        parent::__construct($userdb, $lineobject, $inlistview, $autofocus, $optimizedselect=false);

        $this->isprocessed = true;
    }

    function Show($state, $currentvalue, $rec, $required, $readonly, $recordid, $permissiontype) {
        parent::Show($state, $currentvalue, $rec, $required, $readonly, $recordid, $permissiontype);
        if ($state == 'insert' and $currentvalue == '' ) {
            $currentvalue = $this->DefaultValue();
            if ($currentvalue == '') {
                $currentvalue = 'help';
            }
        }
        $html = "
            <input type='hidden' id='_fld{$this->owner->record->COLUMNNAME}' name='{$this->owner->record->COLUMNNAME}' value='{$currentvalue}' />
            <div class='icons-picker-icon'>
                <i class='material-symbols-outlined md-3x' data-toggle='overlay' data-field='_fld{$this->owner->record->COLUMNNAME}' data-overlay='/services/xhtml/icons/'>{$currentvalue}</i>
                <div><div class='xicon-picker-name'>{$currentvalue}</div></div>
            </div>
        ";
        return $html;

    //     <div class='icon-picker'>
    //     <input type='hidden' id='_fld{$this->owner->record->COLUMNNAME}' name='{$this->owner->record->COLUMNNAME}' value='{$currentvalue}' />
    //     <i class='icon-picker-btn material-symbols-outlined md-4x' data-toggle='overlay' data-field='_fld{$this->owner->record->COLUMNNAME}' data-overlay='/services/xhtml/icons/'>{$currentvalue}</i>
    //     <div class='icon-picker-name'>{$currentvalue}</div>
    // </div>

    }

}

class PLR_TextField extends PLR_GuiElement {

    function __construct($userdb, $lineobject, $inlistview, $autofocus=false) {
        global $_sqlSubsetItems;

        parent::__construct($userdb, $lineobject, $inlistview, $autofocus, $optimizedselect=false);

        $this->isprocessed = true;
    }

    function Show($state, $currentvalue, $rec, $required, $readonly, $recordid, $permissiontype) {
        parent::Show($state, $currentvalue, $rec, $required, $readonly, $recordid, $permissiontype);
        $record =& $this->owner->record;
        if (isset($record->FORMATPARAMS)) $formatparams = explode(':', $record->FORMATPARAMS);
        $editorwidth = isset($formatparams[0]) ? $formatparams[0] : '100%';
        $editorheight = isset($formatparams[1]) ? $formatparams[1] : '200px';
        $_toolbarset = false;

        switch(strtoupper($record->FORMAT)) {
        case 'HTML':
            if (strtoupper($formatparams[3]) == 'TINYMCE') {
                $_pageProcessor = 'TINYMCE';
            } elseif (strtoupper($formatparams[3]) == 'CKE') {
                $_pageProcessor = 'CKE';
                $_toolbarset = isset($formatparams[0]) ? $formatparams[0] : 'Basic';
            } else {
                $_pageProcessor = 'FCK';
                $_toolbarset = isset($formatparams[0]) ? $formatparams[0] : 'Basic_Ext';
            }
            break;
        case 'MARKDOWN':
        case 'TEXTILE': // obsolete, replace by MARKDOWN in database
            $_pageProcessor = 'MARKDOWN';
            break;
        case 'SUMMERNOTE':
            $_pageProcessor = 'SUMMERNOTE';
            break;
        default:
                $_pageProcessor = ''; // normal TEXTAREA
            break;
        }
        $showeditor = false;

        include_once('classes/base/textEditor.class.php');
        $_textEditor = new TextEditor($_pageProcessor, $this->owner->GetColumnName(), $_toolbarset, $editorwidth, $editorheight, $required, $readonly, $this->maxlength, $this->autofocus);
        $_textEditor->setValue($currentvalue);

        $html .= $_textEditor->Create();
        return $html;
    }

}

class PLR_DateField extends PLR_GuiElement {

    function __construct($userdb, $lineobject, $inlistview, $autofocus=false) {
        global $_sqlSubsetItems;

        parent::__construct($userdb, $lineobject, $inlistview, $autofocus, $optimizedselect=false);

        $this->isprocessed = true;
    }

    function Show($state, $currentvalue, $rec, $required, $readonly, $recordid, $permissiontype) {
        global $_CONFIG;

        parent::Show($state, $currentvalue, $rec, $required, $readonly, $recordid, $permissiontype);

        $linerecord =& $this->owner->record;
        $column = $linerecord->COLUMNNAME;

        // if (!empty($linerecord->FORMATPARAMS)) {
        //     $datefmt = $linerecord->FORMATPARAMS;
        // } else {
        //     $_phpformatting = true;
        //     $datefmt =$this->owner->GetDateFormat($_phpformatting);
        // }
        $size = (strlen($currentvalue) > 10) ? 20 : 11;

        if (($permissiontype & UPDATE) == UPDATE and $linerecord->READONLY != 'Y') {
            $inputdate = 'date_input validation_date';
        }
        if ($state == 'view') {
            $html = $currentvalue;
        } else {
            $html = "<input type='text' id='_fld$column' name='".$this->owner->GetColumnName()."' $labeltag {$this->readonlytag} class='form-control text {$inputdate} {$this->requiredclass} {$this->readonlyclass}' value='$currentvalue' size='$size' />";
        }

        return $html;
    }
}

class PLR_TimeField extends PLR_GuiElement {

    function __construct($userdb, $lineobject, $inlistview, $autofocus=false) {
        global $_sqlSubsetItems;

        parent::__construct($userdb, $lineobject, $inlistview, $autofocus, $optimizedselect=false);

        $this->isprocessed = true;
    }

    function Show($state, $currentvalue, $rec, $required, $readonly, $recordid, $permissiontype) {
        parent::Show($state, $currentvalue, $rec, $required, $readonly, $recordid, $permissiontype);

        $linerecord =& $this->owner->record;
        $column = $linerecord->COLUMNNAME;

        if (($permissiontype & UPDATE) == UPDATE and $linerecord->READONLY != 'Y') {
            $inputtime = 'inputtime';
        }

        if (($linerecord->FORMAT??'') == 'PAIRED') {
            // Grouped date field, add class for datepicker
            // Formatparams contains the Start or End classifier
            $_grouped = "datepair {$linerecord->FORMATPARAMS}";
        }
        if ($state == 'view') {
            $html = $currentvalue;
        } else {
            $html = "<input type='text' id='_fld$column' name='".$this->owner->GetColumnName()."' $labeltag {$this->readonlytag} class='text {$_grouped} {$inputtime} {$this->requiredclass} {$this->readonlyclass}' value='$currentvalue' size='$size' />";
        }
        return $html;
    }
}

/*
    Wrapper functions for GUI element classes
*/
function PLR_CreateGUIComponent($lineobject, $linetype, $datatype, $supercolumnname, $listvalues, $format, $userdb, $inlistview, $autofocus, $optimizedselect) {

    if ($linetype == 'COMPONENT') {
        $_result = new PLR_ComponentField($userdb, $lineobject, $inlistview);
    // } elseif ($datatype == 'X' and $format == 'IMAGEUPLOADER') {
    //     $_result = new PLR_ImageUploader($userdb, $lineobject, $inlistview, $autofocus);
    } elseif ($format == 'IMAGEUPLOADER2') {
        $_result = new PLR_ImageUploaderV2($userdb, $lineobject, $inlistview, $autofocus);
    } elseif ($format == 'ICONSELECT') {
        $_result = new PLR_IconSelect($userdb, $lineobject, $inlistview, $autofocus);
    } elseif ($datatype == 'X') {
        $_result = new PLR_TextField($userdb, $lineobject, $inlistview, $autofocus);
    } elseif ($datatype == 'D' or $format == 'DATE') {
        $_result = new PLR_DateField($userdb, $lineobject, $inlistview);
    } elseif ($datatype == 'T' or $format == 'TIMESTAMP') {
        $_result = new PLR_DateField($userdb, $lineobject, $inlistview);
    } elseif ($datatype == 'H') {
        $_result = new PLR_TimeField($userdb, $lineobject, $inlistview);
    } elseif ($datatype == 'J' and $format == 'MULTISELECT' and $supercolumnname != '') {
        $_result = new PLR_LookupPicklist($userdb, $lineobject, $inlistview, $autofocus, $optimizedselect, multiselect:true);
    } elseif ($supercolumnname != '' and $lineobject->record->OPTIONGROUP == 'N') {
        $_result = new PLR_LookupPicklist($userdb, $lineobject, $inlistview, $autofocus, $optimizedselect);
    } elseif (trim($listvalues) != '' or $lineobject->record->OPTIONGROUP == 'Y') {
        $_result = new PLR_OptionGroup($userdb, $lineobject, $inlistview, $autofocus);
    } elseif (trim($format) == 'EMAIL') {
        $_result = new PLR_EmailField($userdb, $lineobject, $inlistview, $autofocus);
    } else {
        $_result = new PLR_EditField($userdb, $lineobject, $inlistview, $autofocus);
    }

    return $_result;
}
