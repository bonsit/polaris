<?php

class base_plrLine extends base_plrRecord {
    var $guielement = false;
    var $database;

    function __construct(&$aowner) {
        parent::__construct($aowner);
    }

    function LoadRecord() {
    }

    function ConcatColumns($joinarray, $column, $supercolumn, $datatype, $supertableprefix, $subsetid, $useinsearchfilter, $forgroupby = false, $format='') {
        global $TABLEPREFIX;

        $result = '';
        if (strpos($supercolumn,',') !== false) {
            // a series of columns
            if ((substr($supercolumn,0,4) != 'NVL(')
            and (substr($supercolumn,0,5) != 'NVL2(')
            and (substr($supercolumn,0,7) != 'CONCAT(')
            and (substr($supercolumn,0,10) != 'CONCAT_WS(')) {
                if ($this->database->userdb)
                    $_tmpdb =& $this->database->userdb;
                else
                    var_dump("PANIC!!!!");
                $result = $_tmpdb->Concat($supercolumn);
                if (strpos($_tmpdb->databaseType, 'mysql') !== false) {
                    // in case of mysql use CONCAT_WS because of the NULL value returned in case of a NULL column
                    $result = str_replace('CONCAT(','CONCAT_WS(\', \', ',$result);
                }
            } else {
                $result = $supercolumn;
            }

            if (!$useinsearchfilter) {
                $alias = $column.'_SSS'.$subsetid;
                $result .= " $alias ";
            }
        } else {
            // just a simple column
            if ($datatype == 'J') {
                if (!$useinsearchfilter) {
                    if ($this->record->FORMAT == 'MULTISELECT') {
                        // pr($joinarray, $column, $supercolumn, $datatype, $supertableprefix, $subsetid, $useinsearchfilter, $this->record);
                        $supertable = $joinarray[$TABLEPREFIX.'.'.$column]['SUPERTABLE'];
                        // pr($supertable, $TABLEPREFIX.$column);
                        $result = " (SELECT GROUP_CONCAT({$supertableprefix}.{$supercolumn} SEPARATOR ', ')
                            FROM {$supertable} {$supertableprefix}
                            WHERE JSON_CONTAINS({$TABLEPREFIX}.{$column}, JSON_QUOTE(CAST({$supertableprefix}.{$this->record->SUPERCOLUMNNAME} AS CHAR)), '$')
                        ) ";
                        // $return .= " GROUP_CONCAT({$tblprefix}{$this->record->SUPERSHOWCOLUMNNAME} SEPARATOR ', ') AS {$this->record->COLUMNNAME}_{$tableprefix}";
                        // GROUP_CONCAT(sss61.ontwikkelsfeer_naam SEPARATOR ', ') AS ONTWIKKELTYPES_sss61,
                    }
                }
            } else {
                $result = $supertableprefix.'.'.$supercolumn;
            }

            if (!$useinsearchfilter and !$forgroupby)
               $result .= " AS {$column}_{$supertableprefix}";
        }
        return $result;
    }

    function GetDateTimeFormat() {
        if ($this->record->DATATYPE == 'D') {
            return $this->GetDateFormat();
        } else {
            return $this->GetDateFormat().' '.$this->getTimeFormat();
        }
    }

    function GetColumns($tblprefix, $useinsearchfilter=false, $joinarray=false, $forgroupby=false) {
        global $_CONFIG;
        $appobj =& $this->AppObject();
        $dbobj = $this->owner->owner->owner->owner->database;
        $return = '';

        if ( ($this->record->LINETYPE == 'COLUMN' or $this->record->LINETYPE == 'COMPONENT') and
            (strpos($this->record->COLUMNNAME, '_DETAILCOUNT') === false )
            and $this->record->COLUMNVISIBLE == 'Y'
            and (!$useinsearchfilter
                or ($useinsearchfilter and $this->record->DATATYPE != 'R')
            )
        ) {
            if ($this->record->DATATYPE == 'H') {
                if (isset($appobj->owner) and $appobj->owner->record->TIMEFORMAT != '') {
                    $fmt = $appobj->owner->record->TIMEFORMAT;
                } else {
                    $fmt = $_CONFIG['default_time_format'];
                }
                if (!$useinsearchfilter and !$forgroupby) {
                    if ( $dbobj->userdb ) {
                        $return .= $dbobj->userdb->SQLDate($fmt, $tblprefix.$this->record->COLUMNNAME).' as '.$this->record->COLUMNNAME.' ';
                    } else {
                        $return .= $tblprefix.$this->record->COLUMNNAME;
                    }
                } else {
                    $return .= $tblprefix.$this->record->COLUMNNAME;
                }
            } elseif ($this->record->DATATYPE == 'D' or $this->record->DATATYPE == 'T') {
                $_fmt = $this->GetDateTimeFormat();
                if (!$useinsearchfilter and !$forgroupby) {
                    $return .= 'DATE_FORMAT('. $tblprefix.$this->record->COLUMNNAME .', \''.$_fmt.'\') as '.$this->record->COLUMNNAME.' ';
                    // $return .= 'DATE_FORMAT('. $tblprefix.$this->record->COLUMNNAME .', \''.$_fmt.'\') as '.$this->record->COLUMNNAME.' ';
                } else {
                    $return .= $tblprefix.$this->record->COLUMNNAME;
                }
                // if (!$useinsearchfilter and !$forgroupby) {
                // } else {
                //     if ( $dbobj->userdb and !$forgroupby) {
                //         // $_fmt = 'd-m-Y';
                //         $return .= $dbobj->userdb->SQLDate($_fmt, $tblprefix.$this->record->COLUMNNAME.' ').' as '.$this->record->COLUMNNAME.' ';;
                //     } else {
                //         $return .= $tblprefix.$this->record->COLUMNNAME;
                //     }
                // }
            } elseif (array_exists($this->record->FORMAT, 'SQL')) {
                $processedcolumn = str_replace('%columnname%', $this->record->COLUMNNAME, $this->record->FORMATPARAMS);
                if ( $useinsearchfilter ) {
                    $return .= $processedcolumn;
                } else {
                    $return .= $processedcolumn .' as '. $this->record->COLUMNNAME;
                }
            } else {
                if ( $useinsearchfilter) {
                    if (($this->record->SUPERSHOWCOLUMNNAME != '') and ($this->record->SUPERSHOWCOLUMNNAME != strtoupper($this->record->SUPERCOLUMNNAME))) {
                        if ($joinarray and ($joinarray[$tblprefix.$this->record->COLUMNNAME] != '')) {
                            if (!$dbobj->IsOracle()) /* no searching in subtables in Oracle, too slow */ {
                                $tableprefix = substr($joinarray[$tblprefix.$this->record->COLUMNNAME]['COLUMN']
                                    , 0
                                    , strpos($joinarray[$tblprefix.$this->record->COLUMNNAME]['COLUMN'],'.'));
                                $return .= $this->ConcatColumns($joinarray, $this->record->COLUMNNAME, $this->record->SUPERSHOWCOLUMNNAME, $this->record->DATATYPE, $tableprefix, $this->record->SUBSETID, $useinsearchfilter, $forgroupby, $this->record->FORMAT ).' , '; /* LET OP spaties rondom komma*/
                            }
                        } else {
                            if (!$dbobj->IsOracle()) /* no searching in subtables in Oracle, too slow */ {
                                $return .= $this->record->SUPERSHOWCOLUMNNAME.' , ';
                            }
                        }
                    }
                    if (preg_match('/^[a-zA-Z0-9_]{1,}$/', $this->record->COLUMNNAME)) {
                        $return .= $tblprefix.$this->record->COLUMNNAME;
                    } else {
                        $return .= $this->record->COLUMNNAME;
                    }
                } else {
                    if (($this->record->SUPERSHOWCOLUMNNAME != '') and ($this->record->SUPERSHOWCOLUMNNAME != strtoupper($this->record->SUPERCOLUMNNAME))) {
                        if (!empty($joinarray)) {
                            $_scn = $joinarray[$tblprefix.$this->record->COLUMNNAME]['COLUMN'];
                            $tableprefix = substr($_scn, 0, strpos($_scn,'.'));
                            if ($_scn != '') {
                                $return .= $this->ConcatColumns($joinarray, $this->record->COLUMNNAME, $this->record->SUPERSHOWCOLUMNNAME, $this->record->DATATYPE, $tableprefix, $this->record->SUBSETID, $useinsearchfilter, $forgroupby, $this->record->FORMAT).' , ';

                            // } elseif ($this->record->DATATYPE == 'J') {
                            //     if (!$useinsearchfilter) {
                            //         pr($this->record->FORMAT);
                            //         if ($this->record->FORMAT == 'MULTISELECT') {
                            //             $return .= " (SELECT GROUP_CONCAT(sss61.ontwikkelsfeer_naam SEPARATOR ', ')
                            //              FROM fb_ontwikkelsferen sss61
                            //              WHERE JSON_CONTAINS(ttt.ONTWIKKELTYPES, JSON_QUOTE(CAST(sss61.ONTWIKKELSFEER_ID AS CHAR)), '$')
                            //             ) AS ONTWIKKELTYPES_sss61";
                            //             // $return .= " GROUP_CONCAT({$tblprefix}{$this->record->SUPERSHOWCOLUMNNAME} SEPARATOR ', ') AS {$this->record->COLUMNNAME}_{$tableprefix}";
                            //             // GROUP_CONCAT(sss61.ontwikkelsfeer_naam SEPARATOR ', ') AS ONTWIKKELTYPES_sss61,
                            //         }
                            //     }
                            } else {
                                $return .= $this->record->SUPERSHOWCOLUMNNAME.' as '.$this->record->SUPERSHOWCOLUMNNAME.'_'.$tableprefix.' , '.$this->record->SUPERSHOWCOLUMNNAME.' a, ';
                            }


                            if ($dbobj->IsOracle()) {
                                $return .= 'ROWIDTOCHAR('.$tableprefix.'.ROWID) AS '.$tableprefix.'_ROWID , ';
                            }
                        }
                    }
                    if (preg_match('/^[a-zA-Z0-9_]{1,}$/', $this->record->COLUMNNAME)) {
                        $return .= $tblprefix.$this->record->COLUMNNAME;
                    } else {
                        // $return .= $this->record->COLUMNNAME;
                    }

                    if ($this->record->DATATYPE == 'J') {
                        if (!$useinsearchfilter) {
                            // For now skip JSON columns in search filters
                            $return .= " , {$tblprefix}{$this->record->COLUMNNAME} AS {$this->record->COLUMNNAME}_JSON_SEARCH";
                        }
                    }

                }
            }

            $return .= ' , ';
        }

        return $return;
    }

    function TranslateListValues($listvalues) {
        $setarray = explode(';', trim($listvalues));
        foreach($setarray as $k => $set) {
            $s = explode('=', $set);
            $tmp = $s[0];
            if ($this->i18n($s[1])) {
                $tmp .= '='.$this->i18n($s[1]);
            }
            $result[] = $tmp;
        }
        return $result;
    }

    function ProcessURL($url) {
        //$result = ereg_replace("\{(.+)\}$" , "{\$rec->\\1}",$url);
        //      preg_match('{(LESIDEE)}', 'aaa\1', $url, $array);
        //      deb($array);
        //      $result = preg_replace('\{(LESIDEE)\}', 'aaa\1', $url);
        return $result;
    }

    function ProcessFieldValue($rec, $fieldvalue, $state=false) {
        global $_CONFIG;

        if (trim($this->record->LISTVALUES) != '') {
            $fieldvalue = $this->GetListValue($fieldvalue);
        }

        if ($fieldvalue != '' and $this->record->DATATYPE == 'N') {
            if ($this->record->DECIMALCOUNT > 0) {
                $fieldvalue = number_format((float) str_replace(',','.',$fieldvalue), $this->record->DECIMALCOUNT, ',','.');
            }
        }
        // Geeft problemen: waarde na eerste duizendpunt wordt afgekapt
        // if ($fieldvalue != '' and $this->record->DATATYPE == 'I') {
        //     $fieldvalue = number_format((integer) $fieldvalue, 0, ",", "." );
        // }

        if ($this->record->DATATYPE == 'D') {
            // if the field is empty, don't apply any date functions
            $datefieldname = $this->record->COLUMNNAME;
            // if (!$this->owner->owner->owner->owner->database->IsOracle())
            //     $datefieldname .= TIMESTAMP_POSTFIX;
            if ($rec[$datefieldname] != '' && $rec[$datefieldname] != '0') {
                $datefmt = 'd-m-Y'; // Fixed PHP date format
                // if (!$this->owner->owner->owner->owner->database->IsOracle())
                //     $fieldvalue = date($datefmt, $rec[$datefieldname]);
            } else {
                $fieldvalue = '';
            }
        }

        if ($this->record->DATATYPE == 'H') {
            // if the field is empty, don't apply any date functions
            // $timefieldname = $this->record->COLUMNNAME.TIMESTAMP_POSTFIX;
            if ($rec->$timefieldname != '') {
                $fieldvalue = $rec->$timefieldname;
            }
            else
                $fieldvalue = '';
        }

        if (($this->record->DATATYPE == 'X') or ($this->record->DATATYPE == 'C')) {
            if (strpos($this->record->FORMATPARAMS, ':showhtml' ) === false ) {
// deze zet " om in &quot;  ??? why???              $fieldvalue = htmlspecialchars($fieldvalue);
            }
            if ($state != 'edit' and ($this->record->CUTOFFTHRESHOLD > 0) and (!$this->owner->owner->owner->owner->HasOption('no-cutoff')) ) {
                if (strlen($fieldvalue) > $this->record->CUTOFFTHRESHOLD) {
                    $fieldvalue = substr($fieldvalue, 0, $this->record->CUTOFFTHRESHOLD) . '...';
                }
            }
        }
        return $fieldvalue;
    }

    function GetValidationFormat() {
        $result = '';
        if ($this->record->FORMAT == 'EMAIL')
            $result = 'validation_email';
        if ($this->record->FORMAT == 'LOWERCASE')
            $result = 'validation_lowercase';
        if ($this->record->FORMAT == 'UPPERCASE' or $this->record->MASK == 'UPPER')
            $result = 'validation_uppercase';
        if ($this->record->FORMAT == 'URL')
            $result = 'URL';
        if ($this->record->FORMAT == 'ELFPROEF')
            $result = 'validation_elfproef';
        if ($this->record->FORMAT == 'BSN')
            $result = 'validation_bsn';

        if ($result == '') {
            switch ($this->record->DATATYPE) {
                case 'D':
                 $result = 'validation_date';
                break;
                case 'I':
                case 'N':
                    if ($this->record->DECIMALCOUNT > 0) {
                        $result = 'validation_float';
                    } else {
                        $result = 'validation_integer';
                    }
                break;
                case 'C':
                case 'V':
                 $result = 'validation_allchars';
                break;
            }
        }
        return $result;
    }

    function GetNumericMaskFormat($validationformat) {
        $mNum = min(9, intval($this->record->TOTALLENGTH));
        if ($validationformat == 'validation_float') {
            $mDec = "data-m-dec=\"".intval($this->record->DECIMALCOUNT)."\"";
            $_tmpnum = str_repeat('9', $mNum);
            if ($this->record->SIGNED == 'Y') {
                $vMin = "data-v-min=\"-$_tmpnum.99\"";
            } else {
                $vMax = "data-v-max=\"$_tmpnum.99\"";
            }
            $numericmask = "$vMin $vMax $mDec";
        } elseif ($validationformat == 'validation_integer') {
            $_tmpnum = str_repeat('9', $mNum);
            if ($this->record->SIGNED == 'Y') {
                $vMin = "data-v-min=\"-$_tmpnum\"";
            } else {
                $vMax = "data-v-max=\"$_tmpnum\"";
            }
            $numericmask = "$vMin $vMax";
        } else {
            $numericmask = '';
        }
        return $numericmask;
    }

    function DisplayDetailLinks($record, $rec) {
        global $_GVARS;

        if ($record->SUBSETID != '') {
            if ($record->SUPERFORM != '') {
                $appobject =& $this->AppObject();
                $RowIDField = "SSS".$record->SUBSETID."_ROWID";
//                $RowIDField = $record->COLUMNNAME;
                $RowID = $this->owner->owner->owner->owner->database->customUrlEncode( $rec->$RowIDField );
                if ($RowID) {
                    $constructlink = $_GVARS['serverpath'].'/app/'.$appobject->record->METANAME.'/form/'.$record->SUPERFORM.'/edit/'.$RowID.'/?maximize=true';
                } else {
                    $selectfield = $record->COLUMNNAME;
                    $selectmasterfield = $record->SUPERCOLUMNNAME;
                    $constructlink = $_GVARS['serverpath'].'/app/'.$appobject->record->METANAME.'/form/'.$record->SUPERFORM.'/?select=true&selectfield=_fld'.$selectfield.'&selectmasterfield='.$selectmasterfield.'&selectshowfield='.$record->SUPERSHOWCOLUMNNAME;
                }
                echo '<span class="input-group-btn">';
      //  <button class="btn btn-white" type="button">Go!</button>

                echo ' <button class="btn btn-white" type="button"><a class="popbox field_tabledetails" title="Details" href="'.$constructlink.'"><i class="fa fa-list"></i><span>Details</span></a>';
                echo '</span>';
            } else {
//                echo " <label title="Linked construct onbekend. Nog koppelen?\">?<label>";
            }
        }
    }

    function IsReadOnly($currentvalue) {
        return ($this->record->READONLY == 'Y'
            or ($this->record->READONLYONVALUE !== '' and isset($currentvalue) and $currentvalue !== ''
                and strpos($this->record->READONLYONVALUE.',', $currentvalue.',') !== FALSE));
    }

    function GetColumnName() {
        // if the COLUMNNAME is not a proper columnname (ie. CONCAT(...,...))
        // then don't show the name attribute, so is does not get saved
        $_result = $this->record->COLUMNNAME;
        if (!preg_match('/^[a-zA-Z0-9_]{1,}$/', $this->record->COLUMNNAME)) {
            $_result = '';
        }

        // if the column is a float datatype then add "__FLOAT" to the column name
        // when this record/column gets saved in ORA8, then PLR knows to convert the decimal sign into a point
        //if ($this->owner->owner->owner->owner->database->IsOracle() and $record->DECIMALCOUNT > 0) {
        if ($this->record->DECIMALCOUNT > 0) {
            $_result .= FLOATCONVERT;
        }
        if ($this->record->DEFAULTCOLUMN == 'Y') {
            $_result .= DEFAULTCOLUMN;
        }

        if ($this->record->UPDATECOLUMN !== 'Y')
            $_result = '';

        return $_result;
    }

    function ShouldDisplayFieldInListView() {
        $_shouldDisplay = ($this->record->VISIBLEINLIST == 'Y'
        and ($this->record->ISGROUPFIELD == 'N'
            or
            ($this->record->ISGROUPFIELD == 'Y' and empty($_SESSION["_GRP_{$this->record->COLUMNNAME}"]))
        ));

        return $_shouldDisplay;
    }

    function ShouldDisplayFieldInFormView() {
        $_shouldDisplay = ($this->record->VISIBLE == 'Y'
        and ($this->record->ISGROUPFIELD == 'N'
            or
            ($this->record->ISGROUPFIELD == 'Y' and empty($_SESSION["_GRP_{$this->record->COLUMNNAME}"]))
        ));

        return $_shouldDisplay;
    }

    function Show($userdb, $rec, $state, $permissiontype, $masterrecord, $pagecolumncount=1, $pinvalues=false) {
        global $_CONFIG;
        global $_GVARS;
        global $_sqlSubsetItems;

        if ($pagecolumncount == 0) $pagecolumncount = 1;

        $record =& $this->record;
        $_po = $this->polaris();

        $column = $record->COLUMNNAME;
        if ($record->FORMAT == 'PUBLISH')
            $this->owner->owner->owner->owner->publishform = 'Y';

        $appobject =& $this->AppObject();
        $formobject =& $this->owner->owner->owner->owner;

        $rsfieldnames = $this->owner->owner->owner->rs->_names;
        if ($this->owner->owner->owner->rs->_names == null) $rsfieldnames = array();
            else $rsfieldnames = $this->owner->owner->owner->rs->_names;

        if (($userdb->databaseType === 'mysql') AND $this->owner->owner->owner->rs->_queryID)
            $fieldflags = mysql_field_flags ($this->owner->owner->owner->rs->_queryID, array_search($column, $rsfieldnames) );
        elseif (($userdb->databaseType === 'mysqli') AND $this->owner->owner->owner->rs->_queryID)
            $fieldflags = false; //mysqli_fetch_field_direct ($this->owner->owner->owner->rs, 0 );
        else
            $fieldflags = false;

        $events = createEvents( $this->record->EVENTS );
        if (($record->COLUMNVISIBLE == 'Y' or $record->LINETYPE == 'COMPONENT' or $record->LINETYPE == 'TEXTLINE') and ($record->FORMAT != 'PUBLISH')) {
            echo '<div id="_row'.$column.'" class="form-group row form-group-md"';

            if (!$this->ShouldDisplayFieldInFormView()) echo ' style="display:none"';
            echo '>';
            if ($record->LINETYPE == 'COLUMN') {

                /* Set the focus on the field automatically */
                $autofocus = ($record->AUTOFOCUS == 'Y')?' autofocus="autofocus" ':'';

                /**
                * Convert value into HTML values, if necessary.
                */
                if ($record->FORMAT != 'HTML') {
                    $currentvalue = htmlspecialchars($rec->$column, ENT_QUOTES);
                } else {
                    $currentvalue = $rec->$column;

                }
                /**
                * Assign default values and groupfilter values
                */
                /**
                * Determine a detailform's default value from the master record
                */
                if ($state == 'insert' or $state == 'detail_insert' or $state == 'clone' or $state == 'detail_clone') {
                    // if this field is a masterfield of a masterdetail relation then fill the field with the right session value
                    if (is_array($formobject->detailfields))
                        $detailarrayflipped = array_flip($formobject->detailfields);
                    else
                        $detailarrayflipped = $formobject->detailfields;

                    $mastercolumnname = $formobject->masterfields[$detailarrayflipped[$column]];
                    if ($currentvalue == '' && ($formobject->detail)
                        && in_array($mastercolumnname, $formobject->masterfields) ) {
                        $currentvalue = $masterrecord->$mastercolumnname;
                    }

                    $_groupfieldvalue = $formobject->GetGroupField($column);
                    // if this field was grouped then fill in the groupvalue
                    if ($currentvalue == '' && (isset($_groupfieldvalue))) {
                        $currentvalue = $_groupfieldvalue;
                    }

                    if ($currentvalue == '' && (isset($pinvalues[$column]))) {
                        $currentvalue = $pinvalues[$column];
                    }

                    $_captionColumnName = $formobject->owner->masterform->record->CAPTIONCOLUMNNAME;
                    if ($currentvalue == '' && $column == $_captionColumnName) {
                        if ((isset($masterrecord->$_captionColumnName)))
                            $currentvalue = $masterrecord->$_captionColumnName;
                    }
                }
                /**
                * Determine the maximum size for the total length
                */
                $pagecolumnwidth = Array(1=>50,2=>25,3=>15);
                $size = Min(Max($record->TOTALLENGTH, 8), $pagecolumnwidth[$pagecolumncount]);
                $selectfieldmaxwidth = 'style="max-width:'.($pagecolumnwidth[$pagecolumncount]*8).'px"';

                /**
                * Determine the default value
                */
                $thedefaultvalue = '';
                // when there is a datascope and the current value is not set, then fill in then datascope value
                $datascope = $appobject->owner->GetDataScope($record->DATABASEID, $record->TABLENAME);
                if (($record->DEFAULTVALUE != '' or $record->LINEDEFAULTVALUE != '')) {
                    if ($record->LINEDEFAULTVALUE != '') {
                        $thedefaultvalue = $record->LINEDEFAULTVALUE;
                    } else {
                        $thedefaultvalue = $record->DEFAULTVALUE;
                    }
                    if (($state == 'insert' or $state == 'detail_insert')
                      or (($state == 'clone' or $state == 'detail_clone') and isset($rec))
                      or (($state == 'edit' or $state == 'clone') and $record->FORCEDEFAULTONUPDATE == 'Y')) {
                        $defaultsource = $masterrecord;
                    } else {
                        $defaultsource = $rec;
                    }
                    $thedefaultvalue = $_po->ReplaceDynamicVariables($thedefaultvalue, $nullvalue = 'NULL', $defaultsource);
                    $thedefaultvalue = ProcessSQL($userdb, $thedefaultvalue);
                } elseif ($datascope[$column]['operand'] == '=') {
                    $thedefaultvalue = $datascope[$column]['filtervalue'];
                }
                // in case of INSERT or UPDATE and ForceDefaultOnUpdate
                // if this field has a default value then use that value

                if ( $currentvalue == ""
                and (($permissiontype & INSERT) == INSERT or ($permissiontype & UPDATE) == UPDATE)
                and (
                     ($state == 'insert' or $state == 'detail_insert')
                  or (($state == 'clone' or $state == 'detail_clone') and isset($rec))
                  or (($state == 'edit' or $state == 'clone') and $record->FORCEDEFAULTONUPDATE == 'Y')
                ) ) {
                    if ($thedefaultvalue != '') {
                        if ($thedefaultvalue[0] == '%' and $thedefaultvalue[strlen($thedefaultvalue) - 1] == '%') {
                            $currentvalue = $_po->ReplaceDynamicVariables($thedefaultvalue);
                        } elseif (substr($thedefaultvalue, 0, 2) == '__' and substr($thedefaultvalue, -2) == '__') {
                            $currentvalue = $thedefaultvalue;
                        } else {
                            $currentvalue = $this->GetEvalString($thedefaultvalue);
                        }

                    }
                    if ($currentvalue == '' and isset($_GET['_fld'.$column])) {
                        $currentvalue = $_GET['_fld'.$column];
                    }
                }
                // if the field is an autoincrement field and we are cloning it from another record
                // then don't use the cloned value but use the '__auto_increment__' value
                if ($state == 'clone' and isset($rec) ) {
                    if ($thedefaultvalue == '__auto_increment__' ) {
                        $currentvalue = $thedefaultvalue;
                    } elseif (strpos($fieldflags, 'auto_increment') !== false ) {
                        $currentvalue = '';
                    }
                }
                if ($currentvalue == '__auto_increment__')
                    $size = strlen($currentvalue);

                /* Show disabled control when field is readonly or password field */
                if ($this->IsReadOnly($currentvalue)
                    or ($formobject->IsLineReadOnly($rec))
                    or ($record->PASSWORD == 'Y')
                    or ($permissiontype & UPDATE) != UPDATE
                ) {
                    $readonly = 'readonly="readonly" tabindex="-1"';
                    $disabled = 'disabled="disabled"';
                    $readonlyclass = 'readonly';
                } else {
                    $readonly = '';
                    $disabled = '';
                    $readonlyclass = '';
                }

                /* Show highlighted control when field is required */
                if (($record->REQUIRED == 'Y' or $record->LINEREQUIRED == 'Y')
                and (!$this->IsReadOnly($currentvalue))
                and (!$formobject->IsLineReadOnly($rec))
                and ($record->VISIBLE == 'Y')) {
                    $required = 'required ';
                    $fieldrequired = true;
                } else {
                    $required = '';
                    $fieldrequired = false;
                }

                if (!empty($datascope)) {
                    // insert the DataScope value and render the input field as 'readonly';
                    if ($state == 'insert' or $state == 'clone') {
                        if ($datascope[$column]['operand'] == '=') {
                            $currentvalue = $datascope[$column]['filtervalue'];
                            $readonly = 'readonly="readonly" tabindex="-1"';
                            $readonlyclass = 'readonly';
                        }
                    } elseif ($state == 'edit') {
                        if ($datascope[$column]['operand'] == '=' and $datascope[$column]['filtervalue'] != '') {
                            $readonly = 'readonly="readonly" tabindex="-1"';
                            $readonlyclass = 'readonly';
                        }
                    }
                }

                if ($record->DEFAULTCOLUMN == 'Y' and $state == 'insert') {
                    $constructname = $this->owner->owner->owner->owner->owner->record->METANAME;
                    if (isset($_SESSION['DEFAULTCOLUMNS'][$constructname][$column])) {
                        $currentvalue = $_SESSION['DEFAULTCOLUMNS'][$constructname][$column];
                    }
                }

                $labeltag = 'label="'.ProcessSQL($userdb, $record->LINEDESCRIPTION).'"';
                $labeltext = ProcessSQL($userdb, $record->LINEDESCRIPTION);
                $labeltext = $this->i18n($labeltext);

                if ($pagecolumncount == 1) {
                    $_labelSize = "col-xs-12 col-md-3";
                    $_controlSize = "col-xs-12 col-md-9";
                } elseif ($pagecolumncount == 2) {
                    $_labelSize = "col-xs-12 col-md-4";
                    $_controlSize = "col-xs-12 col-md-8";
                }

                if ($record->FORMAT != 'HTML') {
                    $columnname = $this->GetColumnName();
                    if ($fieldrequired) {
                        $_fieldrequiredClass = 'required';
                        //;echo "&nbsp;<i class='fa-light fa-solid fa-star-of-life' style='margin-right:-10px;color:#ccc;font-size:0.6em'></i>";
                    }
                    echo "<label class=\"$_labelSize $_fieldrequiredClass col-form-label control-label\" title=\"$columnname\">".$labeltext;

                    if (!$fieldrequired and !$readonly and $state !== 'view') {
                        echo " &nbsp; <span class='optional-field'>optioneel</span>";
                    }

                    echo " </label><div class=\"$_controlSize\">";
                } elseif ($record->FORMAT == 'HTML' and $record->FORMATPARAMS != '') {
                    echo "<label class=\"$_labelSize col-form-label control-label\">".ProcessSQL($userdb, $record->LINEDESCRIPTION);
                    if (substr($record->LINEDESCRIPTION, -1) != '?' )
                        echo ':';
                    echo " </label><div style=\"display:inline;\" class=\"$_controlSize\">";
                } else {
                    echo "<div class=\"$_controlSize\">";
                }

                echo '<div class="input-group m-b-xs">';
                if ($record->UNIT != '' and strlen($record->UNIT) == 1) {
                    echo "<div class=\"input-group-prepend\"><span class=\"input-group-addon\">{$record->UNIT}</span></div>";
                }
                $this->ShowGuiElement($userdb, $inlistview=false, $recordid=$rec->_RECORDID, $state, $currentvalue, $rec, $fieldrequired, $readonly, $permissiontype, $autofocus, $optimizedselect = false);
                if ($record->UNIT != '' and strlen($record->UNIT) > 1) {
                    echo "<div class=\"input-group-append\"><span class=\"input-group-addon\">{$record->UNIT}</span></div>";
                }
                echo "</div>";
                echo "</div>";
            } elseif ($record->LINETYPE == 'COMPONENT') {
                if ($record->FORMAT != '') {
                    echo "<div class='componentline'>";
                    $componentname = strtolower($record->FORMAT);
                    require_once('guicomponents/component.'.$componentname.'.php');
                    $classname = $componentname.'Component()';
                    eval("\$component = new $classname;");
                    echo $component->GenerateXHTML(array($this->owner->owner->owner->owner, $rec, $column, $userdb, $record->FORMATPARAMS));
                    echo "</div>";
                }
            } else {
                $textline = $this->i18n(ProcessSQL($userdb, $record->LINEDESCRIPTION));
                if ($textline !== '')
                    $_textLineClass = 'label';
                echo "<p class='col-xs-12 col-md-5 no-padding column-textline'>$textline </p>";
            }
            echo '</div>';
        }
    }

    function GetEvalString($value) {
        if ($this->polaris()->ContainsPHPFunction($value)) {
            $result = @eval('return '.$value.';');
        } else {
            $result = @eval('return "'.$value.'";');
        }

        if ($result === FALSE) $result = $value;
        return $result;
    }

    function getDateFormat($phpformatting=false) {
        global $_CONFIG;

        $apprecord = & $this->AppObject();
        if (isset($apprecord) && $apprecord->record->DATEFORMAT) {
            $datefmt = $apprecord->record->DATEFORMAT;
        } elseif ($phpformatting) {
            $datefmt = $_CONFIG['default_php_date_format'];
        } else {
            $datefmt = $_CONFIG['default_date_format'];
        }
        return $datefmt;
    }

    function getTimeFormat() {
        global $_CONFIG;

        $apprecord = & $this->AppObject();
        if (isset($apprecord) && $apprecord->record->TIMEFORMAT) {
            $timefmt = $apprecord->record->TIMEFORMAT;
        } else {
            $timefmt = $_CONFIG['default_time_format'];
        }
        return $timefmt;
    }

    function ApplyMask($value, $rec) {
        global $_CONFIG;

        if ($this->record->DATATYPE == 'N' or $this->record->DATATYPE == 'I') {
            if ($this->record->DECIMALCOUNT > 0) {
                // Remove thousand separators ('.') and replace decimal comma with a dot
                $value = str_replace('.', '', $value); // Remove thousand separators
                $value = str_replace(',', '.', $value); // Replace decimal comma with dot

                // Convert the value to a float
                $value = floatval($value);

                // Format the number with DECIMALCOUNT decimal places, comma for decimal point, and dot for thousands separator
                return number_format($value, $this->record->DECIMALCOUNT, ',', '.');
            }
        }
        if ($this->record->DATATYPE == 'D' and !empty($this->record->MASK)) {
            $date = new DateTime($value);
            $value = $date->format($this->record->MASK);
            $value = str_replace(
                array('Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'),
                array('Ma', 'Di', 'Wo', 'Do', 'Vr', 'Za', 'Zo', 'Jan', 'Feb', 'Maa', 'Apr', 'Mei', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dec'),
                $value
            );
        }
        return $value;
    }

    function GetAlign($fieldindex) {
        if ($fieldindex == 1) {
            $align = 'left';
        } elseif ($this->record->SUPERSHOWCOLUMNNAME != '' || $this->record->LISTVALUES != '') {
            $align = 'left';
        } elseif ($this->record->FORMAT == 'POSITION') {
            $align = 'center';
        } else {
            switch ($this->record->DATATYPE) {
                case 'N':
                case 'I':
                 $align = 'right';
                break;
                case 'T':
                 $align = 'left';
                break;
                case 'D':
                 $align = 'left';
                break;
                default:
                //  $align = 'left';
                break;
            }
        }
        return $align;
    }

    function GetListValue($fieldvalue) {
        /* Show the right listvalue if it's a readonly field */
        $found = false;
        $setarray = explode(';', trim($this->record->LISTVALUES));
        foreach($setarray as $listvalue) {
            $valuearray = explode('=', $listvalue);
            if ($this->record->BINARYVALUES == 'Y') {
                if ((intval($fieldvalue) & intval($valuearray[0])) == intval($valuearray[0])) {
                    $parts = explode('|', $valuearray[1]);
                    $val = $parts[1] ?? $parts[0];
                    $_tmpfieldvalue .= '<mark title="'.$parts[0].'" class="'.$val.'">'.$val.'</mark> ';
                    $found = true;
                }
            } else {
                if ($fieldvalue == $valuearray[0]) {
                    $found = true;
                    if ($valuearray[1] != '') {
                        $_tmpfieldvalue = $this->i18n($valuearray[1]);
                    } else {
                        $_tmpfieldvalue = $valuearray[0];
                    }
                    break;
                }
            }
        }

        if ($found)
            return $_tmpfieldvalue;
        elseif ($this->record->BINARYVALUES == 'Y')
            return '';
        else
            return $fieldvalue; //.$this->record->BINARYVALUES;
    }

    function GetGuiElement($userdb, $inlistview, $recordid, $state, $currentvalue, $rec, $required, $readonly, $permissiontype, $autofocus=false, $optimizedselect = false) {
        require_once('classes/base/guielements.class.php');
        $this->guielement = PLR_CreateGUIComponent($this, $this->record->LINETYPE, $this->record->DATATYPE, $this->record->SUPERSHOWCOLUMNNAME, $this->record->LISTVALUES, $this->record->FORMAT, $userdb, $inlistview, $autofocus, $optimizedselect);
        return $this->guielement->Show($state, $currentvalue, $rec, $required, $readonly, $recordid, $permissiontype);
    }

    function ShowGuiElement($userdb, $inlistview, $recordid, $state, $currentvalue, $rec, $required, $readonly, $permissiontype, $autofocus=false, $optimizedselect = false) {
        echo $this->GetGuiElement($userdb, $inlistview, $recordid, $state, $currentvalue, $rec, $required, $readonly, $permissiontype, $autofocus, $optimizedselect);
    }

    function GetSearchField($userdb) {
        $record = &$this->record;
        $columnname = $record->COLUMNNAME;
        if ($record->SUPERSHOWCOLUMNNAME != '' and ($record->SUPERSHOWCOLUMNNAME != strtoupper($record->SUPERCOLUMNNAME))) {
//            $columnname .= ','.$record->SUPERSHOWCOLUMNNAME;
        }
        $label = ProcessSQL($userdb, $record->LINEDESCRIPTION_SHORT);
        return array($columnname, $label, $record->COLUMNNAME);
    }

    function ShowSearchField($columnlabel) {
        $record = &$this->record;
        if (($record->VISIBLE == 'Y' or $record->VISIBLEINLIST == 'Y') and $record->LINETYPE == 'COLUMN') {
            $inputtype = 'search';
            $inputsize = '10';
            $maxlengthcode = '100';
            $column = $columnlabel[0];
            $label = $this->i18n($columnlabel[1]);

            if (is_array($_GET['qc']))
                $searchvalues = array_combine($_GET['qc'], $_GET['q']);
            echo '<div class="form-group">';
            echo "<label class=\"col-sm-4 xcol-form-label control-label\">".$label.": </label>";
            echo "<div  class=\"formcolumnx\">";
            echo "<input type='hidden' name='qc[]' value='$column' />";
            echo "<input type='$inputtype' label='$label' class='text' id='_src{$columnlabel[2]}' name='qv[]' size='$inputsize' maxlength='$maxlengthcode' value='${searchvalues[$column]}' />";
            echo "</div>";
            /*
            // if the COLUMNNAME is not a proper columnname (ie. CONCAT(...,...))
            // then don't show the name columnname
            if (preg_match('^[a-zA-Z0-9_]{1,}$', $record->COLUMNNAME)) {
            }
            */
            echo '</div>';
        }
    }
}
