<?php

class pageTimer {

	// Variables
	var $timeTaken = array() ;
	var $startTime = array();
	var $endTime = array();

    function clearTimers() {
        $this->timeTaken = array();
        $this->startTime = array();
        $this->endTime = array();
    }

	/**
	* @return returns value of variable $timeTaken
	* @desc getTimeTaken : Getting value for variable $timeTaken
	*/
	function getTimeTaken ($token) {
		return $this->timeTaken[$token] ;
	}

	/**
	* @param param : value to be saved in variable $timeTaken
	* @desc setTimeTaken : Setting value for $timeTaken
	*/
	function setTimeTaken ($token, $value) {
		$this->timeTaken[$token] = $value;
	}

	/**
	* @return returns value of variable $startTime
	* @desc getStartTime : Getting value for variable $startTime
	*/
	function getStartTime ($token) {
		return $this->startTime[$token] ;
	}

	/**
	* @param param : value to be saved in variable $startTime
	* @desc setStartTime : Setting value for $startTime
	*/
	function setStartTime ($token, $value) {
		$this->startTime[$token] = $value;
	}

	/**
	* @return returns value of variable $endTime
	* @desc getEndTime : Getting value for variable $endTime
	*/
	function getEndTime($token) {
		if (isset($this->endTime[$token]))
			return $this->endTime[$token];
		else
			return false;
	}

	/**
	* @param param : value to be saved in variable $endTime
	* @desc setEndTime : Setting value for $endTime
	*/
	function setEndTime($token, $value) {
		$this->endTime[$token] = $value;
	}

	function startTimer($token='page') {
		$mtime = microtime();
		$mtime = explode(" ",$mtime);
		$mtime = $mtime[1] + $mtime[0];
		$this->setStartTime($token, $mtime);
	}

	function endTimer($token='page') {
		$mtime = microtime();
		$mtime = explode(" ",$mtime);
		$mtime = $mtime[1] + $mtime[0];
		$this->setEndTime($token, $mtime);
	}

	function findTimeTaken($token) {
		$timeTaken = $this->getEndTime($token) - $this->getStartTime($token);
		$this->setTimeTaken($token, $timeTaken);
	}

	function displayTimeTaken($tokens=null) {
	    $result =  "<div id=\"pagestats\" style=\"display:none\"><h3>Execution times</h3>";
	    if (isset($tokens)) {
            foreach($tokens as $token) {
                $this->findTimeTaken($token);
        		$result .= "$token: ".$this->getTimeTaken($token)." seconds<br />";
            }
        } else {
            foreach($this->startTime as $token => $time) {
                $this->findTimeTaken($token);
        		$result .= "$token: ".$this->getTimeTaken($token)." seconds<br />";
            }
        }
	    $result .= "</div>";
	    return $result;
	}
}
