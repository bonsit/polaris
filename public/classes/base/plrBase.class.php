<?php

function strposX(string $haystack, string $needle, int $number):int|false {
    return strpos($haystack, $needle,
        $number > 1 ?
        strposX($haystack, $needle, $number - 1) + strlen($needle) : 0
    );
}

class base_plrBase {
    var $__polaris = null;

    function __construct() {
        // null
    }

	function &polaris() {
	    if (isset($this->__polaris)) {
	        return $this->__polaris;
        } elseif (strcasecmp(get_class($this->owner), 'Polaris') == 0) {
	        return $this->owner;
	    } elseif (isset($this->owner)) {
	        return $this->owner->polaris();
	    } else {
	        return false;
	    }
	}

	function &plrInstance() {
        if (isset($this->__polaris))
            return $this->__polaris->instance;
        elseif (isset($this->owner)) {
            if (strcasecmp(get_class($this->owner), 'Polaris') == 0)
                return $this->owner->instance;
            else {
                if ( !in_array_cin(get_class_methods($this->owner), 'plrinstance') )
                    debug();
                else
                    return $this->owner->plrInstance();
            }
        } else
            return false;
	}

    function i18n($_resource) {
        return get_resource($_resource, $this->polaris()->language);
    }

    function i18n_trans($_resource) {
        return lang_get($_resource);
    }

    function interpolateMultiVars($_sql, $_params) {
        if (is_array($_params)) {
            foreach($_params as $_k => $_v) {
                if (strpos($_v, '@!') !== false) {
                    $_typ = substr($_v, strpos($_v, '@!') + 2);
                    if ($_typ == 'integer') {
                        $_cleanval = substr($_v, 0, strpos($_v, '@!'));
                        $_pos = strposX($_sql, '?', $_k + 1);
                        $_sql = substr_replace($_sql, $_cleanval, $_pos, 1);
                        unset($_params[$_k]);
                    }
                }
            }
        }
        return array($_sql, $_params);
    }

	function &dbExecute($sql, $params=false, $debug=false) {
	    global $_CONFIG;

        $plrinst =& $this->polaris();
        if ($plrinst) {
            $plrinst->connectInstance();
            $plrinst->instance->debug = $debug;
            [$sql, $params] = $this->interpolateMultiVars($sql, $params);
            try {
                if ($_CONFIG['cachequeries'] and !$_CONFIG['debugmode']) {
                    $_res = $plrinst->instance->CacheExecute($sql, $params);
                } else {
                    $_res = $plrinst->instance->Execute($sql, $params);
                }
            } catch (ADODB_Exception $e) {
                $plrinst->instance->debug = false;

                throw new ADODB_PLR_Exception(false, false, $e->getCode(), $e->getMessage(), false, false, $plrinst->instance);
            }

            return $_res;
        } else {
            echo "No plrInstance found. Tried to execute SQL: $sql";
        }
	}

	function dbGetRow($sql, $params=false, $debug=false) {
        [$sql, $params] = $this->interpolateMultiVars($sql, $params);
        $rows = $this->dbGetAll($sql, $params, $debug);
        return $rows[0] ?? false;
    }

	function &dbGetAll($sql, $params=false, $debug=false) {
	    global $_CONFIG;

        $plrinst =& $this->polaris();
        if ($plrinst) {
            $plrinst->connectInstance();
            $plrinst->instance->debug = $debug;
            [$sql, $params] = $this->interpolateMultiVars($sql, $params);

            if ($_CONFIG['cachequeries'] and !$_CONFIG['debugmode']) {
                $rec = $plrinst->instance->CacheGetAll($sql, $params);
            } else {
                $rec = $plrinst->instance->GetAll($sql, $params);
            }
            $plrinst->instance->debug = false;
            return $rec;
        }
	}

	function &dbGetOne($sql, $params=false) {
        global $_CONFIG;

        $plrinst =& $this->polaris();
        if ($plrinst) {
            $plrinst->connectInstance();
            [$sql, $params] = $this->interpolateMultiVars($sql, $params);

            if ($_CONFIG['cachequeries'] and !$_CONFIG['debugmode'])
                $rec = $plrinst->instance->CacheGetOne($sql, $params);
            else
                $rec = $plrinst->instance->GetOne($sql, $params);
            return $rec;
        }
	}

    function &dbGetCol($sql, $params=false) {
        global $_CONFIG;

        $plrinst =& $this->polaris();
        if ($plrinst) {
            $plrinst->connectInstance();
            [$sql, $params] = $this->interpolateMultiVars($sql, $params);

            if ($_CONFIG['cachequeries'] and !$_CONFIG['debugmode'])
                $rec = $plrinst->instance->CacheGetCol($sql, $params);
            else
                $rec = $plrinst->instance->GetCol($sql, $params);
            return $rec;
        }
	}

	function dbInsertID() {
	    global $_CONFIG;
        $plrinst =& $this->polaris();
        if ($plrinst) {
            $plrinst->connectInstance();
            return $plrinst->instance->Insert_ID();
        } else {
            return false;
        }
	}

	function dbErrorMsg() {
        $plrinst =& $this->polaris();
        if ($plrinst) {
            $plrinst->connectInstance();
            return $plrinst->instance->_errorMsg;
        } else
            return false;
	}

}
