<?php

class base_plrFormTag extends base_plrRecord {
    var $database;

    function plrFormTag(&$aowner) {
        $this->plrRecord($aowner);
    }

    function &LoadRecord($key = false /* HASHED */) {
        global $_sqlFormTag;

        if ($key) $this->key = $key;
        $this->record = GetQueryRecordAsObject($this->plrInstance(), $_sqlFormTag, $this->key, true);

        return $this;
    }

    function Show($style=false, $selected) {
        $basehref = $this->owner->owner->MakeURL();
        $action = $basehref.'?tag='.$this->record->METANAME;

        $_color = 'btn-'. (isset($this->record->COLOR) ? $this->record->COLOR : 'primary');
        $_active = $selected ? 'active' : '';
        $target = '';
        $_label = $this->i18n($this->record->TAGNAME);

        echo " <a href=\"$action\" class=\"btn xbtn-sm $_color $_active xbtn-outline xbtn-rounded\" $target>".$_label."</a>";
        // echo "<li class='nav-item {$_active}'>";
        // echo "<a href='{$action}' class='nav-link' {$target}>{$_label}</a>";
        // echo "</li>";
    }
}
