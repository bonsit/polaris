<?php

class base_plrDatabases extends base_plrRecords {
    var $databases;

    function __construct(&$aowner) {
        parent::__construct($aowner);
    }

    function LoadRecords($clienthash) {
        global $_sqlClientDatabases;
        $rs = $this->dbExecute( $_sqlClientDatabases, array($clienthash) ) or Die('query failed: '.$_sqlClientDatabases);
        $this->databases = array();
        while ($rec = $rs->FetchNextObject(true)) {
          $plrDatabase = new base_plrDatabase($this);
          $plrDatabase->record = $rec;
          $this->databases[] = $plrDatabase;
        }
    }

    function GetSelectList($currentvalue='') {
        return ObjectSetAsSelectList($this->databases, $keycolumn='RECORDID', $namecolumn='DB_DESC', $currentvalue, $id='dbselect', $selectname='RECORDID');
    }
}
