<?php

class base_plrClients extends base_plrRecords {
    var $clients;

    function __construct(&$aowner) {
        parent::__construct($aowner);
    }

    function LoadRecords() {
        global $_sqlClients;

        $rs = $this->dbExecute($_sqlClients) or Die('query failed clients:' . $_sqlClients);
        $this->clients = array();
        while ($rec = $rs->FetchNextObject(true)) {
            $client = new base_plrClient($this);
            $client->record = $rec;
            $this->clients[] =& $client;
        }
    }

    function Show() {
        foreach ( array_keys($this->clients) as $index ) {
            $client =& $this->clients[$index];
        }
    }

    function ShowSelectList($currentclientid, $attr = '') {
        echo ObjectSetAsSelectList($this->clients, $keycolumn = 'CLIENTID', $namecolumn = 'NAME', $currentvalue = $currentclientid, $id = 'clientselect', $selectname = 'CLIENTID2', $attr);
    }
}
