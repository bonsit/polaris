<?php
use Hautelook\Phpass\PasswordHash;

const __RAW__ = '__RAW__';

class base_plrUserGroup extends base_plrRecord {
    var $database = null;
    var $outparams = [];
    var $APIKEY = '';
    var $APISECRET = '';
    var $tablename = 'plr_usergroup';
    var $settingstablename = 'plr_usersetting';
    var $record = [];

    function __construct($aowner, $key=false) {
        // The owner should be a Polaris object
        parent::__construct($aowner);

        $this->database = $this->owner->instance;

        $this->key = $key;

        if ($this->key) {
            $this->loadRecord($this->key);
        }
    }

    // public static function getUserGroupInfo($aowner, $username) {
    //     $_userObject = new base_plrUserGroup($aowner);
    //     $_userObject->loadRecordByName($username);
    //     return $_userObject->record;
    // }

    public function loadRecord($key=false) {
        global $_sqlUserGroupV2;

        $this->record = $this->dbGetRow($_sqlUserGroupV2, [$key]);
        $this->loadAPIInfo();
        return $this;
    }

    public function loadRecordByName($username) {
        $_sql = "
            SELECT U.*, C.IMAGESGROUP_HASH, C.CUSTOMCSS, C.NAME as CLIENTNAME
            FROM plr_usergroup U
            LEFT JOIN plr_client C
            ON U.clientid = C.clientid
            WHERE UCASE(USERGROUPNAME) = ?
        ";
        $this->record = $this->dbGetRow($_sql, [$username]);
        $this->loadAPIInfo();
        return $this;
    }

    public function loadRecordByEmailAddress($emailaddress) {
        $_sql = "
            SELECT U.*, C.IMAGESGROUP_HASH, C.CUSTOMCSS, C.NAME as CLIENTNAME
            FROM plr_usergroup U
            LEFT JOIN plr_client C
            ON U.clientid = C.clientid
            WHERE UCASE(EMAILADDRESS) = ?
        ";
        $this->record = $this->dbGetRow($_sql, [strtoupper($emailaddress)]);
        $this->loadAPIInfo();
        return $this;
    }

    private function _checkUserAlreadyLoggedOn($clientid, $usergroupid) {
        $query = "
          SELECT SESSKEY
          FROM plr_session
          WHERE EXPIREREF = ?
          ";
        if ($rs = $this->owner->instance->GetAll($query, array(MD5($clientid."_".$usergroupid))))
            return $rs[0]['SESSKEY'];
        else
            return false;
    }

    private function loadAPIInfo() {
        if ($this->record === FALSE) return;

        $query = "
            SELECT APIKEY, APISECRET
            FROM plr_client
            WHERE CLIENTID = ?
        ";
        $_rs = $this->dbGetRow($query, [$this->record['CLIENTID']]);
        $this->APIKEY = $_rs['APIKEY'];
        $this->APISECRET = $_rs['APISECRET'];
    }

    private function IsPasswordExpired($lastpasswordchangedate) {
        global $_CONFIG;

        if (empty($_CONFIG['passwordexpiry']) || $_CONFIG['passwordexpiry'] == 0) return false;

        $lastpasswordchangedate = strtotime($lastpasswordchangedate); // Convert to timestamp
        $passwordexpirydate = strtotime('+'.$_CONFIG['passwordexpiry'].' days', $lastpasswordchangedate); // Add the expiry period
        $today = strtotime(date('Y-m-d')); // Get today's date in timestamp format

        return $today > $passwordexpirydate;
    }

    private function shouldPasswordBeChanged($changepasswordnextlogin, $lastpasswordchangedate, $passwordneverexpires, $usercannotchangepassword) {
        if ($passwordneverexpires === 'Y' || $usercannotchangepassword === 'Y') {
            return 'N';
        } else {
            return ($changepasswordnextlogin == 'Y' || $this->IsPasswordExpired($lastpasswordchangedate)) ? 'Y' : 'N';
        }
    }

    private function _changePasswordField($hashedPassword) {
        // The password is changed via this private function, because it is not changed with the updateUser function
        // This explicit call is secure and can be logged
        $query = "
            UPDATE plr_usergroup
            SET userpassword = ?
            WHERE UCASE(USERGROUPNAME) = ?
        ";
        $_result = $this->database->Execute($query, [$hashedPassword, strtoupper($this->record['USERGROUPNAME'])]);
        if ($_result === false) {
            $this->owner->logUserAction('change pw failed', session_id(), $this->record['USERGROUPNAME']);
        } else {
            $this->owner->logUserAction('password changed', session_id(), $this->record['USERGROUPNAME']);
        }
        return $_result;
    }

    private function _doLogin($password='NONE', $ishashed=FALSE) {
        global $_CONFIG;

        $usertype = false;

        try {
            $this->owner->throttleLogins();
        } catch (Exception $e) {
            exit($e->getMessage());
        }

        if ($password === FALSE) {
            $_match = TRUE;
        } else {
            if ($ishashed) {
                $_match = ($password == $this->record['USERPASSWORD']);
            } else {
                $_match = $this->checkPassWordHardend($password, $this->record['USERPASSWORD']);
            }
        }

        if ($_match) {
            $usertype = 'normal';

            if ($this->record['CLIENTADMIN'] == 'Y')
                $usertype = 'client';
            if ($this->record['ROOTADMIN'] == 'Y')
                $usertype = 'root';

            // if $this->userAlreadyLoggedOn() return false;
            $outparams['clientid'] = $this->record['CLIENTID']; // Geef de clientid terug
            $outparams['clientname'] = $this->record['CLIENTNAME'];
            $outparams['imagesgroup_hash'] = $this->record['IMAGESGROUP_HASH'];
            $outparams['userid'] = $this->record['USERGROUPID'];
            $outparams['userhash'] = encodeString("{$outparams['clientid']}_{$this->record['USERGROUPID']}");
            $outparams['usergroupname'] = $this->record['USERGROUPNAME'];
            $outparams['language'] = $this->record['LANGUAGE'];
            $outparams['fullname'] = $this->record['FULLNAME'];
            $outparams['userphoto'] = $this->record['PHOTOURL'];
            $outparams['plrinstance'] = $this->record['PLRINSTANCE'];
            $outparams['options'] = $this->record['OPTIONS'];
            $outparams['usercss'] = $this->record['CUSTOMCSS'];
            $outparams['userbackground'] = $this->record['USER_BACKGROUND'];
            $outparams['2fa'] = ($this->record['2FA'] == 'Y');
            $outparams['phonenumber'] = $this->record['PHONENUMBER'];

            if ($this->record['ACCOUNTDISABLED'] !== 'Y') {
                // Update the number of times the user has logged in
                $this->updateLoginStats();
                $this->owner->logUserAction('success', session_id(), $this->record['USERGROUPNAME']);
                $outparams['changepassword'] = $this->shouldPasswordBeChanged($this->record['CHANGEPASSWORDNEXTLOGON'], $this->record['LASTPASSWORDCHANGEDATE'], $this->record['PASSWORDNEVEREXPIRES'], $this->record['USERCANNOTCHANGEPASSWORD']);
                if ($outparams['changepassword'] === 'Y') {
                    $this->owner->logUserAction('change password', session_id(), $this->record['USERGROUPNAME']);
                }
            } else {
                $usertype = false;
                $outparams['err'] = "3";
                // logging
                $this->owner->logUserAction('disabled account', session_id(), $this->record['USERGROUPNAME']);
            }
        }
        else {
            $outparams['err'] = "1";
            // logging
            $this->owner->logUserAction('INVALID_LOGIN', session_id(), $this->record['USERGROUPNAME']); // Don't change the label INVALID_LOGIN
        }
        $outparams['usertype'] = $usertype;
        return $outparams;
    }

    /**
     * Login a Polaris user
     */
    public function login($username, $password, $onlyclientadmin=false, $ishashed=false) {
        $this->loadRecordByName($username);

        if ($onlyclientadmin == TRUE) {
            $this->record['CLIENTADMIN'] == 'Y';
        }
        $this->outparams = $this->_doLogin($password, $ishashed);

        if ($this->outparams === FALSE || $this->outparams['usertype'] === FALSE) return false;

        /***
        * Create a Cookie session id and store it in the user record
        */
        $cookie_guid = $_SESSION['polarissession'];
        $this->updateUser(['lastlogindate' => __RAW__.'NOW()', 'logincount' => __RAW__.'LOGINCOUNT + 1', 'cookie_guid' => $cookie_guid]);
        return $this;
    }

    public function getUserPassword() {
        return $this->record['USERPASSWORD'];
    }

    public function GetAuthorisationType() {
        global $_CONFIG;

        $_authType = $this->record['AUTHTYPE'];
        if (!isset($_authType) || $_authType === FALSE) {
            $_authType = $_CONFIG['usergrouphandler_type'];
        }

        return $_authType;
    }

    public function hashPassword($password) {
        global $_CONFIG;

        $_hasher = new PasswordHash($_CONFIG['hash_cost_log2'], $_CONFIG['hash_portable']);
        return $_hasher->HashPassword($password);
    }

    public function checkPassWordHardend($password, $hashedPassword) {
        global $_CONFIG;

        $_hasher = new PasswordHash($_CONFIG['hash_cost_log2'], $_CONFIG['hash_portable']);
        return $_hasher->CheckPassword($password, $hashedPassword);
    }

    /**
    * Check a Polaris user account when it's being redirected to another Polaris instance (server)
    */
    public function checkInterInstanceLogin($servername, $key) {
        // needs fixing
        $params = array($servername, $key);
        $outparams = $this->_doLogin($key, $_query, $params, FALSE);

        return [$outparams['usertype'], $outparams];
    }

    private function updateLoginStats() {
        $this->updateUser(['lastlogindate' => __RAW__.'NOW()', 'logincount' => __RAW__.'LOGINCOUNT + 1', 'resethash' => 'NULL']);
    }

    private function userAlreadyLoggedOn() {
        global $_CONFIG;

        if (($usertype != 'root') and ($_CONFIG['multiplelogins'] != true)) {
            if ($sesskey = $this->_checkUserAlreadyLoggedOn($this->record['clientid'], $this->record['usergroupid'])) {
                $_SESSION['loggedinuser'] = md5($this->record['clientid']."_".$this->record['usergroupid']);
                $outparams['err'] = "4";
                $usertype = false;
            }
        }
    }

    public function SuspendUserAccess() {
        return $this->updateUser(['accountdisabled' => 'Y']);
    }

    public function AllowUserAccess() {
        return $this->updateUser(['accountdisabled' => 'N']);
    }

    public function GrantAdminRole() {
        $_orgAdminRole = $this->owner->getOrganisationRole($this->record['CLIENTID']);
        $this->addToGroup($_orgAdminRole);
        return $this->updateUser(['clientadmin' => 'Y']);
    }

    public function RevokeAdminRole() {
        $_orgAdminRole = $this->owner->getOrganisationRole($this->record['CLIENTID']);
        $this->removeFromGroup($_orgAdminRole);
        return $this->updateUser(['clientadmin' => 'N']);
    }

    public function ValidateUsername($username) {
        global $polaris;
        global $g_lang_strings;

        // De gebruikersnaam mag alleen letters, cijfers en de volgende tekens bevatten: @ . _ -
        if (empty(trim($username)) || !preg_match('/^[a-zA-Z0-9@._-]+$/', $username)) {
            throw new Exception($g_lang_strings['user_name_invalid']);
        }
    }

    public function ValidatePassword($password, $retype_password) {
        global $polaris;
        global $g_lang_strings;

        if (empty(trim($password)) || $password !== $retype_password) {
            throw new Exception($g_lang_strings['password_not_correct']);
        }
        if (!$this->IsPasswordStrongEnough($password)) {
            throw new Exception($g_lang_strings['password_not_strong_enough']);
        }
    }

    public function IsPasswordStrongEnough($password) {
        $strength = $this->checkPasswordStrength($password);
        return $strength['strength'] >= 4;
    }

    public function checkPasswordStrength($pwd) {
        $strength = array("Blanco", "Zeer zwak", "Zwak", "Gemiddeld", "Sterk", "Zeer sterk");
        $score = 1;

        if (strlen($pwd) < 1) {
            $score = 1;
        }
        if (strlen($pwd) < 4) {
            $score = 2;
        }
        if (preg_match("/\d{5}/u", $pwd, $matches) > 0) {
            $score = 2;
        }

        if (strlen($pwd) >= 8) {
            $score++;
        }
        if (strlen($pwd) >= 16) {
            $score = $score + 3;
        }

        if (preg_match("/[a-z]/", $pwd) && preg_match("/[A-Z]/", $pwd)) {
            $score++;
        }
        if (preg_match("/[0-9]/", $pwd)) {
            $score++;
        }
        if (preg_match("/.[!,@,#,$,%,^,&,*,?,_,~,-,£,(,)]/", $pwd)) {
            $score++;
        }
        if ($score > 5) $score = 5;
        return ['strength' => $score, 'label' => $strength[$score]];
    }

    /**
     * Change the password of a Polaris user account (also via xmlrpc)
     * Return:
     *   0   Password store failed
     *   1   Password too weak
     *   2   Password stored correctly
     */
    private function _changePassword($password, $skipStrengthTest=false) {
        $_result = 0;
        if ($this->IsPasswordStrongEnough($password) or $skipStrengthTest) {
            $_hashedPassword = $this->hashPassword($password);

            $_result = $this->_changePasswordField($_hashedPassword);

            if ($_result === false) {
                $this->owner->logUserAction('change pw failed', session_id(), $this->record['USERGROUPID']);
                $_result = PASSWORD_NOT_STORED;
            } else {
                $this->updateUser(['changepasswordnextlogon' => 'N', 'lastpasswordchangedate' => __RAW__.'NOW()']);

                $this->owner->logUserAction('change pw success', session_id(), $this->record['USERGROUPID']);
                $_SESSION['changepassword'] = 'N';
                $_result = PASSWORD_STORED;
            }
        } else {
            $_result = PASSWORD_TOO_WEAK;
            $this->owner->logUserAction('password not strong enough', session_id(), $this->record['USERGROUPID']);
        }

        return $_result;
    }

    public function ChangePasswordByName($username, $password, $skipStrengthTest=false) {
        $this->loadRecordByName($username);
        return $this->_changePassword($password, $skipStrengthTest);
    }

    public function ChangePassword($newpassword, $retype_password) {
        global $g_lang_strings;

        $this->ValidatePassword($newpassword, $retype_password);

        $_result = $this->_changePassword($newpassword);
        if ($_result == PASSWORD_TOO_WEAK) {
            throw new Exception($g_lang_strings['password_not_strong_enough']);
        }
        return $_result;
    }

    public function authenticateServiceCall($api_token, $api_call) {
        if (!empty($this->APIKEY)) {
            $_api_secret = $this->APISECRET;
            $_known_api_sig = "api_call{$_GET['app']}{$_GET['const']}api_key{$this->APIKEY}api_pwd{$this->record['USERPASSWORD']}";
            $_known_api_hash = hash_hmac('sha256', $_known_api_sig, $_api_secret);
            if (hash_equals($_known_api_hash, $api_token)) {
                $this->outparams = $this->_doLogin($this->record['USERPASSWORD'], ishashed:true);
                return $this->outparams;
            } else {
                return FALSE;
            }
        }
    }

    public function ChangePasswordNextLogin() {
        return $this->updateUser(['changepasswordnextlogon' => 'Y']);
    }

    public function addToGroup($grouphash) {
        $_result = false;
        $_groupObject = new base_plrUserGroup($this->owner, $grouphash);
        if (!empty($_groupObject) && !empty($_groupObject->record['USERGROUPID'])) {
            $_result = $_groupObject->database->Execute(
                'INSERT INTO plr_membership (clientid, usergroupid, groupid) VALUES (?,?,?)',
                [$this->record['CLIENTID'], $this->record['USERGROUPID'], $_groupObject->record['USERGROUPID']]
            );
        }
        return !empty($_result) ? true : false;
    }

    public function removeFromGroup($grouphash) {
        $_result = false;
        $_groupObject = new base_plrUserGroup($this->owner, $grouphash);
        if (!empty($_groupObject)) {
            $_result = $_groupObject->database->Execute(
                'DELETE FROM plr_membership WHERE clientid = ? AND usergroupid = ? AND groupid = ?',
                [$this->record['CLIENTID'], $this->record['USERGROUPID'], $_groupObject->record['USERGROUPID']]
            );
        }
        return $_result;
    }

    public function removeUser() {
        $_result = true;

        if ($this->record['USERORGROUP'] !== 'USER') {
            throw new Exception("This account is not a user.", E_USER_WARNING);
        }

        try {
            $this->owner->instance->Execute('DELETE FROM plr_membership WHERE clientid = ? AND usergroupid = ?', [$this->record['CLIENTID'], $this->record['USERGROUPID']]);
            $this->owner->instance->Execute('DELETE FROM plr_formpermission WHERE clientid = ? AND usergroupid = ?', [$this->record['CLIENTID'], $this->record['USERGROUPID']]);
            $this->owner->instance->Execute('DELETE FROM plr_usergroup WHERE clientid = ? AND usergroupid = ?', [$this->record['CLIENTID'], $this->record['USERGROUPID']]);
        } catch (ADODB_Exception $e) {
            $_result = false;
        }
        return $_result;
    }

    public function removeGroup() {
        $_result = true;

        if ($this->record['USERORGROUP'] !== 'GROUP') {
            throw new Exception("This account is not a user.", E_USER_WARNING);
        }

        try {
            $this->owner->instance->Execute('DELETE FROM plr_membership WHERE clientid = ? AND usergroupid = ?', [$this->record['CLIENTID'], $this->record['USERGROUPID']]);
            $this->owner->instance->Execute('DELETE FROM plr_formpermission WHERE clientid = ? AND usergroupid = ?', [$this->record['CLIENTID'], $this->record['USERGROUPID']]);
            $this->owner->instance->Execute('DELETE FROM plr_usergroup WHERE clientid = ? AND usergroupid = ?', [$this->record['CLIENTID'], $this->record['USERGROUPID']]);
        } catch (ADODB_Exception $e) {
            $_result = false;
        }
        return $_result;
    }

    public function removeFromApplication($applicationhash, $roletype="USERS") {
        $_result = false;

        $_userRoleID = $this->owner->getApplicationRole($this->record['CLIENTID'], $applicationhash, $roletype);
        if ($_userRoleID) {
            $_result = $this->owner->instance->Execute('DELETE FROM plr_membership WHERE clientid = ? AND usergroupid = ? AND groupid = ?', [$this->record['CLIENTID'], $this->record['USERGROUPID'], $_userRoleID]);
        }
        return $_result;
    }

    public function addToApplication($applicationhash) {
        $_result = false;

        $_userRoleID = $this->owner->getApplicationUserRole($this->record['CLIENTID'], $applicationhash);

        if ($_userRoleID) {
            $_result = $this->owner->instance->Execute('INSERT INTO plr_membership (clientid, usergroupid, groupid) VALUES (?,?,?)', [$this->record['CLIENTID'], $this->record['USERGROUPID'], $_userRoleID]);
        }

        return $_result;
    }

    /**
     * Add a Polaris user account
     */
    public function addUserAccount($clienthash, $username, $fullname, $description, $emailaddress, $password, $password_validate, $clientadmin, &$results, $confirmaccount=false, $options=false, $phonenumber='') {
        global $_sqlDSGClient;
        global $_GVARS;
        global $_CONFIG;
        global $scramble;

        $this->ValidateUsername($username);
        $this->ValidatePassword($password, $password_validate);

        $this->owner->connectInstance();
        $rs = @$this->owner->instance->Execute($_sqlDSGClient, array($clienthash));
        if ($clientrec = $rs->FetchNextObject('true')) {
            $clientid = $clientrec->CLIENTID;
        } else {
            trigger_error("Client hash not found.", E_USER_WARNING);
        }

        $clientadminval = $clientadmin ? 'Y' : 'N';
        $accountconfirmed = $confirmaccount ? 'N' : 'Y';
        $hashedPassword = $this->hashPassword($password);
        $use2fa = empty($_CONFIG['2fa_provider']) ? 'N' : 'Y';
        $query = "
            INSERT INTO plr_usergroup
            (CLIENTID, USERGROUPNAME, USERORGROUP, FULLNAME, PHONENUMBER, DESCRIPTION, EMAILADDRESS, USERPASSWORD, CLIENTADMIN, ACCOUNTCONFIRMED, CREATEDATE, OPTIONS, `2FA`)
            VALUES ('$clientid', '$username', 'USER', '$fullname', '$phonenumber', '$description', '$emailaddress', '$hashedPassword', '$clientadminval', '$accountconfirmed', now(), '$options', '$use2fa')
        ";

        try {
            $rs = $this->owner->instance->Execute($query);
            $userid = $this->owner->instance->Insert_ID();

            if (!$userid) {
                throw new Exception("Could not add user to Polaris.", E_USER_WARNING);
            }
            $query = "
                INSERT INTO plr_membership (clientid, usergroupid, groupid)
                SELECT clientid, $userid, usergroupid
                FROM plr_usergroup
                WHERE clientid = $clientid
                AND autocreatemembership = 'Y'
            ";
            $this->owner->instance->Execute($query);

            // send back the results (for the client of this service)
            $results = array();
            $results['clienthash'] = $clienthash;
            $results['usergroupid'] = $userid;
            $results['usergroupname'] = $username;
            $results['emailaddress'] = $emailaddress;
            if ($confirmaccount)
                $results['confirmurl'] = $_GVARS['serverroot'].'/confirm.php?a='.md5($clientid .'_'. $userid.$scramble);
        } catch (Exception $e) {
            if ($e->getCode() == 1062) {
                throw new Exception('De gebruikersnaam of emailadres is al in gebruik. Kies een andere naam of emailadres.');
            } else {
                throw new Exception($e->getMessage());
            }
        }

        try {
            $_userhash = encodeString("{$clientid}_{$userid}");
            $_newUser = new base_plrUserGroup($this->owner, $_userhash);
            if (empty($_newUser->record))
                return false;
        } catch (Exception $e) {
            throw new Exception("Could not load user record.", E_USER_WARNING);
        }

        return $_newUser;
    }

    private function updateUserTable($tablename, $fieldvalues, $changeableFields) {
        $_updateParts = [];
        $_updateValues = [];
        foreach ($changeableFields as $_fieldname => $_validValues) {
            if (isset($fieldvalues[$_fieldname]) && ($_validValues === false || in_array($fieldvalues[$_fieldname], explode(',', $_validValues)))) {
                $_value = $fieldvalues[$_fieldname];
                if (substr($_value, 0, 7) === __RAW__) {
                    $_updateParts[] = "{$_fieldname} = " . substr($_value, 7);
                } else {
                    $_updateParts[] = "{$_fieldname} = ?";
                    $_updateValues[] = $fieldvalues[$_fieldname];
                }
            }
        }

        if (empty($_updateParts)) {
            return false;
        }

        $_updateParts = implode(', ', $_updateParts);
        $query = "
            UPDATE {$tablename}
            SET {$_updateParts}
            WHERE clientid = ? AND usergroupid = ?
        ";

        $_updateValues[] = $this->record['CLIENTID'];
        $_updateValues[] = $this->record['USERGROUPID'];

        $this->owner->connectInstance();
        $result = $this->owner->instance->Execute($query, $_updateValues);

        if ($result === false) {
            $this->owner->logUserAction('User update failed', session_id(), $this->record['USERGROUPNAME']);
        }
        return $result;
    }
    /**
     * Change the properties of a Polaris user account
     */
    public function updateUser($fieldvalues) {
        if (empty($this->record) || empty($this->record['USERGROUPID'])) {
            throw new Exception("No user record loaded.", E_USER_WARNING);
        }

        $_changeableFields = [
            'USERGROUPNAME' => FALSE,
            'FULLNAME' => FALSE,
            'DESCRIPTION' => FALSE,
            'EMAILADDRESS' => FALSE,
            'PHOTOURL' => FALSE,
            'CLIENTADMIN' => 'Y,N',
            'CHANGEPASSWORDNEXTLOGON' => 'Y,N',
            'USERCANNOTCHANGEPASSWORD' => 'Y,N',
            'PASSWORDNEVEREXPIRES' => 'Y,N',
            'ACCOUNTDISABLED' => 'Y,N',
            'LANGUAGE' => FALSE,
            'SERVERMAPPING' => FALSE,
            'OPTIONS' => FALSE,
            'COOKIE_GUID' => FALSE,
            'LOGINCOUNT' => FALSE,
            'RESETHASH' => FALSE,
            'LASTLOGINDATE' => FALSE,
            'LASTPASSWORDCHANGEDATE' => FALSE,
            '2FA' => 'Y,N',
            'PHONENUMBER' => FALSE,
            '2FACODE' => FALSE,
        ];

        // Normalize fieldvalues keys to lowercase
        $normalizedFieldValues = array_change_key_case($fieldvalues, CASE_UPPER);

        $result = $this->updateUserTable($this->tablename, $normalizedFieldValues, $_changeableFields);
        $result = $this->updateUserSettings($normalizedFieldValues);

        return $result;
    }

    public function updateUserSettings($fieldvalues) {
        if (empty($this->record) || empty($this->record['USERGROUPID'])) {
            throw new Exception("No user record loaded.", E_USER_WARNING);
        }

        $_changeableFields = [
            'POSITIONMENU' => FALSE,
            'HEADERCOLOR' => FALSE,
            'STARTUPMESSAGE' => FALSE,
            'NAVIGATIONSTYLE' => FALSE,
        ];

        foreach($fieldvalues as $key => $value) {
            if (array_key_exists($key, $_changeableFields)) {
                $this->owner->instance->Replace(
                    $this->settingstablename,
                    array(
                        "clientid" => $this->record['CLIENTID'],
                        "usergroupid" => $this->record['USERGROUPID'],
                        "applicationid"=>1,
                        "setting"=>$key,
                        "value"=>$value)
                , array("clientid","usergroupid","applicationid","setting"), true);
            }
        }

        return true;
    }

}