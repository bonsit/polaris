<?php

class base_plrPage extends base_plrRecord {
    var $lines = null;

    function __construct(&$aowner) {
      parent::__construct($aowner);
    }

    function LoadRecord() {
      if (!isset($this->lines)) {
        $this->lines = new base_plrLines($this);
        $this->lines->LoadRecords();
      }
    }

    function GetPageTabName() {
      return 'form'.$this->record->FORMID.'_page'.$this->record->PAGEID;
    }

    function GetPageColumnCount(&$lines, $pagecolumncount) {
      $_result = $pagecolumncount;
      $_max = 0;
      foreach($lines as $line) {
        if ($line->record->COLUMNGROUPINDEX > $_max)
          $_max = $line->record->COLUMNGROUPINDEX;
      }
      if ($_max > $pagecolumncount) {
        $_result = $_max;
      }
      return $_result;
    }

    function Show($userdb, $rec, $recordid, $state, $permissiontype, $masterrecord, $pinvalues) {
        global $_GVARS;
        global $_CONFIG;

        if ($this->record->JASPERREPORTNAME != '') {
            $_po = $this->polaris();
            $reporturl = str_replace("%REPORTNAME%",$this->record->JASPERREPORTNAME,$_CONFIG["polarisreporturl"]);
            $reporturl = str_replace("%REPORTPARAMS%",$this->record->JASPERREPORTPARAMS,$reporturl);
            $linkhref = $_po->ReplaceDynamicVariables($reporturl, '', $rec, $this->owner->owner->database);
            echo '<div id="reportview" class="reportview"><p id="evengeduld">Even geduld a.u.b. <i class="fa fa-spinner fa-spin"></i></p>';
            echo '<iframe id="ireport" style="width:100%;min-height:500px;height:75%;border:0;" do_src="'.$linkhref.'"></iframe>';
            echo '</div>';
        } else {
            $hascomponent = false;
            $groupclass = array('odd','even');
            $linekeys = array_keys($this->lines->lines);
            $pagecolumncount = intval($this->record->PAGECOLUMNCOUNT);
            $totallines = count($linekeys);
            $pagecolumncount = $this->GetPageColumnCount($this->lines->lines, $pagecolumncount);
            echo '<div id="formview" class="formview alignlabels groups_'.$pagecolumncount.'">';

            $_grid_column_settings = 'xs-col-12 col-md-6';

            if ($pagecolumncount > 1 or $line->record->COLUMNGROUPINDEX > 0)
                echo '<div class="'.$_grid_column_settings.'">';
            foreach($linekeys as $index ) {
                $line =& $this->lines->lines[$index];
                 if ($line->record->GROUPID !== $this->lines->lines[$index-1]->record->GROUPID) {
                     if ($hascomponent) {
                         echo '</div></div>';
                         $hascomponent = false;
                     }
                     if ($line->record->GROUPID != NULL and $line->record->GROUPID != '0') {
                         echo '<div class="componentblock '.$groupclass[$index % 2].'" id="pagegroup'.$index.'">';
                         echo '<div class="componentcontent" id="componentcontent'.$index.'">';
                         $hascomponent = true;
                     }
                 }
                if (
                  $pagecolumncount > 1 and (round(($totallines+2)/2)+1 == ($index + 1))
                or (($line->record->COLUMNGROUPINDEX !== $this->lines->lines[$index-1]->record->COLUMNGROUPINDEX
                  and isset($this->lines->lines[$index-1]->record->COLUMNGROUPINDEX))
                )) {
                    echo '</div><div class="'.$_grid_column_settings.'">';
                }
                $line->Show($userdb, $rec, $state, $permissiontype, $masterrecord, $pagecolumncount, $pinvalues);
            }
            if ($hascomponent) {
                echo '</div></div>';
            }
            if ($pagecolumncount > 1 or $line->record->COLUMNGROUPINDEX > 0) {
                echo '</div>';// .columngroup
                echo '<div class="columngroupreset"></div>';
            }
            echo '</div>';
        }
    }

    function ShowSearchFields($userdb) {
        $fieldcount = 0;
        foreach( array_keys($this->lines->lines) as $index ) {
            $line =& $this->lines->lines[$index];
            $columnlabel = $line->GetSearchField($userdb);
            $line->ShowSearchField($columnlabel);
            $fieldcount++;
        }
        return $fieldcount;
    }
}
