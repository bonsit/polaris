<?php
global $_DOCROOT;
global $fckversion;

$fckversion = 'fckeditor.2.3';

require_once($_DOCROOT.DIRECTORY_SEPARATOR."guicomponents".DIRECTORY_SEPARATOR."$fckversion".DIRECTORY_SEPARATOR."fckeditor.php") ;

// Create a wrapper class extended from FckEditor
class htmlEditor extends FCKeditor {
    var $__fckversion;

	function __construct($editorname='FCKEditor1', $width=false, $height=false, $required='') {
        global $fckversion, $_DOCROOT, $_GVARS;

        ##  INSTANTIATE THE CLASS
        $this->FCKeditor($editorname);

        ##  SET THE CORRECT VERSION THAT POLARIS USES
        $this->__fckversion = $fckversion;

        ##  SET UP THE BASE PATH
        //    $this->BasePath = basename(dirname(dirname(__FILE__))).DIRECTORY_SEPARATOR.'guicomponents'.DIRECTORY_SEPARATOR.$this->$__fckversion.DIRECTORY_SEPARATOR ;
        //    $this->BasePath = '/'.basename(dirname(dirname(__FILE__))).'/guicomponents/'.$this->$__fckversion.'/' ;
        $this->BasePath = $_GVARS['serverroot'].'/guicomponents/'.$this->__fckversion.'/' ;

        ##  SET UP THE EDITOR VARIABLES
        $this->CanUpload = false ;  // Overrides fck_config.js default configuration
        $this->CanBrowse = false ;  // Overrides fck_config.js default configuration
        $this->ToolbarSet = 'Default_Ext';
        $this->required = $required;

        if ($width)
            $this->Width = $width;
        if ($height)
           $this->Height = $height;

        ##  SET EDITOR CONFIG VARIABLES
        // Any config varibles found in the 'fckconfig.js' file can be set here.
        $this->Config['ToolbarCanCollapse'] = false;
        // Set the path to skin folder
        $this->Config['SkinPath'] = $this->BasePath.'editor/skins/silver/';
        // Set the path to this application images folder
        //    $this->Config['ImageBrowserURL'] = '/'.$this->BasePath.'editor/filemanager/browser/default/browser.html?Connector=extra_connectors/php/connector.php&ServerPath=/home/httpd/vhosts/verlorenuurke.be/httpdocs/user_images/';
        $this->Config['BaseHref'] = '';
        //    $this->Config['EditorAreaCSS'] = 'http://www.verlorenuurke.be/css/default2.css';
	}

	function Render() {
	    $this->Create();
	}

	function Create() {
        return $this->CreateHtml();
	}
}
