<?php

class base_plrDatabase extends base_plrRecord {
    var $userdb;

    function __construct($aowner, $key=false) {
        parent::__construct($aowner, $key);
        if (strcasecmp(get_class($aowner), 'Polaris') == 0)
            $this->__polaris = $aowner;
    }

    function LoadRecord($key=false) {
        global $_sqlDatabase;
        $thekey = ($key)?$key:$this->key;
        if ($rs = $this->dbExecute($_sqlDatabase, $thekey)) {
            $this->record = $rs->FetchNextObject(true);
        } else
            return false;
    }

    function LoadRecordHashed($hasheddatabaseid) {
        global $_sqlDatabaseHashed;

        if (isset($hasheddatabaseid)
        and (strlen($hasheddatabaseid) == 32)  /* MD5 hashed value is 32 chars long */
        and ($rs = $this->dbExecute($_sqlDatabaseHashed, array($hasheddatabaseid))))
            $this->record = $rs->FetchNextObject(true);
        else {
            echo "could not locate userdatabase: $hasheddatabaseid (hashed id)";
            return false;
        }
    }

    function Show() {
        echo $this->record->HOST.' '.$this->record->DATABASENAME.'<br/>';
    }

    function GetUserDBInstance() {
        if ($this->IsOracle()) {
            $_searchstring = 'SERVICE_NAME';
            $_pos = strpos($this->record->HOST, $_searchstring);
            $_strlen = strlen($_searchstring);

            if ($_pos == false) {
                $_searchstring = 'SID';
                $_pos = strpos($this->record->HOST, $_searchstring);
                $_strlen = strlen($_searchstring);
            }
            $_posend = strpos($this->record->HOST, ')', $_pos);
            if ($_pos !== false) {
                $result = str_replace(array(' ','='),'',substr($this->record->HOST, $_pos + $_strlen, $_posend - ($_pos + $_strlen)));
            } else {
                $result = substr($this->record->HOST, 0, 10);
            }
        } else {
            $result = isset($this->record) and $this->record?$this->record->HOST:'';
        }
        return $result;
    }

    function connectUserDB() {
        global $_CONFIG;

        if (!isset($this->userdb) and isset($this->record) and $this->record) {
            $plrins =& $this->polaris();
            $plrins->timer->startTimer('userdatabase_connect');
            $this->userdb = ADONewConnection($this->record->DATABASETYPE);
            $this->userdb->firstrows = isset($_GET['nofirstrows'])?false:true; // 'true' zorgt voor langzame zoekactie in Teveel Betaalde Bedragen, maar 'false' levert overal trage SQL's op

            try {
                switch ($this->record->DATABASETYPE) {
                case 'oci8':
                case 'oci8plr':
                    $this->userdb->PConnect($this->record->HOST, $this->record->ROOTUSERNAME, $this->record->DECROOTPASSWORD);
                    /* If the desired schema is not the current user, then set the databasename as the schema  */
                    //if (isset($this->record->DATABASENAME) and $this->record->DATABASENAME != '' and strcasecmp($this->record->DATABASENAME, $this->record->ROOTUSERNAME) != 0) {
                    if (isset($this->record->DATABASENAME) and $this->record->DATABASENAME) {
                        $this->userdb->Execute("ALTER SESSION SET current_schema=".$this->record->DATABASENAME);
                    }
                    if ($_CONFIG['floatdecimalchar'] == '.')
                        $this->userdb->Execute("ALTER SESSION SET NLS_NUMERIC_CHARACTERS=\".,\"");
                    else
                        $this->userdb->Execute("ALTER SESSION SET NLS_NUMERIC_CHARACTERS=\",.\"");
                    $this->SetOracleUserID($_SESSION['userid']);
                    $this->SetOracleUserLanguage($_SESSION['lang']);
                    break;
                case 'access':
                    $this->userdb->Connect($this->record->DATABASENAME);
                    break;
                case 'ldap':
                    $this->userdb->Connect($this->record->HOST, $this->record->ROOTUSERNAME, $this->record->DECROOTPASSWORD, $this->record->DATABASENAME);
                    break;
                default:
                    $this->userdb->Connect($this->record->HOST, $this->record->ROOTUSERNAME, $this->record->DECROOTPASSWORD, $this->record->DATABASENAME);
                    $this->userdb->Execute("SET NAMES utf8mb4");
                    $this->userdb->Execute("SET lc_time_names = 'nl_NL'");
                    $this->userdb->Execute("SET time_zone = '+02:00'");
                    break;
                }
                if (!$this->userdb)
                    trigger_error('cannot_connect_database', E_USER_WARNING);
            } catch (exception $E) {
                echo $E->msg;
                return $E->msg;
            }

            $plrins->timer->endTimer('userdatabase_connect');
        } else {
            if (!isset($this->record))
                trigger_error('cannot_connect_database', E_USER_WARNING);
        }
        return $this->userdb;
    }

    function SQLHasParams($sql) {
        $result = false;
        $varcount = preg_match_all("/:\\w+/", $sql, $out);
        if ($varcount > 0) {
            $result = $out;
        }
        return $result[0];
    }

    function customUrlEncode($url) {
        if ($this->IsOracle())
            return str_replace(array('+','/'), array('^','$'), $url);
        else
            return $url;
    }

    function customUrlDecode($url) {
        if ($this->IsOracle())
            return str_replace(array('^','$'), array('+','/'), $url);
        else
            return $url;
    }

    function loadTableRecord($tablename) {
        global $_sqlTable;
        return $this->dbGetRow($_sqlTable, array($this->record->CLIENTID, $this->record->DATABASEID, $tablename));
    }

    function metaPrimaryKeys($tablename, $tableprefix=false) {
        global $scramble;
        global $TABLEPREFIX;

        if ($tableprefix) {
            if ($tableprefix === TRUE) {
                $tableprefix = $TABLEPREFIX.'.';
            } else {
                $tableprefix = $tableprefix.'.';
            }
        }

        $clientid = $this->record->CLIENTID;
        $databaseid = $this->record->DATABASEID;
        if ( strtoupper(substr($tablename, 0, 5)) == 'SQL::' ) {
            if ( strpos(strtoupper($tablename), 'AS '._RECORDID) > 0 )
                $keys[] = _RECORDID;
        } else {
            $sql = " SELECT columnname FROM plr_column";
            $sql .= " WHERE clientid = '$clientid' AND databaseid = '$databaseid' AND tablename = '$tablename'";
            $sql .= " AND keycolumn = 'Y' ORDER BY position ASC";

            $rs = $this->dbExecute($sql) or Die('Query failed: '.$sql);
            while ($rec = $rs->FetchNextObject()) {
                if ($tableprefix)
                    $keys[] = $tableprefix.$rec->COLUMNNAME;
                else
                    $keys[] = $rec->COLUMNNAME;
            }

            /**
            *  If PLR_COLUMN does not have the columns
            *  then look with the ADODB function MetaPrimaryKeys for the key columns
            */
            if (($keys ?? null) == null) {
                $this->connectUserDB();
                $keys = $this->userdb->MetaPrimaryKeys($tablename);
                if ($keys) {
                    foreach($keys as $k => $v) {
                        if (!$tableprefix)
                            $keys[$k] = $v;
                        else {
                            if ($tableprefix)
                                $keys[$k] = $tableprefix.$v;
                            else
                                $keys[$k] = $v;
                        }
                    }
                }
            }
        }
        return $keys;
    }

    function makeEncodedKeySelect($table, $recordid, $tableprefix=false, $isview=false) {
        $column = $this->makeRecordIDColumn($table, $tableprefix, $fallbacktofieldnames=true, $isview, $usage='where');
        $sql = '';
        if ($column) {
            if (is_array($recordid)) {
                foreach($recordid as $_recid) {
                    $sql .= "$column = '$_recid' OR ";
                }
                $sql = substr($sql, 0, -4);
            } else {
                $sql .= "$column = '$recordid'";
            }
        } else {
            $sql = 'No key column in PLR_COLUMN | error: '.$table;
        }
        return $sql;
    }

    function makeRecordID($primkeys) {
        global $scramble;

        if (is_array($primkeys)) {
            $this->connectUserDB();

            $result = 'MD5(';
            $_pks = implode(", '_' ,",$primkeys);
            $result .= $this->userdb->Concat("'$scramble'","'_'",$_pks);
            $result .= ") ";
            return $result;
        } else {
            return false;
        }
    }

    function makeRecordIDColumn($table=false, $tableprefix=false, $fallbacktofieldnames=false, $isview=false, $usage=false) {
        global $scramble;

        if (!$table && strcasecmp(get_class($this->owner), 'base_plrForm') == 0) $table = $this->owner->GetDataDestination();
        // $table = strtolower($table);
        switch($this->record->DATABASETYPE) {
        case 'oci8plr':
            // in case it's a view (in Oracle), then it is very possible that you cannot use ROWID, because of a possible UNION
            // NOT ELEGANT CODING, needs improvement: Move 'RIJNR' to the PLR database?
            if ($tableprefix) $tableprefix = $tableprefix.'.';
            $_rowidname = ($isview)?'RIJNR':'ROWID';
            if ($usage == 'where')
                $result = " {$tableprefix}{$_rowidname}";
            else
                $result = " ROWIDTOCHAR({$tableprefix}{$_rowidname})";
        break;
        case 'pdo':
        case 'mysql':
        case 'mysqli':
        case 'mysqlplr':
            $_primkeys = $this->metaPrimaryKeys($table, $tableprefix);
            /**
            * if PLR cannot determine keycolumns for the table, then use ALL fieldnames
            */
            if (!$_primkeys and $fallbacktofieldnames) {
                $fieldnames = $this->userdb->MetaColumns($table);
                foreach($fieldnames as $fieldname => $fieldinfo) {
                    $_primkeys[] = $fieldname;
                }
            }
            $result = $this->makeRecordID($_primkeys);
        break;
        }
        return $result;
    }

    function MakeFieldSetValueString($fieldvalues, $and=false, $fordelete=false, $datatype=false) {
        $values = array_values($fieldvalues);
        $fields = array_keys($fieldvalues);
        for ($i=0; $i<count($values); $i++) {
            switch ($datatype) {
                case 'J':
                    // in case of empty array, then set the value to '(0)'
                    if ($values[$i] == '[]') {
                        $setofvalues = '(NULL)';
                    } else {
                        $setofvalues = str_replace(['[', ']', '&quot;'], ['(', ')', '"'], $values[$i]);
                        if (!isset($setofvalues) or !$setofvalues) {
                            $setofvalues = '(0)';
                        }
                    }
                    $setstring[$i] = $fields[$i] . ' IN ' . $setofvalues;
                    break;
                default:
                    if ($values[$i] == '') {
                        if ($fordelete)
                            $values[$i] = '\'\''; // 'null' werkte niet goed bij de DeleteRecord
                        else
                            $values[$i] = 'null';
                    } elseif ($this->isValidDate($values[$i])) {
                            $values[$i] = $this->userdb->DBDate(str2date($values[$i]));
                    } elseif (!isDatabaseFunction($values[$i])) {
                        $values[$i] = $this->userdb->qstr($values[$i], '\'');//"'".$values[$i]."'";
                    }
                    $setstring[$i] = $fields[$i] . ' = ' . $values[$i];
                    break;
            }
        };
        if ($and)
            return implode(' and ',$setstring);
        else
            return implode(', ',$setstring);
    }

    private function isValidDate($date) {
        if (empty($date)) {
            return false;
        }

        $parsedDate = date_parse($date);
        return $parsedDate['error_count'] === 0 && checkdate($parsedDate['month'], $parsedDate['day'], $parsedDate['year']);
    }

    function MakeFieldsString($fieldvalues) {
        $fields = array_keys($fieldvalues);
        return implode(',',$fields);
    }

    function MakeValuesString($fieldvalues, $saveemptyasnull=true) {
        $values = $this->ProcessValues($fieldvalues, $saveemptyasnull);
        return implode(',',$values);
    }

    function MakeCallFieldsValuesString($fieldvalues, $fordelete=false) {
        // CALL PROC(X=>A, Y=B);
        if ($this->IsOracle()) {
            $fields = array_keys($fieldvalues);
            $values = array_values($fieldvalues);
            for ($i=0; $i<Count($values); $i++) {
                if ($values[$i] == '')
                    if ($fordelete)
                        $values[$i] = '\'\''; // 'null' werkte niet goed bij de DeleteRecord
                    else
                        $values[$i] = 'null';
                elseif (str2date($values[$i]) >= 0) {
                    $values[$i] = $this->userdb->DBDate(str2date($values[$i]));
                }
                // elseif (!isDatabaseFunction($values[$i]))
                //     $values[$i] = $this->userdb->qstr($values[$i],get_magic_quotes_gpc());
                $setstring[$i] = $fields[$i].' => '.$values[$i];
            };
            $_result = implode(', ',$setstring);
        } else {

        }
        return $_result;
    }

    function ProcessFieldValues($fieldvalues, $saveemptyasnull=true, $skipemptyfields=false) {
        if ($skipemptyfields)
            $skipemptyfields = explode(';',$skipemptyfields);

        foreach($fieldvalues as $field => $value) {
            // Skip empty fields?
            if (!isDatabaseFunction($value)) {
                if ($skipemptyfields and in_array($field, $skipemptyfields) and $value== '') {
                    unset($fieldvalues[$field]);
                } elseif ($saveemptyasnull and $value== '') {
                    $fieldvalues[$field] = 'NULL';
                } else {
                    // $fieldvalues[$field] = $this->userdb->qstr($value,get_magic_quotes_gpc());
                }
            }
        }
        return $fieldvalues;
    }

    function ProcessValues($values, $saveemptyasnull=true) {
        $values = array_values($values);
        for ($i=0;$i < Count($values);$i++) {
            if (str2date($values[$i]) >= 0) {
                $values[$i] = $this->userdb->DBDate(str2date($values[$i]));
            }
            elseif (!isDatabaseFunction($values[$i])) {
                if ($saveemptyasnull and $values[$i] == '')
                    $values[$i] = 'NULL';
                else
                    // $values[$i] = $this->userdb->qstr($values[$i],get_magic_quotes_gpc());
                    $values[$i] = $this->userdb->quote($values[$i]);
            }
        };
        return $values;
    }

    function ReadGenID($seqname) {
        $_genIDSQL = "SELECT ($seqname.currval) FROM DUAL";
        return $this->userdb->GetOne($_genIDSQL);
    }

    function ProcessGeneratedFields($fieldvalues, $autoincrement) {
        $autosupervalues = $fieldvalues;

        if (is_array($fieldvalues)) {
            foreach($fieldvalues as $field => $value) {
                $_newvalue = false;

                if (strpos($value, '@READGEN.') !== false ) {
                    // in case the value comes from a sequence generator
                    $_seq = substr($value, strpos($value,'.')+1);
                    $_genvalue = $this->ReadGenID($_seq);
                    if (isset($_genvalue)) {
                        $autosupervalues[$field] = $_genvalue;
                    }
                }
            }
        }
        return $autosupervalues;
    }

    function ProcessAutoIncrementFields($tablename, &$fieldvalues) {
        $autosupervalues = false;

        if (is_array($fieldvalues)) {
            $keys = array_toupper($this->userdb->MetaPrimaryKeys($tablename));
            foreach($fieldvalues as $field => $value) {
                $_newvalue = false;

                if (strpos($value, '@GEN.') !== false ) {
                    // in case the value comes from a sequence generator
                    $_seq = substr($value, strpos($value,'.')+1);
                    $_newvalue = $this->userdb->GenID($_seq);

                    if ($_newvalue) {
                        $fieldvalues[$field] = $_newvalue;
                        $autosupervalues[$field] = str_replace('@GEN.','@READGEN.',$value);
                    }
                } elseif (strpos($value, '@READGEN.') !== false ) {
                    $_newvalue = null;

                    $fieldvalues[$field] = $_newvalue;
                    $autosupervalues[$field] = $value;
                } elseif ($value === '__auto_increment__') {
                    // in case the value comes from a sequence generator
                    $sql = "SELECT $field FROM $tablename ";
                    if ($keys) {
                        $fieldIsMemberOfKey = in_array_cin($keys, $field);
                        $ki = 0;
                        $key = $keys[$ki];
                        if ($fieldIsMemberOfKey and isset($fieldvalues[$key])) {
                            $sql .= ' WHERE ';
                            while($key != $field and ($ki < count($keys))) {
                                $sql .= $key .' = "'. $fieldvalues[$key] .'" AND ';
                                $ki++;
                                $key = $keys[$ki];
                            }
                            $sql = substr($sql,0,-5);
                        } elseif (count($keys) > 1) {
                            // if field is not part of the key, then take only the first keyfield
                            // as a where clause
                            $sql .= ' WHERE ';
                            $sql .= $key .' = "'. $fieldvalues[$key] . '"';
                        }
                    }
          // MySQL          $sql .= " ORDER BY $field + 0 DESC";
                    $sql .= " ORDER BY $field DESC "; // Oracle: + 0 is te traag
                    $maxvalue = $this->userdb->GetOne($sql,$inputarr=false);

                    $_newvalue = $maxvalue ? $maxvalue + 1 : 1;
                    if ($_newvalue) {
                        $fieldvalues[$field] = $_newvalue;
                        $autosupervalues[$field] = $fieldvalues[$field];
                    }
                }
            }
        }
        return $autosupervalues;
    }

    function replaceRecord($tablename, $keyfields, $fieldvalues, &$autosupervalues, $autoQuote = false, $skipemptyfields=false) {
        global $polaris;

        $tablename = strtolower($tablename);

        $asv = $this->ProcessAutoIncrementFields($tablename, $keyfields);
        if ($autosupervalues) {
            if ($asv) $autosupervalues = array_merge($autosupervalues, $asv);
        } else {
            $autosupervalues = $asv;
        }

        $fieldvalues = $this->ProcessFieldValues($fieldvalues, $saveemptyasnull=true, $skipemptyfields);

        try {
            $ret = $this->userdb->Replace($tablename, $fieldvalues, $keyfields, $autoQuote);
        } catch (ADODB_Exception $E) {
            $msg = $E->msg;
            $msg .= "\r\n\r\n".implode(", ", $keyfields);
            $msg .= "\r\n\r\n".implode(", ", $fieldvalues);
            adodb_plr_throw($E->dbms, $E->fn, $E->getCode(), $E->msg, null, null, $this->userdb, $msg);
        }

        if ($ret === false) {
            trigger_error($this->userdb->ErrorMsg().' '.$this->userdb->ErrorNo(), WARNING);
            return false;
        } else {
            return $this->userdb->Insert_ID();
        }
    }

    function updateRecord($tablename, $keyfields, $fieldvalues) {
        global $polaris;

        $fieldAssignSQL = $this->MakeFieldSetValueString($fieldvalues);
        if (is_array($keyfields))
            $keyfieldselect = $this->MakeFieldSetValueString($keyfields, $and=true);
        else
            $keyfieldselect = $keyfields;

        $query  = " UPDATE /* updateRecord */ $tablename";
        $query .= " SET $fieldAssignSQL";
        $query .= " WHERE $keyfieldselect";

        try {
            $this->userdb->Execute($query);
        } catch (ADODB_Exception $E) {
            $msg = $E->msg;
            $msg .= "\r\n\r\n".$query;
            adodb_plr_throw($E->dbms, $E->fn, $E->getCode(), $E->msg, null, null, $this->userdb, $msg);
        }
        $_result = $this->userdb->Affected_Rows() > 0;
        if (!$_result) $polaris->logUserAction($status="No affected rows on update", session_id(), $_SESSION['name'], $query.' '.$this->userdb->ErrorMsg().' '.$this->userdb->ErrorNo());

        return ($_result);
    }

    function deleteHashedRecord($tablename, $recordID) {
        $keyfieldselect = $this->makeEncodedKeySelect($tablename, $recordID);
        $this->deleteRecord($tablename, $keyfieldselect);
    }

    function deleteRecord($tablename, $keyfields) {
        global $polaris;

        if (is_array($keyfields))
            $keyfieldselect = $this->MakeFieldSetValueString($keyfields, $and=true, $fordelete=true);
        else
            $keyfieldselect = $keyfields;

        $tablename = strtolower($tablename);
        $executeDelete = true;
        $_plrTable = $this->loadTableRecord($tablename);
        if (($_plrTable['DELETEPROCEDURE'] ?? '') == '__PLR_VIRTUAL_DELETE') {
            $checkifdeleted = $this->userdb->GetOne("SELECT __DELETED FROM $tablename WHERE $keyfieldselect");
            $executeDelete = $checkifdeleted == '0';
            $query  = ' UPDATE '.$tablename;
            $query .= ' SET __DELETED = 1';
            $query .= ' WHERE '.$keyfieldselect;
        } else {
            $query  = ' DELETE FROM '.$tablename;
            $query .= ' WHERE '.$keyfieldselect;
        }

        if ($executeDelete) {
            try {
                $this->userdb->Execute($query);
            } catch (ADODB_Exception $E) {
                $msg = $E->msg;
                $msg .= "\r\n\r\n".$query;
                adodb_plr_throw($E->dbms, $E->fn, $E->getCode(), $E->msg, null, null, $this->userdb, $msg);
            }

            $_result = ($this->userdb->Affected_Rows() > 0);
            if (!$_result) {
                $polaris->logUserAction($status="delete failed", session_id(), isset($_SESSION['name'])?$_SESSION['name']:'', $query.' '.$this->userdb->ErrorMsg().' '.$this->userdb->ErrorNo());
                // trigger_error($this->userdb->ErrorMsg().' '.$this->userdb->ErrorNo(), E_USER_WARNING);
            }
        } else {
            $_result = true;
        }
        return $_result;
    }

    function insertRecord($tablename, $fieldvalues, &$autosupervalues) {
        global $polaris;

        $tablename = strtolower($tablename);
        $asv = $this->ProcessAutoIncrementFields($tablename, $fieldvalues);
        $callDBProc = $this->FindDBCallProcedure('INSERT', $tablename);
        if (isset($callDBProc) and $callDBProc !== '') {
            $_callparameters = $this->MakeCallFieldsValuesString($fieldvalues);
            $query = "
                BEGIN
                    $callDBProc($_callparameters);
                END;
            ";
        } else {
            $query  = " INSERT INTO $tablename (".$this->MakeFieldsString($fieldvalues).")";
            $query .= ' VALUES('.$this->MakeValuesString($fieldvalues).')';
        }

        try {
            $rs = $this->userdb->Execute($query);
        } catch (ADODB_Exception $E) {
            $msg = $E->msg;
            $msg .= "\r\n\r\n".$query;
            adodb_plr_throw($E->dbms, $E->fn, $E->getCode(), $E->msg, null, null, $this->userdb, $msg);
        }
        if ($rs === false) {
            $polaris->logUserAction($status="insert failed", session_id(), $_SESSION['name'], $query.' '.$this->userdb->ErrorMsg().' '.$this->userdb->ErrorNo());
            return false;
        } else {
            if ($this->IsOracle()) {
                $asv = $this->ProcessGeneratedFields($asv);
                if ($autosupervalues) {
                    if ($asv) $autosupervalues = array_merge($autosupervalues, $asv);
                } else {
                    $autosupervalues = $asv;
                }

                return $autosupervalues;
            } else {
                $_autoincrement = $this->userdb->Insert_ID();
                $asv = $this->ProcessGeneratedFields($fieldvalues, $_autoincrement);
                if ($autosupervalues) {
                    if ($asv) $autosupervalues = array_merge($autosupervalues, $asv);
                } else {
                    $autosupervalues = $asv;
                }

                return $_autoincrement;
            }
        }
    }

    function getRecord($tablename, $hashedkey, $toupper=false) {
        $keyfieldselect = $this->makeEncodedKeySelect($tablename, $hashedkey);

        $query  = ' SELECT * FROM '.strtolower($tablename);
        $query .= ' WHERE '.$keyfieldselect;
        $rs = $this->userdb->Execute($query);
        if ($rs === false) die("query failed: ". $query);
        return $rs->FetchNextObject($toupper);
    }


    function FindDBCallProcedure($proctype, $tablename) {
        global $polaris;
        global $_sqlTable;

        $clientid = $this->record->CLIENTID;
        $databaseid = $this->record->DATABASEID;

        $_tablename = explode('.', $tablename);
        $_tablename = (isset($_tablename[1]) and $_tablename[1] !== '') ? $_tablename[1] : $tablename;
        $_recTable = $polaris->instance->GetRow($_sqlTable, array($clientid, $databaseid, strtoupper($_tablename)));
        if (isset($_recTable[strtoupper($proctype).'PROCEDURE'])) {
            return $_recTable[strtoupper($proctype).'PROCEDURE'];
        } else {
            return '';
        }
    }

    function IsOracle() {
        if (isset($this->record) and $this->record)
            return (($this->record->DATABASETYPE == 'oci8plr' or $this->record->DATABASETYPE == 'oci8'));
        else
            return false;
    }

    function SetOracleUserID($userid) {
        try {
            if (isset($this->userdb)) {
                $_sp = $this->userdb->PrepareSP("BEGIN NHA.Nhauser.SetUserID(:userid); COMMIT; END;");
                $result = $this->userdb->InParameter($_sp, $userid, 'userid');
                $this->userdb->Execute($_sp);
            }
        } catch(exception $E) {
        }
    }

    function GetCurrentOracleUserID() {
        $_sp = $this->userdb->PrepareSP("DECLARE userid INTEGER; BEGIN :userid := NHA.Nhauser.GetUserID(); END;");
        $result = $this->userdb->OutParameter($_sp,$userid,'userid');
        $this->userdb->Execute($_sp);
        return $userid;
    }

    function SetOracleUserLanguage($language) {
        try {
            $_sp = $this->userdb->PrepareSP("BEGIN NHA.NHAUSER.SetUserLang(:language); END;");
            $result = $this->userdb->InParameter($_sp, strtoupper($language), 'language');
            $this->userdb->Execute($_sp);
        } catch(exception $E) {
        }
    }

    function GetSelectList($currentvalue='') {
        if ($this->ConnectUserDB() ) {
            $tables = $this->userdb->MetaTables();
            if ($tables == false) {
                trigger_error ('cannot_determine_tables', E_USER_WARNING);
            }
            return SetAsSelectList($tables, $itemname='TABLENAME', $currentvalue, $id='idselect', $selectname='TABLENAME');
        } else {
            return false;
        }
    }

    function GetTables() {
        if ($this->ConnectUserDB() ) {
            $tables = $this->userdb->MetaTables();
            if ($tables == false) {
                trigger_error ('cannot_determine_tables', E_USER_WARNING);
            }
            return CheckboxList($tables, $cbname='TABLENAME', $defaultcheckedall=true, $includecheckall=false, $keyisname=true);
        } else {
            return false;
        }
    }

    function GetLength($type, $default=-1) {
        // determine the value between ()
        $number = substr($type, strpos($type, '('), strpos($type, ')')- strpos($type, '(') - 1);
        if (is_numeric($number))
            return $number;
        else
            return $default;
    }

    function GetEnum($type) {
        // determine the value between ()
        if (strpos(strtoupper($type), 'ENUM') === false) return '';
        $values = substr($type, strpos($type, '(') + 1, strpos($type, ')')- strpos($type, '(') - 1);
        $values = str_replace('\'','',$values);
        return str_replace(',',';',$values);
    }

    function ImportTables($tables, $overwritetables=false) {
        $this->ConnectUserDB();

        $tablesstring = '"'.implode("\",\"",$tables).'"';
        $clientid = $this->key['CLIENTID'];
        $databaseid = $this->key['DATABASEID'];

        $_extrawhere =  " and tablename IN ($tablesstring)";

        if ($overwritetables) {
            $deletetables = "DELETE FROM plr_table WHERE clientid = $clientid and databaseid = $databaseid $_extrawhere";
            $this->dbExecute($deletetables) or Die('Query failed: '.$deletetables);
            $deletessi = "DELETE FROM plr_subsetitem WHERE clientid = $clientid and databaseid = $databaseid $_extrawhere";
            $this->dbExecute($deletessi) or Die('Query failed: '.$deletessi);
            $deletess = "DELETE FROM plr_subset WHERE clientid = $clientid and databaseid = $databaseid $_extrawhere";
            $this->dbExecute($deletess) or Die('Query failed: '.$deletess);
            $deletecolumns = "DELETE FROM plr_column WHERE clientid = $clientid and databaseid = $databaseid $_extrawhere";
            $this->dbExecute($deletecolumns) or Die('Query failed: '.$deletecolumns);
        }

        foreach($tables as $table) {
            /**
            * build the sentence pattern
            **/
            $columns = $this->userdb->MetaColumns($table);

            $sentencepattern = '';
            foreach($columns as $column) {
                $sentencepattern .= '&lt;'.$column->name.'&gt; ';
            }

            $inserttable = "INSERT IGNORE INTO plr_table (clientid, databaseid, tablename, sentencepattern) VALUES ($clientid, $databaseid, '$table', '$sentencepattern')";
            $this->dbExecute($inserttable) or Die('Query failed: '.$inserttable);

            /**
            * add the Polaris subsets
            **/
            $subsets = $this->userdb->MetaForeignKeys($table);
            if ($subsets) {
                $tablename = $table;
                foreach($subsets as $subsettable => $subsetcolumns) {
                    $insertsubset = "INSERT IGNORE INTO plr_subset (clientid, databaseid, tablename) VALUES ($clientid, $databaseid, '$tablename')";
                    $this->dbExecute($insertsubset) or Die('Query failed: '.$insertsubset);
                    $plrins = $this->polaris();
                    $subsetid = $plrins->instance->Insert_ID();
                    $supertablename = $subsettable;

                    $orderindex = 1;
                    if (!is_array($subsetcolumns)) $subsetcolumns = array($subsetcolumns);
                    foreach($subsetcolumns as $subsetitem) {
                        $items = explode( '=', $subsetitem );
                        $columnname = $items[0];
                        $supercolumnname = $items[1];

                        $insertsubsetitem = "INSERT IGNORE INTO plr_subsetitem (clientid, databaseid, subsetid, tablename, columnname, supertablename, supercolumnname, orderindex) ";
                        $insertsubsetitem .= "VALUES ($clientid, $databaseid, $subsetid, '$table', '$columnname', '$supertablename', '$supercolumnname', $orderindex)";
                        $this->dbExecute($insertsubsetitem) or Die('Query failed: '.$insertsubsetitem);
                        $orderindex++;
                    }
                }
            }

            /**
            * add the Polaris columns
            **/
            $ps = 1;
            $rs = $this->userdb->SelectLimit("SELECT * FROM $table", 1, 0);
            foreach($columns as $column) {
                $lv = '';
                $ml = $column->max_length;
                $dt = $rs->MetaType($column->type);
                if (($ml == -1 or $ml === NULL)) {
                    switch ($dt) {
                    case 'I': $ml = $this->GetLength($column->type, $default=11);    //int
                    break;
                    case 'X': $ml = 65535; //text
                    break;
                    case 'D': $ml = 10;    //date
                    break;
                    case 'T': $ml = 20;    //timestamp, datetime, time
                    break;
                    case 'C': $lv = $this->GetEnum($column->type);    //int
                    break;
                    }
                }
                if ($column->not_null == 1) $req = 'Y';
                else $req = 'N';
                if ($column->primary_key == 1)  $kc = 'Y';
                else $kc = 'N';
                $fn = ucwords(strtolower($column->name));
                $dc = 0;
                $cn = strtoupper($column->name);
                $insertcolumn = " INSERT IGNORE INTO plr_column (clientid, databaseid, tablename, columnname, keycolumn, datatype, totallength, decimalcount, fullname, required, listvalues, position) ";
                $insertcolumn .= " VALUES ($clientid, $databaseid, '$table', '$cn', '$kc', '$dt', $ml, $dc, '$fn','$req', '$lv', $ps)";
                $this->dbExecute($insertcolumn) or Die('Query failed: '.$insertcolumn);
                $ps++;
            }
            $ps--;
            $result[$table] = $ps;
        }
        return $result;
    }

    function ImportFactTypes($clientid, $domainid, $tables, $overwritetables=false) {
        $this->ConnectUserDB();

//        $tablesstring = '"'.implode("\",\"",$tables).'"';

        $_extrawhere =  " and tablename IN ($tablesstring)";

        if ($overwritetables) {
            $deletefacttypes = "DELETE FROM mod_facttype WHERE clientid = $clientid and domainid = $domainid";
            $this->dbExecute($deletefacttypes) or Die('Query failed: '.$deletefacttypes);
            $deleteplaceholders = "DELETE FROM mod_facttype_placeholder WHERE clientid = $clientid and domainid = $domainid";
            $this->dbExecute($deleteplaceholders) or Die('Query failed: '.$deleteplaceholders);
        }

        $_sqlMaxFacttypeId = "SELECT MAX(facttypeid) FROM mod_facttype WHERE clientid = $clientid and domainid = $domainid";
        $_lastFactTypeId = $this->dbGetOne($_sqlMaxFacttypeId);
        if (!$_lastFactTypeId) {
            $_lastFactTypeId = 1;
        } else {
            $_lastFactTypeId++;
        }

        if (!is_array($tables)) {
            $_tables[] = $tables;
        } else {
            $_tables = $tables;
        }
        //$tables = $this->userdb->MetaTables('TABLES');
        //sort($tables);

        foreach($_tables as $table) {
            /**
            * build the sentence pattern
            **/
            $columns = $this->userdb->MetaColumns($table);
            $sentencepattern = '';
            foreach($columns as $column) {
                $sentencepattern .= '<'.$column->name.'> ';
            }

            $inserttable = "INSERT IGNORE INTO mod_facttype (clientid, domainid, facttypeid, facttypename, sentencepattern) VALUES ($clientid, $domainid, $_lastFactTypeId, '$table', '$sentencepattern')";
            $this->dbExecute($inserttable) or Die('Query failed: '.$inserttable);

            /**
            * add the Polaris columns
            **/
            $ps = 1;

            $rs = $this->userdb->SelectLimit("SELECT * FROM $table", 1, 0);
            foreach($columns as $column) {
                $lv = '';
                $ml = $column->max_length;
                $dt = $rs->MetaType($column->type);
                if (($ml == -1 or $ml === NULL)) {
                    switch ($dt) {
                    case 'I': $ml = $this->GetLength($column->type, $default=11);    //int
                    break;
                    case 'X': $ml = 65535; //text
                    break;
                    case 'D': $ml = 10;    //date
                    break;
                    case 'T': $ml = 20;    //timestamp, datetime, time
                    break;
                    case 'C': $lv = $this->GetEnum($column->type);    //int
                    break;
                    }
                }
                if ($column->not_null == 1) $req = 'Y';
                else $req = 'N';
                if ($column->primary_key == 1)  $kc = 'Y';
                else $kc = 'N';
                $fn = ucwords(strtolower($column->name));
                $dc = 0;
                $cn = strtoupper($column->name);
                $insertcolumn = " INSERT IGNORE INTO mod_facttype_placeholder (clientid, domainid, facttypeid, placeholderid, facttypename, placeholdername, keycolumn
                , datatype, totallength, decimalcount, fullname, required, listvalues, position) ";
                $insertcolumn .= " VALUES ($clientid, $domainid, $_lastFactTypeId, $ps, '$table', '$cn', '$kc', '$dt', $ml, $dc, '$fn','$req', '$lv', $ps)";
                $this->dbExecute($insertcolumn) or Die('Query failed: '.$insertcolumn);
                $ps++;
            }
            $ps--;
            $result[$table] = $ps;
            $_lastFactTypeId++;
        }
        return $result;
    }
}
