<?php

class base_plrApplication extends base_plrRecord {
    var $constructs;
    var $_constructs;
    var $usersettings;
    var $groupvalues;
    var $applink;

    function __construct($aowner, $key = false) {
        parent::__construct($aowner, $key);
    }

    function &LoadRecord($key = false, $encoded = true, $metaname = false) {
        global $_sqlApplication;
        global $_sqlApplication_two;
        global $_sqlApplicationMeta;

        if ($key) $this->key = $key;

        if ($encoded)
            $_thesql = $_sqlApplication;
        elseif ($metaname)
            $_thesql = $_sqlApplicationMeta;
        else
            $_thesql = $_sqlApplication_two;

        $this->record = GetQueryRecordAsObject($this->plrInstance(), $_thesql, $this->key, true);
        $this->LoadCurrentUserSettings();
        $this->ApplyUserSettings();

        return $this;
    }

    function EncodedID() {
        return encodeString($this->record->APPLICATIONNAME);
    }

    function _makeApplicationLink($servicetype='normal') {
        global $_GVARS;

        if (isset($this->record->EXTERNALURL) and $this->record->EXTERNALURL != '') {
            $_link = $this->record->EXTERNALURL;
        } else {
            $_output = isset($_GET['output'])?$_GET['output']:'json';

            switch ($servicetype) {
            case 'service':
                $_link = '/services/'.$_output.'/';
                break;
            case 'ajax':
                $_link = '/ajax/json/';
                break;
            case 'caller':
                $_link = '/';
                break;
            case 'application':
                $_link = '/';
                break;
            default:
                $_link = $_GVARS['serverpath'].'/'; // $_GVARS['serverpath'].'/';
                break;
            }

            if ($servicetype === TRUE) {
                $_link = $_GVARS['serverpath'].$_link;
            }

            if (($_SESSION['usertype'] ?? null) == 'root' ) {
                $_link = '/cln/'.$this->record->CLIENTHASHID.$_link.'/app/'.$this->record->METANAME.'/';
            } else {
                $_link .= 'app/'.$this->record->METANAME.'/';
            }
        }

        return $_link;
    }

    function createApplicationLink($servicetype='normal') {
        $this->applink = $this->_makeApplicationLink($servicetype);
        return $this->applink;
    }

    function applicationLink($servicecall=false) {
        return $this->_makeApplicationLink($servicecall);
    }

    function HasSearchOption($option) {
        return (strpos($this->record->SEARCHOPTIONS, $option) !== false );
    }

    function LoadCurrentUserSettings() {
        if ($_SESSION['userid'] ?? null)
            $this->LoadUserSettings($_SESSION['userid']);
    }

    function LoadUserSettings($usergroupid) {
        global $_sqlUserSettings;

        $this->usersettings = $this->dbGetAll($_sqlUserSettings, array($this->record->CLIENTID, $this->record->APPLICATIONID, $usergroupid));
    }

    function ApplyUserSettings() {
        if (isset($this->usersettings) and count($this->usersettings) > 0) {
            foreach($this->usersettings as $setting) {
                $this->record->{$setting['SETTING']} = $setting['VALUE'];
            }
        }
    }

    function Show($currentconstruct = 0) {
        if ($currentconstruct) {
            $this->ShowConstruct($currentconstruct);
        }
    }

    function ShowShortcuts() {
        global $BASEURL;

        echo "<div id='navbox'>";
        echo "<div id='menubox'>";

        $keyfields = array('CLIENTID' => $this->record->CLIENTID,
            'APPLICATIONID' => $this->record->APPLICATIONID);
        $sheets = GetRecordSet($this->plrInstance(), $tablename = 'plr_shortcutsheet', $keyfields);

        $sheetrecord = $sheets->FetchNextObject(true);
        if ($sheetrecord) {
            $i = 1;
            echo "\r\n<SCRIPT LANGUAGE='JAVASCRIPT'>\r\n";
            while ($sheetrecord) {
                echo 'OutBarFolder'.$i.'=new Array("'.$sheetrecord->SHEETNAME.'"'."\r\n";
                $shortcutkeyfields = $keyfields;
                $shortcutkeyfields['SHORTCUTSHEETID'] = $sheetrecord->SHORTCUTSHEETID;
                $shortcuts = GetRecordSet($this->plrInstance(), $tablename = 'plr_shortcut', $shortcutkeyfields);
                while ($shortcut = $shortcuts->FetchNextObject(true)) {
                    $url = $BASEURL.'&amp;const='.$shortcut->CONSTRUCTID;
                    echo ',"images/globe.gif", "'.$shortcut->SHORTCUTNAME.'", "'.$url.'", "window"'."\r\n";
                }
                echo ");\r\n";
                $i++;
                $sheetrecord = $sheets->FetchNextObject(true);
            }
            echo "</SCRIPT>\r\n";
        }
        echo "</div>";
        echo "<a href='$BASEURL&amp;action=chooseapp'>Kies andere applicatie...</a>";
        echo "</div>";
    }

    function LoadConstructs() {
        $consarray = $this->GetConstructs();
        foreach($consarray as $cons) {
            $this->constructs[] = $this->GetConstruct($cons['CONSTRUCTID']);
        }
    }

    function &GetConstruct($constructid, $metaname = false) {
        if (isset($constructid) and $constructid != '') {
            $construct = new base_plrConstruct($this);
            $construct->LoadRecord(array($this->record->CLIENTID, $this->record->APPLICATIONID, $constructid), $metaname);
            /* Remove accesskey specifier from normal use */
            if (isset($construct->record) and $construct->record) {
                $construct->record->CONSTRUCTNAME = str_replace('^','',$construct->record->CONSTRUCTNAME);
                $po = $this->polaris();
                $construct->record->CONSTRUCTNAME = $po->ReplaceDynamicVariables($construct->record->CONSTRUCTNAME, '');
            }

            return $construct;
        } else {
            return false;
        }
    }

    function ShowConstruct($constructid) {
        $construct = GetConstruct($constructid);
        $construct->Show();
    }

    function GetCurrentConstructMetaName() {
        $_const = false;
        if (!empty($_GET['const']))
            $_const = $_GET['const'];

        if ($_const == false) {
            $_const = $this->GetDefaultConstruct()->record->METANAME;
        }

        return $_const;
    }

    function &GetDefaultConstruct($metaname=false) {
        $defaultconstructid = false;
        $result = false;
        if ($this->_constructs) {
            foreach($this->_constructs as $cons) {
                if ($cons['DEFAULTFORM'] == 'Y' ) {
                    if ($metaname) {
                        $defaultconstructid = $cons['METANAME'];
                    } else {
                        $defaultconstructid = $cons['CONSTRUCTID'];
                    }
                }
            }
            if ($defaultconstructid) {
                $result = $this->GetConstruct($defaultconstructid, $metaname);
            }
        } else {
            $result = false;
        }
        return $result;
    }

    function &GetConstructs($superconstruct = 0, $extrawhere = false) {
        global $_sqlConstructsADMIN;
        global $_sqlConstructs;

        if ($_SESSION['usertype'] == 'root') {
            $thesql = $_sqlConstructsADMIN;
            $params = array(
                $superconstruct,
                $_GET['cln'],
                $this->record->APPLICATIONID);
        } else {
            $thesql = $_sqlConstructs;
            $co = $this->ClientObject();
            if (!isset($this->groupvalues))
                $this->groupvalues = $co->owner->GetGroupValues($_SESSION['userid']);
            $params = array(
                $superconstruct,
                $this->record->CLIENTID,
                $this->record->APPLICATIONID);
            $thesql = str_replace('#subselect#', $this->groupvalues, $thesql);
        }
        if ($extrawhere) $thesql .=' AND '.$extrawhere;
        $thesql .= ' ORDER BY ORDERINDEX ';
        $_constructs = $this->dbGetAll($thesql, $params);// or Die('query failed constructs: '.$this->dbErrorMsg());

        if ($_constructs) {
            //$_constructs = $rs->GetAll();
            $po = $this->polaris();

            if ($this->AllSeparators($_constructs)) {
                // Get rid of single separators
                unset($_constructs);
            } else {
                foreach($_constructs as $key => $c) {
                    $_constructs[$key]['CONSTRUCTNAME'] = $po->ReplaceDynamicVariables($c['CONSTRUCTNAME'], '');
                    if ($_constructs[$key]['SHOWCOUNTBADGE'] == 'Y'
                    and isset($_constructs[$key]['RECORDCOUNTER'])
                    and $_constructs[$key]['RECORDCOUNTER'] != ''
//                    and round((time() - strtotime($_constructs[$key]['LASTRECORDCOUNT'])) / 60,2) < 60
                    ) {
                        $_constructs[$key]['RECORDCOUNTER'] = $_constructs[$key]['RECORDCOUNTER'];
                    } else {
                        $_constructs[$key]['RECORDCOUNTER'] = false;
                    }
                    if (strpos($c['CONSTRUCTNAME'], '^') !== false) {
                        $pos = strpos($c['CONSTRUCTNAME'], '^');
                        $accesskey = $c['CONSTRUCTNAME'][$pos + 1];
                        $_constructs[$key]['ACCESSKEY'] = $accesskey;
                        $c['CONSTRUCTNAME'] = substr($c['CONSTRUCTNAME'],0,$pos+2) ."</span>". substr($c['CONSTRUCTNAME'],$pos+2);
                        $c['CONSTRUCTNAME'] = str_replace('^','<span>',$c['CONSTRUCTNAME']);
                        $_constructs[$key]['CONSTRUCTNAME'] = $c['CONSTRUCTNAME'];
                    }
                    if ($c['CONSTRUCTTYPE'] == 'GROUP') {
                        $_constructs[$key]['groupitems'] = $this->GetConstructs($c['CONSTRUCTID']);
                        if ($_constructs[$key]['groupitems'] and count($_constructs[$key]['groupitems']) > 0) {
                            // take all the CONSTRUCTID from the array $_constructs[$key]['groupitems'] and put them in an array
                            $constructids = array();
                            foreach($_constructs[$key]['groupitems'] as $gi) {
                                $constructids[] = $gi['CONSTRUCTID'];
                            }
                            $_constructs[$key]['groupitems_id_array'] = $constructids;

                            foreach($_constructs[$key]['groupitems'] as $gi) {
                                if (isset($_GET['const']) and isset($gi['METANAME']) and ($gi['METANAME'] == $_GET['const'])) {
                                    $_constructs[$key]['ACTIVE'] = true;
                                }
                            }
                        } else {
                            // if there are no subconstructs then don't show the group
                            unset($_constructs[$key]);
                        }
                    }
                }
            }
            if ($_constructs) {
                $_constructs = array_merge($_constructs); //re-index the array
                if ($superconstruct == 0)
                    $this->_constructs = $_constructs;
            }
        } else
            $_constructs = FALSE;
        return $_constructs;
    }

    function AllSeparators($constructs) {
        $result = true;
        foreach($constructs as $c) {
            if ($c['CONSTRUCTTYPE'] != 'SEPARATOR') {
                $result = false;
                break;
            }
        }
        return $result;
    }

    function &ClientObject() {
        $parent = $this->owner;
        while (isset($parent)) {
            if (strcasecmp(get_class($parent), 'base_plrClient') == 0) {
                return $parent;
                break;
            }
            $parent = $parent->owner;
        }
    }

    function &GetForm($formid, $metaname = false) {
        if (isset($formid) and $formid != '') {
            $form = new base_plrForm($this);
            $form->LoadRecord(array($this->record->CLIENTID, $formid), $metaname);

            return $form;
        } else {
            return false;
        }
    }

    function GetQuickNavConstructs() {
        global $_sqlQuickNavConstructs;

        $rs = $this->dbExecute($_sqlQuickNavConstructs, array($this->record->CLIENTID, $this->record->APPLICATIONID, $_SESSION['userid'])) or Die('query failed quicknav: '.$this->dbErrorMsg());
        if ($rs)
            return $rs->GetAll();
        else
            return FALSE;
    }

    function GetAdvertorials() {
        global $_sqlApplicationAdvertorials;

        $rs = $this->dbExecute($_sqlApplicationAdvertorials, array($this->record->CLIENTID, $this->record->APPLICATIONID)) or Die('query failed ads: '.$this->dbErrorMsg());
        if ($rs)
            return $rs->GetAll();
        else
            return FALSE;
    }

    function GenerateForm($masterformid, $databaseid, $tablename) {
        global $polaris;

        $_clientid = $this->record->CLIENTID;
        $_applicationid = $this->record->APPLICATIONID;

        $_dbObj = new base_plrDatabase($polaris, array('CLIENTID'=>$_clientid, 'DATABASEID'=>$databaseid));
        $_dbObj->LoadRecord();

        $tables[] = $tablename;
        $tableresults = $_dbObj->ImportTables($tables, false);
        $formname = ucwords(strtolower($tablename));
        $metaname = str_replace(' ', '', strtolower($formname));
        $insert = ' INSERT INTO plr_form (clientid, formid, formname, metaname, databaseid, tablename, options)';
        $insert .= " VALUES ($_clientid, $masterformid, '$formname', '$metaname', $databaseid, '$tablename', 'no-wordwrap;group-table')";
        try {
            $this->dbExecute($insert) or Die('Failed: '.$insert);
        } catch(Exception $E) {
            // indien de metaname al bestaat, dan voeg een record toe met een andere metaname (metaname + masterformid)
            if ($E->getCode() == 1062 ) {
                $insert = ' INSERT INTO plr_form (clientid, formid, formname, metaname, databaseid, tablename, options)';
                $insert .= " VALUES ($_clientid, $masterformid, '$formname', '{$metaname}_{$masterformid}', $databaseid, '$tablename', 'no-wordwrap;group-table')";
                $this->dbExecute($insert) or Die('Failed: '.$insert);
            }
        }

        $pageid = 1;
        $pagename = 'Pagina 1';
        $insert = ' INSERT INTO plr_page (clientid, formid, pageid, pagename)';
        $insert .= " VALUES ($_clientid, $masterformid, $pageid, '$pagename')";
        $this->dbExecute($insert) or Die('Failed: '.$insert);

        $key = array($_clientid, $databaseid, $tablename);
        $recset = $this->dbGetAll("SELECT * FROM plr_column WHERE CLIENTID=? AND DATABASEID=? AND TABLENAME=? ORDER BY POSITION", $key, false);
        $lineid = 1;
        foreach($recset as $rec) {
            $cn = strtoupper($rec['COLUMNNAME']);
            $ld = ucwords(strtolower($cn));
            $positionindex = $lineid * 10;
            $insert = ' INSERT INTO plr_line (clientid, formid, pageid, lineid, linedescription, linedescription_short, linetype, databaseid, tablename, columnname, positionindex)';
            $insert .= " VALUES ($_clientid, $masterformid, $pageid, $lineid, '$ld', '$ld', 'COLUMN', $databaseid, '$tablename', '$cn', $positionindex)";
            $this->dbExecute($insert) or Die('Failed: '.$insert);
            $lineid++;
        }
        return $formname.' ('.$masterformid.')';
    }

    function getNewConstructId() {
        $clientid = $this->record->CLIENTID;
        $applicationid = $this->record->APPLICATIONID;
        $_constructid = $this->dbGetOne("SELECT MAX(CONSTRUCTID) FROM plr_construct WHERE clientid = $clientid AND applicationid = $applicationid");

        if (!isset($_constructid))
            $_constructid = 1;
        else
            $_constructid++;
        return $_constructid;
    }

    function GenerateBaseConstruct($databaseid, $tablename, $usergroupid) {
        $_clientid = $this->record->CLIENTID;
        $_applicationid = $this->record->APPLICATIONID;

        $_constructid = $this->getNewConstructId();
        $_itemname = $tablename;
        $_metaname = $_itemname;
        $_masterformid = $this->getNewFormId();

        return $this->GenerateConstruct($_clientid, $_applicationid, $_constructid, $_itemname, $_metaname, $_masterformid, $databaseid, $tablename, $usergroupid);
    }

    function GenerateConstruct($clientid, $applicationid, $constructid, $itemname, $metaname, $masterformid, $databaseid, $tablename, $usergroupid) {

        $imageurl = 'fa-face-smile-beam';
        $insert = ' INSERT INTO plr_construct (clientid, applicationid, constructid, constructname, metaname, constructtype, masterformid, imageurl, orderindex)';
        $insert .= " VALUES ($clientid, $applicationid, $constructid, '$itemname', '$metaname', 'CONTENT', $masterformid, '$imageurl', $constructid)";
        $this->dbExecute($insert) or Die('Failed: '.$insert);
        $insert = ' INSERT INTO plr_constructtree (clientid, applicationid, constructid, superconstructid)';
        $insert .= " VALUES ($clientid, $applicationid, $constructid, 0)";
        $this->dbExecute($insert) or Die('Failed: '.$insert);

        $formnames = $this->GenerateForm($masterformid, $databaseid, $tablename);

        if ($usergroupid) {
            $permissiontype = 286; //SELECT + INSERT + UPDATE + DELETE;
            $insert = ' INSERT INTO plr_formpermission (clientid, formid, usergroupid, permissiontype)';
            $insert .= " VALUES ($clientid, $masterformid, $usergroupid, $permissiontype)";
            $this->dbExecute($insert) or Die('Failed: '.$insert);
        }

        return $formnames;
    }

    function getNewFormId() {
        $_clientid = $this->record->CLIENTID;
        $masterformid = $this->dbGetOne('SELECT MAX(FORMID) FROM plr_form WHERE clientid = ?', array($_clientid));
        $masterformid++;
        return $masterformid;
    }

    function GenerateDefaultApplication($database, $tables, $usergroupid = FALSE, $overwriteapp = FALSE) {
        global $g_lang_strings;

        $clientid = $this->key['CLIENTID'];
        $databaseid = $database->record->DATABASEID;
        $recset = $this->dbExecute("SELECT * FROM plr_table WHERE clientid = ? AND databaseid = ?", array($clientid,$databaseid));
        $applicationid = $this->key['APPLICATIONID'];
        $deleteapprecord = false;
        $this->DeleteAppRecords($database, $deleteapprecord, true);
//        $insert = ' INSERT INTO plr_shortcutsheet (clientid, applicationid, shortcutsheetid, sheetname, orderindex)';
//        $insert .= " VALUES ($clientid, $applicationid, 1, '".$g_lang_strings['functions']."', 1)";
//        $this->dbExecute($insert) or Die('Failed: '.$insert);

        $constructid = 1;
        $masterformid = $this->getNewFormId();

        $formnames = array();
        foreach($tables as $tablename) {
            $shortcutid = $constructid;
            $itemname = ucwords(strtolower($tablename));
            $metaname = str_replace(' ','',strtolower($itemname));
            $imageurl = '/i/aesthetica/computer.png';
            if ($tablename) {
                $formnames[] = $this->GenerateConstruct($clientid, $applicationid, $constructid, $itemname, $metaname, $masterformid, $database, $tablename, $usergroupid);
            }
            $masterformid++;
            $constructid++;
        }
        return $formnames;
    }

    function OutputSQLFragment($fragment, $filename, $first=false) {
        if (isset($filename)) {
            file_put_contents($filename, "\n\n".$fragment."\n\n", $first ? FILE_APPEND : null);
        } else {
            echo "\n\n".$fragment."\n\n";
        }
    }

    function ExportAppRecords($filename=false, $includedatabaserecords=true, $astemplate=false) {
        global $polaris;
        global $_PLRINSTANCE;
        global $_CONFIG;
        global $ClientHash;

        $clientid = $this->key[0];
        $applicationid = $this->key[1];

        $clienttables = array('plr_datascope');
        $clientwhereclause = " $ClientHash = \"{$clientid}\"";

        $appformtables = array('plr_form','plr_formpermission','plr_formtag','plr_page','plr_line','plr_alert','plr_action','plr_replyrule','plr_search');
        $appdbtables = array('plr_table','plr_column','plr_subset','plr_subsetitem','plr_errormessages');

        $apptables = array('plr_application','plr_applicationpermission','plr_construct','plr_constructtree','plr_menuitem','plr_parameters');
        $appwhereclause = " $ClientHash = \"{$clientid}\" AND applicationid = {$applicationid}";
        /* Get all formid's */
        $_getformids = "
            SELECT DISTINCT FORMID
            FROM (
                SELECT MASTERFORMID as `FORMID` FROM plr_construct WHERE $appwhereclause
                UNION
                SELECT DETAILFORMID FROM plr_construct WHERE $appwhereclause
                UNION
                SELECT NEWMENUITEMNAME FROM plr_construct WHERE $appwhereclause
            ) AS T2
            WHERE FORMID IS NOT NULL AND FORMID <> 0
        ";
        $_formidsRS = $this->dbGetAll($_getformids);
        $_formids = array();
        foreach($_formidsRS as $_formid) {
            if ($_formid['FORMID'] != 0)
                $_formids[] = $_formid['FORMID'];
        }
        $formwhereclause = '';
        if (!empty($_formids)) {
            $_formids = implode(',', $_formids);
            $formwhereclause = " $ClientHash = \"$clientid\" AND formid IN ($_formids)";
            if ($includedatabaserecords) {
                if ($_formids != '') {
                    $_dbidsRS = $this->dbGetAll("SELECT DISTINCT DATABASEID FROM plr_form WHERE $formwhereclause");
                    $_dbids = array();
                    foreach($_dbidsRS as $_dbid) {
                        if ($_dbid['DATABASEID'] != 0)
                            $_dbids[] = $_dbid['DATABASEID'];
                    }
                    $_dbids = implode(',', $_dbids);
                    $dbwhereclause = " $ClientHash = \"$clientid\" AND databaseid IN ($_dbids)";
               }
            }
        }

        if (isset($filename)) {
            $firstfile = " > $filename";
            $addtofile = " >> $filename";
        } else {
            $firstfile = '';
            $addtofile = '';
        }

        $_settings = $_PLRINSTANCE[$_CONFIG['currentinstance']];
        foreach($clienttables as $clienttable) {
            $this->OutputSQLFragment("DELETE FROM {$clienttable} WHERE {$clientwhereclause};", $filename, true);
            system("mysqldump {$_settings['database']} $clienttable --where='$clientwhereclause' --no-tablespaces --no-create-info --complete-insert --host={$_settings['host']} --user={$_settings['username']} --password={$_settings['password']} {$addtofile}");
        }
        foreach($apptables as $apptable) {
            $this->OutputSQLFragment("DELETE FROM {$apptable} WHERE {$appwhereclause};", $filename);
            system("mysqldump {$_settings['database']} $apptable --where='$appwhereclause' --no-tablespaces --no-create-info --complete-insert --host={$_settings['host']} --user={$_settings['username']} --password={$_settings['password']} {$addtofile}");
        }
        if (!empty($formwhereclause)) {
            foreach($appformtables as $formtable) {
                $this->OutputSQLFragment("DELETE FROM {$formtable} WHERE {$formwhereclause};", $filename);
                system("mysqldump {$_settings['database']} $formtable --where='$formwhereclause' --no-tablespaces --no-create-info --complete-insert --host={$_settings['host']} --user={$_settings['username']} --password={$_settings['password']} {$addtofile}");
            }
        }
        if (!empty($dbwhereclause)) {
            foreach($appdbtables as $dbtable) {
                $this->OutputSQLFragment("DELETE FROM {$dbtable} WHERE {$dbwhereclause};", $filename);
                system("mysqldump {$_settings['database']} $dbtable --where='$dbwhereclause' --no-tablespaces --no-create-info --complete-insert --host={$_settings['host']} --user={$_settings['username']} --password={$_settings['password']} {$addtofile}");
            }
        }
    }

    function DeleteAppRecords($database, $deleteapprecord=false, $includeformrecords=false) {
        global $polaris;

        $clientid = $this->key[0];
        $databaseid = $database->record->DATABASEID;
        $applicationid = $this->key[1];

        $appformtables = array('plr_form','plr_formpermission','plr_page','plr_line');

        $apptables = array('plr_construct','plr_constructtree');
        $appwhereclause = " WHERE clientid = ".$clientid." AND applicationid = ".$applicationid;
        if ($includeformrecords) {
            /* Get all formid's */
            $_getformids = "SELECT masterformid, detailformid FROM plr_construct ";
            $_getformids .= $appwhereclause;
            $_formidsRS = $this->dbGetAll($_getformids);
            $_formids = array();
            foreach($_formidsRS as $_formid) {
                if ($_formid['masterformid'] != 0)
                    $_formids[] = $_formid['masterformid'];
                if ($_formid['detailformid'] != 0)
                    $_formids[] = $_formid['detailformid'];
            }
            $_formids = implode(',', $_formids);

            if ($_formids != '') {
                $formwhereclause = " WHERE clientid = ".$clientid." AND formid IN (".$_formids.")";
                foreach($appformtables as $table) {
                    $delete  = " DELETE FROM $table ";
                    $delete .= $formwhereclause;
                    $this->dbExecute($delete) or Die('Failed: '.$delete);
                }
            }
        }

        foreach($apptables as $table) {
            $delete  = " DELETE FROM $table ";
            $delete .= $appwhereclause;
            $this->dbExecute($delete) or Die('Failed: '.$delete);
        }

        if ($deleteapprecord) {
            $delete  = ' DELETE FROM plr_application ';
            $delete .= $appwhereclause;
            $this->dbExecute($delete) or Die('Failed: '.$delete);
        }
    }
}
