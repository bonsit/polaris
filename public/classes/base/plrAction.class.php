<?php

class base_plrAction extends base_plrRecord {
    var $database;

    function __construct(&$aowner)
    {
        parent::__construct($aowner);
    }

    function &LoadRecord($key = false /* HASHED */)
    {
        global $_sqlAction;

        if ($key) $this->key = $key;
        $this->record = GetQueryRecordAsObject($this->plrInstance(), $_sqlAction, $this->key, true);

        return $this;
    }

    function CheckRowsNeeded() {
        if ($this->record->SELECTEDROWS != '') {
            if ($this->record->SELECTEDROWS == 'ONE')
                return 'needsonerow';
            if ($this->record->SELECTEDROWS == 'ONEORMORE')
                return 'needsonerowormore';
            if ($this->record->SELECTEDROWS == 'ONECHECKED')
                return 'needsonerowchecked';
            if ($this->record->SELECTEDROWS == 'ONEORMORECHECKED')
                return 'needsonerowormorechecked';
        } else {
            for ($i = 1; $i <= 5; $i++) {
                $pname = 'PARAM' . $i;
                if (stripos($this->record->$pname, ':ROWID') !== false) {
                    return 'needsonerow';
                }
            }

            if (stripos($this->record->TEXT, ':ROWID') !== false) {
                return 'needsonerow';
            } else {
                return '';
            }
        }
    }

    function GetDynamicFieldName($string) {
        $pos = strpos($string, '%field[');
        $result = false;
        if ($pos !== false) {
            $result = substr($string, $pos + 7, strpos($string, ']', $pos + 7) - ($pos + 7));
        }
        return $result;
    }

    function GetDynamicFieldNames() {
        $result = array();
        for ($i = 1; $i <= 5; $i++) {
            $pname = 'PARAM' . $i;
            $fieldname = $this->GetDynamicFieldName($this->record->$pname);
            if ($fieldname) $result[] = $fieldname;
        }

        $fieldname = $this->GetDynamicFieldName($this->record->TEXT);
        if ($fieldname) $result[] = $fieldname;
        return $result;
    }

    function PartOfGroup($group) {
        return strpos($this->record->GROUP, $group) !== false;
    }

    function NotConstrainedByConstruct() {
        $_result = false;
        $_constructs = $this->record->SHOWINCONSTRUCTS;
        try {
            if (!empty($_constructs)) {
                $_constructs = explode(',', $_constructs);
                if (in_array((string)$this->owner->owner->owner->record->CONSTRUCTID, $_constructs)) {
                    return true;
                }
            } else {
                $_result = true;
            }
        } catch (exception $E) {
            return $_result;
        }
        return $_result;
    }

    function Show($index, $state, $detailstate, $size = 'medium', $asdropdownitem = false) {
        echo $this->GetAction($index, $state, $detailstate, $size, $asdropdownitem);
    }

    function GetAction($index, $state, $detailstate, $size = 'medium', $asdropdownitem = false) {
        global $_GVARS;
        global $_CONFIG;
        global $scramble;
        global $polaris;

        $_po = $this->polaris();
        if (($state == 'allitems' and $this->record->SHOWINFORMVIEW == 'N')
        or ($state == 'search' and $this->record->SHOWINFORMVIEW == 'N')
        or ($state == 'edit' or $state == 'view' and $this->record->SHOWINFORMVIEW == 'Y')
        or ($detailstate == 'detail_allitems' and $this->record->SHOWINFORMVIEW == 'N')
        or $state == 'MODULE'
        ) {
            $actionname = str_replace(' ', '&nbsp;', htmlentities($this->i18n($this->record->ACTIONNAME), ENT_QUOTES, "UTF-8"));
            if (get_class($this->owner->owner) == 'plrForm')
                $basehref = $this->owner->owner->MakeBaseURL();
            else
                $basehref = '/app/facility/';

            if ($this->record->JASPERREPORTNAMExxx != '') {
                $actiontype = 'jasper';
                $target = "target=\"_blank\"";
                $basehref .= 'module/plr_execjasperreport/';
                $basehref .= '?action=' . $this->record->RECORDID;
                if ($this->record->BEFOREPRINT)
                    $basehref .= '&moduleid=' . $this->record->MODULEPARAM;
                $basehref .= '&' . $this->record->PARAM1;
                $action = $basehref . '&jr=' . $this->record->JASPERREPORTNAME;
            } elseif ($this->record->JASPERREPORTNAME != '') {
                if ($this->record->JASPERREPORTPARAMS == 'original') {
                    $actiontype = 'jasper';
                    $target = "target=\"_blank\"";
                } else {
                    $actiontype = 'jasperside';
                    $target = "target=\"\"";
                }
                // $target = "target=\"_blank\"";
                $basehref .= 'module/plr_execjasperreport/';
                $basehref .= '?action=' . $this->record->RECORDID;
                if ($this->record->BEFOREPRINT)
                    $basehref .= '&moduleid=' . $this->record->MODULEPARAM;
                $basehref .= '&' . $this->record->PARAM1;
                $action = $basehref . '&jr=' . $this->record->JASPERREPORTNAME;
            } elseif ($this->record->ACTIONURL != '') {
                $actiontype = 'url';
                $action = $_po->ReplaceDynamicVariables($this->record->ACTIONURL) . '?a=1';
                $target = "target=\"_blank\"";
            } elseif ($this->record->MODULE != '') {
                if ($this->record->CUSTOMPARAMFORM == 'N') {
                    $actiontype = 'module standardparamform';
                    $basehref .= 'module/' . strtolower($this->record->MODULE) . '/';
                    $action = $basehref . '?action=' . $this->record->RECORDID . '&moduleid=' . $this->record->MODULEPARAM;
                } else {
                    $actiontype = 'module customparamform';
                    $basehref .= 'module/' . strtolower($this->record->MODULE) . '/';
                    $action = $basehref . '?action=' . $this->record->RECORDID . '&moduleid=' . $this->record->MODULEPARAM . '&customparamform=true';
                }
                if ($this->record->TITLE == '') {
                    $target = "target=\"_blank\"";
                }
            } elseif ($this->record->STOREDPROCEDURE != '') {
                $actiontype = 'storedprocedure';
                $basehref .= 'module/plr_execstoredproc/';
                $action = $basehref . '?action=' . $this->record->RECORDID;
            }

            if ($this->record->EVENTS != '') {
                $actiontype = 'event';
                $action = 'javascript:void(0)';
                $jscript = createEvents($_po->ReplaceDynamicVariables($this->record->EVENTS));
            }

            $action .= '&firsttime=true';

            $dynamicfieldnames = $this->GetDynamicFieldNames();
            $dynamicfieldnames = implode(' ', $dynamicfieldnames);

            $rowselect = $this->CheckRowsNeeded();
            if ($this->record->REFRESH == 'D')
                $refreshdata = 'refresh';
            elseif ($this->record->REFRESH == 'R')
                $refreshdata = 'ajaxrefresh';

            if ($this->record->BUTTONICON != '') {
                $icon = displayFontIcon($this->record->BUTTONICON) . '&nbsp;&nbsp;';
                // $icon = '<i class="md '.$this->record->BUTTONICON.'"></i>&nbsp;';
            }

            if ($size == 'small') {
                $_btnsize = 'btn-sm';
            } elseif ($size == 'xsmall') {
                $_btnsize = 'btn-xs';
            } elseif ($size == 'large') {
                $_btnsize = 'btn-lg';
            }

            $_btnStyle = 'btn-' . $this->record->BUTTONSTYLE ?: 'btn-white';
            if ($asdropdownitem) {
                return "<li><a rel=\"{$dynamicfieldnames}\" class=\"{$actiontype} {$rowselect} {$refreshdata}\" $jscript href=\"$action\" $target>" . $icon . $actionname . "</a></li>";
            } else {
                return " <button rel=\"{$dynamicfieldnames}\" class=\"btn {$_btnsize} {$_btnStyle} {$actiontype} {$rowselect} {$refreshdata}\" $jscript href=\"$action\" $target>" . $icon . $actionname . "</button>";
            }
        }
    }
}
