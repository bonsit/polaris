<?php
global $_GVARS, $_CONFIG;
// Define our template directories for Polaris website
define('MAIL_TEMPLATE_DIR', $_GVARS['docroot'].'/templates/app_templates');

class Smarty_Resource_Mail extends Smarty_Resource_Custom {
    // PDO instance
    protected $db;
    // prepared fetch() statement
    protected $fetch;
    // prepared fetchTimestamp() statement
    protected $mtime;

    public function __construct() {
    }

    function _mail_get_template($fieldname, $tpl_name, &$tpl_source) {
        global $g_lang_strings;
        global $_sqlReplyRule;
        global $polaris;

        // populating $tpl_source
        if ($tpl_name != '') {
            $rs = $polaris->instance->Execute($_sqlReplyRule, array($tpl_name));
            $rec = $rs->FetchNextObject(true);
            if ($rec) {
                $tpl_source = $rec->$fieldname;
                return true;
            }
        } else {
            $tpl_source = $g_lang_strings['page_not_found'];
            return true;
        }
    }

    /**
     * Fetch a template and its modification time from database
     *
     * @param string $name template name
     * @param string $source template source
     * @param integer $mtime template modification timestamp (epoch)
     * @return void
     */
    protected function fetch($name, &$source, &$mtime) {
        $this->_mail_get_template('BODYTEXT', $name, $source);
    }

    /**
     * Fetch a template's modification time
     *
     * @note implementing this method is optional. Only implement it if modification times can be accessed faster than loading the comple template source.
     * @param string $name template name
     * @return integer timestamp (epoch) the template was modified
     */
    protected function fetchTimestamp($name) {
        return strtotime("now");
    }
}

class Smarty_Resource_MailSubject extends Smarty_Resource_Mail {
    function fetch($name, &$source, &$mtime) {
        $this->_mail_get_template('SUBJECT', $name, $source);
    }
}


// Create a wrapper class extended from Smarty
class tempengine_mailSmarty extends tempengine_extSmarty {

    var $db;

    function __construct($_language=false) {
        // Run Smarty's constructor
        parent::__construct($_language);

        $this->template_dir = MAIL_TEMPLATE_DIR;
        $this->registerResource("mail", new Smarty_Resource_Mail());
        $this->registerResource("mailsubject", new Smarty_Resource_MailSubject());
    }
}
