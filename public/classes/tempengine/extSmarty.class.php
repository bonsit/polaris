<?php
global $_GVARS, $_CONFIG;

require_once PLR_DIR . '/includes/param.inc.php';

// Define our template directories
define('TEMPLATE_DIR', $_GVARS['docroot'] . '/templates');
define('COMPILE_DIR', ($_CONFIG['smarty_template_folder'] != '') ? $_CONFIG['smarty_template_folder'] : $_GVARS['docroot'] . '/templates_c');
define('CONFIG_DIR', $_GVARS['docroot'] . '/configs');
define('CACHE_DIR', $_GVARS['docroot'] . '/cache');

class My_Security_Policy extends Smarty_Security
{
    public $php_functions = array('var_dump', 'is_array', 'isset', 'array_column', 'in_array', 'empty');
    public $php_modifiers = array('strtoupper', 'substr', 'strpos', 'sprintf', 'money_format', 'format_currency');
    public $allow_constants = true;
    public $secure_dir = array('/var/www/html/modules/_shared/module.plr_executeaction/html/');
}

// Create a wrapper class extended from Smarty
class tempengine_extSmarty extends Smarty
{
    var $currentApplication = null;
    var $servicequery_base;
    var $ajaxquery_base;
    var $callerquery_base;
    var $basicformquery_base;
    var $application_base;
    var $language;

    // $cache and $cache_lifetime are the two main variables
    // that control caching within Smarty
    function __construct($_language = false)
    {
        global $_GVARS;
        global $_CONFIG;
        global $g_lang_strings;

        // Run Smarty's constructor
        parent::__construct();
        // $this->Smarty();

        // enable security
        $this->enableSecurity('My_Security_Policy');

        // Explicitly allow PHP functions as modifiers
        // $this->registerPlugin('modifier', 'var_dump', 'var_dump');
        $this->registerPlugin('modifier', 'substr', 'substr');
        $this->registerPlugin('modifier', 'strtoupper', 'strtoupper');
        $this->registerPlugin('modifier', 'ucfirst', 'ucfirst');
        $this->registerPlugin('modifier', 'strpos', 'strpos');
        $this->registerPlugin('modifier', 'sprintf', 'sprintf');
        $this->registerPlugin('modifier', 'implode', 'implode');
        $this->registerPlugin('modifier', 'is_array', 'is_array');
        $this->registerPlugin('modifier', 'in_array', 'in_array');
        $this->registerPlugin('modifier', 'var_dump', 'var_dump');
        $this->registerPlugin('modifier', 'money_format', 'money_format');
        $this->registerPlugin('modifier', 'format_currency', [
            NumberFormatter::create('nl_NL', NumberFormatter::CURRENCY),
            'formatCurrency'
        ]);

        // Change the default template directories
        $this->template_dir = TEMPLATE_DIR;
        $this->compile_dir = COMPILE_DIR;
        $this->config_dir = CONFIG_DIR;
        $this->cache_dir = CACHE_DIR;

        /***
         * Configure Smarty settings
         */
        $this->compile_check = true;
        $this->debugging = false;
        $this->compile_id = isset($_GVARS['lang']) ? $_GVARS['lang'] : '';
        $this->caching = false;
        $this->use_sub_dirs = true;
        $this->force_compile = true;
        $this->loadFilter('post', 'lang');

        /**
         * Internationalize the date time format
         */
        $i18n_dt = $_CONFIG['currentdatetimeformat'];
        if (isset($g_lang_strings['week']))
            $i18n_dt = str_replace('#week#', $g_lang_strings['week'], $i18n_dt);
        if (isset($g_lang_strings['day']))
            $i18n_dt = str_replace('#day#', $g_lang_strings['day'], $i18n_dt);
        $this->assign('i18n_currentdatetimeformat', $i18n_dt);

        /***
         * Pass the current language to Smarty
         */
        $this->language = $_language;
        $this->assign("lang", $_language);

        /***
         * Set other settings
         */
        $this->assign('config', $_CONFIG);
        $this->SetLocations();
    }

    function SetApplication($app)
    {
        $this->currentApplication = $app;
        $this->SetLocations();
    }

    function setLocations()
    {
        global $_GVARS;

        $this->assign("serverroot", $_GVARS['serverroot']);
        $this->assign("serverpath", $_GVARS['serverpath']);
        $this->assign("docroot", $_GVARS['docroot']);

        if ($this->currentApplication) {
            $this->servicequery_base = $this->currentApplication->applicationLink(BASEURL_SERVICE);
            $this->ajaxquery_base = $this->currentApplication->applicationLink(BASEURL_AJAX);
            $this->callerquery_base = $this->currentApplication->applicationLink(BASEURL_CALLER);
            $this->basicformquery_base = $this->currentApplication->applicationLink(BASEURL_CALLER);
            $this->application_base = $this->currentApplication->applicationLink(BASEURL_APPLICATION);
        } else {
            $this->servicequery_base = '/services/json';
            $this->ajaxquery_base = '/ajax/json';
            $this->callerquery_base = '';

            if (strpos($_SERVER['PHP_SELF'], 'srv_') !== false) {
                $this->callerquery_base .= '/services/json/';
            }
            if (isset($_GET['app'])) {
                $this->callerquery_base .= '/app/' . $_GET['app'] . '/';
                $this->servicequery_base .= '/app/' . $_GET['app'] . '/';
                $this->ajaxquery_base .= '/app/' . $_GET['app'] . '/';
                $this->basicformquery_base .= '/app/' . $_GET['app'] . '/';
                $this->application_base .= '/app/' . $_GET['app'] . '/';
            }
        }

        $this->assign('approot', $_GVARS['serverroot'] . $this->callerquery_base);
        if (isset($this->currentApplication)) {
            $_currentConstruct = $this->currentApplication->GetCurrentConstructMetaName();
            $this->callerquery_base .= 'const/' . $_currentConstruct . '/';
            $this->basicformquery_base .= 'basicform/' . $_currentConstruct . '/';
            $this->servicequery_base .= 'const/' . $_currentConstruct . '/';
            $this->ajaxquery_base .= 'const/' . $_currentConstruct . '/';
        }

        $this->assign('callerpage', $_GVARS['serverroot']);
        $this->assign('callerquery', $this->callerquery_base);
        $this->assign('basicformquery', $this->basicformquery_base);
        $this->assign('servicequery', $this->servicequery_base);
        $this->assign('ajaxquery', $this->ajaxquery_base);
        $this->assign('applicationurl', $this->application_base);

        if (isset($_SERVER['REQUEST_URI'])) {
            $_url = parse_url($_SERVER['REQUEST_URI']);
            if (isset($_url['query'])) $this->assign('callerqueryparams', '?' . $_url['query']);
        }
    }
}
