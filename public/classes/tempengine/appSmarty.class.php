<?php
global $_GVARS, $_CONFIG;
// Define our template directories for Polaris website
define('APP_TEMPLATE_DIR', $_GVARS['docroot'].'/templates/app_templates');
define('APP_CACHING_DIR', $_CONFIG['smarty_cache_folder']);

// Create a wrapper class extended from Smarty
class tempengine_appSmarty extends tempengine_extSmarty {

    function __construct($_language=false) {
        // Run Smarty's constructor
        parent::__construct($_language);
        // Change the default template directories
        $this->template_dir   = APP_TEMPLATE_DIR;
        $this->cache_dir      = APP_CACHING_DIR;
        $this->compile_id     = $this->language;
        $this->caching        = false; // no use, dynamic content
        $this->force_compile  = true;

        $this->assign('plr_environment', $_ENV['ENVIRONMENT']);
        $this->assign('new_relic_active', $_ENV['NEW_RELIC_ACTIVE']);
        $this->assign('prevaction', $_SESSION['prevaction'] ?? '');
        unset($_SESSION['prevaction']);

        /***
        * Set this page default or custom LOCALE settings
        */
        if (!empty($rec->LOCALE))
            setlocale (LC_TIME, $rec->LOCALE);
        elseif (!empty($param['locale']))
            setlocale (LC_TIME, $param['locale']);

        if (isset($_GET['select'])) {
            $this->assign("selectform", $_GET['select']);
            $this->assign("selectfield", $_GET['selectfield']);
            $this->assign("selectmasterfield", $_GET['selectmasterfield']);
            $this->assign("selectshowfield", $_GET['selectshowfield']);
        }
    }

    function setLocations() {
        parent::setLocations();

        $callerquery_search = $this->callerquery_base;
        $callerquery_search = remove_query_arg( $callerquery_search, 'q' );
        $callerquery_search = remove_query_arg( $callerquery_search, 'offset' );

        $callerquery_insert = $this->callerquery_base;
        $callerquery_insert = remove_query_arg( $callerquery_insert, 'action' );
        $callerquery_insert = remove_query_arg( $callerquery_insert, 'offset' );
        $callerquery_insertmaster = $callerquery_insert;
        $callerquery_insertmaster = remove_query_arg( $callerquery_insert, 'rec' );
        $callerquery_insertdetail = $callerquery_insert;
        $callerquery_insertdetail = remove_query_arg( $callerquery_insert, 'detailrec' );

        $callerquery_clean = $this->callerquery_base;
        $callerquery_clean = remove_query_arg( $callerquery_clean, 'rec' );
        $callerquery_clean = remove_query_arg( $callerquery_clean, 'detailrec' );
        $callerquery_clean = remove_query_arg( $callerquery_clean, 'action' );
        $callerquery_clean = remove_query_arg( $callerquery_clean, 'detailaction' );
        //    $callerquery_clean = remove_query_arg( $callerquery_clean, 'q' );
        $callerquery_clean = remove_query_arg( $callerquery_clean, 'q\[\]' );
        $callerquery_clean = remove_query_arg( $callerquery_clean, 'q%5B%5D' );
        //    $callerquery_clean = remove_query_arg( $callerquery_clean, 'qc' );
        $callerquery_clean = remove_query_arg( $callerquery_clean, 'qc\[\]' );
        $callerquery_clean = remove_query_arg( $callerquery_clean, 'qc%5B%5D' );
        $callerquery_clean = remove_query_arg( $callerquery_clean, 'offset' );
        $this->assign('callerquery_clean', $callerquery_clean);
        $this->assign('callerquery_insertmaster', $callerquery_insertmaster);
        $this->assign('callerquery_insertdetail', $callerquery_insertdetail);
        $this->assign('callerquery_search', $callerquery_search);
    }
}
