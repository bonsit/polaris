<?php
global $_GVARS;
global $_CONFIG;

// Define our template directories for the Polaris Designer
define('DSG_TEMPLATE_DIR', $_GVARS['docroot'].'/templates/dsg_templates');
define('DSG_CACHING_DIR', $_CONFIG['smarty_cache_folder']);

// Create a wrapper class extended from Smarty
class tempengine_dsgSmarty extends tempengine_extSmarty {

	function __construct($langid='') {
        global $locations;
        global $g_lang_strings;
        global $protocol;
        global $_GVARS;

        parent::__construct($langid);

        // Change the default template directories
        $this->template_dir = DSG_TEMPLATE_DIR;
        $this->cache_dir = DSG_CACHING_DIR;
        $this->compile_id     = false;
        $this->force_compile  = true;
        // ******** PLR stuff for smarty ******** //
        // $this->config_load('globals.conf');
        $this->assign('locations', $locations);
        $this->assign('yesno', array('Y' => ' '.$g_lang_strings['yes'], 'N' => ' '.$g_lang_strings['no']));
        $this->assign('usertype', $_SESSION['usertype']);
        $username = $_SESSION['name'];
        if ($_SESSION['usertype'] != 'user')
            $username .= ' ('.$_SESSION['usertype'].' admin)';
        $this->assign('username', $username);

        $this->callerquery_base = $_GVARS['serverroot'].'/designer/';
        if (isset($_GET['module']))
            $this->callerquery_base .= $_GET['module'].'/';

        $this->assign('callerpage', $this->callerquery_base);
        $this->assign('callerquery', $this->callerquery_base);
    }
}
