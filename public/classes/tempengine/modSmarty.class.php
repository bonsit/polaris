<?php
global $_GVARS, $_CONFIG;

// Create a Module Template class extended from Smarty
class tempengine_modSmarty extends tempengine_extSmarty {

    function __construct($_language=false) {
        global $_GVARS;
        global $g_lang_strings;

        parent::__construct($_language);
        $this->caching = false;
        $this->force_compile = true;
        $this->assign('yesno', array('Y' => ' '.$g_lang_strings['yes'], 'N' => ' '.$g_lang_strings['no']));
        $this->assign('moduleroot', $_GVARS['serverroot']);
    }

}
