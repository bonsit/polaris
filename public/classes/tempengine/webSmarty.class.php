<?php
global $_GVARS, $_CONFIG;

require_once($_GVARS['docroot'].'/includes/param.inc.php');

// Define our template directories for Polaris website
define('WEB_TEMPLATE_DIR', $_GVARS['docroot'].'/web_templates');

// Create a wrapper class extended from Smarty
class tempengine_webSmarty extends tempengine_extSmarty {

    function __construct($_language=false) {
        global $_GVARS;
        global $webdb;

        $this->db = $webdb;
        $this->defaultmenuitem = 1;
        // Run Smarty's constructor
        parent::__construct($_language);
        // Change the default template directories
        $this->template_dir = WEB_TEMPLATE_DIR;

        $this->register_resource("db", array("db_get_template",
                                       "db_get_timestamp",
                                       "db_get_secure",
                                       "db_get_trusted"));

        /***
        * Pass the parameters of the web_param table to Smarty
        */
        $this->assign("param", $param);

        /***
        * Set the page variable [ page.php?pi=... ]
        * and the menu variable [ page.php?mi=... ]
        */
        $menuid = '';
        $pagename = '';
        if (!isset($menuid) and isset($_GET['mi']))
          $menuid = $_GET['mi'];
        if ($menuid == '') $menuid = $this->defaultmenuitem;
        if (isset($_GET['pi']))
          $pagename = $_GET['pi'];
        /***
        * Load the Page object from the web_page table
        */
        if (isset($this->db) and ($this->db != null)) {
          $rec = $this->GetPageObject($pagename, $menuid);
          if (isset($rec->MENUID))
            $menuid = $rec->MENUID;
          else
            $menuid = 1;
          if (is_object($rec))
            $pageid = $rec->PAGENAME;
          else
            $pageid = $rec;
          $this->assign("menuid",$menuid);
          $this->assign('pageid', $pageid);

          /***
          * Set the subpage variables [ page.php?si=... ]
          */
          if (isset($_GET['si']))
            $subpagename = $_GET['si'];
          if ( isset($subpagename) and ($this->template_exists ( 'db:'.$subpagename )) )
            $this->assign('subpageid', 'db:' . $subpagename);
          /***
          * Set this page default or custom STYLESHEET settings
          */
          if (!empty($rec->STYLESHEET))
            $this->assign('stylesheet', $rec->STYLESHEET);
          elseif (!empty($param['stylesheet']))
            $this->assign('stylesheet', $param['stylesheet']);
          /***
          * Set this page default or custom LOCALE settings
          */
          if (!empty($rec->LOCALE))
            setlocale (LC_TIME, $rec->LOCALE);
          elseif (!empty($param['locale']))
            setlocale (LC_TIME, $param['locale']);
          /***
          * Set this page default or custom ENCODING settings
          */
          if (!empty($rec->ENCODING))
            $this->assign('encoding', $rec->ENCODING);
          elseif (!empty($param['encoding']))
            $this->assign('encoding', $param['encoding']);
        }

        $this->assign('param', $param);
    }

    function GetPageObject($pagename, &$menuindex) {
        global $_sqlFirstPage, $_sqlPage, $_GVARS;

        $_sqlFirstPage = '
          SELECT p.pagename, p.sqlsource, p.encoding, p.locale, p.stylesheet, m.menuid
            FROM web_page p
            LEFT JOIN web_menu m
            ON m.pagename = p.pagename
            WHERE (parentmenuid = ? OR menuid = ?)
            AND LANGUAGE = ?
            AND p.LANGUAGE = LANGUAGE
            ORDER BY orderindex
        ';
        $_sqlPage = '
          SELECT p.pagename, p.sqlsource, p.encoding, p.locale, p.stylesheet, m.parentmenuid, m.menuid
            FROM web_page p
            LEFT JOIN web_menu m
            ON m.pagename = p.pagename
            WHERE upper(p.pagename) = ?
            AND p.language = ?
        ';

        $rec = false;
        if ($pagename != '') {
            $res = $this->db->Execute($_sqlPage, array(strtoupper($pagename), $_GVARS['lang'])) or Die('$_sqlPage failed: '.$_sqlPage);
            $rec = $res->FetchNextObject(true);
            if (($menuindex != 0) and isset($rec->MENUID))
                $menuindex = $rec->MENUID;
        }

        if ($rec == false) {
            $res = $this->db->Execute($_sqlFirstPage, array($menuindex, $menuindex, $_GVARS['lang'])) or Die('$_sqlFirstPage failed: '.$_sqlFirstPage);
            $rec = $res->FetchNextObject(true);
            if ($rec == false) {
                $res = $this->db->Execute($_sqlFirstPage, array($this->defaultmenuitem, $this->defaultmenuitem, $_GVARS['lang'])) or Die('$_sqlFirstPage failed: '.$_sqlFirstPage);
               $rec = $res->FetchNextObject(true);
            }
        }
        return $rec;
    }
}

function combine_where($sql, $filter) {
    if ($filter) {
        $orderpos = strpos(strtolower($sql), 'order by');
        $wherepos = strpos(strtolower($sql), 'where ');
        if ($orderpos === FALSE) {
            if ($wherepos === FALSE)
                $sql = "$sql where $filter";
            else
                $sql = "$sql and $filter";
        } else {
            $select = substr($sql, 0, $orderpos - 1);
            $orderby = substr($sql, $orderpos);
            if ($wherepos === FALSE)
                $sql = "$select where $filter $orderby";
            else
                $sql = "$select and $filter $orderby";
        }
    }
    return $sql;
}

// put these function somewhere in your application
function db_get_template ($tpl_name, &$tpl_source, &$smarty_obj) {
	global $webdb, $_GVARS, $g_lang_strings;
  // populating $tpl_source
	if ($tpl_name != '') {
    $rs = $webdb->Execute("
   	             select html, sqlsource, field(language, 'FR', 'NL' , 'AA') as orderindex
                  from web_page
                  where pagename='$tpl_name'
   						   and (language = '{$_GVARS['lang']}'
  							   or language = 'NL'
  								 or language = 'AA')
  							 order by orderindex
   							 ");
   	$rec = $rs->FetchNextObject(true);
    if ($rec) {
      if ($rec->SQLSOURCE != '') {
      	if (substr($rec->SQLSOURCE, 0, 5) == 'SQL::') {
      	  $sql = substr($rec->SQLSOURCE, 5, 999);
      	  $sql = str_replace(':language', "'{$_GVARS['lang']}'", $sql);
          foreach ($_GET as $key => $waarde) {
            $sql = str_replace(":$key", "$waarde", $sql);
          }
          foreach ($smarty_obj->_tpl_vars as $key => $waarde) {
            if (! is_array($waarde)) {
              $sql = str_replace(":$key", "$waarde", $sql);
            }
          }

      	}
      	else
      	  $sql = 'SELECT * FROM '.$rec->SQLSOURCE;
  			$filter = $smarty_obj->_tpl_vars['sqlfilter'];
  			$sql = combine_where($sql, $filter);

        $items = $webdb->GetAll($sql); // or Die('$web_page sqlsource failed: '.$sql);
        $smarty_obj->assign('items', $items);
      }
      $tpl_source = $rec->HTML;
      return true;
    }
	}
	else {
		$tpl_source = $g_lang_strings['page_not_found'];
		return  true;
	}
}

function db_get_timestamp($tpl_name, &$tpl_timestamp, &$smarty_obj)
{
	global $webdb, $_GVARS;
  // do database call here to fetch your template,
  // populating $tpl_source
	if ($tpl_name != '') {
    $rs = $webdb->Execute("
  	             select timestamp, field(language, 'FR', 'NL' , 'AA') as orderindex
                 from web_page
                 where pagename='$tpl_name'
  						   and (language = '{$_GVARS['lang']}'
  						   or language = 'NL'
  							 or language = 'AA')
  						 order by orderindex
  							 ");
  	$rec = $rs->FetchNextObject(true);
    if ($rec) {
      $tpl_timestamp = $rec->TIMESTAMP;
      return true;
    }	else {
      return false;
  	}
	}	else {
		$tpl_timestamp = 0;
		return true;
	}
}

function db_get_secure($tpl_name, &$smarty_obj)
{
    // assume all templates are secure
    return true;
}

function db_get_trusted($tpl_name, &$smarty_obj)
{
    // not used for templates
}
