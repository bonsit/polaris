<?php
// Define our template directories for Polaris portal
define('POR_TEMPLATE_DIR', $_GVARS['docroot'].'/templates/por_templates');

// Create a wrapper class extended from Smarty
class POR_Smarty extends Ext_Smarty {

	function __construct($_language=false) {
        // Run Smarty's constructor
        parent::__construct($_language);
        // Change the default template directories
        $this->template_dir = POR_TEMPLATE_DIR;

		$this->unregister_resource("db");
		$this->caching = false;
		$this->force_compile = true;
	}
}