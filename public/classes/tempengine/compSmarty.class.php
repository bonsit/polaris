<?php
global $_GVARS, $_CONFIG;

// Create a Module Template class extended from Smarty
define('COMP_TEMPLATE_DIR', $_GVARS['docroot'].'/guicomponents');

// Create a wrapper class extended from Smarty
class tempengine_compSmarty extends tempengine_extSmarty {

    function __construct($_language=false) {
        parent::__construct($_language);

        // Change the default template directories
        $this->template_dir   = COMP_TEMPLATE_DIR;
        $this->compile_id     = $this->language;
        $this->caching        = false; // no use, dynamic content
        $this->force_compile  = true;
    }

}
