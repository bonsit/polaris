#!/bin/bash
#path=/usr/local/mysql-5.1.57-osx10.6-x86_64/bin
thepath=/opt/local/lib/mysql5/bin

echo "Met dit tool kunt u een Polaris Productie database overzetten naar een andere omgeving."
echo
echo "Naar welke omgeving wilt u de Productie gegevens (polaris_prod@172.16.1.16) overzetten? "
echo "[1] Naar de Ontwikkelomgeving (polarisnha@localhost)"
echo "[2] Naar de Testomgeving (polaris_ontwikkel@172.16.1.10)"
echo
read -p "Uw keuze: " keuze
echo
stty -echo
read -p "Vul het wachtwoord van de Productie database in: " prodpass; echo
stty echo

if [ x"$keuze" = "x1" ]
then
    desthost=localhost
    destdb=polaris_nha
    destuser=root
    destpass=
elif [ x"$keuze" = "x2" ]
then
    desthost=172.16.1.10
    destdb=polaris_ontwikkel
    destuser=nhadev
    destpass=nha2009
    customcss=nha_default_TEST.css
fi

if [ x"$keuze" = "x1" -o x"$keuze" = "x2" ]
then
    $thepath/mysqldump --user=nhadev --password=$prodpass --host=172.16.1.16 polaris_prod | $thepath/mysql --user=$destuser --password=$destpass --host=$desthost -D $destdb

    if [ "$?" -ne "0" ]; then
        echo "Sorry, er ging iets mis met het importeren van de Polaris database."
        echo
        exit 1
    fi

    if [ x"$keuze" = "x1" ]
    then
        $thepath/mysql --user=$destuser --password=$destpass --host=$desthost -D $destdb -e 'update plr_application set customcss="css/client/nha_default_ONTW.css" where applicationid = 1'
    else
        $thepath/mysql --user=$destuser --password=$destpass --host=$desthost -D $destdb -e 'update plr_application set customcss="css/client/nha_default_TEST.css" where applicationid = 1'
    fi
    echo
    echo "De gegevens zijn overgezet!"
fi
