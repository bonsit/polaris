<?php
require_once('guicomponents/component._base.php');
// require_once('includes/datelocale.inc.php');

/***
* Shows a Schedule component
**/

class maxia_schemaComponent extends _BaseComponent {
    var $_startyear;
    var $_endyear;
    var $_datelocale;
    var $_currentyear;

    function __construct($owner=false) {
        parent::__construct($owner);
    }

    function render($html, $record, $fields) {
        $_keys = $fields;
        $_values = $fields;
        array_walk($_keys, function(&$_value) {
            $_value = '{'.$_value.'}';
        });
        array_walk($_values, function(&$_value) use ($record) {
            $_value = $record->$_value;
        });
        $html = str_replace($_keys, $_values, $html);
        return $html;
    }

    function GenerateXHTML($params) {
        parent::GenerateXHTML($params);

        $this->AddComponentJS('schema.js');

        $_fields = array(
            'SCHEMA_TYPE',
            'SCHEMA_EENMALIG_START_DATUM',
            'SCHEMA_WEEK_HERHALING',
            'SCHEMA_MAAND_HERHALING',
            'SCHEMA_MAAND_START_DAG',
            'SCHEMA_PERIODIEK',
            'SCHEMA_START_TIJD',
            'SCHEMA_DAGELIJKS_START_DATUM',
            'SCHEMA_WEEK_START_DATUM',
            'SCHEMA_MAANDELIJKS_START_DATUM'
        );

        // $_html = file_get_contents("guicomponents/maxia_schema.html");
        $_html = $this->Fetch('schema.tpl.php');
        $_r = $this->render($_html, $this->rec, $_fields);
        // $_r .= '<script type="text/javascript">';
        // $_r .= file_get_contents("guicomponents/maxia_schema.js");
        // $_r .= '</script>';

        return $_r;
    }
}
