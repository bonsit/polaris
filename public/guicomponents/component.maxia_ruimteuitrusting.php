<?php
require_once('guicomponents/component._base.php');
// require_once('includes/datelocale.inc.php');

/***
* Shows a Ruimte uitrusting component
**/

class maxia_ruimteuitrustingComponent extends _BaseComponent {
    var $_startyear;
    var $_endyear;
    var $_datelocale;
    var $_currentyear;
    var $_cfi = null;

    function __construct($owner=false) {
        parent::__construct($owner);
    }

    function render($html, $record, $fields) {
        $_keys = $fields;
        $_values = $fields;
        array_walk($_keys, function(&$_value) {
            $_value = '{'.$_value.'}';
        });
        array_walk($_values, function(&$_value) use ($record) {
            $_value = $record->$_value;
        });
        $html = str_replace($_keys, $_values, $html);
        return $html;
    }

    function GetActieveLocatie() {
        return $_SESSION['_GRP_LOCATIE'] ?? false;
    }

    function ProcessRecordset($recordset) {
        global $scramble;

        require_once('includes/cloudflare_images.php');

        if (!$this->_cfi) {
            $this->_cfi = new bartb\CloudFlare\Images($_ENV['CLOUDFLARE_IMAGES_ACCOUNT'], $_ENV['CLOUDFLARE_IMAGES_TOKEN'], $_ENV['CLOUDFLARE_ACCOUNT_HASH'], $_ENV['CLOUDFLARE_IMAGES_DELIVERY_URL']);
        }

        foreach($recordset as $index => $rec) {
            if (isset($rec['FOTOS'])) {
                $_foto = explode(',', $rec['FOTOS'])[0];
                if (!empty($_foto)) {
                    $keyfieldselect = $this->owner->database->makeEncodedKeySelect('mx_mediabeheer', $_foto);
                    $_sql = "SELECT CLOUD_IMAGE_ID FROM mx_mediabeheer WHERE {$keyfieldselect}";
                    $_imageRecord = $this->owner->database->userdb->GetRow($_sql);
                    $_result = $this->_cfi->getImageUrl($_imageRecord['CLOUD_IMAGE_ID'], $size);

                    $recordset[$index]['UITRUSTING_FOTO'] = $_result;
                }
            }
        }

        return $recordset;
    }

    function GenerateXHTML($params) {
        global $scramble;

        parent::GenerateXHTML($params);

        $this->AddComponentJS('ruimteuitrusting.js');

        if (!empty($this->rec->LOCATIE)) {
            $_huidigeLocatie = $this->rec->LOCATIE;
        } else {
            $_huidigeLocatie = $this->GetActieveLocatie();
        }

        $_uitrusting = '
            SELECT ru.RUIMTE_ID AS CHECKED, u.UITRUSTING_ID, ru.RUIMTE_ID, UITRUSTING_NAAM, UITRUSTING_OMSCHRIJVING,
            FOTOS,
            MD5(CONCAT("'.$scramble.'", "_", ru.RUIMTEUITRUSTING_ID)) AS PLR__RECORDID
            FROM fb_uitrusting u LEFT JOIN `fb_ruimte_uitrusting` ru
            ON u.`uitrusting_id` = ru.`uitrusting_id`
            AND u.`client_hash` = ru.`client_hash`
            AND ru.`ruimte_id` = ?
            AND ru.__DELETED <> 1
            WHERE u.client_hash = ?
            AND u.__DELETED <> 1
            -- AND u.LOCATIE = ?
            AND (JSON_CONTAINS(u.LOCATIES, \'"'.$_huidigeLocatie.'"\') -- locatie in set van locaties
                OR "'.$_huidigeLocatie.'" = ""                         -- of locatie is leeg
                OR JSON_EXTRACT(u.LOCATIES, \'$[0]\') = ""             -- of uitrusting is voor alle locaties
                OR u.LOCATIES IS NULL
            )
            ORDER BY VOLGORDE
        ';
        $_uitrustingrs = $this->userdb->GetAll($_uitrusting, array(
            $this->rec->RUIMTE_ID, $_SESSION['clienthash']
        ));
        // momenteel geen foto's bij de ruimtes (is wat veel van het goed)
        // $_uitrustingrs = $this->ProcessRecordset($_uitrustingrs);
        $this->smarty->assign('uitrusting', $_uitrustingrs);
        $_html = $this->fetch('ruimteuitrusting.tpl.html');

        return $_html;
    }

}
