jQuery(function () {
    // Initialize Check All input
    var InitializeCheckAll = function() {
        if ($(".icheck.checkbox_uitrusting:checked").length == $(".icheck.checkbox_uitrusting").length) {
            $("#_fldCHECKALL_UITRUSTING").iCheck('check');
        } else {
            $("#_fldCHECKALL_UITRUSTING").prop('checked',false).iCheck('update');
        }
    }
    InitializeCheckAll();

    $("#_fldCHECKALL_UITRUSTING").on("ifChanged", function (e) {
        log(e.target.checked);
        $(".icheck.checkbox_uitrusting").iCheck(e.target.checked?'check':'uncheck');
    });

    $(".icheck").on("ifChanged", function(e) {
        var id = this.value;
        var label = this.closest('label');
        var inp = label.querySelector('input[name="fb_ruimte_uitrusting!_hdnRecordID[]"]');
        if (inp) {
            if (inp.value != '') {
                var state = e.target.checked ? '__nothing' : 'delete';
            } else {
                var state = e.target.checked ? 'insert' : '__nothing__';
            }
            label.querySelector('input[name="fb_ruimte_uitrusting!_hdnState[]"]').value = state;
        }
        InitializeCheckAll();
    });

});
