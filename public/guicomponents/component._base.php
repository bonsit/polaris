<?php

class _BaseComponent  {
  var $name;
  var $params;
  var $owner;
  var $userdb;
  var $componentparams;
  var $smarty;
  var $columns;
  var $rec;
  var $componentfolder;
  var $componentpath;

  function __construct($owner=false) {
    global $_GVARS;
    global $polaris;

    $this->owner = $owner;
    $this->name = strtolower(str_replace('Component','',get_class($this)));
    $this->smarty = new tempengine_compSmarty($polaris->language);
    $this->smarty->setTemplateDir($_GVARS['docroot'].'/guicomponents/component.'.$this->name.'/');
    $this->componentfolder = '/guicomponents/';
    $this->componentpath = $_GVARS['serverroot'].$this->componentfolder;
  }

  function CloneItem() {
    return (($_GET['action'] == 'insert' and $_GET['rec'] != '')
    or ($_GET['detailaction'] == 'insert' and $_GET['detailrec'] != ''));
  }

  function AddComponentJS($javascriptModules /* String or Array of strings */ ) {
    global $_GVARS;

    $_GVARS['_includeJavascript'] = $_GVARS['_includeJavascript']??[];

    foreach ((array) $javascriptModules as $str) {
      if (!in_array($str, $_GVARS['_includeJavascript'] )) {
        $_GVARS['_includeJavascript'][] = $this->componentpath.'component.'.$this->name.'/'.$str;
      }
    }
  }

  function AddCustomJS($customJavascript /* String or Array of strings */ ) {
    global $_GVARS;

    $_GVARS['_includeJavascript'] = $_GVARS['_includeJavascript']??[];

    foreach ((array) $customJavascript as $str) {
      if (!in_array($str, $_GVARS['_includeJavascript'] )) {
        $_GVARS['_includeJavascript'][] = $_GVARS['serverpath'].$str;
      }
    }
  }

  function Fetch($template) {
    $_parts = explode('.tpl', $template);
    $_type = end($_parts);

    switch($_type) {
        case '.html':
            $_result = $this->smarty->Fetch($template);
        break;
        case '.php':
          $_result = file_get_contents('.'.$this->componentfolder.'component.'.$this->name.'/'.$template);
      break;
      case '.jsrender':
            $_result = file_get_contents('.'.$this->componentfolder.'component.'.$this->name.'/'.$template);
        break;
    }
    return $_result;
  }

  function GenerateXHTML($params) {
    global $_GVARS;
    global $polaris;

    $this->params = $params;
    $this->owner = $params[0];
    $this->permission = $params[0]->record->PERMISSIONTYPE ?? SELECT & INSERT & UPDATE & DELETE;
    $this->rec = $params[1];
    $this->columns = $params[2];
    $this->userdb = $params[3];
    $this->componentparams = $params[4];
  }

}