<?php
require_once('guicomponents/component._base.php');

/***
* Toont een Aanvraag uitrusting component
**/

class maxia_reserveringUitrustingComponent extends _BaseComponent {
    var $_startyear;
    var $_endyear;
    var $_datelocale;
    var $_currentyear;

    function __construct($owner=false) {
        parent::__construct($owner);
    }

    function GenerateXHTML($params) {
        global $polaris;

        parent::GenerateXHTML($params);

        $this->AddCustomJS('/modules/_shared/module.plr_maxia/js/maxia.js');
        $this->AddCustomJS('/modules/_shared/module.plr_maxia_reserveringen/js/maxia_reserveringen.js');
        $this->AddComponentJS('reserveringuitrusting.js');

        if ($polaris->plrClient->clientparameters['RUFTS'] == 'Y')
            return $this->fetch('reserveringuitrusting2.tpl.jsrender');
        else
            return $this->fetch('reserveringuitrusting.tpl.jsrender');
    }

}
