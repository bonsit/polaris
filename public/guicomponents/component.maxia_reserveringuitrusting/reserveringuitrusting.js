jQuery(function () {

    var refreshAanvraagCheckboxEvents = function() {
        $(".uitrusting-view .icheck").on("ifChanged", function(e) {
            var id = this.value;
            var label = this.closest('label');
            var inp = label.querySelector('input[name="fb_reserveringen_intern_uitrusting!_hdnRecordID[]"]');
            if (inp) {
                if (inp.value != '') {
                    var state = e.target.checked ? '__nothing' : 'delete';
                } else {
                    var state = e.target.checked ? 'insert' : '__nothing__';
                }
                label.querySelector('input[name="fb_reserveringen_intern_uitrusting!_hdnState[]"]').value = state;
                $("#_fldSTATUS").val('nieuw');
            }
        });
    }

    var vulAanvraagUitrustingView = function(aanvraagid, ruimteid, rrule, duur) {
        Maxia.getAanvraagUitrusting(aanvraagid, ruimteid, rrule, duur, function(data, textStatus) {
            if (textStatus == 'success') {
                if (data.length > 0) {
                    Maxia.Reserveringen.uitrustingData.uitrustingen(data);
                    Polaris.Base.initializeIChecks();
                    refreshAanvraagCheckboxEvents();
                }
            } else {
                alert('De uitrusting kon niet geladen worden. Error: ' + textStatus);
            }
        });
    }

    var triggerRefresh = function() {
        vulAanvraagUitrustingView($('#_fldAANVRAAG_ID').val(), $("#_fldRUIMTE").val(), $("#_fldCOMPONENTRRULE").val(), $("#_fldRRULE_DUUR").val());
    }
    $("#_fldRUIMTE").on('change', triggerRefresh);
    $(document).on("maxia.rrule.changed", triggerRefresh);

    triggerRefresh();
});
