maxiaRRule = {
    isEditMode: false,
    switchcount: false,
    rrulevalue: null,
    rrule: null,
    duur: null,
    previewcal: null,
    ajaxInvoke: false,
    schematypes: ['E', 'D', 'W', 'M'],
    defaultschematype: 'E',
    schematype: null,
    perioden: {
        'ochtend': {'start': '08:00', 'eind': '12:00'},
        'middag': {'start': '13:00', 'eind': '17:00'},
        'heledag': {'start': '08:00', 'eind': '17:00'},
    },
    initialize: function () {
        const editStateValue = $("#dataform input[name=_hdnState]").val();
        this.isEditMode = editStateValue === 'edit';
        const rruleField = $("#_fldCOMPONENTRRULE");
        const defaultOptions = [];

        // Function to set the rrule and rrulevalue to default
        const setDefaultRRule = () => {
            this.rrule = new rrule.RRule(defaultOptions);
            this.rrulevalue = '';
            this.schematype = this.defaultschematype;

            if (!this.isEditMode) {
                let ruimte = Polaris.Base.getLocationVariable('ruimte');
                if (!isNullOrEmpty(ruimte)) {
                    $("#_fldRUIMTE").val(ruimte).trigger('change');
                }

                let startdatum = Polaris.Base.getLocationVariable('startdatum');
                let einddatum = Polaris.Base.getLocationVariable('einddatum');
                let starttijd = Polaris.Base.getLocationVariable('starttijd');
                let eindtijd = Polaris.Base.getLocationVariable('eindtijd');

                if ((startdatum && (startdatum == einddatum)) || (isNullOrEmpty(startdatum))) {
                    this.schematype = 'E';
                    this.rrule.options.dtstart = this.datumNaarRRuleFormaat(startdatum, true).toJSDate();
                } else {
                    this.schematype = 'D';
                    this.rrule.options.dtstart = this.datumNaarRRuleFormaat(startdatum, true).toJSDate();
                    let einddatumObj = this.datumNaarRRuleFormaat(einddatum, true);
                    // 1 dag later, ivm einddatum is exclusief
                    einddatumObj = einddatumObj.plus({ days: 1 });
                    this.rrule.options.until = einddatumObj.toJSDate();
                }
                if (starttijd) {
                    this.rrule.options.byhour = [parseInt(starttijd.split(':')[0])];
                    this.rrule.options.byminute = [parseInt(starttijd.split(':')[1])];
                    this.duur = this.convertTijdenNaarDuur(starttijd, eindtijd);
                    this.vulDagdeel('.aangepastetijd', this.rrule.options.byhour[0], this.rrule.options.byminute[0], this.duur);
                } else {
                    // Use luxon to set hours and minutes with correct rounding
                    const now = luxon.DateTime.now();
                    const hours = now.hour;
                    const minutes = Math.round(now.minute / 15) * 15;

                    // Adjust hours if minutes round up to 60
                    this.rrule.options.byhour = minutes >= 60 ? [hours + 1] : [hours];
                    this.rrule.options.byminute = minutes >= 60 ? [0] : [minutes];
                }
            }
        };

        if (this.isEditMode && rruleField.val() !== '') {
            this.rrulevalue = rruleField.val();

            try {
                this.rrule = rrule.RRule.fromString(this.rrulevalue);
                this.schematype = rrule.Frequency[this.rrule.options.freq][0];

                // If the frequency is Daily and count is 1, it's a single event
                if (this.schematype === 'D' && this.rrule.options.count === 1) {
                    this.rrulevalue = '';
                    this.schematype = 'E';
                }
            } catch (e) {
                console.error("Error parsing rrule", e);
                Polaris.Base.errorDialog('RRule is niet correct ingevuld. We gebruiken de standaardwaarden.', this.rrulevalue);
                setDefaultRRule();
            }
        } else {

            setDefaultRRule();
        }

        // Initialize inputs and tabs
        this.initialiseerGeneralInputs();
        this.initialiseerWeek();
        this.initialiseerMaand();
        this.initialiseerSchemaTabs();
        this.initialiseerDatumsPreview();

        this.toonRRule();
        this.verwerkRRule();

        this.refreshDatumsPreview();
        this.initialiseerExDatumsComponent();

        // Attach click event to save buttons
        $("#btnSaveForm,#wizard-finish-btn").off('click').on('click', (e) => {
            e.preventDefault();
            e.stopImmediatePropagation();
            if (maxiaRRule.verwerkRRule(true)) {
                if (this.validateForm()) {
                    $("#dataform").trigger('submit');
                }
            }
        });

        $('.wizard').on("leaveStep", function(e, anchorObject, currentStepIndex, nextStepIndex, stepDirection) {
            if (nextStepIndex == 3) {
                maxiaRRule.broadcastChange();
            }
            return maxiaRRule.checkBeschikbaarheid(e, anchorObject, currentStepIndex, nextStepIndex, stepDirection);
         });
    },
    validateForm: function() {
        let isValid = true;
        if ($('#preview-datums-toggler')[0].checked) {
            if ($('#preview-datums span.btn').length > 0 && $('#preview-datums span.btn-default').length === 0) {
                Polaris.Base.errorDialog('U kunt niet alle datums verwijderen uit de reservering.', 'Laat minimaal één datum actief.');
                isValid = false;
            }
        }
        return isValid;
    },
    initialiseerGeneralInputs: function() {
        const self = this;
        const today = luxon.DateTime.now();

        const appendOptions = (selector, options) => {
            const fragment = document.createDocumentFragment();
            options.forEach(({ key, value }) => {
                const optionElem = new Option(value, key, false, false);
                fragment.appendChild(optionElem);
            });
            $(selector).append(fragment);
        };

        this.schematypes.forEach(schematype => {
            const startSelector = `#_fldSCHEMA_START_DATUM_${schematype}`;
            const endSelector = `#_fldSCHEMA_EIND_DATUM_${schematype}`;
            const timeRowSelector = `#_rowSCHEMA_AANGEPASTE_TIJD_${schematype}`;

            // Initialize date values
            $(startSelector).val(prevVal => prevVal || today.toFormat('dd-MM-yyyy'));
            $(endSelector).val(prevVal => {
                if (prevVal) return prevVal;
                const increment = endSelector === "#_fldSCHEMA_EIND_DATUM_M" ? { months: 1 } : { weeks: 1 };
                return today.plus(increment).toFormat('dd-MM-yyyy');
            });

            // Event listeners for date changes
            $(startSelector).on('change', function(e) {
                const currentVal = $(this).val();
                self.schematypes.forEach(innerSchematype => {
                    const innerSelector = `#_fldSCHEMA_START_DATUM_${innerSchematype}`;
                    if (this.id.at(-1) !== innerSchematype) {
                        $(innerSelector).val(currentVal);
                    }
                });
                self.refreshDatumsPreview();

            });

            $(endSelector).on('change', function(e) {
                const currentVal = $(this).val();
                self.schematypes.forEach(innerSchematype => {
                    const innerSelector = `#_fldSCHEMA_EIND_DATUM_${innerSchematype}`;
                    if (this.id.at(-1) !== innerSchematype) {
                        $(innerSelector).val(currentVal);
                    }
                });
                self.refreshDatumsPreview();
            });

            // Event listeners for time changes
            $(timeRowSelector).on('change', ".inputtime", function() {
                const changedValue = $(this).val();
                const isStartInput = $(this).hasClass('start');
                const isEndInput = $(this).hasClass('end');

                self.schematypes.forEach(innerSchematype => {
                    const innerSelector = `#_rowSCHEMA_AANGEPASTE_TIJD_${innerSchematype}`;
                    let targetElem;

                    if (isStartInput) {
                        targetElem = $(innerSelector).find(".inputtime.start");
                    } else if (isEndInput) {
                        targetElem = $(innerSelector).find(".inputtime.end");
                    }

                    if (this.id.at(-1) !== innerSchematype) {
                        targetElem.val(changedValue);
                    }
                });
                self.verwerkRRule();
            });

            // Initialize time pickers
            Polaris.Form.initializeTimePickers(timeRowSelector);

            // Append options
            let eindTijdOpties = [
                { key: 'datum', value: 'tot' },
            ];
            if (this.switchcount) {
                eindTijdOpties.push({ key: 'aantalkeer', value: 'aantal' });
            }
            appendOptions(`#_fldSCHEMA_EINDE_${schematype}`, eindTijdOpties);

            // Event listener for end time option change
            $(`#_fldSCHEMA_EINDE_${schematype}`).on('change', function() {
                const isAantalKeer = this.value === 'aantalkeer';
                $(`#_rowSCHEMA_AANTALKEER_${schematype}`).toggle(isAantalKeer);
                $(`#_rowSCHEMA_EINDDATUM_${schematype}`).toggle(!isAantalKeer);
                const aantalElem = $(`#_fldSCHEMA_EINDE_AANTALKEER_${schematype}`);
                if (!aantalElem.val()) aantalElem.val(5);
            }).trigger('change');
        });

        // Other event listeners outside the loop

        // ???
        // $("#_fldSCHEMA_EINDTIJD_E,#_fldSCHEMA_EINDTIJD_D,#_fldSCHEMA_EINDTIJD_W,#_fldSCHEMA_EINDTIJD_M").on('change', function() {
        //     const val = $(this).val();
        //     if (val) $("#_fldSCHEMA_DAGDEEL").val(val);
        // });
    },
    broadcastChange: function() {
        $(document).trigger("maxia.rrule.changed");
    },
    initialiseerWeek: function() {
        const $weekdagButtons = $(".weekdagselectie button.wday");
        const $weekenddayButtons = $(".weekdagselectie button.weekendday");
        const $werkweekButton = $(".weekdagselectie button.ww");

        $(".weekdagselectie").on('click', 'button', function(e) {
            const $clickedButton = $(this);

            if ($clickedButton.hasClass('wday') || $clickedButton.hasClass('weekendday')) {
                $clickedButton.toggleClass('active');
            } else if ($clickedButton.is($werkweekButton)) {
                if ($weekdagButtons.filter('.active').length === 5) {
                    $weekdagButtons.removeClass('active');
                } else {
                    $weekdagButtons.addClass('active');
                }
                $weekenddayButtons.removeClass('active');
            }

            $("#_fldWEEK_SELECTIE_VALIDATE").val($(".weekdagselectie button.active").length > 0 ? 'valid' : '');
            maxiaRRule.verwerkRRule();
            maxiaRRule.refreshDatumsPreview();
        });

        $("#_fldSCHEMA_PERIODIEK").on('change', function() {
            maxiaRRule.verwerkRRule();
            maxiaRRule.refreshDatumsPreview();
        });
    },
    initialiseerMaand: function () {
        $(".maandselectie button.maand").on('click', function(e) {
            $(this).toggleClass('active');
            const heeftMaandGekozen = maxiaRRule.updateDagSelect();
            $("#_fldMAAND_SELECTIE_VALIDATE").val(heeftMaandGekozen ? 'valid' : '');
            maxiaRRule.verwerkRRule();
            maxiaRRule.refreshDatumsPreview();
        });
        $('#_fldSCHEMA_MAAND_START_DAG').on('change', function() {
            maxiaRRule.verwerkRRule();
            maxiaRRule.refreshDatumsPreview();
        });
    },
    initialiseerPreviewKalender: function() {
        maxiaRRule.previewcal = new FullCalendar.Calendar(document.getElementById('previewcalendar'), {
            schedulerLicenseKey: '0445008747-fcs-1681420837',
            initialView: 'dayGridMonth',
            headerToolbar: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            events: [],
        });
        maxiaRRule.previewcal.render();
    },
    updatePreviewKalender: function(dynamicEvents) {
        if (maxiaRRule.previewcal) {
            maxiaRRule.previewcal('removeEvents'); // Clear existing events
            maxiaRRule.previewcal('addEventSource', dynamicEvents); // Add new events
        }
    },
    initialiseerSchemaTabs: function () {
        $('#fldFrequentie a').on('click', function (e) {
            e.preventDefault();

            // Only make the current schema type active and show the current tab
            $('#fldFrequentie a').removeClass('active');
            $(this).addClass('active');
            $(this).tab('show');

            maxiaRRule.verwerkRRule();
            // maxiaRRule.toonRRule();
            maxiaRRule.refreshDatumsPreview();
        });
    },
    initialiseerExDatumsComponent: function() {
        // Initialiseer de preview kalender
        $("#preview-datums-container").on('ifToggled', function(e) {
            e.preventDefault();
            var isChecked = e.target.checked;
            if (isChecked) {
                maxiaRRule.verwerkRRule();
                maxiaRRule.refreshDatumsPreview();
            }
            // Toggle the preview-datums container
            $("#preview-datums").toggle(isChecked);

            // If unchecked clear the EX_DATUMS field
            if (!isChecked) {
                $("#_fldEX_DATUMS").val('');
            }
        });
        if (!isNullOrEmpty($("#_fldEX_DATUMS").val())) {
            $('#preview-datums-toggler').iCheck('check');
            $("#preview-datums").show();
            this.scrollToFirstDanger();
        } else {
            $('#preview-datums-toggler').iCheck('uncheck');
            $("#preview-datums").hide();
        }
        Polaris.Base.initializeIChecks($('#preview-datums-container'));
    },
    initialiseerDatumsPreview: function() {
        $("#preview-datums span.btn").on('click', function (e) {
            e.preventDefault();
            const $clickedButton = $(this);

            // Toggle button class
            $clickedButton.toggleClass('btn-default btn-danger');

            // Retrieve excluded dates and update the EX_DATUMS field
            const excludedDates = $("#preview-datums span.btn-danger").map(function() {
                return $(this).data('datum');
            }).get();

            // Update the EX_DATUMS field
            $("#_fldEX_DATUMS").val(excludedDates.join(','));
        });
    },
    scrollToFirstDanger: function() {
        const $firstDanger = $("#preview-datums .btn-danger:first");
        if ($firstDanger.length > 0) {
            const scrollTop = $firstDanger.offset().top - $("#preview-datums").offset().top + $("#preview-datums").scrollTop();
            $("#preview-datums").animate({ scrollTop: scrollTop }, 'fast');
        }
    },
    checkBeschikbaarheid: function(e, anchorObject, currentStepIndex, nextStepIndex, stepDirection) {
        if (currentStepIndex == 0 && stepDirection == 'forward' && maxiaRRule.ajaxInvoke == false) {
            maxiaRRule.verwerkRRule();
            maxiaRRule.ajaxInvoke = true;
            $.getJSON(_servicequery, {
                'event': 'checkbeschikbaarheid'
                , 'RRULE': $("#_fldCOMPONENTRRULE").val()
                , 'RRULE_DUUR': $("#_fldRRULE_DUUR").val()
                , 'RUIMTE': $("#_fldRUIMTE").val()
            })
            .done(function(data) {
                if (data.result == true) {
                    $('.wizard').smartWizard("next");
                } else {
                    Polaris.Base.modalDialog(data.error, { close: function() {}, type: 'type-danger' })
                }
            })
            .fail(function() {
                console.error('De ruimtes konden niet geladen worden.');
            })
            .always(function() {
                maxiaRRule.ajaxInvoke = false;
            });

            return false;
        } else {
            maxiaRRule.ajaxInvoke = false;
            return true;
        };
    },
    /**
     * Geeft de som van alle invoerwaarden in een groep.
     * @param {jQuery} groep - Een groep van jQuery input-elementen.
     * @return {number} De som van de invoerwaarden.
     */
    verwerkInputs: function (groep) {
        let som = 0;

        groep.each(function() {
            const waarde = parseFloat($(this).val());
            if (!isNaN(waarde)) {
                som += waarde;
            }
        });

        return som;
    },
    converteerWeekdagen: function(group) {
        const result = group.map((index, day) => rrule.RRule[$(day).data('val')]).get();
        return result;
    },
    dagenPerMaand: [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31], // Januari tot December
    minsteDagenInMaanden(maanden) {
        const dagenLijst = maanden.map(maand => this.dagenPerMaand[maand - 1]); // Verminder met 1 omdat arrays 0-geïndexeerd zijn
        return Math.min(...dagenLijst);
    },
    updateDagSelect: function() {
        const dagSelectie = $('#_fldSCHEMA_MAAND_START_DAG');
        const geselecteerdeMaanden = $(".maand.active").map((_, el) => $(el).data('val')).get();
        const huidigeWaarde = dagSelectie.val();

        if (geselecteerdeMaanden.length > 0) {
            dagSelectie.empty();

            const maxDagNummer = this.minsteDagenInMaanden(geselecteerdeMaanden);
            const opties = Array.from({ length: maxDagNummer }, (_, i) => `<option>${i + 1}</option>`);

            dagSelectie.append(opties);

            if (huidigeWaarde && (huidigeWaarde <= maxDagNummer)) {
                dagSelectie.val(huidigeWaarde);
            }

            // dagSelectie.trigger('change');
        }
        return geselecteerdeMaanden.length > 0;
    },
    datumNaarRRuleFormaat: function(datestring, fix_tz) {
        // Define a fallback to the current date and time if the input is invalid
        const now = luxon.DateTime.now();

        // Check if the datestring is not empty or null
        if (!datestring) {
            console.warn('Invalid date string provided. Returning current date and time. ' + datestring);
            return now;
        }

        // Parse date string to Date object with format dd-MM-yyyy
        let parsedDate = luxon.DateTime.fromFormat(datestring, 'dd-MM-yyyy', {
            zone: 'local' // Use local timezone or you can specify a timezone like 'Europe/Amsterdam'
        });

        // Check if the date parsing was successful
        if (!parsedDate.isValid) {
            console.warn('Date parsing failed: ' + parsedDate.invalidExplanation + '. Returning current date and time.');
            return now;
        }

        let result = parsedDate;

        // Apply timezone fix if needed
        if (fix_tz) {
            result = parsedDate.plus({ hours:2 });
        }

        return result;
    },
    getFormattedDate: function(dateobject, fix_tz) {
        let tmpDate = luxon.DateTime.fromJSDate(dateobject)
        if (fix_tz) {
            tmpDate = tmpDate.minus({ hours: 2});
        }
        return tmpDate.toFormat('dd-MM-yyyy');
    },
    convertTijdenNaarDuur: function(starttijd, eindtijd) {
        // Duur in 00:00 formaat
        const [startuur, startminuten] = starttijd.split(':').map(Number);
        const [einduur, eindminuten] = eindtijd.split(':').map(Number);
        return `${String(einduur - startuur).padStart(2, '0')}:${String(eindminuten - startminuten).padStart(2, '0')}`;

    },
    getDuurInMinuten: function(duurinstring) {
        [duur_h, duur_m] = String(duurinstring).split(':').map(Number);
        duur_h = parseInt(duur_h, 10) || 0;
        duur_m = parseInt(duur_m, 10) || 0;

        duur_in_minuten = duur_h * 60 + duur_m;
        return duur_in_minuten;
    },
    vulDagdeel: function(containerclass, uur, minuten, duur) {
        // Get the container and check if it exists
        const containers = $(`${containerclass}`);
        if (!containers.length) {
            console.warn(`Containers with class ${containerclass} not found.`);
            return;
        }
        duur_in_minuten = this.getDuurInMinuten(duur);

        // Create a luxon DateTime object
        const tmpdatum = luxon.DateTime.fromObject({
            year: 2017,
            month: 5,
            day: 15,
            hour: uur,
            minute: minuten
        });
        // Format the start and end times
        const starttijd = tmpdatum.toFormat('HH:mm');
        const eindtijd = tmpdatum.plus({ minutes: duur_in_minuten }).toFormat('HH:mm');

        containers.each(container => {
            // Find the start and end time elements and set their values
            $(".datepair.start", container).val(starttijd);
            $(".datepair.end", container).val(eindtijd);
        });
    },
    toonRRule: function() {
        if (!this.schematypes.includes(this.schematype)) {
            alert('Unsupported frequency schema detected. Supported schema types are: E, D, W, M');
        }
        if (!this.duur) {
            this.duur = $("#_fldRRULE_DUUR").val();
        }

        // Only make the current schema type active and show the current tab
        $('#fldFrequentie a').removeClass('active');
        $(`#fldFrequentie a.schema_${this.schematype}`).addClass('active').tab('show');

        // Clean and add required class from (in)active tabs inputs fields
        $(".componentline :input").removeClass('required').removeAttr('required');
        $("#tab" + this.schematype + " input[type=text],#tab" + this.schematype + " select").addClass('required').attr('required', true);

        // Vul de dagdeel velden in
        this.vulDagdeel('.aangepastetijd', this.rrule.options.byhour[0], this.rrule.options.byminute[0], this.duur);

        // Vul de start/einddatum in
        $(`#_fldSCHEMA_START_DATUM_${this.schematype}`).datepicker('setDate', this.getFormattedDate(this.rrule.options.dtstart));
        $(`#_fldSCHEMA_EIND_DATUM_${this.schematype}`).datepicker('setDate', this.getFormattedDate(this.rrule.options.until, true));

        switch (this.schematype) {
            case 'E':
                break;
            case 'D':
                if (this.switchcount && this.rrule.options.count) {
                    $("#_fldSCHEMA_DAGELIJKS_EINDE_AANTALKEER_D").val(this.rrule.options.count);
                }
                break;
            case 'W':
                this.rrule.options.byweekday.forEach(dag => {
                    const dagAfkorting = new rrule.Weekday(dag).toString();
                    $(`#_fldWEEKDAGSELECTIE button[data-val='${dagAfkorting}']`).addClass('active');
                });
                $("#_fldWEEK_SELECTIE_VALIDATE").val(this.rrule.options.byweekday.length > 0 ? 'valid' : '');
                $("#_fldSCHEMA_PERIODIEK").val(this.rrule.options.interval).trigger('change');
                break;
            case 'M':
                if (this.rrule.options.bymonth) {
                    this.rrule.options.bymonth.forEach(month => {
                        $(`#_fldMAANDSELECTIE button[data-val='${month}']`).addClass('active');
                    });
                }
                const heeftMaandGekozen = this.updateDagSelect();
                $("#_fldMAAND_SELECTIE_VALIDATE").val(heeftMaandGekozen ? 'valid' : '');
                $("#_fldSCHEMA_MAAND_START_DAG").val(this.rrule.options.bymonthday);
                break;
        }
    },
    stelRegelOptiesIn: function(rule) {
        rule.options.byhour = [0];
        rule.options.byminute = [0];
        rule.options.bysecond = [0];
        rule.options.bymonth = null;
        rule.options.bymonthday = null;
        rule.options.byweekday = null;
        rule.options.count = null;
    },
    verwerkEindeOpties: function(eindeTypeSelector, eindeDatumSelector, eindeAantalSelector) {
        if ($(eindeTypeSelector).val() == 'datum' && $(eindeDatumSelector).val() != '') {
            let einddatum = this.datumNaarRRuleFormaat($(eindeDatumSelector).val(), true);
            return einddatum.toJSDate();
        } else {
            return $(eindeAantalSelector).val() || 1;
        }
    },
    tijdNaarRRuleFormaat: function(containerid) {
        // Get the container and check if it exists
        const container = $(`#${containerid}`);
        if (!container.length) {
            console.warn(`Container with ID ${containerid} not found.`);
            return null;
        }

        // Find the start and end time elements
        const starttijdelem = $(".datepair.start", container);
        const eindtijdelem = $(".datepair.end", container);

        // Split the values from the input elements
        const [uur_start, minuten_start] = starttijdelem.val().split(':').map(Number);
        const [uur_eind, minuten_eind] = eindtijdelem.val().split(':').map(Number);

        // Create luxon DateTime objects for start and end times
        const startdatum = luxon.DateTime.fromObject({
            year: 2017,
            month: 5,
            day: 15,
            hour: uur_start,
            minute: minuten_start
        });
        const einddatum = luxon.DateTime.fromObject({
            year: 2017,
            month: 5,
            day: 15,
            hour: uur_eind,
            minute: minuten_eind
        });

        // Calculate the duration in hours
        diff_object = einddatum.diff(startdatum, ['hours', 'minutes']);
        // Format the duration to ensure it's in HH:mm format
        const duur_uren = String(diff_object.hours).padStart(2, '0');
        const duur_minuten = String(diff_object.minutes).padStart(2, '0');
        const duur = `${duur_uren}:${duur_minuten}`;
        return [starttijdelem.val(), uur_start, minuten_start, eindtijdelem.val(), duur];
    },
    refreshDatumsPreview: function() {
        let checkbox = $("#preview-datums-container .icheck");
        if (this.schematype == 'E') {
            checkbox.prop('disabled', true).prop('checked', false).iCheck('update');
            $("#preview-datums-container label").addClass('text-muted');
            $("#preview-datums").hide();
        } else {
            checkbox.prop('disabled', false).iCheck('update');
            $("#preview-datums-container label").removeClass('text-muted');
            $("#preview-datums").toggle(checkbox.prop('checked'));
        }

        if (!isNullOrEmpty(this.rrulevalue)) {
            let testrule = rrule.RRule.fromString(this.rrulevalue);
            // loop through all the dates and store them in an array in the format yyyy-mm-dd, with a status of 'available'
            let res_datums = testrule.all().map(date => {
                return {
                    status: 'included',
                    datum: date.toISOString().split('T')[0],
                }
            });
            const exclude_datums = $("#_fldEX_DATUMS").val();
            if (!isNullOrEmpty(exclude_datums)) {
                // loop through events array and check if the date is in the exclude_datums array and set the status to 'Niet beschikbaar'
                res_datums.forEach(event => {
                    if (exclude_datums.includes(event.datum)) {
                        event.status = 'excluded';
                    }
                });
            }
            $("#preview-datums").empty();
            // put res_datums in a nicely formatted div with ID fldEX_DATUMS
            // divide the dates in three columns
            res_datums.forEach(event => {
                let statusClass = event.status === 'included' ? 'btn-default' : 'btn-danger';
                // display the datum in dd-mm-yyyy format
                let prettydatum = event.datum.split('-').reverse().join('-');
                $("#preview-datums").append(`<span class="btn ${statusClass}" data-datum="${event.datum}" data-stat="${event.status}">${prettydatum}</span>`);
            });
            this.initialiseerDatumsPreview();
        }
    },
    verwerkRRule: function(validate = false) {
        this.schematype = $("#fldFrequentie .active").data('schema');
        this.stelRegelOptiesIn(this.rrule);
        this.rrule.options.dtstart = this.datumNaarRRuleFormaat($("#_fldSCHEMA_START_DATUM_" + this.schematype).val(), true).toJSDate();

        [tijd_start, uur_start, minuten_start, tijd_eind, duur] = this.tijdNaarRRuleFormaat('_rowSCHEMA_DAGDEEL_' + this.schematype);
        this.rrule.options.byhour = [uur_start];
        this.rrule.options.byminute = [minuten_start];
        this.vulDagdeel('.aangepastetijd', this.rrule.options.byhour[0], this.rrule.options.byminute[0], duur);
        $('#_fldRRULE_DUUR').val(duur);

        if (this.switchcount && $(`#_fldSCHEMA_EINDE_${this.schematype}`).val() === 'aantalkeer') {
            this.rrule.options.count = $(`#_fldSCHEMA_EINDE_AANTALKEER_${this.schematype}`).val();
        } else {
            if (this.schematype !== 'E') {
                let untildate = this.datumNaarRRuleFormaat($(`#_fldSCHEMA_EIND_DATUM_${this.schematype}`).val(), true);
                this.rrule.options.until = untildate.toJSDate();
                if (this.rrule.options.until.toString() == 'Invalid Date' || this.rrule.options.until < this.rrule.options.dtstart) {
                    this.rrule.options.until = this.rrule.options.dtstart;
                }
            }
        }

        switch (this.schematype) {
            case 'E':
                this.rrule.options.freq = rrule.RRule.DAILY;
                this.rrule.options.count = 1;
                this.rrule.options.until = null;
                this.rrule.options.interval = 1;
                break;

            case 'D':
                this.rrule.options.freq = rrule.RRule.DAILY;
                this.rrule.options.interval = 1;
                break;

            case 'W':
                this.rrule.options.freq = rrule.RRule.WEEKLY;
                this.rrule.options.byweekday = this.converteerWeekdagen($("#_fldWEEKDAGSELECTIE button[data-val].active"));
                this.rrule.options.interval = parseInt($("#_fldSCHEMA_PERIODIEK").val());
                break;

            case 'M':
                this.rrule.options.freq = rrule.RRule.MONTHLY;
                this.rrule.options.interval = 1;
                this.rrule.options.bymonth = $("#_fldMAANDSELECTIE button[data-val].active").map((_, el) => $(el).data('val')).get();
                this.rrule.options.bymonthday = $("#_fldSCHEMA_MAAND_START_DAG").val();
                break;
        }

        // Store the RRule object as a string
        this.rrulevalue = rrule.RRule.optionsToString(this.rrule.options);
        $("#_fldCOMPONENTRRULE").val(this.rrulevalue).trigger('blur');

        return true;
    },

};

jQuery(function() {
    maxiaRRule.initialize();
});