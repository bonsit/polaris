maxiaSchema = {
    getSmallestDayNumber: function (months) {
        const daysInMonth = {
            1: 31,  // January
            2: 28,  // February
            3: 31,  // March
            4: 30,  // April
            5: 31,  // May
            6: 30,  // June
            7: 31,  // July
            8: 31,  // August
            9: 30,  // September
            10: 31, // October
            11: 30, // November
            12: 31  // December
        };

        let largestDay = 31;
        for (const month of months) {
            const days = daysInMonth[month];
            if (days < largestDay) {
                largestDay = days;
            }
        }

        return largestDay;
    },
    powerOfTwoDecomposition: function (value) {
        const powers = [];

        while (value > 0) {
            const power = value & -value; // Get the lowest set bit using bitwise AND
            powers.push(power);
            value -= power;
        }

        return powers;
    },
    summarizeInputs: function (group) {
        var sum = 0;

        $(group).each(function () {
            var value = parseFloat($(this).val());

            if (!isNaN(value)) {
                sum += value;
            }
        });

        return sum;
    },
    updateDagSelect: function () {
        var sel = $('#_fldSCHEMA_MAAND_START_DAG');

        var months = [];
        $(".maand:checked").each(function() {
            months.push($(this).val());
        });

        var currentValue = sel.val();
        // Clear any existing options
        sel.empty();

        var maxDay = maxiaSchema.getSmallestDayNumber(months);
        // Loop through the options array and create option elements
        for (var i = 1; i <= maxDay; i++) {
            var option = $("<option></option>").text(i);
            sel.append(option);
        }
        if (currentValue && (currentValue <= maxDay))
            sel.val(currentValue);
        sel.trigger('change');
    },
    initializeDefaultButtons: function () {
        $("#dataform").on('submit', function (e) {
            e.preventDefault();
            console.log('extra save');
        });
    },
    initializeGeneralInputs: function () {
        let today = Polaris.Base.dateToString(new Date());
        $("#_fldSCHEMA_DAGELIJKS_START_DATUM,#_fldSCHEMA_WEEK_START_DATUM,#_fldSCHEMA_MAANDELIJKS_START_DATUM,#_fldSCHEMA_EENMALIG_START_DATUM").each(function() {
            if ($(this).val() == '') {
                $(this).val(today);
            }
        });

        $("#_fldSCHEMA_START_TIJD_D,#_fldSCHEMA_START_TIJD_W,#_fldSCHEMA_START_TIJD_M,#_fldSCHEMA_START_TIJD_E").on('change', function () {
            if ($(this).val()) {
                $("#_fldSCHEMA_START_TIJD").val($(this).val());
            }
        });

        // Add the options
        let dagdelen = [
            {key:'ochtend',value:'Ochtend'},
            {key:'middag',value:'Middag'},
            {key:'heledag',value:'Hele dag'},
            // {key:'aangepast',value:'Aangepast...'}
        ];
        $(dagdelen).each(function(index, elem) {
            let elemOption = new Option(elem.value, elem.key, false, false);
            $("#_fldSCHEMA_START_TIJD_D,#_fldSCHEMA_START_TIJD_W,#_fldSCHEMA_START_TIJD_E").append(elemOption);
        })

        // Add the month options
        dagdelen = [
            {key:'ochtend',value:'Ochtend'},
            {key:'middag',value:'Middag'},
            {key:'heledag',value:'Hele dag'},
            {key:'tweedagen',value:'Twee dagen'},
            {key:'driedagen',value:'Drie dagen'},
            {key:'vierdagen',value:'Vier dagen'},
            {key:'vijfdagen',value:'Vijf dagen'},
            {key:'eenweek',value:'Een week'},
            {key:'tweeweken',value:'Twee weken'},
            {key:'drieweken',value:'Drie weken'},
            {key:'vierweken',value:'Vier weken'}
        ];
        $(dagdelen).each(function(index, elem) {
            let elemOption = new Option(elem.value, elem.key, false, false);
            $("#_fldSCHEMA_START_TIJD_M").append(elemOption);
        })

        // Store start time in all the start time fields
        $("#_fldSCHEMA_START_TIJD_D,#_fldSCHEMA_START_TIJD_W,#_fldSCHEMA_START_TIJD_M,#_fldSCHEMA_START_TIJD_E").val($("#_fldSCHEMA_START_TIJD").val()).trigger('change');
    },
    initializeSchemaTabs: function () {
        $('#fldFrequentie a').on('click', function (e) {
            e.preventDefault();
            // Store value
            var schemaType = $(this).data('schema');
            $("#_fldSCHEMA_TYPE").val(schemaType);

            // Only make the current schema type active and show the current tab
            $('#fldFrequentie a').removeClass('active');
            $(this).addClass('active');
            $(this).tab('show');

            // Remove required class from inactive tabs inputs fields
            $(".componentline input").removeClass('required');

            // Make inputs fields for current tab required
            var tabID = $(this).attr('href').substring(1);
            $("#" + tabID + " input[type=text],#" + tabID + " select").addClass('required').attr('required', true);

            // Make sure the Weekly startdate field is filtered out from validation
            // because the initial Week period is every week (with no startdate)
            $('#_fldSCHEMA_PERIODIEK').trigger('change');
        });

        var schemaType = $("#_fldSCHEMA_TYPE").val();
        $('#fldFrequentie a.schema_' + schemaType).toggleClass('active').tab('show');
        $("#tab" + schemaType + " input[type=text]").addClass('required').attr('required', true);

        switch (schemaType) {
            case 'D':
                break;
            case 'M':
                $(".maand").iCheck('uncheck');
                maanden = maxiaSchema.powerOfTwoDecomposition(parseInt($("#_fldSCHEMA_MAAND_HERHALING").val()));
                $('.maandblock input').filter(function (index) {
                    return maanden.includes(parseInt($(this).val()));
                }).iCheck('check');
                break;
            case 'W':
                $(".weekdag").iCheck('uncheck');
                dagen = maxiaSchema.powerOfTwoDecomposition(parseInt($("#_fldSCHEMA_WEEK_HERHALING").val()));
                $('.weekdagblock input').filter(function (index) {
                    return dagen.includes(parseInt($(this).val()));
                }).iCheck('check');
                break;
            case 'E':
                /*...*/
                break;
            default:
                break;
        }
    },
    updateElkeDag: function () {
        $("#_fldELKEDAG").iCheck($(".weekdag:checked").length == 7 ? 'check' : 'uncheck');
    },
    updateElkeMaand: function () {
        $("#_fldELKEMAAND").iCheck($(".maand:checked").length == 12 ? 'check' : 'uncheck');
    },
    updateWekelijks: function() {
    },
    initializeWeek: function () {
        /* WEEK functions */
        $("#_fldELKEDAG").on("ifClicked", function (e) {
            /* This is strange code. The ! is opposite of what you expect  */
            if (!$(this).is(':checked')) {
                $("#_fldSCHEMA_WEEK_HERHALING").val(maxiaSchema.summarizeInputs('.weekdag'));
                $(".weekdag").iCheck('check');
            } else {
                $("#_fldSCHEMA_WEEK_HERHALING").val(0);
                $(".weekdag").iCheck('uncheck');
            }
        });

        $(".weekdag").on("ifClicked", function (e) {
            setTimeout(function () {
                $("#_fldSCHEMA_WEEK_HERHALING").val(maxiaSchema.summarizeInputs('.weekdag:checked'));
                maxiaSchema.updateElkeDag();
            }, 20);
        });

        $("#_fldSCHEMA_PERIODIEK ").val($("#_fldSCHEMA_PERIODIEK").data('value'));
        if ($('#_fldSCHEMA_PERIODIEK').val() === null) {
            $('#_fldSCHEMA_PERIODIEK').val('1').trigger('change');
        }

        maxiaSchema.updateElkeDag();
    },
    initializeMaand: function () {
        /* MAAND functions */
        $("#_fldELKEMAAND").on("ifClicked", function (e) {
            /* This is strange code. The ! is opposite of what you expect  */
            if (!$(this).is(':checked')) {
                $("#_fldSCHEMA_MAAND_HERHALING").val(maxiaSchema.summarizeInputs('.maand'));
                $(".maand").iCheck('check');
            } else {
                $("#_fldSCHEMA_MAAND_HERHALING").val(0);
                $(".maand").iCheck('uncheck');
            }
        });

        $(".maand").on("ifClicked", function (e) {
            setTimeout(function () {
                $("#_fldSCHEMA_MAAND_HERHALING").val(maxiaSchema.summarizeInputs('.maand:checked'));
                maxiaSchema.updateElkeMaand();
                maxiaSchema.updateDagSelect();
            }, 20);
        });

        maxiaSchema.updateElkeMaand();
        maxiaSchema.updateDagSelect();
        $("#_fldSCHEMA_MAAND_START_DAG ").val($("#_fldSCHEMA_MAAND_START_DAG ").data('value')).trigger('change');
    }
};

jQuery(function() {
    maxiaSchema.initializeDefaultButtons();
    maxiaSchema.initializeSchemaTabs();
    maxiaSchema.initializeGeneralInputs();
    maxiaSchema.initializeMaand();
    maxiaSchema.initializeWeek();
});