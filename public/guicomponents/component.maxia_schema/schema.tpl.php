<div class="row">
    <div class="col-md-2">
        <!-- List group -->
        <div class="list-group" id="fldFrequentie" role="tablist">
            <a class="list-group-item list-group-item-action schema_D" data-toggle="list" href="#tabD" data-schema="D" role="tab">Dagelijks</a>
            <a class="list-group-item list-group-item-action schema_W" data-toggle="list" href="#tabW" data-schema="W" role="tab">Wekelijks</a>
            <a class="list-group-item list-group-item-action schema_M" data-toggle="list" href="#tabM" data-schema="M" role="tab">Maandelijks</a>
            <a class="list-group-item list-group-item-action schema_E" data-toggle="list" href="#tabE" data-schema="E" role="tab">Eenmalig</a>
        </div>
    </div>
    <div class="col-md-10">
        <!-- Tab panes -->
        <div class="tab-content">
            <div class="tab-pane active" id="tabD" role="tabpanel">

                <div class="row">
                    <div class="col-sm-2">
                        <b>Vanaf wanneer</b>
                    </div>
                    <div class="col-sm-10 form-group form-group-sm">
                        <div id="_rowSCHEMA_DAGELIJKS_START_DATUM">
                            <div class="input-group">
                                <input type="text" id="_fldSCHEMA_DAGELIJKS_START_DATUM" name="SCHEMA_DAGELIJKS_START_DATUM" class="text date_input  " value="{SCHEMA_DAGELIJKS_START_DATUM}" size="11">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-2">
                        <b>Dagdeel&nbsp;</b>
                    </div>
                    <div class="col-sm-10 form-group form-group-sm">
                        <div id="_rowSCHEMA_START_TIJD_DAGELIJKS">
                            <div class="input-group">
                                <select class="select2" id="_fldSCHEMA_START_TIJD_D"></select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="tabW" role="tabpanel">

                <div class="row" id="_fldWEEKWANNEERSTARTEN">
                    <div class="col-sm-2">
                        <b>Vanaf wanneer</b>
                    </div>
                    <div class="col-sm-10 form-group form-group-sm">
                        <input type="text" id="_fldSCHEMA_WEEK_START_DATUM" class="date_input" name="SCHEMA_WEEK_START_DATUM" value="{SCHEMA_WEEK_START_DATUM}" size="11" />
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-2">
                        <b>Frequentie</b>
                    </div>
                    <div class="col-sm-10 form-group form-group-sm">
                        <select class="select2" data-allow-clear="false" id="_fldSCHEMA_PERIODIEK" name="SCHEMA_PERIODIEK" data-value="{SCHEMA_PERIODIEK}">
                            <option value="1">Elke week</option>
                            <option value="2">Om de twee weken</option>
                            <option value="3">Om de drie weken</option>
                        </select>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-2">
                        <b>Welke dagen</b>
                    </div>
                    <div class="col-sm-10 form-group form-group-sm">
                        <div class="row">
                            <div class="col-sm-12 form-group form-group-sm">
                                <label>
                                    <input type="checkbox" class="icheck" id="_fldELKEDAG" value="255"> Elke dag
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 form-group form-group-sm">
                                <label class="weekdagblock">
                                    <input type="checkbox" class="icheck weekdag" id="_fldMAANDAG" value="1"> Maandag
                                </label>
                                <label class="weekdagblock">
                                    <input type="checkbox" class="icheck weekdag" id="_fldDINSDAG" value="2"> Dinsdag
                                </label>
                                <label class="weekdagblock">
                                    <input type="checkbox" class="icheck weekdag" id="_fldWOENDAG" value="4"> Woensdag
                                </label>
                                <label class="weekdagblock">
                                    <input type="checkbox" class="icheck weekdag" id="_fldDONDERDAG" value="8"> Donderdag
                                </label>
                                <label class="weekdagblock">
                                    <input type="checkbox" class="icheck weekdag" id="_fldVRIJDAG" value="16"> Vrijdag
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 form-group form-group-sm">
                                <label class="weekdagblock">
                                    <input type="checkbox" class="icheck weekdag" id="_fldZATERDAG" value="32"> Zaterdag
                                </label>
                                <label class="xcheckbox-inline weekdagblock">
                                    <input type="checkbox" class="icheck weekdag" id="_fldZONDAG" value="64"> Zondag
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-2">
                        <b>Dagdeel</b>
                    </div>
                    <div class="col-sm-10 form-group form-group-sm">
                        <div id="_rowSCHEMA_START_TIJD">
                            <div class="input-group">
                                <select class="select2" id="_fldSCHEMA_START_TIJD_W"></select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="tabM" role="tabpanel">
                <div class="row">

                    <div class="row" id="_fldMAANDWANNEERSTARTEN">
                        <div class="col-sm-2">
                            <b>Vanaf wanneer</b>
                        </div>
                        <div class="col-sm-10 form-group form-group-sm">
                            <input type="text" id="_fldSCHEMA_MAANDELIJKS_START_DATUM" class="date_input" name="SCHEMA_MAANDELIJKS_START_DATUM" value="{SCHEMA_MAANDELIJKS_START_DATUM}" size="11" />
                        </div>
                    </div>

                    <div class="col-sm-2">
                        <b>Welke maanden</b>
                    </div>
                    <div class="col-sm-10 form-group form-group-sm">
                        <div class="row">
                            <div class="col-sm-12 form-group form-group-sm">
                                <label>
                                    <input type="checkbox" class="icheck" value="" id="_fldELKEMAAND"> Elke maand
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 form-group form-group-sm">
                                <label class="maandblock">
                                    <input type="checkbox" class="icheck maand" value="1" id="_fldJAN"> Januari
                                </label>
                                <label class="maandblock">
                                    <input type="checkbox" class="icheck maand" value="2" id="_fldFEB"> Februari
                                </label>
                                <label class="maandblock">
                                    <input type="checkbox" class="icheck maand" value="4" id="_fldMRT"> Maart
                                </label>
                                <label class="maandblock">
                                    <input type="checkbox" class="icheck maand" value="8" id="_fldAPR"> April
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 form-group form-group-sm">
                                <label class="maandblock">
                                    <input type="checkbox" class="icheck maand" value="16" id="_fldMEI"> Mei
                                </label>
                                <label class="maandblock">
                                    <input type="checkbox" class="icheck maand" value="32" id="_fldJUN"> Juni
                                </label>
                                <label class="maandblock">
                                    <input type="checkbox" class="icheck maand" value="64" id="_fldJUL"> Juli
                                </label>
                                <label class="maandblock">
                                    <input type="checkbox" class="icheck maand" value="128" id="_fldAUG"> Augustus
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <label class="maandblock">
                                    <input type="checkbox" class="icheck maand" value="256" id="_fldSEP"> September
                                </label>
                                <label class="maandblock">
                                    <input type="checkbox" class="icheck maand" value="512" id="_fldOKT"> Oktober
                                </label>
                                <label class="maandblock">
                                    <input type="checkbox" class="icheck maand" value="1024" id="_fldNOV"> November
                                </label>
                                <label class="maandblock">
                                    <input type="checkbox" class="icheck maand" value="2048" id="_fldDEV"> December
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <input type="text" name="SCHEMA_MAAND_HERHALING" id="_fldSCHEMA_MAAND_HERHALING" value="{SCHEMA_MAAND_HERHALING}"> -->
                <div class="row">
                    <div class="col-sm-2">
                        <b>Op dag</b>
                    </div>
                    <div class="col-sm-10 form-group form-group-sm">
                        <div id="_rowSCHEMA_MAAND_START_DAG" class="">
                            <div class="input-group">
                                <select class="select2" size="2" name="SCHEMA_MAAND_START_DAG" id="_fldSCHEMA_MAAND_START_DAG" data-value="{SCHEMA_MAAND_START_DAG}"></select>
                                <!-- <input type="text" label="Schema_start_dag" class="text validation_allchars" id="_fldSCHEMA_MAAND_START_DAG" size="10" maxlength="10" value="{SCHEMA_MAAND_START_DAG}"> -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-2">
                        <b>Periode</b>
                    </div>
                    <div class="col-sm-10 form-group form-group-sm">
                        <div id="_rowSCHEMA_START_TIJD_MAANDELIJKS" class="">
                            <div class="input-group">
                                <select class="select2" id="_fldSCHEMA_START_TIJD_M"></select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="tabE" role="tabpanel">

                <div class="row">
                    <div class="col-sm-2">
                        <b>Eenmalig op</b>
                    </div>
                    <div class="col-sm-10 form-group form-group-sm">
                        <div id="_rowSCHEMA_EENMALIG_START_DATUM">
                            <div class="input-group">
                                <input type="text" id="_fldSCHEMA_EENMALIG_START_DATUM" name="SCHEMA_EENMALIG_START_DATUM" class="text date_input" value="{SCHEMA_EENMALIG_START_DATUM}" size="11">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-2">
                        <b>Dagdeel</b>
                    </div>
                    <div class="col-sm-10 form-group form-group-sm">
                        <div id="_rowSCHEMA_START_TIJD">
                            <div class="input-group">
                                <select class="select2" id="_fldSCHEMA_START_TIJD_E"></select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<style>
    .maandblock {width:120px;}
</style>