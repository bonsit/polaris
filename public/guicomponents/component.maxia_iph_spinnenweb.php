<?php
require_once 'guicomponents/component._base.php';

/***
* Shows a IPH Spinnenweb component
**/
class maxia_iph_spinnenwebComponent extends _BaseComponent {

    function __construct($owner=false) {
        parent::__construct($owner);
    }

    function GenerateXHTML($params) {
        parent::GenerateXHTML($params);

        $_fields = [
            'DEELNEMER_ID',
            'DATUM',
            'INGEVULD_DOOR',
            'CLIENT_HASH'
        ];

        $_sql = "
        SELECT *
        FROM vw_do_positievegezondheid
        WHERE DEELNEMER_ID = '{$this->rec->DEELNEMER_ID}'
        AND CLIENT_HASH = '{$_SESSION['clienthash']}'
        AND UPPER(INGEVULD_DOOR) LIKE '%DEELNEMER%'
        AND __DELETED = '0'
        ORDER BY DATUM DESC LIMIT 1";

        $_rs = $this->owner->database->userdb->GetRow($_sql);
        $this->smarty->assign('item', $_rs);
        return $this->Fetch('spinnenweb.tpl.html');
    }
}
