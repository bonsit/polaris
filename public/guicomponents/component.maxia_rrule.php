<?php
require_once('guicomponents/component._base.php');

/***
* Shows a RRule component
**/
class maxia_rruleComponent extends _BaseComponent {

    function __construct($owner=false) {
        parent::__construct($owner);
    }

    function render($html, $record, $fields) {
        $_keys = $fields;
        $_values = $fields;
        array_walk($_keys, function(&$_value) {
            $_value = '{'.$_value.'}';
        });
        array_walk($_values, function(&$_value) use ($record) {
            $_value = $record->$_value;
        });
        $html = str_replace($_keys, $_values, $html);
        return $html;
    }

    function GenerateXHTML($params) {
        parent::GenerateXHTML($params);

        // $this->AddCustomJS('/assets/dist/luxon.js');
        $this->AddCustomJS('/assets/dist/rrule.min.js');
        $this->AddComponentJS('maxia_rrule.js');

        $_fields = array(
            'SCHEMA_TYPE',
            'SCHEMA_EENMALIG_START_DATUM',
            'SCHEMA_WEEK_HERHALING',
            'SCHEMA_MAAND_HERHALING',
            'SCHEMA_MAAND_START_DAG',
            'SCHEMA_PERIODIEK',
            'SCHEMA_START_TIJD',
            'SCHEMA_DAGELIJKS_START_DATUM',
            'SCHEMA_WEEK_START_DATUM',
            'SCHEMA_MAANDELIJKS_START_DATUM'
        );

        list($_rruleKolom, $_startDatumKolom, $_eindDatumKolom, $_startTijdKolom, $_eindTijdKolom) = explode(';', $this->componentparams);

        $this->smarty->assign('RRULEKOLOM', $_rruleKolom);
        $this->smarty->assign('RRULE', $this->rec->$_rruleKolom);
        if (!empty($this->rec->EX_DATUMS))
            $_exDatums = explode(',', $this->rec->EX_DATUMS);
        else
            $_exDatums = '';
        $this->smarty->assign('EX_DATUMS', $_exDatums);
        $_html = $this->Fetch('rrule.tpl.html');

        return $this->render($_html, $this->rec, $_fields);
    }
}
