<?php
require_once('guicomponents/component._base.php');
require_once('includes/datelocale.inc.php');

/***
* Shows a Schedule component
**/

class maxia_schemaComponent extends _BaseComponent {
    var $_startyear;
    var $_endyear;
    var $_datelocale;
    var $_currentyear;

    function __construct($owner=false) {
        parent::__construct($owner);

        $this->_datelocale = new DateLocale();
        $this->_datelocale->Set( $_GLOBALS['_locale'] );

        $currentdate = getdate();
        $this->_currentyear = $currentdate['year'];
        $this->_startyear = $this->_currentyear - 5;
        $this->_endyear = $this->_currentyear + 5;
    }

    function GenerateXHTML($params) {
        parent::GenerateXHTML($params);

        $rec = $params[1];
        $columns = explode(';',$params[2]);

        $_r = file_get_contents("guicomponents/wvh_schema.html");
        $_r .= file_get_contents("guicomponents/wvh_schema.js");

        return $_r;
    }
}
