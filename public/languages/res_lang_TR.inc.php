<?
# Polaris designer resources #
$s_polaris = 'Polaris';
$s_pagetitle = 'Polaris - Platform for Webapplications';
$s_polaris_main = 'Polaris Webbased Applications';
$s_polaris_about = 'About Polaris';
$s_polaris_apps = 'Web applications';
$s_polaris_designer = 'Designer';
$s_polaris_system_settings = 'Systeem settings';

# general resources #
$s_yes = 'Yes';
$s_no = 'No';
$s_via = 'Via';
$s_your_location = 'Your location';
$s_welcome = 'Welcome';
$s_welcome_to = 'Welcome to';
$s_all_applications = 'All applications';
$s_your_applications = 'Your applications';
$s_loggedinas = 'Logged in as';
$s_click_to_start = 'Click to start the application';
$s_of = 'of';
$s_items = 'Items';
$s_your_functions = 'Your functions';
$s_go_to = 'Go to';
$s_properties = 'Properties';
$s_step = 'Step';
$s_check_all = 'Check all';
$s_check_none = 'Check none';
$s_functions = 'Functions';
$s_required_fields = 'The following fields are mandatory';
$s_connection_successful = 'Database connection was successful!';
$s_connection_not_successful = 'Database connection failed';
$s_login_message = 'With this login form, system administrator can get access to Polaris (the webbased administration and content management system).';
$s_only_licenced_users_message = 'This webservice is provided for authorised Polaris users.';

# object actions (keep short) #
$s_action = 'action';
$s_edit = 'edit';
$s_view = 'view';
$s_delete = 'delete';
$s_show_users = 'show users';

$s_applications = 'APPLICATIONS';
$s_designer = 'DESIGNER';
$s_ctrlpanel = 'CONFIG';
$s_help = 'HELP';
$s_logout = 'LOGOFF';
$s_login = 'LOGIN';

# modules #
$s_modules = 'Management';
$s_modules_system = 'System Management';
$s_mod_application_manager = 'Application manager';
$s_mod_user_manager = 'User manager';
$s_mod_db_manager = 'Database manager';
$s_mod_backup_restore = 'Backup &amp; Restore';
$s_mod_client_manager = 'Client manager';
$s_mod_system_settings = 'System settings';

# user field resources #
$s_user_account_of = 'User account of';
$s_create_membership = 'Create membership for this member';
$s_create_group_membership = 'Create membership for this group';

$s_username = 'User name';
$s_fullname = 'Full name';
$s_password = 'Password';
$s_description = 'Description';
$s_email_address = 'Email address';
$s_change_password_next_logon = 'Change password with next login';
$s_user_cannot_change_password = 'User cannot change password';
$s_password_never_expires = 'Password never expires';
$s_account_disabled = 'Account disabled';
$s_last_login_date = 'Last login date';
$s_login_count = 'Login count';
$s_client_admin = 'User is Client administrator';
$s_root_admin = 'User is Polaris administrator';
$s_change_password = 'Change password';

$s_groupname = 'Group name';
$s_account = 'Account';
$s_group = 'Group';
$s_type = 'Type';
$s_name = 'Name';
$s_last_login = 'Last login';
$s_action = 'Action';
$s_logins = 'Number of logins';

$s_user_list = 'User list';
$s_client_list = 'Client list';
$s_application_list = 'Application list';
$s_your_applications = 'Your Applications'; 

# client field resources #
$s_client = 'Client';
$s_client_country = 'Country';
$s_client_address = 'Address';
$s_client_zipcode = 'Postal code';
$s_client_telephone = 'Telephone #';

# application field resources #
$s_application = 'Application';
$s_application_description = 'Title';
$s_application_registered = 'Registered to';
$s_application_version = 'Version';

$s_apps_choose_from_list = 'To start an application, please choose one from the list below.';
$s_apps_non_available    = 'You are not autorized for an application, please contact <a href="mailto:info@debster.nl">Debster</a>'; 

$s_test_connection = 'Test connection';
$s_save_settings = 'Save settings';
$s_go = 'GO';

$s_backup_restore = 'Backup/Restore of Polaris database';

# Search form #
$s_search					= 'Search';
$s_search_title  			= 'Search for a';
$s_search_record  		= 'record';
$s_search_column_label  = 'Search in';
$s_search_value_label   = 'Search for value';

# record limit
$s_recordlimit_label = 'Show';
$s_recordlimit_hint = 'Number of records retrieved from the database';
$s_navigate_first = 'first';
$s_navigate_prev = 'previous';
$s_navigate_next = 'next';
$s_navigate_last = 'last';
$s_navigate_first_hint = 'Show first record page';
$s_navigate_prev_hint = 'Show previous record page';
$s_navigate_next_hint = 'Show next record page';
$s_navigate_last_hint = 'Show last record page';

$s_filter_title = 'Filter items';
$s_filter_cancel = 'Show all items (cancel filter)';

# image resources #
$s_image_view = 'view image';
$s_image_view_hint = 'View image in a new window';
$s_image_view_normal_size = 'View in normal size';
$s_image_delete = 'Delete image';
$s_image_add_new = 'Add image...';
$s_image_file_size = 'File size';
$s_image_edit = 'Edit image';
$s_image_edit_hint = '';
$s_image_select_button = 'Image list';
$s_image_select = 'select an image';
$s_image_select_hint = 'Opens list with images in a new window';
$s_image_upload_text = 'You can upload one or more images. Browse for you image with the \'Browse...\' button (max. 5 image per upload) and press Send.';
$s_pick_image = 'Choose an image from the list below.';
$s_no_images = 'There are no images.';
$s_selected_image = 'Selected image'; 
$s_image_preview = 'Preview';
$s_choose_image = 'Choose&nbsp;image'; 
$s_upload_images = 'Upload&nbsp;images';

# item resources #
$s_add_item = 'Add an item';
$s_n_items = 'items';
$s_all_items = 'All items';
$s_add_item_to_list = 'Add an item to this list';
$s_click_and_edit_item = 'Click on an item to change its record.';
$s_click_and_view_item = 'Click on an item to view its record.';
$s_no_item = 'none';
$s_add_item_after_save = 'Save and add new item';
$s_no_item_selected = 'There are no selected items.';
$s_confirm_delete_1_item = 'Are you sure you want to delete this record?';
$s_confirm_delete_n_items = 'Are you sure you want to delete these records?';

# 
$s_check_webpage = 'Check this webpage';
$s_check_webpage_hint = 'View this data live on your own website';

# button labels #
$s_but_save = 'Save';
$s_but_save_hint = 'Save current record';
$s_but_cancel = 'Cancel';
$s_but_cancel_hint = 'Cancel changes and go to previous page';
$s_but_close = 'Close';
$s_but_close_hint = 'Go to previous page';
$s_but_new = 'New Record';
$s_but_new_hint = 'Add new record';
$s_but_delete = 'Delete';
$s_but_delete_hint = 'Delete selected records';
$s_but_refresh = 'Refresh';
$s_but_refresh_hint = 'Refresh information';
$s_but_find = 'Find';
$s_but_find_hint = 'Search for records';
$s_but_copynew = 'Make copy';
$s_but_copynew_hint = 'Make copy of selected record';
$s_but_select = 'Select';
$s_but_select_hint = 'Select the current choice';
$s_but_browse = 'Browse';
$s_but_browse_hint = 'Browse for a file.';
$s_but_upload = 'Upload';
$s_but_working = 'busy...';

$s_but_nav_first = '<<';
$s_but_nav_last  = '>>';
$s_but_nav_prev  = '<';
$s_but_nav_next  = '>';

# designer functions #
$s_what_now = 'What do you want to do now?';
$s_where_to_now = 'Where do you want to go now?';
$s_add_client = 'Add a client...';

## Errors and warnings ##
$s_form_not_found = 'Could not find form! Please try again.';
$s_page_not_found = 'This page is currently not available.';
?>