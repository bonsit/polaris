<?php
	function find_var( $p_lang_arr, $p_lang_count, $p_eng_var ) {
		for ($i=0;$i<$p_lang_count;$i++) {
			#echo "============".$p_lang_arr[$i]." ==== $i - $p_eng_var<br />";
			if ( get_variable( $p_lang_arr[$i] ) == $p_eng_var ) {
				#echo $i." aaaa ".$p_lang_arr[$i]." AAAAAAA $p_eng_var ZZZZZZZ<br />";
				return $i;
			}
		}

		return -1;
	}

	### Get VALUE
	function get_value( $p_string ) {
		$p_string = trim( $p_string );
		return substr( $p_string, strpos( $p_string, "=" )+1, strlen( $p_string )-1 );
	}

	### Get VARIABLE
	function get_variable( $p_string ) {
		if ( strpos( " ".$p_string, "#" ) > 0 ) {
			$p_string = trim( $p_string );

			$arr = explode( " ", $p_string );

			# need to catch situation where it matches against the ##### string at the top of the file
			if ( isset( $arr[1] ) ) {
				return $arr[0].$arr[1];
			} else {
				return $arr[0];
			}
		} else {
			$p_string = trim( $p_string );

			$arr = explode( " ", $p_string );
			return $arr[0];
		}
	}

	### returns if is VARIABLE
	function is_var( $p_string ) {
		$p_string = " ".$p_string;

		### The # character means not a variable
		if ( strpos( $p_string, "#" ) > 0 ) {
			return false;
		} else if ( strpos( $p_string, "$" ) > 0 ) {
			return true;
		}
	}

	### returns if is COMMENT
	function is_comment( $p_string ) {
		$p_string = " ".$p_string;

		### The # character means not a variable
		if ( strpos( $p_string, "#" ) > 0 ) {
			return true;
		} else {
			return false;
		}
	}

	function print_blue( $p_string ) {
		echo "<font color=#0000ff><b>".htmlspecialchars( $p_string )."</b></font><br />";
	}

	function print_green( $p_string ) {
		echo "<font color=#00ff00><b>".htmlspecialchars( $p_string )."</b></font><br />";
	}

	function print_red( $p_string ) {
		echo "<font color=#ff0000><b>".htmlspecialchars( $p_string )."</b></font><br />";
	}

	function print_line( $p_string ) {
		echo htmlspecialchars( $p_string )."<br />";
	}

	function print_italics( $p_string ) {
		echo "<i>".htmlspecialchars( $p_string )."</i><br />";
	}

	function process_file( $p_lang ) {
		global $p_var, $g_mantis_path;

		$path = $g_mantis_path . "";

		$english = file( $path."res_lang_NL.inc.php" );
		$english_count = count( $english );
		$lang = file( $path."res_lang_".$p_lang.".inc.php" );
		$lang_count = count( $lang );



		#echo "333 - $lang[$lang_counter] <br />";
		#echo "444 - $lang[$eng_counter] <br />";

		### parse variables, look for missing
		$t_not_found = 0;
		while ( $eng_counter < $english_count ) {
			$eng_counter++;
			$lang_counter++;

			# check if there are more english strings than in the language file
			if ( !isset( $lang[$lang_counter] ) ) {
				continue;
			}
			$lang_str = $lang[$lang_counter];
			$eng_str = $english[$eng_counter];
			$lang_str_trim = trim( $lang_str );
			$eng_str_trim = trim( $eng_str );
			$lang_var = get_variable( $lang_str );
			$eng_var = get_variable( $eng_str );
			### if the strings are Variables then print in color
			if ((is_var( $lang_str ) && is_var( $eng_str )) &&
				($lang_var == $eng_var)) {
				### print in RED if same string
				if ( $lang_str_trim == $eng_str_trim ) {
					print_red( $eng_str );
				} else {
					print_line( $lang_str );
				}
			} else if ( empty( $lang_str_trim ) && empty( $eng_str_trim )) {
				print_line( '' );
			} else if (is_comment( $lang_str ) &&
						is_comment( $eng_str ) &&
						( $lang_str_trim == $eng_str_trim ) ) {
				print_italics( $lang_str );
			} else if ( $lang_str_trim == '?>' ) {
				print_line( $lang_str );
			} else {
				$s_var = $eng_var;
				#echo "ZOP ZOP ZOP : $s_var<br />";
				$location = find_var( $lang, $lang_count, $s_var );
				if ( $location > -1 ) {
					#echo "AAA - $location : $lang[$location] : $lang_counter : ";
					$lang_counter = $location;
				}

				### missing or out of order
				if ( is_var( $eng_str ) ) {
					#echo $s_var."000000000000";
					if ( $location > -1 ) {
						#echo "AAA - $location : $lang[$location] : $lang_counter : ";
						print_blue( $lang[$location] );  # out of order
						#$lang_counter = $location;
					} else {
						#echo "QQQ $lang_counter";
						print_green( $eng_str ); # missing line
					}
				} else if (is_comment( $eng_str )) {
					#echo "ITALICS ";
					print_italics( $eng_str );
				} else {
					#echo "PLAIN ";
					print_line( $eng_str );
				}
			}
		}
	}
?>

<html><body>

<?php
 $f_lang = $_GET['f_lang'];

?>
<span class="page_title">Localization Diff</span>
<hr size=1 noshade width="100%">
<p>

<table width=100%>
<tr valign=top>
	<td width=100%>
		<b><font size=+1><a name=page_top>Localization Diff - <?php echo $f_lang ?></a></font></b>
		<p>
		<a href="localization.php">Back to Localization</a>
		<p>
		Items in <font color=#ff0000>RED</font> are not translated (or are the same as in english)<br />
		Items in <font color=#0000ff>BLUE</font> are out of order.<br />
		Items in <font color=#00ff00>GREEN</font> are missing.<br />
		<p>
		<table cellspacing=1 border=0 bgcolor=#888888>
		<tr bgcolor=#8888ff>
			<td colspan=2>
				<b><?php echo $f_lang ?></b>
			</td>
		</tr>
		<tr bgcolor=#eeeeee>
			<td align=left colspan=2>
			<?php process_file( $f_lang ) ?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>

</body></html>
