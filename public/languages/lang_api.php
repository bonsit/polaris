<?php
	# Polaris - a php based application generator / CMS system
	# Copyright (C) 2000 - 2004  Bart Bons - bartbons@debster.nl

	###########################################################################
	# Language (Internationalization) API
	###########################################################################

	# Cache of localization strings in the language specified by the last
	# lang_load call
	$g_lang_strings = array();

	# Currently loaded language
	$g_lang_current = '';

    if (isset($_locale))
        if( !setlocale (LC_TIME, $_locale) )
            echo "<p>Locale '$_locale'not supported on this server</p>";

	# Loads the specified language and stores it in $g_lang_strings,
	# to be used by lang_get
	function lang_load_mod( $p_lang, $_directory=false ) {
		global $g_lang_strings;
		global $g_lang_current;
		global $_CONFIG;

		// define current language here so that when custom_strings_inc is
		// included it knows the current language
		$p_lang = strtoupper($p_lang);
		$g_lang_current = $p_lang;

		if ($_directory)
			$t_lang_dir = $_directory.'/languages/';
		else
			$t_lang_dir = dirname ( dirname ( __FILE__ ) ).'/languages/';
		$_lang_resource = $t_lang_dir . 'res_lang_'.$p_lang.'.inc.php';
		if (file_exists($_lang_resource)) {
			include_once($_lang_resource);
		}
		$_combi_resource = $t_lang_dir . 'res_lang_COMBI.inc.php';
		if (file_exists($_combi_resource))
			require_once( $_combi_resource );

		$t_vars = get_defined_vars();

		if (!empty($_date_format_placeholder))
			$_CONFIG['date_format_placeholder'] = $_date_format_placeholder;

		foreach ( array_keys( $t_vars ) as $t_var ) {
			//$t_lang_var = ereg_replace( '^s_', '', $t_var );
			$t_lang_var = preg_replace( '/^s_/', '', $t_var );
//			$t_lang_var = str_replace( 's_', '', $t_var );
			if ( $t_lang_var != $t_var || 'PLR_ERROR' == $t_var ) {
				$g_lang_strings[$t_lang_var] = $$t_var;
			}
		}

		## set the locale which is defined in each language resource file
		if (isset($_locale)) {
			setlocale(LC_ALL, $_locale);
		}
	}

	# Loads the specified language and stores it in $g_lang_strings,
	# to be used by lang_get
	function lang_load( $p_lang ) {
		global $g_lang_current;

		if ( $g_lang_current == $p_lang || $p_lang == '') {
			return;
		}
		lang_load_mod($p_lang);
	}

	# ------------------
	# Loads the users language or, if the database is unavailable, the default language
	function lang_load_default() {
		$t_active_language = 'NL';

		lang_load( $t_active_language );
	}

	# ------------------
	# Ensures that a language file has been loaded
	function lang_ensure_loaded() {
		global $g_lang_current;
		# Load the language, if necessary
		if ( '' == $g_lang_current ) {
			lang_load_default();
		}
	}

	# ------------------
	# Retrieves an internationalized string and wraps it in double quotes, and escapes it
	function lang_getjson( $p_string, $p_param = false) {
		$txt = str_replace('"', '\"', lang_get( $p_string, $p_param));
		return '"'.$txt.'"';
	}

	# ------------------
	# Retrieves an internationalized string
	#  This function will return one of (in order of preference):
	#    1. The string in the current users preferred language (if defined)
	#    2. The string in English
	function lang_get( $p_string, $p_param = false) {
		global $g_lang_strings;

		lang_ensure_loaded();

		# note in the current implementation we always return the same value
		#  because we do not have a concept of falling back on a language.  The
		#  language files actually *contain* English strings if none has been
		#  defined in the correct language
		if ( lang_exists( $p_string ) ) {
			if ($p_param == false)
				return $g_lang_strings[$p_string];
			else
				return sprintf($g_lang_strings[$p_string], $p_param);
		} else {
			echo "Language resource not found: $p_string";
			//trigger_error( ERROR_LANG_STRING_NOT_FOUND, WARNING );
			return '';
		}
	}

	# ------------------
	# Check the language entry, if found return true, otherwise return false.
	function lang_exists( $p_string ) {
		global $g_lang_strings;

		lang_ensure_loaded();

		return ( isset( $g_lang_strings[$p_string] ) );
	}

	# ------------------
	# Get language:
	# - If found, return the appropriate string (as lang_get()).
	# - If not found, no default supplied, return the supplied string as is.
	# - If not found, default supplied, return default.
	function lang_get_defaulted( $p_string, $p_default = null ) {
		if ( lang_exists( $p_string) ) {
			return lang_get( $p_string );
		} else {
			if ( null === $p_default ) {
				return $p_string;
			} else {
				return $p_default;
			}
		}
	}
