<?php
$_locale = 'nld_nld';

# Polaris designer resources #
$s_polaris = 'Polaris';
$s_pagetitle = 'Polaris � Piattaforma per applicazioni Web';
$s_polaris_main = 'Polaris applicazioni Web';
$s_polaris_about = 'Di Polaris';
$s_polaris_apps = 'Applicazioni Web';
$s_polaris_designer = 'Gestione';
$s_polaris_system_settings = 'Impostazioni sistema';
$s_polaris_overview = 'iniziare';

# general resources #
$s_yes = 'Si';
$s_no = 'No';
$s_via = 'Per';
$s_your_location = 'La sua posizione';
$s_welcome = 'Benvenuto';
$s_welcome_to = 'Benvenuto a';
$s_all_applications = 'Tutte applicazioni';
$s_your_applications = 'Le sue applicazioni';
$s_loggedinas = 'Nome utente corrente';
$s_click_to_start = 'Clicca per iniziare applicazione';
$s_of = 'di';
$s_items = 'Voci';
$s_your_functions = 'Le sue funzioni';
$s_go_to = 'Andare a';
$s_properties = 'Propriet�';
$s_step = 'Passo';
$s_check_all = 'Quadrettare tutto';
$s_check_none = 'Quadrettare niente';
$s_functions = 'Funzioni';
$s_required_fields = 'Riempire i seguenti campi';
$s_connection_successful = 'Connessione con banca dati avvenuta con successo!';
$s_connection_not_successful = 'Connessione con banca dati non avvenuta con succusso';
$s_login_message = 'Con il modulo di sotto i amministratori possono connettere con Polaris, il sistema in linea per amministrazione e content management.';
$s_only_licenced_users_message = 'Questo servizio Web � solo mirato a utenti con licenza.';
$s_create = 'Crea';
$s_new = 'nuovo';
$s_quick_navigation = 'Menu Rapido';
$s_webstatistics = 'Statistiche Web';
$s_new_polaris_features = 'Conosce gi`a tutte le funzioni nuove di Polaris?';
$s_read_more = 'Altro';
$s_move_up = 'Su';
$s_move_down = 'Gi�';
$s_click_to_reply = 'Clicca per inviare una pisposta';
$s_send_email = 'Invia email';
$s_value = 'Valore';
$s_key_column = 'Campo di chiave';
$s_key_column_hint = 'Campo chiave: i valori dei campi di chave identificano il record corrente';

# object actions (keep short) #
$s_action = 'azione';
$s_edit = 'modifica';
$s_view = 'visualizza';
$s_delete = 'cancella';
$s_show_users = 'mostra utenti';
$s_show_pages = 'mostra pagine';
$s_published = 'Pubblicato';
$s_publish = 'Pubblica';
$s_save_publish = 'Salva &amp; Pubblica';

$s_applications = 'APPLICAZIONI';
$s_designer = 'GESTIONE';
$s_ctrlpanel = 'PANNELLO DI CONTROLLO';
$s_help = 'GUIDA IN LINEA';
$s_logout = 'LOG OUT';
$s_login = 'LOG IN';

$s_configuration = 'Configurazione';
$s_system_management = 'Gestione sistema';
$s_your_application = 'La sua Applicazione';

# modules #
$s_modules = 'Moduli Gestione';
$s_modules_system = 'Moduli Sistema';
$s_mod_application_manager = 'Gestione applicazioni';
$s_mod_user_manager = 'Gestione utenti';
$s_mod_db_manager = 'Gestione banca dati';
$s_mod_backup_restore = 'Backup/Restituire';
$s_mod_client_manager = 'Gestione clienti';
$s_mod_system_settings = 'Impostazioni sistema';

$s_user_list = 'Lista utenti';
$s_client_list = 'Lista clienti';
$s_application_list = 'Lista applicazioni';
$s_your_applications = 'Le sue applicazioni';

# user field resources #
$s_user_account_of = 'Gebruikersaccount van';
$s_group_properties_of = 'Eigenschappen van de groep';
$s_create_membership = 'Maak deze gebruiker lid van een groep';
$s_create_membership_for = 'Maak lidmaatschap voor';
$s_create_group_membership = 'Maak deze groep lid van een andere groep';
$s_is_member_of = 'is lid van de volgende groepen';
$s_no_users_exists = 'Er zijn nog geen gebruikers aangemaakt.';
$s_user_has_no_memberships = 'Deze gebruiker heeft momenteel geen lidmaatschappen.';
$s_group_has_no_members = 'Deze groep heeft momenteel geen leden.';
$s_cancel_membership = 'Lidmaatschap opheffen';
$s_registered_user = 'Geregistreerde Gebruiker';
$s_memberships_of = 'Lidmaatschappen van';
$s_members_of = 'Leden van';
$s_add_selected = 'Geselecteerd items toevoegen';
$s_add_all = 'Alle items toevoegen';
$s_remove_selected = 'Geselecteerd items verwijderen';
$s_remove_all = 'Alle items verwijderen';

$s_user = 'Gebruiker';
$s_username = 'Gebruikersnaam';
$s_fullname = 'Volledige naam';
$s_password = 'Wachtwoord';
$s_description = 'Omschrijving';
$s_email_address = 'Emailadres';
$s_change_password_next_logon = 'Wachtwoord wijzigen bij volgende aanmelding';
$s_user_cannot_change_password = 'Kan gebruiker zelf wachtwoord wijzigen';
$s_password_never_expires = 'Wachtwoord blijft altijd geldig';
$s_account_disabled = 'Account geblokkeerd';
$s_last_login_date = 'Datum laatste keer aangemeld';
$s_login_count = 'Aantal keer aangemeld';
$s_client_admin = 'Gebruiker is klantbeheerder';
$s_root_admin = 'Gebruiker is Polarisbeheerder';
$s_change_password = 'Wijzig wachtwoord';
$s_change_your_password = 'Wijzig uw wachtwoord';
$s_new_password = 'Nieuw wachtwoord';
$s_retype_password = 'Herhaal wachtwoord';
$s_change_password_text = 'Voordat u verder kunt gaan met Polaris, dient u eerst 
 uw wachtwoord te wijzigen.';
$s_password_not_correct = 'Het wachtwoord is niet juist ingevuld. Probeer opnieuw.'; 
$s_login_not_correct = 'Gebruikernaam en wachtwoord zijn niet correct. Probeer opnieuw...';

$s_group = 'Groep';
$s_groupname = 'Groepnaam';
$s_account = 'Account';
$s_type = 'Type';
$s_name = 'Naam';
$s_last_login = 'Laatste login';
$s_action = 'Actie';
$s_logins = 'Aantal Logins';

$s_usergroup_desc = 'Hieronder ziet u alle (groepen van) gebruikeraccounts die deel uitmaken van uw Polaris administratie. 
U kunt accounts toevoegen, wijzigen en verwijderen.';
$s_new_user_desc = 'Hieronder kunt u de gegevens van de nieuwe gebruiker invullen.';
$s_edit_user_desc = 'Hieronder kunt u het huidige account aanpassen. Ook kunt u de gebruiker lid maken van
een of meerdere groepen.';
$s_new_group_desc = 'Hieronder kunt u de gegevens van de nieuwe groep invullen.';
$s_edit_group_desc = 'Hieronder kunt u de huidige groep aanpassen. Ook kunt u gebruikers lid maken van deze groep.';

# User settings field resources #
$s_user_settings = 'Impostazioni untente';
$s_user_profile = 'Profilo utente';

# client field resources #
$s_client_account_of = 'Klant account van';

$s_client = 'Klant';
$s_name = 'Naam';
$s_country = 'Land';
$s_address = 'Adres';
$s_city = 'Plaats';
$s_zipcode = 'Postcode';
$s_telephone = 'Telefoonnr.';
$s_fax = 'Fax';
$s_postaddress = 'Bezorg adres';
$s_postcity = 'Bezorg plaats';
$s_postzipcode = 'Bezorg postcode';
$s_default_language = 'Standaard taal voor nieuwe gebruikers';
$s_limits = 'Limieten';

$s_choose_client = 'Kies een klant waarvan u de items wilt zien (of kies alle klanten)';
$s_all_clients = 'Alle klanten';

# application field resources #
$s_application = 'Applicatie';
$s_application_name = 'Applicatienaam';
$s_application_description = 'Omschrijving';
$s_application_registered = 'Registreerd aan';
$s_application_version = 'versione';
$s_application_picture = 'Applicatie afbeelding (Splash)';
$s_application_image = 'Applicatie pictogram';
$s_orderindex = 'Volgorde index';
$s_startup_message = 'Melding bij opstarten';
$s_startup_message_formcolor = 'Achtergrondkleur van opstartmelding';
$s_helpnr = 'Helpnummer';
$s_helpfile = 'Helpbestand';
$s_searchoptions = 'Zoekopties';
$s_website = 'Website URL';
$s_ftpserver = 'FTP server';
$s_ftpuser = 'FTP gebruiker';
$s_ftppassword = 'FTP wachtwoord';

$s_apps_choose_from_list = 'Om met een applicatie te werken kiest u er een uit onderstaande lijst.';
$s_apps_non_available    = 'U bent voor geen applicatie geautoriseerd, neem contact op met <a href="mailto:info@debster.nl">Debster</a>'; 

$s_test_connection = 'Test verbinding';
$s_save_settings = 'Instellingen opslaan';
$s_go = 'GO';

# form field resources
$s_forms = 'Formulieren';

$s_formname = 'Formulier';
$s_databaseid = 'Database';
$s_tablename = 'Tabelnaam';
$s_filter = 'Filter';
$s_pagecolumncount = 'Aantal kolommen';
$s_webpageurl = 'Webpagina URL';
$s_defaultviewtype = 'Standaard formuliertype';
$s_showrecordsasconstructs = 'showrecordsasconstructs';
$s_captioncolumnname = 'Kolomnaam van detailinfo form.';
$s_sortcolumnname = 'Sorteer op kolom';
$s_searchcolumnname = 'Zoek op kolom';
$s_panelhelptext = 'Formulier helptekst';
$s_preload = 'preload';
$s_newrecordinformview = 'Toon nieuw in formview?';
$s_selectview = 'Selectie view';
$s_selectcolumnnames = 'Selectie kolomnamen';
$s_selectshowcolumns = 'Selectie presentatie kolomnamen';
$s_selectquery = 'Selectie query';

$s_database_script = 'database script';
$s_axmls_script = 'ADODB XML Schemas';
$s_backup_restore = 'Backup/Restore van klant database';
$s_backup = 'Backup';
$s_restore = 'Restore';

# form permission resources
$s_autorisation = 'Autorizzazione';

# page field resources
$s_pages = 'pagine';
$s_page = 'pagina';

# container resources
$s_navigation = 'Navigazione';

# Webpage, Webparam resources
$s_webpage_list = 'Pagina Web\'s';
$s_webpages_desc = 'Di sotto si vedono tutte le pagine Web del suo website (a che ha accesso).
Si possono aggiungere, modificare o eliminare pagine Web.';
$s_no_webpage_exists = 'Non � autorizzato a modificare una pagina Web, o non ci sono disponibili pagine Web.';
$s_webpage_name = 'Nome pagina';
$s_languages = 'Lingue';
$s_content = 'Contenuto';
$s_edit_webpage = 'Modifica pagina Web';
$s_webparam_name = 'Parametro';

# record limit
$s_recordlimit_label = 'Mostra';
$s_recordlimit_hint = 'Numero records mostrati alla volta';
$s_navigate_first = 'primo';
$s_navigate_prev = 'precedente';
$s_navigate_next = 'seguente';
$s_navigate_last = 'ultimo';
$s_navigate_first_hint = 'Mostra la prima pagina record';
$s_navigate_prev_hint = 'Mostra precedente pagina record';
$s_navigate_next_hint = 'Mostra seguente pagina record';
$s_navigate_last_hint = 'Mostra ultima pagina record';

$s_filter_title = 'Filtro voci';
$s_filter_cancel = 'Mostra tutte voci (annulare filtro)';

# Refresh resources
$s_refreshinterval_label = 'Ververs scherm';
$s_n_seconds = '%s sec';
$s_n_minutes = '%s min';
$s_never_refresh = 'nooit';

# image resources #
$s_image_view = 'mostra immagine';
$s_image_view_hint = 'Mostra immagine in finestra nuova';
$s_image_view_normal_size = 'Mostra a dimensione normale';
$s_image_delete = 'Cancella immagine';
$s_image_add_new = 'Aggiungi immagine...';
$s_image_file_size = 'Dimensione file';
$s_image_edit = 'Modifica immagine';
$s_image_edit_hint = '';
$s_image_select_button = 'lista immagini';
$s_image_select = 'seleziona immagine';
$s_image_select_hint = 'Apri lista immagini in finestra nuova.';
$s_image_upload_text = 'Qui � possibile per upload immagini. Cerca immagini con bottone \'Sfogliare...\' (massimo 5 immagini) e premi Inviare.';
$s_pick_image = 'Seleziona immagine dalla lista sottostante.';
$s_no_images = 'Non ci sono immagini.';
$s_selected_image = 'Immagine selezionata';
$s_image_preview = 'Esempio';
$s_choose_image = 'Seleziona&nbsp;immagine';
$s_upload_images = 'Immagini&nbsp;upload';

# item resources #
$s_no_item = 'non';
$s_item = 'voce';
$s_n_items = 'voci';
$s_show_all_items = 'Tutte %s'; # default %s value is s_items #
$s_add_item = 'Aggiungi %s'; # default %s value is s_item #
$s_add_item_to_list = 'Aggiungi %s a questa lista'; # default %s value is s_item #
$s_add_item_after_save = 'Salva e aggiungi %s nuova'; # default %s value is s_item #
$s_list_is_empty = 'La lista con \'%s\' � ancora vuota.'; # default %s value is s_items #
$s_you_can_add_to_list = 'Ora puo aggiungere una voce alla lista.'; # default %s value is s_item #
$s_all_items = 'Tutte voci';
$s_click_and_edit_item = 'Clicca su voce per modificare record.';
$s_click_and_view_item = 'Clicca su voce per visualizzare record.';
$s_no_item_selected = 'Non ci sono record selezionati.';
$s_confirm_delete_1_item = 'Sei sicuro di voler cancellare questo record?';
$s_confirm_delete_n_items = 'Sei sicuro di voler cancellare questi record?';
$s_edit_item = '%s wijzigen';
$s_view_item = '%s bekijken';
$s_add_an_item = '%s toevoegen';
$s_add_new_item = 'Nieuw %s record toevoegen';
$s_clone_an_item = '%s klonen';
$s_clone_item = "Kloon dit item";

# Search form #
$s_search = 'Cercare';
$s_search_title = 'Cercare un particolare';
$s_search_record = 'record';
$s_search_column_label = 'Cercare dentro';
$s_search_value_label = 'Cercare il valore';
$s_search_advanced = 'Cercare avanzato';
$s_filtered = 'filtrato!';
$s_filtered_hint = 'Clicca su \'Tutte voci\' per cancellare questo filtro';

#
$s_check_webpage = 'Controlla pagina Web';
$s_seperate_window = 'in finestra separata';
$s_check_webpage_hint = 'Visualizza questi dati sul proprio website';

# button labels #
$s_but_save = 'Salva';
$s_but_save_hint = 'Salva record corrente';
$s_but_cancel = 'Annulla';
$s_but_cancel_hint = 'Ritornare alla finestra precedente';
$s_but_close = 'Chiudi';
$s_but_close_hint = 'Chiudere finestra';
$s_but_new = 'Record nuovo';
$s_but_new_hint = 'Aggiungere record nuovo';
$s_but_delete = 'Cancella';
$s_but_delete_hint = 'Cancellare record ';
$s_but_refresh = 'Rinnova';
$s_but_refresh_hint = 'Rinnovare dati';
$s_but_find = 'Cerca';
$s_but_find_hint = 'Cercare un record in particolare';
$s_but_copynew = 'Copia';
$s_but_copynew_hint = 'Copiare record selezionato';
$s_but_select = 'Seleziona';
$s_but_select_hint = 'Selezionare scelta corrente';
$s_but_browse = 'Sfoglia';
$s_but_browse_hint = 'Sfogliare un file in particolare.';
$s_but_upload = 'Invia';
$s_but_working = 'eseguendo...';

$s_but_nav_first = '<<';
$s_but_nav_last = '>>';
$s_but_nav_prev = '<';
$s_but_nav_next = '>';

# designer functions #
$s_what_now = 'Cosa vuole fare?';
$s_where_to_now = 'Dove vuole andare?';
$s_add_client = 'Aggiungi cliente...';

# Language resources #
$s_choose_language = 'Seleziona lingua';

# Errors and warnings #
$s_go_back_after_insert_error = 'Wijzig uw record en probeer opnieuw';
$s_form_not_found = 'Modulo non trovato! Prova di nuovo.';
$s_page_not_found = 'Questa pagina Web ora non � disponibile.';
$s_cannot_determine_tables = 'Non � possibile definire le tabelle della banca dati.';
$s_cannot_connect_database = 'Non era possibile connettersi con la banca dati. Controlla i dati e prova di nuovo.';
$s_cannot_insert_database = 'Polaris non poteva salvare i suoi dati.';
$s_cannot_update_record = 'Non era possibile aggiornare i suoi dati';
$s_error_512 = 'Non � possibile salvare questo record. Esiste gi`a un record con la stessa identificazione.';

# Language names
$s_lang_nl = 'Olandese';
$s_lang_fr = 'Francese';
$s_lang_en = 'Inglese';
$s_lang_de = 'Tedesco';
$s_lang_se = 'Swedish';
$s_lang_it = 'Italiano';
$s_lang_aa = 'Gen�rico';

?>