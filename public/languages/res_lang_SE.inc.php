<?
$_locale = 'se_se';

# Polaris designer resources #
$s_polaris = 'Polaris';
$s_pagetitle = 'Polaris - Plattform f�r Webbans�kningar';
$s_polaris_designer = 'Polaris designer';
$s_polaris_settings = 'Polaris inst�llningar';
$s_polaris_main = 'Polaris Webbaserade Ans�kningar';
$s_polaris_about = 'Om Polaris';
$s_polaris_system_settings = 'System settings';

# general resources #
$s_yes = 'Ja';
$s_no = 'Nej';
$s_via = 'Via';
$s_your_location = 'Din lokalisering';
$s_welcome = 'V�lkommen';
$s_welcome_to = 'V�lkommen till';
$s_all_applications = 'Alla Ans�kningar';
$s_your_applications = 'Din Ans�kning';
$s_loggedinas = 'Inloggad som';
$s_click_to_start = 'Klicka f�r att starta ans�kningen';
$s_of = 'av';
$s_items = 'Artikel';
$s_your_functions = 'Din funktionerna';

# object actions (keep short) #
$s_action = 'handling';
$s_edit = 'redigera';
$s_view = 'visa';
$s_delete = 'ta bort';
$s_show_users = 'visa anv�ndare';

$s_applications = 'ANS�KNINGAR';
$s_designer = 'DESIGNER';
$s_ctrlpanel = 'CONFIGURERA';
$s_help = 'HJ�LP';
$s_logout = 'LOGGA AV';
$s_login = 'LOGGA IN';

# modules #
$s_modules = 'Management';
$s_modules_system = 'System Management';
$s_mod_application_manager = 'Application manager';
$s_mod_user_manager = 'User manager';
$s_mod_db_manager = 'Database manager';
$s_mod_backup_restore = 'Backup &amp; Restore';
$s_mod_client_manager = 'Client manager';
$s_mod_system_settings = 'System settings';

$s_user_list = 'Anv�ndarlista';
$s_client_list = 'Kundlista';
$s_application_list = 'Ans�kningslista';
$s_your_applications = 'Din Ans�kan';

# user field resources #
$s_user_account_of = 'Anv�ndarkonto av';
$s_create_membership = 'Skapa medlemskap f�r denna medlem';
$s_create_group_membership = 'Ska medlemskap f�r denna grupp';
$s_is_member_of = 'is member of the following groups';
$s_no_users_exists = 'No users have been created.';
$s_user_has_no_memberships = 'This user has no memberships.';
$s_group_has_no_members = 'This group has no members.';
$s_cancel_membership = 'Cancel membership';
$s_registered_user = 'Current Member Login';

$s_username = 'Anv�ndarnamn';
$s_fullname = 'Hela Namnet';
$s_password = 'L�senord';
$s_description = 'Beskrivning';
$s_email_address = 'Email adress';
$s_change_password_next_logon = '�ndra l�senord med n�sta inlogging';
$s_user_cannot_change_password = 'Anv�ndare kan inte �ndra l�senord';
$s_password_never_expires = 'L�senord f�rfaller aldrig';
$s_account_disabled = 'Konto nedst�ngt';
$s_last_login_date = 'Senaste inloggnings datum';
$s_login_count = 'inloggnings r�knare';
$s_client_admin = 'Anv�ndare �r Kund Administrat�r';
$s_root_admin = 'Anv�ndare �r Polaris Administrat�r';
$s_change_password = '�ndra l�senord';
$s_change_your_password = 'Change your password';
$s_new_password = 'New password';
$s_retype_password = 'Retype password';
$s_change_password_text = 'Before you can continue with Polaris, you have to change your password.';
$s_password_not_correct = 'The password was not correct. Make sure you enter the password twice and try again.'; 
$s_login_not_correct = 'Username and password are not correct. Please try again...';

$s_groupname = 'Grupp Namn';
$s_account = 'Konto';
$s_group = 'Grupp';
$s_type = 'Typ';
$s_name = 'Namn';
$s_last_login = 'Senaste Inloggning';
$s_action = 'Handling';
$s_logins = 'Antal inloggningar';

# client field resources #
$s_client = 'Kund';
$s_client_country = 'Land';
$s_client_address = 'Adress';
$s_client_zipcode = 'Postkod';
$s_client_telephone = 'Telefon #';

# application field resources #
$s_application = 'Ans�kan';
$s_application_description = 'Titel';
$s_application_registered = 'Registrerad till';
$s_application_version = 'Version';

$s_apps_choose_from_list = 'F�r att starta en ans�kan, var v�nlig v�lj en fr�n listan nedan.';
$s_apps_non_available    = 'Du �r inte ber�ttigad f�r en ans�kan, var v�nlig kontakta <a href="mailto:info@debster.nl">Debster</a>'; 

$s_test_connection = 'Test f�rbindelse';
$s_save_settings = 'Spara inst�llningar';
$s_go = 'OK';

$s_backup_restore = 'Backup/�terst�llning av Polaris databas';

# Search form #
$s_search								= 'S�k';
$s_search_title  				= 'S�k efter en';
$s_search_record  			= 'meriter';
$s_search_column_label  = 'S�k i';
$s_search_value_label   = 'S�k efter v�rde';

# record limit
$s_recordlimit_label = 'Visa';
$s_recordlimit_hint = 'Antal f�rteckningar framtagna fr�n databasen';
$s_navigate_first = 'f�rsta';
$s_navigate_prev = 'senaste';
$s_navigate_next = 'n�sta';
$s_navigate_last = 'sista';
$s_navigate_first_hint = 'Visa f�rsta f�rtecknings sida';
$s_navigate_prev_hint = 'Visa senaste f�rtecknings sida';
$s_navigate_next_hint = 'Visa n�sta f�rtecknings sida';
$s_navigate_last_hint = 'Visa sista f�rtecknings sida';

$s_filter_title = 'Filtrera artiklar';
$s_filter_cancel = 'Visa alla artiklar (stoppa filter)';

# image resources #
$s_image_view = 'visa bild';
$s_image_view_hint = 'Visa bild i nytt f�nster';
$s_image_view_normal_size = 'Visa i normal storlek';
$s_image_delete = 'Ta bort bild';
$s_image_add_new = 'L�gg till...';
$s_image_file_size = 'Fil storlek';
$s_image_edit = 'Redigera bild';
$s_image_edit_hint = '';
$s_image_select_button = 'Bild lista';
$s_image_select = 'V�lj en bild';
$s_image_select_hint = '�ppnar lista med bilder i nytt f�nster';
$s_image_upload_text = 'Du kan ladda up en eller flera bilder. Bl�ddra efter din bild med \'Bl�ddra...\' knappen (max. 5 bilder per uppladdning) och tryck Ladda upp.';
$s_pick_image = 'V�lj en bild fr�n listan under.';
$s_no_images = 'Det finns inga bilder.';
$s_selected_image = 'Vald bild'; 
$s_image_preview = 'F�rhandsgranskning';
$s_choose_image = 'V�lj&nbsp;bild';
$s_upload_images = 'Ladda upp&nbsp;bilder';

# item resources #
$s_add_item = 'L�gg till artikel';
$s_n_items = 'artiklar';
$s_all_items = 'Alla artiklar';
$s_add_item_to_list = 'L�gg till en artikel till den h�r listan';
$s_click_and_edit_item = 'Klicka p� en artikel f�r att �ndra dess f�rteckning.';
$s_click_and_view_item = 'Klicka p� en artikel f�r att visa dess f�rteckning.';
$s_no_item = 'inga';
$s_add_item_after_save = 'Spara och l�gg till ny artikel';
$s_no_item_selected = 'Det finns inga valda artiklar.';
$s_confirm_delete_1_item = '�r du s�ker p� att du vill ta bort den h�r f�rteckningen?';
$s_confirm_delete_n_items = '�r du s�ker p� att du vill ta bort dessa f�rteckningar?';

# 
$s_check_webpage = 'Visa Hemsida';
$s_check_webpage_hint = 'Visa den h�r informationen live p� din hemsida';

# button labels #
$s_but_save = 'Spara';
$s_but_save_hint = 'Spara aktuell f�rteckning';
$s_but_cancel = 'Avbryt';
$s_but_cancel_hint = 'Avbryt �ndringarna och �terg� till senaste sida';
$s_but_close = 'St�ng';
$s_but_close_hint = 'G� till senaste sida';
$s_but_new = 'Ny f�rteckning';
$s_but_new_hint = 'L�gg till ny f�rteckning';
$s_but_delete = 'Ta bort';
$s_but_delete_hint = 'Ta bort vald f�rteckning';
$s_but_refresh = 'Ladda om';
$s_but_refresh_hint = 'Ladda om informationen';
$s_but_find = 'S�k';
$s_but_find_hint = 'S�k efter f�rteckningar';
$s_but_copynew = 'G�r kopia';
$s_but_copynew_hint = 'G�r kopia av vald f�rteckning';
$s_but_select = 'V�lj';
$s_but_select_hint = 'V�lj aktuellt val';
$s_but_browse = 'Bl�ddra';
$s_but_browse_hint = 'Bl�ddra efter fil.';
$s_but_upload = 'Ladda Upp';
$s_but_working = 'aktiv...';

$s_but_nav_first = '<<';
$s_but_nav_last  = '>>';
$s_but_nav_prev  = '<';
$s_but_nav_next  = '>';

# designer functions #
$s_what_now = 'Vad vill du g�ra nu?';
$s_add_client = 'L�gg till kund...';

# Errors and warnings #
$s_form_not_found = 'Kunde inte hitta formul�r! Var v�nlig f�rs�k igen.';
$s_page_not_found = 'Kunde inte hitta sida.';
?>