<?php
$_locale = array('nl_NL','nld_nld','nl_NL.utf8','dutch');
$_rss_locale = 'nl-nl';
$_date_format_placeholder = 'DD-MM-JJJJ';

# Polaris designer resources #
$s_polaris = 'Maxia';
$s_pagetitle = 'Maxia';
$s_polaris_main = 'Maxia';
$s_polaris_about = 'Over Maxia';
$s_polaris_apps = 'Applicaties';
$s_polaris_designer = 'Beheer';
$s_polaris_system_settings = 'Systeem instellingen';
$s_polaris_overview = 'beheersysteem';

# general resources #
$s_yes = 'Ja';
$s_no = 'Nee';
$s_via = 'Via';
$s_your_location = 'U bent hier';
$s_welcome = 'Welkom';
$s_welcome_to = 'Welkom bij';
$s_all_applications = 'Home';
$s_your_applications = 'Uw applicaties';
$s_loggedinas = 'Ingelogd als';
$s_click_to_start = 'Klik om applicatie te starten';
$s_of = 'van';
$s_until = 'tot';
$s_items = 'items';
$s_your_functions = 'Uw functies';
$s_go_to = 'Ga naar';
$s_properties = 'Eigenschappen';
$s_step = 'Stap';
$s_check_all = 'Alles aanvinken';
$s_check_none = 'Niets aanvinken';
$s_check_all_short = 'Alles';
$s_check_none_short = 'Niets';
$s_functions = 'Functies';
$s_required_fields = 'De volgende velden dienen ingevuld te worden';
$s_connection_successful = 'Databaseverbinding is succesvol!';
$s_connection_not_successful = 'Databaseverbinding is niet gelukt';
$s_login_message = 'Middels onderstaand formulier kunnen systeemadministrators toegang krijgen tot Polaris, het online administratie en content management systeem.';
$s_only_licenced_users_message = 'Deze webservice is alleen bedoeld voor gelicencieerde gebruikers van Polaris.';
$s_create = 'Maak';
$s_new = 'nieuwe';
$s_quick_navigation = 'Snel Menu';
$s_webstatistics = 'Webstatistieken';
$s_new_polaris_features = 'Kent u de nieuwe Polaris functies al?';
$s_read_more = 'lees meer';
$s_more = 'meer';
$s_move_up = 'Verplaats een rij omhoog';
$s_move_down = 'Verplaats een rij omlaag';
$s_click_to_reply = 'Klik om email te versturen';
$s_send_email = 'Stuur email';
$s_value = 'Waarde';
$s_key_column = 'Sleutelveld';
$s_key_column_hint = 'Sleutelveld: de waarden van het sleutelveld(en) identificeren het huidige record';
$s_orderindex = 'Volgorde index';
$s_visible = 'Zichtbaar';
$s_enter_your_account = 'Vul uw account gegevens in om u aan te melden.';
$s_calendar = 'Kalender';
$s_remember_on_this_computer = 'Onthouden op deze computer';
$s_maximize_screen = 'maximaal';
$s_maximize_screen_hint = 'Maximaliseer formulier';
$s_separate_screen = 'ontkoppel';
$s_separate_screen_hint = 'Toon formulier in apart venster';
$s_cancel_sorting = 'Sortering opheffen';
$s_pick_items = 'Items kiezen';
$s_no_items = 'Geen items';
$s_apply_filter = 'Filter toepassen';
$s_set_focus = 'Set focus';
$s_please_wait = 'Even geduld a.u.b.';
$s_start_new_polaris_session = 'Start nieuwe Polaris sessie';
$s_week = 'week';
$s_day = 'dag';
$s_ready_working = 'Klaar met werken?';
$s_logoff_text = 'Vergeet niet op <strong>LOG UIT</strong> te klikken, wanneer u klaar bent met '.$s_polaris.'. Hiermee voorkomt u dat onbevoegde personen met uw applicatie kunnen werken.';
$s_unknown = 'onbekend';

$s_searchform_title = 'Zoeken naar items';
$s_case_sensitive = 'Hoofdlettergevoelig';
$s_search_fullword = 'Zoek op geheel woord';
$s_search_fromstart = 'Zoek vanaf begin';
$s_new_search = 'Nieuwe zoekactie';
$s_new_search_hint = 'Maak zoekvelden leeg en verwijder gepinde items';
$s_search_now = 'Nu zoeken';
$s_no_search_available = 'geen zoekfunctie beschikbaar';
$s_column = 'Kolom';
$s_value = 'Waarde';
$s_no_pinned_items = 'geen items';
$s_pinned_items_title = 'Gepinde items';
$s_delete_pinned_items = 'Verwijder alle gepinde items';
$s_delete_pin = 'Verwijder pin';
$s_pin_this_record = 'Pin dit record';
$s_search_first_text = 'U dient eerst te zoeken, voordat items worden getoond.';

$s_month_names = "Januari_Februari_Maart_April_Mei_Juni_Juli_Augustus_September_Oktober_November_December";
$s_short_month_names = "Jan_Feb_Maa_Apr_Mei_Jun_Jul_Aug_Sep_Okt_Nov_Dec";
$s_short_day_names = "Zo_Ma_Di_Wo_Do_Vr_Za";
$s_changes_not_saved = "U heeft de wijzigingen nog niet opgeslagen.<br />Wilt u het item opslaan?";
$s_long_operation = "Deze actie kan even duren... Weet u zeker dat u verder wilt gaan?";
$s_insufficient_rights_cannot_update = "U heeft niet genoeg rechten om deze gegevens te wijzigen.";
$s_password_strengths = "Blanco_Zeer zwak_Zwak_Gemiddeld_Sterk_Zeer sterk";
$s_polaris_maintenance = "{$s_polaris} is <span id=\"duration_time\">?</span> niet beschikbaar. <br/>Reden: __reason__";
$s_dont_close_polaris = "U hoeft {$s_polaris} niet af te sluiten.";
$s_save_record = "Wilt u het item opslaan?";
$s_enter_required_fields = "U dient de verplichte velden in te vullen";
$s_please_wait = "Een moment a.u.b...";
$s_no_item_selected = "U heeft geen item geselecteerd.";
$s_more_than_one_item_selected = "U heeft meer dan een item geselecteerd.";
$s_print = 'Druk af&hellip;';
$s_none = 'geen';
$s_printersettings = 'Printer instellingen';
$s_switch_to = 'Wissel naar';

# validations
$s_enter_valid_date = "Vul hier een geldige datum in (dd-mm-jjjj).";
$s_phone_number_check = "Een telefoonnummer bestaat uit minimaal 10 cijfers";
$s_person_older_than_10 = "Alleen personen van 10 jaar of ouder zijn toegestaan";
$s_housenumber_check = "Een Nederlands huisnr mag alleen cijfers bevatten (gebruik het veld Toevoeging voor andere tekens).";
$s_incorrect_field = "U heeft een veld niet correct ingevuld.";
$s_incorrect_field_plural = "U heeft __count__ velden niet correct ingevuld.";
$s_elf_proef = "Deze waarde voldoet niet aan de elfproef.";
$s_bsn_check = "Deze waarde voldoet niet aan de BSN controle.";
$s_no_whitespace = "Geen spaties toegestaan.";

# object actions (keep short) #
$s_action = 'aktie';
$s_edit = 'Wijzig';
$s_view = 'Bekijk';
$s_clone = 'Kloon';
$s_delete = 'Verwijder';
$s_show_users = 'toon gebruikers';
$s_show_pages = 'toon pagina\'s';
$s_published = 'Gepubliceerd';
$s_publish = 'Publiceer';
$s_save_publish = 'Opslaan &amp; Publiceer';
$s_select = 'selecteer';
$s_open = 'open';
$s_extra = 'extra';
$s_clean = 'wissen';
$s_copy = 'kopieer';

$s_applications = 'Applicaties';
$s_designer = 'Beheer';
$s_ctrlpanel = 'Instellingen';
$s_help = 'Help';
$s_logout = 'Log uit';
$s_login = 'Log in';

$s_configuration = 'Configuratie';
$s_system_management = 'Systeem management';
$s_your_application = 'Uw applicatie';

# modules #
$s_modules = 'Beheer Modules';
$s_modules_system = 'Systeem Modules';
$s_mod_application_manager = 'Applicaties';
$s_mod_user_manager = 'Gebruikers';
$s_mod_db_manager = 'Databases';
$s_mod_backup_restore = 'Backup/Restore';
$s_mod_client_manager = 'Klanten';
$s_mod_system_settings = 'Systeem instellingen';
$s_mod_template_manager = 'Prefab-applicaties';
$s_mod_site_generator = 'Applicatie generator';

$s_user_list = 'Gebruikersoverzicht';
$s_client_list = 'Klantenoverzicht';
$s_application_list = 'Applicatie overzicht';
$s_your_applications = 'Uw Applicaties';
$s_sitegenerator_list = 'Websites overzicht';

# user field resources #
$s_user_account_of = 'Gebruikersaccount van';
$s_group_properties_of = 'Eigenschappen van de groep';
$s_create_membership = 'Maak deze gebruiker lid van een groep';
$s_create_members = 'Maak gebruikers(groepen) lid van deze groep';
$s_create_membership_for = 'Maak lidmaatschap voor';
$s_create_group_membership = 'Maak deze groep lid van een andere groep';
$s_is_member_of = 'is lid van de volgende groepen';
$s_no_users_exists = 'Er zijn nog geen gebruikers aangemaakt.';
$s_user_has_no_memberships = 'Deze gebruiker heeft momenteel geen lidmaatschappen.';
$s_group_has_no_members = 'Deze groep heeft momenteel geen leden.';
$s_cancel_membership = 'Lidmaatschap opheffen';
$s_registered_user = 'Geregistreerde Gebruiker';
$s_memberships_of = 'Lidmaatschappen van';
$s_groupmembership = 'Groepslidmaatschap';
$s_product_access = 'Producttoegang';
$s_members_of = 'Leden van';
$s_add_selected = 'Geselecteerd items toevoegen';
$s_add_all = 'Alle items toevoegen';
$s_remove_selected = 'Geselecteerd items verwijderen';
$s_remove_all = 'Alle items verwijderen';

$s_user = 'Gebruiker';
$s_users = 'Gebruikers';
$s_username = 'Gebruikersnaam';
$s_fullname = 'Volledige naam';
$s_password = 'Wachtwoord';
$s_description = 'Omschrijving';
$s_email_address = 'Emailadres';
$s_change_password_next_logon = 'Wachtwoord wijzigen bij volgende aanmelding';
$s_user_cannot_change_password = 'Kan gebruiker zelf wachtwoord wijzigen';
$s_password_never_expires = 'Wachtwoord blijft altijd geldig';
$s_account_disabled = 'Account geblokkeerd';
$s_last_login_date = 'Datum laatste keer aangemeld';
$s_login_count = 'Aantal keer aangemeld';
$s_client_admin_short = 'Organisatiebeheerder';
$s_client_admin = 'Gebruiker is klantbeheerder';
$s_root_admin = 'Gebruiker is Polarisbeheerder';
$s_change_password = 'Wijzig wachtwoord';
$s_change_your_password = 'Wijzig uw wachtwoord';
$s_reset_your_password = 'Stel uw wachtwoord opnieuw in';
$s_new_password = 'Nieuw wachtwoord';
$s_retype_password = 'Herhaal wachtwoord';
$s_login_text = 'Vul uw account gegevens in om u aan te melden.';
$s_forgot_password = 'Wachtwoord vergeten?';
$s_forgot_password_text = 'Vul uw emailadres in en klik op de knop om een email te ontvangen waarmee u uw wachtwoord kunt herstellen.';
$s_no_account = 'Heb je geen account?';
$s_create_account = 'Maak een account';
$s_change_password_text = 'Voordat u verder kunt gaan, dient u eerst uw wachtwoord te wijzigen.';
$s_password_not_correct = 'Het wachtwoord is niet juist ingevuld. Probeer opnieuw.';
$s_password_not_strong_enough = 'Het wachtwoord is niet sterk genoeg.';
$s_login_not_correct = 'Gebruikernaam en wachtwoord zijn niet correct. Probeer opnieuw...';
$s_polarisinstance = 'Polaris instance';
$s_auth_type = 'Authorisatie type';
$s_user_language = 'Gebruikerstaal';
$s_user_exists = 'Deze gebruikersnaam/emailadres is al in gebruik';
$s_user_name_invalid = 'De gebruikersnaam mag alleen letters, cijfers en de volgende tekens bevatten: @ . _ -';
$s_loginerror_1 = 'De combinatie van gebruikersnaam en wachtwoord is niet bekend.';
$s_loginerror_2 = '';
$s_loginerror_3 = 'Uw account is geblokkeerd. Neem contact op met de systeembeheerder.';
$s_loginerror_4 = 'Er is een andere gebruiker ingelogd met dit account. Log eerst uit op de andere locatie en probeer opnieuw.';

$s_group = 'Groep';
$s_groups = 'Groepen';
$s_groupname = 'Groepnaam';
$s_account = 'Account';
$s_type = 'Type';
$s_name = 'Naam';
$s_last_login = 'Laatste login';
$s_last_active = 'Laatst actief';
$s_show_details = 'Details tonen';
$s_action = 'Actie';
$s_logins = 'Aantal Logins';
$s_autocreatemembership = 'Nieuwe leden automatisch<br /> lid maken van deze groep?';
$s_language = 'Taal';
$s_2facodecheck = 'Controle nummer';

$s_totalusers = 'Totaal aantal gebruikers';
$s_activeusers = 'Actieve gebruikers';
$s_totalgroups = 'Totaal aantal groepen';
$s_admins = 'Beheerders';
$s_access_to = 'Toegang tot';
$s_members = 'Groepsleden';
$s_status = 'Status';
$s_productaccess = 'Producttoegang';

$s_usergroup_desc = 'Hieronder ziet u alle (groepen van) gebruikeraccounts die deel uitmaken van uw Polaris administratie.
U kunt accounts toevoegen, wijzigen en verwijderen.';
$s_new_user_desc = 'Hieronder kunt u de gegevens van de nieuwe gebruiker invullen.';
$s_edit_user_desc = 'Hieronder kunt u het huidige account aanpassen. Ook kunt u de gebruiker lid maken van
een of meerdere groepen.';
$s_new_group_desc = 'Hieronder kunt u de gegevens van de nieuwe groep invullen.';
$s_edit_group_desc = 'Hieronder kunt u de huidige groep aanpassen. Ook kunt u gebruikers lid maken van deze groep.';

# User settings field resources #
$s_user_settings = 'Gebruikersinstellingen';
$s_user_profile = 'Gebruikersprofiel';

# client field resources #
$s_client_account_of = 'Klant account van';

$s_client = 'Klant';
$s_name = 'Naam';
$s_country = 'Land';
$s_address = 'Adres';
$s_city = 'Plaats';
$s_zipcode = 'Postcode';
$s_telephone = 'Telefoonnr.';
$s_fax = 'Fax';
$s_postaddress = 'Bezorg adres';
$s_postcity = 'Bezorg plaats';
$s_postzipcode = 'Bezorg postcode';
$s_default_language = 'Standaard taal voor nieuwe gebruikers';
$s_limits = 'Limieten';

$s_choose_client = 'Kies een klant waarvan u de items wilt zien (of kies alle klanten)';
$s_all_clients = 'Alle klanten';

# application field resources #
$s_application = 'Applicatie';
$s_application_name = 'Applicatienaam';
$s_application_description = 'Omschrijving';
$s_application_registered = 'Registreerd aan';
$s_application_version = 'versie';
$s_application_picture = 'Applicatie afbeelding (Splash)';
$s_application_image = 'Applicatie pictogram';
$s_startup_message = 'Melding bij opstarten';
$s_startup_message_formcolor = 'Achtergrondkleur van opstartmelding';
$s_helpnr = 'Helpnummer';
$s_helpfile = 'Helpbestand';
$s_searchoptions = 'Zoekopties';
$s_website = 'Website URL';
$s_ftpserver = 'FTP server';
$s_ftpuser = 'FTP gebruiker';
$s_ftppassword = 'FTP wachtwoord';

$s_apps_choose_from_list = 'Om met een applicatie te werken kiest u er een uit onderstaande lijst.';
$s_apps_non_available_header = 'Geen autorisatie';
$s_apps_non_available = 'U bent voor geen applicatie geautoriseerd, neem contact op met uw systeembeheerder.';

$s_test_connection = 'Test verbinding';
$s_save_settings = 'Instellingen opslaan';
$s_go = 'GO';

# database field resources #
$s_database_list = 'Database overzicht';
$s_db_description = 'Naam';
$s_databasename = 'Database';
$s_host = 'Host';
$s_databasetype = 'Database type';
$s_rootusername = 'Rootgebruiker';
$s_rootpassword = 'Rootwachtwoord';
$s_database = 'Database';
$s_loginwithuseraccount = 'Gebruik useraccount om in te loggen in database';
$s_errortablename = 'Foutentabelnaam';
$s_errorcodecolumnname = 'Foutcode kolomnaam';
$s_errormessagecolumnname = 'Foutbericht kolomnaam';

# subset field resources #
$s_subsets = 'Deelverzamelingen';



# table field resources #
$s_tables = 'Tabellen';



# tag resources #
$s_add_a_tag = 'Tag toevoegen';
$s_tag_already_exists = 'Deze tag bestaat al';
$s_delete_tag = 'Verwijder tag';
$s_view_tagged_items = 'Bekijk items met deze tag';


# kenniskunde resources #

$s_knowledgetics = 'Kenniskunde';

$s_kk_overview = 'Kenniskunde overzicht';
$s_facttypediagrams = 'Feittypediagrammen';

$s_ftds = 'FTD\'s';


# templates resources #
$s_templates_list = 'Lijst met Prefab-applicaties';

# form field resources
$s_forms = 'Formulieren';
$s_pieces = 'stuks';

$s_formname = 'Formulier';
$s_databaseid = 'Database';
$s_tablename = 'Tabelnaam';
$s_filter = 'Filter';
$s_pagecolumncount = 'Aantal kolommen';
$s_webpageurl = 'Webpagina URL';
$s_defaultviewtype = 'Standaard formuliertype';
$s_showrecordsasconstructs = 'showrecordsasconstructs';
$s_captioncolumnname = 'Kolomnaam van detailinfo form.';
$s_sortcolumnname = 'Sorteer op kolom';
$s_searchcolumnname = 'Zoek op kolom';
$s_panelhelptext = 'Formulier helptekst';
$s_preload = 'preload';
$s_newrecordinformview = 'Toon nieuw in formview?';
$s_selectview = 'Selectie view';
$s_selectcolumnnames = 'Selectie kolomnamen';
$s_selectshowcolumns = 'Selectie presentatie kolomnamen';
$s_selectquery = 'Selectie query';
$s_show_record_number = 'Toon aantal items';

$s_database_script = 'database script';
$s_axmls_script = 'ADODB XML Schemas';
$s_backup_restore = 'Backup/Restore van klant database';
$s_backup = 'Backup';
$s_restore = 'Restore';
$s_show_page_stats = 'Toon pagina stats';

# form permission resources
$s_autorisation = 'Autorisatie';

# page field resources
$s_pages = 'pagina\'s';
$s_page = 'pagina';

# container resources
$s_navigation = 'Navigatie';

# Webpage, Webparam resources
$s_webpage_list = 'Web pagina\'s';
$s_webpages_desc = 'Hieronder ziet u alle webpagina\'s van uw website (waartoe u toegang heeft).
U kunt webpagina\'s toevoegen, wijzigen en verwijderen.';
$s_no_webpage_exists = 'U bent niet geautoriseerd voor het wijzigen van een webpagina of er zijn geen webpagina\'s beschikbaar.';
$s_webpage_name = 'Pagina naam';
$s_webpage_title = 'Titel';
$s_languages = 'Talen';
$s_content = 'Inhoud';
$s_edit_webpage = 'Wijzig webpagina';
$s_webparam_name = 'Parameter';

# Webmenu resources
$s_menuid = 'Menuitem ID';
$s_menuname = 'Menuitem naam';
$s_url = 'URL';
$s_pagename = 'Paginanaam';
$s_parentmenuid = 'Is onderdeel van menu(item)';
$s_target = 'Doel/target';
$s_seo_keywords = 'SEO keywords';
$s_seo_keywords_help = '(keywords scheiden door spatie)';

# record limit
$s_recordlimit_label = 'Items per pagina';
$s_recordlimit_hint = 'Aantal items dat per keer wordt getoond';
$s_navigate_first = 'eerste';
$s_navigate_prev = 'vorige';
$s_navigate_next = 'volgende';
$s_navigate_last = 'laatste';
$s_navigate_first_hint = 'Toon de eerste pagina';
$s_navigate_prev_hint = 'Toon de vorige pagina';
$s_navigate_next_hint = 'Toon de volgende pagina';
$s_navigate_last_hint = 'Toon de laatste pagina';

$s_filter_title = 'Filter items';
$s_filter_cancel = 'Toon alle items (filter opheffen)';
$s_filter_cancel_short = 'Filter opheffen';

# Refresh resources
$s_refreshinterval_label = 'Ververs scherm';
$s_n_seconds = '%s sec';
$s_n_minutes = '%s min';
$s_never_refresh = 'nooit';

# image resources #
$s_image_view = 'toon afbeelding';
$s_image_view_hint = 'Toon afbeelding in een nieuw venster';
$s_image_view_normal_size = 'Toon op ware grootte';
$s_image_normal_size = 'ware grootte';
$s_image_delete = 'Verwijder afbeelding';
$s_image_add_new = 'Afbeelding toevoegen';
$s_image_file_size = 'Bestands grootte';
$s_image_edit = 'wijzig afbeelding';
$s_image_edit_hint = '';
$s_image_select_button = 'afbeeldinglijst';
$s_image_select = 'selecteer een afbeelding';
$s_image_select_hint = 'Open lijst met afbeeldingen in een nieuw venster.';
$s_image_upload_text = 'U kunt hier een of meerdere afbeeldingen uploaden. Zoek uw afbeelding met de knop \'Bladeren...\' (max. 5 afbeeldingen) en druk op Verzenden.';
$s_pick_image = 'Kies een afbeelding uit onderstaande lijst.';
$s_no_images = 'Er zijn geen afbeeldingen.';
$s_selected_image = 'Geselecteerde afbeelding';
$s_image_preview = 'Voorbeeld';
$s_choose_image = 'Kies&nbsp;afbeelding';
$s_upload_images = 'Afbeeldingen&nbsp;uploaden';

# files resources #
$s_pick_file = 'Kies een bestand uit onderstaande lijst.';
$s_choose_file = 'Kies&nbsp;bestand';
$s_upload_files = 'Bestanden&nbsp;uploaden';
$s_no_files = 'Er zijn geen bestanden';
$s_selected_file = 'Geselecteerd bestand';

# item resources #
$s_no_item = 'geen';
$s_item = 'item';
$s_n_items = 'items';
$s_all = 'Alle';
$s_show_all_items = 'Alle %s';  # default %s value is s_items #
$s_add_item = 'Voeg een %s toe'; # default %s value is s_item #
$s_add_item_to_list = 'Voeg een %s toe aan deze lijst'; # default %s value is s_item #
$s_add_item_after_save = 'Opslaan en nieuw %s toevoegen'; # default %s value is s_item #
$s_add_detailitem_after_save = 'Opslaan en %s toevoegen';
$s_list_is_empty = 'De lijst met \'%s\' is nog leeg.';  # default %s value is s_items #
$s_you_can_add_to_list = 'U kunt nu een %s aan deze lijst toevoegen.';  # default %s value is s_item #
$s_all_items = 'Alle items';
$s_click_and_edit_item = 'Klik op een item om de gegevens te wijzigen.';
$s_click_and_view_item = 'Klik op een item om de gegevens te bekijken.';
$s_no_item_selected = 'Er zijn geen records geselecteerd.';
$s_confirmation = 'Bevestigen';
$s_confirm_delete_1_tag = 'Weet u zeker dat u de tag \'%s\' wilt verwijderen?';
$s_confirm_delete_item = 'Weet u zeker dat u dit item wilt verwijderen?';
$s_confirm_delete_item_plural = 'Weet u zeker dat u deze __count__ items wilt verwijderen?';
$s_edit_item = '%s wijzigen';
$s_view_item = '%s bekijken';
$s_add_an_item = '%s toevoegen';
$s_add_new_item = 'Nieuw %s item toevoegen';
$s_clone_an_item = '%s klonen';
$s_clone_item = "Kloon dit item";
$s_view_details = 'Bekijk';
$s_item_deleted = 'item verwijderd';
$s_items_deleted = '%s items verwijderd';
$s_changes_saved = 'wijzigingen opgeslagen';
$s_item_added = 'item toegevoegd';
$s_details = 'details';
$s_no_item_selected = "U heeft geen item geselecteerd.";
$s_more_than_one_item_selected = "U heeft meer dan een item geselecteerd.";
$s_error = "Fout";

# Search form #
$s_search				= 'Zoeken';
$s_search_active        = 'Zoek';
$s_search_title  		= 'Zoek naar een bepaald';
$s_search_record  		= 'record';
$s_search_column_label  = 'Zoek in';
$s_search_value_label   = 'Zoek naar de waarde';
$s_search_advanced      = 'uitgebreid zoeken';
$s_filtered             = 'gefilterd!';
$s_filtered_hint        = 'Klik op \'Alle %s\' om deze zoekactie op te heffen';
$s_cancel_filtered      = 'Filter opheffen';
$s_no_records_found     = 'Er zijn geen items gevonden die voldoen aan uw zoekcriterium';
$s_alter_search         = 'Wijzig uw zoekterm of klik op \'Alle %s\' om alle items te tonen.';
$s_no_records_found_pin = 'Er zijn geen items gevonden die voldoen aan uw pins.';
$s_alter_pin            = 'Verwijder een of meerdere gepinde items.';
$s_filtered_pin         = 'alleen gepinde items!';
$s_filtered_hint_pin    = 'Klik op \'Verwijder alle pins\' om alle items te zien.';
$s_search_result_title	= 'Zoekresultaat';
$s_searchform_title = 'Zoeken naar gegevens';
$s_searchall_title = 'Alle kolommen';
$s_case_sensitive = 'Hoofdlettergevoelig';
$s_search_fullword = 'Zoek op geheel woord';
$s_search_fromstart = 'Zoek vanaf begin';
$s_new_search = 'Nieuwe zoekactie';
$s_new_search_hint = 'Maak zoekvelden leeg en verwijder gepinde items';
$s_toggle_search_panel = 'Klap het zoekpaneel open of dicht';
$s_search_advanced_options = 'Toon geavanceerde opties';
$s_search_now = 'Zoeken';
$s_no_search_available = 'geen zoekfunctie beschikbaar';
$s_column = 'Kolom';
$s_value = 'Waarde';
$s_no_pinned_items = 'geen items';
$s_pinned_items_title = 'Gepinde items';
$s_delete_pinned_items = 'Verwijder alle gepinde items';
$s_delete_pin = 'Verwijder pin';
$s_pin_this_record = 'Pin dit item';
$s_search_first_text = 'U dient eerst te zoeken, voordat gegevens worden getoond.';

$s_search_this_function = 'Zoek in deze functie';
$s_search_this_application = 'Zoek in de hele applicatie';

$s_search_result		= 'Zoekresultaat';

$s_maintenance          = 'onderhoud';
$s_maintenance1         = 'om';
$s_maintenance2         = 'uur';
$s_maintenance3         = 'Hoe lang';

# import export #
$s_import_export = 'import/export';
$s_import = 'Import';
$s_export = 'Export';
$s_import_action = 'Importeren';
$s_export_action = 'Exporteren';
$s_export_no_permission = 'U heeft niet genoeg rechten om gegevens te exporteren';
$s_import_no_permission = 'U heeft niet genoeg rechten om gegevens te importeren';
$s_import_export_items = '%s importeren/exporteren';

#
$s_check_webpage = 'Controleer webpagina';
$s_seperate_window = 'in een apart venster';
$s_check_webpage_hint = 'Bekijk deze data live op uw eigen website';

# button labels #
$s_but_ok = 'OK';
$s_but_save = 'Opslaan';
$s_but_save_hint = 'Huidige gegevens opslaan';
$s_but_save_and_pin = 'Opslaan en pinnen';
$s_but_save_and_pin_hint = 'Huidige gegevens opslaan en vastpinnen';
$s_but_save_and_new = 'Opslaan en Nieuw';
$s_but_back = 'Terug';
$s_but_back_hint = 'Terug naar het vorige formulier';
$s_but_cancel = 'Annuleren';
$s_but_cancel_hint = 'Terug naar het vorige formulier';
$s_but_edit = 'Bewerken';
$s_but_edit_hint = 'Bewerk het huidige item';
$s_but_close = 'Sluiten';
$s_but_close_hint = 'Sluit het formulier af';
$s_but_new = 'Nieuw item';
$s_but_new_hint = 'Nieuw item aanmaken';
$s_but_delete = 'Verwijder';
$s_but_delete_hint = 'Geselecteerde gegevens verwijderen';
$s_but_refresh = 'Vernieuwen';
$s_but_refresh_hint = 'Informatie vernieuwen';
$s_but_find = 'Zoeken';
$s_but_find_hint = 'Zoek naar bepaalde gegevens';
$s_but_copynew = 'Maak kopie';
$s_but_copynew_hint = 'Kopie maken van geselecteerd gegevens';
$s_but_select = 'Selecteren';
$s_but_select_hint = 'Selecteerd de huidige keuze';
$s_but_browse = 'Bladeren';
$s_but_browse_hint = 'Blader naar een bepaald bestand.';
$s_but_upload = 'Verzenden';
$s_but_working = 'bezig...';
$s_but_add = 'Voeg toe';
$s_but_search = 'Zoeken';
$s_but_search_hint = 'Zoeken naar bepaalde gegevens';
$s_but_export = 'Exporteer';
$s_but_import = 'Importeer';
$s_but_preview = 'Toon op scherm';
$s_but_download = 'Download';
$s_but_pinrecord = 'Pin vast';
$s_but_pinrecord_hint = 'Geselecteerde gegevens vastpinnen';
$s_but_bulkchange = 'Multi-wijziging';
$s_but_bulkchange_hint = 'Multi-wijziging';
$s_but_savedsearches = 'Zoekhistorie';
$s_bigsearch_placeholder = 'Typ om te zoeken';
$s_add_logbook_entry = 'Schrijf een bericht voor teamleden';

$s_but_nav_first = '<<';
$s_but_nav_last  = '>>';
$s_but_nav_prev  = '<';
$s_but_nav_next  = '>';

# designer functions #
$s_what_now = 'Wat wilt u doen?';
$s_where_to_now = 'Waar wilt u naar toe?';
$s_add_client = 'Voeg een klant toe...';

# Language resources #
$s_choose_language = 'Kies taal';

# Errors and warnings #
$s_go_back_after_insert_error = 'Wijzig uw gegevens en probeer opnieuw';
$s_form_not_found = 'Formulier is niet gevonden! Probeer opnieuw.';
$s_page_not_found = 'Deze pagina is momenteel niet beschikbaar.';
$s_cannot_determine_tables = 'De tabellen van de database kunnen niet worden vastgesteld.';
$s_cannot_connect_database = 'Er kon geen verbinding met de database worden gemaakt. Controleer uw gegevens en probeer opnieuw.';
$s_cannot_insert_database = 'Polaris kon uw databasegegevens niet opslaan.';
$s_cannot_update_record = 'De gegevens konden niet worden bijgewerkt';
$s_invalid_email = 'Dit emailadres is niet geldig.';
$s_error_512 = 'Uw actie kan niet worden uitgevoerd.';
$s_error_1062 = 'Uw actie kan niet worden uitgevoerd.';

# Language names #
$s_lang_nl = 'Nederlands';
$s_lang_fr = 'Frans';
$s_lang_en = 'Engels';
$s_lang_de = 'Duits';
$s_lang_se = 'Zweeds';
$s_lang_it = 'Italiaans';
$s_lang_aa = 'Generiek';
