<?php header('Content-Type: application/json; charset=utf-8');
include_once("lang_api.php");
?>
{
    "date_input": {
        "month_names" : <?php echo lang_getjson('month_names') ?>,
        "short_month_names" : <?php echo lang_getjson('short_month_names') ?>,
        "short_day_names" : <?php echo lang_getjson('short_day_names') ?>
    },
    "plr" : {
        "yes": <?php echo lang_getjson('yes') ?>,
        "no": <?php echo lang_getjson('no') ?>,
        "ok": <?php echo lang_getjson('but_ok') ?>,
        "cancel": <?php echo lang_getjson('but_cancel') ?>,
        "close": <?php echo lang_getjson('but_close') ?>,
        "view": <?php echo lang_getjson('view_details') ?>,
        "changes_not_saved": <?php echo lang_getjson('changes_not_saved') ?>,
        "long_operation": <?php echo lang_getjson('long_operation') ?>,
        "insufficient_rights_cannot_update": <?php echo lang_getjson('insufficient_rights_cannot_update') ?>,
        "password_strengths": <?php echo lang_getjson('password_strengths') ?>,
        "error": <?php echo lang_getjson('error') ?>,
        "polaris_maintenance": <?php echo lang_getjson('polaris_maintenance') ?>,
        "dont_close_polaris": <?php echo lang_getjson('dont_close_polaris') ?>,
        "save_record": <?php echo lang_getjson('save_record') ?>,
        "enter_required_fields": <?php echo lang_getjson('enter_required_fields') ?>,
        "please_wait": <?php echo lang_getjson('please_wait') ?>,
        "no_item_selected": <?php echo lang_getjson('no_item_selected') ?>,
        "more_than_one_item_selected": <?php echo lang_getjson('more_than_one_item_selected') ?>,
        "required_fields": <?php echo lang_getjson('required_fields') ?>,
        "confirmation": <?php echo lang_getjson('confirmation') ?>,
        "confirm_delete_item": <?php echo lang_getjson('confirm_delete_item') ?>,
        "confirm_delete_item_plural": <?php echo lang_getjson('confirm_delete_item_plural') ?>,
        "changes_saved": <?php echo lang_getjson('changes_saved') ?>
    },
    "validation": {
        "enter_valid_date": <?php echo lang_getjson('enter_valid_date') ?>,
        "enter_valid_elf_proef": <?php echo lang_getjson('elf_proef') ?>,
        "enter_valid_bsn": <?php echo lang_getjson('bsn_check') ?>,
        "phone_number_check": <?php echo lang_getjson('phone_number_check') ?>,
        "person_older_than_10": <?php echo lang_getjson('person_older_than_10') ?>,
        "invalid_email": <?php echo lang_getjson('invalid_email') ?>,
        "housenumber_check": <?php echo lang_getjson('housenumber_check') ?>,
        "incorrect_field": <?php echo lang_getjson('incorrect_field') ?>,
        "incorrect_field_plural": <?php echo lang_getjson('incorrect_field_plural') ?>
    }
}