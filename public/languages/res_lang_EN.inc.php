<?php
$_locale = 'en_en';
$_rss_locale = 'en-en';
$_date_format_placeholder = 'MM-DD-JJJJ';

# Polaris designer resources #
$s_polaris = 'Polaris';
$s_pagetitle = 'Polaris - Platform for Webapplications';
$s_polaris_main = 'Polaris';
$s_polaris_about = 'About Polaris';
$s_polaris_apps = 'Applications';
$s_polaris_designer = 'Manager';
$s_polaris_system_settings = 'System settings';
$s_polaris_overview = 'start';

# general resources #
$s_yes = 'Yes';
$s_no = 'No';
$s_via = 'Via';
$s_your_location = 'You are here';
$s_welcome = 'Welcome';
$s_welcome_to = 'Welcome to';
$s_all_applications = 'All applications';
$s_your_applications = 'Your applications';
$s_loggedinas = 'Logged in as';
$s_click_to_start = 'Click to start the application';
$s_of = 'of';
$s_until = '-';
$s_items = 'Items';
$s_your_functions = 'Your functions';
$s_go_to = 'Go to';
$s_properties = 'Properties';
$s_step = 'Step';
$s_check_all = 'Check all';
$s_check_none = 'Check none';
$s_check_all_short = 'All';
$s_check_none_short = 'None';
$s_functions = 'Functions';
$s_required_fields = 'The following fields are mandatory';
$s_connection_successful = 'Database connection was successful!';
$s_connection_not_successful = 'Database connection failed';
$s_login_message = 'With this login form, system administrator can get access to Polaris (the webbased administration and content management system).';
$s_only_licenced_users_message = 'This webservice is provided for authorised Polaris users.';
$s_create = 'Create';
$s_new = 'new';
$s_quick_navigation = 'Quick Menu';
$s_webstatistics = 'Webstatistics';
$s_new_polaris_features = 'Have you seen this new Polaris function?';
$s_separate_screen_hint = 'Show form in seperate window';
$s_read_more = 'read more';
$s_move_up = 'Move one row up';
$s_move_down = 'Move one row down';
$s_maximize_screen_hint = 'Maximize form';
$s_click_to_reply = 'Click to reply';
$s_send_email = 'Send email';
$s_value = 'Value';
$s_key_column = 'Key field';
$s_key_column_hint = 'Key field: the value of the key field(s) identifies the current record';
$s_orderindex = 'Order index';
$s_visible = 'Visible';
$s_enter_your_account = 'Please enter your account info to logon.';
$s_calendar = 'Calendar';
$s_remember_on_this_computer = 'Remember on this computer';
$s_maximize_screen = 'maximize';
$s_maximize_screen_hint = 'Maximize form';
$s_separate_screen = 'seperate';
$s_separate_screen_hint = 'Show form in seperate window';
$s_cancel_sorting = 'Cancel sorting';
$s_pick_items = 'Pick items';
$s_no_items = 'No items';
$s_apply_filter = 'Apply filter';
$s_set_focus = 'Set focus';
$s_please_wait = 'Please wait';
$s_start_new_polaris_session = 'Start new Polaris session';
$s_week = 'week';
$s_day = 'day';
$s_ready_working = 'Finished with this application?';
$s_logoff_text = 'Vergeet niet op <strong>LOG UIT</strong> te klikken, wanneer u klaar bent met Polaris. Hiermee voorkomt u dat onbevoegde personen met uw applicatie kunnen werken.';
$s_unknown = 'unknown';
$s_searchform_title = 'Search for items';
$s_case_sensitive = 'Case sensitive';
$s_search_fullword = 'Search on full word';
$s_search_fromstart = 'Search from start';
$s_new_search = 'New search';
$s_new_search_hint = 'Clear search fields and delete pins';
$s_search_now = 'Search now';
$s_no_search_available = 'no search available';
$s_column = 'Field';
$s_value = 'Value';
$s_no_pinned_items = 'no pins';
$s_pinned_items_title = 'Pinned items';
$s_delete_pinned_items = 'Delete all pins';
$s_delete_pin = 'Delete pin';
$s_pin_this_record = 'Pin this record';
$s_search_first_text = 'You need to search first, before items are displayed.';
$s_month_names = "January_February_March_April_May_June_July_August_September_October_November_December";
$s_short_month_names = "Jan_Feb_Maa_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec";
$s_short_day_names = "Su_Mo_Tu_We_Th_Fr_Sa";
$s_changes_not_saved = "You haven't saved your changes.<br />Do you want to save the record?";
$s_long_operation = "This action can take a while... Are your sure you wan't to continue?";
$s_insufficient_rights_cannot_update = "U heeft niet genoeg rechten om deze gegevens te wijzigen.";
$s_password_strengths = "Blanco_Zeer zwak_Zwak_Gemiddeld_Sterk_Zeer sterk";
$s_polaris_maintenance = "Polaris is <span id=\"duration_time\">?</span> niet beschikbaar. <br/>Reden: __reason__";
$s_dont_close_polaris = "U hoeft Polaris niet af te sluiten.";
$s_save_record = "Do you want to save this item?";
$s_enter_required_fields = "Please fil-in the required fields.";
$s_please_wait = "One moment please...";
$s_no_item_selected = "You did not select an item.";
$s_more_than_one_item_selected = "You selected more than one item.";
$s_print = 'Print&hellip;';
$s_none = 'none';
$s_printersettings = 'Printer settings';
$s_switch_to = 'Switch to';

# validations
$s_enter_valid_date = "Enter a valid date (dd-mm-yyyy).";
$s_phone_number_check = "A phonenumber consists of at least 10 digits";
$s_person_older_than_10 = "Only people of 10 years or older are permitted";
$s_housenumber_check = "A Dutch house number can only contain numbers (use the field Addition for other characters).";
$s_incorrect_field = "You entered a field incorrectly.";
$s_incorrect_field_plural = "You entered __count__ fiels incorrectly.";
$s_elf_proef = "This value does not meet the elfproef.";
$s_bsn_check = "This value does not meet the BSN check.";
$s_no_whitespace = "No white space please.";

# object actions (keep short) #
$s_action = 'action';
$s_edit = 'edit';
$s_view = 'view';
$s_clone = 'clone';
$s_delete = 'delete';
$s_show_users = 'show users';
$s_show_pages = 'show pages';
$s_published = 'Published';
$s_publish = 'Publish';
$s_save_publish = 'Opslaan &amp; Publiceer';
$s_select = 'select';
$s_open = 'open';
$s_extra = 'extra';
$s_clean = 'clean';
$s_copy = 'copy';

$s_applications = 'Applications';
$s_designer = 'Designer';
$s_ctrlpanel = 'Control panel';
$s_help = 'Help';
$s_logout = 'Logout';
$s_login = 'Login';

$s_configuration = 'Configuration';
$s_system_management = 'System management';
$s_your_application = 'Your application';

# modules #
$s_modules = 'Management';
$s_modules_system = 'System Management';
$s_mod_application_manager = 'Application manager';
$s_mod_user_manager = 'User manager';
$s_mod_db_manager = 'Database manager';
$s_mod_backup_restore = 'Backup &amp; Restore';
$s_mod_client_manager = 'Client manager';
$s_mod_system_settings = 'System settings';
$s_mod_template_manager = 'Prefab-applications';

$s_user_list = 'User list';
$s_client_list = 'Client list';
$s_application_list = 'Application list';
$s_your_applications = 'Your Applications';

# user field resources #
$s_user_account_of = 'User account of';
$s_group_properties_of = 'Properties of the group ';
$s_create_membership = 'Create membership for this member';
$s_create_members = 'Make user(group)s part of this group';

$s_create_membership_for = 'Create membership for';
$s_create_group_membership = 'Create membership for this group';
$s_autocreatemembership = 'New members automatically<br /> part of this group?';
$s_is_member_of = 'is member of the following groups';
$s_no_users_exists = 'No users have been created.';
$s_user_has_no_memberships = 'This user has no memberships.';
$s_group_has_no_members = 'This group has no members.';
$s_cancel_membership = 'Cancel membership';
$s_registered_user = 'Current Member Login';
$s_memberships_of = 'Memberships of';
$s_members_of = 'Members of';
$s_add_selected = 'Add selected items';
$s_add_all = 'Add all items';
$s_remove_selected = 'Remove selected items';
$s_remove_all = 'Remove all items';

$s_user = 'User';
$s_username = 'User name';
$s_fullname = 'Full name';
$s_password = 'Password';
$s_description = 'Description';
$s_email_address = 'Email address';
$s_change_password_next_logon = 'Change password with next login';
$s_user_cannot_change_password = 'User cannot change password';
$s_password_never_expires = 'Password never expires';
$s_account_disabled = 'Account disabled';
$s_last_login_date = 'Last login date';
$s_login_count = 'Login count';
$s_client_admin = 'User is Client administrator';
$s_root_admin = 'User is Polaris administrator';
$s_change_password = 'Change password';
$s_change_your_password = 'Change your Polaris password';
$s_new_password = 'New password';
$s_retype_password = 'Retype password';
$s_login_text = 'Please enter your credentials to login.';
$s_forgot_password = 'Forgot password?';
$s_no_account = 'Don\'t have an account?';
$s_create_account = 'Create an account';
$s_change_password_text = 'Before you can continue with Polaris, you have to change your password.';
$s_password_not_correct = 'The password was not correct. Make sure you enter the password twice and try again.';
$s_login_not_correct = 'Username and password are not correct. Please try again...';
$s_polarisinstance = 'Polaris instance';
$s_auth_type = 'Authorisatie type';
$s_user_language = 'Nützer Sprache';
$s_loginerror_1 = 'The combination of your username and password is not correct.';
$s_loginerror_2 = '';
$s_loginerror_3 = '';
$s_loginerror_4 = 'Another user is logged on with this account. Please logout of the other instance and try again.';

$s_group = 'Group';
$s_groupname = 'Group name';
$s_account = 'Account';
$s_type = 'Type';
$s_name = 'Name';
$s_last_login = 'Last login';
$s_action = 'Action';
$s_logins = 'Number of logins';
$s_autocreatemembership = 'Automatically make new accounts <br /> member of this group?';
$s_language = 'Language';

$s_usergroup_desc = 'Here you see all user and group accounts which are part of your Polaris administration.
You can add, edit and delete accounts.';
$s_new_user_desc = 'Please provide the user\'s personal information and account settings.';
$s_edit_user_desc = 'Here you can edit the current account. You can also create group memberships.';
$s_new_group_desc = 'Please provide the group\'s information.';
$s_edit_group_desc = 'Here you can edit the current group account. You can also make memberships for this group.';

# User settings field resources #
$s_user_settings = 'User settings';
$s_user_profile = 'Profile';

# client field resources #
$s_client_account_of = 'Client account of';

$s_client_name = 'Name';
$s_client = 'Client';
$s_country = 'Country';
$s_address = 'Address';
$s_zipcode = 'Postal code';
$s_telephone = 'Telephone #';
$s_fax = 'Fax';
$s_postaddress = 'Delivery - address';
$s_postcity = 'Delivery - city';
$s_postzipcode = 'Delivery - postal code';
$s_default_language = 'Default language for new users';
$s_limits = 'Limits';

$s_choose_client = 'Choose a client to see its items (or choose all clients)';
$s_all_clients = 'All clients';

# application field resources #
$s_application = 'Application';
$s_application_description = 'Title';
$s_application_registered = 'Registered to';
$s_application_version = 'version';

$s_apps_choose_from_list = 'To start an application, please choose one from the list below.';
$s_apps_non_available    = 'You are not autorized for an application, please contact your system administrator.';

$s_test_connection = 'Test connection';
$s_save_settings = 'Save settings';
$s_go = 'GO';

# form field resources
$s_forms = 'Forms';

$s_formname = 'Form';
$s_databaseid = 'Database';
$s_tablename = 'Table name';
$s_filter = 'Filter';
$s_pagecolumncount = 'Page column count';
$s_webpageurl = 'Webpage URL';
$s_defaultviewtype = 'Default viewtype';
$s_showrecordsasconstructs = 'Show records as constructs';
$s_captioncolumnname = 'Column name of the detail form.';
$s_sortcolumnname = 'Sort on column';
$s_searchcolumnname = 'Search on column';
$s_panelhelptext = 'Form help text';
$s_preload = 'preload';
$s_newrecordinformview = 'Show new record in form view?';
$s_selectview = 'Select view';
$s_selectcolumnnames = 'Select colomn names';
$s_selectshowcolumns = 'Select show columns';
$s_selectquery = 'Select query';

$s_database_script = 'database script';
$s_axmls_script = 'ADODB XML Schemas';
$s_backup_restore = 'Backup/Restore of client database';
$s_backup = 'Backup';
$s_restore = 'Restore';

$s_database_script = 'database script';
$s_axmls_script = 'ADODB XML Schemas';
$s_backup_restore = 'Backup/Restore van klant database';
$s_backup = 'Backup';
$s_restore = 'Restore';
$s_show_page_stats = 'Show page stats';

# form permission resources
$s_autorisation = 'Autorisatie';

# page field resources
$s_pages = 'pages';
$s_page = 'page';

# container resources
$s_navigation = 'Navigation';

# Webpage, Webparam resources
$s_webpage_list = 'Web pages';
$s_webpages_desc = 'Below you see all the web pages of your website (to which you have access).
You can add, change and delete webpages.';
$s_no_webpage_exists = 'You are not autorized for any webpage or there are no web pages available.';
$s_webpage_name = 'Page name';
$s_webpage_title = 'Title';
$s_languages = 'Languages';
$s_content = 'Content';
$s_edit_webpage = 'Edit web page';
$s_webparam_name = 'Parameter';

# Webmenu resources
$s_menuid = 'Menuitem ID';
$s_menuname = 'Menuitem naam';
$s_url = 'URL';
$s_pagename = 'Paginanaam';
$s_parentmenuid = 'Is onderdeel van menu(item)';
$s_target = 'Doel/target';
$s_seo_keywords = 'SEO keywords';
$s_seo_keywords_help = '(keywords scheiden door spatie)';

# record limit
$s_recordlimit_label = 'Show';
$s_recordlimit_hint = 'Number of records retrieved from the database';
$s_navigate_first = 'first';
$s_navigate_prev = 'previous';
$s_navigate_next = 'next';
$s_navigate_last = 'last';
$s_navigate_first_hint = 'Show first record page';
$s_navigate_prev_hint = 'Show previous record page';
$s_navigate_next_hint = 'Show next record page';
$s_navigate_last_hint = 'Show last record page';

$s_filter_title = 'Filter items';
$s_filter_cancel = 'Show all items (cancel filter)';
$s_filter_cancel_short = 'Cancel filter';

# Refresh resources
$s_refreshinterval_label = 'Refresh form';
$s_n_seconds = '%s sec';
$s_n_minutes = '%s min';
$s_never_refresh = 'never';

# image resources #
$s_image_view = 'view image';
$s_image_view_hint = 'View image in a new window';
$s_image_view_normal_size = 'View in normal size';
$s_image_normal_size = 'ware grootte';
$s_image_delete = 'Delete image';
$s_image_add_new = 'Add image...';
$s_image_file_size = 'File size';
$s_image_edit = 'Edit image';
$s_image_edit_hint = '';
$s_image_select_button = 'Image list';
$s_image_select = 'select an image';
$s_image_select_hint = 'Opens list with images in a new window';
$s_image_upload_text = 'You can upload one or more images. Browse for you image with the \'Browse...\' button (max. 5 image per upload) and press Send.';
$s_pick_image = 'Choose an image from the list below.';
$s_no_images = 'There are no images.';
$s_selected_image = 'Selected image';
$s_image_preview = 'Preview';
$s_choose_image = 'Choose&nbsp;image';
$s_upload_images = 'Upload&nbsp;images';

# files resources #
$s_pick_file = 'Kies een bestand uit onderstaande lijst.';
$s_choose_file = 'Kies&nbsp;bestand';
$s_upload_files = 'Bestanden&nbsp;uploaden';
$s_no_files = 'Er zijn geen bestanden';
$s_selected_file = 'Geselecteerd bestand';

# item resources #
$s_no_item = 'none';
$s_item = 'item';
$s_n_items = 'items';
$s_all = 'All';
$s_show_all_items = 'All %s';  # default %s value is s_items #
$s_add_item = 'Add %s'; # default %s value is s_item #
$s_add_item_to_list = 'Add %s to this list'; # default %s value is s_item #
$s_add_item_after_save = 'Save and add new %s'; # default %s value is s_item #
$s_add_detailitem_after_save = 'Save and add new %s';
$s_list_is_empty = 'The list with \'%s\' is empty.';  # default %s value is s_items #
$s_you_can_add_to_list = 'You can now add a new %s to this list.';  # default %s value is s_item #
$s_all_items = 'All items';
$s_click_and_edit_item = 'Click on an item to change its record.';
$s_click_and_view_item = 'Click on an item to view its record.';
$s_no_item_selected = 'There are no selected items.';
$s_confirmation = 'Confirm';
$s_confirm_delete_1_tag = 'Weet u zeker dat u de tag \'%s\' wilt verwijderen?';
$s_confirm_delete_item = 'Are you sure you want to delete this record?';
$s_confirm_delete_item_plural = 'Are you sure you want to delete these records?';
$s_edit_item = 'Change %s';
$s_view_item = 'View %s';
$s_add_an_item = 'Add %s';
$s_add_new_item = 'Add new %s';
$s_clone_an_item = 'Clone %s';
$s_clone_item = "Clone this item";
$s_view_details = 'View';
$s_item_deleted = 'item deleted';
$s_items_deleted = '%s items deleted';
$s_item_saved = 'changes saved';
$s_item_added = 'item added';
$s_changes_saved = 'Changes saved';
$s_details = 'details';
$s_no_item_selected = "Please select an item.";
$s_more_than_one_item_selected = "More than one item selected.";
$s_error = "Error";


# Search form #
$s_search			    = 'Search';
$s_search_active        = 'Search';
$s_search_title  	    = 'Search for a';
$s_search_record  		= 'record';
$s_search_column_label  = 'Search in';
$s_search_value_label   = 'Search for value';
$s_search_advanced      = 'advanced search';
$s_filtered             = 'filtered!';
$s_filtered_hint        = 'Click \'All items\' to cancel this search result';
$s_cancel_filtered      = 'Cancel Filter';
$s_no_records_found     = 'Your search did not match any items.';
$s_alter_search         = 'Change your search criteria  or click \'All %s\' to see all items.';
$s_no_records_found_pin = 'No items where found that match your pinned items.';
$s_alter_pin            = 'Delete one or more pinned items.';
$s_filtered_pin         = 'only pinned items!';
$s_filtered_hint_pin    = 'Click \'Delete all pinned items\' to view all records';
$s_search_result_title  = 'Searchresult';
$s_searchform_title = 'Search for information';
$s_searchall_title = 'All columns';
$s_case_sensitive = 'Case-sensitive';
$s_search_fullword = 'Search on whole word';
$s_search_fromstart = 'Search from start';
$s_new_search = 'New search';
$s_new_search_hint = 'Clear searchfields and delete pinned items';
$s_toggle_search_panel = 'Toggle searchpanel';
$s_search_advanced_options = 'Show advanced options';
$s_search_now = 'Search';
$s_no_search_available = 'search fuction not available';
$s_column = 'Column';
$s_value = 'Value';
$s_no_pinned_items = 'no items';
$s_pinned_items_title = 'Pinned items';
$s_delete_pinned_items = 'Delete all pinned items';
$s_delete_pin = 'Delete pin';
$s_pin_this_record = 'Pin this item';
$s_search_first_text = 'You need to search for information...';

$s_search_this_function = 'Search in this function';
$s_search_this_application = 'Search the complete application';

$s_search_result	    = 'search result';

$s_maintenance          = 'maintenance';
$s_maintenance1         = 'at';
$s_maintenance2         = 'hour';
$s_maintenance3         = 'Duration';

# import export #
$s_import_export = 'import/export';
$s_import = 'Import';
$s_export = 'Export';
$s_import_action = 'Import';
$s_export_action = 'Export';
$s_export_no_permission = 'You do not have the privilege to export data';
$s_import_no_permission = 'You do not have the privilege to import data';

#
$s_check_webpage = 'Check this webpage';
$s_seperate_window = 'in a new window';
$s_check_webpage_hint = 'View this data live on your own website';

# button labels #
$s_but_ok = 'OK';
$s_but_save = 'Save';
$s_but_save_hint = 'Save current record';
$s_but_cancel = 'Cancel';
$s_but_cancel_hint = 'Cancel changes and go to previous page';
$s_but_close = 'Close';
$s_but_close_hint = 'Go to previous page';
$s_but_new = 'New Record';
$s_but_new_hint = 'Add new record';
$s_but_delete = 'Delete';
$s_but_delete_hint = 'Delete selected records';
$s_but_refresh = 'Refresh';
$s_but_refresh_hint = 'Refresh information';
$s_but_find = 'Find';
$s_but_find_hint = 'Search for records';
$s_but_copynew = 'Make copy';
$s_but_copynew_hint = 'Make copy of selected record';
$s_but_select = 'Select';
$s_but_select_hint = 'Select the current choice';
$s_but_browse = 'Browse';
$s_but_browse_hint = 'Browse for a file.';
$s_but_upload = 'Upload';
$s_but_working = 'busy...';
$s_but_add = 'Add';
$s_but_search = 'Search';
$s_but_search_hint = 'Search for records';
$s_but_export = 'Export';
$s_but_import = 'Import';
$s_but_preview = 'Preview';
$s_but_download = 'Download';
$s_but_pinrecord = 'Pin';
$s_but_pinrecord_hint = 'Pin selected items';
$s_but_bulkchange = 'Bulk change';
$s_but_bulkchange_hint = 'Bulk change';
$s_but_savedsearches = 'Saved searches';
$s_bigsearch_placeholder = 'Search by keyword or phrase';
$s_add_logbook_entry = 'Write a message for team members';

$s_but_nav_first = '<<';
$s_but_nav_last  = '>>';
$s_but_nav_prev  = '<';
$s_but_nav_next  = '>';

# designer functions #
$s_what_now = 'What do you want to do now?';
$s_where_to_now = 'Where do you want to go now?';
$s_add_client = 'Add a client...';

# Language resources #
$s_choose_language = 'Choose language';

## Errors and warnings ##
$s_go_back_after_insert_error = 'Change your record and try again.';
$s_form_not_found = 'Could not find form! Please try again.';
$s_page_not_found = 'This page is currently not available.';
$s_cannot_determine_tables = 'Cannot determine the tables of the database.';
$s_cannot_connect_database = 'Cannot connect to database. Check your settings and try again.';
$s_cannot_insert_database = 'Polaris could not save your database-record.';
$s_cannot_update_record = 'The record could not be updated.';
$s_error_512 = 'Your action could not be executed.';
$s_error_1062 = 'Your action could not be executed.';

# Language names
$s_lang_nl = 'Dutch';
$s_lang_fr = 'French';
$s_lang_en = 'English';
$s_lang_de = 'German';
$s_lang_se = 'Swedish';
$s_lang_it = 'Italian';
$s_lang_aa = 'Generic';
