<?php

require_once '../includes/app_includes.inc.php';

/**
 * Create the Polaris object
 */
$polaris = new Polaris();
$polaris->initialize();

function curl($url,$params = array(),$is_coockie_set = false) {
    if(!$is_coockie_set) {
        /* STEP 1. let’s create a cookie file */
        $ckfile = tempnam ("/tmp", "CURLCOOKIE");

        /* STEP 2. visit the homepage to set the cookie properly */
        $ch = curl_init ($url);
        curl_setopt ($ch, CURLOPT_COOKIEJAR, $ckfile);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec ($ch);
    }

    $str = ''; $str_arr= array();
    foreach($params as $key => $value) {
        $str_arr[] = urlencode($key)."=".urlencode($value);
    }
    if(!empty($str_arr))
        $str = '?'.implode('&',$str_arr);

    /* STEP 3. visit cookiepage.php */

    $Url = $url.$str;

    $ch = curl_init ($Url);
    curl_setopt ($ch, CURLOPT_COOKIEFILE, $ckfile);
    curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);

    $output = curl_exec ($ch);
    return $output;
}

function TranslateMulti($text, $conversion = 'nl_to_de') {
    $_lines = explode('. ', $text);
    foreach($_lines as $_line) {
        $_results[] = Translate($_line, $conversion);
    }
    return implode('. ', $_results);
}

function Translate($word, $conversion = 'nl_to_de') {
    $word = str_replace('. ', ',, ', $word);
    $word = urlencode($word);
    $arr_langs = explode('_to_', $conversion);
    $url = "http://translate.google.com/translate_a/t?client=t&text=$word&hl=".$arr_langs[1]."&sl=".$arr_langs[0]."&tl=".$arr_langs[1]."&ie=UTF-8&oe=UTF-8&multires=1&otf=1&pc=1&trs=1&ssel=3&tsel=6&sc=1";

    $name_en = curl($url);

    $name_en = explode('"',$name_en);
    $result = $name_en[1];
    return $result;
}

?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<body>
<?php
mysql_set_charset('utf8');
$polaris->instance->Execute("SET NAMES UTF8;");
$_update = 'UPDATE plr_errormessages SET erroromschrijving_de = ? WHERE errorcode = ?';
$_sql = 'SELECT errorcode, erroromschrijving_nl from plr_errormessages';
$_rs = $polaris->instance->GetAll($_sql);
foreach($_rs as $_rec) {
    echo $_rec['erroromschrijving_nl']."\n";
    $_trans = TranslateMulti($_rec['erroromschrijving_nl']);
    echo "<br/>=> $_trans";
//    $polaris->instance->debug=true;
    $polaris->instance->Execute($_update, array($_trans, $_rec['errorcode']));
}
$_postUpdate =  "UPDATE plr_errormessages SET erroromschrijving_de = replace(erroromschrijving_de, '\\u0026 ', '&')";
$polaris->instance->Execute($_postUpdate);
?>
</body>
</html>