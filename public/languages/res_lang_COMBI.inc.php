<?php

# concatenation of resources #
#
# ******** DO NOT LOCALIZE ********* #
#
$s_new_group = "$s_new $s_group";
$s_new_user = "$s_new $s_user";
$s_new_client = "$s_new $s_client";
$s_new_application = "$s_new $s_application";

$s_make_new_group = "$s_create $s_new_group";
$s_make_new_user = "$s_create $s_new_user";
$s_make_new_client = "$s_create $s_new_client";
$s_make_new_application = "$s_create $s_new_application";

$s_import_export = "$s_import/$s_export";
#
# ***** END OF 'DO NOT LOCALIZE' ****** #
#
?>