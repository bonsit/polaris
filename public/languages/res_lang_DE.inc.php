<?php
$_locale = array('de_DE');
$_rss_locale = 'de-DE';
$_date_format_placeholder = 'TT-MM-JJJJ';

# Polaris designer resources #
$s_polaris = 'Polaris';
$s_pagetitle = 'Polaris - Plattform für Web-Anwendungen';
$s_polaris_main = 'Polaris';
$s_polaris_about = 'Über Polaris';
$s_polaris_apps = 'Anwendungen';
$s_polaris_designer = 'Anwendungsverwaltung';
$s_polaris_system_settings = 'System-Einstellungen';
$s_polaris_overview = 'Management';

# general resources #
$s_yes = 'Ja';
$s_no = 'Nein';
$s_via = 'Durch';
$s_your_location = 'Sie sind hier';
$s_welcome = 'Wilkommen';
$s_welcome_to = 'Wilkommen bei';
$s_all_applications = 'Home';
$s_your_applications = 'Ihre Anwendungen';
$s_loggedinas = 'Angemeldet als';
$s_click_to_start = 'Klicken Sie zum Starten der Anwendung';
$s_of = 'von';
$s_items = 'Zeilen';
$s_your_functions = 'Ihre Funktionen';
$s_go_to = 'Gehe zu';
$s_properties = 'Features';
$s_step = 'Schritt';
$s_check_all = 'alles ankreuzen';
$s_check_none = 'Nichts ankreuzen';
$s_check_all_short = 'Alles';
$s_check_none_short = 'Nichts';
$s_functions = 'Funktionen';
$s_required_fields = 'Die nachfolgenden Felder müssen ausgefüllt werden';
$s_connection_successful = 'Datenbank Verbindung ist erfolgreich!';
$s_connection_not_successful = 'Datenbank Verbindung ist nicht erfolgreich!';
$s_login_message = 'Mit Hilfe des unten stehenden Formular bekommen Systemadministratoren Zugang zu Polaris, der online Administration und dem Content Management Sytem.';
$s_only_licenced_users_message = 'Dieser Service is nur für registrierte Benutzer von Polaris gedacht.';
$s_create = 'machen';
$s_new = 'neue';
$s_quick_navigation = 'Schnelle Navigation';
$s_webstatistics = 'Webstatistiken';
$s_new_polaris_features = 'Kennen Sie die neuen Funktionen von Polaris schon?';
$s_read_more = 'Lesen Sie mehr';
$s_more = 'mehr';
$s_move_up = 'Eine Reihe nach oben verschieben';
$s_move_down = 'Eine Reihe nach unten verschieben';
$s_click_to_reply = 'Klicken Sie zum Verschicken der E-Mail';
$s_send_email = 'E-Mail schicken';
$s_value = 'Wert';
$s_key_column = 'Schlüsselfeld';
$s_key_column_hint = 'Schlüsselfelder: die Werte der Schlüsselfelder identifizieren den heutigen Rekord';
$s_orderindex = 'Reihenfolge des Index';
$s_visible = 'zu sehen';
$s_enter_your_account = 'Geben Sie Ihre Accountdaten an um sich an zu melden.';
$s_calendar = 'Kalender';
$s_remember_on_this_computer = 'Auf diesem Comuter speichern';
$s_maximize_screen = 'maximal';
$s_maximize_screen_hint = 'Das Formular maximieren';
$s_separate_screen = 'trennen';
$s_separate_screen_hint = 'Zeige Formular in einem separaten Fenster';
$s_cancel_sorting = 'Sortierung aufheben';
$s_pick_items = 'Zeilen auswählen';
$s_no_items = 'Keine Zeilen';
$s_apply_filter = 'Filter anwenden';
$s_set_focus = 'Set focus';
$s_please_wait = 'Bitte warten';
$s_start_new_polaris_session = 'Starten Sie eine neue Polaris-Sitzung';
$s_week = 'Woche';
$s_day = 'Tag';
$s_ready_working = 'Fertig mit arbeiten?';
$s_logoff_text = 'Denken Sie daran, <strong>LOG OUT</strong> zu wählen, wenn Sie fertig sind mit Polaris. Dies verhindert, dass Unbefugte mit Ihrer Anwendung arbeiten.';
$s_unknown = 'unbekannt';
$s_searchform_title = 'Suche nach Zeilen';
$s_case_sensitive = 'Groß-/Kleinschreibung';
$s_search_fullword = 'Nach ganzen Wörtern suchen';
$s_search_fromstart = 'Suche am Anfang starten';
$s_new_search = 'Neue Suche';
$s_new_search_hint = 'Suchfelder leeren und Heftungen löschen';
$s_search_now = 'Suchen';
$s_no_search_available = 'keine Suchfunktion zur Verfügung';
$s_column = 'Spalte';
$s_value = 'Wert';
$s_no_pinned_items = 'keine Heften';
$s_pinned_items_title = 'Heften';
$s_delete_pinned_items = 'Alle Heften entfernen';
$s_delete_pin = 'Heften entfernen';
$s_pin_this_record = 'Diesem Zeile anheften';
$s_search_first_text = 'Sie müssen zunächst eine Suche starten, damit Ihnen Ergebnisse angezeigt werden.';
$s_month_names = "Januar_Februar_März_April_Mai_Juni_Juli_August_September_Oktober_November_Dezember";
$s_short_month_names = "Jan_Feb_Mär_Apr_Mai_Jun_Jul_Aug_Sep_Okt_Nov_Dez";
$s_short_day_names = "So_Mo_Di_Mi_Do_Fr_Sa";
$s_changes_not_saved = "Sie haben noch ungesicherte Änderungen.<br /> Jetzt sichern?";
$s_long_operation = "Diese Aktion kann eine Weile dauern ... Möchten Sie fortfahren? ";
$s_insufficient_rights_cannot_update = "Sie sind nicht berechtigt diese Informationen zu bearbeiten.";
$s_password_strengths = "Blanko_Sehr schwach_Schwach_Mittel_Stark_Sehr stark";
$s_polaris_maintenance = 'Polaris ist <span id="duration_time">?</span> nicht verfügbar. <br/>Grund: __reason__';
$s_dont_close_polaris = "Sie brauchen Polaris nicht ab zu schließen.";
$s_save_record = "Wollen Sie diese Zeile sichern?";
$s_enter_required_fields = "Sie müssen alle verpflichteten Felder ausfüllen";
$s_please_wait = "Einen Moment bitte&hellip;";
$s_no_item_selected = "Sie haben kein Zeile ausgewählt.";
$s_more_than_one_item_selected = "Sie haben mehr als ein Zeile ausgewählt.";
$s_print = 'Drucken&hellip;';
$s_none = 'keine';
$s_printersettings = 'Druckereinstellungen';

# validations
$s_enter_valid_date = "Geben Sie hier ein gültiges Datum ein (tt-mm-jjjj).";
$s_phone_number_check = "Eine Telefonnummer besteht aus mindestens acht Zahlen";
$s_person_older_than_10 = "Es sind nur Personen von mindestens 10 Jahren zugelassen";
$s_housenumber_check = "Eine deutsche Hausnummerdarf nur Zahlen beinhalten, verwenden Sie für die Zusätze das vorgesehene Feld.";
$s_incorrect_field = "Sie haben ein Feld nicht richig ausgefüllt.";
$s_incorrect_field_plural = "Sie haben __count__ Felder nicht richtig ausgefüllt.";

# object actions (keep short) #
$s_action = 'Aktion';
$s_edit = 'ändern';
$s_view = 'ansehen';
$s_clone = 'klonen';
$s_delete = 'entfernen';
$s_show_users = 'Benutzer anzeigen';
$s_show_pages = 'Seite anzeigen';
$s_published = 'Veröffentlicht';
$s_publish = 'veröffentlichen';
$s_save_publish = 'Speichern &amp; veröffentlichen';
$s_select = 'auswählen';
$s_open = 'öffnen';
$s_extra = 'extra';
$s_clean = 'löschen';
$s_copy = 'kopieren';

$s_applications = 'Anwendungen';
$s_designer = 'Verwaltung';
$s_ctrlpanel = 'Einstellungen';
$s_help = 'Hilfe';
$s_logout = 'Ausloggen';
$s_login = 'Einloggen';

$s_configuration = 'Konfiguration';
$s_system_management = 'System Management';
$s_your_application = 'Ihre Anwendung';

# modules #
$s_modules = 'Verwaltungs Module';
$s_modules_system = 'System Module';
$s_mod_application_manager = 'Appikationenverwaltung';
$s_mod_user_manager = 'Benutzerverwaltung';
$s_mod_db_manager = 'Datenbankverwaltung';
$s_mod_backup_restore = 'Backup/Restore';
$s_mod_client_manager = 'Kundenverwaltung';
$s_mod_system_settings = 'System Einstellungen';
$s_mod_template_manager = 'vorgefertigte Applikationen';
$s_mod_site_generator = 'Applikationsgenerator';

$s_user_list = 'Benutzer Übersicht';
$s_client_list = 'Kundenübersicht';
$s_application_list = 'Überblick über die Anwendungen';
$s_your_applications = 'Ihre Anwendungen';
$s_sitegenerator_list = 'Websiteübersicht';

# user field resources #
$s_user_account_of = 'Benutzeraccount von';
$s_group_properties_of = 'Eigenschaften der Gruppe';
$s_create_membership = 'Diesen Benutzer zu einer Gruppe hinzufügen';
$s_create_members = 'Diesen Benutzer(Gruppen) zu der Gruppe hizufügen';
$s_create_membership_for = 'Mitgliedschaft anlegen für';
$s_create_group_membership = 'Diese Gruppe einer anderen Gruppe hinzufügen';
$s_is_member_of = 'ist Mitglied folgender Gruppen';
$s_no_users_exists = 'Es sind noch keine Benutzer angelegt.';
$s_user_has_no_memberships = 'Dieser Benutzer hat zu Zeit keine Mitgliedschaften.';
$s_group_has_no_members = 'Diese Gruppe hat zur Zeit keine Mitglieder.';
$s_cancel_membership = 'Mitgliedschaft aufheben';
$s_registered_user = 'Registrierter Benutzer';
$s_memberships_of = 'Mitgliedschaften von';
$s_members_of = 'Mitglied von';
$s_add_selected = 'Ausgewählte Elemente hinzufügen';
$s_add_all = 'Alle Elemente hinzufügen';
$s_remove_selected = 'Ausgewählte Elemente entfernen';
$s_remove_all = 'Alle Elemente entfernen';

$s_user = 'Benutzer';
$s_username = 'Benutzername';
$s_fullname = 'Vollständiger Name';
$s_password = 'Passwort';
$s_description = 'Umschreibung';
$s_email_address = 'E-Mail-Adresse';
$s_change_password_next_logon = 'Passwort beim nächsten Anmelden ändern';
$s_user_cannot_change_password = 'Kann der Benutzer das Passwort selber ändern';
$s_password_never_expires = 'Passwort bliebt immer gültig';
$s_account_disabled = 'Account gesperrt';
$s_last_login_date = 'Datum letztes Mal angemeldet';
$s_login_count = 'Anzahl der angemeldeten Male';
$s_client_admin = 'Benutzer ist Kundenverwalter';
$s_root_admin = 'Benutzer ist Polarisverwalter';
$s_change_password = 'Passwort ändern';
$s_change_your_password = 'Ändern Sie Ihr Polaris Passwort';
$s_new_password = 'Neues Passwort';
$s_retype_password = 'Passwort wiederholen';
$s_login_text = 'Geben Sie Ihre Account-Informationen an um sich anzumelden.';
$s_change_password_text = 'Bevor Sie fortfahren können, müssen Sie zuerst Ihr Passwort ändern.';
$s_password_not_correct = 'Das Passwort ist nicht korrekt eingegeben. Versuchen Sie es nochmals.';
$s_password_not_strong_enough = 'Das Passwort ist nicht sicher genug.';
$s_login_not_correct = 'Benutzername und Passwort sind nicht korrekt. Versuchen Sie es nochmals...';
$s_polarisinstance = 'Polaris instance';
$s_auth_type = 'Authorisatie type';
$s_user_language = 'Nützer Sprache';
$s_loginerror_1 = 'Die Kombination von Benutzername und Passwort ist nicht bekannt.';
$s_loginerror_2 = '';
$s_loginerror_3 = '';
$s_loginerror_4 = 'Ein anderer Benutzer ist mit diesem Account angemeldet. Loggen Sie sich zunächst dort aus und versuchen Sie es dann nochmals.';

$s_group = 'Gruppe';
$s_groupname = 'Gruppenname';
$s_account = 'Account';
$s_type = 'Type';
$s_name = 'Name';
$s_last_login = 'Letzter Login';
$s_action = 'Aktion';
$s_logins = 'Anzahl der Logins';
$s_autocreatemembership = 'Neue Mitglieder automatisch<br /> dieser Gruppe hinzufügen?';
$s_language = 'Sprache';

$s_usergroup_desc = 'Unten sehen Sie alle (Gruppen der) Benutzeraccounts, die Teil Ihrer Polarisadministration sind.
Sie können Accounts hinzufügen, verändern und entfernen.';
$s_new_user_desc = 'Unten können Sie die Daten eines neuen Benutzers eingeben.';
$s_edit_user_desc = 'Unten können Sie den aktuelle Account anpassen. Auch können Sie den Benutzer als Mitglied hinzufügen von
einer oder mehreren Gruppen.';
$s_new_group_desc = 'Unten können Sie die Daten der neuen Gruppe eingeben.';
$s_edit_group_desc = 'Unten können Sie die aktuelle Gruppe anpassen. Sie können auch Benutzer Mitglied der Gruppe werden lassen.';

# User settings field resources #
$s_user_settings = 'Benutzereinstellungen';
$s_user_profile = 'Benutzer';

# client field resources #
$s_client_account_of = 'Kundenaccount von';

$s_client = 'Kunde';
$s_name = 'Name';
$s_country = 'Land';
$s_address = 'Adresse';
$s_city = 'Ort';
$s_zipcode = 'Postleitzahl';
$s_telephone = 'Telefonnummer.';
$s_fax = 'Fax';
$s_postaddress = 'Lieferadresse';
$s_postcity = 'Lieferort';
$s_postzipcode = 'Lieferpostleitzahl';
$s_default_language = 'Standardsprache für neue Benutzer';
$s_limits = 'Limits';

$s_choose_client = 'Wählen Sie einen Kunden von dem Sie die Elemente sehen wollen (oder wählen Sie alle Kunden)';
$s_all_clients = 'Alle Kunden';

# application field resources #
$s_application = 'Anwendung';
$s_application_name = 'Anwendungsname';
$s_application_description = 'Umschreibung';
$s_application_registered = 'Registriert bei';
$s_application_version = 'Version';
$s_application_picture = 'Anwendungsabbildung (Splash)';
$s_application_image = 'Anwendungspiktogramm';
$s_startup_message = 'Meldung beim Aufrufen';
$s_startup_message_formcolor = 'Hintergrundfarbe der Startmeldung';
$s_helpnr = 'Hilfenummer';
$s_helpfile = 'Hilfsdatei';
$s_searchoptions = 'Suchoptionen';
$s_website = 'Website URL';
$s_ftpserver = 'FTP Server';
$s_ftpuser = 'FTP Benutzer';
$s_ftppassword = 'FTP Passwort';

$s_apps_choose_from_list = 'Um eine Anwendung zu verwenden, wählen Sie aus der Liste unten.';
$s_apps_non_available    = 'Sie sind für keine Anwendung zugelassen. Kontaktieren Sie bitte Ihren Systemadministrator.';

$s_test_connection = 'Test Konnektion';
$s_save_settings = 'Einstellungen speichern';
$s_go = 'Los';

# database field resources #
$s_database_list = 'Datenbank Übersicht';
$s_db_description = 'Name';
$s_databasename = 'Datenbankname';
$s_host = 'Moderator';
$s_databasetype = 'Datenbank Typ';
$s_rootusername = 'Administratorname';
$s_rootpassword = 'Administratorpasswort';
$s_database = 'Datenbank';
$s_loginwithuseraccount = 'Verwenden Sie den Useraccount um sich in die Datenbank ein zu loggen';
$s_errortablename = 'Fehlertabellenname';
$s_errorcodecolumnname = 'Fehlercode Spaltenname';
$s_errormessagecolumnname = 'Fehlerbericht Spaltenname';

# subset field resources #
$s_subsets = 'Teilversammlung';



# table field resources #
$s_tables = 'Tabellen';



# tag resources #
$s_add_a_tag = 'Etikett hinzufügen';
$s_tag_already_exists = 'Dieses Etikett gibt es schon';
$s_delete_tag = 'Etikett entfernen';
$s_view_tagged_items = 'Schaun Sie sich Elemente mit diesem Etikett an';


# kenniskunde resources #

$s_knowledgetics = 'Wissenskunde';

$s_kk_overview = 'Übersicht Wissenskunde';
$s_facttypediagrams = 'Tatsache Typ Diagramme';

$s_ftds = 'TTD\'s';


# templates resources #
$s_templates_list = 'Liste der Schablonen der Anwendungen';

# form field resources
$s_forms = 'Formulare';
$s_pieces = 'Stück';

$s_formname = 'Formular';
$s_databaseid = 'Datenbank';
$s_tablename = 'Tabellenname';
$s_filter = 'Filter';
$s_pagecolumncount = 'Anzahl Spalten';
$s_webpageurl = 'Webseite URL';
$s_defaultviewtype = 'Standard Formulartyp';
$s_showrecordsasconstructs = 'showrecordsasconstructs';
$s_captioncolumnname = 'Spaltenname der Detailinfo Form.';
$s_sortcolumnname = 'Nach Spalte sortieren';
$s_searchcolumnname = 'Nach Spalte suchen';
$s_panelhelptext = 'Formular Hilfstext';
$s_preload = 'preload';
$s_newrecordinformview = 'Neu in formview zeigen?';
$s_selectview = 'Selektion Ansicht';
$s_selectcolumnnames = 'Selektion Spaltenname';
$s_selectshowcolumns = 'Selektion Präsentation Spaltenname';
$s_selectquery = 'Selektion Suchanfrage';
$s_show_record_number = 'Anzahl der Zeilen anzeigen';

$s_database_script = 'Datenbank Skript';
$s_axmls_script = 'ADODB XML Skript';
$s_backup_restore = 'Backup/Restore der Kunden Dateibank';
$s_backup = 'Backup';
$s_restore = 'Restore';
$s_show_page_stats = 'Zeige Seite Statistiken';

# form permission resources
$s_autorisation = 'Autorisierung';

# page field resources
$s_pages = 'Seite/n';
$s_page = 'Seite';

# container resources
$s_navigation = 'Navigation';

# Webpage, Webparam resources
$s_webpage_list = 'Web Seite\'n';
$s_webpages_desc = 'Unten sehen Sie alle Webseiten ihrer Seite (zu denen Sie Zugang haben).
Sie können Webseiten hinzufügen, ändern oder löschen.';
$s_no_webpage_exists = 'Sie sind nicht berechtigt diese Webseite zu ändern oder es sind keine Webseiten verfügbar.';
$s_webpage_name = 'Seitenname';
$s_webpage_title = 'Titel';
$s_languages = 'Sprachen';
$s_content = 'Inhalt';
$s_edit_webpage = 'Ändere Webseite';
$s_webparam_name = 'Parameter';

# Webmenu resources
$s_menuid = 'Menüelement ID';
$s_menuname = 'Menüname';
$s_url = 'URL';
$s_pagename = 'Seitenname';
$s_parentmenuid = 'Ist Teil des Menüelements';
$s_target = 'Ziell/target';
$s_seo_keywords = 'SEO Keyworter';
$s_seo_keywords_help = '(Keywörter durch Leerzeichen trennen)';

# record limit
$s_recordlimit_label = 'Zeile pro Seite';
$s_recordlimit_hint = 'Anzahl der Datensätze die gleichzeitig angezeigt werden';
$s_navigate_first = 'erste';
$s_navigate_prev = 'zurück';
$s_navigate_next = 'nächste';
$s_navigate_last = 'letzte';
$s_navigate_first_hint = 'Zeige die erste Datensatz Seite';
$s_navigate_prev_hint = 'Zeige die vorherige Datensatz Seite';
$s_navigate_next_hint = 'Zeige die nächste Datensatz Seite';
$s_navigate_last_hint = 'Zeige die letzte Datensatz Seite';

$s_filter_title = 'Thema filtern';
$s_filter_cancel = 'Alle Themen zeigen (Filter entfernen)';
$s_filter_cancel_short = 'Filter entfernen';

# Refresh resources
$s_refreshinterval_label = 'Neuer Schirm';
$s_n_seconds = '%s Sek';
$s_n_minutes = '%s Min';
$s_never_refresh = 'niemals';

# image resources #
$s_image_view = 'Abbildung zeigen';
$s_image_view_hint = 'Abbildung in einem neuen Fenster öffnen';
$s_image_view_normal_size = 'In originalgröße zeigen';
$s_image_normal_size = 'Originalgröße';
$s_image_delete = 'Abbildung löschen';
$s_image_add_new = 'Abbildung hinzufügen';
$s_image_file_size = 'Bestehende Größe';
$s_image_edit = 'Abbildung ändern';
$s_image_edit_hint = '';
$s_image_select_button = 'Abbildungsliste';
$s_image_select = 'Eine Abbildung auswählen';
$s_image_select_hint = 'Öffne Liste mit Abbildungen in einem neuen Fenster.';
$s_image_upload_text = 'Sie können hier auch mehrere Abbildungen hochladen. Suchen Sie Ihre Abbildung mit dem Butten \'Blättern...\' (max. 5 Abbildungen) und drücken Sie auf Versenden.';
$s_pick_image = 'Wählen Sie eine Abbildung aus unten stehender Liste.';
$s_no_images = 'Keine Abbildungen vorhanden.';
$s_selected_image = 'Ausgewählte Abbildung';
$s_image_preview = 'Beispiel';
$s_choose_image = 'Entscheiden&nbsp;Abbildung';
$s_upload_images = 'Abbildungenn&nbsp;hochladen';

# files resources #
$s_pick_file = 'Wählen Sie eine Datei aus unten stehender Liste aus.';
$s_choose_file = 'Entscheiden&nbsp;Datei';
$s_upload_files = 'Dateien&nbsp;hochladen';
$s_no_files = 'Es sind keine Dateine vorhanden';
$s_selected_file = 'Ausgewählte Datei';

# item resources #
$s_no_item = 'kein';
$s_item = 'Zeile';
$s_n_items = 'Zeilen';
$s_show_all_items = 'Alle %s';  # default %s value is s_items #
$s_add_item = 'Neue %s'; # default %s value is s_item #
$s_add_item_to_list = 'Neue %s-Zeile hinzufügen'; # default %s value is s_item #
$s_add_item_after_save = 'Sichern und Hinzufügen neuer %s'; # default %s value is s_item #
$s_add_detailitem_after_save = 'Sichern und %s Hinzufügen';
$s_list_is_empty = 'Die Liste mit \'%s\' ist noch leer.';  # default %s value is s_items #
$s_you_can_add_to_list = 'Sie können nun ein %s an die Liste anfügen.';  # default %s value is s_item #
$s_all_items = 'Alle Zeilen';
$s_click_and_edit_item = 'Klicken Sie auf einem Zeile, um die Zeile zu bearbeiten.';
$s_click_and_view_item = 'Klicken Sie auf einem Zeile, um die Zeile an zu zeigen.';
$s_no_item_selected = 'Sie haben keine Zeile ausgewählt.';
$s_confirmation = 'Um zu bestätigen';
$s_confirm_delete_1_tag = 'Sind Sie sicher das Sie der Tag \'%s\' löschen wollen?';
$s_confirm_delete_item = 'Wollen Sie die ausgewählte Zeile löschen?';
$s_confirm_delete_item_plural = 'Wollen Sie die __count__ ausgewählten Zeilen löschen?';
$s_edit_item = '%s bearbeiten';
$s_view_item = '%s anzeigen';
$s_add_an_item = '%s hinzufügen';
$s_add_new_item = 'Neue %s hinzufügen';
$s_clone_an_item = '%s klonen';
$s_clone_item = "Klone dieser Zeile";
$s_view_details = 'anzeigen';
$s_item_deleted = 'Zeile gelöscht';
$s_items_deleted = '%s Zeilen gelöscht';
$s_changes_saved = 'Änderungen gespeichert';
$s_item_added = 'Element hinzugefügt';
$s_details = 'Details';
$s_no_item_selected = "Sie haben keine Zeile ausgewählt.";
$s_more_than_one_item_selected = "Sie haben mehr alas eine Zeile ausgewählt.";
$s_error = "Fehler";

# Search form #
$s_search               = 'Suchen';
$s_search_active        = 'Suche';
$s_search_title         = 'Suche einen';
$s_search_record        = 'Zeile';
$s_search_column_label  = 'Suche in';
$s_search_value_label   = 'Suche nach der Eingabe';
$s_search_advanced      = 'Ausführlich suchen';
$s_filtered             = 'Gefiltert!';
$s_filtered_hint        = 'Klicken Sie auf \'Alle %s\' um die Suche auf zu heben';
$s_cancel_filtered      = 'Filter aufheben';
$s_no_records_found     = 'Es wurden keine Zeile gefunden, die Ihren Suchkriterien entsprechen.';
$s_alter_search         = 'Ändern Sie Ihren Suchterm oder klicken Sie auf \'Alle %s\' damit alle Zeilen angezeigt werden.';
$s_no_records_found_pin = 'Es wurden keine Zeilen gefunden, die Ihren gehefte Zeilen entsprechen.';
$s_alter_pin            = 'Entfernen Sie ein oder mehrere gehefte Zeilen.';
$s_filtered_pin         = 'Nur geheften Zeilen!';
$s_filtered_hint_pin    = 'Klicken Sie auf \'Entferne alle geheften Zeilen\' damit Sie alle Zeilen sehen können.';
$s_search_result_title  = 'Ergebnisse';

$s_search_this_function = 'Suche im dieser Funktion';
$s_search_this_application = 'Suche in der ganzen Anwendung';

$s_search_result        = 'Ergebnisse';

$s_maintenance          = 'Wartung';
$s_maintenance1         = 'um';
$s_maintenance2         = 'Uhr';
$s_maintenance3         = 'Zeit';

# import export #
$s_import_export = 'Import/Export';
$s_import = 'Import';
$s_export = 'Export';
$s_import_action = 'Importieren';
$s_export_action = 'Exportieren';
$s_export_no_permission = 'Sie haben nicht genug Rechte Informationen zu exportieren';
$s_import_no_permission = 'Sie haben nicht genug Rechte Informationen zu importieren';
$s_import_export_items = '%s importieren/exportieren';

#
$s_check_webpage = 'Kontrollieren Sie die Webseite';
$s_seperate_window = 'In einem seperaten Fenster';
$s_check_webpage_hint = 'Schauen Sie sich diese Datei live auf der Webseite an';

# button labels #
$s_but_ok = 'OK';
$s_but_save = 'Sichern';
$s_but_save_hint = 'Aktuelle Zeile speichern';
$s_but_save_and_pin = 'Speichern und anheften';
$s_but_save_and_pin_hint = 'Aktuelle Zeile speichern und anheften';
$s_but_save_and_new = 'Speichern und neue Zeile';
$s_but_cancel = 'Abbrechen';
$s_but_cancel_hint = 'Zurück zum vorherigen Formular';
$s_but_close = 'Schließen';
$s_but_close_hint = 'Das Fenster schließen';
$s_but_new = 'Neue Zeile';
$s_but_new_hint = 'Neue Zeile machen';
$s_but_delete = 'Zeile löschen';
$s_but_delete_hint = 'Ausgewählte Zeile löschen';
$s_but_refresh = 'Neu laden';
$s_but_refresh_hint = 'Tabelleninhalt neu laden';
$s_but_find = 'Suchen';
$s_but_find_hint = 'Suche nach Zeile';
$s_but_copynew = 'Zeile kopieren';
$s_but_copynew_hint = 'Ausgewählte Zeile kopieren';
$s_but_select = 'Auswählen';
$s_but_select_hint = 'Wählen Sie die aktuelle Wahl aus';
$s_but_browse = 'Blättern';
$s_but_browse_hint = 'Zu einer bestimmten Datei blättern.';
$s_but_upload = 'Senden';
$s_but_working = 'beschäftigt...';
$s_but_add = 'Hinzufügen';
$s_but_search = 'Suchen';
$s_but_search_hint = 'Dateien suchen';
$s_but_export = 'Exportieren';
$s_but_import = 'Importieren';
$s_but_preview = 'Vorschau';
$s_but_download = 'Download';
$s_but_pinrecord = 'Anheften';
$s_but_pinrecord_hint = 'Ausgewählte Zeile anheften';
$s_but_bulkchange = 'Multi-Bearbeitung';
$s_but_bulkchange_hint = 'Multi-Bearbeitung';

$s_but_nav_first = '<<';
$s_but_nav_last  = '>>';
$s_but_nav_prev  = '<';
$s_but_nav_next  = '>';

# designer functions #
$s_what_now = 'Was wollen Sie tun?';
$s_where_to_now = 'Wo wollen Sie hin?';
$s_add_client = 'Einen Kunden hinzu fügen an...';

# Language resources #
$s_choose_language = 'Wählen Sie eine Sprache';

# Errors and warnings #
$s_go_back_after_insert_error = 'Ändern Sie Ihre Zeile und versuchen Sie es erneut';
$s_form_not_found = 'Formular wurde nicht gefunden! Versuchen Sie es erneut.';
$s_page_not_found = 'Diese Seite steht zur Zeit nicht zur Verfügung.';
$s_cannot_determine_tables = 'Die Tabellen der Datenbank können nicht bestimmt werden.';
$s_cannot_connect_database = 'Es konnt keine Verbindung mit der Datenbank gelegt werden. Kontrollieren Sie Ihre Daten und versuchen Sie es erneut.';
$s_cannot_insert_database = 'Polaris konnteIhre Datenbankinformationen nicht speichern.';
$s_cannot_update_record = 'Die Daten konnten nicht bearbeitet werden';
$s_invalid_email = 'Dit emailadres is niet geldig.';
$s_error_512 = 'Ihr Befehl kann nicht ausgeführt werden.';
$s_error_1062 = 'Ihr Befehl kann nicht ausgeführt werden.';

# Language names #
$s_lang_nl = 'Holländisch';
$s_lang_fr = 'Französisch';
$s_lang_en = 'Englisch';
$s_lang_de = 'Deutsch';
$s_lang_se = 'Schwedisch';
$s_lang_it = 'Italienisch';
$s_lang_aa = 'Generic';
