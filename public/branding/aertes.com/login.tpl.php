<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>MAXIA - Jouw automatische marketingmachine</title>

    <link href='//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" media='all' href="{$serverpath}/assets/dist/main.min.css"  />
    <link rel="stylesheet" type="text/css" media='all' href="{$serverpath}/bower_components/fontawesome/css/font-awesome.css"  />
    <link href="/branding/{$brand}/style.css" rel="stylesheet">

    <script type="text/javascript" src="{$serverpath}/bower_components/jquery/dist/jquery.js"></script>

</head>

<body class="blue">
    <!-- Home Section -->
    <div id="home">
        <div class="row">
            <div class="col-md-3 col-md-push-9">
            </div>
            <div class="col-md-6 col-sm-12">
                <img class="maxialogo" src="/branding/{$brand}/logo.png" alt="Maxia - Jouw automatische marketing machine" />
            </div>
        </div>
        <div class="row">
          &nbsp;
        </div>
        <div class="row">
            <div class="col-sm-4 col-md-push-4">
                <div class="blocks-wrapper clearfix">
                    <div class="block block-left width-50 bg-white">
                        <div class="search-wrapper" id="js_search">
                            <form id="logintemplatex" class="form-horizontal" method="post" name="loginform" action="{$serverroot}">
                                <input type="hidden" name="_hdnAction" value="login" />
                                <input id="user" name="username" accesskey="n" placeholder="Uw accountnaam" class="form-control" type="text" autofocus="autofocus" value="" /><br />
                                <input id="pass" name="password" accesskey="w" placeholder="Uw wachtwoord" class="form-control" type="password" value="" />
                                <button type="submit" class="button btn btn-warning">{lang login}</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <p>&copy; 2019, Powered by <a href="https://polarisnow.com">Polaris</a></p>
                </div>
            </div>
        </div>

    </div><!-- /.container -->

    <script type="text/javascript">
    sessionStorage.plrses = '{php}global $polaris; echo $polaris->polarissession; {/php}';
    </script>

</body>

</html>