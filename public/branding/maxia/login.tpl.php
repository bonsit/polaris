{if $hasbrandglobals}{config_load file="brandglobals.conf"}{/if}
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Add Your favicon here -->
    <!--<link rel="icon" href="img/favicon.ico">-->

    <title>Maxia</title>

    <!-- Bootstrap core CSS -->
    <link href="/branding/maxia/css/bootstrap.min.css" rel="stylesheet">

    <!-- Animation CSS -->
    <link href="/branding/maxia/css/animate.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" media='all' href="{$serverpath}/assets/css/fontawesome-pro/css/all.min.css" />

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="/branding/maxia/css/style.css" rel="stylesheet">
</head>
<body id="page-top">
<div class="navbar-wrapper">
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header page-scroll">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigatie</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <span class="navbar-brand" id="brandLogo" href="#">Maxia</span>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a class="page-scroll" href="#overons">Over ons</a></li>
                        <li><a class="page-scroll" href="#features">Oplossingen</a></li>
                        <li><a class="page-scroll" href="#contact">Contact</a></li>
                        <li><a href="#" data-toggle="modal" data-target="#modalLogin">Inloggen</a></li>
                    </ul>
                </div>
            </div>
        </nav>
</div>

<div id="inSlider" class="carousel slide carousel-fade" data-ride="carousel">
    <div class="carousel-inner" role="listbox">
        <div class="item active">
            <div class="container">
                <div class="row m-b-xl">
                    <div class="col-lg-8 col-md-10">
                        <div class="carousel-caption">
                            <h1>Software voor het sociale werkdomein</h1>
                            <p class="tagline">De ideale balans tussen automatiseren en digitale vaardigheden ontwikkelen.</p>

                            <p class="login">
                                <a class="btn btn-success btn-maxia" data-toggle="modal" data-target="#modalLogin"><i class="fa fa-sign-in"></i> Inloggen</a>
                                {if $loginerror}
                                <span class="text-danger" style="margin-top:8px;display:block;font-size:1em;font-weight:normal;max-width:240px;">{$loginerror}</span>
                                <br><a href="{$serverroot}/forgotpassword/">Wachtwoord opnieuw instellen</a>
                                {/if}
                            </p>

                        </div>
                    </div>
                </div>
            </div>
            <!-- Set background for slide in css -->
            <div class="header-back two"></div>
        </div>
    </div>
</div>

<a name="topmore"></a>
<section id="features" class="container features">
    <div class="row">
        <div class="col-lg-12"><br><br></div>
    </div>

    <div class="row">
        <div class="col-lg-6 col-lg-offset-3 text-center">
            <div class="navy-line"></div>
            <h1 class="maxia-header">Wat is Maxia?</h1>
        </div>
    </div>
    <div class="row features-block">
        <div class="col-lg-6 features-text wow fadeInLeft">
            <h4>Maxia is speciaal ontworpen voor sociale werkdomein.</h4>

            <p>Onze software-oplossing ontlast organisaties bij het beheren van de dagelijkse bedrijfsprocessen.</p>

            <p>Ga direct aan de slag met onze geïntegreerde modules en laagdrempelige tools, zoals <b>facilitybeheer, locatieverhuur, deelnemerontwikkeling en wagenparkmanagement</b>.
            Of, als u specifieke wensen heeft, bieden we ook maatwerkmodules.
            </p>

            <h4>Maar dat is nog niet alles... </h4>
            <p>Tegelijkertijd biedt Maxia een <b>beschermde en veilige leeromgeving</b> voor het ontwikkelen van digitale vaardigheden.
            Dit betekent dat deelnemers beter worden voorbereid op de reguliere arbeidsmarkt.
            </p>

            <p></p>

            <p>U krijgt volledige controle over uw bedrijfsprocessen... en – waar gewenst – met inzet van uw eigen deelnemers.</p>
        </div>
        <div class="col-lg-6 text-right wow fadeInRight align-center">
            <img src="/branding/maxia/img/illus2.png" alt="dashboard" class="img-responsive center-block" style="width:300px">
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6 col-lg-offset-3 text-center">
            <div class="navy-line"></div>
            <h1 class="maxia-header-2">Wat betekent Maxia voor uw organisatie?</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 text-center wow fadeInLeft">
            <div>
                <i class="fa fa-person-shelter features-icon"></i>
                <h2>Juiste balans tussen automatiseren en digitale vaardigheden</h2>
                <p>Deelnemers kunnen hun digitale vaardigheden verbeteren in een veilige en
                    beschermde omgeving, zonder bang te zijn dat ze iets stuk maken.
                    Hiermee worden ze beter voorbereid op de reguliere arbeidsmarkt.</p>
            </div>
            <div class="m-t-lg">
                <i class="fa fa-globe features-icon"></i>
                <h2>Ontlasten van je organisatie</h2>
                <p>Maxia biedt verschillende modules aan waarmee je dagelijkse processen in het sociale werkdomein kunt beheren. Zodat medewerkers zich kunnen focussen op het ondersteunen van de deelnemers.</p>
            </div>
        </div>
        <div class="col-md-6 text-center  wow zoomIn">
            <br><br>
            <img src="/branding/maxia/img/perspective2.png" alt="dashboard" class="img-responsive">
        </div>
        <div class="col-md-3 text-center wow fadeInRight">
            <div>
                <i class="fa fa-user-circle features-icon"></i>
                <h2>Gebruikersvriendelijke software voor het sociale werkdomein</h2>
                <p>Onze software-oplossing is ontworpen om sociale ontwikkelbedrijven te digitaliseren, met eigen mensen, middelen en op eigen tempo.
                    Een SaaS-oplossing zonder installatie, beheer en onderhoud.
                </p>
            </div>
            <div class="m-t-lg">
                <i class="fa fa-people-arrows features-icon"></i>
                <h2>Vergroot de betrokkenheid van uw deelnemers</h2>
                <p>Met onze community app houdt u deelnemers interactief betrokken bij de ontwikkelingen van uw organisatie.
                 Uw eigen nieuwsberichten, agendafunctie, smoelenboek en vraagbaak voor dagelijkse werkzaamheden.
                </p>
            </div>
        </div>
    </div>

</section>
{*
<section id="features" class="container services">
    <div class="row">
        <div class="col-sm-3">
            <h2>Facility beheer</h2>
            <p>Deze module omvat alle activiteiten die nodig zijn bij de verhuur en onderhoud van gebouwen, apparatuur en andere middelen. Bijvoorbeeld gepland onderhoud, storingen en schoonmaak, maar ook het boeken van vergaderruimtes en bijbehorende faciliteiten.</p>
            <p><a class="navy-link" href="#" role="button">Details &raquo;</a></p>
        </div>
        <div class="col-sm-3">
            <h2>Kandidaatontwikkeling</h2>
            <p>Maxia helpt ontwikkelcoaches met de ontwikkeling van kandidaten die een afstand tot de arbeidsmarkt hebben. Met de juiste tools kunnen kandidaten gevolgd worden in het werving-, en selectieproces. </p>
            <p><a class="navy-link" href="#" role="button">Details &raquo;</a></p>
        </div>
        <div class="col-sm-3">
            <h2>Voorraadbeheer</h2>
            <p>Het beheer van de voorraad binnen jouw organisatie kan door Maxia weggeregeld worden. Het omvat het bijhouden van de voorraadniveaus van de koffie tot wc-papier en het klaarmaken van bestellingen.</p>
            <p><a class="navy-link" href="#" role="button">Details &raquo;</a></p>
        </div>
        <div class="col-sm-3">
            <h2>Kennisbank</h2>
            <p>Een flexibele kennisbank waar je werkinstructies in kwijt kan. Hiermee kun je  kunnen kandidaten de onderliggende taken veilig kunnen uitvoeren.</p>
            <p><a class="navy-link" href="#" role="button">Details &raquo;</a></p>
        </div>
    </div>
</section>

<section id="team" class="gray-section team">
    <div class="container">
        <div class="row m-b-lg">
            <div class="col-lg-12 text-center">
                <div class="navy-line"></div>
                <h1>Wat zijn de voordelen van Maxia?</h1>
                <p>Door de bril van specifieke gebruikers leggen we de voordelen uit.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 wow fadeInLeft">
                <div class="team-member">
                    <img src="/branding/maxia/img/avatar7.jpg" class="img-responsive img-circle img-small" alt="">
                    <h4><span class="navy">Manager</span></h4>
                    <p>Tekst volgt nog...</p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="team-member wow zoomIn">
                    <img src="/branding/maxia/img/avatar1.jpg" class="img-responsive img-circle img-small" alt="">
                    <h4><span class="navy">Ontwikkelcoach</span></h4>
                    <p>Tekst volgt nog...</p>
                </div>
            </div>
            <div class="col-sm-4 wow fadeInRight">
                <div class="team-member">
                    <img src="/branding/maxia/img/avatar6.jpg" class="img-responsive img-circle img-small" alt="">
                    <h4><span class="navy">Kandidaat</h4>
                    <p>Tekst volgt nog...</p>
                </div>
            </div>
        </div>
    </div>
</section>

<a name="businessbooster"><br></a>

<section class="features">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="navy-line"></div>
                <h2>Facilitybeheer </h2>
                <p>INSPINIA Admin Theme is a premium admin dashboard template with flat design concept. It is fully responsive admin dashboard template built with Bootstrap 3+ Framework, HTML5 and CSS3, Media query. It has a huge collection of reusable UI components and integrated with latest jQuery plugins.</p>
            </div>
        </div>
        <div class="row features-block">
            <div class="col-lg-3 features-text wow fadeInLeft">
                <small>INSPINIA</small>
                <h2>Perfectly designed </h2>
                <p>INSPINIA Admin Theme is a premium admin dashboard template with flat design concept. It is fully responsive admin dashboard template built with Bootstrap 3+ Framework, HTML5 and CSS3, Media query. It has a huge collection of reusable UI components and integrated with latest jQuery plugins.</p>
                <a href="" class="btn btn-primary">Learn more</a>
            </div>
            <div class="col-lg-6 text-right m-t-n-lg wow zoomIn">
                <img src="/branding/maxia/img/iphone.jpg" class="img-responsive" alt="dashboard">
            </div>
            <div class="col-lg-3 features-text text-right wow fadeInRight">
                <small>INSPINIA</small>
                <h2>Perfectly designed </h2>
                <p>INSPINIA Admin Theme is a premium admin dashboard template with flat design concept. It is fully responsive admin dashboard template built with Bootstrap 3+ Framework, HTML5 and CSS3, Media query. It has a huge collection of reusable UI components and integrated with latest jQuery plugins.</p>
                <a href="" class="btn btn-primary">Learn more</a>
            </div>
        </div>
    </div>
</section>
*}

<section class="features">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="navy-line"></div>
                <h1>Krachtige modules. Eenvoudig in gebruik.</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-5 col-lg-offset-1 features-text">

                <h2>Facilitybeheer </h2>
                <img class="pull-right mod-image" src="/branding/maxia/img/facilitybeheer.png" />
                <p>Deze module omvat alle activiteiten die nodig zijn bij het beheer, verhuur en onderhoud van de gebouwen van een sociale ontwikkelbedrijf. Bijvoorbeeld gepland onderhoud, storingen en schoonmaak, maar ook het boeken van vergaderruimtes, flexplekken en bijbehorende faciliteiten. Met makkelijke werkorders zodat de kandidaten de taken kunnen uitvoeren.</p>
            </div>
            <div class="col-lg-5 features-text">

                <h2>Voorraadbeheer </h2>
                <img class="pull-right mod-image" src="/branding/maxia/img/voorraadbeheer.png" />
                <p>Maxia regelt het beheer van de voorraad binnen het sociale ontwikkelbedrijf. Het omvat het bijhouden van de voorraadniveaus van de koffie tot de kleding van de groenvoorziening en het klaarmaken van bestellingen.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-5 col-lg-offset-1 features-text">

                <h2>Kennisbank</h2>
                <img class="pull-right mod-image" src="/branding/maxia/img/kennisbank.png" />
                <p>Een flexibele kennisbank met werkinstructies om informatie en kennis binnen het ontwikkelbedrijf te delen.
                Medewerkers van het ontwikkelbedrijf vullen de kennisbank met relevante content.
                De deelnemers kunnen deze informatie zelf opzoeken en gebruiken in hun dagelijkse werkzaamheden.</p>
            </div>
            <div class="col-lg-5 features-text">

                <h2>Wagenparkmanagment</h2>
                <img class="pull-right mod-image" src="/branding/maxia/img/wagenparkmanagement.png" />
                <p>
                    Met deze module kunt u uw wagenpark beheren, zoals het inplannen van voertuigonderhoud, reserveren van voertuigen en
                    bijhouden van brandstofverbruik.
                    Zo heeft u continue zicht op de efficiëntie, betrouwbaarheid en veiligheid van uw wagenpark, waardoor u kosten kunt optimaliseren en uw bedrijfsvoering kunt stroomlijnen.
                </p>
            </div>
        </div>
    </div>
</section>


<section id="overons" class="overons">

    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-1">
                <h1>Over ons</h1>
                <p>
                    Bij Maxia staat continue groei en ontwikkeling centraal.
                    We werken nauw samen met ontwikkelbedrijven en begeleiden deelnemers op hun persoonlijke groeipad.
                    Hoe doen we dit? Samen met ontwikkelbedrijven ontwikkelen we opleidingsprogramma's die aansluiten bij de behoeften van deelnemers, Maxia en de arbeidsmarkt.
                    Ons doel is een inspirerende omgeving te creëren waarin deelnemers vanuit ontwikkelbedrijven een vaste aanstelling binnen Maxia kunnen vinden en tegelijkertijd essentiële digitale vaardigheden ontwikkelen.
                </p>

                <p>Onze benadering is gebaseerd op enkele belangrijke principes:</p>

                <ol>
                    <li>Deelnemers staan centraal en ontwikkelen zich op hun eigen tempo.</li>
                    <li>We bieden een veilige omgeving waarin deelnemers kunnen werken aan hun persoonlijke groei.</li>
                    <li>Onze specialisten begeleiden deelnemers met geduld en respect.</li>
                    <li>We werken samen met overheden en partners om iedereen gelijke kansen te bieden.</li>
                    <li>Onze missie is duurzaamheid, en daarom zetten we in op circulaire processen.</li>
                </ol>

            </div>
        </div>
    </div>

</section>

<section id="contact" class="gray-section contact">
    <div class="container">
        <div class="row m-b-lg">
            <div class="col-lg-12 text-center">
                <div class="navy-line"></div>
                <h1>Wij <i class="fa fa-heart"></i> van onze klanten</h1>
                <p>Ideen en suggesties zijn meer dan welkom.</p>
            </div>
        </div>
        <div class="row m-b-lg">
            <div class="col-lg-3 col-lg-offset-3">
                <address>
                    <strong><span class="navy">Maxia</span></strong><br/>
                    Beekerweg 13<br>
                    6235CA Ulestraten<br>
                    Nederland<br>
                    <a href="mailto:info@maxia.nl">info@maxia.nl</a>
                </address>
            </div>
            <div class="col-lg-4">
                <p class="text-color">

                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 text-center">
                <a href="mailto:info@maxia.nl" class="btn btn-primary btn-maxia">Stuur ons een email</a>
{*                <p class="m-t-sm">
                    Of volg ons op de socials
                </p>
                <ul class="list-inline social-icon">
                    <li><a href="#"><i class="fa fa-twitter"></i></a>
                    </li>
                    <li><a href="#"><i class="fa fa-facebook"></i></a>
                    </li>
                    <li><a href="#"><i class="fa fa-linkedin"></i></a>
                    </li>
                </ul>
                *}
            </div>
        </div>
        <div class="row">
        <div class="col-lg-2 xcol-lg-offset-2 text-center m-t-lg m-b-lg">
                <p><small>Illustratie door <a style="color:#666" href="https://vonikdesign.com">Vonikdesign</a></small></p>
            </div>
            <div class="col-lg-4 col-lg-offset-2 text-center m-t-lg m-b-lg">
                <p><strong>&copy; {$smarty.now|date_format:"%Y"} BONS IT Solutions BV</strong><br/> Alle rechten voorbehouden</p>
            </div>
        </div>
    </div>
</section>

<div class="modal inmodal" id="modalLogin" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInDown">
            <form id="logintemplatex" class="form-horizontal" method="post" name="loginform" action="{$serverroot}">
            <div class="modal-header">
            Vul uw account gegevens in
            </div>
            <div class="modal-body">
                <input type="hidden" name="_hdnAction" value="login" />
                <fieldset>
                    <div class="form-group">
                        <label for="user" class="control-label col-lg-offset-1 col-lg-3">{lang username}:</label>
                        <div class="col-sm-3">
                            <input id="user" autofocus name="username" accesskey="n" class="form-control" type="text" value="" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="pass" class="control-label col-lg-offset-1 col-lg-3">{lang password}:</label>
                        <div class="col-sm-3">
                            <input id="pass" name="password" accesskey="w" class="form-control" type="password" value="" />
                        </div>
                    </div>
                </fieldset>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">{lang but_close}</button>
                <button type="submit" class="btn btn-primary">{lang login}</button>
            </div>
            </form>
        </div>
    </div>
</div>

<script src="/branding/maxia/js/jquery-2.1.1.js"></script>
<script src="/branding/maxia/js/classie.js"></script>
<script src="/branding/maxia/js/bootstrap.min.js"></script>
<script src="/branding/maxia/js/cbpAnimatedHeader.js"></script>
<script src="/branding/maxia/js/wow.min.js"></script>
<script type="text/javascript">
sessionStorage.setItem("tempScrollTop", 0);
$('#modalLogin').on('shown.bs.modal', function () {
  $('#user').trigger('focus')
});
</script>
</body>
</html>