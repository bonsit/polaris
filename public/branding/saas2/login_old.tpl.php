{if $hasbrandglobals}{config_load file="brandglobals.conf"}{/if}
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Add Your favicon here -->
    <!--<link rel="icon" href="img/favicon.ico">-->

    <title>{lang pagetitle}</title>

    <!-- Bootstrap core CSS -->
    <link href="branding/saas2/css/bootstrap.min.css" rel="stylesheet">

    <!-- Animation CSS -->
    <link href="branding/saas2/css/animate.min.css" rel="stylesheet">
    <link href="branding/saas2/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="branding/saas2/css/style.css" rel="stylesheet">
</head>
<body id="page-top">
<div class="navbar-wrapper">
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header page-scroll">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigatie</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" id="btnLogin" href="#">INLOGGEN</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a class="page-scroll" href="#page-top">Start</a></li>
                        <li><a class="page-scroll" href="#features">Wat is het?</a></li>
                        <li><a class="page-scroll" href="#testimonials">Referenties</a></li>
                        <li><a class="page-scroll" href="#pricing">Prijs</a></li>
                        <li><a class="page-scroll" href="#contact">Contact</a></li>
                    </ul>
                </div>
            </div>
        </nav>
</div>
<div id="inSlider" class="carousel slide carousel-fade" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#inSlider" data-slide-to="0" class="active"></li>
        <li data-target="#inSlider" data-slide-to="1"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
<!--         <div class="item active">
            <div class="container">
                <div class="carousel-caption blank">
                    <h1>Zet bezoekers om<br> in klanten</h1>
                    <p>Met onze BusinessBooster® krijg je in no-time meer...</p>
                    <p><a class="btn btn-lg btn-primary" href="#businessbooster" role="button">Learn more</a></p>
                </div>
            </div>
            <div class="header-back two"></div>
        </div>
 -->
        <div class="item">
            <div class="container">
                <div class="carousel-caption">
                    <h1>Teveel tijd kwijt <br>aan je administratie?</h1>
                    <p>Begin vandaag met je eigen <br>virtuele backoffice voor ZZP'ers.</p>
                    <p>
                        <a class="btn btn-lg btn-primary" href="#" role="button">GRATIS UITPROBEREN RELATIEBEHEER</a>
                        <a class="caption-link" href="#topmore" role="button">Begin vandaag met relatiebeheer.</a>
                    </p>
                </div>
                <div class="carousel-image wow zoomIn">
                    <img src="branding/saas2/img/laptop2.png" alt="laptop"/>
                </div>
            </div>
            <!-- Set background for slide in css -->
            <div class="header-back one"></div>
        </div>
<!--         <div class="item">
            <div class="container">
                <div class="carousel-caption blank">
                    <h1>Wil je meer klanten<br> en meer omzet?</h1>
                    <p>Eenvoudige marketing automatisering op zijn best. Met onze BusinessBooster® krijg je in no-time meer...</p>
                    <p><a class="btn btn-lg btn-primary" href="#businessbooster" role="button">Learn more</a></p>
                </div>
            </div>
            <div class="header-back two"></div>
        </div>
 -->    </div>
    <a class="left carousel-control" href="#inSlider" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Vorige</span>
    </a>
    <a class="right carousel-control" href="#inSlider" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Volgende</span>
    </a>
</div>


<a name="topmore"></a>
<section  class="container features">
    <div class="row">
        <div class="col-lg-12"><br><br></div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-lg-offset-3 text-center">
            <div class="navy-line"></div>
            <h1>Bewaar jij je klantgegevens nog in Excel?</h1>
            <p class="lead">Staan al je workshop-aanmeldingen verspreid in je mailbox?
                Ben je een Post-It jockey geworden?<br>
                Kortom, ben je het overzicht kwijt?<br>
                Maak kennis met <b>Polaris</b></p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 text-center wow fadeInLeft">
            <div>
                <i class="fa fa-mobile features-icon"></i>
                            <h2>Alle relatiegegevens overzichtelijk bij elkaar. </h2>
                            <p>Verlies nooit meer belangrijke gegevens en notities van je relaties, leads en klanten.
                                En met de handige Vervolgactie functie weet je precies wanneer je een relatie moet contacteren.</p>

                <p></p>
            </div>
            <div class="m-t-lg">
                <i class="fa fa-bar-chart features-icon"></i>
                <h2>Handige zoekfuncties</h2>
                <p>Nooit meer zoeken naar je relatiegegevens, een online applicatie waarmee je alle bedrijfsinformatie kunt vastleggen en beheren. Van klantgegevens tot workshopaanmeldingen, van leads tot productgegevens. Veilig in de cloud.</p>
            </div>
        </div>
        <div class="col-md-6 text-center  wow zoomIn">
            <img src="branding/saas2/img/perspective.png" alt="dashboard" class="img-responsive">
        </div>
        <div class="col-md-3 text-center wow fadeInRight">
            <div>
                <i class="fa fa-envelope features-icon"></i>
                <h2>Mail pages</h2>
                <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit. Morbi leo risus.</p>
            </div>
            <div class="m-t-lg">
                <i class="fa fa-google features-icon"></i>
                <h2>AngularJS version</h2>
                <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit. Morbi leo risus.</p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-lg-offset-3 text-center">
            <div class="navy-line"></div>
            <h1>Wat is Polaris?</h1>
            <p  class="lead">Polaris ondersteunt de bedrijfsprocessen van je onderneming.
                        Een virtuele back-office die waakt over je relaties, producten, internet-leads
                        en andere administratieve gegevens.</p>

        </div>
    </div>
    <div class="row features-block">
        <div class="col-lg-6 features-text wow fadeInLeft">
            <small>POLARIS</small>
            <h2>Ontworpen voor ZZP'ers</h2>
            <p>Grote ondernemingen hebben veelal maatwerk software om hun processen te beheren. Maar wat we merkten is dat ook veel kleine ondernemers en zzp'ers behoefte hebben aan een back-office systeem.
                        Waarom? Omdat veel ondernemers <em>te veel tijd kwijt</em> zijn met het beheren van hun
                        belangrijkste bedrijfsgegevens: klantgegevens, leads, internet aanvragen, voorraad, enz.
                        En dat is tijd die kun je niet besteden daar waar het telt: <b>je klanten!</b></p>
            <a href="" class="btn btn-primary">Learn more</a>
        </div>
        <div class="col-lg-6 text-right wow fadeInRight">
            <img src="branding/saas2/img/dashboard.png" alt="dashboard" class="img-responsive pull-right">
        </div>
    </div>
</section>

<section id="features" class="container services">
    <div class="row">
        <div class="col-sm-3">
            <h2>Relatiebeheer</h2>
            <p>Alle relatiegegevens, leads en klanten op één plek bij elkaar. En met de handige Vervolgactie functie weet je precies wanneer je een relatie moet contacteren.</p>
            <p><a class="navy-link" href="#" role="button">Details &raquo;</a></p>
        </div>
        <div class="col-sm-3">
            <h2>Aanmeldingen</h2>
            <p>Nieuwe aanvragen of aanmeldingen worden automatisch verzameld. Koppel Polaris aan je web-formulier en nieuwe aanvragen en aanmeldingen staan automatisch in Polaris.<br> Klaar om verwerkt te worden.</p>
            <p><a class="navy-link" href="#" role="button">Details &raquo;</a></p>
        </div>
        <div class="col-sm-3">
            <h2>Email-triggers</h2>
            <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit. Morbi leo risus.</p>
            <p><a class="navy-link" href="#" role="button">Details &raquo;</a></p>
        </div>
        <div class="col-sm-3">
            <h2>Advanced Forms</h2>
            <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit. Morbi leo risus.</p>
            <p><a class="navy-link" href="#" role="button">Details &raquo;</a></p>
        </div>
    </div>
</section>

<section id="team" class="gray-section team">
    <div class="container">
        <div class="row m-b-lg">
            <div class="col-lg-12 text-center">
                <div class="navy-line"></div>
                <h1>Voor wie is Polaris geschikt?</h1>
                <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 wow fadeInLeft">
                <div class="team-member">
                    <img src="branding/saas2/img/avatar3.jpg" class="img-responsive img-circle img-small" alt="">
                    <h4><span class="navy">Lifecoach</span></h4>
                    <p>Met Polaris weet ik precies wanneer ik mijn klanten moet bellen voor een follow-up.</p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="team-member wow zoomIn">
                    <img src="branding/saas2/img/avatar1.jpg" class="img-responsive img-circle img-small" alt="">
                    <h4><span class="navy">Marketingspecialist</span></h4>
                    <p>Met Polaris houdt....</p>
                </div>
            </div>
            <div class="col-sm-4 wow fadeInRight">
                <div class="team-member">
                    <img src="branding/saas2/img/avatar2.jpg" class="img-responsive img-circle img-small" alt="">
                    <h4><span class="navy">Ontwerper</h4>
                    <p>Lorem ipsum dolor sit amet, illum fastidii dissentias quo ne. Sea ne sint animal iisque, nam an soluta sensibus.</p>
                    <ul class="list-inline social-icon">
                        <li><a href="#"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 text-center m-t-lg m-b-lg">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut eaque, laboriosam veritatis, quos non quis ad perspiciatis, totam corporis ea, alias ut unde.</p>
            </div>
        </div>
    </div>
</section>

<a name="businessbooster"><br></a>
<section class="features">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="navy-line"></div>
                <h1>Krijg meer omzet met onze Business Booster®</h1>
                <p>Maak kennis met onze Business Booster en krijg meer omzet</p>
            </div>
        </div>
        <div class="row features-block">
            <div class="col-lg-3 features-text wow fadeInLeft">
                <small>INSPINIA</small>
                <h2>Perfectly designed </h2>
                <p>INSPINIA Admin Theme is a premium admin dashboard template with flat design concept. It is fully responsive admin dashboard template built with Bootstrap 3+ Framework, HTML5 and CSS3, Media query. It has a huge collection of reusable UI components and integrated with latest jQuery plugins.</p>
                <a href="" class="btn btn-primary">Learn more</a>
            </div>
            <div class="col-lg-6 text-right m-t-n-lg wow zoomIn">
                <img src="branding/saas2/img/iphone.jpg" class="img-responsive" alt="dashboard">
            </div>
            <div class="col-lg-3 features-text text-right wow fadeInRight">
                <small>INSPINIA</small>
                <h2>Perfectly designed </h2>
                <p>INSPINIA Admin Theme is a premium admin dashboard template with flat design concept. It is fully responsive admin dashboard template built with Bootstrap 3+ Framework, HTML5 and CSS3, Media query. It has a huge collection of reusable UI components and integrated with latest jQuery plugins.</p>
                <a href="" class="btn btn-primary">Learn more</a>
            </div>
        </div>
    </div>

</section>

<section id="testimonials" class="navy-section testimonials">

    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center wow zoomIn">
                <i class="fa fa-comment big-icon"></i>
                <h1>
                    What our users say
                </h1>
                <div class="testimonials-text">
                    <i>"We find that Data Mundi delivers on its promise. We get great insights about early-startups across the globe."</i>
                </div>
                <small>
                    <strong>12.02.2017 - Andy Smith</strong>
                </small>
            </div>
        </div>
    </div>

</section>
<section class="features">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="navy-line"></div>
                <h1>More and more extra great feautres</h1>
                <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-5 col-lg-offset-1 features-text">
                <small>INSPINIA</small>
                <h2>Perfectly designed </h2>
                <i class="fa fa-bar-chart big-icon pull-right"></i>
                <p>INSPINIA Admin Theme is a premium admin dashboard template with flat design concept. It is fully responsive admin dashboard template built with Bootstrap 3+ Framework, HTML5 and CSS3, Media query. It has a huge collection of reusable UI components and integrated with.</p>
            </div>
            <div class="col-lg-5 features-text">
                <small>INSPINIA</small>
                <h2>Perfectly designed </h2>
                <i class="fa fa-bolt big-icon pull-right"></i>
                <p>INSPINIA Admin Theme is a premium admin dashboard template with flat design concept. It is fully responsive admin dashboard template built with Bootstrap 3+ Framework, HTML5 and CSS3, Media query. It has a huge collection of reusable UI components and integrated with.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-5 col-lg-offset-1 features-text">
                <small>INSPINIA</small>
                <h2>Perfectly designed </h2>
                <i class="fa fa-clock-o big-icon pull-right"></i>
                <p>INSPINIA Admin Theme is a premium admin dashboard template with flat design concept. It is fully responsive admin dashboard template built with Bootstrap 3+ Framework, HTML5 and CSS3, Media query. It has a huge collection of reusable UI components and integrated with.</p>
            </div>
            <div class="col-lg-5 features-text">
                <small>INSPINIA</small>
                <h2>Perfectly designed </h2>
                <i class="fa fa-users big-icon pull-right"></i>
                <p>INSPINIA Admin Theme is a premium admin dashboard template with flat design concept. It is fully responsive admin dashboard template built with Bootstrap 3+ Framework, HTML5 and CSS3, Media query. It has a huge collection of reusable UI components and integrated with.</p>
            </div>
        </div>
    </div>

</section>
<section id="pricing" class="pricing">
    <div class="container">
        <div class="row m-b-lg">
            <div class="col-lg-12 text-center">
                <div class="navy-line"></div>
                <h1>App Pricing</h1>
                <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 wow zoomIn">
                <ul class="pricing-plan list-unstyled">
                    <li class="pricing-title">
                        Free
                    </li>
                    <li class="pricing-desc">
                        Lorem ipsum dolor sit amet, illum fastidii dissentias quo ne. Sea ne sint animal iisque, nam an soluta sensibus.
                    </li>
                    <li class="pricing-price">
                        <span>gratis</span>
                    </li>
                    <li>
                        Dashboards
                    </li>
                    <li>
                        Projects view
                    </li>
                    <li>
                        Contacts
                    </li>
                    <li>
                        Calendar
                    </li>
                    <li>
                        AngularJs
                    </li>
                    <li>
                        <a class="btn btn-primary btn-xs" href="#">Signup</a>
                    </li>
                </ul>
            </div>

            <div class="col-lg-4 wow zoomIn">
                <ul class="pricing-plan list-unstyled selected">
                    <li class="pricing-title">
                        Standard
                    </li>
                    <li class="pricing-desc">
                        Lorem ipsum dolor sit amet, illum fastidii dissentias quo ne. Sea ne sint animal iisque, nam an soluta sensibus.
                    </li>
                    <li class="pricing-price">
                        <span>$22</span> / month
                    </li>
                    <li>
                        Dashboards
                    </li>
                    <li>
                        Projects view
                    </li>
                    <li>
                        Contacts
                    </li>
                    <li>
                        Calendar
                    </li>
                    <li>
                        AngularJs
                    </li>
                    <li>
                        <strong>Support platform</strong>
                    </li>
                    <li class="plan-action">
                        <a class="btn btn-primary btn-xs" href="#">Signup</a>
                    </li>
                </ul>
            </div>

            <div class="col-lg-4 wow zoomIn">
                <ul class="pricing-plan list-unstyled">
                    <li class="pricing-title">
                        Premium
                    </li>
                    <li class="pricing-desc">
                        Lorem ipsum dolor sit amet, illum fastidii dissentias quo ne. Sea ne sint animal iisque, nam an soluta sensibus.
                    </li>
                    <li class="pricing-price">
                        <span>$160</span> / month
                    </li>
                    <li>
                        Dashboards
                    </li>
                    <li>
                        Projects view
                    </li>
                    <li>
                        Contacts
                    </li>
                    <li>
                        Calendar
                    </li>
                    <li>
                        AngularJs
                    </li>
                    <li>
                        <a class="btn btn-primary btn-xs" href="#">Signup</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row m-t-lg">
            <div class="col-lg-8 col-lg-offset-2 text-center m-t-lg">
                <p>*Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. <span class="navy">Various versions</span>  have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>
            </div>
        </div>
    </div>

</section>

<section id="contact" class="gray-section contact">
    <div class="container">
        <div class="row m-b-lg">
            <div class="col-lg-12 text-center">
                <div class="navy-line"></div>
                <h1>We horen graag van u</h1>
                <p>Ideëen, opmerkingen .</p>
            </div>
        </div>
        <div class="row m-b-lg">
            <div class="col-lg-3 col-lg-offset-3">
                <address>
                    <strong><span class="navy">Bons IT Solutions BV.</span></strong><br/>
                    795 Folsom Ave, Suite 600<br/>
                    San Francisco, CA 94107<br/>
                    <abbr title="Phone">P:</abbr> (123) 456-7890
                </address>
            </div>
            <div class="col-lg-4">
                <p class="text-color">
                    Consectetur adipisicing elit. Aut eaque, totam corporis laboriosam veritatis quis ad perspiciatis, totam corporis laboriosam veritatis, consectetur adipisicing elit quos non quis ad perspiciatis, totam corporis ea,
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 text-center">
                <a href="mailto:test@email.com" class="btn btn-primary">Stuur ons een mail</a>
                <p class="m-t-sm">
                    Of volg ons op sociale netwerken
                </p>
                <ul class="list-inline social-icon">
                    <li><a href="#"><i class="fa fa-twitter"></i></a>
                    </li>
                    <li><a href="#"><i class="fa fa-facebook"></i></a>
                    </li>
                    <li><a href="#"><i class="fa fa-linkedin"></i></a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 text-center m-t-lg m-b-lg">
                <p><strong>&copy; {$smarty.now|date_format:"%Y"} Bons IT Solutions</strong><br/> Alle rechten voorbehouden</p>
            </div>
        </div>
    </div>
</section>

<form id="logintemplate" class="form-horizontal" method="post" name="loginform" action="{$serverroot}" style="display:nonexx">
    <input type="hidden" name="_hdnAction" value="login" />
    <fieldset>
        <div class="form-group">
            <label for="user" class="control-label col-lg-offset-1 col-lg-3">{lang username}:</label>
            <div class="col-sm-6">
                <input id="user" name="username" accesskey="n" class="form-control" type="text" value="" />
            </div>
        </div>
        <div class="form-group">
            <label for="pass" class="control-label col-lg-offset-1 col-lg-3">{lang password}:</label>
            <div class="col-sm-6">
                <input id="pass" name="password" accesskey="w" class="form-control" type="password" value="" />
            </div>
        </div>
    </fieldset>
</form>

<script src="branding/saas2/js/jquery-2.1.1.js"></script>
<script src="branding/saas2/js/pace.min.js"></script>
<script src="branding/saas2/js/bootstrap.min.js"></script>
<script src="{$serverpath}/bower_components/bootstrap3-dialog/dist/js/bootstrap-dialog.js"></script>
<script src="branding/saas2/js/classie.js"></script>
<script src="branding/saas2/js/cbpAnimatedHeader.js"></script>
<script src="branding/saas2/js/wow.min.js"></script>
<script src="{$serverpath}/js/scripts/inspinia.js"></script>
<!-- <script src="branding/saas2/js/inspinia.js"></script>
 --></body>
</html>
