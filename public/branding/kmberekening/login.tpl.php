<!DOCTYPE html>
<head>
  <meta charset="utf-8">
  <title>Home</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="/branding/kmberekening/stylesheet.css?v=2" type="text/css" media="screen" />
</head>
<body>
  <div id="site" class="wrapper">
    <header class="home ">
      <div class="container">
        <div class="row">
          <div class="col-xs-3 col-sm-3 ">
            <div class="logo"><a href="/home" title="Home Envida"><img src="{$serverroot}/branding/kmberekening/pics/logo.png" alt="logo Envida" /></a></div>
          </div>
        </div>
        <div class="mobile-nav-wrapper">
        </div>
      </div>
    </header>
    <section class="img-header clearfix">
      <div class="header-img">
        <div id="slider-home">
          <div class="slides clearfix">
            <img
            src="{$serverroot}/branding/kmberekening/pics/back_home.jpg"
            alt="" />
          </div>
        </div>
      </div>
      <div class="quick-menu">
        <div class="container">
          <div class="row">
            <div class="col-sm-12">
              <div class="blocks-wrapper clearfix">
                <div class="block block-left width-50 bg-white">
                  <div class="fs-l txt-black">Inloggen bij KM berekening?</div>
                  <div class="search-wrapper" id="js_search">
                    <form id="logintemplatex" class="form-horizontal" method="post" name="loginform" action="{$serverroot}">
                      <input type="hidden" name="_hdnAction" value="login" />
                      <input id="user" name="username" accesskey="n" placeholder="Uw accountnaam" class="form-control" type="text" value="" /><br/>
                      <input id="pass" name="password" accesskey="w" placeholder="Uw wachtwoord" class="form-control" type="password" value="" />
                      <button type="submit" class="button btn btn-primary">{lang login}</button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
</body>
</html>