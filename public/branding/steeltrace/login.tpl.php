{if $hasbrandglobals}{config_load file="brandglobals.conf"}{/if}
{include file="brandheader.tpl.php" title="Content Management Systeem"}
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

<style type="text/css">
html, body {ldelim}
    height:100%;
{rdelim}
body {ldelim}
    font-family: "Roboto", Helvetica, sans-serif;
    font-size:100%;
    color:#fff;
    letter-spacing: 1px;
    margin:0px;
    padding:0px 0px 0px 0px;
    background-color:rgba(0,0,0,0.5);
    background-image: url(branding/steeltrace/home.jpg);
    background-size: auto 100%;
    background-repeat: no-repeat;
    background-position: left top;
  {rdelim}
</style>

<div id="plrlogin">
<div id="plrlogo"><span>{#polaris_alias#|default:Polaris}</span></div>

{if $loginerror}
<span style="color:#F1ABAB" class="loginerror">{$loginerror}</span>
{*<a href="/polaris/?method=dropsessions">Andere gebruiker geforceerd uitloggen</a>*}
{/if}

<p id="login_text">
{if !$loginerror}
{lang login_text}
{/if}
</p>

{if $encryptsalt}
<form method="post" name="loginform" action="{$serverroot}" id="loginform" onsubmit="document.forms[0]._hdnEncrypted.value='true';Polaris.Base.encryptPasswordFields('pass','encryptpass','{$encryptsalt}');return false;">
<input type="hidden" name="_hdnEncrypted" value="false" />
<input type="hidden" id="encryptpass" name="_hdnEncryptedPassword" value="" />
{else}
<form method="post" name="loginform" action="{$serverroot}">
{/if}
<input type="hidden" name="_hdnAction" value="login" />

<fieldset>
<table>
<tr>
<td><label for="user" class="field">{lang username}:</label>
<input id="user" name="username" accesskey="n" autofocus="autofocus" class="text required" type="text" value="" /></td>
</tr><tr>
<td><label for="pass" class="field">{lang password}:</label>
<input id="pass" name="password" accesskey="w" class="text required" type="password" value="" /></td>
</tr></table>
<div class="buttons"><button type="submit" class="btn btn-success"><i class="fa fa-fw fa-sign-in"></i> Login</button></div>
</fieldset>

{if $multipleinstances}
<br /><br /><br />
<label for="plrinstance">{lang polarisinstance}:&nbsp;</label> {$instancesselect}
{/if}
</form>


</div>
{include file="brandfooter.tpl.php"}
