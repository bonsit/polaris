<?php
require_once '../../../includes/app_includes.inc.php';
/**
 * Create the Polaris object
 */
$polaris = new base_Polaris();
$polaris->initialize();

?>
<!doctype html>
<html lang="nl">

<head>
    <meta charset="utf-8">
    <title>Contact | WerkvoorHeerlen</title>

    <meta name="viewport" content="width=1366">

    <meta property="og:title" content="Contact | WerkvoorHeerlen">
    <meta property="og:url" content="https://werkvoorheerlen.nl/contact">

    <link rel="stylesheet" href="https://werkvoorheerlen.nl//css/werkvoorheerlen.css?v=15382269">

    <!-- <link rel="stylesheet" href="/assets/dist/main.min.css"> -->

    <script async defer data-domain="werkvoorheerlen.nl" src="https://plausible.io/js/plausible.js"></script>

    <style>
        body {
            background-color: #fff;
        }
        section {
            border: none;
            box-shadow: none;
        }
        .header {
            position: relative;
            height:70px;
        }
        h2 {
            font-weight: bold;
        }
        .webp .main {
/*            background: none;*/
        }
        .header__logo {
            padding:10px 0 0 10px;
            width:250px;
        }
        .form__field {
            margin-bottom: 10px;
        }
        .form__field span {
            font-size:1.2em;

        }
        .form__field input[type="text"],
        .form__field input[type="phone"],
        .form__field select,
        .form__field input[type="email"] {
          background-color: #ffffff;
          border-radius: 0;
          border: none;
          width: 100%;
          height: 32px;
          margin: 0;
          padding: 0 0.3rem;
          color: #000000;
          font-family: "din-2014-narrow",sans-serif;
          font-size: 1.2rem;
          border: 1px solid rgba(0, 0, 0, 0.2);
          margin-bottom: 0;
      }
      label.ruimte {
        padding:20px;
      }
        label.active {
            border:2px solid orange;
            padding:18px;
        }

        #contact-submit.hidden {
            display: block;
        }
    </style>

</head>

<body>

    <div id="werkvoorheerlen">

        <header class="header">

            <div class="header__container">

                <figure class="header__logo">

                    <a href="https://werkvoorheerlen.nl/">


                        <!-- <img src="https://werkvoorheerlen.nl/images/menu-logo.svg" alt="Logo WerkvoorHeerlen"/> -->
                        <img src="/branding/werkvoorheerlen/img/BVS_logo.png" alt="Logo Bovengrondse Vakschool"/>

                    </a>

                </figure>

                <nav class="header__navigation">

                    <ul class="header__navigation__menu" id="menu" role="navigation">

                        <li class="header__navigation__menu__item">

                            <a class="header__navigation__menu__item__link" href="https://werkvoorheerlen.nl/">Ruimte reserveren</a>

                        </li>

                    </ul>

                    <button class="header__navigation__toggle" aria-expanded="false" aria-controls="menu">

                        <span></span>
                        <span></span>
                        <span></span>

                    </button>

                </nav>

            </div>

        </header>

        <main class="main" style="margin-top: 50px;">

            <section class="section section--contact" id="item-639" >

                <form method="post" action="http://werkvoorheerlen.maxia.nl:8080/process_message.php" accept-charset="UTF-8" class="form">

                    <input type="hidden" name="client_hash" value="547ce4f12bdd425641109c98e9a89acf">
                    <input type="hidden" name="redirect" value="http://werkvoorheerlen.maxia.nl:8080/branding/werkvoorheerlen/contact/bedankt.php">
                    <input type="hidden" name="sendto" value="bart@bonsit.nl">
                    <input type="hidden" name="emailfield" value="emailadres">
                    <input type="hidden" name="interesses" value="">
                    <input type="hidden" name="form" value="6e1ffb7f80f4ca50a09ffe4b23784578">
                    <input type="hidden" required data-msg="U dient dit veld in te vullen">


                    <label class="form__field">
                        <span>Uw naam</span>
                        <input type="text" name="contactpersoon" required value="Bart"/>
                    </label>

                    <label class="form__field">
                        <span>Uw e-mailadres</span>
                        <input type="email" name="contactpersoon_email" required value="bart@hijgendhert.nl"/>
                    </label>

                    <label class="form__field">
                        <span>Uw telefoonnummer</span>
                        <input type="text" name="contactpersoon_telefoon" required value="06-24959159"/>
                    </label>

                    <label class="form__field">
                        <span>Bedrijfsnaam</span>
                        <input type="text" name="bedrijfsnaam" required value="Restaurant Het Hijgend Hert"/>
                    </label>

                    <label class="form__field">
                        <span>Reserveringsdatum</span>
                        <input type="text" name="gewenste_datum" required value="2023-05-12"/>
                    </label>

                    <label class="form__field">
                        <span>Reserveringstijd</span>
                        <select name="gewenste_tijd">
                            <<option value="ochtend">Ochtend</option>
                            <<option value="middag">Middag</option>
                            <<option value="heledag">Hele dag</option>
                        </select>
                    </label>

                    <label class="form__field form__field--full">
                        <span>Soort ruimte</span>
                    </label>

                    <label class="form__field ruimte" id="4">

                            <h2>Vergaderruimte "Welten"</h2>
                            <img src="/branding/werkvoorheerlen/contact/ruimtefoto1.jpg" width="400px">

                            <h4>Max aantal personen: 16</h4>

                    </label>

                    <label class="form__field ruimte" id="17">

                            <h2>Vergaderruimte "Heksenberg"</h2>
                            <img src="/branding/werkvoorheerlen/contact/ruimtefoto1.jpg" width="400px">

                            <h4>Max aantal personen: 12</h4>


                    </label>

                    <label class="form__field ruimte" id="60">

                            <h2>Vergaderruimte "De Koumen"</h2>
                            <img src="/branding/werkvoorheerlen/contact/ruimtefoto1.jpg" width="400px">

                            <h4>Max aantal personen: 12</h4>

                    </label>

                        <input name="soort_ruimte" class="form-control" id="_fldRUIMTE_ID" label="Ruimte" />

                        <option value=""></option><option value="4">1.18 Vergaderruimte</option><option value="17">0.14 Open Spreekkamers</option><option value="60">1.02 Taalcafe</option><option value="62">1.04 Leslokaal + WP</option><option value="63">1.05 Leslokaal + WP</option><option value="73">1.16 Vergaderruimte</option><option value="74">1.17 Vergaderruimte</option><option value="75">1.15 Vergaderruimte</option></select>
                    </label>

                    <label class="form__field">
                        <span>Aantal personen</span>
                        <input type="text" name="aantal_personen" required value="6"/>
                    </label>

                    <label class="form__field">
                        <span>Soort lunch</span>
                        <input type="text" name="soort_lunch" required value="Luxe"/>
                    </label>

                    <label class="form__field form__field--full">
                        <span>Opmerking</span>
                        <textarea rows="3" cols="40" id="message" name="opmerking"></textarea>
                    </label>

                    <label class="form__field">
                        <input id="contact-submit" class="hidden" type="submit" value="Versturen">
                    </label>
                    <br><br><br><br><br><br><br>
                </form>

            </section>

        </main>

        <footer class="footer">

            <div class="footer__content">

                <div class="footer__content__column">

                    <h4>Werk voor Heerlen</h4>
                    <p></p>
                    <ul>

                        <li>
                            <a href="https://werkvoorheerlen.nl/diensten">Diensten</a>
                            <a href="https://werkvoorheerlen.nl/projecten">
                                <br>
                            </a>
                        </li>
                        <li>
                            <a href="https://werkvoorheerlen.nl/projecten">Projecten</a>
                        </li>
                        <li>
                            <a href="https://werkvoorheerlen.nl/over-ons">Over ons</a>
                            <br>
                        </li>
                        <li>
                            <a href="https://werkvoorheerlen.nl/organisatie">Organisatie</a>
                        </li>
                        <li>
                            <a href="https://werkvoorheerlen.nl/contact">Contact</a>
                            <br>
                        </li>

                    </ul>

                </div>

                <div class="footer__content__column">

                    <h4>Algemeen</h4>
                    <p></p>
                    <ul>

                        <li>
                            <a href="https://werkvoorheerlen.nl/uploads/Privacy-Statement-Stichting-WerkvoorHeerlen-21-04-2022.pdf" target="_blank">Privacy Statement</a>
                        </li>
                        <li>
                            <a href="https://werkvoorheerlen.nl/uploads/HUISHOUDELIJK-REGLEMENT_website.pdf" target="_blank">Huishoudelijk reglement</a>
                        </li>
                        <li>
                            <a href="https://werkvoorheerlen.nl/uploads/KLACHTENCOMMISSIE-VERTROUWENSPERSONEN-EN-KLOKKENLUIDERS-website.pdf" target="_blank">Klachtencommissie & Vertrouwenspersonen</a>
                        </li>
                    </ul>
                    <ul>
                        <li>
                            <a href="https://werkvoorheerlen.nl/over-ons">Werken bij</a>
                            <br>
                        </li>

                    </ul>

                </div>

                <div class="footer__content__column">

                    <h4>Contact</h4>
                    <p>
                        Diepenbrockstraat 15
                        <br>
                        6411 TJ HEERLEN
                        <br>
                        <a href="mailto:info@werkvoorheerlen.nl">info@werkvoorheerlen.nl</a>
                    </p>
                    <p>
                        <br>
                    </p>

                </div>

                <div class="footer__content__column">
                </div>

                <div class="footer__content__column">

                    <p>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                    </p>
                    <p>
                        <br>
                    </p>

                </div>

                <div class="footer__content__column">

                    <p></p>
                    <p></p>
                    <p></p>
                    <p></p>
                    <p></p>
                    <figure>
                        <img src="https://werkvoorheerlen.nl/uploads/Logo-Europees-Sociaal-Fonds-klein.jpg" data-image="1776" style="opacity: 1;">
                    </figure>
                    <p></p>

                </div>

            </div>

        </footer>

    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <!-- <script src="https://werkvoorheerlen.nl/js/modernizr.js"></script> -->
    <!-- <script src="https://werkvoorheerlen.nl/js/lity.min.js"></script> -->
    <!-- <script src="https://werkvoorheerlen.nl/js/werkvoorheerlen.js?v=769727"></script> -->

    <<script type="text/javascript">
        $(document).ready(function() {
            $(".ruimte").click(function() {
                console.log($(this).attr('id'));
                $('label.ruimte').removeClass('active');
                $(this).toggleClass('active');
                $("#_fldRUIMTE_ID").val($(this).attr('id'));
            });
        })
    </script>

</body>

</html>
