{if $hasbrandglobals}{config_load file="brandglobals.conf"}{/if}
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Add Your favicon here -->
    <!-- <link rel="icon" href="img/favicon.ico"> -->

    <title>Maxia</title>

    <!-- Bootstrap core CSS -->
    <link href="/branding/werkvoorheerlen/css/bootstrap.min.css" rel="stylesheet">

    <!-- Animation CSS -->
    <script src="https://use.fontawesome.com/849b74bb02.js"></script>

    <!-- Custom styles for this template -->
    <link href="/branding/werkvoorheerlen/css/style.css" rel="stylesheet">
</head>
<body id="page-top">
<div class="navbar-wrapper">
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header page-scroll">
                    <span class="navbar-brand" id="brandLogo" href="#"><img src="/branding/werkvoorheerlen/img/menu-logo.png" style="width:100px"></span>
                </div>
            </div>
        </nav>
</div>

<div class="container row">
    <div class="login-widget col-12-lg">
        <div class="widget-content">
            <h3>Inloggen bij Maxia Online</h3>

            <form id="logintemplatex" class="form-horizontal" method="post" name="loginform" action="{$serverroot}">
                <div class="modal-body">
                    <input type="hidden" name="_hdnAction" value="login" />
                    <fieldset>
                        <div class="form-group">
                            <label for="user" class="control-label col-lg-offset-1 col-lg-4">{lang username}: </label>
                            <div class="col-sm-6">
                                <input id="user" name="username" accesskey="n" class="form-control" type="text" value="" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="pass" class="control-label col-lg-offset-1 col-lg-4">{lang password}: </label>
                            <div class="col-sm-6">
                                <input id="pass" name="password" accesskey="w" class="form-control" type="password" value="" />
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">{lang login}</button>
                </div>
            </form>
        </div>
        <div class="widget-bottom"></div>
    </div>
</div>
<!-- Set background for slide in css -->
<div class="header-back two"></div>


<section id="contact" class="gray-section contactx">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 text-center m-t-lg m-b-lg">
                <h3><strong>&copy; {$smarty.now|date_format:"%Y"} Powered by Maxia</strong></h3>
            </div>
        </div>
    </div>
</section>

<script src="/branding/werkvoorheerlen/js/jquery-2.1.1.js"></script>
<!-- <script src="/branding/werkvoorheerlen/js/bootstrap.min.js"></script> -->
</body>
</html>