{if $hasbrandglobals}{config_load file="brandglobals.conf"}{/if}
<!DOCTYPE html>
<html>
    <head><title>{$config.polarisname}</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf8" />
        <meta name="robots" content="noindex,nofollow">


    <link rel="stylesheet" type="text/css" media='all' href="{$serverpath}/bower_components/fontawesome/css/font-awesome.css" />


    <link rel="stylesheet" type="text/css" media='all' href="{$serverpath}/bower_components/bootstrap/dist/css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" media='all' href="{$serverpath}/bower_components/toastr/toastr.css" />
    <link rel="stylesheet" type="text/css" media='all' href="{$serverpath}/bower_components/animate.css/animate.css" />
    <link rel="stylesheet" type="text/css" media='all' href="{$serverpath}/bower_components/bootstrap-markdown/css/bootstrap-markdown.min.css" />
    <link rel="stylesheet" type="text/css" media='all' href="{$serverpath}/bower_components/iCheck/skins/square/green.css" />
    <link rel="stylesheet" type="text/css" media='all' href="{$serverpath}/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.css" />
    <link rel="stylesheet" type="text/css" media='all' href="{$serverpath}/css/font-financial.css" />
    <link rel="stylesheet" type="text/css" media='all' href="{$serverpath}/css/font-line-icons.css" />
    <link rel="stylesheet" type="text/css" media='all' href="{$serverpath}/styles/style-2.9.2.css" />
    <link rel="stylesheet" type="text/css" media='all' href="{$serverpath}/styles/style-custom.css" />
    <link rel="stylesheet" type="text/css" media='all' href="{$serverpath}/js2/jquery/date_input.css" />
    <link rel="stylesheet" type="text/css" media='all' href="{$serverpath}/js2/jquery/date_input.css" />
    <link rel="stylesheet" type="text/css" media='all' href="{$serverpath}/css/plrdefaultdark.css" />

    <link rel="stylesheet" type="text/css" media='all' href="{$serverpath}/bower_components/fontawesome/css/font-awesome.css" />
    <link href='https://fonts.googleapis.com/css?family=Londrina Outline:700,400' rel='stylesheet' type='text/css'>
    <!-- Lato Font -->
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:300,700' rel='stylesheet' type='text/css'>
    <!-- Stylesheet -->
    <link href="/branding/{$brand}/style.css" rel="stylesheet">


            <link rel="fluid-icon" href="{$serverroot}/i/prima/plricon4.png" title="Polaris" />
            <link rel="shortcut icon" type="image/x-icon" href="{$serverroot}/favicon.ico" />
            <script type="">
            var _loadevents_ = [];
            var _serverroot = "{php}global $_GVARS; echo $_GVARS['serverroot']{/php}";
            </script>
        </head>
        <body id="{$pageid}">
            <div class="frame">
                <div class="plrlogin">
                        <img src="{$serverpath}/branding/{$brand}/fronttemplate8.jpg" class="image" />
                        <div id="butLogin" class="loginbutton"><a href="#" class="jqModal">inloggen</a></div>

                        <span class="buttons startnu">
                        <button id="probeer" type="button" class="button secondary">
                            Start vandaag nog
                            <br /><i class="fa fa-angle-right"></i> &nbsp;<small>MET POLARIS</small>
                            &nbsp;<i class="fa fa-angle-left"></i>
                        </button></span>

                        <div class="plrlogo"><span>Polaris - je virtuele back-office</span></div>

                </div>
                <div class="content section1">
                    <div class="textblock">
                        <h1>Bewaar jij je klantgegevens<br> nog in Excel?</h1>
                        <p>Staan al je workshop-aanmeldingen verspreid in je mailbox? Of doe je misschien nog alles met pen en papier? Dan wordt het tijd voor een professionelere aanpak.
                            Maak kennis met <b>Polaris</b>, een online systeem waar je perfect al je
                        informatie kunt vastleggen en beheren. Van klantgegevens tot workshopaanmeldingen, van leads tot productgegevens. Veilig in de cloud.</p>
                        <p>Voor een vast bedrag per maand krijg jij de beschikking over een eigen back-office systeem.
                            Te gebruiken vanaf elke plek en op elk moment.
                        Dus niet alleen thuis op je desktop of laptop, maar ook via je smartphone of tablet op een zonnig terrasje*.
                        </p>
                    </div>

                </div>
                <div class="content section2">
                    <div class="textblock">
                        <hr class="fix" /><br/>
                        <h1>Wat is {$config.polarisname}?</h1>

                        <p>{$config.polarisname} ondersteunt de bedrijfsprocessen van je onderneming.
                        Een virtuele back-office die waakt over je relaties, aanbiedingen, internet-leads
                        en andere administratieve gegevens.</p>

                        <p>{$config.polarisname} is een online, <i>in the cloud</i>, applicatie en heeft de afgelopen jaren zijn
                        sporen verdiend bij bedrijven zoals KPN, Getronics, NHA en andere
                        multinationals. Daar werd en wordt het gebruikt als een maatwerk (back-office) systeem, waarmee
                        verschillende informatiebronnen tot een toegankelijk en overzichtelijk geheel worden gevormd.</p>

                        <p>Maar wat we merkten is dat ook veel kleine ondernemers en zzp'ers behoefte hebben aan een back-office systeem.
                        Waarom? Omdat veel ondernemers <em>te veel tijd kwijt</em> zijn met het beheren van hun
                        belangrijkste bedrijfsgegevens: klantgegevens, leads, internet aanvragen, voorraad, enz.
                        En dat is tijd die kun je niet besteden daar waar het telt: <b>je klanten!</b></p>

                    </div>
                </div>
                <div class="content section_hilights">
                    <div class="textblock">
                        <h1>Dat kan makkelijker!</h1>

                        <div class="punt_links">
                            <ul class="appslist">
                                <li style="background-color:#008A00">
                                    <div class="apps_content">
                                        <i class="fa fa-star fa-4x"></i>
                                        <div class="appname">Relaties</div>
                                    </div>
                                </li>
                            </ul>
                            <h2>Relatiebeheer.<br/>
                                Alle relatiegegevens op één plek bij elkaar. </h2>
                            <p>Verlies nooit meer belangrijke gegevens en notities van je relaties, leads en klanten.
                                En met de handige Vervolgactie functie weet je precies wanneer je een relatie moet contacteren.</p>
                        </div>

                        <div class="punt_rechts">
                            <ul class="appslist">
                                <li style="background-color:#60A917">
                                    <div class="apps_content">
                                        <i class="fa fa-check-square fa-4x"></i>
                                        <div class="appname">Aanmeldingen</div>
                                    </div>
                                </li>
                            </ul>
                            <h2>Nieuwe aanvragen of aanmeldingen worden automatisch verzameld.</h2>
                            <p>Koppel Polaris aan je web-formulier en nieuwe aanvragen en aanmeldingen staan automatisch in Polaris.<br> Klaar om verwerkt te worden.</p>
                            <p></p>
                        </div>

                        <div class="punt_links">
                            <ul class="appslist">
                                <li style="background-color:#FA6800">
                                    <div class="apps_content">
                                        <i class="fa fa-paper-plane fa-4x"></i>
                                        <div class="appname">Email-triggers</div>
                                    </div>
                                </li>
                            </ul>
                            <h2>Stuur automatisch een bericht naar je nieuwe klanten.</h2>
                            <p>Stuur automatisch een bericht wanneer iemand zich aanmeld via je website.</p>
                        </div>
                    </div>
                </div>
                <div class="content section4">
                    <div class="textblock">
                {*
                        <p>Met Polaris kun je eenvoudig en snel alle gegevens van je relaties, workshops, producten, aanvragen enz. vastleggen, doorzoeken of mail-mergen.</p>
                        <a href="{$serverpath}/branding/{$brand}/voorbeeld1.png"><img src="{$serverpath}/branding/{$brand}/voorbeeld1.png" width="200px" /></a>
                        <a href="{$serverpath}/branding/{$brand}/voorbeeld2.png"><img src="{$serverpath}/branding/{$brand}/voorbeeld2.png" width="300px" /></a>

                        <h1>De Voordelen</h1>
                        <ul>
                            <li>Overzichtelijk menu met al jouw modules</li>
                            <li>Eenvoudig gegevens toevoegen, wijzigen en opzoeken.</li>
                            <li>Modulaire opbouw: Geen functies die je toch niet gebruikt.</li>
                            <li>Koppel je website aan Polaris en verzamel al je leads in Polaris.</li>
                            <li>Automatische follow-ups voor potentiele klanten.</li>
                            <li>Ga je uitbreiden? Geen probleem: je kunt eenvoudig medewerker-accounts toevoegen.</li>
                            <li>Geen software of updates installeren. Alles werkt via de browser.</li>
                            <li>Geen IT beheer nodig: Wij houden alles draaiende en zorgen voor backups.</li>
                            <li>Jouw gegevens zijn van jou: de gegevens kun je altijd exporteren. </li>
                            <li>Polaris draait in de Cloud: dus op elk device te gebruiken.</li>
                        </ul>

                        <h1>Begin vandaag</h1>
                        <p>Creeër meer tijd voor je klanten en start vandaag nog met Polaris.</p>
                        <p>Til je onderneming naar een hoger niveau... <em>vanaf € 9,50 per maand</em>.<p>


                        <table class="data prijslijst striped">
                            <thead>
                                <tr>
                                    <td class="right"> </td>
                                    <th style="width:20%" class="center">Basis</th>
                                    <th style="width:20%" class="center">Zilver</th>
                                    <th style="width:20%" class="center">Goud</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="right">Gratis klantenbeheer </td>
                                    <td class="center"><i class="fa fa-check"></i></td>
                                    <td class="center"><i class="fa fa-check"></i></td>
                                    <td class="center"><i class="fa fa-check"></i></td>
                                </tr>
                                <tr>
                                    <td class="right">Workshopbeheer </td>
                                    <td class="center"></td>
                                    <td class="center"><i class="fa fa-check"></i></td>
                                    <td class="center"><i class="fa fa-check"></i></td>
                                </tr>
                                <tr>
                                    <td class="right">Maatwerk administraties </td>
                                    <td class="center"></td>
                                    <td class="center"><i class="fa fa-check"></i></td>
                                    <td class="center"><i class="fa fa-check"></i></td>
                                </tr>
                                <tr>
                                    <td class="right">Leadsbeheer </td>
                                    <td class="center"></td>
                                    <td class="center"><i class="fa fa-check"></i></td>
                                    <td class="center"><i class="fa fa-check"></i></td>
                                </tr>
                                <tr>
                                    <td class="right">Email collector </td>
                                    <td class="center"></td>
                                    <td class="center"></td>
                                    <td class="center"><i class="fa fa-check"></i></td>
                                </tr>
                                <tr>
                                    <td class="right">Mailmarketing </td>
                                    <td class="center"></td>
                                    <td class="center"></td>
                                    <td class="center"><i class="fa fa-check"></i></td>
                                </tr>
                                <tr>
                                    <td class="right">Alerts </td>
                                    <td class="center"></td>
                                    <td class="center"></td>
                                    <td class="center"><i class="fa fa-check"></i></td>
                                </tr>
                                <tr>
                                    <td class="right">Elk moment opzegbaar </td>
                                    <td class="center">n.v.t.</td>
                                    <td class="center"><i class="fa fa-check"></i></td>
                                    <td class="center"><i class="fa fa-check"></i></td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td class="right"><b>Prijs per maand</b> </td>
                                    <td class="center">GRATIS</td>
                                    <td class="center">€ 9,50</td>
                                    <td class="center">€ 19,50</td>
                                </tr>
                                <tr class="footer">
                                    <td class="right"> </td>
                                    <td class="center"><span class="buttons"><button class="secondary aanmelden">Probeer nu</button></span></td>
                                    <td class="center"><span class="buttons"><button class="secondary aanmelden">Probeer nu</button></span></td>
                                    <td class="center"><span class="buttons"><button class="secondary aanmelden">Probeer nu</button></span></td>
                                </tr>
                            </tfoot>
                        </table>
                        <br/>

                        <h1>Nog niet overtuigd?</h1>
                        <p>Geen probleem! Voor de kat-uit-de-boom-kijkers hebben we nu een aanbieding waar je geen nee tegen kunt zeggen:<br>
                        Meld je aan en gebruik Polaris om je klantgegevens te beheren. Helemaal GRATIS en het blijft ook gratis! Binnenkort beschikbaar...

                        <span class="buttons startnu">
                        <button id="probeer" type="button" class="button secondary">
                            Start vandaag nog
                            <br /><i class="fa fa-angle-right"></i> &nbsp;<small>GRATIS KLANTENBEHEER</small>
                            &nbsp;<i class="fa fa-angle-left"></i>
                        </button></span>

                        </p>
*}

                        <h1>Waar begin jij mee?</h1>

                        <ul class="appslist">
                            <li style="background-color:#008A00">
                                <div class="apps_content">
                                    <i class="fa fa-star fa-4x"></i>
                                    <div class="appname">Klanten</div>
                                </div>
                            </li>
                            <li style="background-color:#60A917">
                                <div class="apps_content">
                                    <i class="fa fa-check-square fa-4x"></i>
                                    <div class="appname">Aanmeldingen</div>
                                </div>
                            </li>
                            <li style="background-color:#A4C400">
                                <div class="apps_content">
                                    <i class="fa fa-cube fa-4x"></i>
                                    <div class="appname">Voorraad</div>
                                </div>
                            </li>
                            <li style="background-color:#00ABA9">
                                <div class="apps_content">
                                    <i class="fa fa-user fa-4x"></i>
                                    <div class="appname">Personeel</div>
                                </div>
                            </li>

                            <li style="background-color:#1BA1E2">
                                <div class="apps_content">
                                    <i class="fa fa-home fa-4x"></i>
                                    <div class="appname">Website</div>
                                </div>
                            </li>
                            <li style="background-color:#0050EF">
                                <div class="apps_content">
                                    <i class="fa fa-truck fa-4x"></i>
                                    <div class="appname">Logistiek</div>
                                </div>
                            </li>
                            <li style="background-color:#6A00FF">
                                <div class="apps_content">
                                    <i class="fa fa-tachometer fa-4x"></i>
                                    <div class="appname">Dashboards</div>
                                </div>
                            </li>
                            <li style="background-color:#AA00FF">
                                <div class="apps_content">
                                    <i class="fa fa-file-text fa-4x"></i>
                                    <div class="appname">Rapportage</div>
                                </div>
                            </li>

                            <li style="background-color:#A20025">
                                <div class="apps_content"><i class="fa fa-graduation-cap fa-4x"></i>
                                    <div class="appname">Kennisbeheer</div>
                                </div>
                            </li>
                            <li style="background-color:#E51400">
                                <div class="apps_content"><i class="fa fa-bar-chart-o fa-4x"></i>
                                    <div class="appname">Marketing</div>
                                </div>
                            </li>
                            <li style="background-color:#FA6800">
                                <div class="apps_content">
                                    <i class="fa fa-paper-plane fa-4x"></i>
                                    <div class="appname">Email-triggers</div>
                                </div>
                            </li>
                            <li style="background-color:#F0A30A">
                                <div class="apps_content">
                                    <i class="fa fa-gears fa-4x"></i>
                                    <div class="appname">&lt;maatwerk&gt;</div>
                                </div>
                            </li>
                        </ul>
                        <hr class="fix" /><br/>
                        <center><h1><strong>Aanmelden momenteel op aanvraag!</strong></h1></center>
                        <p>Neem contact op voor meer informatie: <a href="mailto:info@bonsit.nl">info@bonsit.nl</a></p>

                    </div>
                </div>
                <div id="footer">
                    <p>* Polaris draait op elk apparaat met een moderne webbrowser. Het zonnig terrasje is niet erbij inbegrepen.</p>
                    <p>{#PolarisCopyright#}</p>
                </div>
            </div>
            <form id="loginform" class="loginform jqmWindow jqmShadow" method="post" name="loginform" action=".">
                {if $loginerror}
                <span style="color:#F1ABAB" class="loginerror">{$loginerror}</span>
                {*<a href="/polaris/?method=dropsessions">Andere gebruiker geforceerd uitloggen</a>*}
                {/if}
                <p id="login_text">
                {if !$loginerror}
                {lang login_text}
                {/if}
                </p>
                <input type="hidden" name="_hdnAction" value="login" />
                <div class="formviewrow"><label class="label">Gebruikersnaam:</label> <input id="user" name="username" accesskey="n" placeholder="{lang username}" autofocus="autofocus" class="text required" type="text" value="" /></div>
                <br/>
                <div class="formviewrow"><label class="label">Wachtwoord:</label> <input id="pass" name="password" accesskey="w" placeholder="{lang password}" class="text required" type="password" value="" /></div>
                <div class="formviewrow"><label class="label">&nbsp;</label> <button type="submit" class="submitbutton">{lang login}</button></div>
                {if $multipleinstances}
                <br /><br /><br />
                <label for="plrinstance">{lang polarisinstance}:&nbsp;</label> {$instancesselect}
                {/if}
            </form>
            <script type="text/javascript">
            var g_lang = "{$lang}";
            </script>
            <script type="text/javascript" src="{$serverpath}/min/f=/{$serverpath}/javascript/jquery/jquery-2.0.2.js"></script>
            <script type="text/javascript" src="{$serverpath}/min/g=js&{#BuildVersion#}"></script>
            <script type="text/javascript">
            $("#loginform").jqm();
            $("#probeer,.aanmelden").click(function() {ldelim}alert('Binnenkort beschikbaar... Stuur een e-mail voor beta try-out'){rdelim});
            sessionStorage.plrses = '{php}global $polaris; echo $polaris->polarissession; {/php}';
            </script>
            {if $resetpolarissession == true}
            {/if}
            <script type="text/javascript">
            log("plrses: " + sessionStorage.plrses);
            </script>
        </body>
    </html>