<link rel="stylesheet" type="text/css" media='all' href="{$serverpath}/min/f={$modulepath}/css/default.css&amp;{#BuildVersion#}" />
<div class="lane">
    <div class="row ">
        <div class="col-sm-12">

            <h3 class="lane-title"><a href="#"><i class="icon icon-1153"></i></a> Top vindingen <span> Top 10  van beoordelingen over de waarnemingen in het BFS rapport</span></h3>
            <ul class="notes">
                <li>
                    <div>
                        <h4>Noodzaak</h4>
                        <p>M.n. Staf, Productie en Marketing ervaren een an- dere noodzaak om te veranderen, waardoor niet de juiste prioriteiten worden gesteld.</p>
                        <a href="#"><i class="fa fa-trash-o "></i></a>
                    </div>
                </li>
                <li>
                    <div>
                        <h4>Noodzaak</h4>
                        <p>De noodzaak om te veranderen wordt als zeer laag ervaren en vormt een bedreiging voor het verander- proces en de toekomst van de organisatie.</p>
                        <a href="#"><i class="fa fa-trash-o "></i></a>
                    </div>
                </li>
                <li>
                    <div>
                        <h4>Koers</h4>
                        <p>De missie is niet eenduidig en wordt door verschil- lende afdelingen en verschillende management niveaus anders geïnterpreteerd.</p>
                        <a href="#"><i class="fa fa-trash-o "></i></a>
                    </div>
                </li>
                <li>
                    <div>
                        <h4>Koers</h4>
                        <p>Door de onduidelijkheid kunnen initiatieven niet worden getoetst aan koers/beleid en ontstaat er wildgroei.</p>
                        <a href="#"><i class="fa fa-trash-o "></i></a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="lane">
    <div class="row ">
        <div class="col-sm-12">
            <h3 class="lane-title"><a href="#"><i class="icon icon-1153"></i></a> Wat aan te pakken? <span> Laat ruimte voor de organisatie om oplossingen te bedenken</span></h3>
            <ul class="notes">
                <li>
                    <div>
                        <p>Noodzaak benadrukken en meer toepassen op klantperspectief.</p>
                        <a href="#"><i class="fa fa-trash-o "></i></a>
                    </div>
                </li>
                <li>
                    <div>
                        <p>Koers van de organisatie en de afdelingen verhelderen, afstemmen en toetsen.</p>
                        <a href="#"><i class="fa fa-trash-o "></i></a>
                    </div>
                </li>
                <li>
                    <div>
                        <p>Meer betrekken van medewerkers door het Management. zowel voor het draagvlak als inhoudelijk, om beschikbare kennis en ervaring vanuit de verschillende perspectieven mee te wegen bij keuzes.</p>
                        <a href="#"><i class="fa fa-trash-o "></i></a>
                    </div>
                </li>
                <li>
                    <div>
                        <p>Juiste inhoudelijke mensen betrekken bij projecten.</p>
                        <a href="#"><i class="fa fa-trash-o "></i></a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="lane">
    <div class="row">
        <div class="col-sm-12">
            <h3 class="lane-title"><a href="#"><i class="icon icon-1153"></i></a> Oplossingen <span> Over welke oplossingen kunnen we overeenstemming hebben?</span></h3>
            <ul class="notes">
                <li>
                    <div>
                        <h4>The generated Lorem Ipsum </h4>
                        <p>The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>
                        <a href="#"><i class="fa fa-trash-o "></i></a>
                    </div>
                </li>
                <li>
                    <div>
                        <h4>The generated Lorem Ipsum </h4>
                        <p>The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>
                        <a href="#"><i class="fa fa-trash-o "></i></a>
                    </div>
                </li>
                <li>
                    <div>
                        <h4>The generated Lorem Ipsum </h4>
                        <p>The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>
                        <a href="#"><i class="fa fa-trash-o "></i></a>
                    </div>
                </li>
                <li>
                    <div>
                        <h4>The generated Lorem Ipsum </h4>
                        <p>The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>
                        <a href="#"><i class="fa fa-trash-o "></i></a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="lane">
    <div class="row">
        <div class="col-sm-12">
            <h3 class="lane-title"><a href="#"><i class="icon icon-1153"></i></a> Verdienmodel <span>Welke soort inkomsten zijn er?</span></h3>
        </div>
    </div>
</div>

<div class="lane">
    <div class="row">
        <div class="col-sm-12">
            <h3 class="lane-title"><a href="#"><i class="icon icon-1153"></i></a> Klantrelaties <span>Welke contactpunten zijn er? Uitstraling, community, evenementen, service, garantie, vragen beantwoorden.</span></h3>
        </div>
    </div>
</div>
