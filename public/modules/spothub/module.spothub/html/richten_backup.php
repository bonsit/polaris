<link rel="stylesheet" type="text/css" media='all' href="{$serverpath}/min/f={$modulepath}/css/default.css&amp;{#BuildVersion#}" />
<div class="lane">
    <div class="row ">
        <div class="col-sm-12">
            <h3 class="lane-title"><a href="#"><i class="icon icon-1153"></i></a> Waarde propositie <span>Welk voordeel verschaft het product en welke pijnen neemt het weg?</span></h3>
            <ul class="notes">
                <li>
                    <div>
                        <h4>Continue inzicht in dossier</h4>
                        <p>Clienten krijgen continue inzicht in de status van hun dossier.</p>
                        <a href="#"><i class="fa fa-trash-o "></i></a>
                    </div>
                </li>
                <li>
                    <div>
                        <h4>Laagdrempelig contact </h4>
                        <p>Clienten kunnen even via hun smartphone een vraag stellen aan VDL</p>
                        <a href="#"><i class="fa fa-trash-o "></i></a>
                    </div>
                </li>
                <li>
                    <div>
                        <h4>Eenvoudig documenten delen</h4>
                        <p>Clienten kunnen documenten makkelijk delen via email (speciaal emailadres) en website.</p>
                        <a href="#"><i class="fa fa-trash-o "></i></a>
                    </div>
                </li>
                <li>
                    <div>
                        <h4>Een digitaal dossier</h4>
                        <p>Het kantoor heeft een compleet digitaal dossier van alle communicatiemomenten, dossier documenten en andere zaken.</p>
                        <a href="#"><i class="fa fa-trash-o "></i></a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="lane">
    <div class="row ">
        <div class="col-sm-12">
            <h3 class="lane-title"><a href="#"><i class="icon icon-1153"></i></a> Kernactiviteiten <span>Wat zijn de hoofdactiviteiten die je moet uitvoeren om het product te leveren?</span></h3>
            <ul class="notes">
                <li>
                    <div>
                        <h4>Webapplicatie in de lucht houden</h4>
                        <p>The years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>
                        <a href="#"><i class="fa fa-trash-o "></i></a>
                    </div>
                </li>
                <li>
                    <div>
                        <h4>Continue backups faciliteren</h4>
                        <p>The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.</p>
                        <a href="#"><i class="fa fa-trash-o "></i></a>
                    </div>
                </li>
                <li>
                    <div>
                        <h4>Marketing</h4>
                        <p>Ipsum used since the 1500s is reproduced below for those interested.</p>
                        <a href="#"><i class="fa fa-trash-o "></i></a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="lane">
    <div class="row">
        <div class="col-sm-12">
            <h3 class="lane-title"><a href="#"><i class="icon icon-1153"></i></a> Verkoopkanalen <span>Via welke kanalen wordt het product verkocht?</span></h3>
            <ul class="notes">
                <li>
                    <div>
                        <h4>The generated Lorem Ipsum </h4>
                        <p>The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>
                        <a href="#"><i class="fa fa-trash-o "></i></a>
                    </div>
                </li>
                <li>
                    <div>
                        <h4>Contrary to popular belief</h4>
                        <p>Hampden-Sydney College in Virginia, looked up one.</p>
                        <a href="#"><i class="fa fa-trash-o "></i></a>
                    </div>
                </li>
                <li>
                    <div>
                        <h4>There are many variations</h4>
                        <p>All the Lorem Ipsum generators on the Internet .</p>
                        <a href="#"><i class="fa fa-trash-o "></i></a>
                    </div>
                </li>
                <li>
                    <div>
                        <h4>Ipsum used standard chunk of Lorem</h4>
                        <p>Standard chunk  is reproduced below for those.</p>
                        <a href="#"><i class="fa fa-trash-o "></i></a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="lane">
    <div class="row">
        <div class="col-sm-12">
            <h3 class="lane-title"><a href="#"><i class="icon icon-1153"></i></a> Verdienmodel <span>Welke soort inkomsten zijn er?</span></h3>
        </div>
    </div>
</div>

<div class="lane">
    <div class="row">
        <div class="col-sm-12">
            <h3 class="lane-title"><a href="#"><i class="icon icon-1153"></i></a> Klantrelaties <span>Welke contactpunten zijn er? Uitstraling, community, evenementen, service, garantie, vragen beantwoorden.</span></h3>
        </div>
    </div>
</div>
