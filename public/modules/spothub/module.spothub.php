<?php
require_once('includes/mainactions.inc.php');
require_once('_shared/module._base.php');

class ModuleSpotHub extends _BaseModule {

    function __construct($moduleid, $module, $_clientname) {
        global $_GVARS;

        parent::__construct($moduleid, $module);

        $this->processed = false;
//        $this->showDefaultButtons = true;


        /**
        * What client are we talking about? (nog doen: eventueel selecteerbaar maken)
        */
        $this->clientid = $_SESSION['clientid'];
    }

    function Process() {
        global $_GVARS;
        global $polaris;

        // $_plrinstance = $this->form->plrInstance();
        // $_plrinstance->SetFetchMode(ADODB_FETCH_ASSOC);
        // $this->form->database->userdb->SetFetchMode(ADODB_FETCH_ASSOC);

        if (isset($_POST['_hdnProcessedByModule'])) {
            $_allesRecord = $_POST;

            try {
                switch($_POST['_hdnFORMTYPE']) {
                case '...':
                    ///...
                    break;
                }

                $resultArray = Array('result'=>$result, 'query'=>$_SERVER['REQUEST_URI'], 'error'=>false);
                echo json_encode($resultArray);

            } catch (Exception $E) {
                $detailedmessage = $E->completemsg."\r\n".parse_backtrace(debug_backtrace());

                $resultArray = Array('result'=>false, 'query'=>$_SERVER['REQUEST_URI'], 'error'=>$E->msg, 'detailed_error'=>$detailedmessage);
                echo  json_encode($resultArray);
            }

        }
    }

    function ShowRichten($moduleid) {
        //$_rs = $this->form->GetViewContent($detail=false, $masterrecord=false);
        //$_items = $_rs->GetAll();
        //$this->smarty->assign('items', $_items);
        $this->smarty->display("richten.tpl.php");
    }

    function Show($outputformat='xhtml', $rec=false) {
        global $_GVARS;
        global $polaris;

        parent::Show($outputformat, $rec);
        /**
        * Show the custommade forms when user views a list of items
        */
        switch ($this->moduleid) {
        case 1:
            switch($this->state) {
            case 'allitems':
            case 'search':
                $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/sharedlib.js';
                $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/spothub.js';
                $this->ShowRichten($this->moduleid);
            break;
            case 'edit':
            case 'insert':
                //$this->ShowPZNForm($this->moduleid);
            break;
            }
        break;
       }
    }

}