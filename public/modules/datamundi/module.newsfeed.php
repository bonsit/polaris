<?php
require('_shared/module._base.php');

class Module extends _BaseModule {

    function Module($moduleid, $module) {
        global $_GVARS;
        global $polaris;

        parent::__construct($moduleid, $module);

        $this->processed = false;

        /**
        * What client are we talking about? (nog doen: eventueel selecteerbaar maken)
        */
        $this->clientid = $_SESSION['clientid'];

        $_databaseid = 1;
        /* Load the user database */
        $db = new plrDatabase($polaris);
        $db->LoadRecord(array($this->clientid, $_databaseid));
        $this->userdatabase = $db->connectUserDB();
    }

    function Process() {
        global $_GVARS;
    }

    function GetNewsFeed() {
        $_sql = 'SELECT * FROM ';

        $this->form->LoadViewContent($detail=false, $masterrecord=false, $limit=false, $this->__GetCustomFilter());
        return $this->form->rsdata->GetAll();
    }

    function Show($outputformat='xhtml', $rec=false) {
        $this->showControls = false;
        $this->showDefaultButtons = false;
        $this->smarty->assign('newsfeed', $this->GetNewsFeed());
        $this->smarty->display("newsfeed.tpl.php");
    }

}