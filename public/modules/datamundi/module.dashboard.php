<?php
require('_shared/module._base.php');

class Module extends _BaseModule {

    function Module($moduleid, $module) {
        global $_GVARS;
        global $polaris;

        parent::__construct($moduleid, $module);

        $this->processed = false;

        /**
        * What client are we talking about? (nog doen: eventueel selecteerbaar maken)
        */
        $this->clientid = $_SESSION['clientid'];

        $_databaseid = 1;
        /* Load the user database */
        $db = new plrDatabase($polaris);
        $db->LoadRecord(array($this->clientid, $_databaseid));
        $this->userdatabase = $db->connectUserDB();
    }

    function Process() {
        global $_GVARS;
    }

    function GetHeatMap($accuracy) {
        $_sql = "
        select latitude lat, `longitude` lng, count(*) as count
        from organisation
        where latitude <> 0.0
        and longitude <> 0.0
        group by round(latitude, $accuracy), round(longitude, $accuracy)
        ";
        $_result = $this->userdatabase->GetAll($_sql);

        return json_encode($_result);
    }

    function Show($outputformat='xhtml', $rec=false) {
        $this->showControls = false;
        $this->showDefaultButtons = false;
        $this->smarty->assign('heatmap', $this->GetHeatMap($accuracy=3));
        $this->smarty->display("dashboard.tpl.php");
    }

}