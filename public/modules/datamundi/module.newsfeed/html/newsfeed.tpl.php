{include file="shared.tpl.php"}

<div class="row blog">
    <div class="col-md-3">
        <div class="iboxx">
            {literal}
            <a class="twitter-timeline" href="https://twitter.com/AlixRufas">Tweets by AlixRufas</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
            <a class="twitter-timeline" href="https://twitter.com/hashtag/brightlands" width="300px" data-widget-id="823837685827702788">#brightlands Tweets</a> <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
            {/literal}
        </div>
    </div>
    <div class="col-md-9">
        <div class="row">
        {section name=i loop=$newsfeed}
            <div class="col-lg-6">
                <div class="ibox">
                    <div class="ibox-title">
                        <a href="{$newsfeed[i].LINK}" class="btn-link">
                            <h2>
                               {$newsfeed[i].NAME}
                            </h2>
                        </a>
                    </div>
                    <div class="ibox-contentx">
                        <div class="m-b-xs">
                            <img src="{$newsfeed[i].IMAGE}" width="100%;" />
                            <span class="text-muted"><i class="fa fa-clock-o"></i>{$newsfeed[i].NEWSDATE}</span>
                        </div>
                        <p>
                            {$newsfeed[i].DESCRIPTION}
                        </p>
                        <div class="row">
                            <div class="col-md-6">
                                    <h5>Tags:</h5>
                                    {assign var=tags value=','|explode:$newsfeed[i].TAGS}
                                    {section name=t loop=$tags}
                                    <button class="btn btn-primary btn-xs" type="button">{$tags[t]}</button>
                                    {/section}
                            </div>
                            <div class="col-md-6">
                                <div class="small text-right">
                                    <h5>Stats:</h5>
                                    <i class="fa fa-eye"> </i> 144 views
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        {sectionelse}
        <p>No news found with the search keywords.</p>
        {/section}
        </div>
    </div>
</div>
