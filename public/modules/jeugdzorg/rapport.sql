select CASE domein WHEN 'emot' THEN 'Emotioneel' WHEN 'rela' THEN 'Relaties' WHEN 'vlgh' 
THEN 'Veiligheid' WHEN 'mat' THEN 'Materieel' WHEN 'ontw' THEN 'Ontwikkeling' WHEN 'gezh' THEN 'Gezondheid' END AS domein
, CASE niveau WHEN 'o' THEN 'Omgeving' WHEN 'g' THEN 'Gezin' WHEN 'k' THEN 'Kind' END as niveau
, probleem, case soort WHEN 'SYSTEM' THEN 'Systeem waarde' WHEN 'USER' THEN 'Door gebruiker toegevoegd' END AS soort
from probleem
order by domein, niveau, soort


select CASE domein WHEN 'emot' THEN 'Emotioneel' WHEN 'rela' THEN 'Relaties' WHEN 'vlgh' 
THEN 'Veiligheid' WHEN 'mat' THEN 'Materieel' WHEN 'ontw' THEN 'Ontwikkeling' WHEN 'gezh' THEN 'Gezondheid' END AS domein
, CASE niveau WHEN 'o' THEN 'Omgeving' WHEN 'g' THEN 'Gezin' WHEN 'k' THEN 'Kind' END as niveau
, zorg, case soort WHEN 'SYSTEM' THEN 'Systeem waarde' WHEN 'USER' THEN 'Door gebruiker toegevoegd' END AS soort
from zorg
order by domein, niveau, soort

