{include file="shared.tpl.php"}

<input type="hidden" id="_fldWizardFinished" name="WIZARD_FINISHED" value="" />

<div class="wizard" id="formwizard">
    <h3>Jeugdige</h3>
    <section>
        <div class="container-fluid">
            <div class="col-sm-offset-4">
                <h2>Vul hieronder de gegevens van de jeugdige in.</h2>
                {include file="pzn_mainform_set1.tpl.php" wizard=true}
            </div>
        </div>
    </section>
    <h3>Adres</h3>
    <section>
        <div class="container-fluid">
            <div class="col-sm-offset-4">
                <h2>Waar woont de jeudige op dit moment?</h2>
                {include file="pzn_mainform_set2.tpl.php" wizard=true}
            </div>
        </div>
    </section>
    <h3>Verzorger</h3>
    <section>
        <div class="container-fluid">
            <div class="col-sm-offset-4">
                <h2>Wat is de soort relatie van de verzorger</h2>
                {include file="pzn_mainform_set3.tpl.php" wizard=true}
            </div>
        </div>

    </section>
    <h3>Ouder(s)</h3>
    <section>
        <div class="container-fluid">
            <div class="col-sm-offset-4">
                <h2>Zijn er nog (andere) ouders die toegang krijgen tot dit PZN?</h2>
                {include file="pzn_mainform_set4.tpl.php" wizard=true}
            </div>
        </div>

    </section>
    <h3>Netwerkdeelnemers</h3>
    <section>
        <div class="container-fluid">
            <div class="col-sm-offset-2">
                <h2>Welke zorgverleners of mantelzorgers wilt u uitnodigen?</h2>
                {include file="pzn_mainform_set5.tpl.php" wizard=true}
            </div>
        </div>

    </section>
</div>

{literal}
<style>
.footer {display:none;}

.wizard>.steps>ul>li {
    width:15%;
}

</style>
{/literal}