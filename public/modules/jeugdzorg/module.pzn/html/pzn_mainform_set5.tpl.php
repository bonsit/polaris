<div class="overzicht">
    <div class="content bodyscroll">
        <table id="netwerkdeelnemer" class="table data table-striped jdenticons" tabindex="10">
          <thead>
            <tr>
              <th xstyle="width:10%">Soort deelnemer</th>
              <th xstyle="width:30%">Naam</th>
              <th>Soort zorg</th>
              <th>Uitgenodigd?</th>
              <th {if $modulestatus !== 'INGEDIEND'}style="display:none"{/if}>Uitgenodigd op</th>
              <th>Ingestuurd op</th>
              {if $modulestatus == 'INGEDIEND' or $modulestatus == 'INITIEEL'}
              <th>&nbsp;</th>
              {/if}
            </tr>
          </thead>
          <tbody>
            <tr class="row">
              <td style="display:none" class="id"></td>
              <td class="soort"></td>
              <td class="naam"></td>
              <td class="soortzorg"></td>
              <td class="uitgenodigd"></td>
              <td {if $modulestatus !== 'INGEDIEND'}style="display:none"{/if}class="statusdatum"></td>
              <td class="ingestuurd"></td>
              <td class="actie">
                {if ($iscoordinator and $modulestatus !== 'INITIEEL')}
                <span class="reactive">
                    <button class="btn btn-default btn-circle btn-outline xbtn-sm reactiveerDeelnemer" type="button"><i class="fa fa-circle-thin"></i>
                    </button>
                </span>
                {/if}
                {if ($isjeugdigeofverzorger and $modulestatus !== 'INGEDIEND') or ($iscoordinator)}
                <span class="verwijder">
                    <button class="btn btn-default btn-circle btn-outline xbtn-sm verwijderItem" type="button"><i class="fa fa-trash"></i>
                    </button>
                </span>
                {/if}
              </td>
              {if ($modulestatus == 'INGEDIEND' and $iscoordinator) or $modulestatus == 'INITIEEL'}
              {/if}
            </tr>
          </tbody>
        </table>
    </div>

    {if $iscoordinator}
    <button class="btn btn-sm btn-default" id="btnUitnodigenDeelnemers"><i class="fa fa-envelope-open-o"></i> &nbsp;Deelnemers uitnodigen</button>
    {/if}
    {*if ($isjeugdigeofverzorger and $modulestatus == 'INITIEEL') or ($iscoordinator)*}
    {if ($isjeugdigeofverzorger) or ($iscoordinator)}
      {if $wizard}
      <button class="btn btn-sm btn-primary" id="btnVoegtoeZorgverlener">Voeg een deelnemer toe</button>
      {else}
      <button class="btn btn-sm btn-default" id="btnVoegtoeZorgverlener"><i class="fa fa-plus"></i> &nbsp;Voeg deelnemer toe</button>
      <button type="button" class="btn btn-sm btn-default" id="btnRefreshItemListND"><i class="fa fa-refresh"></i></button>
      {/if}
    {/if}

</div>
