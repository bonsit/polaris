<div class="columngroup">

    <div class="overzicht">
        <div class="content bodyscroll">
            <table id="oudervoogdtabel" class="table data table-striped" tabindex="10">
              <thead>
                <tr>
                  <th xstyle="width:30%">Naam</th>
                  <th>Relatie</th>
                  <th>Emailadres</th>
                  <th>&nbsp;</th>
                </tr>
              </thead>
              <tbody>
                <tr class="row">
                  <td style="display:none" class="id"></td>
                  <td class="naam"></td>
                  <td class="relatie"></td>
                  <td class="emailadres"></td>
                  <td class="actie">
                    <button class="btn btn-default btn-circle btn-outline btn-sm verwijderItem" data-toggle="tooltip" data-placement="left" data-original-title="Verwijder" type="button"><i class="fa fa-times"></i>
                    </button>
                  </td>
                </tr>
              </tbody>
            </table>
        </div>

        {if $wizard}
        <button type="button" class="btn btn-sm btn-primary" id="btnVoegtoeItemOuderVoogd" accesskey="CTRL+N">Ja, voeg een ouder toe</button>
        {else}
        <button type="button" class="btn btn-sm btn-default" id="btnVoegtoeItemOuderVoogd" accesskey="CTRL+N"><i class="fa fa-plus"></i> &nbsp;Voeg toe</button>
        <button type="button" class="btn btn-sm btn-default" id="btnRefreshItemListVGD"><i class="fa fa-refresh"></i></button>
        {/if}

    </div>
</div>