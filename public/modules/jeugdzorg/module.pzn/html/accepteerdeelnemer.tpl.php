{include file="topfields.tpl.php"}

<div class="panel-bodyx">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
        <p>U bent uitgenodigd om deel te nemen aan het PZN van {$item.J_VOORNAAM} {$item.J_ACHTERNAAM}.</p>
        <p>Als u de uitnodiging niet accepteer, dan dient u een reden op te geven.</p>
        <button class="btn btn-success">Accepteer</button><br/>
        <button class="btn btn-danger">Wijs af</button>
        <input class="form-control btn-sm" type="text" name="redenafwijzen" value="" />
        </div>
    </div>
</div>
