<input type="hidden" id="_fldSOORTCOHORT" value="" />
<h1>Nieuwe zorgvrager</h1>

<div id="_rowCOHORT_DATUMGELDIGVANAF" class="formviewrow">
    <label class="label">Geldig vanaf: 01-08-&nbsp;</label>
    <div class="formcolumn">
         <input type="text" label="Geldigheid" class="text" id="_fldCOHORT_JAARGELDIGVANAF" size="8" maxlength='4' value="" />
    </div>
</div>

<div id="_rowCOHORT_DATUMGELDIGTM" class="formviewrow">
    <label class="label">&nbsp;Geldig t/m: 31-07-</label>
    <div class="formcolumn">
         <input type="text" label="Geldigheid" class="text" id="_fldCOHORT_JAARGELDIGTM" size="8" maxlength='4' value="" />
    </div>
</div>

<div id="_rowCOHORT_NEEMINFOVANALGEMENE" class="formviewrow">
    <label class="label"></label>
    <div class="formcolumn">
         <input type="checkbox" label="Neem info over" id="_fldCOHORT_NEEMINFOVANALGEMENE" value="J" />
         &nbsp; Neem info over van algemene cursus
    </div>
</div>

<br />
<div class="buttons">
    <button type="button" tabindex="8800" id="btnSAVEANDCLOSECOHORTCURSUS" class="jqmAccept positive">OK</button>
    <button type="button" tabindex="8810" id="btnCLOSE" class="jqmClose secondary">Annuleer</button>
</div>
