<div class="columngroup">
    <div id="_rowJ_STRAATNAAM" class="form-group form-group-sm">
        <label class="col-sm-4 control-label text-right" title="Adres">Adres</label>
        <div class="col-sm-3">
            <div class="input-group">
                <input type="text" placeholder="Straatnaam" label="Straatnaam" class="input-sm form-control text required validation_allchars" id="_fldJ_STRAATNAAM" name="J_STRAATNAAM" size="50" maxlength="100" value="" autocomplete="street-address" />
            </div>
        </div>
        <div class="col-sm-2">
            <div class="input-group">
                <input type="text" placeholder="Nr" label="Huisnummer" class="input-sm form-control text required validation_allchars" id="_fldJ_HUISNUMMER" name="J_HUISNUMMER" size="15" maxlength="15" value="" autocomplete="" />
            </div>
        </div>
        <div class="col-sm-2">
            <div class="input-group">
                <input type="text" placeholder="Toevoeging" label="Toevoeging" class="input-sm form-control text validation_allchars" id="_fldJ_TOEVOEGING" name="J_TOEVOEGING" size="10" maxlength="10" value="" />
            </div>
        </div>
    </div>

    <div id="_rowJ_POSTCODE" class="form-group form-group-sm">
        <label class="col-sm-4 control-label text-right" title="Postcode">Postcode</label>
        <div class="col-sm-8">
            <div class="input-group">
                <input type="text" label="Postcode" placeholder="Postcode" class="input-sm form-control text required validation_uppercase" id="_fldJ_POSTCODE" name="J_POSTCODE" size="10" maxlength="10" value="" autocomplete="postal-code" />
            </div>
        </div>
    </div>
    <div id="_rowJ_PLAATSNAAM" class="form-group form-group-sm">
        <label class="col-sm-4 control-label text-right" title="Plaatsnaam">Plaatsnaam</label>
        <div class="col-sm-8">
            <div class="input-group">
                <input type="text" label="Plaatsnaam" placeholder="Plaatsnaam" class="input-sm form-control text required validation_allchars" id="_fldJ_PLAATSNAAM" name="J_PLAATSNAAM" size="50" maxlength="50" value="" autocomplete="address-level2" />
            </div>
        </div>
    </div>
</div>
