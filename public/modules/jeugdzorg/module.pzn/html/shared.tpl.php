<link rel="stylesheet" type="text/css" media='all' href="{$serverpath}{$modulepath}/css/default.css" />
<link rel="stylesheet" type="text/css" media='all' href="{$serverpath}{$modulepath}/css/chosen/chosen.css" />
<link rel="stylesheet" type="text/css" media='all' href="{$serverpath}/bower_components/At.js/dist/css/jquery.atwho.css" />
<link rel="stylesheet" type="text/css" media='all' href="{$serverpath}/bower_components/jquery.steps/demo/css/jquery.steps.css" />
<link rel="stylesheet" type="text/css" media='all' href="{$serverpath}/bower_components/enjoyhint/src/jquery.enjoyhint.css" />

<input type="hidden" name="_hdnModuleID" value="{$moduleid}" />
<input type="hidden" name="_hdnProcessedByModule" value="true" />
<input type="hidden" name="_hdnRecordID" id="_fldRecordID" value="{$item.PLR__RECORDID}" />


<input type="hidden" name="_hdnIsJeugdigeOfVerzorger" value="{if $isjeugdigeofverzorger}true{else}false{/if}" />
<input type="hidden" name="_hdnIsNetwerkdeelenemer" value="{if $isnetwerkdeelnemer}true{else}false{/if}" />
<input type="hidden" name="_hdnIsCoordinator" value="{if $iscoordinator}true{else}false{/if}" />
<input type="hidden" name="_hdnActiefVolgnummer" value="{$actiefvolgnummer}" />
<input type="hidden" name="_hdnGekozenZorgverlener" value="{$zorgverlener}" />

<!-- <input type="hidden" id="_fldPZN_ID" value="{$item.PLR__RECORDID}"> -->
<input type="hidden" id="_fldVersie" value="{$huidigeversie}">
