<div class="pzn_topfields">
    <div class="row m-b-lg m-t-lg">
        <div class="col-sm-5">

            <div class="row">
                <div class="col-sm-12" id="profielblock">
                    <div class="profile-image">
                        <img src="{$serverroot}/userphotos/{if $item.J_GESLACHT == 'V'}girl{else}boy{/if}.png" class="img-editable img-circle circle-border m-b-md" alt="profile" />
                        {if $isjeugdigeofverzorger or $iscoordinator and 1==2}
                        {*<div id="btnEditImage" class="img-edit-label"><i class="fa fa-edit"></i><span>Wijzig foto</span></div>*}
                        {/if}
                    </div>
                    <div class="profile-info">
                        <div class="">
                            <div>
                                <h2 class="no-margins">
                                    {$item.J_VOORNAAM} {$item.J_ACHTERNAAM}
                                </h2>
                                <h4>PZN {$item.PZN_ID}</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <table class="table small m-b-xs">
                <tbody>
                <tr>
                    <td>
                        <strong>{$item.J_STRAATNAAM}</strong> {$item.J_HUISNUMMER}
                    </td>

                </tr>
                <tr>
                    <td>
                        <strong>{$item.J_POSTCODE}</strong> {$item.J_PLAATSNAAM}
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>{$item.J_TELEFOONNUMMER}</strong>
                    </td>
                </tr>
                {if $config.debugmode == true}
                <tr>
                    <td>
                        <strong>Module: {$moduleid}</strong>
                    </td>
                </tr>
                {/if}
                </tbody>
            </table>
        </div>
        <div class="col-md-2 status-velden">
            <input type="hidden" id="_fldSTATUS" value="{$item.STATUS}" />
            <small>status</small>
            <h2 class="no-margins huidigestatus">{$item.STATUS}</h2>
            <small>
            {if $item.STATUS == 'INGEDIEND' and !$isnetwerkdeelnemer}
                De coordinator gaat de netwerkdeelnemers uitnodigen om
                hun bijdrage te leveren.
            {/if}
            </small>
            {if $item.STATUS == 'INGEDIEND'}
            <small>Cirkelmodel invullen voor</small>
            <h2 class="no-margins cm_invullenvoor">{$item.EINDDATUM_OPSTELLEN_ZORGPLAN|date_format:"%d-%m-%Y"}</h2>
            {/if}
            {if $item.STATUS == 'RTOGESTART'}
            <small>RTO afronden voor</small>
            <h2 class="no-margins rto_invullenvoor"><span>{$item.EINDDATUM_RTO|date_format:"%d-%m-%Y"}</span>
            </h2>
            {/if}
            {if $item.MOETACCEPTEREN}
            <button id="btnAccepteerUitnodiging" type="button" class="btn btn-sm btn-warning">Accepteer uitnodiging</button>
            {/if}
        </div>
        <div class="col-md-2">
            <!-- Split dropright button -->
            <div {if !($iscoordinator or (!$iscoordinator and !$item.MOETACCEPTEREN))}style="display:none"{/if} class="btn-group" id="btnBekijkZorgprofielND">
              <button type="button" class="btn btn-sm btn-primary" id="btnBekijkZorgprofielNDSameWindow">Ga naar cirkelmodel</button>
              <button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown">
                <span class="fa fa-arrow-right"></span>
              </button>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#" id="btnBekijkZorgprofielNDNewWindow">Open cirkelmodel in nieuw venster</a></li>
              </ul>
            </div>
            {if ($moduleid==4 or $iscoordinator) and $item.STATUS !== 'INGEDIEND' and $item.STATUS !== 'RTOGESTART' and $item.STATUS !== 'HETOOG'}
            <button id="btnPZNIndienen" type="button" type="button" class="btn btn-success">Indienen bij coordinator &nbsp;<i class="fa fa-check"></i></button>
            {/if}

            {if $moduleid==20 and $iscoordinator}
                {if $item.STATUS !== 'RTOGESTART' and $item.STATUS !== 'HETOOG'}
                <button id="btnPZNRTO" type="button" class="btn btn-sm btn-success">RTO Starten voor alle deelnemers...</button>
                {/if}
            {/if}
        </div>
    </div>
</div>

    <div class="columngroupreset"></div>
