<input type="hidden" name="_hdnModuleID" value="{$moduleid}" />

<div class="ibox">
    <div class="ibox-title">
        <h5>{$items|@count} PZN Uitnodigingen</h5>
        <div class="ibox-tools">
            <a href="" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#dlgUitnodigen"><i class="fa fa-paper-plane"></i> &nbsp;Nieuwe jeugdige uitnodigen</a>
            <a href="{$callerquery}" class="btn btn-primary btn-sm"><i class="fa fa-refresh"></i></a>
        </div>
    </div>
    <div class="ibox-content">

        <div class="table-responsive">
        <table class="table issue-tracker">
            <tbody>
                {section loop=$items name=i}
                <tr>
                    <td>
                        <span class="badge badge-{if $items[i].STATUS == 'UITGENODIGD'}success{elseif $items[i].STATUS == 'AANGEMELD'}primary{else}warning{/if}"><small>{$items[i].STATUS}</small></span>
                    </td>
                    <td>
                        {$items[i].DATUM_UITNODIGING}
                    </td>
                    <td>
                        {$items[i].UITGENODIGDE_EMAIL}
                    </td>
                    <td>
                        {$items[i].VOORNAAM} {$items[i].ACHTERNAAM}
                    </td>
                    <td>
                        {$items[i].DATUM_UITGENODIGD}
                    </td>
                    <td class="issue-infox">
                        # {$items[i].ID}

                        {*<small>
                            [ {$items[i].MESSAGEID} ]
                        </small>
                        *}
                    </td>
                    <td>
                        {if $items[i].STATUS neq 'AANGEMELD'}
                        <span class="pie">{$items[i].TIMEPASSED}/5</span> {$items[i].TIMEPASSED}d
                        {/if}
                    </td>
                    <td class="text-right">
                        <button class="btn btn-white btn-xs btnVerstuurUitnodigingOpnieuw" data-id="{$items[i].ID}"> <i class="fa fa-undo"></i> Opnieuw</button>
                    </td>
                </tr>
                {/section}
            </tbody>
        </table>
        </div>
    </div>
</div>
</form>
