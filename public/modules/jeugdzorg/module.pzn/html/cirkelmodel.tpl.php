{include file="shared.tpl.php"}

{if $cirkel_editable}<input type="hidden" id="cirkel_editable" value="true" />{/if}
<div class="row">
    <div class="col-sm-2 text-left">
        <button class="btn btn-primary btn-outline btn-sm" type="button" id="btnCloseScreen" href="{$_backurl}"><i class="fa fa-arrow-left"></i>&nbsp; Terug naar deelnemers en voortgang</button>
    </div>
    <div class="col-sm-8 text-center">
        <div id="btngrpZorgverlener" class="btn-groupx btngrpZorgverlener" xdata-toggle="buttons-checkbox">
        {section name=i loop=$zorgverleners}
            <button class="btn btn-rounded {if $zorgverleners[i].SOORTDEELNEMER == 'Verzorger'}btn-outline{/if} btn-sm {if $zorgverleners[i].VOLGNUMMER == $zorgverlener}btn-primary{else}btn-default {/if}" type="button" data-val="{$zorgverleners[i].VOLGNUMMER}">{if $actiefvolgnummer == $zorgverleners[i].VOLGNUMMER}Mijzelf{else}{$zorgverleners[i].NAAM}{/if}</button>
        {/section}
            <button class="btn btn-rounded btn-sm {if 99999 == $zorgverlener}btn-primary{else}btn-default {/if}" type="button" data-val="99999">Samengesteld</button>
        </div>
    </div>

    <div class="col-sm-1 text-right">
        <div class="btn-group">
            <button data-toggle="dropdown" class="btn btn-primary btn-sm dropdown-toggle">{if $islaatsteversie}Laatste versie{else}Versie {$huidigeversie}{/if} <span class="caret"></span></button>
            <ul class="dropdown-menu dropdown-menu-right">
                {if $versies|@count > 1}
                {foreach from=$versies key=k item=i}
                <li {if $k == $huidigeversie}class="active"{/if}><a href="{$serverroot}/app/pzn/const/cirkelmodel/{$item.PLR__RECORDID}/{$k}/?zorgverlener={$zorgverlener}&_back={$_backurl|escape:'url'}">Versie {$k} &nbsp; <b>{$i.EINDDATUM|date_format:"%d-%m-%Y"|default:"Actief"}</b> &nbsp; {$i.OMSCHRIJVING}
                </a></li>
                {/foreach}
                <li class="divider"></li>
                {/if}
                {if $iscoordinator}
                <li><a href="#" id="btnNieuweVersie"><i class="fa fa-file"></i> &nbsp; Nieuwe versie...</a></li>
                {if $versies|@count > 1}
                <li><a href="#" id="btnVerwijderVersie" data-versie="{$huidigeversie}"><i class="fa fa-trash"></i> &nbsp; Verwijder deze versie</a></li>
                {/if}
                {/if}
            </ul>
        </div>
    </div>
    <div class="col-sm-1 xtext-right">
        <a href="#" id="btnToonCirkelHelp"><i class="fa fa-question-circle fa-2x" style="color:#bcbcbc"></i></a>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <br>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-md-5 cirkel-model-profielinfo">
        <div class="profile-image">
            <img src="{$serverroot}/userphotos/{if $item.J_GESLACHT == 'M'}boy{else}girl{/if}.png" class="img-editable img-circle circle-border m-b-md" alt="profile" />
        </div>
        <div class="text-center xprofielinfo">
            <h2 class="no-margins">
                {$item.J_VOORNAAM} {$item.J_ACHTERNAAM}
            </h2>
            <h4>PZN {$item.PZN_ID}</h4>
        </div>

        <svg id="holder" class="{$cirkel_editable}" xwidth="100%" xheight="100%" viewBox="20 -20 520 400" xpreserveAspectRatio="xMaxYMax"></svg>

        {if $islaatsteversie == true}
        <div class="text-center">
            <div class="cirkel-info">
                {if $item.STATUS == 'INGEDIEND'}
                <small>Cirkelmodel invullen voor</small>
                <h2 class="no-margins">{$item.EINDDATUM_OPSTELLEN_ZORGPLAN|date_format:"%d-%m-%Y"}</h2>
                {/if}
            </div>
            {section name=i loop=$zorgverleners}
                {if $zorgverlener == $zorgverleners[i].VOLGNUMMER}
                    {if isset($zorgverleners[i].CIRKELMODELAFGEROND)}
                    <h3 id="txtCirkelModelAfgerond">Cirkelmodel afgerond.</h3>
                    {else}
                        {if $actiefvolgnummer == $zorgverleners[i].VOLGNUMMER}
                            <button type="button" id="btnCirkelModelKlaar" class="btn btn-success">Klaar met cirkelmodel</button>
                        {else}
                            <h3 id="txtCirkelModelAfgerond">Nog bezig met invullen...</h3>
                        {/if}
                    {/if}
                {/if}
            {/section}
        </div>
        {/if}
    </div>
    <div class="col-sm-12 col-md-7">

        <div class="pagetabcontainer" style="display:none">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a id="tab_probleemspec" data-toggle="tab" href="#probleemspec">Aandachtspunt</a>
                </li>
            </ul>
        </div>

        <!-- <select class="slcZorgverlener" id="slcZorgverlener">{html_options options=$zorgverleners selected=$zorgverlener}</select> -->
        <input type="hidden" name="zorgbesteed" value="N">
<!--         <input type="hidden" label="PZN id" disabled class="input-sm form-control text required validation_allchars valid" id="_fldPZN_ID" name="PZN_ID" size="50" maxlength="50" value="{$item.PLR__RECORDID}">
 -->
        <div id="nodata" class="nodata" style="display:none"><h3>Er zijn geen aandachtspunten bekend. Klik op een segment om er een toe te voegen.</h3>
            <img src="{$moduleroot}/html/arrow.png" width="200px"></div>
        <div id="probleemspec" style="display:none">
            <div class="content bodyscroll">
                <table id="probleemspecificatie" class="table data xtable-striped {if $cirkel_editable}editable{/if}" tabindex="10">
                    <thead>
                        <tr>
                            <th id="th_naam2">Aandachtspunten
                                <button id="toggleZorgBesteed" class="btn btn-default btn-xs btn-roundedx"> Toggle besteedde zorg </button>
                            </th>
                            <th xstyle="width:30%">Gebied</th>
                            <th>Ernst</th>
                            <th></th>
                            <th class="hide">ID</th>
                            <th class="hide">Code</th>
                            <th class="hide">Domein</th>
                            <th class="hide">Niveau</th>
                        </tr>
                    </thead>
                    <tbody>
<!--                         <tr class="row" id="adg_">
                            <td class="naam"></td>
                            <td class="probleem"></td>
                            <td class="gebied"></td>
                            <td class="ernst"></td>
                            <td class="aantalzorg"></td>
                            <td class="actie">
                                <button class="btn btn-default btn-outline btn-xs wijzigItem" data-toggle="tooltip" data-placement="left" data-original-title="Wijzig" type="button"><i class="fa fa-edit"></i>
                                </button>
                                <button class="btn btn-default btn-outline btn-xs verwijderItem" data-toggle="tooltip" data-placement="left" data-original-title="Verwijder" type="button"><i class="fa fa-trash"></i>
                                </button>
                            </td>
                            <td class="id hide"></td>
                            <td class="code hide"></td>
                            <td class="domein hide"></td>
                            <td class="niveau hide"></td>
                        </tr> -->
                    </tbody>
                </table>

<!--                 {if $cirkel_editable}
                <button id="btnToonVerleendeZorgForm" class="btn btn-primary xbtn-outline btn-sm" type="button">Voeg verleende zorg toe...
                </button>
                {/if}
 -->
            </div>
<!--             <div id="pnlVerleendeZorg" class="content fixedheight">
                <table id="verleendezorgtabel" class="table data table-striped" tabindex="10">
                    <thead>
                        <tr>
                            <th xstyle="width:10%" class="hide">ID</th>
                            <th xstyle="width:10%">Verleende zorg</th>
                            <th xstyle="width:30%">Frequentie</th>
                            <th>Intensiteit</th>
                            <th>Sinds</th>
                            <th>Tot</th>
                            <th>Effect</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="row">
                            <td class="id hide"></td>
                            <td class="zorg"></td>
                            <td class="frequentie"></td>
                            <td class="intensiteit"></td>
                            <td class="sinds"></td>
                            <td class="tot"></td>
                            <td class="effect"></td>
                            <td class="actie">
                                <button class="btn btn-default btn-outline btn-xs wijzigItem" data-toggle="tooltip" data-placement="left" data-original-title="Wijzig" type="button"><i class="fa fa-edit"></i>
                                </button>
                                <button class="btn btn-default btn-outline btn-xs verwijderItem" data-toggle="tooltip" data-placement="left" data-original-title="Verwijder" type="button"><i class="fa fa-trash"></i>
                                </button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div> -->
        </div>
    </div>
</div>

<div id="dlgVoegAandachtsGebiedToe" class="modal inmodal fade" aria-hidden="true" tabindex="-1">
    <div class="modal-dialog modal-lg" data-backdrop="static">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Aandachtspunt toevoegen</h4>
            </div>
            <form class="form-horizontal" id="frmVoegAandachtsGebiedToe">
                <input type="hidden" name="_hdnState" value="insert" />
                <div class="modal-body">
                    <div class="row form-inline form-smx">
                        <input id="segment" type="hidden" name="segment" class="form-control input-sm" placeholder="Kies segment">
                        <div class="col-sm-8">
                            <div class="form-group">
                                <div class="row">
                                        <label>Aandachtspunt &nbsp;</label><br>
                                    <div class="input-group">
                                        <select id="slcProbleem" name="probleem" class="form-control input-sm">
                                        </select>
                                    </div>
                                </div>
                                <div class="row" id="aandachtsgebiedandersrow" style="display:none">
                                    <div class="input-group">
                                        <input type="text" style="width:360px" name="aandachtsgebiedanders" id="aandachtsgebiedanders" placeholder="Vul aandachtspunt in..." class="form-control" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Ernst</label><br/>
                                <div class="ernstgroup">
                                    <label class="radio ernst-zwaar" for="option-0">
                                        <input name="ERNST" id="option-0" value="zwaar" type="radio"> Zwaar
                                    </label>
                                    <label class="radio ernst-matig" for="option-1">
                                        <input name="ERNST" id="option-1" value="matig" type="radio"> Matig
                                    </label>
                                    <label class="radio ernst-kans" for="option-2">
                                        <input name="ERNST" id="option-2" value="kans" type="radio"> Kans
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Annuleer</button>
                    <button type="button" id="btnBewaarAandachtsGebied" class="btn btn-primary">Voeg toe</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal inmodal" id="dlgVoegZorgToe" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content x_animated fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Sluiten</span></button>
                <h4 class="modal-title">Geef de verleende zorg aan</h4>
            </div>
            <form class="form-horizontal" id="frmVoegZorgToe">
                <input type="hidden" name="_hdnRecordID" value="" />
                <input type="hidden" name="_hdnState" value="insert" />
                <div class="panel-body">
                    <div class="">

                        <div class="row form-group">
                            <div class="col-sm-12 col-md-4">
                                Verleende zorg
                            </div>
                            <div class="col-sm-12 col-md-8">
                                <select id="slcZorg" name="ZORG" class="form-control xinput-sm">
                                    <!--<option>Andere cellen...</option> -->
                                    <option></option>
                                    <option>Anders...</option>
                                </select>
                            </div>
                        </div>
                        <div class="row form-group" id="zorgandersrow" style="display:none">
                            <div class="col-sm-12 col-md-4">
                                Andere zorg
                            </div>
                            <div class="col-sm-12 col-md-8">
                                <div class="input-group">
                                    <input type="text" style="min-width:360px;" name="zorganders" id="zorganders" placeholder="Vul zorg in..." class="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-12 col-md-4">
                                Frequentie
                            </div>
                            <div class="col-sm-12 col-md-8">
                                <div class="btn-group" id="grpFreqentie" data-toggle="buttons">
                                    <input type="text" class="pull-left form-control" name="FREQUENTIEAANTAL" value="1" style="width:50px" />
                                    <label class="btn btn-default active" data-toggle="tab"><input type="radio" tabindex="-1" name="FREQUENTIE" value="Per jaar" checked="checked" />Per jaar</label>
                                    <label class="btn btn-default" data-toggle="tab"><input type="radio" tabindex="-1" name="FREQUENTIE" value="Per maand" />Per maand</label>
                                    <label class="btn btn-default" data-toggle="tab"><input type="radio" tabindex="-1" name="FREQUENTIE" value="Per week" />Per week</label>
                                    <label class="btn btn-default" data-toggle="tab"><input type="radio" tabindex="-1" name="FREQUENTIE" value="Per dag" />Per dag</label>
                                    <label class="btn btn-default" data-toggle="tab"><input type="radio" tabindex="-1" name="FREQUENTIE" value="Eenmalig" />Eenmalig</label>
                                    <label class="btn btn-default" data-toggle="tab"><input type="radio" tabindex="-1" name="FREQUENTIE" value="Nvt" />NVT.</label>
                                </div>
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-sm-12 col-md-4">
                                Duur
                            </div>

                            <div class="col-sm-12 col-md-8">
                                <div class="btn-group" id="grpDuur" data-toggle="buttons">
                                    <input type="text" class="pull-left form-control" name="INTENSITEITAANTAL" value="20" style="width:50px" />
                                    <label class="btn btn-default active" data-toggle="tab"><input type="radio" tabindex="-1" name="INTENSITEIT" value="Minuten" checked="checked" />Minuten</label>
                                    <label class="btn btn-default" data-toggle="tab"><input type="radio" tabindex="-1" name="INTENSITEIT" value="Uur" />Uur</label>
                                    <label class="btn btn-default" data-toggle="tab"><input type="radio" tabindex="-1" name="INTENSITEIT" value="Dagdeel" />Dagen</label>
                                    <label class="btn btn-default" data-toggle="tab"><input type="radio" tabindex="-1" name="INTENSITEIT" value="Nvt" />NVT.</label>
                                </div>
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-sm-12 col-md-4">
                                Sinds/tot
                            </div>

                            <div class="col-sm-12 col-md-8">
                                <div class="input-daterange input-group" id="datepicker">
                                    <input type="text" class="input-smx form-control" name="SINDS" value=""/>
                                    <span class="input-group-addon">tot</span>
                                    <input type="text" class="input-smx form-control" name="TOT" value="" />
                                </div>
                            </div>
                        </div>

                        <div class="row form-group form-inline form-sm">
                            <div class="col-sm-12 col-md-4">
                                Effect van de zorg
                            </div>

                            <div class="col-sm-12 col-md-8">
                                <div class="ernstgroup required">
                                    <label class="radio effect-label-3" for="effect-option-3"> Onbek.
                                        <input name="EFFECT" id="effect-option-3" value="3" type="radio">
                                    </label>
                                    <label class="radio effect-label-0" for="effect-option-0"> Nauwelijks
                                        <input name="EFFECT" id="effect-option-0" value="0" type="radio">
                                    </label>
                                    <label class="radio effect-label-1" for="effect-option-1"> Matig
                                        <input name="EFFECT" id="effect-option-1" value="1" type="radio">
                                    </label>
                                    <label class="radio effect-label-2" for="effect-option-2"> Positief
                                        <input name="EFFECT" id="effect-option-2" value="2" type="radio">
                                    </label>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Annuleren</button>
                    <button type="button" class="btn btn-primary btnSave" id="btnBewaarVerleendeZorg">Voeg toe</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal inmodal" id="dlgVoegVersieToe" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content x_animated fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Sluiten</span></button>
                <h4 class="modal-title">Wilt u een nieuwe versie starten?</h4>
            </div>
            <form class="form-horizontal" id="frmVoegZorgToe">
                <input type="hidden" name="_hdnRecordID" value="" />
                <input type="hidden" name="_hdnState" value="insert" />
                <div class="panel-body">
                    <div class="">

                        <div class="row form-group">
                            <div class="col-sm-12 col-md-4">
                                Omschrijf deze versie
                            </div>
                            <div class="col-sm-12 col-md-8">
                                <input type="text" maxlength="50" style="width:360px;" name="OMSCHRIJVING" placeholder="Korte omschrijving (max 50 karakters)" />
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Annuleren</button>
                    <button type="button" class="btn btn-primary btnSave" id="btnStartNieuweVersie">Start versie</button>
                </div>
            </form>
        </div>
    </div>
</div>
{literal}
<style>
    #topbar {display:none;}
</style>
{/literal}