<div class="row">
    {section loop=$items name=i}
    <div class="col-sm-6 col-md-4 col-lg-3">
        {if $items[i].WISSEL_COORDINATOR == $actiefvolgnummer and $items[i].WISSEL_STATUS == 'VERSTUURD'}
        <div class="badge badge-danger pull-right">Uitnodiging</div>
        {/if}
        <span class="badge badge-success pull-left">{$items[i].STATUS}</span>
        <div class="widget-head-color-box navy-bg p-sm text-center">

            <a href="{$callerquery}edit/{$items[i].PLR__RECORDID}/"><div class="m-b-md">
                <h2 class="font-bold no-margins">
                    {$items[i].J_VOORNAAM} {$items[i].J_ACHTERNAAM}
                </h2>
                PZN #{$items[i].PZN_ID}
            </div></a>
            <a href="{$callerquery}edit/{$items[i].PLR__RECORDID}/"><img src="{$serverroot}/userphotos/{if $items[i].J_GESLACHT == 'M'}boy{else}girl{/if}.png" style="width:100px;" class="img-circle circle-border m-b-md" /></a>
            <div>
                <span>{$items[i].J_STRAATNAAM} {$items[i].J_HUISNUMMER}{$items[i].J_TOEVOEGING}</span> &nbsp;|&nbsp;
                <span>{$items[i].J_POSTCODE}</span> &nbsp;|&nbsp;
                <span>{$items[i].J_PLAATSNAAM}</span><br>
                {$items[i].J_TELEFOONNUMMER}<br>
                <div class="btn-group">
                    {if $items[i].MOETACCEPTEREN == true}
                    <button type="button" class="accepteerUitnodiging btn btn-sm btn-warning" data-recordid="{$items[i].PLR__RECORDID}" data-pznid="{$items[i].PZN_ID}">Accepteer uitnodiging</button>
                    {/if}
                    <a href="{$callerquery}edit/{$items[i].PLR__RECORDID}/" class="btn btn-success btn-sm" title="{lang pin_this_record}"><i class="fa fa-edit"></i> Bekijk</a>
                {*
                    <a href="tel:{$items[i].J_TELEFOONNUMMER}" class="btn btn-success"><i class="fa fa-phone"></i> Bel </a>
                    <a href="mailto:{$items[i].V_E_MAILADRES}?subject=PZN" class="btn btn-success chatsession" data-pzn="{$items[i].PZN_ID}"><i class="fa fa-send"></i> Mail</a>
                    <a href="{$callerquery}edit/{$items[i].PLR__RECORDID}/" class="btn btn-success" title="{lang pin_this_record}"><i class="fa fa-edit"></i> Bekijk</a>
                *}
                </div>
            </div>
        </div>
    </div>

    {sectionelse}
    <h3>Er is geen informatie gevonden.</h3>
<!--     <a href="" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#dlgUitnodigen"><i class="fa fa-paper-plane"></i> &nbsp;Nieuwe jeugdige aanmelden</a>
 -->
    {/section}
</div>
