{if $moduleid neq 15}{* 15 = uitnodigingen *}
<div class="modal inmodal" id="dlgTZV" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content x_animated fadeIn">
            <div class="modal-header">
                <i class="fa fa-user-plus modal-icon"></i>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Sluiten</span></button>
                <h4 class="modal-title">Ouder</h4>
            </div>
            <form class="form-horizontal" id="frmTZV">
                <input type="hidden" name="_hdnRecordID" value="" />
                <input type="hidden" name="_hdnPersoonID" value="" />
                <input type="hidden" name="_hdnState" value="insert" />
                <div class="modal-body">

                    <div class="form-group">
                        <label class="col-lg-3 control-label">Geslacht</label>
                        <div class="col-lg-9 radio required">
                            <label>
                                <input type="radio" value="M" id="tzvGeslachtM" name="GESLACHT"> Man
                            </label> &nbsp;
                            <label>
                                <input type="radio" value="V" id="tzvGeslachtV" name="GESLACHT"> Vrouw
                            </label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label">Voornaam</label>
                        <div class="col-lg-9"><input type="text" placeholder="Voornaam" class="required form-control" name="VOORNAAM"></div>
                    </div>

                    <div class="form-group"><label class="col-lg-3 control-label">Achternaam</label>
                        <div class="col-lg-9"><input type="text" placeholder="Achternaam" class="required form-control" name="ACHTERNAAM"></div>
                    </div>

                    <div class="form-group"><label class="col-lg-3 control-label">Email</label>
                        <div class="col-lg-9"><input type="email" placeholder="Email" class="form-control" name="E_MAILADRES">
                        </div>
                    </div>

                    <div class="form-group"><label class="col-lg-3 control-label"><i class="fa fa-warning"></i></label>
                        <div class="col-lg-9">Indien het emailadres wordt ingevuld, dan krijgt deze persoon ook toegang tot dit PZN.
                        </div>
                    </div>

                    <div class="form-group"><label class="col-lg-3 control-label">Soort relatie</label>
                        <div class="col-lg-9">
                            <select class="required form-control" name="SOORTRELATIE">
                                <option>Ouder</option>
                                <option>Pleegouder</option>
                                <option>Verzorger</option>
                                <!-- <option>Voogd</option>
                                <option>Gezinsvoogd</option> -->
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Annuleren</button>
                    <button type="button" class="btn btn-primary btnSave" id="btnTZVSave">Voeg toe</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal inmodal" id="dlgND" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content x_animated fadeIn">
            <div class="modal-header">
                <i class="fa fa-user-plus modal-icon"></i>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Sluiten</span></button>
                <h4 class="modal-title">Netwerkdeelnemer</h4>
            </div>
            <form class="form-horizontal" id="frmND">
                <input type="hidden" name="_hdnRecordID" value="" />
                <input type="hidden" name="_hdnPersoonID" value="" />
                <input type="hidden" name="_hdnState" value="insert" />
                <input type="hidden" name="ISCOORDINATOR" value="" />
                <div class="modal-body">

                    <div class="tabs-container">

                        <div class="btn-group" id="soortdeelnemergroup" data-toggle="buttons">
                            <label class="btn btn-primary" data-toggle="tab" href="#tab-1"><input type="radio" tabindex="-1" name="SOORTDEELNEMER" value="Zorgverlener" />Geregis. zorgverlener</label>
                            <label class="btn btn-primary" data-toggle="tab" href="#tab-2"><input type="radio" tabindex="-1" name="SOORTDEELNEMER" value="Mantelzorger" />Mantelzorger</label>
                            <label class="btn btn-primary" data-toggle="tab" href="#tab-3"><input type="radio" tabindex="-1" name="SOORTDEELNEMER" value="Extra-deelnemer" />Extra deelnemer</label>
                        </div>

                        <div class="tab-content">
                            <div id="tab-1" class="tab-pane active">
                                <div class="panel-body">
                                    <p>Kies uw zorgverlener door in de lijst de functie te kiezen en daarna de gewenste zorgverlener.</p>
                                    <select id="fldSoortFunctie" placeholder="Kies de functie" class="form-control">{html_options options=$soortfuncties}</select>
                                    <select size="8" style="width:100%" id="zorgVerlenerID" class="required form-control" name="ZORGVERLENER_VOLGNUMMER" value=""></select>
                                    {*
                                    <input type="hidden" id="zorgVerlenerID" class="required" name="ZORGVERLENER_VOLGNUMMER" value="" />
                                    <input type="text" style="width:100%" id="zoekZorgverlener" class="required form-control input-md" placeholder="Zoek een zorgverlener" data-provide="typeahead" autocomplete="off" value="" />
                                    *}
                                </div>
                            </div>
                            <div id="tab-2" class="tab-pane">
                                <div class="panel-body">

                                    <div class="form-group"><label class="col-lg-3 control-label">Voornaam</label>
                                        <div class="col-lg-9"><input type="text" placeholder="Voornaam" class="required form-control" name="VOORNAAM"></div>
                                    </div>
                                    <div class="form-group"><label class="col-lg-3 control-label">Achternaam</label>
                                        <div class="col-lg-9"><input type="text" placeholder="Achternaam" class="required form-control" name="ACHTERNAAM"></div>
                                    </div>

                                    <div class="form-group"><label class="col-lg-3 control-label">Aard relatie</label>
                                        <div class="col-lg-9">
                                            <input type="text" class="required form-control" placeholder="Aard relatie" name="SOORTZORG" />
                                        </div>
                                    </div>

                                    <div class="form-group"><label class="col-lg-3 control-label">Telefoonnr</label>

                                        <div class="col-lg-9"><input type="text" placeholder="Telefoonnr" class="form-control" name="TELEFOONNUMMER">
                                        </div>
                                    </div>

                                    <div class="form-group"><label class="col-lg-3 control-label">Email</label>

                                        <div class="col-lg-9"><input type="email" placeholder="Email" class="required form-control" name="E_MAILADRES">
                                        </div>
                                    </div>

                                    <div class="form-group nd_uitnod_opnieuw" id=""><label class="col-lg-3 control-label">&nbsp;</label>
                                        <div class="col-lg-9"><button class="btn btn-success btn-sm btnNDOpnieuwVersturen">Uitnodiging opnieuw sturen</button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div id="tab-3" class="tab-pane">
                                <div class="panel-body">

                                    <div class="form-group"><label class="col-lg-3 control-label">Voornaam</label>
                                        <div class="col-lg-9"><input type="text" placeholder="Voornaam" class="required form-control" name="VOORNAAM"></div>
                                    </div>
                                    <div class="form-group"><label class="col-lg-3 control-label">Achternaam</label>
                                        <div class="col-lg-9"><input type="text" placeholder="Achternaam" class="required form-control" name="ACHTERNAAM"></div>
                                    </div>

                                    <div class="form-group"><label class="col-lg-3 control-label">Soort zorg</label>
                                        <div class="col-lg-9">
                                            <input type="text" class="required form-control" placeholder="Soort zorg" name="SOORTZORG" />
                                            {*<select class="required form-control" name="SOORTZORG">{html_options options=$soortzorg}</select>*}
                                        </div>
                                    </div>

                                    <div class="form-group"><label class="col-lg-3 control-label">Telefoonnr</label>

                                        <div class="col-lg-9"><input type="text" placeholder="Telefoonnr" class="form-control" name="TELEFOONNUMMER">
                                        </div>
                                    </div>

                                    <div class="form-group"><label class="col-lg-3 control-label">Email</label>

                                        <div class="col-lg-9"><input type="email" placeholder="Email" class="required form-control" name="E_MAILADRES">
                                        </div>
                                    </div>

                                    <div class="form-group nd_uitnod_opnieuw" id=""><label class="col-lg-3 control-label">&nbsp;</label>
                                        <div class="col-lg-9"><button class="btn btn-success btn-sm btnNDOpnieuwVersturen">Uitnodiging opnieuw sturen</button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Annuleren</button>
                    <button type="button" class="btn btn-primary btnSave" id="btnGZVSave">Voeg toe</button>
                </div>

            </form>
        </div>
    </div>
</div>

<div class="modal inmodal" id="dlgConfirmGZVCoordinator" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content x_animated fadeIn">
            <div class="modal-header">
                <i class="fa fa-user-plus modal-icon"></i>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Sluiten</span></button>
                <h4 class="modal-title">Beantwoorden uitnodiging</h4>
            </div>
            <form class="form-horizontal" id="frmConfirmGZVCoordinator">
                <input type="hidden" name="_hdnRecordID" value="" />
                <input type="hidden" name="_hdnState" value="insert" />
                <div class="modal-body">
                    <div class="form-group"><label class="col-lg-3 control-label">Naam</label>
                        <div class="col-lg-9"><input type="text" placeholder="Naam" class="required form-control" name="NAAM"></div>
                    </div>

                    <div class="form-group"><label class="col-lg-3 control-label">Soort zorg</label>
                        <div class="col-lg-9">
                            <select class="required form-control" name="SOORT_ZORG">{html_options options=$soortzorg}</select>
                        </div>
                    </div>

                    <div class="form-group"><label class="col-lg-3 control-label">Telefoonnr</label>

                        <div class="col-lg-9"><input type="text" placeholder="Telefoonnr" class="form-control" name="TELEFOONNR">
                        </div>
                    </div>

                    <div class="form-group"><label class="col-lg-3 control-label">Email</label>

                        <div class="col-lg-9"><input type="email" placeholder="Email" class="form-control" name="EMAILADRES">
                        </div>
                    </div>

                    <div class="form-group"><label class="col-lg-3 control-label">Beslissing t.a.v. uitnodiding</label>
                        <div class="col-lg-9 radio required">
                            <label>
                                <input type="radio" value="M" id="tzvBeslissingA" checked disabled name="BESLISSING"> Accepteren
                            </label> &nbsp;
                            <label>
                                <input type="radio" value="V" id="tzvBeslissingW" disabled name="BESLISSING"> Weigeren
                            </label>
                        </div>
                    </div>

                    <div class="form-group"><label class="col-lg-3 control-label">Motivatie weigering</label>

                        <div class="col-lg-9"><input type="text" placeholder="Motivatie" class="form-control" name="MOTIVATIE">
                        </div>
                    </div>

                    <div class="form-group"><label class="col-lg-3 control-label">Toelichting</label>

                        <div class="col-lg-9">
                            <textarea type="text" placeholder="Toelichting" class="form-control" name="TOELICHTING">
                        </textarea>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Annuleren</button>
                    <button type="button" class="btn btn-primary btnSave" id="btnConfirmGZVCoordinatorSave">Opslaan</button>
                </div>

            </form>
        </div>
    </div>
</div>

<div class="modal inmodal" id="dlgConfirmGZV" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content x_animated fadeIn">
            <div class="modal-header">
                <i class="fa fa-user-plus modal-icon"></i>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Sluiten</span></button>
                <h4 class="modal-title">Uitnodiging geregistreerde zorgverlener</h4>
            </div>
            <form class="form-horizontal" id="frmConfirmGZV">
                <input type="hidden" name="_hdnRecordID" value="" />
                <input type="hidden" name="_hdnState" value="insert" />
                <div class="modal-body">
                    <div class="form-group"><label class="col-lg-3 control-label">Naam</label>
                        <div class="col-lg-9"><input type="text" placeholder="Naam" class="required form-control" name="NAAM"></div>
                    </div>

                    <div class="form-group"><label class="col-lg-3 control-label">Soort zorg</label>
                        <div class="col-lg-9">
                            <select class="required form-control" name="SOORT_ZORG">{html_options options=$soortzorg}</select>
                        </div>
                    </div>

                    <div class="form-group"><label class="col-lg-3 control-label">Telefoonnr</label>

                        <div class="col-lg-9"><input type="text" placeholder="Telefoonnr" class="form-control" name="TELEFOONNR">
                        </div>
                    </div>

                    <div class="form-group"><label class="col-lg-3 control-label">Email</label>

                        <div class="col-lg-9"><input type="email" placeholder="Email" class="form-control" name="EMAILADRES">
                        </div>
                    </div>

                    <div class="form-group"><label class="col-lg-3 control-label">Beoordeling coördinator. Uitnodigen?</label>
                        <div class="col-lg-9 radio required">
                            <label>
                                <input type="radio" value="M" id="tzvBeslissingA" checked name="BESLISSING"> Ja
                            </label> &nbsp;
                            <label>
                                <input type="radio" value="V" id="tzvBeslissingW" name="BESLISSING"> ?
                            </label>
                        </div>
                    </div>

                    <div class="form-group"><label class="col-lg-3 control-label">Opmerking</label>

                        <div class="col-lg-9">
                            <textarea type="text" placeholder="Toelichting" class="form-control" name="TOELICHTING"></textarea>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Annuleren</button>
                    <button type="button" class="btn btn-primary btnSave" id="btnConfirmGZVSave">Opslaan</button>
                </div>

            </form>
        </div>
    </div>
</div>

<div class="modal inmodal" id="dlgNGZV" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content x_animated fadeIn">
            <div class="modal-header">
                <i class="fa fa-user-plus modal-icon"></i>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Sluiten</span></button>
                <h4 class="modal-title">Persoon in Netwerk</h4>
            </div>
            <form class="form-horizontal" id="frmNGZV">
                <input type="hidden" name="_hdnRecordID" value="" />
                <input type="hidden" name="_hdnState" value="insert" />
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-lg-3 control-label">Geslacht</label>
                        <div class="col-lg-9 radio required">
                            <label>
                                <input type="radio" value="M" id="ngzvGeslachtM" name="GESLACHT"> Man
                            </label> &nbsp;
                            <label>
                                <input type="radio" value="V" id="ngzvGeslachtV" name="GESLACHT"> Vrouw
                            </label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label">Voornaam</label>
                        <div class="col-lg-9"><input type="text" placeholder="Voornaam" class="required form-control" name="VOORNAAM"></div>
                    </div>

                    <div class="form-group"><label class="col-lg-3 control-label">Achternaam</label>
                        <div class="col-lg-9"><input type="text" placeholder="Achternaam" class="required form-control" name="ACHTERNAAM"></div>
                    </div>

                    <div id="_rowV_STRAATNAAM" class="form-group">
                        <label class="col-lg-3 control-label text-right" title="STRAATNAAM">Adres</label>
                        <div class="col-sm-4">
                            <div class="input-group">
                                <input type="text" placeholder="Straatnaam" label="Straatnaam" class="input-sm form-control text  validation_allchars" id="_fldV_STRAATNAAM" name="STRAATNAAM" size="50" maxlength="100" value="">
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="input-group">
                                <input type="text" placeholder="Nr" label="Huisnummer" class="input-sm form-control text  validation_allchars" id="_fldV_HUISNUMMER" name="HUISNUMMER" size="15" maxlength="15" value="">
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="input-group">
                                <input type="text" placeholder="Toevoeging" label="Toevoeging" class="input-sm form-control text validation_allchars" id="_fldV_TOEVOEGING" name="TOEVOEGING" size="10" maxlength="10" value="">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label text-right" title="POSTCODE">Postcode</label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <input type="text" label="Postcode" placeholder="Postcode" class="input-sm form-control text validation_allchars" id="_fldNGV_POSTCODE" name="POSTCODE" size="10" maxlength="10" value="">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label text-right" title="PLAATSNAAM">Plaatsnaam</label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <input type="text" label="Plaatsnaam" placeholder="Plaatsnaam" class="input-sm form-control text validation_allchars" id="_fldNGV_PLAATSNAAM" name="PLAATSNAAM" size="50" maxlength="50" value="">
                            </div>
                        </div>
                    </div>

                    <div class="form-group"><label class="col-lg-3 control-label">Soort relatie</label>
                        <div class="col-lg-9"><input type="text" placeholder="Relatie" class="required form-control" name="RELATIE_TOV_ZORGGERECHTIGDE">
                        </div>
                    </div>

                    <div class="form-group"><label class="col-lg-3 control-label">Telefoonnr</label>
                        <div class="col-lg-9"><input type="text" placeholder="Telefoonnr" class="form-control" name="TELEFOONNUMMER">
                        </div>
                    </div>

                    <div class="form-group"><label class="col-lg-3 control-label">Email</label>
                        <div class="col-lg-9"><input type="email" placeholder="Email" class="required form-control" name="E_MAILADRES">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Annuleren</button>
                    <button type="button" class="btn btn-primary btnSave" id="btnNGZVSave">Voeg toe</button>
                </div>

            </form>
        </div>
    </div>
</div>

<div class="modal inmodal" id="dlgZorgprofiel" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content x_animated fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Sluiten</span></button>
                <h4 class="modal-title">Zorgprofiel</h4>
            </div>
            <form class="form-horizontal" id="frmZorgprofiel">
                <input type="hidden" name="_hdnRecordID" value="" />
                <input type="hidden" name="_hdnState" value="insert" />
                <div class="modal-body">
                    {*include file="cirkelmodel.tpl.php"*}
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Annuleren</button>
                    <button type="button" class="btn btn-primary btnSave" id="btnNGZVSave">Voeg toe</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal inmodal" id="dlgWijzigCoordinator" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content x_animated fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Sluiten</span></button>
                <h4 class="modal-title">Nodig nieuwe coordinator uit</h4>
            </div>
            <form class="form-horizontal" id="frmCoordinatorWissel">
                <div class="panel-body">
                    <!-- De a's bij <p>...naam...</p> en de placeholder zijn speciale a's zodat de autofill van de browser niet geactiveerd wordt => ɑ -->
                    <p>Zoek een coordinator door de nɑɑm of woonplaats in te typen...</p>
                    <input type="hidden" id="coordinatorID" name="COORDINATOR_VOLGNUMMER" value="" />
                    <input type="text" style="width:100%" id="zoekCoordinator" class="form-control input-md always-on" placeholder="Nɑɑm of woonplaats" data-provide="typeahead" autocomplete="off" />

                    <textarea class="form-control always-on" name="BERICHT" placeholder="Persoonlijk bericht voor nieuwe coördinator"></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Annuleren</button>
                    <button type="button" class="btn btn-primary btnSave" id="btnWijzigCoordinatorSave">Uitnodigen</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal inmodal" id="dlgRTOStarten" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content x_animated fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Sluiten</span></button>
                <h4 class="modal-title">Nieuwe rondetafeloverleg</h4>
            </div>
            <form class="form-horizontal" id="frmRTOStarten">
                <div class="panel-body">
                    <label><input type="checkbox" checked="checked" id="fldNotificeerDeadline" class="required" value="J"> Notificeer netwerkdeelnemers over nieuwe deadline</label>
                    <br/><br/>
                    <p>Wanneer moet het rondetafel overleg klaar zijn?</p>
                    <div class="input-group date">
                        <input type="text" id="fldRTODeadline" placeholder="Datum" label="Datum" format="dd-mm-yyyy" class="input-sm form-control text validation_date date_input required" >
                        <div class="input-group-addon">
                            <span class="glyphicon glyphicon-th"></span>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Annuleren</button>
                    <button type="button" class="btn btn-primary btnSave" id="btnRTOStartenSave">RTO Starten</button>
                </div>
            </form>
        </div>
    </div>
</div>

{/if}

<div class="modal inmodal" id="dlgUitnodigen" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content x_animated fadeIn">
            <div class="modal-header">
                <i class="fa fa-user-plus modal-icon"></i>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Sluiten</span></button>
                <h4 class="modal-title">Uitnodiging PZN</h4>
            </div>
            <form class="form-horizontal" id="frmUitnodigen" method="">
                <input type="hidden" name="_hdnState" value="insert" />
                <input type="hidden" name="_hdnFormHash" value="{$_hdnFormHash}" />
                <input type="hidden" name="_hdnTable" value="PZN_UITNODIGING" />
                <div class="modal-body">

                        <p>Vul hieronder de gegevens in van de jeugdige of de verzorger.</p>

                    <div class="form-group">
                        <label class="col-lg-4 control-label">Voornaam jeugdige</label>
                        <div class="col-lg-8"><input type="text" placeholder="Voornaam" class="required form-control" name="VOORNAAM"></div>
                    </div>

                    <div class="form-group"><label class="col-lg-4 control-label">Achternaam jeugdige</label>
                        <div class="col-lg-8"><input type="text" placeholder="Achternaam" class="required form-control" name="ACHTERNAAM"></div>
                    </div>

                    <div class="form-group"><label class="col-lg-4 control-label">Verantwoordlijke </label>
                        <div class="col-lg-8 radio required">
                            <label>
                                <input type="radio" value="Verzorger" checked="checked" id="fldSOORTUITNODIGINGV" name="SOORTUITNODIGING"> Verzorger
                            </label> &nbsp;
                            <label>
                                <input type="radio" value="Jeugdige" id="fldSOORTUITNODIGINGJ" name="SOORTUITNODIGING"> Jeugdige
                            </label>
                        </div>
                    </div>

                    <div class="form-group">
                        &nbsp;
                    </div>

                    <div id="grpVerzorgerGroep">
                        <div class="form-group">
                            <label class="col-lg-4 control-label">Voornaam verzorger</label>
                            <div class="col-lg-8"><input type="text" placeholder="Voornaam" class="required form-control" name="VOORNAAM_VERZORGER"></div>
                        </div>

                        <div class="form-group"><label class="col-lg-4 control-label">Achternaam verzorger</label>
                            <div class="col-lg-8"><input type="text" placeholder="Achternaam" class="required form-control" name="ACHTERNAAM_VERZORGER"></div>
                        </div>
                    </div>

                    <div class="form-group"><label class="col-lg-4 control-label">Email verantwoordelijke</label>
                        <div class="col-lg-8"><input type="email" placeholder="Email" class="required form-control" name="UITGENODIGDE_EMAIL">
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Annuleren</button>
                    <button type="button" class="btn btn-primary btnSave" id="btnUitnodigingSave">Nodig uit</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal inmodal" id="dlgDeelnemersUitnodigen" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content x_animated fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Sluiten</span></button>
                <h4 class="modal-title">Weet u zeker dat u de deelnemers wilt uitnodigen?</h4>
            </div>
            <form class="form-horizontal" id="frmDeelnemersUitnodigen">
                <input type="hidden" name="_hdnRecordID" value="" />
                <input type="hidden" name="_hdnState" value="insert" />
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-lg-3 control-label">Persoonlijke boodschap</label>
                        <div class="col-lg-9">
                            <textarea type="text" autofocus="autofocus" id="fldToelichting" placeholder="Toelichting" class="form-control" name="TOELICHTING"></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label">Cirkelmodel invullen vóór</label>
                        <div class="col-lg-9">
                            <div class="input-group date">
                                <input type="text" id="fldCMDeadline" name="CMDEADLINE" placeholder="Datum" label="Datum" format="dd-mm-yyyy" class="input-sm form-control text validation_date date_input required" >
                                <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-th"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Annuleren</button>
                    <button type="button" class="btn btn-primary btnSave" id="btnConfirmDeelnemersUitnodigen">Uitnodigen</button>
                </div>

            </form>
        </div>
    </div>
</div>