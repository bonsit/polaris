{include file="shared.tpl.php"}

<input type="hidden" name="_hdnModuleID" value="{$moduleid}" />
<input type="hidden" name="_hdnActiefVolgnummer" value="{$actiefvolgnummer}" />

<div class="ibox chat-view">
    <div class="ibox-content fh-breadcrumb">
        <div class="row full-height-scroll">

            <div class="col-md-9 no-padding">
                <div class="chat-discussion">

                    <div class="chat-entry">
                        <i class="chat-avatar fa fa-2x fa-user"></i>
                        <div class="chat">
                            <a class="chat-author" href="#"></a>
                            <span class="chat-time"></span>
                            <span class="chat-content"></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="chat-users">


                    <div class="users-list">
                        <div class="chat-user">
                            <img class="chat-avatar" src="" alt="">
                            <div class="chat-user-name">
                                <a href="#">Karl Jordan</a>
                            </div>
                        </div>
                        <div class="chat-user">
                            <img class="chat-avatar" src="" alt="">
                            <div class="chat-user-name">
                                <a href="#">Monica Smith</a>
                            </div>
                        </div>
                        <div class="chat-user">
                            <span class="pull-right label label-primary">Online</span>
                            <img class="chat-avatar" src="" alt="">
                            <div class="chat-user-name">
                                <a href="#">Michael Smith</a>
                            </div>
                        </div>
                        <div class="chat-user">
                            <span class="pull-right label label-primary">Online</span>
                            <img class="chat-avatar" src="" alt="">
                            <div class="chat-user-name">
                                <a href="#">Janet Smith</a>
                            </div>
                        </div>
                        <div class="chat-user">
                            <img class="chat-avatar" src="" alt="">
                            <div class="chat-user-name">
                                <a href="#">Alice Smith</a>
                            </div>
                        </div>
                        <div class="chat-user">
                            <img class="chat-avatar" src="" alt="">
                            <div class="chat-user-name">
                                <a href="#">Monica Cale</a>
                            </div>
                        </div>
                        <div class="chat-user">
                            <img class="chat-avatar" src="" alt="">
                            <div class="chat-user-name">
                                <a href="#">Mark Jordan</a>
                            </div>
                        </div>
                        <div class="chat-user">
                            <span class="pull-right label label-primary">Online</span>
                            <img class="chat-avatar" src="" alt="">
                            <div class="chat-user-name">
                                <a href="#">Janet Smith</a>
                            </div>
                        </div>

                    </div>

                </div>
            </div>


        </div>
        <div class="row chatfield">
            <div class="col-sm-12 no-padding">
                <textarea class="form-control chatarea" id="txtBericht" placeholder="Bericht voor netwerk van {$item.J_VOORNAAM} {$item.J_ACHTERNAAM}"></textarea>
            </div>
        </div>
    </div>
</div>
