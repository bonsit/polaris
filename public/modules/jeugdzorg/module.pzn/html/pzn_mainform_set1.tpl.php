<div class="columngroup">
    <div id="_rowJ_BSN" class="form-group form-group-sm">
        <label class="col-sm-4 control-label text-right" title="Burgerservicenummer">Burgerservicenummer</label>
        <div class="col-sm-8">
            <div class="input-group">
                <input type="text" label="Burgerservicenummer" placeholder="Burgerservicenummer" class="input-sm form-control text required validation_integer" data-v-max="999999999" id="_fldJ_BSN" name="J_BSN" size="15" maxlength="9" value="" />
            </div>
        </div>
    </div>
    <div id="_rowJ_VOORNAAM" class="form-group form-group-sm">
        <label class="col-sm-4 control-label text-right" title="Voornaam">Voornaam</label>
        <div class="col-sm-8">
            <div class="input-group">
                <input type="text" label="Voornaam"  placeholder="Voornaam" class="input-sm form-control text required validation_allchars" id="_fldJ_VOORNAAM" name="J_VOORNAAM" size="50" maxlength="50" value="" autocomplete="given-name" />
            </div>
        </div>
    </div>
    <div id="_rowJ_ACHTERNAAM" class="form-group form-group-sm">
        <label class="col-sm-4 control-label text-right" title="Achternaam">Achternaam</label>
        <div class="col-sm-8">
            <div class="input-group">
                <input type="text" label="Achternaam"  placeholder="Achternaam" class="input-sm form-control text required validation_allchars" id="_fldJ_ACHTERNAAM" name="J_ACHTERNAAM" size="50" maxlength="100" value="" autocomplete="family-name" />
            </div>
        </div>
    </div>
    <div id="_rowJ_GESLACHT" class="form-group form-group-sm">
        <label class="col-sm-4 control-label text-right" title="Geslacht">Geslacht</label>
        <div class="col-sm-8">
            <div class="input-group">
                <div class="radio required">
                    <label><input type="radio" id="_fldJ_GESLACHT:M" checked="checked" name="J_GESLACHT" value="M" class="" label="Geslacht" /> Man</label>
                    &nbsp;
                    <label><input type="radio" id="_fldJ_GESLACHT:V" name="J_GESLACHT" value="V" class="" label="Geslacht" /> Vrouw</label>
                </div>
            </div>
        </div>
    </div>
    <div id="_rowJ_GEBOORTEDATUM" class="form-group form-group-sm">
        <label class="col-sm-4 control-label text-right" title="Geboortedatum">Geboortedatum</label>
        <div class="col-sm-5">
            <div class="input-group date">
                <input type="text" id="_fldJ_GEBOORTEDATUM" placeholder="Geboortedatum" name="J_GEBOORTEDATUM" label="Geboortedatum" format="dd-mm-yyyy" class="input-sm form-control text validation_date date_input required" size="20"  autocomplete="bday" />
                <div class="input-group-addon">
                    <span class="glyphicon glyphicon-th"></span>
                </div>
            </div>
        </div>
    </div>
</div>
