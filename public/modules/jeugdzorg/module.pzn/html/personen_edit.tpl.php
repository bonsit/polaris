<input type="hidden" name="_hdnRecordID" value="{$items[0].PLR__RECORDID}" />
<input type="hidden" name="_hdnAction" value="plr_save" />
<input type="hidden" name="_hdnFORMTYPE" value="bewaar_persoon" />
<input type="hidden" name="_hdnState" value="{if $isinserting}insert{else}edit{/if}" />
<input type="hidden" name="_hdnProcessedByModule" value="true" />
<input type="hidden" name="_hdnClientID" value="{$items[0].CLIENTID}" />
<input type="hidden" name="USERGROUPID" value="{$items[0].USERGROUPID}" />

<div id="form23_page1" class="tab-pane active">
   <div id="formview" class="formview alignlabels groups_2">
      <div class="xs-col-12 col-md-5">
         <div id="_rowVOORNAAM" class="form-group form-group-sm">
            <label class="col-xs-12 col-md-5 control-label" title="VOORNAAM">Voornaam </label>
            <div class="col-xs-12 col-md-5">
               <div class="input-group"><input type="text" label="Voornaam" class="text required validation_allchars valid" id="_fldVOORNAAM" name="VOORNAAM" size="50" maxlength="50" value="{$items[0].VOORNAAM}"></div>
            </div>
         </div>
         <div id="_rowACHTERNAAM" class="form-group form-group-sm">
            <label class="col-xs-12 col-md-5 control-label" title="ACHTERNAAM">Achternaam </label>
            <div class="col-xs-12 col-md-5">
               <div class="input-group"><input type="text" label="Achternaam" class="text required  validation_allchars" id="_fldACHTERNAAM" name="ACHTERNAAM" size="50" maxlength="100" value="{$items[0].ACHTERNAAM}"></div>
            </div>
         </div>
         <div id="_rowSTRAATNAAM" class="form-group form-group-sm">
            <label class="col-xs-12 col-md-5 control-label" title="STRAATNAAM">Straatnaam </label>
            <div class="col-xs-12 col-md-5">
               <div class="input-group"><input type="text" label="Straatnaam" class="text   validation_allchars" id="_fldSTRAATNAAM" name="STRAATNAAM" size="50" maxlength="100" value="{$items[0].STRAATNAAM}"></div>
            </div>
         </div>
         <div id="_rowHUISNUMMER" class="form-group form-group-sm">
            <label class="col-xs-12 col-md-5 control-label" title="HUISNUMMER">Huisnummer </label>
            <div class="col-xs-12 col-md-5">
               <div class="input-group"><input type="text" label="Huisnummer" class="text   validation_allchars" id="_fldHUISNUMMER" name="HUISNUMMER" size="15" maxlength="15" value="{$items[0].HUISNUMMER}"></div>
            </div>
         </div>
         <div id="_rowTOEVOEGING" class="form-group form-group-sm">
            <label class="col-xs-12 col-md-5 control-label" title="TOEVOEGING">Toevoeging </label>
            <div class="col-xs-12 col-md-5">
               <div class="input-group"><input type="text" label="Toevoeging" class="text   validation_allchars" id="_fldTOEVOEGING" name="TOEVOEGING" size="10" maxlength="10" value="{$items[0].TOEVOEGING}"></div>
            </div>
         </div>
         <div id="_rowPOSTCODE" class="form-group form-group-sm">
            <label class="col-xs-12 col-md-5 control-label" title="POSTCODE">Postcode </label>
            <div class="col-xs-12 col-md-5">
               <div class="input-group"><input type="text" label="Postcode" class="text   validation_allchars" id="_fldPOSTCODE" name="POSTCODE" size="10" maxlength="10" value="{$items[0].POSTCODE}"></div>
            </div>
         </div>
         <div id="_rowPLAATSNAAM" class="form-group form-group-sm">
            <label class="col-xs-12 col-md-5 control-label" title="PLAATSNAAM">Plaatsnaam </label>
            <div class="col-xs-12 col-md-5">
               <div class="input-group"><input type="text" label="Plaatsnaam" class="text   validation_allchars" id="_fldPLAATSNAAM" name="PLAATSNAAM" size="50" maxlength="50" value="{$items[0].PLAATSNAAM}"></div>
            </div>
         </div>
         <div id="_rowROL" class="form-group form-group-sm">
            <label class="col-xs-12 col-md-5 control-label" title="ROL">Rol </label>
            <div class="col-xs-12 col-md-5">
               <div class="input-group">
                  {html_options name="ROL" values=$rollen output=$rollen selected=$items[0].ROL class="form-control"}
               </div>
            </div>
         </div>
         <div id="_rowSOORTZORG" class="form-group form-group-sm">
            <label class="col-xs-12 col-md-5 control-label" title="SOORTZORG">Soortzorg </label>
            <div class="col-xs-12 col-md-5">
               <div class="input-group">
                    {html_options name="SOORT_ZORG" options=$soortzorg selected=$items[0].SOORT_ZORG class="form-control"}
               </div>
            </div>
         </div>
      </div>
      <div class="xs-col-12 col-md-5">
         <div id="_rowE_MAILADRES" class="form-group form-group-sm">
            <label class="col-xs-12 col-md-5 control-label" title="E_MAILADRES">E_mailadres </label>
            <div class="col-xs-12 col-md-5">
               <div class="input-group"><input type="text" label="E_mailadres" class="text required  validation_allchars" id="_fldE_MAILADRES" name="E_MAILADRES" size="50" maxlength="50" value="{$items[0].E_MAILADRES}"></div>
            </div>
         </div>
         <div id="_rowPASSWORD" class="form-group form-group-sm">
            <label class="col-xs-12 col-md-5 control-label" title="PASSWORD">Wachtwoord </label>
            <div class="col-xs-12 col-md-5">
               <div class="input-group"><input type="password" label="Wachtwoord" class="text validation_integer required" id="_fldPASSWORD" name="PASSWORD" size="20" maxlength="20" value="{if $items[0].USERGROUPID neq ''}__nochange__{/if}"></div>
            </div>
         </div>
         <div id="_rowTELEFOONNUMMER" class="form-group form-group-sm">
            <label class="col-xs-12 col-md-5 control-label" title="TELEFOONNUMMER">Telefoonnummer </label>
            <div class="col-xs-12 col-md-5">
               <div class="input-group"><input type="text" label="Telefoonnummer" class="text   validation_allchars" id="_fldTELEFOONNUMMER" name="TELEFOONNUMMER" size="20" maxlength="20" value="{$items[0].TELEFOONNUMMER}"></div>
            </div>
         </div>
         <div id="_rowGEBOORTEDATUM" class="form-group form-group-sm">
            <label class="col-xs-12 col-md-5 control-label" title="GEBOORTEDATUM">Geboortedatum </label>
            <div class="col-xs-12 col-md-5">
               <div class="input-group date" style="width:10em">
                     <input type="text" id="_fldGEBOORTEDATUM" name="GEBOORTEDATUM" class="text date_input" value="{$items[0].GEBOORTEDATUM}" size="11" date-format="dd-mm-yyyy">
                     <div class="input-group-addon">
                        <span class="glyphicon glyphicon-th"></span>
                     </div>
               </div>
            </div>
         </div>
         <div id="_rowBSN" class="form-group form-group-sm">
            <label class="col-xs-12 col-md-5 control-label" title="BSN">Bsn </label>
            <div class="col-xs-12 col-md-5">
               <div class="input-group"><input type="text" label="Bsn" class="text   validation_integer" id="_fldBSN" name="BSN" size="20" maxlength="20" value="{$items[0].BSN}"></div>
            </div>
         </div>
         <div id="_rowGESLACHT" class="form-group form-group-sm">
            <label class="col-xs-12 col-md-5 control-label" title="GESLACHT">Geslacht </label>
            <div class="col-xs-12 col-md-5">
               <div class="input-group">
                  <select size="1" name="GESLACHT" id="_fldGESLACHT" class="form-control ">
                     <option value=""></option>
                     <option value="M" {if $items[0].GESLACHT == 'M'}selected{/if}>Man</option>
                     <option value="V" {if $items[0].GESLACHT == 'V'}selected{/if}>Vrouw</option>
                     <option value="O" {if $items[0].GESLACHT == 'O'}selected{/if}>Onbekend</option>
                  </select>
               </div>
            </div>
         </div>
      </div>
      <div class="columngroupreset"></div>
   </div>
</div>