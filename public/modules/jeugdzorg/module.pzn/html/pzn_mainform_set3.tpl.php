<div class="columngroup">
    <div id="_rowV_E_MAILADRES" class="form-group form-group-sm">
        <label class="col-sm-4 control-label text-right" title="V_E_MAILADRES">Emailadres</label>
        <div class="col-sm-8">
            <div class="input-group">
                <input type="text" label="Emailadres" class="input-sm form-control" disabled="disabled" id="_fldV_E_MAILADRES" name="V_E_MAILADRES" size="50" maxlength="50" value="">
            </div>
        </div>
    </div>
     <div id="_rowRELATIE_TOV_ZORGGERECHTIGDE" class="form-group form-group-sm">
        <label class="col-sm-4 control-label text-right" title="RELATIE_TOV_ZORGGERECHTIGDE">Soort relatie</label>
        <div class="col-sm-8">
            <div class="input-group">
                <select size="1" name="RELATIE_TOV_ZORGGERECHTIGDE" id="_fldRELATIE_TOV_ZORGGERECHTIGDE" class="form-control ">
                    <option value="Ouder">Ouder</option>
                    <option value="Jeugdige">Jeugdige zelf</option>
                    <option value="Pleegouder">Pleegouder</option>
                    <option value="Voogd">Voogd</option>
                    <option value="Gezinsvoogd">Gezinsvoogd</option>
                    <option value="Verzorger">Verzorger</option>
                </select>
            </div>
        </div>
    </div>
    <div class="zorgvrager_extrablock">
        <div id="_rowV_GESLACHT" class="form-group form-group-sm">
            <label class="col-sm-4 control-label text-right" title="V_GESLACHT">Geslacht</label>
            <div class="col-sm-8">
                <div class="input-group">
                    <div class="radio">
                        <label><input type="radio" id="_fldV_GESLACHT:M" name="V_GESLACHT" value="M" class="" label="Geslacht">Man</label> &nbsp; &nbsp;<label><input type="radio" id="_fldV_GESLACHT:V" name="V_GESLACHT" value="V" class="" label="Geslacht">Vrouw</label> &nbsp; &nbsp;
                    </div>
                </div>
            </div>
        </div>
        <div id="_rowV_VOORNAAM" class="form-group form-group-sm">
            <label class="col-sm-4 control-label text-right" title="V_VOORNAAM">Voornaam</label>
            <div class="col-sm-8">
                <div class="input-group">
                    <input type="text" label="Voornaam" class="input-sm form-control text appear_required validation_allchars" id="_fldV_VOORNAAM" name="V_VOORNAAM" size="20" maxlength="20" value="">
                </div>
            </div>
        </div>
        <div id="_rowV_ACHTERNAAM" class="form-group form-group-sm">
            <label class="col-sm-4 control-label text-right" title="V_ACHTERNAAM">Achternaam</label>
            <div class="col-sm-8">
                <div class="input-group">
                    <input type="text" label="Achternaam" class="input-sm form-control appear_required text validation_allchars" id="_fldV_ACHTERNAAM" name="V_ACHTERNAAM" size="50" maxlength="100" value="">
                </div>
            </div>
        </div>

        {if $wizard}
        <div id="_rowV_ACHTERNAAM" class="form-group form-group-sm">
            <div class="col-sm-4"></div>
            <div class="col-sm-8">
                <button type="button" class="btn btn-sm btn-primary" id="btnKopieerAdres">Kopieer adres van jeugdige</button>
            </div>
        </div>
        {/if}

        <div id="_rowV_STRAATNAAM" class="form-group form-group-sm">
            <label class="col-sm-4 control-label text-right" title="V_STRAATNAAM">Adres</label>
            <div class="col-sm-3">
                <div class="input-group">
                    <input type="text" placeholder="Straatnaam" label="Straatnaam" class="input-sm form-control text  validation_allchars" id="_fldV_STRAATNAAM" name="V_STRAATNAAM" size="50" maxlength="100" value="">
                </div>
            </div>
            <div class="col-sm-2">
                <div class="input-group">
                    <input type="text" placeholder="Nr" label="Huisnummer" class="input-sm form-control text  validation_allchars" id="_fldV_HUISNUMMER" name="V_HUISNUMMER" size="15" maxlength="15" value="">
                </div>
            </div>
            <div class="col-sm-2">
                <div class="input-group">
                    <input type="text" placeholder="Toevoeging" label="Toevoeging" class="input-sm form-control text validation_allchars" id="_fldV_TOEVOEGING" name="V_TOEVOEGING" size="10" maxlength="10" value="">
                </div>
            </div>
        </div>

        <div id="_rowV_POSTCODE" class="form-group form-group-sm">
            <label class="col-sm-4 control-label text-right" title="V_POSTCODE">Postcode</label>
            <div class="col-sm-8">
                <div class="input-group">
                    <input type="text" label="Postcode" placeholder="Postcode" class="input-sm form-control text validation_allchars" id="_fldV_POSTCODE" name="V_POSTCODE" size="10" maxlength="10" value="">
                </div>
            </div>
        </div>
        <div id="_rowV_PLAATSNAAM" class="form-group form-group-sm">
            <label class="col-sm-4 control-label text-right" title="V_PLAATSNAAM">Plaatsnaam</label>
            <div class="col-sm-8">
                <div class="input-group">
                    <input type="text" label="Plaatsnaam" placeholder="Plaatsnaam" class="input-sm form-control text validation_allchars" id="_fldV_PLAATSNAAM" name="V_PLAATSNAAM" size="50" maxlength="50" value="">
                </div>
            </div>
        </div>
    </div ><!-- einde extra block -->
    <div id="_rowV_TELEFOONNUMMER" class="form-group form-group-sm">
        <label class="col-sm-4 control-label text-right" title="V_TELEFOONNUMMER">Telefoonnummer</label>
        <div class="col-sm-8">
            <div class="input-group">
                <input type="text" label="Telefoonnummer" placeholder="Telefoonnummer" class="input-sm form-control text validation_allchars" id="_fldV_TELEFOONNUMMER" name="V_TELEFOONNUMMER" size="20" maxlength="20" value="">
            </div>
        </div>
    </div>
</div>