{include file="shared.tpl.php"}

<input type="hidden" name="_hdnJeugdigeRecordID" value="{$item.PLR__JEUGDIGERECORDID}" />
<input type="hidden" name="_hdnVerzorgerRecordID" value="{$item.PLR__VERZORGERRECORDID}" />
<input type="hidden" name="_hdnCoordinatorRecordID" value="{$item.PLR__COORDINATORRECORDID}" />

{include file="topfields.tpl.php"}

<div class="pagetabcontainer">
    <ul class="nav nav-tabs">
        <li class="active">
            <a id="tab_form1_page1" data-toggle="tab" href="#form1_page1">Jeugdige</a>
        </li>
        <li>
            <a id="tab_form1_page2" data-toggle="tab" href="#form1_page2">Verzorger</a>
        </li>
        <li>
            <a id="tab_frm_ouder" data-toggle="tab" href="#frm_ouder">Ouder(s)</a>
        </li>
        <li>
            <a id="tab_frm_coordinator" data-toggle="tab" href="#frm_coordinator">Coördinator {if $item.WISSEL_COORDINATOR == $actiefvolgnummer and $item.WISSEL_STATUS == 'VERSTUURD'}<i class="fa fa-warning" style="color:orange"></i>{/if}</a>
        </li>
        <li>
            <a id="tab_frm_netwerkd" data-toggle="tab" href="#frm_netwerkd">Netwerkdeelnemers</a>
        </li>
        <li>
            <a id="tab_frm_rondetafel" data-toggle="tab" href="#frm_rondetafel">Ronde tafel</a>
        </li>
        <li>
            <a id="tab_frm_oogmonitor" data-toggle="tab" href="#frm_oogmonitor">Het oog</a>
        </li>
    </ul>
</div>

<div class="panel-bodyx">
    <div class="tab-content">

        <div id="form1_page1" class="tab-pane active">
            <div id="formview" class="formview alignlabels groups_1 dirtyform">
                {include file="pzn_mainform_set1.tpl.php"}

                {include file="pzn_mainform_set2.tpl.php"}
                <div class="columngroupreset"></div>
                {*if $iscoordinator}<button class="btn btn-default" id="btnWijzigPZNGegevens">Wijzig gegevens</button>{/if*}
            </div>
        </div>
        <div id="form1_page2" class="tab-pane">
            <div id="formview2" class="formview alignlabels groups_1">

                {include file="pzn_mainform_set3.tpl.php"}
                <div class="columngroupreset"></div>
            </div>
        </div>
        <div id="frm_ouder" class="tab-pane">
            <div id="formview3" class="formview alignlabels groups_1">
                {include file="pzn_mainform_set4.tpl.php"}
                <div class="columngroupreset"></div>
            </div>
        </div>
        <div id="frm_netwerkd" class="tab-pane">
            <div id="formview4" class="formview alignlabels groups_1">
                {include file="pzn_mainform_set5.tpl.php"}
            </div>
        </div>

        <div id="frm_rondetafel" class="tab-pane">
            <div id="formview9" class="formview alignlabels groups_1">

                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 chat-panel">
                        <div class="chat-discussion">
                            <div class="chat-entry">
                                <i class="chat-avatar fa fa-2x fa-user"></i>
                                <div class="chat">
                                    <a class="chat-author" href="#"></a>
                                    <span class="chat-time"></span>
                                    <span class="chat-content"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="chatfield rto-entryfield">
                    <div class="{if $iscoordinator}col-sm-9{else}col-sm-12{/if} no-padding">
                        <div class="row">
                            <div class="col-xs-11 no-padding">
                                <textarea class="form-control chatarea always-on" id="txtBerichtRTO" placeholder="Bericht voor netwerk van {$item.J_VOORNAAM} {$item.J_ACHTERNAAM}"></textarea>
                            </div>
                            <div class="col-xs-1 no-padding">
                                <button type="button" id="btnSendMessageRTO" class="btn btn-primary btn-outline xdim"><i class="fa fa-send fa-2x"></i></button>
                            </div>
                        </div>
                    </div>
                    {if $iscoordinator}
                    <div class="col-sm-3 no-padding">

                        {if $moduleid==20 and $iscoordinator and ($item.STATUS === 'RTOGESTART' or $item.STATUS === 'HETOOG')}
                        <div class="switch">
                            <button type="button" id="btnRTOPauze" class="btn btn-success btn-sm">RTO Pauzeren</button>
                            <button type="button" id="btnRTOOpenen" class="btn btn-success btn-sm">RTO Openen</button>
                            <button type="button" id="btnRTOEinde" class="btn btn-danger btn-sm">RTO Eindigen</button>

<!--                             RTO Gepauzeerd?
                            <div class="onoffswitch">
                                <input type="checkbox" checked="checked" class="onoffswitch-checkbox" id="btnRTOPauze">
                                <label class="onoffswitch-label" for="btnRTOPauze">
                                    <span class="onoffswitch-inner"></span>
                                    <span class="onoffswitch-switch"></span>
                                </label>
                            </div>
 -->
                        </div>
                        {/if}

                    </div>
                    {/if}
                </div>
            </div>
        </div>

        <div id="frm_oogmonitor" class="tab-pane">
            <div id="formview10" class="formview alignlabels groups_1">

                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 chat-panel">
                        <div class="chat-oogmonitor">
                            <div class="chat-entry">
                                <i class="chat-avatar fa fa-2x fa-user"></i>
                                <div class="chat">
                                    <a class="chat-author" href="#"></a>
                                    <span class="chat-time"></span>
                                    <span class="chat-content"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="chatfield oogmonitor-entryfield">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-xs-8 no-padding">
                                <textarea class="form-control chatarea always-on" id="txtBerichtOog" placeholder="Bericht voor netwerk van {$item.J_VOORNAAM} {$item.J_ACHTERNAAM}"></textarea>
                            </div>
                            <div class="col-xs-3 no-padding">

                                <button style="width:100px" class="btn btn-sm btn-white btn-outline" type="button">Uitvoering:</button>
                                <div id="oog-uitvoering" class="btn-group btn-group-toggle" data-toggle="buttons">
                                    <div class="btn btn-sm" role="button" data-twbs-toggle-buttons-class-active="uitvoering-rood">
                                      <input type="radio" name="ooguitvoering" value="1" >Geen
                                    </div>
                                    <div class="btn btn-sm" role="button" data-twbs-toggle-buttons-class-active="uitvoering-oranje">
                                      <input type="radio" name="ooguitvoering" value="2">Matig
                                    </div>
                                    <div class="btn btn-sm" role="button" data-twbs-toggle-buttons-class-active="uitvoering-groen">
                                      <input type="radio" name="ooguitvoering" value="3">Goed
                                    </div>
                                </div>
                                <br/>
                                <button style="width:100px" class="btn btn-sm btn-white btn-outline" type="button">Resultaat:</button>
                                <div id="oog-resultaat" class="btn-group btn-group-toggle" data-toggle="buttons">
                                    <div class="btn btn-sm" role="button" data-twbs-toggle-buttons-class-active="uitvoering-rood">
                                      <input type="radio" name="oogresultaat" value="1" >Geen
                                    </div>
                                    <div class="btn btn-sm" role="button" data-twbs-toggle-buttons-class-active="uitvoering-oranje">
                                      <input type="radio" name="oogresultaat" value="2">Matig
                                    </div>
                                    <div class="btn btn-sm" role="button" data-twbs-toggle-buttons-class-active="uitvoering-groen">
                                      <input type="radio" name="oogresultaat" value="3">Goed
                                    </div>
                                </div>

                            </div>
                            <div class="col-xs-1 no-padding">
                                <button type="button" id="btnSendMessageOog" class="btn btn-primary btn-outline xdim"><i class="fa fa-eye fa-2x"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="frm_coordinator" class="tab-pane">
            <div id="formview8" class="formview alignlabels groups_1">
<!--                 <div class="columngroup">
                    <div id="_rowC_E_MAILADRES" class="form-group form-group-sm">
                        <label class="col-sm-4 control-label text-right" title="C_E_MAILADRES">Emailadres</label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <input type="text" label="Emailadres" class="input-sm form-control" disabled="disabled" id="_fldC_E_MAILADRES" name="C_E_MAILADRES" size="50" maxlength="50" value="">
                            </div>
                        </div>
                    </div>
 -->                    <div class="coordinator_extrablock">
<!--                         <div id="_rowC_GESLACHT" class="form-group form-group-sm">
                            <label class="col-sm-4 control-label text-right" title="C_GESLACHT">Geslacht</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <div class="radio">
                                        <label><input type="radio" id="_fldC_GESLACHT:M" name="C_GESLACHT" value="M" class="" label="Geslacht">Man</label> &nbsp; &nbsp;<label><input type="radio" id="_fldC_GESLACHT:V" name="C_GESLACHT" value="V" class="" label="Geslacht">Vrouw</label> &nbsp; &nbsp;
                                    </div>
                                </div>
                            </div>
                        </div>
 -->
                        <div id="_rowC_VOORNAAM" class="form-group form-group-sm">
                            <label class="col-sm-4 control-label text-right" title="C_VOORNAAM">Naam coordinator</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <input type="text" label="Voornaam" class="input-sm always-on form-control text appear_required validation_allchars" id="_fldCOMPLETENAAM" disabled="disabled"  name="C_COMPLETENAAM" size="20" maxlength="20" value="">
                                    {if ($iscoordinator)}
                                    <button type="button" id="btnWijzigCoordinator" class="btn btn-success btn-sm">Wijzig coördinator...</button>
                                    {/if}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12 col-md-4 col-md-offset-4 coordinatorwijzigingen" style="display:none">
                            <h3>Vorige en lopende wijzigingen</h3>
                            <table id="coordinatorwijzigingen" class="table table-sm" tabindex="10">
                              <thead>
                                <tr>
                                  <th>Datum</th>
                                  <th>Coordinator</th>
                                  <th>Status</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr class="row">
                                  <td class="datum_wissel"></td>
                                  <td class="coordinator"></td>
                                  <td class="status"></td>
                                </tr>
                              </tbody>
                            </table>
                        </div>
                        </div>

                </div>
                <div class="columngroupreset"></div>

            </div>
        </div>
    </div>
</div>
