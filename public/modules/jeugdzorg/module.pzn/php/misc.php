<?php

function SimpleCrypt( $string, $action = 'e' ) {
    $secret_key = 'bdGqzzi9DnGhcawdwmtbWANUdiPAccJfbzbxBtGxwP';
    $secret_iv = 'FRfcNTeyHyfnDRjWUswVkoLpwrZwWgcs3MJrTxTyjN';

    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );

    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }

    return $output;
}

function GetPersoonVolgnummer($database, $usergroupid) {
    $_sql = "SELECT VOLGNUMMER FROM PZN_PERSOON WHERE USERGROUPID = ?";
    return $database->userdb->GetOne($_sql, array($usergroupid));
}

function BewaarPersoonPermissies($clientid, $usergroupid, $rol) {
    global $polaris;

    $_result = false;
    $_groupid = false;

    switch ($rol) {
    case 'Zorgverlener':
        $_groupid = 4;
        break;
    case 'Ouder':
    case 'Pleegouder':
        $_groupid = 6;
        break;
    case 'Netwerk':
        $_groupid = 6;
        break;
    case 'Verzorger':
        $_groupid = 5;
        break;
    case 'Jeugdige':
        $_groupid = 5;
        break;
    case 'Coordinator':
        $_groupid = 9;
        break;
    case 'Superuser':
        $_groupid = 100;
        break;
    }

    if ($_groupid) {
        $_sql = "INSERT INTO plr_membership (clientid, usergroupid, groupid) VALUES (?,?,?)";
        $_result = $polaris->instance->Execute($_sql, array($clientid, $usergroupid, $_groupid));
    }
    return $_result;
}

function GetSoortZorg($database, $volgnummer) {
    $_sql = 'SELECT SOORT_ZORG FROM PZN_PERSOON WHERE VOLGNUMMER = ?';
    $_soortZorg = $database->GetOne($_sql, array($volgnummer));
    return $_soortZorg;
}

