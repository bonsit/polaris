JeugdZorg.Uitnodigen = {
    initialize: function() {
        var Self = JeugdZorg.Uitnodigen;

        if (jQuery().peity) {
            $("span.pie").peity("pie", {
                fill: ['#1ab394', '#d7d7d7', '#ffffff']
            });
        }

        $('#dlgUitnodigen').on('shown.bs.modal', function() {
            $(this).find('input:visible:first').focus();
        });

        Polaris.Form.initializeFormValidation($("#frmUitnodigen"));

        $("#btnUitnodigingSave").click(function() {
            Self.saveUitnodiging();
        });

        $(".btnVerstuurUitnodigingOpnieuw").click(function(e) {
            e.preventDefault();
            var that = this;
            Polaris.Base.modalDialog('Wilt u de uitnodiging opnieuw versturen?', {
                yes: function(dialogItself) {
                    Self.stuurUitnodigingOpnieuw(that);
                    dialogItself.close();
                },
                no: function(dialogItself) {
                    dialogItself.close();
                }
            });

        });

        $("#fldSOORTUITNODIGINGV,#fldSOORTUITNODIGINGJ").change(function() {
            $("#grpVerzorgerGroep").toggle( ($(this).val() == 'Verzorger') );
        });

    },
    gegevensInOrde: function() {
        return true;
    },
    saveUitnodiging: function() {
        var Self = JeugdZorg.Uitnodigen;

        if (Self.gegevensInOrde()) {
            var senddata = [];

            senddata.push({name: '_hdnProcessedByModule', value: 'true'});
            senddata.push({name: '_hdnFORMTYPE', value: 'pzn_uitnodiging'});
            $($("#frmUitnodigen").serializeArray()).each(function(i, obj) {
                senddata.push({name: obj.name, value: obj.value});
            });

            Polaris.Ajax.postJSON(_servicequery, senddata, null, 'Gegevens opgeslagen.', function (data) {
                log(data);
                if (data.result == true) {
                    $("#dlgUitnodigen").modal('hide');
                    location.reload();
                } else {
                    alert('result = false');
                }
            });
        } else {
            $("#dlgNGZV").modal('hide');
        }
    },
    stuurUitnodigingOpnieuw: function(elem) {
        var Self = JeugdZorg.Uitnodigen;

        var senddata = [];
        senddata.push({name: '_hdnProcessedByModule', value: 'true'});
        senddata.push({name: '_hdnFORMTYPE', value: 'pzn_stuuruitnodigingopnieuw'});
        senddata.push({name: 'ID', value: $(elem).data('id')});
        Polaris.Ajax.postJSON(_servicequery, senddata, null, 'Opnieuw verstuurd.', function (data) {
            if (data.result == true) {
                //location.reload();
            }
        });
    }
}

$(document).ready(function() {
    var Self = JeugdZorg.Uitnodigen;

    Self.initialize();
})