var JeugdZorg = {

    _verwijderItem: function(row, pznid, volgnummer, formtype) {
        var Self = JeugdZorg;

        var senddata = [];

        senddata.push({name: '_hdnFORMTYPE', value: formtype});
        senddata.push({name: '_hdnProcessedByModule', value: 'true'});
        senddata.push({name: '_hdnState', value: 'delete'});
        senddata.push({name: 'pzn', value: pznid});
        senddata.push({name: 'volgnummer', value: volgnummer});
        var naam = $(this).parent().find('td.naam').text();
        Polaris.Ajax.postJSON(_servicequery, senddata, null, 'Item '+ naam +' verwijderd.', function (data) {
            $(row).hide('slow');
        });
        return $(row).parents('tbody').find('tr').index($(row).parents('tr'));
    },

    setFields: function(fields, data, form) {
        if (data) {
            $(fields).each(function(i, elem) {
                if (elem.indexOf('@radio') > 0) {
                    var radioElem = elem.replace('@radio','');
                    var radio = form.find("input[name="+radioElem+"]").filter("[value="+data[radioElem]+"]");
                    radio.prop('checked', true);
                    if (radio.parents('label').length > 0)
                        radio.parents('label').button('toggle').tab('show');
                } else if (elem.indexOf('@select') > 0) {
                    var selectElem = elem.replace('@select','');
                    form.find("select[name="+selectElem+"]").val(data[selectElem]);
                } else {
                    // we need the blur here, because Safari f*cks up with placeholder text still being show when field has a value
                    form.find("input[name="+elem+"]").val(data[elem]||"").change().blur();
                }
            });
            form.find(".date_input").datepicker('update');
            $("input:visible:not([disabled]):first",form).focus();
        }
    },

};

