JeugdZorg.CirkelOverzicht = {
    dataProfielItems: [],
    dataZorgBesteedItems: [],
    dataZorgplan: [],
    dataProblemen: [],
    dataZorg: [],
    BesteeddeZorgVelden: [
        'DOMEIN', 'DOMEIN_CODE', 'FREQUENTIEAANTAL', 'INTENSITEITAANTAL'
        , 'NIVEAU', 'NIVEAU_CODE', 'PLR__RECORDID', 'PROBLEEM@select', 'SINDS', 'TOT', 'VOLGNUMMER', 'ZORG@select'
    ],

    initialize: function() {
        var Self = JeugdZorg.CirkelOverzicht;

        if ($('#holder').length > 0) {
            JeugdZorg.cirkelModel.init('#holder', Self.cirkelKeuze);
        }

        $("#btnToonCirkelHelp").click(function() {
            //JeugdZorg.Tour.run();
        });

        $("#btnCirkelModelKlaar").click(Self.cirkelModelKlaar);

        $("#btnBewaarAandachtsGebied").click(Self.bewaarAandachtsGebied);

        $("#btnToonVerleendeZorgForm").click(Self.toonVerleendeZorgForm);
        $("#btnBewaarVerleendeZorg").click(Self.bewaarVerleendeZorg);

        $("#btnNieuweVersie").click(Self.toonNieuweVersie);
        $("#btnStartNieuweVersie").click(Self.startNieuweVersie);

        $("#btngrpZorgverlener button").click(function() {
            var url = Polaris.Base.removeLocationVariable('zorgverlener');
            url = Polaris.Base.addLocationVariable('zorgverlener', $(this).data('val'));
            window.location = url;
        });

        if ($("#slcProbleem").length > 0) {
            $("#slcProbleem").chosen({width: "25vw"});
            $('#slcProbleem').on('change', Self.probleemOfZorgKeuze);
        }
        $("#slcZorg").on('change', Self.probleemOfZorgKeuze);

        $(".ernst button").click(function() {
            $(".ernst button").not(this).removeClass('active');
            $(this).addClass('active');
        });

        $('.ernst').on('click','input[type=radio]',function() {
            $(this).closest('.ernst').find('.radio-inline, .radio').removeClass('checked');
            $(this).closest('.radio-inline, .radio').addClass('checked');
        });

        $('#dlgVoegAandachtsGebiedToe').on('hidden.bs.modal', function () {
            JeugdZorg.cirkelModel.clearAll();
        });
        if ($("input[name=_hdnGekozenZorgverlener]").val() != 99999) {
            $("#th_naam").hide();
            $(".naam").hide();
        }

        $("#btnVerwijderVersie").click(Self.verwijderVersie);

        $("#toggleZorgBesteed").click(function(e) {
            e.preventDefault();
            Self.toggleZorgBesteed();
        });

        $("#probleemspecificatie").dblclick(function(e) {
            e.preventDefault();
            var tr = $(e.target).parents('tr');
            var btn = $(e.target);
            if (!btn.is('button')) {
                btn = btn.parents('button');
            }
            var editable = ($("#cirkel_editable").length > 0);
            if (tr.hasClass('subitem') && editable) {
                Self.wijzigVerleendeZorg(tr);
            } else if (tr.hasClass('item') && editable) {
                Self.wijzigAandachtsGebied(tr);
            }
        });

        $("#probleemspecificatie").click(function(e) {
            e.preventDefault();

            var tr = $(e.target).parents('tr');
            var btn = $(e.target);
            if (!btn.is('button')) {
                btn = btn.parents('button');
            }

            var item = tr.data('item');
            Self.selecteerRegel(tr);

            var editable = ($("#cirkel_editable").length > 0);
            if (tr.hasClass('subitem') && editable) {
                if (btn.hasClass('verwijderItem')) {
                    if (confirm('Weet u zeker dat u de zorg "'+item.ZORG+'" wilt verwijderen?')) {
                        Self.verwijderZorgBesteedItem(tr, item.VOLGNUMMER);
                    };
                }
                if (btn.hasClass('wijzigItem')) {
                    Self.wijzigVerleendeZorg(tr);
                }
            } else if (tr.hasClass('item') && editable) {
                if (btn.hasClass('verwijderItem')) {
                    if (confirm('Weet u zeker dat u het aandachtspunt "'+item.PROBLEEM+'" wilt verwijderen?')) {
                        Self.verwijderAandachtsGebied(tr, item.VOLGNUMMER);
                    };
                }
                if ($(btn).hasClass('wijzigItem')) {
                    Self.wijzigAandachtsGebied(tr);
                }
                if (btn.hasClass('voegtoeZorgBesteedItem')) {
                    Self.toonVerleendeZorgForm();
                }
            }
        });
        /*****************/
        /* Profiel items */
        /*****************/
        // Self.htmlProfielItems = $('#probleemspecificatie tbody');
        // var ProfielItemsDirectives = {
        //     'tr': {
        //         'regel<-': {
        //             '@id+': 'regel.VOLGNUMMER',
        //             'td.id': 'regel.VOLGNUMMER',
        //             'td.code': 'regel.CODE',
        //             'td.naam': function(arg) {
        //                 if (!isUndefined(arg.items[arg.pos-1]) && arg.items[arg.pos-1].NAAM == arg.item.NAAM) {
        //                     return '';
        //                 } else {
        //                     return arg.item.NAAM;
        //                 }
        //             },
        //             'td.probleem': function(arg) {
        //                 return wrap(arg.item.PROBLEEM, 60);
        //             },
        //             'td.domein': 'regel.DOMEIN_CODE',
        //             'td.niveau': 'regel.NIVEAU_CODE',
        //             'td.ernst': function(arg) {
        //                 return '<label class="ernst ernst-'+arg.item.ERNST+'">'+arg.item.ERNST+'</label>';
        //             },
        //             'td.aantalzorg': function(arg) {
        //                 var classname = '';
        //                 switch (arg.item.AANTALZORGBESTEED) {
        //                     case '0':
        //                         classname = 'danger';
        //                     break;
        //                     default:
        //                         classname = 'plain';
        //                 }
        //                 return '<span class="badge badge-'+classname+'">'+arg.item.AANTALZORGBESTEED+'</span>'
        //             },
        //             'td.gebied': function(arg) {
        //                 return arg.item.NIVEAU + ' ' + arg.item.DOMEIN;
        //             },
        //             '@onclick': function(arg) {
        //                 return 'JeugdZorg.CirkelOverzicht.selecteerRegel(this);JeugdZorg.CirkelOverzicht.getZorgBesteed(' + arg.pos + ');';
        //             },
        //             // '@ondblclick': function(arg) {
        //             //     return 'JeugdZorg.CirkelOverzicht.wijzigAandachtsGebied(' + arg.pos + ');';
        //             // },
        //             '@class': function(arg) {
        //                 return arg.item.ERNST;
        //             }
        //         }
        //     }
        // };
        //if (Self.htmlProfielItems.length > 0) Self.templateProfielItems = Self.htmlProfielItems.compile(ProfielItemsDirectives);

        /*****************/
        /* Verleende Zorg items */
        /*****************/
        // Self.htmlZorgBesteedItems = $('#verleendezorgtabel tbody');
        // var ZorgBesteedItemsDirectives = {
        //     'tr': {
        //         'regel<-': {
        //             'td.id': 'regel.VOLGNUMMER',
        //             'td.zorg': function(arg) {
        //                 return wordWrap(arg.item.ZORG, 40);
        //             },
        //             'td.frequentie': function(arg) {
        //                 return arg.item.FREQUENTIEAANTAL +' '+ arg.item.FREQUENTIE;
        //             },
        //             'td.intensiteit': function(arg) {
        //                 return arg.item.INTENSITEITAANTAL +' '+ arg.item.INTENSITEIT;
        //             },
        //             'td.sinds': 'regel.SINDS',
        //             'td.tot': 'regel.TOT',
        //             'td.effect': function(arg) {
        //                 return '<label class="radio effect-'+arg.item.EFFECT_CODE+'">'+arg.item.EFFECT+'</label>';
        //             },
        //             '@onclick': function(arg) {
        //                 return 'JeugdZorg.CirkelOverzicht.selecteerRegel(this);';
        //             },
        //             '@ondblclick': function(arg) {
        //                 return 'JeugdZorg.CirkelOverzicht.wijzigVerleendeZorg(' + arg.pos + ');';
        //             },
        //             '@class': function(arg) {
        //                 return arg.item.EFFECT;
        //             }
        //         }
        //     }
        // };
        //if (Self.htmlZorgBesteedItems.length > 0) Self.templateZorgBesteedItems = Self.htmlZorgBesteedItems.compile(ZorgBesteedItemsDirectives);

        Self.updateButtons();
    },
    serviceCall: function() {
        return _servicequery + $("#_fldRecordID").val() + '/' + $("#_fldVersie").val() + '/';
    },
    selecteerRegel: function(row) {
        var Self = JeugdZorg.CirkelOverzicht;

        if (!Self.huidigeRij().is(row)) {
            if (typeof row == 'number') {
                var tn = Self.huidigeItemsTabelNaam();
                var trs = $('#'+tn+' tbody tr');
                $(trs.get(row)).toggleClass('focusrow', true);
            } else {
                var trs = $(row).parent().find('tr');
                trs.removeClass('focusrow');
                $(row).toggleClass('focusrow', true);
            }
            if (row.hasClass('item')) {
                Self.toonVerleendeZorg(row);
            }
            Self.updateButtons();
        }
    },
    updateButtons: function() {
        var Self = JeugdZorg.CirkelOverzicht;

        var huidigeRij = Self.huidigeRij();
        $("#btnToonVerleendeZorgForm").prop('disabled', huidigeRij.length == 0);
    },
    huidigeItemsTabelNaam: function() {
        return 'probleemspecificatie';
    },
    huidigeRij: function(type) {
        var Self = JeugdZorg.CirkelOverzicht;

        type = type || 'focusrow';
        var tn = Self.huidigeItemsTabelNaam();
        return $('#'+tn+' tbody tr.'+type);
    },
    huidigeRijIndex: function(type) {
        var Self = JeugdZorg.CirkelOverzicht;

        type = type || 'focusrow';
        var tn = Self.huidigeItemsTabelNaam();
        var huidigeRij = Self.huidigeRij();
        var pos = 0;

        if (!isUndefined(huidigeRij)) {
            pos = $('#'+tn+' tbody tr').index(huidigeRij);
        }
        return pos;
    },
    probleemOfZorgKeuze: function(e) {
        var Self = JeugdZorg.CirkelOverzicht;

        var element = '#aandachtsgebied';
        if (e.target.name == 'ZORG') {
            element = "#zorg";
        }
        $(element + "andersrow").toggle($(this).val().indexOf('Anders...') >= 0);
        setTimeout(function() { $(element+'anders').focus() }, 100);
    },
    toonAandachtsGebiedForm: function(codes) {
        var Self = JeugdZorg.CirkelOverzicht;

        Self.initAandachtsGebiedForm('insert', codes[0], codes[1]);
        $("#dlgVoegAandachtsGebiedToe").modal('show');
    },
    wijzigAandachtsGebied: function(rij) {
        var Self = JeugdZorg.CirkelOverzicht;

        var data = rij.data('item');
        var form = $("#dlgVoegAandachtsGebiedToe");

        Self.initAandachtsGebiedForm('edit', data.DOMEIN_CODE, data.NIVEAU_CODE);
        form.find("input[name=_hdnRecordID]").val(data.PLR__RECORDID);
        form.find(":input[name=probleem]").val(data.PROBLEEM).trigger("chosen:updated");
        form.find("input[name=ERNST]").filter("[value="+data.ERNST+"]").prop('checked', true);
        form.modal('show');
    },
    initAandachtsGebiedForm: function(state, domein, niveau) {
        var Self = JeugdZorg.CirkelOverzicht;

        Self.vulProbleemLijst(domein, niveau);
        $("#aandachtsgebiedandersrow").hide(); // anders veld verbergen
        $("#dlgVoegAandachtsGebiedToe input[name=_hdnState]").val(state);

        if (state == 'insert') {
            $("#btnBewaarAandachtsGebied").text('Voeg toe');
            $("#dlgVoegAandachtsGebiedToe input[name=ERNST]").prop('checked', false);
        } else {
            $("#btnBewaarAandachtsGebied").text('Wijzig');
        }

        var segment = JeugdZorg.cirkelModel.segmenten_codes.indexOf(domein);
        var laag = JeugdZorg.cirkelModel.lagen_codes.indexOf(niveau);

        $("#dlgVoegAandachtsGebiedToe .modal-title").text('Aandachtspunt toevoegen van ' + JeugdZorg.cirkelModel.segmenten[segment] + ' ' + JeugdZorg.cirkelModel.lagen[laag]);

    },
    toggleZorgBesteed: function() {
        $("#probleemspecificatie tr.subitem").toggle();
    },
    toonVerleendeZorgForm: function() {
        var Self = JeugdZorg.CirkelOverzicht;

        Self.initZorgForm('insert');
        $("#dlgVoegZorgToe").modal('show');
    },
    toonVerleendeZorg: function(rij) {
        var Self = JeugdZorg.CirkelOverzicht;

        Self.getZorgBesteed(rij);
    },
    wijzigVerleendeZorg: function(rij) {
        var Self = JeugdZorg.CirkelOverzicht;

        if (Self.huidigeRijIndex() == -1) {
            Polaris.Base.errorDialog('U dient een aandachtspunt te selecteren.');
            return false;
        }

        var data = rij.data('item');
        var form = $("#dlgVoegZorgToe");

        Self.initZorgForm('edit');

        JeugdZorg.setFields(Self.BesteeddeZorgVelden, data, form);
        form.find("input[name=_hdnRecordID]").val(data.PLR__RECORDID);
        form.find("input[name=EFFECT]").filter("[value="+data.EFFECT_CODE+"]").prop('checked', true);
        form.find("input[name=FREQUENTIE]").filter("[value='"+data.FREQUENTIE+"']").click();
        form.find("input[name=INTENSITEIT]").filter("[value='"+data.INTENSITEIT+"']").click();

        form.modal('show');
    },
    initZorgForm: function(state) {
        var Self = JeugdZorg.CirkelOverzicht;

        var rij = Self.huidigeRij();
        var domein = rij.data('item').DOMEIN_CODE;
        var niveau = rij.data('item').NIVEAU_CODE;
        var subsetZorg = [];

        $(Self.dataZorg).each(function(i, zorg) {
            if (zorg.domein == domein && zorg.niveau == niveau) {
                subsetZorg.push(zorg);
            }
        });
        $("#slcZorg").children().remove().end()
        .addOptions(subsetZorg, 'zorg', 'zorg')
        .addOptions([{'zorg': ''}], 'zorg', 'zorg')
        .addOptions([{'zorg': 'Anders...'}], 'zorg', 'zorg');

        $("#dlgVoegZorgToe input[name=_hdnState]").val(state);
        $("#zorgandersrow").hide(); // anders veld verbergen

        if (state == 'insert') {
            $("#btnBewaarVerleendeZorg").text('Voeg toe');
        } else {
            $("#btnBewaarVerleendeZorg").text('Wijzig');
        }
    },
    verwijderVersie: function(e) {
        var Self = JeugdZorg.CirkelOverzicht;

        e.preventDefault();
        if (confirm('Weet u zeker dat u versie ' + $(this).data('versie') + ' wilt verwijderen? Deze actie kan niet ongedaan worden.')) {
            var senddata = [];
            senddata.push({name: '_hdnDatabase', value: $("input[name=_hdnDatabase]").val()});
            senddata.push({name: '_hdnFORMTYPE', value: 'verwijderVersie'});
            senddata.push({name: '_hdnProcessedByModule', value: 'true'});
            senddata.push({name: '_hdnRecordID', value: Polaris.Base.customUrlEncode(Self.dataPZN.PLR__RECORDID)});
            senddata.push({name: 'VERSIE', value: $(this).data('versie')});

            Polaris.Ajax.postJSON(Self.serviceCall(), senddata, null, 'Versie ' +$(this).data('versie')+ ' is verwijderd', function() {
                location.reload();
            });

        }
    },
    startNieuweVersie: function() {
        var Self = JeugdZorg.CirkelOverzicht;

        var senddata = [];
        var form = $("#dlgVoegVersieToe");
        senddata.push({name: '_hdnDatabase', value: $("input[name=_hdnDatabase]").val()});
        senddata.push({name: '_hdnFORMTYPE', value: 'startNieuweVersie'});
        senddata.push({name: '_hdnProcessedByModule', value: 'true'});
        senddata.push({name: '_hdnRecordID', value: Polaris.Base.customUrlEncode(Self.dataPZN.PLR__RECORDID)});
        senddata.push({name: 'OMSCHRIJVING', value: form.find("input[name=OMSCHRIJVING]").val()});

        Polaris.Ajax.postJSON(Self.serviceCall(), senddata, null, 'Nieuwe versie gestart', function() {
            location.reload();
        });
    },
    toonNieuweVersie: function() {
        var Self = JeugdZorg.CirkelOverzicht;

        $("#dlgVoegVersieToe").modal('show');
        $("#dlgVoegVersieToe input[type=text]").focus();
    },
    vulOverzichtGegevens: function() {
        var Self = JeugdZorg.CirkelOverzicht;

        if (Self.dataPZN) {
            $("#_fldRecordID").val(Polaris.Base.customUrlEncode(Self.dataPZN.PLR__RECORDID));
            $("#_fldCOMPLETENAAM").val(Self.dataPZN.C_VOORNAAM +' '+ Self.dataPZN.C_ACHTERNAAM);
            $("#_fldRELATIE_TOV_ZORGGERECHTIGDE").change();
            Self.vulProfielRegels();
            Self.getZorgBesteed();
            JeugdZorg.cirkelModel.updateData(Self.dataProfielItems);
        }
    },
    getZorgverlenerGegevens: function(zorgverlener, callback) {
        var Self = JeugdZorg.CirkelOverzicht;

        $.getJSON(Self.serviceCall(), {
            'func': 'pznprofiel',
            'zorgverlener': zorgverlener
        }, function(data, textStatus) {
            /* Leg data vast als lokale/globale var */
            Self.dataPZN = data.PZN[0];
            Self.dataProfielItems = data.profielRegels;
            Self.dataProblemen = data.problemen;
            Self.dataZorg = data.zorg;
            if (textStatus == 'success') {
                if (callback)
                    callback();
            } else {
                Polaris.Base.modalMessage(textStatus);
            }
        });
    },
    refreshData: function() {
        var Self = JeugdZorg.CirkelOverzicht;

        if (Polaris.Base.getLocationVariable('volgnummer') != ''
        ) {
            Self.getZorgverlenerGegevens($("input[name=_hdnGekozenZorgverlener]").val(), Self.vulOverzichtGegevens);
        }
    },
    updateZorgBesteed: function(e) {
        var Self = JeugdZorg.CirkelOverzicht;

        var volgnr = this.name.split('_');
        volgnr = volgnr[1];

        var senddata = [];
        senddata.push({name: '_hdnDatabase', value: $("input[name=_hdnDatabase]").val()});
        senddata.push({name: '_hdnFORMTYPE', value: 'updateZorgBesteed'});
        senddata.push({name: '_hdnProcessedByModule', value: 'true'});
        senddata.push({name: '_hdnRecordID', value: Polaris.Base.customUrlEncode(Self.dataPZN.PLR__RECORDID)});
        senddata.push({name: 'volgnummer', value: volgnr});
        senddata.push({name: 'zorgbesteed', value: $(this).val()});

        Polaris.Ajax.postJSON(Self.serviceCall(), senddata, null, 'Gegevens opgeslagen.', function (data) {
            Self.refreshData();
        });
    },
    vulProfielRegels: function(callback) {
        var Self = JeugdZorg.CirkelOverzicht;

        if (Self.dataProfielItems.length) {
            $("#probleemspec").show();
            $("#nodata").hide();
            var table = $("#probleemspecificatie tbody");
            table.find('tr').remove();
            var combinedview = $("input[name=_hdnGekozenZorgverlener]").val() == 99999;
            var showactions = combinedview ? 'hide' : '';
            $(Self.dataProfielItems).each(function(index, elem) {
                if (combinedview)
                    (() => {
                        if (!isUndefined(Self.dataProfielItems[index-1]) && Self.dataProfielItems[index-1].NAAM == elem.NAAM) {
                            return false;
                        } else {
                            table.append($(`
                                <tr class="xrow">
                                    <td colspan="6" class="naam">${elem.NAAM}</td>
                                </tr>
                            `));
                        }
                    })();
                var cleanprobleem = wrap(elem.PROBLEEM, 80);
                var badgeclassname = elem.AANTALZORGBESTEED == '0' ? 'danger' : 'plain';
                var newtr = $(`
                    <tr class="item ${elem.ERNST}" id="adg_${elem.VOLGNUMMER}">
                        <td class="probleem">&#x25CB;&nbsp;${cleanprobleem}</td>
                        <td class="gebied">${elem.DOMEIN}</td>
                        <td class="ernst"><label class="ernst ernst-${elem.ERNST}">${elem.ERNST}</label></td>
                        <td class="aantalzorg"><span class="badge badge-${badgeclassname}">${elem.AANTALZORGBESTEED}</span></td>
                        <td class="actie ${showactions}">
                            <button title="Verwijder aandachtsgebied" class="pull-right btn btn-default btn-outline btn-xs verwijderItem" data-toggle="tooltip" data-placement="left" data-original-title="Verwijder" type="button"><i class="fa fa-trash"></i></button>
                            <button title="Wijzig aandachtsgebied" class="pull-right btn btn-default btn-outline btn-xs wijzigItem" data-toggle="tooltip" data-placement="left" data-original-title="Wijzig" type="button"><i class="fa fa-edit"></i></button>
                            <button title="Voeg verleende zorg toe" class="pull-right btn btn-primary btn-outline btn-xs voegtoeZorgBesteedItem" data-toggle="tooltip" data-placement="left" data-original-title="Verwijder" type="button"><i class="fa fa-plus"></i></button>
                        </td>
                        <td class="id hide">${elem.VOLGNUMMER}</td>
                        <td class="code hide">${elem.CODE}</td>
                        <td class="domein hide">${elem.DOMEIN_CODE}</td>
                        <td class="niveau hide">${elem.NIVEAU_CODE}</td>
                    </tr>
                `).data('item', elem);
                table.append(newtr);
            });
        } else {
            $("#probleemspec").hide();
            $("#nodata").show();
        }
        if (callback) callback();
    },
    vulZorgBesteedRegels: function(rij) {
        // rij is altijd besteede zorg of leeg
        var Self = JeugdZorg.CirkelOverzicht;

        $("#probleemspecificatie").hide();
        if (!isUndefined(rij)) {
            rij.nextUntil("tr.item").remove();
        } else {
            $("#probleemspecificatie tr.subitem").remove();
        }
        var showactions = $("input[name=_hdnGekozenZorgverlener]").val() != 99999 ? '' : 'hidebuttons';
        $(Self.dataZorgBesteedItems).each(function(index, elem) {
            if (isUndefined(rij) || !isUndefined(rij) && rij[0].id == 'adg_'+elem.PROBLEEM_VOLGNUMMER) {
                tr = document.getElementById("adg_"+elem.PROBLEEM_VOLGNUMMER);
                var cleanzorg = wrap(elem.ZORG, 70);
                var freq = elem.FREQUENTIE !== 'Nvt' ? elem.FREQUENTIEAANTAL + ' x ' + elem.FREQUENTIE : '';
                var intens = elem.INTENSITEIT !== 'Nvt' ?  ',&nbsp;' + elem.INTENSITEITAANTAL + '&nbsp;' + elem.INTENSITEIT + '&nbsp;&nbsp;&nbsp;' : '';
                var sinds = elem.SINDS !== null ?  'vanaf&nbsp;' + elem.SINDS : '';
                var tot = elem.TOT !== null ? '&nbsp;tot&nbsp;' + elem.TOT : '';
                newtr = $(`
                    <tr class="xrow subitem effect-${elem.EFFECT_CODE}" id="bz_${elem.VOLGNUMMER}">
                        <td class="zorg" colspan="4">${cleanzorg}<br><span class="freq">${freq} ${intens} ${sinds} ${tot}</span></td>
                        <td class="actie ${showactions}">
                            <button class="pull-right btn btn-default btn-outline btn-xs verwijderItem" data-toggle="tooltip" data-placement="left" data-original-title="Verwijder" type="button"><i class="fa fa-trash"></i></button>
                            <button class="pull-right btn btn-default btn-outline btn-xs wijzigItem" data-toggle="tooltip" data-placement="left" data-original-title="Wijzig" type="button"><i class="fa fa-edit"></i></button>
                        </td>
                    </tr>
                `).data('item', elem);
                newtr.insertAfter(tr);
            }
        });

        $("#probleemspecificatie").show();
        $(window).trigger('resize');
    },
    getZorgBesteed: function(rij) {
        var Self = JeugdZorg.CirkelOverzicht;

        var probleem_volgnummer = '';
        if (!isUndefined(rij)) {
            if (rij.data('item').PROBLEEM_VOLGNUMMER) {
                var probleem_volgnummer = rij.data('item').PROBLEEM_VOLGNUMMER;
            } else {
                var probleem_volgnummer = rij.data('item').VOLGNUMMER;
            }
        }
        log(probleem_volgnummer);
        var zorgverlener = $("input[name=_hdnGekozenZorgverlener]").val();
        //Self.clearTZVGegevens();
        $.getJSON(Self.serviceCall(), {
            'func': 'zorgbesteed',
            'volgnummer': probleem_volgnummer,
            'zorgverlener': zorgverlener
        }, function(data, textStatus) {
            /* Leg data vast als lokale/globale var */
            Self.dataZorgBesteedItems = data.zorgBesteedRegels;
            if (textStatus == 'success') {
                Self.vulZorgBesteedRegels(rij);
            } else {
                Polaris.Base.modalMessage(textStatus);
            }
        });
    },
    getZorgplan: function(PZNID, callback) {
        var Self = JeugdZorg.CirkelOverzicht;

        $.getJSON(Self.serviceCall(), {
            'func': 'zorgplan'
        }, function(data, textStatus) {
            /* Leg data vast als lokale/globale var */
            Self.dataZorgplan = data;
            if (textStatus == 'success') {
                if (callback)
                    callback(data);
            } else {
                Polaris.Base.modalMessage(textStatus);
            }
        });
    },
    cirkelModelKlaar: function() {
        var Self = JeugdZorg.CirkelOverzicht;

        if (confirm('Weet u zeker dat u klaar bent met dit cirkelmodel?')) {
            var senddata = [];
            senddata.push({name: '_hdnDatabase', value: $("input[name=_hdnDatabase]").val()});
            senddata.push({name: '_hdnFORMTYPE', value: 'pzn_cmklaar'});
            senddata.push({name: '_hdnProcessedByModule', value: 'true'});
            senddata.push({name: '_hdnRecordID', value: Polaris.Base.customUrlEncode(Self.dataPZN.PLR__RECORDID)});
            senddata.push({name: 'PZN', value: $("#_fldRecordID").val()});

            Polaris.Ajax.postJSON(Self.serviceCall(), senddata, null, 'Cirkelmodel afgerond.', function () {
                //$("#btnCirkelModelKlaar").prop('disabled', 'disabled');
                window.close();
            });
        }
    },
    verwijderAandachtsGebied: function(row, volgnummer) {
        var Self = JeugdZorg.CirkelOverzicht;

        var rowIndex = JeugdZorg._verwijderItem(row, $("#_fldRecordID").val(), volgnummer, 'verwijderProfiel');
        Self.dataProfielItems.splice( $.inArray(Self.dataProfielItems[rowIndex], Self.dataProfielItems), 1);
        JeugdZorg.cirkelModel.updateData(Self.dataProfielItems);
    },
    verwijderZorgBesteedItem: function(row, volgnummer) {
        var Self = JeugdZorg.CirkelOverzicht;

        var rowIndex = JeugdZorg._verwijderItem(row, $("#_fldRecordID").val(), volgnummer, 'verwijderZorgBesteed');
    },
    vulProbleemLijst: function(domein, niveau) {
        var Self = JeugdZorg.CirkelOverzicht;
        var huidigeProblemen = $.grep(Self.dataProblemen, function(v) {
            return v.domein == domein && v.niveau == niveau;
        });
        $("#slcProbleem").children().remove().end()
        .addOptions(huidigeProblemen, 'probleem', 'probleem')
        .addOptions([{'probleem': ''}], 'probleem', 'probleem')
        .addOptions([{'probleem': 'Anders...'}], 'probleem', 'probleem')
        .trigger("chosen:updated");
    },
    gegevensProfielInOrde: function() {
        if (isUndefined($("input[name=ERNST]:checked").val())) {
            Polaris.Base.modalMessage('U dient een ernst te kiezen.');
            return false;
        }
        return true;
    },
    bewaarAandachtsGebied: function(e) {
        var Self = JeugdZorg.CirkelOverzicht;

        e.preventDefault();

        if (Self.gegevensProfielInOrde()) {
            var senddata = [];
            var rij = Self.huidigeRij();
            if (rij.length == 0) {
                var segment = $("#segment").val().split(',');
                var domein_code = segment[0];
                var niveau_code = segment[1];
                var probleem_volgnummer = null;
            } else {
                var domein_code = rij.data('item').DOMEIN_CODE;
                var niveau_code = rij.data('item').NIVEAU_CODE;
                var probleem_volgnummer = rij.data('item').VOLGNUMMER;
            }

            senddata.push({name: '_hdnDatabase', value: $("input[name=_hdnDatabase]").val()});
            senddata.push({name: '_hdnFORMTYPE', value: 'pzn_profiel'});
            senddata.push({name: '_hdnProcessedByModule', value: 'true'});
            senddata.push({name: '_hdnRecordID', value: Polaris.Base.customUrlEncode(Self.dataPZN.PLR__RECORDID)});
            senddata.push({name: '_hdnState', value: $("#dlgVoegAandachtsGebiedToe input[name=_hdnState]").val()});
            senddata.push({name: 'PZN', value: $("#_fldRecordID").val()});
            senddata.push({name: 'ZORGVERLENER', value: $("input[name=_hdnGekozenZorgverlener]").val()});
            senddata.push({name: 'PROBLEEM', value: $("#slcProbleem").val()});
            senddata.push({name: 'ERNST', value: $("input[name=ERNST]:checked").val()});
            senddata.push({name: 'DOMEIN', value: domein_code});
            senddata.push({name: 'NIVEAU', value: niveau_code});
            senddata.push({name: 'PROBLEEM_VOLGNUMMER', value: probleem_volgnummer});
            $($("#dataform").serializeArray()).each(function(i, obj) {
                senddata.push({name: obj.name, value: obj.value});
            });
            Polaris.Ajax.postJSON(Self.serviceCall(), senddata, null, 'Gegevens opgeslagen.', function () {
                $("#dlgVoegAandachtsGebiedToe").modal('hide');
                $("#aandachtsgebiedanders").val('');
                Self.refreshData();
            });
        }
    },
    gegevensBestedeZorgInOrde: function() {
        if (isUndefined($("input[name=EFFECT]:checked").val())) {
            Polaris.Base.modalMessage('U dient een effect te kiezen.');
            return false;
        }
        // if ($("input[name=SINDS]").val().trim().length == 0) {
        //     Polaris.Base.modalMessage('U dient aan te geven vanaf wanneer de verleende zorg geldt.');
        //     return false;
        // }
        return true;
    },
    bewaarVerleendeZorg: function(e) {
        var Self = JeugdZorg.CirkelOverzicht;

        e.preventDefault();
        if (Self.gegevensBestedeZorgInOrde()) {
            var senddata = [];
            senddata.push({name: '_hdnDatabase', value: $("input[name=_hdnDatabase]").val()});
            senddata.push({name: '_hdnFORMTYPE', value: 'bewaardeZorg'});
            senddata.push({name: '_hdnProcessedByModule', value: 'true'});
            senddata.push({name: '_hdnRecordID', value: Polaris.Base.customUrlEncode(Self.dataPZN.PLR__RECORDID)});
            var rij = Self.huidigeRij();
            if (rij.hasClass('item')) {
                var parentitem = rij.data('item');
                var probleem_volgnummer = parentitem.VOLGNUMMER;
            } else {
                //var parentitem = rij.prev().data('item');
                var parentitem = rij.data('item');
                var probleem_volgnummer = parentitem.PROBLEEM_VOLGNUMMER;
            }
            senddata.push({name: 'DOMEIN', value: parentitem.DOMEIN_CODE});
            senddata.push({name: 'NIVEAU', value: parentitem.NIVEAU_CODE});
            senddata.push({name: 'PROBLEEM_VOLGNUMMER', value: probleem_volgnummer});
            senddata.push({name: 'ZORGVERLENER', value: $("input[name=_hdnGekozenZorgverlener]").val()});
            $($("#frmVoegZorgToe").serializeArray()).each(function(i, obj) {
                senddata.push({name: obj.name, value: obj.value});
            });
            Polaris.Ajax.postJSON(Self.serviceCall(), senddata, null, 'Verleende zorg opgeslagen.', function (data) {
                $("#dlgVoegZorgToe").modal('hide');
                $("#zorganders").val('');
                Self.getZorgBesteed();
            });
        }
    },
    cirkelKeuze: function(elem, codes, active) {
        var Self = JeugdZorg.CirkelOverzicht;

        if (active) {
            var ok = true;
            if (($("input[name=_hdnActiefVolgnummer]").val() !== $("input[name=_hdnGekozenZorgverlener]").val())) {
                if ($("input[name=_hdnIsCoordinator]").val() == 'true') {
                    ok = confirm('U gaat een cirkelmodel van een andere deelnemer wijzigen. Doorgaan?');
                } else {
                    Polaris.Base.modalMessage('U kunt de cirkelmodellen van anderen niet wijzigen.');
                    ok = false;
                }

            }

            if (ok) {
                $("#segment").val(codes);
                Self.toonAandachtsGebiedForm(codes);
            }
        } else {
            $("#segment").val('<Kies segment>');
        }
    }
}

JeugdZorg.CirkelOverzicht.refreshData();
JeugdZorg.CirkelOverzicht.initialize();

$(document).ready(function() {
    var Self = JeugdZorg.CirkelOverzicht;

    $("#pnlVerleendeZorg").css({'min-height': '200px'});
});
