JeugdZorg.Chatsessie = {
    dataPZN: null,
    dataChatSessie: [],
    lastVolgnummer: 0,
    RTOStatus: '',
    htmlChat: '',
    htmlChatOog: '',
    timer: null,
    placeholder: null,
    kleurenUitvoering: {1: 'rood', 2: 'oranje', 3: 'groen'},
    kleurenResultaat: {1: 'rood', 2: 'oranje', 3: 'groen'},

    resizeWindow: function() {
        if ($(".chat-panel").length)
            $(".chat-panel").height($(window).height() - $("#tab_frm_rondetafel").offset().top - 170);
    },
    initialize: function() {
        var Self = JeugdZorg.Chatsessie;

        Self.htmlChat = $('.chat-discussion');
        Self.htmlChatOog = $('.chat-oogmonitor');

        $("#txtBerichtRTO").keypress(function(e) {
            if (e.which == 13) {
                e.preventDefault();
                $("#txtBerichtRTO").val($("#txtBerichtRTO").val()+"\n");
            }
        });
        $("#txtBerichtRTO").autoGrow();
        $("#btnSendMessageRTO").click(function() {
            Self.verstuurBericht("#txtBerichtRTO", 'RTO');
        });


        $("#txtBerichtOog").keypress(function(e) {
            if (e.which == 13) {
                e.preventDefault();
                $("#txtBerichtOog").val($("#txtBerichtOog").val()+"\n");
            }
        });
        $("#txtBerichtOog").autoGrow();
        $("#btnSendMessageOog").click(function() {
            Self.verstuurBericht("#txtBerichtOog", 'OOG');
        });

        // Generic
        $(window).resize(Self.resizeWindow);
        $(".chatfield").appendTo(".footer").hide();
        Self.resizeWindow();
        Self.scrollToBottom();
        Self.timer = new Self.recurringTimer(Self.refreshData, 10000);

        $(".chatarea").autoGrow();

        $("#btnPZNRTO").click(Self.toonRTOStarten);
        $("#btnRTOStartenSave").click(Self.RTOStarten);

        $("#btnRTOPauze").click(function(e) {
            if (!$(this).hasClass('disabled'))
                Self.pauzeerRTO(true);
        });
        $("#btnRTOOpenen").click(function() {
            if (!$(this).hasClass('disabled'))
                Self.toonRTOStarten();
            //Self.verstuurBericht("#txtBerichtRTO", 'RTO', '<i class="fa fa-bell"></i> <b>ER IS EEN DEADLINE VOOR DIT RTO:</b>');
        });
        $("#btnRTOEinde").click(function() {
            if (confirm("Weet u zeker dat u het RTO wilt afsluiten?")) {
                Self.eindeRTO();
                Self.verstuurBericht("#txtBerichtRTO", 'RTO', '<i class="fa fa-stop"></i> <b>DIT RTO IS AFGEROND. WE GAAN VERDER MET HET OOG.</b>');
            }
        });
    },
    gegevensRTOStartenInOrde: function() {
        var now = new Date();
        $("#fldRTODeadline").datepicker('update')
        if (+($("#fldRTODeadline").datepicker('getDate')) < +(now)) {
            Polaris.Base.modalMessage('De datum moet in de toekomst liggen.');
            return false;
        }
        return true;
    },
    btnRTOButtonsOpPauze: function(onoff) {
        if (onoff) {
            $("#btnRTOPauze").removeClass('disabled').addClass('btn-success');
            $("#btnRTOOpenen").addClass('disabled').removeClass('btn-success');
        } else {
            $("#btnRTOPauze").addClass('disabled').removeClass('btn-success');
            $("#btnRTOOpenen").removeClass('disabled').addClass('btn-success');
        }
    },
    pauzeerRTO: function(onoff) {
        var Self = JeugdZorg.Chatsessie;

        var senddata = [];

        senddata.push({name: '_hdnFORMTYPE', value: 'pauzeerRTO'});
        senddata.push({name: '_hdnProcessedByModule', value: 'true'});
        senddata.push({name: '_hdnRecordID', value: $("#_fldRecordID").val()});
        senddata.push({name: '_hdnState', value: 'insert'});
        senddata.push({name: 'pzn', value: $("#_fldRecordID").val()});
        senddata.push({name: 'onoff', value: onoff});

        Self.btnRTOButtonsOpPauze(onoff);

        Polaris.Ajax.postJSON(_servicequery, senddata, null, null, function () {
        });
    },
    eindeRTO: function() {
        var Self = JeugdZorg.Chatsessie;

        var senddata = [];

        senddata.push({name: '_hdnDatabase', value: $("input[name=_hdnDatabase]").val()});
        senddata.push({name: '_hdnFORMTYPE', value: 'pzn_rtoeinde'});
        senddata.push({name: '_hdnRecordID', value: $("#_fldRecordID").val()});
        senddata.push({name: '_hdnProcessedByModule', value: 'true'});
        senddata.push({name: '_hdnState', value: 'edit'});
        senddata.push({name: 'pzn', value: $("#_fldRecordID").val()});
        Polaris.Ajax.postJSON(_servicequery, senddata, null, null, function () {
            //
        });
    },
    toonRTOStarten: function() {
        var Self = JeugdZorg.Chatsessie;

        $("#dlgRTOStarten").modal('show');

        $("#fldRTODeadline").val($('.rto_invullenvoor span').text());
        $("#fldRTODeadline").change().datepicker('update');
        // Indien modal gecanceld wordt dan toggle terugzetten
        $("#dlgRTOStarten").on('hidden.bs.modal', function() {
            Self.btnRTOButtonsOpPauze(false);
            $('.rto_invullenvoor span').text($("#fldRTODeadline").val());
        });
        return true;
    },
    RTOStarten: function(e) {
        var Self = JeugdZorg.Chatsessie;

        e.preventDefault();

        if (Self.gegevensRTOStartenInOrde()) {
            $("#dlgRTOStarten").modal('hide');
            var senddata = [];
            senddata.push({name: '_hdnDatabase', value: $("input[name=_hdnDatabase]").val()});
            senddata.push({name: '_hdnFORMTYPE', value: 'pzn_rtostarten'});
            senddata.push({name: '_hdnProcessedByModule', value: 'true'});
            senddata.push({name: '_hdnRecordID', value: $("#_fldRecordID").val()});
            senddata.push({name: 'PZN', value: $("#_fldRecordID").val()});
            senddata.push({name: 'DEADLINE', value: $("#fldRTODeadline").val()});
            senddata.push({name: 'NOTIFICEERDEADLINE', value: $("#fldNotificeerDeadline").prop('checked') ? 'J' : 'N'});

            Polaris.Ajax.postJSON(_servicequery, senddata, null, 'RTO is gestart.', function () {
                $("#_fldSTATUS").val('RTOGESTART').parent().find('.huidigestatus').text("RTO GESTART");
                $("#btnPZNRTO").hide();
                $("#tab_frm_rondetafel").parent().show();
                $("#tab_frm_rondetafel").click();
                $("#tab_frm_oogmonitor").parent().show();

                Self.verstuurBericht("#txtBerichtRTO", 'RTO', '<i class="fa fa-bell"></i> <b>ER IS EEN DEADLINE VOOR DIT RTO: '+$("#fldRTODeadline").val()+'</b>');
                Self.pauzeerRTO(false);
            });
        };
    },
    _verstuurBericht: function(bericht, soort, callback) {
        var Self = JeugdZorg.Chatsessie;

        if (bericht == '') return;

        Self.timer.pause();
        var senddata = [];

        senddata.push({name: '_hdnFORMTYPE', value: 'bewaarbericht'});
        senddata.push({name: '_hdnProcessedByModule', value: 'true'});
        senddata.push({name: '_hdnState', value: 'insert'});
        senddata.push({name: '_hdnMESSAGETYPE', value: soort});
        senddata.push({name: 'pzn', value: $("#_fldRecordID").val()});
        senddata.push({name: 'bericht', value: bericht});

        if (soort == 'OOG') {
            if(typeof $("#oog-uitvoering input:checked").val() == 'undefined'
            || typeof $("#oog-resultaat input:checked").val() == 'undefined') {
                Polaris.Base.modalMessage('Selecteer een Uitvoering en Resultaat');
                Self.timer.resume();
                return;
            }
            senddata.push({name: 'uitvoering', value: $("#oog-uitvoering input:checked").val()});
            senddata.push({name: 'resultaat', value: $("#oog-resultaat input:checked").val()});
        }

        Polaris.Ajax.postJSON(_servicequery, senddata, null, null, function () {
            Self.refreshData(callback);
            Self.timer.resume();
        });
    },
    verstuurBericht: function(id, soort, extraboodschap) {
        var Self = JeugdZorg.Chatsessie;

        var bericht = $(id).val();
        if (extraboodschap)
            bericht = extraboodschap + '\n' + bericht;
        Self._verstuurBericht(bericht, soort, function() {
            $(id).val('').css({'height':'auto'});
            Self.scrollToBottom();
        });
    },
    _getChatSessie: function(PZNID, callback) {
        var Self = JeugdZorg.Chatsessie;

        var last = 0;

        $.getJSON(_servicequery, {
            'func': 'pznchatsessie',
            'last': Self.lastVolgnummer,
            'pznid': PZNID
        }, function(data, textStatus) {
            /* Leg data vast als lokale/globale var */
            Self.RTOStatus = data.status;

            if (!isUndefined(data.chatSessie)) {
                if (data.chatSessie.length > 0) {
                    Self.dataChatSessie.push.apply(Self.dataChatSessie, data.chatSessie);
                    Self.lastVolgnummer = Self.dataChatSessie[Self.dataChatSessie.length-1].VOLGNUMMER;
                    Self.htmlChat.html(Self.renderChat(Self.dataChatSessie, 'RTO'));
                    Self.htmlChatOog.html(Self.renderChat(Self.dataChatSessie, 'OOG'));
                }
            } else {
                Self.lastVolgnummer = 0;
            }
            if (textStatus == 'success') {
                if (callback)
                    callback();
            } else {
                Polaris.Base.modalMessage(textStatus);
            }
        });
    },
    RTOBeschikbaar : function() {
        var Self = JeugdZorg.Chatsessie;
        return (Self.RTOStatus !== 'PAUZE' && $("#_fldSTATUS").val() == 'RTOGESTART');
    },
    refreshData: function(callback) {
        var Self = JeugdZorg.Chatsessie;
        //autoclear = autoclear || false;
        if ($("#tab_frm_rondetafel").is(":visible")) {
            if (!isUndefined($("#_fldRecordID").val()) && $("#_fldRecordID").val() != '') {
                Self._getChatSessie($("#_fldRecordID").val(), function() {
                    var scrollPos1 = $('.chat-discussion')[0].scrollTop;
                    var scrollPos2 = $('.chat-oogmonitor')[0].scrollTop;
                    Self.vulChatSessie();
                    Self.btnRTOButtonsOpPauze(Self.RTOBeschikbaar());
                    $(".pauzetext").toggle(Self.RTOBeschikbaar());
                    if (callback) {
                        callback();
                    }
                    else {
                        // restore previous scroll position
                        $('.chat-discussion').scrollTop(scrollPos1);
                        $('.chat-oogmonitor').scrollTop(scrollPos2);
                    }
                });
            }
        }
    },
    scrollToBottom: function() {
        $('.chat-discussion').scrollTop($('.chat-discussion')[0].scrollHeight);
        $('.chat-oogmonitor').scrollTop($('.chat-oogmonitor')[0].scrollHeight + 100);
    },
    renderItem: function(item, showname) {
        var Self = JeugdZorg.Chatsessie;

        var chat = $("<div/>", {class: 'chat'});

        if (showname) {
            var a = $("<div/>", {href: '#', text: item.ZORGVERLENER_NAAM, class: 'chat-author'});
            var t = $("<span/>", {text: item.AANMAAKDATUM + ' ' + item.AANMAAKTIJD, class: 'chat-time'});
            a.append(t);
            chat.append(a);
        }

        if (item.MESSAGETYPE == 'OOG') {
            var b = $("<div/>", {
                text: 'Uitvoering',
                class:'btn btn-sm uitvoering-'+Self.kleurenUitvoering[item.UITVOERING],
                role:"button"
            });
            chat.append(b);
            b = $("<div/>", {
                text: 'Resultaat',
                class:'btn btn-sm resultaat-'+Self.kleurenResultaat[item.RESULTAAT],
                role:"button"
            });
            chat.append(b);
        }
        var c = $("<span/>", {class: 'chat-content'}).append(Polaris.Base.nl2br(item.BERICHT));
        chat.append(c);

        var chatentry = $("<div/>", {class: 'chat-entry'});

        if (showname)
            chatentry.append($("<i/>", {class:"chat-avatar fa fa-2x fa-user"}));
        chatentry.append(chat);
        return chatentry;
    },
    renderChat: function(data, messagetype) {
        var Self = JeugdZorg.Chatsessie;

        var allitems = $();
        var skipname = false;

        $.each(data, function(i, item) {
            showname = ((i > 0) && data[i-1].ZORGVERLENER_NAAM != item.ZORGVERLENER_NAAM) || ((i > 0) && data[i-1].AANMAAKDATUM != item.AANMAAKDATUM);
            allitems = allitems.add(Self.renderItem(item, showname || i == 0));
        });
        return allitems;
    },
    vulChatSessie: function() {
        var Self = JeugdZorg.Chatsessie;

        var RTOData = jQuery.grep($.makeArray( Self.dataChatSessie ), function( a ) {
            return a.MESSAGETYPE == 'RTO';
        });

        var OOGData = jQuery.grep($.makeArray( Self.dataChatSessie ), function( a ) {
            return a.MESSAGETYPE == 'OOG';
        });

        Self.htmlChat.html(Self.renderChat(RTOData, 'RTO'));
        Self.htmlChatOog.html(Self.renderChat(OOGData, 'OOG'));

        var isCoordinator = $("input[name=_hdnIsCoordinator]").val() == 'true';
        var isJeugdigeOfVerzorger = $("input[name=_hdnIsJeugdigeOfVerzorger]").val() == 'true';
        if (Self.placeholder == null)
            Self.placeholder = $("#txtBerichtRTO").attr('placeholder');
        if (!isCoordinator && !isJeugdigeOfVerzorger) {
            if (Self.RTOBeschikbaar()) {
                $("#txtBerichtRTO").prop('placeholder', Self.placeholder);
                $("#txtBerichtRTO").prop('disabled', false);
            } else {
                $("#txtBerichtRTO").prop('placeholder', 'Dit RTO is gepauzeerd...');
                $("#txtBerichtRTO").prop('disabled', true);
            }
        }
    },
    recurringTimer: function(callback, delay) {
        var timerId, start, remaining = delay;

        this.pause = function() {
            window.clearTimeout(timerId);
            remaining -= new Date() - start;
        };

        var resume = function() {
            start = new Date();
            timerId = window.setTimeout(function() {
                remaining = delay;
                resume();
                callback();
            }, remaining);
        };

        this.resume = resume;

        this.resume();
    }
}

$(document).ready(function() {
    var Self = JeugdZorg.Chatsessie;

    if ($(".chat-panel").length) {
        Self.initialize();
        Self.refreshData();
    }
});
