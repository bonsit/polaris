JeugdZorg.cirkelModel = {
    r: null,
    arcs: null,
    data: [],

    defaults: {
        offsetLeft: 90,
        offsetTop: 165,
        showRadioButtons: false
    },

    ernstIndex: {'zwaar': 3, 'middel': 2, 'matig': 1, 'kans': 0},
    ernstKleuren: {'zwaar': 'red', 'middel': '#d99690', 'matig': '#ffc001', 'kans': '#99ff99'},

    lagen: ['Omgeving', 'Gezin', 'Kind'],
    lagen_codes: ['o', 'g', 'k'],
    segmenten: ['Emotioneel', 'Relaties', 'Veiligheid', 'Materieel', 'Ontwikkeling', 'Gezondheid'],
    segmenten_codes: ['emot', 'rela', 'vlgh', 'mat', 'ontw', 'gezh'],

    initializeLocalization: function() {
        $.jsperanto.init(function(t){
        }, {
            fallbackLang: 'nl',
            dicoPath: _serverroot+'/modules/jeugdzorg/module.pzn/languages',
            setDollarT: true,
            async : false,
            lang:g_lang.toLowerCase()
        });
    },
    resetSegments: function(arcs, current) {
        var Self =  JeugdZorg.cirkelModel;

        arcs.forEach(function(arc) {
            if (arc !== current) {
                arc.active = false;
                Self.activateSegment(arc);
            }
        })
    },
    findColor: function(seg) {
        var Self =  JeugdZorg.cirkelModel;

        var foundColor = false;
        $(Self.data).each(function(i, elem) {
            var probleem = seg.index[0] + '-' + seg.index[1];
            if (elem.DOMEIN + '-' + elem.NIVEAU == probleem) {
                if (Self.isMaxErnst(Self.data, elem)) {
                    foundColor = Self.ernstKleuren[elem.ERNST];
                }
            }
        });
        if (seg.occupied && seg.active) {
            foundColor = 'green';
        }
        if (!foundColor) {
            foundColor = '#fff';
        }
        return foundColor;
    },
    activateSegment: function(seg) {
        var Self =  JeugdZorg.cirkelModel;

        var fillclr = null;
        if (seg.active) {
            fillclr = 'rgb(240,240,240)';
        } else {
            fillclr = Self.findColor(seg);
        }

        if (Self.alleZorgBesteed(Self.data, seg) || seg.active) {
            seg.attr('fill', fillclr);
        } else {

            // Now lets create pattern
            var pat = Self.r.path("M10-5-10,15M15,0,0,15M0-5-20,15").attr({
                    fill: '#fff',
                    stroke: fillclr,
                    strokeWidth: 5
                });
            // To create pattern,
            // just specify dimensions in pattern method:
            var p = pat.pattern(0, 0, 10, 10);

            seg.attr('fill', p);
        }

        // if (seg.occupied) {
        //     seg.attr('cursor', 'default');
        // } else {
        //     seg.attr('cursor', 'pointer');
        // }
    },
    showCircleText: function(i, text) {
        var Self =  JeugdZorg.cirkelModel;

        var boxHeight = [25, 25, 25],
            boxWidth = [90, 60, 50],
            boxCenter = [200, 200, 200],
            boxTop = [-165, -115, -63];
        var chbWidth = boxHeight[0] - 8;

        var rect = Self.r.rect(Self.settings.offsetLeft + boxCenter[i] - 25 - (boxWidth[i]/2), Self.settings.offsetTop + boxTop[i], boxWidth[i] + chbWidth - 10 + (boxHeight[i] - chbWidth)/2, boxHeight[i], boxHeight[i]/2).attr({fill: '#000'});
        if (Self.settings.showRadioButtons) {
            var chb = Self.r.rect(Self.settings.offsetLeft + boxCenter[i] - 35 + (boxWidth[i]/2), Self.settings.offsetTop + boxTop[i] + (boxHeight[i] - chbWidth)/2, chbWidth, chbWidth, chbWidth/2).attr({fill: '#fff'});
            chb.attr({cursor: "pointer"});
            chb.click(function() {
                alert('clicked');
            });
        }
        var attr = {font: "1em Helvetica", opacity: 1};
        var circleTextOffset = -20;
        if (Self.settings.showRadioButtons) {
            circleTextOffset = -29;
        }
        var txt = Self.r.text(Self.settings.offsetLeft + boxCenter[i] + circleTextOffset, Self.settings.offsetTop + boxTop[i] + 16, text).attr(attr).attr({fill: "#fff", 'text-anchor': 'middle'});
    },
    showSegmentText: function(i, text) {
        var Self =  JeugdZorg.cirkelModel;
        var boxCenter = [300, 360 + (+Self.settings.showRadioButtons) * 20, 300, 10, -70 - (+Self.settings.showRadioButtons) * 20, -0],
            boxTop = [-152, -13, 120, 120, -13, -152];
        var chbWidth = 18;

        var textParts = text.split('\n');
        var attr = {font: "1em Helvetica", opacity: 1};
        if (textParts.length > 1) {
            Self.r.text(Self.settings.offsetLeft + boxCenter[i], Self.settings.offsetTop + boxTop[i] + 12, textParts[0]).attr(attr).attr({fill: "#000"});
            Self.r.text(Self.settings.offsetLeft + boxCenter[i], Self.settings.offsetTop + boxTop[i] + 27, textParts[1]).attr(attr).attr({fill: "#000"});
        } else {
            Self.r.text(Self.settings.offsetLeft + boxCenter[i], Self.settings.offsetTop + boxTop[i] + 12, text).attr(attr).attr({fill: "#000"});
        }
    },
    alleZorgBesteed: function(data, theElem) {
        var Self =  JeugdZorg.cirkelModel;

        var indexes = theElem.indexcodes;
        var result = true;

        $(data).each(function(i, elem) {
            if (indexes[0] == elem.DOMEIN_CODE && indexes[1] == elem.NIVEAU_CODE) {
                if (elem.ZORGBESTEED !== 'J' && elem.ZORGBESTEED !== '' && elem.ZORGBESTEED !== null) {
                    result = false;
                }
            }
        });
        return result;
    },
    isMaxErnst: function(data, theElem) {
        var Self =  JeugdZorg.cirkelModel;

        var result = true;
        $(data).each(function(i, elem) {
            if ((theElem.VOLGNUMMER !== elem.VOLGNUMMER) && theElem.DOMEIN_CODE == elem.DOMEIN_CODE && theElem.NIVEAU_CODE == elem.NIVEAU_CODE) {
                if (Self.ernstIndex[elem.ERNST] > Self.ernstIndex[theElem.ERNST]) {
                    result = false;
                }
            }
        });
        return result;
    },
    clearAll: function() {
        var Self =  JeugdZorg.cirkelModel;

        Self.resetSegments(Self.arcs);
    },
    updateData: function(data) {
        var Self =  JeugdZorg.cirkelModel;

        Self.data = data;

        var processed = false;
        if (Self.arcs) {
            Self.arcs.forEach(function(arc) {
                processed = false;
                arc.occupied = false;
                arc.zorgbesteed= false;
                arc.filter_visible = true;
                $(Self.data).each(function(i, elem) {
                    var probleem = arc.indexcodes[0] + '-' + arc.indexcodes[1];
                    if (elem.DOMEIN_CODE + '-' + elem.NIVEAU_CODE == probleem) {
                        arc.occupied = true;
                        arc.active = false;
                        arc.zorgbesteed = (elem.ZORGBESTEED == 'J');
                        arc.filter_visible = true;
                        Self.activateSegment(arc);
                    }
                    processed = true;
                });
                if (!processed)
                    Self.activateSegment(arc);
            });
        }
    },
    init: function(holder, callback, options) {
        if ($(holder).length == 0) return;

        var Self =  JeugdZorg.cirkelModel;

        $(".profielinfo").insertBefore($(".constimg")).fadeIn('slow');

        if ($('input[name=_hdnIsJeugdigeOfVerzorger]').val() == 'true') {
            var _container = 'pzn_verzorger';
        } else {
            var _container = 'pzn_ingediend';
        }
        $("#btnCloseScreen").click(function() {
            var _url = _serverroot + $(this).attr('href');
            window.location = _url;
        })

        Self.settings = $.extend({}, Self.defaults, options);

        Self.r = Snap(holder);

        segmentPath = function (x, y, r, a1, a2) {
            var flag = (a2 - a1) > 180;
            a1 = (a1 % 360) * Math.PI / 180;
            a2 = (a2 % 360) * Math.PI / 180;
            return {
                path: [["M", x, y], ["l", r * Math.cos(a1), r * Math.sin(a1)], ["A", r, r, 0, +flag, 1, x + r * Math.cos(a2), y + r * Math.sin(a2)], ["z"]],
                fill: '#fff'
            };
        };

        var angle = 30,
            segmentCount = 6,
            total = 180,
            start = 270,
            chbWidth = 10;

        var radius = 165,
            angleCb = 0,
            step = (2*Math.PI) / segmentCount,
            x = 0, y = 0;

        Self.arcs = [];
        for (i = 0; i < segmentCount; i++) {
            var val = 360 / total * angle;
            (function (i, val) {
                for (var si = 0; si < 3; si++) {
                    var segment = Self.r.path(segmentPath(Self.settings.offsetLeft + 180, Self.settings.offsetTop, 150 - (50 * si), start, start + val));
                    segment.attr({stroke: "#ccc"});
                    if ($(holder).hasClass('editable')) {
                        segment
                        .attr({cursor: "pointer"})
                        .click(function () {
                            this.active = !this.active;
                            Self.resetSegments(Self.arcs, this);
                            Self.activateSegment(this);
                            if (callback)
                                callback(this.index, this.indexcodes, this.active);
                        });
                    }

                    segment['index'] = [Self.segmenten[i], Self.lagen[si]];
                    segment['indexcodes'] = [Self.segmenten_codes[i], Self.lagen_codes[si]];
                    segment['active'] = false;
                    Self.arcs.push(segment);

                    if (Self.settings.showRadioButtons) {
                        x = Math.round(radius/2 + radius * Math.cos(angleCb) - chbWidth/2) + 102;
                        y = Math.round(radius/2 + radius * Math.sin(angleCb) - chbWidth/2) - 78;

                        var chb = Self.r.circle(Self.settings.offsetLeft + x, Self.settings.offsetTop + y, chbWidth).attr({fill: '#fff'});
                        chb.attr({cursor: "pointer"});
                        var chbCheck = Self.r.path("M2,8 L5,2 L8,8")
                        chbCheck.attr({fill:"#000",cursor: "pointer"});

                        angleCb += step;
                    }
                }

            })(i, val);
            start += val;
        }

        Self.showCircleText(0, 'Omgeving');
        Self.showCircleText(1, 'Gezin');
        Self.showCircleText(2, 'Kind');

        Self.showSegmentText(0, 'Emotioneel welzijn en\n geestelijke gezondheid');
        Self.showSegmentText(1, 'Relaties en\n netwerk');
        Self.showSegmentText(2, 'Veiligheid');
        Self.showSegmentText(3, 'Materiële\nsituatie');
        Self.showSegmentText(4, 'Scholing en\n ontwikkeling');
        Self.showSegmentText(5, 'Fysieke\n gezondheid');

        Self.initializeLocalization();
    }
}
