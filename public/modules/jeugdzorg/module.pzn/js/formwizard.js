$(document).ready(function() {
    var form = $("#dataform");
    form.validate({
        errorPlacement: function errorPlacement(error, element) { element.before(error); },
        rules: {
        }
    });
    $("#formwizard").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        autoFocus: true,
        labels: {
            cancel: "Annuleren",
            current: "current step:",
            pagination: "Pagination",
            finish: "Einde",
            next: "Volgende",
            previous: "Vorige",
            loading: "Laden..."
        },
        onStepChanging: function (event, currentIndex, newIndex) {
            // Allways allow step back to the previous step even if the current step is not valid!
            if (currentIndex > newIndex) {
                return true;
            }

            form.validate().settings.ignore = ":disabled,:hidden";

            if (form.valid() && currentIndex < 4) {
                JeugdZorg.PZN.bewaarPZN();
            }
            return form.valid();
        },
        onStepChanged: function (event, currentIndex, priorIndex) {
            switch(currentIndex) {
                case 3:
                    JeugdZorg.PZN.refreshData('tzv');
                break;
                case 4:
                    JeugdZorg.PZN.refreshData('gzv');
                break;
            }

            Polaris.Form.setFocusFirstField(this);
        },
        onFinishing: function (event, currentIndex) {
            form.validate().settings.ignore = ":disabled";
            return form.valid();
        },
        onFinished: function (event, currentIndex) {
            $("#_fldWizardFinished").val('true');
            form.submit();
        }
    });

    $("#btnKopieerAdres").click(function() {
        var getFields = ['STRAATNAAM', 'HUISNUMMER', 'TOEVOEGING', 'POSTCODE', 'PLAATSNAAM'];
        getFields.map(function(elem) {
            $("#_fldV_"+elem).val($("#_fldJ_"+elem).val());
        });
        if ($("#_fldV_TELEFOONNUMMER").val() == '')
            $("#_fldV_TELEFOONNUMMER").val($("#_fldJ_TELEFOONNUMMER").val());
    });


});
