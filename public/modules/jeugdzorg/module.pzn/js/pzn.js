JeugdZorg.PZN = {
    htmlOuderVoogd: '',
    htmlCoordinatorWissels: '',
    htmlNetwerkDeelnemer: '',
    htmlZorgplan: '',
    templateOuderVoogd: '',
    templateCoordinatorWissels: '',
    templateNetwerkDeelnemer: '',
    templateVerleendeZorgProbleem: '',
    templateZorgplan: '',
    dataPZN: [],
    dataOuderVoogd: [],
    dataCoordinatorWissels: [],
    dataNetwerkDeelnemer: [],
    dataZorgverleners: [],
    PZNVelden: ['PZN_ID', 'STATUS', 'AANMAAKDATUM', 'UITNODIGINGSDATUM', 'EINDDATUM_BEANTW_UITNODIGING', 'STARTDATUM_OPSTELLEN_ZORGPLAN'
    , 'EINDDATUM_OPSTELLEN_ZORGPLAN'
    , 'PLR__JEUGDIGERECORDID', 'J_GESLACHT@radio', 'J_VOORNAAM', 'J_TUSSENVOEGSEL', 'J_ACHTERNAAM', 'J_STRAATNAAM', 'J_HUISNUMMER', 'J_TOEVOEGING', 'J_POSTCODE', 'J_PLAATSNAAM', 'J_E_MAILADRES', 'J_TELEFOONNUMMER', 'J_ROL', 'J_USERGROUPID', 'J_GEBOORTEDATUM', 'J_BSN'
    , 'RELATIE_TOV_ZORGGERECHTIGDE@select'
    , 'PLR__VERZORGERRECORDID', 'V_GESLACHT@radio', 'V_VOORNAAM', 'V_TUSSENVOEGSEL', 'V_ACHTERNAAM', 'V_STRAATNAAM', 'V_HUISNUMMER', 'V_TOEVOEGING', 'V_POSTCODE', 'V_PLAATSNAAM', 'V_E_MAILADRES', 'V_TELEFOONNUMMER', 'V_ROL', 'V_USERGROUPID'
    , 'PLR__COORDINATORRECORDID', 'C_GESLACHT@radio', 'C_VOORNAAM', 'C_TUSSENVOEGSEL', 'C_ACHTERNAAM', 'C_STRAATNAAM', 'C_HUISNUMMER', 'C_TOEVOEGING', 'C_POSTCODE', 'C_PLAATSNAAM', 'C_E_MAILADRES', 'C_TELEFOONNUMMER', 'C_ROL', 'C_USERGROUPID'
    ],
    TZVVelden: ['VOLGNUMMER', 'ACHTERNAAM', 'TUSSENVOEGSEL', 'RELATIE@select', 'E_MAILADRES', 'VOORNAAM', 'GESLACHT@radio', 'SOORTRELATIE@select'],
    NetwerkDeelnemerVelden: ['SOORTDEELNEMER@radio', 'VOLGNUMMER', 'VOORNAAM', 'TUSSENVOEGSEL', 'ACHTERNAAM', 'TELEFOONNUMMER', 'E_MAILADRES', 'ISCOORDINATOR', 'SOORTZORG@select', 'SOORTZORG', 'UITGENODIGD', 'STATUS', 'DATUM_UITGENODIGD'],
    CoordinatorWijzigingVelden: ['DATUM_WISSEL', 'NIEUWECOORDINATOR', 'STATUS'],

    initialize: function() {
        var Self = JeugdZorg.PZN;

        /****************/
        /* Ouder/voogd */
        /****************/
        Self.htmlOuderVoogd = $('#oudervoogdtabel tbody');
        var OuderVoogdDirectives = {
            'tr': {
                'oudervoogd<-': {
                    'td.id': 'oudervoogd.VOLGNUMMER',
                    'td.naam': function(arg) {
                        return arg.item.VOORNAAM + ' ' + arg.item.ACHTERNAAM;
                    },
                    'td.relatie': 'oudervoogd.SOORTRELATIE',
                    'td.emailadres': 'oudervoogd.E_MAILADRES',
                    '@onclick': function(arg) {
                        return 'JeugdZorg.PZN.selecteerRegel(this);';
                    },
                    '@ondblclick': function(arg) {
                        return 'JeugdZorg.PZN.wijzigOuderVoogd(' + arg.pos + ');';
                    },
                    '@class': 'focus'
                }
            }
        };
        if (Self.htmlOuderVoogd.length > 0) Self.templateOuderVoogd = Self.htmlOuderVoogd.compile(OuderVoogdDirectives);

        $("#btnVoegtoeItemOuderVoogd").click(Self.voegToeOuderVoogd);
        $("#frmTZV .btnSave").click(Self.bewaarOuderVoogd);

        $("#_fldRELATIE_TOV_ZORGGERECHTIGDE").change(function() {
            var notJeugdige = $(this).val() !== 'Jeugdige';
            $(".zorgvrager_extrablock").toggle(notJeugdige);
            $("#_fldV_VOORNAAM,#_fldV_ACHTERNAAM").toggleClass('appear_required', notJeugdige);
        }).change();

        /*********************/
        /* Netwerk deelnemer */
        /*********************/
        Self.htmlNetwerkDeelnemer = $('#netwerkdeelnemer tbody');
        var isND = $("input[name=_hdnIsNetwerkdeelenemer]").val() == 'true';
        var isCD = $("input[name=_hdnIsCoordinator]").val() == 'true';
        var actiefVolgnummer = $("input[name=_hdnActiefVolgnummer]").val();
        var NetwerkDeelnemerDirectives = {
            'tr': {
                'zorgverlener<-': {
                    'td.id': 'zorgverlener.VOLGNUMMER',
                    'td.soort': function(arg) {
                        return arg.item.SOORTDEELNEMER + (arg.item.ISCOORDINATOR == 'J' ? ' &#10045;':'') ;
                    },
                    'td.naam': function(arg) {
                        return arg.item.VOORNAAM + ' ' + arg.item.ACHTERNAAM;
                    },
                    'td.soortzorg': 'zorgverlener.SOORTZORG',
                    'td.uitgenodigd': function(arg) {
                        var res = '';
                        log(arg.item.STATUS);
                        switch(arg.item.STATUS) {
                            case 'VERSTUURD':
                            case 'AANGEMELD':
                                res = res + ' <i class="fa fa-check text-info" title="Uitgenodigd op '+arg.item.DATUM_UITGENODIGD+'"></i>';
                                if (arg.item.STATUS == 'AANGEMELD')
                                    res = res + '<i class="fa fa-check text-info" title="Status: '+arg.item.STATUS+'"></i>';
                                break;
                            default:
                                res = res + 'Nog niet';
                        }
                        return (res);
                    },
                    'td.statusdatum': 'zorgverlener.DATUM_UITGENODIGD',
                    'td.ingestuurd': 'zorgverlener.CIRKELMODELAFGEROND',
                    '@onclick': function(arg) {
                        return 'JeugdZorg.PZN.selecteerRegel(this);';
                    },
                    '@ondblclick': function(arg) {
                        return 'JeugdZorg.PZN.wijzigNetwerkDeelnemer(' + arg.pos + ');';
                    },
                    '@class': 'focus'
                },
                filter:function(arg){
                  return ( arg.item.SOORTDEELNEMER !== 'Jeugdige' && arg.item.SOORTDEELNEMER !== 'Verzorger' );
                }
            }
        };

        if (Self.htmlNetwerkDeelnemer.length > 0) Self.templateNetwerkDeelnemer = Self.htmlNetwerkDeelnemer.compile(NetwerkDeelnemerDirectives);

        //$("#btnVoegtoeCoordinator").click(Self.voegToeGeregisCoordinator);
        $("#btnVoegtoeZorgverlener").click(Self.voegToeNetwerkDeelnemer);
        $("#frmND .btnSave").click(Self.bewaarNetwerkDeelnemer);
        $("#btnPZNIndienen").click(Self.PZNIndienen);

        $("#btnConfirmDeelnemersUitnodigen").click(Self.ConfirmDeelnemersUitnodigen);

        /************************************/
        /* Coordinator wissels */
        /************************************/
        Self.htmlCoordinatorWissels = $('#coordinatorwijzigingen tbody');
        var CoordinatorWisselsDirectives = {
            'tr': {
                'cw<-': {
                    'td.datum_wissel': 'cw.DATUM_WISSEL',
                    'td.coordinator': 'cw.COORDINATOR',
                    'td.status': function(arg) {
                        var actiefVolgnummer = $("input[name=_hdnActiefVolgnummer]").val();
                        log(actiefVolgnummer);
                        log(arg.item.NIEUWECOORDINATOR);
                        return (arg.item.STATUS == 'VERSTUURD' && actiefVolgnummer == arg.item.NIEUWECOORDINATOR) ? '<button onclick="JeugdZorg.PZN.accepteerCoordinatorWissel(event)" type="button" class="btn btn-default cw_accept">Accepteer</button>' : (arg.item.STATUS || '');
                    },
                    '@class': 'focus'
                }
            }
        };
        if (Self.htmlCoordinatorWissels.length > 0) Self.templateCoordinatorWissels = Self.htmlCoordinatorWissels.compile(CoordinatorWisselsDirectives);

        $("#btnBeantwoordUitnodigingCoordinator").click(function(e) {
            e.preventDefault();
            Self.ConfirmGeregisZorgverlener(Self.huidigeRijIndex());
        });
        $("#btnUitnodigenDeelnemers").click(Self.deelnemersUitnodigen);

        $("#frmConfirmGZV .bntSave").click(Self.OpslaanBeslissingGeregisZorgverlener)

        if ($("#_fldWizardFinished").length == 0) {
            $("#btnSaveForm").click(Self.bewaarPZN);
            //$("#dataform").submit(Self.bewaarPZN);
            //$("#btnSaveForm").off('click');
        } else {
            $("#dataform").submit(Self.bewaarPZN);
            $("#btnSaveForm").off('click');
        }

        //$("#btnSaveForm").click(Self.bewaarPZN);

        $("#btnBekijkZorgprofielND").click(Self.bekijkZorgprofiel);

        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });

        $(".chatsession").click(function(e) {
            e.preventDefault();
            var url = _serverroot + '/app/pzn/const/tafelronde/?pznid=' + $(this).data('pzn');
            window.location = url;
        });

        Self.initZoekZorgVerlener();

        $("#btnWijzigPZNGegevens").click(Self.wijzigGegevens);

        $(".btnNDOpnieuwVersturen").click(Self.deelnemerOpnieuwUitnodigen);
        $("#btnWijzigCoordinator").click(Self.wijzigCoordinator);
        $("#btnWijzigCoordinatorSave").click(Self.bewaarCoordinatorWissel);

        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            $('.footer .buttons').show();
            $(".rto-entryfield,.oogmonitor-entryfield").hide();
            switch($(e.target).attr("href")) {
                case '#frm_coordinator':
                    $('.footer .buttons').hide();
                    Self.vulCoordinatorWissels();
                    break;
                case '#frm_netwerkd':
                    $('.footer .buttons').hide();
                    Self.refreshData('gzv');
                    break;
                case '#frm_ouder':
                    $('.footer .buttons').hide();
                    Self.refreshData('tzv');
                    break;
                case '#frm_rondetafel':
                    $('.footer .buttons').hide();
                    $(".rto-entryfield").show().find("textarea").focus();
                    $(window).resize();
                    JeugdZorg.Chatsessie.scrollToBottom();
                    break;
                case '#frm_oogmonitor':
                    $('.footer .buttons').hide();
                    $(".oogmonitor-entryfield").show().find("textarea").focus();
                    $(window).resize();
                    JeugdZorg.Chatsessie.scrollToBottom();
                    break;
            }
        });


        $("#btnRefreshItemListND").click(function(e) { Self.refreshData('gzv')} );
        $("#btnRefreshItemListVGD").click(function(e) { Self.refreshData('tzv')} );

        $("#fldSoortFunctie").change(Self.initZoekZorgVerlener);

        $("#btnAccepteerUitnodiging").click(Self.uitnodigingAccepteren);
        $(".accepteerUitnodiging").click(Self.uitnodigingAccepterenOverzicht);

        $(".oog-resultaat button, .oog-uitvoering button").click(function() {
            log($(this));
            log($(this).siblings());
            $(this).addClass("active").siblings().removeClass("active");
        });
        if (jQuery().twbsToggleButtons)
            $(".btn-group-toggle").twbsToggleButtons();

        if ($.fn.bootstrapWizard) {
            $('#rootwizard').bootstrapWizard({onNext: function(tab, navigation, index) {
                if(index==2) {
                    // Make sure we entered the name
                    if(!$('#name').val()) {
                        alert('You must enter your name');
                        $('#name').focus();
                        return false;
                    }
                }

                // Set the name for the next tab
                $('#tab3').html('Hello, ' + $('#name').val());

            }, onTabShow: function(tab, navigation, index) {
                var $total = navigation.find('li').length;
                var $current = index+1;
                var $percent = ($current/$total) * 100;
                $('#rootwizard .progress-bar').css({width:$percent+'%'});
            }});
        }
    },
    initZoekZorgVerlener: function() {
        var Self = JeugdZorg.PZN;

        if ($.fn.typeahead) {

            Self.dataZorgverleners = [];
            $("#zorgVerlenerID").find('option').remove();
            $.getJSON(_servicequery, {
                'func': 'pzn_persoon',
                'soort': $("#fldSoortFunctie").val()
            }, function(data, textStatus) {
                if (textStatus == 'success') {
                    $.map(data, function(elem, i) {
                        Self.dataZorgverleners.push({"id": elem.VOLGNUMMER, "name": elem.VOORNAAM + ' ' + elem.ACHTERNAAM + '  |  ' + (elem.SOORT_ZORG||'') + '  |  ' + (elem.PLAATSNAAM||'')});
                    });
                    $("#zorgVerlenerID").addOptions(Self.dataZorgverleners, 'id','name');
                }
            });

        }
    },
    initMention: function(deelnemers, ouders) {
        var tmpall = deelnemers.concat(ouders);
        var personen = [];
        $.map(tmpall, function(val, i) {
            var username = val.VOORNAAM + '' + val.ACHTERNAAM;
            username = username.replace(/ /g, '');
            personen.push({'id':val.VOLGNUMMER
            , 'name':val.VOORNAAM + ' ' + val.ACHTERNAAM
            , 'username':username}); //val.E_MAILADRES.replace("@", "_")
        });
        if ($.fn.atwho) {
            log('here');
            $('#txtBerichtRTO').atwho({
                at: "@",
                insertTpl: "@${username}",
                data: personen,
                limit: 99
            });
        }
    },
    huidigeRij: function(type) {
        var Self = JeugdZorg.PZN;

        type = type || 'focusrow';
        var tn = Self.huidigeItemsTabelNaam();
        return $('#'+tn+' tbody tr.'+type);
    },
    huidigeRijIndex: function(type) {
        var Self = JeugdZorg.PZN;

        type = type || 'focusrow';
        var tn = Self.huidigeItemsTabelNaam();
        var huidigeRij = Self.huidigeRij();
        var pos = 0;

        if (!isUndefined(huidigeRij)) {
            pos = $('#'+tn+' tbody tr').index(huidigeRij);
        }
        return pos;
    },
    huidigeRijVolgnummer: function(type) {
        var Self = JeugdZorg.PZN;

        type = type || 'focusrow';
        var tn = Self.huidigeItemsTabelNaam();
        var huidigeRij = Self.huidigeRij();
        var pos = 0;

        if (!isUndefined(huidigeRij)) {
            pos = parseInt($(".id", huidigeRij).text());
        }
        return pos;
    },
    huidigObject: function() {
        var Self = JeugdZorg.PZN;

        var data = Self.huidigeItemsDataObject();
        if (data) {
            var theItem;
            var rijVolgnummer = Self.huidigeRijVolgnummer();
            $.each(data, function(i, item) {
                if (item.VOLGNUMMER == rijVolgnummer) {
                    theItem = item;
                    return false;
                }
            });
            return theItem;
        } else {
            return null;
        }
    },
    huidigePageTab: function() {
        return $(".pagetabcontainer li.active a").prop('id');
    },
    huidigeItemsDataObject: function() {
        var Self = JeugdZorg.PZN;

        var data;
        var pageTab = Self.huidigePageTab();
        if (pageTab == 'tab_frm_netwerkd') {
            data = Self.dataNetwerkDeelnemer;
        // } else if (pageTab == 'tab_frm_overzicht') {
        //     data = Self.dataZorgplan
        } else if (pageTab == 'tab_frm_ouder') {
            data = Self.dataOuderVoogd;
        }
        return data;
    },
    huidigeItemsTabelNaam: function() {
        var Self = JeugdZorg.PZN;

        var tn;
        var pageTab = Self.huidigePageTab();
        if (pageTab == 'tab_frm_netwerkd') {
            tn = 'netwerkdeelnemer';
        // } else if (pageTab == 'tab_frm_overzicht') {
        //     tn = 'zorgplantabel';
        } else if (pageTab == 'tab_frm_ouder') {
            tn = 'oudervoogdtabel';
        } else if (pageTab == 'tab_probleemspec') {
            tn = 'probleemspecificatie';
        }
        return tn;
    },
    selecteerRegel: function(row) {
        var Self = JeugdZorg.PZN;

        if (typeof row == 'number') {
            var tn = Self.huidigeItemsTabelNaam();
            var trs = $('#'+tn+' tbody tr');
            $(trs.get(row)).toggleClass('focusrow', true);
        } else {
            var trs = $(row).parent().find('tr');
            trs.removeClass('focusrow');
            $(row).toggleClass('focusrow', true);
        }

        Self.updateButtonsAndTabs();
    },
    verwijderOuderItem: function(row, volgnummer) {
        var Self = JeugdZorg.PZN;
        JeugdZorg._verwijderItem(row, Self.dataPZN.PLR__RECORDID, volgnummer, 'verwijderOuder');
    },
    verwijderNetwerkDeelnemer: function(row, volgnummer) {
        var Self = JeugdZorg.PZN;

        JeugdZorg._verwijderItem(row, Self.dataPZN.PLR__RECORDID, volgnummer, 'verwijderNetwerkDeelnemer');
    },
    reactiveerNetwerkDeelnemer: function(row, volgnummer) {
        var Self = JeugdZorg.PZN;
        var senddata = [];

        senddata.push({name: '_hdnFORMTYPE', value: 'reactiveerDeelnemer'});
        senddata.push({name: '_hdnProcessedByModule', value: 'true'});
        senddata.push({name: '_hdnState', value: 'edit'});
        senddata.push({name: 'pzn', value: Self.dataPZN.PLR__RECORDID});
        senddata.push({name: 'volgnummer', value: volgnummer});

        var naam = $(row).parent().parent().parent().find('td.naam').text();
        Polaris.Ajax.postJSON(_servicequery, senddata, null, naam +' kan het cirkelmodel weer aanpassen.', function (data) {
            $(row).parent().parent().parent().find('td.ingestuurd').text('');
        });
        return $(row).parents('tbody').find('tr').index($(row).parents('tr'));
    },
    vulOudersVoogden: function() {
        var Self = JeugdZorg.PZN;

        if (Self.templateOuderVoogd !== '') {
            Self.htmlOuderVoogd = Self.htmlOuderVoogd.render(Self.dataOuderVoogd, Self.templateOuderVoogd);
            $("#oudervoogdtabel .verwijderItem").click(function(e) {
                e.preventDefault();
                var item = $(this).parents('tr').find('td.id').text();
                var naam = $(this).parents('tr').find('td.naam').text();
                if (confirm('Weet u zeker dat u '+ naam +' wilt verwijderen?')) {
                    Self.verwijderOuderItem(this, item);
                };
            });
        }
    },
    wijzigOuderVoogd: function(rij) {
        var Self = JeugdZorg.PZN;

        var data = Self.dataOuderVoogd[rij];
        var form = $("#frmTZV");
        form.find("input[name=_hdnState]").val('edit');
        form.find("input[name=_hdnRecordID]").val(data.PLR__RECORDID);
        form.find("input[name=_hdnPersoonID]").val(data.PLR__PERSOONRECORDID);
        Self.updateWijzigButtons();

        JeugdZorg.setFields(Self.TZVVelden, data, form);
        $("#dlgTZV").modal('show');
    },
    voegToeOuderVoogd: function(e) {
        var Self = JeugdZorg.PZN;

        e.preventDefault();
        var form = $("#frmTZV");
        form.find("input[name=_hdnState]").val('insert');
        JeugdZorg.setFields(Self.TZVVelden, [], form);
        $("#btnTZVSave").text('Voeg toe');
        $("#dlgTZV").modal('show');
    },
    bewaarOuderVoogd: function() {
        var Self = JeugdZorg.PZN;

        if (Self.gegevensInOrde()) {
            var senddata = [];

            senddata.push({name: '_hdnDatabase', value: $("input[name=_hdnDatabase]").val()});
            senddata.push({name: '_hdnFORMTYPE', value: 'pzn_tzv'});
            senddata.push({name: '_hdnProcessedByModule', value: 'true'});
            senddata.push({name: '_hdnRecordID', value: Polaris.Base.customUrlEncode(Self.dataPZN.PLR__RECORDID)});
            senddata.push({name: 'PZN', value: $("#_fldRecordID").val()});
            $($("#frmTZV").serializeArray()).each(function(i, obj) {
                senddata.push({name: obj.name, value: obj.value});
            });
            log(senddata);
            Polaris.Ajax.postJSON(_servicequery, senddata, null, 'Gegevens opgeslagen.', function () {
                $("#dlgTZV").modal('hide');
                Self.refreshData('tzv');
            });
        } else {
            $("#dlgTZV").modal('hide');
        }
    },
    getCoordinatorWissels: function(PZNID, callback) {
        var Self = JeugdZorg.PZN;

        $.getJSON(_servicequery, {
            'func': 'coordinatorwissels',
            'pznid': PZNID
        }, function(data, textStatus) {
            /* Leg data vast als lokale/globale var */
            Self.dataCoordinatorWissels = data;
            if (textStatus == 'success') {
                if (callback)
                    callback(data);
            } else {
                Polaris.Base.modalMessage(textStatus);
            }
        });
    },
    vulCoordinatorWissels: function() {
        var Self = JeugdZorg.PZN;

        Self.getCoordinatorWissels($("#_fldRecordID").val(), function(data) {
            $(".coordinatorwijzigingen").toggle(data.length > 0);
            if (Self.templateCoordinatorWissels !== '')
                Self.htmlCoordinatorWissels = Self.htmlCoordinatorWissels.render(Self.dataCoordinatorWissels, Self.templateCoordinatorWissels);
        });
    },
    vulNetwerkDeelnemers: function() {
        var Self = JeugdZorg.PZN;

        if (Self.templateNetwerkDeelnemer !== '') {
            Self.htmlNetwerkDeelnemer = Self.htmlNetwerkDeelnemer.render(Self.dataNetwerkDeelnemer, Self.templateNetwerkDeelnemer);
            //jdenticon.update("#frm_netwerkd canvas");
        }
        $("#netwerkdeelnemer .verwijderItem").click(function(e) {
            e.preventDefault();
            $(this).parents('tr').click();
            var item = Self.huidigObject();
            if (item.ISCOORDINATOR == 'N') {
                var naam = $(this).parents('tr').find('td.naam').text();
                if (confirm('Weet u zeker dat u '+ naam +' wilt verwijderen?')) {
                    Self.verwijderNetwerkDeelnemer(this, item.VOLGNUMMER);
                };
            } else {
                Polaris.Base.modalMessage('U kunt de coordinator niet verwijderen.<br> Als u een andere coordinator wilt uitnodigen, dan kun u dit doen op het Coordinator tabblad.', 3000, 'error');
            }
        });
        $("#netwerkdeelnemer .reactiveerDeelnemer").click(function(e) {
            e.preventDefault();
            $(this).parents('tr').click();
            var item = Self.huidigObject();
            var naam = $(this).parents('tr').find('td.naam').text();
            if (item.CIRKELMODELAFGEROND) {
                if (confirm('Weet u zeker dat '+ naam +' zijn of haar cirkelmodel weer mag wijzigen?')) {
                    Self.reactiveerNetwerkDeelnemer(this, item.VOLGNUMMER);
                }
            } else {
                Polaris.Base.modalMessage('Deze persoon kan zijn of haar cirkelmodel al aanpassen.', 3000, 'error');
            }
        });
    },
    _voegToeNetwerkDeelnemer: function(titel, iscoordinator) {
        var Self = JeugdZorg.PZN;

        var form = $("#frmND");
        $("#dlgND .modal-title").text(titel);
        form.find("input[name=_hdnState]").val('insert');
        $("#zoekZorgverlener").val('');
        $("#soortdeelnemergroup").show();
        $(".nd_uitnod_opnieuw").hide();
        JeugdZorg.setFields(Self.NetwerkDeelnemerVelden, [], form);
        form.find("label:first").button('toggle').tab('show');
        $("#dlgND .btnSave").text('Opslaan');

        $('#dlgND')
        .on('shown.bs.modal', function() {
            $(this).find('input[tabindex!=-1]:visible:first').focus();
        })
        .modal('show');
    },
    voegToeNetwerkDeelnemer: function(e) {
        var Self = JeugdZorg.PZN;

        e.preventDefault();
        Self._voegToeNetwerkDeelnemer('Netwerkdeelnemer', false);
    },
    accepteerCoordinatorWissel: function(e) {
        var Self = JeugdZorg.PZN;

        e.preventDefault();

        var senddata = [];
        var huidigObject = Self.huidigObject();

        senddata.push({name: '_hdnDatabase', value: $("input[name=_hdnDatabase]").val()});
        senddata.push({name: '_hdnFORMTYPE', value: 'accepteercoordinatorwissel'});
        senddata.push({name: '_hdnProcessedByModule', value: 'true'});
        senddata.push({name: '_hdnRecordID', value: $("input[name=_hdnRecordID]").val()});

        Polaris.Ajax.postJSON(_servicequery, senddata, null, 'Uw acceptatie is vastgelegd. U bent nu de coordinator van dit PZN.', function(data) {
            Self.refreshData();
            Self.vulCoordinatorWissels();
        });
    },
    wijzigNetwerkDeelnemer: function(rij) {
        var Self = JeugdZorg.PZN;

        var data = Self.dataNetwerkDeelnemer[rij];

        if (data.ISCOORDINATOR == 'N') {
            var form = $("#frmND");

            form.find("input[name=_hdnState]").val('edit');
            form.find("input[name=_hdnRecordID]").val(data.PLR__RECORDID);
            form.find("input[name=_hdnPersoonID]").val(data.PLR__PERSOONRECORDID);

            $("#soortdeelnemergroup").hide();
            $(".nd_uitnod_opnieuw").show();
            Self.updateWijzigButtons();
            JeugdZorg.setFields(Self.NetwerkDeelnemerVelden, data, form);
            $("#dlgND").modal('show');
            $("#dlgND .modal-title").text(form.find("input[name=SOORTDEELNEMER]:checked").val());
        } else {
            Polaris.Base.modalDialog('U kunt de coordinator gegevens niet wijzigen. Klik op het tabblad \'Coordinator\' om een andere coordinator te kiezen.');
        }
    },
    deelnemerOpnieuwUitnodigen: function(e) {
        var Self = JeugdZorg.PZN;

        var senddata = [];

        var huidigObject = Self.huidigObject();
        e.preventDefault();
        senddata.push({name: '_hdnDatabase', value: $("input[name=_hdnDatabase]").val()});
        senddata.push({name: '_hdnFORMTYPE', value: 'deelnemeropnieuwuitnodigen'});
        senddata.push({name: '_hdnProcessedByModule', value: 'true'});
        senddata.push({name: '_hdnRecordID', value: $("input[name=_hdnRecordID]").val()});
        senddata.push({name: '_hdnDeelnemerRecordID', value: huidigObject.PLR__RECORDID});
        senddata.push({name: 'PZN', value: $("input[name=_hdnRecordID]").val()});

        Polaris.Ajax.postJSON(_servicequery, senddata, null, 'Deelnemer is opnieuw uitgenodigd.', function(data) {
            log(data);
        });
    },
    deelnemersUitnodigen: function(e) {
        e.preventDefault();

        var iedereenUitgenodigd = true;
        $.each(JeugdZorg.PZN.dataNetwerkDeelnemer, function(i, item) {
            if (item.UITGENODIGD == 'N') {
                iedereenUitgenodigd = false;
                return false;
            }
        });
        if (iedereenUitgenodigd) {
            Polaris.Base.modalMessage('Alle deelnemers zijn al uitgenodigd.');
        } else {
            $("#dlgDeelnemersUitnodigen").modal('show');
            $("#fldCMDeadline").val('');
            $("#fldToelichting").val('').focus();
        }
    },
    deelnemersUitnodigenInOrde: function() {
        var now = new Date();
        $("#fldCMDeadline").datepicker('update')
        if ($("#fldCMDeadline").datepicker('getDate') < now) {
            Polaris.Base.modalMessage('De datum moet in de toekomst liggen.');
            return false;
        }
        return true;
    },
    ConfirmDeelnemersUitnodigen: function() {
        var Self = JeugdZorg.PZN;

        var senddata = [];

        if (Self.deelnemersUitnodigenInOrde()) {
            senddata.push({name: '_hdnDatabase', value: $("input[name=_hdnDatabase]").val()});
            senddata.push({name: '_hdnFORMTYPE', value: 'deelnemersuitnodigen'});
            senddata.push({name: '_hdnProcessedByModule', value: 'true'});
            senddata.push({name: 'PZN', value: $("#_fldRecordID").val()});
            senddata.push({name: 'BERICHT', value: $("#fldToelichting").val()});
            senddata.push({name: 'CMDEADLINE', value: $("#fldCMDeadline").val()});
            Polaris.Ajax.postJSON(_servicequery, senddata, null, null, function(data) {
                $("#dlgDeelnemersUitnodigen").modal('hide');
                Self.refreshData('gzv');
                Polaris.Base.modalMessage(data.result + ' deelnemers zijn uitgenodigd.', null, (data.result == 0)?'warning':'success');
            });
        }
    },
    ConfirmGeregisZorgverlener: function(rij) {
        var Self = JeugdZorg.PZN;

        var data = Self.dataNetwerkDeelnemer[rij];
        if (data.ISCOORDINATOR == 'J') {
            var form = $("#frmConfirmGZVCoordinator");
        } else {
            var form = $("#frmConfirmGZV");
        }

        form.find("input[name=_hdnState]").val('edit');
        form.find("input[name=_hdnRecordID]").val(data.PLR__RECORDID);
        if (data.ISCOORDINATOR == 'J') {
            form.find("input[name=EMAILADRES]").addClass('required');
            JeugdZorg.setFields(Self.OuderVoogdVelden, data, form);
            $("#dlgConfirmGZVCoordinator").modal('show');
        } else {
            form.find("input[name=EMAILADRES]").removeClass('required');
            form.find("textarea[name=TOELICHTING]").prop('readonly', false);
            JeugdZorg.setFields(Self.OuderVoogdVelden, data, form);
            $("#dlgConfirmGZV").modal('show');
        }
    },
    OpslaanBeslissingGeregisZorgverlener: function() {
        var Self = JeugdZorg.PZN;

        if (Self.gegevensInOrde()) {
            var senddata = [];

            senddata.push({name: '_hdnDatabase', value: $("input[name=_hdnDatabase]").val()});
            senddata.push({name: '_hdnFORMTYPE', value: 'pzn_gzv'});
            senddata.push({name: '_hdnProcessedByModule', value: 'true'});
            senddata.push({name: '_hdnRecordID', value: Polaris.Base.customUrlEncode(Self.dataPZN.PLR__RECORDID)});
            senddata.push({name: 'PZN', value: $("#_fldRecordID").val()});
            $($("#frmConfirmGZV").serializeArray()).each(function(i, obj) {
                senddata.push({name: obj.name, value: obj.value});
            });

            Polaris.Ajax.postJSON(_servicequery, senddata, null, 'Gegevens opgeslagen.', function () {
                $("#dlgConfirmGZV").modal('hide');
                Self.refreshData('gzv');
            });
        } else {
            $("#dlgND").modal('hide');
        }
    },
    _uitnodigingAccepteren: function(recordID, callback) {
        var Self = JeugdZorg.PZN;

        var senddata = [];
        senddata.push({name: '_hdnDatabase', value: $("input[name=_hdnDatabase]").val()});
        senddata.push({name: '_hdnFORMTYPE', value: 'uitnodigingaccepteren'});
        senddata.push({name: '_hdnProcessedByModule', value: 'true'});
        senddata.push({name: '_hdnRecordID', value: recordID});
        Polaris.Ajax.postJSON(_servicequery, senddata, null, null, function (data) {
            if (data.result > 0) {
                Polaris.Base.modalMessage('Uitnodiging geaccepteerd.');
                if (callback) callback();
            } else {
                Polaris.Base.modalMessage('De uitnodiging is al geaccepteerd.');
            }
        });
    },
    uitnodigingAccepteren: function(e) {
        var Self = JeugdZorg.PZN;

        e.preventDefault();
        Self._uitnodigingAccepteren(Polaris.Base.customUrlEncode(Self.dataPZN.PLR__RECORDID), function(){
            $("#btnBekijkZorgprofielND").show();
            $("#btnAccepteerUitnodiging").prop('disabled', true);
            Self.refreshData('gzv');
        });
    },
    uitnodigingAccepterenOverzicht: function(e) {
        var Self = JeugdZorg.PZN;

        var elem = $(e.target);
        e.stopPropagation();
        var recordid = elem.data('recordid');
        Self._uitnodigingAccepteren(recordid, function(){
            elem.prop({'disabled':'disabled'});
        });
    },
    updateButtonsAndTabs: function() {
        var Self = JeugdZorg.PZN;

        var huidigObject = Self.huidigObject();

        if (Self.statusIsInitieel()) {
            $("#btnBekijkZorgprofielND,#btnBeantwoordUitnodigingCoordinator,#btnUitnodigenDeelnemers").hide();
            //$("#tab_frm_rondetafel").parent().hide();
            $("#tab_frm_oogmonitor").parent().hide();
        } else {
            if (Self.statusIsIngediend()) {
                $("#btnUitnodigenDeelnemers").show();
                //$("#tab_frm_rondetafel").parent().hide();
                $("#tab_frm_oogmonitor").parent().hide();
            }
            if (huidigObject !== null) {
                $("#btnBekijkZorgprofielND,#btnBeantwoordUitnodigingCoordinator,#btnUitnodigenDeelnemers").show();
                $("#btnBeantwoordUitnodigingCoordinator").prop('disabled', true);
                if (isUndefined(huidigObject.ISCOORDINATOR)) {
                    $("#btnBeantwoordUitnodigingCoordinator").prop('disabled', true);
                } else {
                    if (huidigObject.ISCOORDINATOR == 'J') {
                        $("#btnBeantwoordUitnodigingCoordinator").prop('disabled', false);
                    } else {
                        $("#btnBeantwoordUitnodigingCoordinator").prop('disabled', true);
                    }
                }
            }
        }
    },
    wijzigGegevens: function(e) {
        var Self = JeugdZorg.PZN;

        e.preventDefault();
        cfp = 286;
        Self.updateWijzigButtons();
        Self.toggleBasisFormulier();
    },
    updateWijzigButtons: function() {
        var Self = JeugdZorg.PZN;

        if (cfp == 2) {
            $(".modal .btnSave").text('Sluiten');
        } else {
            $(".modal .btnSave").text('Wijzig');
        }
    },
    wijzigCoordinator: function(e) {
        var Self = JeugdZorg.PZN;

        e.preventDefault();

        if (isUndefined(Self.dataPZN['UITNODIGINGSDATUM'])) {
            Polaris.Base.modalMessage('U kunt een andere coordinator kiezen nadat u de netwerkdeelnemers hebt uitgenodigd.', 3000, 'error');
            return;
        }

        if ($.fn.typeahead) {
            $.get('/services/json/app/pzn/form/pzn_persoon/?qc[]=ROL&qv[]=ZORGVERLENER', function(data){
                Self.dataZorgverleners = [];
                $.map(data, function(elem, i) {
                    Self.dataZorgverleners.push({"id": elem.VOLGNUMMER, "name": elem.VOORNAAM + ' ' + elem.ACHTERNAAM + ' | ' + (elem.PLAATSNAAM || '')});
                });
                $("#zoekCoordinator").typeahead({
                    source: Self.dataZorgverleners
                    , updater: function(item) {
                        $("#coordinatorID").val(item.id);
                        return item.name;
                    }
                });
                $("#dlgWijzigCoordinator").modal("show");
                $("#zoekCoordinator").focus();
            },'json');
        }
    },
    bewaarCoordinatorWissel: function() {
        var Self = JeugdZorg.PZN;

        if ($("#coordinatorID").val() != '') {
            var senddata = [];

            senddata.push({name: '_hdnDatabase', value: $("input[name=_hdnDatabase]").val()});
            senddata.push({name: '_hdnFORMTYPE', value: 'pzn_coordinatorwissel'});
            senddata.push({name: '_hdnProcessedByModule', value: 'true'});
            senddata.push({name: '_hdnRecordID', value: Polaris.Base.customUrlEncode(Self.dataPZN.PLR__RECORDID)});
            senddata.push({name: 'PZN', value: $("#_fldRecordID").val()});
            senddata.push({name: 'COORDINATOR_VOLGNUMMER', value: $(":input[name=COORDINATOR_VOLGNUMMER]").val()});
            senddata.push({name: 'BERICHT', value: $(":input[name=BERICHT]").val()});

            Polaris.Ajax.postJSON(_servicequery, senddata, null, 'De nieuwe coordinator wordt uitgenodigd.', function () {
                $("#dlgWijzigCoordinator").modal('hide');
                Self.vulCoordinatorWissels();
            });
        } else {
            //
        }
    },
    bewaarNetwerkDeelnemer: function() {
        var Self = JeugdZorg.PZN;

        if (Self.gegevensNetwerkDeelnemerInOrde()) {
            var senddata = [];

            senddata.push({name: '_hdnDatabase', value: $("input[name=_hdnDatabase]").val()});
            senddata.push({name: '_hdnFORMTYPE', value: 'pzn_gzv'});
            senddata.push({name: '_hdnProcessedByModule', value: 'true'});
            senddata.push({name: '_hdnRecordID', value: Polaris.Base.customUrlEncode($("#frmND input[name=_hdnPersoonID]").val())});
            senddata.push({name: '_hdnState', value: $("#frmND input[name=_hdnState]").val()});
            senddata.push({name: 'PZN', value: $("#_fldRecordID").val()});
            senddata.push({name: 'SOORTDEELNEMER', value: $("#frmND input[name=SOORTDEELNEMER]:checked").val()});
            senddata.push({name: 'ZORGVERLENER_VOLGNUMMER', value: $("#frmND input[name=ZORGVERLENER_VOLGNUMMER]").val()});
            senddata.push({name: 'ISCOORDINATOR', value: $("#frmND input[name=ISCOORDINATOR]").val()});
            senddata = senddata.concat($("#frmND .tab-content .active :input").serializeArray());
            Polaris.Ajax.postJSON(_servicequery, senddata, null, 'Gegevens opgeslagen.', function () {
                $("#dlgND").modal('hide');
                Self.refreshData('gzv');
            });
        }
    },
    PZNIndienen: function(e) {
        var Self = JeugdZorg.PZN;

        e.preventDefault();

        if (Self.gegevensKlaarOmInTeDienen()) {
            Self.bewaarPZN(e);
            var senddata = [];
            senddata.push({name: '_hdnDatabase', value: $("input[name=_hdnDatabase]").val()});
            senddata.push({name: '_hdnFORMTYPE', value: 'pzn_indienen'});
            senddata.push({name: '_hdnProcessedByModule', value: 'true'});
            senddata.push({name: '_hdnRecordID', value: Polaris.Base.customUrlEncode(Self.dataPZN.PLR__RECORDID)});
            senddata.push({name: 'PZN', value: $("#_fldRecordID").val()});

            Polaris.Ajax.postJSON(_servicequery, senddata, null, 'PZN ingediend.', function () {
                $("#_fldSTATUS").val('INGEDIEND').parent().find('h2').text("INGEDIEND");
                $("#btnPZNIndienen").hide();
                //$("#tab_frm_overzicht").parent().show();
            });
        }
    },
    statusIsInitieel: function() {
        return ($("#_fldSTATUS").val() == 'INITIEEL');
    },
    statusIsIngediend: function() {
        return ($("#_fldSTATUS").val() == 'INGEDIEND');
    },
    statusIsDoorgezet: function() {
        return ($("#_fldSTATUS").val() == 'DOORGEZET');
    },
    statusIsAangehouden: function() {
        return ($("#_fldSTATUS").val() == 'AANGEHOUDEN');
    },
    toggleBasisFormulier: function() {
        var Self = JeugdZorg.PZN;

        if (cfp == 2) {
            Self.disableForm();
            $("#btnVoegtoeItemOuderVoogd").hide();
        } else {
            Self.enableForm();
            $("#btnVoegtoeItemOuderVoogd").show();
        }
    },
    _toggleForm: function(onoff) {
        $("#dataform :input:not(:button):not(.always-on)").prop('disabled', (onoff)?'disabled':'');
    },
    disableForm: function() {
        JeugdZorg.PZN._toggleForm(true);
    },
    enableForm: function() {
        JeugdZorg.PZN._toggleForm(false);
    },
    vulBasisGegevens: function() {
        var Self = JeugdZorg.PZN;

        if (Self.dataPZN) {
            Self.toggleBasisFormulier();
            JeugdZorg.setFields(Self.PZNVelden, Self.dataPZN, $("#dataform"));
            if (Self.dataPZN.RELATIE_TOV_ZORGGERECHTIGDE == 'Jeugdige') {
                $("#_fldV_E_MAILADRES").val(Self.dataPZN.J_E_MAILADRES);
                $("#_fldV_TELEFOONNUMMER").val(Self.dataPZN.J_TELEFOONNUMMER);
            }

            $("#_fldRecordID").val(Polaris.Base.customUrlEncode(Self.dataPZN.PLR__RECORDID));
            $("#_fldCOMPLETENAAM").val(Self.dataPZN.C_VOORNAAM +' '+ Self.dataPZN.C_ACHTERNAAM);
            $("#_fldRELATIE_TOV_ZORGGERECHTIGDE").change();
            //Self.vulProfielRegels();
            //JeugdZorg.cirkelModel.updateData(Self.dataProfielItems);
        }
        Self.updateButtonsAndTabs();
    },
    _getGegevens: function(PZNID, callback) {
        var Self = JeugdZorg.PZN;
        $("#btnRefreshItemListND i,#btnRefreshItemListVGD i").addClass('fa-pulse');

        $.getJSON(_servicequery, {
            'func': 'pzn',
            'pznid': PZNID
        }, function(data, textStatus) {
            /* Leg data vast als lokale/globale var */
            Self.dataPZN = data.PZN[0];
            Self.dataOuderVoogd = data.ouderVoogd;
            Self.dataNetwerkDeelnemer = data.netwerkDeelnemers;

            Self.initMention(Self.dataNetwerkDeelnemer, Self.dataOuderVoogd);

            $("#btnRefreshItemListND i,#btnRefreshItemListVGD i").removeClass('fa-pulse');

            if (textStatus == 'success') {
                if (callback)
                    callback();
            } else {
                Polaris.Base.modalMessage(textStatus);
            }
        });
    },
    gegevensKlaarOmInTeDienen: function() {
        var Self = JeugdZorg.PZN;

        var empty = $("#formview :input.required,#formview2 :input.appear_required").filter(function() {
            return this.value === "";
        });
        if (empty.length) {
            toastr.warning('Voordat u kunt indienen, moet u alle verplichte (gele) velden invullen.');
            return false;
        }
        if (Self.dataNetwerkDeelnemer.length == 0) {
            toastr.warning('Voordat u kunt indienen, moet u minstens één netwerkdeelnemer toevoegen.');
            return false;
        }
        // if ($(":input[name=_hdnIsCoordinator]").val() == 'true') {
        //     toastr.warning('Alleen jeugdige of verzorgers kunnen een PZN bij u indienen.');
        //     return false;
        // }

        if (!confirm('Heeft u alle gewenste deelnemers toegevoegd?')) {
            return false;
        };
        return true;
    },
    gegevensNetwerkDeelnemerInOrde: function() {
        var data = $("#frmND .tab-content .active :input.required").serializeArray();
        for(var key in data) {
                log(data[key]);
            if (data.hasOwnProperty(key)) {
                log(data[key]);
                // skip loop if the property is from prototype
                if (data[key].value == '') {
                    Polaris.Base.modalMessage('U dient alle verplichte velden in te vullen');
                    return false;
                }
            }
        };
        return true;
    },
    gegevensInOrde: function() {
        return (cfp !== 2);
    },
    dirname: function(path) {
        return path.replace(/\\/g, '/').replace(/\/[^\/]*\/?$/, '');
    },
    bewaarPZN: function(e) {
        var Self = JeugdZorg.PZN;

        if (!isUndefined(e))
            e.preventDefault();
        if (Self.gegevensInOrde()) {
            var senddata = [];

            senddata.push({name: '_hdnFORMTYPE', value: 'pzn'});
            senddata.push({name: '_hdnProcessedByModule', value: 'true'});
            senddata.push({name: '_hdnState', value: 'edit'});
            $($("#dataform").serializeArray()).each(function(i, obj) {
                senddata.push({name: obj.name, value: obj.value});
            });

            Polaris.Ajax.postJSON(_servicequery, senddata, null, 'Gegevens opgeslagen.', function (data) {
            });
        }
    },
    refreshData: function(soort) {
        var Self = JeugdZorg.PZN;
        if (soort == 'tzv') {
            Self._getGegevens($("#_fldRecordID").val(), Self.vulOudersVoogden);
        } else if (soort == 'gzv') {
            Self._getGegevens($("#_fldRecordID").val(), Self.vulNetwerkDeelnemers);
        } else if (soort == 'zorgbesteed') {
            var rijIndex = Self.huidigeRijIndex();
            Self.getZorgBesteed(rijIndex, function() {
                Self.selecteerRegel(rijIndex);
            });
        } else {
            Self._getGegevens($("#_fldRecordID").val(), Self.vulBasisGegevens);
        }
    },
    bekijkZorgprofiel: function(e) {
        var Self = JeugdZorg.PZN;

        e.preventDefault();

        var data = Self.dataNetwerkDeelnemer;
        var pznid = $("#_fldRecordID").val();
        var versie = $("#_fldVersie").val();
        var volgnummer  = $("input[name=_hdnActiefVolgnummer]").val();

        var item = data[Self.huidigeRijIndex()];
        if (!isUndefined(item)) {
            volgnummer = item.VOLGNUMMER;
        }

        var _url = _serverroot + "/app/pzn/const/cirkelmodel/"+pznid+"/"+versie+"/?_hdnPlrSes=" + sessionStorage.plrses + '&zorgverlener=' + volgnummer;
        _url = _url + "&_back=" + encodeURIComponent(window.location.pathname)
        if (e.target.id == "btnBekijkZorgprofielNDSameWindow")
//            window.open(_url+"&newwindow=true");
            window.location = _url;  //Probleem met back (history... nog fixen)
        else if ((e.target.id == "btnBekijkZorgprofielNDNewWindow"))
            window.open(_url+"&newwindow=true");
    },
    accepteerUitnodigingModal: function(elem) {
        Polaris.Base.modalDialog('Wilt u de uitnodiging accepteren?', {
            yes: function(dialogItself) {
                Self.accepteerUitnodiging(true);
                dialogItself.close();
            },
            no: function(dialogItself) {
                Self.accepteerUitnodiging(false);
                dialogItself.close();
            }
        });
    }
};

$(document).ready(function() {
    var Self = JeugdZorg.PZN;

    Self.initialize();
    Self.refreshData();

    $('#probleemlijst').on('hide.bs.dropdown', function () {
        return false;
    });
    $('#btnProbleemLijst,document').on('click', function () {
        $('.probleemlijst').toggle();
    });
    $('window').click(function () {
        $('.probleemlijst').hide();
    });

    Polaris.Form.initializeDatePickers();

    if (JeugdZorg.Tour)
        JeugdZorg.Tour.initialize();
});
