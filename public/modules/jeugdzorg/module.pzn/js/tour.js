JeugdZorg.Tour = {
    tourInstance: null,
    initialize: function() {
        //initialize instance
        this.tourInstance = new EnjoyHint({
            "nextButton" : {text: "Volgende"},
            "skipButton" : {text: "Overslaan"}
        });

        var enjoyhint_script_steps = [
        {
            'next #profielblock' : 'Momenteel bekijk je het dossier van deze jeugdige.<br/> Het PZN nummer is de unieke code van dit dossier.',
            'event':'click',
            'shape': 'rectangle',
            'showSkip': false,
            "nextButton" : {text: "Volgende"}
        },
        {
            'next .pagetabcontainer' : 'Alle informatie van dit dossier is te vinden onder verschillende tabbladen',
            'showSkip': false,
            "nextButton" : {text: "Volgende"}
        },
        {
            'next #form1_page1' : 'De gegevens kun je hier aanvullen.',
            'showSkip': false,
            "nextButton" : {text: "Volgende"}
        },
        {
            'next .status-velden' : 'Hier zie je wat de status is van dit PZN en wanneer de deelnemers iets moeten invullen.',
            'showSkip': false,
            "nextButton" : {text: "Volgende"}
        },
        {
            'next #btnPZNIndienen' : 'Als je alle informatie hebt ingevuld en de zorgverleners hebt uitgekozen,<br/> dan kun je het PZN indienen bij de coördinator.',
            'showSkip': false,
            "nextButton" : {text: "Volgende"}
        },
        {
            'next #btnBekijkZorgprofielND' : 'Via het cirkelmodel ga je aangeven wat het zorgplan is.',
            'showSkip': false,
            "nextButton" : {text: "Volgende"}
        },
        {
            'next #btnSaveForm' : '...en vergeet niet op Opslaan te drukken wanneer je klaar bent.',
            'showSkip': false,
            "nextButton" : {text: "Volgende"}
        }];

        //set script config
        this.tourInstance.set(enjoyhint_script_steps);

        $(document).on('tour:help-start', function() {
            JeugdZorg.Tour.run();
        });
    },
    run: function() {
        //run Enjoyhint script
        this.tourInstance.run();
    }
}
