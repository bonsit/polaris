<?php
include_once("../../../../languages/lang_api.php");
$f = explode('.', basename(__file__));
$_language_directory = 'modules'.DIRECTORY_SEPARATOR.'jeugdzorg'.DIRECTORY_SEPARATOR.'module.pzn';
lang_load_mod($f[0], dirname(dirname(__FILE__)));
require("_generic_.json.php");
