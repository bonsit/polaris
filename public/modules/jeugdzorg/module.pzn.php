<?php
require_once '_shared/module._base.php';
require_once 'jeugdzorg/module.pzn/php/misc.php';

CONST SUPERUSERGROUP = 100;

class ModulePZN extends _BaseModule {

    function __construct($moduleid, $module, $_clientname) {
        parent::__construct($moduleid, $module, $_clientname);

        $this->processed = false;
        $this->showDefaultButtons = false;

        $this->moduleStatus = array(1=>'INITIEEL', 4=>'INGEDIEND', 5=>'RTOGESTART', 6=>'HETOOG');

        /**
        * What client are we talking about? (nog doen: eventueel selecteerbaar maken)
        */
        $this->clientid = $_SESSION['clientid'];
    }

    function ProcessRequest($_allesRecord) {
        if (isset($_allesRecord['pzn']))
            $_allesRecord['pzn'] = $this->GetPZNFromEncrypted($_allesRecord['pzn']);
        if (isset($_allesRecord['PZN']))
            $_allesRecord['PZN'] = $this->GetPZNFromEncrypted($_allesRecord['PZN']);
        switch($_allesRecord['_hdnFORMTYPE']) {
        case 'pzn':
            $result = $this->VerwerkPZN($_allesRecord);
            break;
        case 'pzn_tzv':
            $result = $this->VerwerkPZNOuderVoogd($_allesRecord);
            break;
        case 'pzn_gzv':
            $result = $this->VerwerkPZNNetwerkDeelnemer($_allesRecord);
            break;
        case 'pzn_profiel':
            $result = $this->voegToeProfielItem($_allesRecord);
            break;
        // case 'pzn_verleendezorg':
        //     $result = $this->voegToeVerleendeZorg($_allesRecord);
        //     break;
        case 'verwijderOuder':
            $result = $this->verwijderOuder($_allesRecord);
            break;
        case 'reactiveerDeelnemer':
            $result = $this->reactiveerDeelnemer($_allesRecord);
            break;
        case 'verwijderNetwerkDeelnemer':
            $result = $this->verwijderNetwerkDeelnemer($_allesRecord);
            break;
        case 'deelnemersuitnodigen':
            $result = $this->deelnemersUitnodigen($_allesRecord);
            break;
        case 'deelnemeropnieuwuitnodigen':
            $result = $this->deelnemerOpnieuwUitnodigen($_allesRecord);
            break;
        case 'verwijderNGZV':
            $result = $this->verwijderNGZV($_allesRecord);
            break;
        case 'verwijderProfiel':
            $result = $this->verwijderProfiel($_allesRecord);
            break;
        case 'verwijderZorgBesteed':
            $result = $this->verwijderZorgBesteed($_allesRecord);
            break;
        case 'updateZorgBesteed':
            $result = $this->updateZorgBesteed($_allesRecord);
            break;
        case 'bewaarbericht':
            $result = $this->BewaarBericht($_allesRecord);
            break;
        case 'pzn_uitnodiging':
            $result = $this->BewaarUitnodiging($_allesRecord);
            break;
        case 'pzn_stuuruitnodigingopnieuw':
            $result = $this->StuurUitnodigingOpnieuw($_allesRecord);
            break;
        case 'pzn_indienen':
            $result = $this->PZNIndienen($_allesRecord);
            break;
        case 'pzn_coordinatorwissel':
            $result = $this->CoordinatorWisselen($_allesRecord);
            break;
        case 'accepteercoordinatorwissel':
            $result = $this->AccepteerCoordinatorWissel($_allesRecord);
            break;
        case 'uitnodigingaccepteren':
            $result = $this->AccepteerUitnodiging($_allesRecord);
            break;
        case 'bewaardeZorg':
            $result = $this->BewaarZorg($_allesRecord);
            break;
        case 'pzn_rtostarten':
            $result = $this->RTOStarten($_allesRecord);
            break;
        case 'pzn_rtoeinde':
            $result = $this->RTOEinde($_allesRecord);
            break;
        case 'bewaar_persoon':
            $result = $this->BewaarPersoon($_allesRecord);
            break;
        case 'pzn_cmklaar':
            $result = $this->CirkelModelKlaar($_allesRecord);
            break;
        case 'bewaar_aandachtsgebied':
            $result = $this->BewaarAandachtsgebied($_allesRecord);
            break;
        case 'startNieuweVersie':
            $result = $this->StartNieuweVersie($_allesRecord);
            break;
        case 'verwijderVersie':
            $result = $this->VerwijderVersie($_allesRecord);
            break;
        case 'pauzeerRTO':
            $result = $this->PauzeerRTO($_allesRecord);
            break;
        }

        return $result;
    }

    function verwijderOuder($_allesRecord) {
        $_result = false;
        try {
            if (isset($_allesRecord['pzn']) && isset($_allesRecord['volgnummer'])) {
                $_sql = 'DELETE FROM PZN_OUDERVOOGD WHERE PZN = ? AND VOLGNUMMER = ?';
                $this->form->database->userdb->Execute($_sql, array($_allesRecord['pzn'], $_allesRecord['volgnummer']));
                $_result = true;
            }
        } catch (ADODB_Exception $E) {
            $msg = 'Deze zorgvrager kan niet verwijderd worden. ('.$E->msg.')';
            adodb_plr_throw($E->dbms, $E->fn, $E->getCode(), $E->msg, $E->p1, $E->p2, $this->database, $msg);
        }

        return $_result;
    }

    function reactiveerDeelnemer($_allesRecord) {
        global $polaris;

        $_result = false;
        try {
            if (isset($_allesRecord['pzn']) && isset($_allesRecord['volgnummer'])) {
                $_sql = "UPDATE PZN_NETWERKDEELNEMER SET CIRKELMODELAFGEROND = NULL WHERE PZN = ? AND VOLGNUMMER = ?";
                $this->form->database->userdb->Execute($_sql, array($_allesRecord['pzn'], $_allesRecord['volgnummer']));
                $_result = $this->form->database->userdb->Affected_Rows();

                return $_result;
            }
        } catch (ADODB_Exception $E) {
            $msg = 'Het cirkelmodel kan niet geactiveerd worden. ('.$E->msg.')';
            adodb_plr_throw($E->dbms, $E->fn, $E->getCode(), $E->msg, $E->p1, $E->p2, $this->database, $msg);
        }

        return $_result;
    }

    function verwijderNetwerkDeelnemer($_allesRecord) {
        $_result = false;
        try {
            if (isset($_allesRecord['pzn']) && isset($_allesRecord['volgnummer'])) {
                $_sql = 'DELETE FROM PZN_NETWERKDEELNEMER WHERE PZN = ? AND VOLGNUMMER = ? AND ISCOORDINATOR <> "J"';
                $this->form->database->userdb->Execute($_sql, array($_allesRecord['pzn'], $_allesRecord['volgnummer']));
                $_result = true;
            }
        } catch (ADODB_Exception $E) {
            $msg = 'Deze zorgverlener kan niet verwijderd worden. ('.$E->msg.')';
            adodb_plr_throw($E->dbms, $E->fn, $E->getCode(), $E->msg, $E->p1, $E->p2, $this->database, $msg);
        }

        return $_result;
    }

    function deelnemersUitnodigen($_allesRecord) {
        global $polaris;

        $_result = false;
        try {
            if (isset($_allesRecord['PZN'])) {
                $_pzn = $_allesRecord['PZN'];
                $_deadlineDate = date('y-m-d', strtotime($_allesRecord['CMDEADLINE']));
                $_sql = "UPDATE PERSOONLIJKZORGNETWERK2 SET UITNODIGINGSDATUM = NOW(), STARTDATUM_OPSTELLEN_ZORGPLAN = NOW(), EINDDATUM_OPSTELLEN_ZORGPLAN = ? WHERE PZN_ID = ? AND UITNODIGINGSDATUM IS NULL"; //
                $this->form->database->userdb->Execute($_sql, array($_deadlineDate, $_pzn));

                $_sql = "UPDATE PZN_NETWERKDEELNEMER SET UITGENODIGD = 'J' WHERE PZN = ? AND UITGENODIGD <> 'J'"; //
                $this->form->database->userdb->Execute($_sql, array($_pzn));

                $_recordSelector = $this->form->database->makeRecordIDColumn('PZN_NETWERKDEELNEMER');

                $_personen = $this->form->database->userdb->GetRow("SELECT J_VOORNAAM, J_ACHTERNAAM, C_USERGROUPID, C_VOORNAAM, C_ACHTERNAAM, IFNULL(V_VOORNAAM, J_VOORNAAM) AS V_VOORNAAM, IFNULL(V_ACHTERNAAM, J_ACHTERNAAM) AS V_ACHTERNAAM FROM vw_compleet WHERE PZN_ID = ?", array($_pzn));

                $_sql = "SELECT PZN, SOORTDEELNEMER, VOLGNUMMER, GESLACHT, VOORNAAM
                , ACHTERNAAM, E_MAILADRES, CLIENTID, USERGROUPID, $_recordSelector AS PLR__RECORDID
                FROM vw_netwerkdeelnemer
                WHERE PZN = ? AND UITGENODIGD = 'J' AND DATUM_UITGENODIGD IS NULL";
                $_rs = $this->form->database->userdb->GetAll($_sql, $_pzn);

                foreach($_rs as $_deelnemer) {
                    if (empty($_deelnemer['USERGROUPID'])) {
                        $this->VerwerkUitnodiging($_deelnemer['E_MAILADRES'], 'Netwerk', $_personen['J_VOORNAAM'], $_personen['J_ACHTERNAAM'], $_personen['C_USERGROUPID'], $_deelnemer['VOORNAAM'], $_deelnemer['ACHTERNAAM'], $_personen['V_VOORNAAM'], $_personen['V_ACHTERNAAM']);
                    }
                    $_deelnemer = array_merge($_deelnemer, $_personen);
                    $_deelnemer['BERICHT'] = $_allesRecord['BERICHT'];
                    $_deelnemer['EINDDATUM_OPSTELLEN_ZORGPLAN'] = $_allesRecord['CMDEADLINE'];
                    $this->QueueDeelnemerUitnodiging($_deelnemer['E_MAILADRES'], $_deelnemer, $_deelnemer['PLR__RECORDID'], $_allesRecord['_hdnRecordID']);
                }

                $_result = count($_rs);
            }
        } catch (ADODB_Exception $E) {
            $msg = 'Er ging iets mis bij het uitnodigen van de deelnemers. ('.$E->msg.')';
            adodb_plr_throw($E->dbms, $E->fn, $E->getCode(), $E->msg, $E->p1, $E->p2, $this->database, $msg);
        }

        return $_result;
    }

    function GetNetwerkDeelnemer($pzn, $volgnummer) {
        global $scramble;

        if ($pzn and $volgnummer) {
            $_rs = $this->GetNetwerkDeelnemerFromHash(MD5($scramble.'_'.$pzn.'_'.$volgnummer));
        } else {
            $_rs = NULL;
        }
        return $_rs;
    }

    function GetNetwerkDeelnemerFromHash($deelnemerhash) {
        global $scramble;

        if ($deelnemerhash) {
            $_sql = "SELECT PZN, SOORTDEELNEMER, VOLGNUMMER, GESLACHT, VOORNAAM
            , ACHTERNAAM, E_MAILADRES, CLIENTID, USERGROUPID, CIRKELMODELAFGEROND
            FROM VW_NETWERKDEELNEMER
            WHERE MD5(CONCAT('$scramble', '_', PZN, '_', VOLGNUMMER)) = ?";
            $_rs = $this->form->database->userdb->GetAll($_sql, array($deelnemerhash));
        } else {
            $_rs = NULL;
        }
        return $_rs;
    }

    function deelnemerOpnieuwUitnodigen($_allesRecord) {
        global $polaris;
        global $scramble;

        $_result = false;
        try {
            if (isset($_allesRecord['PZN'])) {
                $_deelnemerRecordID = $_allesRecord['_hdnDeelnemerRecordID'];
                $_recordID = $_allesRecord['_hdnRecordID'];
                $_where = "MD5(CONCAT('$scramble', '_', PZN, '_', VOLGNUMMER))";
                $_sql = "UPDATE PZN_NETWERKDEELNEMER SET UITGENODIGD = 'J', DATUM_UITGENODIGD = NULL WHERE $_where = ?"; //
                $this->form->database->userdb->Execute($_sql, array($_deelnemerRecordID));

                $_rs = $this->GetNetwerkDeelnemerFromHash($_deelnemerRecordID);

                foreach($_rs as $_deelnemer) {
                    $_personen = $this->form->database->userdb->GetRow("SELECT J_VOORNAAM, J_ACHTERNAAM, C_USERGROUPID, C_VOORNAAM, C_ACHTERNAAM, IFNULL(V_VOORNAAM, J_VOORNAAM) AS V_VOORNAAM, IFNULL(V_ACHTERNAAM, J_ACHTERNAAM) AS V_ACHTERNAAM FROM vw_compleet WHERE PZN_ID = ?", array($_deelnemer['PZN']));

                    if (empty($_deelnemer['USERGROUPID'])) {
                        $this->VerwerkUitnodiging($_deelnemer['E_MAILADRES'], 'Netwerk', $_personen['J_VOORNAAM'], $_personen['J_ACHTERNAAM'], $_personen['C_USERGROUPID'], $_deelnemer['VOORNAAM'], $_deelnemer['ACHTERNAAM'], $_personen['V_VOORNAAM'], $_personen['V_ACHTERNAAM']);
                    }
                    $_deelnemer = array_merge($_deelnemer, $_personen);
                    $this->QueueDeelnemerUitnodiging($_deelnemer['E_MAILADRES'], $_deelnemer, $_deelnemerRecordID, $_recordID);
                }

                $_result = $_allesRecord['PZN'];
            }
        } catch (ADODB_Exception $E) {
            $msg = 'Er ging iets mis bij het uitnodigen van de deelnemers. ('.$E->msg.')';
            adodb_plr_throw($E->dbms, $E->fn, $E->getCode(), $E->msg, $E->p1, $E->p2, $this->database, $msg);
        }

        return $_result;
    }

    function verwijderNGZV($_allesRecord) {
        $_result = false;
        try {
            if (isset($_allesRecord['pzn']) && isset($_allesRecord['volgnummer'])) {
                $_allesRecord['pzn'] = $this->GetPZNFromEncrypted($_allesRecord['pzn']);
                $_sql = 'DELETE FROM PZN_VERTROUWENSPERSOON WHERE PZN = ? AND VOLGNUMMER = ?';
                $this->form->database->userdb->Execute($_sql, array($_allesRecord['pzn'], $_allesRecord['volgnummer']));
                $_result = true;
            }
        } catch (ADODB_Exception $E) {
            $msg = 'Deze niet geregistreerde zorgverlener kan niet verwijderd worden. ('.$E->msg.')';
            adodb_plr_throw($E->dbms, $E->fn, $E->getCode(), $E->msg, $E->p1, $E->p2, $this->database, $msg);
        }

        return $_result;
    }

    function verwijderProfiel($_allesRecord) {
        $_result = false;
        try {
            if (isset($_allesRecord['pzn']) && isset($_allesRecord['volgnummer'])) {
                $_sql = 'DELETE FROM PZN_ZORGBESTEED WHERE PROBLEEM_VOLGNUMMER = ?';
                $this->form->database->userdb->Execute($_sql, array($_allesRecord['volgnummer']));
                $_sql = 'DELETE FROM PZN_PROFIEL WHERE PZN = ? AND VOLGNUMMER = ?';
                $this->form->database->userdb->Execute($_sql, array($_allesRecord['pzn'], $_allesRecord['volgnummer']));
                $_result = true;
            }
            if ($_result === false)
                throw new Exception('PZNID: '.var_export($_allesRecord['pzn'], true));
        } catch (Exception $E) {
            $msg = 'Deze profielregel kan niet verwijderd worden. ('.$E->getMessage().')';
            adodb_plr_throw($E->dbms, $E->fn, $E->getCode(), $E->getMessage(), $E->p1, $E->p2, $this->database, $msg);
        }

        return $_result;
    }

    function verwijderZorgBesteed($_allesRecord) {
        $_result = false;
        try {
            if (isset($_allesRecord['volgnummer'])) {
                $_sql = 'DELETE FROM PZN_ZORGBESTEED WHERE VOLGNUMMER = ?';
                $this->form->database->userdb->Execute($_sql, array($_allesRecord['volgnummer']));
                $_result = true;
            }
        } catch (ADODB_Exception $E) {
            $msg = 'Deze zorg kan niet verwijderd worden. ('.$E->msg.')';
            adodb_plr_throw($E->dbms, $E->fn, $E->getCode(), $E->msg, $E->p1, $E->p2, $this->database, $msg);
        }

        return $_result;
    }

    function updateZorgBesteed($_allesRecord) {
        $_result = false;
        try {
            if (isset($_allesRecord['PZN']) && isset($_allesRecord['volgnummer'])) {
                $_sql = 'UPDATE PZN_PROFIEL SET ZORGBESTEED = ? WHERE PZN = ? AND VOLGNUMMER = ?';
                $this->form->database->userdb->Execute($_sql, array($_allesRecord['zorgbesteed'], $_allesRecord['PZN'], $_allesRecord['volgnummer']));
                $_result = true;
            }
        } catch (ADODB_Exception $E) {
            $msg = 'Deze profielregel kan niet aangepast worden. ('.$E->msg.')';
            adodb_plr_throw($E->dbms, $E->fn, $E->getCode(), $E->msg, $E->p1, $E->p2, $this->database, $msg);
        }

        return $_result;
    }

    function QueuePZNIngediend($email, $values) {
        global $polaris;
        global $_GVARS;

        $values['FORMHASH'] = '3810ce9f1868866a806e0538f9a33579';
        $polaris->AddToQueue('send_pzningediend_mail', array('email' => $email, 'insertValues' => $values));
    }

    function QueueCoordinatorWissel($email, $values, $id) {
        global $polaris;
        global $_GVARS;
        global $scramble;

        $values['FORMHASH'] = 'ae344f8f92ba5e33840c2142046dc0cd'; // PZN_COORDINATORWISSEL
        $values['ID'] = $id;

        $polaris->AddToQueue('send_coordinatorwissel_mail', array('email' => $email, 'insertValues' => $values, 'ID' => $id));
    }

    function QueueUitnodiging($email, $values, $id) {
        global $polaris;
        global $_GVARS;
        global $scramble;

        $_linkhash = md5($id.$scramble);
        $values['ID'] = $id;
        $values['FORMHASH'] = 'e4233030af7fae6e0e965016d6c861e4'; // PZN_UITNODIGING
        $values['LINK'] = $_GVARS['serverroot']."/createaccount/{$values['FORMHASH']}/{$_linkhash}/";

        $polaris->AddToQueue('send_invite_mail', array('email' => $email, 'insertValues' => $values, 'ID' => $id));
    }

    function QueueDeelnemerUitnodiging($email, $values, $deelnemerid, $recordid) {
        global $polaris;
        global $_GVARS;

        $values['FORMHASH'] = '641462623c3309b7e5c8736773439d22';
        $values['ACCEPTLINK'] = $_GVARS['serverroot']."/app/pzn/const/pzn_initieel/edit/{$recordid}/?func=accept&dn=$deelnemerid";
        $polaris->AddToQueue('send_deelnemeruitnodiging', array('email' => $email, 'insertValues' => $values));
    }

    function QueueRTOGestart($email, $values) {
        global $polaris;

        $values['FORMHASH'] = 'e4233030af7fae6e0e965016d6c861e4';

        $polaris->AddToQueue('send_rtogestart_mail', array('email' => $email, 'insertValues' => $values));
    }

    function QueueOogGestart($email, $values) {
        global $polaris;

        $values['FORMHASH'] = '...';

        $polaris->AddToQueue('send_ooggestart_mail', array('email' => $email, 'insertValues' => $values));
    }

    function QueueCirkelModelAfgerond($email, $values) {
        global $polaris;

        $values['FORMHASH'] = '3810ce9f1868866a806e0538f9a33579';
        $polaris->AddToQueue('send_cirkelmodelafgerond_mail', array('email' => $email, 'insertValues' => $values));
    }

    function VerwerkPersoonAccount($pzn, $volgnummer) {
        $_rs = $this->form->database->userdb->GetRow("
            SELECT USERGROUPID, VOLGNUMMER, E_MAILADRES, VOORNAAM, ACHTERNAAM, SOORTRELATIE FROM (
                SELECT USERGROUPID, VOLGNUMMER, E_MAILADRES, VOORNAAM, ACHTERNAAM, SOORTRELATIE
                FROM VW_OUDERVOOGD
                UNION
                SELECT USERGROUPID, VOLGNUMMER, E_MAILADRES, VOORNAAM, ACHTERNAAM, SOORTDEELNEMER
                FROM VW_NETWERKDEELNEMER
            ) V
            WHERE V.VOLGNUMMER = ?", array($volgnummer));

        $_personen = $this->form->database->userdb->GetRow("SELECT J_VOORNAAM, J_ACHTERNAAM, V_VOORNAAM, V_ACHTERNAAM, C_USERGROUPID FROM vw_compleet WHERE PZN_ID = ?", array($pzn));

        if (isset($_rs['E_MAILADRES']) and !isset($_rs['USERGROUPID'])) {
            $this->VerwerkUitnodiging($_rs['E_MAILADRES'], $_rs['SOORTRELATIE'], $_personen['J_VOORNAAM'], $_personen['J_ACHTERNAAM'], $_personen['C_USERGROUPID'], $_rs['VOORNAAM'], $_rs['ACHTERNAAM'], $_personen['V_VOORNAAM'], $_personen['V_ACHTERNAAM']);
        }
    }

    function VerwerkUitnodiging($email, $soortuitnodiging, $voornaam, $achternaam, $uitnodiger
        , $voornaam_verzorger, $achternaam_verzorger, $v_voornaam, $v_achternaam) {
        if (!empty($email) && !empty($achternaam)) {
            $_status = 'UITNODIGEN...';
            $uitnodigerVolgnummer = GetPersoonVolgnummer($this->form->database, $uitnodiger);
            $_sql = 'INSERT INTO PZN_UITNODIGING (`UITGENODIGDE_EMAIL`, `DATUM_UITNODIGING`, `SOORTUITNODIGING`, `VOORNAAM`, `ACHTERNAAM`, `STATUS`, `VOORNAAM_VERZORGER`, `ACHTERNAAM_VERZORGER`, `ZORGVERLENER_VOLGNUMMER`, `ZORGVERLENER_USERID`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
            $this->form->database->userdb->Execute($_sql, array(
                $email,
                date("Y-m-d H:i:s"),
                $soortuitnodiging,
                $voornaam,
                $achternaam,
                $_status,
                $voornaam_verzorger,
                $achternaam_verzorger,
                $uitnodigerVolgnummer,
                $uitnodiger
            ));
            $_lastInsertID = $this->form->database->userdb->Insert_ID();

            $_rs = $this->form->database->userdb->GetRow("SELECT p.VOORNAAM AS C_VOORNAAM, p.ACHTERNAAM AS C_ACHTERNAAM
            FROM PZN_PERSOON p
            WHERE p.VOLGNUMMER = ?", array($uitnodigerVolgnummer));

            $_insertValues = array(
                  'UITGENODIGDE_EMAIL'=>$email
                , 'SOORTUITNODIGING' => $soortuitnodiging
                , 'VOORNAAM' => $voornaam
                , 'ACHTERNAAM' => $achternaam
                , 'VOORNAAM_VERZORGER' => $voornaam_verzorger
                , 'ACHTERNAAM_VERZORGER' => $achternaam_verzorger
                , 'C_VOORNAAM' => $_rs['C_VOORNAAM']
                , 'C_ACHTERNAAM' => $_rs['C_ACHTERNAAM']
                , 'V_VOORNAAM' => $v_voornaam
                , 'V_ACHTERNAAM' => $v_achternaam
            );

            $this->QueueUitnodiging($email, $_insertValues, $_lastInsertID);

            $_result = true;
        }

        return $_result;
    }

    function BewaarUitnodiging($_allesRecord) {
        global $polaris;
        global $scramble;

        $_result = false;
        try {
            $_exists = $polaris->getUserGroupInfo($_allesRecord['UITGENODIGDE_EMAIL']);
            if ($_exists === FALSE) {
                $_result = $this->VerwerkUitnodiging($_allesRecord['UITGENODIGDE_EMAIL'], $_allesRecord['SOORTUITNODIGING'], $_allesRecord['VOORNAAM'], $_allesRecord['ACHTERNAAM'], $_SESSION['userid'], $_allesRecord['VOORNAAM_VERZORGER'], $_allesRecord['ACHTERNAAM_VERZORGER'], '', '');
            } else {
                throw new Exception('Er is al een account met dit emailadres.');
            }
        } catch (ADODB_Exception $E) {
            $msg = 'De uitnodiging kon niet worden bewaard. ('.$E->msg.')';
            adodb_plr_throw($E->dbms, $E->fn, $E->getCode(), $E->msg, $E->p1, $E->p2, $this->database, $msg);
        }

        return $_result;
    }

    function StuurUitnodigingOpnieuw($_allesRecord) {
        global $polaris;

        $_result = false;
        try {
            $_tableName = 'PZN_UITNODIGING';
            $_ID = $_allesRecord['ID'];
            $_record["STATUS"] = "UITGENODIGD...";
            $this->form->database->userdb->AutoExecute($_tableName, $_record, 'UPDATE', 'ID = '.$_ID);
            $_rs = $this->form->database->userdb->GetRow("SELECT u.*, p.VOORNAAM AS C_VOORNAAM, p.ACHTERNAAM AS C_ACHTERNAAM
            FROM PZN_UITNODIGING u LEFT JOIN PZN_PERSOON p
            ON u.ZORGVERLENER_VOLGNUMMER = p.VOLGNUMMER WHERE u.ID = ?", array($_ID));

            $this->QueueUitnodiging($_rs['UITGENODIGDE_EMAIL'], $_rs, $_ID);

            $_result = true;
        } catch (ADODB_Exception $E) {
            $msg = 'De uitnodiging kon niet worden bewaard. ('.$E->msg.')';
            adodb_plr_throw($E->dbms, $E->fn, $E->getCode(), $E->msg, $E->p1, $E->p2, $this->database, $msg);
        }

        return $_result;
    }

    function VerwerkAtWhos($id, $bericht, $datetime) {
        $_sql = "SELECT * FROM pzn_persoon WHERE usergroupid is not null
        AND replace(concat(voornaam, achternaam), ' ', '') = ?";
        $_insert = "INSERT INTO pzn_chatsessie_atwho (VOLGNUMMER, PERSOON, GESCHREVENOP) VALUES (?,?,?)";
        $db = $this->form->database->userdb;
        $stm = $db->Prepare($_sql);
        preg_match_all("/@(\S+)/i", $bericht, $_out, PREG_PATTERN_ORDER);
        foreach($_out[1] as $_key => $_item) {
            $_rs = $db->GetRow($stm, array($_item));
            if ($_rs) {
                $db->Execute($_insert, array($id, $_rs['VOLGNUMMER'], $datetime));
            }
        }
    }

    function VerwerkGelezenChats($pznid_encrypted, $lastitem) {
        $pznid = $this->GetPZNFromEncrypted($pznid_encrypted);
        $_persoonvolgnummer = GetPersoonVolgnummer($this->form->database, $_SESSION['userid']);
        $_sql = "UPDATE pzn_chatsessie_atwho SET `GELEZEN` = 'Y' WHERE VOLGNUMMER IN (SELECT VOLGNUMMER FROM pzn_chatsessie WHERE PZN_ID = ? and VOLGNUMMER <= ?) AND PERSOON = ?";
        $this->form->database->userdb->Execute($_sql, array($pznid, $lastitem, $_persoonvolgnummer));
    }

    function _PauzeerRTO($pznid_encrypted, $pauzeer) {
        $_pznid = $this->GetPZNFromEncrypted($pznid_encrypted);
        $_db = $this->form->database->userdb;
        $_persoonvolgnummer = GetPersoonVolgnummer($this->form->database, $_SESSION['userid']);
        $_datetime = date('Y-m-d H:i:s');

        $_result = null;
        if ($this->IsCoordinator($_pznid, $_persoonvolgnummer)) {
            $_sql = 'UPDATE PERSOONLIJKZORGNETWERK2 SET RTO_STATUS = ? WHERE PZN_ID = ?';
            if ($pauzeer) {
                $_db->Execute($_sql, array('PAUZE', $_pznid));
                $_bericht = '<i class="fa fa-pause"></i> <b>DIT RTO IS GEPAUZEERD voor overleg tussen coördinator en verzorger/jeugdige.</b>';
            } else {
                $_db->Execute($_sql, array('VRIJ', $_pznid));
                $_bericht = '<i class="fa fa-pause"></i> <b>DIT RTO IS GEOPEND VOOR GEBRUIK DOOR ALLE DEELNEMERS.</b>';
            }
            $this->_BewaarBericht($_pznid, $_datetime, $_persoonvolgnummer, $_bericht);
        }
        return $_result;
    }

    function PauzeerRTO($_allesRecord) {
        $_pznid_encrypted = $_allesRecord['_hdnRecordID'];
        $_pauzeer = ($_allesRecord['onoff'] == 'true');
        $_result = $this->_PauzeerRTO($_pznid_encrypted, $_pauzeer);

        return $_result;
    }

    function _BewaarBericht($pznid, $datetime, $persoonvolgnummer, $bericht, $type='RTO', $uitvoering=false, $resultaat=false) {
        $_db = $this->form->database->userdb;
        $_sql = 'INSERT INTO PZN_CHATSESSIE (PZN_ID, AANMAAKDATUM, ZORGVERLENER, BERICHT, MESSAGETYPE, UITVOERING, RESULTAAT) VALUES (?, ?, ?, ?, ?, ?, ?)';
        $_db->Execute($_sql, array($pznid, $datetime, $persoonvolgnummer, $bericht, $type, $uitvoering, $resultaat));
        return $_db->Insert_ID();
    }

    function BewaarBericht($_allesRecord) {
        $_result = false;
        try {
            if (isset($_allesRecord['pzn']) && isset($_allesRecord['bericht'])) {
                $db = $this->form->database->userdb;
                $_persoonvolgnummer = GetPersoonVolgnummer($this->form->database, $_SESSION['userid']);
                $_isencrypted = FALSE;
                $_pzn = $this->GetPZNData($_allesRecord['pzn'], $_isencrypted);
                $_pznid = $_pzn[0]['PZN_ID'];

                if (!empty($_allesRecord['bericht'])) {
                    $_zorgverlener = $_SESSION['fullname'];
                    $_datetime = date('Y-m-d H:i:s');
                    $_id = $this->_BewaarBericht($_pznid, $_datetime, $_persoonvolgnummer, $_allesRecord['bericht'], $_allesRecord['_hdnMESSAGETYPE'], $_allesRecord['uitvoering'], $_allesRecord['resultaat']);

                    $this->VerwerkAtWhos($_id, $_allesRecord['bericht'], $_datetime);
                    $_result = true;
                }
            }
        } catch (ADODB_Exception $E) {
            $msg = 'Dit bericht kon niet vastgelegd worden. ('.$E->msg.')';
            adodb_plr_throw($E->dbms, $E->fn, $E->getCode(), $E->msg, $E->p1, $E->p2, $this->database, $msg);
        }

        return $_result;
    }

    function BewaarCustomAandachtsgebied($domein, $niveau, $probleem) {
        $_sql = "INSERT INTO PROBLEEM (DOMEIN, NIVEAU, PROBLEEM, SOORT) VALUES (?,?,?,?)";
        try {
            $this->form->database->userdb->Execute($_sql, array($domein, $niveau, $probleem, 'USER'));
        } catch (Exception $e) {

        }
    }

    function voegToeProfielItem($_allesRecord) {
        $_result = false;
        if ($this->ZorgverlenerBestaat($_allesRecord['PZN'], $_allesRecord['ZORGVERLENER'])) {
            $_record['_hdnDatabase'] = $_allesRecord['_hdnDatabase'];
            $_record['_hdnTable'] = 'PZN_PROFIEL';
            $_record['_hdnAction'] = 'save';
            $_record['_hdnState'] = $_allesRecord['_hdnState'];
            $_record['ERNST'] = $_allesRecord['ERNST'];
            $_record['PROBLEEM'] = $_allesRecord['PROBLEEM'];
            if ($_allesRecord['PROBLEEM'] == 'Anders...') {
                if (isset($_allesRecord['aandachtsgebiedanders']))
                    $_record['PROBLEEM'] = $_allesRecord['aandachtsgebiedanders'];
            }
            if ($_record['_hdnState'] == 'insert') {
                $_record['PZN'] = $_allesRecord['PZN'];
                $_record['VERSIE'] = $this->GetHuidigeVersie();
                $_record['CODE'] = $_allesRecord['CODE'];
                $s = explode(',', $_allesRecord['segment']);
                $_record['DOMEIN'] = $s[0];
                $_record['NIVEAU'] = $s[1];
                $_record['ERNST'] = $_allesRecord['ERNST'];
                $_record['ZORGBESTEED'] = ''; // ff niks
                $_record['ZORGVERLENER'] = $_allesRecord['ZORGVERLENER'];
                $_result = SaveRecord($_record);
            } else {
                $_sql = 'UPDATE PZN_PROFIEL SET PROBLEEM = ?, ERNST = ? WHERE VOLGNUMMER = ?';
                $this->form->database->userdb->Execute($_sql
                    , array($_record['PROBLEEM'], $_record['ERNST'], $_allesRecord['PROBLEEM_VOLGNUMMER']));
            }

            if ($_allesRecord['PROBLEEM'] == 'Anders...') {
                $this->BewaarCustomAandachtsgebied($_record['DOMEIN'], $_record['NIVEAU'], $_record['PROBLEEM']);
            }
        }

        return $_result;
    }

    function GetProfielProbleem($_pzn, $_volgnummer) {
        $_sql = "SELECT t.PROBLEEM FROM PZN_PROFIEL t WHERE PZN = ? AND VOLGNUMMER = ?";
        $_result = $this->form->database->userdb->GetOne($_sql, array($_pzn, $_volgnummer));
        return $_result;
    }

//     function voegToeVerleendeZorg($_allesRecord) {
//         $_record['_hdnDatabase'] = $_allesRecord['_hdnDatabase'];
//         $_record['_hdnTable'] = 'PZN_ZORGBESTEED';
//         $_record['_hdnAction'] = 'save';
//         $_record['_hdnRecordID'] = $_allesRecord['_hdnRecordID'];
//         $_record['_hdnState'] = 'insert';
//         $_record['PZN'] = $_allesRecord['PZN'];
//         $_record['ZORGVERLENER'] = $_allesRecord['ZORGVERLENER'];
// $polaris->LogQuick($_allesRecord);
//         foreach($_allesRecord['probleem_check'] as $_field=>$_volgnummer) {
//             $_record['DOMEIN'] = $_allesRecord['probleem_domein_'.$_volgnummer][0];
//             $_record['NIVEAU'] = $_allesRecord['probleem_niveau_'.$_volgnummer][0];
//             $_record['EFFECT'] = $_allesRecord['probleem_effect_'.$_volgnummer][0];
//             $_record['ZORG'] = $_allesRecord['zorg'];
//             $_record['frequentie'] = $_allesRecord['frequentie'];
//             if ($_allesRecord['intensiteit'] == 'Nvt') {
//                 $_record['frequentieaantal'] = '';
//             } else {
//                 $_record['frequentieaantal'] = $_allesRecord['frequentieaantal'];
//             }
//             $_record['intensiteit'] = $_allesRecord['intensiteit'];
//             if ($_allesRecord['intensiteit'] == 'Nvt') {
//                 $_record['intensiteitaantal'] = '';
//             } else {
//                 $_record['intensiteitaantal'] = $_allesRecord['intensiteitaantal'];
//             }
//             $_record['sinds'] = $_allesRecord['sinds'];
//             $_record['tot'] = $_allesRecord['tot'];
//             $_record['probleem'] = $this->GetProfielProbleem($_allesRecord['PZN'], $_volgnummer);

//             $_result = SaveRecord($_record);
//         }

//         return $_result;
//     }

    function GetPZNPersonen($recordid) {
        global $scramble;

        $_sql = "SELECT JEUGDIGE_VOLGNUMMER, VERZORGER_VOLGNUMMER, COORDINATOR_VOLGNUMMER
        , MD5(CONCAT('$scramble', '_', JEUGDIGE_VOLGNUMMER)) AS PLR__JEUGDIGERECORDID
        , MD5(CONCAT('$scramble', '_', VERZORGER_VOLGNUMMER)) AS PLR__VERZORGERRECORDID
        , MD5(CONCAT('$scramble', '_', COORDINATOR_VOLGNUMMER)) AS PLR__COORDINATORRECORDID
        FROM VW_COMPLEET WHERE md5(concat('$scramble', '_',PZN_ID)) = ?";
        $_row = $this->form->database->userdb->GetRow($_sql, array($recordid));
        return $_row;
    }

    function VerwerkPZN($_allesRecord) {
        global $polaris;


        $_personen = $this->GetPZNPersonen($_allesRecord["_hdnRecordID"]);

        $_record['_hdnDatabase'] = $_allesRecord['_hdnDatabase'];
        $_record['_hdnProcessedByModule'] = 'true';
        $_record['_hdnTable'] = 'PZN_PERSOON';
        $_record['_hdnState'] = 'edit';
        $_record['_hdnAction'] = 'save';
        $_record['_hdnRecordID'] = $_personen['PLR__JEUGDIGERECORDID'];
        if (empty($_personen['JEUGDIGE_VOLGNUMMER'])) {
            $_record['_hdnState'] = 'insert';
        }
        $_record['BSN'] = $_allesRecord['J_BSN'];
        $_record['VOORNAAM'] = $_allesRecord['J_VOORNAAM'];
        $_record['ACHTERNAAM'] = $_allesRecord['J_ACHTERNAAM'];
        $_record['GESLACHT'] = $_allesRecord['J_GESLACHT'];
        $_record['STRAATNAAM'] = $_allesRecord['J_STRAATNAAM'];
        $_record['HUISNUMMER'] = $_allesRecord['J_HUISNUMMER'];
        $_record['TOEVOEGING'] = $_allesRecord['J_TOEVOEGING'];
        $_record['POSTCODE'] = $_allesRecord['J_POSTCODE'];
        $_record['PLAATSNAAM'] = $_allesRecord['J_PLAATSNAAM'];
        $_record['GEBOORTEDATUM'] = $_allesRecord['J_GEBOORTEDATUM'];
        if ($_allesRecord['RELATIE_TOV_ZORGGERECHTIGDE'] == 'Jeugdige') {
            $_record['TELEFOONNUMMER'] = $_allesRecord['V_TELEFOONNUMMER'];
        }
        $_jeugdigeVolgnummer = SaveRecord($_record);

        if ($_allesRecord['RELATIE_TOV_ZORGGERECHTIGDE'] == 'Verzorger') {
            unset($_record);
            $_record['_hdnDatabase'] = $_allesRecord['_hdnDatabase'];
            $_record['_hdnProcessedByModule'] = 'true';
            $_record['_hdnTable'] = 'PZN_PERSOON';
            $_record['_hdnAction'] = 'save';
            $_record['_hdnRecordID'] = $_personen['PLR__VERZORGERRECORDID'];
            $_record['_hdnState'] = 'edit';
            if (empty($_personen['VERZORGER_VOLGNUMMER'])) {
                $_record['_hdnState'] = 'insert';
            }
            $_record['GESLACHT'] = $_allesRecord['V_GESLACHT'];
            $_record['VOORNAAM'] = $_allesRecord['V_VOORNAAM'];
            $_record['ACHTERNAAM'] = $_allesRecord['V_ACHTERNAAM'];
            $_record['STRAATNAAM'] = $_allesRecord['V_STRAATNAAM'];
            $_record['HUISNUMMER'] = $_allesRecord['V_HUISNUMMER'];
            $_record['TOEVOEGING'] = $_allesRecord['V_TOEVOEGING'];
            $_record['POSTCODE'] = $_allesRecord['V_POSTCODE'];
            $_record['PLAATSNAAM'] = $_allesRecord['V_PLAATSNAAM'];
            $_record['TELEFOONNUMMER'] = $_allesRecord['V_TELEFOONNUMMER'];
            $_verzorgerVolgnummer = SaveRecord($_record);
        }

        unset($_record);
        $_record['_hdnDatabase'] = $_allesRecord['_hdnDatabase'];
        $_record['_hdnProcessedByModule'] = 'true';
        $_record['_hdnTable'] = 'PZN_PERSOON';
        $_record['_hdnAction'] = 'save';
        $_record['_hdnRecordID'] = $_personen['PLR__COORDINATORRECORDID'];
        $_record['_hdnState'] = 'edit';
        if (empty($_personen['COORDINATOR_VOLGNUMMER'])) {
            $_record['_hdnState'] = 'insert';
        }
        $_record['VOORNAAM'] = $_allesRecord['C_VOORNAAM'];
        $_record['ACHTERNAAM'] = $_allesRecord['C_ACHTERNAAM'];
        $_record['STRAATNAAM'] = $_allesRecord['C_STRAATNAAM'];
        $_record['HUISNUMMER'] = $_allesRecord['C_HUISNUMMER'];
        $_record['TOEVOEGING'] = $_allesRecord['C_TOEVOEGING'];
        $_record['POSTCODE'] = $_allesRecord['C_POSTCODE'];
        $_record['PLAATSNAAM'] = $_allesRecord['C_PLAATSNAAM'];
        $_record['TELEFOONNUMMER'] = $_allesRecord['C_TELEFOONNUMMER'];
        $_coordinatorVolgnummer = SaveRecord($_record);

        unset($_record);
        $_record['_hdnDatabase'] = $_allesRecord['_hdnDatabase'];
        $_record['_hdnProcessedByModule'] = 'true';
        $_record['_hdnTable'] = 'PERSOONLIJKZORGNETWERK2';
        $_record['_hdnAction'] = 'save';
        $_record['_hdnRecordID'] = $_allesRecord['_hdnRecordID'];
        $_record['_hdnState'] = 'edit';
        $_record['UITNODIGINGSDATUM'] = $_allesRecord['UITNODIGINGSDATUM'];
        $_record['EINDDATUM_BEANTW_UITNODIGING'] = $_allesRecord['EINDDATUM_BEANTW_UITNODIGING'];
//        $_record['STARTDATUM_OPSTELLEN_ZORGPLAN'] = $_allesRecord['STARTDATUM_OPSTELLEN_ZORGPLAN'];
//        $_record['EINDDATUM_OPSTELLEN_ZORGPLAN'] = $_allesRecord['EINDDATUM_OPSTELLEN_ZORGPLAN'];
        $_record['RELATIE_TOV_ZORGGERECHTIGDE'] = $_allesRecord['RELATIE_TOV_ZORGGERECHTIGDE'];

        if (is_numeric($_jeugdigeVolgnummer))
            $_record['JEUGDIGE_VOLGNUMMER'] = $_jeugdigeVolgnummer;
        if (is_numeric($_verzorgerVolgnummer))
            $_record['VERZORGER_VOLGNUMMER'] = $_verzorgerVolgnummer;
        // else
        //     $_record['VERZORGER_VOLGNUMMER'] = '';
        if (is_numeric($_coordinatorVolgnummer))
            $_record['COORDINATOR_VOLGNUMMER'] = $_coordinatorVolgnummer;
        if ($_allesRecord['WIZARD_FINISHED'] == 'true')
            $_record['WIZARDKLAAR'] = 'Y';
        // $_record['MOTIVATIE_TOELICHTING'] = $_allesRecord['MOTIVATIE_TOELICHTING'];
        // $_record['MOTIVATIE_AANHOUDING'] = $_allesRecord['MOTIVATIE_AANHOUDING'];
        $_result = SaveRecord($_record);

        return $_result;
    }

    function PZNIndienen($_allesRecord) {
        $_record['_hdnDatabase'] = $_allesRecord['_hdnDatabase'];
        $_record['_hdnTable'] = 'PERSOONLIJKZORGNETWERK2';
        $_record['_hdnAction'] = 'save';
        $_record['_hdnRecordID'] = $_allesRecord['_hdnRecordID'];
        $_record['_hdnState'] = 'edit';
        $_record['STATUS'] = $this->moduleStatus[4];
        $_result = SaveRecord($_record);

        if ($_result) {
            $_pzn = $this->form->database->userdb->GetRow("SELECT C_E_MAILADRES, PZN_ID, J_VOORNAAM, J_ACHTERNAAM, C_VOORNAAM, C_ACHTERNAAM
             FROM VW_COMPLEET WHERE PZN_ID = ?", array($_allesRecord['PZN']));
            $this->QueuePZNIngediend($_pzn['C_E_MAILADRES'], $_pzn);
        }

        return $_result;
    }

    function RTOStarten($_allesRecord) {
        $_record['_hdnDatabase'] = $_allesRecord['_hdnDatabase'];
        $_record['_hdnTable'] = 'PERSOONLIJKZORGNETWERK2';
        $_record['_hdnAction'] = 'save';
        $_record['_hdnRecordID'] = $_allesRecord['_hdnRecordID'];
        $_record['_hdnState'] = 'edit';
        $_record['STATUS'] = $this->moduleStatus[5];
        $_record['STARTDATUM_RTO'] = date('Y-m-d H:i:s');
        $_record['EINDDATUM_RTO'] = $_allesRecord['DEADLINE'];

        $_result = SaveRecord($_record);

        if ($_result and ($_allesRecord['NOTIFICEERDEADLINE'] == 'J')) {
            $_pzn = $this->form->database->userdb->GetRow("SELECT J_VOORNAAM, J_ACHTERNAAM, C_VOORNAAM, C_ACHTERNAAM
             FROM VW_COMPLEET WHERE PZN_ID = ?", array($_allesRecord['PZN']));

            $_rs = $this->form->database->userdb->GetAll("SELECT E_MAILADRES, PZN, VOORNAAM, ACHTERNAAM
             FROM VW_NETWERKDEELNEMER WHERE PZN = ?", array($_allesRecord['PZN']));

            foreach($_rs as $_key => $_deelnemer) {
                $_deelnemer = array_merge($_deelnemer, $_pzn);
                $_deelnemer['EINDDATUM_RTO'] = $_record['EINDDATUM_RTO'];
                $this->QueueRTOGestart($_deelnemer['E_MAILADRES'], $_deelnemer);
            }
        }

        return $_result;
    }

    function RTOEinde($_allesRecord) {
        $_record['_hdnDatabase'] = $_allesRecord['_hdnDatabase'];
        $_record['_hdnTable'] = 'PERSOONLIJKZORGNETWERK2';
        $_record['_hdnAction'] = 'save';
        $_record['_hdnRecordID'] = $_allesRecord['_hdnRecordID'];
        $_record['_hdnState'] = 'edit';
        $_record['STATUS'] = $this->moduleStatus[6];
        $_result = SaveRecord($_record);

        // if ($_result and ($_allesRecord['NOTIFICEERDEADLINE'] == 'J')) {
        //     $_pzn = $this->form->database->userdb->GetRow("SELECT J_VOORNAAM, J_ACHTERNAAM, C_VOORNAAM, C_ACHTERNAAM
        //      FROM VW_COMPLEET WHERE PZN_ID = ?", array($_allesRecord['PZN']));

        //     $_rs = $this->form->database->userdb->GetAll("SELECT E_MAILADRES, PZN, VOORNAAM, ACHTERNAAM
        //      FROM VW_NETWERKDEELNEMER WHERE PZN = ?", array($_allesRecord['PZN']));

        //     foreach($_rs as $_key => $_deelnemer) {
        //         $_deelnemer = array_merge($_deelnemer, $_pzn);
        //         $_deelnemer['EINDDATUM_RTO'] = $_record['EINDDATUM_RTO'];
        //         $this->QueueOogGestart($_deelnemer['E_MAILADRES'], $_deelnemer);
        //     }
        // }

        return $_result;
    }

    function AccepteerCoordinatorWissel($_allesRecord) {
        $_nieuweStatus = 'ACCEPT';
        $_recordSelector = $this->form->database->makeRecordIDColumn('PERSOONLIJKZORGNETWERK2');
        $_sql = "SELECT PZN_ID FROM PERSOONLIJKZORGNETWERK2 WHERE $_recordSelector = ?";
        $_pznid = $this->form->database->userdb->GetOne($_sql, array($_allesRecord['_hdnRecordID']));
        $_sql = "UPDATE PZN_COORDINATORWISSEL SET STATUS = '$_nieuweStatus' WHERE STATUS = 'VERSTUURD' AND PZN = ?";
        $_result = $this->form->database->userdb->Execute($_sql, array($_pznid));
        $_sql = "SELECT NIEUWECOORDINATOR, DATUM_WISSEL, p.SOORT_ZORG
        FROM PZN_COORDINATORWISSEL c LEFT JOIN PZN_PERSOON p ON c.`NIEUWECOORDINATOR` = p.`VOLGNUMMER`
        WHERE c.PZN = ? AND c.STATUS = '$_nieuweStatus'
        ORDER BY DATUM_WISSEL DESC";
        $_cw = $this->form->database->userdb->GetRow($_sql, array($_pznid));

        $_sql = "UPDATE PERSOONLIJKZORGNETWERK2 SET COORDINATOR_VOLGNUMMER = ? WHERE PZN_ID = ?";
        $_result = $this->form->database->userdb->Execute($_sql, array($_cw['NIEUWECOORDINATOR'], $_pznid));

        $_sql = "UPDATE PZN_NETWERKDEELNEMER SET ISCOORDINATOR = 'N', SOORTDEELNEMER = 'Zorgverlener' WHERE PZN = ? AND ISCOORDINATOR = 'J'";
        $_result = $this->form->database->userdb->Execute($_sql, array($_pznid));

        $_sql = "UPDATE PZN_NETWERKDEELNEMER SET ISCOORDINATOR = 'J', DATUM_UITGENODIGD = ?, SOORTDEELNEMER = 'Coordinator', SOORTZORG = ? WHERE PZN = ? AND VOLGNUMMER = ?";
        $_result = $this->form->database->userdb->Execute($_sql, array($_cw['DATUM_WISSEL'], $_cw['SOORT_ZORG'], $_pznid, $_cw['NIEUWECOORDINATOR']));

        return true;
    }

    function CoordinatorWisselen($_allesRecord) {
        $_record['_hdnDatabase'] = $_allesRecord['_hdnDatabase'];
        $_record['_hdnTable'] = 'PZN_COORDINATORWISSEL';
        $_record['_hdnAction'] = 'save';
        $_record['_hdnRecordID'] = $_allesRecord['_hdnRecordID'];
        $_record['_hdnState'] = 'insert';
        $_record['PZN'] = $_allesRecord['PZN'];
        $_record['DATUM_WISSEL'] = date('Y-m-d H:i:s');
        $_record['NIEUWECOORDINATOR'] = $_allesRecord['COORDINATOR_VOLGNUMMER'];
        $_record['BERICHT'] = $_allesRecord['BERICHT'];
        $_result = SaveRecord($_record);

        $_sql = "UPDATE PZN_COORDINATORWISSEL SET STATUS = 'AFGEBROKEN' WHERE (STATUS IS NULL OR STATUS = 'VERSTUURD') AND PZN = ? AND VOLGNUMMER < ?";
        $this->form->database->userdb->Execute($_sql, array($_record['PZN'], $_result));

        $_recordSelector = $this->form->database->makeRecordIDColumn('PZN_COORDINATORWISSEL');
        $_sql = "SELECT PZN, VOLGNUMMER, DATUM_WISSEL, NIEUWECOORDINATOR, VOORNAAM, ACHTERNAAM, BERICHT
        , STATUS, E_MAILADRES, CLIENTID, USERGROUPID
        , $_recordSelector AS PLR__RECORDID
        FROM vw_coordinatorwissel WHERE PZN = ? AND VOLGNUMMER = ?";
        $_rs = $this->form->database->userdb->GetRow($_sql, array($_record['PZN'], $_result));

        $this->QueueCoordinatorWissel($_rs['E_MAILADRES'], $_rs, $_result);

        return $_result;
    }

    function AccepteerUitnodiging($_allesRecord) {
        $_persoonvolgnummer = GetPersoonVolgnummer($this->form->database, $_SESSION['userid']);
        $_key = $this->KeyPZNIDSelector('PZN');
        $_sql = "UPDATE PZN_NETWERKDEELNEMER SET STATUS = 'AANGEMELD' WHERE $_key = ? AND VOLGNUMMER = ?";
        $this->form->database->userdb->Execute($_sql, array($_allesRecord['_hdnRecordID'], $_persoonvolgnummer));
        $_result = $this->form->database->userdb->Affected_Rows();
        return $_result;
    }

    function BewaarCustomZorg($domein, $niveau, $zorg) {
        $_sql = "INSERT INTO ZORG (DOMEIN, NIVEAU, ZORG, SOORT) VALUES (?,?,?,?)";
        try {
            $this->form->database->userdb->Execute($_sql, array($domein, $niveau, $zorg, 'USER'));
        } catch (Exception $e) {
        }
    }

    function BewaarZorg($_allesRecord) {
        global $polaris;

        $_record = ExtractTableFields($_allesRecord);
        $_record['_hdnDatabase'] = $_allesRecord['_hdnDatabase'];
        $_record['_hdnTable'] = 'PZN_ZORGBESTEED';
        $_record['_hdnState'] = $_allesRecord['_hdnState'];
        $_record['_hdnRecordID'] = $_allesRecord['_hdnRecordID'];
        unset($_record['ZORGVERLENER']);
        $_record['PZN'] = $this->GetPZNFromEncrypted($_GET['pznid']);
        $_record['VERSIE'] = $this->GetHuidigeVersie();
        if ($_allesRecord['_hdnState'] == 'insert') {
            $_record['ZORGVERLENER'] = $_allesRecord['ZORGVERLENER'];
        }
        if ($_record['FREQUENTIE'] == 'Nvt') {
            $_record['FREQUENTIEAANTAL'] = ' ';
        } else {
            $_record['FREQUENTIEAANTAL'] = $_record['FREQUENTIEAANTAL'];
        }
        if ($_record['INTENSITEIT'] == 'Nvt') {
            $_record['INTENSITEITAANTAL'] = ' ';
        } else {
            $_record['INTENSITEITAANTAL'] = $_record['INTENSITEITAANTAL'];
        }

        if ($_allesRecord['ZORG'] == 'Anders...') {
            if (isset($_allesRecord['zorganders']))
                $_record['ZORG'] = $_allesRecord['zorganders'];
            $this->BewaarCustomZorg($_record['DOMEIN'], $_record['NIVEAU'], $_record['ZORG']);
        }

        unset($_record['zorganders']);
        return SaveRecord($_record);
    }

    function createUserAccount($_allesRecord) {
        global $polaris;

        $_exists = $polaris->getUserGroupInfo($_allesRecord['E_MAILADRES']);
        if ($_exists) {
            $result = $_exists->USERGROUPID;
        } else {
            $_clientHash = $this->form->record->CLIENTHASH;
            $_result = $polaris->addUserAccount(
                $_clientHash,
                $_allesRecord['E_MAILADRES'],
                $_allesRecord['VOORNAAM'].' '.$_allesRecord['ACHTERNAAM'],
                '', // description
                $_allesRecord['E_MAILADRES'],
                $_allesRecord['PASSWORD'],
                'N', // clientadmin
                'N', // confirm account
                $_resultsArray
                , false,
                $_allesRecord['TELEFOONNUMMER']
            );
            $result = $_resultsArray['usergroupid'];
        }
        return $result;
    }

    function PersoonBestaat($_emailadres) {
        $_sql = "SELECT count(*) FROM pzn_persoon WHERE E_MAILADRES = ?";
        $result = $this->form->database->userdb->GetOne($_sql, array($_emailadres));
        return ($result == 1);
    }

    function BewaarPersoon($_allesRecord) {
        global $polaris;

        if (($_allesRecord['_hdnState'] !== 'edit') AND $this->PersoonBestaat($_allesRecord['E_MAILADRES'])) {
            $msg = 'Dit emailadres is al in gebruik bij een andere persoon.';
            adodb_plr_throw($this->database, 'BewaarPersoon', $msg, $msg, null, null, $this->database, $msg);
        } else {
            $_record = ExtractTableFields($_allesRecord);

            if ($_allesRecord['PASSWORD'] !== '' and $_allesRecord['PASSWORD'] !== '__nochange__') {
                if ($_allesRecord['USERGROUPID'] == '') {
                    $_usergroupid = $this->createUserAccount($_allesRecord);
                    $_record['USERGROUPID'] = $_usergroupid;
                } else {
                    $_strengthResult = $polaris->changePassword($_allesRecord['E_MAILADRES'], $_allesRecord['PASSWORD'], true);
                    $polaris->changePasswordNextLogon(strtolower($_allesRecord['E_MAILADRES']), 'Y');
                }
            }

            $_sqlDelete = "DELETE FROM plr_membership WHERE clientid = ? AND usergroupid = ?";
            $polaris->instance->Execute($_sqlDelete, array($this->clientid, $_record['USERGROUPID']));
            BewaarPersoonPermissies($this->clientid, $_record['USERGROUPID'], $_allesRecord['ROL']);

            unset($_record['PASSWORD']);
            $_record['_hdnDatabase'] = $_allesRecord['_hdnDatabase'];
            $_record['_hdnTable'] = 'PZN_PERSOON';
            $_record['_hdnState'] = $_allesRecord['_hdnState'];
            $_record['_hdnRecordID'] = $_allesRecord['_hdnRecordID'];

            $_result = SaveRecord($_record);
        }
        return $_result;
    }

    function CirkelModelKlaar($_allesRecord) {
        global $polaris;

        $_persoonvolgnummer = GetPersoonVolgnummer($this->form->database, $_SESSION['userid']);
        $_sql = "UPDATE PZN_NETWERKDEELNEMER SET CIRKELMODELAFGEROND = NOW() WHERE PZN = ? AND VOLGNUMMER = ?";
        $this->form->database->userdb->Execute($_sql, array($_allesRecord['PZN'], $_persoonvolgnummer));
        $_result = $this->form->database->userdb->Affected_Rows();

        if ($_result > 0 and !$this->IsCoordinator($_allesRecord['PZN'], $_persoonvolgnummer)) {
            $_pzn = $this->form->database->userdb->GetRow("SELECT C_E_MAILADRES, PZN_ID, J_VOORNAAM, J_ACHTERNAAM, C_VOORNAAM, C_ACHTERNAAM
             FROM VW_COMPLEET WHERE PZN_ID = ?", array($_allesRecord['PZN']));
            $_values = $_pzn;
            $_netwerkdeelnemer = $this->form->database->userdb->GetRow("SELECT CIRKELMODELAFGEROND, VOORNAAM AS N_VOORNAAM, ACHTERNAAM AS N_ACHTERNAAM
            FROM VW_NETWERKDEELNEMER WHERE PZN = ? AND VOLGNUMMER = ?"
                , array($_allesRecord['PZN'], $_persoonvolgnummer));
            $_values = array_merge($_values, $_netwerkdeelnemer);
            $_email = $_pzn['C_E_MAILADRES'];
            $this->QueueCirkelModelAfgerond($_email, $_values);
        }

        return $_result;
    }

    function BewaarAandachtsgebied($_allesRecord) {
        global $polaris;

        $s = explode(',', $_allesRecord['segment']);
        $_record['DOMEIN'] = $s[0];
        $_record['NIVEAU'] = $s[1];

        try {
            $_sqlInsert = 'INSERT INTO PROBLEEM (domein, niveau, probleem) VALUES (?, ?, ?)';
            $this->form->database->userdb->Execute($_sqlInsert, array($_record['DOMEIN'], $_record['NIVEAU'], $_allesRecord['PROBLEEM']));
        } catch (ADODB_Exception $E) {

        }
    }

    function StartNieuweVersie($_allesRecord) {
        $_db = $this->form->database->userdb;
        $_pznid_encrypted = $_allesRecord['_hdnRecordID'];
        $_versies = $this->GetProfielVersies($_pznid_encrypted);
        $_keyVersies = array_keys($_versies);
        $_laatsteversie = end($_keyVersies);
        $_nieuweversie = $_laatsteversie + 1;
        $_keySelector = $this->KeyPZNIDSelector('PZN');
        $_omschrijving = substr($_allesRecord['OMSCHRIJVING'], 0, 50);

        $_eindVersieSQL = "UPDATE pzn_versie SET einddatum = now() WHERE $_keySelector = ? AND EINDDATUM IS NULL AND VERSIE = ?";
        $_db->Execute($_eindVersieSQL, array($_pznid_encrypted, $_laatsteversie));

        $_sql = "INSERT INTO pzn_versie (`PZN`, `VERSIE`, `OMSCHRIJVING`)
        SELECT `PZN`, ?, ?
        FROM pzn_versie
        WHERE $_keySelector = ?
        AND VERSIE = ?";

        try {
            $_db->Execute($_sql, array($_nieuweversie, $_omschrijving, $_pznid_encrypted, $_laatsteversie));
        } catch (Exception $e) {
            $_sqlUpdateVersie = "UPDATE pzn_versie SET DELETED = 'N', EINDDATUM = NULL, OMSCHRIJVING = ? WHERE $_keySelector = ? AND VERSIE = ?";
            $_db->Execute($_sqlUpdateVersie, array($_omschrijving, $_pznid_encrypted, $_nieuweversie));

            $_sqlDeleteVorigeVersie = "DELETE FROM pzn_profiel WHERE $_keySelector = ? AND VERSIE = ?";
            $_db->Execute($_sqlDeleteVorigeVersie, array($_pznid_encrypted, $_nieuweversie));
        }

        $_sql = "SELECT `VOLGNUMMER`, `PZN`, ? AS `VERSIE`, `CODE`, `DOMEIN`, `NIVEAU`, `PROBLEEM`, `ERNST`, `ZORGBESTEED`, `ZORGVERLENER`
        FROM pzn_profiel
        WHERE $_keySelector = ?
        AND VERSIE = ?";
        $_rs = $_db->GetAll($_sql, array($_nieuweversie, $_pznid_encrypted, $_laatsteversie));

        $_insertProfielSQL = "INSERT INTO pzn_profiel (`PZN`, `VERSIE`, `CODE`, `DOMEIN`, `NIVEAU`, `PROBLEEM`, `ERNST`, `ZORGBESTEED`, `ZORGVERLENER`) VALUES (?,?,?,?,?,?,?,?,?)";
        $_insertProfiel = $_db->Prepare($_insertProfielSQL);

        $_insertZorgSQL = "INSERT INTO pzn_zorgbesteed (`ZORG`, `DOMEIN`, `NIVEAU`, `PROBLEEM`, `PROBLEEM_VOLGNUMMER`, `EFFECT`, `FREQUENTIEAANTAL`, `FREQUENTIE`, `INTENSITEITAANTAL`, `INTENSITEIT`, `SINDS`, `TOT`, `ZORGVERLENER`, `PZN`, `VERSIE`)
        SELECT `ZORG`, `DOMEIN`, `NIVEAU`, `PROBLEEM`, ?, `EFFECT`, `FREQUENTIEAANTAL`, `FREQUENTIE`, `INTENSITEITAANTAL`, `INTENSITEIT`, `SINDS`, `TOT`, `ZORGVERLENER`, `PZN`, $_nieuweversie
        FROM pzn_zorgbesteed
        WHERE PROBLEEM_VOLGNUMMER = ?";

        foreach($_rs as $_record) {
            $_oudVolgNummer = $_record['VOLGNUMMER'];
            unset($_record['VOLGNUMMER']);
            $_result = $_db->Execute($_insertProfielSQL, $_record);

            $_newVolgNummer = $_db->insert_Id();
            $_db->Execute($_insertZorgSQL, array($_newVolgNummer, $_oudVolgNummer));
        }

        $_sqlResetCirkelsAfgerond = "UPDATE pzn_netwerkdeelnemer SET CIRKELMODELAFGEROND = NULL WHERE $_keySelector = ?";
        $_db->Execute($_sqlResetCirkelsAfgerond, array($_pznid_encrypted));

        $this->_PauzeerRTO($_pznid_encrypted, true);

        return true;
    }

    function VerwijderVersie($_allesRecord) {
        $_pznid_encrypted = $_allesRecord['_hdnRecordID'];
        $_versie = intval($_allesRecord['VERSIE']);
        $_keySelector = $this->KeyPZNIDSelector('PZN');
        $_sql = "UPDATE pzn_versie SET DELETED = 'Y'
        WHERE $_keySelector = ?
        AND VERSIE = ?";
        $this->form->database->userdb->Execute($_sql, array($_pznid_encrypted, $_versie));

        return true;
    }

    function VerwerkPZNOuderVoogd($_allesRecord) {
        $_record = ExtractTableFields($_allesRecord);
        unset($_record['PZN']);
        unset($_record['SOORTRELATIE']);
        $_record['_hdnDatabase'] = $_allesRecord['_hdnDatabase'];
        $_record['_hdnTable'] = 'PZN_PERSOON';
        $_record['_hdnState'] = $_allesRecord['_hdnState'];
        $_record['_hdnRecordID'] = $_allesRecord['_hdnPersoonID'];
        $_persoonvolgnummer = SaveRecord($_record);

        unset($_record);
        $_record['_hdnDatabase'] = $_allesRecord['_hdnDatabase'];
        $_record['_hdnTable'] = 'PZN_OUDERVOOGD';
        $_record['_hdnState'] = $_allesRecord['_hdnState'];
        $_record['_hdnRecordID'] = $_allesRecord['_hdnRecordID'];
        $_record['SOORTRELATIE'] = $_allesRecord['SOORTRELATIE'];
        if ($_allesRecord['_hdnState'] == 'insert') {
            $_record['PZN'] = $_allesRecord['PZN'];
            $_record['VOLGNUMMER'] = $_persoonvolgnummer;
        }

        $_result = SaveRecord($_record);

        if (is_numeric($_persoonvolgnummer)) {
            $this->VerwerkPersoonAccount($_record['PZN'], $_persoonvolgnummer);
        }

        return $_result;
    }

    function VerwerkPZNNetwerkDeelnemer($_allesRecord) {
        global $polaris;

        if ($_allesRecord['SOORTDEELNEMER'] == 'Zorgverlener') {
            $_persoonvolgnummer = $_allesRecord['ZORGVERLENER_VOLGNUMMER'];
            $_soortZorg = GetSoortZorg($this->form->database->userdb, $_allesRecord["ZORGVERLENER_VOLGNUMMER"]);
            $_allesRecord['SOORTZORG'] = $_soortZorg;
        } else {
            $_exists = $polaris->getUserGroupInfo($_allesRecord['E_MAILADRES']);
            if ($_exists) {
                $msg = 'Deze extra deelnemer is al bekend in PZN met dit emailadres. Kijk onder geregistreerde zorgverleners.';
                adodb_plr_throw($this->database, 'VerwerkPZNNetwerkDeelnemer', $msg, $msg, null, null, $this->database, $msg);
            } else {
                $_record['_hdnDatabase'] = $_allesRecord['_hdnDatabase'];
                $_record['_hdnTable'] = 'PZN_PERSOON';
                $_record['_hdnState'] = $_allesRecord['_hdnState'];
                $_record['_hdnRecordID'] = $_allesRecord['_hdnRecordID'];
                $_record['VOORNAAM'] = $_allesRecord['VOORNAAM'];
                $_record['ACHTERNAAM'] = $_allesRecord['ACHTERNAAM'];
                $_record['TELEFOONNUMMER'] = $_allesRecord['TELEFOONNUMMER'];
                $_record['E_MAILADRES'] = $_allesRecord['E_MAILADRES'];
                $_persoonvolgnummer = SaveRecord($_record);
            }
        }

        unset($_record);
        $_record['_hdnDatabase'] = $_allesRecord['_hdnDatabase'];
        $_record['_hdnTable'] = 'PZN_NETWERKDEELNEMER';
        $_record['_hdnState'] = $_allesRecord['_hdnState'];
        $_record['_hdnRecordID'] = $_allesRecord['_hdnRecordID'];
        $_record['SOORTDEELNEMER'] = $_allesRecord['SOORTDEELNEMER'];
        $_record['SOORTZORG'] = $_allesRecord['SOORTZORG'];
        if ($_allesRecord['_hdnState'] == 'insert') {
            $_record['PZN'] = $_allesRecord['PZN'];
            $_record['VOLGNUMMER'] = $_persoonvolgnummer;
        }

        $_result = SaveRecord($_record);
        return $_result;
    }

    function ShowListView($moduleid) {
        if ($this->outputformat == 'xhtml') {
            $this->form->LoadViewContent($detail=false, $masterrecord=false, false, false);
//            $this->form->ShowBasicActions('master', $this->state);

            $this->form->ShowListView($this->state, $this->permission);
        }
    }

    function ZorgverlenerBestaat($pznid, $volgnummer) {
        $_zorgVerlener = $this->GetZorgverleners($pznid, $volgnummer);
        if (!empty($_zorgVerlener[0])) {
            return $_zorgVerlener[0];
        } else {
            return false;
        }
    }

    function CheckUitnodigingen($items, $actiefvolgnummer) {
        foreach($items as $_idx => $_item) {
            $_zorgverlener = $this->ZorgverlenerBestaat($_item['PZN_ID'], $actiefvolgnummer);
            if ($_zorgverlener !== false) {
                $items[$_idx]['MOETACCEPTEREN'] = ($_zorgverlener['STATUS'] == 'VERSTUURD');
            }
        }
        return $items;
    }

    function CreatePersonenFilter($pznids) {
        $_filter = false;
        if (is_array($pznids)) {
            $_pzns = implode(',', array_column($pznids, 'PZN_ID') );
        } else {
            $_pzns = $pznids;
        }
        if ($_pzns !== '')
            $_filter = "PZN_ID IN (". $_pzns . ")";
        return $_filter;
    }

    function ShowPersonenOverzicht($moduleid, $pznids=false) {
        $_filter = $this->CreatePersonenFilter($pznids);
        $_rs = $this->form->GetViewContent($detail=false, $masterrecord=false, false, $_filter);
        $_persoonvolgnummer = GetPersoonVolgnummer($this->form->database, $_SESSION['userid']);
        $_items = $_rs;
        $_items = $this->CheckUitnodigingen($_items, $_persoonvolgnummer);
        $this->smarty->assign('items', $_items);
        $this->smarty->assign('actiefvolgnummer', $_persoonvolgnummer);
        $this->smarty->display("personenoverzicht.tpl.php");
    }

    function ShowPZNForm($moduleid, $pznids=false) {
        if ($this->outputformat == 'xhtml') {
            $this->showControls = false;
            $_filter = $this->CreatePersonenFilter($pznids);
            $_rs = $this->form->GetViewContent($detail=false, $masterrecord=false, 1, $_filter);
            $_items = $_rs;
            $_persoonvolgnummer = GetPersoonVolgnummer($this->form->database, $_SESSION['userid']);
            $_items = $this->CheckUitnodigingen($_items, $_persoonvolgnummer);
            $_versies = $this->GetProfielVersies($_items[0]['PLR__RECORDID']);
            $_keyVersies = array_keys($_versies);
            $_laatsteversie = end($_keyVersies);
            if (!$_laatsteversie) $_laatsteversie = 1;
            $this->smarty->assign("huidigeversie", $_laatsteversie);
            $this->smarty->assign('moduleid', $moduleid);
            $this->smarty->assign('isjeugdigeofverzorger', $this->IsJeugdigeOfVerzorger($_items[0]['PZN_ID'], $_persoonvolgnummer));
            $this->smarty->assign('iscoordinator', $this->IsCoordinator($_items[0]['PZN_ID'], $_persoonvolgnummer));
            $this->smarty->assign('isnetwerkdeelnemer', $this->IsNetwerkdeelnemer($_items[0]['PZN_ID'], $_persoonvolgnummer));
            $this->smarty->assign('actiefvolgnummer', $_persoonvolgnummer);
            $this->smarty->assign('modulestatus', $_items[0]['STATUS']);
            $this->smarty->assign('item', $_items[0]);
            $this->smarty->assign('readonly', $this->permission == 2);
            if ($moduleid == 4 and $_items[0]['WIZARDKLAAR'] == 'N') {
                $this->smarty->display("pzn_wizard.tpl.php");
            } else {
                $this->smarty->display("pzn_mainform.tpl.php");
            }
        }
    }

    function ShowWizardForm($moduleid, $pznids = false) {
        if ($this->outputformat == 'xhtml') {
            $_filter = $this->CreatePersonenFilter($pznids);
            $_rs = $this->form->GetViewContent($detail=false, $masterrecord=false, 1, $_filter);
            $_items = $_rs->GetAll();
            $_persoonvolgnummer = GetPersoonVolgnummer($this->form->database, $_SESSION['userid']);
            $this->smarty->assign('isjeugdigeofverzorger', $this->IsJeugdigeOfVerzorger($_items[0]['PZN_ID'], $_persoonvolgnummer));
            $this->smarty->assign('iscoordinator', $this->IsCoordinator($_items[0]['PZN_ID'], $_persoonvolgnummer));
            $this->smarty->assign('isnetwerkdeelnemer', $this->IsNetwerkdeelnemer($_items[0]['PZN_ID'], $_persoonvolgnummer));
            $this->smarty->assign('actiefvolgnummer', $_persoonvolgnummer);
            $this->smarty->assign('modulestatus', $_items[0]['STATUS']);
            $this->smarty->assign('item', $_items[0]);
            $this->smarty->display("pzn_wizard.tpl.php");
        }
    }

    function ShowAcceptPZN($moduleid) {
        $_rs = $this->form->GetViewContent($detail=false, $masterrecord=false, 1);
        $_items = $_rs->GetAll();
        $this->smarty->assign('item', $_items[0]);
        $this->smarty->display('accepteerdeelnemer.tpl.php');
    }

    function ShowPZNTimeline() {
        if ($this->outputformat == 'xhtml') {
            $this->showControls = false;
//            $_rs = $this->form->GetViewContent($detail=false, $masterrecord=false);
            //$_items = $_rs->GetAll();
            //$this->smarty->assign('item', $_items[0]);
            $this->smarty->display("pzn_timeline.tpl.php");
        }
    }

    function ShowPZNChatSessie() {
        global $polaris;

        if ($this->outputformat == 'xhtml') {
            $this->showControls = false;
            $_rs = $this->form->GetViewContent($detail=false, $masterrecord=false, 1);
            $_items = $_rs->GetAll();
            $this->smarty->assign('item', $_items[0]);
            $_persoonvolgnummer = GetPersoonVolgnummer($this->form->database, $_SESSION['userid']);
            $this->smarty->assign('actiefvolgnummer', $_persoonvolgnummer);
            $this->smarty->assign("polarissession", $polaris->polarissession);
            $this->smarty->display("pzn_chatview.tpl.php");
        }
    }

    function GetProfielVersies($_pznid_encrypted) {
        $_keySelect = $this->KeyPZNIDSelector('PZN');
        $_sql = "SELECT DISTINCT VERSIE, EINDDATUM, OMSCHRIJVING
        FROM pzn_versie
        WHERE $_keySelect = ? AND DELETED = 'N' ORDER BY VERSIE";
        $_rs = $this->form->database->userdb->GetAssoc($_sql, array($_pznid_encrypted));
        // if (count($_rs) == 0) {
        //     $_rs = array('1', 'Geen');
        // }
        return $_rs;
    }

    function ShowCirkelModel($_pznid_encrypted) {
        if ($this->outputformat == 'xhtml') {
            $this->showControls = false;
            $_pzn = $this->GetPZNData($_pznid_encrypted);
            $_pznid = $_pzn[0]['PZN_ID'];
            $_rto_gepauzeerd = ($_pzn[0]['RTO_STATUS'] == 'PAUZE');
            $this->smarty->assign("item", $_pzn[0]);
            $_versies = $this->GetProfielVersies($_pznid_encrypted);
            $_keyVersies = array_keys($_versies);
            $_huidigeversie = $this->GetHuidigeVersie($_versies);
            $_laatsteversie = (end($_keyVersies) == $_huidigeversie) ? true : false;
            $_persoonvolgnummer = GetPersoonVolgnummer($this->form->database, $_SESSION['userid']);
            $_cirkelAfgerond = $this->CirkelIsAfgerond($_pznid, $_persoonvolgnummer);

            $_isCoordinator = $this->IsCoordinator($_pznid, $_persoonvolgnummer);
            $_isJeugdigeOfVerzorger = $this->IsJeugdigeOfVerzorger($_pznid, $_persoonvolgnummer);
            $_isNetwerkDeelnemer = $this->IsNetwerkdeelnemer($_pznid, $_persoonvolgnummer);

            $this->smarty->assign("versies", $_versies);
            $this->smarty->assign("huidigeversie", $_huidigeversie);
            $this->smarty->assign("islaatsteversie", $_laatsteversie);
            $this->smarty->assign("_backurl", $_GET['_back']);
            $this->smarty->assign("recordid", $this->form->database->customUrlEncode($_pzn[0]['PLR__RECORDID']));
            $this->smarty->assign('isjeugdigeofverzorger', $_isJeugdigeOfVerzorger);
            $this->smarty->assign('iscoordinator', $_isCoordinator);
            $this->smarty->assign('isnetwerkdeelnemer', $_isNetwerkDeelnemer);
            $this->smarty->assign('actiefvolgnummer', $_persoonvolgnummer);
            $this->smarty->assign('cirkelafgerond', $_cirkelAfgerond);
            $_zorgverleners = $this->GetZorgverlenersOpties($_pznid);
            $_huidigezorgverlener = !empty($_GET['zorgverlener']) ? $_GET['zorgverlener'] : '99999';
            $_huidigeGevonden = in_array($_huidigezorgverlener, array_column($_zorgverleners, 'VOLGNUMMER'));
            if ($_huidigeGevonden === false)
                $_huidigezorgverlener = '99999';
            $this->smarty->assign("zorgverleners", $_zorgverleners);
            $this->smarty->assign("zorgverlener", $_huidigezorgverlener);
            $this->smarty->assign("cirkel_editable", ($_laatsteversie
            and (!$_rto_gepauzeerd or $_isCoordinator or $_isJeugdigeOfVerzorger)
            and (
                (
                    (
                        $this->IsHuidigeDeelnemer($_pznid, $_huidigezorgverlener)
                        and !$_cirkelAfgerond
                    )
                    or (
                        $_isCoordinator
                    )
                    or (
                        $_isJeugdigeOfVerzorger and !$_cirkelAfgerond
                    )
                )
            ) and $_huidigezorgverlener !== '99999')
            ? "editable" : '');
            $this->smarty->display("cirkelmodel.tpl.php");
        }
    }

    function ShowUitnodigingOverzicht($moduleid) {
        if ($this->outputformat == 'xhtml') {
            $this->showControls = false;
            $_rs = $this->form->GetViewContent($detail=false, $masterrecord=false);
            $_items = $_rs;
            foreach($_items as $_key => $_item) {
                $datetime1 = new DateTime($_item['DATUM_UITNODIGING']);
                $datetime2 = new DateTime('now');
                $interval = $datetime1->diff($datetime2);
                $_items[$_key]['TIMEPASSED'] = $interval->days;
            }

            $this->smarty->assign("_hdnFormHash", $this->form->record->RECORDID);
            $this->smarty->assign("items", $_items);
            $this->smarty->display("uitnodigingoverzicht.tpl.php");
        }
    }

    function ShowPZNUitnodigingForm() {
        if ($this->outputformat == 'xhtml') {
            $this->showControls = false;
            $_pzn = $this->GetPZNData($_pznid);
            $this->smarty->assign("item", $_pzn[0]);
            $this->smarty->assign("recordid", $this->form->database->customUrlEncode($_pzn[0]['PLR__RECORDID']));
            $this->smarty->assign("zorgverleners", $this->GetZorgverlenersOpties($_pznid));
            $this->smarty->display("uitnodiging.tpl.php");
        }
    }

    function ShowPersoonForm() {
        if ($this->outputformat == 'xhtml') {
            $this->showControls = true;
            $_rs = $this->form->GetViewContent($detail=false, $masterrecord=false, false);
            $_items = $_rs;
            $_inserting = isset($_items[0]) ? false : true;
            $this->smarty->assign("items", $_items);
            $this->smarty->assign("isinserting", $_inserting);
            $this->smarty->assign('rollen', array('Zorgverlener', 'Coordinator', 'Extra-deelnemer', 'Jeugdige', 'Verzorger'));
            $this->smarty->assign('soortzorg', $this->GetSoortenZorg());
            $this->smarty->display("personen_edit.tpl.php");
        }
    }

    function GetPZNAlles($pznid_encrypted) {
        $_array = Array();
        $_pzn = $this->GetPZNData($pznid_encrypted);
        $_pznid = $_pzn[0]['PZN_ID'];
        $_oudervoogd = $this->GetOuderVoogd($_pznid);
        $_zorgVerleners = $this->GetZorgverleners($_pznid);
        return json_encode(array('PZN'=>$_pzn
        , 'ouderVoogd'=>$_oudervoogd
        , 'netwerkDeelnemers'=>$_zorgVerleners
        ));
    }

    function GetUitnodigingen() {
        $_sql = "SELECT ttt.*, DATEDIFF(CURDATE(),STR_TO_DATE(DATUM_UITNODIGING, '%m/%d/%Y')) AS DAGEN FROM PZN_UITNODIGING ttt ORDER BY datum_uitnodiging";
        $_rs = $this->form->database->userdb->GetAll($_sql);
        return $_rs;
    }

    function GetProblemen() {
        $_sql = 'SELECT domein, niveau, probleem FROM probleem ORDER BY domein, niveau';
        $_rs = $this->form->database->userdb->GetAll($_sql);
        return $_rs;
    }

    function GetZorg() {
        $_sql = 'SELECT domein, niveau, zorg FROM zorg order by domein, niveau';
        $_rs = $this->form->database->userdb->GetAll($_sql);
        return $_rs;
    }

    function GetPZNProfiel($_pznid, $_versie, $_zorgverlener) {
        $_array = Array();
        $_pzn = $this->GetPZNData($_pznid);
        if ($_zorgverlener == '99999') {
            $_profielRegels = $this->GetZorgplan($_pznid, $_versie);
        } else {
            $_profielRegels = $this->GetProfielRegels($_pznid, $_versie, $_zorgverlener);
        }
        $_problemen = $this->GetProblemen();
        $_zorg = $this->GetZorg();
        return json_encode(array('PZN'=>$_pzn
        , 'profielRegels'=>$_profielRegels
//        , 'zorgBesteedRegels'=>$_zorgBesteedRegels
        , 'problemen'=>$_problemen
        , 'zorg'=>$_zorg
        ));
    }

    function KeyPZNIDSelector($columnname = 'PZN_ID') {
        global $scramble;

        return "MD5(CONCAT('$scramble', '_', $columnname))";
    }

    function GetPZNFromEncrypted($pznid_encrypted) {
        $_key = $this->KeyPZNIDSelector();
        $_sql = "SELECT PZN_ID FROM PERSOONLIJKZORGNETWERK2 WHERE $_key = '$pznid_encrypted'";
        return $this->form->database->userdb->GetOne($_sql);
    }

    function GetPZNData($_pznid, $_encrypted=true) {
        global $scramble;

        if ($_encrypted)
            $keySelector = $this->KeyPZNIDSelector();
        else
            $keySelector = 'PZN_ID';

        $_sql = "SELECT p.*
        , $keySelector AS PLR__RECORDID
        , MD5(CONCAT('$scramble', '_', J_VOLGNUMMER)) AS PLR__JEUGDIGERECORDID
        , MD5(CONCAT('$scramble', '_', V_VOLGNUMMER)) AS PLR__VERZORGERRECORDID
        , MD5(CONCAT('$scramble', '_', C_VOLGNUMMER)) AS PLR__COORDINATORRECORDID
        FROM VW_COMPLEET p WHERE $keySelector = ?";
        $_rs = $this->form->database->userdb->GetAll($_sql, array($_pznid));
        return $_rs;
    }

    function _GetTableData($pznid, $tablename, $volgnummer=false) {
        global $scramble;

        $_sql = "SELECT t.*, STATUS, sysdate() as STATUSDATUM
        , MD5(CONCAT('$scramble', '_', PZN, '_', VOLGNUMMER)) AS PLR__RECORDID
        , MD5(CONCAT('$scramble', '_', VOLGNUMMER)) AS PLR__PERSOONRECORDID
        , MD5(t.E_MAILADRES) AS IDENTICON
        FROM $tablename t WHERE PZN = ? ";
        $_keys = array($pznid);
        if ($volgnummer) {
            $_sql .= " AND VOLGNUMMER = ? ";
            $_keys = array($pznid, $volgnummer);
        }
        $_sql .= " ORDER BY PZN, VOLGNUMMER";
        $_rs = $this->form->database->userdb->GetAll($_sql, $_keys);
        return $_rs;
    }

    function GetOuderVoogd($_pznid) {
        return $this->_GetTableData($_pznid, 'VW_OUDERVOOGD');
    }

    function GetZorgverleners($_pznid, $_volgnummer=false) {
        return $this->_GetTableData($_pznid, 'VW_NETWERKDEELNEMER', $_volgnummer);
    }

    function GetVertrouwensPersoon($_pznid) {
        return $this->_GetTableData($_pznid, 'PZN_VERTROUWENSPERSOON');
    }

    function GetProfielRegels($pznid, $versie, $_zorgverlener) {
        $keySelector = $this->KeyPZNIDSelector('t.PZN');
        $_sql = "SELECT t.*, 'Uitgenodigd' AS status, sysdate() as statusdatum
        , t.DOMEIN AS DOMEIN_CODE
        , t.NIVEAU AS NIVEAU_CODE
        , ELT(FIELD(t.domein, 'emot', 'rela', 'vlgh', 'mat', 'ontw', 'gezh')
            , 'Emotioneel', 'Relaties', 'Veiligheid', 'Materieel', 'Ontwikkeling', 'Gezondheid') as DOMEIN
        , ELT(FIELD(t.niveau, 'o', 'g', 'k')
            , 'Omgeving', 'Gezin', 'Kind') as NIVEAU
        , CONCAT(ELT(FIELD(n.SOORTDEELNEMER, 'Coordinator', 'Zorgverlener', 'Mantelzorger', 'Extra-deelnemer', 'Verzorger', 'Jeugdige')
            , 'COR', 'ZOV', 'MAZ', 'DLN', 'VZR', 'JGD'), ' ', n.ACHTERNAAM) AS NAAM
        , (SELECT COUNT(*) FROM PZN_ZORGBESTEED z WHERE t.`VOLGNUMMER`=z.`PROBLEEM_VOLGNUMMER` ) AS AANTALZORGBESTEED
        , $keySelector AS PLR__RECORDID
        FROM PZN_PROFIEL t
        LEFT JOIN vw_netwerkdeelnemer n
        ON t.ZORGVERLENER = n.VOLGNUMMER AND t.PZN = n.PZN
        WHERE $keySelector = ? AND versie = ?
        AND t.ZORGVERLENER = ?
        ORDER BY t.PZN, t.VOLGNUMMER";
        return $this->form->database->userdb->GetAll($_sql, array($pznid, $versie, $_zorgverlener));
    }

    function GetZorgplan($pznid, $versie) {
        $keySelector = $this->KeyPZNIDSelector('t.PZN');
        $_sql = "SELECT t.*, $keySelector AS PLR__RECORDID
        , DOMEIN AS DOMEIN_CODE
        , NIVEAU AS NIVEAU_CODE
        , ELT(FIELD(domein, 'emot', 'rela', 'vlgh', 'mat', 'ontw', 'gezh')
            , 'Emotioneel', 'Relaties', 'Veiligheid', 'Materieel', 'Ontwikkeling', 'Gezondheid') as DOMEIN
        , ELT(FIELD(niveau, 'o', 'g', 'k')
            , 'Omgeving', 'Gezin', 'Kind') as NIVEAU
        , CONCAT(ELT(FIELD(n.SOORTDEELNEMER, 'Coordinator', 'Zorgverlener', 'Mantelzorger', 'Extra-deelnemer', 'Verzorger', 'Jeugdige')
            , 'COR', 'ZOV', 'MAZ', 'DLN', 'VZR', 'JGD'), ' ', n.ACHTERNAAM) AS NAAM
        , (SELECT COUNT(*) FROM PZN_ZORGBESTEED z WHERE t.`VOLGNUMMER`=z.`PROBLEEM_VOLGNUMMER` ) AS AANTALZORGBESTEED
        , CAST(ELT(FIELD(n.SOORTDEELNEMER, 'Coordinator', 'Zorgverlener', 'Mantelzorger', 'Extra-deelnemer', 'Verzorger', 'Jeugdige')
            , 3, 10, 10, 10, 2, 1) AS SIGNED) AS ORDER_BY
        FROM PZN_PROFIEL t
        LEFT JOIN vw_netwerkdeelnemer n
        ON t.ZORGVERLENER = n.VOLGNUMMER AND t.PZN = n.PZN
        WHERE $keySelector = ? AND versie = ?
        AND n.SOORTDEELNEMER <> 'Verzorger'
        ORDER BY ORDER_BY, n.VOLGNUMMER, domein, niveau";
        return $this->form->database->userdb->GetAll($_sql, array($pznid, $versie));
    }

    function GetZorgPlanIDs($pznid, $versie, $zorgverlener) {
        $keySelector = $this->KeyPZNIDSelector('t.PZN');
        $_sql = "SELECT t.VOLGNUMMER
        FROM PZN_PROFIEL t
        LEFT JOIN vw_netwerkdeelnemer n
        ON t.ZORGVERLENER = n.VOLGNUMMER AND t.PZN = n.PZN
        WHERE $keySelector = ? AND versie = ?
        ";
        //AND n.SOORTDEELNEMER <> 'Verzorger'
        return $this->form->database->userdb->GetCol($_sql, array($pznid, $versie));

    }

    function GetZorgBesteedRegels($_probleemvolgnummer, $_zorgverlener) {
        $_recordSelector = $this->form->database->makeRecordIDColumn('PZN_ZORGBESTEED');
        $_sql = "SELECT t.*, $_recordSelector AS PLR__RECORDID
        , DOMEIN AS DOMEIN_CODE
        , NIVEAU AS NIVEAU_CODE
        , ELT(FIELD(domein, 'emot', 'rela', 'vlgh', 'mat', 'ontw', 'gezh')
            , 'Emotioneel', 'Relaties', 'Veiligheid', 'Materieel', 'Ontwikkeling', 'Gezondheid') as DOMEIN
        , ELT(FIELD(niveau, 'o', 'g', 'k')
            , 'Omgeving', 'Gezin', 'Kind') as NIVEAU
        , ELT(FIELD(effect, 0, 1, 2, 3)
            , 'Nauwelijks', 'Matig', 'Positief', 'Onbekend') as EFFECT
        , effect as EFFECT_CODE
        FROM PZN_ZORGBESTEED t
        WHERE PROBLEEM_VOLGNUMMER = ? AND ZORGVERLENER = ?
        ORDER BY VOLGNUMMER";

        $_params = array($_probleemvolgnummer, $_zorgverlener);

        $_rs = $this->form->database->userdb->GetAll($_sql, $_params);
        return json_encode(array(
            'zorgBesteedRegels'=>$_rs
        ));
    }

    function GetZorgBesteedRegelsCombined($pznid, $versie, $zorgverlener) {
        $_ids = $this->GetZorgPlanIDs($pznid, $versie, $zorgverlener);
        $_recordSelector = $this->form->database->makeRecordIDColumn('PZN_ZORGBESTEED');
        $_sql = "SELECT t.*, $_recordSelector AS PLR__RECORDID
        , DOMEIN AS DOMEIN_CODE
        , NIVEAU AS NIVEAU_CODE
        , ELT(FIELD(domein, 'emot', 'rela', 'vlgh', 'mat', 'ontw', 'gezh')
            , 'Emotioneel', 'Relaties', 'Veiligheid', 'Materieel', 'Ontwikkeling', 'Gezondheid') as DOMEIN
        , ELT(FIELD(niveau, 'o', 'g', 'k')
            , 'Omgeving', 'Gezin', 'Kind') as NIVEAU
        , ELT(FIELD(effect, 0, 1, 2, 3)
            , 'Nauwelijks', 'Matig', 'Positief', 'Onbekend') as EFFECT
        , effect as EFFECT_CODE
        FROM PZN_ZORGBESTEED t
        WHERE PROBLEEM_VOLGNUMMER in (__array_id__)
        AND VERSIE = ? ";
        if ($zorgverlener !== '99999') {
            $_sql = $_sql . "AND ZORGVERLENER = $zorgverlener";
        }
        $_sql = $_sql . " ORDER BY PROBLEEM_VOLGNUMMER, VOLGNUMMER";
        $_params = join($_ids, ',');
        $_sql = str_replace('__array_id__', $_params, $_sql);
        $_rs = $this->form->database->userdb->GetAll($_sql, array($versie));
        return json_encode(array(
            'zorgBesteedRegels'=>$_rs
        ));
    }

    function ScanAtWhos($items) {
        foreach($items as $_key => $_item) {
            $_item['BERICHT'] = preg_replace('/@(\S+)/i', '<span class="atwho">@${1}</span>', $_item['BERICHT']);
            $items[$_key] = $_item;
        }
        return $items;
    }

    function GetChatsessie($pznid_encrypted, $last) {
        $_array = Array();

        $_pznid_encrypted = $this->KeyPZNIDSelector();
        if (isset($last) and $last > 0) {
            $_where = "AND t.VOLGNUMMER > $last";
        }
        $_sql = "
        SELECT DATE_FORMAT(AANMAAKDATUM, '%d-%m-%Y') AS AANMAAKDATUM, BERICHT, UITVOERING, RESULTAAT, DATE_FORMAT(AANMAAKDATUM, '%k:%i') AS AANMAAKTIJD
        , t.MESSAGETYPE
        , t.VOLGNUMMER, CONCAT(p.VOORNAAM, ' ', p.ACHTERNAAM) AS ZORGVERLENER_NAAM, MD5(PZN_ID) AS PLR__RECORDID
        FROM PZN_CHATSESSIE t
        LEFT JOIN PZN_PERSOON p
        ON p.VOLGNUMMER = t.ZORGVERLENER
        WHERE $_pznid_encrypted = ?
        $_where
        ORDER BY VOLGNUMMER ASC
        ";
        $_rs = $this->form->database->userdb->GetAll($_sql, array($pznid_encrypted));
        $_rs = $this->ScanAtWhos($_rs);

        $_sqlStatus = "SELECT RTO_STATUS FROM persoonlijkzorgnetwerk2 WHERE $_pznid_encrypted = ?";
        $_rtoStatus = $this->form->database->userdb->GetOne($_sqlStatus, array($pznid_encrypted));

        return json_encode(array('chatSessie'=>$_rs, 'status'=>$_rtoStatus));
    }

    function GetZorgverlenersOpties($_pznid) {
        $_persoonvolgnummer = GetPersoonVolgnummer($this->form->database, $_SESSION['userid']);
        $_sql = "
        SELECT n.VOLGNUMMER
        , CONCAT(
        ELT(FIELD(SOORTDEELNEMER, 'Coordinator', 'Zorgverlener', 'Mantelzorger', 'Extra-deelnemer', 'Verzorger', 'Jeugdige')
            , 'COR', 'ZOV', 'MAZ', 'DLN', 'VRZ', 'JGD'), ' <b>', n.ACHTERNAAM, '</b> ', IFNULL(SOORTZORG, '')) AS NAAM, CIRKELMODELAFGEROND
        , SOORTDEELNEMER
        FROM vw_netwerkdeelnemer n
        WHERE PZN = ?
        ORDER BY ELT(FIELD(SOORTDEELNEMER, 'Verzorger', 'Jeugdige', 'Coordinator', 'Zorgverlener', 'Mantelzorger', 'Extra-deelnemer')
            , 1, 2, 3, 4, 5, 6), 2";
        //ORDER BY FIELD(VOLGNUMMER, $_persoonvolgnummer, 1, 0) DESC, 2";

        $_rs = $this->form->database->userdb->GetAll($_sql, array($_pznid));
        return $_rs;
    }

    function GetSoortenZorg() {
        $_sql = 'SELECT SOORTZORG, SOORTZORG AS OMSCHRIJVING FROM SOORTZORG ORDER BY SOORTZORG';
        $_rs = $this->form->database->userdb->GetAssoc($_sql);
        return $_rs;
    }

    function GetSoortFuncties() {
        $_sql = 'SELECT SOORT_ZORG, SOORT_ZORG AS OMSCHRIJVING FROM pzn_persoon WHERE ROL = "Zorgverlener"';
        $_rs = $this->form->database->userdb->GetAssoc($_sql);
        $_rs['Extra-deelnemer'] = 'Extra-deelnemer';
        return $_rs;
    }

    function GetCoordinatorWissels($pznid_encrypted) {
        $_keySelect = $this->KeyPZNIDSelector('PZN');
        $_sql = "SELECT DATUM_WISSEL, STATUS, CONCAT(VOORNAAM, ' ', ACHTERNAAM) AS COORDINATOR, NIEUWECOORDINATOR
        FROM vw_coordinatorwissel
        WHERE $_keySelect = ? ORDER BY VOLGNUMMER DESC LIMIT 0,10";
        return json_encode($this->form->database->userdb->GetAll($_sql, array($pznid_encrypted)));
    }

    function GetGeregistreerdeZorgverleners($soort) {
        if ($soort !== 'Extra-deelnemer') {
            $_sql = "select SOORT_ZORG, VOLGNUMMER, VOORNAAM, ACHTERNAAM, PLAATSNAAM
            FROM pzn_persoon
            WHERE SOORT_ZORG = ?
            ";
        } else {
            $_sql = "select SOORTZORG AS SOORT_ZORG, VOLGNUMMER, VOORNAAM, ACHTERNAAM, PLAATSNAAM
            FROM vw_netwerkdeelnemer
            WHERE SOORTDEELNEMER = ?
            ";
        }
        return json_encode($this->form->database->userdb->GetAll($_sql, array($soort)));
    }

    function GetHuidigeVersie($versies = false) {
        if ($versies == false)
            $versies = $this->GetProfielVersies($_GET['pznid']);
        $_keyversies = array_keys($versies);
        $_huidigeversie = (!empty($_GET['versie'])) ? $_GET['versie'] : end($_keyversies);
        if (!in_array($_huidigeversie, $_keyversies))
            $_huidigeversie = end($_keyversies);
        if (!$_huidigeversie)
            $_huidigeversie = 1;

        return $_huidigeversie;
    }

    function ProcessJson() {
        $_content = false;

        $_func = $_GET['func'];
        $_pznid_encrypted = $_GET['pznid'];
        $_zorgverlener = $_GET['zorgverlener'];
        if (!empty($_GET['soort']))
            $_soort = $_GET['soort'];
        $_huidigeversie = $this->GetHuidigeVersie();
        if (!empty($_func)) {
            switch ($_func) {
                case 'pzn':
                    $_content = $this->GetPZNAlles($_pznid_encrypted);
                    break;
                case 'zorgplan':
                    $_content = $this->GetZorgplan($_pznid_encrypted, $_GET['versie']);
                    break;
                case 'zorgbesteed':
                    //$_content = $this->GetZorgBesteedRegels
                        $_content = $this->GetZorgBesteedRegelsCombined($_pznid_encrypted, $_GET['versie'], $_zorgverlener);
                    if ($_zorgverlener == '99999') {
                    } else {
//                        $_content = $this->GetZorgBesteedRegels($_GET['volgnummer'], $_zorgverlener);
                    }
                    break;
                case 'pznchatsessie':
                    $this->VerwerkGelezenChats($_pznid_encrypted, $_GET['last']);
                    $_content = $this->GetChatsessie($_pznid_encrypted, $_GET['last']);
                    break;
                case 'pznprofiel':
                    $_content = $this->GetPZNProfiel($_pznid_encrypted, $_huidigeversie, $_zorgverlener);
                    break;
                case 'coordinatorwissels':
                    $_content = $this->GetCoordinatorWissels($_pznid_encrypted);
                    break;
                case 'pzn_persoon':
                    $_content = $this->GetGeregistreerdeZorgverleners($_soort);
                    break;
                default:
                    break;
            }
        }
        if ($_content !== false) {
            echo $_content;
            exit; // stop with the rest of the html
        }
        return false;
    }

    function UserHasSuperPowers() {
        global $polaris;
        return in_array(SUPERUSERGROUP, explode(',', $polaris->groupvalues));
    }

    function GetPZNSFromUserID($userid) {
        if ($this->UserHasSuperPowers()) {
            // Show all PZN's, no restrictions
            $_rs = '';
        } else {
            $_persoonvolgnummer = GetPersoonVolgnummer($this->form->database, $userid);
            $_sql = "SELECT PZN_ID FROM PERSOONLIJKZORGNETWERK2
            WHERE VERZORGER_VOLGNUMMER = ?
            OR JEUGDIGE_VOLGNUMMER = ?
            UNION
            SELECT PZN
            FROM PZN_OUDERVOOGD
            WHERE VOLGNUMMER = ?
            ";
            $_rs = $this->form->database->userdb->GetAll($_sql, array($_persoonvolgnummer, $_persoonvolgnummer, $_persoonvolgnummer));
            if (count($_rs) == 0) {
                $_sql = "SELECT PZN AS PZN_ID FROM PZN_NETWERKDEELNEMER WHERE VOLGNUMMER = ?
                UNION
                SELECT PZN AS PZN_ID FROM PZN_COORDINATORWISSEL WHERE NIEUWECOORDINATOR = ?";
                $_rs = $this->form->database->userdb->GetAll($_sql, array($_persoonvolgnummer, $_persoonvolgnummer));
            }
        }
        return $_rs;
    }

    function GetCurrentState() {
        if (in_array($this->moduleid, [4])) {
            return 'edit';
        }
    }

    function ShowModalForms() {
        if (in_array($this->moduleid, [4, 8, 10, 15, 20])) {
            $this->smarty->assign('soortzorg', $this->GetSoortenZorg());
            $this->smarty->assign('soortfuncties', $this->GetSoortFuncties());
            $this->smarty->display('modalforms.tpl.php');
        }
    }

    function IsCoordinator($pznid, $persoonvolgnummer) {
        $_SQL = "SELECT COORDINATOR_VOLGNUMMER FROM PERSOONLIJKZORGNETWERK2 WHERE PZN_ID = ?";
        $_check = $this->form->database->userdb->GetOne($_SQL, array($pznid));
        return ($_check == $persoonvolgnummer);
    }

    function IsJeugdigeOfVerzorger($pznid, $persoonvolgnummer) {
        $_SQL = "SELECT COUNT(*) FROM PERSOONLIJKZORGNETWERK2
        WHERE PZN_ID = ? AND (JEUGDIGE_VOLGNUMMER = ? OR VERZORGER_VOLGNUMMER = ?)";
        $_checkCount = $this->form->database->userdb->GetOne($_SQL, array($pznid, $persoonvolgnummer, $persoonvolgnummer));
        return ($_checkCount == 1);
    }

    function IsNetwerkdeelnemer($pznid, $persoonvolgnummer) {
        $_SQL = "SELECT COUNT(*) FROM PZN_NETWERKDEELNEMER WHERE PZN = ? AND VOLGNUMMER = ?";
        $_count = $this->form->database->userdb->GetOne($_SQL, array($pznid, $persoonvolgnummer));
        return ($_count > 0);
    }

    function IsHuidigeDeelVanPZN($pznid) {
        $_persoonvolgnummer = GetPersoonVolgnummer($this->form->database, $_SESSION['userid']);
        return $this->IsNetwerkdeelnemer($pznid, $_persoonvolgnummer) or $this->IsJeugdigeOfVerzorger($pznid, $_persoonvolgnummer);
    }

    function IsHuidigeDeelnemerFromHash($deelnemerhash) {
        $_rs = $this->GetNetwerkDeelnemerFromHash($deelnemerhash);
        return ($_rs['USERGROUPID'] == $_SESSION['userid']);
    }

    function IsHuidigeDeelnemer($pzn, $deelnemervolgnummer) {
        $_rs = $this->GetNetwerkDeelnemer($pzn, $deelnemervolgnummer);
        return ($_rs[0]['USERGROUPID'] == $_SESSION['userid']);
    }

    function CirkelIsAfgerond($pzn, $deelnemervolgnummer) {
        $_rs = $this->GetNetwerkDeelnemer($pzn, $deelnemervolgnummer);
        return ($_rs[0]['CIRKELMODELAFGEROND'] !== '' and !empty($_rs[0]['CIRKELMODELAFGEROND']));
    }

    function Show($outputformat='xhtml', $rec=false) {
        global $_GVARS;
        global $polaris;

        $_plrinstance = $this->form->plrInstance();
        $_plrinstance->SetFetchMode(ADODB_FETCH_ASSOC);
        $this->form->database->userdb->SetFetchMode(ADODB_FETCH_ASSOC);

        parent::Show($outputformat, $rec);
//var_dump($this->moduleid);
//var_dump($this->form->record->FORMID);
        $this->ProcessJson();
        /**
        * Show the custommade forms when user views a list of items
        */

        if ($outputformat == 'xhtml') {
            switch ($this->moduleid) {
            //case 40000:     // Verzorger/Jeugdige
            case 8:     // beginnende PZN's
            case 30:    // Ouder
            case 20:    // actieve PZN's
                $_pznids = $this->GetPZNSFromUserID($_SESSION['userid']);
                switch($this->state) {
                case 'allitems':
                case 'search':
                    $this->AddModuleJS(['sharedlib.js', 'pzn.js']);
                    $this->ShowPersonenOverzicht($this->moduleid, $_pznids);
                break;
                case 'edit':
                case 'insert':
                    $this->AddCustomJS('/bower_components/enjoyhint/enjoyhint.js');
                    $this->AddCustomJS('/bower_components/Caret.js/dist/jquery.caret.js');
                    $this->AddCustomJS('/bower_components/At.js/dist/js/jquery.atwho.js');
                    $this->AddCustomJS('/bower_components/bootstrap3-typeahead/bootstrap3-typeahead.js');
                    $this->AddModuleJS(['TwbsToggleButtons.js', 'jquery.twbs-toggle-buttons.js'
                    , 'sharedlib.js', 'pzn.js', 'tour.js', 'chatsessie.js', 'jquery.autogrowtextarea.js']);
                    $this->showDefaultButtons = false;
                    $this->ShowPZNForm($this->moduleid, $_pznids);
                break;
                }
            break;
            case 4:     // Verzorger/Jeugdige
                $_pznids = $this->GetPZNSFromUserID($_SESSION['userid']);
                if (count($_pznids) == 1 or in_array($this->state, array('edit','insert'))) {
                    $this->AddCustomJS('/bower_components/enjoyhint/enjoyhint.js');
                    $this->AddCustomJS('/bower_components/Caret.js/dist/jquery.caret.js');
                    $this->AddCustomJS('/bower_components/At.js/dist/js/jquery.atwho.js');
                    $this->AddCustomJS('/bower_components/bootstrap3-typeahead/bootstrap3-typeahead.js');
                    $this->AddCustomJS('/bower_components/jquery.steps/build/jquery.steps.js');

                    $this->AddModuleJS(['TwbsToggleButtons.js', 'jquery.twbs-toggle-buttons.js'
                    , 'sharedlib.js', 'formwizard.js', 'pzn.js', 'tour.js', 'chatsessie.js', 'jquery.autogrowtextarea.js']);
                    $this->showDefaultButtons = false;
                    $this->ShowPZNForm($this->moduleid, $_pznids);
                } else {
                    $this->AddModuleJS(['sharedlib.js', 'pzn.js']);
                    $this->ShowPersonenOverzicht($this->moduleid, $_pznids);
                }
            break;
            case 10:    // Cirkelmodel
                if ($outputformat == 'xhtml') {
                    $this->AddCustomJS('/bower_components/snap.svg/dist/snap.svg.js');
                    $this->AddCustomJS('/js/plugins/chosen/chosen.jquery.js');
                    $this->AddModuleJS(['sharedlib.js', 'cirkelmodel.js', 'cirkeloverzicht.js']);
                    if (isset($_GET['pznid'])) {
                        $this->ShowCirkelModel($_GET['pznid']);
                    }
                }
            break;
            case 15:    // Uitnodigingsformulier
                switch($this->state) {
                case 'allitems':
                case 'search':
                    $this->AddCustomJS('/bower_components/peity/jquery.peity.js');
                    $this->AddModuleJS(['sharedlib.js', 'uitnodigen.js']);

                    $this->ShowUitnodigingOverzicht($this->moduleid);
                break;
                case 'edit':
                case 'insert':
                    $this->AddModuleJS(['sharedlib.js', 'uitnodigen.js']);
                    $this->ShowPZNUitnodigingForm($this->moduleid);
                break;
                }
            break;
            case 23:    // Persoon formulier
                switch($this->state) {
                case 'allitems':
                case 'search':
                    $this->showDefaultButtons = true;
                    $this->ShowListView($this->moduleid);
                break;
                case 'edit':
                case 'insert':
                    $this->showDefaultButtons = true;
                    $this->ShowPersoonForm();
                break;
                }
            break;
            }
        }
    }
}

// delete from `pzn_netwerkdeelnemer` where pzn in (20171127001,20171129001,20171115002,20171125001);
// delete from `pzn_oudervoogd` where pzn in (20171127001,20171129001,20171115002,20171125001);
// delete from `pzn_profiel` where pzn in (20171127001,20171129001,20171115002,20171125001);
// delete from `persoonlijkzorgnetwerk2` where pzn_id in (20171127001,20171129001,20171115002,20171125001);
// delete from pzn_zorgbesteed where probleem_volgnummer not in (select volgnummer from pzn_profiel);

// insert into pzn_versie (pzn, versie)
// (select distinct pzn_id, 1 from `persoonlijkzorgnetwerk2` where pzn_id not in (select pzn from pzn_versie))


// insert into pzn_versie (pzn, versie)
// select distinct pzn, versie from pzn_profiel
// where (pzn, versie) not in (select pzn, versie from pzn_versie)
// order by pzn, versie