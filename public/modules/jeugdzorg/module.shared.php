<?php
require_once '_shared/module._base.php';
require_once 'jeugdzorg/module.pzn/php/misc.php';

class ModuleShared extends _BaseModule {
    var $client;
    var $database;

    function __construct($moduleid, $module, $_clientname) {
        parent::__construct($moduleid, $module);

        $this->databaseid = 1;
    }

    function GetNewPZNID() {
        $_sql = "
            SELECT IFNULL(MAX(PZN_ID), CONCAT(DATE_FORMAT(NOW(), '%Y%m%d'), '000')) + 1
            FROM PERSOONLIJKZORGNETWERK2
            WHERE PZN_ID LIKE CONCAT(DATE_FORMAT(NOW(), '%Y%m%d'), '%')
        ";
        return $this->database->userdb->GetOne($_sql);
    }

    function InsertOrUpdatePersoon($fields, $emailadres) {
        $_volgnummer = $this->database->userdb->GetOne('SELECT VOLGNUMMER FROM PZN_PERSOON WHERE E_MAILADRES = "'.$emailadres.'"');
        if (empty($emailadres) or empty($_volgnummer)) {
                $_result = $this->database->userdb->AutoExecute('PZN_PERSOON', $fields, 'INSERT');
                $_id = $this->database->userdb->Insert_ID();
        } else {
            if ($_volgnummer) {
                $_result = $this->database->userdb->AutoExecute('PZN_PERSOON', $fields, 'UPDATE', 'VOLGNUMMER = '.$_volgnummer, true);
                $_id = $_volgnummer;
            }
        }
        return $_id;
    }

    function isExtraOuder($soortouder) {
        return in_array($soortouder, array('Ouder', 'Pleegouder')); //, 'Verzorger'
    }

    function CreateAccount($user) {
        if (empty($user['LINKHASH'])) return;

        global $polaris;

        $_clientid = $this->client->record->CLIENTID;
        $_usergroupid = $user['USERGROUPID'];
        $this->database = new base_plrDatabase($this->client);
        $this->database->LoadRecord(array($_clientid, $this->databaseid));
        $this->database->connectUserDB();
        $this->database->userdb->SetFetchMode(ADODB_FETCH_ASSOC);

        /**
         * use the LINKHASH to find the correct record in the initial table (PZN_UITNODIGING)
         * and set the status to Aangemeld
         */
        $_where = 'MD5(CONCAT(ID, "'.$this->client->getGlobalScrambler().'")) = "'.$user['LINKHASH'].'"';
        $_record["STATUS"] = "AANGEMELD";
        $this->database->userdb->AutoExecute('PZN_UITNODIGING', $_record, 'UPDATE', $_where);

        /**
         * create PERSOON record
         */
        $_uitnodiging = $this->database->userdb->GetRow('SELECT * FROM PZN_UITNODIGING WHERE '.$_where);

        // VTA van uitnodiging opslaan als PERSOON
        // ALS SOORTUITNODIGING gelijk is aan Verzorger dan
        // aparte PERSOON Opvoeren
        // ANDERS emailadres opslaan bij Jeugdige PERSOON
        if ($_uitnodiging["SOORTUITNODIGING"] == 'Jeugdige') {
            unset($_record);
            $_record["VOORNAAM"] = $_uitnodiging['VOORNAAM'];
            $_record["ACHTERNAAM"] = $_uitnodiging['ACHTERNAAM'];
            $_record["ROL"] = 'Jeugdige';
            $_record["E_MAILADRES"] = $_uitnodiging["UITGENODIGDE_EMAIL"];
            $_record["CLIENTID"] = $_clientid;
            $_record["USERGROUPID"] = $_usergroupid;
            $_record["TELEFOONNUMMER"] = $user["PHONENUMBER"];
            $_idJeugdige = $this->InsertOrUpdatePersoon($_record, $_record["E_MAILADRES"]);
            $_idVerzorger = null;
        } elseif ($_uitnodiging["SOORTUITNODIGING"] == 'Verzorger') {
            unset($_record);
            $_record["VOORNAAM"] = $_uitnodiging['VOORNAAM'];
            $_record["ACHTERNAAM"] = $_uitnodiging['ACHTERNAAM'];
            $_record["ROL"] = 'Jeugdige';
            $_record["E_MAILADRES"] = null;
            $_record["CLIENTID"] = null;
            $_record["USERGROUPID"] = null;
            $_idJeugdige = $this->InsertOrUpdatePersoon($_record, null);

            unset($_record);
            $_record["VOORNAAM"] = $_uitnodiging['VOORNAAM_VERZORGER'];
            $_record["ACHTERNAAM"] = $_uitnodiging['ACHTERNAAM_VERZORGER'];
            $_record["ROL"] = 'Verzorger';
            $_record["E_MAILADRES"] = $_uitnodiging["UITGENODIGDE_EMAIL"];
            $_record["CLIENTID"] = $_clientid;
            $_record["USERGROUPID"] = $_usergroupid;
            $_record["TELEFOONNUMMER"] = $user["PHONENUMBER"];
            $_idVerzorger = $this->InsertOrUpdatePersoon($_record, $_record["E_MAILADRES"]);
        } elseif ($_uitnodiging["SOORTUITNODIGING"] == 'Netwerk') {
            unset($_record);
            $_record["VOORNAAM"] = $_uitnodiging['VOORNAAM_VERZORGER'];
            $_record["ACHTERNAAM"] = $_uitnodiging['ACHTERNAAM_VERZORGER'];
            $_record["ROL"] = 'Verzorger';
            $_record["E_MAILADRES"] = $_uitnodiging["UITGENODIGDE_EMAIL"];
            $_record["CLIENTID"] = $_clientid;
            $_record["USERGROUPID"] = $_usergroupid;
            $_record["TELEFOONNUMMER"] = $user["PHONENUMBER"];
            $_idZorgverlener = $this->InsertOrUpdatePersoon($_record, $_record["E_MAILADRES"]);
        } elseif ($this->isExtraOuder($_uitnodiging["SOORTUITNODIGING"])) {
            unset($_record);
            $_record["VOORNAAM"] = $_uitnodiging['VOORNAAM_VERZORGER'];
            $_record["ACHTERNAAM"] = $_uitnodiging['ACHTERNAAM_VERZORGER'];
            $_record["ROL"] = 'Verzorger';
            $_record["E_MAILADRES"] = $_uitnodiging["UITGENODIGDE_EMAIL"];
            $_record["CLIENTID"] = $_clientid;
            $_record["USERGROUPID"] = $_usergroupid;
            $_idZorgverlener = $this->InsertOrUpdatePersoon($_record, $_record["E_MAILADRES"]);
        }

        BewaarPersoonPermissies($_clientid, $_record["USERGROUPID"], $_uitnodiging["SOORTUITNODIGING"]);

        if ($_uitnodiging["SOORTUITNODIGING"] !== 'Netwerk' and !$this->isExtraOuder($_uitnodiging["SOORTUITNODIGING"]) and !empty($_uitnodiging["ZORGVERLENER_VOLGNUMMER"])) {
            /**
             * Start PZN record
             */
            unset($_record);
            $_pzn = $this->GetNewPZNID();
            $_record["PZN_ID"] = $_pzn;
            $_record["AANMAAKDATUM"] = date("Y-m-d H:i:s");
            $_record["UITNODIGINGSDATUM"] = $_uitnodiging["DATUM_UITNODIGING"];
            $_record["RELATIE_TOV_ZORGGERECHTIGDE"] = $_uitnodiging["SOORTUITNODIGING"];
            $_record["COORDINATOR_VOLGNUMMER"] = $_uitnodiging["ZORGVERLENER_VOLGNUMMER"];
            $_record["VERZORGER_VOLGNUMMER"] = $_idVerzorger;
            $_record["JEUGDIGE_VOLGNUMMER"] = $_idJeugdige;
            $_table = 'PERSOONLIJKZORGNETWERK2';
            $_result = $this->database->userdb->AutoExecute($_table, $_record, 'INSERT');

            /**
             * Maak eerste netwerkdeelnemer aan (coordinator)
             */
            unset($_record);
            $_record["PZN"] = $_pzn;
            $_record["VOLGNUMMER"] =  $_uitnodiging["ZORGVERLENER_VOLGNUMMER"];
            $_record["SOORTDEELNEMER"] = 'Coordinator';
            $_record["ISCOORDINATOR"] = 'J';
            $_soortZorg = GetSoortZorg($this->database->userdb, $_record["VOLGNUMMER"]);
            $_record["SOORTZORG"] = $_soortZorg;
            $_record["UITGENODIGD"] = 'J';
            $_record["DATUM_UITGENODIGD"] = date("Y-m-d H:i:s");
            $_record["STATUS"] = 'AANGEMELD';
            $_table = 'PZN_NETWERKDEELNEMER';
            $_result = $this->database->userdb->AutoExecute($_table, $_record, 'INSERT');

            /**
             * Maak jeugdige/verzorger aan als netwerkdeelnemer zodat deze het cirkelmodel kan invullen
             */
            unset($_record);
            $_record["PZN"] = $_pzn;
            $_record["VOLGNUMMER"] =  isset($_idVerzorger) ? $_idVerzorger : $_idJeugdige;
            $_record["SOORTDEELNEMER"] = $_uitnodiging['SOORTUITNODIGING'];
            $_record["ISCOORDINATOR"] = 'N';
            $_soortZorg = GetSoortZorg($this->database->userdb, $_record["VOLGNUMMER"]);
            $_record["SOORTZORG"] = $_uitnodiging["SOORTUITNODIGING"];
            $_record["UITGENODIGD"] = 'J';
            $_record["DATUM_UITGENODIGD"] = date("Y-m-d H:i:s");
            $_record["STATUS"] = 'AANGEMELD';
            $_table = 'PZN_NETWERKDEELNEMER';
            $_result = $this->database->userdb->AutoExecute($_table, $_record, 'INSERT');

            /**
             * Start een initiele versie van Cirkelmodel
             */
            unset($_record);
            $_record["PZN"] = $_pzn;
            $_record["VERSIE"] = 1;
            $_record["OMSCHRIJVING"] =  'Initiële versie';
            $_table = 'PZN_VERSIE';
            $_result = $this->database->userdb->AutoExecute($_table, $_record, 'INSERT');
        }

        return $_result;
    }
}