<?php
// use Michelf\MarkdownExtra;
// use League\CommonMark\CommonMarkConverter;
use League\CommonMark\Environment\Environment;
use League\CommonMark\Extension\CommonMark\CommonMarkCoreExtension;
use League\CommonMark\Extension\GithubFlavoredMarkdownExtension;
use League\CommonMark\Extension\Attributes\AttributesExtension;
use League\CommonMark\Extension\DefaultAttributes\DefaultAttributesExtension;
use League\CommonMark\MarkdownConverter;

require('_shared/module.plr_maxia.php');

class ModulePLR_Maxia_Logboek extends ModulePLR_Maxia {
    var $stylesheet;

    const LOGBOEK = 1;

    function __construct($moduleid, $module, $clientname=false) {
        parent::__construct($moduleid, $module, $clientname);
        $this->stylesheet = 'default.css';
    }

    function __GetCustomFilter() {
        $_filter = $this->form->AddDataScopeFilter();
        return $_filter;
    }

    function ProcessFiles($files) {
    }

    function ProcessPostRequests($record, $event){
        $this->CheckedLoggedIn();

        switch ($event) {
            case 'logboek_toevoegen':
                $_result = $this->BewaarLogboekNotitie($record);
                break;
            case 'logboekitem_updaten':
                $_result = $this->WijzigLogboekItem($record);
                break;
            case 'logboek_item_verwijderen':
                $_result = $this->UpdateLogboekItem($record['PLR__RECORDID'], 'prullenbak');
                break;
            case 'logboek_item_permanentverwijderen':
                $_result = $this->verwijderPermanentLogboekItem($record['PLR__RECORDID']);
                break;
            case 'logboek_item_archiveren':
                $_result = $this->ArchiveerLogboekItem($record['PLR__RECORDID'], $record['aanuit']);
                break;
            case 'logboek_item_vastpinnen':
                $_result = $this->PinnenLogboekItem($record['PLR__RECORDID'], $record['aanuit']);
                break;
            case 'logboek_item_herstellen':
                $_result = $this->UpdateLogboekItem($record['PLR__RECORDID'], 'actief');
                break;
            case 'logboek_prullenbak_leegmaken':
                $_result = $this->PrullenbakLeegmaken();
                break;
            case 'logboek_item_kopieren':
                $_result = $this->KopieerLogboekItem($record['PLR__RECORDID']);
                break;
            case 'logboek_item_toggle_checkbox':
                $_result = $this->toggleCheckboxLogboekItem($record['PLR__RECORDID'], $record['aanuit'], $record['checkbox']);
                break;
            case 'logboek_item_wijzig_checkboxes':
                $_result = $this->wijzigCheckboxesLogboekItem($record['PLR__RECORDID']);
                break;
        }
        return $_result;
    }

    function ProcessShowRequests($record, $event){
        $_content = false;
        // $this->form->database->userdb->SetFetchMode(ADODB_FETCH_ASSOC);

        switch ($event) {
            case 'logboek':
                $_content = $this->GetLogboek($_GET['selectie'], $_GET['periode'], $_GET['datum'], $_GET['q']);
                break;
            case 'logboek_item_details':
                $_content = $this->GetLogboekItemDetails($record['id']);
                break;
            default:
                break;
        }

        if ($_content !== false) {
            echo $_content;
            exit; // stop with the rest of the html
        }
    }

    function BewaarLogboekNotitie($record) {
        $_result = false;
        $_opmerking = $record['NOTITIE'];
        // $_favoriet = $record['FAVORIET'];
        $_tags = $record['TAGS'];
        $_status = $record['STATUS'] ?? 'actief';
        $_tijdstip = date('Y-m-d H:i:s');
        $_locatie = $this->GetActieveDefaultLocatie();

        $_sql = "INSERT INTO mx_logboek
        (OPMERKING, GEBRUIKERSNAAM, TIJDSTIP, LOCATIE, TAGS, `STATUS`, CLIENT_HASH) VALUES (?, ?, ?, ?, ?, ?, ?)";
        $_result = $this->form->database->userdb->Execute($_sql, [$_opmerking, $_SESSION['name'], $_tijdstip, $_locatie, $_tags, $_status, $this->clienthash]);

        return $_result;
    }

    function WijzigLogboekItem($record) {
        $_result = false;
        $_opmerking = $record['NOTITIE'];
        $_recordID = $record['PLR__RECORDID'];

        $_sql = "UPDATE mx_logboek ttt SET OPMERKING = ? WHERE {$this->__GetCustomFilter()} AND {$this->form->database->makeRecordIDColumn('mx_logboek')} = ?";
        $_result = $this->form->database->userdb->Execute($_sql, [$_opmerking, $_recordID]);

        return $_result;
    }

    function UpdateLogboekItem($recordID, $status) {
        $_result = false;

        if (in_array($status, ['sjablonen', 'actief', 'prullenbak', 'archief', 'pin']) === false) {
            return $_result;
        }
        $_sql = "UPDATE mx_logboek ttt SET status = ? WHERE {$this->__GetCustomFilter()} AND {$this->form->database->makeRecordIDColumn('mx_logboek')} = ?";
        $_result = $this->form->database->userdb->Execute($_sql, [$status, $recordID]);

        return $_result;
    }

    function verwijderPermanentLogboekItem($recordID) {
        $_sql = "UPDATE mx_logboek ttt SET __DELETED = 1 WHERE {$this->__GetCustomFilter()} AND {$this->form->database->makeRecordIDColumn('mx_logboek')} = ?";
        $_result = $this->form->database->userdb->Execute($_sql, [$recordID]);

        return $_result;
    }

    function ArchiveerLogboekItem($recordID, $aanuit) {
        $status = ($aanuit === 'aan') ? 'archief' : 'actief';
        $sql = "UPDATE mx_logboek ttt SET status = ? WHERE {$this->__GetCustomFilter()} AND {$this->form->database->makeRecordIDColumn('mx_logboek')} = ?";
        $params = [$status, $recordID];
        $result = $this->form->database->userdb->Execute($sql, $params);

        return $result;
    }

    function PinnenLogboekItem($recordID, $aanuit) {
        $favoriet = ($aanuit === 'aan') ? 'Y' : 'N';
        $sql = "UPDATE mx_logboek ttt SET favoriet = ? WHERE {$this->__GetCustomFilter()} AND {$this->form->database->makeRecordIDColumn('mx_logboek')} = ?";
        $params = [$favoriet, $recordID];
        $result = $this->form->database->userdb->Execute($sql, $params);

        return $result;
    }

    function PrullenbakLeegmaken() {
        $_sql = "UPDATE mx_logboek ttt SET __DELETED = 1 WHERE {$this->__GetCustomFilter()} AND status = 'prullenbak'";
        $_result = $this->form->database->userdb->Execute($_sql);

        return $_result;
    }

    function KopieerLogboekItem($recordID) {
        $_logitem = $this->_GetLogboekItemDetails($recordID);

        $_opmerking = $this->toggleCheckboxesOpmerking($_logitem['OPMERKING'], 'uit');

        $_sql = "
        INSERT INTO mx_logboek
        (OPMERKING, GEBRUIKERSNAAM, TIJDSTIP, LOCATIE, TAGS, `STATUS`, CLIENT_HASH)
        VALUES (?, ?, NOW(), ?, ?, ?, ?)
        ";
        $_result = $this->form->database->userdb->Execute($_sql,
            [
                $_opmerking,
                $_SESSION['name'],
                $_logitem['LOCATIE'],
                $_logitem['TAGS'],
                $_logitem['STATUS'],
                $this->clienthash,
            ]
        );

        return $_result;
    }

    function toggleCheckboxLogboekItem($recordID, $aanuit, $tekst) {
        if (empty($tekst)) {
            return false;
        }
        $_logitem = $this->_GetLogboekItemDetails($recordID);
        $_nieuweOpmerking = $_logitem['OPMERKING'];
        // replace - [ ] foo with - [x] foo or - [x] foo with - [ ] foo, where foo is the $tekst
        // even if the $tekst has characters like ** and ~~
        $_nieuweOpmerking = preg_replace("/(- \[)( |x)(\] (([~\*]*){$tekst}([~\*]*)))$/m", ($aanuit === 'aan') ? "- [x] $5{$tekst}$6" : "- [ ] $5{$tekst}$6", $_nieuweOpmerking);

        if (empty($_nieuweOpmerking) || $_nieuweOpmerking == $_logitem['OPMERKING']) {
            return false;
        }

        $_sql = "UPDATE mx_logboek ttt SET OPMERKING = ? WHERE {$this->__GetCustomFilter()} AND {$this->form->database->makeRecordIDColumn('mx_logboek')} = ?";
        $_result = $this->form->database->userdb->Execute($_sql, [$_nieuweOpmerking, $recordID]);

        return $_result;
    }

    function toggleCheckboxesOpmerking($opmerking, $aanuit) {
        // Check or uncheck all checkboxes in the opmerking $_nieuweOpmerking
        $opmerking = preg_replace("/(- \[)( |x)(\] (.*))$/m", ($aanuit === 'aan') ? "- [x] $4" : "- [ ] $4", $opmerking);

        return $opmerking;
    }

    function _wijzigCheckboxesOpmerking($opmerking, $aanuit) {
        // Prefix every line that has content with "- [ ]"
        // or remove "- [ ]" from every line
        if ($aanuit === true) {
            $opmerking = preg_replace("/^(.+)/m", "- [ ] $1", $opmerking);
        } else {
            $opmerking = preg_replace("/- \[( |x)\] /m", "", $opmerking);
        }

        return $opmerking;
    }

    function wijzigCheckboxesLogboekItem($recordID) {
        $_logitem = $this->_GetLogboekItemDetails($recordID);
        $_nieuweOpmerking = $_logitem['OPMERKING'];

        // determine if every line is prefixed with "- [ ]", if so than set $aanuit to True
        // if every line is not prefixed with "- [x]", set $aanuit to False
        $aanuit = (preg_match("/^(- \[ \].+)$/m", $_nieuweOpmerking)) ? true : false;

        // Now toggle the checkboxes
        $aanuit = !$aanuit;

        $_nieuweOpmerking = $this->_wijzigCheckboxesOpmerking($_nieuweOpmerking, $aanuit);

        if ($_nieuweOpmerking == $_logitem['OPMERKING']) {
            return false;
        }

        $_sql = "UPDATE mx_logboek ttt SET OPMERKING = ? WHERE {$this->__GetCustomFilter()} AND {$this->form->database->makeRecordIDColumn('mx_logboek')} = ?";
        $_result = $this->form->database->userdb->Execute($_sql, [$_nieuweOpmerking, $recordID]);

        return $_result;
    }

    function _GetLogboekItemDetails($recordID) {
        $_sql = "
        SELECT GEBRUIKERSNAAM, OPMERKING, TIJDSTIP, FAVORIET, LOCATIE, TAGS, `STATUS`
        , {$this->form->database->makeRecordIDColumn('mx_logboek')} AS PLR__RECORDID
        FROM mx_logboek ttt
        WHERE {$this->__GetCustomFilter()} AND {$this->form->database->makeRecordIDColumn('mx_logboek')} = ?";
        $_result = $this->form->database->userdb->GetRow($_sql, [$recordID]);

        return $_result;
    }

    function GetLogboekItemDetails($recordID) {
        return json_encode($this->_GetLogboekItemDetails($recordID));
    }

    function GetPeriode($periode, $datum) {
        $_periode = '';
        switch ($periode) {
            case 'vandaag':
                $_periode = "DATE(TIJDSTIP) = CURDATE()";
                break;
            case 'week':
                $_periode = "YEARWEEK(TIJDSTIP) = YEARWEEK(CURDATE())";
                break;
            case 'keuze':
                if (empty($datum)) {
                    $datum = date('Y-m-d');
                } else {
                    // convert $datum from d-m-Y to Y-m-d

                    if (strtotime($datum) === false) {
                        $datum = date('Y-m-d');
                    } else {
                        $datum = date('Y-m-d', strtotime($datum));
                    }
                }

                $_periode = "DATE(TIJDSTIP) = '{$datum}'";
                break;
            default:
                $_periode = "1=1";
                break;
        }
        // pr(strtotime($datum), $datum, $_periode);

        return $_periode;
    }

    function GetSelectie($selectie) {
        $_selectie = ['actief'];
        if (!empty($selectie)) {
            if (in_array($selectie, ['sjablonen', 'archief', 'prullenbak'])) {
                $_selectie = [$selectie];
            }
            if ($selectie == 'actief') {
                $_selectie = ['actief', 'pin'];
            }
        }
        return $_selectie;
    }

    function GetSearchString($searchvalue) {
        $_searchstring = '';
        if ($searchvalue) {
            // $_searchvalue = $this->form->database->escape($searchvalue);
            $searchvalue = implode('%\' OR OPMERKING LIKE \'%', $searchvalue);
            $searchvalue = "'%{$searchvalue}%'";
            $_searchstring = "AND (OPMERKING LIKE {$searchvalue})";
        }
        return $_searchstring;
    }

    function ProcessSQLQuery($selectie, $periode, $datum=false, $searchvalue=false) {
        $_selectie = $this->GetSelectie($selectie);
        $_selectieString = implode("','", $_selectie);
        $periode = in_array($periode, ['vandaag', 'week', 'alles', 'keuze']) ? $periode : 'vandaag';
        $_periode = $this->GetPeriode($periode, $datum);
        $_searchstring = $this->GetSearchString($searchvalue);

        $_sql = "
        SELECT l.LOCATIE_CODE, OPMERKING, GEBRUIKERSNAAM, FAVORIET, LOCATIE, TAGS, STATUS, TIJDSTIP, TIJDSTIP AS HUMANIZE_DATE
        , SUBSTR(GEBRUIKERSNAAM, 1, IF(POSITION('@' IN GEBRUIKERSNAAM) > 0, POSITION('@' IN GEBRUIKERSNAAM), 100)) AS GEBRUIKERSNAAM_KORT
        , DAYNAME(TIJDSTIP) AS DAGNAAM_LANG
        , DATE_FORMAT(TIJDSTIP , '%a' ) AS DAGNAAM
        , DATE_FORMAT(TIJDSTIP, '%d %b %Y') AS DATUM
        , DATE_FORMAT(TIJDSTIP, '%d %b') AS DATUM_KORT
        , DATE_FORMAT(TIJDSTIP, '%H:%i') AS TIJD
        , {$this->form->database->makeRecordIDColumn('mx_logboek', true)} AS PLR__RECORDID
        FROM mx_logboek ttt
        LEFT JOIN fb_locaties l ON ttt.locatie = l.locatie_id
        WHERE {$this->__GetCustomFilter()}
        AND status in ('{$_selectieString}')
        AND ({$_periode} OR FAVORIET = 'Y')
        {$_searchstring}
        AND (
        ? = 0  -- Als de parameter voor locatie 0 is, negeer de locatievoorwaarde
        OR ttt.locatie = ?
        OR ttt.locatie IS NULL
        OR ttt.locatie = 0
        )
        ORDER BY TIJDSTIP DESC";

        return $_sql;
    }

    function RetrieveLogEntries($sql, $locatie) {
        try {
            $_result = $this->form->database->userdb->GetAll($sql, [$locatie, $locatie]);
            $_result = $this->ProcessRecordset($_result);
        } catch (Exception $e) {
            $_result = [];
        }
        return $_result;
    }

    function ConfigureMarkdownParser() {
        $config = [
            'renderer' => [
                'block_separator' => "\n",
                'inner_separator' => "\n",
                'soft_break'      => "<br>",
            ],
            'commonmark' => [
                'enable_em' => true,
                'enable_strong' => true,
                'use_asterisk' => true,
                'use_underscore' => true,
                'unordered_list_markers' => ['-', '*', '+'],
            ],
            'html_input' => 'strip',
            'allow_unsafe_links' => false,
            'max_nesting_level' => 5,
            'slug_normalizer' => [
                'max_length' => 255,
            ],
        ];
        // Configure the Environment with all the CommonMark and GFM parsers/renderers
        $environment = new Environment($config);
        $environment->addExtension(new CommonMarkCoreExtension());
        $environment->addExtension(new GithubFlavoredMarkdownExtension());
        $environment->addExtension(new DefaultAttributesExtension());
        $environment->addExtension(new AttributesExtension());
        return new MarkdownConverter($environment);
    }

    function ConvertEntriesToMarkdown($entries, $parser) {
        foreach($entries as $key => $record) {
            if (!empty($record['OPMERKING'])) {
                $entries[$key]['OPMERKING'] = $parser->convert($record['OPMERKING'])->getContent();
            }
        }
        return $entries;
    }

    function GetLogboek($selectie, $periode, $datum=false, $searchvalue=false) {
        $_sql = $this->ProcessSQLQuery($selectie, $periode, $datum, $searchvalue);
        $_locatie = $this->GetActieveDefaultLocatie();
        $_result = $this->RetrieveLogEntries($_sql, $_locatie);
        $_parser = $this->ConfigureMarkdownParser();
        $_result = $this->ConvertEntriesToMarkdown($_result, $_parser);
        return json_encode($_result);
    }

    function ShowFormView($state, $permission, $detail=false, $masterrecord=false) {
        $this->smarty->assign('permission', $permission);
        $this->smarty->assign('logboek_mainjs', $this->modulepath.'js/logboek_main.js');

        $this->smarty->display("logboek_insert.tpl.php");
    }

    function ShowListView($state, $permission, $detail=false, $masterrecord=false) {
        $this->AddCustomJS(['/modules/_shared/module.plr_maxia/js/maxia.js']);
        $this->AddModuleJS(['logboek_main.js']);

        $this->CheckHuidigeSessieLocatie();
        $this->smarty->assign('permission', $permission);
        $this->smarty->display("logboek.tpl.php");
    }

}
