<?php
require '_shared/module.plr_maxia.php';

use chillerlan\QRCode\QRCode;
use chillerlan\QRCode\QROptions;
use Fpdf\Fpdf;

// Custom PDF class to allow SVG insertion
class PDF extends Fpdf {
    function ImageSVG($file, $x=null, $y=null, $w=0, $h=0, $link='') {
        $this->Image($file, $x, $y, $w, $h, 'SVG', $link);
    }
}

class ModulePLR_Maxia_Ideeenbus extends ModulePLR_Maxia {
    var $stylesheet;
    var $gen_form;
    var $client;

    function __construct($moduleid, $module, $clientname=false) {
        parent::__construct($moduleid, $module, $clientname);

        $this->stylesheet = 'default.css';
    }

    function __GetCustomFilter() {
        $_customfilter = $this->form->AddDataScopeFilter('');
        return $_customfilter;
    }

    private function getQRCodeRecord($hash) {
        global $scramble;
        global $polaris;
        global $ClientHash;

        $_sql = "
        SELECT
            QRCODE_ID,
            CLIENT_HASH,
            PLEK,
            ACHTERGRONDKLEUR,
            LETTERKLEUR,
            ACTIEF
        FROM
            ib_qrcodes
        WHERE
            __DELETED = 0
            -- AND ACTIEF = 'Y'
            AND MD5(CONCAT('{$scramble}', '_', CLIENT_HASH, '_', QRCODE_ID)) = ?
        ";
        if (isset($this->form)) {
            $row = $this->form->database->userdb->GetRow($_sql, [$hash]);
        } else {
            $row = $this->userdatabase->userdb->GetRow($_sql, [$hash]);
        }

        if (count($row) > 0) {
            $_sqlClient = "
            SELECT
                NAME
            FROM
                plr_client
            WHERE
                {$ClientHash} = ?
            ";
            $_client = $this->polaris->instance->GetRow($_sqlClient, [$row['CLIENT_HASH']]);
            $row['CLIENTNAME'] = $_client['NAME'];
            return $row;
        } else {
            return false;
        }
    }

    private function VoegIdeeToe($record) {
        global $polaris;

        //**** Keep everything simple because the receiver is a simple form */

        // Decode the id to get the hash
        $hash = decodeStringForUrl($_GET['id']);

        if (!isset($hash)) {
            return ['success' => false, 'message' => 'Er is een fout opgetreden bij het toevoegen van het idee.'];
        }

        // Get the QR code record
        $qrCodeRecord = $this->getQRCodeRecord($hash);
        // Prepare the insert data
        $insertData = [
            $this->client->record->CLIENTHASHID,
            $qrCodeRecord['QRCODE_ID'],
            $record['idee'],
            $record['naam'],
            $record['team'],
            'nieuw',
            $_SERVER['REMOTE_ADDR'],
            $_SERVER['HTTP_USER_AGENT']
        ];

        // Create the SQL insert statement
        $sql = "INSERT INTO ib_ideeenbus
                (client_hash, qr_code_id, beschrijving, indiener_naam, indiener_team, status, ip_adres, user_agent)
                VALUES
                (?, ?, ?, ?, ?, ?, ?, ?)";

        // Execute the insert
        try {
            $result = $this->userdatabase->userdb->Execute($sql, $insertData);

            if ($result) {
                return ['success' => true, 'message' => 'Idee succesvol toegevoegd.'];
            } else {
                return ['success' => false, 'message' => 'Er is een fout opgetreden bij het toevoegen van het idee.'];
            }
        } catch (Exception $e) {
            return ['success' => false, 'message' => 'Database fout: ' . $e->getMessage()];
        }
    }

    function ProcessPostRequests($record, $event){
        switch ($event) {
            case 'ideeenbusform':
                $_result = $this->VoegIdeeToe($record);
                break;
        }
        return $_result;
    }

    function ProcessShowRequests($record, $event) {
        switch ($event) {
            case 'ideeenbusposter':
                $justdisplay = false;
                $_result = $this->DownloadPoster($record['rec'], $record['clientname'], $justdisplay);
                // exit; // PDF is downloaded and displayed in the browser, no more processing
                break;
        }
        return $_result;
    }

    function ProcessRequest($event=false) {
        // parent::ProcessRequest($event);
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $result = $this->ProcessPostRequests($_POST, $event) ?? null;
        } else {
            $result = $this->ProcessShowRequests($_GET, $event) ?? null;
        }
        return $result;
    }
//

    private function verhoogAantalBekeken($qrcode_id) {
        $sql = "UPDATE ib_qrcodes SET aantal_views = COALESCE(aantal_views, 0) + 1 WHERE QRCODE_ID = ?";
        $this->userdatabase->userdb->Execute($sql, [$qrcode_id]);
    }

    function ShowIdeeenBusForm($id) {
        $decodedID = decodeStringForUrl($id);
        $item = $this->getQRCodeRecord($decodedID);

        $this->verhoogAantalBekeken($item['QRCODE_ID']);

        $this->smarty->assign('item', $item);
        $this->smarty->assign('id', $id);

        echo $this->smarty->Fetch('ideeenbusform.tpl.html');
    }

    function ShowBedanktBoodschap() {
        echo $this->smarty->Fetch('bedanktboodschap.tpl.html');
    }

    function ShowFormView($state, $permission, $detail=false, $masterrecord=false) {
        global $polaris;

        $this->ShowModuleFormview();
    }

    function CreateQRCodeURL($clienthash, $qrcode_id) {
        global $_GVARS;

        $baseUrl = $_GVARS['serverroot']."/ideeenbus/{id}/"; // Base URL

        $_id = $clienthash.'_'.$qrcode_id;
        $_id = encodeString($_id);
        $_id = encodeStringForUrl($_id);

        $uniqueUrl = str_replace('{id}', $_id, $baseUrl);
        return $uniqueUrl;
    }

    function ShowListView($state, $permission, $detail=false, $masterrecord=false) {
        global $polaris;
        global $_GVARS;

        $_customfilter = $this->__GetCustomFilter();

        $this->AddCustomJS(['/modules/_shared/module.plr_maxia/js/maxia.js']);
        $this->AddModuleJS(['maxia_ideeenbus.js']);

        $_items = $this->form->GetViewContent($detail=false, $masterrecord=false, $limit=false, $_customfilter);
        // loop the $_items and add the qrcode to the items

        $baseUrl = $_GVARS['serverroot']."/ideeenbus/{id}/"; // Base URL

        $optionsDownload = new QROptions([
            'outputType' => QRCode::OUTPUT_MARKUP_SVG,
            'scale' => 5,
            'imageTransparent' => false,
            'eccLevel' => QRCode::ECC_L,
        ]);

        // it's generally a good idea to wrap the reader in a try/catch block because it WILL throw eventually
        try{
            foreach ($_items as $i => $item) {
                // Assume each item has a unique 'id' field. Adjust if needed.
                $uniqueUrl = $this->CreateQRCodeURL($item['CLIENT_HASH'], $item['QRCODE_ID']);
                $qr = new QRCode($optionsDownload);
                $qrcode = $qr->render($uniqueUrl);
                $_items[$i]['qrcode'] = $qrcode;
                $_items[$i]['url'] = $uniqueUrl;
            }
        } catch (Exception $e) {
            echo "Error generating QR code: " . $e->getMessage();
        }

        switch($this->moduleid) {
        case 1:
            $this->smarty->assign('items', $_items);
            $this->smarty->assign('selecteditem', $_GET['cursuscode']);
            $this->smarty->assign('stylesheet', $this->stylesheet);
            echo $this->smarty->Fetch('qrcodes.tpl.html');
        break;
        }
    }

    function CreatePoster($options) {
        global $_GVARS;

        $fontPath = $_GVARS['docroot'] . '/assets/css/bree-serif-v17-latin/';
        $fontFile = 'BreeSerif-Regular.php';

        // Create PDF with Unicode support
        $pdf = new FPDF('P', 'mm', 'A4');
        $pdf->AddPage();

        // Add the Bree Serif font with Unicode support
        $pdf->AddFont('BreeSerif', '', $fontFile, $fontPath);
        $pdf->SetFont('BreeSerif', '', 86);

        // Convert the text to UTF-8 and then to ISO-8859-1 (Latin-1)
        $title = iconv('UTF-8', 'ISO-8859-1//TRANSLIT', 'Ideeënbus');
        // Set background color
        $backgroundColor = sscanf($options['achtergrondkleur'], "#%02x%02x%02x");
        $pdf->SetFillColor($backgroundColor[0], $backgroundColor[1], $backgroundColor[2]);
        $pdf->Rect(0, 0, 210, 297, 'F');

        $textColor = sscanf($options['letterkleur'], "#%02x%02x%02x");
        $pdf->SetTextColor($textColor[0], $textColor[1], $textColor[2]);
        $pdf->SetXY(10, 20);
        $pdf->Cell(190, 20, $title, 0, 1, 'C');

        // Add instructions
        $pdf->SetFont('BreeSerif', '', 28);
        $pdf->SetXY(10, 55);

        // Calculate the line height (adjust the multiplier as needed)
        $lineHeight = 13;

        $pdf->MultiCell(190, $lineHeight, "Scan de QR code met je telefoon\nen deel jouw idee, opmerking of\nsuggestie.", 0, 'C');

        // Add "Makkelijk en anoniem" text
        // $pdf->SetFont('BreeSerif', '', 14);
        $pdf->SetXY(10, 100);
        $pdf->Cell(190, 10, "Makkelijk en anoniem.", 0, 1, 'C');

        // Generate QR code
        $optionsDownload = new QROptions([
            'outputType' => QRCode::OUTPUT_IMAGE_PNG,
            'scale' => 5,
            'imageTransparent' => false,
            'eccLevel' => QRCode::ECC_L,
            'outputBase64' => false
        ]);

        $qr = new QRCode($optionsDownload);
        $qrImageData = $qr->render($options['url']);

        // Save QR code to temporary file
        $tempFile = tempnam(sys_get_temp_dir(), 'qr_') . '.png';
        if (file_put_contents($tempFile, $qrImageData) === false) {
            throw new Exception("Unable to write QR code to temporary file.");
        }

        // Add QR code to PDF
        $pdf->Image($tempFile, 60, 125, 90, 90);

        // Delete temporary file
        if (!unlink($tempFile)) {
            error_log("Warning: Unable to delete temporary file: $tempFile");
        }

        // Add company name
        $pdf->SetFont('BreeSerif', '', 38);
        $pdf->SetXY(10, 235);
        $pdf->Cell(190, 10, $options['bedrijfsnaam'], 0, 1, 'C');

        // Add "Sociale werkdomein" text
        $pdf->SetFont('BreeSerif', '', 18);
        $pdf->SetXY(10, 258);
        $pdf->Cell(190, 10, "Maxia.nl", 0, 1, 'C');
        $pdf->SetFont('BreeSerif', '', 12);
        $pdf->SetXY(10, 266);
        $pdf->Cell(190, 10, "Software voor het sociale werkdomein", 0, 1, 'C');

        // Return PDF as string
        return $pdf->Output('S');
    }

    function DownloadPoster($qrCodeID, $bedrijfsnaam, $display = false) {

        $qrCodeRecord = $this->getQRCodeRecord($qrCodeID);
        $_options = [
            'url' => $this->CreateQRCodeURL($qrCodeRecord['CLIENT_HASH'], $qrCodeRecord['QRCODE_ID']),
            'bedrijfsnaam' => $qrCodeRecord['CLIENTNAME'],
            'achtergrondkleur' => $qrCodeRecord['ACHTERGRONDKLEUR'],
            'letterkleur' => $qrCodeRecord['LETTERKLEUR'],
        ];

        $_poster = $this->CreatePoster($_options);

        // Set PDF headers
        header('Content-Type: application/pdf');

        if ($display) {
            // Display in browser
            header('Content-Disposition: inline; filename="ideeenbus_poster.pdf"');
        } else {
            // Download
            header('Content-Disposition: attachment; filename="ideeenbus_poster.pdf"');
        }

        header('Cache-Control: private, max-age=0, must-revalidate');
        header('Pragma: public');
        echo $_poster;
        return true;
    }

}



