<?php
require('_shared/module.plr_maxia.php');
require('includes/cloudflare_images.php');

class ModulePLR_Maxia_MediaBeheer extends ModulePLR_Maxia {
    var $stylesheet;

    const MEDIA_BEHEER = 1;

    function __construct($moduleid, $module, $clientname=false) {
        parent::__construct($moduleid, $module, $clientname);
        $this->stylesheet = 'default.css';
    }

    function initCloudFlareImages() {
        return new bartb\CloudFlare\Images($_ENV['CLOUDFLARE_IMAGES_ACCOUNT'], $_ENV['CLOUDFLARE_IMAGES_TOKEN'], $_ENV['CLOUDFLARE_ACCOUNT_HASH'], $_ENV['CLOUDFLARE_IMAGES_DELIVERY_URL']);
    }

    function getCustomID() {
        return $_SESSION['imagesgroup_hash'].'-'.format_uuidv4(random_bytes(16));
    }

    function ProcessFiles($files) {
        if ($files["MEDIAFILES"]) {
            $cfi = $this->initCloudFlareImages();

            $meta_data = [
                'environment' => $_ENV['ENVIRONMENT'],
                'server' => $_SERVER['SERVER_ADDR'],
            ];

            $_parent_folder_id = $this->GetParentFolderID($_POST['CURRENTFOLDER'])[0];
            foreach ($files['MEDIAFILES']['name'] as $key => $file) {
                $tmp_name = $files['MEDIAFILES']['tmp_name'][$key];
                $name = $files['MEDIAFILES']['name'][$key];
                $custom_id = $this->getCustomID();
                if (!empty($tmp_name)) {
                    try {
                        $_id = $cfi->uploadImageFile($tmp_name, $name, true, $meta_data, $custom_id);
                        $this->insertMediaFile($custom_id, $name, $_parent_folder_id);
                    } catch (Exception $e) {
                        echo "Error: ".$e->getMessage();
                    }
                } else {
                    // throw new Exception("File not {$name} found on filesystem");
                }
            }
        }
    }

    function ProcessPostRequests($allesRecord, $event){

        switch ($event) {
            case 'updatemediafile':
                $_result = $this->UpdateMediaFile($allesRecord);
                break;
            case 'verwijdermediafile':
                $_result = $this->DeleteMediaFile($allesRecord);
                break;
            case 'nieuwefolder':
                $_result = $this->NieuweFolder($allesRecord);
                break;
            case 'synccloudimages':
                $_result = $this->SyncCloudImages($allesRecord);
                break;
        }
        return $_result;
    }

    function ProcessShowRequests($allesRecord, $event){
        $_content = false;
        $this->form->database->userdb->SetFetchMode(ADODB_FETCH_ASSOC);

        switch($this->moduleid) {
            case self::MEDIA_BEHEER:
                switch ($event) {
                    case 'mediafiles':
                        $_content = $this->GetMediaFiles();
                        break;
                    default:
                        break;
                }
            break;
        }
        if ($_content !== false) {
            echo $_content;
            exit; // stop with the rest of the html
        }
    }

    function ProcessRequest($event=false) {
        if ($event) {
            return $this->ProcessPostRequests($_POST, $event);
        } elseif (isset($_GET['func'])) {
            return $this->ProcessShowRequests($_GET, $_GET['func']);
        }
        return false;
    }

    function DeleteMediaFile($allesRecord) {
        try {
            $this->form->database->deleteHashedRecord($allesRecord['_hdnTable'], $allesRecord['_hdnRecordID']);

            $keyfieldselect = $this->form->database->makeEncodedKeySelect($allesRecord['_hdnTable'], $allesRecord['_hdnRecordID']);
            $_sql = "SELECT CLOUD_IMAGE_ID FROM mx_mediabeheer WHERE {$keyfieldselect}";
            $_cf_image_id = $this->form->database->userdb->GetOne($_sql);
            $_cfi = $this->initCloudFlareImages();

            if ($allesRecord['PERMANENT'] == 'true') {
                $_cfi->deleteImage($_cf_image_id);
            } else {
                $_cfi->virtualDeleteImage($_cf_image_id);
            }

        } catch (Exception $e) {
            echo "Error: ".$e->getMessage();
        }
        return true;
    }

    function GetParentFolderID($folderhash) {
        $_row = $this->form->database->userdb->GetRow("
        SELECT MEDIA_ID
        , BESTANDSNAAM
        , (SELECT MD5(CONCAT(CLIENT_HASH,'_',MEDIA_ID)) FROM MX_MEDIABEHEER P WHERE P.MEDIA_ID = M.PARENT_FOLDER_ID) AS SUPERPARENT_HASH
        FROM mx_mediabeheer m
        WHERE MD5(CONCAT(CLIENT_HASH,'_',MEDIA_ID)) = '{$folderhash}'"
        );
        $parentfolderid = null;
        $parentfoldername = null;
        $superparenthash = null;
        if ($_row) {
            $parentfolderid = $_row['MEDIA_ID'];
            $parentfoldername = $_row['BESTANDSNAAM'];
            $superparenthash = $_row['SUPERPARENT_HASH'];
        }
        if ($parentfolderid == 0) {
            $parentfolderid = null;
        }
        return array($parentfolderid, $parentfoldername, $superparenthash);
    }

    function SyncCloudImages($allesRecord) {
        global $scramble;
        global $polaris;

        $_result = true;
        $_imagesgroup_hash = $_SESSION['imagesgroup_hash'];
        $_environment = $_ENV['ENVIRONMENT'];

        $_cfi = $this->initCloudFlareImages();
        $_allImages = $_cfi->listImages();

        $_sqlCount = "
            SELECT COUNT(*) FROM mx_mediabeheer WHERE CLOUD_IMAGE_ID = ?
        ";
        $_sqlInsert = "
            INSERT INTO mx_mediabeheer (CLIENT_HASH, CLOUD_IMAGE_ID, BESTANDSNAAM, BESTANDSTYPE)
            VALUES (?, ?, ?, ?)
        ";

        foreach($_allImages->images as $_image) {
            // Make sure to only sync images that belong to the current client and environment
            if ((explode('-',$_image->id)[0] == $_imagesgroup_hash) && ($_image->meta->environment == $_environment)) {
                $_count = $this->form->database->userdb->GetOne($_sqlCount, array($_image->id));
                if ($_count == 0) {
                    $bestandstype = pathinfo($_image->filename, PATHINFO_EXTENSION);
                    $this->form->database->userdb->Execute($_sqlInsert, array($this->clienthash, $_image->id, $_image->filename, $bestandstype));
                }
            }
        }

        return $_result;
    }

    function NieuweFolder($allesRecord) {
        // vind de parent folder media_id
        $_parent_folder_id = $this->GetParentFolderID($allesRecord['parent_folder_id'])[0];

        $autosupervalues = [];
        $_fieldValues['CLIENT_HASH'] = $this->clienthash;
        $_fieldValues['BESTANDSNAAM'] = sanitize_filename($allesRecord['foldernaam']);
        $_fieldValues['BESTANDSTYPE'] = 'FOLDER';
        $_fieldValues['PARENT_FOLDER_ID'] = $_parent_folder_id;

        $this->form->database->insertRecord($allesRecord['_hdnTable'], $_fieldValues, $autosupervalues);
    }

    function GetMediaFiles() {
        $_result = [];
        if (!empty($_GET['folder'])) {
            $_parentfolder = $_GET['folder'];
            $_parentfolderObject = $this->GetParentFolderID($_parentfolder);
            $_parentfoldername = $_parentfolderObject[1];
            $_customfilter = "MD5(CONCAT(CLIENT_HASH,'_',PARENT_FOLDER_ID)) = '{$_parentfolder}'";
            $_result['root'] = false;
            $_result['parentfolder'] = $_parentfolderObject[2];
            $_result['currentfolder'] = $_parentfolder;
            $_result['currentfoldername'] = $_parentfoldername;
        } else {
            $_customfilter = "PARENT_FOLDER_ID IS NULL";
            $_result['root'] = true;
            $_result['parentfolder'] = '';
            $_result['currentfolder'] = '';
            $_result['currentfoldername'] = 'Root';
        }
        $joinarray = [];
        $_customfilter = $this->form->AddSearchFilter($_customfilter, $_GET['q'], true, $joinarray);
        $_recordset = $this->form->GetViewContent($detail=false, $masterrecord=false, $limit=false, $_customfilter);
        $_cfi = $this->initCloudFlareImages();
        foreach($_recordset as $_index => $_record) {
            $_recordset[$_index]['IMAGEURL_THUMBNAIL'] = $_cfi->getImageUrl($_record['CLOUD_IMAGE_ID'], 'thumbnail');
            $_recordset[$_index]['IMAGEURL_LARGE'] = $_cfi->getImageUrl($_record['CLOUD_IMAGE_ID'], 'large');
            $_recordset[$_index]['IMAGEURL_PUBLIC'] = $_cfi->getImageUrl($_record['CLOUD_IMAGE_ID'], 'public');
            $_recordset[$_index]['BESTANDSNAAM_KORT'] = abbreviateFilename($_record['BESTANDSNAAM'], 25);
            if ($_record['BESTANDSTYPE'] == 'FOLDER') {
                $_recordset[$_index]['FOLDER_HASH'] = hashstr($this->clienthash.'_'.$_record['MEDIA_ID']);
            } elseif (!empty($_record['PARENT_FOLDER_ID'])) {
                $_recordset[$_index]['FOLDER_HASH'] = hashstr($this->clienthash.'_'.$_record['PARENT_FOLDER_ID']);
            }
        }
        // sorteer $_recordset op BESTANDSTYPE en BESTANDSNAAM
        usort($_recordset, function($a, $b) {
            return $a['BESTANDSTYPE'] <=> $b['BESTANDSTYPE'] ?: $a['BESTANDSNAAM'] <=> $b['BESTANDSNAAM'];
        });
        $_result['files'] = $_recordset;
        return json_encode($_result);
    }

    function insertMediaFile($cloudFlareImageId, $naam, $parentfolder=null) {
        $_sql = "
            INSERT INTO mx_mediabeheer
            (CLIENT_HASH, CLOUD_IMAGE_ID, BESTANDSNAAM, BESTANDSTYPE, PARENT_FOLDER_ID)
            VALUES
            (?, ?, ?, ?, ?)
        ";
        $bestandstype = pathinfo($naam, PATHINFO_EXTENSION);
        $this->form->database->userdb->Execute($_sql, array($this->clienthash, $cloudFlareImageId, $naam, $bestandstype, $parentfolder));
    }

    function ShowFormView($state, $permission, $detail=false, $masterrecord=false) {
        $_customfilter = $this->__GetCustomFilter();
        $_recordset = $this->form->GetViewContent($detail=false, $masterrecord=false, $limit=false, $_customfilter);

        $this->smarty->assign('item', $_recordset[0]);
        $this->smarty->display("lockeritem.tpl.php");
    }

    function ShowListView($state, $permission, $detail=false, $masterrecord=false) {
        $this->AddCustomJS(['/modules/_shared/module.plr_maxia/js/maxia.js']);
        $this->AddModuleJS(['main.js']);

        $this->smarty->assign('permission', $permission);
        if (empty($_SESSION['imagesgroup_hash'])) {
            echo "<h3 class='text-danger'>Je klantomgeving heeft geen imagesgroup-hash gedefinieerd. Contacteer je administrator.</h3>";
            return;
        }
        if (isset($_GET['compact'])) {
            $this->smarty->display("mediabeheer_compact.tpl.php");
        } else {
            $this->smarty->display("mediabeheer.tpl.php");
        }
    }

}
