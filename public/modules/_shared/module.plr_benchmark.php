<?php
require('module._base.php');

class Module extends _BaseModule {

    function Module($moduleid, $module) {
        global $_GVARS;
        global $polaris;

        parent::_BaseModule($moduleid, $module);

        $this->processed = false;
        
        /**
        * What client are we talking about? (nog doen: eventueel selecteerbaar maken)
        */
        $this->clientid = $_SESSION['clientid'];

        $_databaseid = 3;
        /* Load the user database */
        $db = new plrDatabase($polaris);
        $db->LoadRecord(array($this->clientid, $_databaseid));
        $this->userdatabase = $db->connectUserDB();
    }
    
    function Process() {
        global $_GVARS;
    }
    
    function Test_TableAsSelectList() {
        global $polaris;

        $polaris->timer->clearTimers(); 

        $runtime = 500;
        
        $polaris->timer->startTimer('TableAsSelectList_Cache'); 
        for($i=0;$i<$runtime;$i++) {
            $result = TableAsSelectList($this->userdatabase, $tablename = 'LAND', $supercolumn="LANDCODE", $supershowcolumnname="LANDNAAM", $currentvalue="NL", $id = '_fldLANDCODE', $selectname = "LANDCODE", $whereclause = '', $fieldrequired=true, $extra = '', $orderby = '2', $cachedata=true);
        }
        $polaris->timer->endTimer('TableAsSelectList_Cache'); 

        $polaris->timer->startTimer('TableAsSelectList_NoCache'); 
        for($i=0;$i<$runtime;$i++) {
            $result = TableAsSelectList($this->userdatabase, $tablename = 'LAND', $supercolumn="LANDCODE", $supershowcolumnname="LANDNAAM", $currentvalue="NL", $id = '_fldLANDCODE', $selectname = "LANDCODE", $whereclause = '', $fieldrequired=true, $extra = '', $orderby = '2', $cachedata=false);
        }
        $polaris->timer->endTimer('TableAsSelectList_NoCache'); 

        $polaris->timer->displayTimeTaken();
    }

    function Show($outputformat='xhtml', $rec=false) {
        global $_GVARS;

        parent::Show($outputformat, $rec);

        $this->Test_TableAsSelectList();        
    }

}