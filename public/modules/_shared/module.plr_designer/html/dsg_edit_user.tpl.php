{if $recordstate eq #rec_insert#}
<h1>{lang make_new_user}</h1>
<p>{lang new_user_desc}</p>
{else}
<h1>{lang user_account_of} {$usergroup.FULLNAME|default:$usergroup.USERGROUPNAME}</h1>
<p>{lang edit_user_desc}</p>
{/if}

<form id="userform" method="post" action="{$callerquery}" class="form-horizontal validate" enctype="multipart/form-data">
<input type="hidden" name="_hdnDatabaseID" value="{$databaseid}" />
<input type="hidden" name="_hdnRecordID" value="{$recordid}" />
<input type="hidden" name="_hdnAction" value="{$recordstate}" />
<input type="hidden" name="PHOTOURL" value="{$usergroup.PHOTOURL}" />
<input type="hidden" name="MAX_FILE_SIZE" value="500000" />
{if $recordstate eq #rec_insert#}
<input type="hidden" name="CLIENTID" value="{$smarty.session.currentclient}" />
<input type="hidden" name="USERORGROUP" value="{$USERORGROUP}" />
{/if}

<div class="row">
    <div class="col-lg-6">
        <div class="row">
            <div class="form-group"><label class="col-xs-12 col-sm-4 control-label">{lang client}</label>
                <div class="col-xs-12 col-sm-8">{include file="showclientselect.tpl.php" style="selectonly" readonly="false" class="required"} <span class="help-block m-b-none">Selectuur de Polaris klant.</span>
                </div>
            </div>
            <div class="form-group"><label class="col-xs-12 col-sm-4 control-label">{lang username}</label>
                <div class="col-xs-12 col-sm-8">
                    {if $smarty.session.usertype == 'root' or $recordstate eq #rec_insert#}
                    <input name="USERGROUPNAME" class="text required" label="{lang username}" type="text" value="{$usergroup.USERGROUPNAME|default:$smarty.post.USERGROUPNAME}" />
                    {else}
                    {$usergroup.USERGROUPNAME}
                    {/if}
                </div>
            </div>
            <div class="form-group"><label class="col-xs-12 col-sm-4 control-label">{lang fullname}</label>
                <div class="col-xs-12 col-sm-8">
                    <input name="FULLNAME" class="text required" label="{lang fullname}" type="text" value="{$usergroup.FULLNAME}" />
                </div>
            </div>
            <div class="form-group"><label class="col-xs-12 col-sm-4 control-label">{lang description}</label>
                <div class="col-xs-12 col-sm-8">
                    <input name="DESCRIPTION" class="text required" type="text" value="{$usergroup.DESCRIPTION}" />
                </div>
            </div>
            <div class="form-group"><label class="col-xs-12 col-sm-4 control-label">{lang email_address}</label>
                <div class="col-xs-12 col-sm-8">
                    <input name="EMAILADDRESS" class="text required" type="text" value="{$usergroup.EMAILADDRESS}" />
                </div>
            </div>
            <div class="form-group"><label class="col-xs-12 col-sm-4 control-label">{lang password}</label>
                <div class="col-xs-12 col-sm-8">
                    <input name="USERPASSWORD" class="text required" type="password" value="{$usergroup.USERPASSWORD}" />
                </div>
            </div>
            <div class="form-group"><label class="col-xs-12 col-sm-4 control-label">{lang login_count}</label>
                <div class="col-xs-12 col-sm-8">
                    {$usergroup.LOGINCOUNT}
                </div>
            </div>
            <div class="form-group"><label class="col-xs-12 col-sm-4 control-label">{lang last_login_date}</label>
                <div class="col-xs-12 col-sm-8">
                    {$usergroup.LASTLOGINDATE|date_format:"%d %B %Y"}
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6">

        <div class="form-group"><label class="col-xs-12 col-sm-4 control-label">{lang change_password_next_logon}?</label>
            <div class="col-xs-12 col-sm-8">
                {$yesno}
                {html_radios_plus name="CHANGEPASSWORDNEXTLOGON" options=$yesno checked=$usergroup.CHANGEPASSWORDNEXTLOGON default='N' separator='&nbsp;&nbsp;' class='radiobutton'}
            </div>
        </div>
        <div class="form-group"><label class="col-xs-12 col-sm-4 control-label">{lang last_login_date}</label>
            <div class="col-xs-12 col-sm-8">
                {$usergroup.LASTLOGINDATE|date_format:"%d %B %Y"}
            </div>
        </div>
        <div class="form-group"><label class="col-xs-12 col-sm-4 control-label">{lang last_login_date}</label>
            <div class="col-xs-12 col-sm-8">
                {$usergroup.LASTLOGINDATE|date_format:"%d %B %Y"}
            </div>
        </div>
        <div class="form-group"><label class="col-xs-12 col-sm-4 control-label">{lang last_login_date}</label>
            <div class="col-xs-12 col-sm-8">
                {$usergroup.LASTLOGINDATE|date_format:"%d %B %Y"}
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-offset-2 col-xs-12 col-sm-8">
                <div class="i-checks"><label> <input type="checkbox"><i></i> Remember me </label></div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-offset-2 col-xs-12 col-sm-8">
                <button class="btn btn-sm btn-white" type="submit">Sign in</button>
            </div>
        </div>
    </div>
</div>
</form>


{if $usertype eq 'root'}
<tr><td>{lang client}:</td><td>{include file="showclientselect.tpl.php" style="selectonly" readonly="false" class="required"}</td></tr>
{/if}
<tr><td class="label">{lang username}:</td><td>
{if $smarty.session.usertype == 'root' or $recordstate eq #rec_insert#}
<input name="USERGROUPNAME" class="text required" label="{lang username}" type="text" value="{$usergroup.USERGROUPNAME|default:$smarty.post.USERGROUPNAME}" />
{else}
{$usergroup.USERGROUPNAME}
{/if}
</td></tr>
<tr><td class="label">{lang fullname}:</td><td><input name="FULLNAME" class="text required" label="{lang fullname}" type="text" value="{$usergroup.FULLNAME}" /></td></tr>
<tr><td class="label">{lang description}:</td><td><input name="DESCRIPTION" class="text" label="{lang description}" type="text" size="40" value="{$usergroup.DESCRIPTION}" /></td></tr>
<tr><td class="label">{lang email_address}:</td><td><input name="EMAILADDRESS" class="text" label="{lang email_address}" type="text" size="40" value="{$usergroup.EMAILADDRESS}" /></td></tr>
<tr><td class="label">{lang password}:</td><td><input class="text required" label="{lang password}" type="password" name="USERPASSWORD" value="{$userpassword}"></td></tr>
{if $recordstate neq #rec_insert#}
<tr><td class="label">{lang login_count}:</td><td>{$usergroup.LOGINCOUNT}</td></tr>
<tr><td class="label">{lang last_login_date}:</td><td>{$usergroup.LASTLOGINDATE|date_format:"%d %B %Y"}</td></tr>
{/if}
<tr><td class="label">{lang change_password_next_logon}?</td><td><div class="required">{html_radios_plus name="CHANGEPASSWORDNEXTLOGON" options=$yesno checked=$usergroup.CHANGEPASSWORDNEXTLOGON default='N' separator='&nbsp;&nbsp;' class='radiobutton' label=' change_password_next_logon'}</div></td></tr>
<tr><td class="label">{lang user_cannot_change_password}?</td><td><div class="required">{html_radios_plus name="USERCANNOTCHANGEPASSWORD" options=$yesno checked=$usergroup.USERCANNOTCHANGEPASSWORD default='Y' separator='&nbsp;&nbsp;' class='radiobutton' label='user_cannot_change_password'}</div></td></tr>
<tr><td class="label">{lang password_never_expires}?</td><td><div class="required">{html_radios_plus name="PASSWORDNEVEREXPIRES" options=$yesno checked=$usergroup.PASSWORDNEVEREXPIRES default='Y' separator='&nbsp;&nbsp;' class='radiobutton' label='password_never_expires'}</div></td></tr>
<tr><td class="label">{lang account_disabled}?</td><td><div class="required">{html_radios_plus name="ACCOUNTDISABLED" options=$yesno checked=$usergroup.ACCOUNTDISABLED default='N' separator='&nbsp;&nbsp;' class='radiobutton' label='account_disabled'}</div></td></tr>
{if $smarty.session.usertype == 'client' or $smarty.session.usertype == 'root'}
<tr><td class="label">{lang client_admin}?</td><td><div class="required">{html_radios_plus name="CLIENTADMIN" options=$yesno checked=$usergroup.CLIENTADMIN default='N' separator='&nbsp;&nbsp;' class='radiobutton' label='client_admin'}</div></td></tr>
{/if}
{if $smarty.session.usertype == 'root'}
<tr><td class="label">{lang root_admin}?</td><td><div class="required">{html_radios_plus name="ROOTADMIN" options=$yesno checked=$usergroup.ROOTADMIN default='N' separator='&nbsp;&nbsp;' class='radiobutton' label='root_admin'}</div></td></tr>
{/if}
<tr><td class="label">Foto van gebruiker:</td><td>{if $usergroup.PHOTOURL ne ''}<img src="{$serverroot}/userphotos/{$usergroup.PHOTOURL}" alt="foto van {$usergroup.USERGROUPNAME}" />{else}Geen foto{/if}</td></tr>
<tr><td class="label">Nieuwe foto uploaden:</td><td><input class="text file" type="file" accept="*.gif,*.jpg" size="40" name="PHOTO" /><br />De foto wordt automatisch verkleind tot max. {$config.userimage_maxwidth}px breed en {$config.userimage_maxheight}px hoog.<br />Alleen JPG, PNG en GIF bestanden worden geaccepteerd.</td></tr>
</table>
<input type="submit" value="{lang but_save}">&nbsp;<input type="button" value="{lang but_cancel}" class="protect" onClick="location.href='{$callerquery}'" />
</form>

{if $recordstate neq #rec_insert#}
<h2 class="mainactions">{$usergroup.FULLNAME|default:$usergroup.USERGROUPNAME} {lang is_member_of}</h2>
{section name=j loop=$memberships}
{if $smarty.section.j.first}
<table class="group-table data striped" summary="overzicht">
<thead><tr><th width="40%">{lang group}</th><th class="">{lang description}</th
><th class="">{lang action}</th></tr></thead>
{/if}
<tr><td><a href="?action={#action_edit#}&rec={$memberships[j].RECORDID}">{$memberships[j].USERGROUPNAME}</a></td
><td>{$memberships[j].FULLNAME}</td><td><a href="?action={#action_cancelmembership#}&rec={$memberships[j].RECORDID}&user={$recordid}">{lang cancel_membership}</a></td></tr>
{if $smarty.section.j.last}
</table>
{/if}
{sectionelse}
<p>{lang user_has_no_memberships}</p>
{/section}

<p><br /><img src="{$serverroot}/i/beos/people.png" height="{#small_icon_height#}" width="{#small_icon_width#}" alt="" class="im" />
<a href="?action={#action_createmembership#}&rec={$recordid}">{lang create_membership}...</a></p>
{/if}
