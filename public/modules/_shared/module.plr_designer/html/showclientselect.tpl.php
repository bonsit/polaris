{if $usertype eq 'root'}
{if $readonly eq 'true'}
{php}
	global $polaris, $_sqlDSGClient, $currentclient;
    $rs_client = $polaris->instance->GetAll($_sqlDSGClient, array($currentclient)) or Die('Query failed:'.$_sqlDSGClient);
	$polaris->tpl->assign('clientname', $rs_client[0]['NAME']);
{/php}
{else}
{php}
	global $polaris, $_sqlDSGClients;
  $rs_clients = $polaris->instance->GetAll($_sqlDSGClients) or Die('Query failed:'.$_sqlDSGClients);
	$polaris->tpl->assign('clients', $rs_clients);
{/php}
{/if}
{if $style neq 'selectonly'}
<div class="clientpreselect">{lang choose_client}:<br />
<form name="clientselect" method="POST" action=".">
{/if}
{if $readonly eq 'true'}
{$clientname}
{else}
<select name="_hdnClientHash" size="1" class="{$class}">
{if $style neq 'selectonly'}<option value="all">&lt;{lang all_clients}&gt;</option>{/if}
{section name=n loop=$clients}
{if $clients[n].RECORDID eq $currentclient}
	{assign var="selected" value="selected='selected'"}
{else}
	{assign var="selected" value=""}
{/if}
<option value="{$clients[n].RECORDID}" {$selected}>{$clients[n].NAME}</option>
{/section}
</select>
{/if}
{if $style neq 'selectonly'}
&nbsp;<input type="submit" value="Go" /></form>
</div>
{/if}
{/if}
