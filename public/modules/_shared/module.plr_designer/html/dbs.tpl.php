<h1>{lang database_list}</h1>

{include file="showclientselect.tpl.php"}

<table class="table table-striped" summary="data"><thead><tr>
<th>{lang databaseid}</th>
{if $smarty.session.currentclient eq ''}
<th>{lang client}</th>
{/if}
<th>{lang host}</th>
<th>{lang databasetype}</th>
<th>{lang databasename}</th>
<th>{lang rootusername}</th>
<th width="20%" class="center">{lang delete}</th></tr></thead>
<tbody>
{section name=i loop=$recset}<tr>
<td><a title="{lang edit}" href="?action=edit&rec={$recset[i].RECORDID}">{$recset[i].DESCRIPTION}</a></td>
{if $smarty.session.currentclient eq ''}
<td>{$recset[i].CLIENTID}</td>
{/if}
<td>{$recset[i].HOST}</td>
<td>{$recset[i].DATABASETYPE}</td>
<td>{$recset[i].DATABASENAME}</td>
<td>{$recset[i].ROOTUSERNAME}</td>
<td class="center"><input name="recordid[]" value="rec:{$recset[i].RECORDID}" type="checkbox"></td></tr>
{/section}
<tfoot><tr>
{if $smarty.session.currentclient eq ''}
<td></td>
{/if}
<td></td><td></td><td></td><td></td><td></td><td class="center"><input type="submit" name="_hdnDeleteButton" value="{lang but_delete}" class="protect" onclick="return CheckDel('dataform');" title="{lang but_delete_hint}" tabindex="2"></td></tr></tfoot></table>
