{include file="basemenu.tpl.php"}

{if $recordstate eq #rec_insert#}
<h1>{lang make_new_group}</h1>
<p>{lang new_group_desc}</p>
{else}
<h1>{lang group_properties_of} '{$usergroup.USERGROUPNAME}'</h1>
<p>{lang edit_group_desc}</p>
{/if}
<form name="userform" id="formview" method="POST" action="{$callerquery}" class="validate">
<input type="hidden" name="_hdnDatabaseID" value="{$databaseid}" />
<input type="hidden" name="_hdnRecordID" value="{$recordid}" />
<input type="hidden" name="_hdnAction" value="{$recordstate}" />
{if $recordstate eq #rec_insert#}
<input type="hidden" name="CLIENTID" value="{$smarty.session.currentclient}" />
<input type="hidden" name="USERORGROUP" value="{$USERORGROUP}" />
{/if}
{if $usertype eq 'root'}
<div class="formviewrow"><label class="label">{lang client}:</label><div class="formcolumn">{include file="showclientselect.tpl.php" style="selectonly" readonly="false" class="required"}</div></div>
{/if}
<div class="formviewrow"><label class="label">{lang group}:</label><div class="formcolumn"><input class="text required" name="USERGROUPNAME" type="text" value="{$usergroup.USERGROUPNAME}"></div></div>
<div class="formviewrow"><label class="label">{lang description}:</label><div class="formcolumn"><input class="text" name="DESCRIPTION" type="text" size="40" value="{$usergroup.DESCRIPTION}" /></div></div>
<div class="formviewrow"><label class="label">{lang autocreatemembership}</label><div class="formcolumn"><input class="radio" name="AUTOCREATEMEMBERSHIP" type="radio" value="Y"{if $usergroup.AUTOCREATEMEMBERSHIP eq 'Y'}checked="checked"{/if} />{lang yes}  <input class="radio" name="AUTOCREATEMEMBERSHIP" type="radio" value="N"{if $usergroup.AUTOCREATEMEMBERSHIP neq 'Y'}checked="checked"{/if} />{lang no}</div></div>
<div class="formviewrow"><label class="label"></label><div class="formcolumn"><input type="submit" value="{lang but_save}" /> <input type="button" onClick="location.href='{$callerquery}'" value="{lang but_cancel}" class="protect" /></div></div>

</form>

{if $recordstate neq #rec_insert#}
<h2 class="mainactions">Leden van de groep '{$usergroup.USERGROUPNAME}'</h2>

{section name=j loop=$memberships}
{if $smarty.section.j.first}
<table class="group-table data striped" summary="data">
<thead><tr><th width="20%">{lang account}</th><th class="">{lang fullname}</th
><th class="">{lang action}</th></tr></thead>
<tbody>
{/if}
<tr><td><a href="?action={#action_edit#}&rec={$memberships[j].RECORDID}">{$memberships[j].USERGROUPNAME}</a></td
><td>{$memberships[j].FULLNAME}</td><td><a href="?action={#action_cancelmembership#}&rec={$recordid}&user={$memberships[j].RECORDID}">{lang cancel_membership}</a></td></tr>
{if $smarty.section.j.last}
</tbody></table>
{/if}
{sectionelse}
<p>{lang group_has_no_members}</p>
{/section}

<p><br />
<div class="buttons small">
<img src="{$serverroot}/i/beos/person.png" height="{#small_icon_height#}" width="{#small_icon_width#}" alt="" class="im" />
<a class="button" href="?action={#action_createmembers#}&rec={$recordid}">{lang create_members}...</a>
</div>
{/if}

{include file="footer.tpl.php"}