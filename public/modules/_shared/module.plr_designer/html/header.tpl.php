<!DOCTYPE html>
<html lang="nl">
<head>
<title>{if $constructname ne ''}{$constructname|get_resource:$lang} - {/if}{if $applicationname ne ''}{$applicationname} - {/if}{lang pagetitle}</title>

<meta http-equiv="Content-Type" content="text/html; charset=utf8" />

<link rel="shortcut icon" href="{$serverroot}/favicon.ico" />
<link rel="fluid-icon" href="{$serverroot}/i/prima/plricon4.png" title="Polaris" />
{if $currentform->record->RSSCOLUMNS}<link rel="alternate" type="application/rss+xml" title="{$currentform->record->FORMNAME}" href="{$currentform->RSSLink()}" />{/if}

<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
<script src="https://use.fontawesome.com/849b74bb02.js"></script>
<link href="{$serverpath}/bower_components/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
<link href="{$serverpath}/bower_components/bootstrap/dist/css/glyphicons-bootstrap.css" rel="stylesheet">
<link href="{$serverpath}/bower_components/bootstrap/dist/css/glyphicons.css" rel="stylesheet">
<link href="{$serverroot}/css/plugins/toastr/toastr.min.css" rel="stylesheet">
<link href="{$serverroot}/css/font-financial.css" rel="stylesheet">
<link href="{$serverroot}/css/font-line-icons.css" rel="stylesheet">
<link href="{$serverpath}/styles/animate.css" rel="stylesheet">
<link href="{$serverpath}/styles/style.css" rel="stylesheet">

<link rel="stylesheet" type="text/css" media='all' href="{$serverpath}/min/f={$config.plrcss},{$config.plrcss_tabs},{$config.plrfonticons}{if ($appstylesheet)},{$serverpath}/{$appstylesheet}{elseif ($clientstylesheet)},{$serverpath}/{$clientstylesheet}{/if}{if $smarty.session.branding},{$serverpath}/branding/{$smarty.session.brand}/style.css{/if}{if ($userstylesheet)},{$serverpath}/css/{$userstylesheet}{/if},{$serverpath}/js2/jquery/date_input.css,{$serverpath}/js2/jquery-greybox/greybox.css,{$config.plrprintcss}&{#BuildVersion#}" />

<script type="text/javascript" src="{$serverpath}/min/f=/{$serverpath}/js2/jquery/jquery-2.0.2.js{if $config.debugmode eq false}&1{/if}"></script>

<script type="text/javascript">
var _loadevents_ = [], _serverroot = "{$serverroot}", _callerquery = _serverroot+"{$callerquery}", _basicformquery = _serverroot+"{$basicformquery}", _servicequery = _serverroot+"{$servicequery}", _ajaxquery = _serverroot+"{$ajaxquery}";
var formid = "{$currentformid|default:-1}";
var g_lang = "{$lang}";

</script>
</head>
<body class="canvas top-navigation full-height-layout {if $selectmode}body-small{/if} {$service} {$currentaction} {$currentdetailaction} {if !$maximizeform}{$navigationstyle}{/if} search-{$searchstyle}" xoncontextmenu="return false;">
