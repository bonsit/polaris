{if $maintenance|@count > 0}
<div id="maintenance_window" style="display:none"><i class="fa fa-exclamation-triangle"></i> Polaris {lang maintenance} {lang maintenance1} {$maintenance[0].starttime} {lang maintenance2} (<span id="mw_start">?</span>). &nbsp; {lang maintenance3}: <span id="mw_duration">?</span> min.</div>
{/if}

<ul class="nav navbar-top-links pull-right">
    {if $config.showuserimage}
    <li>
    <div id="userimage">
        <img src="{$serverroot}/userphotos/{$smarty.session.userphoto|default:"anon.jpg"}" alt="{$smarty.session.fullname}">
    </div>
    </li>
    {/if}
    <li>
        <div class="dropdown">
            <a href="#" class="username dropdown-toggle" data-toggle="dropdown">
                {$username} <i class="fa fa-angle-down"></i>
            </a>
            <ul class="dropdown-menu" role="menu">
                <li id="usersettings" ><a tabindex="-1" href="{$serverroot}/user/"><i class="fa fa-cog fa-fw"></i> {lang user_settings}</a></li>
                <li class="dropdown-divider"></li>
                {if $smarty.session.usertype == 'client' or $smarty.session.usertype == 'root'}
                <li><a tabindex="-1" target="_blank" href="{$serverroot}/designer/"><i class="fa fa-bolt fa-fw"></i> {lang polaris_designer}</a></li>
                {/if}
                {if $config.showdatabaseconnection}
                <li class="disabled">
                    <a href="javascript:void(0)"><i class="fa fa-link fa-fw"></i> <span title="{$currentform->database->record->HOST}">Connection {$currentuserdbinstance|default:'none'}</span>
                    </a>
                </li>
                <li class="dropdown-divider"></li>
                {/if}
                <li><a tabindex="-1" href="{$serverroot}/?method=logout"><i class="fa fa-power-off fa-fw"></i> {lang logout}</a></li>
            </ul>

        </div>
    </li>

<!--     <li class="first">
        {$smarty.now|date_format:$i18n_currentdatetimeformat}
    </li>
 -->

    <li>
        <a href="?method=logout">
            <i class="fa fa-sign-out"></i> {lang logout}
        </a>
    </li>
</ul>

<div id="usermenusettings" class="dropdown dropdown-tip">
</div>

<!-- <button type="button" class="xnavbar-toggle" data-toggle="offcanvas" data-target=".navmenu" data-canvas="body">Zoekknopje</button> -->

