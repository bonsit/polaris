{include file="basemenu.tpl.php"}
<script type="text/javascript" src="{$serverroot}/javascript/polaris/SelectContainer.js"></script>
<script type="text/javascript">
{literal}

function ToggleButtons() {
  $('#remove_selected').attr('disabled', $('#memberships')[0].selectedIndex == -1 );
  $('#add_selected').attr('disabled', $('#groups')[0].selectedIndex == -1);
}

$(document).ready(function() {
    SC_ContainersNoSelection('memberships', 'groups');
});

{/literal}
</script>

<h1>{lang memberships_of} {$usergroup.FULLNAME}</h1>
<p>Hieronder kunt u de gebruiker lid maken van een of meer groepen.</p>

<form name="userform" method="POST" action="{$callerpage}?action={#action_edit#}&rec={$recordid}" class="validate">
<input type="hidden" name="_hdnDatabaseID" value="{$databaseid}" />
<input type="hidden" name="_hdnRecordID" value="{$recordid}" />
<input type="hidden" name="_hdnAction" value="{#rec_memberships#}" />

<div class="select_container" style="width:40%;float:left;" >Momenteel lid van:<br />
<select id="memberships" name="newmemberships[]" style="width:100%;" size="30" multiple="multiple" ondblclick="SC_MoveSelected('memberships', 'groups');return false" onchange="ToggleButtons();" >
{section name=i loop=$memberships}
<option value="{$memberships[i].RECORDID}">{$memberships[i].USERGROUPNAME} - {$memberships[i].DESCRIPTION}</option>
{/section}
</select>
</div>
<div class="select_buttons" style="float:left;width:100px;">
<button type="button" id="remove_selected" title="{lang remove_selected}" disabled="disabled" onclick="SC_MoveSelected('memberships', 'groups');return false">&gt;</button>
<button type="button" id="remove_all" title="{lang remove_all}" onclick="SC_MoveAll('memberships', 'groups');return false">&gt;&gt;</button>
<br /><br />
<button type="button" id="add_selected" title="{lang add_selected}" disabled="disabled" onclick="SC_MoveSelected('groups', 'memberships');return false">&lt;</button>
<button type="button" id="add_all" title="{lang add_all}" onclick="SC_MoveAll('groups', 'memberships');return false">&lt;&lt;</button>
</div>
<div class="select_container" style="float:left;width:40%;" >Beschikbare groepen:<br />
<select id="groups" size="30" style="width:100%" multiple="multiple" ondblclick="SC_MoveSelected('groups', 'memberships');return false"  onchange="ToggleButtons();">
{section name=i loop=$groups}
<option value="{$groups[i].RECORDID}">{$groups[i].USERGROUPNAME} - {$groups[i].DESCRIPTION}</option>
{/section}
</select>
</div>

<div class="clear" style="clear:both;">&nbsp;</div>
<input type="submit" value="{lang but_save}" class="protect" onclick="CS_SelectAll('memberships')"> <input type="button" onClick="location.href='{$callerpage}?action=edit&rec={$recordid}'" value="{lang but_cancel}" class="protect">
</form>

{include file="footer.tpl.php"}