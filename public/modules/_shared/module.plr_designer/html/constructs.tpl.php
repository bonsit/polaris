  <div id="topbar" class="border-bottom white-bg">
    <nav class="navbar navbar-static-top" role="navigation">
      <div class="navbar-header">
        <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
        <i class="fa fa-reorder">
        </i>
        </button>
        <a href="#" class="navbar-brand">Polaris beheer</a>
      </div>
      <div class="navbar-collapse collapse" id="navbar">

        <ul id="constructs" class="nav navbar-nav">

      <li class="desaturate normalmenu  ">
        <a tabindex="-1" href="/designer/usergroups/">
          <i class="fa fa-users fa-lg fa-fw">
          </i>
          <span class="nav-label">{lang mod_user_manager}</span>
        </a>
      </li>
      <li class="desaturate normalmenu  ">
        <a tabindex="-1" href="/designer/backup/">
          <i class="fa fa-medkit fa-lg fa-fw">
          </i>
          <span class="nav-label">{lang mod_backup_restore}</span>
        </a>
      </li>
      <li class="desaturate normalmenu  ">
        <a tabindex="-1" href="/designer/appgen/">
          <i class="fa fa-rocket fa-lg fa-fw">
          </i>
          <span class="nav-label">{lang mod_site_generator}</span>
        </a>
      </li>
      <li class="normalmenu  ">
        <a tabindex="-1" href="/designer/dbs/">
          <i class="fa fa-database fa-lg fa-fw">
          </i>
          <span class="nav-label">{lang mod_db_manager}</span>
        </a>
      </li>
<!--       <li class="normalmenu  ">
        <a tabindex="-1" href="/designer/clients/">
          <i class="fa fa-user-o fa-lg fa-fw">
          </i>
          <span class="nav-label">{lang mod_client_manager}</span>
        </a>
      </li>
      <li class="normalmenu  ">
        <a tabindex="-1" href="/designer/templates/">
          <i class="fa fa-th-large fa-lg fa-fw">
          </i>
          <span class="nav-label">{lang mod_template_manager}</span>
        </a>
      </li>
 -->
      <li class="normalmenu  ">
        <a tabindex="-1" href="/designer/settings/">
          <i class="fa fa-cogs fa-lg fa-fw">
          </i>
          <span class="nav-label">{lang mod_system_settings}</span>
        </a>
      </li>

        </ul>

          {include file="topbar.tpl.php"}

        </div>
    </nav>
  </div>
