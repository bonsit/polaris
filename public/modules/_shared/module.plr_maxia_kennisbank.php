<?php
require('_shared/module._base.php');

class ModulePLR_Maxia_Kennisbank extends _BaseModule {
    var $stylesheet;

    function __construct($moduleid, $module, $clientname=false) {
        parent::__construct($moduleid, $module, $clientname);

        $this->stylesheet = 'default.css';
    }

    function ProcessRequest($event=false) {
        $_content = false;

        try {
            $_func = $_GET['action'];
            if (!empty($_func)) {
                switch($this->moduleid) {
                case 1:
                break;
                }
            }
            if ($_content !== false) {
                echo $_content;
                exit; // stop with the rest of the html
            }
        } catch (Exception $e) {
            echo "Wrong parameters: ".$e;
        }

        return false;
    }

    function ShowFormView($state, $permission, $detail=false, $masterrecord=false) {
        $_customfilter = $this->__GetCustomFilter();
        $_recordset = $this->form->GetViewContent($detail=false, $masterrecord=false, $limit=false, $_customfilter);

        $this->smarty->assign('item', $_recordset[0]);
        $this->smarty->display("instructiesoverzicht.tpl.php");
    }

    function ShowListView($state, $permission, $detail=false, $masterrecord=false) {
        $_customfilter = $this->__GetCustomFilter();
        $_recordset = $this->form->GetViewContent($detail=false, $masterrecord=false, $limit=false, $_customfilter);
        $this->ShowModuleListview($_recordset);
    }

}
