<?php
require_once('includes/mainactions.inc.php');
require_once('_shared/module._base.php');

class ModulePLR_WebPage extends _BaseModule {
    var $stylesheet;

    function ModulePLR_WebPage($moduleid, $module, $clientname=false) {
        global $_GVARS;
        parent::__construct($moduleid, $module, $clientname, 'web_page');

        $this->processed = false;
        $this->stylesheet = 'default.css';
    }

    function __GetCustomFilter() {
        return "editable='Y'";
    }

    function Process() {
        $this->userdatabase->SetFetchMode(ADODB_FETCH_ASSOC);
        $this->parray = extractParams($this->params);
        //$masterfield = $this->parray['masterfield'];

        if (isset($_POST['_hdnAction'])) {
            switch ($_POST['_hdnAction']) {
            case 'publish':
                return $this->PublishWebpage($_POST['_hdnRecordID']);
            break;
            case 'save':
                return SaveRecord($_POST);
            break;
            }
        }
    }

    function PublishWebpage($rec) {
        $_tablename = $this->form->record->TABLENAME;
        $_key = $this->form->database->makeRecordID(array('ID'));
        $_sql = "UPDATE ".$_tablename." SET published = IF(published=1, 0, 1) WHERE ". $_key ." = '$rec'";
        $this->userdatabase->Execute($_sql);
        return true;
    }

    function ShowListView($state, $permission, $detail=false, $masterrecord=false) {
        $_customfilter = $this->__GetCustomFilter();
        $_recordset = $this->_moduleform->GetViewContent($detail=false, $masterrecord=false, $limit=false, $_customfilter);
        $this->ShowModuleListview($_recordset);
        // var_dump($_recordset);
    }

    function ShowFormView($state, $permission, $detail=false, $masterrecord=false) {
        $_customfilter = $this->__GetCustomFilter();
        $_recordset = $this->_moduleform->GetViewContent($detail=false, $masterrecord=false, $limit=false, $_customfilter);
        $this->ShowModuleFormview($_recordset);


        // $_rs = $_rs->GetAll();
        // $this->smarty->assign('stylesheet', $this->stylesheet);
        // $this->smarty->assign('items', $_rs);
        // $this->ShowModuleListview('web_page', $_rs);
        // echo $this->smarty->Fetch("pageedit.tpl.php");
    }

    function Show($outputformat='xhtml', $rec=false) {
        global $_GVARS;

        if ($this->form->outputformat == 'json') {
            echo json_encode($this->data);
        } elseif ($this->form->outputformat == 'php') {
            echo serialize($this->data);
        } else {
            parent::Show();
            switch($_GET['action']) {
            case '':
                $this->ShowListView($this->state, $this->permission, $detail=false, $masterrecord=false);
            break;
            case 'edit':
            case 'insert':
                $this->ShowFormView($this->state, $this->permission, $detail=false, $masterrecord=false);
            break;
            }
        }
    }

}
