<?php
require_once('includes/mainactions.inc.php');
require_once('module._base.php');

class ModulePLR_Parameters extends _BaseModule {
    var $stylesheet;

    function ModulePLR_Parameters($moduleid, $module, $clientname=false) {
        global $_GVARS;
        parent::__construct($moduleid, $module, false, $clientname);

        $this->processed = false;
        $this->stylesheet = 'default.css';
    }

    function __GetCustomFilter() {
        $_filter = $this->form->AddDataScopeFilter();
        return $_filter;
    }

    function ProcessRequest($event=false) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $result = $this->ProcessPostRequests($_POST, $event);
        } else {
            $this->ProcessShowRequests($_GET, $event);
        }
        return $result;
    }

    function ProcessPostRequests($record, $event){
        $_result = false;
        // switch ($event) {
        //     case 'plr_save':
        //         $_result = $this->VoegAssetsToe($record);
        //         break;
        // }
        return $_result;
    }

    function ProcessShowRequests($record, $event){

        $_content = false;
        switch ($event) {
            case 'parameters':
                $_content = $this->GetParameters();
                break;
            default:
                break;
        }

        if ($_content !== false) {
            echo $_content;
            exit; // stop with the rest of the html
        }
    }
    // function ProcessRequest($event=false) {
    //     if (isset($_POST['_hdnAction'])) {
    //         switch ($_POST['_hdnAction']) {
    //         case 'plr_save':
    //             $_POST['UPDATED_AT'] = date('Y-m-d H:i:s');
    //             $_POST['BLOG_ID'] = 1;
    //             $result = SaveRecord($_POST);
    //             if ($result == true) {
    //                 $_GET['action'] == '';
    //             }
    //         break;
    //         }
    //     }
    // }

    function GetParameters() {
        $_sql = "
        SELECT ttt.GROEPCODE, ttt.GROEPNAAM, ttt.VOLGORDE, ttt.OMSCHRIJVING AS GROEPOMSCHRIJVING
        FROM mx_parametergroep ttt
        WHERE {$this->__GetCustomFilter()}
        ORDER BY ttt.VOLGORDE
        ";
        $_rsGroepen = $this->userdatabase->GetAll($_sql);

        $_sql = "
        SELECT ttt.GROEP, ttt.PARAMETERCODE, ttt.PARAMETERNAAM, ttt.OMSCHRIJVING, ttt.DATATYPE, ttt.WAARDE, ttt.LOOKUPQUERY
        , {$this->form->database->makeRecordIDColumn('mx_parameters', 'ttt')} AS PLR__RECORDID
        FROM mx_parameters ttt
        RIGHT JOIN mx_parametergroep tttt ON tttt.GROEPCODE = ttt.GROEP
        AND tttt.CLIENT_HASH = ttt.CLIENT_HASH
        WHERE {$this->__GetCustomFilter()}
        AND ttt.WIJZIGBAAR = 'Y'
        ";
        $_rsParameters = $this->userdatabase->GetAll($_sql);
        $_rsParameters = $this->ProcessLookupParameters($_rsParameters);

        // // walk through the set and create a json with the parameter groups and parameters as a subarray
        // $transformedArray = [];
        // foreach ($_rs as $item) {
        //     // Extract values from the item
        //     $groupCode = $item["GROEPCODE"];
        //     $groupName = $item["GROEPNAAM"];
        //     $groupDescription = $item["GROEPOMSCHRIJVING"];
        //     $order = $item["VOLGORDE"];
        //     $parameterCode = $item["PARAMETERCODE"];
        //     $parameterName = $item["PARAMETERNAAM"];
        //     $description = $item["OMSCHRIJVING"];
        //     $dataType = $item["DATATYPE"];
        //     $value = $item["WAARDE"];
        //     $lookupQuery = $item["LOOKUPQUERY"];
        //     $waardeLijst = json_encode($item["WAARDELIJST"], JSON_HEX_QUOT | JSON_HEX_TAG);
        //     $recordId = $item["PLR__RECORDID"];

        //     // Create a unique key for each group
        //     $key = array_search($groupCode, array_column($transformedArray, "groepcode"));

        //     // If group does not exist, add it to the transformed array
        //     if ($key === false) {
        //         $transformedArray[] = [
        //             "groepcode" => $groupCode,
        //             "groepnaam" => $groupName,
        //             "groepomschrijving" => $groupDescription,
        //             "volgorde" => $order,
        //             "parameters" => []
        //         ];
        //         $key = count($transformedArray) - 1;
        //     }

        //     // Add parameter to the corresponding group
        //     $transformedArray[$key]["parameters"][] = [
        //         "parametercode" => $parameterCode,
        //         "parameternaam" => $parameterName,
        //         "omschrijving" => $description,
        //         "datatype" => $dataType,
        //         "waarde" => $value,
        //         "waardelijst" => $waardeLijst,
        //         "lookupquery" => $lookupQuery,
        //         "plr__recordid" => $recordId
        //     ];
        // }

        // Create the final JSON
        $json = json_encode(["parametergroepen" => $_rsGroepen, "parameters" => $_rsParameters]);
        return $json;
    }

    function ProcessLookupParameters($recordset) {
        global $polaris;

        foreach($recordset as $index => $record) {
            if ($record['DATATYPE'] == 'L') {
                $_sql = $polaris->ReplaceDynamicVariables($record['LOOKUPQUERY']);
                $_select = SQLAsSelectList($this->userdatabase, $_sql, 0, 1, $record['WAARDE'], $id='idselect', $selectname="_hdnRecordID{$record['PLR__RECORDID']}", $extra="data-column='{$record['PLR__RECORDID']}'");
                $recordset[$index]['WAARDELIJST'] = $_select;
                unset($recordset[$index]['LOOKUPQUERY']);
            }
        }
        return $recordset;
    }

    function ShowListView($state, $permission, $detail=false, $masterrecord=false) {
        $this->AddModuleJS(['parameters_main.js']);

        $this->smarty->assign('permission', $permission);
        $this->smarty->display("parameters.tpl.php");
    }

}
