<?php
require('_shared/module.plr_maxia.php');

class ModulePLR_Maxia_Zelfwijs extends ModulePLR_Maxia {

    const MOD_INSTRUCTIES = 1;
    const MOD_CATEGORIEEN = 2;

    function __construct($moduleid, $module, $clientname=false) {
        parent::__construct($moduleid, $module, $clientname);

        $this->stylesheet = 'default.css';
        $this->AssignParameters();
    }

    function __GetCustomFilter() {
        $_filter = $this->form->AddDataScopeFilter();
        $_filter = $this->form->owner->AddMasterFilter($_filter);
        return $_filter;
    }

    function AssignParameters() {
        switch($this->moduleid) {
            case 1:
                $this->smarty->assign("status_vrij","Vrij");
                $this->smarty->assign("status_bezet","Bezet");
                break;
            case 2:
                $this->smarty->assign("status_vrij","Beschikbaar");
                $this->smarty->assign("status_bezet","Uitgeleend");
                break;
            case 4:
                $this->smarty->assign("status_vrij","Beschikbaar");
                $this->smarty->assign("status_bezet","Uitgeleend");
                break;
            default:
                $this->smarty->assign("status_vrij","Beschikbaar");
                $this->smarty->assign("status_bezet","Uitgeleend");
                break;
        }
    }

    function ProcessPostRequests($record, $event){
        switch ($event) {
            case 'assetstoevoegen':
                $_result = $this->VoegAssetsToe($record);
                break;
            case 'assetuitlenen':
                $_result = $this->AssetUitlenen($record);
                break;
            case 'assetontvangst':
                $_result = $this->AssetOntvangst($record);
                break;
            case 'assetbuitenwerking':
                $_result = $this->AssetBuitenwerking($record);
                break;
            case 'assetverlengen':
                $_result = $this->AssetVerlengen($record);
                break;
            case 'assetuitlenendetails':
                $_result = $this->BewaarAssetUitleenDetails($record);
                break;
        }
        return $_result;
    }

    function ProcessShowRequests($record, $event){

        $_content = false;
        switch ($event) {
            case 'instructies':
                $_content = $this->GetInstructies();
                break;
            case 'instructie':
                $_instructie_id = $_GET['instructie_id'];
                $_content = $this->GetInstructieDetails($_instructie_id);
                break;
            default:
                break;
        }

        if ($_content !== false) {
            echo $_content;
            exit; // stop with the rest of the html
        }
    }

    function IsAssetBeschikbaar($asset_id) {
        $_sql = "
        SELECT INGELEVERD
        FROM fb_assetuitlening ttt
        WHERE {$this->__GetCustomFilter()} AND ASSET = ?
        AND EINDDATUM IS NULL
        ORDER BY STARTDATUM DESC
        LIMIT 0,1
        ";
        $_result = $this->form->database->userdb->GetOne($_sql, array($asset_id));

        return (empty($_result) or $_result == 'N');
    }

    function AssetBuitenwerking($record) {
        $_assetrecord = $this->_GetAssetDetails($record['_hdnRecordID'])['result'];
        if ($_assetrecord['STATUS'] == 'ingebruik') {
            throw new Exception('Dit item is momenteel uitgeleend.');
        }
        $_sql = "UPDATE fb_assets ttt SET BUITENWERKING = 'Y' WHERE {$this->__GetCustomFilter()} AND ASSET_ID = ?";
        $_result = $this->form->database->userdb->Execute($_sql, array($_assetrecord['ASSET_ID']));

        return json_encode($_result);
    }

    function AssetOntvangst($record) {
        $_assetrecord = $this->_GetAssetDetails($record['_hdnRecordID'])['result'];
        $_beschikbaar = $this->IsAssetBeschikbaar($_assetrecord['ASSET_ID']);
        if (!$_beschikbaar) {
            throw new Exception('Dit item is momenteel niet uitgeleend.');
        }
        $_sql = "UPDATE fb_assetuitlening ttt SET EINDDATUM = NOW(), INGELEVERD = 'Y', ONTVANGSTDOOR = ? WHERE {$this->__GetCustomFilter()} AND ASSETUITLENING_ID = ? AND INGELEVERD = 'N'";
        $_result = $this->form->database->userdb->Execute($_sql, array($_SESSION['name'], $_assetrecord['ASSETUITLENING_ID']));

        return json_encode($_result);
    }

    function AssetUitlenen($record) {
        global $polaris;

        if (empty($record['PERSOON_NAAM'])) {
            throw new Exception('Vul de naam van de persoon in.');
        }
        $_assetrecord = $this->_GetAssetDetails($record['_hdnRecordID'])['result'];
        $_persoon_naam = $record['PERSOON_NAAM'];
        $_persoon_email = $record['PERSOON_EMAIL'];
        $_persoon_telefoon = $record['PERSOON_TELEFOON'];
        $_opmerking = $record['OPMERKING'];
        $_langdurig = ($record['LANGDURIG'] == 'Y') ? 1 : 0;
        $_beschikbaar = $this->IsAssetBeschikbaar($_assetrecord['ASSET_ID']);

        if ($_langdurig && !$polaris->IsUserMemberOf(GROUP_FACILITY_BEHEER)) {
            throw new Exception('U heeft geen rechten om langdurig uit te lenen.');
        }
        if (!$_beschikbaar) {
            throw new Exception('Dit item is momenteel uitgeleend.');
        }
        if ($_assetrecord['LANGEUITLEEN'] == 'Y' || $_langdurig) {
            if (empty($record['EINDDATUM'])) {
                throw new Exception('U heeft geen einddatum opgegeven.');
            }
            $_maxeinddatum = date('Y-m-d H:i', strtotime($record['EINDDATUM'].' '.($_assetrecord['LANGEUITLEEN_TIJDSTIP']??'12:00')));
            // check if maxeinddatum is within maximum days
            if ($_assetrecord['LANGEUITLEEN_MAXIMUMDAGEN'] > 0 && $_maxeinddatum > date('Y-m-d H:i', strtotime('+'.$_assetrecord['LANGEUITLEEN_MAXIMUMDAGEN'].' day'))) {
                throw new Exception('De maximale uitleentermijn is '.$_assetrecord['LANGEUITLEEN_MAXIMUMDAGEN'].' dagen.');
            }
        } else {
            $_maxeinddatum = date('Y-m-d H:i', strtotime('+'.$_assetrecord['KORTEUITLEEN_DAGEN'].' day', strtotime(date('Y-m-d').' '.$_assetrecord['KORTEUITLEEN_TIJDSTIP'])));
        }

        $_sqlUpdateBeeindigVorigeUitlening = "UPDATE fb_assetuitlening ttt SET EINDDATUM = NOW(), INGELEVERD = 'Y' WHERE {$this->__GetCustomFilter()} AND ASSET = ? AND EINDDATUM IS NULL AND INGELEVERD = 'N'";
        $this->form->database->userdb->Execute($_sqlUpdateBeeindigVorigeUitlening, array($_assetrecord['ASSET_ID']));

        $_sql = "INSERT INTO fb_assetuitlening (ASSET, STARTDATUM, MAX_EINDDATUM, UITGELEENDDOOR, PERSOON_NAAM, PERSOON_EMAIL, PERSOON_TELEFOON, OPMERKING, LANGDURIG, CLIENT_HASH) VALUES (?, NOW(), ?, ?, ?, ?, ?, ?, ?, ?)";
        $_result = $this->form->database->userdb->Execute($_sql, array($_assetrecord['ASSET_ID'], $_maxeinddatum, $_SESSION['name'], $_persoon_naam, $_persoon_email, $_persoon_telefoon, $_opmerking, $_langdurig, $this->clienthash));
        return json_encode($_result);
    }

    function AssetVerlengen($record) {
        $_assetrecord = $this->_GetAssetDetails($record['_hdnRecordID'])['result'];
        $_beschikbaar = $this->IsAssetBeschikbaar($_assetrecord['ASSET_ID']);
        if (!$_beschikbaar) {
            throw new Exception('Dit item is momenteel niet uitgeleend.');
        }
        $_nieuwedatum = $record['NIEUWEDATUM'] .' '. ($_assetrecord['KORTEUITLEEN_TIJDSTIP']??'12:00');
        if (preg_match('/^(0[1-9]|[12][0-9]|3[01])-(0[1-9]|1[0-2])-\d{4} (?:[01]\d|2[0-3]):[0-5]\d$/i', $_nieuwedatum) == false) {
            throw new Exception('Ongeldige datum/tijd formaat: '.$_nieuwedatum);
        }

        $_sql = "UPDATE fb_assetuitlening ttt SET MAX_EINDDATUM = STR_TO_DATE(?, '%d-%m-%Y %H:%i') WHERE {$this->__GetCustomFilter()} AND ASSETUITLENING_ID = ? AND INGELEVERD = 'N'";
        $_result = $this->form->database->userdb->Execute($_sql, array($_nieuwedatum, $_assetrecord['ASSETUITLENING_ID']));

        return json_encode($_result);
    }

    function GetMaximumVolgordenummer($moduleid) {
        $_sql = "
        SELECT MAX(VOLGORDE)
        FROM fb_assets a
        RIGHT JOIN fb_assettypes at ON at.ASSETTYPE_ID = a.ASSET_TYPE
        WHERE at.ASSET_TYPE_CODE = ?";
        $_result = $this->form->database->userdb->GetOne($_sql, array($moduleid));
        // if result is empty, return 0
        return empty($_result) ? 0 : $_result;
    }

    function GetAantalAssets($moduleid) {
        $_sql = "
        SELECT COUNT(*)
        FROM fb_assets a
        RIGHT JOIN fb_assettypes at ON at.ASSETTYPE_ID = a.ASSET_TYPE
        WHERE at.ASSET_TYPE_CODE = ?";
        $_result = $this->form->database->userdb->GetOne($_sql, array($moduleid));
        // if result is empty, return 0
        return empty($_result) ? 0 : $_result;
    }

    function VoegAssetsToe($record) {
        $_aantal = intval($record['AANTAL']);
        $_vanaf = intval($record['VANAF']);
        $_prefix = substr($record['PREFIX'], 0, 20);
        $_locatie = $this->GetActieveDefaultLocatie();

        if (empty($_locatie)) {
            throw new Exception('U dient een locatie te selecteren.');
        }

        if ($_aantal < 0 or $_aantal > self::MAX_TOEVOEGEN_ASSETS) {
            throw new Exception('U kunt maximaal '.self::MAX_TOEVOEGEN_ASSETS.' items in een keer toevoegen.');
        }

        if ($this->GetAantalAssets($this->moduleid) > self::MAX_CAPACITEIT_ASSETS) {
            throw new Exception('U kunt maximaal '.self::MAX_CAPACITEIT_ASSETS.' items van dit type hebben.');
        }

        $_assettype = $this->getAssetType($this->moduleid);
        $_maxVolgorde = $this->GetMaximumVolgordenummer($this->moduleid);

        $_sql = "INSERT INTO fb_assets (ASSET_CODE, ASSET_TYPE, ICOON, LOCATIE, RUIMTE, OPMERKING, VOLGORDE, CLIENT_HASH) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        $_result = [];
        try{
            $_volgordeIndex = $_maxVolgorde + 10;
            for ($i = $_vanaf; $i < ($_vanaf + $_aantal); $i++) {
                $_assetcode = $_prefix . $i;
                $_result[] = $this->form->database->userdb->Execute($_sql, array($_assetcode, $_assettype['ASSETTYPE_ID'], $_assettype['ICOON'], $_locatie, '', '', $_volgordeIndex, $this->clienthash));
                $_volgordeIndex += 10;
            }
        } catch (Exception $e) {
            throw new Exception('Er is al een asset toegevoegd met dezelfde code binnen deze locatie.');
        }

        return json_encode($_result);
    }

    function BewaarAssetUitleenDetails($record) {
        $_assetrecord = $this->_GetAssetDetails($record['_hdnRecordID'])['result'];
        $_persoon_naam = $record['PERSOON_NAAM'];
        $_persoon_email = $record['PERSOON_EMAIL'];
        $_persoon_telefoon = $record['PERSOON_TELEFOON'];
        $_opmerking = $record['OPMERKING'];

        $_sql = "UPDATE fb_assetuitlening ttt SET PERSOON_NAAM = ?, PERSOON_EMAIL = ?, PERSOON_TELEFOON = ?, OPMERKING = ? WHERE {$this->__GetCustomFilter()} AND ASSETUITLENING_ID = ? AND INGELEVERD = 'N'";
        $_result = $this->form->database->userdb->Execute($_sql, array($_persoon_naam, $_persoon_email, $_persoon_telefoon, $_opmerking, $_assetrecord['ASSETUITLENING_ID']));

        return json_encode($_result);
    }

    function getAssetType($moduleid) {
        $_sql = "
        SELECT ASSETTYPE_ID, ASSET_TYPE, OPMERKING, ACTIEF, ICOON, UITLEENBAAR_DOOR
        , ASSET_CODE_PREFIX, RESERVEERBAAR, VERLENGBAAR, LOCATIE_GEBONDEN, KLANTSORTERING
        FROM fb_assettypes WHERE ASSET_TYPE_CODE = ?";
        $_result = $this->form->database->userdb->GetRow($_sql, array($moduleid));
        return $_result;
    }

    function getAssetTypes() {
        $_sql = "
        SELECT ASSETTYPE_ID, ASSET_TYPE, OPMERKING, ACTIEF, ICOON, UITLEENBAAR_DOOR
        , ASSET_CODE_PREFIX, RESERVEERBAAR, VERLENGBAAR, LOCATIE_GEBONDEN
        FROM fb_assettypes
        WHERE client_hash = ? AND ACTIEF = 'Y' ORDER BY ASSET_TYPE";
        $_result = $this->form->database->userdb->GetAll($_sql, array($_SESSION['clienthash']));
        return $_result;
    }

    function _GetAssetDetails($asset_id) {
        global $scramble;

        $_sql = "
            SELECT ASSET_ID, ASSET_CODE, ASSET_SUBCODE, TTT.ICOON, TTT.ASSET_TYPE, ATY.ASSET_TYPE AS ASSET_TYPE_NAME, LOCATIE, RUIMTE
            , CASE
                WHEN TTT.BUITENWERKING = 'Y' THEN 'inactief'
                WHEN atu.INGELEVERD = 'N' THEN 'ingebruik'
                ELSE 'vrij'
            END
            AS `STATUS`
            , DATE_FORMAT(atu.MAX_EINDDATUM, '%e %M %Y') AS MAX_EINDDATUM_DATUM
            , DATE_FORMAT(atu.MAX_EINDDATUM, '%H:%i') AS MAX_EINDDATUM_TIJD
            , DATE_FORMAT(atu.STARTDATUM, '%e %M %Y') AS STARTDATUM_DATUM
            , DATE_FORMAT(atu.STARTDATUM, '%H:%i') AS STARTDATUM_TIJD
            , CONCAT(
                IF(TIMESTAMPDIFF(MINUTE, NOW(), atu.MAX_EINDDATUM) < 0, CEILING(TIMESTAMPDIFF(MINUTE, NOW(), atu.MAX_EINDDATUM) / 60), FLOOR(TIMESTAMPDIFF(MINUTE, NOW(), atu.MAX_EINDDATUM) / 60))
                , ' uur '
                , LPAD(ABS(TIMESTAMPDIFF(MINUTE, NOW(), atu.MAX_EINDDATUM) % 60), 2, '0'), ' min') AS RESTERENDETIJD
            , LEAST(ROUND((1 - TIMESTAMPDIFF(MINUTE, NOW(), atu.MAX_EINDDATUM) / (TIMESTAMPDIFF(MINUTE, ATU.STARTDATUM, ATU.MAX_EINDDATUM))) * 100), 100) AS RESTERENDETIJD_PERCENTAGE
            , IF (NOW() > atu.MAX_EINDDATUM, 1, 0) AS OVERTIJD
            , ATU.LANGDURIG, TTT.OPMERKING AS ASSET_OPMERKING, ATU.OPMERKING AS OPMERKING, ATU.STARTDATUM, ATU.EINDDATUM, ATU.MAX_EINDDATUM, ATU.INGELEVERD
            , ATU.PERSOON_NAAM, ATU.PERSOON_TELEFOON, ATU.PERSOON_EMAIL
            , ".DEFAULT_DB_HASH_FUNCTION."(CONCAT('{$scramble}','_',TTT.ASSET_ID)) AS PLR__RECORDID
            , ASSETUITLENING_ID
            , aty.KORTEUITLEEN_TIJDSTIP
            , aty.KORTEUITLEEN_DAGEN
            , aty.LANGEUITLEEN
            , aty.LANGEUITLEEN_TIJDSTIP
            , aty.LANGEUITLEEN_MAXIMUMDAGEN
            , aty.OPMERKING AS ASSETTYPE_OPMERKING
            FROM vw_fb_assets ttt
            LEFT JOIN fb_assettypes aty ON ttt.asset_type = aty.assettype_id
            AND ttt.client_hash = aty.client_hash
            LEFT JOIN fb_assetuitlening atu ON ttt.asset_id = atu.asset
            AND atu.EINDDATUM IS NULL AND atu.INGELEVERD = 'N'
            WHERE {$this->__GetCustomFilter()}
            AND {$this->form->database->makeRecordIDColumn('fb_assets', true)} = ? ";

        $_mainresult = $this->form->database->userdb->GetRow($_sql, array($asset_id));

        $_sql = "
            SELECT
              DATE_FORMAT(a.STARTDATUM, '%e %M %Y') AS STARTDATUM_DATUM
            , DATE_FORMAT(a.STARTDATUM, '%H:%i') AS STARTDATUM_TIJD
            , DATE_FORMAT(a.EINDDATUM, '%e %M %Y') AS EINDDATUM_DATUM
            , DATE_FORMAT(a.EINDDATUM, '%H:%i') AS EINDDATUM_TIJD
            , a.INGELEVERD, a.PERSOON_NAAM, a.PERSOON_TELEFOON, a.PERSOON_EMAIL
            , ASSETUITLENING_ID
            FROM fb_assetuitlening a
            LEFT JOIN fb_assets ttt ON a.asset = ttt.asset_id
            WHERE a.EINDDATUM IS NOT NULL AND a.INGELEVERD = 'Y'
            AND {$this->__GetCustomFilter()}
            AND {$this->form->database->makeRecordID(['a.ASSET'])} = ?
            ORDER BY a.STARTDATUM DESC
            LIMIT 0,".self::MAX_HISTORIE_ASSETS;

        $_historie = $this->form->database->userdb->GetAll($_sql, array($asset_id));

        return array('result' => $_mainresult, 'historie' => $_historie);
    }

    function GetAssetDetails($asset_id) {
        return json_encode($this->_GetAssetDetails($asset_id));
    }

    function _GetInstructies($status=null) {
        global $scramble;

        $_sql = "
            SELECT ttt.TITEL, ttt.ARTIKELCODE, ttt.CATEGORIE, ct.NAAM AS CATEGORIENAAM, TTT.TAGS, TTT.INHOUD
            , " . $this->form->database->makeRecordIDColumn('kb_artikelen', true) . " AS PLR__RECORDID
            FROM kb_artikelen ttt
            RIGHT JOIN kb_categorien ct ON ttt.categorie = ct.categorie_id
            AND ttt.client_hash = ct.client_hash
            WHERE {$this->__GetCustomFilter()}
        ";

        $_searchfilter = $_GET['q'];
        if (!empty($_searchfilter) and is_array($_searchfilter) and !empty($_searchfilter[0])) {
            $_searchfilter = $_searchfilter[0];
            $_search = " (ttt.ARTIKELCODE LIKE '%{$_searchfilter}%'
            OR ttt.TITEL LIKE '%{$_searchfilter}%'
            OR ttt.INHOUD LIKE '%{$_searchfilter}%'
            ";
            $_sql = sqlCombineWhere($_sql, $_search);
        }

        return $this->form->database->userdb->GetAll($_sql);
    }

    function GetInstructies() {
        return json_encode($this->_GetInstructies());
    }

    function GetAssetsOverview() {
        return json_encode($this->_GetAssets('ingebruik'));
    }

    function ShowFormView($state, $permission, $detail=false, $masterrecord=false) {
        $_customfilter = $this->__GetCustomFilter();
        $_recordset = $this->form->GetViewContent($detail=false, $masterrecord=false, $limit=false, $_customfilter);

        $this->smarty->assign('item', $_recordset[0]);
        $this->smarty->display("lockeritem.tpl.php");
    }

    function ShowListView($state, $permission, $detail=false, $masterrecord=false) {
        global $polaris;

        $this->AddCustomJS(['/modules/_shared/module.plr_maxia/js/maxia.js']);
        $this->AddModuleJS(['zelfwijs_main.js']);

        $this->smarty->assign('permission', $permission);
        $this->smarty->assign('isfacilitybeheerder', $polaris->IsUserMemberOf(GROUP_FACILITY_BEHEER));

        switch ($this->moduleid) {
            case self::MOD_INSTRUCTIES:
                $this->smarty->display("instructies.tpl.php");
                break;
            case self::MOD_CATEGORIEEN:
                $this->form->LoadViewContent($detail, $masterrecord, $limit=false, $this->__GetCustomFilter());
                $this->smarty->assign('items', $this->form->rsdata);
                $this->smarty->display("categorieen.tpl.php");
                break;
            default:
                break;
        }
    }

    function ShowAction($state, $permission, $detail=false, $masterrecord=false) {
        switch($_GET['moduleid']) {
            case 'voegassetstoe':
                $this->smarty->assign('assettypes', $this->getAssetTypes());
                echo $this->Fetch("voegassetstoe.tpl.php");
                break;
            default:
                break;
        }
    }

}
