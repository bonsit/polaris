<style>
    .datum-lijst {
        margin-top: 10px;
        max-height: 200px;
        overflow: scroll;
    }

    .datum-lijst .btn.btn-danger {
        text-decoration: line-through;
    }

</style>
<div class="modal-header">
    <span class="badge badge-default pull-right">{$reserveringscode}</span>
    <h3 class="modal-title">Reserveringdag {$verwijderdatum|date_format:"%d-%m-%Y"} verwijderen</h3>
    <br/>
    <p>Aangevraagd voor: <b>{$contactpersoon}</b> - Omschrijving: <b>{$aanvraag_omschrijving}</b></p>
</div>
<div class="modal-body">
    {{if $datums|count == 1}}
        <p class="bg-warning p-xs"><b>{icon name="warning"} Deze reservering bestaat uit één dag.<br/>
        Om deze dag te verwijderen dient u de hele reservering te verwijderen.</b>
        <br/><br/>Klik op Verwijderen om de hele reservering te verwijderen.</p>
        <input type="hidden" name="VERWIJDER_RESERVERING" value="{$plr__recordid}">
    {{else}}
        <p>Weet u zeker dat u deze dag van de reservering wilt verwijderen?</p>
        <p>(Eventueel kunt u andere dagen selecteren om te verwijderen.)</p>

        <div class="datum-lijst btn-group" id="ajaxdialog">
        {section name=i loop=$datums}
            <span class="btn {if $verwijderdatum == $datums[i]}btn-danger{else if $datums[i]|in_array:$verwijderdedatums}btn-danger disabled{else}btn-default{/if}" data-datum="{$datums[i]}">{$datums[i]|date_format:"%d-%m-%Y"} <input type="checkbox" style="display:none" class="" name="VERWIJDER_DATUM[]" {if $verwijderdatum == $datums[i] || $datums[i]|in_array:$verwijderdedatums}checked{/if} value="{$datums[i]}"></span>
        {/section}
        </div>

    {{/if}}
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default jqmClose" data-dismiss="modal">Annuleren</button>
    <button type="button" class="btn btn-danger jqmAccept" id="delete-reservation">Verwijderen</button>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $(".datum-lijst span.btn").on('click', function(e) {
            $(this).toggleClass('btn-danger');
            var $checkbox = $(this).find('input[type="checkbox"]');
            $checkbox.prop('checked', !$checkbox.prop('checked'));
        });
    });
</script>