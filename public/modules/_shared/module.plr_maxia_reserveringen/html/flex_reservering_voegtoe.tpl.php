<script id="template-flexplek-view" type="text/x-jsrender">
    <input type="hidden" data-link="eventid()" id="_fldEVENTID" />

    <div class="bezetting-group row">
        <div class="col-lg-6">
            {{if datum() == null}}
            <div class="row form-group">
                <h4>Welke datum?</h4>
                <div class="input-group">
                    <input type="text" class="text date_input required form-control" id="flexplek-datum" data-link="datum()||vandaag"  />
                </div>
            </div>
            {{/if}}

            <div class="row form-group">
                <h4>Welke ruimte?</h4>
                <div class="input-group">
                    <select class="select2 required form-control" id="flexplek-ruimte-id" data-link="ruimte_id()">
                    {{for ruimtes()}}
                        <option value="{{:RUIMTE_ID}}"">{{:RUIMTE_CODE}} {{:RUIMTE_NAAM}}</option>
                    {{/for}}
                    </select>
                </div>
            </div>

            <div class="row form-group">
                <h4>Kies de gewenste tijd</h4>
                <div class="input-group">
                    <input type="radio" data-link="
                        {on 'ifChecked' ~periodeClicked}
                        checked{:startTijd()==~constants.ochtend.start && eindTijd()==~constants.middag.eind}" class="icheck flexplek_periode" name="flexplek_periode" value="heledag" id="flexplek_periode-heledag" />
                        <label for="flexplek_periode-heledag">Hele dag</label>
                </div>
            </div>
            <div class="row form-group">
                <div class="input-group">
                    <input type="radio" data-link="
                        {on 'ifChecked' ~periodeClicked}
                        checked{:startTijd()==~constants.ochtend.start && eindTijd()==~constants.ochtend.eind}
                        " class="icheck flexplek_periode" name="flexplek_periode" value="ochtend" id="flexplek_periode-ochtend" />
                        <label for="flexplek_periode-ochtend">Ochtend</label>
                </div>
            </div>

            <div class="row form-group">
                <div class="input-group">
                    <input type="radio" data-link="
                        {on 'ifChecked' ~periodeClicked}
                        checked{:startTijd()==~constants.middag.start && eindTijd()==~constants.middag.eind}
                    " class="icheck flexplek_periode" name="flexplek_periode" value="middag" id="flexplek_periode-middag" />
                    <label for="flexplek_periode-middag">Middag</label>
                </div>
            </div>

            <div class="row form-group">
                <div class="input-group">
                    <input type="radio" data-link="
                        {on 'ifChecked' ~aangepasteTijdClicked}
                        checked{:
                        (startTijd()!==~constants.ochtend.start
                        || eindTijd()!==~constants.ochtend.eind)
                        && (startTijd()!==~constants.middag.start
                        || eindTijd()!==~constants.middag.eind)
                        && (startTijd()!==~constants.ochtend.start
                        || eindTijd()!==~constants.middag.eind)
                    }"
                    class="icheck" name="flexplek_periode" value="tijd" id="flexplek_periode-aangepast">
                    <label for="flexplek_periode-aangepast">Aangepaste tijd...</label>
                    <span class="" id="flex-aangepaste-tijd-panel">
                        <input type="text" size="6" step="900" class="datepair start inputtime" data-link="startTijd()" id="_fldSTARTTIJD" /> t/m
                        <input type="text" size="6" step="900" class="datepair end inputtime" data-link="eindTijd()" id="_fldEINDTIJD" />
                    </span>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <h4>Terugkerende reservering</h4>
            <p>Volgt binnenkort...</p>
            <div id="_fldRRULECOMPONENT"></div>
{{*
            <div class="btn-group schema-type" role="group">
                <button type="button" data-link="
                {on ~schema_typeClicked}
                class{merge:schema_type()=='dagelijks' toggle='active'}
                " class="btn dagelijks btn-sm btn-default">Dagelijks</button>
                <button type="button" data-link="
                {on ~schema_typeClicked}
                class{merge:schema_type()=='wekelijks' toggle='active'}
                " class="btn wekelijks btn-sm btn-default">Wekelijks</button>
            </div>

            <div class="panel panel-default panel-dagelijks" data-link="visible{:schema_type()=='dagelijks'}">
                <div class="panel-body">

                    <div class="form-group row form-group-sm">
                        <label class="col-xs-12 col-md-4 col-form-label control-label" title="Aantal keer">Aantal keer</label>
                        <div class="col-xs-12 col-md-8">
                            <div class="input-group">
                                <input data-link="contactpersoon()" type="text" label="Gereserveerd voor" class="text validate required validation_allchars"
                                    id="_fldAANTALKEER" name="AANTALKEER" size="5" maxlength="2">
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="panel panel-default panel-wekelijks" data-link="visible{:schema_type()=='wekelijks'}">
                <div class="p-m">

                    <div class="col-sm-10 form-group form-group-sm">
                        <label class="checkbox-inlinex xweekdagblock">
                            <input type="checkbox" class="icheck weekdag" id="_fldMAANDAG" value="1"> Ma
                        </label>
                        <label class="checkbox-inlinex xweekdagblock">
                            <input type="checkbox" class="icheck weekdag" id="_fldDINSDAG" value="2"> Di
                        </label>
                        <label class="checkbox-inlinex xweekdagblock">
                            <input type="checkbox" class="icheck weekdag" id="_fldWOENDAG" value="4"> Wo
                        </label>
                        <label class="checkbox-inlinex xweekdagblock">
                            <input type="checkbox" class="icheck weekdag" id="_fldDONDERDAG" value="8"> Do
                        </label>
                        <label class="checkbox-inlinex xweekdagblock">
                            <input type="checkbox" class="icheck weekdag" id="_fldVRIJDAG" value="16"> Vr
                        </label><br><br>
                        <label class="checkbox-inlinex xweekdagblock">
                            <input type="checkbox" class="icheck weekdag" id="_fldZATERDAG" value="32"> Za
                        </label>
                        <label class="checkbox-inlinex xweekdagblock">
                            <input type="checkbox" class="icheck weekdag" id="_fldZONDAG" value="64"> Zo
                        </label>
                    </div>

                </div>
            </div>
*}}


        </div>
    </div>

    <hr>
    <div id="_rowCONTACTPERSOON" class="form-group row form-group-sm">
        <label class="col-xs-12 col-md-3 col-form-label control-label" title="CONTACTPERSOON">Gereserveerd voor</label>
        <div class="col-xs-12 col-md-9">
            <div class="input-group">
                <input tabindex="5" autofocus data-link="contactpersoon()" type="text" label="Gereserveerd voor" class="text validate required validation_allchars"
                    id="_fldCONTACTPERSOON" name="CONTACTPERSOON" size="50" maxlength="100">
            </div>
        </div>
    </div>

    <div id="_rowCONTACTPERSOON_EMAIL" class="form-group row form-group-sm">
        <label class="col-xs-12 col-md-3 col-form-label control-label" title="CONTACTPERSOON_EMAIL">Medewerker e-mailadres </label>
        <div class="col-xs-12 col-md-9">
            <div class="input-group">
                <input tabindex="10" data-link="contactpersoon_email()" type="text" label="Medewerker email" class="text validate required validation_email"
                    id="_fldCONTACTPERSOON_EMAIL" name="CONTACTPERSOON_EMAIL" size="50" maxlength="100">
            </div>
        </div>
    </div>
</script>

<form id="flexplek-form">
<?php
/*

require(getcwd().'../../../../../includes/app_includes.inc.php');

$polaris = new base_Polaris();
// $polaris->start();

echo "<div class='componentline'>";
$componentname = 'maxia_rrule';
echo file_get_contents(PLR_DIR.'/guicomponents/component.'.$componentname.'.php');
require_once(PLR_DIR.'/guicomponents/component.'.$componentname.'.php');
$classname = $componentname.'Component()';
// maxia_rruleComponent
$component = new $classname();
$userdb = null;
echo $component->GenerateXHTML(array($polaris, false, 'RRULE', $userdb, 'RRULE'));
echo "</div>";
*/
?>
    <h2>Flexplek reserveren</h2>
    <div id="flexplek-view" class="flexplek-view"></div>
    <div id="bezetting-view" class="bezetting-view"></div>
    <div class="buttons">
        <button type="button" tabindex="8800" id="btnFLEXSAVEANDCLOSE" class="btn btn-primary jqmAccept positive">Reserveer</button>
        <button type="button" tabindex="8810" id="btnFLEXCLOSE" class="btn btn-default jqmClose secondary">Annuleer</button>

        <button type="button" tabindex="8820" id="btnFLEXDELETE" class="btn btn-danger jqmClose secondary pull-right">Verwijder</button>
    </div>
</form>
