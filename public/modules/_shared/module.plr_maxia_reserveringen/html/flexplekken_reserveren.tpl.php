{include file="shared.tpl.php"}

{literal}<script id="flexplek-blok" type="text/x-jsrender">
    <div class="flexplek_blok selected">
        <div class="FLEXPLEK">
            <div class="ruimte">
                <div class="RUIMTE_CODE">{{:RUIMTE_CODE}}</div>
                <div class="RUIMTE_NAAM">{{:RUIMTE_NAAM}}</div>
            </div>
            <div class="progress-title">Vrij {{:AANTALVRIJ}} van {{:AANTALFLEXPLEKKEN}}</div>
            <div class="progress">
                <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
        </div>
    </div>
</script>{/literal}

<div id="flexplekken-reserveren" class="row">
    <div class="col-xs-3" style="min-width: 230px;">

        <h3>Kies de gewenste flexplek</h3>

        <div id="datepicker-maand"></div>
        <input type="hidden" id="flex-gekozen-datum">



    </div>
    <div class="col-xs-9">

        <div class=" form-group row form-group-sm">
            <div id="flexplekken-blok" class="input-group col-sm-12x"></div>
        </div>

    </div>
    <div class="col-md-6">
    </div>
</div>
