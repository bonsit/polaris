Maxia.FlexplekkenKalender = {};
Base.update(Maxia.FlexplekkenKalender, {
    modelFlexplek: null,
    flexplekData: null,
    calendar: null,
    initialiseer: function() {
        const Self = Maxia.FlexplekkenKalender;

        Self.initialiseerKalender();
        Maxia.initialiseerKalenderEvents(Self.calendar);
    },
    initialiseerKalender: function() {
        const Self = Maxia.FlexplekkenKalender;

        var calEl = $('#flexplekken_calendar');
        if (calEl.length == 0) return;

        this.modelFlexplek = $.views.viewModels({
            getters: ["eventid","datum","description","startTijd","eindTijd","locatie"
            ,"contactpersoon","contactpersoon_email","plr__recordid","ruimte_id"
            ,"ruimte_code","ruimte_naam","schema_type","ruimtes"], // get/set properties
        });
        this.modelFlexplek.ruimtes = [];

        var bewaardeView = Maxia.getKalenderView('customDag');
        var bewaardeDate = Maxia.getKalenderDatum(new Date());

        Self.calendar = new FullCalendar.Calendar($('#flexplekken_calendar')[0], {
            schedulerLicenseKey: '0445008747-fcs-1681420837',
            initialView: bewaardeView,
            initialDate: bewaardeDate,
            height: $(window).height()*0.75,
            locale: 'nl',
            firstDay: 1,
            timeZone: 'Europe/Amsterdam',
            scrollTime: '07:00',
            scrollTimeReset: false,
            headerToolbar: {
                left: 'voegReserveringToe',
                center: 'title',
                right: 'customDag,customTimeGridWeek,customDayGridMonth,printList today,prev,next'
            },
            weekNumbers: false,
            businessHours: {
                // days of week. an array of zero-based day of week integers (0=Sunday)
                daysOfWeek: [ 1, 2, 3, 4, 5 ], // Monday - Friday
                startTime: '7:00',
                endTime: '18:00',
            },
            // slotMinTime: '07:00',
            nowIndicator: true,
            contentHeight: 500,
            editable: true,
            slotEventOverlap: false,
            selectable: true,
            eventStartEditable: true,
            eventResizableFromStart: true,
            eventDisplay: 'block',
            // eventDisplay: 'list-item',
            // eventColor: '#378006',
            // eventTextColor: '#000',
            allDayText: 'hele dag',
            listDaySideFormat: false,
            listDayFormat: {
                month: 'long',
                year: 'numeric',
                day: 'numeric',
                weekday: 'long'
            },
            customButtons: {
                voegReserveringToe: {
                    text: 'Reserveer flexplek',
                    click: function(e, elem) {
                        Maxia.FlexplekkenKalender.nieuweReservering();
                    }
                }
            },
            buttonText: {
                'today': 'Vandaag'
            },
            resourceAreaColumns: [
                {
                  field: 'title',
                  headerContent: 'Ruimtes'
                }
            ],
            resources: '/services/json/app/facility/form/overzicht_flexplekken/?event=resources',
            events: '/services/json/app/facility/form/overzicht_flexplekken/?event=events',
            views: {
                customDag: {
                    type: 'resourceTimeGridDay',
                    buttonText: 'Dag',
                    titleFormat: {
                        year: 'numeric', month: 'long', day: 'numeric', weekday: 'long'
                    },
                },
                customResourceTimelineDay: {
                    type: 'resourceTimelineDay',
                    buttonText: 'Ruimtes/dag',
                    dateAlignment: 'day',
                    resourceAreaWidth: "10%",
                },
                customResourceWeek: {
                    type: 'resourceTimelineWeek',
                    buttonText: 'Ruimtes/week',
                    scrollTime: '12:00',
                    resourceAreaWidth: "10%",
                },
                customDayGridWeek: {
                    type: 'dayGridWeek',
                    buttonText: 'Week',
                },
                customDayGridMonth: {
                    type: 'dayGridMonth',
                    buttonText: 'Maand',
                },
                customDayGridYear: {
                    type: 'dayGridYear',
                    buttonText: 'Jaar',
                },
                customTimeGridWeek: {
                    type: 'timeGridWeek',
                    duration: { days: 7 },
                    buttonText: 'Week',
                    dateAlignment: 'week',
                    // hiddenDays: [0, 6],
                },
                printList: {
                    type: 'listWeek',
                    duration: { days: 6 },
                    buttonText: 'Lijst',
                    dateAlignment: 'week',
                    // hiddenDays: [0, 6],
                }
            },
            viewDidMount: function(cal) {
                Maxia.bewaarKalenderView(cal);
                Maxia.bewaarKalenderDatum(Self.calendar);
            },
            eventResize: function(info) {
                let eventObject = Maxia.FlexplekkenKalender.getPartsFromEvent(info.event);
                Self.updateReservering(info, 'edit', eventObject.plr__recordid, eventObject.datum, eventObject.startTijd, eventObject.eindTijd, eventObject.contactpersoon, eventObject.contactpersoon_email, eventObject.ruimte_id);
            },
            eventDrop: function(info) {
                if (confirm('Weet u zeker dat u deze reservering wilt verplaatsen?')) {
                    let eventObject = Maxia.FlexplekkenKalender.getPartsFromEvent(info.event);
                    Self.updateReservering(info, 'edit', eventObject.plr__recordid, eventObject.datum, eventObject.startTijd, eventObject.eindTijd, eventObject.contactpersoon, eventObject.contactpersoon_email, eventObject.ruimte_id);
                } else {
                    info.revert();
                }
            },
            select: function(info) {
                Maxia.FlexplekkenKalender.nieuweReservering(info);
            },
            eventClick: function(info) {
                info.jsEvent.preventDefault();
                Maxia.FlexplekkenKalender.wijzigReservering(info);
            },
            resourceLabelDidMount: function(info) {
                $('.fc-col-header-cell-cushion', info.el).popover({
                    placement:'bottom',
                    delay: {
                        'show':500,
                        'hide': 0
                    },
                    trigger:'hover',
                    container:'body',
                    html:true,
                    content:info.resource.extendedProps.description
                });
            },
            eventDidMount: function(info) {
                let divtitle = $('<div />').addClass('title');
                let divtime = $('.fc-event-time', info.el);
                const spanroomid = $('<div />').addClass('roomid').html(info.event.extendedProps.ruimte_code);
                const spanroomname = $('<span />').addClass('roomname').html(info.event.extendedProps.ruimte_naam);
                const divperson = $('<div />').addClass('person').html(info.event.extendedProps.contactpersoon);
                // divtime.append(spanroomid);
                divtitle.html(spanroomname).append(spanroomid).append(divperson);
                $(".fc-event-title", info.el).html('');
                $(".fc-event-title", info.el).html(divtitle);

                let cleandescription = divtime.text()
                + '<br>' + info.event.extendedProps.ruimte_naam
                + '' + '<div class="roomid dark">Ruimte ' + info.event.extendedProps.ruimte_code + '</div>'
                + '' + info.event.extendedProps.contactpersoon;

                $('.fc-event-main', info.el).popover({
                    placement:'bottom',
                    delay: {
                        'show':500,
                        'hide': 0
                    },
                    trigger:'hover',
                    container:'body',
                    html:true,
                    content:cleandescription
                });
            },
        });
        Self.calendar.render();
    },
    helpers: {
        constants: {
            'ochtend': {'start': '08:00', 'eind': '12:00'},
            'middag': {'start': '13:00', 'eind': '17:00'},
            'heledag': {'start': '08:00', 'eind': '17:00'},
        },
        periodeClicked: function(e) {
            const Self = Maxia.FlexplekkenKalender;

            let tijdperiode = e.currentTarget.value;
            this.startTijd(Self.helpers.constants[tijdperiode]['start']);
            this.eindTijd(Self.helpers.constants[tijdperiode]['eind']);

            $("#flex-aangepaste-tijd-panel").hide();
        },
        aangepasteTijdClicked: function(e) {
            $("#flex-aangepaste-tijd-panel").show();
            $("#_fldSTARTTIJD").trigger('focus');
        },
        schema_typeClicked: function(e) {
            this.schema_type($(e.currentTarget).hasClass('dagelijks') ? 'dagelijks' : 'wekelijks');
            $(".schema-type .btn").removeClass('active');
            $(e.currentTarget).addClass('active');
        },
    },
    vulFlexReserveringForm: function(info, state, eventObject) {
        const Self = Maxia.FlexplekkenKalender;

        $.when(
            /**
             * Haal de ruimtes op
             */
            Self.getRuimtes(eventObject).done(function(data) {
                eventObject.ruimtes = data;
            }),
        ).then(function() {
            if (Maxia.FlexplekkenKalender.calendar.view.type == 'customDag') {
                let huidigedatum = Self.calendar.getDate().toLocaleDateString('nl-nl');
                eventObject.datum = huidigedatum;
            }

            /**
             * Map het event object naar het flexplek data model
             */
            Self.flexplekData = Self.modelFlexplek.map(eventObject);

            /**
             * Link templates aan view
             */
            $.templates("#template-flexplek-view").link("#flexplek-view", Self.flexplekData, Self.helpers);

            /**
             * Initialiseer formulier voor elementen en acties
             */
            let form = $("#flexplek-form");
            Polaris.Form.initializeFormValidation(form);
            Polaris.Form.initializeDatePickers(form);
            Polaris.Form.initializeTimePickers("#flex-aangepaste-tijd-panel");
            Polaris.Base.initializeIChecks(form);
            Polaris.Base.initializeSelect2(form);
            $("#flexplek-ruimte-id").trigger('change');
            $("#flex-aangepaste-tijd-panel").toggle($("#flexplek_periode-aangepast:checked").length == 1);

            $("#btnFLEXDELETE")
            .toggle(state == 'edit')
            .on('click', function() {
                if (confirm("Weet u zeker dat u deze reservering wilt verwijderen?")) {
                    Self.verwijderReservering($("#_fldEVENTID").val());
                };
            });
            $("#btnFLEXSAVEANDCLOSE").text(state == 'edit'?'Opslaan':'Reserveren');

        })
    },
    nieuweReservering: function(info) {
        const Self = Maxia.FlexplekkenKalender;

        Polaris.Window.showModuleForm('/modules/_shared/module.plr_maxia_reserveringen/html/flex_reservering_voegtoe.tpl.php'
        , /* on Accept */ function() {
            return Self.updateReservering(info, 'insert', Self.flexplekData.plr__recordid(), Self.flexplekData.datum(), Self.flexplekData.startTijd(), Self.flexplekData.eindTijd(), Self.flexplekData.contactpersoon(), Self.flexplekData.contactpersoon_email(), Self.flexplekData.ruimte_id());
        }
        , /* on Show   */ function() {
            Self.vulFlexReserveringForm(info, 'insert', Self.getPartsFromEvent(info));
        });
    },
    wijzigReservering: function(info) {
        const Self = Maxia.FlexplekkenKalender;

        Polaris.Window.showModuleForm('/modules/_shared/module.plr_maxia_reserveringen/html/flex_reservering_voegtoe.tpl.php'
        , /* on Accept */ function() {
            return Self.updateReservering(info, 'edit', Self.flexplekData.plr__recordid(), Self.flexplekData.datum(), Self.flexplekData.startTijd(), Self.flexplekData.eindTijd(), Self.flexplekData.contactpersoon(), Self.flexplekData.contactpersoon_email(), Self.flexplekData.ruimte_id());
        }
        , /* on Show   */ function() {
            Self.vulFlexReserveringForm(info, 'edit', Self.getPartsFromEvent(info));
        });
    },
    getPartsFromEvent: function(info) {
        let eventObject = {
            'locatie': $("#_fld_GRP_LOCATIE").val(),
            'vandaag': new Date().toLocaleDateString('nl-nl'),
            'gekozentijd': null,
            'schema_type': 'wekelijks',
            'datum': null,
            'startTijd': '08:00',
            'eindTijd': '17:00',
        };

        if (info) {
            let event = (info.event) ? info.event : info;

            eventObject.eventid = event.id

            if (event.startStr.indexOf('T') == -1) {
                eventObject.datum = new Date(event.startStr).toLocaleDateString('nl-nl');
            } else {
                eventObject.datum = new Date(event.startStr.split('T')[0]).toLocaleDateString('nl-nl');
                eventObject.startTijd = event.startStr.split('T')[1].slice(0, 5);
                eventObject.eindTijd = event.endStr.split('T')[1].slice(0, 5);
            }
            Object.assign(eventObject, event.extendedProps);
            if (event.extendedProps) {
                // If DRAGGED event, get destination room id from event definition
                eventObject.ruimte_id = event.extendedProps.ruimte_id; //event.getResources()[0].id;
            } else if (event.resource) {
                // If NEW event, get destination room id from event resource
                log(event.resource.id);
                eventObject.ruimte_id = event.resource.id;
            }
        }

        return eventObject;
    },
    verwijderReservering: function(id) {
        let $form = $("#dataform");
        let data = {
            '_hdnAction':'plr_delete'
            , '_hdnDatabase': $form.find(":input[name=_hdnDatabase]").val()
            , '_hdnTable': $form.find(":input[name=_hdnTable]").val()
            , '_hdnForm': $form.find(":input[name=_hdnForm]").val()
            , '_hdnRecordID': id
        };
        Polaris.Ajax.postJSON(_ajaxquery, data, 'Reservering is verwijderd', null, function() {
            let event = Maxia.FlexplekkenKalender.calendar.getEventById(id);
            if (event) event.remove();
        });
    },
    updateReservering: function(info, state, plr__recordid, datum, startTijd, eindTijd, contactpersoon, contactpersoon_email, ruimte_id) {
        /**
         * Valideer de velden van het flexplek formulier
         * Indien ok, dan bewaar de reservering
         */
        let result = ($("#flexplek-form").length == 0) || ($("#flexplek-form").length > 0 && $("#flexplek-form").valid());
        if (result) {
            let $form = $("#dataform");
            let data = {
                '_hdnAction':'bewaarflexreservering'
                , '_hdnProcessedByModule':'true'
                , '_hdnForm': $("#dataform :input[name=_hdnForm]").val()
                , '_hdnState': state
                , '_hdnRecordID': plr__recordid
                , 'GEWENSTE_DATUM': datum
                , 'GEWENSTE_TIJD_START': startTijd
                , 'GEWENSTE_TIJD_EIND': eindTijd
                , 'CONTACTPERSOON': contactpersoon
                , 'CONTACTPERSOON_EMAIL': contactpersoon_email
                , 'RUIMTE_FLEX': ruimte_id
            };
            Polaris.Ajax.postJSON(_ajaxquery, data, 'Reservering is aangepast', null, function() {
                Maxia.FlexplekkenKalender.calendar.refetchEvents();
            }, function(data) {
                Polaris.Base.modalDialog(
                    data.error,
                    { close: function() {}, type: 'type-danger' }
                );
                info.revert();
                return false;
            });
        }
        return result;
    },
    getRuimtes: function(eventObject) {
        return $.getJSON('/services/json/app/facility/const/overzicht_flexplek/', {
            'event': 'flexplekken_beschikbareruimtes',
            'datum':eventObject.datum,
            'tijdperiode_start':eventObject.startTijd,
            'tijdperiode_eind':eventObject.eindTijd,
        })
        .fail(function() {
            alert('De ruimtes konden niet geladen worden.');
        });
    },
});

jQuery(function () {
    Maxia.FlexplekkenKalender.initialiseer();
});
