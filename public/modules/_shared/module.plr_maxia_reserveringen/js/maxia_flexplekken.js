// Maxia.Flexplekken = {};
// Base.update(Maxia.Flexplekken, {
//     modelFlexplekken: null,
//     flexplekkenData: null,
//     modelBezetting: null,
//     bezettingData: null,
//     datePicker: null,
//     vorigeDatum: null,
//     _prevEtage: null,
//     timeperiods : {
//         'heledag':'hele dag',
//         'ochtend':'ochtend',
//         'middag':'middag',
//         'tijd':'aangepaste tijd',
//     },
//     initialiseer: function() {
//         const Self = Maxia.Flexplekken;

//         if ($("#template-flexplekken-view").length > 0) {
//             // $.views.settings.debugMode(true);

//             $("input[name=flexplek_periode]").on('ifChecked', function(e) {
//                 log('Radio button checked');
//                 let timeperiod = Self.timeperiods[$(this).val()];
//                 Self.flexplekkenData.HUIDIGETIJDPERIODE($(this).val());
//                 Self.flexplekkenData.PRETTY_HUIDIGETIJDPERIODE(timeperiod);
//                 Self.refreshFlexPlekkenBlok();
//             });

//             $("#flex-gekozen-locatie").select2({minimumResultsForSearch: -1}).on('change', function() {
//                 log('Locatie changed');
//                 // Trigger change event to update locaties when location changes
//                 Self.flexplekkenData.HUIDIGELOCATIE($(this).val());
//                 Self.refreshFlexPlekkenBlok();
//             });

//             Self.initialiseerFlexplekken().done(function() {
//                 log('Klaar met flexplekken ophalen');
//                 Self.initialiseerDatePicker();

//                 Self.flexplekkenData.PRETTY_HUIDIGETIJDPERIODE(Self.timeperiods[Self.flexplekkenData.HUIDIGETIJDPERIODE()]);
//             });

//             Self.initialiseerBezetting();
//         }
//     },
//     renderHelpers: {
//         /**
//          * Dit zijn de functies die enkele eigenschappen berekenen in onze template.
//          * We geven ze door bij het koppelen van gegevens aan onze template
//          * en gebruiken ze met het ~-teken, bijvoorbeeld: {:~getBezetting()}
//          */
//         getBezetting: function() {
//             return this.data.VOLLEDIGBEZET == 'Y' ? 'volledigbezet' : 'vrij';
//         },
//         getBezetDoor: function() {
//             if (this.data.BEZETDOOR) {
//                 var pers = this.data.BEZETDOOR.split('#');
//                 return "Bezet door:<br>" + pers.join('<br>');
//             }
//         },
//         getVrij: function() {
//             var vrij = Math.max(0, this.data.AANTALFLEXPLEKKEN-this.data.AANTALBEZET);
//             var result = '';
//             if (vrij == 0)
//                 result = 'Bezet';
//             else
//                 result = 'Vrij ' + vrij + ' van ' + this.data.AANTALFLEXPLEKKEN;
//             return result;
//         },
//         flexplekClicked: function(e) {
//             let data = $.view(e.currentTarget).data;
//             let aantalbezet = data['AANTALBEZET'];
//             $(".flexplek_blok").removeClass('selected');
//             $(e.currentTarget).addClass('selected');

//             Maxia.Flexplekken.flexplekkenData.HUIDIGERUIMTE(data['RUIMTE_ID']);
//             Maxia.Flexplekken.vulBezettingBlok(Maxia.Flexplekken.flexplekkenData.HUIDIGERUIMTE(), aantalbezet);
//         },
//         reserveerClicked: function(e) {
//             e.preventDefault();
//             let data = $.view(e.currentTarget).data;
//             Polaris.Window.showModuleForm('/modules/_shared/module.plr_maxia_reserveringen/html/flex_reservering_voegtoe.tpl.php'
//             , /* on Accept */ Maxia.Flexplekken.bewaarFlexReservering()
//             , /* on Show   */ function() {
//                 // Self.vulBriefcodes();
//             });
//         },
//         etages: function(item, index, items) {
//             if (index===0 || item.ETAGE!==items[index-1].ETAGE) {
//                 return true;
//             }
//         },
//         flexplekken: function(item, index, items) {
//             return item.ETAGE===this.props.etage;
//         },
//         sortEtages: function(a, b) {
//             // Return 1, -1 or 0 to specify relative position of 'a' and 'b' in the sort order
//             if (a.ETAGE == 'BEG') return 0;
//             if (a.ETAGE == 'ET1') return 1;
//         },
//     },
//     bewaarFlexReservering: function() {
//         log('save');
//     },
//     initialiseerFlexplekken: function() {
//         const Self = Maxia.Flexplekken;

//         /**
//          * Definieer alle modellen die we gebruiken binnen de Flexplek module
//          */
//         this.modelFlexplekken = $.views.viewModels({
//             getters: ["HUIDIGELOCATIE","HUIDIGEDATUM","PRETTY_HUIDIGEDATUM","PRETTY_HUIDIGETIJDPERIODE"
//             ,"HUIDIGETIJDPERIODE","HUIDIGERUIMTE","flexplekken","locaties"],
//         });

//         Maxia.getLocaties(function(locaties, textStatus) {
//             if (textStatus == 'success') {
//                 Self.flexplekkenData = Self.modelFlexplekken(locaties[0]['LOCATIE_CODE'],null,null,null,'heledag',0,[],locaties);
//                 $.templates("#template-flexplekken-view").link("#flexplekken-view", Self.flexplekkenData, Self.renderHelpers);
//             } else {
//                 alert('De locaties konden niet geladen worden. Error: ' + textStatus);
//             }
//         });
//     },
//     initialiseerBezetting: function() {
//         const Self = Maxia.Flexplekken;
//         log('bezetting initialiseren');

//         /**
//          * Definieer alle modellen die we gebruiken binnen de Flexplek module
//          */
//         this.modelBezetting = $.views.viewModels({
//             getters: ["bezetting"], // get/set properties
//         });

//         Self.bezettingData = this.modelBezetting([]);
//         $.templates("#template-bezetting-view").link("#bezetting-view", Self.bezettingData);
//     },
//     initialiseerDatePicker: function() {
//         const Self = Maxia.Flexplekken;

//         /**
//          * Zet de datepicker netjes op het scherm
//          */
//         this.dp = $('#datepicker-maand');
//         this.dp.datepicker({
//             todayBtn: true,
//             language: "nl",
//             daysOfWeekHighlighted: "0,6",
//             calendarWeeks: true,
//             todayHighlight: true,
//             toggleActive: true,
//             format: 'dd-mm-yyyy'
//         })
//         .on('changeDate', function(e) {
//             if (e.format() == '') {
//                 // Voorkom dat gebruiker datum leeg maakt
//                 Self.dp.datepicker( 'setDate', Self.vorigeDatum );
//             }
//             log('onchangedate');
//             Self.flexplekkenData.HUIDIGEDATUM(e.format());
//             Self.flexplekkenData.PRETTY_HUIDIGEDATUM(e.format('d MM yyyy'));
//             Self.refreshFlexPlekkenBlok();
//             Self.vorigeDatum = e.format();
//         });

//         // Zet de datepicker op vandaag
//         var date = new Date();
//         var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
//         this.dp.datepicker( 'setDate', today );
//     },
//     vulBezettingBlok: function(ruimteid, aantalbezet) {
//         const Self = Maxia.Flexplekken;

//         log(ruimteid);
//         // Leeg het bezettingblok
//         Self.bezettingData.bezetting([]);

//         if (aantalbezet > 0) {
//             const locatie = Self.flexplekkenData.HUIDIGELOCATIE();
//             const datum = Self.flexplekkenData.HUIDIGEDATUM();
//             const tijdperiode = Self.flexplekkenData.HUIDIGETIJDPERIODE();

//             console.log(locatie, datum, ruimteid, tijdperiode);
//             Maxia.getFlexReserveringen(locatie, datum, ruimteid, tijdperiode, function(data, textStatus) {
//                 if (textStatus == 'success') {
//                     Self.bezettingData.bezetting(data);
//                 } else {
//                     alert('De flexplekken konden niet geladen worden. Error: ' + textStatus);
//                 }
//             });

//         }
//     },
//     refreshFlexPlekkenBlok: function() {
//         const Self = Maxia.Flexplekken;

//         const locatie = Self.flexplekkenData.HUIDIGELOCATIE();
//         const datum = Self.flexplekkenData.HUIDIGEDATUM();
//         const tijdperiode = Self.flexplekkenData.HUIDIGETIJDPERIODE();

//         Maxia.getFlexPlekken(locatie, datum, tijdperiode, function(data, textStatus) {
//             if (textStatus == 'success') {
//                 let vorigeRuimte = Maxia.Flexplekken.flexplekkenData.HUIDIGERUIMTE();
//                 Self.bezettingData.bezetting([]);

//                 Self.flexplekkenData.flexplekken(data);

//                 $(".flexplek_blok[data-ruimteid="+vorigeRuimte+"]").trigger('click');
//                 Maxia.Flexplekken.flexplekkenData.HUIDIGERUIMTE(vorigeRuimte);
//             } else {
//                 alert('De flexplekken konden niet geladen worden. Error: ' + textStatus);
//             }
//         });
//     }
// });

// jQuery(function () {
//     Maxia.Flexplekken.initialiseer();
// });
