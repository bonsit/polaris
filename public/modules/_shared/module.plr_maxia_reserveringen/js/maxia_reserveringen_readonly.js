Maxia.Reserveringen = {
    calendar: null,
    uitrustingData: null,
    modelUitrusting: null,
    initialiseer: function() {
        const Self = Maxia.Reserveringen;

        Self.initialiseerReserveringKalender();
        Maxia.initialiseerKalenderEvents(Maxia.Reserveringen.calendar);

        // Voeg de groep locatie toe aan de buttons van Fullcalendar
        $("#_fld_GRP_LOCATIE").prependTo(".fc-toolbar-chunk:nth-child(3)");
        Polaris.Base.initializeSelect2($("#reservations_calendar_readonly"));
        $("#filtergroup").hide();

        // Voeg de Maxia naam toe aan de button bar
        $("<div />").addClass("fc-toolbar-title polaris-name").text(brand_name).prependTo(".fc-toolbar-chunk:first");
    },
    initialiseerReserveringKalender: function() {
        const Self = Maxia.Reserveringen;

        if ($('#reservations_calendar_readonly').length == 0) return;

        // Haal opgeslagen weergave en datum op
        var bewaardeView = Maxia.getKalenderView('customDag');
        var bewaardeDate = Maxia.getKalenderDatum(new Date());

        // Haal weergave op uit URL-parameter indien geldig
        var paramView = Polaris.Base.getLocationVariable('view');
        var toegestaneViews = ['customDag', 'customDagLijst', 'customTimeGridWeek', 'customDayGridMonth', 'printList'];
        // Controleer of paramView bestaat en een geldige weergave is
        if (paramView && toegestaneViews.includes(paramView)) {
            bewaardeView = paramView;
        }

        deltaheight = deltaheight || 0;
        Self.calendar = new FullCalendar.Calendar($('#reservations_calendar_readonly')[0], {
            schedulerLicenseKey: '0445008747-fcs-1681420837',
            initialView: bewaardeView,
            initialDate: bewaardeDate,
            navLinks: false,
            height: $(window).height() - deltaheight - 60,
            resourceAreaWidth: '20%',
            locale: 'nl',
            firstDay: 1,
            timeZone: 'Europe/Amsterdam',
            scrollTime: '08:00',
            scrollTimeReset: false,
            headerToolbar: {
                left: 'customDag,customDagLijst,customTimeGridWeek,customDayGridMonth,printList',
                center: 'title',
                right: 'today,prev,next'
            },
            weekNumbers: false,
            businessHours: {
                // days of week. an array of zero-based day of week integers (0=Sunday)
                daysOfWeek: [ 1, 2, 3, 4, 5 ], // Monday - Friday
                startTime: '7:00',
                endTime: '18:00',
            },
            // slotMinTime: '07:00',
            nowIndicator: true,
            editable: false,
            eventStartEditable: false,
            eventResizableFromStart: false,
            selectable: false,
            eventDisplay: 'block',
            // eventDisplay: 'list-item',
            // eventColor: '#378006',
            // eventTextColor: '#000',
            allDayText: 'hele dag',
            listDaySideFormat: false,
            listDayFormat: {
                month: 'long',
                year: 'numeric',
                day: 'numeric',
                weekday: 'long'
            },
            buttonText: {
                'today': 'Vandaag'
            },
            resourceAreaColumns: [
                {
                  field: 'title',
                  headerContent: 'Ruimtes'
                }
            ],
            eventClick: function(info) {
                info.jsEvent.preventDefault();
            },
            resourceOrder: 'title',
            resources: '/services/json/app/facility/form/reserveringen_overzicht/?event=resources',
            events: '/services/json/app/facility/form/reserveringen_overzicht/?event=events',
            views: {
                customDag: {
                    type: 'resourceTimeGridDay',
                    buttonText: 'Dag',
                    titleFormat: {
                        year: 'numeric', month: 'long', day: 'numeric', weekday: 'long'
                    },
                },
                customDagLijst: {
                    type: 'resourceTimeline',
                    buttonText: 'Daglijst',
                    dateAlignment: 'day',
                    // hiddenDays: [0, 6],
                    titleFormat: {
                        year: 'numeric', month: 'long', day: 'numeric', weekday: 'long'
                    },
                },
                customTimeGridWeek: {
                    type: 'timeGridWeek',
                    duration: { days: 7 },
                    buttonText: 'Week',
                    dateAlignment: 'week',
                },
                customDayGridMonth: {
                    type: 'dayGridMonth',
                    buttonText: 'Maand',
                },
                printList: {
                    type: 'listWeek',
                    duration: { days: 6 },
                    buttonText: 'Lijst',
                    dateAlignment: 'week',
                    // hiddenDays: [0, 6],
                }
            },
            viewDidMount: function(cal) {
                Maxia.bewaarKalenderView(cal);
                Maxia.bewaarKalenderDatum(Self.calendar);
            },
            resourceLabelDidMount: function(info) {
                if (info.view.type == 'customDagLijst') {
                    // add info.resource.extendedProps.name and info.resource.extendedProps.description to the cell
                    let container = $('<div />').addClass('roominfo');
                    const res_name = $('<div />').addClass('resource-name').html(info.resource.extendedProps.name);
                    const res_desc = $('<div />').addClass('resource-desc').html(info.resource.extendedProps.description);
                    container.append(res_name).append(res_desc);
                    $('.fc-datagrid-cell-cushion', info.el).append(container);

                } else if (info.view.type == 'customDag') {
                    $('.fc-col-header-cell-cushion,.fc-datagrid-cell-cushion', info.el).popover({
                        placement:'bottom',
                        delay: {
                            'show':500,
                            'hide': 0
                        },
                        trigger:'hover',
                        container:'body',
                        html:true,
                        content:info.resource.extendedProps.description
                    });
                }
            },
            eventDidMount: function(info) {
                // let timestr = info.event.start.getUTCHours()+':' + (info.event.start.getUTCMinutes() < 10 ? '0' : '') + info.event.start.getUTCMinutes() + ' - ' + info.event.end.getUTCHours()+':' + (info.event.end.getUTCMinutes() < 10 ? '0' : '') + info.event.end.getUTCMinutes();
                let startTime = `${info.event.start.getUTCHours()}:${info.event.start.getUTCMinutes().toString().padStart(2, '0')}`;
                let endTime = `${info.event.end.getUTCHours()}:${info.event.end.getUTCMinutes().toString().padStart(2, '0')}`;
                let timestr = `${startTime} - ${endTime}`;
                if (info.view.type == 'printList') {
                    let divcontainer = $('<span />').addClass('containerbox');
                    const spanroomid = $('<span />').addClass('roomid').html(info.event.extendedProps.roomid);
                    const spanroomname = $('<span />').addClass('roomname').html(info.event.extendedProps.roomname);
                    const divperson = $('<span />').addClass('person').html(info.event.extendedProps.description + ' (' + info.event.extendedProps.person + ')');
                    let equips = [];
                    if (info.event.extendedProps.equipment)
                        equips = info.event.extendedProps.equipment.split('§');
                    const divequipment = $('<span />').addClass('equip').html('Voorzieningen:<br/>');
                    $.each(equips, function(index, equip) {
                        let parts = equip.split('_');
                        if (parts[1])
                            equip = equip.replace('_', ' <i class="fa fa-arrow-right"></i> ');
                        else
                            equip = equip.replace('_', '');
                        divequipment.append(equip).append('<br>');
                    });
                    divcontainer.append(spanroomid);
                    divcontainer.append(spanroomname);
                    divcontainer.append(divperson);
                    divcontainer.append(divequipment);
                    $("td.fc-list-event-title a", info.el).html(divcontainer);
                } else {
                    // return;
                    // Add popup to event
                    let divtitle = $('<div />').addClass('title');
                    // let divtime = $('.fc-event-time', info.el);
                    const spanroomid = $('<div />').addClass('roomid').html(info.event.extendedProps.roomid);
                    const spanroomname = $('<span />').addClass('roomname').html(info.event.extendedProps.roomname);
                    const divperson = $('<div />').addClass('person').html(info.event.extendedProps.person);
                    const divdesc = $('<div />').addClass('desc').html(info.event.extendedProps.description);
                    // divtime.append(spanroomid);
                    divtitle.html(spanroomname).append(spanroomid).append(divperson).append(divdesc);
                    $(".fc-event-title", info.el).html(divtitle);

                    let preppedEquipment = '';
                    if (info.event.extendedProps.equipment) {
                        const arrow = ' <i class="fa fa-arrow-right"></i> ';
                        preppedEquipment = info.event.extendedProps.equipment
                        .replace(/_([^§]+)/g, arrow + '$1')
                        .replace(/_/g, '')
                        .replace(/§/g, '<br>');
                    }

                    let cleandescription = timestr
                    + '<br><br>' + '<b>Ruimte ' + info.event.extendedProps.roomid + '</b>'
                    + '<br>' + info.event.extendedProps.roomname
                    + '<br>' + info.event.extendedProps.person
                    + '<br><div class="popover-reserveringcode">' + info.event.extendedProps.reserveringcode + '</div>'

                    + '<br>' + info.event.extendedProps.description
                    + '<br><br>' + preppedEquipment;
                    log(cleandescription);

                    $('.fc-event-main', info.el).popover({
                        placement:'bottom',
                        delay: {
                            'show':500,
                            'hide': 0
                        },
                        trigger:'hover',
                        container:'body',
                        html:true,
                        content:cleandescription
                    });
                }
            },
        });
        Self.calendar.render();
    },
}

jQuery(function () {
    Maxia.Reserveringen.initialiseer();
});