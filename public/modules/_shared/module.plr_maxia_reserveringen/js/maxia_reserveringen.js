Maxia.Reserveringen = {
    calendar: null,
    uitrustingData: null,
    modelUitrusting: null,
    initialiseer: function() {
        const Self = Maxia.Reserveringen;

        Self.initialiseerReserveringKalender();
        Maxia.initialiseerKalenderEvents(Maxia.Reserveringen.calendar);
        Self.initialiseerUitrustingBlok();
    },
    initialiseerUitrustingBlok: function() {
        const Self = Maxia.Reserveringen;

        if ($("#template-uitrusting-view").length > 0) {
            $.views.settings.debugMode(true);

            /**
             * Dit zijn de functies die enkele eigenschappen berekenen in onze template.
             * We geven ze door bij het koppelen van gegevens aan onze template
             * en gebruiken ze met het ~-teken, bijvoorbeeld: {:~getBezetting()}
             */
            var renderRelpers = {
                getBezetting: function(item) {
                    return this.data.VOLLEDIGBEZET == 'Y' ? 'volledigbezet' : 'vrij';
                },
                flexplekClicked: function(item) {
                    if ($(item.target).data('bezetting') != 'volledigbezet') {
                        $(".flexplek_blok").removeClass('selected');
                        $(item.target).addClass('selected');
                        $("#_fldRUIMTE_FLEX").val($(item.target).data('ruimteid'));
                    }
                }
            };

            /**
             * Definieer de modellen die we gebruiken binnen de Uitrusting component
             */
            this.modelUitrustingen = $.views.viewModels({
                getters: ["uitrustingen"],
            });

            /**
             * Zet lege waarden in ons Uitrusting model en koppel dit aan onze template
             */
            this.uitrustingData = this.modelUitrustingen([]);
            $.templates("#template-uitrusting-view").link("#uitrusting-view", this.uitrustingData, renderRelpers);
        }
    },
    initialiseerReserveringKalender: function() {
        const Self = Maxia.Reserveringen;

        if ($('#reservations_calendar').length == 0) return;

        // Haal opgeslagen weergave en datum op
        var bewaardeView = Maxia.getKalenderView('customDag');
        var bewaardeDate = Maxia.getKalenderDatum(new Date());

        // Haal weergave op uit URL-parameter indien geldig
        var paramView = Polaris.Base.getLocationVariable('view');
        var toegestaneViews = ['customDag', 'customDagLijst', 'customTimeGridWeek', 'customDayGridMonth', 'printList'];
        // Controleer of paramView bestaat en een geldige weergave is
        if (paramView && toegestaneViews.includes(paramView)) {
            bewaardeView = paramView;
        }

        deltaheight = deltaheight || 0;
        Self.calendar = new FullCalendar.Calendar($('#reservations_calendar')[0], {
            schedulerLicenseKey: '0445008747-fcs-1681420837',
            initialView: bewaardeView,
            initialDate: bewaardeDate,
            navLinks: true,
            height: $(window).height() - deltaheight - 60,
            resourceAreaWidth: '20%',
            locale: 'nl',
            firstDay: 1,
            timeZone: 'Europe/Amsterdam',
            scrollTime: '08:00',
            scrollTimeReset: false,
            headerToolbar: {
                left: 'voegReserveringToe',
                center: 'title',
                right: 'customDag,customDagLijst,customTimeGridWeek,customDayGridMonth,printList today,prev,next'
            },
            weekNumbers: false,
            businessHours: {
                // days of week. an array of zero-based day of week integers (0=Sunday)
                daysOfWeek: [ 1, 2, 3, 4, 5 ], // Monday - Friday
                startTime: '7:00',
                endTime: '18:00',
            },
            // slotMinTime: '07:00',
            nowIndicator: true,
            editable: false,
            eventStartEditable: false,
            eventResizableFromStart: false,
            selectable: true,
            eventDisplay: 'block',
            // eventDisplay: 'list-item',
            // eventColor: '#378006',
            // eventTextColor: '#000',
            allDayText: 'hele dag',
            listDaySideFormat: false,
            listDayFormat: {
                month: 'long',
                year: 'numeric',
                day: 'numeric',
                weekday: 'long'
            },
            customButtons: {
                voegReserveringToe: {
                  text: 'Voeg een reservering toe',
                  click: function() {
                    // Polaris.Form.showOverlay('http://main.maxia.local:8080/app/facility/basicform/reserveringen_intern_wizard/insert/?_hdnPlrSes=2184a2f92e78ca252b27db34645440a4');
                    window.location.href = '/app/facility/const/reserveringen_intern_rrule/insert/';
                  },
                }
            },
            buttonText: {
                'today': 'Vandaag'
            },
            resourceAreaColumns: [
                {
                  field: 'title',
                  headerContent: 'Ruimtes'
                }
            ],
            resourceGroupField: 'locatie',
            resourceOrder: 'locatie,title',
            resources: '/services/json/app/facility/form/reserveringen_overzicht/?event=resources',
            events: '/services/json/app/facility/form/reserveringen_overzicht/?event=events',
            views: {
                customDag: {
                    type: 'resourceTimeGridDay',
                    buttonText: 'Dag',
                    titleFormat: {
                        year: 'numeric', month: 'long', day: 'numeric', weekday: 'long'
                    },
                },
                customDagLijst: {
                    type: 'resourceTimeline',
                    buttonText: 'Daglijst',
                    dateAlignment: 'day',
                    // hiddenDays: [0, 6],
                    titleFormat: {
                        year: 'numeric', month: 'long', day: 'numeric', weekday: 'long'
                    },
                },
                customTimeGridWeek: {
                    type: 'timeGridWeek',
                    duration: { days: 7 },
                    buttonText: 'Week',
                    dateAlignment: 'week',
                },
                customDayGridMonth: {
                    type: 'dayGridMonth',
                    buttonText: 'Maand',
                },
                printList: {
                    type: 'listWeek',
                    duration: { days: 6 },
                    buttonText: 'Lijst',
                    dateAlignment: 'week',
                    // hiddenDays: [0, 6],
                }
            },
            viewDidMount: function(cal) {
                Maxia.bewaarKalenderView(cal);
                Maxia.bewaarKalenderDatum(Self.calendar);
            },
            select: function(info) {
                // take all start time, end time,  resource id and add to url
                var startParts = info.startStr.split('T');
                var endParts = info.endStr.split('T');
                // convert to date format dd-MM-yyyy
                startParts[0] = startParts[0].split('-').reverse().join('-');
                endParts[0] = endParts[0].split('-').reverse().join('-');
                var options = '?startdatum=' + startParts[0] + '&einddatum=' + endParts[0];
                if (startParts[1])
                    options += '&starttijd=' + startParts[1] + '&eindtijd=' + endParts[1];
                if (info.resource)
                    options += '&ruimte=' + info.resource.id;
                // Polaris.Form.showOverlay('/app/facility/basicform/reserveringen_intern_wizard/insert/' + options);
                window.location.href = '/app/facility/const/reserveringen_intern_rrule/insert/' + options;
                // http://main.maxia.local:8080/app/facility/const/reserveringen_intern_rrule/insert/?startdate=2024-04-16&einddatum=2024-04-16&starttijd=10:00:00&eindtijd=12:00:00
            },
            eventResize: function(info) {
                Self.saveNewEvent(info);
            },
            eventDrop: function(info) {
                Self.saveNewEvent(info);
            },
            resourceLabelDidMount: function(info) {
                if (info.view.type == 'customDagLijst') {
                    // add info.resource.extendedProps.name and info.resource.extendedProps.description to the cell
                    let container = $('<div />').addClass('roominfo');
                    let imagePopup = '';
                    if (info.resource.extendedProps.foto !== '') {
                        imagePopup = '<i data-foto="'+info.resource.extendedProps.foto+'" class="foto-popup material-symbols-outlined">file_map</i> ';
                    }
                    const res_name = $('<div />').addClass('resource-name').html(imagePopup + info.resource.extendedProps.name);
                    const res_desc = '';
                    container.append(res_name).append(res_desc);
                    $('.fc-datagrid-cell-cushion', info.el).append(container);

                    if (info.resource.extendedProps.foto !== '') {
                        $('.foto-popup', info.el).popover({
                            placement:'right',
                            delay: {
                                'show':300,
                                'hide': 0
                            },
                            trigger:'hover',
                            container:'body',
                            html:true,
                            content:'<img src="'+info.resource.extendedProps.foto+'" class="plattegrond-ruimte-foto" />'
                        }).on('click', function(e) {
                            e.preventDefault();
                            e.stopPropagation();
                            window.open($(this).data('foto'), '_blank');
                        });
                    }

                } else if (info.view.type == 'customDag') {
                    let image = '';
                    if (info.resource.extendedProps.foto)  {
                        image = '<br><br><img src="'+info.resource.extendedProps.foto+'" class="roundedx img-thumbnail img-responsive"></img>';
                    }
                    $('.fc-col-header-cell-cushion,.fc-datagrid-cell-cushion', info.el).popover({
                        placement:'bottom',
                        delay: {
                            'show':500,
                            'hide': 0
                        },
                        trigger:'hover',
                        container:'body',
                        html:true,
                        content:'<b><small>Locatie '
                            +info.resource.extendedProps.locatie
                            +'</small></b><br/>'
                            +info.resource.extendedProps.description
                            +image
                    });
                }
            },
            eventDidMount: function(info) {
                let startTime = `${info.event.start.getUTCHours()}:${info.event.start.getUTCMinutes().toString().padStart(2, '0')}`;
                let endTime = `${info.event.end.getUTCHours()}:${info.event.end.getUTCMinutes().toString().padStart(2, '0')}`;
                let timestr = `${startTime} - ${endTime}`;
                if (info.view.type == 'printList') {
                    let divcontainer = $('<span />').addClass('containerbox');
                    const spanroomid = $('<span />').addClass('roomid').html(info.event.extendedProps.roomid);
                    const spanroomname = $('<span />').addClass('roomname').html(info.event.extendedProps.roomname);
                    const divperson = $('<span />').addClass('person').html(info.event.extendedProps.description + ' (' + info.event.extendedProps.person + ')');
                    let equips = [];
                    if (info.event.extendedProps.equipment)
                        equips = info.event.extendedProps.equipment.split('§');
                    const divequipment = $('<span />').addClass('equip').html('Voorzieningen:<br/>');
                    $.each(equips, function(index, equip) {
                        let parts = equip.split('_');
                        if (parts[1])
                            equip = equip.replace('_', ' <i class="fa fa-arrow-right"></i> ');
                        else
                            equip = equip.replace('_', '');
                        divequipment.append(equip).append('<br>');
                    });
                    divcontainer.append(spanroomid);
                    divcontainer.append(spanroomname);
                    divcontainer.append(divperson);
                    divcontainer.append(divequipment);
                    $("td.fc-list-event-title a", info.el).html(divcontainer);
                } else {
                    // return;
                    // Add popup to event
                    let divtitle = $('<div />').addClass('title');
                    // let divtime = $('.fc-event-time', info.el);
                    const spanroomid = $('<div />').addClass('roomid').html(info.event.extendedProps.roomid);
                    const spanroomname = $('<span />').addClass('roomname').html(info.event.extendedProps.roomname);
                    const divperson = $('<div />').addClass('person').html(info.event.extendedProps.person);
                    const divdesc = $('<div />').addClass('desc').html(info.event.extendedProps.description);

                    // divtime.append(spanroomid);
                    divtitle.html(spanroomname).append(spanroomid).append(divperson).append(divdesc);
                    $(".fc-event-title", info.el).html(divtitle);

                    let preppedEquipment = '';
                    if (info.event.extendedProps.equipment) {
                        const arrow = ' <i class="fa fa-arrow-right"></i> ';
                        preppedEquipment = info.event.extendedProps.equipment
                        .replace(/_([^§]+)/g, arrow + '$1')
                        .replace(/_/g, '')
                        .replace(/§/g, '<br>');
                    }

                    let cleandescription = timestr;

                    if (info.event.extendedProps.foto !== '') {
                        cleandescription += '<img src="'+info.event.extendedProps.foto+'" class="rsv-ruimte-foto"/>';
                    }

                    cleandescription += '<br><br>' + '<b>Ruimte ' + info.event.extendedProps.roomid + '</b>'
                    + '<br>' + info.event.extendedProps.roomname
                    + '<br>' + info.event.extendedProps.person
                    + '<br><div class="popover-reserveringcode">' + info.event.extendedProps.reserveringcode + '</div>'

                    + '<br>' + info.event.extendedProps.description
                    + '<br><br>' + preppedEquipment;

                    $('.fc-event-main', info.el).popover({
                        placement:'bottom',
                        delay: {
                            'show':500,
                            'hide': 0
                        },
                        trigger:'hover',
                        container:'body',
                        html:true,
                        content:cleandescription
                    });

                    // Add click function to event
                    $(info.el).on('click', function(e) {
                        e.preventDefault();
                        window.location.href = info.event.url;
                    });

                    // Voeg dropdown toe met extended functies voor het event
                    var dropdowncontainer = '<div class="dropdown dropup btn-event-group"><button type="button" class="btn btn-event dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="">more_horiz</i></button>'
                    + '<ul class="reserveringen-functies-dropdown dropdown-menu dropdown-menu-right" role="menu">'
                    + '<li class="dropdown-item"><a class="event-print" data-toggle="overlay" '
                    + 'data-recordid="' + info.event.extendedProps.recordid + '" '
                    //http://main.maxia.local:8080/showreport/pdf/maxia_reservering_werkbon_rrule/?ROWIDFILTER=%270108b938015f5073083480fb4e59e975%27&CLIENT_HASH=547ce4f12bdd425641109c98e9a89acf
                    + 'href="/app/facility/module/plr_execjasperreport/?event=11ae696ef22aca6fdf78f920447b67c5&jr=maxia_reservering_werkbon_rrule&firsttime=true'
                    // + 'href="/showreport/pdf/maxia_reservering_werkbon_rrule/?'
                    + '"><i class="material-symbols-outlined">print</i> Afdrukken</a></li>'
                    + '<li class="dropdown-item"><a class="event-delete" data-toggle="overlay" href="#"><i class="material-symbols-outlined">delete</i> Verwijderen</a></li>'
                    + '</ul></div>';

                    var $dropdowncontainer = $(dropdowncontainer);
                    var $dropdownbutton = $("button", $dropdowncontainer);

                    $(info.el).append($dropdowncontainer);

                    $dropdowncontainer.on('click', function(e) {
                        e.preventDefault();
                        e.stopPropagation();
                        $dropdownbutton.dropdown('toggle');
                    });

                    $dropdowncontainer.on('click', 'a', function(e) {
                        e.preventDefault();
                        e.stopPropagation();

                        if ($(this).hasClass('event-print')) {
                            Self.reserveringAfdrukken(e.target);
                        } else if ($(this).hasClass('event-delete')) {
                            Self.verwijderEvent(info);
                        }
                    });
                }
            },
        });
        Self.calendar.render();
    },
    bepaalImage: function(fotos) {
        if (fotos) {
            return fotos.split(',')[0];
        }
        return '';
    },
    saveNewEvent: function(info) {
        $form = $("#dataform");

        var parts = info.event.start.toISOString().split('T'); // Split the string at 'T'
        let currentdate = parts[0]; // This will contain the date part
        let currentstarttime = parts[1].slice(0, 5); // This will contain the time part (hh:mm)

        parts = info.event.end.toISOString().split('T'); // Split the string at 'T'
        let currentendtime = parts[1].slice(0, 5); // This will contain the time part (hh:mm)

        let ruimteid = info.event.getResources()[0].id;

        let data = {
              '_hdnAction':'plr_save'
            , '_hdnDatabase': $form.find(":input[name=_hdnDatabase]").val()
            , '_hdnTable': $form.find(":input[name=_hdnTable]").val()
            , '_hdnForm': $form.find(":input[name=_hdnForm]").val()
            , '_hdnState': 'edit'
            , '_hdnRecordID': info.event.extendedProps.recordid
            , 'GEWENSTE_DATUM': currentdate
            , 'GEWENSTE_TIJD_START': currentstarttime
            , 'GEWENSTE_TIJD_EIND': currentendtime
            , 'RUIMTE': ruimteid
        };
        Polaris.Ajax.postJSON(_ajaxquery, data, 'Reservering is aangepast', null, function() {}, info.revert);
    },
    reserveringAfdrukken: function(elem) {
        var url = $(elem).attr('href');
        var recordid = $(elem).data('recordid');
        url += '&ROWIDFILTER=%27' + recordid + '%27';
        Polaris.Form.showOverlay(url);
    },
    verwijderEvent: function(info) {
        const Self = Maxia.Reserveringen;

        var huidigedatum = info.event.start.toISOString().split('T')[0]; // Split the string at 'T'

        var dataConfirm = {
            'event':'bevestig_reservering_verwijderen'
            , '_hdnProcessedByModule':'true'
            , 'PLR__RECORDID': info.event.extendedProps.recordid
            , 'VERWIJDERDATUM': huidigedatum
        };
        Polaris.Window.showModuleForm(_servicequery + '?' + $.param(dataConfirm), function(data) {
            // on Accept
            data = Polaris.Form.convertToKeyValuePairs(data);
            var dataDelete = {
                  'event':'reserveringdatum_verwijderen'
                , '_hdnProcessedByModule':'true'
                , 'PLR__RECORDID': info.event.extendedProps.recordid
            };
            // merge but keep duplicates because of inputs with brackets
            dataDelete = $.extend(dataDelete, data);

            Polaris.Ajax.postJSON(_servicequery, dataDelete, 'Reservering is aangepast', null, function(data) {
                Self.calendar.refetchEvents();
            });
        });
    },
}

jQuery(function () {
    Maxia.Reserveringen.initialiseer();
});