<?php
set_time_limit(300);
require_once('includes/mainactions.inc.php');
require_once('module._base.php');

class ModulePLR_UserSettings extends _BaseModule {

    function Module($moduleid, $module, $clientname=false) {
        parent::__construct($moduleid, $module, $clientname);

        $this->processed = false;
    }

    function DisplayUserSettings() {
        global $polaris;
        global $_CONFIG;

        $this->smarty->assign('recordstate', 'save');
        $usergroup = $polaris->instance->GetRow("SELECT * FROM plr_usergroup WHERE clientid = ? AND usergroupid = ?", array($_SESSION['clientid'], $_SESSION['userid']));
        $this->smarty->assign('usercannotchangepassword', $usergroup['USERCANNOTCHANGEPASSWORD'] == 'Y');
        $this->smarty->assign('usergroup', $usergroup);
        $userpassword = '__notchanged__';
        $this->smarty->assign('userpassword', $userpassword);
        $customsettings = $polaris->instance->GetAssoc("SELECT SETTING, VALUE FROM plr_usersetting WHERE clientid = ? AND applicationid = ? AND usergroupid = ?", array($_SESSION['clientid'], 1, $_SESSION['userid']));
        $this->smarty->assign('positionmenu', $customsettings['NAVIGATIONSTYLE']);
        $this->smarty->assign('2fa', $usergroup['2FA']);
        $this->smarty->assign('phonenumber', $usergroup['PHONENUMBER']);

        return $this->smarty->Fetch('usersettings.tpl.php');
    }

    function ProcessRequest($event=false) {
        if ($_POST['_hdnAction'] == 'save') {
            global $polaris;

            $_result = true;

            if (isset($_SESSION['plrauth']) and $_SESSION['plrauth']) {

                $_userObject = new base_plrUserGroup($polaris);
                $_userObject->LoadRecordByName($_SESSION['name']);

                if (!empty($_POST['USERPASSWORD']) and strtolower($_POST['USERPASSWORD']) !== '__notchanged__') {
                    $_result = $_userObject->ChangePassword($_POST['USERPASSWORD'], $_POST['RETYPEUSERPASSWORD']);
                }

                $_result = $_userObject->updateUser($_POST);
                if ($_result)
                    $polaris->setLanguage($_POST['LANGUAGE']);
            }

            return $_result;
        }
    }

    function ShowListView($state, $permission, $detail=false, $masterrecord=false) {
        if (empty($_POST['_hdnAction'])) {
            echo $this->DisplayUserSettings();
        }
    }

}