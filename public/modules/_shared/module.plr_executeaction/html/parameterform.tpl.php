<form class="form" id="parameterform" {if $target!=''}target="{$target}"{/if}>
    {if $smarty.get.notfirst == ''}<input type="hidden" name="notfirst" value="true" />{/if}
    <input type="hidden" name="activefields" value="{$activefields}" />
    <input type="hidden" name="nullvalues" value="{$nullvalues}" />
    <input type="hidden" name="action" value="{$smarty.get.action}" />
    <input type="hidden" name="module" value="{$smarty.get.module}" />
    <input type="hidden" name="moduleid" value="{$smarty.get.moduleid}" />
    <input type="hidden" name="ROWID" value="{$smarty.get.ROWID}" />
    <div id="dialogtitle"><h2>{$dialogtitle} <p style="float:right;font-weight:normal;font-size:0.3em">{$moduleid}</p></h2></div>

{if $error}
    <input type="hidden" id="_fldActionNotExecuted" name="actionnotexecuted" value="true" />
<p class="error"><i class="fa fa-exclamation-triangle fa-lg"></i> &nbsp; {$error}</p>
{else}
    <div id="dialogtext">{$dialogtext}</div>
    <br />
    {if $param1}<div class="form-group" {if $visible1}style="display:none"{/if}><label class="col-sm-3 col-form-label control-label">{if $label1 != ''}{$label1}:{/if} </label><div class="col-sm-6">{$param1}</div></div>{/if}
    {if $param2}<div class="form-group" {if $visible2}style="display:none"{/if}><label class="col-sm-3 col-form-label control-label">{if $label2 != ''}{$label2}:{/if} </label><div class="col-sm-6">{$param2}</div></div>{/if}
    {if $param3}<div class="form-group" {if $visible3}style="display:none"{/if}><label class="col-sm-3 col-form-label control-label">{if $label3 != ''}{$label3}:{/if} </label><div class="col-sm-6">{$param3}</div></div>{/if}
    {if $param4}<div class="form-group" {if $visible4}style="display:none"{/if}><label class="col-sm-3 col-form-label control-label">{if $label4 != ''}{$label4}:{/if} </label><div class="col-sm-6">{$param4}</div></div>{/if}
    {if $param5}<div class="form-group" {if $visible5}style="display:none"{/if}><label class="col-sm-3 col-form-label control-label">{if $label5 != ''}{$label5}:{/if} </label><div class="col-sm-6">{$param5}</div></div>{/if}
    {if $param6}<div class="form-group" {if $visible6}style="display:none"{/if}><label class="col-sm-3 col-form-label control-label">{if $label6 != ''}{$label6}:{/if} </label><div class="col-sm-6">{$param6}</div></div>{/if}

    <div id="printer_selection" style="display:none">
    <h3>Kies de gewenste printers.</h3>
    {if $printerselectie and $defaultprinter}
    <hr />
    Geselecteerde printer: &nbsp; <select name="SELECTEDPRINTER">{html_options options=$printerselectie selected=$defaultprinter}</select><br />
    {/if}

    {if $defaultprinter2}
    Geselecteerde printer 2: <select name="SELECTEDPRINTER2">{html_options options=$printerselectie selected=$defaultprinter2}</select><br />
    {/if}
    {if $defaultprinter3}
    Geselecteerde printer 3: <select name="SELECTEDPRINTER3">{html_options options=$printerselectie selected=$defaultprinter3}</select><br />
    {/if}
    {if $defaultprinter4}
    Geselecteerde printer 4: <select name="SELECTEDPRINTER4">{html_options options=$printerselectie selected=$defaultprinter4}</select><br />
    {/if}
    {if $defaultprinter5}
    Geselecteerde printer 5: <select name="SELECTEDPRINTER5">{html_options options=$printerselectie selected=$defaultprinter5}</select><br />
    {/if}
    {if $defaultprinter6}
    Geselecteerde printer 6: <select name="SELECTEDPRINTER6">{html_options options=$printerselectie selected=$defaultprinter6}</select><br />
    {/if}
    {if $defaultprinter7}
    Geselecteerde printer 7: <select name="SELECTEDPRINTER7">{html_options options=$printerselectie selected=$defaultprinter7}</select><br />
    {/if}
    </div>
{/if}
    <br />
    <hr class="fix" /><br />
    <div class="buttons">
    <button type="button" class="btn btn-primary positive savebutton {$okbuttondisabled}" value="yes">{lang but_ok}</button>
    <button type="button" class="btn btn-default negative cancelbutton" value="no">{lang but_cancel}</button>
    {if $printerselectie}
    <button type="button" onclick="javascript:$('#printer_selection').toggle();">{lang printersettings}</button>
    <input type="checkbox" name="DEBUG" value="DEBUG" {$debugchecked} /> Debug<br />
    {/if}
    </div>
</form>
{literal}
<script type="text/javascript">
    Polaris.Form.initializeDatePickers();
    Polaris.Form.initializeFormValidation($("#parameterform"));
    $("#parameterform .select2").select2();
    $("#parameterform .validation_float").autoNumeric({aSep:'.',aDec:','});

    $("#parameterform").submit(function(e) {
        e.preventDefault();
    });

    $("#parameterform").keydown(function (event){
        if (event.keyCode == 27) /* Escape */ {
            event.preventDefault();
            $("#parameterform .cancelbutton").click();
            $(document.activeElement).blur();
        }
    });
    $("#parameterform .savebutton").keydown(function (event) {
        if (event.keyCode == 13) /* Enter */ {
            event.preventDefault();
            $("#parameterform .savebutton").click();
        }
    });

    Polaris.Form.adjustLabelWidth($("#parameterform"));
</script>
{/literal}