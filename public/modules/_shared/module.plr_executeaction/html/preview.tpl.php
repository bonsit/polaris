<div id="dialogtitle"><h1>{$dialogtitle}</h1></div>

{if $error}
    <p class="error"><i class="fa fa-exclamation-triangle fa-lg"></i> &nbsp; {$error}</p>
{else}
    <p>
        {lang from}: <b>{$fromaddress}</b><br/>
        {lang to}: <b>{$toaddress|default:'onbekend'}</b><br/>
        {lang subject}: <b>{$subject}</b>
    </p>
    <p>{lang attachments}: {$attachments|default:'geen'}<p>
    <div id="dialogtext" class="bodyscroll scrollarea" style="max-height:300px;"><pre style="white-space: pre-wrap;">{$bodytext}</pre></div>
    <br />
    <hr class="fix" /><br />
    <div class="buttons">
    <button type="button" class="positive closebutton jqmClose" value="close">{lang but_close}</button>
    </div>
{/if}
