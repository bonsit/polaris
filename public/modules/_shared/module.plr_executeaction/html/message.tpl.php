{if $error or $msg != ''}
{literal}<style>
#paramAction { font-weight:bold;}
#paramError { font-size:1.2em; }
</style>{/literal}
</div>
<h2>{$msg}</h2>

{if $error}
<input type="hidden" id="_fldActionNotExecuted" name="actionnotexecuted" value="true" />
<p id="paramError"><i class="fa fa-warning text-warning"></i> {$error}</p>
<button type="button" onclick="javascript:$('#error').toggle();">Details</button>
<div id="error" style="display:none">
    <p id="paramAction">Statement:
        <pre>{$sql}</pre>
    </p>
</div>
{/if}
{else}
    {if $debug !== 'DEBUG'}
    <input type="hidden" id="_fldDONTSHOWMESSAGE" value="true" />
    {/if}
{/if}
<hr class="fix" /><br />
<div class="buttons">
    <button type="button" autofocus class="btn btn-primary savebutton jqmClose">{lang but_close}</button>
</div>

