<html>
<script type="text/javascript" src="{$serverpath}/assets/js/jquery/dist/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" media='all' href="{$serverpath}/assets/css/fontawesome-pro/css/all.min.css"/>
<style>{literal}
    body{background-color:#333;margin:1em;font-family:sans-serif;color:#fff;font-size:0.8em;}
    #printblock {margin-bottom:1em;}
    #feedback {position:absolute;top:40px;left:500px;text-align:center;line-height:2em;}
    button {font-size:1.1em;}
    iframe {border:1px solid #777;}
    @media print {
        @page {
            size: A4 landscape;
            margin: 0cm !important;
        }
    }
{/literal}</style>
<body>
<div id="printblock">
    <h2>{$dialogtitle}</h2>
    <p>{$dialogtext}</p>
    <div id="feedback"><i class="fa fa-spinner fa-spin fa-2x"></i><br />Even geduld a.u.b...</div>
    <div class="row">
        <div class="col-sm-6">
            <button id="printreport" disabled="disabled">Afdrukken en verwerken</button> &nbsp;
        </div>
        <div class="col-sm-6 pull-right">
            <input type="checkbox" name="debugmode" id="debugmode" value="J" /> Debugmode?
        </div>
    </div>
</div>
<iframe id="reportframe" name="reportframe" src="{$reporturl}" style="top:0px; left:0px; bottom:0px; right:0px; width:40%; height:100%; border:none; margin:0; padding:0; overflow:hidden;"></iframe>
<script>{literal}
var doAfterEvent = function() {
    if (!$("#debugmode").attr('checked')) {
        $.ajax({ url: "{/literal}{$afterprintevent}{literal}", success: function(data, textStatus){
            if (textStatus == 'success') {
                $("#feedback").html("Rapport verwerkt.");
            }
            // This command closed the wrong windows on Windows
            // Search for a better way to close the window
            // window.close();
        }});
    }
}

jQuery(function () {
    $("#printreport").click(function() {
        window.frames["reportframe"].focus();
        window.frames["reportframe"].print();

        {/literal}{if $hasafterprintevent}
        doAfterEvent();
        {/if}{literal}
    });

    $("#reportframe").on("load", function () {
        $("#printreport").removeAttr('disabled');
        $("#feedback").html("");
    });
});

{/literal}</script>
</body>
</html>