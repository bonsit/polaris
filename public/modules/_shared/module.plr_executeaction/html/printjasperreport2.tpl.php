<style>{literal}
    /* body{background-color:#333;margin:1em;font-family:sans-serif;color:#fff;font-size:0.8em;} */
    #printblock {margin-bottom:1em;}
    button {font-size:1.1em;}
    iframe {border:1px solid #777;}
    @media print {
        @page {
            size: A4 landscape;
            margin: 0cm !important;
        }
    }
{/literal}</style>

<div id="printblock">
    <div class="row">
        <div class="col-sm-12">
            <h2>{$dialogtitle}</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-10">
            <p>{$dialogtext}</p>
        </div>
        <div class="col-sm-2">
            <div id="feedback" class=""></div>
        </div>
    </div>

    <div class="row no-gutter">
        <div class="col-sm-10">
            <button id="printreport" class="btn btn-primary" disabled="disabled"><i style="color:#fff" class="fa fa-spinner fa-spin fa-2x"></i> &nbsp; Even geduld a.u.b...</button> &nbsp;
        </div>
        <div class="col-sm-2 pull-right">
            <input type="checkbox" name="debugmode" id="debugmode" value="J" /> Debugmode?
        </div>
    </div>
</div>
<iframe id="reportframe" name="reportframe" src="{$reporturl}" style="top:0px; left:0px; bottom:0px; right:0px; width:100%; height:80%; border:none; margin:0; padding:0; overflow:hidden;"></iframe>
<script>{literal}
var doAfterEvent = function() {
    // Give the document focus
    window.focus();

    // Remove focus from any focused element
    if (document.activeElement) {
        document.activeElement.blur();
    }

    if (!$("#debugmode").attr('checked')) {
        $.ajax({ url: "{/literal}{$afterprintevent}{literal}", success: function(data, textStatus){
            if (textStatus == 'success') {
                $("#feedback").html("Rapport verwerkt.");
            }
            // This command closed the wrong windows on Windows
            // Search for a better way to close the window
            // window.close();
        }});
    }
}

jQuery(function () {
    $("#printreport").click(function() {
        window.frames["reportframe"].focus();
        window.frames["reportframe"].print();

        {/literal}{if $hasafterprintevent}
        doAfterEvent();
        {/if}{literal}
    });

    $("#reportframe").on("load", function () {
        $("#printreport").removeAttr('disabled').html("Afdrukken en verwerken");
        $("#feedback").html("");
    });
});

{/literal}</script>
