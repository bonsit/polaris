<div class="form-details-panel">
    <div class="media-details">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="form-details-content">

                    <div class="row">
                        <div class="col-lg-11 col-md-11 col-sm-12 no-padding">
                            <div class="media-details-title">Media details</div>
                        </div>
                        <div class="col-lg-1 col-md-1 col-sm-12" style="vertical-align:bottom">
                            <div class="dropdown" xstyle="display:inline">
                                <a href="#" class="btn btn-outline dropdown-toggle" data-toggle="dropdown" id="media-dropdown" type="button" title="Info" >{icon name="md-more_horiz" size="2x"}</a>
                                <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                    <li><a class="media-url" data-mediasize="thumbnail" target="_blank" title="Open thumbnail">{icon name="md-open_in_new"} Open thumbnail (200)</a></li>
                                    <li><a class="media-url" data-mediasize="public" target="_blank" title="Open medium">{icon name="md-open_in_new"} Open medium (1024)</a></li>
                                    <li><a class="media-url" data-mediasize="large" target="_blank" title="Open large">{icon name="md-open_in_new"} Open large (2800)</a></li>
                                    <li class="divider"></li>
                                    <li id="media-delete" class="text-danger"><a title="Verwijder">{icon name="md-delete"} {lang delete}</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div id="media-preview" class="media-preview"></div>

                    <p>ID: <span id="media-id" class="copy-clipboard"></span></p>
                    <p>Bestandsnaam: <span id="media-naam" class="copy-clipboard"></span> </p>
                    <p>Omschrijving: <span id="media-omschrijving" class="copy-clipboard"></span></p>
                    <p>Tags: <span id="media-tags"></span></p>
                    <h5>URL's</h5>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="form-group">
                                <p class="col-lg-3">Thumbnail</p>
                                <div class="col-lg-9">
                                <input type="text" readonly style="width:100%;" data-mediasize="thumbnail" class="media-url copy-clipboard" />
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="form-group">
                                <p class="col-lg-3">Medium</p>
                                <div class="col-lg-9">
                                <input type="text" readonly style="width:100%;" data-mediasize="public" class="media-url copy-clipboard" />
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="form-group">
                                <p class="col-lg-3">Large</p>
                                <div class="col-lg-9">
                                <input type="text" readonly style="width:100%;" data-mediasize="large" class="media-url copy-clipboard" />
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="dropzone" class="dropzone">
            <input type="hidden" id="_hdnMEDIAFILES" name="MEDIAFILES">
    </div>
</div>