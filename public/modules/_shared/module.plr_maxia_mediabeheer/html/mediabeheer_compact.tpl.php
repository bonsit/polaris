<script type="text/javascript" src="{$serverpath}{$modulepath}/../../module.plr_maxia/js/maxia.js?{#BuildVersion#}"></script>
<script type="text/javascript" src="{$serverpath}{$modulepath}/js/main.js?{#BuildVersion#}"></script>
<script type="text/javascript" src="{$serverpath}/assets/dist/dropzone-min.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="{$serverpath}/assets/dist/dropzone.css" />

{literal}<script id="template-mediabeheer-view" type="text/x-jsrender">
    {{if root() !== true}}
        <div class="" data-link="
        {on 'click' 'img' ~select}
        {on 'dblclick' 'img' ~open}"
        >
            <a href="#{{:parentfolder()}}">
                <div class="media-image">
                    <img src="/assets/images/folder-icon-back.png" data-ori="/assets/images/folder-icon.png" class="img-responsive"/>
                </div>
            </a>
        </div>
    {{/if}}

    {^{for files()}}
        <div id="{{:PLR__RECORDID}}" class="mediabeheer-view-compact-item sortable-draggable {{if BESTANDSTYPE == 'FOLDER'}}filtered{{/if}}"
        data-link="
        {on 'click' 'img' ~select}
        {on 'dblclick' 'img' ~open}"
        >
            <a href="#{{:FOLDER_HASH}}">
                <div class="media-image">
                {{if BESTANDSTYPE == 'FOLDER'}}
                    <img src="/assets/images/folder-icon.png" data-ori="/assets/images/folder-icon.png" data-bestandsnaam="{{:BESTANDSNAAM}}" data-bestandstype="{{:BESTANDSTYPE}}"
                    data-id="{{:PLR__RECORDID}}" data-cloudid="{{:PLR__RECORDID}}" data-naam="{{:NAAM}}" data-folderid="{{:FOLDER_HASH}}" data-omschrijving="{{:OMSCHRIJVING}}" data-tags="{{:TAGS}}" class="img-responsive"
                    />
                {{else}}
                    <img src="{{:IMAGEURL_THUMBNAIL}}" data-bestandstype="{{:BESTANDSTYPE}}" data-ori="{{:IMAGEURL_PUBLIC}}" data-bestandsnaam="{{:BESTANDSNAAM}}"
                    data-id="{{:PLR__RECORDID}}" data-cloudid="{{:CLOUD_IMAGE_ID}}" data-naam="{{:NAAM}}" data-folderid="{{:FOLDER_HASH}}" data-omschrijving="{{:OMSCHRIJVING}}" data-tags="{{:TAGS}}" class="rounded sortable-handle img-thumbnail img-responsive"
                    />
                {{/if}}
                <p class="bestandsnaam">{{:BESTANDSNAAM_KORT}}</p>
                </div>
            </a>
        </div>
    {{/for}}
</script>{/literal}

<div class="media-grid2">

    <button class="btn btn-default pull-right" id="btn-upload">Upload...</button>

    <h3 class="m-b-md">Sleep de gewenste media naar het item</h3>
    <div id="mediabeheer-view" class="shared-media mediabeheer-compact-view sortable-draggablex sortable-mediax"></div>

    <div class="media-upload">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="media-upload-content">
                    <div id="dropzone" class="dropzone dropzone-previews">
                        <input type="hidden" id="_hdnMEDIAFILES" name="MEDIAFILES">
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
