<link rel="stylesheet" type="text/css" media='all' href="{$moduleroot}css/default.css" />
<script type="text/javascript" src="/assets/dist/Sortable.min.js"></script>
<script type="text/javascript" src="/assets/dist/dropzone-min.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="/assets/dist/dropzone.css" />

{literal}<script id="template-mediabeheer-view" type="text/x-jsrender">
    <div class="row list-group" data-link="
        {on 'click' 'img' ~select}
        {on 'dblclick' 'img' ~open}
    ">

    {{if root() !== true}}
        <div class="col-lg-2 col-md-2 col-sm-4">
            <a href="#{{:parentfolder()}}">
                <div class="media-image">
                    <img src="/assets/images/folder-icon-back.png" data-ori="/assets/images/folder-icon.png" class="img-responsive"/>
                </div>
            </a>
        </div>
    {{/if}}

    {^{for files()}}
        <div class="col-lg-2 col-md-2 col-sm-4 xlist-group-item">
            <a href="#{{:FOLDER_HASH}}">
                <div class="media-image">
                {{if BESTANDSTYPE == 'FOLDER'}}
                    <img src="/assets/images/folder-icon.png" data-ori-public="/assets/images/folder-icon.png" data-bestandsnaam="{{:BESTANDSNAAM}}" data-bestandstype="{{:BESTANDSTYPE}}"
                    data-id="{{:PLR__RECORDID}}" data-cloudid="{{:PLR__RECORDID}}" data-naam="{{:NAAM}}" data-folderid="{{:FOLDER_HASH}}" data-omschrijving="{{:OMSCHRIJVING}}" data-tags="{{:TAGS}}" class="img-responsive"
                    />
                {{else}}
                    <img src="{{:IMAGEURL_THUMBNAIL}}" data-bestandstype="{{:BESTANDSTYPE}}" data-ori-thumbnail="{{:IMAGEURL_THUMBNAIL}}" data-ori-large="{{:IMAGEURL_LARGE}}" data-ori-public="{{:IMAGEURL_PUBLIC}}" data-bestandsnaam="{{:BESTANDSNAAM}}"
                    data-id="{{:PLR__RECORDID}}" data-cloudid="{{:CLOUD_IMAGE_ID}}" data-naam="{{:NAAM}}" data-folderid="{{:FOLDER_HASH}}" data-omschrijving="{{:OMSCHRIJVING}}" data-tags="{{:TAGS}}" class="rounded img-thumbnail img-responsive"
                    />
                {{/if}}
                <p class="bestandsnaam">{{:BESTANDSNAAM_KORT}}</p>
                </div>
            </a>
        </div>
    {{/for}}
    </div>
</script>{/literal}

<div class="form-main-content">
    <div class="element-detail-box">
        <div class="media-buttonbar">
            <button class="btn btn-default" id="btn-nieuwe-folder">Nieuwe folder...</button>
            <button class="btn btn-default" id="btn-upload">Upload...</button>
            {if $permission == '286'}
            <div class="dropdown" style="display:inline">
                <a href="#" class="btn btn-outline dropdown-toggle" data-toggle="dropdown" id="media-advanced" type="button" title="Geavanceerd" >{icon name="menu"}</a>
                <ul class="dropdown-menu dropdown-menu-right" role="menu">
                    <li id="btn-sync-cloud-images"><a tabindex="-1" href="#">{icon name="md-sync"} Sync cloud images</a></li>
                </ul>
            </div>
            {/if}
        </div>

        <div class="media-grid">
            <div id="mediabeheer-view" class="mediabeheer-view"></div>
        </div>
    </div>

    {include file="sidepanel.tpl.php"}
</div>
<div class="dropzone-previews dropzone"></div>

{include file="modalforms.tpl.php"}