</form>
<div class="modal inmodal" id="dlgMediaVerwijder" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content x_animated fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only">Sluiten</span></button>
                <h5 class="modal-title">Media verwijderen</h5>
            </div>
            <form class="form-horizontal" id="frmMediaVerwijder">
                <input type="hidden" id="_hdnRecordID" value="" />
                <input type="hidden" value="N" id="_hdnPermanent" name="PERMANENT">

                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-lg-3 control-label">Bestandsnaam</label>
                        <div class="col-lg-9 control-label">
                            <span id="_txtBestandsnaam">aaa</span>
                        </div>
                    </div>

                    <div class="form-group is-folder">
                        <h3>
                            <i class="fa fa-warning fa-2x text-warning"></i>
                            Deze actie verwijdert ook alle media in de (sub)folder
                        </h3>
                    </div>

                </div>

                <div class="modal-footer">
                    {if $permission == '286'}
                    <button type="button" class="btn btn-danger pull-left enlarge" id="btnMediaDeletePermanentBevestig" title="Deze actie verwijderd de media van de cloud opslag."><span>Verwijder permanent</span></button>
                    {/if}
                    <button type="button" class="btn btn-white" data-dismiss="modal">{lang but_cancel}</button>
                    <button type="button" class="btn btn-primary btnSave" id="btnMediaDeleteBevestig">{lang but_delete}</button>
                </div>
            </form>
        </div>
    </div>
</div>
