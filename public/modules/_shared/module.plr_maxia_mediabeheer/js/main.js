Maxia.MediaBeheer = {};
Base.update(Maxia.MediaBeheer, {
    modelMediaBeheer: null,
    mediaBeheerData: null,
    currentfolder: null,
    parentfolder: null,
    dropzone: null,
    initialiseer: function() {
        var Self = Maxia.MediaBeheer;

        Self.initialiseerMedia();

        Self.dropzoneconfig = {
            url: "/app/facility/const/mediabeheer/",
            uploadMultiple: true,
            createImageThumbnails: true,
            thumbnailMethod: 'contain',
            resizeMethod: 'contain',
            maxFiles: 10,
            acceptedFiles: '.jpg,.png,.jpeg,.svg',
            autoProcessQueue: true,
            paramName: "MEDIAFILES", // The name that will be used to transfer the file
            maxFilesize: 10, // MB
            previewsContainer: ".dropzone-previews",
            // You probably don't want the whole body
            // to be clickable to select files
            clickable: true,
            dictDefaultMessage: "",
            init: function () {
                this.on("complete", function(file) {
                    this.removeFile(file);
                    Self.refreshMedia();
                });
                this.on("dragstart", function(event) {
                    const dt = event.dataTransfer;
                    dt.effectAllowed = 'none';
                    if ( dt.files.length == 0 ) {
                        return false;
                    }
                });
                this.on("sendingmultiple", function (file, xhr, formData) {
                    formData.append('CURRENTFOLDER', Self.currentfolder);
                });
            }
        },
        Self.dropzone = new Dropzone($("#dropzone")[0], Self.dropzoneconfig);
    },
    refreshMedia: function(parentfolder) {
        var Self = Maxia.MediaBeheer;
        // var huidigeFolder = window.location.hash.substring(1);

        parentfolder = parentfolder || window.location.hash.substring(1);
        Self.getMedia(parentfolder, function(mediafiles, textStatus) {
            if (textStatus == 'success') {
                Self.currentfolder = mediafiles.currentfolder;
                Self.mediaBeheerData = Self.modelMediaBeheer(mediafiles.root, mediafiles.currentfolder, mediafiles.currentfoldername, mediafiles.parentfolder, mediafiles.files);
                $.templates("#template-mediabeheer-view").link("#mediabeheer-view", Self.mediaBeheerData);
                $("#recordIndicator").html(' &nbsp;/&nbsp; ' + mediafiles.currentfoldername);

                // Refresh the drag and drop functionality
                // new Sortable(document.getElementById('mediabeheer-view'), {

                // });
                // $("#mediabeheer-view").sortable( "refresh");
                Polaris.Dataview.initializeSortableMedia('mediabeheer-view', {sort: false, grouppull: false});
            } else {
                alert('De media konden niet geladen worden. Error: ' + textStatus);
            }
        });
    },
    getMedia: function(parentfolder, callback) {
        return $.getJSON('/services/json/app/facility/const/mediabeheer/', {
            'func': 'mediafiles',
            'folder': parentfolder,
            'q': Maxia.getGetVar('q[]')
        }, callback);
    },
    renderHelpers: {
        /**
         * Dit zijn de functies die enkele eigenschappen berekenen in onze template.
         * We geven ze door bij het koppelen van gegevens aan onze template
         * en gebruiken ze met het ~-teken, bijvoorbeeld: {:~getBezetting()}
         */
        select: function(ev, eventArgs) {
            var elem = $(ev.target);
            $(".media-grid img").removeClass("selected");
            elem.toggleClass('selected');
            $(".form-details-content").toggle(elem.hasClass('selected'));

            $("#media-id").html(elem.data("cloudid"));
            $("#media-naam").html(elem.data("bestandsnaam"));
            $("#media-omschrijving").text(elem.data("omschrijving"));
            $("#media-tags").text(elem.data("tags"));
            $("#media-preview").find('img').remove();
            $("#media-preview").append('<img src="' + elem.data("ori-public") + '" alt="' + elem.data("bestandsnaam") + '">');

            // Cache elements for better performance
            const $mediaUrl = $(".media-url");
            $mediaUrl.each(function(i, med) {
                const $med = $(med);
                const size = $med.data("mediasize");
                // if $med is an anchor, set the href attribute, else set the value attribute
                if ($med.is('a')) {
                    $med.attr('href', elem.data("ori-" + size));
                } else {
                    $med.val(elem.data("ori-" + size)).on('click', selectInputText);
                }
            });

            // Function to select text in input fields
            function selectInputText(e) {
                $(this).trigger('select');
            }
        },
        open: function(ev, eventArgs) {
            const Self = Maxia.MediaBeheer;

            var elem = $(ev.target);
            Self.mediaBeheerData.parentfolder = Self.currentfolder;
            Maxia.MediaBeheer.refreshMedia(elem.data('folderid'));
        }
    },
    bevestigVerwijderMedia: function(e) {
        e.preventDefault();
        var selected = $(".media-grid img.selected");

        $("#_hdnRecordID").val(selected.data('id'));
        $("#permanentJA").iCheck('uncheck');
        $("#dlgMediaVerwijder .modal-title").text(selected.data('bestandstype') == 'FOLDER' ? 'Folder verwijderen': 'Media verwijderen');
        $("#dlgMediaVerwijder .is-folder").toggle(selected.data('bestandstype') == 'FOLDER');
        $("#dlgMediaVerwijder #_txtBestandsnaam").text(selected.data('bestandsnaam'));

        $("#dlgMediaVerwijder").modal('show');
    },
    verwijderMedia: function() {
        const Self = Maxia.MediaBeheer;

        let $baseform = $("#dataform");
        let $form = $("#frmMediaVerwijder");
        let data = {
            '_hdnAction':'verwijdermediafile'
            , '_hdnProcessedByModule':'true'
            , '_hdnDatabase': $baseform.find(":input[name=_hdnDatabase]").val()
            , '_hdnTable': $baseform.find(":input[name=_hdnTable]").val()
            , '_hdnForm': $baseform.find(":input[name=_hdnForm]").val()
            , '_hdnRecordID': $("#_hdnRecordID").val()
            , 'PERMANENT': $form.find(":input[name=PERMANENT]").val() == 'Y'
        };
        $("#btnMediaDeleteBevestig").prop( "disabled", true );

        Polaris.Ajax.postJSON(_ajaxquery, data, 'Bestand is verwijderd', null, function() {
            $("#dlgMediaVerwijder").modal('hide');
            $(".form-details-content").hide();
            $("#_hdnPermanent").val('N');
            $("#btnMediaDeleteBevestig").prop( "disabled", false );
            Self.refreshMedia();
        });
    },
    syncCloudImages: function() {
        const Self = Maxia.MediaBeheer;

        var $form = $("#dataform");
        var data = {
            '_hdnAction':'synccloudimages'
            , '_hdnProcessedByModule':'true'
            , '_hdnDatabase': $form.find(":input[name=_hdnDatabase]").val()
            , '_hdnForm': $form.find(":input[name=_hdnForm]").val()
        };
        Polaris.Ajax.postJSON(_ajaxquery, data, 'Afbeeldingen zijn gesynchroniseerd', null, function() {
            Self.refreshMedia();
        });
    },
    nieuweFolder: function() {
        const Self = Maxia.MediaBeheer;

        var folder = prompt('Geef de naam van de nieuwe folder op');
        if (folder) {
            var $form = $("#dataform");
            var data = {
                '_hdnAction':'nieuwefolder'
                , '_hdnProcessedByModule':'true'
                , '_hdnDatabase': $form.find(":input[name=_hdnDatabase]").val()
                , '_hdnTable': $form.find(":input[name=_hdnTable]").val()
                , '_hdnForm': $form.find(":input[name=_hdnForm]").val()
                , 'foldernaam': folder
                , 'parent_folder_id': Self.currentfolder
            };
            Polaris.Ajax.postJSON(_ajaxquery, data, 'Folder is aangemaakt', null, function() {
                Self.refreshMedia();
            });
        }
    },
    initialiseerMedia: function() {
        const Self = Maxia.MediaBeheer;

        this.modelMediaBeheer = $.views.viewModels({
            getters: ['root', 'currentfolder', 'currentfoldername', 'parentfolder', 'files'],
        });
        $.views.helpers(Self.renderHelpers)

        Self.refreshMedia();

        $(".form-details-content").hide();

        $("#btn-upload").on('click', function(e) {
            e.preventDefault();
            Self.dropzone.hiddenFileInput.click();
        });

        $("#btn-nieuwe-folder").on('click', function(e) {
            e.preventDefault();
            Self.nieuweFolder();
        });

        $("#media-delete").on('click', Self.bevestigVerwijderMedia);
        $("#btnMediaDeleteBevestig").on('click', Self.verwijderMedia);
        $("#btnMediaDeletePermanentBevestig").on('click', function() {
            $("#_hdnPermanent").val('Y');
            Self.verwijderMedia();
        });

        $("#btn-sync-cloud-images").on('click', Self.syncCloudImages);
    },
});

jQuery(function () {
    Maxia.MediaBeheer.initialiseer();
});
