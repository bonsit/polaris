<?php
require_once('module._base.php');

class Module extends _BaseModule {
    var $stylesheet;

    function Module($moduleid, $module, $clientname=false) {
        parent::_BaseModule($moduleid, $module, $clientname);

        $this->processed = false;
    }

    function Process() {
    }

    function ShowDashboard() {
        echo $this->smarty->Fetch("dashboard.tpl.php");
    }

    function Show($outputformat='xhtml', $rec=false) {
        global $_GVARS;

        $this->ShowDashboard();
    }

}
