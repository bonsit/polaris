<?php
require_once('module.plr_executeaction.php');

function Wrap($text, $wrapit) {
    if ($wrapit)
        $text = "'$text'";
    return $text;
}

class ModulePLR_ExecJasperReport extends ModulePLR_ExecuteAction {

    function __construct($moduleid, $module) {
        global $_GVARS;

        parent::__construct($moduleid, $module);

        $this->processed = false;
        /**
        * What client are we talking about? (nog doen: eventueel selecteerbaar maken)
        */
        $this->clientid = $_SESSION['clientid'];
    }

    function DisplayParameterForm() {
        global $polaris;

        // If the moduleid is present then we have a BEFOREPRINT action to process
        $_beforePrint = $this->currentaction->record->BEFOREPRINT;
        if (!empty($_beforePrint)) {
            $_actionid = $_GET['action'];
            $_client = new base_plrClient($polaris, $this->clientid);
            // Pick up the module that can be used for processing actions
            // The module can be found in the Module column
            $_actionmodule = $_client->GetModule($this->currentaction->record->MODULE, $_beforePrint, $_GET);
            $_result = $_actionmodule->ProcessAction($_actionid, $_beforePrint, $_GET);
        } else {
            $_result = true;
        }

        if ($_result === true) {
            // Instead of displaying the standard Parameter form, display the report preview form
            $this->DisplayReportPreview($_GET['jr']);
        } else {
            $this->DisplayMessage($_result);
        }
    }

    function DisplayReportPreview($reportname) {
        global $_CONFIG;
        global $_GVARS;
        global $polaris;

        $currentlinerecord = arrayToObject($_GET);

        $reporturl = str_replace("%REPORTNAME%",$reportname,$_CONFIG["polarisreporturl"]);
        $reporturl = str_replace("%REPORTPARAMS%",'',$reporturl);
        // $reporturl = $polaris->ReplaceDynamicVariables($reporturl);
        $params = $_GET;
        unset($params['app']);
        unset($params['module']);
        unset($params['moduleid']);
        unset($params['action']);
        unset($params['jr']);
        unset($params['firsttime']);
        unset($params['%field']); // FIX THIS PROPERLY
        $params['CLIENT_HASH'] = $_SESSION['clienthash'];
        $_restparams = http_build_query($params);
        $reporturl .= '&'.$_restparams;
        if ($_GET['ROWIDFILTER'] != '') {
            // $wherefilter = rawurlencode("AND ROWIDTOCHAR(TOTALENPICKLIJST.ROWID) = {$_GET['ROWIDFILTER']}");
            // $reporturl .= "&WHEREFILTER=$wherefilter";
        } else {
            $parts = $this->GetParts($this->currentaction->record->PARAM1);
            if ($parts[0] == 'WHEREFILTER') {
                if ($parts[2] == 'ISNULL')
                    $operator = "IS NULL";
                elseif ($parts[2] == 'ISNOTNULL')
                    $operator = "IS NOT NULL";

                $wherefilter = rawurlencode("AND {$parts[1]} $operator");
                $reporturl .= "&WHEREFILTER=$wherefilter";
            }
        }
        $this->actionsmarty->assign('polarispath', $_GVARS['serverpath']);
        $this->actionsmarty->assign('reporturl', $reporturl);
        $basehref = $_GVARS['serverroot']."/app/facility/module/".strtolower($this->currentaction->record->MODULE)."/";
        $afterprintevent = $basehref.'?action='.$this->currentaction->record->RECORDID.'&moduleid='
          .$this->currentaction->record->AFTERPRINT.'&afterprint=true&'.$_restparams;
        $this->actionsmarty->assign('afterprintevent', $afterprintevent);
        $this->actionsmarty->assign('hasafterprintevent', $this->currentaction->record->AFTERPRINT != '');

        $this->actionsmarty->assign('actionname', $this->currentaction->record->ACTIONNAME);
        $this->actionsmarty->assign('dialogtitle', $this->currentaction->record->TITLE);
        $text = $polaris->_ReplaceDynamicVariables($this->currentaction->record->TEXT, $nullvalue='', $currentlinerecord);
        $this->actionsmarty->assign('dialogtext', ProcessSQL($this->userdatabase, $text));
        $this->actionsmarty->display("printjasperreport2.tpl.php");
    }
}