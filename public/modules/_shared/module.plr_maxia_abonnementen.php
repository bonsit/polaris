<?php
require('_shared/module._base.php');

class ModulePLR_Maxia_Abonnementen extends _BaseModule {
    var $stylesheet;
    var $gen_form;

    function __construct($moduleid, $module, $clientname=false) {
        parent::__construct($moduleid, $module, $clientname);

        $this->stylesheet = 'default.css';
    }

    function ProcessRequest($event=false) {
        $_content = false;

        $this->form->database->userdb->SetFetchMode(ADODB_FETCH_ASSOC);
        try {
            $_func = $_GET['action'];
            if (!empty($_func)) {
                switch($this->moduleid) {
                case 1:
                break;
                }
            }
            if ($_content !== false) {
                echo $_content;
                exit; // stop with the rest of the html
            }
        } catch (Exception $e) {
            echo "Wrong parameters: ".$e;
        }

        return false;
    }

    function ShowListView($state, $permission, $detail=false, $masterrecord=false) {
        global $polaris;

        $_rs = $polaris->instance->GetAll("
            SELECT a.SUBSCRIPTIONID, a.APPLICATIONNAME, a.DESCRIPTION AS APPDESC
            , a.PRICEPERMONTH_MONTHLY AS APP_PRICEPERMONTH_MONTHLY
            , a.PRICEPERMONTH_YEARLY AS APP_PRICEPERMONTH_YEARLY
            , m.MODULENAME, m.DESCRIPTION AS MODDESC
            , m.PRICEPERMONTH_MONTHLY AS MOD_PRICEPERMONTH_MONTHLY
            , m.PRICEPERMONTH_YEARLY AS MOD_PRICEPERMONTH_YEARLY
            , m.STANDARDMODULE
            , t.BASE_PRICEPERMONTH_MONTHLY
            FROM plr_subscriptionapps a
            LEFT JOIN plr_subscriptiontypes t ON 2 = t.SUBSCRIPTIONID
            LEFT JOIN plr_subscriptionmodules m ON a.SUBSCRIPTIONID = m.SUBSCRIPTIONID AND m.ACTIVE = 'Y'
            WHERE a.ACTIVE = 'Y' AND a.SITE = ? AND t.SUBSCRIPTIONID = 2
            ORDER BY a.POSITION, m.POSITION
        ", $_ENV['POLARISINSTANCE']);

        $_items = [];
        $i = 0;
        foreach($_rs as $_key => $_row) {
            if ($_row['SUBSCRIPTIONID'] !== $_rs[$_key-1]['SUBSCRIPTIONID']) {
                if (isset($_rs[$_key-1]['SUBSCRIPTIONID'])) {
                    $i++;
                }
                $_items[$i] = $_row;
            }
            $_items[$i]['modules'][] = $_row;
        }
        $this->smarty->assign('items', $_items);
        $this->smarty->assign('selecteditem', $_GET['cursuscode']);
        $this->smarty->assign('stylesheet', $this->stylesheet);
        // $this->AddModuleJS(['maxia.js']);
        // $this->AddModuleJS(['maxia_subscriptions.js']);

        switch($this->moduleid) {
        case 1:
            echo $this->smarty->Fetch('subscriptions.tpl.html');
        break;
        }
    }

}
