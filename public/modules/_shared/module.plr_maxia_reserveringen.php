<?php
require('_shared/module.plr_maxia.php');

class ModulePLR_Maxia_Reserveringen extends ModulePLR_Maxia {

    const INTERNE_RESERVERINGEN_KALENDER = 1;
    const FLEXPLEK_RESERVERINGEN = 2;
    const FLEXPLEK_RESERVERINGEN_OUD = 3;
    const INTERNE_RESERVERINGEN = 4;

    const INTERNE_RESERVERINGEN_KALENDER_READONLY = 5;

    const CONSTRUCT_RESERVERINGEN_OVERZICHT_INTERN = 160;

    const STAT_BEZIG = 'bezig';
    const STAT_BINNENKORT = 'binnenkort';
    const STAT_VANDAAG = 'vandaag';

    const MAX_MAANDEN_IN_DE_TOEKOMST = 24;
    const MAX_DUUR_RESERVERING = 12;

    var $stylesheet;
    var $gen_form;
    var $processed;
    var $currentaction;

    function __construct($moduleid, $module, $clientname=false) {
        global $_GVARS;
        parent::__construct($moduleid, $module, $clientname);

        $this->processed = false;
        $this->stylesheet = 'default.css';
        $this->showControls = true;
        $this->showDefaultButtons = false;
    }

    function ProcessPostRequests($allesRecord, $event){
        switch ($event) {
            case 'bewaarflexreservering':
                $_result = $this->BewaarFlexplekReservering($allesRecord);
                break;
            case 'bewaarinternereservering':
                $_result = $this->BewaarInterneReservering($allesRecord);
                break;
            case 'reserveringdatum_verwijderen':
                $_result = $this->VerwijderReserveringDatum($allesRecord);
                break;
        }
        return $_result;
    }

    function ProcessShowRequests($allesRecord, $event){
        $_content = false;
        $this->form->database->userdb->SetFetchMode(ADODB_FETCH_ASSOC);

        switch($this->moduleid) {
            case self::INTERNE_RESERVERINGEN_KALENDER:
                $_start_date = new DateTimeImmutable($_GET['start']);
                $_end_date = new DateTimeImmutable($_GET['end']);
                switch ($event) {
                    case 'aanvraaguitrustingen':
                        $_content = $this->GetReserveringUitrusting($_GET['aanvraagid'], $_GET['ruimteid'], $_GET['rrule'], $_GET['duur'], $_GET['action']);
                        break;
                    case 'events':
                        $_content = $this->GetReserveringData($_start_date, $_end_date);
                        break;
                    case 'resources':
                        $_content = $this->GetReserveringLocaties($_start_date, $_end_date);
                        break;
                    case 'bevestig_reservering_verwijderen':
                        return $this->BevestigVerwijderReserveringData($_GET);
                    default:
                        break;
                }
            break;
            case self::FLEXPLEK_RESERVERINGEN:
                $_start_date = new DateTimeImmutable($_GET['start']??'');
                $_end_date = new DateTimeImmutable($_GET['end']??'');
                switch ($event) {
                    case 'locaties':
                        $_content = $this->GetActieveLocatiesAsJSON();
                        break;
                    case 'flexplekken_beschikbareruimtes':
                        $_content = $this->GetBeschikbareRuimtes($_GET['datum'], $_GET['tijdperiode_start'], $_GET['tijdperiode_eind']);
                        break;
                    case 'flexplekken_beschikbaarheid':
                        $_content = json_encode($this->GetBeschikbareFlexplekken($_GET['locatie'], $_GET['datum'], $_GET['tijdperiode'], false));
                        break;
                    case 'flexplekken_reserveringen':
                        $_content = $this->GetFlexplekkenReserveringen($_GET['locatie'], $_GET['datum'], $_GET['ruimte'], $_GET['tijdperiode']);
                        break;
                    case 'events':
                        $_content = $this->GetFlexPlekkenData($_start_date, $_end_date);
                        break;
                    case 'resources':
                        $_content = $this->GetFlexPlekkenLocaties($_start_date, $_end_date);
                        break;
                    default:
                        break;
                }
            case self::INTERNE_RESERVERINGEN:
                switch ($event) {
                    case 'checkbeschikbaarheid':
                        return $this->IsInterneRuimteVrij($_GET['RRULE'], $_GET['RRULE_DUUR'], $_GET['RUIMTE']);
                    default:
                        break;
                }
            break;
        }
        if ($_content !== false) {
            echo $_content;
            exit; // stop with the rest of the html
        }
    }

    function __GetCustomFilter() {
        // $_result = "`locatie_code` = (
        //     SELECT waarde
        //     FROM fb_parameters p
        //     WHERE p.parametercode = 'ACL' and p.client_hash = ttt.CLIENT_HASH
        //  )
        // ";
        $this->form->GetGroupFields(self::CONSTRUCT_RESERVERINGEN_OVERZICHT_INTERN);
        $_result = $this->form->AddGroupFilter('');
        $_result = $this->form->AddDataScopeFilter($_result);
        return $_result;
    }

    function IsInterneRuimteVrij($rrulestring, $rrule_duur, $ruimteid) {
        $_rrule = new RRule\RRule($rrulestring);
        $_rrule = $this->SanitizeRRule($_rrule);
        $this->CheckRRuleValidity($rrulestring, $rrule_duur);

        list($_tijdstart, $_tijdeind) = $this->GetRRuleTimeObjects($_rrule, $rrule_duur);
        $_db = $this->form->database->userdb;
        $_db->BeginTrans();
        try {
            $this->SaveRRuleTijdstippen($_rrule, $this->clienthash, null, 'I', $ruimteid, $_tijdstart, $_tijdeind);
        } catch (ADODB_Exception $E) {
            throw new Exception('De ruimte is niet vrij op de gewenste datum en tijd: '.$E->getMessage().' # '.$E->getCode());
        } finally {
            $_db->rollbackTrans();
        }

        return true;
    }

    function IsFlexPlekVrij($clienthash, $plrrecordid, $ruimteid, $datum, $tijdperiode_start, $tijdperiode_eind) {
        global $scramble;

        $_sql = "
            SELECT (SELECT AANTALFLEXPLEKKEN FROM fb_ruimtes r WHERE r.ruimte_id = ri2.ruimte_flex)
            AS AANTALFLEXPLEKKEN, COUNT(ri2.AANVRAAG_ID) AS AANTALBEZET
            FROM fb_reserveringen_intern ri2
            WHERE ri2.`client_hash` = ?
            AND ri2.__DELETED = 0
            AND ri2.`reservering_type` = 'F'
            AND ri2.`ruimte_flex` = ?
            AND ri2.gewenste_datum = STR_TO_DATE(?, '%d-%m-%Y')
            AND (
                (? >= gewenste_tijd_start AND ? < gewenste_tijd_eind)
                OR (? > gewenste_tijd_start AND ? <= gewenste_tijd_eind)
                OR (gewenste_tijd_start >= ? AND gewenste_tijd_eind <= ?)
            )
        ";

        if ($plrrecordid) {
            $_keyselect = $this->form->database->makeEncodedKeySelect('fb_reserveringen_intern', $plrrecordid);
            $_sql .= " AND NOT ({$_keyselect})";
        }

        list($_starttijd, $_eindtijd) = $this->DetermineTimeAsString($tijdperiode_start, $tijdperiode_eind);
        $_rs = $this->form->database->userdb->GetRow($_sql, array(
            $clienthash,
            $ruimteid,
            $datum,
            $_starttijd, $_starttijd,
            $_eindtijd, $_eindtijd,
            $_starttijd, $_eindtijd,
        ));
        return (($_rs['AANTALBEZET'] == 0) or ($_rs['AANTALBEZET'] > 0 and $_rs['AANTALBEZET'] < $_rs['AANTALFLEXPLEKKEN']));
    }

    function GetBeschikbareRuimtes($datum, $tijdperiode_start, $tijdperiode_eind) {
        $_where = $this->__GetCustomFilter();
        $_sql = "
            SELECT DISTINCT RUIMTE_ID, RUIMTE_CODE, RUIMTE_NAAM
            FROM fb_ruimtes ttt
            WHERE {$_where}
            AND ttt.aantalflexplekken > 0
            ORDER BY RUIMTE_CODE
        ";
        $_rs = $this->form->database->userdb->GetAll($_sql);
        return json_encode($_rs);
    }

    function GetBeschikbareFlexplekken($locatie, $datum, $tijdperiode_start, $tijdperiode_eind) {
        global $scramble;

        $_sql = "
            SELECT
                ttt.RUIMTE_CODE,
                ttt.RUIMTE_NAAM,
                ttt.AANTALFLEXPLEKKEN,
                ttt.ETAGE,
                ttt.RUIMTE_ID AS RUIMTE_ID,
                (
                    SELECT COUNT(aanvraag_id)
                    FROM fb_reserveringen_intern ri2
                    WHERE ri2.`client_hash` = ttt.client_hash
                    AND ri2.__DELETED = 0
                    AND ri2.`reservering_type` = 'F'
                    AND ri2.`ruimte_flex` = ttt.ruimte_id
                    AND ri2.gewenste_datum = ri.gewenste_datum
                    AND (
                        (? >= gewenste_tijd_start AND ? < gewenste_tijd_eind)
                        OR (? > gewenste_tijd_start AND ? <= gewenste_tijd_eind)
                        OR (gewenste_tijd_start >= ? AND gewenste_tijd_eind <= ?)
                    )
                ) AS AANTALBEZET,
                GROUP_CONCAT(ri.`contactpersoon` SEPARATOR '#') AS BEZETDOOR,
                GROUP_CONCAT(ri.`gewenste_tijd` SEPARATOR '#') AS BEZETTIJDEN,
                {$this->form->database->makeRecordIDColumn('fb_ruimtes', true)} AS PLR__RECORDID
            FROM
                fb_ruimtes ttt
            LEFT JOIN
                fb_locaties l
            ON
                ttt.`locatie` = l.`locatie_id`
            LEFT JOIN
                fb_reserveringen_intern ri
            ON
                ttt.ruimte_id = ri.ruimte_flex
                AND ri.__DELETED = 0
                AND ri.`reservering_type` = 'F'
                AND (
                    (? >= gewenste_tijd_start AND ? < gewenste_tijd_eind)
                    OR (? > gewenste_tijd_start AND ? <= gewenste_tijd_eind)
                    OR (gewenste_tijd_start >= ? AND gewenste_tijd_eind <= ?)
                )
                AND ri.gewenste_datum = STR_TO_DATE(?, '%d-%m-%Y')
            WHERE
                ttt.aantalflexplekken > 0
                AND l.`locatie_code` = ?
                AND ttt.`client_hash` = ?
            GROUP BY
                ttt.ruimte_id, ttt.ruimte_code, ttt.ruimte_naam, ttt.aantalflexplekken
            ORDER BY
                ttt.ETAGE, ttt.ruimte_code
        ";

        list($_starttijd, $_eindtijd) = $this->DetermineTimeAsString($tijdperiode_start, $tijdperiode_eind);
        $_rs = $this->form->database->userdb->GetAll($_sql, array(
            $_starttijd, $_starttijd,
            $_eindtijd, $_eindtijd,
            $_starttijd, $_eindtijd,
            $_starttijd, $_starttijd,
            $_eindtijd, $_eindtijd,
            $_starttijd, $_eindtijd,
            $datum,
            $locatie,
            $this->clienthash
        ));
        return $_rs;
    }

    function GetFlexplekkenReserveringen($locatie, $datum, $ruimte, $tijdperiode) {
        global $scramble;

        $_sql = "
            SELECT
                RI.RESERVERING_CODE,
                RI.CONTACTPERSOON,
                RI.CONTACTPERSOON_EMAIL,
                RI.CONTACTPERSOON_TELEFOON,
                RI.AANVRAAG_OMSCHRIJVING,
                RI.GEWENSTE_TIJD,
                R.RUIMTE_NAAM,
                R.RUIMTE_CODE
            FROM
                fb_reserveringen_intern ttt
            LEFT JOIN
                fb_ruimtes r
                ON r.RUIMTE_ID = ttt.ruimte_flex
            LEFT JOIN
                fb_locaties l
                ON r.`locatie` = l.`locatie_id`
            WHERE
                AND ttt.`reservering_type` = 'F'
                -- AND l.`locatie_code` = ?
                AND ttt.gewenste_datum = STR_TO_DATE(?, '%d-%m-%Y')
                AND (
                    (? between ttt.`gewenste_tijd_start` AND ttt.`gewenste_tijd_eind`
                      OR ttt.`gewenste_tijd_start` = ?)
                 OR (? between  ttt.`gewenste_tijd_start` AND ttt.`gewenste_tijd_eind`
          		     OR ttt.`gewenste_tijd_eind` = ?)
                )
                AND ttt.`ruimte_flex` = ?
        ";

        list($_starttijd, $_eindtijd) = $this->DetermineTimeAsString($tijdperiode);
        $_sql = $this->form->AddDataScopeFilter($_sql);
        $_rs = $this->form->database->userdb->GetAll($_sql, array(
            $datum,
            $_starttijd, $_starttijd,
            $_eindtijd, $_eindtijd,
            $ruimte
        ));
        return json_encode($_rs);
    }

    function GetRuimte($ruimteid) {
        $_result = [];
        if (!empty($ruimteid)) {
            $_sql = "
                SELECT LOCATIE, RUIMTE_ID, RUIMTE_CODE, RUIMTE_NAAM, RUIMTE_OMSCHRIJVING
                FROM fb_ruimtes
                WHERE RUIMTE_ID = ?
                AND CLIENT_HASH = ?
            ";
            $_result = $this->form->database->userdb->GetRow($_sql, array($ruimteid, $this->clienthash));
        }
        return $_result;
    }

    function GetReserveringUitrusting($aanvraagid, $ruimteid, $rrule, $duur, $action='edit') {
        global $scramble;


        $_huidigeLocatie = $this->GetRuimte($ruimteid)['LOCATIE'];
        if (empty($_huidigeLocatie)) {
            $_huidigeLocatie = $this->CheckHuidigeSessieLocatie();
        }

        $aanvraagid = $aanvraagid ?? 0;
        $_uitrusting = "
            SELECT * FROM (
                SELECT CASE au.__DELETED WHEN 0 THEN au.AANVRAAG_ID ELSE 0 END AS CHECKED,
                u.`UITRUSTING_NAAM`, r.`RUIMTE_CODE`,
                {$this->form->database->makeRecordIDColumn('fb_reserveringen_intern_uitrusting', 'au')} AS PLR__RECORDID,
                ru.UITRUSTING_ID, false AS VERWIJDERD_VAN_RUIMTEUITRUSTING,
                u.UITRUSTING_AANTAL,
                u.VOLGORDE, u.FOTOS
                FROM fb_ruimte_uitrusting ru
                LEFT JOIN `fb_reserveringen_intern_uitrusting` au
                ON ru.`uitrusting_id` = au.`uitrusting_id` AND au.AANVRAAG_ID = ?
                LEFT JOIN fb_ruimtes r ON r.ruimte_id = ru.ruimte_id AND r.LOCATIE = ?
                LEFT JOIN fb_uitrusting u ON ru.`uitrusting_id` = u.`uitrusting_id`
                WHERE ru.`ruimte_id` = ?
                AND ru.client_hash = ?
                AND ru.__DELETED = 0
                AND u.__DELETED = 0
                AND (JSON_CONTAINS(u.LOCATIES, CONCAT('\"',CAST(r.LOCATIE AS JSON),'\"'))
                    OR u.LOCATIES IS NULL                       -- of uitrusting is voor alle locaties (NULL value)
                    OR JSON_EXTRACT(u.LOCATIES, '$[0]') = \"\"  -- of uitrusting is voor alle locaties ([\"\"] value)
                )

                UNION DISTINCT

                SELECT CASE au.__DELETED WHEN 0 THEN au.AANVRAAG_ID ELSE 0 END AS CHECKED,
                u.`UITRUSTING_NAAM`, r.`RUIMTE_CODE`,
                {$this->form->database->makeRecordIDColumn('fb_reserveringen_intern_uitrusting', 'au')} AS PLR__RECORDID,
                u.UITRUSTING_ID, true AS VERWIJDERD_VAN_RUIMTEUITRUSTING,
                u.UITRUSTING_AANTAL,
                u.VOLGORDE, u.FOTOS
                FROM `fb_reserveringen_intern_uitrusting` au
                LEFT JOIN fb_reserveringen_intern a ON au.`aanvraag_id` = a.`aanvraag_id`
                LEFT JOIN fb_ruimtes r ON a.ruimte = r.ruimte_id AND r.LOCATIE = ?
                LEFT JOIN fb_uitrusting u ON au.`uitrusting_id` = u.`uitrusting_id`
                WHERE (au.AANVRAAG_ID = ?)
                AND au.client_hash = ?
            ) alles
            GROUP BY UITRUSTING_ID
            ORDER BY VOLGORDE
        ";

        $_uitrustingrs = $this->form->database->userdb->GetAll($_uitrusting, array(
            $aanvraagid
            , $_huidigeLocatie
            , $ruimteid
            , $this->clienthash
            , $_huidigeLocatie
            , $aanvraagid
            , $this->clienthash
        ));

        if (!empty($_uitrustingrs)) {
            $_uitrustingrs = $this->BepaalUitrustingStatus($rrule, $duur, $_uitrustingrs, $action);
            $_uitrustingrs = $this->ProcessFotos($_uitrustingrs);
        }
        return json_encode($_uitrustingrs);
    }

    function BepaalUitrustingStatus($rrule, $duur, $uitrusting, $action) {
        global $polaris;

        try {
            $_actieveLocatie = $this->CheckHuidigeSessieLocatie();

            $_rrule = new RRule\RRule($rrule);
            $_rrule = $this->SanitizeRRule($_rrule);

            foreach ($_rrule as $occurrence) {
                $_datums[] = $occurrence->format('Y-m-d');
            }
            list($_starttijd, $_eindtijd) = $this->GetRRuleTimeObjects($_rrule, $duur);
            $_tijdIntervallen = $this->genereerTijdIntervallen($_starttijd, $_eindtijd);

            // genereer een sql zoals: select distinct aanvraag from fb_reserveringen_intern_rrule rr where rr.`datum` in ('2024-03-25', '2024-03-26') and rr.tijdstip in ('09:00', '09:15', '09:30', '09:45', '10:00')
            $_sql = "
                SELECT DISTINCT AANVRAAG
                FROM fb_reserveringen_intern_rrule rr
                LEFT JOIN fb_reserveringen_intern ri ON rr.aanvraag = ri.aanvraag_id
                WHERE rr.datum IN ('".implode("','", $_datums)."')
                AND rr.tijdstip IN ('".implode("','", $_tijdIntervallen)."')
                AND ri.LOCATIE = ?
                AND ri.__DELETED = 0
            ";
            $_aanvragen = $this->form->database->userdb->GetAll($_sql, array($_actieveLocatie));
            $_aanvragenLijst = implode(',', array_map(function($a) { return $a['AANVRAAG']; }, $_aanvragen));

            if (empty($_aanvragenLijst)) $_aanvragenLijst = 0;
            // Bepaal hoeveel uitrusting er in gebruik is
            $_where = "ttt.`AANVRAAG_ID` IN ({$_aanvragenLijst})";
            $_where = $this->form->AddDataScopeFilter($_where);
            $_sqlIngebruik = "
                SELECT ttt.UITRUSTING_ID, UIT.`UITRUSTING_NAAM`, COUNT(ttt.UITRUSTING_ID) AS AANTALINGEBRUIK
                FROM FB_RESERVERINGEN_INTERN_UITRUSTING ttt
                LEFT JOIN FB_UITRUSTING UIT ON ttt.UITRUSTING_ID = UIT.`UITRUSTING_ID`
                AND UIT.__DELETED = 0
                AND UIT.LOCATIE = ?
                WHERE {$_where}
                GROUP BY UITRUSTING_ID
            ";
            $_ingebruik = $this->form->database->userdb->GetAll($_sqlIngebruik, array($_actieveLocatie));

            // Uitrusting is selecteerbaar als de clientparameter UITRBP = 'Y' en de actie insert is

            if ($action == 'edit') {
                $_uitrustingAltijdSelecteerbaar = true;
            } else {
                if ($polaris->plrClient->clientparameters)
                    $_uitrustingAltijdSelecteerbaar = $polaris->plrClient->clientparameters['UITRBP'] == 'Y';
            }
            // Loop door de uitrusting heen en vul de status in
            foreach ($uitrusting as $key => $value) {
                $uitrusting[$key]['AANTALINGEBRUIK'] = 0;
                foreach ($_ingebruik as $ingebruik) {
                    if ($value['UITRUSTING_ID'] == $ingebruik['UITRUSTING_ID']) {
                        $uitrusting[$key]['AANTALINGEBRUIK'] = $ingebruik['AANTALINGEBRUIK'];
                    }
                    $uitrusting[$key]['DISABLED'] = (($ingebruik['AANTALINGEBRUIK'] >= $value['UITRUSTING_AANTAL']) && !$_uitrustingAltijdSelecteerbaar);
                }
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            $polaris->logUserAction("BepaalUitrustingStatus failed", session_id(), $_SESSION['name'], $_sql.' '.$e->getMessage());
        }
        return $uitrusting;
    }

    function GetReserveringData($_start, $_end) {
        global $scramble;

        $_extraWhere = $this->__GetCustomFilter();

        $_sql = "
            SELECT ttt.AANVRAAG_ID , ttt.CLIENT_HASH , ttt.AANVRAGER_NAAM, ttt.RESERVERING_CODE
            , GROUP_CONCAT(sss24.UITRUSTING_NAAM, '_', IFNULL(sss25.ONTWIKKELSFEER_NAAM, '') ORDER BY sss24.VOLGORDE ASC SEPARATOR '§')
            AS UITRUSTINGEN
            , DATE_FORMAT( ttt.GEWENSTE_DATUM, '%d-%m-%Y') as GEWENSTE_DATUM
            , DATE_FORMAT( ttt.GEWENSTE_EINDDATUM, '%d-%m-%Y') as GEWENSTE_EINDDATUM
            , ttt.GEWENSTE_TIJD_START
            , ttt.GEWENSTE_TIJD_EIND
            , ttt.GEWENSTE_TIJD , ttt.AANVRAAG_OMSCHRIJVING , ttt.CONTACTPERSOON , ttt.CONTACTPERSOON_EMAIL
            , ttt.RRULE, ttt.RRULE_DUUR, ttt.EX_DATUMS
            , ttt.CONTACTPERSOON_TELEFOON , ttt.AANTAL_PERSONEN , ttt.STATUS
            , DATE_FORMAT( ttt.AANVRAAGDATUM, '') as AANVRAAGDATUM
            , ttt.OPMERKING , sss22.ruimte_select AS RUIMTE_sss22 , ttt.RUIMTE, sss22.RUIMTE_CODE, sss22.RUIMTE_NAAM
            , sss22.FOTOS
            , sss25.ONTWIKKELSFEER_NAAM
            , {$this->form->database->makeRecordIDColumn('fb_reserveringen_intern', true)} AS PLR__RECORDID
            FROM fb_reserveringen_intern ttt
            JOIN fb_ruimtes sss22 ON ttt.ruimte=sss22.RUIMTE_ID
            LEFT JOIN fb_reserveringen_intern_uitrusting sss23 ON ttt.AANVRAAG_ID = sss23.AANVRAAG_ID
            LEFT JOIN fb_uitrusting sss24 ON sss23.UITRUSTING_ID = sss24.UITRUSTING_ID
            LEFT JOIN fb_ontwikkelsferen sss25 ON sss24.ONTWIKKELSFEER = sss25.ONTWIKKELSFEER_ID
            LEFT JOIN fb_locaties sss27 ON ttt.LOCATIE = sss27.LOCATIE_ID
            WHERE ttt.RESERVERING_TYPE = 'I'
            AND ttt.RRULE IS NOT NULL
            AND GEWENSTE_DATUM <= ?
            AND GEWENSTE_EINDDATUM >= ?
            AND {$_extraWhere}
            GROUP BY ttt.AANVRAAG_ID
        ";
        // AND GEWENSTE_DATUM - interval 1 month <= ?
        // AND ? <= GEWENSTE_EINDDATUM + interval 7 day
        $_startdate = $_start->format('Y-m-d');
        $_enddate = $_end->format('Y-m-d');
        $_today = new DateTimeImmutable('today');
        $_rs = $this->form->database->userdb->GetAll($_sql, array($_enddate, $_startdate));
        $_result = array();

        foreach ($_rs as $key => $value) {
            $_thedate = new DateTimeImmutable($value['GEWENSTE_DATUM']);
            $_theEnddate = new DateTimeImmutable($value['GEWENSTE_EINDDATUM']);
            $_timesplit = explode(':', $value['GEWENSTE_TIJD_START']);
            $_dailystart = $_thedate->add(new DateInterval("PT{$_timesplit[0]}H{$_timesplit[1]}M"));
            $_timesplit = explode(':', $value['GEWENSTE_TIJD_EIND']);
            $_dailyend = $_thedate->add(new DateInterval("PT{$_timesplit[0]}H{$_timesplit[1]}M"));

            $_backgroundColors = array('nieuw'=>'#ed7a86','afgedrukt'=>'#583f7b','afgehandeld'=>'#51976f');

            if ($_theEnddate < $_today) {
                $_backgroundColor = '#ccc';
                $_url = '/app/facility/const/archief_int_aanvragen/?q[]='.$value['RESERVERING_CODE'];
            } else {
                $_backgroundColor = $_backgroundColors[$value['STATUS']] ?? '#1d84c6';
                $_url = '/app/facility/const/reserveringen_intern_rrule/?q[]='.$value['RESERVERING_CODE'];
            }

            // turn EX_DATUMS (comma separated) into an array, and add the T + gewenste tijdstart time to it
            $_excludeDatums = [];
            if (!empty($value['EX_DATUMS'])) {
                $_excludeDatums = explode(',', $value['EX_DATUMS']);
                foreach ($_excludeDatums as $key => $datum) {
                    $_excludeDatums[$key] = $datum . 'T' . $value['GEWENSTE_TIJD_START'];
                }
            }

            $_foto = $this->processImageUrl($value['FOTOS'], index:1, size:'thumbnail');

            $_resource = array(
                'resourceId' => $value['RUIMTE'],
                'title' => $value['RUIMTE_sss22'],
                'url' => $_url,
                'roomid' => $value['RUIMTE_CODE'],
                'roomname' => $value['RUIMTE_NAAM'],
                'person' => $value['CONTACTPERSOON'],
                'description' => $value['AANVRAAG_OMSCHRIJVING'],
                'recordid' => $value['PLR__RECORDID'],
                'rrule' => str_replace("\r\n","\n",$value['RRULE']),
                'duration' => $value['RRULE_DUUR'],
                'exdate' => $_excludeDatums,
                'backgroundColor' => $_backgroundColor,
                'borderColor' => $_backgroundColor,
                'equipment' => $value['UITRUSTINGEN'],
                'reserveringcode' => $value['RESERVERING_CODE'],
                'start' => $_dailystart->format('Y-m-d\TH:i:s'),
                'end' => $_dailyend->format('Y-m-d\TH:i:s'),
                'foto' => $_foto,
            );
            $_result[] = $_resource;
        }

        return json_encode($_result);
    }

    function GetReserveringLocaties($start, $end) {
        global $scramble;

        $_extraWhere = $this->__GetCustomFilter();

        $_sql = "
        SELECT  ttt.RUIMTE_ID, ttt.RUIMTE_CODE,  ttt.RUIMTE_NAAM
        ,  ttt.RUIMTE_OMSCHRIJVING ,  ttt.BEZETTING ,  ttt.MAX_CAPACITEIT
        ,  ttt.AANTALFLEXPLEKKEN ,  ttt.OPPERVLAKTE
        ,  ttt.ETAGE , sss8.locatie_naam AS LOCATIE_sss8
        ,  ttt.LOCATIE ,  ttt.BESCHIKBAARVOORVERHUUR
        ,  ttt.RUIMTE_SELECT_SIMPEL, ttt.RUIMTE_SELECT
        ,  ttt.FOTOS
        , {$this->form->database->makeRecordIDColumn('fb_ruimtes', true)} AS PLR__RECORDID
        FROM fb_ruimtes ttt
        INNER JOIN fb_locaties sss8 ON ttt.locatie=sss8.LOCATIE_ID
        AND sss8.__DELETED = 0 AND sss8.ACTIEF = 'Y'
        WHERE ttt.BESCHIKBAARVOORVERHUUR & 4 > 0
        AND {$_extraWhere}
        ";

        $_rs = $this->form->database->userdb->GetAll($_sql);
        $_result = array();
        if ($_rs) {
            foreach ($_rs as $key => $value) {
                $_foto = $this->processImageUrl($value['FOTOS'], 0);

                $_resource = array(
                    'id' => $value['RUIMTE_ID'],
                    'locatie' => $value['LOCATIE_sss8'],
                    'title' => $value['RUIMTE_CODE'],
                    'name' => $value['RUIMTE_NAAM'] . ' (max. ' . $value['MAX_CAPACITEIT'] . ' pers)',
                    'description' => $value['RUIMTE_SELECT'],
                    'foto' => $_foto
                );
                $_result[] = $_resource;
            }
        }

        return json_encode($_result);
    }

    function WeekPatternToArray($pattern) {
        $_days = ['Mon','Tue','Wed','Thu','Fri','Sat','Sun'];
        for($i=0; $i <= 6; $i++) {
            if (($pattern & pow(2, $i)) == pow(2, $i)) {
                $_result[] = ($_days[$i]);
            }
        }
        return $_result;
    }

    function MonthPatternToArray($pattern) {
        for($i=0; $i <= 11; $i++) {
            if (($pattern & pow(2, $i)) == pow(2, $i)) {
                $_result[] = $i;
            }
        }
        return $_result;
    }

    function AddTime($date, $time) {
        $_time = explode(':', $time);
        $modified = clone $date;
        // $date->add(new DateInterval('PT'.$_time[0].'H'.$_time[1].'M0S'));
        $modified = $modified->modify('+'.$_time[0].'hours +'.$_time[1].'minutes +0seconds');

        return $modified;
    }

    function DetermineTimeAsString($tijdperiode_start, $tijdperiode_eind=false) {
        switch ($tijdperiode_start) {
            case 'ochtend':
                $_starttijd = '08:00';
                $_eindtijd = '12:00';
            break;
            case 'middag':
                $_starttijd = '13:00';
                $_eindtijd = '17:00';
            break;
            case 'heledag':
                $_starttijd = '08:00';
                $_eindtijd = '17:00';
            break;
            default:
                $_starttijd = $tijdperiode_start;
                $_eindtijd = $tijdperiode_eind??'17:00';
        }
        return array($_starttijd, $_eindtijd);
    }

    function DetermineTime($date, $daypart) {
        $startTime = clone $date;

        switch ($daypart) {
            case 'ochtend':
                $startTime = $startTime->modify('+8hours +0minutes +0seconds');
                $endTime = $startTime->modify('+4hours +0minutes +0seconds');
                break;
            case 'middag':
                $startTime = $startTime->modify('+13hours +0minutes +0seconds');
                $endTime = $startTime->modify('+4hours +0minutes +0seconds');
                break;
            case 'heledag':
                $startTime = $startTime->modify('+8hours +0minutes +0seconds');
                $endTime = $startTime->modify('+9hours +0minutes +0seconds');
                break;
        }
        return array($startTime, $endTime);
    }

    function sanitizeTime($time) {
        // Validate the input format
        $time = substr($time, 0, 5);
        if (!preg_match('/^(\d{2}):(\d{2})$/', $time, $matches)) {
            $time = '08:00';        }
        return $time;
    }

    function GetFlexPlekkenData($_start, $_end) {
        global $scramble;

        $_extraWhere = $this->__GetCustomFilter();
        $_sql = "
            SELECT ttt.AANVRAAG_ID , ttt.CLIENT_HASH , ttt.AANVRAGER_NAAM
            , DATE_FORMAT( ttt.GEWENSTE_DATUM, '%d-%m-%Y') as GEWENSTE_DATUM
            , ttt.GEWENSTE_TIJD_START
            , ttt.GEWENSTE_TIJD_EIND
            , ttt.GEWENSTE_TIJD , ttt.AANVRAAG_OMSCHRIJVING , ttt.CONTACTPERSOON , ttt.CONTACTPERSOON_EMAIL
            , ttt.CONTACTPERSOON_TELEFOON , ttt.AANTAL_PERSONEN , ttt.STATUS
            , DATE_FORMAT( ttt.AANVRAAGDATUM, '') as AANVRAAGDATUM
            , ttt.OPMERKING , sss22.ruimte_select AS RUIMTE_sss22 , ttt.RUIMTE_FLEX, sss22.RUIMTE_CODE, sss22.RUIMTE_NAAM
            , {$this->form->database->makeRecordIDColumn('fb_reserveringen_intern', true)} AS PLR__RECORDID
            FROM fb_reserveringen_intern ttt
            LEFT JOIN fb_ruimtes sss22 ON ttt.RUIMTE_FLEX = sss22.RUIMTE_ID
            WHERE ttt.RESERVERING_TYPE = 'F'
            AND GEWENSTE_DATUM >= ?
            AND GEWENSTE_DATUM <= ?
            AND {$_extraWhere}
            GROUP BY ttt.AANVRAAG_ID
        ";

        $_startdate = $_start->format('Y-m-d');
        $_enddate = $_end->format('Y-m-d');
        $_today = new DateTimeImmutable('today');
        $_rs = $this->form->database->userdb->GetAll($_sql, array($_startdate, $_enddate));
        $_result = array();
        foreach ($_rs as $key => $value) {

            $_thedate = new DateTimeImmutable($value['GEWENSTE_DATUM']);
            $_timesplit = explode(':', ($this->sanitizeTime($value['GEWENSTE_TIJD_START'])));
            $_dailystart = $_thedate->add(new DateInterval("PT{$_timesplit[0]}H{$_timesplit[1]}M"));

            $_timesplit = explode(':', ($this->sanitizeTime($value['GEWENSTE_TIJD_EIND'])));
            $_dailyend = $_thedate->add(new DateInterval("PT{$_timesplit[0]}H{$_timesplit[1]}M"));

            $_backgroundColors = array('nieuw'=>'#ed7a86','afgedrukt'=>'#583f7b','afgehandeld'=>'#51976f');
            // $_backgroundColors = array('nieuw'=>'#a27479','afgedrukt'=>'#907eab','afgehandeld'=>'#749e88');

            if ($_thedate < $_today) {
                $_backgroundColor = '#ccc';
            } else {
                $_backgroundColor = '#6b9bb8';
            }

            $_resource = array(
                'id' => $value['PLR__RECORDID'],
                'resourceId' => $value['RUIMTE_FLEX'],
                'title' => $value['RUIMTE_sss22'],
                'ruimte_id' => $value['RUIMTE_FLEX'],
                'ruimte_code' => $value['RUIMTE_CODE'],
                'ruimte_naam' => $value['RUIMTE_NAAM'],
                'contactpersoon' => $value['CONTACTPERSOON'],
                'contactpersoon_email' => $value['CONTACTPERSOON_EMAIL'],
                'description' => $value['AANVRAAG_OMSCHRIJVING'],
                'plr__recordid' => $value['PLR__RECORDID'],
                'start' => $_dailystart->format('Y-m-d\TH:i:s'),
                'end' => $_dailyend->format('Y-m-d\TH:i:s'),
                'url' => '/app/facility/const/reserveringen_flexplek/edit/'.$value['PLR__RECORDID'].'/',
                'backgroundcolor' => $_backgroundColor,
                'bordercolor' => $_backgroundColor,
            );
            $_result[] = $_resource;
        }

        return json_encode($_result);
    }

    function GetFlexPlekkenLocaties($start, $end) {
        global $scramble;

        $_extraWhere = $this->__GetCustomFilter();
        $_sql = "
        SELECT
            ttt.RUIMTE_ID,
            ttt.RUIMTE_CODE,
            ttt.RUIMTE_NAAM,
            ttt.RUIMTE_OMSCHRIJVING,
            ttt.BEZETTING,
            ttt.MAX_CAPACITEIT,
            ttt.AANTALFLEXPLEKKEN,
            ttt.OPPERVLAKTE,
            ttt.ETAGE,
            sss8.locatie_naam AS LOCATIE_sss8,
            ttt.LOCATIE,
            ttt.BESCHIKBAARVOORVERHUUR,
            ttt.FOTO,
            {$this->form->database->makeRecordIDColumn('fb_ruimtes', true)} AS PLR__RECORDID
        FROM
            fb_ruimtes ttt
        LEFT JOIN
            fb_locaties sss8 ON ttt.locatie = sss8.LOCATIE_ID
        WHERE {$_extraWhere} AND ttt.AANTALFLEXPLEKKEN > 0
        ";

        $_rs = $this->form->database->userdb->GetAll($_sql);
        $_result = array();
        if ($_rs) {
            foreach ($_rs as $key => $value) {
                $_resource = array(
                    'id' => $value['RUIMTE_ID'],
                    'title' => $value['RUIMTE_CODE'],
                    'description' => $value['RUIMTE_NAAM']
                        . '<br>Flexplekken: ' . $value['AANTALFLEXPLEKKEN']
                );
                $_result[] = $_resource;
            }
        }

        return json_encode($_result);
    }

    function BewaarFlexplekReservering($recordArray) {
        // Validation
        if(empty($recordArray['GEWENSTE_DATUM'])
        || empty($recordArray['GEWENSTE_TIJD_START'])
        || empty($recordArray['GEWENSTE_TIJD_EIND'])
        || empty($recordArray['CONTACTPERSOON'])
        || empty($recordArray['RUIMTE_FLEX'])
        ) {
            throw new Exception('Een of meer verplichte velden zijn leeg');
        }

        if(empty($_SESSION['_GRP_LOCATIE'])) {
            throw new Exception('U heeft geen locatie geselecteerd');
        }

        $_tableName = 'fb_reserveringen_intern';
        $_fieldValues = StripHiddenFields($recordArray);
        $_ruimteVrij = $this->IsFlexPlekVrij($this->clienthash, $recordArray['_hdnRecordID']??false, $recordArray['RUIMTE_FLEX'], $recordArray['GEWENSTE_DATUM'], $recordArray['GEWENSTE_TIJD_START'], $recordArray['GEWENSTE_TIJD_EIND']);

        if (!$_ruimteVrij) throw new Exception('De ruimte is niet vrij op de gewenste datum en tijd');

        $_fieldValues['CLIENT_HASH'] = $_SESSION['clienthash'];
        $_fieldValues['RESERVERING_TYPE'] = 'F';
        $_fieldValues['LOCATIE'] = $_SESSION['_GRP_LOCATIE'];
        switch($recordArray['_hdnState']) {
            case 'edit':
                $_keyfieldselect = $this->form->database->makeEncodedKeySelect($_tableName, $recordArray['_hdnRecordID']);
                $this->form->database->updateRecord($_tableName, $_keyfieldselect, $_fieldValues);
                break;
            case 'insert':
                $autosupervalues = [];
                $this->form->database->insertRecord($_tableName, $_fieldValues, $autosupervalues);
                break;
            default:
        }
        return true;
    }

    function CheckRRuleValidity($rrule, $rrule_duur){
        // Validation
        if (empty($rrule_duur)
        || empty($rrule)
        ) {
            throw new Exception('Een of meer verplichte velden zijn leeg (RRULE of RRULE_DUUR)');
        }

        if ($rrule_duur == '00:00') {
            throw new Exception('Een reservering moet minimaal 15 minuten duren.');
        }

        $_tmpRRule = new RRule\RRule($rrule);
        $rrule = $this->SanitizeRRule($_tmpRRule);

        // Check if the RRULE has an ending
        if ($rrule->isInfinite()) {
            throw new Exception('De reservering kan niet oneindig zijn. Kies een einddatum of duur.');
        }

        if (count($rrule) == 0) {
            throw new Exception('De reservering heeft geen dagen in de gekozen periode. Kies een andere periode.');
        }

        // Check if RRULE duration is longer than 12 months
        if ($rrule->getRule()['UNTIL']) {
            $diff = $rrule->getRule()['UNTIL']->diff($rrule->getRule()['DTSTART'], true);
            $yearsInMonths = $diff->format('%y') * 12;
            $months = $diff->format('%m');
            $totalMonths = $yearsInMonths + $months;
            if ($totalMonths > self::MAX_DUUR_RESERVERING) {
                throw new Exception('Een reservering kan maximaal '.self::MAX_DUUR_RESERVERING.' maanden duren.');
            }
        }

        // Check if DTSTART date is less than 24 months from now
        if ($rrule->getRule()['DTSTART'] > (new DateTime('now'))->modify('+'.self::MAX_MAANDEN_IN_DE_TOEKOMST.' months')) {
            throw new Exception('De reservering kan maximaal '.self::MAX_MAANDEN_IN_DE_TOEKOMST.' maanden in de toekomst liggen.');
        }

        // Check if UNTIL date is after DTSTART date
        if ($rrule->getRule()['COUNT'] !== '1' && $rrule->getRule()['DTSTART'] > $rrule->getRule()['UNTIL']) {
            throw new Exception('De startdatum moet voor de einddatum liggen.');
        }
    }
    function GetRRuleTimeObjects($rrule, $duur) {
        $startTime = DateTime::createFromFormat('H:i', vsprintf('%02d:%02d', [
            $rrule->getRule()['BYHOUR'],
            $rrule->getRule()['BYMINUTE']
        ]));

        if ($startTime === false) {
            throw new Exception("Invalid start time format.");
        }

        list($hours, $minutes) = explode(':', $duur);

        // Round minutes to the nearest 15-minute block and ensure hours and minutes have the correct sign
        $minutes = round($minutes / 15) * 15;
        $hours = (int)$hours; // Cast to integer to ensure we have a number
        $minutes = (int)$minutes; // Cast to integer to ensure we have a number

        // If the duration is negative, subtract the interval from the start time
        if ($hours < 0 || $minutes < 0) {
            $hours = abs($hours);
            $minutes = abs($minutes);
            $duration = new DateInterval("PT{$hours}H{$minutes}M");
            $endTime = clone $startTime;
            $endTime->sub($duration);
        } else {
            $duration = new DateInterval("PT{$hours}H{$minutes}M");
            $endTime = clone $startTime;
            $endTime->add($duration);
        }

        if ($endTime <= $startTime) {
            throw new Exception("De eindtijd moet minimaal 15 minuten na de starttijd liggen. \n Starttijd {$startTime->format('H:i')} Eindtijd {$endTime->format('H:i')}.");
        }

        return [$startTime->format('H:i'), $endTime->format('H:i')];
    }

    function genereerTijdIntervallen($startTijd, $eindTijd, $intervalInMinuten = 15) {
        $start = DateTime::createFromFormat('H:i', $startTijd);
        $einde = DateTime::createFromFormat('H:i', $eindTijd);
        $interval = new DateInterval('PT' . $intervalInMinuten . 'M');
        $eenMinuut = new DateInterval('PT1M');

        $tijden = [];

        if ($start === false || $einde === false) {
            throw new Exception("Ongeldig tijdsformaat");
        }

        while($start <= $einde) {
            if (count($tijden) == 0) {
                $_tmpTijd = clone $start;
                $_tmpTijd->add($eenMinuut);
                $tijden[] = $_tmpTijd->format('H:i');
            } else {
                $tijden[] = $start->format('H:i');
            }
            $start->add($interval);
        }

        return $tijden;
    }

    function MaakParam($kolomnaam) {
        return $this->form->database->userdb->param($kolomnaam);
    }

    function SaveRRuleTijdstippen($rrule, $clienthash, $aanvraag, $reserveringstype, $ruimte, $startTijd, $eindTijd, $excludeDatums=false) {
        global $polaris;
        global $_GVARS;

        $_db = $this->form->database->userdb;
        $_db->bulkBind = true;

        // Als eerste, verwijder alle vorige tijdstippen van de aanvraag (indien aanwezig)
        if ($aanvraag)
            $_db->Execute("
                DELETE FROM fb_reserveringen_intern_rrule
                WHERE client_hash = ? AND aanvraag = ?
            ", array($clienthash, $aanvraag));

        if (count($rrule) == 0) { return false; }

        // Prepare het SQL insert statement
        $_kolommen = ['client_hash', 'aanvraag', 'reservering_type', 'ruimte', 'datum', 'tijdstip'];
        $_kolommenSelect = implode(',', $_kolommen);
        $_sql = "INSERT INTO fb_reserveringen_intern_rrule ({$_kolommenSelect})";

        // Verwerk alle datums van de RRULE
        $_values = '';
        // Zet de excludeDatums om in een array, indien aanwezig
        $_excludeDatums = $excludeDatums ? explode(',', $excludeDatums) : false;
        foreach ($rrule as $_datum) {
            if ($_excludeDatums && in_array($_datum->format('Y-m-d'), $_excludeDatums)) {
                continue;
            }
            $_tijdstippen = $this->genereerTijdIntervallen($startTijd, $eindTijd);
            // Voeg alle tijdstippen toe
            foreach($_tijdstippen as $_tijdstip) {
                $_values .= '(\'';
                $_values .= implode('\',\'', array($clienthash, $aanvraag, $reserveringstype, $ruimte, $_datum->format('Y-m-d'), $_tijdstip));
                $_values .= "'),";
            }
        }
        $_values = rtrim($_values, ',');
        try {
            if (!empty($_values))
                $_db->execute($_sql . ' VALUES ' . $_values);
        } catch (ADODB_Exception $E) {
            if ($E->getCode() == MYSQL_DUPLICATE_ENTRY) {
                $_message = 'De ruimte is niet vrij op de gewenste datum en tijd.';
                $_reserveringRecord = $this->GetReserveringRecord($E->getMessage());

                if (isset($_reserveringRecord)) {
                    $_href = '/app/facility/const/reserveringen_intern_rrule/?q[]=' . $_reserveringRecord['reservering_code'];
                    $_message .= "<br><br> (Wordt al gebruikt door reservering met code <a target='_blank' href='{$_href}'>{$_reserveringRecord['reservering_code']}</a> voor {$_reserveringRecord['contactpersoon']}.";

                    if (!empty($_reserveringRecord['gewenste_einddatum']) && $_reserveringRecord['gewenste_einddatum'] != $_reserveringRecord['gewenste_datum']) {
                        $_message .= "<br>Van {$_reserveringRecord['gewenste_datum']} tot {$_reserveringRecord['gewenste_einddatum']}";
                    } else {
                        $_message .= "<br>Op {$_reserveringRecord['gewenste_datum']}";
                    }

                    $_message .= ", om {$_reserveringRecord['gewenste_tijd']})";
                }

                $polaris->logQuick($E->getMessage());
                $polaris->logQuick($_message);
                throw new Exception($_message);
            } else {
                throw $E;
            }
        }
        return true;
    }

    function GetReserveringRecord($errormessage) {
        // find the string between 'Duplicate entry ' and ' for key'
        preg_match_all("/Duplicate entry '([^']*)' for key/", $errormessage, $_matches);
        if ($_matches) {
            $_duplicate = $_matches[1][0];
            $_duplicateValues = explode('-', $_duplicate);
            $_clienthash = $_SESSION['clienthash'];
            $_reservering_type = $_duplicateValues[1];
            $_ruimte = $_duplicateValues[2];
            $_datum = $_duplicateValues[3].'-'.$_duplicateValues[4].'-'.$_duplicateValues[5];
            $_tijdstip = $_duplicateValues[6];
            $_bestaandeReservering = $this->form->database->userdb->GetRow('
                SELECT i.reservering_code, i.contactpersoon, i.gewenste_datum, i.gewenste_einddatum, i.gewenste_tijd
                FROM fb_reserveringen_intern_rrule r
                LEFT JOIN fb_reserveringen_intern i ON r.aanvraag = i.aanvraag_id AND r.client_hash = i.client_hash
                WHERE r.client_hash = ? AND r.reservering_type = ? AND r.ruimte = ? AND r.datum = ? AND r.tijdstip = ?'
                , array($_clienthash, $_reservering_type, $_ruimte, $_datum, $_tijdstip));
            return $_bestaandeReservering;
        }
    }

    function SanitizeRRule(RRule\RRule $rrule) {
        // Get the RRule's properties as an array
        $rules = $rrule->getRule();

        // Check if BYMINUTE is set and if so, sanitize it
        if (isset($rules['BYMINUTE'])) {
            // Break down the minutes into an array if they're not already
            $minutes = is_array($rules['BYMINUTE']) ? $rules['BYMINUTE'] : explode(',', $rules['BYMINUTE']);

            // Map each minute to the nearest 15-minute interval
            $sanitizedMinutes = array_map(function ($minute) {
                return round($minute / 15) * 15 % 60; // Mod 60 to ensure minute is within 0-59
            }, $minutes);

            // Remove duplicates and sort the minutes
            $sanitizedMinutes = array_unique($sanitizedMinutes);
            sort($sanitizedMinutes);

            // Set the sanitized minutes back to the rules array
            $rules['BYMINUTE'] = implode(',', $sanitizedMinutes);
        }

        // Create a new RRule object with the sanitized rules
        return new RRule\RRule($rules);
    }

    function ReserveringInNatuurlijkeTaal($rruletekst, $gewensteDatum, $exDatums, $rrule) {
        $schema = $this->GetHumanreadableRRule($rruletekst);

        if ($rrule->getRule()['COUNT'] == '1') {
            $result = "Datum: {$gewensteDatum}";
        } else {
            $result = "Vanaf {$gewensteDatum}, ";
            $result .= $schema;
        }
        if (!empty($exDatums)) {
            // date format convert to 'd-m-Y' and implode with comma
            $exDatums = implode(', ', array_map(function($date) {
                return DateTime::createFromFormat('Y-m-d', $date)->format('d-m-Y');
            }, explode(',', $exDatums)));
            $result .= ' (behalve op ' . $exDatums . ')';
        }
        return $result;
    }

    function GetHumanReadableRRule($rrule) {
        $_no_time_rule_str = preg_replace('/BY(HOUR|MINUTE|SECOND)=\d+/', '', $rrule);
        $_no_time_rule_str = str_replace(';;;', '', $_no_time_rule_str);
        $_no_time_rule = new RRule\RRule($_no_time_rule_str);
        $result = str_replace('0 ├Minute: 0┤', '', $_no_time_rule->humanReadable([
            'locale' => 'nl',
            'include_start' => false,
            'date_formatter' => function($date) {
                return $date->format('d-m-Y');
            },
        ]));
        $result = str_replace('tot en met', 'tot', $result);
        $result = strtolower($result);
        return $result;
    }

    function BewaarInterneReservering($recordArray) {
        $this->CheckRRuleValidity($recordArray['RRULE'], $recordArray['RRULE_DUUR']);

        $_tmpRRule = new RRule\RRule($recordArray['RRULE']);
        $rrule = $this->SanitizeRRule($_tmpRRule);

        // Vul de afleidbare velden in
        $recordArray['GEWENSTE_DATUM'] = $rrule->getRule()['DTSTART']->format('d-m-Y');
        $recordArray['GEWENSTE_EINDDATUM'] = $rrule->getRule()['COUNT'] == '1' ? $recordArray['GEWENSTE_DATUM']: $rrule->getRule()['UNTIL']->format('d-m-Y');
        list($recordArray['GEWENSTE_TIJD_START'], $recordArray['GEWENSTE_TIJD_EIND']) = $this->GetRRuleTimeObjects($rrule, $recordArray['RRULE_DUUR']);

        // Maak een rrule zonder tijd omdat die rare dingen laat zien bij HumanReadable
        $recordArray['SCHEMA_TERUGKEREND'] = $this->ReserveringInNatuurlijkeTaal($recordArray['RRULE'], $recordArray['GEWENSTE_DATUM'], $recordArray['EX_DATUMS'], $rrule);

        $_db = $this->form->database->userdb;
        $_db->beginTrans();
        try {
            $_genValue = SaveRecord($recordArray, $this->form->database);
            $_aanvraagID = $recordArray['_hdnState'] == 'insert' ? $_genValue : $recordArray['AANVRAAG_ID'];
            if (!is_numeric($_aanvraagID)) throw new Exception('Aanvraag ID is nul of niet numeriek.');

            try {
                $this->SaveRRuleTijdstippen($rrule, $this->clienthash, $_aanvraagID, $recordArray['RESERVERING_TYPE'], $recordArray['RUIMTE'], $recordArray['GEWENSTE_TIJD_START'], $recordArray['GEWENSTE_TIJD_EIND'], $recordArray['EX_DATUMS']);
            } catch (ADODB_Exception $E) {
                $_db->rollbackTrans();
                throw new Exception('De ruimte is niet vrij op de gewenste datum en tijd.');
            }
        } catch (ADODB_Exception $E) {
            $_db->rollbackTrans();
            throw new Exception('De reservering kon niet worden opgeslagen. ' . $E->getMessage());
        }
        $_db->commitTrans();

        return true;
    }

    function VerwijderRRuleTijdstippen($aanvraagID, $exDatums) {
        // Wrap each date in single quotes
        $quotedDates = array_map(function($date) {
            return "'" . $date . "'";
        }, $exDatums);

        // SQL statement with placeholders
        $_sql = "DELETE FROM fb_reserveringen_intern_rrule WHERE client_hash = ? AND aanvraag = ? AND datum IN (?)";

        // Prepare parameters array
        $params = array($this->clienthash, $aanvraagID, implode(',', $quotedDates));

        $this->form->database->userdb->Execute($_sql, $params);
    }

    function VerwijderReserveringDatum($record) {
        if (empty($record['PLR__RECORDID'])) {
            throw new Exception('Geen reservering met de ID gevonden.');
        }

        if (!empty($record['VERWIJDER_RESERVERING'])) {
            $this->form->database->deleteHashedRecord('fb_reserveringen_intern', $record['VERWIJDER_RESERVERING']);
        } else {
            if (!empty($record['VERWIJDER_DATUM'])) {
                // Sorteer de datums
                sort($record['VERWIJDER_DATUM']);
                // Maak een comma-separated string van de verwijderdatums
                $_verwijderdatums = implode(',', $record['VERWIJDER_DATUM']);
            } else {
                $_verwijderdatums = '';
            }
            $_rs = $this->GetReservering($record['PLR__RECORDID']);
            $_saveRecord['_hdnDatabase'] = $this->form->databasehash;
            $_saveRecord['_hdnTable'] = 'fb_reserveringen_intern';
            $_saveRecord['_hdnState'] = 'edit';
            $_saveRecord['_hdnRecordID'] = $record['PLR__RECORDID'];
            $_saveRecord['AANVRAAG_ID'] = $_rs['AANVRAAG_ID'];
            $_saveRecord['RESERVERING_TYPE'] = $_rs['RESERVERING_TYPE'];
            $_saveRecord['RRULE'] = $_rs['RRULE'];
            $_saveRecord['RRULE_DUUR'] = $_rs['RRULE_DUUR'];
            $_saveRecord['RUIMTE'] = $_rs['RUIMTE'];
            $_saveRecord['EX_DATUMS'] = $_verwijderdatums;

            try {
                $this->BewaarInterneReservering($_saveRecord);
            } catch (Exception $E) {
                throw new Exception('De reservering kon niet worden aangepast. ' . $E->getMessage());
            }

        }

        return true;
    }

    function BevestigVerwijderReserveringData($record) {
        if (empty($record['PLR__RECORDID'])) {
            throw new Exception('Geen reservering met de ID gevonden.');
        }

        $_rs = $this->GetReservering($record['PLR__RECORDID']);

        $_datums = $this->GetReserveringDatums($_rs);
        $_verwijderde_datums = $this->GetReserveringVerwijderdeDatums($_rs);
        $_verwijder_datum = date('Y-m-d', strtotime($record['VERWIJDERDATUM']));
        $this->smarty->assign('reserveringscode', $_rs['RESERVERING_CODE']);
        $this->smarty->assign('contactpersoon', $_rs['CONTACTPERSOON']);
        $this->smarty->assign('aanvraag_omschrijving', $_rs['AANVRAAG_OMSCHRIJVING']);
        $this->smarty->assign('plr__recordid', $record['PLR__RECORDID']);
        $this->smarty->assign('datums', $_datums);

        $this->smarty->assign('verwijderdedatums', $_verwijderde_datums);
        $this->smarty->assign('verwijderdatum', $_verwijder_datum);
        echo $this->Fetch('bevestig_verwijder_reservering.tpl.php');
        exit();
    }

    function GetReservering($recordID) {
        $_extraWhere = $this->__GetCustomFilter();

        $_sql = "SELECT AANVRAAG_ID, RESERVERING_TYPE, RESERVERING_CODE, CONTACTPERSOON, AANVRAAG_OMSCHRIJVING, RRULE, RRULE_DUUR, RUIMTE, EX_DATUMS FROM fb_reserveringen_intern ttt WHERE $_extraWhere AND {$this->form->database->makeRecordIDColumn('fb_reserveringen_intern', true)} = ?";
        $_rs = $this->form->database->userdb->GetRow($_sql, array($recordID));

        if (empty($_rs))
            throw new Exception('Geen reservering met de ID gevonden.');

        return $_rs;
    }

    function GetReserveringDatums($rs) {
        $result = [];

        if (!empty($rs['RRULE']))  {
            $_rrule = new RRule\RRule($rs['RRULE']);
            foreach ( $_rrule as $occurrence ) {
                $result[] = $occurrence->format('Y-m-d');
            }
        }

        return $result;
    }

    function GetReserveringVerwijderdeDatums($rs) {
        $_result = [];

        if (!empty($rs['EX_DATUMS']))  {
            $_result = explode(',', $rs['EX_DATUMS']);
        }

        return $_result;
    }


    function GetLocatieAndRuimte($masterrecord) {
        $_sql = "SELECT l.locatie_naam, l.locatie_code FROM fb_locaties l WHERE l.locatie_id = ?";
        $_rs = $this->form->database->userdb->GetRow($_sql, array($masterrecord->LOCATIE));
        $masterrecord->LOCATIE_NAAM = $_rs['locatie_code'].' '.$_rs['locatie_naam'];

        $_sql = "SELECT r.ruimte_naam, r.ruimte_code FROM fb_ruimtes r WHERE r.ruimte_id = ?";
        $_rs = $this->form->database->userdb->GetRow($_sql, array($masterrecord->RUIMTE));
        $masterrecord->RUIMTE_NAAM = $_rs['ruimte_naam'].' ('.$_rs['ruimte_code'].')';

        return $masterrecord;
    }

    function generateICSFile($masterrecord) {
        // Generate a unique identifier
        $uid = $masterrecord->RESERVERING_CODE . '@' . $_SERVER['HTTP_HOST'];
        // Create the ICS content
        $ics_content = "BEGIN:VCALENDAR\r\n";
        $ics_content .= "VERSION:2.0\r\n";
        $ics_content .= "PRODID:-//Maxia//Reserveringen//NL\r\n";
        $ics_content .= "BEGIN:VEVENT\r\n";
        $ics_content .= "UID:" . $uid . "\r\n";
        $ics_content .= "DTSTAMP:" . gmdate('Ymd\THis\Z') . "\r\n";
        $ics_content .= "DTSTART;TZID=Europe/Amsterdam:" . $this->formatDateTimeForICS($masterrecord->GEWENSTE_DATUM, $masterrecord->GEWENSTE_TIJD_START) . "\r\n";
        $ics_content .= "DTEND;TZID=Europe/Amsterdam:" . $this->formatDateTimeForICS($masterrecord->GEWENSTE_EINDDATUM ?? $masterrecord->GEWENSTE_DATUM, $masterrecord->GEWENSTE_TIJD_EIND) . "\r\n";

        $ics_content .= $this->getTimeZoneDefinition();

        $ics_content .= "SUMMARY:" . $this->escapeStringForICS($masterrecord->AANVRAAG_OMSCHRIJVING) . "\r\n";
        $ics_content .= "DESCRIPTION:" . $this->escapeStringForICS($masterrecord->AANVRAAG_OMSCHRIJVING . ' (Reserveringscode: ' . $masterrecord->RESERVERING_CODE. ')') . "\r\n";
        $ics_content .= "LOCATION:" . $this->escapeStringForICS($masterrecord->RUIMTE_NAAM ?? $masterrecord->RUIMTE_CODE) . "\r\n";

        if (!empty($masterrecord->RRULE)) {
            $ics_content .= "RRULE:" . $masterrecord->RRULE . "\r\n";
        }

        $ics_content .= "ORGANIZER;CN=" . $this->escapeStringForICS($masterrecord->CONTACTPERSOON) . ":mailto:" . $masterrecord->CONTACTPERSOON_EMAIL . "\r\n";
        $ics_content .= "STATUS:CONFIRMED\r\n";
        $ics_content .= "END:VEVENT\r\n";
        $ics_content .= "END:VCALENDAR\r\n";

        // Set headers for file download
        header('Content-Type: text/calendar; charset=utf-8');
        header('Content-Disposition: attachment; filename="maxia_reservering_' . $masterrecord->RESERVERING_CODE . '.ics"');

        // Output the ICS content
        echo $ics_content;
    }

    function getTimeZoneDefinition() {
        return "BEGIN:VTIMEZONE\r\n" .
               "TZID:Europe/Amsterdam\r\n" .
               "BEGIN:DAYLIGHT\r\n" .
               "TZOFFSETFROM:+0100\r\n" .
               "TZOFFSETTO:+0200\r\n" .
               "TZNAME:CEST\r\n" .
               "DTSTART:19700329T020000\r\n" .
               "RRULE:FREQ=YEARLY;BYDAY=-1SU;BYMONTH=3\r\n" .
               "END:DAYLIGHT\r\n" .
               "BEGIN:STANDARD\r\n" .
               "TZOFFSETFROM:+0200\r\n" .
               "TZOFFSETTO:+0100\r\n" .
               "TZNAME:CET\r\n" .
               "DTSTART:19701025T030000\r\n" .
               "RRULE:FREQ=YEARLY;BYDAY=-1SU;BYMONTH=10\r\n" .
               "END:STANDARD\r\n" .
               "END:VTIMEZONE\r\n";
    }

    function formatDateTimeForICS($date, $time) {
        $datetime = DateTime::createFromFormat('Y-m-d H:i', $date . ' ' . $time, new DateTimeZone('Europe/Amsterdam'));
        return $datetime->format('Ymd\THis');
    }

    function escapeStringForICS($string) {
        $string = str_replace(array("\r\n", "\n", "\r"), "\\n", $string);
        $string = addcslashes($string, ',;');
        return $string;
    }

    function GetActiviteitStatus($datum, $mintijd, $maxtijd) {
        $_result = '';
        if (!empty($datum)) {
            // creeer nieuwe datum objecten op basis van $datum en $mintijd en $maxtijd
            $_min_tijdstip = new DateTime($datum . ' ' . $mintijd);
            $_max_tijdstip = new DateTime($datum . ' ' . $maxtijd);

            $_huidigeTijd = new DateTime('now');
            // $_huidigeTijd->modify('-1 hour');

            // indien de huidige tijd tussen de min en max datum ligt, dan is de activiteit_status 'bezig'
            // indien de huidige tijd één uur voor de aanvangstijd ligt, dan is de activiteit_status 'binnenkort'
            // indien de datum  vandaag is, dan is de activiteit_status 'vandaag'
            $_eenUurLatereStart = clone $_huidigeTijd;
            $_eenUurLatereStart->modify('+1 hour');

            if ($_huidigeTijd >= $_min_tijdstip && $_huidigeTijd <= $_max_tijdstip) {
                $_result = self::STAT_BEZIG;
            } elseif (($_eenUurLatereStart >= $_min_tijdstip) && ($_huidigeTijd <= $_min_tijdstip)) {
                $_result = self::STAT_BINNENKORT;
            // } elseif ($_huidigeTijd > $_max_tijdstip) {
            //     $_result = 'afgelopen';
            } elseif ($_huidigeTijd->format('Y-m-d') == $_min_tijdstip->format('Y-m-d')
                && $_huidigeTijd <= $_max_tijdstip) {
                $_result = self::STAT_VANDAAG;
            }
        }
        return $_result;
    }

    function ShowListView($state, $permission, $detail=false, $masterrecord=false) {
        $this->smarty->assign('stylesheet', $this->stylesheet);
        $this->AddCustomJS(['/modules/_shared/module.plr_maxia/js/maxia.js']);

        $this->form->LoadViewContent($detail, $masterrecord, $limit=false, $this->__GetCustomFilter());
        $this->smarty->assign('hasactivelocation', !empty($this->GetActieveDefaultLocatie()));
        switch($this->moduleid) {
            case self::INTERNE_RESERVERINGEN_KALENDER:
                $this->AddCustomJS([
                    '/assets/dist/rrule.min.js',
                    '/assets/dist/index.global.js',
                    '/modules/_shared/module.plr_maxia/js/maxia.js'
                ]);
                $this->AddModuleJS(['maxia_reserveringen.js']);
                echo $this->Fetch('reserveringen_kalender.tpl.php');
                break;
            case self::INTERNE_RESERVERINGEN_KALENDER_READONLY:
                $this->AddCustomJS([
                    '/assets/dist/rrule.min.js',
                    '/assets/dist/index.global.js',
                    '/modules/_shared/module.plr_maxia/js/maxia.js'
                ]);
                $this->AddModuleJS(['maxia_reserveringen_readonly.js']);
                echo $this->Fetch('reserveringen_kalender_readonly.tpl.php');
                break;
            case self::FLEXPLEK_RESERVERINGEN:
                $this->AddModuleJS(['maxia_flexplekken_kalender.js']);
                echo $this->Fetch('flexplekken_kalender.tpl.php');
                break;
            case self::INTERNE_RESERVERINGEN:
                $this->AddModuleJS(['maxia_reserveringen.js']);
                $this->showDefaultButtons = true;
                $this->ShowModuleListview($this->form->rsdata);
                break;
        }
    }

    function ShowFormView($state, $permission, $detail=false, $masterrecord=false) {
        global $polaris;

        switch($this->moduleid) {
            case self::INTERNE_RESERVERINGEN:
                if ($_GET['ical'] == 'true') {
                    $masterrecord = $this->GetLocatieAndRuimte($masterrecord);
                    $this->generateICSFile($masterrecord);
                } else {
                    $this->AddCustomJS([
                        '/assets/dist/rrule.min.js',
                        '/assets/dist/index.global.js',
                        '/modules/_shared/module.plr_maxia/js/maxia.js'
                    ]);
                    $this->AddModuleJS(['maxia_reserveringen.js']);
                    $this->ShowModuleFormview([]);
                }
                break;
        }
    }
}
