<?php
set_time_limit(300);
require_once 'sql/mysql-lang.inc.php';
require_once '_shared/module._base.php';

class ModulePLR_Designer extends _BaseModule {

    function __construct($moduleid, $module, $clientname=false) {
        parent::__construct($moduleid, $module, $clientname);

        $this->processed = false;
        $this->stylesheet = 'default.css';
    }

    function ProcessRequest($event=false) {
        global $polaris;
        global $FormpermHash;

        $error = false;

        if (isset($_POST['_hdnProcessedByModule'])) {

            try {
                $result = true;
                if ($_POST['_hdnState'] == 'insert') {
                    $_sql = "
                    ";
                    $polaris->instance->Execute($_sql,
                      array());
                }

                $endresult = array('result'=>$result, 'error'=>$error);

                echo json_encode(utf8json($endresult));
            } catch (Exception $E) {
                $result = false;
                $detailedmessage = $E->completemsg."\r\n".parse_backtrace(debug_backtrace());
                $polaris->LogQuick($detailedmessage);

                $resultArray = Array('result'=>false, 'query'=>$_SERVER['REQUEST_URI'], 'error'=>$E->msg, 'detailed_error'=>$detailedmessage);
                echo  json_encode(utf8json($resultArray));
            }
            exit;
        }
    }

    function DisplayUserGroupsList() {
        global $polaris;
        global $_sqlUsersGroups;
        global $_sqlClientUsersGroups;

        if ($this->outputformat == 'xhtml') {

            $currentclient = $_SESSION['clienthash'];

            if (!$currentclient)
                $rs_usersgroups = $polaris->instance->Execute($_sqlUsersGroups) or Die('Query failed:'.$_sqlUsersGroups);
            else
                $rs_usersgroups = $polaris->instance->Execute($_sqlClientUsersGroups, array($currentclient)) or Die('Query failed:'.$_sqlClientUsersGroups);

            if ($rs_usersgroups)
                $rs_usersgroups = $rs_usersgroups->GetArray();

            $this->smarty->assign('usergroup', $rs_usersgroups);
            $this->smarty->assign('modulepage', 'usergroups.tpl.php');
            $this->smarty->display('designer_main.tpl.php');
        }
    }

    function DisplayUserGroup() {
        $this->smarty->assign('modulepage', 'dsg_edit_user.tpl.php');
        $this->smarty->display('designer_main.tpl.php');
    }

    function DisplayDesignerPage($page) {
        $this->smarty->assign('modulepage', $page.'.tpl.php');
        $this->smarty->display('designer_main.tpl.php');
    }

    function ShowListView($state, $permission, $detail=false, $masterrecord=false) {
        // $this->smarty->assign('stylesheet', $this->stylesheet);
        // $this->AddCustomJS(['/modules/_shared/module.plr_maxia/js/maxia.js']);
        switch($this->moduleid) {
        case 'usergroups':
            $this->AddModuleJS(['....js']);
            if (isset($_GET['action'])) {
                $this->DisplayUserGroup();
            } else {
                $this->DisplayUserGroupsList();
            }
            break;
        }
    }

    // function Show($outputformat='xhtml', $rec=false) {
    //     global $polaris;
    //     global $_GVARS;
    //     global $_sqlDatabases;
    //     global $_sqlClientDatabases;

    //     $polaris->instance->SetFetchMode(ADODB_FETCH_ASSOC);

    //     parent::Show();

    //     switch ($this->moduleid) {
    //     case 'dbs':
    //         if (!$currentclient)
    //             $rs_dbs = $polaris->instance->Execute($_sqlDatabases) or Die('_sqlDatabases Query failed:'.$_sqlDatabases);
    //         else
    //             $rs_dbs = $polaris->instance->Execute($_sqlClientDatabases, array($currentclient)) or Die('_sqlDatabases Query failed:'.$_sqlClientDatabases);
    //         if ($rs_dbs)
    //             $rs_dbs = $rs_dbs->GetArray();

    //         $this->smarty->assign('recset', $rs_dbs);
    //         $this->DisplayDesignerPage($this->moduleid);

    //         break;
    //     case 'usergroups':
    //         /***
    //         * Usersgroups
    //         */
    //         if ($_GET['func'] != '') {
    //             if ($_GET['func'] == 'plr_clients') {
    //                 echo $this->GetPLRClients();
    //             }
    //         } else {
    //             $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/shared.js';
    //             if (isset($_GET['action'])) {
    //                 $this->DisplayUserGroup();
    //             } else {
    //                 $this->DisplayUserGroupsList();
    //             }
    //         }
    //         break;
    //     default:
    //         $this->DisplayDesignerPage($this->moduleid);
    //     }
    // }
}
