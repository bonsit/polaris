<link rel="stylesheet" type="text/css" media='all' href="{$moduleroot}/css/{$stylesheet}" />
<style>
    table.planner th {ldelim}width:{$colperc}%;{rdelim}
</style>

<span id="planning_extra_action">{$masterselect}</span>

<br /><div id="planner_selectbar">{$selectbar}</div>

<table id="planning_1" class="planner striped {$view}">
<thead {if $headerfontcolor}style="color:{$headerfontcolor}"{/if}><tr>
<th class="firstcol">&nbsp;</th>
{section name=i loop=$cols}
<th style="background-color:{cycle values=$headercolors}; color:{$headerfontcolors[i]};" {if $smarty.now|date_format:"%Y%m%d" == $cols[i].value}class="today"{/if}>
{if $cols[i].link != ''}<a href="{$cols[i].href}" class="popbox">{/if}
{$cols[i].label}
{if $cols[i].link != ''}</a>{/if}
</th>
{/section}
</tr></thead>

{$bodydata}

</table>

<script type="text/javascript" src='{$parentmoduleroot}/javascript/planner.js'></script>
<script type="text/javascript" src='{$parentmoduleroot}/javascript/tooltip.js'></script>