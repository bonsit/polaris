<link rel="stylesheet" type="text/css" media='all' href="{$moduleroot}/css/{$stylesheet}" />
<style>
    table.planner th {ldelim}width:{$colperc}%;{rdelim}
</style>

<span id="planning_extra_action">{$masterselect}</span>
<div id="planner_bar">
<span id="planner_selectbar">{$selectbar}</span>
</div>

{if $filterrows}
<input type="hidden" name="_hdnProcessedByModule" value="true" />
<input type="hidden" name="_hdnCancelfilter" value="" />
{/if}
<table id="planning_1" class="planner striped {$view}">
<thead {if $headerfontcolor}style="color:{$headerfontcolor}"{/if}><tr>
<th class="firstcol">
{if $filterrows}
<input type="submit" value="{lang set_focus}" />&nbsp;<a href="#" id="checkall">{lang check_all_short}</a>
<a href="#" id="checknone">{lang check_none_short}</a>
{if isset($smarty.session.row_filtervalues)}<a href="#" id="cancelfilter">{lang filter_cancel_short}</a>&nbsp;{/if}
{else}
&nbsp;{/if}</th>
{section name=i loop=$cols}
<th style="background-color:{cycle values=$headercolors}; color:{$headerfontcolors[i]};" {if $smarty.now|date_format:"%Y%m%d" == $cols[i].value}class="today"{/if}>
{if $cols[i].link != ''}<a href="{$cols[i].link}" {if $cols[i].linkaspopup == 'true'}class="popbox"{/if}>{/if}
{$cols[i].label}
{if $cols[i].link != ''}</a>{/if}
{if $cols[i].extratext != ''}<br /><span class="extratext">{$cols[i].extratext}</span>{/if}
</th>
{/section}
</tr></thead>

{$bodydata}

</table>

<script type="text/javascript" src='{$parentmoduleroot}/javascript/planner.js'></script>
<script type="text/javascript" src='{$parentmoduleroot}/javascript/tooltip.js'></script>