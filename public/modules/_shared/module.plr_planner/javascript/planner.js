Planner = {
    confirmAction: function(actionid) {
        var actions = ['dit item'];
        return confirm('Weet u zeker dat u '+actions[actionid]+'wilt verwijderen?');
    },

    createSelectUrlQuery: function(name, value) {
        var url = window.location.protocol+'//'+window.location.host+window.location.pathname;
        var query = {};
        if (window.location.search.substr(1) != '')
            query = Polaris.Base.parseQueryString(window.location.search.substr(1));
        query[name] = value;
        return url+'?'+Polaris.Base.queryString(query);
    },

    checkAllRows: function(formid, onoff) {
        $('#'+formid+':checkbox').val('checked', onoff);
    },

    _init_: function() {
        $('select.plannerselect').change(
            function(e) { 
                window.location = Planner.createSelectUrlQuery($(this).attr('name'), $(this).val());
            }
        );

        //if ($('planning_extra_action') && $('planning_extra_action').childNodes.length > 0)
        //    appendChildNodes($('constructtitle'), $('planning_extra_action').childNodes);

        Polaris.Visual.stripeTables('planning_1');
        
        Polaris.Form.adjustLabelWidth('formview');
        
        if (typeof(sweetTitles) != "undefined")
            sweetTitles.init();

        if ($('#checkall').length) {
            var filterrows = $('#filterrows');
            $('#checkall').click(function() {
                Planner.checkAllRows('dataform',true);
            });
            $('#checknone').click(function() {
                Planner.checkAllRows('dataform',false);
            });
            if ($('#cancelfilter').length) {
                $('#cancelfilter').click(function() {
                    $('#dataform')._hdnCancelfilter.value='true';
                    $('#dataform').submit();
                });
            }
        }

    }
};

_loadevents_[_loadevents_.length] = Planner._init_;
