Array.prototype.inArray = function (value) {
	var i;
	for (i=0; i < this.length; i++) {
		if (this[i] === value) {
			return true;
		}
	}
	return false;
};

/*
Sweet Titles (c) Creative Commons 2005
http://creativecommons.org/licenses/by-sa/2.5/
Author: Dustin Diaz | http://www.dustindiaz.com
*/
var sweetTitles = { 
    startTime: 10,
    endOpacity: .95,
	xCord : 0,								// @Number: x pixel value of current cursor position
	yCord : 0,								// @Number: y pixel value of current cursor position
	tipElements : ['a','abbr','acronym','span'],	// @Array: Allowable elements that can have the toolTip
	obj : Object,							// @Element: That of which you're hovering over
	tip : Object,							// @Element: The actual toolTip itself
	active : 0,								// @Number: 0: Not Active || 1: Active
	init : function(elem) {
	    elem = $(elem);
	    
		if ( !document.getElementById ||
			!document.createElement ||
			!document.getElementsByTagName ) {
			return;
		}
		var i,j;
		this.tip = document.createElement('div');
		this.tip.id = 'toolTip';
        this.tip.className = 'hovertip_wrap0';

		document.getElementsByTagName('body')[0].appendChild(this.tip);
		this.tip.style.top = '0';
		this.tip.style.visibility = 'hidden';
		var tipLen = this.tipElements.length;

        startelem = (elem)?elem:document;
		for ( i=0; i<tipLen; i++ ) {
		    var current = $(this.tipElements[i]+'.tooltip',startelem);
//			var current = startelem.getElementsByTagName(this.tipElements[i]);
			var curLen = current.length;
			for ( j=0; j<curLen; j++ ) {
			    if (current[j].title != '') {
                    current[j].mouseover(this.tipOver);
                    current[j].mouseout(this.tipOut);
                    current[j].click(this.tipOut);
                    current[j].attr('tip',current[j].title);
                    current[j].attr('title','');
                }
			}
		}
	},
	updateXY : function(e) {
        var pointer = e.mouse();

        sweetTitles.xCord = pointer.page.x;
        sweetTitles.yCord = pointer.page.y;
	},
	tipOut: function() {
		if ( window.tID ) {
			clearTimeout(tID);
		}
		if ( window.opacityID ) {
			clearTimeout(opacityID);
		}
		sweetTitles.tip.style.visibility = 'hidden';
	},
	checkNode : function() {
		var trueObj = this.obj;
		if ( this.tipElements.inArray(trueObj.nodeName.toLowerCase()) ) {
			return trueObj;
		} else {
			return trueObj.parentNode;
		}
	},
	tipOver : function(e) {
		sweetTitles.obj = this;
		sweetTitles.updateXY(e);
		sweetTitles.tipShow();
//		//tID = window.setTimeout("sweetTitles.tipShow()",sweetTitles.startTime);
	},
	tipShow : function() {		
		var scrX = Number(this.xCord);
		var scrY = Number(this.yCord);
		var tp = parseInt(scrY+15);
		var lt = parseInt(scrX+10);
		var anch = this.checkNode();

		this.tip.innerHTML = "<div class=\"hovertip_wrap1\"><div class=\"hovertip_wrap2\"><div class=\"hovertip_wrap3\">"+anch.getAttribute('tip')+"</div></div></div>";
        var ep = MochiKit.Position.positionedOffset(anch);
        var ed = getElementDimensions(anch);
		var p = getViewportPosition();
		var d = getViewportDimensions();
		if ( parseInt(d.w+p.x) < parseInt(this.tip.offsetWidth+lt) ) {
			this.tip.style.left = parseInt(lt-(this.tip.offsetWidth+30))+'px';
		} else {
			this.tip.style.left = lt+'px';
		}
		if ( parseInt(d.h+p.y) < parseInt(this.tip.offsetHeight+tp) ) {
			this.tip.style.top = parseInt(tp-(this.tip.offsetHeight+30))+'px';
		} else {
			this.tip.style.top = tp+'px';
		}
		this.tip.style.visibility = 'visible';
		$(this.tip).css('opacity', sweetTitles.endOpacity);
		//this.tipFade(10);
	},
	tipFade: function(opac) {
		var passed = parseInt(opac);
		var newOpac = parseInt(passed+10);
		if ( newOpac < sweetTitles.endOpacity ) {
			this.tip.style.opacity = '.'+newOpac;
			this.tip.style.filter = "alpha(opacity:"+newOpac+")";
			sweetTitles.tipFade(newOpac);
			//opacityID = window.setTimeout("sweetTitles.tipFade('"+newOpac+"')",5);
		} else { 
			this.tip.style.opacity = "."+sweetTitles.endOpacity;
			this.tip.style.filter = "alpha(opacity:"+sweetTitles.endOpacity+")";
		}
	}
};

//addLoadEvent(function(){sweetTitles.init()});