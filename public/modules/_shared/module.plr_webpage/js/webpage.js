 PLR_Webpage = {
    deleteRecord: function(li, recordId) {
        if (Polaris.Dataview.confirmDeleteAction(1)) {
            var $form = $(li).parents('form');
            var data = {
              '_hdnAction':'delete'
            , '_hdnDatabase': $form.find(":input[name=_hdnDatabase]").val()
            , '_hdnTable': $form.find(":input[name=_hdnTable]").val()
            , '_hdnForm': $form.find(":input[name=_hdnForm]").val()
            , '_hdnRecordID': recordId
            };
            $.postJSON(_ajaxquery, data, function(data, textStatus) {
                if (textStatus == 'success' && data.result != false) {
                    li.hide(function() {
                        li.remove();
                    });
                } else {
                    alert("An error has occured: "+data.error);
                }
            });
        }
    },
    publishRecord: function(li, recordId) {
        var $form = $(li).parents('form');
        var data = {
          '_hdnAction':'publish'
        , '_hdnProcessedByModule':'true'
        // , '_hdnDatabase': $form.find(":input[name=_hdnDatabase]").val()
        // , '_hdnTable': $form.find(":input[name=_hdnTable]").val()
        , '_hdnForm': $form.find(":input[name=_hdnForm]").val()
        , '_hdnRecordID': recordId
        };
        $.postJSON(_ajaxquery, data, function(data, textStatus) {
            if (textStatus == 'success' && data.result != false) {
                if ($(".pblog_published_value", li).val() == 1) {
                    $(".pblog_published_value", li).val(0);
                    $('.pblog_published_at', li).html('Nog niet');
                } else {
                    $(".pblog_published_value", li).val(1);
                    $('.pblog_published_at', li).html('...');
                }
            } else {
                alert("An error has occured: "+data.error);
            }
        });
    },
    _init_: function() {
        var simplemde = new SimpleMDE({
            element: $("#plrwebpage_editor")[0],
            autofocus: true,
            spellChecker: false
        });
        $('#plrwebpage_editor').on('input', function() {
            $("#btnSaveForm").toggleClass('disabled', false);
        }).focus();

        $("#btnSaveForm").click(function() {
            $("#plrwebpage_editor").val(simplemde.value());
            $("#btnSaveForm").toggleClass('disabled', true);
            //$("#plrblog_body").val( simplemde.value() );
        });

        $(".btnDelete").click(function(e) {
            e.preventDefault();
            PLR_Webpage.deleteRecord($(e.currentTarget).parents('li'), $(this).data('rec'));
        });

        $(".btnPublish").click(function(e) {
            e.preventDefault();
            PLR_Webpage.publishRecord($(e.currentTarget).parents('li'), $(this).data('rec'));
        });

        $(".pblog_status").change(function(e) {
            PLR_Webpage.publishRecord($(e.currentTarget).parents('li'), $(this).data('rec'));
        });
    }
};

$(document).ready(function() {
    $('#plrwebpage_editor').focus();
});

_loadevents_[_loadevents_.length] = PLR_Webpage._init_;
