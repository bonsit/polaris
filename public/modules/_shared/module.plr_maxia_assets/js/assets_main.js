Maxia.Assets = {};
Base.update(Maxia.Assets, {
    assetsModel: null,
    assetsData: {assets: []},
    historieModel: null,
    historieData: null,
    currentfolder: null,
    parentfolder: null,
    _prevLocatie: '',
    initialiseer: function() {
        var Self = Maxia.Assets;

        Self.initialiseerAssets();
        Self.initialiseerHistorie()
        Self.initialiseerLiveZoeken();
        Self.initialiseerModals();
    },
    refreshAssets: function(searchvalue, huidigeID) {
        var Self = Maxia.Assets;

        if (isNullOrEmpty(searchvalue))  {
            searchvalue = Maxia.getGetVar('q[]');
        }

        Self.getAssets(searchvalue, function(assets, textStatus) {
            if (textStatus == 'success') {
                Maxia.Assets._prevLocatie = null;
                $.observable(Maxia.Assets.assetsData).setProperty("assets", assets);

                if (huidigeID) {
                    Self.assetsData.assets.forEach(function(item) {
                        if (item.PLR__RECORDID == huidigeID) {
                            const div = $(".assets-view .asset[data-id=" + huidigeID + "]");
                            Maxia.Assets.refreshAsset(div);
                            Self.selectAsset(div);
                        }
                    });
                }
            } else {
                alert('De items konden niet geladen worden. Error: ' + textStatus);
            }
        });
    },
    getAssetDetails: function(asset_id, callback) {
        return $.getJSON(_servicequery, {
            'event': 'assetdetails',
            'asset_id': asset_id
        }, callback);
    },
    getAssets: function(searchvalue, callback) {
        return $.getJSON(_servicequery, {
            'event': 'assets',
            'q[]': searchvalue || ''
        }, callback);
    },
    getAssetsOverview: function(searchvalue, callback) {
        return $.getJSON(_servicequery, {
            'event': 'assetsoverview',
            'q[]': searchvalue || ''
        }, callback);
    },
    renderHelpers: {
        /**
         * Dit zijn de functies die enkele eigenschappen berekenen in onze template.
         * We geven ze door bij het koppelen van gegevens aan onze template
         * en gebruiken ze met het ~-teken, bijvoorbeeld: {:~getBezetting()}
         */
        select: function(ev, eventArgs) {
            var elem = $(ev.target);
            if (!elem.hasClass('asset-selector')) {
                elem = elem.closest('.asset-selector');
            }
            Maxia.Assets.refreshAsset(elem);
            Maxia.Assets.selectAsset(elem);
            Maxia.Assets.toggleEditableElements(false);
        },
        arrayCount() {
            return Maxia.Assets.assetsData.assets.length;
        },
        huidigeLocatie: function() {
            return huidigeLocatie;
        },
        locatieCount: function() {
            // tel de unieke locatie's
            var locaties = {};
            Maxia.Assets.assetsData.assets.forEach(function(item) {
                locaties[item.LOCATIE] = true;
            });
            return Object.keys(locaties).length;
        },
        prevLocatie: function(val) {
            if (val===undefined) {
                return Maxia.Assets._prevLocatie;
            } else {
                Maxia.Assets._prevLocatie = val;
                return "";
            }
        },
        overviewFilter: function(item, index, items) { // Helper for category filter
            // var str = this.props.catFilterString;
            // Return true if item.category contains the tagCtx.props.catFilterString string
            return item.STATUS == 'ingebruik';
            // return str ? item.category.toLowerCase().indexOf(str.toLowerCase()) !== -1 : true;
        },
    },
    selectAsset: function(elem) {
        $(".asset-selector").removeClass("selected");
        elem.toggleClass('selected');
        $(".form-details-content").toggle(elem.hasClass('selected'));
    },
    refreshAsset: function(elem) {
        const Self = Maxia.Assets;

        Maxia.Assets.getAssetDetails(elem.data('id'), function(data) {
            const item = data.result;
            elem[0].objectdata = item;

            Self.historieData = Self.historieModel(data.historie);
            $.templates("#template-assets-historie").link("#asset-historie", Self.historieData);

            if (item.STATUS == 'ingebruik') {
                $(".asset-status .asset-vrij").addClass('btn-white');
                $(".asset-status .asset-vrij").removeClass('btn-primary');
                $(".asset-status .asset-bezet").addClass('btn-danger');
                $(".asset-status .asset-bezet").removeClass('btn-white');

                $("#asset-uitlening").show();
                $("#asset-geen-uitlening").hide();
            } else {
                $(".asset-status .asset-vrij").addClass('btn-primary');
                $(".asset-status .asset-vrij").removeClass('btn-white');
                $(".asset-status .asset-bezet").addClass('btn-white');
                $(".asset-status .asset-bezet").removeClass('btn-danger');

                $("#asset-uitlening").hide();
                $("#asset-geen-uitlening").show();
            }
            $("#asset-code").html(item.ASSET_CODE);
            $("#asset-subcode").html(item.ASSET_SUBCODE).toggle(!isNullOrEmpty(item.ASSET_SUBCODE));
            $("#asset-type-container").removeClass().addClass('asset-'+item.ASSET_TYPE_NAME.toLowerCase());

            $("#asset-soort").html(item.ASSET_TYPE_NAME);
            $("#asset-ruimte").html(item.RUIMTE);
            $("#asset-locatie").html(item.LOCATIE);
            $("#asset-opmerking").text(item.OPMERKING || '');
            $("#asset-opmerking-input").val(item.OPMERKING||'');
            $("#asset-uitgeleendaan").text(item.PERSOON_NAAM||'');
            $("#asset-uitgeleendaan-input").val(item.PERSOON_NAAM||'');
            if (item.STARTDATUM_DATUM) {
                let text = item.STARTDATUM_DATUM + ' - ' + item.STARTDATUM_TIJD + ' uur';
                $("#asset-uitgeleendvanaf span").html(text);
            }
            if (item.MAX_EINDDATUM_DATUM) {
                let text = item.MAX_EINDDATUM_DATUM + ' - ' + item.MAX_EINDDATUM_TIJD + ' uur';
                if (item.OVERTIJD == 1) {
                    $("#asset-uitgeleendtot").removeClass('btn-default').addClass('btn-warning');
                } else {
                    $("#asset-uitgeleendtot").removeClass('btn-warning').addClass('btn-default');
                }
                $("#asset-uitgeleendtot span").html(text);
            } else {
                $("#asset-uitgeleendtot").removeClass('btn-primary btn-danger');
                $("#asset-uitgeleendtot span").html('');
            }

            $("#asset-uitgeleendaan_telefoon").text(item.PERSOON_TELEFOON||'- geen telefoon -');
            $("#asset-uitgeleendaan_telefoon-input").val(item.PERSOON_TELEFOON||'');
            $("#asset-uitgeleendaan_email").text(item.PERSOON_EMAIL||'- geen email -');
            $("#asset-uitgeleendaan_email-input").val(item.PERSOON_EMAIL||'');
            $("#asset-langdurig-text").text(item.LANGDURIG?'langdurig ':'');

            $("#asset-asset-opmerking").text(!isNullOrEmpty(item.ASSET_OPMERKING) ? item.ASSET_OPMERKING : '')
            .toggleClass('active', !isNullOrEmpty(item.ASSET_OPMERKING));

            $("#asset-asset-buitenwerking").toggle(item.STATUS == "inactief");

            $("#group-asset-inleveren").toggle(item.STATUS == "ingebruik");
            $("#btn-asset-uitlenen").toggle(item.STATUS == "vrij");
        });
    },
    initialiseerLiveZoeken: function() {
        const Self = Maxia.Assets;

        let searchField = $("#_hdnGlobalSearch");

        searchField
        .on('keypress', function(e) {
            if (e.which == 13) {
                e.preventDefault();
                e.stopPropagation();
            }
        })
        .on('keyup', function(e) {
            clearTimeout($(this).data('timer'));
            var search = this.value;
            if (search.length >= 1) {
                $(this).data('timer', setTimeout(function() {
                    Self.refreshAssets(searchField.val());
                }, 250));
            } else {
                Self.refreshAssets();
            }
        });
    },
    huidigeAsset: function() {
        return $(".asset-selector.selected");
    },
    toggleEditableElements: function(editmode) {
        $("#btn-asset-inleveren").prop('disabled', editmode);
        $("#btn-asset-verlengen").prop('disabled', editmode);
        $(".asset-uitlening :input").toggle(editmode);
        $(".asset-uitlening .editable").toggle(!editmode);

        $("#btn-asset-edit").toggle(!editmode);
        $("#btn-asset-save").toggle(editmode);
        $("#btn-asset-cancel").toggle(editmode);
    },
    initialiseerHistorie: function() {
        const Self = Maxia.Assets;

        this.historieModel = $.views.viewModels({
            getters: ['historie'],
        });
    },
    initialiseerAssets: function() {
        const Self = Maxia.Assets;

        $.views.helpers(Self.renderHelpers);
        $.views.converters("lower", function(val) {
            if (val)
                return val.toLowerCase();
        });
        $.views.settings.debugMode(false);

        $.templates("#template-assets-view").link("#assets-view", Self.assetsData);
        $.templates("#template-assets-overview").link("#assets-overview-content", Self.assetsData);

        Self.refreshAssets();

        $("#btn-asset-edit").on('click', function(e) {
            e.preventDefault();
            var elem = Self.huidigeAsset();
            if (!elem.length) {
                Polaris.Base.modalMessage2('U moet een item selecteren om deze actie uit te voeren.');
                return;
            }
            Self.toggleEditableElements(true);
            $("input[name='PERSOON_NAAM']").trigger('focus');
        });
        $("#btn-asset-save").on('click', function(e) {
            e.preventDefault();

            Self.saveAssetUitleenDetails();
        });
        $("#btn-asset-cancel").on('click', function(e) {
            e.preventDefault();

            var elem = Self.huidigeAsset();
            Self.toggleEditableElements(false);
            Self.refreshAsset(elem);
        });

        $("#btn-asset-uitlenen,#btn-asset-bezet").on('click', function(e) {
            e.preventDefault();
            if (Self.huidigeAsset().data("status") !== "vrij") { return; };

            $("#dlgAssetUitlenen").modal('show');
        });

        $("#btn-asset-verlengen").on('click', function(e) {
            e.preventDefault();
            if (Self.huidigeAsset().data("status") !== "ingebruik") { return; };

            $("#dlgAssetVerlengen").modal('show');
        });

        $("#btn-langdurig-asset").on('click', function(e) {
            e.preventDefault();

            var elem = Self.huidigeAsset();
            if (elem.data("status") == "ingebruik") { Polaris.Base.modalMessage2('Dit item is momenteel uitgeleend.') ; return; };
            elem.data('langdurig', 'Y');

            $("#dlgAssetUitlenen").modal('show');
        });

        $("#btn-asset-inleveren,#btn-asset-vrij").on('click', function(e) {
            e.preventDefault();
            if (Self.huidigeAsset().data("status") !== "ingebruik") { return; };

            $("#dlgAssetOntvangst").modal('show');
        });

        $(".assets-details-content").hide();
        $("#group-asset-inleveren, #btn-asset-uitlenen").hide();
    },
    initialiseerDatePicker: function(elem) {
        $(elem).datepicker({
            language: "nl",
            format: "dd-mm-yyyy",
            weekStart: 1,
            daysOfWeekHighlighted: "0,6",
            calendarWeeks: false,
            todayHighlight: false,
            todayBtn: true,
            keyboardNavigation: false,
            forceParse: false,
            keepEmptyValues: true,
            startDate: new Date()
        });
    },
    initialiseerModals: function() {
        const Self = Maxia.Assets;

        $("#btn-toon-overview").on('click', function(e) {
            e.preventDefault();
            // if button is active, show the overzicht
            if ($(this).hasClass('active')) {
                // toggle active class
                $(this).removeClass('active');

                $("#assets-view").show();
                // $("#form-main-content").show();
                $("#assets-overview").hide();


            } else {
                $(this).addClass('active');

                $("#assets-overview").show();
                // $("#form-main-content").hide();
                $("#assets-view").hide();


            }
        });

        $("#assets-view").show();
        // $("#form-main-content").show();
        $("#assets-overview").hide();

        $("#dlgAssetUitlenen").on('show.bs.modal', function(e) {
            const elem = Self.huidigeAsset();
            log(elem);
            const data = elem[0].objectdata;
            if (data) {
                Polaris.Form.initializeFormValidation("#frmAssetUitlenen");

                $("#asset-name").text(data.ASSET_TYPE_NAME + ' ' + data.ASSET_CODE);
                $("#modal-asset-opmerking").text((data.ASSETTYPE_OPMERKING||'') + ' ' + (data.ASSET_OPMERKING||''))
                .toggleClass('active', !isNullOrEmpty(data.ASSET_OPMERKING) || !isNullOrEmpty(data.ASSETTYPE_OPMERKING));

                const inclusiefDatum = (data.LANGEUITLEEN == 'Y' || elem.data('langdurig') == 'Y');
                if (inclusiefDatum) {
                    Self.initialiseerDatePicker("#asset-uitlenen-datum-picker");
                }
                $("#grp-asset-uitlenen-datum-picker").toggle(inclusiefDatum);
                $("#grp-asset-uitlenen-kolom1")
                .toggleClass('col-lg-12', inclusiefDatum)
                .toggleClass('col-lg-7', inclusiefDatum);
            }
        });

        $("#dlgAssetVerlengen").on('show.bs.modal', function(e) {
            Self.initialiseerDatePicker("#verlenging-datum");
        });

        $("#dlgAssetOntvangst").on('show.bs.modal', function(e) {
            $("#dlgAssetOntvangst input.icheck").iCheck('uncheck');
        });

        $("#dlgAssetsToevoegen").on('show.bs.modal', function(e) {
            Polaris.Form.initializeFormValidation("#frmAssetsToevoegen");

            $("#dlgAssetsToevoegen input[name=PREFIX]").val(asset_code_prefix);
        });

        $("#btn-asset-ontvangst-bewaar").on('click', function(e) {
            e.preventDefault();
            var elem = Self.huidigeAsset();
            if (elem.data("status") == "vrij") { return; };
            var data = Polaris.Form.convertToKeyValuePairs($("#frmAssetOntvangst").serializeArray());
            if (data.ONTVANGST == 'Y')  {
                data['_hdnRecordID'] = elem.data("id");
                Polaris.Ajax.postJSON(_servicequery, data, null, 'Asset is ingeleverd', function() {
                    Self.refreshAssets(null, elem.data("id"));
                });
            }
            $("#dlgAssetOntvangst").modal('hide');
        });

        $("#btn-asset-uitlenen-bewaar").on('click', function(e) {
            e.preventDefault();
            var asset = Self.huidigeAsset();
            var data = Polaris.Form.convertToKeyValuePairs($("#frmAssetUitlenen").serializeArray());
            data.EINDDATUM = $("#asset-uitlenen-datum-picker").datepicker('getFormattedDate');

            if ($("#frmAssetUitlenen").valid()) {
                if ((asset[0].objectdata.LANGEUITLEEN == 'Y' || asset.data('langdurig') == 'Y')) {
                    if (data.EINDDATUM == '') {
                        Polaris.Base.modalMessage2('U moet een datum invullen om deze actie uit te voeren.');
                        return;
                    } else {
                        data.LANGDURIG = 'Y';
                    }
                }

                data['_hdnRecordID'] = asset.data("id");

                Polaris.Ajax.postJSON(_servicequery, data, null, 'Asset is uitgeleend', function() {
                    $("#dlgAssetUitlenen").modal('hide');
                    // set val to empty for not-hidden inputs
                    $("#frmAssetUitlenen :input:not([type=hidden])").val('');
                    Self.refreshAssets(null, data['_hdnRecordID']);
                    asset.data('langdurig', 'N');
                });
            }
        });

        $("#btn-asset-verlengen-bewaar").on('click', function(e) {
            e.preventDefault();
            var elem = Self.huidigeAsset();
            var data = Polaris.Form.convertToKeyValuePairs($("#frmAssetVerlengen").serializeArray());
            data['NIEUWEDATUM'] = $("#verlenging-datum").datepicker('getFormattedDate');
            if (data['NIEUWEDATUM'] == '') {
                Polaris.Base.modalMessage2('U moet een datum kiezen om deze actie uit te voeren.');
                return;
            }
            data['_hdnRecordID'] = elem.data("id");
            Polaris.Ajax.postJSON(_servicequery, data, null, 'Asset is verlengd', function() {
                $("#dlgAssetVerlengen").modal('hide');
                $("#frmAssetVerlengen :input:not([type=hidden])").val('');
                Self.refreshAssets(null, data['_hdnRecordID']);
            });
        });

        $("#btn-cancel-assets").on('click', function(e) {
            e.preventDefault();

            var asset = Self.huidigeAsset();
            if (!asset.length) {
                Polaris.Base.modalMessage2('U moet een item selecteren om deze actie uit te voeren.');
                return;
            }

            Polaris.Base.modalDialog('Bent u zeker dat u dit item buiten werking wilt stellen?<br/> <h2>'
            + asset[0].objectdata.ASSET_TYPE_NAME + ' '
            + asset[0].objectdata.ASSET_CODE + '</h2>', {
                yes: function (dialogItself) {
                    var data = {
                        'event':'assetbuitenwerking'
                      , '_hdnProcessedByModule':'true'
                      , '_hdnRecordID': asset[0].objectdata.PLR__RECORDID
                    };
                    Polaris.Ajax.postJSON(_servicequery, data, null, 'Asset is buitenwerking gesteld', function(data) {
                        dialogItself.close();
                        Self.refreshAssets();
                    });
                },
                cancel: function (dialogItself) {
                    dialogItself.close();
                },
            });
        });

        $("#dlgAssetsToevoegen input").on('keyup', function(e) {
            const dlg = $("#dlgAssetsToevoegen");
            const aantal = $("input[name='AANTAL']", dlg).val();
            const vanaf = $("input[name='VANAF']", dlg).val();
            const prefix = $("input[name='PREFIX']", dlg).val();
            let voorbeeld = '';
            try {
                voorbeeld = 'Van ' + prefix + vanaf + ' tot en met ' + prefix + (parseInt(vanaf) + parseInt(aantal) - 1);
            } catch (error) {
                voorbeeld = 'Foutieve invoer. Controleer de velden.';
            }
            $("#asset-add-preview").text(voorbeeld);
        });
        $("#btnAssetsSave").on('click', function(e) {
            e.preventDefault();
            var dlg = $("#dlgAssetsToevoegen");
            var $form = $("#dataform");
            var data = {
                  '_hdnAction':'assetstoevoegen'
                , '_hdnProcessedByModule':'true'
                , '_hdnDatabase': $form.find(":input[name=_hdnDatabase]").val()
                , '_hdnTable': $form.find(":input[name=_hdnTable]").val()
                , '_hdnForm': $form.find(":input[name=_hdnForm]").val()
                , 'AANTAL': $("input[name='AANTAL']", dlg).val()
                , 'VANAF': $("input[name='VANAF']", dlg).val()
                , 'PREFIX': $("input[name='PREFIX']", dlg).val()
            };
            Polaris.Ajax.postJSON(_ajaxquery, data, null, 'Assets zijn toegevoegd', function() {
                $("#dlgAssetsToevoegen").modal('hide');
                Self.refreshAssets();
            });
        });
    },
    saveAssetUitleenDetails: function() {
        const Self = Maxia.Assets;

        var asset = Self.huidigeAsset();
        var dlg = $("#asset-uitlening-form");
        var data = {
            'event':'assetuitlenendetails'
            , '_hdnProcessedByModule':'true'
            , '_hdnRecordID': asset[0].objectdata.PLR__RECORDID
            , 'PERSOON_NAAM': $(":input[name='PERSOON_NAAM']", dlg).val()
            , 'PERSOON_EMAIL': $(":input[name='PERSOON_EMAIL']", dlg).val()
            , 'PERSOON_TELEFOON': $(":input[name='PERSOON_TELEFOON']", dlg).val()
            , 'OPMERKING': $(":input[name='OPMERKING']", dlg).val()
        };

        Polaris.Ajax.postJSON(_servicequery, data, null, 'Gegevens opgeslagen', function(result) {
            Self.toggleEditableElements(false);
            Self.refreshAsset(asset);
        });
    }
});

jQuery(function () {
    Maxia.Assets.initialiseer();
});
