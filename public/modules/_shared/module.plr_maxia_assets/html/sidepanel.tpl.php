{literal}<script id="template-assets-historie" type="text/x-jsrender">
    <table class="table">
        <tbody>
            {{for historie()}}
                {{if #getIndex() == 0 || #parent.data[#getIndex()-1].STARTDATUM_DATUM != STARTDATUM_DATUM}}
                <tr>
                    <td class="asset-historie-datum">{{:STARTDATUM_DATUM}} {{if EINDDATUM_DATUM != STARTDATUM_DATUM}} - {{:EINDDATUM_DATUM}}{{/if}}</td>
                </tr>
                {{/if}}
                <tr>
                    <td>
                        <p data-toggle="tooltip" title="{{:PERSOON_EMAIL}}">{{:PERSOON_NAAM}}</p>
                        <mark class="inuit">VAN</mark> {{:STARTDATUM_TIJD}} &nbsp;<mark class="inuit">TOT</mark> {{:EINDDATUM_TIJD}}
                    </td>
                </tr>
            {{else}}
            <tr>
                <td>Dit item is niet eerder uitgeleend.</td>
            </tr>
            {{/for}}
        </tbody>
    </table>
</script>{/literal}

<div class="form-details-panel">
    <div class="media-details">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="form-details-content">
                    <div class="btn-group asset-status">
                        <button id="btn-asset-vrij" class="asset-vrij font-bold btn btn-lg btn-white"
                            type="button">{$status_vrij}</button>
                        <button id="btn-asset-bezet" class="asset-bezet font-bold btn btn-lg btn-white"
                            type="button">{$status_bezet}</button>
                        {if $isfacilitybeheerder}
                        <div class="dropdown pull-right" style="display:inline">
                            <a href="#" class="btn btn-outline dropdown-toggle" data-toggle="dropdown"
                                id="media-advanced" type="button" title="Geavanceerd">{icon name="menu"}</a>
                            <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                <li><a tabindex="-1" data-toggle="modal" data-target="#dlgAssetsToevoegen" href="#"
                                        id="btn-add-assets">{icon name="add_box"} Voeg {$assettype_name}-items toe</a>
                                </li>
                                <li><a tabindex="-1" href="#" id="btn-langdurig-asset">{icon name="more_time"}
                                        {$assettype_name} langdurig uitlenen </a></li>
                                <li><a tabindex="-1" href="#" id="btn-cancel-assets">{icon name="do_not_disturb_on"}
                                        {$assettype_name} buiten gebruik </a></li>
                            </ul>
                        </div>
                        {/if}
                        <button title="Toont de uitgeleende items in lijstweergave" data-toggle="tooltip" id="btn-toon-overview" class="btn btn-lg btn-white pull-right" type="button">{icon name="approval_delegation"}</button>
                    </div>

                    <div class="tabs-container m-t">

                        <ul class="nav nav-tabs" role="tablist">
                            <li class="active"><a class="nav-link" data-toggle="tab" href="#tab-details"> Details</a></li>
                            <li><a class="nav-link" data-toggle="tab" href="#tab-historie">Historie</a></li>
                        </ul>
                        <div class="tab-content">
                            <div role="tabpanel" id="tab-details" class="tab-pane active">
                                <div class="panel-body">
                                    <div id="asset-type-container">

                                        <div id="asset-uitlening" class="asset-uitlening" style="display:none">
                                            <form id="asset-uitlening-form">
                                                <a href="#" id="btn-asset-edit" title="Wijzig persoonsgegevens" class="btn-asset-edit btn btn-xs btn-outline pull-right">{icon name="edit" size="1x"}</a>
                                                <div class="btn-group pull-right">
                                                <a href="#" id="btn-asset-save" style="display:none" class="btn-asset-save btn btn-primary btn-xs">{icon name="check" size="1x"}</a>
                                                <a href="#" id="btn-asset-cancel" style="display:none" class="btn-asset-cancel btn btn-danger btn-xs">{icon name="close" size="1x"}</a>
                                                </div>

                                                <div class="asset-uitlening-field">Momenteel <b><span id="asset-langdurig-text"></span></b>uitgeleend aan</div>
                                                <div class="asset-uitlening-field"><span id="asset-uitgeleendaan" class="asset-uitgeleendaan editable"></span> <input placeholder="Naam" id="asset-uitgeleendaan-input" name="PERSOON_NAAM" required="required" class="required" style="display:none;width:100%;margin:0;padding:0;" value="" /></div>
                                                <div class="asset-uitlening-field"><span id="asset-uitgeleendaan_telefoon" class="editable">Tel:</span><input placeholder="Telefoon" id="asset-uitgeleendaan_telefoon-input"  name="PERSOON_TELEFOON" style="display:none;width:100%;margin:0;padding:0;" value="" /></div>
                                                <div class="asset-uitlening-field"><span id="asset-uitgeleendaan_email" class="editable">Email:</span><input placeholder="Email" id="asset-uitgeleendaan_email-input" name="PERSOON_EMAIL" style="display:none;width:100%;margin:0;padding:0;" value="" /></div>
                                                <div class="asset-uitlening-field"><span id="asset-opmerking" class="editable asset-opmerking">Opmerking: Geen</span><textarea placeholder="Opmerking" id="asset-opmerking-input" name="OPMERKING" style="display:none;width:100%;margin:0;padding:0;"></textarea></div>
                                                <div class="asset-uitlening-field labelx btn btn-default xlabel-default asset-uitgeleendvanaf" id="asset-uitgeleendvanaf" data-toggle="tooltip" title="Uitgeleend vanaf"><mark class="pull-left inuit">VAN</mark> <span class="pull-right"></span></div>
                                                <div class="asset-uitlening-field labelx btn asset-uitgeleendtot" id="asset-uitgeleendtot" data-toggle="tooltip" title="Uitgeleend tot"><mark class="pull-left inuit">TOT</mark> <span class="pull-right"></span></div>
                                            </form>
                                        </div>
                                        <div id="asset-geen-uitlening" class="asset-geen-uitlening" style="display:none">
                                            <p>Item is momenteel niet uitgeleend.</p>
                                        </div>
                                        <div id="group-asset-inleveren">
                                            <button class="btn btn-success" id="btn-asset-inleveren">Inleveren</button>
                                            {if $asset_verlengbaar eq 'Y'}
                                            <button class="btn btn-default" id="btn-asset-verlengen">Verlengen</button>
                                            {/if}
                                        </div>
                                        <button class="btn btn-success" id="btn-asset-uitlenen">Uitlenen</button>
                                        </p>

                                        <hr>
                                        <p>Soort: <b><span id="asset-soort"></span></b></p>

                                        <p>Code: <b><span id="asset-code"></span></b></p>
                                        <p><span class="badge asset-subcode" id="asset-subcode"></span></p>

                                        <!-- <p>Ruimte: <span id="asset-ruimte"></span> (<span id="asset-locatie"></span>)</p> -->
                                        <div id="asset-asset-opmerking" class="asset-opmerking"></div>
                                        <div id="asset-asset-buitenwerking" style="display:none" class="asset-opmerking active">Item buitenwerking gesteld.</div>

                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" id="tab-historie" class="tab-pane">
                                <div class="panel-body">
                                    <div id="asset-historie" class="asset-historie"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {if $asset_reserveerbaar eq 'Y' and $voorlopigniet}
                    <p>&nbsp;</p>
                    <h3>Reserveringen</h3>
                    <button class="btn btn-default" type="button" id="btn-asset-reservering">Voeg reservering
                        toe...</button>
                    {/if}

                </div>
            </div>
        </div>
    </div>
</div>