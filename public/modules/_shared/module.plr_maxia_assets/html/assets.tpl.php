</form>
{include file="shared.tpl.php"}

<script type="text/javascript">var asset_type = "{$asset_type}"; var asset_code_prefix = "{$asset_code_prefix}";</script>

{literal}

<script id="template-asset-item" type="text/x-jsrender">
    <div {{if MIN_WIDTH}}style="min-width:{{:MIN_WIDTH}}"{{/if}} class="asset asset-selector asset-{{lower:ASSET_TYPE_NAME}} {{if OVERTIJD == 1}}badgecounter overtijd{{/if}} {{if LANGDURIG == 1}}badgecounter langdurig{{/if}} {{if BUITENWERKING == 'Y'}}buitenwerking{{/if}} {{:STATUS}}"
    data-id="{{:PLR__RECORDID}}"
    data-status="{{:STATUS}}"
    >
        <span class="asset-icoon">{{:ICOON}}</span>
        <span class="asset-code">{{:ASSET_CODE}}</span>
        <span class="asset-subcode-container">{{if ASSET_SUBCODE}}<span class="xasset-subcode-inset"><span class="asset-subcode-blue"></span><span class="asset-subcode">{{:ASSET_SUBCODE}}</span></span>{{/if}}</span>

        {{if LANGDURIG == 0}}
        <div class="progress-mini" data-toggle="tooltip" data-delay='400' {{if OVERTIJD == 0}}title="Nog {{:RESTERENDETIJD}} resterende tijd"{{else}}title="Moet ingeleverd worden"{{/if}}>
            <div class="progress-bar" style="width: {{:RESTERENDETIJD_PERCENTAGE}}%"></div>
        </div>
        {{/if}}
    </div>
</script>

<script id="template-assetoverview-item" type="text/x-jsrender">
    <tr style="color:#fff;" class="asset-selector asset-{{lower:ASSET_TYPE_NAME}} {{if OVERTIJD == 1}}badgecounter overtijd{{/if}} {{if LANGDURIG == 1}}badgecounter langdurig{{/if}} {{if BUITENWERKING == 'Y'}}buitenwerking{{/if}} {{:STATUS}}"
        data-link="
                {on 'click' ~select}
                {on 'dblclick' ~open}
            "
        data-id="{{:PLR__RECORDID}}"
        data-status="{{:STATUS}}"
    >
        <td><span class="asset-icoon">{{:ICOON}}</span> <b>{{:ASSET_CODE}}</b> &nbsp; <span class="badge asset-subcode">{{:ASSET_SUBCODE}}</span></td>
        <td>{{:PERSOON_NAAM}}</td>
        <td>{{:PERSOON_TELEFOON}}</td>
        <td>{{:PERSOON_EMAIL}}</td>
    </tr>
</script>

<script id="template-assets-view" type="text/x-jsrender">
    {^{for assets ~thearray=assets}}

        {{if !~prevLocatie()}}

            {{if ~huidigeLocatie() == ''}}
                <div class="asset-view-locatie-header">Locatie: <span>{{:LOCATIE_NAAM}}</span></div>
            {{/if}}

            <div class="assets-view"
                data-link="
                    {on 'click' '.asset' ~select}
                    {on 'dblclick' '.asset' ~open}
                "
            >
            {{include tmpl="#template-asset-item"/}}

        {{else ~prevLocatie() === LOCATIE}}

            {{include tmpl="#template-asset-item"/}}

        {{else}}
            </div>

            <div class="asset-view-locatie-header">Locatie: <span>{{:LOCATIE_NAAM}}</span></div>
            <div class="assets-view"
                data-link="
                    {on 'click' '.asset' ~select}
                    {on 'dblclick' '.asset' ~open}
                "
            >

            {{include tmpl="#template-asset-item"/}}

        {{/if}}

        {{:~prevLocatie(LOCATIE)}}

        {{if #getIndex() === ~thearray.length - 1}}
            </div>
        {{/if}}

    {{else}}
        <h3>Er zijn geen items beschikbaar.</h3>
        {/literal}
        {*if !empty($hasactivelocation)}<a class="btn btn-primary" data-toggle="modal" data-target="#dlgAssetsToevoegen">Voeg items toe...</a>{/if*}{literal}
    {{/for}}
</script>

<script id="template-assets-overview" type="text/x-jsrender">
    {^{for assets ~thearray=assets filter=~overviewFilter}}

        {{if !~prevLocatie()}}

            {{if ~huidigeLocatie() == ''}}
                <tr class="asset-view-locatie-header"><td colspan="4">Locatie: <span>{{:LOCATIE_NAAM}}</span></td></tr>
            {{/if}}

            {{include tmpl="#template-assetoverview-item"/}}

        {{else ~prevLocatie() === LOCATIE}}

            {{include tmpl="#template-assetoverview-item"/}}

        {{else}}

            <tr class="asset-view-locatie-header"><td colspan="4">Locatie: <span>{{:LOCATIE_NAAM}}</span></td></tr>

            {{include tmpl="#template-assetoverview-item"/}}

        {{/if}}

        {{:~prevLocatie(LOCATIE)}}

    {{else}}
        <h3>Er zijn geen items uitgeleend.</h3>
    {{/for}}
</script>

{/literal}

<div id="form-main-content" class="form-main-content">
    <div id="assets-view" class="element-detail-box"></div>
    <div id="assets-overview" class="assets-overview element-detail-box">
        <table class="table table-striped table-hover">
            <tbody id="assets-overview-content" >
            </tbody>
        </table>
    </div>
    {include file="sidepanel.tpl.php"}
</div>

{include file="modalforms.tpl.php"}

<script type="text/javascript">
	$(".asset").on('click', function() {
		var url = '/app/facility/basicform/assets/edit/690f98e1bf9aa9663a2cb2e43585181c/';
		Polaris.Form.showOverlay(url, '40vmax');
	});
</script>