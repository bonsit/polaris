Maxia.DeelnemerOntwikkeling = class {

    initialiseer() {

        $("#breadcrumb_last").html($("#volledigenaam").html());

        $("#btnDataviewImportExport").on('click', function(e) {
            e.preventDefault();
            let url = '/app/deelnemerontwikkeling/basicform/do_deelnemers/impexp/'
            Polaris.Form.showOverlay(url, '40vmax', null, {noblocking: true});
        });

        Polaris.Dataview.initializeRecordMenu();

        this.initialiseerVergoedingen();
    }

    vulMaandSelect() {
        $("fldMaandSelect").empty();
        // vul de maand select met een jaar terug tot en met de huidige maand. Met deze options: <option value="102024">oktober, 2024</option>
        for (let i = 0; i <= 12; i++) {
            let date = new Date();
            date.setMonth(date.getMonth() - i);
            let month = date.toLocaleString('nl-NL', { month: 'long' });
            let year = date.getFullYear();
            let value = date.getFullYear() + ('0' + (date.getMonth() + 1)).slice(-2);
            let option = `<option value="${value}">${month}, ${year}</option>`;
            $("#fldMaandSelect").append(option);
        }
    }

    initialiseerVergoedingen() {
        $("#btnShowGenereerVergoedingenForm").on('click', function() {
            $('#generateVergoedingenModal').modal('show');
        });

        $('#generateVergoedingenModal').on('shown.bs.modal', () => {
            this.vulMaandSelect();
        });

        $('#btnGenerateVergoedingen').on('click', this.genereerVergoedingen);
    }

    initialiseerGezondheidsOverzicht(id) {
        var ctx = document.getElementById(id).getContext('2d');

        for (var i = barChartData.labels.length; i < 15; i++) {
            barChartData.labels.push("");
        }

        window.myBar = new Chart(ctx, {
            type: 'bar',
            data: barChartData,
            options: {
                maintainAspectRatio: false,
                responsive: true,
                legend: {
                    display: true,
                    position: "left",
                    align: "start",
                    labels: {
                        boxWidth: 10,
                        fontSize: 10,
                        padding: 10
                    }
                },
                scales: {
                    xAxes: [{
                        gridLines: {
                            color: "rgba(0,0,0,0.1)",
                            zeroLineColor: 'rgba(0,0,0,0)',
                        },
                        ticks: {
                            display: true,
                            fontColor: 'rgba(0,0,0,0.4)',
                        }
                    }],
                    yAxes: [{
                        gridLines: {
                            color: "rgba(0,0,0,0.1)",
                            zeroLineColor: 'rgba(0,0,0,0)',
                        },
                        ticks: {
                            beginAtZero: true,
                            min: 0,
                            max: 10,
                            stepSize: 1,
                            fontColor: 'rgba(0,0,0,0.4)',
                            padding: 25
                        }
                    }]
                },
                title: {
                    display: false,
                    text: 'Scores van nieuw naar oud'
                },
                layout: {
                    padding: {
                        left: -10,
                        right: 0,
                        top: 0,
                        bottom: 0
                    }
                }
            }
        });
    }

    genereerVergoedingen() {
        var selectedMonth = $('#fldMaandSelect').val();
        if (selectedMonth) {

            Polaris.Ajax.postJSON('/services/json/app/deelnemerontwikkeling/const/vergoedingen_overzicht/',
                {
                    _hdnDatabase: '{$databasehash}',
                    _hdnProcessedByModule: 'true',
                    event: 'genereervergoedingen',
                    selectedMonth: selectedMonth
                },
                null,
                null,
                function(data) {
                    // Handle success
                    alert('Er zijn ' + data.result.vergoedingenInserted + ' vergoedingen gegenereerd voor de betreffende maand');
                    $('#generateVergoedingenModal').modal('hide');
                },
                function(error) {
                    alert('Er is een fout opgetreden bij het genereren van vergoedingen: ' + error);
                }
            );

        } else {
            alert('Selecteer eerst een maand');
        }
    }
};

jQuery(function () {
    Maxia.DO_instance = new Maxia.DeelnemerOntwikkeling();
    Maxia.DO_instance.initialiseer();
});