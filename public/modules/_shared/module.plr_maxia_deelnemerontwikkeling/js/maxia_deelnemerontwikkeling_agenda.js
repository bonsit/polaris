Maxia.DeelnemerOntwikkelingAgenda = class extends Maxia.DeelnemerOntwikkeling {
    calendar = null;

    initialiseer() {
        super.initialiseer();
        this.initialiseerKalender();
        Maxia.initialiseerKalenderEvents(this.calendar);
    }

    getNextWeekRange() {
        const today = new Date();

        // Bepaal de dag van de week (0 = zondag, 1 = maandag, ... 6 = zaterdag)
        const currentDay = today.getDay();

        // Bereken de verschuiving naar maandag (0 voor maandag, -1 voor zondag, etc.)
        const mondayOffset = currentDay === 0 ? -6 : 1 - currentDay;

        // Bereken maandag van de huidige week
        const currentMonday = new Date(today);
        currentMonday.setDate(today.getDate() + mondayOffset);

        // Bereken de maandag van de volgende week door 7 dagen toe te voegen aan de huidige maandag
        const nextMonday = new Date(currentMonday);
        nextMonday.setDate(currentMonday.getDate() + 7);

        // Bereken vrijdag van de volgende week door 4 dagen toe te voegen aan de volgende maandag
        const nextFriday = new Date(nextMonday);
        nextFriday.setDate(nextMonday.getDate() + 4);

        return {
            monday: nextMonday,
            friday: nextFriday
        };
    }

    checkWeekBevatActiviteiten(start, end, callback) {
        // Bouw de URL op met query parameters
        var url = '/services/json/app/deelnemerontwikkeling/const/do_agenda/?event=check_week_bevat_activiteiten';
        url += '&start=' + encodeURIComponent(start);
        url += '&end=' + encodeURIComponent(end);

        // Gebruik $.getJSON om de GET-aanroep te doen
        $.getJSON(url, function(data) {
            if (data.result !== false) {
                Polaris.Base.modalDialog('Er zijn al ' + data.result + ' activiteiten ingepland voor de volgende week. Wilt u deze overschrijven?', {
                    type: BootstrapDialog.TYPE_DANGER,
                    yes: function (dialogItself) {
                        callback(true);
                        dialogItself.close();
                    },
                    cancel: function (dialogItself) {
                        dialogItself.close();
                    },
                });
            } else {
                callback(false);
            }
        }).fail(function() {
            console.error("Fout bij het ophalen van activiteiten voor de volgende week");
            callback(false); // In geval van fout, gaan we ervan uit dat er geen activiteiten zijn
        });
    }

    activiteitToevoegen() {
        // Set the title
        $("#addAgendaActiviteitModal .modal-title").text('Activiteit toevoegen');

        $("#btnAgendaActiviteitModalDelete").hide();

        // Reset all form fields
        $('#frmAgendaActiviteitModal')[0].reset();

        // Reset hidden fields
        $('#frmAgendaActiviteitModal input[name="_hdnRecordID"]').val('');
        $('#frmAgendaActiviteitModal input[name="_hdnState"]').val('insert');
        $('#frmAgendaActiviteitModal input[name="_hdnRecordID"]').val('');
        $('#frmAgendaActiviteitModal input[name="ACTIVITEIT_ID"]').val('__auto_increment__');

        // Reset date and time fields
        $('#fldTijdstipAanvangDatum, #fldTijdstipEindDatum').val('');
        $('input[name="TIJDSTIP_AANVANG_TIJD"], input[name="TIJDSTIP_EIND_TIJD"]').val('');

        // Reset select fields
        $('#fldOntwikkelsfeer').val('').trigger('change');

        // Clear deelnemer rows
        $('#deelnemer-rijen').empty();

        // Set default values if needed
        $('#fldGroepsGrootte').val('1');

        // Show the modal
        $('#addAgendaActiviteitModal').modal('show');
    }

    activiteitBewerken(info) {
        if (info.event._def.resourceIds.length > 1) {
            Polaris.Base.modalDialog('Er is meer dan 1 onderhoud aanwezig voor deze activiteit. Deze activiteit kan niet bewerkt worden.', {
                type: BootstrapDialog.TYPE_DANGER,
                yes: function (dialogItself) {
                    dialogItself.close();
                },
            });
            return;
        }

        const resourceId = info.event._def.resourceIds[0];

        $("#addAgendaActiviteitModal .modal-title").text('Activiteit bewerken');

        $("#btnAgendaActiviteitModalDelete").show();

        $.getJSON(_servicequery+'?event=getactiviteit&id=' + resourceId, function(data) {

            if (data.result && data.result.activiteit) {
                var activiteit = data.result.activiteit;
                var deelnemers = data.result.deelnemers;

                // Populate activiteit fields
                $('#frmAgendaActiviteitModal input[name="_hdnRecordID"]').val(activiteit.PLR__RECORDID);
                $('#frmAgendaActiviteitModal input[name="_hdnState"]').val('edit');
                $('#frmAgendaActiviteitModal input[name="GEBRUIKER"]').val(activiteit.gebruiker);
                $('#frmAgendaActiviteitModal input[name="GEBRUIKERNAAM"]').val(activiteit.gebruikernaam);
                $('#frmAgendaActiviteitModal input[name="LOCATIE"]').val(activiteit.locatie);
                $('#frmAgendaActiviteitModal input[name="ACTIVITEIT_ID"]').val(activiteit.activiteit_id);

                // Format dates to 'd-m-Y'
                function formatDate(dateString) {
                    const [year, month, day] = new Date(dateString).toISOString().split('T')[0].split('-');
                    return `${day}-${month}-${year}`;
                }

                // Split datetime strings into date and time
                var startDateTime = activiteit.tijdstip_aanvang.split(' ');
                var endDateTime = activiteit.tijdstip_einde.split(' ');

                $('#fldTijdstipAanvangDatum').val(formatDate(startDateTime[0]));
                $('input[name="TIJDSTIP_AANVANG_TIJD"]').val(startDateTime[1].substr(0, 5)); // Only take HH:MM
                $('#fldTijdstipEindDatum').val(formatDate(endDateTime[0]));
                $('input[name="TIJDSTIP_EIND_TIJD"]').val(endDateTime[1].substr(0, 5)); // Only take HH:MM

                $('#fldGroepsGrootte').val(activiteit.groepsgrootte);
                $('#fldOntwikkelsfeer').val(activiteit.ontwikkelsfeer).trigger('change');

                // Clear existing deelnemer rows
                $('#deelnemer-rijen').empty();

                // Add deelnemer rows
                deelnemers.forEach(function(deelnemer) {
                    var newRow = $('#templateDeelnemerRij').clone().removeAttr('id').addClass('deelnemer-rij').show();
                    newRow.find('input[name="do_activiteit_deelnemers!_hdnState[]"]').val('edit');
                    newRow.find('input[name="do_activiteit_deelnemers!_hdnRecordID[]"]').val(deelnemer.PLR__RECORDID);
                    newRow.find('input[name="do_activiteit_deelnemers!ACTIVITEIT[]"]').val(activiteit.activiteit_id);
                    newRow.find('select[name="do_activiteit_deelnemers!DEELNEMER[]"]').val(deelnemer.deelnemer).trigger('change');
                    newRow.find('select[name="do_activiteit_deelnemers!AANWEZIGHEID_STATUS[]"]').val(deelnemer.aanwezigheid_status);

                    $('#deelnemer-rijen').append(newRow);
                });

                // Initialize Select2 for newly added rows
                // $('#deelnemer-rijen .select2').select2();
            }

            $('#addAgendaActiviteitModal').modal('show');
        });

    }

    kopieerWeek(overschrijven) {
        if (!this.calendar) {
            console.error('Calendar is not initialized');
            return;
        }

        const nextWeekRange = this.getNextWeekRange();
        const self = this; // Capture the outer 'this' context

        // Formatteer de activeStart en activeEnd naar 'DD Maand YYYY'
        const formatDate = (date) => date.toLocaleDateString('nl-NL', {
            day: '2-digit',
            month: 'long',
            year: 'numeric'
        });
        const formatDateShort = (date) => date.toLocaleDateString('nl-NL', {
            day: '2-digit',
            month: 'numeric',
            year: 'numeric'
        });

        const activeStartFormatted = formatDate(this.calendar.view.activeStart);
        const activeEndFormatted = formatDate(this.calendar.view.activeEnd);

        const startKopieerProcess = (overschrijf) => {
            Polaris.Base.modalDialog('Wilt u deze week kopiëren naar volgende week? <br><br>'
                + '<p>Maandag <b>' + activeStartFormatted + '</b> t/m vrijdag <b>' + activeEndFormatted + '</b></p>'
                + '<p>naar volgende week:</p>'
                + 'Maandag <b>' + formatDate(nextWeekRange.monday) + '</b> t/m vrijdag <b>' + formatDate(nextWeekRange.friday) + '</b>'
                + (overschrijf ? '<br><br><p class="text-danger"><b>Let op:</b> Er zijn al activiteiten ingepland voor de volgende week. Deze zullen worden overschreven.</p>' : '')
            , {
                type: BootstrapDialog.TYPE_CONFIRMATION,
                yes: function (dialogItself) {
                    var start = self.calendar.view.activeStart;
                    var end = self.calendar.view.activeEnd;
                    var startdate = start.toISOString().split('T')[0];
                    var enddate = end.toISOString().split('T')[0];

                    Polaris.Ajax.postJSON('/services/json/app/deelnemerontwikkeling/const/do_agenda/?event=kopieerweek',
                        {
                            startdate: startdate,
                            enddate: enddate,
                            overschrijf: overschrijf
                        },
                        null,
                        null,
                        function(data) {
                            dialogItself.close();
                        }
                    );
                },
                cancel: function (dialogItself) {
                    dialogItself.close();
                },
            });
        };

        this.checkWeekBevatActiviteiten(formatDateShort(nextWeekRange.monday), formatDateShort(nextWeekRange.friday), startKopieerProcess);
    }

    static formatDeelnemers(deelnemersString) {
        if (isUndefined(deelnemersString) || !deelnemersString) return '';

        const deelnemersArray = deelnemersString.split('|');
        let output = '<div class="deelnemerspopup">';

        deelnemersArray.forEach(deelnemer => {
            // if deelnemer is empty, skip
            if (!isUndefined(deelnemer)) {
                const deelnemerDetails = deelnemer.split('#');
                const aanwezig = deelnemerDetails[2] === '1' ? 'bg-primary' : '';
                output += `<div><span class='badge ${aanwezig} '>${deelnemerDetails[0]}</span> <span class='deelnemernaam'>${deelnemerDetails[1]}</span></div>\n`;
            }
        });

        output += '</div>';
        return output;
    }

    initialiseerKalender() {
        if ($('#do_agenda').length > 0) {

            // var date = new Date();
            // var d = date.getDate();
            // var m = date.getMonth();
            // var y = date.getFullYear();

            // Haal opgeslagen weergave en datum op
            var bewaardeView = Maxia.getKalenderView('customWeek');
            var bewaardeDate = Maxia.getKalenderDatum(new Date());

            // Haal weergave op uit URL-parameter indien geldig
            var paramView = Polaris.Base.getLocationVariable('view');
            var toegestaneViews = ['customDay', 'customWeek', 'customMonth', 'customListWeek'];
            // Controleer of paramView bestaat en een geldige weergave is
            if (paramView && toegestaneViews.includes(paramView)) {
                bewaardeView = paramView;
            }

            const self = this; // Capture the outer 'this' context

            this.calendar = new FullCalendar.Calendar($('#do_agenda')[0], {
                schedulerLicenseKey: '0445008747-fcs-1681420837',
                initialView: bewaardeView,
                initialDate: bewaardeDate,
                height: $(window).height() - deltaheight - 60,
                handleWindowResize: true,
                editable: true,
                locale: 'nl',
                firstDay: 1,
                weekends: false,
                allDaySlot: false,
                timeZone: 'Europe/Amsterdam',
                slotMinTime: '07:00',
                slotMaxTime: '23:00',
                eventDisplay: 'block',
                resourceOrder: 'group',
                resourceAreaWidth: '20%',
                dayPopoverFormat: { month: 'long', day: 'numeric', year: 'numeric' },
                headerToolbar: {
                    start: 'customDay,customWeek,customMonth,customListWeek', // will normally be on the left. if RTL, will be on the right
                    center: 'title',
                    end: 'activiteittoevoegen kopieerweek today,prev,next' // will normally be on the right. if RTL, will be on the left
                },
                buttonText: {
                    'today': 'Vandaag'
                },
                events: '/services/json/app/deelnemerontwikkeling/const/do_agenda/?event=events',
                views: {
                    customDay: {
                        type: 'timeGridDay',
                        buttonText: 'Dag',
                    },
                    customWeek: {
                        type: 'timeGridWeek',
                        buttonText: 'Week',
                        eventContent: function(arg) {
                            return {
                                html: '<div class="fc-event-time">' + arg.timeText + '</div><div class="fc-event-title-container">' + arg.event.title + '</div><div class="badge">' + arg.event.extendedProps.clients_count + ' / ' + arg.event.extendedProps.group_size + '</div>'
                            };
                        }
                    },
                    customMonth: {
                        type: 'dayGridMonth',
                        buttonText: 'Maand',
                        dayMaxEventRows: 8,
                    },
                    customListWeek: {
                        type: 'listWeek',
                        buttonText: 'Lijst',
                    },
                },
                customButtons: {
                    kopieerweek: {
                        text: 'Kopieer week',
                        click: () => {
                            this.kopieerWeek();
                        }
                    },
                    activiteittoevoegen: {
                        text: 'Activiteit toevoegen',
                        click: () => {
                            this.activiteitToevoegen();
                        }
                    }
                },
                datesSet: function(dateInfo) {
                    Maxia.bewaarKalenderView(dateInfo);
                    Maxia.bewaarKalenderDatum(self.calendar);
                },
                eventDidMount: function(info) {
                    let startTime = `${info.event.start.getUTCHours()}:${info.event.start.getUTCMinutes().toString().padStart(2, '0')}`;
                    let endTime = `${info.event.end.getUTCHours()}:${info.event.end.getUTCMinutes().toString().padStart(2, '0')}`;
                    let startDate = info.event.start.toLocaleDateString('nl-NL', {
                        weekday: 'long',
                        year: 'numeric',
                        month: 'long',
                        day: 'numeric'
                    });

                    $(info.el).popover({
                        placement:'bottom',
                        container:'body',
                        delay: {
                            'show':500,
                            'hide': 0
                        },
                            trigger:'hover',
                        html:true,
                        content:'<b>'+info.event.title + ' &mdash; '
                        + startDate + ' &mdash; '
                        + startTime + ' ' + endTime + '</b><br><br>'
                        + Maxia.DeelnemerOntwikkelingAgenda.formatDeelnemers(info.event.extendedProps.description)
                    });
                },
                eventClick: function(info) {
                    self.activiteitBewerken(info);
                },
                droppable: false, // this allows things to be dropped onto the calendar
                resourceGroupField: 'group',
                resourceAreaColumns: [
                    {
                      field: 'title',
                      headerContent: 'Onderhoud'
                    }
                ],
            });
            this.calendar.render();
        }
    }

};

// Change the initialization at the bottom of the file
jQuery(function () {
    log();
    Maxia.DO_instance = new Maxia.DeelnemerOntwikkelingAgenda();
    Maxia.DO_instance.initialiseer();
});