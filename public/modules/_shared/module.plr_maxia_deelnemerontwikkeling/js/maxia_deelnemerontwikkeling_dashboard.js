Maxia.DeelnemerOntwikkelingDashboard = class extends Maxia.DeelnemerOntwikkeling {
    constructor() {
        super();
        // Any additional initialization can go here
    }

    initialiseerGezondheidsMetingen(id) {
        const data = {
            // labels: [
            //     'Lichaamsfuncties',
            //     ['Mentaal','welbevinden'],
            //     'Zingeving',
            //     'Kwaliteit van leven',
            //     'Meedoen',
            //     ['Dagelijks','leven']
            // ],
            labels: [
                '',
                '',
                '',
                '',
                '',
                ''
            ],
            datasets: [{
                label: 'My Empty Dataset',
				data: [0,10],
                fill: false,
				spanGaps: false,
                backgroundColor: 'transparent',
                borderColor: 'transparent',
                pointBackgroundColor: 'transparent',
                pointBorderColor: '#fff',
                pointHoverBackgroundColor: '#fff',
                pointHoverBorderColor: 'transparent'
            }, {
                label: 'My Second Dataset',
                data: [7, 7, 7, 7, 7, 7,],
                fill: true,
                backgroundColor: 'rgba(54, 162, 235, 0.2)',
                borderColor: 'rgb(54, 162, 235)',
                pointBackgroundColor: 'rgb(54, 162, 235)',
                pointBorderColor: '#fff',
                pointHoverBackgroundColor: '#fff',
                pointHoverBorderColor: 'rgb(54, 162, 235)'
            }]
        };

        var options = {
            devicePixelRatio: 2,
            locale: 'nl',
            plugins: {
                legend: {
                    display: false,
                }
            },
            scales: {
                r: {
                    pointLabels: {
                        font: {
                            size: 14,
                        }
                    }
                },
            },
            scale: {
                reverse: false,
                ticks: {
                    beginAtZero: true,
                    min: 0,
                    max: 10,
                    stepSize: 2,
                }
            },
            responsive: true,
            maintainAspectRatio: true,
            layout: {
                padding: {
                    left: 0,
                    right: 0,
                    top: 0,
                    bottom: 0
                }
            },
            tooltips: {
                callbacks: {
                    title: (tooltipItem, data) => false
                }
            }
        };

        const config = {
            type: 'radar',
            data: data,
            options: options,
        };

        new Chart(id, config);
    }
};

Maxia.DO_instance = new Maxia.DeelnemerOntwikkelingDashboard();
Maxia.DO_instance.initialiseer();
