Maxia.DeelnemerOntwikkelingModalforms = class {
    initialiseer() {
        Polaris.Form.initializeDatePickers();
        Polaris.Form.initializeTimePickers();
        Polaris.Base.initializeSelect2($('.modal'));
        // Polaris.Base.initializeIChecks(form);

        this.initializeAgendaModal();
    }

    initializeAgendaModal() {
        // make sure the fldTijdstipEindDatum is the same as the fldTijdstipAanvangDatum
        // but only if the fldTijdstipEindDatum is empty or earlier than the fldTijdstipAanvangDatum
        $("#fldTijdstipAanvangDatum").on('change', function(e) {
            const $fldTijdstipAanvangDatum = $(e.target);
            const $fldTijdstipEindDatum = $('#fldTijdstipEindDatum');
            if ($fldTijdstipEindDatum.val() == '' || $fldTijdstipEindDatum.val() < $fldTijdstipAanvangDatum.val()) {
                $fldTijdstipEindDatum.val($fldTijdstipAanvangDatum.val());
            }
        });

        $(document).on('click', '.deelnemer-rij .btnRemoveDeelnemer', function(e) {
            const $deelnemerRij = $(this).closest('.deelnemer-rij');
            $deelnemerRij.find('input[name="do_activiteit_deelnemers!_hdnState[]"]').val('delete');
            $deelnemerRij.hide();
        });

        $("#btnAgendaAddDeelnemer").on('click', function(e) {
            const maxGroupSize = parseInt($("#fldGroepsGrootte").val(), 10) || 50;
            const currentDeelnemers = $('.deelnemer-rij').length;

            if (currentDeelnemers >= maxGroupSize) {
                alert('Het aantal deelnemers mag niet groter zijn dan de groepsgrootte');
                return;
            }
            const deelnemerRijClone = $('#templateDeelnemerRij').clone(true, true);
            deelnemerRijClone
                .find('input[name="do_activiteit_deelnemers!_hdnState[]"]').val('insert')
                .end()
                .removeAttr('id')
                .addClass('deelnemer-rij')
                .appendTo('#deelnemer-rijen')
                .show();
        });

        $("#btnAgendaActiviteitModalSave").on('click', function(e) {
            const modalform = $('#frmAgendaActiviteitModal');
            const data = modalform.serializeArray();

            Polaris.Ajax.postJSON(_servicequery, data, null, 'Activiteit opgeslagen', (result) => {
                if (!result?.error) {
                    modalform.modal('hide');       // Hide the modal after saving
                    window.location.reload();      // Refresh the window if needed
                }
            });
        });

        $("#btnAgendaActiviteitModalDelete").on('click', function(e) {

            Polaris.Dataview.confirmDeleteAction(1, function() {
                const modalform = $('#frmAgendaActiviteitModal');
                var data = modalform.serializeArray();

                data.forEach(item => {
                    if (item.name === '_hdnState') {
                        item.value = 'delete';
                    }
                });

                Polaris.Ajax.postJSON(_servicequery, data, null, 'Activiteit verwijderd', (result) => {
                    if (!result?.error) {
                        modalform.modal('hide');
                    }
                });
            });

        });

    }
}

Maxia.DO_modalforms = new Maxia.DeelnemerOntwikkelingModalforms();
Maxia.DO_modalforms.initialiseer();
