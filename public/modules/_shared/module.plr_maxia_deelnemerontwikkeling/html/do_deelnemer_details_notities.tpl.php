<div id="datalistview">
{section name=i loop=$details}
<div class="do-deelnemer-item do-detailsview-item">
    <div class="row">
        <div class="col-md-11">
            <h4>{$details[i].GEBRUIKERNAAM}</h4>

            <h5 data-toggle="tooltip" data-delay='{ldelim}"show":"500"{rdelim}' data-placement="left" data-toggle title="{$details[i].DATUMTIJD}">{$details[i].DATUMTIJD|date_format:"%d %b %Y"}
            </h5>
        </div>
        <div class="col-md-1">
            <a href="#" class="dropdown-toggle pull-right" data-toggle="dropdown">{icon name="md-more_horiz" size="2x"}</a>
            <ul class="dropdown-menu dropdown-menu-right" role="menu">
                <li class="btn-do-item-wijzig"><a tabindex="-1" data-id="{$details[i].PLR__RECORDID}" href="{$callerquery}view/{$masterrecord.PLR__RECORDID}/do_notities/edit/{$details[i].PLR__RECORDID}/">{icon name="edit_square"} {lang but_edit}</a></li>
                <li role="separator" class="divider"></li>
                <li class="btn-do-item-delete text-danger"><a tabindex="-1" class="generic-detailsview-delete-link" data-id="{$details[i].PLR__RECORDID}" href="#">{icon name="delete"} {lang but_delete}</a></li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <p>{$details[i].NOTITIE}</p>
        </div>
    </div>
</div>
{sectionelse}
<div class="row">
    <div class="col-md-12">
        {if isset($smarty.get.qd)}
        <p>{lang no_records_found}</p>
        {else}
        <p>Er is nog geen notitie vastgelegd.</p>
        {/if}
    </div>
</div>
{/section}
</div>