{include file="shared.tpl.php"}

<div class="element-detail-box do-dashboard-container">

    {if $locaties|count > 1}
    <div id="locaties" class="row locatie-items">
        <div class="col-lg-12">
            <h3>Kies je standaard locatie</h3>
        </div>
        <div class="col-lg-12">
            <div class="btn-group">
            {section name=i loop=$locaties}
            <button class="locatie-item btn btn-lg btn-default btn-outlinex {if $huidigelocatie == $locaties[i].LOCATIE_ID}active{/if}" type="button" data-name="_GRP_LOCATIE" data-value="{$locaties[i].LOCATIE_ID}" onclick="javascript:Polaris.Form.groupFilter(this)">
                <div class="locatie-item-naam">{$locaties[i].LOCATIE_NAAM}</div>
            </button>
            {/section}
            </div>
        </div>
    </div>
    {/if}

    <div class="row do-dashboard-row">
        <div class="col-lg-4 col-sm-12">

            <div class="ibox ">
                <div id="do-dashboard-gemiddelde">
                    <div class="widget do-dashboard-gemiddelde text-center">
                        {if $gemiddelde_deelnemer_cijfer.voor_komma == 0}
                        <p class="do-gemiddeld-cijfer">?</p>
                        <p>Er zijn nog geen cijfers ingevoerd</p>
                        {else}
                        <p class="do-gemiddeld-cijfer">{$gemiddelde_deelnemer_cijfer.voor_komma}<span style="font-size:0.5em;margin-left:-30px;">.{$gemiddelde_deelnemer_cijfer.na_komma}</span></p>
                        <p class="no-margins">Onze deelnemers geven zichzelf<br> gemiddeld een {$gemiddelde_deelnemer_cijfer.voor_komma}.{$gemiddelde_deelnemer_cijfer.na_komma}.</p>
                        {/if}

                    </div>
                </div>
            </div>

        </div>

        <div class="col-lg-4 col-sm-12">
            <div class="do-spinnenweb">
                <canvas id="gezondheidsmetingChart"></canvas>
            </div>
        </div>

        <div class="col-lg-4 col-sm-12">
            <div class="ibox">
                <div class="ibox-content do-dashboard-historie" id="do-dashboard-historie">
                    <div class="full-height-scroll">
                        {section name=i loop=$items}
                        <div class="historie-item">
                            <a
                                href="/app/deelnemerontwikkeling/const/do_deelnemers/view/{$items[i].DEELNEMER__RECORDID}/{$items[i].ITEMTYPE}/">
                                <div class="do-historie-titel">Nieuwe {$items[i].TYPE} toegevoegd aan
                                    {$items[i].VOLLEDIGE_NAAM}</div>
                                <div class="do-historie-datum">{$items[i].HUMANIZE_DATE} geleden</div>
                            </a>
                        </div>
                        {sectionelse}
                        <p>Nog geen gegevens beschikbaar</p>
                        {/section}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

{literal}
<script>
    jQuery(function () {
        Maxia.DO_instance.initialiseerGezondheidsMetingen('gezondheidsmetingChart');
    });
</script>{/literal}
