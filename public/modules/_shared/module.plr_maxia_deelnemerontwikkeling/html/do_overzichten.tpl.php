{include file="shared.tpl.php"}

<div class="container do-overzichten-lijst element-detail-box">
    <div class="flex-row row">
        <div class="col-md-3">
            <div class="ibox">
                <div class="ibox-content"  style="background-image:url(/branding/porthos/images/bck1.png);background-position:center 40px">
                </div>
                <div class="ibox-title flex-text">
                    <a href="{$serverroot}/app/deelnemerontwikkeling/const/vw_do_deelnemers_overzicht/" class="btn btn-xs btn-default pull-right">{icon name="list"}</a>

                    <h4>Deelnemers</h4>
                    <p>Informatie over deelnemers</p>
                    </form>
                    <form autocomplete="off" name="dataform" id="dataform" method="post" action="/app/deelnemerontwikkeling/const/vw_do_deelnemers_overzicht/?" enctype="multipart/form-data">
                        <input type="hidden" name="_hdnAction" value="plr_export">
                        <input type="hidden" name="exportresult" value="export_all">
                        <input type="hidden" name="exporttype" value="export_csv_comma">
                        <button type="submit" class="btn btn-block btn-success">Download Deelnemers <small>(.csv)</small></button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="ibox">
                <div class="ibox-content"  style="background-image:url(/branding/porthos/images/bck3.png);">
                </div>
                <div class="ibox-title flex-text">
                    <a href="{$serverroot}/app/deelnemerontwikkeling/const/vw_do_positievegezondheid/" class="btn btn-xs btn-default pull-right">{icon name="list"}</a>
                    <h4>Positieve gezondheid</h4>
                    <p>Informatie over positieve gezondheid</p>
                    </form>
                    <form autocomplete="off" name="dataform" id="dataform" method="post" action="/app/deelnemerontwikkeling/const/vw_do_positievegezondheid/?" enctype="multipart/form-data">
                        <input type="hidden" name="_hdnAction" value="plr_export">
                        <input type="hidden" name="exportresult" value="export_all">
                        <input type="hidden" name="exporttype" value="export_csv_comma">
                        <button type="submit" class="btn btn-block btn-success">Download Gezondheidsmetingen <small>(.csv)</small></button>
                    </form>
                </div>
            </div>

        </div>
        <div class="col-md-3">
            <div class="ibox">
                <div class="ibox-content"  style="background-image:url(/branding/porthos/images/bck2.png);">
                </div>
                <div class="ibox-title flex-text">
                    <a href="{$serverroot}/app/deelnemerontwikkeling/const/vw_do_vergoedingen_overzicht/" class="btn btn-xs btn-default pull-right">{icon name="list"}</a>
                    <h4>Vergoedingen</h4>
                    <p>Informatie over reis- en andere vergoedingen</p>

                    </form>
                    <form autocomplete="off" name="dataform" id="dataform" method="post" action="/app/deelnemerontwikkeling/const/vw_do_vergoedingen_overzicht/?" enctype="multipart/form-data">
                        <input type="hidden" name="_hdnAction" value="plr_export">
                        <input type="hidden" name="exportresult" value="export_all">
                        <input type="hidden" name="exporttype" value="export_csv_comma">
                        <button type="submit" class="btn btn-block btn-success">Download Vergoedigen <small>(.csv)</small></button>
                    </form>

                    <br/>

                    <button id="btnShowGenereerVergoedingenForm" type="button" class="btn btn-block btn-success">Genereer vergoedigen...</button>
                </div>
            </div>

        </div>
        <div class="col-md-3">
            <div class="ibox">
                <div class="ibox-content"  style="background-image:url(/branding/porthos/images/bck4.png);">
                </div>
                <div class="ibox-title flex-text">
                    <a href="{$serverroot}/app/deelnemerontwikkeling/const/vw_financieringscalculator/" class="btn btn-xs btn-default pull-right">{icon name="list"}</a>
                    <h4>Financiering</h4>
                    <p>Fincanciering overzichten voor gemeentes</p>

                    <form autocomplete="off" name="dataform" id="dataform" method="post" action="/app/deelnemerontwikkeling/const/vw_financieringscalculator/" enctype="multipart/form-data">
                        <input type="hidden" name="_hdnAction" value="plr_export">
                        <input type="hidden" name="exportresult" value="export_all">
                        <input type="hidden" name="exporttype" value="export_csv_comma">
                        <button type="submit" class="btn btn-block btn-success">Download Financieringscalculator <small>(.csv)</small></button>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>

{include file="modalforms.tpl.php"}