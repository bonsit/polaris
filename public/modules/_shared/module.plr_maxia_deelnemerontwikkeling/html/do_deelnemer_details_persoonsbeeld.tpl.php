<div id="datalistview">
{section name=i loop=$details}
<div class="do-deelnemer-item">
    <div class="row">
        <div class="col-md-9">
            <h4>{$details[i].GEBRUIKERNAAM}</h4>
            <h5>{$details[i].DATUMTIJD} </h5>
        </div>
        <div class="col-md-3">
            <div class="btn-group pull-right">
                <a href="{$callerquery}edit/{$item.PLR__RECORDID}/?menu=persoonsbeeld&persoonsbeeld={$details[i].PLR__RECORDID}" class="dropdown-toggle pull-right" data-toggle="dropdown">{icon name="md-more_horiz" size="2x"}</a>
                <ul class="dropdown-menu dropdown-menu-right" role="menu">
                    <li class="btn-do-item-wijzig"><a tabindex="-1" data-id="{$details[i].PLR__RECORDID}" href="{$callerquery}view/{$masterrecord.PLR__RECORDID}/do_persoonsbeeld/edit/{$details[i].PLR__RECORDID}/">{icon name="edit_square"} {lang but_edit}</a></li>
                    <li role="separator" class="divider"></li>
                    <li class="btn-do-item-delete text-danger"><a tabindex="-1" class="generic-detailsview-delete-link" data-id="{$details[i].PLR__RECORDID}" href="#">{icon name="delete"} {lang but_delete}</a></li>
                </ul>
            </div>

            {if $details[0].VERSIE}
            <div class="btn-group pull-right">
                <button data-toggle="dropdown" class="xbadge btn btn-sm btn-success btn-outline dropdown-toggle" aria-expanded="false">Versie {$details[0].VERSIE}
                    {icon name="md-arrow_drop_down" size="1x"}
                </button>
                <ul class="dropdown-menu">
                    {section name=j loop=$details[0].ALL_VERSIONS}
                        {if $details[0].ALL_VERSIONS[j] != $details[0].VERSIE}
                        <li><a class="dropdown-item" href="?menu=persoonsbeeld&version={$details[0].ALL_VERSIONS[j]}">Versie {$details[0].ALL_VERSIONS[j]}</a></li>
                        {/if}
                    {/section}
                </ul>
            </div>
            {/if}

        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <p>{$details[i].PERSOONSBEELD}</p>
        </div>
        </div>
</div>
{sectionelse}
<div class="row">
    <div class="col-md-12">
        {if isset($smarty.get.qd)}
        <p>{lang no_records_found}</p>
        {else}
        <p>Er is nog geen beeld vastgelegd.</p>
        {/if}
    </div>
</div>
{/section}
</div>
