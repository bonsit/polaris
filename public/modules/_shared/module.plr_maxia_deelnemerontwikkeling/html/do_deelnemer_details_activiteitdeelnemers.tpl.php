<div id='do_deelnemer_agenda'></div>

{literal}
<script>
    jQuery(function () {

        var calendarEl = document.getElementById('do_deelnemer_agenda');

        var calendar = new FullCalendar.Calendar(calendarEl, {
            schedulerLicenseKey: '0445008747-fcs-1681420837',
            selectable: true,
            height: 'auto',
            weekends: true,
            firstDay: 1,
            weekends: false,
            themeSystem: 'bootstrap',
            bootstrapFontAwesome: false,
            allDaySlot: false,
            buttonText: {
                prev: '',
                next: '',
            },
            initialView: 'listWeek',
            headerToolbar: {
                start: '',
                center: 'title',
                end: 'prev,next,today' // will normally be on the right. if RTL, will be on the left
            },
            buttonText: {
                'today': 'Vandaag'
            },
            navLinks: false,
            locale: 'nl-NL',
            timeZone: 'UTC',
            slotLabelFormat:
            {
                hour: 'numeric',
                minute: '2-digit',
            },
            events: {
                url: '/services/json/app/deelnemerontwikkeling/const/do_activiteit_deelnemers/',
                method: 'GET',
                extraParams: function() { // a function that returns an object
                    return {
                        event: 'events',
                        deelnemer: $("#_hdnDeelnemer").val()
                    };
                },
            },
            views: {
                listWeek: {
                    listDayFormat: {
                        year: 'numeric',
                        month: 'long',
                        day: 'numeric',
                        weekday: 'long'
                    },
                    listDaySideFormat: false,
                    eventContent: function (arg) {
                        console.log(arg);
                        //   	//return the html
                        return {
                            html: '<div class="d-flex"><div class="badge" style="background-color: ' + arg.backgroundColor + '">' + arg.event.extendedProps.clients_count + ' / ' + arg.event.extendedProps.group_size + '</div> &nbsp; <span class="fc-event-title-container">' + arg.event.title + '</span></div>'
                        };
                    }
                },
                timeGridWeek: {
                    dayHeaderContent: function (arg) {
                        var weekday = FullCalendar.formatDate(arg.date, {
                            weekday: 'short',
                            timeZone: 'UTC',
                            locale: 'nl-NL'
                        });

                        var day = FullCalendar.formatDate(arg.date, {
                            day: '2-digit',
                            //month: '2-digit',
                            timeZone: 'UTC',
                            locale: 'nl-NL'
                        });

                        return {
                            html: '<div><div class="header-weekday">' + weekday + '</div><div class="header-day">' + day + '</div></div>'
                        };
                    },
                    eventContent: function (arg) {
                        //return the html
                        return {
                            html: '<div class="fc-event-time">' + arg.timeText + '</div><div class="fc-event-title-container">' + arg.event.title + '</div><div class="badge">' + arg.event.extendedProps.clients_count + ' / ' + arg.event.extendedProps.group_size + '</div>'
                        };
                    }
                }
            },
            eventClassNames: function (arg) {
                return ['open-eventFormModal'];
            },
            eventDidMount: function (arg) {
                arg.el.setAttribute('data-event', arg.event.id);
            },
            loading: function (isLoading) {
                if (isLoading) {
                    // isLoading gives boolean value
                    //show your loader here
                    document.body.classList.add("loading");
                } else {
                    //hide your loader here
                    document.body.classList.remove("loading");
                }
            },
            moreLinkContent: function (arg) {
                return '+' + arg.num + ' meer';
            },
            viewDidMount: function () {
                // updatecalendarTitle();
            },
            dateClick: function (arg) {

            },
            select: function (arg) {

            },
            noEventsContent: function (arg) {
                return {
                    html: '<div class="event-empty-state"></div><div><h2>Sadly...</h2><h6>No events to display!</h6></div>'
                };
            }
        });

        calendar.render();

    });
</script>{/literal}