<script>
var barChartData = {
    "labels": [
        {section name=idx loop=$details}
            "{$details[idx].__CREATEDON_ORI|date_format:'%d %B, %Y'}"{if !$smarty.section.idx.last}, {/if}
        {/section}
    ],
    "datasets": [{
        "label": "Lichaamsfuncties",
        "hidden": false,
        "backgroundColor": "#ec1a3b",
        "borderColor": "#ec1a3b",
        "borderWidth": 1,
        "data": [
            {section name=idx loop=$details}
                "{$details[idx].LICHAAMSFUNCTIES}"{if !$smarty.section.idx.last}, {/if}
            {/section}
        ]
    }, {
        "label": "Mentaal Welbevinden",
        "hidden": false,
        "backgroundColor": "#00aae6",
        "borderColor": "#00aae6",
        "borderWidth": 1,
        "data": [
            {section name=idx loop=$details}
                "{$details[idx].MENTAAL_WELBEVINDEN}"{if !$smarty.section.idx.last}, {/if}
            {/section}
        ]
    }, {
        "label": "Zingeving",
        "hidden": false,
        "backgroundColor": "#803493",
        "borderColor": "#803493",
        "borderWidth": 1,
        "data": [
            {section name=idx loop=$details}
                "{$details[idx].ZINGEVING}"{if !$smarty.section.idx.last}, {/if}
            {/section}
        ]
    }, {
        "label": "Meedoen",
        "hidden": false,
        "backgroundColor": "#f37721",
        "borderColor": "#f37721",
        "borderWidth": 1,
        "data": [
            {section name=idx loop=$details}
                "{$details[idx].SOCIALE_PARTICIPATIE}"{if !$smarty.section.idx.last}, {/if}
            {/section}
        ]
    }, {
        "label": "Kwaliteit van Leven",
        "hidden": false,
        "backgroundColor": "#f89c1c",
        "borderColor": "#f89c1c",
        "borderWidth": 1,
        "data": [
            {section name=idx loop=$details}
                "{$details[idx].KWALITEIT_VAN_LEVEN}"{if !$smarty.section.idx.last}, {/if}
            {/section}
        ]
    }, {
        "label": "Dagelijks Functioneren",
        "hidden": false,
        "backgroundColor": "#8bc53f",
        "borderColor": "#8bc53f",
        "borderWidth": 1,
        "data": [
            {section name=idx loop=$details}
                "{$details[idx].DAGELIJKS_FUNCTIONEREN}"{if !$smarty.section.idx.last}, {/if}
            {/section}
        ]
    }]
};
</script>

<div class="" style="height:21vmax">
    <canvas id="gezondheidsmetingBar" ></canvas>
</div>

<br/><br/>

<div class="table-responsive">

<table class="table table-hover" style="font-size:1.1em;margin-bottom:150px;">
    <thead>
        <tr>
            <th>Datum</th>
            <th></th>
            <th>Ingevuld door</th>
            <th style="width:25%;">Notitie</th>
            <th width="25px" scope="col">
                <svg
                xmlns="http://www.w3.org/2000/svg" width="40px" height="40px" viewBox="0 0 135.26 135.26">
                <title>Lichaamsfuncties</title>
                <path d="M579.26,384.63A67.63,67.63,0,1,1,511.63,317a67.63,67.63,0,0,1,67.63,67.63Z"
                    transform="translate(-444 -317)" fill="#ed1a3b"></path>
                <path
                    d="M536.91,386h-8.44l-10.36-10.36a11,11,0,0,0-7.81-3.24h-21a.86.86,0,0,0-.85.85v6.8a.85.85,0,0,0,.85.85h17.25L496.76,403H479.13a.85.85,0,0,0-.85.85v6.8a.85.85,0,0,0,.85.85h22.6a.85.85,0,0,0,.78-.51l5-11.16,8.53,6.4,2.93,17.95a.86.86,0,0,0,.84.72h.14l6.71-1.1a.85.85,0,0,0,.7-1l-3.43-21a.93.93,0,0,0-.33-.55L511,391.86l3.48-7.83,10.22,10.22a.82.82,0,0,0,.6.25h11.6a.86.86,0,0,0,.85-.85v-6.8a.85.85,0,0,0-.85-.85Z"
                    transform="translate(-444 -317)" fill="#fff"></path>
                <path
                    d="M523.32,368.14a11.89,11.89,0,1,0-11.89-11.88,11.9,11.9,0,0,0,11.89,11.88Zm-5.63-15.68a6.79,6.79,0,1,1-1.16,3.8,6.8,6.8,0,0,1,1.16-3.8Z"
                    transform="translate(-444 -317)" fill="#fff"></path>
                </svg>
            </th>
            <th class="text-center">
                <svg
                    xmlns="http://www.w3.org/2000/svg" width="40px" height="40px" viewBox="0 0 135.26 135.26">
                    <title>Mentaal welbevinden</title>
                    <path d="M579.26,384.63A67.63,67.63,0,1,1,511.63,317a67.63,67.63,0,0,1,67.63,67.63Z"
                        transform="translate(-444 -317)" fill="#00aae7"></path>
                    <path
                        d="M487.16,363.09a.86.86,0,0,0-1.2.11l-4.35,5.23a.86.86,0,0,0-.2.62.82.82,0,0,0,.31.57l18.86,15.72v38.8a.85.85,0,0,0,.85.85h6.8a.85.85,0,0,0,.85-.85V381.76a.83.83,0,0,0-.31-.65l-21.61-18Z"
                        transform="translate(-444 -317)" fill="#fff"></path>
                    <path
                        d="M541.65,368.43l-4.36-5.23a.84.84,0,0,0-1.19-.11l-21.62,18a.85.85,0,0,0-.3.65v42.38a.85.85,0,0,0,.85.85h6.79a.85.85,0,0,0,.85-.85v-38.8l18.87-15.72a.85.85,0,0,0,.11-1.19Z"
                        transform="translate(-444 -317)" fill="#fff"></path>
                    <path
                        d="M523.52,356.16a11.9,11.9,0,1,0-11.89,11.9,11.9,11.9,0,0,0,11.89-11.9Zm-5.63,2.65A6.8,6.8,0,0,1,506.82,361a6.9,6.9,0,0,1-1.45-2.16,6.71,6.71,0,0,1-.54-2.65,6.8,6.8,0,0,1,13.6,0,6.89,6.89,0,0,1-.54,2.65Z"
                        transform="translate(-444 -317)" fill="#fff"></path>
                </svg>
            </th>
            <th class="text-center">
                <svg
                    xmlns="http://www.w3.org/2000/svg" width="40px" height="40px" viewBox="0 0 135.26 135.26">
                    <title>Zingeving</title>
                    <path d="M579.26,384.63A67.63,67.63,0,1,1,511.63,317a67.63,67.63,0,0,1,67.63,67.63Z"
                        transform="translate(-444 -317)" fill="#803594"></path>
                    <path
                        d="M527.59,356.16a11.9,11.9,0,1,0-11.9,11.9,11.91,11.91,0,0,0,11.9-11.9ZM522,358.81a6.79,6.79,0,1,1,.54-2.65,6.76,6.76,0,0,1-.54,2.65Z"
                        transform="translate(-444 -317)" fill="#fff"></path>
                    <path
                        d="M488,366.24a.48.48,0,0,0,.34-.14l8.26-8.27a4.7,4.7,0,0,0,0-6.66,4.8,4.8,0,0,0-6.29-.31L488,353.13l-1.95-2a4.78,4.78,0,0,0-3.34-1.35,4.67,4.67,0,0,0-3.32,8l8.27,8.29a.48.48,0,0,0,.34.14Zm.19-.29Z"
                        transform="translate(-444 -317)" fill="#fff"></path>
                    <path
                        d="M503.16,372H478.87a.34.34,0,0,0-.33.34v7.47a.34.34,0,0,0,.33.34h24.29a1.54,1.54,0,0,1,1.53,1.53v43a.34.34,0,0,0,.34.34h7.47a.35.35,0,0,0,.34-.34v-43a9.7,9.7,0,0,0-9.68-9.68Z"
                        transform="translate(-444 -317)" fill="#fff"></path>
                    <path
                        d="M530.09,372a11.56,11.56,0,0,0-11.54,11.55v41.1a.34.34,0,0,0,.33.34h7.48a.34.34,0,0,0,.33-.34v-41.1a3.4,3.4,0,1,1,6.8,0v18.68a.34.34,0,0,0,.34.34h7.47a.34.34,0,0,0,.34-.34V383.53A11.56,11.56,0,0,0,530.09,372Z"
                        transform="translate(-444 -317)" fill="#fff"></path>
                </svg>
            </th>
            <th class="text-center">
                <svg
                    xmlns="http://www.w3.org/2000/svg" width="40px" height="40px" viewBox="0 0 135.26 135.26">
                    <title>Meedoen</title>
                    <path d="M579.26,384.63A67.63,67.63,0,1,1,511.63,317a67.63,67.63,0,0,1,67.63,67.63Z"
                        transform="translate(-444 -317)" fill="#f47721"></path>
                    <path
                        d="M532,374.43a7.68,7.68,0,0,0-5.9,2.78l-14.49,18.45-14.51-18.48a7.73,7.73,0,0,0-5.92-2.75,7.64,7.64,0,0,0-7.61,7.65v40.78a.85.85,0,0,0,.85.85h6.8a.85.85,0,0,0,.85-.85V384.54L507,403.47a6,6,0,0,0,9.36,0l14.86-18.93v38.32a.85.85,0,0,0,.85.85h6.8a.85.85,0,0,0,.85-.85V382.08a7.66,7.66,0,0,0-7.65-7.65Z"
                        transform="translate(-444 -317)" fill="#fff"></path>
                    <path
                        d="M503.13,357.44a11.9,11.9,0,1,0-11.89,11.89,11.91,11.91,0,0,0,11.89-11.89ZM485,354.79a6.89,6.89,0,0,1,3.62-3.62,7,7,0,0,1,1.28-.39,6.72,6.72,0,0,1,2.73,0,7,7,0,0,1,1.28.39,6.78,6.78,0,0,1,4.15,6.27,6.8,6.8,0,0,1-13.59,0,6.89,6.89,0,0,1,.53-2.65Z"
                        transform="translate(-444 -317)" fill="#fff"></path>
                    <path
                        d="M532,345.54a11.9,11.9,0,1,0,11.9,11.9,11.92,11.92,0,0,0-11.9-11.9Zm0,18.69a6.8,6.8,0,0,1-6.8-6.79,6.71,6.71,0,0,1,.54-2.65,6.85,6.85,0,0,1,3.62-3.62,6.69,6.69,0,0,1,1.27-.39,6.84,6.84,0,0,1,1.37-.14,6.78,6.78,0,0,1,6.26,4.15,6.71,6.71,0,0,1,.54,2.65,6.8,6.8,0,0,1-6.8,6.79Z"
                        transform="translate(-444 -317)" fill="#fff"></path>
                </svg>
            </th>
            <th class="text-center">
                <svg
                    xmlns="http://www.w3.org/2000/svg" width="40px" height="40px" viewBox="0 0 135.26 135.26">
                    <title>Kwaliteit van leven</title>
                    <path d="M579.26,384.63A67.63,67.63,0,1,1,511.63,317a67.63,67.63,0,0,1,67.63,67.63Z"
                        transform="translate(-444 -317)" fill="#f99d1c"></path>
                    <path
                        d="M523.76,368.11A11.9,11.9,0,1,0,511.87,380a11.91,11.91,0,0,0,11.89-11.9Zm-16.7,4.8a6.68,6.68,0,0,1-.83-1,6.8,6.8,0,0,1,9.43-9.44,7.38,7.38,0,0,1,1,.84,6.12,6.12,0,0,1,.83,1,6.8,6.8,0,0,1-5.63,10.6,6.86,6.86,0,0,1-4.81-2Z"
                        transform="translate(-444 -317)" fill="#fff"></path>
                    <path
                        d="M494,373.2v-5.95a.42.42,0,0,0-.42-.42h-7.31a.42.42,0,0,0-.42.42v5.95a.42.42,0,0,0,.42.42h7.31a.42.42,0,0,0,.42-.42Z"
                        transform="translate(-444 -317)" fill="#fff"></path>
                    <path
                        d="M537.51,366.83H530.2a.42.42,0,0,0-.42.42v5.95a.42.42,0,0,0,.42.42h7.31a.43.43,0,0,0,.43-.42v-5.95a.43.43,0,0,0-.43-.42Z"
                        transform="translate(-444 -317)" fill="#fff"></path>
                    <path
                        d="M494.79,356.83a.42.42,0,0,0,.3.12.41.41,0,0,0,.3-.12l4.21-4.21a.41.41,0,0,0,0-.6l-5.17-5.17a.43.43,0,0,0-.6,0l-4.2,4.21a.42.42,0,0,0,0,.6l5.16,5.17Z"
                        transform="translate(-444 -317)" fill="#fff"></path>
                    <path
                        d="M528.22,356.83a.41.41,0,0,0,.3.12.42.42,0,0,0,.3-.12l5.17-5.17a.43.43,0,0,0,0-.6l-4.21-4.21a.43.43,0,0,0-.6,0L524,352a.43.43,0,0,0,0,.6l4.21,4.21Z"
                        transform="translate(-444 -317)" fill="#fff"></path>
                    <path
                        d="M508.89,347.35h6a.42.42,0,0,0,.42-.42v-7.31a.42.42,0,0,0-.42-.42h-6a.42.42,0,0,0-.42.42v7.31a.42.42,0,0,0,.42.42Z"
                        transform="translate(-444 -317)" fill="#fff"></path>
                    <path
                        d="M526,382.71a11.56,11.56,0,0,0-11.55,11.55v30.12a.34.34,0,0,0,.34.34h7.47a.34.34,0,0,0,.34-.34V394.26a3.4,3.4,0,0,1,6.8,0v13.33a.33.33,0,0,0,.33.33h7.47a.34.34,0,0,0,.34-.33V394.26A11.56,11.56,0,0,0,526,382.71Z"
                        transform="translate(-444 -317)" fill="#fff"></path>
                    <path
                        d="M498,382.71a11.56,11.56,0,0,1,11.55,11.55v30.12a.34.34,0,0,1-.34.34H501.7a.34.34,0,0,1-.34-.34V394.26a3.4,3.4,0,0,0-6.8,0v13.33a.33.33,0,0,1-.33.33h-7.48a.33.33,0,0,1-.33-.33V394.26A11.55,11.55,0,0,1,498,382.71Z"
                        transform="translate(-444 -317)" fill="#fff"></path>
                </svg>
            </th>
            <th class="text-center">
                <svg
                    xmlns="http://www.w3.org/2000/svg" width="40px" height="40px" viewBox="0 0 130.93 130.93">
                    <title>Dagelijks functioneren</title>
                    <path d="M576.93,384.46A65.47,65.47,0,1,1,511.46,319a65.47,65.47,0,0,1,65.47,65.46Z"
                        transform="translate(-446 -319)" fill="#8dc63f"></path>
                    <path
                        d="M544.44,379.38h-6.58a.82.82,0,0,0-.82.82v18.92h-9a7.41,7.41,0,0,0-7.41,7.4v9.87a.83.83,0,0,0,.83.82H528a.83.83,0,0,0,.83-.82v-9.05h9a7.41,7.41,0,0,0,7.41-7.4V380.2a.83.83,0,0,0-.83-.82Z"
                        transform="translate(-446 -319)" fill="#fff"></path>
                    <path
                        d="M491.82,399.12h-5.77V380.2a.83.83,0,0,0-.83-.82h-6.58a.82.82,0,0,0-.82.82v19.74a7.41,7.41,0,0,0,7.4,7.4H491v9.05a.83.83,0,0,0,.83.82h6.56a.83.83,0,0,0,.83-.82v-9.87a7.41,7.41,0,0,0-7.39-7.4Z"
                        transform="translate(-446 -319)" fill="#fff"></path>
                    <path
                        d="M491,386.78v6.58a.82.82,0,0,0,.82.82h39.48a.83.83,0,0,0,.83-.82v-6.58a.83.83,0,0,0-.83-.82H491.8a.82.82,0,0,0-.82.82Z"
                        transform="translate(-446 -319)" fill="#fff"></path>
                    <path
                        d="M493.64,363.22a11.51,11.51,0,1,0-11.51,11.51,11.53,11.53,0,0,0,11.51-11.51Zm-17,3.67a6.57,6.57,0,0,1,5.45-10.24,6.36,6.36,0,0,1,1.32.13A6.63,6.63,0,0,1,487.2,359a6.57,6.57,0,0,1-3.75,10.62,6.35,6.35,0,0,1-1.32.14,6.57,6.57,0,0,1-5.45-2.91Z"
                        transform="translate(-446 -319)" fill="#fff"></path>
                    <path
                        d="M540.8,351.71a11.51,11.51,0,1,0,11.51,11.51,11.53,11.53,0,0,0-11.51-11.51Zm2.55,17.57a6.56,6.56,0,0,1-2.55.52,6.45,6.45,0,0,1-1.33-.14,6.57,6.57,0,0,1-5.11-5.12,6.35,6.35,0,0,1-.14-1.32,6.43,6.43,0,0,1,.14-1.32,6.6,6.6,0,0,1,5.11-5.12,6.45,6.45,0,0,1,1.33-.13,6.39,6.39,0,0,1,2.55.52,6.57,6.57,0,0,1,0,12.11Z"
                        transform="translate(-446 -319)" fill="#fff"></path>
                </svg>
            </th>
        </tr>
    </thead>
    <tbody>
        {section name=idx loop=$details}
        <tr class="dr">
            <!-- Display the date formatted -->

            <td>{if $details[idx].STATUS == 2}Gemailed op <br/>{/if}
                {$details[idx].__CREATEDON_ORI|date_format:"%d-%m-%Y"}
            </td>

            <!-- Display the more actions dropdown -->
            <td class="text-centerx dropdown">
                <a class="edit_mnu jump" data-toggle="dropdown" href="/app/deelnemerontwikkeling/const/do_deelnemers/view/{$masterrecord.PLR__RECORDID}/do_positievegezondheid/edit/{$details[idx].PLR__RECORDID}/">
                {icon name="more_vert"}
                </a>
            </td>

            <!-- Display the person who filled it out -->
            <td>
                {if $details[idx].INGEVULD_DOOR == 1}
                    Deelnemer
                {else if $details[idx].INGEVULD_DOOR == 2}
                    Deelnemer en Professional
                {else if $details[idx].INGEVULD_DOOR == 3}
                    Professional
                {/if}
            </td>

            <!-- Display the note (notitie) -->
            <td>
                <a href="#" data-toggle="popover" data-trigger="hover" data-html="true" data-placement="top" data-content="{$details[idx].NOTITIE}">
                    {$details[idx].NOTITIE|truncate:100|wordwrap:30:"<br />\n"}
                </a>
            </td>

            <!-- Display the Lichaamsfuncties value -->
            <td class="text-center">
                {if $details[idx].LICHAAMSFUNCTIES && $details[idx].LICHAAMSFUNCTIES != '0'}
                    {$details[idx].LICHAAMSFUNCTIES}
                {/if}
            </td>

            <!-- Display the Mentaal Welbevinden value -->
            <td class="text-center">
                {if $details[idx].MENTAAL_WELBEVINDEN && $details[idx].MENTAAL_WELBEVINDEN != '0'}
                    {$details[idx].MENTAAL_WELBEVINDEN}
                {/if}
            </td>

            <!-- Display the Zingeving value -->
            <td class="text-center">
                {if $details[idx].ZINGEVING && $details[idx].ZINGEVING != '0'}
                    {$details[idx].ZINGEVING}
                {/if}
            </td>

            <!-- Display the Sociale Participatie value -->
            <td class="text-center">
                {if $details[idx].SOCIALE_PARTICIPATIE && $details[idx].SOCIALE_PARTICIPATIE != '0'}
                    {$details[idx].SOCIALE_PARTICIPATIE}
                {/if}
            </td>

            <!-- Display the Kwaliteit van Leven value -->
            <td class="text-center">
                {if $details[idx].KWALITEIT_VAN_LEVEN && $details[idx].KWALITEIT_VAN_LEVEN != '0'}
                    {$details[idx].KWALITEIT_VAN_LEVEN}
                {/if}
            </td>

            <!-- Display the Dagelijks Functioneren value -->
            <td class="text-center">
                {if $details[idx].DAGELIJKS_FUNCTIONEREN && $details[idx].DAGELIJKS_FUNCTIONEREN != '0'}
                    {$details[idx].DAGELIJKS_FUNCTIONEREN}
                {/if}
            </td>

        </tr>
        {/section}
    </tbody>
</table>
</div>

{literal}<script>
    jQuery(function () {
        Maxia.DO_instance = new Maxia.DeelnemerOntwikkeling();
        Maxia.DO_instance.initialiseerGezondheidsOverzicht('gezondheidsmetingBar');
    });
</script>{/literal}