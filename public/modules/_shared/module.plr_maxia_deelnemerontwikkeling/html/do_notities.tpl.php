{include file="shared.tpl.php"}
<div class="ibox-contentx">
{section name=i loop=$items}
<div class="do-deelnemer-item">
    <div class="row">
        <div class="col-md-1">
            <h4>{$items[i].GEBRUIKERNAAM}</h4>
        </div>
        <div class="col-md-2">
            <p>{$items[i].DATUMTIJD|date_format:"%e %B %Y"}</p>
            <p>{$items[i].DATUMTIJD|date_format:"%H:%m"} uur</p>
        </div>
        <div class="col-md-9 do-deelnemer-notitie">
            <h3>{$items[i].DEELNEMER_sss57}</h3>
            <p>{$items[i].NOTITIE}</p>
        </div>
    </div>
</div>
{/section}
</div>