</form>

<div class="modal inmodal fade" id="addVergoedingModal" data-itemtype="vergoeding" data-metaname="do_vergoedingen" tabindex="-1" role="dialog"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only">Sluiten</span></button>
                <h4 class="modal-title">Vergoeding toevoegen</h4>
            </div>
            <form class="form-horizontal" id="frmVergoedingModal">
                <input type="hidden" name="_hdnDatabase" value="{$databasehash}" />
                <input type="hidden" name="_hdnAction" value="plr_save" />
                <input type="hidden" name="_hdnTable" value="do_vergoedingen" />
                <input type="hidden" name="_hdnRecordID" value="" />
                <input type="hidden" name="_hdnState" value="insert" />
                <input type="hidden" name="GEBRUIKER" value="%userid%" />
                <input type="hidden" name="GEBRUIKERNAAM" value="%username%" />

                <div class="modal-body">

                    <div class="form-group">
                        <label class="col-lg-3 control-label">Prijs</label>
                        <div class="col-lg-9"><input type="number" step=".01" placeholder="Prijs"
                                class="required form-control" name="PRIJS" value="0"></div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label">Type</label>
                        <div class="col-lg-9">
                            <select class="form-control" name="TYPE_VERGOEDING">
                                <option value="1">Reiskostenvergoeding</option>
                                <option value="2">Overig</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-12 control-label">Omschrijving</label>
                        <div class="col-lg-12">
                            <textarea type="text" rows="7" placeholder="Type uw omschrijving"
                                class="required form-control" name="OMSCHRIJVING"></textarea>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger pull-left btnDelete">Verwijderen</button>
                    <button type="button" class="btn btn-white" data-dismiss="modal">Annuleren</button>
                    <button type="button" class="btn btn-primary btnSave" id="btnVergoedingModalSave">Opslaan</button>
                </div>

            </form>
        </div>
    </div>
</div>

<div class="modal inmodal fade" id="addNotitieModal" data-itemtype="notitie" data-metaname="do_notities" tabindex="-1" role="dialog"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only">Sluiten</span></button>
                <h4 class="modal-title">Notitie toevoegen</h4>
            </div>
            <form class="form-horizontal" id="frmNotitieModal">
                <input type="hidden" name="_hdnDatabase" value="{$databasehash}" />
                <input type="hidden" name="_hdnAction" value="plr_save" />
                <input type="hidden" name="_hdnTable" value="do_notities" />
                <input type="hidden" name="_hdnRecordID" value="" />
                <input type="hidden" name="_hdnState" value="insert" />
                <input type="hidden" name="GEBRUIKER" value="%userid%" />
                <input type="hidden" name="GEBRUIKERNAAM" value="%username%" />
                <div class="modal-body">
                    <div class="form-group">
                        <div class="col-lg-12"><textarea type="text" autofocus="autofocus" rows="7"
                                placeholder="Type uw notitie" class="required form-control" name="NOTITIE"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger pull-left btnDelete">Verwijderen</button>
                    <button type="button" class="btn btn-white" data-dismiss="modal">Annuleren</button>
                    <button type="button" class="btn btn-primary btnSave" id="btnNotitieModalSave">Opslaan</button>
                </div>

            </form>
        </div>
    </div>
</div>

<div class="modal inmodal fade" id="addDoelstellingModal" data-itemtype="doelstelling" data-metaname="do_doelstellingen" tabindex="-1" role="dialog"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only">Sluiten</span></button>
                <h4 class="modal-title">Doelstelling(en) toevoegen</h4>
            </div>
            <form class="form-horizontal" id="frmDoelstellingModal">
                <input type="hidden" name="_hdnAction" value="plr_save" />
                <input type="hidden" name="_hdnDatabase" value="{$databasehash}" />
                <input type="hidden" name="_hdnTable" value="do_doelstellingen" />
                <input type="hidden" name="_hdnRecordID" value="" />
                <input type="hidden" name="_hdnState" value="insert" />
                <input type="hidden" name="GEBRUIKER" value="%userid%" />
                <input type="hidden" name="GEBRUIKERNAAM" value="%username%" />
                <div class="modal-body">

                    <div class="form-group">
                        <div class="col-lg-12">
                            <textarea type="text" autofocus="autofocus" rows="7" placeholder="Type uw doelstelling"
                                class="required form-control" name="DOELSTELLING">
Lichaamsfuncties
Mentaal welbevinden
Zingeving
Kwaliteit van leven
Meedoen
Dagelijks functioneren</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-12 control-label">
                            <input type='hidden' value='N' name='AFGEROND'>
                            <input type="checkbox" class="icheck form-control" name="AFGEROND" value="Y">
                            Afgerond?</label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger pull-left btnDelete">Verwijderen</button>
                    <button type="button" class="btn btn-white" data-dismiss="modal">Annuleren</button>
                    <button type="button" class="btn btn-primary btnSave" id="btnDoelstellingModalSave">Opslaan</button>
                </div>

            </form>
        </div>
    </div>
</div>

<div class="modal inmodal fade" id="mailUitnodigingModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only">Sluiten</span></button>

                <h3 class="modal-titlex">Wil je deze gebruiker een uitnodiging sturen om een positieve gezondheidsmeting
                    in te laten vullen?</h3>
            </div>
            <form class="form-horizontal" id="frmNotitieModal">
                <input type="hidden" name="_hdnRecordID" value="" />
                <input type="hidden" name="_hdnState" value="insert" />
                <div class="modal-body">

                    <div class="form-group">
                        <div class="col-lg-12">
                            <select class="form-control" name="SOORTFORMULIER">
                                <option value="1">Generiek formulier</option>
                                <option value="2">Twee willekeurige vragen formulier</option>
                            </select>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Annuleren</button>
                    <button type="button" class="btn btn-primary btnSave" id="btnNotitieModalSave">Opslaan</button>
                </div>

            </form>
        </div>
    </div>
</div>

<div class="modal inmodal fade" id="addMetingModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only">Sluiten</span></button>
                <h4 class="modal-title">Meeting toevoegen</h4>
            </div>
            <form class="form-horizontal" id="frmNotitieModal">
                <input type="hidden" name="_hdnItemType" value="meting" />
                <input type="hidden" name="_hdnRecordID" value="" />
                <input type="hidden" name="_hdnState" value="insert" />
                <div class="modal-body">
                    <form id="save-positivehealth-frontend" name="save-positivehealth-frontend" method="post">

                        <input type="hidden" id="_wpnonce_positivehealth" name="_wpnonce_positivehealth"
                            value="8340039240"><input type="hidden" name="_wp_http_referer"
                            value="/wp-admin/admin-ajax.php?action=create_positivehealth_form_frontend">
                        <input type="hidden" name="positivehealth-id" id="positivehealth-id">
                        <input type="hidden" name="positivehealth-client" id="positivehealth-client" value="6743">

                        <div class="form-group">
                            <label class="col-lg-5 control-label">Lichaamsfuncties score</label>
                            <div class="col-lg-7"><input type="number" placeholder="0-10" class="required form-control"
                                    name="positivehealth-physical-functioning"></div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-5 control-label">Mentaal welbevinden score</label>
                            <div class="col-lg-7"><input type="number" placeholder="0-10" class="required form-control"
                                    name="positivehealth-physical-functioning"></div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-5 control-label">Zingeving score</label>
                            <div class="col-lg-7"><input type="number" placeholder="0-10" class="required form-control"
                                    name="positivehealth-physical-functioning"></div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-5 control-label">Kwaliteit van leven score</label>
                            <div class="col-lg-7"><input type="number" placeholder="0-10" class="required form-control"
                                    name="positivehealth-physical-functioning"></div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-5 control-label">Meedoen score</label>
                            <div class="col-lg-7"><input type="number" placeholder="0-10" class="required form-control"
                                    name="positivehealth-physical-functioning"></div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-5 control-label">Dagelijks functioneren score</label>
                            <div class="col-lg-7"><input type="number" placeholder="0-10" class="required form-control"
                                    name="positivehealth-physical-functioning"></div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-5 control-label">Gevoel score</label>
                            <div class="col-lg-7"><input type="number" placeholder="0-10" class="required form-control"
                                    name="positivehealth-physical-functioning"></div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-12">
                                <label class="col-lg-5 control-label">Locatie</label>
                                <select class="form-control" name="Locatie">
                                    <option value="1">Locatie1</option>
                                    <option value="2">Locatie2</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-12">
                                <select class="form-control" name="Ingevulddoor">
                                    <option value="1">Deelnemer</option>
                                    <option value="2">Deelnemer en professional</option>
                                    <option value="3">Professional</option>
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Annuleren</button>
                    <button type="button" class="btn btn-primary btnSave" id="btnMetingModalSave">Opslaan</button>
                </div>

            </form>
        </div>
    </div>
</div>

<div class="modal inmodal fade" id="addAgendaActiviteitModal" data-itemtype="activiteit" data-metaname="do_activiteiten" tabindex="-1" role="dialog"
    aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only">Sluiten</span></button>
                <h4 class="modal-title">Activiteit toevoegen</h4>
            </div>
            <form class="form-horizontal validate" id="frmAgendaActiviteitModal">
                <input type="hidden" name="_hdnDatabase" value="{$databasehash}" />
                <input type="hidden" name="event" value="do_agendaactiviteit" />
                <input type="hidden" name="_hdnProcessedByModule" value="true" />
                <input type="hidden" name="_hdnTable" value="do_activiteiten" />
                <input type="hidden" name="_hdnRecordID" value="" />
                <input type="hidden" name="_hdnState" value="insert" />
                <input type="hidden" name="GEBRUIKER" value="%userid%" />
                <input type="hidden" name="GEBRUIKERNAAM" value="%fullname%" />
                <input type="hidden" name="LOCATIE" value="{$huidigelocatie}" />
                <input type="hidden" name="ACTIVITEIT_ID" value="__auto_increment__" />

                <div class="modal-body">

                    <div class="row form-group">
                        <label class="col-xs-12 col-md-3 required col-form-label control-label" title="Startdatum">Vanaf </label>
                        <div class="col-xs-12 col-md-5">
                                <input type="text" id="fldTijdstipAanvangDatum" placeholder="Kies startdatum"
                                class="text date_input required form-control" name="TIJDSTIP_AANVANG_DATUM"
                                value="{$vandaag}" />
                        </div>
                        <div class="col-xs-12 col-md-4">
                            <input type="text" placeholder="Kies begintijd" class="required inputtime form-control"
                                name="TIJDSTIP_AANVANG_TIJD" />
                        </div>
                    </div>

                    <div class="row form-group ">
                        <label class="col-xs-12 col-md-3 required col-form-label control-label" title="Einddatum">Tot en met </label>
                        <div class="col-xs-12 col-md-5">

                            <input type="text" id="fldTijdstipEindDatum" placeholder="Kies einddatum"
                                class="text date_input required form-control" name="TIJDSTIP_EIND_DATUM"
                                value="{$vandaag}" />
                        </div>
                        <div class="col-xs-12 col-md-4">
                            <input type="text" placeholder="Kies eindtijd" class="required inputtime form-control"
                                name="TIJDSTIP_EIND_TIJD" />
                        </div>
                    </div>

                    <div class="row form-group">
                        <label class="col-xs-12 col-md-3 required col-form-label control-label" title="Groepsgrootte">Groepsgrootte </label>
                        <div class="col-sm-12 col-md-9">
                            <input required="required" min="1" step="1" type="number" id="fldGroepsGrootte" class="huisnr form-control invalid"
                                placeholder="Vul grootte in" name="GROEPSGROOTTE"
                                value="1">
                        </div>
                    </div>

                    <div class="row form-group">
                        <label class="col-xs-12 col-md-3 required col-form-label control-label" title="Ontwikkelsfeer">Ontwikkelsfeer </label>
                        <div class="col-xs-12 col-md-9">
                            <select required="required" data-width="100%" class="select2 form-control" id="fldOntwikkelsfeer"
                                name="ONTWIKKELSFEER" title="Kies ontwikkelsfeer...">
                                {section name=i loop=$ontwikkelsferen}
                                <option value="{$ontwikkelsferen[i].ontwikkelsfeer_id}">
                                    {$ontwikkelsferen[i].ontwikkelsfeer_naam}</option>
                                {/section}
                            </select>
                        </div>
                    </div>

                    <div style="display:none" id="templateDeelnemerRij" class="row form-group">
                        <input type="hidden" name="do_activiteit_deelnemers!_hdnState[]" value="__skiptemplate__" />
                        <input type="hidden" name="do_activiteit_deelnemers!_hdnRecordID[]" value="" />
                        <input type="hidden" name="do_activiteit_deelnemers!ACTIVITEIT[]" value="__parentvalue__:ACTIVITEIT_ID" />
                        <input type="hidden" name="do_activiteit_deelnemers!CLIENT_HASH[]" value="%clienthash%" />

                        <label class="col-xs-12 col-md-3 required col-form-label control-label deelnemer-label" title="Deelnemer">Deelnemer</label>
                        <div class="col-xs-5 col-md-5">
                            <select required="required" data-width="100%" class="fldDeelnemer select2xx form-control"
                                name="do_activiteit_deelnemers!DEELNEMER[]" data-placeholdertext="deelnemer...">
                                <option></option>
                                {section name=i loop=$deelnemers}
                                    <option value="{$deelnemers[i].DEELNEMER_ID}">
                                        {$deelnemers[i].VOLLEDIGE_NAAM}
                                    </option>
                                {/section}
                            </select>
                        </div>
                        <div class="col-xs-3 col-md-3">
                            <select required="required" data-width="100%" class="fldAanwezig select2xx form-control"
                                name="do_activiteit_deelnemers!AANWEZIGHEID_STATUS[]">
                                <option value="1">✓ &nbsp; Aanwezig</option>
                                <option value="2">✗ &nbsp; Afwezig</option>
                            </select>
                        </div>
                        <div class="col-xs-1 col-md-1">
                            <button type="button" class="btnRemoveDeelnemer btn btn-outline btn-primary btn-sm pull-right">{icon name="person_remove"}</button>
                        </div>
                    </div>

                    <div id="deelnemer-rijen">
                    </div>

                    <div class="row form-group">
                        <div class="col-xs-12 m-t-sm">
                            <button type="button" id="btnAgendaAddDeelnemer" class="btn btn-primary btn-sm pull-right">{icon name="person_add"}</button>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger pull-left" id="btnAgendaActiviteitModalDelete">Verwijderen</button>
                    <button type="button" class="btn btn-white" data-dismiss="modal">Annuleren</button>
                    <button type="button" class="btn btn-primary btnSave" id="btnAgendaActiviteitModalSave">Opslaan</button>
                </div>

            </form>
        </div>
    </div>
</div>

<div class="modal inmodal fade" id="generateVergoedingenModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lgx">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Genereer Maandelijkse Reiskosten</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p>1. Zorg ervoor dat alle activiteiten van de geselecteerde maand correcte, up-to-date deelnemers statussen hebben.</p>
                <p>2. Als je reeds auto-gegenereerde vergoedingen voor een maand hebt toegevoegd en je dit voor die maand opnieuw doet, worden alle hiervoor toegevoegde auto-gegenereerde vergoedingen verwijderd.</p>
                <div class="form-row">
                    <div class="form-group col">
                        <div class="d-flex">
                            <div class="dropdown bootstrap-select dropup" style="width: 100%;">
                                <select id="fldMaandSelect" class="selectpicker select2" title="Kies een maand..." data-width="100%" tabindex="-98">
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col">
                        <button type="button" class="btn btn-success" id="btnGenerateVergoedingen">Generate</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>