<?php
require_once('module.plr_executeaction.php');

class ModulePLR_UsergroupsAction extends ModulePLR_ExecuteAction {

    function __construct($moduleid, $module, $clientname=false) {
        parent::__construct($moduleid, $module);
    }

    function ProcessAction($actionid, $moduleid, $allesRecord, $parameters=false) {
        $_result = false;
        $_rowids = $allesRecord['ROWIDFILTER'] ?? false;
        $_rowid = $allesRecord['ROWID'] ?? false;
        switch($moduleid) {
        case 'usergroup_revoke':
            $_result = $this->RevokeUser($_rowid);
            break;
        default:
        break;
        }
        return $_result;
    }

    function GetUsergroupRecord($rowid) {
        global $scramble;

        $this->lastStatement = "
        SELECT * FROM plr_usergroup
        WHERE MD5(CONCAT(?,'_',CLIENTID, '_' ,USERGROUPID)) = ?
        ";
        return $this->userdatabase->GetRow($this->lastStatement, array($scramble, $rowid));
    }

    function RevokeUser($_rowid) {
        global $scramble;

        $_currentuser = $this->GetUsergroupRecord($_rowid);
        if ($_SESSION['userid'] == $_currentuser['USERGROUPID']) {
            return 'U kunt uw eigen rechten niet intrekken.';
        }
        $this->lastStatement = "
        UPDATE plr_usergroup SET accountdisabled = 'Y'
        WHERE MD5(CONCAT(?,'_',CLIENTID, '_' ,USERGROUPID)) = ?
        ";
        $this->userdatabase->Execute($this->lastStatement, array($scramble, $_rowid));
        return $this->userdatabase->Affected_Rows() > 0;
    }

}