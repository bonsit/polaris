<?php
set_time_limit(300);
require_once('_shared/module._base.php');
require_once('includes/mainactions.inc.php');

class ModulePLR_Permissions extends _BaseModule {

    function __construct($moduleid, $module) {
        parent::__construct($moduleid, $module);

        $this->processed = false;
    }

    function _DisplayPermissionsForm($form) {
        global $polaris;
        global $_GVARS;

        if ($this->outputformat == 'xhtml') {
            $this->smarty->assign('save_clientid', $_SESSION['dsg_clientid']);
            $this->smarty->assign('save_appid', $_SESSION['dsg_appid']);
            $this->smarty->assign('save_formid', $_SESSION['dsg_formid']);
            $this->smarty->assign('save_usergroupid', $_SESSION['dsg_usergroupid']);

            $this->smarty->display($form);
        }
    }

    function DisplayFormPermissions() {
        $this->_DisplayPermissionsForm('formpermissions.tpl.php');
    }

    function DisplayUserGroupPermissions() {
        $this->_DisplayPermissionsForm('usergrouppermissions.tpl.php');
    }

    function ProcessRequest($event=false) {
        global $polaris;
        global $FormpermHash;

        $this->form->database->userdb->SetFetchMode(ADODB_FETCH_ASSOC);

        $error = false;
        if (isset($_GET['action'])) {
            switch ($_GET['action']) {
                case 'plr_clients':
                    echo $this->GetPLRClients();
                    break;
                case 'plr_apps':
                    echo $this->GetPLRApps($_GET['clientid']);
                    break;
                case 'plr_forms':
                    echo $this->GetPLRForms($_GET['clientid'], $_GET['appid']);
                    break;
                case 'plr_appusergroups':
                    echo $this->GetAppUserGroups($_GET['clientid'], $_GET['appid']);
                    break;
                case 'plr_usergroupspermissions':
                    echo $this->GetPLRUsergroupPermissions($_GET['clientid'], $_GET['appid'], $_GET['usergroupid']);
                    break;
                case 'plr_formspermissions':
                    echo $this->GetPLRFormsPermissions($_GET['clientid'], $_GET['appid'], $_GET['formid']);
                    break;
                case 'plr_usergroups':
                    echo $this->GetUserGroups($_GET['clientid']);
                    break;
            }
            exit();
        } else {

            if (isset($_GET['clientid']))
                $_SESSION['dsg_clientid'] = $_GET['clientid'];
            if (isset($_GET['appid']))
                $_SESSION['dsg_appid'] = $_GET['appid'];
            if (isset($_GET['formid']))
                $_SESSION['dsg_formid'] = $_GET['formid'];
            if (isset($_GET['usergroupid']))
                $_SESSION['dsg_usergroupid'] = $_GET['usergroupid'];

            if (isset($_POST['_hdnProcessedByModule'])) {

                try {
                    $result = true;
                    if ($_POST['_hdnState'] == 'insert') {
                        $_sql = "
                        INSERT INTO plr_formpermission (CLIENTID, FORMID, USERGROUPID, PERMISSIONTYPE)
                        VALUES (?,?,?,?)
                        ";
                        $this->form->database->userdb->Execute($_sql,
                        array($_POST['clientid'], $_POST['formid'], $_POST['usergroupid'], $_POST['permissiontype']));
                    }
                    if ($_POST['_hdnState'] == 'edit') {
                        $_sql = "
                        UPDATE plr_formpermission SET PERMISSIONTYPE = ?
                        WHERE $FormpermHash = ?
                        ";
                        $this->form->database->userdb->Execute($_sql,
                        array($_POST['permissiontype'], $_POST['_hdnRecordID']));
                    }
                    if ($_POST['_hdnAction'] == 'delete') {
                        $_sql = "
                        DELETE FROM plr_formpermission
                        WHERE $FormpermHash = ?
                        ";
                        $this->form->database->userdb->Execute($_sql,
                        array($_POST['_hdnRecordID']));
                    }
                    $endresult = array('result'=>$result, 'error'=>$error);

                    echo json_encode(utf8json($endresult));
                } catch (Exception $E) {
                    $result = false;
                    $detailedmessage = $E->completemsg."\r\n".parse_backtrace(debug_backtrace());
                    $polaris->LogQuick($detailedmessage);

                    $resultArray = Array('result'=>false, 'query'=>$_SERVER['REQUEST_URI'], 'error'=>$E->msg, 'detailed_error'=>$detailedmessage);
                    echo  json_encode(utf8json($resultArray));
                }
                exit;
            }
        }
    }

    function GetPLRClients() {
        $_sqlClients = "SELECT CLIENTID, NAME FROM plr_client";
        $_clients = $this->form->database->userdb->GetAll($_sqlClients);

        // return the result as JSON
        $result = array('clients' => $_clients);
        return json_encode(utf8json($result));
    }

    function GetPLRApps($clientid) {
        $_sqlApps = "SELECT APPLICATIONID, APPLICATIONNAME FROM plr_application WHERE CLIENTID = ?";
        $_apps = $this->form->database->userdb->GetAll($_sqlApps, array($clientid));

        foreach($_apps as $_k => $_rec) {
            $_rec['APPLICATIONNAME'] = get_resource($_rec['APPLICATIONNAME'], 'NL');
            $_apps[$_k] = $_rec;
        }

        // return the result as JSON
        $result = array('apps' => $_apps);
        return json_encode(utf8json($result));
    }

    function GetPLRForms($clientid, $appid) {
        $_sqlForms = "
        SELECT DISTINCT F.CLIENTID, F.FORMID, CONCAT(F.FORMNAME,' (ID: ',F.FORMID,')') AS FORMNAME
        FROM plr_form F
        WHERE F.CLIENTID = ?
        AND F.FORMID IN (
        	SELECT MASTERFORMID
        	FROM plr_construct C
        	WHERE C.CLIENTID = F.CLIENTID AND C.APPLICATIONID = ?
        	UNION
        	SELECT DETAILFORMID
        	FROM plr_construct C
        	WHERE C.CLIENTID = F.CLIENTID AND C.APPLICATIONID = ?
        	UNION
        	SELECT NEWMENUITEMNAME
        	FROM plr_construct C
        	WHERE C.CLIENTID = F.CLIENTID AND C.APPLICATIONID = ?
        )
        ORDER BY UPPER(FORMNAME)
        ";
        $_forms = $this->form->database->userdb->GetAll($_sqlForms, array($clientid, $appid, $appid, $appid));

        foreach($_forms as $_k => $_rec) {
            $_rec['FORMNAME'] = get_resource($_rec['FORMNAME'], 'NL');
            $_forms[$_k] = $_rec;
        }

        // return the result as JSON
        $result = array('forms' => $_forms);
        return json_encode(utf8json($result));
    }

    function GetPLRFormsPermissions($clientid, $appid, $formid) {
        global $scramble;

        $_sqlFP = "
        SELECT F.CLIENTID, F.FORMID, F.FORMNAME, F.METANAME, P.USERGROUPID, P.PERMISSIONTYPE
        , U.USERGROUPNAME, U.USERORGROUP, U.DESCRIPTION, U.ACCOUNTDISABLED,
        GROUP_CONCAT(DISTINCT US.USERGROUPNAME ORDER BY US.USERGROUPNAME SEPARATOR ', ') MEMBERS
        , MD5(CONCAT(F.CLIENTID, \"_\", F.FORMID, \"_\", P.USERGROUPID, \"".$scramble."\")) as PLR__RECORDID
        FROM plr_form F
        LEFT JOIN plr_formpermission P ON F.CLIENTID = P.CLIENTID AND F.FORMID = P.FORMID
        LEFT JOIN plr_usergroup U ON F.CLIENTID = U.CLIENTID AND P.USERGROUPID = U.USERGROUPID
        LEFT JOIN plr_membership M ON F.CLIENTID = M.CLIENTID AND P.USERGROUPID = M.GROUPID
        LEFT JOIN plr_usergroup US ON F.CLIENTID = US.CLIENTID AND M.USERGROUPID = US.USERGROUPID
        WHERE F.CLIENTID = ?
        AND F.FORMID = ?
        GROUP BY U.USERGROUPID
        ORDER BY 3
        ";
        $_fp = $this->form->database->userdb->GetAll($_sqlFP, array("CLIENTID"=>$clientid, "FORMID"=>$formid));

        foreach($_fp as $_k => $_rec) {
            $_rec['FORMNAME'] = get_resource($_rec['FORMNAME'], 'NL');
            $_fp[$_k] = $_rec;
        }

        // return the result as JSON
        $result = array('formspermissions' => $_fp);

        return json_encode(utf8json($result));
    }

    function GetPLRUsergroupPermissions($clientid, $appid, $usergroupid) {
        global $polaris;
        global $scramble;

        $_sqlUGP = "
        SELECT F.CLIENTID, F.FORMID, F.FORMNAME, F.METANAME, P.USERGROUPID, P.PERMISSIONTYPE
        , U.USERGROUPNAME, U.USERORGROUP, U.DESCRIPTION, U.ACCOUNTDISABLED,
        GROUP_CONCAT(DISTINCT US.USERGROUPNAME ORDER BY US.USERGROUPNAME SEPARATOR ', ') MEMBERS
        , MD5(CONCAT(F.CLIENTID, \"_\", F.FORMID, \"_\", P.USERGROUPID, \"".$scramble."\")) as PLR__RECORDID
        FROM plr_form F
        LEFT JOIN plr_formpermission P ON F.CLIENTID = P.CLIENTID AND F.FORMID = P.FORMID
        LEFT JOIN plr_usergroup U ON F.CLIENTID = U.CLIENTID AND P.USERGROUPID = U.USERGROUPID
        LEFT JOIN plr_membership M ON F.CLIENTID = M.CLIENTID AND P.USERGROUPID = M.GROUPID
        LEFT JOIN plr_usergroup US ON F.CLIENTID = US.CLIENTID AND M.USERGROUPID = US.USERGROUPID
        WHERE F.CLIENTID = ?
        AND P.USERGROUPID = ?
        AND F.FORMID IN (
        	SELECT DISTINCT MASTERFORMID
        	FROM plr_construct C
        	WHERE C.CLIENTID = F.CLIENTID AND C.APPLICATIONID = ?
        	UNION
        	SELECT DISTINCT DETAILFORMID
        	FROM plr_construct C
        	WHERE C.CLIENTID = F.CLIENTID AND C.APPLICATIONID = ?)
        GROUP BY F.FORMID
        ORDER BY 3
        ";
        $_ugp = $this->form->database->userdb->GetAll($_sqlUGP, array($clientid, $usergroupid, $appid, $appid));
        foreach($_ugp as $_k => $_rec) {
            $_rec['FORMNAME'] = get_resource($_rec['FORMNAME'], 'NL');
            $_ugp[$_k] = $_rec;
        }

        // return the result as JSON
        $result = array('usergroupspermissions' => $_ugp);

        return json_encode(utf8json($result));
    }

    function GetUserGroups($clientid) {
        $_sql = "
        SELECT USERGROUPID
        , CONCAT(USERGROUPNAME, ' - ', IFNULL(DESCRIPTION, '<geen omschrijving>'), IF(ACCOUNTDISABLED='Y', ' (X)','')) AS USERGROUPNAME
        , USERORGROUP
        , ACCOUNTDISABLED
        FROM plr_usergroup
        WHERE CLIENTID = ?
        ORDER BY USERORGROUP ASC, USERGROUPNAME ASC
        ";

        $_ug = $this->form->database->userdb->GetAll($_sql, array($clientid));
        // return the result as JSON
        $result = array('usergroups' => $_ug);
        return json_encode(utf8json($result));
    }

    function GetAppUserGroups($clientid, $appid) {
        $_sql = "
        SELECT DISTINCT U.USERGROUPID, CONCAT(U.USERGROUPNAME, ' - ', IFNULL(U.DESCRIPTION, '<geen omschrijving>'), IF(U.ACCOUNTDISABLED='Y', ' (X)','')) AS USERGROUPNAME, U.USERORGROUP, U.ACCOUNTDISABLED
        , GROUP_CONCAT(DISTINCT US.USERGROUPNAME ORDER BY US.USERGROUPNAME SEPARATOR ', ') MEMBERS
        FROM plr_usergroup U
        LEFT JOIN plr_formpermission FP ON U.CLIENTID = FP.CLIENTID AND U.USERGROUPID = FP.USERGROUPID
        LEFT JOIN plr_membership M ON U.CLIENTID = M.CLIENTID AND FP.USERGROUPID = M.GROUPID
        LEFT JOIN plr_usergroup US ON U.CLIENTID = US.CLIENTID AND M.USERGROUPID = US.USERGROUPID
        WHERE U.CLIENTID = ?
        GROUP BY U.USERGROUPID
        ORDER BY U.USERORGROUP ASC, U.USERGROUPNAME ASC
        ";
/*

        AND FP.FORMID IN (
            SELECT DISTINCT MASTERFORMID
            FROM plr_construct C
            WHERE C.CLIENTID = U.CLIENTID AND C.APPLICATIONID = ?
            UNION
            SELECT DISTINCT DETAILFORMID
            FROM plr_construct C
            WHERE C.CLIENTID = U.CLIENTID AND C.APPLICATIONID = ?)

        $_ug = $this->form->database->userdb->GetAll($_sql, array($clientid, $appid, $appid));

 */
        $_ug = $this->form->database->userdb->GetAll($_sql, array($clientid));
        // return the result as JSON
        $result = array('usergroups' => $_ug);
        return json_encode(utf8json($result));
    }

    function ShowListView($state, $permission, $detail=false, $masterrecord=false) {
        switch ($this->moduleid) {
            case 1:
                $this->AddModuleJS(['shared.js', 'formpermissions.js']);
                $this->DisplayFormPermissions();
                break;
            case 2:
                $this->AddModuleJS(['shared.js', 'usergrouppermissions.js']);
                $this->DisplayUserGroupPermissions();
                break;
        }
    }

}