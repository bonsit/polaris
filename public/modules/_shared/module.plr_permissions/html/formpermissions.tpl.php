<link rel="stylesheet" type="text/css" media='all' href="{$serverpath}{$modulepath}css/default.css?{#BuildVersion#}" />
<script type="text/javascript">
var save_clientid = {$save_clientid|default:0};
var save_appid = {$save_appid|default:0};
var save_formid = {$save_formid|default:0};
</script>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-content">

                <div class="row">
                    <div class="col-lg-12">

                        <div class="form-group row"><label class="col-sm-2 col-form-label">Klant:</label>

                            <div class="col-sm-10">
                                <select class="select2 form-control m-b" id="plrclients"></select>
                            </div>
                        </div>
                        <div class="form-group row"><label class="col-sm-2 col-form-label">Applicatie:</label>

                            <div class="col-sm-10">
                                <select class="select2 form-control m-b" id="plrapplications"></select>
                            </div>
                        </div>
                        <div class="form-group row"><label class="col-sm-2 col-form-label">Formulier:</label>

                            <div class="col-sm-10">
                                <select class="select2 form-control m-b" id="plrforms"></select>
                            </div>
                        </div>
                        <hr>

                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-9">
                                Permissies/Acties

                                <table id="formspermissions" style="width:100%;" class="perm_listview data striped">
                                    <tbody>
                                        <tr>
                                            <td class="usergroup" style="width:20%"></td>
                                            <td class="permissions" style="width:70%"></td>
                                            <td class="actions" style="width:2%;"><div class="btn btn-warning btn-sm delbut" title="Verwijder item">Delete</div></td>
                                        </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-lg-3">
                        <div id="allmembertoggle">&nbsp;</div> Gebruikers/groepen
                            <table id="formspermissions_f" style="width:100%;" class="data striped footerscroll">
                                <tfoot>
                                    <tr>
                                        <td class="usergroup" xstyle="width:25%" colspan="2">
                                            <select id="selectusergroups">
                                            <option value="">Voeg permissie toe...</option>
                                            <optgroup class="groups" label="Groepen">
                                            </optgroup>
                                            <optgroup class="users" label="Gebruikers">
                                            </optgroup>
                                            </select>
                                        </td>
                                        <td class="permissions" sxtyle="width:75%"></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>

                    <div>
                        <div class="bodyscroll scrollarea">
                        </div>
                        <div class="xfooterscroll">
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>

