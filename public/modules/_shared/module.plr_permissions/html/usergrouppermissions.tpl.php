<link rel="stylesheet" type="text/css" media='all' href="{$serverpath}{$modulepath}css/default.css?{#BuildVersion#}" />
<script type="text/javascript">
var save_clientid = {$save_clientid|default:0};
var save_appid = {$save_appid|default:0};
var save_usergroupid = {$save_usergroupid|default:0};
</script>

<div class="row">
    <div class="col-md-6">
        <div class="form-group row">
            <div class="col-xs-12 col-md-3 text-right">
                <label class="control-label">Klant:</label>
            </div>
            <div class="col-xs-12 col-md-9">
                <select id="plrclients" class="select2"></select>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-xs-12 col-md-3 text-right">
                <label class="control-label">Applicatie:</label>
            </div>
            <div class="col-xs-12 col-md-9">
                <select id="plrapplications" class="select2"></select>
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12 col-md-3 text-right">
                <label class="control-label">Gebruikersgroep:</label>
            </div>
            <div class="col-xs-12 col-md-9">
                <select id="plrusergroups" class="select2">
                    <optgroup class="groups" label="Groepen">
                    </optgroup>
                    <optgroup class="users" label="Gebruikers">
                    </optgroup>
                </select>
            </div>
        </div>        
    </div>
    <div class="col-md-6">
        <div id="members"></div></div></div>
    </div>
</div>
<br/>
<br/>
<div class="wrapper">
    <table id="usergroupspermissions_header" style="width:100%;" class="data">
        <thead class="">
            <tr>
                <th style="width:25%">Formulieren</th>
                <th style="width:75%">Permissies/Acties</th>
            </tr>
        </thead>
    </table>
    <div class="bodyscroll scrollarea">
        <table id="usergroupspermissions" style="width:100%;" class="perm_listview data striped">
            <tbody>
                <tr>
                    <td class="form" style="width:25%"></td>
                    <td class="actions" style="width:2%;"><div class="delbut" title="Verwijder item"><i class="fa fa-cancel"></i><span>Delete</span></div></td>
                    <td class="permissions" sxtyle="width:75%"></td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="footerscroll">
        <table id="usergroupspermissions_f" style="width:100%;" class="data striped">
            <tfoot>
                <tr>
                    <td class="form" style="width:25%" colspan="2">
                        <select id="selectforms">
                        <option value="">Voeg permissie toe...</option>
                        <optgroup class="forms" label="Formulieren">
                        </optgroup>
                        </select>
                    </td>
                    <td class="permissions" sxtyle="width:75%"></td>
                </tr>
            </tfoot>
        </table>
    </div>
</div>
