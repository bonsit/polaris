var PolarisDesigner = {};

PolarisDesigner.SharedLib = {
    plr_clients: null,
    plr_apps: null,
    plr_usergroups: null,
    plr_formspermissions: null,
    plr_usergroupspermissions: null,

    makePermissionBar: function(permid, permissiontype) {
        permissiontype = parseInt(permissiontype);
        _hidden = (permissiontype & 1)?"checked=\"checked\"":"";
        _select = (permissiontype & 2)?"checked=\"checked\"":"";
        _insert = (permissiontype & 4)?"checked=\"checked\"":"";
        _update = (permissiontype & 8)?"checked=\"checked\"":"";
        _delete = (permissiontype & 16)?"checked=\"checked\"":"";
        _import = (permissiontype & 32)?"checked=\"checked\"":"";
        _export = (permissiontype & 64)?"checked=\"checked\"":"";
        _clone = (permissiontype & 128)?"checked=\"checked\"":"";
        _search = (permissiontype & 256)?"checked=\"checked\"":"";

        var html;
        html =  "<input type=\"hidden\" name=\"PERMID\" value=\""+permid+"\" />";
        html += "<input type=\"checkbox\" name=\"SELECT\" "+(_select) + " value=\"2\" /> Select &nbsp;";
        html += "<input type=\"checkbox\" name=\"INSERT\" "+(_insert) + " value=\"4\" /> Insert &nbsp;";
        html += "<input type=\"checkbox\" name=\"UPDATE\" "+(_update) + " value=\"8\" /> Update &nbsp;";
        html += "<input type=\"checkbox\" name=\"DELETE\" "+(_delete) + " value=\"16\" /> Delete &nbsp;";
        html += "<input type=\"checkbox\" name=\"IMPORT\" "+(_import) + " value=\"32\" /> Import &nbsp;";
        html += "<input type=\"checkbox\" name=\"EXPORT\" "+(_export) + " value=\"64\" /> Export &nbsp;";
        html += "<input type=\"checkbox\" name=\"CLONE\" "+(_clone) + " value=\"128\" /> Clone &nbsp;";
        html += "<input type=\"checkbox\" name=\"SEARCH\" "+(_search) + " value=\"256\" /> Search ";
        return html;
    },
    getPermissions: function(row) {
        var _select = parseInt(row.find("input[name=SELECT]:checked").val() || 0);
        var _insert = parseInt(row.find("input[name=INSERT]:checked").val() || 0);
        var _update = parseInt(row.find("input[name=UPDATE]:checked").val() || 0);
        var _delete = parseInt(row.find("input[name=DELETE]:checked").val() || 0);
        var _import = parseInt(row.find("input[name=IMPORT]:checked").val() || 0);
        var _export = parseInt(row.find("input[name=EXPORT]:checked").val() || 0);
        var _clone = parseInt(row.find("input[name=CLONE]:checked").val() || 0);
        var _search = parseInt(row.find("input[name=SEARCH]:checked").val() || 0);
        var result = _select + _insert + _update + _delete + _import + _export + _clone + _search;
        return result;
    },
	getClients: function(callback) {
		$.getJSON(_servicequery, {
			'event': 'plr_clients'
		},
		function(data, textStatus) {
			if (textStatus == 'success') {
				PolarisDesigner.SharedLib.plr_clients = data;
				if (callback) callback(data);
			} else {
				console.log('Geen plr_clients gevonden: ' + textStatus);
			}
		});
	},
	getApps: function(clientid, callback) {
		$.getJSON(_servicequery, {
			'event': 'plr_apps',
			'clientid': clientid
		},
		function(data, textStatus) {
			if (textStatus == 'success') {
				PolarisDesigner.SharedLib.plr_apps = data;
				if (callback) callback(data);
			} else {
				console.log('Geen plr_apps gevonden: ' + textStatus);
			}
		});
	},
	getForms: function(clientid, appid, callback) {
		$.getJSON(_servicequery, {
			'event': 'plr_forms',
			'clientid': clientid,
			'appid': appid
		},
		function(data, textStatus) {
			if (textStatus == 'success') {
				PolarisDesigner.SharedLib.plr_forms = data;
				if (callback) callback(data);
			} else {
				console.log('Geen plr_forms gevonden: ' + textStatus);
			}
		});
	},
	getAppUserGroups: function(clientid, appid, callback) {
		$.getJSON(_servicequery, {
			'event': 'plr_appusergroups',
			'clientid': clientid,
			'appid': appid
		},
		function(data, textStatus) {
			if (textStatus == 'success') {
				PolarisDesigner.SharedLib.plr_usergroups = data;
				if (callback) callback(data);
			} else {
				console.log('Geen plr_clients gevonden: ' + textStatus);
			}
		});
	},
	getUserGroups: function(clientid, callback) {
		$.getJSON(_servicequery, {
			'event': 'plr_usergroups',
			'clientid': clientid
		},
		function(data, textStatus) {
			if (textStatus == 'success') {
				PolarisDesigner.SharedLib.plr_usergroups = data;
				if (callback) callback(data);
			} else {
				console.log('Geen plr_clients gevonden: ' + textStatus);
			}
		});
	},
	getFormsPermissions: function(clientid, appid, formid, callback) {
		$.getJSON(_servicequery, {
			'event': 'plr_formspermissions',
			'clientid': clientid,
			'appid': appid,
			'formid': formid
		},
		function(data, textStatus) {
			if (textStatus == 'success') {
				PolarisDesigner.SharedLib.plr_formspermissions = data;
				if (callback) callback(data);
			} else {
				console.log('Geen plr_formpermissions gevonden: ' + textStatus);
			}
		});
	},
	getUsergroupsPermissions: function(clientid, appid, usergroupid, callback) {
		$.getJSON(_servicequery, {
			'event': 'plr_usergroupspermissions',
			'clientid': clientid,
			'appid': appid,
			'usergroupid': usergroupid
		},
		function(data, textStatus) {
			if (textStatus == 'success') {
				PolarisDesigner.SharedLib.plr_usergroupspermissions = data;
				if (callback) callback(data);
			} else {
				console.log('Geen plr_usergroupspermissions gevonden: ' + textStatus);
			}
		});
	},
    _savePermission: function(params, row, callback) {
		var Self = PolarisDesigner.UserGroupPermissions;

        row.append($("<img id=\"imgwait\" />").attr('src', _serverroot + '/images/wait.gif'));
        $.postJSON(_servicequery, params,
        function(data, textStatus) {
            if (data.result != false) {
                if (callback) callback();
            } else {
        		Polaris.Base.errorDialog(data.error, data.detailed_error);
            }
            $("#imgwait").delay(500).queue(function() {
                $(this).remove();
            });
        });
    },
    updatePermission: function(elem) {
		var Self = PolarisDesigner.SharedLib;

        var $row = $(elem).parents("tr");
        var recid = $row.find("input[name=PERMID]").val();
        var permissiontype = Self.getPermissions($row);
        var params = {
            '_hdnAction': 'save',
            '_hdnState': 'edit',
            '_hdnProcessedByModule': 'true',
            '_hdnRecordID': recid,
            'permissiontype': permissiontype
        };

        PolarisDesigner.SharedLib._savePermission(params, $row);
    },
    deletePermission: function(elem) {
		var Self = PolarisDesigner.SharedLib;

        var $row = $(elem).parents("tr");

        if (confirm("Wilt u dit permissie-item verwijderen?")) {
            var recid = $row.find("input[name=PERMID]").val();
            var params = {
                '_hdnAction': 'delete',
                '_hdnProcessedByModule': 'true',
                '_hdnRecordID': recid
            };
            Self._savePermission(params, $row, function() {
                $row.fadeOut(500);
            });
        } else {
            $row.removeClass('removeattention');
        }
    },
    initializeListView: function(listview) {
        $("tbody", listview).on('click', function(e) {
            if (e.target.tagName == 'INPUT' && e.target.type == 'checkbox') {
                $(document).trigger('updatePermission', [e.target]);
            }
        });
        $('tbody tr', listview).on('mouseover mouseout', function(event) {
            if (event.type == 'mouseover') {
                // do something on mouseover
                $(".delbut",this).addClass('hover');
            } else {
                // do something on mouseout
                $(".delbut",this).removeClass('hover');
            }
        });
        $('tbody .delbut', listview).on('mouseover mouseout', function(event) {
            if (event.type == 'mouseover') {
                // do something on mouseover
                $(this).parents("tr").addClass('hover');
            } else {
                // do something on mouseout
                $(this).parents("tr").removeClass('hover');
            }
        });

        $(".delbut").on("click", function(e) {
            $(document).trigger('deletePermission', [e.target]);
        });

        $("#allmembertoggle").on('click', function() {
            $(this).toggleClass('open');
            if ($(this).hasClass('open')) {
                $(".membertoggle").addClass('open');
                $(".members").fadeIn();
            } else {
                $(".membertoggle").removeClass('open');
                $(".members").fadeOut();
            }
        });
        $(".membertoggle").on('click', function() {
            $(this).toggleClass('open');

            if ($(this).hasClass('open'))
                $(this).parent().find(".members").fadeIn();
            else
                $(this).parent().find(".members").fadeOut();
        });
    }
}