PolarisDesigner.FormPermissions = {
    plr_clients: null,
    plr_apps: null,
    initialize: function() {
		var Self = PolarisDesigner.FormPermissions;

		Self.htmlFormPermissions = $('#formspermissions tbody');

		//directive to render the template
		var directivesPermissions = {
			'tr': {
				'fp<-formspermissions': {
					'td.usergroup': function(arg) {
					    if (arg.item.USERORGROUP == 'USER')
    					    return arg.item.USERGROUPNAME;
    					else
    					    return '<b>' + arg.item.USERGROUPNAME + '</b>'+' <span class="membertoggle"><i class="fa fa-caret-down"></i></span> <br/><div class="members">'+arg.item.MEMBERS.replace(/,/g, '<br>')+'</div>';
					},
					'td.permissions': function(arg) {
						return PolarisDesigner.SharedLib.makePermissionBar(arg.item.PLR__RECORDID, arg.item.PERMISSIONTYPE);
					}
				}
			}
		};
		Self.templatePermissions = Self.htmlFormPermissions.compile(directivesPermissions);

        $("#plrclients").on('change', PolarisDesigner.FormPermissions.clientChanged);
        $("#plrapplications").on('change', PolarisDesigner.FormPermissions.appChanged);
        $("#plrforms").on('change', PolarisDesigner.FormPermissions.formChanged);
        $("#selectusergroups").on('change', PolarisDesigner.FormPermissions.usergroupChanged);

        $(document).on('updatePermission', function(e, elem) {
            PolarisDesigner.SharedLib.updatePermission(elem);
        });
        $(document).on('deletePermission', function(e, elem) {
            PolarisDesigner.SharedLib.deletePermission(elem);
        });

    },
	clientChanged: function() {
	    clientid = $("#plrclients").val();

        PolarisDesigner.SharedLib.getUserGroups(clientid, function(data) {
            var users = Array(), groups = Array();
            $.each(data.usergroups, function(i, elem) {
                if (elem.USERORGROUP == 'USER')
                    users.push(elem);
                else
                    groups.push(elem);
            });

            $("#selectusergroups optgroup option").remove();
            $("#selectusergroups .groups").addOptions(groups, 'USERGROUPID', 'USERGROUPNAME');
            $("#selectusergroups .users").addOptions(users, 'USERGROUPID', 'USERGROUPNAME');
        });

        PolarisDesigner.SharedLib.getApps(clientid, function(data) {
            $("#plrapplications option").remove();
            $("#plrapplications")
            .addOptions(data.apps, 'APPLICATIONID', 'APPLICATIONNAME', save_appid)
            .change();
        });
	},
	appChanged: function() {
		var Self = PolarisDesigner.FormPermissions;

	    var clientid = $("#plrclients").val();
	    var appid = $("#plrapplications").val();

        PolarisDesigner.SharedLib.getForms(clientid, appid, function(data) {
            $("#plrforms option").remove();
            $("#plrforms")
            .addOptions(data.forms, 'FORMID', 'FORMNAME', save_formid)
            .change();
        });
	},
	formChanged: function() {
		var Self = PolarisDesigner.FormPermissions;

	    var clientid = $("#plrclients").val();
	    var appid = $("#plrapplications").val();
	    var formid = $("#plrforms").val();

        PolarisDesigner.SharedLib.getFormsPermissions(clientid, appid, formid, function(data) {
            Self.htmlFormPermissions = Self.htmlFormPermissions.render(data, Self.templatePermissions);
            PolarisDesigner.SharedLib.initializeListView($("#formspermissions"));
        });
    },
    usergroupChanged: function() {
		var Self = PolarisDesigner.FormPermissions;

		$('.footerscroll tfoot .permissions')
		.html(PolarisDesigner.SharedLib.makePermissionBar('',0))
		.append(" &nbsp; <div class=\"buttons small\" style=\"display:inline\"><button id=\"newpermsave\" type=\"button\">Opslaan</button> <button id=\"newpermcancel\" type=\"button\">Annuleren</button></div>");
		$("#newpermsave").on('click', function() {
		    Self.newPermission(this);
		    $('.footerscroll tfoot .permissions').html('');
		});
		$("#newpermcancel").on('click', function() {
		    $('.footerscroll tfoot .permissions').html('');
		});
    },
    newPermission: function(elem) {
		var Self = PolarisDesigner.FormPermissions;

        var $row = $(elem).parents("tr");

        var clientid = $("#plrclients").val();
        var formid = $("#plrforms").val();
        var usergroupid = $("#selectusergroups").val();

        var permissiontype = PolarisDesigner.SharedLib.getPermissions($row);
        var params = {
            '_hdnAction': 'save',
            '_hdnState': 'insert',
            '_hdnProcessedByModule': 'true',
            'clientid': clientid,
            'formid': formid,
            'usergroupid': usergroupid,
            'permissiontype': permissiontype
        };
        PolarisDesigner.SharedLib._savePermission(params, $row, function() {
            $("#selectusergroups").val('');
            $(":checkbox", $row).attr('checked', false);
            Self.formChanged();
        });
    }
}

jQuery(function () { 
    PolarisDesigner.FormPermissions.initialize();

    PolarisDesigner.SharedLib.getClients(function(data) {
        $("#plrclients option").remove();
        $("#plrclients")
        .addOptions(data.clients, 'CLIENTID', 'NAME', save_clientid)
        .change();
    });
});