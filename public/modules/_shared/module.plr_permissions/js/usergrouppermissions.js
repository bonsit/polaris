PolarisDesigner.UserGroupPermissions = {
    initialize: function() {
		var Self = PolarisDesigner.UserGroupPermissions;

        Self.htmlUsergroupPermissions = $('#usergroupspermissions tbody');

		//directive to render the template
		var directivesPermissions = {
			'tr': {
				'ugp<-usergroupspermissions': {
					'td.form': function(arg) {
    				    return '<b>' + arg.item.FORMNAME + '</b>';
					},
					'td.permissions': function(arg) {
						return PolarisDesigner.SharedLib.makePermissionBar(arg.item.PLR__RECORDID, arg.item.PERMISSIONTYPE);
					}
				}
			}
		};
		Self.templatePermissions = Self.htmlUsergroupPermissions.compile(directivesPermissions);

        $("#plrclients").on('change', PolarisDesigner.UserGroupPermissions.clientChanged);
        $("#plrapplications").on('change', PolarisDesigner.UserGroupPermissions.appChanged);
        $("#plrusergroups").on('change', PolarisDesigner.UserGroupPermissions.usergroupChanged);
        $("#selectforms").on('change', PolarisDesigner.UserGroupPermissions.formChanged);

        $(document).on('updatePermission', function(e, elem) {
            PolarisDesigner.SharedLib.updatePermission(elem);
        });
        $(document).on('deletePermission', function(e, elem) {
            PolarisDesigner.SharedLib.deletePermission(elem);
        });
    },
	clientChanged: function() {
	    var clientid = $("#plrclients").val();

        PolarisDesigner.SharedLib.getApps(clientid, function(data) {
            $("#plrapplications option").remove();
            $("#plrapplications")
            .addOptions(data.apps, 'APPLICATIONID', 'APPLICATIONNAME', save_appid)
            .change();
        });
	},
	appChanged: function() {
		var Self = PolarisDesigner.UserGroupPermissions;

	    var clientid = $("#plrclients").val();
	    var appid = $("#plrapplications").val();
	    var usergroupid = $("#plrusergroups").val();

        PolarisDesigner.SharedLib.getAppUserGroups(clientid, appid, function(data) {
            var users = Array(), groups = Array();
            $.each(data.usergroups, function(i, elem) {
                if (elem.USERORGROUP == 'USER')
                    users.push(elem);
                else
                    groups.push(elem);
            });

            $("#plrusergroups optgroup option").remove();
            $("#plrusergroups .groups").addOptions(groups, 'USERGROUPID', 'USERGROUPNAME');
            $("#plrusergroups .users").addOptions(users, 'USERGROUPID', 'USERGROUPNAME');
            $("#plrusergroups").val(save_usergroupid).trigger('change');
        });

        PolarisDesigner.SharedLib.getForms(clientid, appid, function(data) {
            $("#selectforms optgroup option").remove();
            $("#selectforms optgroup")
            .addOptions(data.forms, 'FORMID', 'FORMNAME');
        });
	},
    usergroupChanged: function() {
		var Self = PolarisDesigner.UserGroupPermissions;

	    var clientid = $("#plrclients").val();
	    var appid = $("#plrapplications").val();
	    var usergroupid = $("#plrusergroups").val();
	    var usergroupsi = $("#plrusergroups").prop('selectedIndex');

        if (usergroupsi >= 0 && PolarisDesigner.SharedLib.plr_usergroups.usergroups[usergroupsi].USERORGROUP == 'GROUP')
            $("#members").html("Groepsleden: " + PolarisDesigner.SharedLib.plr_usergroups.usergroups[usergroupsi].MEMBERS.replace(/,/g, '<br>'));
        else
            $("#members").html("");

        if (appid && usergroupid) {
            PolarisDesigner.SharedLib.getUsergroupsPermissions(clientid, appid, usergroupid, function(data) {
                Self.htmlUsergroupPermissions = Self.htmlUsergroupPermissions.render(data, Self.templatePermissions);
                PolarisDesigner.SharedLib.initializeListView($("#usergroupspermissions"));
            });
        }
    },
	formChanged: function() {
		var Self = PolarisDesigner.UserGroupPermissions;

		$('.perm_listview tfoot .permissions')
		.html(PolarisDesigner.SharedLib.makePermissionBar('',0))
		.append(" &nbsp; <div class=\"buttons small\" style=\"display:inline\"><button id=\"newpermsave\" type=\"button\">Opslaan</button> <button id=\"newpermcancel\" type=\"button\">Annuleren</button></div>");
		$("#newpermsave").click(function() {
		    Self.newPermission(this);
		});
		$("#newpermcancel").click(function() {
		    $('.perm_listview tfoot .permissions').html('');
		});
    },
    newPermission: function(elem) {
		var Self = PolarisDesigner.UserGroupPermissions;

        var $row = $(elem).parents("tr");

        var clientid = $("#plrclients").val();
        var usergroupid = $("#plrusergroups").val();
        var formid = $("#selectforms").val();

        var permissiontype = PolarisDesigner.SharedLib.getPermissions($row);
        var params = {
            '_hdnAction': 'save',
            '_hdnState': 'insert',
            '_hdnProcessedByModule': 'true',
            'clientid': clientid,
            'formid': formid,
            'usergroupid': usergroupid,
            'permissiontype': permissiontype
        };
        PolarisDesigner.SharedLib._savePermission(params, $row, function() {
            $("#selectusergroups").val('');
            $(":checkbox", $row).prop('checked', false);
            Self.formChanged();
        });
    }
}

jQuery(function () { 
    PolarisDesigner.UserGroupPermissions.initialize();

    PolarisDesigner.SharedLib.getClients(function(data) {
        $("#plrclients option").remove();
        $("#plrclients")
        .addOptions(data.clients, 'CLIENTID', 'NAME', save_clientid)
        .change();
    });
});