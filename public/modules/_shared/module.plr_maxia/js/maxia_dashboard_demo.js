MaxiaDashboardDemo = {
    initialiseer: function() {
        if ($("#flot-dashboard-chart").length > 0) {
            var data2 = [
                [gd(2023, 4, 1), 7], [gd(2023, 4, 2), 6], [gd(2023, 4, 3), 4], [gd(2023, 4, 4), 8],
                [gd(2023, 4, 5), 9], [gd(2023, 4, 6), 7], [gd(2023, 4, 7), 5], [gd(2023, 4, 8), 4],
                [gd(2023, 4, 9), 7], [gd(2023, 4, 10), 8], [gd(2023, 4, 11), 9], [gd(2023, 4, 12), 6],
                [gd(2023, 4, 13), 4], [gd(2023, 4, 14), 5], [gd(2023, 4, 15), 11], [gd(2023, 4, 16), 8],
                [gd(2023, 4, 17), 8], [gd(2023, 4, 18), 11], [gd(2023, 4, 19), 11], [gd(2023, 4, 20), 6],
                [gd(2023, 4, 21), 6], [gd(2023, 4, 22), 8], [gd(2023, 4, 23), 11], [gd(2023, 4, 24), 13],
                [gd(2023, 4, 25), 7], [gd(2023, 4, 26), 9], [gd(2023, 4, 27), 9], [gd(2023, 4, 28), 8],
                [gd(2023, 4, 29), 5], [gd(2023, 4, 30), 8], [gd(2023, 4, 31), 25]
            ];

            var data3 = [
                [gd(2023, 4, 1), 800], [gd(2023, 4, 2), 500], [gd(2023, 4, 3), 600], [gd(2023, 4, 4), 700],
                [gd(2023, 4, 5), 500], [gd(2023, 4, 6), 456], [gd(2023, 4, 7), 800], [gd(2023, 4, 8), 589],
                [gd(2023, 4, 9), 467], [gd(2023, 4, 10), 876], [gd(2023, 4, 11), 689], [gd(2023, 4, 12), 700],
                [gd(2023, 4, 13), 500], [gd(2023, 4, 14), 600], [gd(2023, 4, 15), 700], [gd(2023, 4, 16), 786],
                [gd(2023, 4, 17), 345], [gd(2023, 4, 18), 888], [gd(2023, 4, 19), 888], [gd(2023, 4, 20), 888],
                [gd(2023, 4, 21), 987], [gd(2023, 4, 22), 444], [gd(2023, 4, 23), 999], [gd(2023, 4, 24), 567],
                [gd(2023, 4, 25), 786], [gd(2023, 4, 26), 666], [gd(2023, 4, 27), 888], [gd(2023, 4, 28), 900],
                [gd(2023, 4, 29), 178], [gd(2023, 4, 30), 555], [gd(2023, 4, 31), 993]
            ];

            var dataset = [
                {
                    label: "Interne reserveringen",
                    data: data3,
                    color: "#1ab394",
                    bars: {
                        show: true,
                        align: "center",
                        barWidth: 24 * 60 * 60 * 600,
                        lineWidth:0
                    }

                }, {
                    label: "Externe reserveringen",
                    data: data2,
                    yaxis: 2,
                    color: "#1C84C6",
                    lines: {
                        lineWidth:1,
                            show: true,
                            fill: true,
                        fillColor: {
                            colors: [{
                                opacity: 0.2
                            }, {
                                opacity: 0.4
                            }]
                        }
                    },
                    splines: {
                        show: false,
                        tension: 0.6,
                        lineWidth: 1,
                        fill: 0.1
                    },
                }
            ];


            var options = {
                xaxis: {
                    mode: "time",
                    tickSize: [3, "day"],
                    tickLength: 0,
                    axisLabel: "Date",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Arial',
                    axisLabelPadding: 10,
                    color: "#d5d5d5"
                },
                yaxes: [{
                    position: "left",
                    max: 1070,
                    color: "#d5d5d5",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Arial',
                    axisLabelPadding: 3
                }, {
                    position: "right",
                    clolor: "#d5d5d5",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: ' Arial',
                    axisLabelPadding: 67
                }
                ],
                legend: {
                    noColumns: 1,
                    labelBoxBorderColor: "#000000",
                    position: "nw"
                },
                grid: {
                    hoverable: false,
                    borderWidth: 0
                }
            };

            function gd(year, month, day) {
                return new Date(year, month - 1, day).getTime();
            }

            var previousPoint = null, previousLabel = null;

            $.plot($("#flot-dashboard-chart"), dataset, options);
        }
    }
};

jQuery(function () {
    MaxiaDashboardDemo.initialiseer();
});
