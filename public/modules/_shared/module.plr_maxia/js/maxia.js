Maxia = {};
Base.update(Maxia, {
    getGetVar: function(varname) {
        var $_GET = {};
        document.location.search.replace(/\??(?:([^=]+)=([^&]*)&?)/g, function () {
            function decode(s) {
                return decodeURIComponent(s.split("+").join(" "));
            }
            $_GET[decode(arguments[1])] = decode(arguments[2]);
        });
        return $_GET[varname];
    },
    getAanvraagUitrusting: function(aanvraagid, ruimteid, rrule, duur, callback) {
        return $.getJSON('/services/json/app/facility/const/reserveringen/', {
            'event': 'aanvraaguitrustingen',
            'action': $('body').hasClass('edit') ? 'edit' : 'insert',
            'aanvraagid':aanvraagid,
            'ruimteid':ruimteid,
            'rrule':rrule,
            'duur':duur,
        }, callback);
    },
    getLocaties: function(callback) {
        return $.getJSON('/services/json/app/facility/const/overzicht_flexplek/', {
            'event': 'locaties'
        }, callback);
    },
    getFlexReserveringen: function(locatie, datum, ruimte, tijdperiode, callback) {
        return $.getJSON('/services/json/app/facility/const/overzicht_flexplek/', {
            'event': 'flexplekken_reserveringen',
            'locatie':locatie,
            'datum':datum,
            'ruimte':ruimte,
            'tijdperiode':tijdperiode
        }, callback);
    },
    getFlexPlekken: function(locatie, datum, tijdperiode, callback) {
        return $.getJSON('/services/json/app/facility/const/overzicht_flexplek/', {
            'event': 'flexplekken_beschikbaarheid',
            'locatie':locatie,
            'datum':datum,
            'tijdperiode':tijdperiode
        }, callback);
    },
    initialiseerKalenderEvents: function(cal) {
        $('.fc-prev-button,.fc-next-button,.fc-today-button').on('click', function() {
            Maxia.bewaarKalenderDatum(cal);
        });
    },
    bewaarLogboekPeriode: function(periode) {
        if (sessionStorage)
            sessionStorage.setItem('logboekPeriode', periode);
    },
    bewaarLogboekWeergave: function(weergave) {
        if (sessionStorage)
            sessionStorage.setItem('logboekWeergave', weergave);
    },
    bewaarLogboekKeuzeDatum: function(datum) {
        if (sessionStorage)
            sessionStorage.setItem('logboekKeuzeDatum', datum);
    },
    bewaarKalenderView: function(cal) {
        if (sessionStorage && cal)
            sessionStorage.setItem(`calendarView.${formid}`, cal.view.type);
    },
    bewaarKalenderDatum: function(cal) {
        if (sessionStorage && cal)
            sessionStorage.setItem(`calendarDate.${formid}`, cal.getDate().toISOString());
    },
    getLogboekPeriode: function(defaultPeriode) {
        return (sessionStorage) ? sessionStorage.getItem('logboekPeriode') || defaultPeriode : defaultPeriode;
    },
    getLogboekWeergave: function(defaultWeergave) {
        return (sessionStorage) ? sessionStorage.getItem('logboekWeergave') || defaultWeergave : defaultWeergave;
    },
    getLogboekKeuzeDatum: function(defaultDatum) {
        return (sessionStorage) ? sessionStorage.getItem('logboekKeuzeDatum') || defaultDatum : defaultDatum;
    },
    getKalenderView: function(defaultView) {
        return (sessionStorage) ? sessionStorage.getItem(`calendarView.${formid}`) || defaultView : defaultView;
    },
    getKalenderDatum: function(defaultDate) {
        return (sessionStorage) ? sessionStorage.getItem(`calendarDate.${formid}`) || defaultDate : defaultDate;
    },
});
