{include file="shared.tpl.php"}

<div class="element-detail-box">

{if $locaties|count > 1}
<div id="locaties" class="row locatie-items">
    <div class="col-lg-12">
        <h3>Kies je standaard locatie</h3>
    </div>
    <div class="col-lg-12">
        <div class="btn-group">
        {section name=i loop=$locaties}
        <button class="locatie-item btn btn-lg btn-default btn-outlinex {if $huidigelocatie == $locaties[i].LOCATIE_ID}active{/if}" type="button" data-name="_GRP_LOCATIE" data-value="{$locaties[i].LOCATIE_ID}" onclick="javascript:Polaris.Form.groupFilter(this)">
            <div class="locatie-item-naam">{$locaties[i].LOCATIE_NAAM}</div>
        </button>
        {/section}
        </div>
    </div>
</div>
{/if}

<div id="quickactions1" class="row quickactions">
    {section name=i loop=$items}
    <div class="col-lg-3 col-md-3 col-sm-5">
        <a id="qa_{$items[i].QUICKACTION_ID}" href="{$items[i].URL}">
            <div class="widget {if $items[i].KLEUR[0] ne '#'}{$items[i].KLEUR}{/if}" {if $items[i].KLEUR[0] eq '#'}style="background-color:{$items[i].KLEUR};"{/if}>
                <div class="row">
                    <div class="col-12">
                        <h2>{$items[i].OMSCHRIJVING}</h2>
                    </div>
                    <div class="icon">{icon name=$items[i].ICOON size="3x" style=""}</div>
                </div>
            </div>
        </a>
    </div>
    {/section}
</div>

</div>