<?php
require_once('_shared/module._base.php');

use Coduo\PHPHumanizer\DateTimeHumanizer;
use Michelf\MarkdownExtra;

CONST GROUP_FACILITY_BEHEER = 'app-facilitymanagement-beheer';

class ModulePLR_Maxia extends _BaseModule {
    var $stylesheet;
    var $gen_form;
    var $processed;
    var $currentaction;
    var $clienthash;
    var $_cfi = null;

    const DEMO_DASHBOARD = 1;
    const QUICKACTIONS = 2;
    const MX_LOGBOEK = 3;

    function __construct($moduleid, $module, $clientname=false) {
        global $_GVARS;
        parent::__construct($moduleid, $module, $clientname);

        $this->processed = false;
        $this->stylesheet = 'default.css';
        $this->showControls = true;
        $this->showDefaultButtons = false;
        $this->clienthash = $_SESSION['clienthash'] ?? false;
    }

    function ProcessPostRequests($record, $event) {
        // This base function doesn't return anything, but child classes may override
        // to return an array. The return type is purposely omitted here.
    }

    function ProcessShowRequests($record, $event) {
        // This base function doesn't return anything, but child classes may override
        // to return an array. The return type is purposely omitted here.
    }

    function ProcessRequest($event=false) {
        parent::ProcessRequest($event);

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $result = $this->ProcessPostRequests($_POST, $event) ?? null;
        } else {
            $result = $this->ProcessShowRequests($_GET, $event) ?? null;
        }
        return $result;
    }

    function processImageUrl($fotos_string, $index=0, $size='public') {
        $_result = '';
        $_foto = explode(',', $fotos_string)[$index];

        try {
            if (!empty($_foto)) {
                require_once('includes/cloudflare_images.php');

                if (!$this->_cfi) {
                    $this->_cfi = new bartb\CloudFlare\Images($_ENV['CLOUDFLARE_IMAGES_ACCOUNT'], $_ENV['CLOUDFLARE_IMAGES_TOKEN'], $_ENV['CLOUDFLARE_ACCOUNT_HASH'], $_ENV['CLOUDFLARE_IMAGES_DELIVERY_URL']);
                }

                $keyfieldselect = $this->form->database->makeEncodedKeySelect('mx_mediabeheer', $_foto);
                $_sql = "SELECT CLOUD_IMAGE_ID FROM mx_mediabeheer WHERE __DELETED = '0' AND {$keyfieldselect}";
                $_imageRecord = $this->form->database->userdb->GetRow($_sql);
                if ($_imageRecord && !empty($_imageRecord['CLOUD_IMAGE_ID'])) {
                    $_result = $this->_cfi->getImageUrl($_imageRecord['CLOUD_IMAGE_ID'], $size);
                }
            }
        } catch (Exception $e) {
            // Silent catch
            return $_result;
        }
        return $_result;
    }

    function ProcessFotos($recordset) {
        foreach($recordset as $index => $rec) {
            if (isset($rec['FOTOS'])) {
                $recordset[$index]['FOTO_URL'] = $this->processImageUrl($rec['FOTOS'], 0, 'thumbnail');
                unset($recordset[$index]['FOTOS']);
            }
        }
        return $recordset;
    }

    function ProcessRecordset($recordset) {
        global $scramble;

        $_parser = new MarkdownExtra;

        if (!is_array($recordset)) {
            return $recordset;
        }
        foreach($recordset as $index => $rec) {
            if (isset($rec['HUMANIZE_DATE'])) {
                $recordset[$index]['HUMANIZE_DATE'] = DateTimeHumanizer::difference(new \DateTime(), new \DateTime($rec['HUMANIZE_DATE']), 'nl');
            }
            if (isset($rec['__CREATEDON'])) {
                $recordset[$index]['__CREATEDON_ORI'] = $rec['__CREATEDON'];
                $recordset[$index]['__CREATEDON'] = DateTimeHumanizer::difference(new \DateTime(), new \DateTime($rec['__CREATEDON']), 'nl');
            }
            if (!isset($rec['DATUMTIJD'])) {
                $recordset[$index]['DATUMTIJD'] = $rec['__CHANGEDON'] ?? $rec['__CREATEDON'];
            }
            if (isset($rec['NOTITIE'])) {
                $rec['NOTITIE'] = str_replace("\n", "  \n", $rec['NOTITIE']);
                $recordset[$index]['NOTITIE'] = $_parser->transform($rec['NOTITIE']);
            }
            if (isset($rec['DOELSTELLING'])) {
                $recordset[$index]['DOELSTELLING'] = $_parser->transform($rec['DOELSTELLING']);
            }
            if (isset($rec['PERSOONSBEELD'])) {
                $recordset[$index]['PERSOONSBEELD'] = $_parser->transform($rec['PERSOONSBEELD']);
            }
            if (isset($rec['ALL_VERSIONS'])) {
                $recordset[$index]['ALL_VERSIONS'] = explode(',',$rec['ALL_VERSIONS']);
            }
        }
        return $recordset;
    }

    function CheckedLoggedIn() {
        if (!isset($_SESSION['clienthash'])) {
            throw new Exception('U bent niet ingelogd.');
        }
    }

//    $_locaties = $this->form->database->userdb->GetAll("SELECT * FROM fb_locaties WHERE CLIENT_HASH = ? AND __DELETED = 0 AND ACTIEF = 'Y'", array($this->clienthash));
    function GetActieveLocaties() {
        global $polaris;

        $_sql = "
            SELECT LOCATIE_ID, LOCATIE_NAAM, LOCATIE_CODE, LOCATIE_OMSCHRIJVING
            FROM fb_locaties ttt
            WHERE CLIENT_HASH = ?
            AND ACTIEF = 'Y'
            AND __DELETED = 0
        ";
        $_datascope = $polaris->plrClient->GetDataScopeFilter($this->form->database->record->DATABASEID, 'fb_locaties');
        $_sql = sqlCombineWhere($_sql, $_datascope);
        $_rs = $this->form->database->userdb->GetAll($_sql, [$this->clienthash]);
        return $_rs;
    }

    function GetActieveLocatiesAsJSON() {
        return json_encode($this->GetActieveDefaultLocatie());
    }

    /**
     * Dit wordt gebruikt wanneer een organisatie meerdere locaties heeft, maar
     * reserveringsactiviteiten alleen gekoppeld moeten worden aan één specifieke locatie.
     * Bijvoorbeeld wanneer een locatie moet worden ingesteld vanwege een verhuizing naar een nieuw gebouw.
     */
    function GetActieveDefaultLocatie() {
        $_sql = "
        SELECT locatie_id
        FROM fb_locaties ttt
        WHERE `locatie_id` = (
            SELECT waarde
            FROM mx_parameters p
            WHERE p.parametercode = 'ACL' and p.client_hash = ttt.client_hash
         )
        ";
        $_sql = $this->form->AddDataScopeFilter($_sql);
        $result = $this->form->database->userdb->GetOne($_sql);
        if (!isset($_SESSION['_GRP_LOCATIE']) and isset($result)) {
            $_SESSION['_GRP_LOCATIE'] = $result;
        }
        return $_SESSION['_GRP_LOCATIE'];
    }

    function CheckHuidigeSessieLocatie() {
        if ($_SESSION[GROUP_PREFIX.'LOCATIE']) {
            $result = $_SESSION[GROUP_PREFIX.'LOCATIE'];
        } else {
            $result = false;
        }

        $this->smarty->assign('hasactivelocation', $result);
        return $result;
    }

    function ProcesLocatie() {
        if (isset($_GET[GROUP_PREFIX.'LOCATIE'])) {
            $_potentieleLocatie = $_GET[GROUP_PREFIX.'LOCATIE'];
            $_sql = "SELECT LOCATIE_ID FROM fb_locaties WHERE LOCATIE_ID = ? AND CLIENT_HASH = ?";
            $_rs = $this->form->database->userdb->GetOne($_sql, array($_potentieleLocatie, $this->clienthash));
            if ($_rs) {
                $_SESSION[GROUP_PREFIX.'LOCATIE'] = $_potentieleLocatie;
                return $_SESSION[GROUP_PREFIX.'LOCATIE'];
            }
        }
        return $_SESSION[GROUP_PREFIX.'LOCATIE'];
    }

    function ShowFormView($state, $permission, $detail=false, $masterrecord=false) {
    }

    function ShowListView($state, $permission, $detail=false, $masterrecord=false) {
        $_customfilter = $this->__GetCustomFilter();

        $this->smarty->assign('permission', $permission);
        $this->smarty->assign('selecteditem', $_GET['cursuscode']);
        $this->smarty->assign('stylesheet', $this->stylesheet);
        $this->AddModuleJS(['maxia.js']);

        switch($this->moduleid) {
        case Self::DEMO_DASHBOARD:
            $this->AddModuleJS(['maxia_dashboard_demo.js']);
            echo $this->smarty->Fetch('dashboard.tpl.php');
            break;
        case Self::QUICKACTIONS:
            $this->ProcesLocatie();
            // $this->CheckHuidigeSessieLocatie();
            $_rs = $this->form->GetViewContent($detail=false, $masterrecord=false, $limit=false, $_customfilter);
            $this->smarty->assign('huidigelocatie', $this->GetActieveDefaultLocatie());
            $this->smarty->assign('items', $_rs);
            $this->smarty->assign('locaties', $this->GetActieveLocaties());
            echo $this->smarty->Fetch('directregelen.tpl.php');
            break;
        }
    }

}
