<?php
require_once('module._base.php');
require_once('includes/datefuncs.inc.php');

class ModulePLR_Planner extends _BaseModule {
    var $cols;
    var $rows;
    var $selectbar;
    var $headercolors;
    var $headerfontcolor;
    var $stylesheet;
    var $filterrows;
    var $currentdate;
    var $currentmonth;
    var $currentweek;
    var $currentday;

    function __construct($moduleid, $module) {
        global $_GVARS;
        parent::__construct($moduleid, $module);

        $this->stylesheet = 'default.css';
        $this->headerfontcolor = false;
        $this->filterrows = false;
    }

    function Process() {
        $this->userdatabase->SetFetchMode(ADODB_FETCH_ASSOC);
        $this->parray = extractParams($this->params);
        $masterfield = $this->parray['masterfield'];

        if (isset($_GET['datetime'])) {
            $dateparts = explode('T',$_GET['datetime']);
            $this->currentdate = strtotime($dateparts[0]);
        } else {
            $this->currentdate = time();
        }

        $this->currentday = isset($_GET['day'])?$_GET['day']:date('d', $this->currentdate);
        $this->currentweek = isset($_GET['week'])?$_GET['week']:date('W', $this->currentdate);
        $this->currentmonth = isset($_GET['month'])?$_GET['month']:date('m', $this->currentdate);
        $tmpyear = date('Y', $this->currentdate);
        if (($this->currentweek == '1') and ($this->currentmonth == '12'))
            $tmpyear++;
        $this->currentyear = isset($_GET['year'])?$_GET['year']:$tmpyear;
        $this->currentview = isset($_GET['view'])?$_GET['view']:$this->defaultview;
        if (isset($_POST['_hdnCancelfilter']) and $_POST['_hdnCancelfilter'] == 'true') {
            unset($_SESSION['row_filtervalues']);
        }

    }

    function GetRightDay($daynumber) {
        // in:  sun, mon, tue, wed, thu, fri, sat
        // out: mon, tue, wed, thu, fri, sat, sun
        switch($daynumber) {
        case 0: return 6;
        break;
        case 1: return 0;
        break;
        case 2: return 1;
        break;
        case 3: return 2;
        break;
        case 4: return 3;
        break;
        case 5: return 4;
        break;
        case 6: return 5;
        break;
        }
    }

    function ConvertColors($colors) {
        require_once("includes/class_color.inc.php");

        $c = new color();
        foreach($colors as $color) {
            $rgbs = explode(',',substr($color,4,-1));
            $c->set_from_rgb($rgbs[0],$rgbs[1],$rgbs[2]);
            $temp = $c->get_hsl();
            $level1 = max(floatval($temp[1]) - 0.15, 0);
            $level2 = min(floatval($temp[2]) + 0.25, 1);
            $c->set_from_hsl($temp[0],$level1,$level2);
            $temp = $c->get_rgbhex();
            $nc[] = "#$temp";
        }
        return $nc;
    }

    function CreateArray($type, $values) {
        switch($type) {
        case 'day':
            $today = strtotime("{$this->currentyear}-{$this->currentmonth}-{$this->currentday}");
            $starttime = $values[1];
            $endtime = $values[2];
            $interval = $values[3];
            $j=0;
            for ($i=$starttime;$i<$endtime;$i++) {
                $time = $today + $i*60*60;
                $result[$j]['value'] = date('YmdHi',$time);
                $result[$j]['label'] = '<span class="hour">'.date('H',$time).'</span><br /><span class="time">'.date('i',$time).'</span>';
                $result[$j]['date'] = date('Y-m-d H:i',$time);
                $j++;
                $time = $today + $i*60*60 + 60*30;
                $result[$j]['value'] = date('YmdHi',$time);
                $result[$j]['label'] = '<br /><span class="time">'.date('i',$time).'</span>';
                $result[$j]['date'] = date('Y-m-d H:i',$time);
                $j++;
            }
            return $result;
        break;
        case 'week':
            $weekdates = week_dates($values[0],$values[1]);
            foreach($weekdates as $k => $v) {
                $result[$k]['value'] = date('Ymd',$v);
                $result[$k]['label'] = strftime('%A %e',$v);
                $result[$k]['date'] = $v;
            }
            return $result;
        break;
        case 'month':
            $nrofdays = date('t', mktime(0, 0, 0, $values[0], 1, $values[1]));
            for($i=0;$i<$nrofdays;$i++) {
                $v = mktime(0, 0, 0, $values[0], $i+1, $values[1]);
                $result[$i]['value'] = date('Ymd',$v);
                $result[$i]['label'] = strftime('d',$v);
                $result[$i]['date'] = $v;
            }
            return $result;
        break;
        }
    }

    function ProcessSelectBar($options) {
        $r = '';
        if (!$options) return $r;
        $basehref = $_SERVER['REQUEST_URI'];
        $basehref = remove_query_arg($basehref, 'datetime');
        $basehref = remove_query_arg($basehref, 'week');
        $basehref = remove_query_arg($basehref, 'month');
        foreach($options as $k => $option) {
            switch($option) {
            case 'today':
                $r .= "<a id=\"{$option}_a\" class=\"dateview\" href=\"$basehref\" title=\"vandaag\"><span id=\"$option\">ga naar vandaag</span></a>";
            break;
            case 'prevday':
            case 'nextday':
                $caption = ($option=='nextday')?'volgende':'vorige';
                if ($option=='nextday') {
                    $href = add_query_arg($basehref, 'datetime', date('Ymd',strtotime("tomorrow", $this->currentdate)) );
                } else {
                    $href = add_query_arg($basehref, 'datetime', date('Ymd',strtotime("yesterday", $this->currentdate)) );
                }
                $r .= "<a id=\"{$option}_a\" href=\"$href\" title=\"$caption dag\"><span id=\"$option\">$caption</span></a>";
            break;
            case 'prevweek':
            case 'nextweek':
                $caption = ($option=='nextweek')?'volgende':'vorige';
                if ($option=='nextweek') {
                    if ($this->currentweek != 52) {
                        $href = add_query_arg($basehref, 'week', $this->currentweek + 1 );
                    } else {
                        $href = add_query_arg($basehref, 'week', 1 );
                        $href = add_query_arg($href, 'year', $this->currentyear + 1 );
                    }
                } else {
                    if ($this->currentweek != 1) {
                        $href = add_query_arg($basehref, 'week', $this->currentweek - 1 );
                    } else {
                        $href = add_query_arg($basehref, 'week', 52 );
                        $href = add_query_arg($href, 'year', intval($this->currentyear) - 1 );
                    }
                }
                $r .= "<a id=\"{$option}_a\" href=\"$href\" title=\"$caption week\"><span id=\"$option\">$caption</span></a>";
            break;
            case 'prevmonth':
            case 'nextmonth':
                $caption = ($option=='nextmonth')?'volgende':'vorige';
                if ($option=='nextmonth') {
                    if ($this->currentmonth != 12) {
                        $href = add_query_arg($basehref, 'month', $this->currentmonth + 1 );
                    } else {
                        $href = add_query_arg($basehref, 'month', 1 );
                        $href = add_query_arg($href, 'year', $this->currentyear + 1 );
                    }
                } else {
                    if ($this->currentmonth != 1) {
                        $href = add_query_arg($basehref, 'month', $this->currentmonth - 1 );
                    } else {
                        $href = add_query_arg($basehref, 'month', 12 );
                        $href = add_query_arg($href, 'year', intval($this->currentyear) - 1 );
                    }
                }
                $r .= "<a id=\"{$option}_a\" href=\"$href\" title=\"$caption week\"><span id=\"$option\">$caption</span></a>";
            break;
            case 'dayselect':
                unset($set);
                for($i=1;$i<=31;$i++) {
                    $time = mktime(0, 0, 0, $this->currentmonth, $i, $this->currentyear);
                    $set[$i-1] = strftime('%Y%m%d', $time).'='.strftime('%d', $time). ' '.ucwords(strftime('%a', $time));
                }
                $currentday = strftime('%Y%m%d', mktime(0, 0, 0, $this->currentmonth, $this->currentday, $this->currentyear));
                $r .= SetAsSelectList($set, $itemname='day', $currentvalue=$currentday, $id='dayselect', $selectname='datetime', $required=true, 'plannerselect');
            break;
            case 'weekselect':
                unset($set);
                for($i=1;$i<=52;$i++) {
                    $weekdates = week_dates($i,$this->currentyear);
                    $day1 = date('d',$weekdates[0]);
                    $day7 = date('d',$weekdates[6]);
                    $mon = strftime('%b',$weekdates[6]);
                    $set[$i-1] = "$i=week $i - $day1/$day7 $mon";
                }
                $r .= SetAsSelectList($set, $itemname='week', $currentvalue=$this->currentweek, $id='weekselect', $selectname='week', $required=true, 'plannerselect');
            break;
            case 'monthselect':
                unset($set);
                for($i=1;$i<=12;$i++) {
                    $set[$i-1] = $i.'='.strftime('%B', mktime(0, 0, 0, $i, 1, 2006));
                }
                $r .= SetAsSelectList($set, $itemname='maand', $currentvalue=$this->currentmonth, $id='monthselect', $selectname='month', $required=true, 'plannerselect');
            break;
            case 'yearselect':
                unset($set);
                for($i=2005;$i<=2020;$i++) {
                    $set[$i-1] = $i;
                }
                $r .= SetAsSelectList($set, $itemname='jaar', $currentvalue=$this->currentyear, $id='yearselect', $selectname='year', $required=true, 'plannerselect');
            break;
            case 'dayview':
            case 'monthview':
            case 'weekview':
                $href = remove_query_arg($basehref, 'view');
                $type = str_replace('view','',$option);
                $captions = array('monthview'=>'maand', 'weekview'=>'week', 'dayview'=>'dag');
                $caption = $captions[$option];
                $href = add_query_arg($href, 'view', $type );
                $time = strtotime($this->currentyear . '0104 +' . ($this->currentweek - 1). ' weeks');
                if ($option == 'dayview') $href = add_query_arg($href, 'datetime', date('Ymd',$time) );
                if ($option == 'weekview') $href = add_query_arg($href, 'week', date('W',$this->currentdate) );

                $current = ($this->currentview == $type)?'current':'';
                $r .= "<a id=\"{$type}view_a\" class=\"dateview {$current}\" href=\"$href\" title=\"{$caption} overzicht\"><span>$type view</span></a>";
            break;
            }
        }
        return $r;
    }

    function ShowCell($cell) {
        $r = '';
        if ($cell['hinttext'] != '') $r .= "<span class=\"tooltip\" title=\"{$cell['hinttext']}\">";
        if ($cell['link'] != '') {
            $r .= "<a href=\"{$cell['link']}\" class=\"tooltip";
            if ($cell['linkaspopup']) $r .= " popbox pause_countdown";
            $r .= "\"";
            if ($cell['color']) $r .= " style=\"color:{$cell['color']}\"";
            $r .= ">";
        }
        $r .= "<span class=\"first\">{$cell['firstlabel']}</span>";
        if ($cell['secondlabel'] != '')
            $r .= "<span class=\"second\">{$cell['secondlabel']}</span>";
        if ($cell['link'] != '')
            $r .= "</a>";
        if ($cell['hinttext'] != '') $r .= "</span>";
        $r .= "<br />";
        return $r;
    }

    function ShowCellActions($cell) {
        $r = '';
        if (is_array($cell))
            $cell = $cell[0];
        if (isset($cell['cellactions']))
            foreach($cell['cellactions'] as $key => $cellaction) {
                $r .= $cellaction;
            }
        return $r;
    }

    function ShowCellItemActions($cell) {
        $r = '';
        if (isset($cell['cellitemactions']))
            foreach($cell['cellitemactions'] as $key => $cellaction) {
                $r .= $cellaction;
            }
        return $r;
    }

    function RenderTableBody($data) {
        $t = "<tbody>";
        if (!$this->rows) return false;

        foreach($this->rows as $row) {

            $t .= "<tr {$row['rowextra']}>";
            $t .= "<td class=\"firstcol\">";
            if ($this->filterrows) {
                $t .= "<input type=\"checkbox\" name=\"fr[]\" checked=\"checked\" value=\"{$row['value']}\" />";
            }
            if (isset($row['link'])) {
                $t .= "<a href=\"{$row['link']}\" class=\"popbox tooltip pause_countdown\"";
                if ($row['hinttext'] != '') $t .= " title=\"{$row['hinttext']}\" ";
                $t .= ">";
            }
            $t .= $row['label'];
            if (isset($row['link']))
                $t .= "</a>";
            $t .= "</td>";
            $ci = 1;
            foreach($this->cols as $k => $col) {
                $t .= "<td ";
                if ($data[$col['value']][$row['value']][0]['cellcolor'] != '') {
                    $t .= "style=\"background-color:".$data[$col['value']][$row['value']][0]['cellcolor']."\"";
                }
                if (evennumber($ci))
                    $t .= "class=\"even\"";
                $t .= " >";

                $t .= $this->ShowCellActions($data[$col['value']][$row['value']]);

                if (is_array($data[$col['value']][$row['value']])) {
                    if ($data[$col['value']][$row['value']][0]['cellaction'] != '') {
                        $t .= $data[$col['value']][$row['value']][0]['cellaction'];
                    }

                    foreach($data[$col['value']][$row['value']] as $cell)
                        if ($cell['firstlabel'] != '')  {
                            $t .= $this->ShowCellItemActions($cell);
                            $t .= $this->ShowCell($cell);
                        }
                } else {
                    if ($data[$col['value']][$row['value']]['firstlabel'] != '')  {
                        $t .= $this->ShowCell($data[$col['value']][$row['value']]);
                    }
                }
                $t .= "</td>"; //<br /><hr class=\"fix\" />
                $ci++;
            }
            $t .= "</tr>";
        }
        $t .= "</tbody>";
        return $t;
    }

    function ShowCustomListView($state, $permission, $detail=false, $masterrecord=false) {
//        $rs = $this->form->GetViewContent($detail=false, $masterrecord=false, $limit=false, $customfilter);
//        $customfilter = $this->__GetCustomFilter();

        /* match the weekends with the last two colors */
        if ($this->currentview == 'month') {
            $datearray = getdate(mktime(0,0,0,$this->currentmonth,1,$this->currentyear));
            $dayoffset = $this->GetRightDay($datearray['wday']);
            for($i=0;$i<$dayoffset;$i++) {
                array_push($this->headercolors, array_shift($this->headercolors));
            }
        }
        $this->smarty->assign('now', $this->currentdate);
        $this->smarty->assign('filterrows', $this->filterrows);
        $this->smarty->assign('cols', $this->cols);
        $this->smarty->assign('colperc', floor(100 / count($this->cols)));
        $this->smarty->assign('rows', $this->rows);
        $this->smarty->assign('bodydata', $this->RenderTableBody($this->data));
        $this->smarty->assign('masterselect', $this->masterselect);
        $this->smarty->assign('selectbar', $this->ProcessSelectBar($this->selectbar));
        $this->smarty->assign('view', $this->currentview);
        $this->smarty->assign('headerfontcolor', $this->headerfontcolor);
        $this->smarty->assign('headercolors', $this->headercolors);
        if (!$this->headerfontcolor) {
            $this->headerfontcolors = $this->ConvertColors($this->headercolors);
            $this->smarty->assign('headerfontcolors', $this->headerfontcolors);
        }
        $this->smarty->assign('stylesheet', $this->stylesheet);
        echo $this->smarty->Fetch('planner.tpl');
    }

    function ShowCustomFormView($state, $permission, $detail=false, $masterrecord=false) {
        global $_GVARS;

        if (isset($_GET['view'])) {
            $this->smarty->template_dir = $_GVARS['docroot'].'/'.$_GVARS['module_dir'].$this->orig_module.'/html/';
            echo $this->smarty->Fetch($_GET['view'].".tpl");
        }
    }

    function Show($outputformat='xhtml', $rec=false) {
        global $_GVARS;

    //    $this->GetData();
        if ($this->form->outputformat == 'json') {
            echo json_encode($this->data);
        } elseif ($this->form->outputformat == 'php') {
            echo serialize($this->data);
        } else {
            parent::Show();

            switch($_GET['moduleaction']) {
            case '':
                $GLOBALS['includeGreyBox'] = true;
                $this->ShowCustomListView($this->state, $this->permission, $detail=false, $masterrecord=false);
            break;
            case 'custom':
                $GLOBALS['includeGreyBox'] = true;
                $this->ShowCustomFormView($this->state, $this->permission, $detail=false, $masterrecord=false);
            break;
            case 'searchresult':
                $this->form->ShowSearchForm($this->state, $this->permission, $detail=false, $masterrecord=false);
                $this->form->ShowButtons($this->state, $this->permission);
                $this->form->ShowNavigationLinks($this->state, $this->permissiontype, $detail=false);
            break;
            }
        }
    }

}
