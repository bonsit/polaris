<?php
require('_shared/module.plr_maxia.php');

class ModulePLR_Maxia_ExterneReserveringen extends ModulePLR_Maxia {
    public $clientname;
    public $client;
    public $database;

    function GetRuimtes() {
        $_sql = "
            SELECT *
            FROM fb_ruimtes ttt
            WHERE CLIENT_HASH = ?
            AND BESCHIKBAARVOORVERHUUR & 8 > 0
            AND `locatie` = (
                SELECT locatie_id
                FROM fb_locaties l
                WHERE l.client_hash = ttt.client_hash
                AND `locatie_code` = (
                    SELECT waarde
                    FROM mx_parameters p
                    WHERE p.parametercode = 'ACL' and p.client_hash = l.client_hash
                )
            )
        ";
        $_rs = $this->database->userdb->Execute($_sql, array($this->client->record->CLIENTHASHID));
        $_result = $_rs->GetAll();
        return $_result;
    }

    function ShowReservering($stylesheet) {
        // Display the main reservation form
        $this->smarty->assign('ruimtes', $this->GetRuimtes());
        $this->smarty->assign('stylesheet', $stylesheet);
        echo $this->Fetch('externereservering.tpl.php');
    }
}
