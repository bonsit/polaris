<?php
require_once '_shared/module._base.php';

use Michelf\MarkdownExtra;

class ModulePLR_ExecuteAction extends _BaseModule {
    var $currentaction;
    var $actionsmarty;
    var $dbobject;
    var $lastStatement;

    function __construct($moduleid, $module) {
        global $_GVARS;
        global $polaris;

        parent::__construct($moduleid, $module);

        $this->actionsmarty = clone $this->smarty;
        $this->actionsmarty->template_dir = $_GVARS['docroot'].DIRECTORY_SEPARATOR.$_GVARS['module_dir'].DIRECTORY_SEPARATOR.'_shared'.DIRECTORY_SEPARATOR.'module.plr_executeaction'.DIRECTORY_SEPARATOR.'html'.DIRECTORY_SEPARATOR;
        /* find the action record */
        if (isset($_GET['action'])) {
            $this->currentaction = new base_plrAction($polaris);
            $this->currentaction->LoadRecord($_GET['action']); /* hashed actionid */
        }

        /* Load the user database */
        $this->dbobject = new base_plrDatabase($polaris);
        $this->dbobject->LoadRecord(array($this->currentaction->record->CLIENTID, $this->currentaction->record->DATABASEID));
        $this->userdatabase = $this->dbobject->connectUserDB();
        // $this->userdatabase->SetFetchMode(ADODB_FETCH_ASSOC);

        $this->processed = false;
        /**
        * What client are we talking about? (nog doen: eventueel selecteerbaar maken)
        */
        $this->clientid = $_SESSION['clientid'];
    }

    function ProcessRequest($event=false) {
        global $polaris;

        $_result = false;
        if (!empty($_GET['action'])) {
            $_actionid = $_GET['action'];
            $_moduleid = $_GET['moduleid'];
            $parameters = $this->ParamsNeedValue($this->currentaction->record);

            $_result = $this->ProcessAction($_actionid, $_moduleid, $_GET, $parameters);
            // if ($_result === true) {
                // echo "<span id='_fldDONTSHOWMESSAGE'></span>";
            // }
        }
        return $_result;
    }

    function DisplayMessage($result) {
        global $polaris;

        if ($result === true) {
            if ($this->currentaction->record->OKMESSAGE != '')
                $this->actionsmarty->assign('msg', $this->currentaction->record->OKMESSAGE);
        } else {
            $this->actionsmarty->assign('msg', 'Deze actie kon niet uitgevoerd worden.');
            $this->actionsmarty->assign('sql', $this->lastStatement);
            if (is_string($result))
                $this->actionsmarty->assign('error', $result);
            else
                $this->actionsmarty->assign('error', $polaris->ExtractPrettyMessage($this->userdatabase->ErrorMsg()));
        }
        $this->actionsmarty->assign('debug', $_GET['DEBUG']);
        $this->actionsmarty->display("message.tpl.php");
    }

    function DisplayParameterForm() {
        global $polaris;


        $_error = false;
        $currentlinerecord = arrayToObject($_GET);
        $this->actionsmarty->assign('actionname', $this->currentaction->record->ACTIONNAME);
        $this->actionsmarty->assign('dialogtitle', $this->currentaction->record->TITLE);
        $text = $polaris->_ReplaceDynamicVariables($this->currentaction->record->TEXT, $nullvalue='', $currentlinerecord);
        try {
            $text = ProcessSQL($this->userdatabase, $text);
        } catch (Exception $e) {
            $_error = 'Selecteer een record en probeer opnieuw.';
        }

        if ($_error) {
            $this->DisplayMessage($_error);
        } else {
            $text = MarkdownExtra::defaultTransform($text);
            $this->actionsmarty->assign('dialogtext', $text);

            for($i=1;$i<=5;$i++) {
                $pfield = "PARAM".$i;

                if ($this->currentaction->record->$pfield != '') {
                    $parts = $this->GetParts($this->currentaction->record->$pfield);
                    $selectname = 'param'.$i;
                    $valuesource = trim($parts[2]);
                    $currentvalue = $_GET[$selectname];
                    $valuesource = $polaris->_ReplaceDynamicVariables($valuesource, $nullvalue='', $currentlinerecord);
                    if (!$currentvalue) {
                        $currentvalue = $_GET[strtoupper($parts[0])];
                    }
                    if (strpos($valuesource, 'SQL::') !== FALSE) {
                        /* process param SELECT */
                        $sql = str_replace('SQL::', '', $valuesource);
                        $sql = $polaris->_ReplaceDynamicVariables($sql, $nullvalue='', $currentlinerecord);
                        $input = SQLAsSelectList($this->userdatabase, $sql, $keycolumn=0, $namecolumn=1, $currentvalue, $id='param'.$i.'select', $selectname, '', true);
                    } elseif (strpos($valuesource, ';') !== FALSE) {
                        /* process param LISTVALUES */
                        $setarray = explode(';', trim($valuesource));
                        $input = SetAsSelectList($setarray, $itemname=false, $currentvalue, $id='idselect', $selectname, $required=true);
                    } else {
                        if (strpos($parts[2], '%field[ROWID]%') !== FALSE) {
                            $readonly = 'readonly="readonly"';
                        } else {
                            $readonly = '';
                        }
                        if (!$currentvalue) {
                            $currentvalue = $polaris->_ReplaceDynamicVariables($valuesource, $nullvalue='', $currentlinerecord);
                            $currentvalue = $this->dbobject->customUrlDecode($currentvalue);
                        }
                        $requiredclass = $parts['required']?'appear_required':'';
                        $date_inputclass = ($parts[1] == 'DATE')?' date_input':'' ;
                        $input = '<input type="text" '.$readonly.' size="20" name="'.$selectname.'" class="'.$requiredclass.$date_inputclass.' form-control" value="'.$currentvalue.'" /> ';
                    }
                    $_activefields[] = $selectname;
                    $_nullvalues[] = $parts['required']?"EMPTY":"NULL";
                    $this->actionsmarty->assign('label'.$i, $parts[0]);
                    $this->actionsmarty->assign('param'.$i, $input);
                    $this->actionsmarty->assign('visible'.$i, $parts[0] == 'HIDDEN');
                }
                if ($_activefields)
                    $this->actionsmarty->assign('activefields', implode(',',$_activefields));
                if ($_nullvalues)
                    $this->actionsmarty->assign('nullvalues', implode(',',$_nullvalues));
            }
            $this->actionsmarty->display("parameterform.tpl.php");
        }

        return false; // Return false to prevent automatic processing of action (without displaying form)
    }

    function GetParts($param) {
        $firstspace = strpos($param, ' ');
        if ($firstspace)
            $secondspace = strpos($param, ' ', $firstspace + 1);
        $parts[0] = substr($param, 0, $firstspace);
        if (isset($secondspace) and $secondspace === false) {
            $parts[1] = substr($param, ($firstspace ? $firstspace + 1 : 0));
            $parts[2] = '';
        } else {
            // $parts[1] = substr($param, $firstspace ? $firstspace + 1 : 0);
            $parts[1] = substr($param, 0);
            $parts[2] = substr($param, $secondspace);
        }
        if ($parts[1][strlen($parts[1])-1] == '!') {
            $parts[1] = str_replace('!','',$parts[1]);
            $parts['required'] = true;
        }
        return $parts;
    }

    function ParamsNeedValue($rec) {
        $noparams = false;
        $params = [];

        for($i=1;$i<=5;$i++) {
            $pfield = "PARAM".$i;
            if (isset($rec->$pfield) and $rec->$pfield != '') {
                if (isset($_GET['param'.$i]) and $_GET['param'.$i] != '') {
                    if ($_GET['ROWID'] == $_GET['param'.$i])
                        $params[$i-1] = "ROWIDS:".$_GET['param'.$i];
                    else
                        $params[$i-1] = $_GET['param'.$i];
                    $noparams = false;
//                } else {
//                    return false;
                }
            } else {
                $noparams = true;
            }
        }
        if ($noparams and !$params)
            return true;
        else
            return $params;
    }

    function Show($outputformat='xhtml', $rec=false) {
        /* When it's the first time, show the parameter form */
        $displayDialogForFirstTime = !isset($_GET['notfirst']);
        $reportHasBeenPrinted = ($_GET['afterprint']??'') == 'true';

        if ($displayDialogForFirstTime and !$reportHasBeenPrinted) {
            $this->DisplayParameterForm();
        } else {
            $result = $this->ProcessRequest($_GET);
            $this->DisplayMessage($result);
        }

    }
}