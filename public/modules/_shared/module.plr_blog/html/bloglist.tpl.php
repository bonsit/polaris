{include file="shared.tpl.php"}

<div class="pblog">
<ul class="pblog_list">
{section loop=$items name=i}
    <li>
        <input type="hidden" class="pblog_published_value" value="{$items[i].PUBLISHED}" />
        <a href="{$serverroot}{$callerquery}edit/{$items[i].PLR__RECORDID}/"><h1>{$items[i].TITLE|truncate:27:"..."}</h1></a>
        <p class="pblog_published">
            Aangemaakt op: {$items[i].CREATED_AT|date_format:"%e %B %Y"}<br/>
            Gepubliceerd op: <span class="pblog_published_at">{if $items[i].PUBLISHED == 0}Nog niet{else}{$items[i].PUBLISHED_AT|date_format:"%e %B %Y"}{/if}</span><br/>
            Status:
            <select class="pblog_status" data-rec="{$items[i].PLR__RECORDID}">
                <option value="publish" {if $items[i].PUBLISHED == 1}selected="selected"{/if}>Gepubliceerd</option>
                <option value="concept" {if $items[i].PUBLISHED == 0}selected="selected"{/if}>Concept</option>
            </select>
        </p>
        <a href="{$serverroot}{$callerquery}edit/{$items[i].PLR__RECORDID}/" class="btn btn-primary">Wijzig</a>
        <a href="javascript:void()" data-rec="{$items[i].PLR__RECORDID}" class="btn btn-default btnDelete">Verwijder</a>
    </li>
{/section}
</ul>
</div>
