<link rel="stylesheet" type="text/css" media='all' href="{$moduleroot}/css/{$stylesheet}" />
<script type="text/javascript" src='{$moduleroot}/js/blog.js'></script>

<!-- CSS -->
<link rel="stylesheet" href="{$moduleroot}/css/medium-editor/medium-editor.min.css">
<link rel="stylesheet" href="{$moduleroot}/css/medium-editor/themes/default.min.css" id="medium-editor-theme">
<!-- JS -->
<script src="{$moduleroot}/js/medium-editor/medium-editor.js"></script>

<input type="hidden" name="_hdnProcessedByModule" value="true" />
