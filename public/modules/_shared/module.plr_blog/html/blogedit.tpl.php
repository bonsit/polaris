{include file="shared.tpl.php"}

<input type="hidden" name="_hdnRecordID" value="{$items[0].PLR__RECORDID}" />
<textarea type="hidden" id="plrblog_body" name="BODY">{$items[0].BODY_PREVIEW}</textarea>

<div class="pblog pblog_edit">
{section loop=$items name=i}
    <h1><input id="pblog_title" class="pblog_title" name="TITLE" type="text" value="{$items[i].TITLE}" placeholder="Titel" /></h1>
    <div id="plrblog_editor" class="pblog_body">{$items[i].BODY_PREVIEW}</div>
{/section}
</div>
