<div class="ibox">
    <div class="ibox-title">
        <h2>{lang user_settings}</h2>
    </div>
    <div class="ibox-content">
        <div class="row">
            <div class="col-md-12">
                <form id="userform" method="post" action="{$serverroot}/user/" class="form-horizontal validate" enctype="multipart/form-data">
                    <input type="hidden" name="_hdnProcessedByModule" value="true" />
                    <input type="hidden" name="_hdnRecordID" value="{$usergroup.RECORDID}" />
                    <input type="hidden" name="_hdnAction" value="{$recordstate}" />
                    <input type="hidden" name="MAX_FILE_SIZE" value="500000" />
                    <input type="hidden" name="_hdnUserGroupName" value="{$usergroup.USERGROUPNAME}" />

                        <div class="form-group"><label class="col-lg-3 control-label">{lang username}</label>

                            <div class="col-lg-9 form-control-static">{$usergroup.USERGROUPNAME}
                            </div>
                        </div>
                        {if !$usercannotchangepassword}
                        <div class="form-group"><label class="col-lg-3 control-label">{lang password}</label>

                            <div class="col-lg-9 form-control-static"><input class="text required" type="password" id="us_password" name="USERPASSWORD" value="{$userpassword}" /> <span id="pwstrength"></span>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-3 control-label">{lang retype_password}</label>

                            <div class="col-lg-9 form-control-static"><input class="text required" type="password" id="us_retypepw" name="RETYPEUSERPASSWORD" value="{$userpassword}" /> <span id="retypecorrect"></span>
                            </div>
                        </div>
                        {/if}
                        <div class="form-group"><label class="col-lg-3 control-label">{lang language}</label>

                            <div class="col-lg-9 form-control-static">
                                <select name="LANGUAGE">
                                    <option value="NL" {if $usergroup.LANGUAGE == 'NL'}selected{/if}>Nederlands</option>
                                    <option value="DE" {if $usergroup.LANGUAGE == 'DE'}selected{/if}>German</option>
                                    <option value="EN" {if $usergroup.LANGUAGE == 'EN'}selected{/if}>English</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-3 control-label">Positie menu</label>

                            <div class="col-lg-9 form-control-static"><input type="radio" id="set_horizontal" name="POSITIONMENU" value="horizontal" {if $positionmenu == 'horizontal'}checked="checked"{/if} /> <label for="set_horizontal"> Horizontal</label> &nbsp; <input type="radio" id="set_vertical" name="POSITIONMENU" value="vertical" {if $positionmenu == 'vertical'}checked="checked"{/if} /> <label for="set_vertical"> Vertical</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label">Extra beveiliging (2FA)</label>

                            <div class="col-lg-9 form-control-static"><input type="radio" id="set_2fa_yes" name="2FA" value="Y" {if $2fa == 'Y'}checked="checked"{/if} /> <label for="set_2fa_yes"> Aan</label> &nbsp; <input type="radio" id="set_2fa_no" name="2FA" value="N" {if $2fa == 'N'}checked="checked"{/if} /> <label for="set_2fa_no"> Uit</label>
                            </div>
                        </div>

                        <div class="form-group"><label class="col-lg-3 control-label">{lang telephone}</label>
                            <div class="col-lg-9 form-control-static"><input type="text" id="phoneNumber" class="disabled" name="PHONENUMBER" value="{$phonenumber}" />
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-offset-3 col-lg-9">
                                <button id="but_usersetting_ok" class="btn btn-primary disabled jqmAccept" type="submit">{lang but_save}</button>
                                <button id="but_usersetting_cancel" class="btn btn-default jqmClose" type="button">{lang but_cancel}</button>
                            </div>
                        </div>

                    </form>
            </div>
        </div>
    </div>
</div>

<script>{literal}
$("#userform :input").change(function(e){
    $("#but_usersetting_ok").removeClass("disabled").parent().find("img").remove();
});
$("#us_password").keyup(function(e){
    if ($("#us_password").val() != '__nonchanged__') {
        $("#pwstrength").html(Polaris.Base.checkPassword($("#us_password").val()));
    }
});
$("#us_retypepw").blur(function(e){
    if ($("#us_retypepw").val() != $("#us_password").val()) {
        $("#retypecorrect").text('Wachtwoorden zijn niet gelijk.');
    } else {
        $("#retypecorrect").text('');
    }
});

$("#but_usersetting_ok").on('click', function(e) {
    e.preventDefault();

    var form = $("#userform");
    if (form.valid()) {
        var data = Polaris.Form.convertToKeyValuePairs(form.serializeArray());
        Polaris.Ajax.postJSON(_serverroot + "/user/", data, null, null, function(data) {
            $("#but_usersetting_ok").addClass("disabled");
            Polaris.Base.modalMessage('Instellingen zijn opgeslagen');
        }, function(data) {
            if (data.error != '') {
                Polaris.Base.modalMessage(data.error);
            }
            $("#but_usersetting_ok").removeClass("disabled");
        });
    }
});

$("#headerColor").colorPicker().change(function() {
    $("#headercomplete").css('background-color', $(this).val());
});
$("input[name=2FA]").change(function() {
    $("#phoneNumber").toggleClass('readonly', $("#set_2fa_no").prop('checked'));

});

{/literal}</script>