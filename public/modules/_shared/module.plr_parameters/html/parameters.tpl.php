<style>
	.parameter-text-save {
		font-size:0.8em;
	}
	.parameter-container .select2 {
		width:200px;
	}
	.parameter-groepen {
		margin-bottom: 20px;
		display: flex;
  		justify-content: center;
	}
	.parameter-block {
		margin-bottom: 20px !important;
	}
</style>

{literal}

<script id="template-parameter-items" type="text/x-jsrender">

	<div class="tab-pane active">

	{^{for parameters}}

		<div class="row" >
			<div class="col-lg-8 col-lg-offset-2 col-md-7 col-md-offset-1 col-sm-12 col-sm-offset-0">
				<div class="row parameter-block equal">
					<div class="col-lg-8 col-sm-8 parameter-container">
						<div class="p-md">
							<h2>{{:PARAMETERNAAM}}</h2>
							<p>{{:OMSCHRIJVING}}</p>
						</div>
					</div>
					<div class="col-lg-4 col-sm-4 parameter-container">
						<div class="p-lgx expand">
							{{if DATATYPE == 'B'}}
							<div class="checkbox-slider--b-flat checkbox-slider-md checkbox-slider-info">
								<label>
									<input type="checkbox" data-link="{on 'change' ~checkboxSave}" class="parameter-checkbox" name="_hdnRecordID{{:PLR__RECORDID}}"
									{{if WIJZIGBAAR == 'N'}}readonly="readonly"{{/if}}
									{{if WAARDE == 'Y'}}checked="checked"{{/if}} data-column="{{:PLR__RECORDID}}"><span></span>
								</label>
							</div>
							{{/if}}
							{{if DATATYPE == 'S'}}
								<div class="input-groupx">
									<div class="row">
										<div class="col-lg-9">
											<input type="text" class="parameter-text" name="_hdnRecordID{{:PLR__RECORDID}}" data-column="{{:PLR__RECORDID}}" value="{{:WAARDE}}" />
										</div>
										<div class="col-lg-1">
											{^{on ~textSave}}
											<button class="pull-left btn btn-sm parameter-text-save" data-column="{{:PLR__RECORDID}}"><i class="material-symbols-outlined md-1x">check</i></button>
											{{/on}}
										</div>
									</div>
								</div>
							{{/if}}
							{{if DATATYPE == 'L'}}
								<div class="input-group">
									{{:WAARDELIJST}}
								</div>
							{{/if}}
						</div>
					</div>
				</div>
			</div>
		</div>

	{{/for}}
	<h4>Gebruikers dienen opnieuw in te loggen, om wijzigingen te effectueren.</h4>

	</div>

</script>
{/literal}

<div class="element-detail-box container">

	<div id="parameter-view" class="logboek-view"></div>

</div>
