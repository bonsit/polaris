Polaris.Parameters = {};
Base.update(Polaris.Parameters, {
    parametersModel: null,
    parametersData: {
        parametergroepen: [],
        parameters: [],
        huidigegroep: 'poep'
    },
    parametersSelectie: null,
    init: function() {
        var Self = this;

        Self.initParameters();
        Self.refreshParameters();
    },
    initParameters: function() {
        const Self = Polaris.Parameters;

        // this.parametersModel = $.views.viewModels({
        //     getters: ['parametergroepen', 'parameters'], huidigegroep : ''
        // });
        $.views.helpers(Self.renderHelpers);
        $.views.helpers.groepFiltering.depends = "~huidigegroep";
    },
    initGroepen: function() {
        $('#parameter-tabs li').on('click', function() {
            $('#parameter-tabs li').removeClass('active');
            $(this).addClass('active');
            Polaris.Parameters.parametersData.huidigegroep = $(this).data('groep');
            log(Polaris.Parameters.parametersData.huidigegroep);
        });
        var eersteGroep = $('#parameter-tabs li:last-child');
        eersteGroep.addClass('active');
        Polaris.Parameters.parametersData.huidigegroep = eersteGroep.data('groep');
    },
    initSelectTags: function() {
        $(document).on('change', '.input-group select', function(event) {
            Polaris.Parameters.renderHelpers.selectSave.call(this, event, null);
        })
    },
    getData: function(elem) {
        var $form = $(elem).parents('form');
        var data = {
          '_hdnAction':'plr_save'
        , '_hdnState':'edit'
        , '_hdnDatabase': $form.find(":input[name=_hdnDatabase]").val()
        , '_hdnTable': $form.find(":input[name=_hdnTable]").val()
        , '_hdnForm': $form.find(":input[name=_hdnForm]").val()
        , '_hdnRecordID': $(elem).data('column')
        , 'waarde': $(elem).val()
        };
        return data;
    },
    renderHelpers: {
        tabClicked: function(ev, eventArgs) {
            $('.parameter-groepen li').removeClass('active');
            var listItem = $(ev.target).parent();
            listItem.addClass('active');
            Polaris.Parameters.parametersData.huidigegroep = listItem.data('groep');
        },

        textSave: function(ev, eventArgs) {
            ev.preventDefault();
            ev.stopPropagation();
            let button = $(ev.target).parents('button');
            let inputField = $("input[name=_hdnRecordID" + button.data('column')+"]");
            let data = Polaris.Parameters.getData(inputField);
            data.waarde = inputField.val();
            Polaris.Ajax.postJSON(_ajaxquery, data, null, 'Wijziging vastgelegd');
        },

        selectSave: function(ev, eventArgs) {
            ev.preventDefault();
            ev.stopPropagation();
            let select = $("select[name=_hdnRecordID" + $(ev.target).data('column')+"]");
            let data = Polaris.Parameters.getData(select);
            Polaris.Ajax.postJSON(_ajaxquery, data, null, 'Wijziging vastgelegd');
        },

        checkboxSave: function(ev, eventArgs) {
            ev.preventDefault();
            ev.stopPropagation();
            let checkbox = $(ev.target);
            let data = Polaris.Parameters.getData(ev.target);
            data.waarde = checkbox.prop('checked') ? 'Y' : 'N';
            Polaris.Ajax.postJSON(_ajaxquery, data, null, 'Wijziging vastgelegd', function(data) {});
        },

        groepFiltering: function(item, index, items) {
            return item.GROEP === this.props.groep;
        }
    },
    refreshParameters: function() {
        var Self = this;

        $.getJSON('/services/json/app/facility/const/parameters/', {
            'event': 'parameters',
            '_hdnProcessedByModule': 'true'
        }, function(data) {
            var groep = data.parametergroepen[0].GROEPCODE;
            // Self.parametersData = Self.parametersModel(data.parametergroepen, data.parameters);
            Self.parametersData.parametergroepen = data.parametergroepen;
            Self.parametersData.parameters = data.parameters;
            Self.parametersData.huidigegroep = groep;
            log('a', Self.parametersData.huidigegroep);
            // $.templates("#template-parameter-tabs").link("#parameter-tabs", Self.parametersData);
            $.templates("#template-parameter-items").link("#parameter-view", Self.parametersData);
            Self.initGroepen();
            Self.initSelectTags();
        });
    },
});

jQuery(function () {
    Polaris.Parameters.init();
});
