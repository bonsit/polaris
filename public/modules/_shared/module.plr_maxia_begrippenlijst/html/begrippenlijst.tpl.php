
<div class="ibox-content">
    <div class="float-right">
        <button class="btn btn-white" type="button">Model</button>
        <button class="btn btn-white" type="button">Publishing</button>
        <button class="btn btn-white" type="button">Modern</button>
    </div>
    <div class="xarticle-title">
    <span class="text-muted"><i class="fa fa-clock-o"></i> 28th Oct 2015</span>

    <h2>{$item.BEGRIP}</h2>

    </div>
    <p>{$item.DEFINITIE1}</p>

    <hr>
    <div class="row">
        <div class="col-md-6">
                <h5>Tags:</h5>
                <div class="badge badge-primary " >Model</div>
                <div class="badge badge-primary " >Publishing</div>
        </div>
        <div class="col-md-6">
            <div class="small text-right">
                <h5>Stats:</h5>
                <div> <i class="fa fa-comments-o"> </i> 56 comments </div>
                <i class="fa fa-eye"> </i> 144 views
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">

            <h2>Comments:</h2>
            <div class="social-feed-box">
                <div class="social-avatar">
                    <a href="" class="float-left">
                        <img alt="image" src="img/a1.jpg">
                    </a>
                    <div class="media-body">
                        <a href="#">
                            Andrew Williams
                        </a>
                        <small class="text-muted">Today 4:21 pm - 12.06.2014</small>
                    </div>
                </div>
                <div class="social-body">
                    <p>
                        Many desktop publishing packages and web page editors now use Lorem Ipsum as their
                        default model text, and a search for 'lorem ipsum' will uncover many web sites still
                        default model text.
                    </p>
                </div>
            </div>
            <div class="social-feed-box">
                <div class="social-avatar">
                    <a href="" class="float-left">
                        <img alt="image" src="img/a2.jpg">
                    </a>
                    <div class="media-body">
                        <a href="#">
                            Michael Novek
                        </a>
                        <small class="text-muted">Today 4:21 pm - 12.06.2014</small>
                    </div>
                </div>
                <div class="social-body">
                    <p>
                        Many desktop publishing packages and web page editors now use Lorem Ipsum as their
                        default model text, and a search for 'lorem ipsum' will uncover many web sites still
                        default model text, and a search for 'lorem ipsum' will uncover many web sites still
                        in their infancy. Packages and web page editors now use Lorem Ipsum as their
                        default model text.
                    </p>
                </div>
            </div>
            <div class="social-feed-box">
                <div class="social-avatar">
                    <a href="" class="float-left">
                        <img alt="image" src="img/a3.jpg">
                    </a>
                    <div class="media-body">
                        <a href="#">
                            Alice Mediater
                        </a>
                        <small class="text-muted">Today 4:21 pm - 12.06.2014</small>
                    </div>
                </div>
                <div class="social-body">
                    <p>
                        Many desktop publishing packages and web page editors now use Lorem Ipsum as their
                        default model text, and a search for 'lorem ipsum' will uncover many web sites still
                        in their infancy. Packages and web page editors now use Lorem Ipsum as their
                        default model text.
                    </p>
                </div>
            </div>
            <div class="social-feed-box">
                <div class="social-avatar">
                    <a href="" class="float-left">
                        <img alt="image" src="img/a5.jpg">
                    </a>
                    <div class="media-body">
                        <a href="#">
                            Monica Flex
                        </a>
                        <small class="text-muted">Today 4:21 pm - 12.06.2014</small>
                    </div>
                </div>
                <div class="social-body">
                    <p>
                        Many desktop publishing packages and web page editors now use Lorem Ipsum as their
                        default model text, and a search for 'lorem ipsum' will uncover many web sites still
                        in their infancy. Packages and web page editors now use Lorem Ipsum as their
                        default model text.
                    </p>
                </div>
            </div>


        </div>
    </div>


</div>