<?php
require_once('includes/mainactions.inc.php');
require_once('module._base.php');

use Michelf\Markdown;

class ModulePLR_Blog extends _BaseModule {
    var $stylesheet;

    function ModulePLR_Blog($moduleid, $module, $clientname=false) {
        global $_GVARS;
        parent::__construct($moduleid, $module, false, $clientname);

        $this->processed = false;
        $this->stylesheet = 'default.css';
    }

    function ProcessRequest($_allesRecord, $parameters) {
        if (isset($_POST['_hdnAction'])) {
            switch ($_POST['_hdnAction']) {
            case 'publish':
                return $this->PublishArticle($_POST['_hdnRecordID']);
            break;
            case 'plr_save':
                $_POST['UPDATED_AT'] = date('Y-m-d H:i:s');
                $_POST['BLOG_ID'] = 1;
                $result = SaveRecord($_POST);
                if ($result == true) {
                    $_GET['action'] == '';
                }
            break;
            }
        }
    }

    function PublishArticle($rec) {
        $_tablename = $this->form->record->TABLENAME;
        $_key = $this->form->database->makeRecordID(array('ID'));
        $_sql = "UPDATE ".$_tablename." SET published = IF(published=1, 0, 1) WHERE ". $_key ." = '$rec'";
        $this->userdatabase->Execute($_sql);
        return true;
    }

    function ShowListView($state, $permission, $detail=false, $masterrecord=false) {
        $_customfilter = $this->__GetCustomFilter();
        $_rs = $this->form->GetViewContent($detail=false, $masterrecord=false, $limit=false, $_customfilter);
        $this->smarty->assign('stylesheet', $this->stylesheet);
        $this->smarty->assign('items', $_rs);
        echo $this->smarty->Fetch('bloglist.tpl.php');
    }

    function ShowFormView($state, $permission, $detail=false, $masterrecord=false) {
        $_customfilter = $this->__GetCustomFilter();
        $_rs = $this->form->GetViewContent($detail=false, $masterrecord=false, $limit=false, $_customfilter);
        $_rs[0]['BODY_PREVIEW'] = Markdown::defaultTransform($_rs[0]['BODY']);
        $this->smarty->assign('stylesheet', $this->stylesheet);
        $this->smarty->assign('items', $_rs);
        echo $this->smarty->Fetch("blogedit.tpl.php");
    }

    function Show($outputformat='xhtml', $rec=false) {
        global $_GVARS;

        if ($this->form->outputformat == 'json') {
            echo json_encode($this->data);
        } elseif ($this->form->outputformat == 'php') {
            echo serialize($this->data);
        } else {
            parent::Show();
            switch($_GET['action']) {
            case '':
                $this->ShowListView($this->state, $this->permission, $detail=false, $masterrecord=false);
            break;
            case 'edit':
            case 'insert':
                $this->ShowFormView($this->state, $this->permission, $detail=false, $masterrecord=false);
            break;
            }
        }
    }

}
