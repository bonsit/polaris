<html>
<head>
    <link rel="stylesheet" type="text/css"
        href="//www.bovengrondsevakschool.nl/wp-content/cache/wpfc-minified/lzkkj4ak/e54is.css" media="all" />
</head>
<body>
    <div class="l-canvas type_wide">
        <section class="l-section wpb_row bvs_row-svg-6 height_medium">
            <div
                class="vc_col-sm-12 vc_col-lg-offset-2 vc_col-lg-8 vc_col-md-offset-1 vc_col-md-10 wpb_column vc_column_container">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        <div class="w-post-elm post_custom_field type_text tekstkader_p_home_4 color_link_inherit">
                            <h1>Ruimte reservering</h1>
                            <p>Kies de gewenste ruimte en vul uw gegevens in.</p>

                            {section name=i loop=$ruimtes}
                            <h4>{$ruimtes[i].ruimte_code}</h4>
                            <p>{$ruimtes[i].ruimte_id}</p>
                            <p>{$ruimtes[i].ruimte_naam}</p>
                            {if !empty($ruimtes[i].foto)}
                            <img style="max-width:250px" src="data:image/jpeg;base64,{$ruimtes[i].foto}">
                            {else}
                            <p>Geen afbeelding</p
                            {/if}
                            {/section}

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</body>