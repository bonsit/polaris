<?php
require('_shared/module._base.php');

class ModulePLR_Usergroups extends _BaseModule {
    var $stylesheet;
    var $usergroup;

    function __construct($moduleid, $module, $clientname=false) {
        parent::__construct($moduleid, $module, $clientname);

        $this->stylesheet = 'default.css';
    }

    function __GetCustomFilter() {
        $_customfilter = '';
        if ($this->moduleid == 1) {
            $_customfilter = " CLIENTID = '{$_SESSION['clientid']}'";
        }
        return $_customfilter;
    }

    function ProcessPostRequests($record, $event) {

        $_feedback = [
            'suspendaccess' => 'Toegang is opgeschort',
            'assignadminrole' => 'Organisatiebeheerder rol is toegekend',
            'revokeadminrole' => 'Organisatiebeheerder rol is verwijderd',
            'resetpassword' => 'Wachtwoord is gereset',
            'deleteuser' => 'Gebruiker is verwijderd',
            'grantaccess' => 'Gebruiker is weer toegelaten',
            'updateapplicationpermissions' => 'Permissie is aangepast',
            'revokeapplication' => 'Toegang is ingetrokken'
        ];

        switch ($event) {
            case 'adduser':
                $_result = $this->AddUser($record);
                break;
            case 'changeuser':
                $_result = $this->ChangeUser($record);
                break;
            case 'updateapplicationpermissions':
                $_result = $this->UpdateApplicationPermissions($record);
                break;
            case 'revokeapplication':
                $_result = $this->RevokeApplicationPermissions($record);
                break;
            case 'revokerole':
                $_result = $this->RevokeRole($record);
                break;
            case 'suspendaccess':
                $_result = $this->SuspendAccess($record);
                break;
            case 'grantaccess':
                $_result = $this->GrantAccess($record);
                break;
            case 'assignadminrole':
                $_result = $this->AssignAdminRole($record);
                break;
            case 'revokeadminrole':
                $_result = $this->RevokeAdminRole($record);
                break;
            case 'resetpassword':
                $_result = $this->ResetPassword($record);
                break;
            case 'deleteuser':
                $_result = $this->DeleteUser($record);
                break;
            case 'deletegroup':
                $_result = $this->DeleteGroup($record);
                break;
            case 'adduserroles':
                $_result = $this->AddUserRoles($record);
                break;
            case 'adduserapplicationroles':
                $_result = $this->AddUserApplicationRoles($record);
                break;
        }
        if ($_result == true && !empty($_feedback[$event])) {
            $_SESSION['prevaction'] = $_feedback[$event];
        }
        return $_result;
    }

    function ProcessShowRequests($record, $event) {
        return false;
    }

    function ProcessRequest($event=false) {
        parent::ProcessRequest($event);

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $result = $this->ProcessPostRequests($_POST, $event);
        } else {
            $result = $this->ProcessShowRequests($_GET, $event);
        }
        return $result;
    }

    private function validateAdminAndUser($userhash) {
        $this->assertLoggedInUserIsAdmin();
        $this->assertUserIsNotChangingHimself($userhash);

        $this->usergroup = new base_plrUserGroup($this->polaris, $userhash);

        $this->assertEditableUser($this->usergroup);

        if (!$this->usergroup) {
            throw new Exception('Gebruiker niet gevonden of fout bij het laden van gebruikersgroep.');
        }
    }

    private function validateNotSpecialGroup($userhash) {
        $this->usergroup = new base_plrUserGroup($this->polaris, $userhash);

        $_specialRoles = ['USERS', 'ADMINS', 'ORGADMINS', 'STANDARD'];
        if (in_array($this->usergroup->record['ROLE'], $_specialRoles)) {
            throw new Exception('Dit type groep kan niet verwijderd worden omdat het een standaard onderdeel is van de organisatie.');
        }

        if (!$this->usergroup) {
            throw new Exception('Gebruiker niet gevonden of fout bij het laden van gebruikersgroep.');
        }
    }

    public function assertEditableUser($usergroup) {
        if ($usergroup->record['ROLE'] === 'API' && !$this->polaris->IsRootAdmin()) {
            throw new Exception('Dit type gebruiker kan alleen gewijzigd worden door een systeembeheerder.');
        }
    }

    public function AssertUserIsNotChangingHimself($userhash) {
        if ($userhash == $_SESSION['userhash']) {
            throw new Exception('U kunt uw eigen beheerrechten niet verwijderen');
        }
    }

    function AssertLoggedInUserIsAdmin() {
        if (!($this->polaris->IsClientAdmin() || $this->polaris->IsRootAdmin())) {
            throw new Exception('Alleen een beheerder kan de toegang van een gebruiker opschorten.');
        }
    }

    function AssertLoggedInUserIsRootAdmin() {
        if ($this->polaris->IsRootAdmin()) {
            throw new Exception('Alleen een systeem beheerder kan deze functie uitvoeren.');
        }
    }

    function AssertPresent($value, $name) {
        if (empty($value)) {
            throw new Exception("Het veld '{$name}' is verplicht.");
        }
    }

    function AddUser($record) {
        $_result = false;
        $results = [];

        $clienthash = $this->form->record->CLIENTHASH;
        $username = $record['USERGROUPNAME'];
        $fullname = $record['FULLNAME'];
        $emailaddress = $record['EMAILADDRESS'];
        $password = $record['PASSWORD'];
        $password_validate = $record['PASSWORD_VALIDATE'];
        $clientadmin = false;
        $confirmaccount = false;
        $options = false;
        $phonenumber = '';
        $description = '';

        $this->AssertPresent($username, 'Loginnaam');
        $this->AssertPresent($fullname, 'Volledige naam');
        $this->AssertPresent($record['UGNEWUSERAPPLICATIONS'], 'Kies minimaal één applicatie');

        $_usergroup = new base_plrUserGroup($this->polaris);
        $_userAdded = $_usergroup->addUserAccount($clienthash, $username, $fullname, $description, $emailaddress, $password, $password_validate, $clientadmin, $results, $confirmaccount, $options, $phonenumber);

        if ($_userAdded) {
            // Add the user to the groups of the selected applications
            if ($record['UGNEWUSERAPPLICATIONS'] && is_array($record['UGNEWUSERAPPLICATIONS'])) {
                foreach ($record['UGNEWUSERAPPLICATIONS'] as $applicationhash) {
                    $_result = $_userAdded->addToApplication($applicationhash);
                    if ($_result === false) {
                        throw new Exception('Er is een fout opgetreden bij het toevoegen van de gebruiker aan de applicatie.');
                    }
                }
            }
        }

        return $_result;
    }

    function ChangeUser($record) {
        $this->AssertLoggedInUserIsAdmin();

        $_result = false;
        $this->usergroup = new base_plrUserGroup($this->polaris, $record['USER']);

        if ($this->usergroup) {
            try {
                // Change all properties, excetp the password
                $_result = $this->usergroup->updateUser($record);

                // Change the password if it is set
                if (!empty(trim($record['PASSWORD']))) {
                    $this->usergroup->ChangePassword($record['PASSWORD'], $record['PASSWORD_VALIDATE']);
                }
            } catch (Exception $e) {
                if ($e->getCode() == 1062) {
                    throw new Exception('De gebruikersnaam of emailadres is al in gebruik. Kies een andere naam of emailadres.');
                } else {
                    throw new Exception($e->getMessage());
                }
            }

            if ($_result === false) {
                throw new Exception('Er is een fout opgetreden bij het wijzigen van de gebruikers.');
            }
            $_result = true;
        } else {
            throw new Exception('Er zijn te weinig gegevens voor het wijzigen van de gebruikers.');
        }

        return $_result;
    }

    function UpdateApplicationPermissions($record) {
        $this->AssertLoggedInUserIsAdmin();

        $_result = false;
        $this->usergroup = new base_plrUserGroup($this->polaris, $record['USER']);
        $_group = $record['GROUP'];
        $_isAdd = ($record['VALUE'] === '1');

        // Add the user to the groups of the selected applications
        if ($this->usergroup && !empty($_group)) {
            if ($_isAdd) {
                $_result = $this->usergroup->addToGroup($_group);
            } else {
                $_result = $this->usergroup->removeFromGroup($_group);
            }

            if ($_result === false) {
                throw new Exception('Er is een fout opgetreden bij het wijzigen van de gebruikerrol.');
            }
            $_result = true;
        } else {
            throw new Exception('Er zijn te weinig gegevens voor het wijzigen van de gebruikerrol.');
        }

        return $_result;
    }

    function RevokeApplicationPermissions($record) {
        $this->AssertLoggedInUserIsAdmin();

        $_result = false;
        $this->usergroup = new base_plrUserGroup($this->polaris, $record['USER']);
        $_application = $record['APPLICATION'];

        if ($this->usergroup && !empty($_application)) {
            $_result = $this->usergroup->removeFromApplication($_application, 'USERS');
            if ($_result)
                $_result = $this->usergroup->removeFromApplication($_application, 'ADMINS');
            if ($_result === false) {
                throw new Exception('Er is een fout opgetreden bij het intrekken van de toegang tot de applicatie.');
            }
            $_result = true;
        } else {
            throw new Exception('Er zijn te weinig gegevens voor het intrekken van de toegang tot de applicatie.');
        }

        return $_result;
    }

    function RevokeRole($record) {
        $this->AssertLoggedInUserIsAdmin();

        $_result = false;
        $this->usergroup = new base_plrUserGroup($this->polaris, $record['USER']);
        $_group = $record['GROUP'];

        if ($this->usergroup && !empty($_group)) {
            $_result = $this->usergroup->removeFromGroup($_group);
            if ($_result === false) {
                throw new Exception('Er is een fout opgetreden bij het intrekken van de gebruikerrol.');
            }
            $_result = true;
        } else {
            throw new Exception('Er zijn te weinig gegevens voor het intrekken van de gebruikerrol.');
        }

        return $_result;
    }

    function SuspendAccess($record) {
        $this->validateAdminAndUser($record['USER']);

        $_result = $this->usergroup->SuspendUserAccess();
        return $_result;
    }

    function GrantAccess($record) {
        $this->validateAdminAndUser($record['USER']);
        $_result = $this->usergroup->AllowUserAccess();
        return $_result;
    }

    function AssignAdminRole($record) {
        $this->validateAdminAndUser($record['USER']);
        $_result = $this->usergroup->GrantAdminRole();
        return $_result;
    }

    function RevokeAdminRole($record) {
        $this->validateAdminAndUser($record['USER']);
        $_result = $this->usergroup->RevokeAdminRole();
        return $_result;
    }

    function ResetPassword($record) {
        $this->AssertLoggedInUserIsAdmin();

        $this->usergroup = new base_plrUserGroup($this->polaris, $record['USER']);
        $_result = $this->usergroup->ChangePasswordNextLogin();
        return $_result;
    }

    function DeleteUser($record) {
        $this->validateAdminAndUser($record['USER']);

        try {
            $_result = $this->usergroup->removeUser();
        } catch (ADODB_Exception $e) {
            throw new Exception('Er is een fout opgetreden bij het verwijderen van de gebruiker.');
        } finally {
            // At least disable the account
            $this->usergroup->SuspendUserAccess();
        }
        return $_result;
    }

    function DeleteGroup($record) {
        $this->validateNotSpecialGroup($record['USER']);

        try {
            $_result = $this->usergroup->removeGroup();
        } catch (ADODB_Exception $e) {
            throw new Exception('Er is een fout opgetreden bij het verwijderen van de gebruiker.');
        } finally {
            // At least disable the account
            $this->usergroup->SuspendUserAccess();
        }
        return $_result;
    }

    function AddUserApplicationRoles($record) {
        $this->AssertLoggedInUserIsAdmin();

        $_result = true;
        $_userObject = new base_plrUserGroup($this->polaris, $record['USER']);

        foreach($record['ROLEGROUPS'] as $_rolegroup) {
            $_result = $_userObject->addToGroup($_rolegroup);
        }
        return $_result;
    }

    function AddUserRoles($record) {
        $this->AssertLoggedInUserIsAdmin();

        $_result = true;
        $_userObject = new base_plrUserGroup($this->polaris, $record['USER']);

        foreach($record['USERGROUPS'] as $_rolegroup) {
            $_result = $_userObject->addToGroup($_rolegroup);
        }
        return $_result;
    }

    function GetApplications($clientid) {
        $_sql = "
        SELECT
            a.APPLICATIONID,
            a.APPLICATIONNAME,
            a.METANAME,
            a.DESCRIPTION,
            a.APPTILECOLOR,
            a.IMAGE,
            g.ROLE,
            {$this->form->database->makeRecordIDColumn('plr_application', 'a')} AS PLR__RECORDID
        FROM
            plr_application a
        LEFT JOIN
            plr_applicationpermission ap
        ON a.CLIENTID = ap.CLIENTID AND a.APPLICATIONID = ap.APPLICATIONID
        LEFT JOIN
            plr_usergroup g
        ON ap.CLIENTID = g.CLIENTID AND ap.USERGROUPID = g.USERGROUPID
        WHERE
            a.CLIENTID = ?
            AND ap.PERMISSIONTYPE >= 4
        GROUP BY
            a.APPLICATIONID,
            a.APPLICATIONNAME,
            a.METANAME,
            a.DESCRIPTION,
            a.APPTILECOLOR,
            a.IMAGE,
            PLR__RECORDID
        HAVING ROLE IS NOT NULL
        ";
        $_rs = $this->form->database->userdb->Execute($_sql, [$clientid]);
        return $_rs;
    }

    function GetUserGroupStats() {
        $_customfilter = $this->form->AddDataScopeFilter();
        $_sql = "
        SELECT
            COUNT(*) AS TOTAL_USERS,
            SUM(CASE WHEN ACCOUNTDISABLED = 'N' THEN 1 ELSE 0 END) AS ACTIVE_USERS,
            SUM(CASE WHEN CLIENTADMIN = 'Y' THEN 1 ELSE 0 END) AS ACTIVE_ADMINS
        FROM
            PLR_USERGROUP ttt
        WHERE
            USERORGROUP = 'USER'
            AND {$_customfilter}
        ";
        $_rs = $this->form->database->userdb->GetRow($_sql);
        return $_rs;
    }

    function GetUngrantedAppsAndRoles($clientid, $userhash, $groupvalues) {

        $_sql = "
        SELECT
            a.APPLICATIONID,
            {$this->form->database->makeRecordIDColumn('plr_application', 'a')} AS APPLICATIONHASH,
            a.APPLICATIONNAME,
            a.METANAME,
            a.DESCRIPTION,
            a.APPTILECOLOR,
            a.IMAGE,
            (SELECT
                JSON_ARRAYAGG(
                    JSON_OBJECT(
                        'USER', '{$userhash}',
                        'ROLEGROUPHASH', {$this->form->database->makeRecordIDColumn('plr_usergroup', 'all_ap')},
                        'ROLENAME', all_g.FULLNAME
                    )
                )
            FROM
                plr_applicationpermission all_ap
            LEFT JOIN
                plr_usergroup all_g
            ON
                all_ap.CLIENTID = all_g.CLIENTID
                AND all_ap.USERGROUPID = all_g.USERGROUPID
            WHERE
                all_ap.APPLICATIONID = a.APPLICATIONID
                AND all_ap.CLIENTID = a.CLIENTID
                AND all_g.USERORGROUP = 'GROUP'
                AND all_g.USERGROUPID NOT IN ({$groupvalues})
            ) AS ROLES
        FROM
            plr_application a
        LEFT JOIN
            plr_applicationpermission ap
        ON
            a.CLIENTID = ap.CLIENTID
            AND a.APPLICATIONID = ap.APPLICATIONID
        LEFT JOIN
            plr_usergroup g
        ON
            ap.CLIENTID = g.CLIENTID
            AND ap.USERGROUPID = g.USERGROUPID
        WHERE
            a.CLIENTID = ?
            AND ap.PERMISSIONTYPE >= 4
        GROUP BY
            a.APPLICATIONID,
            a.APPLICATIONNAME,
            a.METANAME,
            a.DESCRIPTION,
            a.APPTILECOLOR,
            a.IMAGE
        HAVING ROLES IS NOT NULL
        ";
        $_rs = $this->form->database->userdb->GetAll($_sql, [$clientid]);

        // turn the 'ROLES' json string into an array
        foreach ($_rs as $key => $value) {
            $_rs[$key]['ROLES'] = json_decode($_rs[$key]['ROLES'], true);
        }
        return $_rs;
    }

    function GetUserAppsAndRoles($clientid, $userhash, $groupvalues) {

        $_sql = "
        SELECT
            a.APPLICATIONID,
            a.APPLICATIONNAME,
            a.METANAME,
            a.DESCRIPTION,
            a.APPTILECOLOR,
            a.IMAGE,
            JSON_ARRAYAGG(
                JSON_OBJECT(
                    'USER', '{$userhash}',
                    'GROUP', {$this->form->database->makeRecordIDColumn('plr_usergroup', 'ap')},
                    'ROLE', g.ROLE,
                    'GROUPNAME', g.FULLNAME,
                    'PERMISSION', ap.PERMISSIONTYPE
                )
            ) AS PERMISSIONS,
            GROUP_CONCAT(g.FULLNAME SEPARATOR ', ') AS PERMISSIONS_LABEL,
            (SELECT
                JSON_ARRAYAGG(
                    JSON_OBJECT(
                        'CLIENTID', all_g.CLIENTID,
                        'USER', '{$userhash}',
                        'GROUP', {$this->form->database->makeRecordIDColumn('plr_usergroup', 'all_ap')},
                        'APPLICATION', {$this->form->database->makeRecordIDColumn('plr_application', 'all_ap')},
                        'GROUPNAME', all_g.FULLNAME,
                        'PERMISSION', all_ap.PERMISSIONTYPE,
                        'CURRENTLYSELECTED', CASE WHEN all_ap.USERGROUPID IN ({$groupvalues}) THEN true ELSE false END
                    )
                )
            FROM
                plr_applicationpermission all_ap
            LEFT JOIN
                plr_usergroup all_g
            ON
                all_ap.CLIENTID = all_g.CLIENTID
                AND all_ap.USERGROUPID = all_g.USERGROUPID
            WHERE
                all_ap.APPLICATIONID = a.APPLICATIONID
                AND all_ap.CLIENTID = a.CLIENTID
                AND all_g.USERORGROUP = 'GROUP'
            ) AS ALL_PERMISSIONS
        FROM
            plr_application a
        LEFT JOIN
            plr_applicationpermission ap
        ON
            a.CLIENTID = ap.CLIENTID
            AND a.APPLICATIONID = ap.APPLICATIONID
        LEFT JOIN
            plr_usergroup g
        ON
            ap.CLIENTID = g.CLIENTID
            AND ap.USERGROUPID = g.USERGROUPID
        WHERE
            a.CLIENTID = ?
            AND ap.USERGROUPID IN ({$groupvalues})
            AND ap.PERMISSIONTYPE >= 4
        GROUP BY
            a.APPLICATIONID,
            a.APPLICATIONNAME,
            a.METANAME,
            a.DESCRIPTION,
            a.APPTILECOLOR,
            a.IMAGE
        ";
        $_rs = $this->form->database->userdb->GetAll($_sql, [$clientid]);

        // turn the 'PERMISSIONS' and 'ALL_PERMISSIONS' json string into an array
        foreach ($_rs as $key => $value) {
            $_rs[$key]['PERMISSIONS'] = json_decode($_rs[$key]['PERMISSIONS'], true);
            $_rs[$key]['ALL_PERMISSIONS'] = json_decode($_rs[$key]['ALL_PERMISSIONS'], true);
        }
        return $_rs;
    }

    function GetMembers($clientid, $groupID) {
        $_sql = "
        SELECT
            u.FULLNAME,
            u.DESCRIPTION,
            u.LASTLOGINDATE,
            u.ACCOUNTDISABLED,
            u.EMAILADDRESS,
            {$this->form->database->makeRecordIDColumn('plr_usergroup', 'u')} AS PLR__RECORDID
        FROM plr_usergroup u
        WHERE u.USERGROUPID IN (
            SELECT m.USERGROUPID
            FROM plr_membership m
            WHERE m.CLIENTID = m.CLIENTID AND m.GROUPID = ?
        )
        AND u.CLIENTID = ?
        ";
        $_rs = $this->form->database->userdb->GetAll($_sql, [$groupID, $clientid]);
        return $_rs;
    }

    function GetUserGroups($clientid, $groupvalues) {
        $_sql = "
        SELECT DISTINCT
            ug.USERGROUPID,
            ug.FULLNAME,
            ug.USERGROUPNAME,
            ug.DESCRIPTION,
            CASE
                WHEN m.USERGROUPID IS NOT NULL THEN 'Y'
                ELSE 'N'
            END AS IS_MEMBER,
            {$this->form->database->makeRecordIDColumn('plr_usergroup', 'ug')} AS PLR__RECORDID
        FROM
            plr_usergroup ug
        LEFT JOIN
            plr_membership m
        ON
            ug.CLIENTID = m.CLIENTID
            AND ug.USERGROUPID = m.GROUPID
            AND m.USERGROUPID IN ({$groupvalues})
        WHERE
            ug.CLIENTID = ?
            AND ug.USERORGROUP = 'GROUP'
            AND ug.ROLE NOT IN ('USERS', 'ADMINS', 'ORGADMINS')
        ";

        $_rs = $this->form->database->userdb->GetAll($_sql, [$clientid]);
        return $_rs;
    }

    function CreateAppSelectTag() {
        // Fill the modalform with the applications
        $_apps = $this->GetApplications($_SESSION['clientid']);
        $_selectTag = RecordSetAsSelectList(
            $_apps,
            keycolumn:'PLR__RECORDID',
            namecolumn:'APPLICATIONNAME',
            currentvalue:'',
            id:'ug-new-user-applications',
            selectname:'UGNEWUSERAPPLICATIONS[]',
            attr:"multiple='multiple'",
            required:true,
            data: [
                'select_width' => '100%',
                'placeholdertext' => 'Selecteer een of meerdere applicaties'
            ],
            multiselect:true
        );
        return $_selectTag;
    }

    function GetUsersAndGroups($clientid) {
        $_customfilter = $this->form->AddTagFilter();
        $_filterWhenEmpty = "USERORGROUP='USER' AND ACCOUNTDISABLED='N'";
        $_customfilter = empty($_customfilter) ? "AND ({$_filterWhenEmpty})" : "AND ({$_customfilter})";

        $_sql ="
		SELECT
            ttt.CLIENTID, ttt.USERGROUPID, ttt.USERGROUPNAME, ttt.USERORGROUP, ttt.ROLE, ttt.FULLNAME,
            ttt.DESCRIPTION, ttt.EMAILADDRESS, ttt.PHONENUMBER, ttt.PHOTOURL, ttt.CHANGEPASSWORDNEXTLOGON,
            ttt.USERCANNOTCHANGEPASSWORD, ttt.PASSWORDNEVEREXPIRES, ttt.ACCOUNTDISABLED, ttt.ACCOUNTCONFIRMED,
            ttt.LASTLOGINDATE, ttt.LASTPASSWORDCHANGEDATE, ttt.PASSWORDCHANGEDONDATE, ttt.AUTOCREATEMEMBERSHIP,
            ttt.CLIENTADMIN, ttt.ROOTADMIN, ttt.LOGINCOUNT, ttt.CREATEDATE, ttt.LANGUAGE, ttt.SERVERMAPPING,
            ttt.OPTIONS, ttt.PLRINSTANCE, ttt.COOKIE_GUID, ttt.CUSTOMCSS, ttt.USER_BACKGROUND, ttt.AUTHTYPE,
            IF(ttt.USERORGROUP = 'GROUP', COALESCE(ug.membercount, 0), NULL) AS MEMBERCOUNT,
            IF(ttt.USERORGROUP = 'GROUP', ug.MEMBERS, NULL) AS MEMBERS,
            {$this->form->database->makeRecordIDColumn('plr_usergroup', true)} AS PLR__RECORDID
        FROM
            plr_usergroup ttt
        LEFT JOIN (
            SELECT
                gm.CLIENTID,
                gm.GROUPID,
                COUNT(DISTINCT gm.MEMBERID) AS MEMBERCOUNT,
                JSON_ARRAYAGG(JSON_OBJECT('MEMBERID', gm.MEMBERID, 'MEMBERNAME', gm.MEMBERNAME)) AS MEMBERS
            FROM (SELECT DISTINCT CLIENTID, GROUPID, MEMBERID, USERORGROUP, MEMBERNAME
	            FROM
	                ( SELECT
		                m.CLIENTID,
	    	            m.GROUPID,
	        	        {$this->form->database->makeRecordIDColumn('plr_usergroup', 'm')} AS MEMBERID,
	            	    ug.USERORGROUP,
	                	u.FULLNAME AS MEMBERNAME
		            FROM
	    	            plr_membership m
	        	    JOIN
	            	    plr_usergroup ug
		            ON
	    	            m.CLIENTID = ug.CLIENTID
	        	        AND m.USERGROUPID = ug.USERGROUPID
		            LEFT JOIN
	    	            plr_usergroup u
	        	    ON
	            	    m.CLIENTID = u.CLIENTID
	                	AND m.USERGROUPID = u.USERGROUPID
		            WHERE
	    	            ug.USERORGROUP = 'USER'

	            UNION ALL

		            SELECT
		                m.CLIENTID,
		                gm.GROUPID,
		                {$this->form->database->makeRecordIDColumn('plr_usergroup', 'm')} AS MEMBERID,
		                ug.USERORGROUP,
		                u.FULLNAME AS MEMBERNAME
		            FROM
		                plr_membership m
		            JOIN
		                plr_membership gm
		            ON
		                m.CLIENTID = gm.CLIENTID
		                AND m.GROUPID = gm.USERGROUPID
		            JOIN
		                plr_usergroup ug
		            ON
		                m.CLIENTID = ug.CLIENTID
		                AND m.USERGROUPID = ug.USERGROUPID
		            LEFT JOIN
		                plr_usergroup u
		            ON
		                m.CLIENTID = u.CLIENTID
		                AND m.USERGROUPID = u.USERGROUPID
		            WHERE
		                ug.USERORGROUP = 'USER'


	            ) gm_distinct ORDER BY MEMBERNAME
	         ) gm
            GROUP BY
                gm.CLIENTID,
                gm.GROUPID
            ORDER BY gm.GROUPID
        ) ug
        ON
            ttt.CLIENTID = ug.CLIENTID
            AND ttt.USERGROUPID = ug.GROUPID
        WHERE
            ttt.CLIENTID = ? {$_customfilter}
        ORDER BY
            FIELD(ttt.ACCOUNTDISABLED, 'Y') ASC,
            FIELD(ttt.CLIENTADMIN, 'Y') DESC,
            ttt.FULLNAME ASC
        ";

        $_recordset = $this->form->database->userdb->GetAll($_sql, [$clientid]);
        foreach ($_recordset as $key => $value) {
            $_recordset[$key]['MEMBERS'] = json_decode($_recordset[$key]['MEMBERS'], true);
        }

        return $_recordset;
    }

    function ShowFormView($state, $permission, $detail=false, $masterrecord=false) {
        if ($state == 'edit') {
            $this->form->options->showbuttonbar = false;
            // $this->AssertLoggedInUserIsAdmin();

            $_customfilter = $this->__GetCustomFilter();
            $_recordset = $this->form->GetViewContent($detail=false, $masterrecord=false, $limit=false, $_customfilter)[0];
            $_userhash = $_recordset['PLR__RECORDID'];
            $_clientid = $_recordset['CLIENTID'];
            $_userid = $_recordset['USERGROUPID'];

            $this->usergroup = new base_plrUserGroup($this->polaris, $_userhash);

            if ($_recordset['USERORGROUP'] == 'USER') {
                $_groupvalues = GetGroupValues($this->polaris->instance, $_clientid, $_userid);

                $this->smarty->assign('userapps', $this->GetUserAppsAndRoles($_clientid, $_userhash, $_groupvalues));
                $this->smarty->assign('usergroups', $this->GetUserGroups($_clientid, $_userid));
                $this->smarty->assign('userhash', $_userhash);
                $this->smarty->assign('item', $_recordset);
                $this->smarty->assign('editing_yourself', ($_userhash == $_SESSION['userhash']) ? true : false);
                $_apps = $this->GetUngrantedAppsAndRoles($_clientid, $_userhash, $_groupvalues);
                $this->smarty->assign('applications', $_apps);
                $this->smarty->display("user_edit.tpl.php");
            } elseif ($_recordset['USERORGROUP'] == 'GROUP') {
                $this->smarty->assign('item', $_recordset);
                $this->smarty->assign('members', $this->GetMembers($_clientid, $_userid));
                $this->smarty->assign('grouphash', $_userhash);
                $this->smarty->display("group_edit.tpl.php");
            } else {
                echo "<div class='element-detail-box'>Gebruiker of groep niet gevonden.</div>";
            }
        }
    }

    function ShowListView($state, $permission, $detail=false, $masterrecord=false) {
        $this->smarty->assign('applications', $this->CreateAppSelectTag());
        $this->smarty->assign('items', $this->GetUsersAndGroups($_SESSION['clientid']));

        $_stats = $this->GetUserGroupStats();
        $this->smarty->assign('total_users', $_stats['TOTAL_USERS']);
        $this->smarty->assign('active_users', $_stats['ACTIVE_USERS']);
        $this->smarty->assign('actieve_admins', $_stats['ACTIVE_ADMINS']);

        $this->smarty->display("usergroup_listview.tpl.php");
    }

}
