<?php
require('_shared/module.plr_maxia.php');

class ModulePLR_Maxia_GeplandOnderhoud extends ModulePLR_Maxia {
    var $stylesheet;
    var $gen_form;
    var $processed;
    var $currentaction;

    function __construct($moduleid, $module, $clientname=false) {
        global $_GVARS;
        parent::__construct($moduleid, $module, $clientname);

        $this->processed = false;
        $this->stylesheet = 'default.css';
        $this->showControls = true;
        $this->showDefaultButtons = false;
    }

    function ProcessRequest($event=false) {
        $_content = false;

        $this->form->database->userdb->SetFetchMode(ADODB_FETCH_ASSOC);
        try {
            $_func = $_GET['event'];
            if (!empty($_func)) {
                switch($this->moduleid) {
                case 1:
                    switch ($_func) {
                        case 'events':
                        case 'resources':
                            $_clienthash = $_SESSION['clienthash'];
                            $_start_date = new DateTimeImmutable($_GET['start']);
                            $_end_date = new DateTimeImmutable($_GET['end']);

                            $_content = $this->GetGeplandOnderhoudData($_func, $_clienthash, $_start_date, $_end_date);
                        break;
                        default:
                        break;
                    }
                break;
                }
            }
            if ($_content !== false) {
                echo $_content;
                exit; // stop with the rest of the html
            }
        } catch (Exception $e) {
            echo "Wrong parameters: ".$e;
        }

        return false;
    }

    function GetGeplandOnderhoudData($_func, $_clientid, $_start, $_end) {
        global $scramble;

        $_sql = "
         SELECT ttt.PRETTYPLANNING,
         ttt.ONDERHOUD_ID,
         ttt.CLIENT_HASH ,
         sss7.soort_onderhoud_naam AS SOORT_ONDERHOUD_sss7,
         ttt.SOORT_ONDERHOUD ,
         sss5.categorie_naam AS CATEGORIE_sss5,
         ttt.CATEGORIE ,
         sss6.impact_naam AS IMPACT_sss6,
         ttt.IMPACT ,
         sss10.ontwikkelsfeer_naam AS ONTWIKKELSFEER_sss10,
         sss10.KLEUR AS ONTWIKKELSFEER_KLEUR,
         ttt.ONTWIKKELSFEER ,
         sss9.ruimte_select AS RUIMTE_sss9,
         sss9.ruimte_code AS RUIMTE_CODE,
         ttt.SOORT_UITVOERDER,
         sss11.relatie_select AS RELATIE_sss11,
         ttt.RUIMTE,
         ttt.ONDERHOUD_OMSCHRIJVING,
         ttt.AANTAL_KANDIDATEN,
         ttt.AANVRAGER_NAAM,
         ttt.SCHEMA_TYPE,
         ttt.SCHEMA_WEEK_HERHALING,
         ttt.SCHEMA_WEEK_HERHALING,
         ttt.SCHEMA_MAAND_HERHALING,
         ttt.SCHEMA_MAAND_START_DAG,
         ttt.SCHEMA_PERIODIEK ,
         DATE_FORMAT( ttt.SCHEMA_WEEK_START_DATUM, '%d-%m-%Y') as SCHEMA_WEEK_START_DATUM  ,
         DATE_FORMAT( ttt.SCHEMA_EENMALIG_START_DATUM, '%d-%m-%Y') as SCHEMA_EENMALIG_START_DATUM  ,
         DATE_FORMAT( ttt.SCHEMA_DAGELIJKS_START_DATUM, '%d-%m-%Y') as SCHEMA_DAGELIJKS_START_DATUM  ,
         DATE_FORMAT( ttt.SCHEMA_MAANDELIJKS_START_DATUM, '%d-%m-%Y') as SCHEMA_MAANDELIJKS_START_DATUM  ,
         ttt.SCHEMA_START_TIJD,
         ttt.MELDING_TIJD,
         ttt.STATUS,
         ttt.BESCHRIJVING,
         ttt.OPLOSSING ,
         MD5(CONCAT('{$scramble}','_', ttt.ONDERHOUD_ID))  AS PLR__RECORDID
         FROM fb_geplandonderhoud ttt
         LEFT JOIN fb_categorien sss5
            ON ttt.categorie=sss5.CATEGORIE_ID AND sss5.__DELETED = 0
        LEFT JOIN fb_impacts sss6
            ON ttt.impact=sss6.IMPACT_ID AND sss6.__DELETED = 0
        LEFT JOIN fb_soort_onderhoud sss7
            ON ttt.soort_onderhoud=sss7.SOORT_ONDERHOUD_ID AND sss7.__DELETED = 0
        LEFT JOIN fb_ruimtes sss9
            ON ttt.ruimte=sss9.RUIMTE_ID AND sss9.__DELETED = 0
        LEFT JOIN fb_ontwikkelsferen sss10
            ON ttt.ontwikkelsfeer=sss10.ONTWIKKELSFEER_ID AND sss10.__DELETED = 0 AND sss10.actief = 'Y'
        LEFT JOIN mx_relaties sss11
            ON ttt.externe_uitvoerder=sss11.RELATIE_ID
        WHERE (ttt.CLIENT_HASH = ?) AND ttt.SCHEMA_TYPE IS NOT NULL
        AND ttt.__DELETED = 0
        AND ttt.STATUS = 'Y'
        ORDER BY ONTWIKKELSFEER_sss10
        ";

        $_rs = $this->form->database->userdb->GetAll($_sql, array($_SESSION['clienthash']));

        $_result = array();
        if ($_rs) {
            switch ($_func) {
                case 'events':
                    $_result = $this->CreateEvents($_rs, $_start, $_end);
                break;
                case 'resources':
                    foreach ($_rs as $key => $value) {
                        if ($value['SOORT_UITVOERDER'] == 'I') {
                            $_group = $value['ONTWIKKELSFEER_sss10'];
                        } else {
                            $_group = $value['RELATIE_sss11'];
                        }
                        $_resource = array(
                            'id' => $value['ONDERHOUD_ID'],
                            'group' => $_group,
                            'title' => $value['ONDERHOUD_OMSCHRIJVING'].' ('.$value['AANTAL_KANDIDATEN'].' pers)',
                        );
                        $_result[] = $_resource;
                    }
                break;
            }
        }

        return json_encode($_result);
    }

    function WeekPatternToArray($pattern) {
        $_days = ['Mon','Tue','Wed','Thu','Fri','Sat','Sun'];
        for($i=0; $i <= 6; $i++) {
            if (($pattern & pow(2, $i)) == pow(2, $i)) {
                $_result[] = ($_days[$i]);
            }
        }
        return $_result;
    }

    function MonthPatternToArray($pattern) {
        for($i=0; $i <= 11; $i++) {
            if (($pattern & pow(2, $i)) == pow(2, $i)) {
                $_result[] = $i + 1;
            }
        }
        return $_result;
    }

    function AddTime($date, $time) {
        $_time = explode(':', $time);
        $modified = clone $date;
        // $date->add(new DateInterval('PT'.$_time[0].'H'.$_time[1].'M0S'));
        $modified = $modified->modify('+'.$_time[0].'hours +'.$_time[1].'minutes +0seconds');

        return $modified;
    }

    function DetermineTime($date, $daypart) {
        $startTime = clone $date;

        $tijdVerschillen = array(
            'ochtend'   => array('+8 hours', '+4 hours', false),
            'middag'    => array('+13 hours', '+4 hours', false),
            'heledag'   => array('+8 hours', '+9 hours', true),
            'tweedagen' => array('+0 hours', '+2 days', true),
            'driedagen' => array('+0 hours', '+3 days', true),
            'vierdagen' => array('+0 hours', '+4 days', true),
            'vijfdagen' => array('+0 hours', '+5 days', true),
            'eenweek'   => array('+0 hours', '+1 week', true),
            'tweeweken' => array('+0 hours', '+2 weeks', true),
            'drieweken' => array('+0 hours', '+3 weeks', true),
            'vierweken' => array('+0 hours', '+1 month', true),
        );

        $tijdVerschil = $tijdVerschillen[$daypart] ?? array('+8 hours', '+9 hours', true);
        $startTime = $startTime->modify($tijdVerschil[0]);
        $endTime = $startTime->modify($tijdVerschil[1]);

        return array($startTime, $endTime, $tijdVerschil[2]);
    }

    function CreateEventsNew($planning, $start, $end) {
        $_events = array();

        $_interval = $start->diff($end, true);
        foreach($planning as $item) {
            $_title = $item['ONDERHOUD_OMSCHRIJVING'];
            if (!empty($item['RUIMTE_CODE']))
                $_title .= ' ('.$item['RUIMTE_CODE'] . ')';
            if (!empty($item['ONTWIKKELSFEER_sss10']))
                $_title .= ' - '.$item['ONTWIKKELSFEER_sss10'];
            $_description = $item['PRETTYPLANNING'];
            $_url = '/app/facility/const/geplandonderhoud/edit/'.$item['PLR__RECORDID'].'/';
            $_backgroundcolor = $item['ONTWIKKELSFEER_KLEUR'];

            switch($item['SCHEMA_TYPE']) {
                case 'D': // Dag
                    $_dailystart = new DateTimeImmutable($item['SCHEMA_DAGELIJKS_START_DATUM']);

                    for ($i=0; $i < $_interval->days; $i++) {
                        $_iday = $start->modify("+$i day");
                        list($_idayStart, $_idayEnd, $_allDay) = $this->DetermineTime($_iday, $item['SCHEMA_START_TIJD']);

                        if (($_iday >= $_dailystart)
                        and ($end >= $_dailystart)) {
                            $_events[] = array(
                                'resourceId' => $item['ONDERHOUD_ID'],
                                'title' => $_title,
                                'description' => $_description,
                                'start' => $_idayStart->format('Y-m-d\TH:i:s') . '+00:00',
                                'end' => $_idayEnd->format('Y-m-d\TH:i:s') . '+00:00',
                                'url' => $_url,
                                'backgroundColor' => $_backgroundcolor,
                                'borderColor' => $_backgroundcolor,
                            );
                        }
                    }
                break;
                case 'W': // Week
                    $_weekRepeat = $this->WeekPatternToArray($item['SCHEMA_WEEK_HERHALING']);
                    $_weekStart = new DateTimeImmutable($item['SCHEMA_WEEK_START_DATUM']);
                    $weekPeriodiek = intval($item['SCHEMA_PERIODIEK']) ?? 1;
                    for ($i=0; $i < $_interval->days; $i++) {
                        $_iday = $start->modify("+$i day");

                        $_weeklyStart = $_weekStart->modify("+$weekPeriodiek weeks");

                        if ($_iday >= $_weekStart) {
                            $_diffDays = $_weekStart->diff($_iday)->days;
                            $_diffWeeks = floor($_diffDays / 7) + 1;

                            // var_dump($_diffDays);
                            // var_dump('_iday: '.$_iday->format('d-m-D'));
                            // var_dump('_diffWeeks: '.$_diffWeeks);
                            // var_dump('mod '. $_diffWeeks % 4);
                            // var_dump('_weekStart: '.$_weekStart->format('d-m'));
                            if (($_iday >= $start)
                                and ($_iday <= $end)
                                // and ($_iday >= $_weekStart)
                                and (
                                    ($weekPeriodiek == 1 and $_diffWeeks % 1 == 0) or // Event occurs every week
                                    ($weekPeriodiek == 2 and $_diffWeeks % 2 == 1) or // Event occurs every two weeks
                                    ($weekPeriodiek == 3 and $_diffWeeks % 3 == 1) // Event occurs every three weeks
                                    // ($weekPeriodiek == 4 and $_diffWeeks % 4 == 1)    // Event occurs every four weeks
                                ) and in_array($_iday->format('D'), $_weekRepeat)
                            ) {
                                // var_dump('found: '.$_iday->format('d-m'));
                                // echo "\r\n";
                                list($_idayStart, $_idayEnd, $_allDay) = $this->DetermineTime($_iday, $item['SCHEMA_START_TIJD']);

                                $_events[] = array(
                                    'resourceId' => $item['ONDERHOUD_ID'],
                                    'title' => $_title,
                                    'description' => $_description,
                                    'start' => $_idayStart->format('Y-m-d\TH:i:s') . '+00:00',
                                    'end' => $_idayEnd->format('Y-m-d\TH:i:s') . '+00:00',
                                    'url' => $_url,
                                    'backgroundColor' => $_backgroundcolor,
                                    'borderColor' => $_backgroundcolor,
                                );
                            }
                        }

                        // $_weekStart = $_weeklyStart;

                        // Update the reference date for the current interval
                        $reference_date_interval = clone $_weekStart;
                        $reference_date_interval->modify("+$weekPeriodiek weeks");

                        if ($_iday >= $reference_date_interval) {
                            $_weekStart->modify("+$weekPeriodiek weeks");
                        }
                    }
                break;
                case 'M': // Maand
                    $_fromDate = new DateTimeImmutable($item['SCHEMA_MAANDELIJKS_START_DATUM']);
                    list($_idayStart, $_idayEnd, $_allDay) = $this->DetermineTime($start, $item['SCHEMA_START_TIJD']);

                    $_rrule = array(
                        'freq' => 'weekly',
                        'interval' => 1,
                        'byday' => 'tu',
                        'byhour' => 8,
                        // 'byweekday' => [ 'mo', 'fr' ],
                        'dtstart' => $_fromDate->format('Y-m-d\TH:i:s') . '+00:00',
                        'until' => $_fromDate->modify("+3 years")->format('Y-m-d\TH:i:s') . '+00:00'
                    );

                    $_events[] = array(
                        'resourceId' => $item['ONDERHOUD_ID'],
                        'title' => $_title,
                        'description' => $_description,
                        'url' => $_url,
                        // 'allDay' => $_allDay,
                        'backgroundColor' => $_backgroundcolor,
                        'borderColor' => $_backgroundcolor,
                        'rrule' => $_rrule,
                        'duration' => '03:00',
                    );
                break;
                case 'E': // Eenmalig
                    $_oneOffDate = new DateTimeImmutable($item['SCHEMA_EENMALIG_START_DATUM']);
                    list($_oneOffDateStart, $_oneOffDateEnd, $_allDay) = $this->DetermineTime($_oneOffDate, $item['SCHEMA_START_TIJD']);

                    $_events[] = array(
                        'resourceId' => $item['ONDERHOUD_ID'],
                        'title' => $_title,
                        'description' => $_description,
                        'start' => $_oneOffDateStart->format('Y-m-d\TH:i:s') . '+00:00',
                        'end' => $_oneOffDateEnd->format('Y-m-d\TH:i:s') . '+00:00',
                        'url' => $_url,
                        'backgroundColor' => $_backgroundcolor,
                        'borderColor' => $_backgroundcolor,
                    );
                break;
            }
        }

        return $_events;
    }

    function CreateEvents($planning, $start, $end) {
        $_events = array();

        $_interval = $start->diff($end, true);
        foreach($planning as $item) {
            $_title = $item['ONDERHOUD_OMSCHRIJVING'];
            if (!empty($item['RUIMTE_CODE']))
                $_title .= ' ('.$item['RUIMTE_CODE'] . ')';
            if (!empty($item['ONTWIKKELSFEER_sss10']))
                $_title .= ' - '.$item['ONTWIKKELSFEER_sss10'];
            if (!empty($item['RELATIE_sss11']))
                $_title .= ' - '.$item['RELATIE_sss11'];
            $_description = $item['PRETTYPLANNING'];
            $_url = '/app/facility/const/geplandonderhoud/edit/'.$item['PLR__RECORDID'].'/';
            if ($item['SOORT_UITVOERDER'] == 'I') {
                $_backgroundcolor = $item['ONTWIKKELSFEER_KLEUR'];
            } else {
                // externe uitvoerder = donker groen
                $_backgroundcolor = '#0E5C4C';
            }

            switch($item['SCHEMA_TYPE']) {
                case 'D': // Dag
                    $_dailystart = new DateTimeImmutable($item['SCHEMA_DAGELIJKS_START_DATUM']);

                    for ($i=0; $i < $_interval->days; $i++) {
                        $_iday = $start->modify("+$i day");
                        list($_idayStart, $_idayEnd, $_allDay) = $this->DetermineTime($_iday, $item['SCHEMA_START_TIJD']);

                        if (($_iday >= $_dailystart)
                        and ($end >= $_dailystart)) {
                            $_events[] = array(
                                'resourceId' => $item['ONDERHOUD_ID'],
                                'title' => $_title,
                                'description' => $_description,
                                'start' => $_idayStart->format('Y-m-d\TH:i:s') . '+00:00',
                                'end' => $_idayEnd->format('Y-m-d\TH:i:s') . '+00:00',
                                'url' => $_url,
                                'backgroundColor' => $_backgroundcolor,
                                'borderColor' => $_backgroundcolor,
                            );
                        }
                    }
                break;
                case 'W': // Week
                    $_weekRepeat = $this->WeekPatternToArray($item['SCHEMA_WEEK_HERHALING']);
                    $_weekStart = new DateTimeImmutable($item['SCHEMA_WEEK_START_DATUM']);
                    $weekPeriodiek = intval($item['SCHEMA_PERIODIEK']) ?? 1;
                    for ($i=0; $i < $_interval->days; $i++) {
                        $_iday = $start->modify("+$i day");

                        $_weeklyStart = $_weekStart->modify("+$weekPeriodiek weeks");

                        if ($_iday >= $_weekStart) {
                            $_diffDays = $_weekStart->diff($_iday)->days;
                            $_diffWeeks = floor($_diffDays / 7) + 1;

                            // var_dump($_diffDays);
                            // var_dump('_iday: '.$_iday->format('d-m-D'));
                            // var_dump('_diffWeeks: '.$_diffWeeks);
                            // var_dump('mod '. $_diffWeeks % 4);
                            // var_dump('_weekStart: '.$_weekStart->format('d-m'));
                            if (($_iday >= $start)
                                and ($_iday <= $end)
                                // and ($_iday >= $_weekStart)
                                and (
                                    ($weekPeriodiek == 1 and $_diffWeeks % 1 == 0) or // Event occurs every week
                                    ($weekPeriodiek == 2 and $_diffWeeks % 2 == 1) or // Event occurs every two weeks
                                    ($weekPeriodiek == 3 and $_diffWeeks % 3 == 1) // Event occurs every three weeks
                                    // ($weekPeriodiek == 4 and $_diffWeeks % 4 == 1)    // Event occurs every four weeks
                                ) and in_array($_iday->format('D'), $_weekRepeat)
                            ) {
                                // var_dump('found: '.$_iday->format('d-m'));
                                // echo "\r\n";
                                list($_idayStart, $_idayEnd, $_allDay) = $this->DetermineTime($_iday, $item['SCHEMA_START_TIJD']);

                                $_events[] = array(
                                    'resourceId' => $item['ONDERHOUD_ID'],
                                    'title' => $_title,
                                    'description' => $_description,
                                    'start' => $_idayStart->format('Y-m-d\TH:i:s') . '+00:00',
                                    'end' => $_idayEnd->format('Y-m-d\TH:i:s') . '+00:00',
                                    'url' => $_url,
                                    'backgroundColor' => $_backgroundcolor,
                                    'borderColor' => $_backgroundcolor,
                                );
                            }
                        }

                        // $_weekStart = $_weeklyStart;

                        // Update the reference date for the current interval
                        $reference_date_interval = clone $_weekStart;
                        $reference_date_interval->modify("+$weekPeriodiek weeks");

                        if ($_iday >= $reference_date_interval) {
                            $_weekStart->modify("+$weekPeriodiek weeks");
                        }
                    }
                break;
                case 'M': // Maand
                    $_monthRepeat = $this->MonthPatternToArray($item['SCHEMA_MAAND_HERHALING']);
                    $_fromDate = new DateTimeImmutable($item['SCHEMA_MAANDELIJKS_START_DATUM']);
                    $_monthDay = $item['SCHEMA_MAAND_START_DAG'];
                    for ($i=0; $i < $_interval->days; $i++) {
                        $_iday = $start->modify("+$i day");
                        list($_idayStart, $_idayEnd, $_allDay) = $this->DetermineTime($_iday, $item['SCHEMA_START_TIJD']);
                        if ($_iday->format('d') == $_monthDay
                            and in_array($_iday->format('n'), $_monthRepeat)
                        ) {
                            $_events[] = array(
                                'resourceId' => $item['ONDERHOUD_ID'],
                                'title' => $_title,
                                'description' => $_description,
                                'start' => $_idayStart->format('Y-m-d\TH:i:s') . '+00:00',
                                'end' => $_idayEnd->format('Y-m-d\TH:i:s') . '+00:00',
                                'url' => $_url,
                                'allDay' => $_allDay,
                                'backgroundColor' => $_backgroundcolor,
                                'borderColor' => $_backgroundcolor,
                            );
                        }
                    }
                break;
                case 'E': // Eenmalig
                    $_oneOffDate = new DateTimeImmutable($item['SCHEMA_EENMALIG_START_DATUM']);
                    list($_oneOffDateStart, $_oneOffDateEnd, $_allDay) = $this->DetermineTime($_oneOffDate, $item['SCHEMA_START_TIJD']);

                    $_events[] = array(
                        'resourceId' => $item['ONDERHOUD_ID'],
                        'title' => $_title,
                        'description' => $_description,
                        'start' => $_oneOffDateStart->format('Y-m-d\TH:i:s') . '+00:00',
                        'end' => $_oneOffDateEnd->format('Y-m-d\TH:i:s') . '+00:00',
                        'url' => $_url,
                        'backgroundColor' => $_backgroundcolor,
                        'borderColor' => $_backgroundcolor,
                    );
                break;
            }
        }

        return $_events;
    }

    function ShowListView($state, $permission, $detail=false, $masterrecord=false) {
        $this->smarty->assign('stylesheet', $this->stylesheet);
        $this->AddCustomJS(['/modules/_shared/module.plr_maxia/js/maxia.js']);

        switch($this->moduleid) {
        case 1:
            $this->AddModuleJS(['maxia_geplandonderhoud.js']);
            echo $this->smarty->Fetch('geplandonderhoud_kalender.tpl.php');
        break;
        }
    }

}
