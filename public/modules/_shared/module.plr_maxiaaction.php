<?php
require_once('module.plr_executeaction.php');

class ModulePLR_MaxiaAction extends ModulePLR_ExecuteAction {

    function __construct($moduleid, $module, $clientname=false) {
        parent::__construct($moduleid, $module);
    }

    /**
     * Reservations module
     */
    function CheckReservationStatus($rowids, $status) {
        global $scramble;

        $this->lastStatement = "
        SELECT COUNT(*) FROM fb_reserveringen_intern
        WHERE  MD5(CONCAT(?,'_', AANVRAAG_ID)) IN ( ".$rowids." )
        AND STATUS <> ?
        ";
        return $this->userdatabase->GetOne($this->lastStatement, array($scramble, $status)) == 0;
    }

    private function _ProcessReservation($rowids, $state) {
        global $scramble;

        $this->lastStatement = "
        UPDATE fb_reserveringen_intern SET status = ?
        WHERE  MD5(CONCAT(?,'_', AANVRAAG_ID)) IN ( ".$rowids." )
        ";
        $this->userdatabase->Execute($this->lastStatement, array($state, $scramble));
        return $this->userdatabase->Affected_Rows();
    }

    function ReservationPrinted($rowids) {
        $_result = false;
        if ($this->CheckReservationStatus($rowids, 'nieuw')) {
            $this->_ProcessReservation($rowids, 'afgedrukt');
            $_result = true;
        }
        return $_result;
    }

    function ReservationProcessed($rowids) {
        $_new = $this->CheckReservationStatus($rowids, 'nieuw');
        $_processed = $this->CheckReservationStatus($rowids, 'afgehandeld');
        if (!$_new and !$_processed) {
            $this->_ProcessReservation($rowids, 'afgehandeld');
            $_result = true;
        } else {
            if ($_new)
                $_result = 'Deze reservering moet nog afgedrukt worden.';
            else
                $_result = 'Deze reservering is al afgehandeld.';
        }
        return $_result;
    }

    /**
     * Issue module
     */

    function CheckIssueStatus($rowids, $status) {
        global $scramble;

        if (empty($rowids)) return false;

        // Turn Array('a', 'b') into "a","b"
        $_statuslist = substr(json_encode($status), 1, -1);
        $this->lastStatement = "
        SELECT * FROM fb_meldingen
        WHERE MD5(CONCAT(?,'_', MELDING_ID)) IN ( {$rowids} )
        AND STATUS NOT IN ( {$_statuslist} )
        ";
        return ($this->userdatabase->GetOne($this->lastStatement, array($scramble)) == 0);
    }

    private function _ProcessIssue($rowids, $state, $resetmailnotification=FALSE) {
        global $scramble;

        $this->lastStatement = "UPDATE fb_meldingen SET status = ?";

        if ($resetmailnotification == TRUE) {
            $this->lastStatement .= ", __ALERTSTATUS = NULL ";
        }
        if ($state == 'afgehandeld') {
            $this->lastStatement .= ", PERCENTAGEVOLTOOID = 100";
        }

        $this->lastStatement .= " WHERE MD5(CONCAT(?,'_', MELDING_ID)) IN ( ".$rowids." )";

        try {
            $this->userdatabase->Execute($this->lastStatement, array($state, $scramble));
        } catch (exception $E) {
            return $E->getMessage();
        }

        return $this->userdatabase->Affected_Rows() > 0;
    }

    function IssueApproved($rowids) {
        $_result = false;
        if ($this->CheckIssueStatus($rowids, ['nieuw', 'afgekeurd', 'goed'])) {
            $this->_ProcessIssue($rowids, 'goed');
            $_result = true;
        }
        return $_result;
    }

    function IssueRejected($rowids) {
        $_result = false;
        if ($this->CheckIssueStatus($rowids, ['nieuw', 'goed'])) {
            $this->_ProcessIssue($rowids, 'afgekeurd');
            $_result = true;
        }
        return $_result;
    }

    function IssuePrinted($rowids) {
        $_result = false;
        if ($this->CheckIssueStatus($rowids, ['goed'])) {
            $this->_ProcessIssue($rowids, 'inbehandeling');
            $_result = true;
        }
        return $_result;
    }

    function IssueEscalated($rowids) {
        $_result = false;
        if (!$this->CheckIssueStatus($rowids, ['escalatie'])) {
            $this->_ProcessIssue($rowids, 'escalatie', resetmailnotification: TRUE);
            $_result = true;
        } else {
            $_result = 'Deze melding is al geëscaleerd.';
        }
        return $_result;
    }

    function IssueDeescalated($rowids) {
        $_result = false;
        if ($this->CheckIssueStatus($rowids, ['escalatie'])) {
            $this->_ProcessIssue($rowids, 'escalatie_update', resetmailnotification: TRUE);
            $_result = true;
        } else {
            $_result = 'Deze melding is niet geëscaleerd.';
        }
        return $_result;
    }

    function IssueInProcess($rowids) {
        $_new = $this->CheckIssueStatus($rowids, ['nieuw']);
        $_approved = $this->CheckIssueStatus($rowids, ['goed']);
        if (!$_new and $_approved) {
            $this->_ProcessIssue($rowids, 'inbehandeling');
            $_result = true;
        } else {
            if ($_new)
                $_result = 'Deze melding moet nog goedgekeurd worden.';
            else
                $_result = 'Deze melding is al verwerkt.';
        }
        return $_result;
    }

    function IssueProcessed($rowids) {
        $_new = $this->CheckIssueStatus($rowids, ['nieuw']);
        $_processed = $this->CheckIssueStatus($rowids, ['afgehandeld']);
        if (!$_new and !$_processed) {
            $this->_ProcessIssue($rowids, 'afgehandeld');
            $_result = true;
        } else {
            if ($_new)
                $_result = 'Deze melding moet nog afgedrukt worden.';
            else
                $_result = 'Deze melding is al afgehandeld.';
        }
        return $_result;
    }

    function IssueChangeStatus($rowids, $allesRecord, $parameters) {
        global $scramble;

        $_valid_status = array('goed', 'afgekeurd');
        $_nieuwe_status = $parameters[1];
        if (!in_array($_nieuwe_status, $_valid_status))
            return false;

        $_inbehandeling = $this->CheckIssueStatus($rowids, ['inbehandeling']);
        $_isgeescaleerd = $this->CheckIssueStatus($rowids, ['escalatie']);
        if (($_nieuwe_status == 'goed'
            and !$_inbehandeling and !$_isgeescaleerd)
        or ($_nieuwe_status == 'afgekeurd'
            and !$_inbehandeling)
        ) {
            $this->_ProcessIssue($rowids, $_nieuwe_status);
            $_result = true;
        } else {
            if ($_inbehandeling)
                $_result = 'Deze melding is in behandeling en kan niet zomaar goed- of afgekeurd worden.';
            elseif ($_isgeescaleerd)
                $_result = 'Deze melding is geëscaleerd en kan niet zomaar goedgekeurd worden. Vraag of de melding gedeëscaleerd kan worden.';
            else
                $_result = 'Deze melding is al verwerkt.';
        }
        return $_result;
    }

    function IssueShouldPrint($rowids, $allesRecord, $parameters) {
        $_result = true;
        // Voorlopig geen check
        // $_result = $this->CheckIssueStatus($rowids, ['goed','inbehandeling']);
        // if (!$_result) {
        //     $_result = 'U kunt alleen meldingen afdrukken die zijn goedgekeurd.';
        // }
        return $_result;
    }

    function AddCommentToIssue($rowids, $allesRecord, $parameters) {
        global $scramble;

        $_result = true;
        try {
            if (!empty(trim($parameters[1]))) {
                $_opmerking = "\n\n{$parameters[1]} [{$_SESSION['fullname']}]";
                // Prepare the statement
                $this->lastStatement = "UPDATE fb_meldingen
                                        SET OPMERKINGEN = CONCAT(COALESCE(OPMERKINGEN, ''), ?)
                                        WHERE MD5(CONCAT(?,'_', MELDING_ID)) IN ($rowids)";

                // Execute the statement with parameters
                $this->userdatabase->Execute($this->lastStatement, [$_opmerking, $scramble]);
            }
        } catch (Exception $e) {
            // Log the error message
            return $e->getMessage();
        }
        // Return the result
        return $_result;
    }

    function VerwerkDeelnemerExitformulier($rowids, $allesRecord, $parameters) {
        global $scramble;

        $_recordid = $allesRecord['ROWID'];
        $_actiefInactief = $allesRecord['param2'];
        $_vandaag = date('Y-m-d');
        if ($_actiefInactief == 1) { // Actief
            $_uitstroomdatum = null;
            $_uitstroomreden = '';
            $_notitie = $allesRecord['param5'];
        } else {
            $_uitstroomdatum = $allesRecord['param3'];
            if (empty($_uitstroomdatum)) {
                $_uitstroomdatum = $_vandaag;
            } else {
                $_uitstroomdatum = DateTime::createFromFormat('d-m-Y', $_uitstroomdatum)->format('Y-m-d');
            }
            $_uitstroomreden = $allesRecord['param4'];
            $_notitie = $allesRecord['param5'];
        }

        try {
            $this->lastStatement = "
                UPDATE do_deelnemers
                SET uitstroom_datum = ?, uitstroom_reden = ?
                , uitstroom_notitie = ?, deelnemer_status = ?
                WHERE MD5(CONCAT(?,'_', deelnemer_id, '_', CLIENT_HASH)) = ?
                AND CLIENT_HASH = ?
            ";
            $this->userdatabase->Execute($this->lastStatement, [$_uitstroomdatum, $_uitstroomreden, $_notitie, $_actiefInactief, $scramble, $_recordid, $_SESSION['clienthash']]);

            $this->lastStatement = "SELECT deelnemer_id FROM do_deelnemers WHERE MD5(CONCAT(?,'_', deelnemer_id, '_', CLIENT_HASH)) = ?";
            $_deelnemer = $this->userdatabase->GetOne($this->lastStatement, [$scramble, $_recordid]);

            $this->lastStatement = "
                INSERT INTO do_deelnemerstatussen (deelnemer, datum_vanaf, status, notitie, client_hash)
                VALUES (?, ?, ?, ?, ?)
            ";
            $this->userdatabase->Execute($this->lastStatement, [$_deelnemer, $_vandaag, $_actiefInactief, $_notitie, $_SESSION['clienthash']]);

            if ($_actiefInactief == '2') {
                // verwijder deelnemer uit de tabel do_activiteit_deelnemers vanaf de datum van uitstroom
                $this->lastStatement = "UPDATE do_activiteit_deelnemers
                    SET __DELETED = 1
                    WHERE MD5(CONCAT(?,'_', deelnemer, '_', CLIENT_HASH)) = ?
                    AND activiteit IN (
                        SELECT activiteit_id
                        FROM do_activiteiten
                        WHERE tijdstip_aanvang > ?
                        AND CLIENT_HASH = ?
                    )";
                $this->userdatabase->Execute($this->lastStatement, [$scramble, $_recordid, $_uitstroomdatum, $_SESSION['clienthash']]);
            }
        } catch (Exception $e) {
            return 'Er is een fout opgetreden';
        }
        return true;
    }

    function VerstuurMailUitnodiging($rowids, $allesRecord, $parameters) {
        global $scramble;

        $deelnemer_hash = $allesRecord['ROWID'];
        $_sql = "
            SELECT deelnemer_id, email
            FROM do_deelnemers ttt
            WHERE {$this->dbobject->makeRecordIDColumn('do_deelnemers', true)} = ?
        ";
        $deelnemer = $this->userdatabase->GetRow($_sql, [$deelnemer_hash]);

        if (!$deelnemer['deelnemer_id']) {
            return 'Deelnemer niet gevonden';
        }
        if (!$deelnemer['email'] || !filter_var($deelnemer['email'], FILTER_VALIDATE_EMAIL)) {
            return 'Deelnemer heeft geen (valide) e-mailadres';
        }

        $_sqlInsert = "
            INSERT INTO do_positievegezondheid
            (client_hash, deelnemer, gebruiker, gebruikernaam, status, versie, soortvragenlijst)
            VALUES (?, ?, ?, ?, ?, ?, ?)
        ";
        $_status = '2'; // 2 = Mail is verstuurd, wordt opgepikt door plrAlerter
        $_versie = $_SESSION['clientparameters']['AVLVRS'] ?? 1;
        $_soortvragenlijst = $allesRecord['param2'] ?? '1';
        try {
            $this->userdatabase->Execute($_sqlInsert, [$_SESSION['clienthash'], $deelnemer['deelnemer_id'], $_SESSION['userid'], $_SESSION['fullname'], $_status, $_versie, $_soortvragenlijst]);
        } catch (Exception $e) {
            return $e->getMessage();
        }
        return true;
    }

    /**
     * General processing function for all Maxia modules
     */
    function ProcessAction($actionid, $moduleid, $allesRecord, $parameters=false) {
        $_result = false;
        $_rowids = $allesRecord['ROWIDFILTER'] ?? false;
        switch($moduleid) {
        case 'reservering_afgedrukt':
            $_result = $this->ReservationPrinted($_rowids);
            break;
        case 'reservering_afgehandeld':
            $_result = $this->ReservationProcessed($_rowids);
            break;
        case 'melding_goedkeuren':
            $_result = $this->IssueApproved($_rowids);
            break;
        case 'melding_afkeuren':
            $_result = $this->IssueRejected($_rowids);
            break;
        case 'melding_afgedrukt':
            $_result = $this->IssuePrinted($_rowids);
            break;
        case 'melding_inbehandeling':
            $_result = $this->IssueInProcess($_rowids);
            break;
        case 'melding_escaleren':
            $_result = $this->IssueEscalated($_rowids);
            break;
        case 'melding_deescaleren':
            $_result = $this->IssueDeescalated($_rowids);
            break;
        case 'melding_afgehandeld':
            $_result = $this->IssueProcessed($_rowids);
            break;
        case 'melding_wijzigstatus':
            $_result = $this->IssueChangeStatus($_rowids, $allesRecord, $parameters);
            break;
        case 'melding_afdrukcheck':
            $_result = $this->IssueShouldPrint($_rowids, $allesRecord, $parameters);
            break;
        case 'melding_opmerking':
            $_result = $this->AddCommentToIssue($_rowids, $allesRecord, $parameters);
            break;
        case 'deelnemer_exitformulier':
            $_result = $this->VerwerkDeelnemerExitformulier($_rowids, $allesRecord, $parameters);
            break;
        case 'deelnemer_mailuitnodiging':
            $_result = $this->VerstuurMailUitnodiging($_rowids, $allesRecord, $parameters);
            break;
        default:
        break;
        }
        return $_result;
    }
}