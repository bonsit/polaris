MaxiaGeplandOnderhoud = {
    calendar: null,
    initialiseer: function() {
        var Self = MaxiaGeplandOnderhoud;

        Self.initialiseerKalender();
        Maxia.initialiseerKalenderEvents(Self.calendar);
    },
    initialiseerKalender: function() {
        var Self = MaxiaGeplandOnderhoud;

        if ($('#facility_calendar').length > 0) {
            var bewaardeView = Maxia.getKalenderView('customWeek');
            var bewaardeDate = Maxia.getKalenderDatum(new Date());

            Self.calendar = new FullCalendar.Calendar($('#facility_calendar')[0], {
                schedulerLicenseKey: '0445008747-fcs-1681420837',
                initialView: bewaardeView,
                initialDate: bewaardeDate,
                height: $(window).height()*0.80,
                editable: true,
                locale: 'nl',
                firstDay: 1,
                timeZone: 'Europe/Amsterdam',
                slotMinTime: '08:00',
                slotMaxTime: '18:00',
                eventDisplay: 'block',
                resourceOrder: 'group',
                resourceAreaWidth: '20%',
                dayPopoverFormat: { month: 'long', day: 'numeric', year: 'numeric' },
                headerToolbar: {
                    start: 'customWeek,customMonth,customYear,customResource,customListWeek', // will normally be on the left. if RTL, will be on the right
                    center: 'title',
                    end: 'today,prev,next' // will normally be on the right. if RTL, will be on the left
                },
                buttonText: {
                    'today': 'Vandaag'
                },
                views: {
                    customWeek: {
                        type: 'timeGridWeek',
                        buttonText: 'Week',
                    },
                    customMonth: {
                        type: 'dayGridMonth',
                        buttonText: 'Maand',
                    },
                    customYear: {
                        type: 'dayGridYear',
                        buttonText: 'Jaar',
                    },
                    customResource: {
                        type: 'resourceTimelineMonth',
                        buttonText: 'Per ontwikkelsfeer',
                    },
                    customListWeek: {
                        type: 'listWeek',
                        buttonText: 'Lijst',
                    },
                },
                viewDidMount: function(cal) {
                    Maxia.bewaarKalenderView(cal);
                    Maxia.bewaarKalenderDatum(Self.calendar);
                },
                eventDidMount: function(info) {
                    $(info.el).popover({
                        placement:'bottom',
                        container:'body',
                        delay: {
                            'show':300,
                            'hide': 0
                        },
                            trigger:'hover',
                        html:true,
                        content:'<b>'+info.event.title + '</b><br><br>' + info.event.extendedProps.description
                    });
                },
                droppable: true, // this allows things to be dropped onto the calendar
                drop: function() {
                    // is the "remove after drop" checkbox checked?
                    if ($('#drop-remove').is(':checked')) {
                        // if so, remove the element from the "Draggable Events" list
                        $(this).remove();
                    }
                },
                resourceGroupField: 'group',
                resourceAreaColumns: [
                    {
                      field: 'title',
                      headerContent: 'Onderhoud'
                    }
                ],
                resources: '/services/json/app/facility/form/geplandonderhoud_data/?event=resources',
                events: '/services/json/app/facility/form/geplandonderhoud_data/?event=events',
            });
            Self.calendar.render();
        }
    },
};

jQuery(function () {
    MaxiaGeplandOnderhoud.initialiseer();
});
