{include file="shared.tpl.php"}

<div class="element-detail-box">
    <div id="urenregistratie" class="timetracker">
        <div class="row mb2">
            <div class="col-md-11">
                <h3>Uren registreren 29 april 2023</h3>
            </div>
            <div class="col-md-1">
                <button id="go-today" class="btn btn-success btn-outline">Vandaag</button>
            </div>
        </div>
        <div class="row mb2">
            <div class="col-md-11">
                <nav aria-label="Week navigatie">
                    <ul class="pagination" id="dayselection">
                        <li class="button scrollweek" id="prev-week">
                            <a href="#" aria-label="{lang navigate_prev}">
                                <span aria-hidden="true"><i class="fa fa-chevron-left"></i></span>
                            </a>
                        </li>
                        <li class="day button"><a href="#"><b>ma 24</b><br/>00:00</a></li>
                        <li class="day button"><a href="#"><b>di&nbsp;25</b><br/>00:00</a></li>
                        <li class="day button"><a href="#"><b>wo&nbsp;26</b><br/>00:00</a></li>
                        <li class="day button"><a href="#"><b>do&nbsp;27</b><br/>00:00</a></li>
                        <li class="day button"><a href="#"><b>vr&nbsp;28</b><br/>00:00</a></li>
                        <li class="day button"><a href="#"><b>za&nbsp;29</b><br/>00:00</a></li>
                        <li class="day button"><a href="#"><b>zo&nbsp;30</b><br/>00:00</a></li>
                        <li class="button scrollweek" id="next-week">
                            <a href="#" aria-label="{lang navigate_next}">
                                <span aria-hidden="true"><i class="fa fa-chevron-right"></i></span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
            <div class="col-md-1">
                <nav aria-label="Week navigatie">
                    <ul class="pagination">
                        <li class="weektotal day"><a href="#"><b>Weektotaal</b><br/>00:00</a></a></li>
                    </ul>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="col-md-5">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="input3" class="col-sm-2 control-label">Van</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="xcol-md-12">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <input type="text" class="form-control" id="input3">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="input3" class="col-sm-2 control-label">Tot</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="xcol-md-12">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <input type="text" class="form-control" id="input3">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="input3" class="col-sm-2 control-label">Pauzeduur</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="xcol-md-12">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <input type="text" class="form-control" id="input3">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="input3" class="col-sm-2 control-label">Contact</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <select class="select2 form-control" id="input10">
                                            <optgroup label="Alaskan/Hawaiian Time Zone" data-select2-id="select2-data-64-avo2">
                                                <option value="AK" data-select2-id="select2-data-60-5zu3">Alaska</option>
                                                <option value="HI" data-select2-id="select2-data-65-t1ri">Hawaii</option>
                                            </optgroup>
                                            <optgroup label="Pacific Time Zone" data-select2-id="select2-data-66-ttpw">
                                                <option value="CA" data-select2-id="select2-data-67-l7ph">California</option>
                                                <option value="NV" data-select2-id="select2-data-68-1sjm">Nevada</option>
                                                <option value="OR" data-select2-id="select2-data-69-pib0">Oregon</option>
                                                <option value="WA" data-select2-id="select2-data-70-6iag">Washington</option>
                                            </optgroup>
                                            <optgroup label="Mountain Time Zone" data-select2-id="select2-data-71-u7sj">
                                                <option value="AZ" data-select2-id="select2-data-72-bmmk">Arizona</option>
                                                <option value="CO" data-select2-id="select2-data-73-o392">Colorado</option>
                                                <option value="ID" data-select2-id="select2-data-74-vcmz">Idaho</option>
                                                <option value="MT" data-select2-id="select2-data-75-1tg7">Montana</option>
                                                <option value="NE" data-select2-id="select2-data-76-4fg7">Nebraska</option>
                                                <option value="NM" data-select2-id="select2-data-77-perz">New Mexico</option>
                                                <option value="ND" data-select2-id="select2-data-78-w998">North Dakota</option>
                                                <option value="UT" data-select2-id="select2-data-79-hmwx">Utah</option>
                                                <option value="WY" data-select2-id="select2-data-80-w1a8">Wyoming</option>
                                            </optgroup>
                                            <optgroup label="Central Time Zone" data-select2-id="select2-data-81-hclb">
                                                <option value="AL" data-select2-id="select2-data-82-tf03">Alabama</option>
                                                <option value="AR" data-select2-id="select2-data-83-g2a3">Arkansas</option>
                                                <option value="IL" data-select2-id="select2-data-84-cgis">Illinois</option>
                                                <option value="IA" data-select2-id="select2-data-85-rmr2">Iowa</option>
                                                <option value="KS" data-select2-id="select2-data-86-borh">Kansas</option>
                                                <option value="KY" data-select2-id="select2-data-87-1i4d">Kentucky</option>
                                                <option value="LA" data-select2-id="select2-data-88-4sg4">Louisiana</option>
                                                <option value="MN" data-select2-id="select2-data-89-fwkp">Minnesota</option>
                                                <option value="MS" data-select2-id="select2-data-90-36sa">Mississippi</option>
                                                <option value="MO" data-select2-id="select2-data-91-gvxk">Missouri</option>
                                                <option value="OK" data-select2-id="select2-data-92-3k2k">Oklahoma</option>
                                                <option value="SD" data-select2-id="select2-data-93-zpqn">South Dakota</option>
                                                <option value="TX" data-select2-id="select2-data-94-3b27">Texas</option>
                                                <option value="TN" data-select2-id="select2-data-95-7yf3">Tennessee</option>
                                                <option value="WI" data-select2-id="select2-data-96-aor8">Wisconsin</option>
                                            </optgroup>
                                            <optgroup label="Eastern Time Zone" data-select2-id="select2-data-97-wcwj">
                                                <option value="CT" data-select2-id="select2-data-98-w870">Connecticut</option>
                                                <option value="DE" data-select2-id="select2-data-99-ub17">Delaware</option>
                                                <option value="FL" data-select2-id="select2-data-100-9hji">Florida</option>
                                                <option value="GA" data-select2-id="select2-data-101-ohw9">Georgia</option>
                                                <option value="IN" data-select2-id="select2-data-102-feif">Indiana</option>
                                                <option value="ME" data-select2-id="select2-data-103-ek72">Maine</option>
                                                <option value="MD" data-select2-id="select2-data-104-aul7">Maryland</option>
                                                <option value="MA" data-select2-id="select2-data-105-is82">Massachusetts</option>
                                                <option value="MI" data-select2-id="select2-data-106-qkv1">Michigan</option>
                                                <option value="NH" data-select2-id="select2-data-107-cf2p">New Hampshire</option>
                                                <option value="NJ" data-select2-id="select2-data-108-wrl4">New Jersey</option>
                                                <option value="NY" data-select2-id="select2-data-109-ccvj">New York</option>
                                                <option value="NC" data-select2-id="select2-data-110-zawg">North Carolina</option>
                                                <option value="OH" data-select2-id="select2-data-111-sbn9">Ohio</option>
                                                <option value="PA" data-select2-id="select2-data-112-jvz5">Pennsylvania</option>
                                                <option value="RI" data-select2-id="select2-data-113-q9rl">Rhode Island</option>
                                                <option value="SC" data-select2-id="select2-data-114-v35s">South Carolina</option>
                                                <option value="VT" data-select2-id="select2-data-115-j00p">Vermont</option>
                                                <option value="VA" data-select2-id="select2-data-116-r90k">Virginia</option>
                                                <option value="WV" data-select2-id="select2-data-117-j73s">West Virginia</option>
                                            </optgroup>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="input3" class="control-label">Omschrijving</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="input3" placeholder="Tijd">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="input3" class="control-label">Project</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <select class="select2 form-control" id="input3" ><option></option></select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                &nbsp;
                            </div>
                            <div class="col-md-2 starttimer">
                                <div class="form-group pull-right">
                                    <button class="btn btn-lg btn-primary">Start timer</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>