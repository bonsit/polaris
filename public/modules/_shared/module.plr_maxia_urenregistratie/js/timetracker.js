TimeTracker = {

    dayOptions : {
        weekday: "short",
        day: "2-digit"
    },

    initialize: function() {
        var Self = TimeTracker;

        Self.initializeTimetracking()
    },

    formatMinutes: function(totalminutes) {
        var hours = Math.floor(totalminutes / 60);
        var minutes = totalminutes % 60;
        return hours + ":" + minutes;
    },

    formatDay: function(day, totalhours) {
        return '<b>' + day.toLocaleDateString("nl", this.dayOptions) + '</b><br>' + this.formatMinutes(totalhours);
    },

    initializeTimetracking: function() {
        var Self = TimeTracker;

        // Get the current date
        var today = new Date();

        // Set the date to Monday of the current week
        var monday = new Date(today.getFullYear(), today.getMonth(), today.getDate() - today.getDay() + 0);

        // Set the date of the first list item to Monday of the current week
        $('#dayselection li.day:first a').html(monday.toDateString());

        // Set the date of the remaining list items to the subsequent days of the week
        for (var i = 1; i <= 7; i++) {
          var nextDay = new Date(monday);
          nextDay.setDate(monday.getDate() + i);
          $('#dayselection li.day:nth-child(' + (i+1) + ') a').html(this.formatDay(nextDay, 0));
        }

        // Handler for the "Vorige week" button
        $('#prev-week').click(function() {
          monday.setDate(monday.getDate() - 7);

          // Update the list items with the new dates
          for (var i = 0; i <= 7; i++) {
            var nextDay = new Date(monday);
            nextDay.setDate(monday.getDate() + i);
            $('#dayselection li.day:nth-child(' + (i+1) + ') a').html(this.formatDay(nextDay, 0));
          }
        });

        // Handler for the "Volgende week" button
        $('#next-week').click(function() {
          monday.setDate(monday.getDate() + 7);

          // Update the list items with the new dates
          for (var i = 0; i <= 7; i++) {
            var nextDay = new Date(monday);
            nextDay.setDate(monday.getDate() + i);
            $('#dayselection li.day:nth-child(' + (i+1) + ') a').html(this.formatDay(nextDay, 0));
          }
        });

        // Handler for the "Vandaag" button
        $('#go-today').click(function(e) {
            e.preventDefault();
            today = new Date();
            monday = new Date(today.getFullYear(), today.getMonth(), today.getDate() - today.getDay() + 1);

            // Update the list items with the new dates
            $('#dayselection li.day:first a').html(monday.toDateString());
            for (var i = 1; i < 7; i++) {
                var nextDay = new Date(monday);
                nextDay.setDate(monday.getDate() + i);
                $('#dayselection li.day:nth-child(' + (i+1) + ') a').html(this.formatDay(nextDay, 0));
            }
        });
    },
};

$(document).ready(function() {
    var Self = TimeTracker;

    Self.initialize();
});
