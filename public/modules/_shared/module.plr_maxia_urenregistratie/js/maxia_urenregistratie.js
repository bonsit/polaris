MaxiaUrenRegistratie = {
    initialiseer: function() {
        if ($("#dayselection").length > 0) {
            $("#dayselection li").click(function(e) {
                $('#dayselection li').removeClass('active');
                $(this).addClass('active');
            })
        }
    },

};

jQuery(function () {
    MaxiaUrenRegistratie.initialiseer();
});
