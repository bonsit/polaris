Maxia.Logboek = {};
Base.update(Maxia.Logboek, {
    logboekModel: null,
    logboekData: null,
    logboekSelectie: null,
    logboekWeergave: null,
    logboekPeriode: null,
    logboekKeuzeDatum: null,
    fm: null,
    init: function() {
        var Self = this;

        Self.initLogboek();
        Self.initLiveZoeken();
        Self.initLogboekNav();
        Self.initWeergave();
        Self.initKalender();
        Self.initInputVelden($(".logboek-notitie-input"));

        $(window).on('logboek_item_toegevoegd', function() {
            Self.refreshLogboek();
        });

        $("#logboek-prullenbak-leegmaken").on('click', function(e) {
            e.preventDefault();
            if (confirm('Weet u zeker dat u de prullenbak wilt legen?')) {
                var data = {
                    'event':'logboek_prullenbak_leegmaken'
                    , '_hdnProcessedByModule':'true'
                };
                Polaris.Ajax.postJSON(_servicequery, data, null, null, function(result) {
                    Self.refreshLogboek();
                });
            }
        });

        // Feature section add logboek item
        $("#logboek-quick-notitie-opslaan").on('click', function(e) {
            e.preventDefault();

            var data = {
                'event':'logboek_toevoegen'
                , '_hdnProcessedByModule':'true'
                , 'NOTITIE': $("#logboek-quick-input").val()
            };
            query = _serverroot + '/services/json/app/facility/const/mx_logboek/';
            Polaris.Ajax.postJSON(query, data, null, null, function(result) {
                Polaris.Base.modalMessage('Notitie toegevoegd');
                $(window).trigger('logboek_item_toegevoegd');
            });
        });

        $("#btn-logboek-item-bewaar").on('click', function(e) {
            e.preventDefault();
            Self.updateLogboekItem(this);
        });

        $("#logboek-notitie-opslaan").on('click', function(e) {
            e.preventDefault();
            Self.postLogboekItem(function() {
                Self.sluitInputveld();
            });
        });

        $("#logboek-periode-selectie button").on('click', function(e) {
            e.preventDefault();
            $("#logboek-periode-selectie button").removeClass('active');
            $(this).addClass('active');
            Maxia.bewaarLogboekPeriode($(this).data('periode'));
            Self.logboekPeriode = $(this).data('periode');
            Self.refreshLogboek();
            $("#logboek-nav-kalender").toggle(Self.logboekPeriode == 'keuze');
        });
        Self.logboekPeriode = Maxia.getLogboekPeriode('week');
        //  select button with data attribute equal to the value of the variable periode
        $("#logboek-periode-selectie button[data-periode='" + Self.logboekPeriode + "']").addClass('active');
        $("#logboek-nav-kalender").toggle(Self.logboekPeriode == 'keuze');

        Self.refreshLogboek();
    },
    removeEmptyLinesAtEnd: function(text) {
        // Split the text into lines
        const lines = text.split('\n');

        // Iterate over the lines from the end and remove empty lines
        let lastNonEmptyIndex = lines.length - 1;
        for (let i = lines.length - 1; i >= 0; i--) {
            if (lines[i].trim() === '') {
                lastNonEmptyIndex = i - 1;
            } else {
                break;
            }
        }

        // Join the non-empty lines and return the result
        const trimmedText = lines.slice(0, lastNonEmptyIndex + 1).join('\n');
        return trimmedText;
    },
    initInputVelden: function(textareas) {
        var Self = this;

        var enterInputVeld = function(elem) {
            if (elem.rows < 2) { elem.rows = $(elem).data('ori_rows') || 2; }

            // if there are actions for the logboek input field, show them
            $(".logboek-notitie-acties", $(elem).parent()).show();
        };

        var leaveInputVeld = function(elem) {
            if (elem.value == '') {
                elem.rows = $(elem).data('ori_rows');
                $(".logboek-notitie-acties", $(elem).parent()).hide();
            }
        };

        // When key Shift + Enter is pressed, submit the form
        textareas.on('keydown', function(e) {
            if (e.shiftKey && e.which == 13) {
                e.preventDefault();
                Self.postLogboekItem();
            }
            if (e.which == 13) {
                // if previous line starts with '- [ ]', add a new line with '- [ ]'
                // and position cursor at the end of the line
                var lines = $(this).val().split('\n');
                var lastLine = lines[lines.length - 1];
                if (lastLine.startsWith('- [ ]') || lastLine.startsWith('- [x]')) {
                    if (lastLine.trim() == '- [ ]') {
                        // remove last line from textarea
                        lines.pop();
                        $(this).val(lines.join('\n'));
                        this.rows -= 1;
                    } else {
                        e.preventDefault();
                        $(this).val($(this).val() + '\n- [ ] ');
                        var textarea = $(this).get(0);
                        var pos = $(this).val().length;
                        textarea.setSelectionRange(pos, pos);
                        this.rows += 1;
                        this.style.height = (this.scrollHeight) + 20 + 'px';
                    }
                }
            }
        })
        .on('mousedown', function(e) {
            e.stopPropagation();
            enterInputVeld(this);
        })
        .on('focusout', function(e) {
            e.stopPropagation();
            leaveInputVeld(this);
        })
        .on('keypress', function(e) {
            // if user presses enter the logboek-item rows should be increased
            if (e.which == 13) {
                // e.preventDefault();
                e.stopPropagation();
                this.rows += 1;
                this.style.height = 'auto';
                this.style.height = (this.scrollHeight) + 20 + 'px';
            }
        });

        textareas.each(function() {
            var elem = $(this);
            var itemlijstButton = '<div class="logboek-notitie-itemlijst btn btn-circle btn-default btn-sm" data-toggle="tooltip" title="Nieuwe lijst"><i class="material-symbols-outlined">check_box</i></div>';
            elem.before(itemlijstButton);

            $(".logboek-notitie-itemlijst", elem.parent()).on('click', function(e) {
                e.preventDefault();
                enterInputVeld(elem);
                Self.voegCheckboxToeAanHuidigeRegel(elem[0]);
                elem.trigger('focus');
            });
        });

    },
    voegCheckboxToeAanHuidigeRegel: function(textarea) {
        // Get the current cursor position
        var cursorPos = textarea.selectionStart;

        // Get the content of the textarea
        var content = textarea.value;

        // Find the start and end indices of the current line
        var lineStart = content.lastIndexOf('\n', cursorPos - 1) + 1;
        var lineEnd = content.indexOf('\n', cursorPos);
        if (lineEnd === -1) {
            lineEnd = content.length;
        }

        // Extract the current line
        var line = content.substring(lineStart, lineEnd);

        // Define the checkbox prefix
        var checkboxPrefix = "- [ ] ";

        // Check if the line already starts with the checkbox prefix
        if (!line.startsWith(checkboxPrefix)) {
            // If it doesn't, add the checkbox prefix to the beginning of the line
            var modifiedLine = checkboxPrefix + line;

            // Update the textarea content with the modified line
            var modifiedContent = content.substring(0, lineStart) + modifiedLine + content.substring(lineEnd);

            // Set the modified content back to the textarea
            textarea.value = modifiedContent;

            // Move the cursor to the end of the modified line
            textarea.selectionStart = lineStart + modifiedLine.length;
            textarea.selectionEnd = lineStart + modifiedLine.length;
        }
    },
    initLiveZoeken: function() {
        const Self = Maxia.Logboek;

        let searchField = $("#_hdnGlobalSearch");

        searchField
        .on('keypress', function(e) {
            if (e.which == 13) {
                e.preventDefault();
                e.stopPropagation();
            }
        })
        .on('keyup', function(e) {
            clearTimeout($(this).data('timer'));
            var search = this.value;
            if (search.length >= 1) {
                $(this).data('timer', setTimeout(function() {
                    Self.refreshLogboek(searchField.val());
                }, 250));
            } else {
                Self.refreshLogboek();
            }
        });
    },
    initLogboek: function() {
        const Self = Maxia.Logboek;

        this.logboekModel = $.views.viewModels({
            getters: ['logboekitems', 'selectie', 'weergave', 'aantalFavorieten'],
        });
        $.views.helpers(Self.renderHelpers);
        $.views.converters("lower", function(val) {
            if (val)
                return val.toLowerCase();
        });

        $.templates("logboekTemplate", {
            markup: "#template-logboek-view",
            templates: {
                templateLogboekItemView: "#template-logboekitem-view",
            }
        });
    },
    initCheckboxes: function() {
        var Self = this;

        $(".logboek-item input[type=checkbox]")
        .on('change', function(event) {
            var tekst = $(this).parent().text().trim();
            var id = Self.zoekElement(event).data('id');
            var checked = $(this).is(':checked');
            Self.toggleLogboekItemCheckbox(id, tekst, checked);
        })
        .each(function() {
            var elem = $(this);
            elem.attr('disabled', false);
        });
    },
    initLogboekNav: function() {
        var Self = this;
        $("#logboek-nav a").on('click', function(e) {
            e.preventDefault();
            $("#logboek-nav a").removeClass('active');
            $(this).addClass('active');
            Self.logboekSelectie = $(this).data('selectie');
            Self.logboekData = Self.logboekModel(Self.logboekData.logboekitems, Self.logboekSelectie);
            // Toon of verberg de juiste divs
            $("#logboek-notitie-control").toggle(['actief','sjablonen'].includes(Self.logboekSelectie));
            $("#logboek-prullenbak").toggle(Self.logboekSelectie == 'prullenbak');
            Self.refreshLogboek();
        });
        Maxia.Logboek.logboekSelectie = $("#logboek-nav a.active").data('selectie');
    },
    initWeergave: function() {
        var Self = this;
        $("#logboek-weergave-types button").on('click', function(e) {
            e.preventDefault();
            $("#logboek-weergave-types button").removeClass('active');
            $(this).addClass('active');
            Self.logboekWeergave = $(this).data('type');
            Maxia.bewaarLogboekWeergave(Self.logboekWeergave);

            $("#logboek-view-style").removeClass(['logboek-view-list', 'logboek-view-grid']);
            $("#logboek-view-style").addClass('logboek-view-' + Self.logboekWeergave);
            Self.refreshLogboek();
        });
        Self.logboekWeergave = Maxia.getLogboekWeergave('list');
        $("#logboek-weergave-types button[data-type='" + Self.logboekWeergave + "']").addClass('active');
        $("#logboek-view-style").addClass('logboek-view-' + Self.logboekWeergave);
    },
    initKalender: function() {
        var Self = this;

        Self.logboekKeuzeDatum = Maxia.getLogboekKeuzeDatum();

        var datepicker = $("#logboek-nav-kalender").datepicker({
            inline: true,
            showOtherMonths: true,
            todayHighlight: true,
            weekStart: 1,
            calendarWeeks: true,
            todayBtn: true,
            language: "nl",
            dayNamesMin: ['Zo', 'Ma', 'Di', 'Wo', 'Do', 'Vr', 'Za'],
            dateFormat: 'dd-mm-yyyy'
        });

        // First set the date, and then add the event listener
        // because the event listener will trigger a refresh of the logbook
        datepicker.datepicker('setDate', Self.logboekKeuzeDatum);

        datepicker.on('changeDate', function(e) {
            Self.logboekKeuzeDatum = $("#logboek-nav-kalender").data('datepicker').getFormattedDate();
            Maxia.bewaarLogboekKeuzeDatum(Self.logboekKeuzeDatum);

            Maxia.Logboek.refreshLogboek();
        });

    },
    toggleLogboekItemCheckbox: function(id, tekst, checked) {
        var Self = this;

        var data = {
            'event':'logboek_item_toggle_checkbox',
            '_hdnProcessedByModule':'true',
            'PLR__RECORDID': id,
            'checkbox': tekst,
            'aanuit': checked ? 'aan' : 'uit',
        };

        Polaris.Ajax.postJSON(_servicequery, data, null, null, function(result) {
            if (result.result) {
                Polaris.Base.modalMessage('Aangepast');
                Maxia.Logboek.refreshLogboek();
            }
        });
    },
    sluitInputveld: function() {
        $("#logboek-input").val('');
        $("#logboek-input").attr('rows', 1);
        $("#logboek-notitie-acties").hide();
    },
    zoekElement: function(event) {
        var elem = $(event.target);
        if (!elem.hasClass('logboek-item')) {
            elem = elem.parents('.logboek-item');
        }
        return elem;
    },

    // Define a function to handle common logbook actions
    handleLogbookAction: function(elem, event, aanuit, callback) {
        var Self = this;

        // Default value for 'aanuit' parameter
        aanuit = aanuit || 'aan';

        var data = {
            'event': event,
            '_hdnProcessedByModule': 'true',
            'aanuit': aanuit,
            'PLR__RECORDID': elem.data('id')
        };

        Polaris.Ajax.postJSON(_servicequery, data, null, null, function(result) {
            if (callback) callback(result);
            Self.refreshLogboek();
        });
    },

    // Specific logbook action functions
    pinLogboekItem: function(elem, aanuit, callback) {
        this.handleLogbookAction(elem, 'logboek_item_vastpinnen', aanuit, callback);
    },

    archiveerLogboekItem: function(elem, aanuit, callback) {
        this.handleLogbookAction(elem, 'logboek_item_archiveren', aanuit, callback);
    },

    herstelLogboekItem: function(elem, callback) {
        this.handleLogbookAction(elem, 'logboek_item_herstellen', null, callback);
    },

    verwijderLogboekItem: function(elem, callback) {
        this.handleLogbookAction(elem, 'logboek_item_verwijderen', null, callback);
    },

    verwijderPermanentLogboekItem: function(elem, callback) {
        this.handleLogbookAction(elem, 'logboek_item_permanentverwijderen', null, callback);
    },

    kopieerLogboekItem: function(elem, callback) {
        this.handleLogbookAction(elem, 'logboek_item_kopieren', null, callback);
    },
    wijzigCheckboxes: function(elem, callback) {
        this.handleLogbookAction(elem, 'logboek_item_wijzig_checkboxes', null, callback);
    },

    updateLogboekItem: function(elem, callback) {
        var Self = this;

        var form = $("#dlgWijzigLogboekItem");
        var data = {
            'event':'logboekitem_updaten'
            , '_hdnProcessedByModule':'true'
            , 'PLR__RECORDID':form.find(":input[name=_hdnRecordID]").val()
            , 'NOTITIE': form.find(":input[name=OPMERKING]").val()
        };

        Polaris.Ajax.postJSON(_servicequery, data, null, null, function(result) {
            form.find(":input[name=OPMERKING]").val('');
            $("#dlgWijzigLogboekItem").modal('hide');
            if (callback) callback(result);
            Self.refreshLogboek();
        }, function() {
            if (callback) callback(result);
        });

    },

    postLogboekItem: function(callback) {
        var Self = this;

        var notitie = $("#logboek-input").val();

        if (isNullOrEmpty(notitie)) {
            if (callback) callback();
            return;
        }

        var notitiestatus = Self.logboekSelectie == 'sjablonen' ? 'sjablonen' : 'actief';

        var data = {
            'event':'logboek_toevoegen'
            , '_hdnProcessedByModule':'true'
            , 'PLR__RECORDID':''
            , '_hdnState':'insert'
            , 'STATUS': notitiestatus
            , 'NOTITIE': $("#logboek-input").val()
        };

        Polaris.Ajax.postJSON(_servicequery, data, null, null, function(result) {
            $("#logboek-input").val('');
            if (callback) callback(result);
            Self.refreshLogboek();
        }, function() {
            if (callback) callback(result);
        });
    },
    refreshLogboek: function(searchvalue) {
        var Self = this;

        if (Maxia.getGetVar('q[]')) {
            searchvalue = Maxia.getGetVar('q[]');
        }

        Self.getLogboek(searchvalue, function(logboek, textStatus) {
            if (textStatus == 'success') {
                $('.tooltip').tooltip('hide');
                var aantalFavorieten = logboek.filter(function(item) { return item.FAVORIET == 'Y'; }).length;
                Self.logboekData = Self.logboekModel(logboek, Self.logboekSelectie, Self.logboekWeergave, aantalFavorieten);
                $.templates("#template-logboek-view").link("#logboek-view", Self.logboekData);

                Self.initCheckboxes();
            } else {
                alert('De items konden niet geladen worden. Error: ' + textStatus);
            }
        });
    },
    getLogboekDetails: function(id, callback) {
        return $.getJSON(_servicequery, {
            'event': 'logboek_item_details',
            'id': id
        }, callback);
    },
    getLogboek: function(searchvalue, callback) {
        return $.getJSON(_servicequery, {
            'event': 'logboek',
            'selectie': Maxia.Logboek.logboekSelectie,
            'periode': Maxia.Logboek.logboekPeriode,
            'datum': Maxia.Logboek.logboekKeuzeDatum,
            'q[]': searchvalue || ''
        }, callback);
    },
    renderHelpers: {
        /**
         * Dit zijn de functies die enkele eigenschappen berekenen in onze template.
         * We geven ze door bij het koppelen van gegevens aan onze template
         * en gebruiken ze met het ~-teken, bijvoorbeeld: {:~getBezetting()}
         */
        pinnedFiltering: function(item, index, items) {
            var Self = this;
            return item.FAVORIET == 'Y';
        },
        unpinnedFiltering: function(item, index, items) {
            var Self = this;
            return item.FAVORIET == 'N';
        },
        select: function(ev, eventArgs) {
        },
        wijzigLogitem: function(ev, eventArgs) {
            ev.stopPropagation()
            Maxia.Logboek.wijzigLogboekItem(Maxia.Logboek.zoekElement(ev));
        },
        pinLogitem: function(ev, eventArgs) {
            ev.stopPropagation()
            Maxia.Logboek.pinLogboekItem(Maxia.Logboek.zoekElement(ev), 'aan');
        },
        unpinLogitem: function(ev, eventArgs) {
            ev.stopPropagation()
            Maxia.Logboek.pinLogboekItem(Maxia.Logboek.zoekElement(ev), 'uit');
        },
        verwijderLogitem: function(ev, eventArgs) {
            ev.stopPropagation()
            Maxia.Logboek.verwijderLogboekItem(Maxia.Logboek.zoekElement(ev));
        },
        verwijderPermanentLogitem: function(ev, eventArgs) {
            ev.stopPropagation()
            Maxia.Logboek.verwijderPermanentLogboekItem(Maxia.Logboek.zoekElement(ev));
        },
        herstelLogitem: function(ev, eventArgs) {
            ev.stopPropagation()
            Maxia.Logboek.herstelLogboekItem(Maxia.Logboek.zoekElement(ev));
        },
        archiveerLogitem: function(ev, eventArgs) {
            ev.stopPropagation()
            Maxia.Logboek.archiveerLogboekItem(Maxia.Logboek.zoekElement(ev), 'aan');
        },
        dearchiveerLogitem: function(ev, eventArgs) {
            ev.stopPropagation()
            Maxia.Logboek.archiveerLogboekItem(Maxia.Logboek.zoekElement(ev), 'uit');
        },
        kopieerLogitem: function(ev, eventArgs) {
            ev.stopPropagation()
            Maxia.Logboek.kopieerLogboekItem(Maxia.Logboek.zoekElement(ev));
        },
        wijzigCheckboxes: function(ev, eventArgs) {
            ev.stopPropagation();
            Maxia.Logboek.wijzigCheckboxes(Maxia.Logboek.zoekElement(ev));
        },
    },
    wijzigLogboekItem: function(elem) {
        Self = this;

        var id = elem.data('id');

        $(".dropdown.open").toggle();

        Self.getLogboekDetails(id, function(data) {
            var form = $("#dlgWijzigLogboekItem");
            var opmerkingVeld = form.find(":input[name=OPMERKING]");
            form.find(":input[name=_hdnRecordID]").val(data.PLR__RECORDID);
            opmerkingVeld.val(data.OPMERKING);
            form.modal('show');
            opmerkingVeld.trigger('focus');
            // wijzig de rows property van het textarea element, afhangend van het aantal regels in de opmerking
            // maar niet minder dan 5 en niet meer dan 20
            var rows = Math.min(20, Math.max(1, data.OPMERKING.split(/\r\n|\r|\n/).length));
            opmerkingVeld.attr('rows', rows);
            opmerkingVeld.css('height', rows * 2.5 + 'em');
        });
    },
});

jQuery(function () {
    Maxia.Logboek.init();
});
