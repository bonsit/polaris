{include file="shared.tpl.php"}

<div id="logboek-notitie-control" class="logboek-notitie-controlx">
    <textarea data-ori_rows="4" rows="4" id="logboek-quick-input" class="logboek-notitie-input xform-control" placeholder="{lang add_logbook_entry}…"></textarea>
</div>

<button type="button" tabindex="8800" id="logboek-quick-notitie-opslaan" class="btn btn-primary jqmAccept positive">Opslaan</button>
<button type="button" tabindex="8810" id="logboek-notitie-annuleren" class="btn btn-default jqmClose secondary">Annuleer</button>

<script type="text/javascript" src="{$serverpath}/modules/_shared/module.plr_maxia/js/maxia.js"></script>
<script type="text/javascript" src="{$serverpath}{$modulepath}js/logboek_main.js"></script>
