</form>

<div class="modal inmodal" id="dlgWijzigLogboekItem" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content fadeIn">
            <form class="form-horizontal" id="frmWijzigLogboekItem">
                <input type="hidden" name="event" value="logboekItemWijzigen" />
                <input type="hidden" name="_hdnRecordID" value="" />
                <input type="hidden" name="_hdnProcessedByModule" value="true" />

                <div class="modal-body" style="background-color:#fff;">
                    <div class="row">
                            <div class="form-group">
                                <div class="col-lg-12">
                                    <textarea placeholder="Opmerking (optioneel)" style="height:auto;" id="logboek-notitie-wijzig-input" class="logboek-notitie-input form-control" name="OPMERKING"></textarea>
                                </div>
                            </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Annuleren</button>
                    <button type="button" class="btn btn-primary btnSave" id="btn-logboek-item-bewaar">Opslaan</button>
                </div>

            </form>
        </div>
    </div>
</div>