</form>
{include file="shared.tpl.php"}

{literal}
<script id="template-logboekitem-view" type="text/x-jsrender">

    {{if #getIndex() === 0 && ~root.selectie() == 'actief'}}
        {{if FAVORIET == 'Y'}}
            <div class="selectie-header">Vastgezet</div>
        {{else}}
            {{if ~root.aantalFavorieten() > 0}}
                <div class="selectie-header">Overige</div>
            {{/if}}
        {{/if}}
    {{/if}}

    <div class="logboek-item {{if FAVORIET=='Y'}}pinned{{/if}}" data-id="{{:PLR__RECORDID}}" data-link="
        {on 'dblclick' ~wijzigLogitem}
    ">
        <div class="logboek-item-content">
            <div>
                {^{:OPMERKING}}
            </div>
            <div class="pull-right">
                {{if ~root.selectie() !== 'prullenbak'}}
                    {{if FAVORIET == 'Y'}}
                        {^{on ~unpinLogitem}}
                            <button class="auto-hide logboek-item-pin btn btn-default btn-outline btn-circle btn-xs" type="button" data-toggle="tooltip" title="Notitie losmaken" aria-haspopup="true" aria-expanded="false">
                                <span class="pinicon-logboek-item"></span>
                            </button>
                        {{/on}}
                    {{else}}
                        {^{on ~pinLogitem}}
                            <button class="auto-hide logboek-item-pin btn btn-default btn-outline btn-circle btn-xs" type="button" data-toggle="tooltip" title="Notitie vastzetten" aria-haspopup="true" aria-expanded="false">
                                <span class="pinicon-logboek-item"></span>
                            </button>
                        {{/on}}
                    {{/if}}
                {{/if}}
            </div>
        </div>
        <div class="xrow xrow-no-padding logboek-item-footer">
            <div class="logboek-item-info text-navy">
                <small class=""><b>
                    <span data-toggle="tooltip" title="{{:DATUM}} &nbsp;{{:TIJD}} uur">
                        <span class="hide-on-gridview">{{:DATUM}} &nbsp;|&nbsp; {{if LOCATIE_CODE}}{{:LOCATIE_CODE}}{{else}}Alle locaties{{/if}}</span>
                        <span class="hide-on-listview">{{:DATUM_KORT}}</span>
                     </span>
                </b></small>
            </div>
            <div class=" auto-hide logboek-item-gebruiker text-navy">
                <i class="material-symbols-outlined md-1x light">account_box</i>{{:GEBRUIKERSNAAM_KORT}}
            </div>
            <div class="logboek-item-actions">
                <div class="pull-right">
                {{if ~root.selectie() == 'prullenbak'}}
                    {^{on ~verwijderPermanentLogitem}}
                        <button class="auto-hide btn btn-danger btn-outline btn-circle btn-xs" data-toggle="tooltip" title="Permanent verwijderen" type="button" aria-haspopup="true" aria-expanded="false">
                            <i class="material-symbols-outlined md-1x">delete_forever</i>
                        </button>
                    {{/on}}
                    {^{on ~herstelLogitem}}
                        <button class="auto-hide btn btn-default btn-outline btn-circle btn-xs" data-toggle="tooltip" title="Herstellen" type="button" aria-haspopup="true" aria-expanded="false">
                            <i class="material-symbols-outlined md-1x">restore_from_trash</i>
                        </button>
                    {{/on}}
                {{/if}}
                {{if ~root.selectie() == 'archief'}}
                    {^{on ~dearchiveerLogitem}}
                        <button class="auto-hide btn btn-default btn-outline btn-circle btn-xs" data-toggle="tooltip" title="Terugzetten uit archief" type="button" aria-haspopup="true" aria-expanded="false">
                            <i class="material-symbols-outlined md-1x">unarchive</i>
                        </button>
                    {{/on}}
                {{/if}}
                {{if ~root.selectie() == 'actief'}}
                    {^{on ~archiveerLogitem}}
                        <button class="auto-hide btn btn-default btn-outline btn-circle btn-xs" data-toggle="tooltip" title="Archiveren" type="button" aria-haspopup="true" aria-expanded="false">
                            <i class="material-symbols-outlined md-1x">archive</i>
                        </button>
                    {{/on}}
                {{/if}}
                {{if ~root.selectie() !== 'prullenbak'}}
                    <span class="dropdown">
                        <button class="auto-hide btn btn-default btn-outline btn-circle btn-xs dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="material-symbols-outlined md-1x">more_vert</i>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li>
                                {^{on ~wijzigLogitem}}
                                    <a href="#"><i class="material-symbols-outlined md-1x">edit</i> Bewerken</a>
                                {{/on}}
                            </li>
                            <li>
                                {^{on ~kopieerLogitem}}
                                    <a href="#"><i class="material-symbols-outlined md-1x">content_copy</i> Kopie maken</a>
                                {{/on}}
                            </li>
                            <li>
                                {^{on ~wijzigCheckboxes}}
                                    <a href="#"><i class="material-symbols-outlined md-1x">check_box</i> Selectievakjes tonen/verbergen</a>
                                {{/on}}
                            </li>
                            <li>
                                {^{on ~verwijderLogitem}}
                                    <a href="#" class="logboek-delete" data-recordid="{{:PLR__RECORDID}}"><i class="material-symbols-outlined md-1x">delete</i> Verwijderen</a>
                                {{/on}}
                            </li>
                        </ul>
                    </span>
                {{/if}}
                </div>
            </div>
        </div>
    </div>
</script>

<script id="template-logboek-view" type="text/x-jsrender">

    {{for logboekitems() filter=~pinnedFiltering tmpl="templateLogboekItemView" /}}

    {{for logboekitems() filter=~unpinnedFiltering tmpl="templateLogboekItemView" /}}

    {{if logboekitems().length == 0}}
        <h3>
            {{if ~root.selectie() == 'prullenbak'}}
                De prullenbak is leeg.
            {{else ~root.selectie() == 'archief'}}
                Het archief is leeg.
            {{else}}
                Het logboek bevat geen notities voor deze selectie.
            {{/if}}
        </h3>
    {{/if}}
</script>
{/literal}

<div class="form-main-content logboek-main-content">

    <div id="logboek-view-style" class="element-detail-box">
        <div class="logboek-invoer-wrapper">
            <div></div>
            <div id="logboek-invoer" class="logboek-invoer">
                <div id="logboek-notitie-control" class="logboek-notitie-control">
                    <textarea data-ori_rows="1" rows="1" id="logboek-input" class="logboek-notitie-input xform-control" placeholder="{lang add_logbook_entry}…"></textarea>
                    <div id="logboek-notitie-acties" class="logboek-notitie-acties button-group" style="display:none">
                        <button id="logboek-notitie-opslaan" class="btn btn-primary xpull-right">Opslaan</button>
                    </div>
                </div>
            </div>
            <div id="logboek-weergave-types" class="btn-group pull-right">
                <button type="button" class="btn btn-default btn-outline btn-xs" data-toggle="tooltip" title="Lijstweergave" data-type="list"><i class="material-symbols-outlined md-1x">table_rows</i></button>
                <button type="button" class="btn btn-default btn-outline btn-xs" data-toggle="tooltip" title="Gridweergave" data-type="grid"><i class="material-symbols-outlined md-1x">view_module</i></button>
            </div>
        </div>
        <div class="logboek-invoer-wrapper-content">
            <div id="logboek-view" class="logboek-view"></div>

            <div id="logboek-prullenbak" class="logboek-prullenbak" style="display:none">
                <h4>De prullenbak wordt na 7 dagen automatisch geleegd.</h4>
                <button id="logboek-prullenbak-leegmaken" class="btn btn-sm btn-outline btn-danger">Prullenbak leegmaken</button>
            </div>
        </div>
    </div>

    <div class="form-details-panel logboek-panel">
        <div class="sticky-wrapper">
            <div id="logboek-nav" class="logboek-nav">
                <a class="logboek-nav-item active" data-selectie="actief"><i class="material-symbols-outlined pull-left md-1x">note</i> Notities</a>
                <a class="logboek-nav-item" data-selectie="archief" href="#tab-9"><i class="material-symbols-outlined pull-left md-1x">archive</i> Archief</a>
                <a class="logboek-nav-item" data-selectie="prullenbak" href="#tab-10"><i class="material-symbols-outlined pull-left md-1x">delete</i> Prullenbak</a>
            </div>

            <p>Toon notities van</p>
            <div id="logboek-periode-selectie" class="btn-group" role="group" aria-label="Basic example">
                <button type="button" data-periode="vandaag" class="btn btn-default btn-sm">Vandaag</button>
                <button type="button" data-periode="week" class="btn btn-default btn-sm">Deze week</button>
                <button type="button" data-periode="alles" class="btn btn-default btn-sm">Alles</button>
                <button type="button" data-periode="keuze" class="btn btn-default btn-sm">Kies...</button>
            </div>
            <div id="logboek-nav-kalender" style="display:none"></div>
        </div>
    </div>

</div>

{include file="modalforms.tpl.php"}
