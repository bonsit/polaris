<?php
require '_shared/module.plr_maxia.php' ;

class ModulePLR_Maxia_Vragenlijst extends ModulePLR_Maxia {
    public $clientname;
    public $client;
    public $database;

    function ProcessAntwoorden($postData, $posgezondheidObject)
    {
        $clientHash = $this->client->record->CLIENTHASHID;

        $sql = "INSERT INTO do_positievegezondheid_antwoorden
                (client_hash, positievegezondheid, deelnemer, versie, dimensie, vraag, score)
                VALUES (?, ?, ?, ?, ?, ?, ?)";

        foreach ($postData as $key => $value) {
            if (strpos($key, "vraag_") === 0) {
                $parts = explode('_', $key);
                if (count($parts) === 3) {
                    $dimensie = intval($parts[1]);
                    $vraag = intval($parts[2]);
                    $score = $value;

                    $params = [
                        $clientHash,
                        $posgezondheidObject['POSITIEVEGEZONDHEID_ID'],
                        $posgezondheidObject['DEELNEMER_ID'],
                        $posgezondheidObject['VERSIE'],
                        $dimensie,
                        $vraag,
                        $score
                    ];

                    $result = $this->database->userdb->Execute($sql, $params);

                    if (!$result) {
                        $this->DisplayResponse("Er is een fout opgetreden bij het opslaan van antwoord voor vraag {$vraag} in dimensie {$dimensie}.");
                        exit;
                    }
                }
            }
        }
    }

    function ShowGeneriekeVragenlijst($stylesheet, $peiling_id, $deelnemer_id, $lang='NL') {
        $peiling = $this->fetchPeilingData($peiling_id, $lang);
        $lang = $this->validateLanguage($peiling['talen'], $lang);
        $deelnemer = $this->fetchDeelnemerData($deelnemer_id);
        $vragen = $this->fetchVragenData($peiling_id, $lang);
        $this->displayVragenlijst($stylesheet, $lang, $peiling, $deelnemer, $vragen);
    }

    function ShowWillekeurigeVragenlijst($stylesheet, $peiling_id, $deelnemer_id, $aantalVragen, $lang='NL') {
        $peiling = $this->fetchPeilingData($peiling_id, $lang);
        $lang = $this->validateLanguage($peiling['talen'], $lang);
        $deelnemer = $this->fetchDeelnemerData($deelnemer_id);
        $vragen = $this->FetchWillekeurigeVragenData($peiling_id, $aantalVragen, $lang);
        $this->displayVragenlijst($stylesheet, $lang, $peiling, $deelnemer, $vragen);
    }

    function DisplayResponse($message) {
        $this->DisplayMessage($message);
    }

    private function FetchPeilingData($peiling_id, $lang) {
        $sql = "SELECT v.welkomtekst_" . strtoupper($lang) . " AS welkomtekst, antwoordopties, talen
                FROM do_peiling_versies v
                WHERE versie_id = ? AND client_hash = ?";
        return $this->database->userdb->GetRow($sql, [$peiling_id, $this->client->record->CLIENTHASHID]);
    }

    private function ValidateLanguage($talen, $lang) {
        $availableLanguages = json_decode($talen, true) ?: ['NL'];
        return in_array($lang, $availableLanguages) ? $lang : 'NL';
    }

    private function FetchDeelnemerData($deelnemer_id) {
        $sql = "SELECT volledige_naam
                FROM do_deelnemers
                WHERE deelnemer_id = ? AND client_hash = ?";
        return $this->database->userdb->GetRow($sql, [$deelnemer_id, $this->client->record->CLIENTHASHID]);
    }

    private function FetchVragenData($peiling_id, $lang) {
        $sql = "SELECT v.vraag_id, v.dimensie, d.dimensie_nummer, d.dimensie_naam_NL, d.dimensie_naam_EN,
                       d.volgorde, v.volgorde AS vraagvolgorde,
                       v.vraag_" . strtoupper($lang) . " AS vraag
                FROM do_peiling_vragen v
                LEFT JOIN do_peiling_dimensies d ON d.dimensie_id = v.dimensie
                    AND d.client_hash = v.client_hash
                WHERE v.versie = ? AND v.client_hash = ?";
        return $this->database->userdb->GetAll($sql, [$peiling_id, $this->client->record->CLIENTHASHID]);
    }

    private function FetchWillekeurigeVragenData($peiling_id, $aantalVragen, $lang) {
        $sql = "SELECT v.vraag_id, v.dimensie, d.dimensie_nummer, d.dimensie_naam_NL, d.dimensie_naam_EN,
                       d.volgorde, v.volgorde AS vraagvolgorde,
                       v.vraag_" . strtoupper($lang) . " AS vraag
                FROM (
                    SELECT *, ROW_NUMBER() OVER (PARTITION BY dimensie ORDER BY RAND()) as rn
                    FROM do_peiling_vragen
                    WHERE versie = ? AND client_hash = ?
                ) v
                LEFT JOIN do_peiling_dimensies d ON d.dimensie_id = v.dimensie
                    AND d.client_hash = v.client_hash
                WHERE v.rn <= ?
                ORDER BY d.volgorde";
        return $this->database->userdb->GetAll($sql, [$peiling_id, $this->client->record->CLIENTHASHID, $aantalVragen]);
    }

    private function DisplayMessage($message) {
        $this->smarty->assign('message', $message);
        echo $this->Fetch('message.tpl.php');
    }

    private function DisplayVragenlijst($stylesheet, $lang, $peiling, $deelnemer, $vragen) {
        $this->smarty->assign('stylesheet', $stylesheet);
        $this->smarty->assign('lang', $lang);
        $this->smarty->assign('peiling', $peiling);
        $this->smarty->assign('deelnemer', $deelnemer);
        $this->smarty->assign('vragen', $vragen);
        echo $this->Fetch('vragenlijst.tpl.php');
    }

}
