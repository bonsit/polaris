Maxia.Zelfwijs = class {

    instructiesModel = null;
    instructiesData = {instructies: []};

    initialiseer() {
        this.initialiseerInstructies();
        this.initialiseerLiveZoeken();
    }

    refreshInstructies(searchvalue, huidigeID) {
        var that = this;

        if (isNullOrEmpty(searchvalue))  {
            searchvalue = Maxia.getGetVar('q[]');
        }

        this.getInstructies(searchvalue, function(instructies, textStatus) {
            if (textStatus == 'success') {
                log(instructies);
                $.observable(that.instructiesData).setProperty("instructies", instructies);

                if (huidigeID) {
                    this.assetsData.assets.forEach(function(item) {
                        if (item.PLR__RECORDID == huidigeID) {
                            const div = $(".instructies-view .instructies[data-id=" + huidigeID + "]");
                            that.refreshAsset(div);
                            that.selectAsset(div);
                        }
                    });
                }
            } else {
                alert('De items konden niet geladen worden. Error: ' + textStatus);
            }
        });
    }

    getAssetDetails(asset_id, callback) {
        return $.getJSON(_servicequery, {
            'event': 'instructiedetails',
            'asset_id': asset_id
        }, callback);
    }

    getInstructies(searchvalue, callback) {
        return $.getJSON(_servicequery, {
            'event': 'instructies',
            'q[]': searchvalue || ''
        }, callback);
    }

    renderHelpers = {
        /**
         * Dit zijn de functies die enkele eigenschappen berekenen in onze template.
         * We geven ze door bij het koppelen van gegevens aan onze template
         * en gebruiken ze met het ~-teken, bijvoorbeeld: {:~getBezetting()}
         */
        select(ev, eventArgs) {
            var elem = $(ev.target);
            if (!elem.hasClass('instructie-selector')) {
                elem = elem.closest('.instructie-selector');
            }
            Maxia.Assets.refreshAsset(elem);
            Maxia.Assets.selectAsset(elem);
            Maxia.Assets.toggleEditableElements(false);
        },
        stripHtml(html) {
            var tmp = document.createElement("DIV");
            tmp.innerHTML = html;
            return tmp.textContent || tmp.innerText || "";
        },
        truncate(str, maxLength) {
            if (str.length > maxLength) {
                return str.substring(0, maxLength) + '...';
            }
            return str;
        }
    }

    selectAsset(elem) {
        $(".instructie-selector").removeClass("selected");
        elem.toggleClass('selected');
        $(".form-details-content").toggle(elem.hasClass('selected'));
    }

    refreshAsset(elem) {
        const Self = Maxia.Assets;

        Maxia.Assets.getAssetDetails(elem.data('id'), function(data) {
            const item = data.result;
            elem[0].objectdata = item;

            Self.historieData = Self.historieModel(data.historie);
            $.templates("#template-assets-historie").link("#asset-historie", Self.historieData);

            if (item.STATUS == 'ingebruik') {
                $(".asset-status .asset-vrij").addClass('btn-white');
                $(".asset-status .asset-vrij").removeClass('btn-primary');
                $(".asset-status .asset-bezet").addClass('btn-danger');
                $(".asset-status .asset-bezet").removeClass('btn-white');

                $("#asset-uitlening").show();
                $("#asset-geen-uitlening").hide();
            } else {
                $(".asset-status .asset-vrij").addClass('btn-primary');
                $(".asset-status .asset-vrij").removeClass('btn-white');
                $(".asset-status .asset-bezet").addClass('btn-white');
                $(".asset-status .asset-bezet").removeClass('btn-danger');

                $("#asset-uitlening").hide();
                $("#asset-geen-uitlening").show();
            }
            $("#asset-code").html(item.ASSET_CODE);
            $("#asset-subcode").html(item.ASSET_SUBCODE).toggle(!isNullOrEmpty(item.ASSET_SUBCODE));
            $("#asset-type-container").removeClass().addClass('asset-'+item.ASSET_TYPE_NAME.toLowerCase());

            $("#asset-soort").html(item.ASSET_TYPE_NAME);
            $("#asset-ruimte").html(item.RUIMTE);
            $("#asset-locatie").html(item.LOCATIE);
            $("#asset-opmerking").text(item.OPMERKING || '');
            $("#asset-opmerking-input").val(item.OPMERKING||'');
            $("#asset-uitgeleendaan").text(item.PERSOON_NAAM||'');
            $("#asset-uitgeleendaan-input").val(item.PERSOON_NAAM||'');
            if (item.STARTDATUM_DATUM) {
                let text = item.STARTDATUM_DATUM + ' - ' + item.STARTDATUM_TIJD + ' uur';
                $("#asset-uitgeleendvanaf span").html(text);
            }
            if (item.MAX_EINDDATUM_DATUM) {
                let text = item.MAX_EINDDATUM_DATUM + ' - ' + item.MAX_EINDDATUM_TIJD + ' uur';
                if (item.OVERTIJD == 1) {
                    $("#asset-uitgeleendtot").removeClass('btn-default').addClass('btn-warning');
                } else {
                    $("#asset-uitgeleendtot").removeClass('btn-warning').addClass('btn-default');
                }
                $("#asset-uitgeleendtot span").html(text);
            } else {
                $("#asset-uitgeleendtot").removeClass('btn-primary btn-danger');
                $("#asset-uitgeleendtot span").html('');
            }

            $("#asset-uitgeleendaan_telefoon").text(item.PERSOON_TELEFOON||'- geen telefoon -');
            $("#asset-uitgeleendaan_telefoon-input").val(item.PERSOON_TELEFOON||'');
            $("#asset-uitgeleendaan_email").text(item.PERSOON_EMAIL||'- geen email -');
            $("#asset-uitgeleendaan_email-input").val(item.PERSOON_EMAIL||'');
            $("#asset-langdurig-text").text(item.LANGDURIG?'langdurig ':'');

            $("#asset-asset-opmerking").text(!isNullOrEmpty(item.ASSET_OPMERKING) ? item.ASSET_OPMERKING : '')
            .toggleClass('active', !isNullOrEmpty(item.ASSET_OPMERKING));

            $("#asset-asset-buitenwerking").toggle(item.STATUS == "inactief");

            $("#group-asset-inleveren").toggle(item.STATUS == "ingebruik");
            $("#btn-asset-uitlenen").toggle(item.STATUS == "vrij");
        });
    }

    initialiseerLiveZoeken() {
        const Self = Maxia.Assets;

        let searchField = $("#_hdnGlobalSearch");

        searchField
        .on('keypress', function(e) {
            if (e.which == 13) {
                e.preventDefault();
                e.stopPropagation();
            }
        })
        .on('keyup', function(e) {
            clearTimeout($(this).data('timer'));
            var search = this.value;
            if (search.length >= 1) {
                $(this).data('timer', setTimeout(function() {
                    this.refreshInstructies(searchField.val());
                }, 250));
            } else {
                this.refreshInstructies();
            }
        });
    }

    huidigeInstructie() {
        return $(".instructie-selector.selected");
    }

    toggleEditableElements(editmode) {
        $("#btn-asset-inleveren").prop('disabled', editmode);
        $("#btn-asset-verlengen").prop('disabled', editmode);
        $(".asset-uitlening :input").toggle(editmode);
        $(".asset-uitlening .editable").toggle(!editmode);

        $("#btn-asset-edit").toggle(!editmode);
        $("#btn-asset-save").toggle(editmode);
        $("#btn-asset-cancel").toggle(editmode);
    }

    initialiseerInstructies() {
        $.views.helpers(this.renderHelpers);
        $.views.converters("lower", function(val) {
            if (val)
                return val.toLowerCase();
        });
        $.views.settings.debugMode(false);

        $.templates("#template-instructies-view").link("#instructies-view", this.instructiesData);

        this.refreshInstructies();
    }

}


jQuery(function () {
    Maxia.Zelfwijs_instance = new Maxia.Zelfwijs();
    Maxia.Zelfwijs_instance.initialiseer();
});