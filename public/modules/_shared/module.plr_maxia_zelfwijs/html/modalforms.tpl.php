</form>

<div class="modal inmodal" id="dlgAssetUitlenen" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Sluiten</span></button>
                <h4 class="modal-title">Uitlenen aan medewerker</h4>
                <h5 id="asset-name" class="modal-subtitle"></h5>
                <div id="modal-asset-opmerking" class="asset-opmerking"></div>
            </div>
            <form class="form-horizontal" id="frmAssetUitlenen">
                <input type="hidden" name="event" value="assetuitlenen" />
                <input type="hidden" name="_hdnProcessedByModule" value="true" />
                <input type="hidden" name="EINDDATUM" value="" />
                <input type="hidden" name="LANGDURIG" value="" />

                <div class="modal-body">

                    <div class="row">
                        <div class="col-lg-7" id="grp-asset-uitlenen-kolom1">
                            <div class="form-group">
                                <label class="col-lg-3 control-label">Naam*</label>
                                <div class="col-lg-9"><input type="text" autofocus placeholder="Naam" class="text validate required validation_allchars form-control" name="PERSOON_NAAM"></div>
                            </div>

                            <div class="form-group"><label class="col-lg-3 control-label">Email</label>
                                <div class="col-lg-9"><input type="email" placeholder="Email (optioneel)" class="form-control" name="PERSOON_EMAIL">
                                </div>
                            </div>

                            <div class="form-group"><label class="col-lg-3 control-label">Telefoonnr</label>
                                <div class="col-lg-9"><input type="text" placeholder="Telefoonnr (optioneel)" class="form-control" name="PERSOON_TELEFOON">
                                </div>
                            </div>

                            <div class="form-group"><label class="col-lg-3 control-label">Opmerking</label>
                                <div class="col-lg-9"><textarea placeholder="Opmerking (optioneel)" rows="2" class="form-control" name="OPMERKING"></textarea>
                                </div>
                            </div>
                        </div>
                        <div id="grp-asset-uitlenen-datum-picker">
                            <div class="col-lg-5">
                                <div class="form-group">
                                    <div class="col-lg-12">
                                        <label class="col-lg-12 control-label">Tot wanneer?</label>
                                        <div id="asset-uitlenen-datum-picker"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Annuleren</button>
                    <button type="button" class="btn btn-primary btnSave" id="btn-asset-uitlenen-bewaar">Uitlenen</button>
                </div>

            </form>
        </div>
    </div>
</div>

<div class="modal inmodal" id="dlgAssetOntvangst" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Sluiten</span></button>
                <p class="modal-title">Heeft u dit item in ontvangst genomen?</p>
                <div id="asset-name" class="modal-subtitle"></div>
            </div>
            <form class="form-horizontal" id="frmAssetOntvangst">
                <input type="hidden" name="event" value="assetontvangst" />
                <input type="hidden" name="_hdnProcessedByModule" value="true" />
                <div class="modal-body">
                    <div class="form-group" style="font-size:1.5em;">
                        <label class="col-lg-3 control-label"></label>
                        <div class="col-lg-9 radio required">
                            <label>
                                <input class="icheck checkbox-circle" type="radio" value="Y" id="ontvangstJA" name="ONTVANGST"> JA
                            </label> &nbsp;
                            <label>
                                <input class="icheck checkbox-circle" type="radio" value="N" id="ontvangstNEE" name="ONTVANGST"> NEE
                            </label>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Annuleren</button>
                    <button type="button" class="btn btn-primary btnSave" id="btn-asset-ontvangst-bewaar">Bevestig</button>
                </div>

            </form>
        </div>
    </div>
</div>

<div class="modal inmodal" id="dlgAssetVerlengen" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Sluiten</span></button>
                <h5 class="modal-title">Wilt u de periode verlengen?</h5>
                <div id="asset-name" class="modal-subtitle"></div>
            </div>
            <form class="form-horizontal" id="frmAssetVerlengen">
                <input type="hidden" name="event" value="assetverlengen" />
                <input type="hidden" name="_hdnProcessedByModule" value="true" />
                <input type="hidden" name="NIEUWEDATUM" value="" />
                <div class="modal-body">
                    <div class="form-group" style="font-size:1.1em;">
                        <label class="col-md-offset-3 col-lg-4 control-label">Kies de gewenste dag</label>
                        <div class="col-md-offset-3 col-lg-4 required">
                            <div id="verlenging-datum"></div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Annuleren</button>
                    <button type="button" class="btn btn-primary btnSave" id="btn-asset-verlengen-bewaar">Bevestig</button>
                </div>

            </form>
        </div>
    </div>
</div>

<div class="modal inmodal" id="dlgAssetsToevoegen" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Sluiten</span></button>
                <h4 class="modal-title">Items toevoegen</h4>
            </div>
            <form class="form-horizontal" id="frmAssetsToevoegen">
                <input type="hidden" name="_hdnRecordID" value="" />
                <input type="hidden" name="_hdnState" value="insert" />
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-lg-3 control-label">Hoeveel items</label>
                        <div class="col-lg-9"><input type="number" placeholder="Aantal" data-v-min="1" data-v-max="500" class="validation_integer required form-control" name="AANTAL" value="1"></div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label">Start vanaf</label>
                        <div class="col-lg-9"><input type="number" placeholder="Vanaf" data-v-min="1" data-v-max="2000" class="validation_integer required form-control" name="VANAF" value="1"></div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label">Prefix</label>
                        <div class="col-lg-9"><input type="text" placeholder="Optionele prefix" class="form-control" maxlength="20" name="PREFIX"></div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label">Voorbeeld</label>
                        <div class="col-lg-9"><span id="asset-add-preview"></span></div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Annuleren</button>
                    <button type="button" class="btn btn-primary btnSave" id="btnAssetsSave">Toevoegen</button>
                </div>

            </form>
        </div>
    </div>
</div>
