{literal}
<div class="form-details-panel">
    <div class="media-details">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="form-details-content">
                    <h3>Filter</h3>
                    <h4>Tags</h4>
                    <div id="tags-container">
                        {{for tags}}
                            <label>
                                <input type="checkbox" value="{{:value}}"> {{:label}}
                            </label>
                        {{/for}}
                    </div>
                    <button id="more-tags">More</button>

                    <h3>Applications</h3>
                    <div id="applications-container">
                        {{for applications}}
                            <label>
                                <input type="checkbox" value="{{:value}}">
                                <img src="{{:icon}}" alt="{{:name}}"> {{:name}}
                            </label>
                        {{/for}}
                    </div>
                    <button id="more-applications">More</button>
                </div>
            </div>
        </div>
    </div>
</div>
{/literal}
