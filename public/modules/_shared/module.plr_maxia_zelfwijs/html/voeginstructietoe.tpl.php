<form class="form-horizontal" id="frmAssetsToevoegen">
    <h4 class="modal-title">Items toevoegen</h4>

    <input type="hidden" name="_hdnRecordID" value="" />
    <input type="hidden" name="_hdnState" value="insert" />
    <div class="modal-body">
        <div class="form-group">
            <label class="col-lg-3 control-label">Welke uitleensoort?</label>
            <div class="col-lg-9">
                {$uitleensoort_select}
                <select class="form-control required" name="ASSETTYPE">
                    {section name=i loop=1assettypes}
                    <option value="{$assettypes[i].ID}">{$assettypes[i].NAME}</option>
                    {/section}
                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="col-lg-3 control-label">Hoeveel items</label>
            <div class="col-lg-9"><input type="number" placeholder="Aantal" class="required form-control" name="AANTAL" value="1"></div>
        </div>

        <div class="form-group">
            <label class="col-lg-3 control-label">Start vanaf</label>
            <div class="col-lg-9"><input type="number" placeholder="Vanaf" class="required form-control" name="VANAF" value="1"></div>
        </div>

        <div class="form-group">
            <label class="col-lg-3 control-label">Prefix</label>
            <div class="col-lg-9"><input type="text" placeholder="Optionele prefix" class="form-control" name="PREFIX"></div>
        </div>

        <div class="form-group">
            <label class="col-lg-3 control-label">Voorbeeld</label>
            <div class="col-lg-9"><span id="asset-add-preview"></span></div>
        </div>
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-white" id="btnAssetsCancel">Annuleren</button>
        <button type="button" class="btn btn-primary btnSave" id="btnAssetsSave">Toevoegen</button>
    </div>

</form>

<script type="text/javascript">
    jQuery(function () {

        $("#frmAssetsToevoegen input").on('keyup', function(e) {
            const frm = $("#frmAssetsToevoegen");
            const aantal = $("input[name='AANTAL']", frm).val();
            const vanaf = $("input[name='VANAF']", frm).val();
            const prefix = $("input[name='PREFIX']", frm).val();
            let voorbeeld = '';
            try {
                voorbeeld = 'Van ' + prefix + vanaf + ' tot en met ' + prefix + (parseInt(vanaf) + parseInt(aantal) - 1);
            } catch (error) {
                voorbeeld = 'Foutieve invoer. Controleer de velden.';
            }
              $("#asset-add-preview").text(voorbeeld);
        });
        $("#btnAssetsSave").on('click', function(e) {
            e.preventDefault();
            var frm = $("#frmAssetsToevoegen");
            var data = {
                  'event':'assetstoevoegen'
                , '_hdnProcessedByModule':'true'
                , 'AANTAL': $("input[name='AANTAL']", frm).val()
                , 'VANAF': $("input[name='VANAF']", frm).val()
                , 'PREFIX': $("input[name='PREFIX']", frm).val()
            };
            Polaris.Ajax.postJSON(_serverroot + '/services/json/app/facility/const/lockers/', data, null, 'Assets zijn toegevoegd', function() {
                $("#frmAssetsToevoegen").modal('hide');
            });
        });
        $("#frmAssetsToevoegen input").trigger('keyup');

        $("#btnAssetsCancel").on('click', function(e) {
            e.preventDefault();
            $("#ajaxdialog").jqmHide();
        });
    });
</script>