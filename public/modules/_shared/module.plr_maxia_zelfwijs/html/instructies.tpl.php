</form>
{include file="shared.tpl.php"}

{literal}
<style>
    #instructies-view {
        display: flex;
        flex-wrap: wrap;
        justify-content: space-between;
    }

    .instructie {
        background-color: #f8f9fa;
        border: 1px solid #dee2e6;
        border-radius: 4px;
        padding: 17px;
        margin-bottom: 15px;
        box-shadow: 0 2px 4px rgba(0,0,0,0.1);
        width: calc(50% - 15px); /* For 2 items per row */
        box-sizing: border-box;
        display: flex;
        flex-direction: column;
        height: auto; /* Change from fixed height to auto */
        min-height: 200px; /* Add minimum height */
        overflow: hidden;
    }

    .instructie .title {
        font-size: 2rem;
        margin-bottom: 8px;
        color: #333;
        font-family: 'Atkinson Hyperlegible', sans-serif;
        font-weight: 600;
        line-height: 1.2;
        max-height: 2.4em; /* 2 lines */
    }

    .instructie p {
        font-size: 1.5rem;
        line-height: 1.3;
        color: #666;
        flex-grow: 1;
    }

    .instructie .metadata {
        margin-top: 10px;
        font-size: 1.5rem;
        display: inline-flex;
        flex-direction: column;
        gap: 5px;
        align-items: flex-start;
    }

    .instructie .metadata .tags {
        display: flex;
        flex-wrap: wrap;
        gap: 5px;
    }

    .instructie .metadata .badge {
        display: inline-block;
        padding: 3px 7px;
        border-radius: 4px;
        font-weight: 600;
        white-space: nowrap;
    }

    .instructie .metadata .artikelcode {
        background-color: #e7f5ff;
        color: #007bff;
    }

    .instructie .metadata .categorie {
        background-color: #e8f5e9;
        color: #28a745;
    }

    .instructie .metadata .tag {
        background-color: #ffebee;
        color: #dc3545;
    }

    @media (max-width: 1200px) {
        .instructie {
            width: calc(50% - 15px); /* For 2 items per row on smaller screens */
        }
    }

    @media (max-width: 768px) {
        .instructie {
            width: 100%; /* Full width on mobile */
        }
    }
</style>

<script id="template-instructies-view" type="text/x-jsrender">
    {^{for instructies}}
        <div class="instructie instructie-selector" data-id="{{:PLR__RECORDID}}">
            <div class="title">{{:~truncate(TITEL, 200)}}</div>
            <p>{{:~truncate(~stripHtml(INHOUD), 150)}}</p>
            <div class="metadata">
                <span class="badge artikelcode">{{:ARTIKELCODE}}</span>
                <span class="badge categorie">{{:CATEGORIENAAM}}</span>
                {{if TAGS}}
                {{for TAGS.split(' ')}}
                    <span class="badge tag">{{:}}</span>
                {{/for}}
                {{/if}}
            </div>
        </div>
    {{else}}
        <h3>Er zijn geen items beschikbaar.</h3>
    {{/for}}
</script>

{/literal}

<div id="form-main-content" class="form-main-content">
    <div id="instructies-view" class="element-detail-box"></div>
    {include file="sidepanel.tpl.php"}
</div>

{include file="modalforms.tpl.php"}
