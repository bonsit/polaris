<html>
<head>
    <script type="text/javascript" src="/assets/js/jquery/dist/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/assets/dist/main.min.css" media="all" />
    <link rel="stylesheet" type="text/css" media='all' href="/modules/_shared/module.plr_maxia/css/default.css"/>
    {if $stylesheet}
    <link rel="stylesheet" type="text/css" href="{$stylesheet}" media="all" />
    {/if}

    <script type="text/javascript">
    var _loadevents_=[],_serverroot="{$serverroot}",_callerquery=_serverroot+"{$callerquery}",_callerquerydetail=_serverroot+"{$callerquery}",_servicequery=_serverroot+"{$servicequery}",_ajaxquery=_serverroot+"{$ajaxquery}";
    </script>

    <style>
        body {
            color:#000;
            background-color: #eee;
        }
        .language-selectorx {
            position: absolute;
            top: 10px;
            right: 10px;
        }
    </style>
</head>
<body>
    <div class="container row detailview-header text-large">

        <div class="col-md-7 col-sm-12 col-md-offset-4 m-t-xl">

            <div class="language-selector m-b-lg">
                <div class="dropdown pull-right">
                    <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button">
                        Change Language <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right" role="menu">
                        <li><a href="#" onclick="changeLanguage('NL')">Nederlands</a></li>
                        <li><a href="#" onclick="changeLanguage('EN')">English</a></li>
                    </ul>
                </div>
            </div>

            <h3 class="deelnemer-titel">{if $lang == 'NL'}Hallo{else}Hello{/if} {$deelnemer.volledige_naam}</h3>
            <p class="text-lg m-b-xl">{$peiling.welkomtekst}</p>

            <form id="vragenlijstForm" action="{$smarty.server.REQUEST_URI}" method="post">
                <input type="hidden" name="id" value="{$smarty.get.id}" />
                {section name=i loop=$vragen}
                <div class="form-group m-t-lg">
                    <label for="{$vragen[i].vraag_id}">{$vragen[i].vraag}</label>
                    <input type="text" class="m-t-xs form-control" max-value placeholder="0-10" name="vraag_{$vragen[i].dimensie_nummer}_{$vragen[i].vraag_id}" />
                </div>
                {/section}
                <div class="button-group m-t-lg m-b-xl">
                    <button type="submit" id="btnSaveVragenlijst" class="btn btn-primary">Opslaan</button>
                </div>
            </form>
        </div>
    </div>

    <script src="/assets/dist/main.js?v={#BuildVersion#}"></script>

    <script>
        function changeLanguage(language) {
            window.location.href = Polaris.Base.addLocationVariable('lang', language);
        }

        $(document).ready(function() {
            $.validator.addMethod("range0to10", function(value, element) {
                if (value === "") return true; // Optional, allow empty
                var num = parseFloat(value);
                return !isNaN(num) && num >= 0 && num <= 10;
            }, "{if $lang == 'NL'}Voer een nummer in tussen 0 en 10{else}Please enter a number between 0 and 10{/if}");

            $("#vragenlijstForm").validate({
                rules: {
                },
                messages: {
                },
                errorPlacement: function(error, element) {
                    error.insertAfter(element);
                },
                submitHandler: function(form) {
                    form.submit();
                }
            });

            // Add rules dynamically to each input
            $('input[type="text"]').each(function() {
                $(this).rules("add", {
                    required: false,
                    range0to10: true,
                });
            });
        });
    </script>

</body>
</html>