<html>
<head>
    <link rel="stylesheet" type="text/css" href="/assets/dist/main.min.css" media="all" />
    <link rel="stylesheet" type="text/css" media='all' href="/modules/_shared/module.plr_maxia/css/default.css"/>
    {if $stylesheet}
    <link rel="stylesheet" type="text/css" href="{$stylesheet}" media="all" />
    {/if}

    <style>
        body {
            color:#000;
            background-color: #eee;
        }
    </style>
</head>
<body>
    <div class="container row detailview-header text-large">

        <div class="col-md-7 col-sm-12 col-md-offset-4 m-t-xl text-center">

            <h3 class="deelnemer-titel">{$message}</h3>

        </div>
    </div>
</body>
</html>