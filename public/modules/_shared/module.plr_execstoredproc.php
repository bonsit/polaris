<?php
require_once('module.plr_executeaction.php');

function Wrap($text, $wrapit) {
    if ($wrapit)
        $text = "'$text'";
    return $text;
}

class ModulePLR_ExecStoredProc extends ModulePLR_ExecuteAction {

    function __construct($moduleid, $module) {
        global $_GVARS;
        parent::__construct($moduleid, $module);

        $this->processed = false;
        /**
        * What client are we talking about? (nog doen: eventueel selecteerbaar maken)
        */
        $this->clientid = $_SESSION['clientid'];
    }

    function Process() {
        if (isset($_POST['_hdnProcessedByModule'])) {
            /* ... */
        }
    }

    function ExecuteStoredProcedure($params, $activefields, $nullvalues) {
        global $polaris;

        $rec = $this->currentaction->record;
        $sp_base = $this->currentaction->record->STOREDPROCEDURE;

        $keyisrowid = (substr($params[0], 0, 7) == 'ROWIDS:');
        if ($keyisrowid) $params[0] = str_replace("ROWIDS:","",$params[0]);
        $recids = explode(',', $params[0]);
        $activefields = explode(',',$activefields);
        $nullvalues = explode(',',$nullvalues);

        foreach($recids as $index => $recid) {
            $prepparams = $params;

            if ($keyisrowid) $recid = $this->dbobject->customUrlDecode($recid);
            $sp = $sp_base."(";

            if (is_array($prepparams)) {
                if (substr($recid,0,1) == '\'' and substr($recid,-1) == '\'')
                    $prepparams[0] = "$recid";
                else
                    $prepparams[0] = "'$recid'";
                /* Add the params */
                for($i=2;$i<=5;$i++) {
                    if (in_array('param'.$i, $activefields)) {
                        $splits = explode(',',$prepparams[$i-1]);
                        if (isset($splits[$index])) {
                            $prepparams[$i-1] = $splits[$index];
                        } else {
                            $prepparams[$i-1] = $splits[0];
                        }
                        $pfield = "PARAM".$i;
                        if (isset($rec->$pfield)) {
                            $parts = $this->GetParts($rec->$pfield);
                            if (isset($prepparams[$i-1])) {
                                if (($prepparams[$i-1] != '') or ($prepparams[$i-1] == '' and $nullvalues[$i-1] == 'EMPTY')) {
                                    $prepparams[$i-1] = Wrap($prepparams[$i-1], $parts[1] != 'NUMBER');
                                } else {
                                    if ($nullvalues[$i-1] == 'NULL') {
                                        $prepparams[$i-1] = 'NULL';
                                    }
                                }
                            } else {
                                $prepparams[$i-1] = 'NULL';
                            }
                        }
                    }
                }
                ksort($prepparams);
                $sp .= implode(',',$prepparams);
            }
            $sp .= ")";

            $this->lastStatement = "call $sp;";
            $polaris->logUserAction($status="execstoredproc", session_id(), $_SESSION['username'], $this->lastStatement);
            # For oracle, Prepare and PrepareSP are identical
            // $preparedSP = $this->userdatabase->PrepareSP($this->lastStatement);
            try {
                $this->userdatabase->Execute($this->lastStatement);
                $result = true;
            } catch(Exception $e) {
                $result = false;
                return $result;
            }
        }
        return $result;
    }

    function Show($outputformat='xhtml', $rec=false) {
        global $polaris;

        parent::Show($outputformat, $rec);

        /* When it's the first time, show the parameter form */
        $displayDialogForFirstTime = !isset($_GET['notfirst']);
        if ($displayDialogForFirstTime) {
            $this->DisplayParameterForm();
        } else {
            $paramvalues = $this->ParamsNeedValue($this->currentaction->record);
            $result = $this->ExecuteStoredProcedure($paramvalues, $activefields=$_GET['activefields'], $nullvalues=$_GET['nullvalues']);
            $this->DisplayMessage($result);
        }
    }
}