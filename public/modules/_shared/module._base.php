<?php
// require_once 'classes/temp_engine/mod_smarty.class.php';
require_once 'includes/mainactions.inc.php';

class _BaseModule {
    var $polaris;
    var $smarty;
    var $moduleid;
    var $module;
    var $_moduleform;
    var $moduleroot;
    var $parentmoduleroot;
    var $modulepath;
    var $modulefolder;
    var $scriptspath;
    var $params;
    var $state;
    var $formmetaname;
    var $form;
    var $clientname;
    var $permission;
    var $userdatabase;
    var $action;
    var $basehref;
	var $outputformat;
	var $masterrecord;
	var $rec;
	var $showControls;
	var $showDefaultButtons;
    var $clientid;

	function __construct($moduleid, $module, $clientname=false, $formmetaname=false) {
        global $_GVARS;
        global $polaris;

        $this->moduleid = $moduleid;
        $this->module = 'module.'.strtolower($module);
        $this->formmetaname = $formmetaname;
        $this->polaris = $polaris;

        if ($clientname)
            $this->clientname = $clientname;
        elseif ($polaris->plrClient != NULL)
            $this->clientname = str_replace(' ', '_', strtolower($polaris->plrClient->record->NAME)) ;

            $this->smarty = new tempengine_modSmarty($polaris->language);

        $this->smarty->SetApplication($polaris->currentApp);
        // Change the default template directories to the modules template dir
        if (substr($module, 0, 4) == 'plr_') {
            $_clientpart = '_shared';
        } else {
            $_clientpart = $this->clientname;
        }
        $this->moduleroot = $_GVARS['serverroot'].'/'.$_GVARS['module_dir'].'/'.$_clientpart.'/'.$this->module.'/';
        $this->modulepath = $_GVARS['serverpath'].'/'.$_GVARS['module_dir'].'/'.$_clientpart.'/'.$this->module.'/';
        $this->scriptspath = $_GVARS['serverpath'].'/../../'.$_GVARS['module_dir'].'/'.$_clientpart.'/'.$this->module.'/';
        $this->modulefolder = $_clientpart.DIRECTORY_SEPARATOR.$this->module;
        if (!isset($this->parentmoduleroot))
            $this->parentmoduleroot = $_GVARS['serverroot'].'/'.$_GVARS['module_dir'].'/'.$this->clientname.'/'.$this->module;
        $this->smarty->setTemplateDir([
            '.'.$this->modulepath.'html'.DIRECTORY_SEPARATOR,
            $this->scriptspath.'/html'.DIRECTORY_SEPARATOR // making it work for scripts in the scripts folder
        ]);

        $_language_directory = 'modules'.DIRECTORY_SEPARATOR.$this->modulefolder;
        lang_load_mod($polaris->language, $_language_directory);

        $this->smarty->assign('moduleroot', $this->moduleroot);
        $this->smarty->assign('modulepath', $this->modulepath);
        $this->smarty->assign('parentmoduleroot', $this->parentmoduleroot);
        $this->smarty->assign('moduleid', $this->moduleid);
        $this->smarty->assign('clientparameters', $_SESSION['clientparameters'] ?? []);

        $this->smarty->assignByRef('module',$this);

        $this->showControls = false;
        $this->showDefaultButtons = false;
    }

    function AddModuleJS($javascriptModules /* String or Array of strings */ ) {
        global $_GVARS;

        foreach ((array) $javascriptModules as $str) {
            $_GVARS['_includeJavascript'][] = $this->modulepath.'js/'.$str;
        }
    }

    function AddCustomJS($customJavascript /* String or Array of strings */ ) {
        global $_GVARS;

        foreach ((array) $customJavascript as $str) {
            $_GVARS['_includeJavascript'][] = $_GVARS['serverpath'].$str;
        }
    }

    function Fetch($template) {
        $_parts = explode('.tpl', $template);
        $_type = end($_parts);
        switch($_type) {
            case '.php':
                $_result = $this->smarty->Fetch($template);
            break;
            case '.jsrender':
                $_result = file_get_contents('.'.$this->modulepath.'html/'.$template);
            break;
        }
        return $_result;
    }

    function GetCurrentState() {
        /**
         * Override this in the module to set the State parameter
         */
    }

    function ShowModuleGenericView($_type, $_recordset=false) {
        global $polaris;

        if ($_recordset) {
            $this->form->rsdata = $_recordset;
        }

        switch ($_type) {
            case 'listview':
                $this->form->ShowListview($this->state, $this->permission);
                break;
            case 'formview':
                $this->form->ShowFormview($this->state, SELECT + INSERT + UPDATE + DELETE + SEARCH);
                break;
        }
    }

    function ShowModuleListview($_recordset=false) {
        $this->ShowModuleGenericView('listview', $_recordset);
    }

    function ShowModuleFormview($_recordset=false) {
        $this->ShowModuleGenericView('formview', $_recordset);
    }

    function ShowModalForms() {
        /**
         * Override this in the module to add modal forms just before the end body-tag
         */
    }

    function ProcessRequest($method=false) {
        if (!isset($_SESSION['clienthash'])) {
            throw new Exception('U bent niet ingelogd.');
        }

        // Use something like this in the module :
        //
        // switch($allesRecord['_hdnFORMTYPE']) {
        // case '...':
        //      ....
        //     break;
        // }
        //...

        return false;
    }

    function ProcessFiles($files) {
        return false;
    }

    function ProcessAction($action, $moduleid, $allesRecord, $parameters=false) {
        return false;
    }

    function Process($method) {
        global $polaris;
        global $scramble;

        try {
            if (isset($this->form)) $this->form->database->userdb->SetFetchMode(ADODB_FETCH_ASSOC);
            $resultProcess = $this->ProcessRequest($method);
            echo json_encode(Array('result'=>$resultProcess, 'query'=>$_SERVER['REQUEST_URI'], 'error'=>false));
        } catch (Exception $E) {
            $_message = $polaris->cleanStringFromPasswords($E->getMessage());
            $resultArray = Array('result'=>false, 'query'=>$_SERVER['REQUEST_URI'], 'error'=>$_message, 'detailed_error'=>'');
            echo json_encode($resultArray);
        }
    }

    function Show($outputformat='xhtml', $rec=false) {
        global $polaris;

        $this->outputformat = $outputformat;
        if ($this->outputformat == 'json') {
            $this->Process($_REQUEST['event']);
        } elseif (!empty($_FILES)) {
            $this->ProcessFiles($_FILES);
        } else {
            switch($_GET['action']) {
            case '':
                $this->ShowListView($this->state, $this->permission, $this->form->detail, $this->form->masterrecord);
            break;
            case 'edit':
            case 'view':
            case 'insert':
                $this->ShowFormView($this->state, $this->permission, $this->form->detail, $this->form->masterrecord);
            default:
                $this->ShowAction($this->state, $this->permission, $this->form->detail, $this->form->masterrecord);
            break;
            }
        }
    }

    function ShowListView($state, $permission, $detail=false, $masterrecord=false) {
        return null;
    }

    function ShowFormView($state, $permission, $detail=false, $masterrecord=false) {
        return null;
    }

    function ShowAction($state, $permission, $detail=false, $masterrecord=false) {
        return null;
    }

    function GetHTMLForm($filename) {
        return $this->smarty->fetch($filename);
    }

    function __GetCustomFilter() {
        return false;
    }

}
