<?php
require('_shared/module._base.php');

class ModulePLR_Maxia_Urenregistratie extends _BaseModule {
    var $stylesheet;
    var $gen_form;
    var $processed;
    var $currentaction;

    function __construct($moduleid, $module, $clientname=false) {
        global $_GVARS;
        parent::__construct($moduleid, $module, $clientname);

        $this->processed = false;
        $this->stylesheet = 'default.css';
        $this->showControls = true;
        $this->showDefaultButtons = false;
    }

    function ProcessRequest($event=false) {
        $_content = false;

        $this->form->database->userdb->SetFetchMode(ADODB_FETCH_ASSOC);
        try {
            $_func = $_GET['action'];
            if (!empty($_func)) {
                switch($this->moduleid) {
                case 1:
                break;
                }
            }
            if ($_content !== false) {
                echo $_content;
                exit; // stop with the rest of the html
            }
        } catch (Exception $e) {
            echo "Wrong parameters: ".$e;
        }

        return false;
    }

    function ShowListView($state, $permission, $detail=false, $masterrecord=false) {
        switch($this->moduleid) {
        case 1:
            $this->AddModuleJS(['timetracker.js']);
            echo $this->smarty->Fetch('ureninvoeren.tpl.php');
        break;
        }
    }

    function ShowFormView($state, $permission, $detail=false, $masterrecord=false) {
        $_customfilter = $this->__GetCustomFilter();

        $this->gen_form = new base_plrForm($this->form->owner);
        $this->gen_form->LoadRecord(array(12, 15));
        // $_rs = $this->gen_form->GetViewContent($detail=false, $masterrecord=false, $limit=false, $_customfilter);
        // $this->smarty->assign('items', $_rs);
        $this->gen_form->ShowFormView($this->state, $this->permission, $detail=false, $masterrecord=false);
    }

}
