jQuery(function () {
    $("#action_masterinsert").on('click', function (e) {
        e.preventDefault();
        $("#dlgAddUser").modal('show');
    });

    $("#dlgAddUser").on('show.bs.modal', function(e) {
        Polaris.Form.initializeFormValidation("#frmAddUser");
    });

    $("#btn-add-user").on('click', function (e) {
        e.preventDefault();
        var form = $("#frmAddUser");
        if (form.valid()) {
            var data = Polaris.Form.convertToKeyValuePairs(form.serializeArray());
            Polaris.Ajax.postJSON(_servicequery, data, null, 'Gebruiker is toegevoegd', function(data) {
                $("#dlgAddUser").modal('hide');
                window.location.reload();
            });
        }
    });

    /* Show Add user to Application form */

    $("#btnAddUserToApplication").on('click', function (e) {
        e.preventDefault();
        $("#dlgAddUserToApplication").modal('show');
    });

    $("#btn-add-user-application-roles:not(.disabled)").on('click', function (e) {
        e.preventDefault();
        var form = $("#frmAddUserToApplication");
        if (form.valid()) {
            var data = Polaris.Form.convertToKeyValuePairs(form.serializeArray());
            if (data['ROLEGROUPS[]'] === undefined) {
                Polaris.Base.errorDialog('Selecteer minimaal één rol');
            } else {
                data.USER = $("#fldUSERHASH").val() || $(this).data('user');
                Polaris.Ajax.postJSON(_servicequery, data, null, null, function(data) {
                    $("#dlgAddUserToApplication").modal('hide');
                    window.location.reload();
                });
            }
        }
    });

    /* Show Add user to Group form */
    $("#btnAddUserToGroups").on('click', function (e) {
        e.preventDefault();
        $("#dlgAddUserToGroups").modal('show');
    });

    $("#btn-add-user-rolegroups:not(.disabled)").on('click', function (e) {
        e.preventDefault();
        var form = $("#frmAddUserToGroups");
        if (form.valid()) {
            var data = Polaris.Form.convertToKeyValuePairs(form.serializeArray());
            if (data['USERGROUPS[]'] === undefined) {
                Polaris.Base.errorDialog('Selecteer minimaal één groep');
            } else {
                data.USER = $("#fldUSERHASH").val() || $(this).data('user');
                Polaris.Ajax.postJSON(_servicequery, data, null, 'Gebruiker is toegevoegd aan groep', function(data) {
                    $("#dlgAddUserToGroups").modal('hide');
                    window.location.reload();
                });
            }
        }
    });

    /* Change user properties  */
    $("#btnChangeUserProps").on('click', function (e) {
        e.preventDefault();
        $("#dlgChangeUserProps").modal('show');
    });

    $("#btn-change-user-props-save").on('click', function (e) {
        e.preventDefault();
        var form = $("#frmChangeUserProps");
        if (form.valid()) {
            var data = Polaris.Form.convertToKeyValuePairs(form.serializeArray());
            data.USER = $("#fldUSERHASH").val() || $(this).data('user');
            Polaris.Ajax.postJSON(_servicequery, data, null, 'Gebruiker is aangepast', function(data) {
                $("#dlgChangeUserProps").modal('hide');
                window.location.reload();
            });
        }
    });

    // Gemeenschappelijke functie voor het versturen van een AJAX verzoek
    function handleButtonClick(eventType, confirmMessage, successMessage) {
        successMessage = successMessage || null;
        return function (e) {
            e.preventDefault();
            var $this = $(this);
            var executeAction = function (dialogItself) {
                var data = {
                    '_hdnProcessedByModule': true,
                    'event': eventType,
                    'USER': $("#fldUSERHASH").val() || $this.data('user'),
                    'GROUP': $this.data('group'),
                    'APPLICATION': $this.data('application'),
                    'VALUE': $this.prop('checked') ? 1 : 0
                };

                Polaris.Ajax.postJSON(_servicequery, data, null, successMessage, function(data) {
                    if (dialogItself) dialogItself.close();
                    // refresh window
                    window.location.reload();
                });
            }

            if (confirmMessage) {
                Polaris.Base.modalDialog(confirmMessage, {
                    yes: executeAction,
                    cancel: function (dialogItself) {
                        dialogItself.close();
                    }
                });
            } else {
                executeAction();
            }
        };
    }

    // Event handlers for simple user actions
    $("#btnSuspendAccess:not(.disabled)").on('click', handleButtonClick('suspendaccess'));
    $("#btnAssignAdminRole:not(.disabled)").on('click', handleButtonClick('assignadminrole'));
    $("#btnRevokeAdminRole:not(.disabled)").on('click', handleButtonClick('revokeadminrole'));

    $("#btnResetPassword:not(.disabled)").on('click', handleButtonClick('resetpassword', 'Weet u zeker dat de gebruiker bij de volgende login zijn wachtwoord moet wijzigen?'));
    $("#btnDeleteUser:not(.disabled)").on('click', handleButtonClick('deleteuser', 'Weet u zeker dat u deze gebruiker wilt verwijderen?'));
    $("#btnDeleteGroup:not(.disabled)").on('click', handleButtonClick('deletegroup', 'Weet u zeker dat u deze groep wilt verwijderen?'));
    $("#btnGrantAccess:not(.disabled),.ug-grantaccess:not(.disabled)").on('click', handleButtonClick('grantaccess'));
    $(".ug-permissions").on('ifToggled', handleButtonClick('updateapplicationpermissions'));
    $(".ug-revoke-application:not(.disabled)").on('click', handleButtonClick('revokeapplication'));
    $(".ug-revoke-role:not(.disabled)").on('click', handleButtonClick('revokerole'));

});