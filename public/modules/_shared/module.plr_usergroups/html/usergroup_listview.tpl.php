{include file="shared.tpl.php"}

<div class="element-detail-box">

{if $smarty.get.tag !== 'groups'}
<div class="ug-stats">
    <table>
        <tr>
            <td class="">
                <div class="ug-stat m-r">
                    <div class="ug-stat-label">{lang totalusers}</div>
                    <div class="ug-stat-value text-success"><a href="?tag=allusers">{$total_users}</a></div>
                </div>
            </td>
            <td>
                <div class="ug-stat  m-r">
                    <div class="ug-stat-label">{lang activeusers}</div>
                    <div class="ug-stat-value text-success"><a href="?tag=users">{$active_users}</a></div>
                </div>
            </td>
            <td>
                <div class="ug-stat">
                    <div class="ug-stat-label">{lang admins}</div>
                    <div class="ug-stat-value text-success"><a href="?tag=admins">{$actieve_admins}</a></div>
                </div>
            </td>
        </tr>
    </table>
</div>
{/if}

{section name=i loop=$items}
    {if $smarty.section.i.first}
    <form name="users" id="dataform" method="POST" action=".">
    <input type="hidden" name="_hdnDatabaseID" value="{$databaseid}" />
    <input type="hidden" name="_hdnAction" value="{#rec_delete#}" />

    <table id="datalistview" class="table table-striped table-hover dataTable group-table ug-overview" summary="overzicht">
        <thead>
            <tr>
            {if $items[i].USERORGROUP eq 'USER'}
                <th>{lang user}</th>
                <th>{lang last_active}</th>
                <th>{lang status}</th>
                <th>{lang action}</th>
            {else}
            <th>{lang group}</th>
                <th class="">{lang members}</th>
                <th>{lang access_to}</th>
                <th>{lang action}</th>
            {/if}
            </tr>
        </thead>
        <tbody class="">
    {/if}

    <tr class="dr {if $items[i].ACCOUNTDISABLED == 'Y'}disabled{/if}" id="row@{$items[i].PLR__RECORDID}">
    {if $items[i].USERORGROUP eq 'USER'}
        <td><a title="{lang edit}" href="{$callerquery}edit/{$items[i].PLR__RECORDID}/">
            <div class="ug-avatar">
                {if $items[i].USERORGROUP eq 'USER'}
                    {icon name="md-account_circle" size="3x"}
                {else}
                    {icon name="md-communities" size="3x"}
                {/if}
            </div>
            <div class="ug-fullname">
                {if $items[i].USERORGROUP eq 'USER'}{$items[i].FULLNAME}{else}{$items[i].DESCRIPTION}{/if}
                {if $items[i].CLIENTADMIN eq 'Y'}<mark title="Soort gebruiker" class="ug-tag">{lang client_admin_short}</mark>{/if}
            </div>

            <div class="ug-username">{$items[i].USERGROUPNAME}</div>
        </a></td>
        <td>{$items[i].LASTLOGINDATE|date_format:"%e %h %Y"|default:"nog niet ingelogd"|lower}</td>
        <td>{if $items[i].ACCOUNTDISABLED == 'Y'}Opgeschort{else}Actief{/if}</td>
        <td>
            {if $items[i].ACCOUNTDISABLED == 'Y'}
            <a href="#" class="ug-grantaccess" data-user="{$items[i].PLR__RECORDID}">Toegang herstellen</a>
            {else}
            <a href="{$callerquery}edit/{$items[i].PLR__RECORDID}/">{lang show_details}</a>
            {/if}
        </td>
    {else}
        <td width="80%">
            <div class="ug-avatar">{icon name="md-communities" size="3x"}</div>
            <div class="ug-list-info">
                <div class="ug-fullname">
                    <a title="{lang edit}" href="{$callerquery}edit/{$items[i].PLR__RECORDID}/">{$items[i].FULLNAME}</a>
                    <p class="small text-muted">{$items[i].DESCRIPTION}</p>
                </div>
                <div class="ug-username">id: {$items[i].USERGROUPNAME}
                </div>
            </div>
        </td>
        <td>
            <div class="dropdown xdropdown-hover">
                {$items[i].MEMBERCOUNT} <i title="{$items[i].MEMBERS}" data-toggle="dropdown" class="material-symbols-outlined">info</i>
                <ul class="dropdown-menu">
                    <li class="dropdown-header">De leden van de groep</li>
                    {section name=j loop=$items[i].MEMBERS}
                    <li class="dropdown-item"><a href="{$callerquery}edit/{$items[i].MEMBERS[j].MEMBERID}/">{$items[i].MEMBERS[j].MEMBERNAME}</a></li>
                    {sectionelse}
                    <li class="dropdown-header">- Geen leden -</li>
                    {/section}
                </ul>
            </div>
        </td>
        <td>{$items[i].MEMBERSHIPS}</td>
        <td><a href="{$callerquery}edit/{$items[i].PLR__RECORDID}/">{lang show_details}</a></td>
    {/if}
    </tr>

    {if $smarty.section.i.last}
    </tbody>
    </table>

    </form>
    {/if}
{sectionelse}
<h1>{lang no_users_exists}</h1>
{/section}

</div>

{include file="usergroup_modalforms.tpl.php"}