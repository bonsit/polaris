</form>

<div class="modal inmodal" id="dlgAddUser" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only">Sluiten</span></button>
                <h4 class="modal-title">Nieuwe gebruiker toevoegen</h4>
            </div>

            <div class="modal-body tabs-container ">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item active">
                        <a class="nav-link" data-toggle="tab" href="#add-user-tab" role="tab">Gebruiker
                            toevoegen</a>
                    </li>
                    <li class="nav-item disabled">
                        <a class="nav-link" data-toggle="tab" href="#invite-user-tab" role="tab">Gebruiker
                            uitnodigen</a>
                    </li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <!-- Add User Tab -->
                    <div class="tab-pane active" id="add-user-tab" role="tabpanel">
                        <div class="panel-body">
                            <form class="form-horizontal password-change-validation" id="frmAddUser">
                                <input type="hidden" name="event" value="adduser">
                                <input type="hidden" name="_hdnProcessedByModule" value="true">

                                <div class="form-group">
                                    <label class="col-lg-4 control-label">Login naam</label>
                                    <div class="col-lg-8">
                                        <input type="text" autofocus placeholder="Login naam"
                                            class="text validate nowhitespace required validation_allchars form-control"
                                            name="USERGROUPNAME">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-4 control-label">Wachtwoord</label>
                                    <div class="col-lg-8">
                                        <input type="password" autocomplete="new-password" placeholder="Wachtwoord"
                                            class="form-control validate cp_password required" name="PASSWORD">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-4 control-label">Typ nogmaals</label>
                                    <div class="col-lg-8">
                                        <input type="password" autocomplete="new-password" placeholder="Wachtwoord"
                                            class="form-control validate cp_password_comfirm required"
                                            name="PASSWORD_VALIDATE">
                                    </div>
                                    <span class="cp_retypecorrect"></span>
                                </div>

                                <div class="text-danger font-larger cp_password_strength"></div>

                                <hr>

                                <div class="form-group">
                                    <label class="col-lg-4 control-label">Volledige naam</label>
                                    <div class="col-lg-8">
                                        <input type="text" placeholder="Volledige naam"
                                            class="text validate required validation_allchars form-control"
                                            name="FULLNAME">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-4 control-label">Email</label>
                                    <div class="col-lg-8">
                                        <input type="email" placeholder="Email (optioneel)" class="form-control"
                                            name="EMAILADDRESS">
                                    </div>
                                </div>

                                <hr>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-12 m-b-sm">
                                            <label class="control-label">Tot welke applicatie heeft deze gebruiker
                                                toegang?</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            {$applications}
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                    <!-- Invite User Tab -->
                    <div class="tab-pane" id="invite-user-tab" role="tabpanel">
                        <div class="panel-body">
                            <form class="form-horizontal" id="frmInviteUser">
                                <input type="hidden" name="event" value="inviteuser">
                                <input type="hidden" name="_hdnProcessedByModule" value="true">

                                <p class="text-danger">Deze functionaliteit volgt later</p>
                                <div class="form-group">
                                    <label class="col-lg-4 control-label">Email</label>
                                    <div class="col-lg-8">
                                        <input type="email" placeholder="Email" readonly="readonly"
                                            class="form-control" name="INVITE_EMAIL">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-4 control-label">Bericht</label>
                                    <div class="col-lg-8">
                                        <textarea placeholder="Bericht (optioneel)" readonly="readonly"
                                            class="form-control" name="INVITE_MESSAGE"></textarea>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Annuleren</button>
                <button type="button" class="btn btn-primary btnSave" id="btn-add-user">Toevoegen</button>
            </div>
        </div>
    </div>
</div>

<div class="modal inmodal" id="dlgAddUserToApplication" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only">Sluiten</span></button>
                <h4 class="modal-title text-left m-b-md">Toegang geven</h4>
                <p class="font-larger text-left">Tot deze producten heeft {$item.FULLNAME|default:$item.USERGROUPNAME}
                    nog geen toegang. Wijs een productrol toe om het vereiste toegangsniveau te verlenen.</p>
            </div>

            <div class="modal-body">
                <form class="form-horizontal" id="frmAddUserToApplication">
                    <input type="hidden" name="event" value="adduserapplicationroles" />
                    <input type="hidden" name="_hdnProcessedByModule" value="true" />
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-12 m-b-sm">
                                <div class="col-lg-5">Product</div>
                                <div class="col-lg-7"><b>Productrollen</b></div>
                            </div>
                        </div>
                        <div class="row">
                            {section name=i loop=$applications}
                            <div class="col-lg-12">
                                <div class="form-group">

                                    <label class="col-lg-5 control-label">
                                        <span class="usergroup-applicatie-icon"
                                            style="background-color:{$applications[i].APPTILECOLOR};"
                                            title="{$applications[i].APPLICATIONNAME}">
                                            <i class="material-symbols-filled md-1x">{$applications[i].IMAGE}</i>
                                        </span>

                                        {$applications[i].APPLICATIONNAME}
                                    </label>
                                    <div class="col-lg-7">
                                        <select class="select2 form-control" data-select_width="100%"
                                            name="ROLEGROUPS[]" multiple>
                                            {section name=j loop=$applications[i].ROLES}
                                            <option value="{$applications[i].ROLES[j].ROLEGROUPHASH}">
                                                {$applications[i].ROLES[j].ROLENAME}</option>
                                            {/section}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            {sectionelse}
                            <p class="text-info font-larger font-bold">De gebruiker heeft al toegang tot alle beschikbare
                                applicaties.</p>
                            {/section}
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Annuleren</button>
                <button type="button" class="btn btn-primary btnSave {if $applications|count == 0}disabled{/if}" id="btn-add-user-application-roles">Toegang
                    geven</button>
            </div>
        </div>
    </div>
</div>

<div class="modal inmodal" id="dlgAddUserToGroups" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only">Sluiten</span></button>
                <h4 class="modal-title text-left m-b-md">Voeg {$item.FULLNAME|default:$item.USERGROUPNAME} toe aan
                    groepen</h4>
            </div>

            <div class="modal-body">
                <form class="form-horizontal" id="frmAddUserToGroups">
                    <input type="hidden" name="event" value="adduserroles" />
                    <input type="hidden" name="_hdnProcessedByModule" value="true" />
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-12 m-b-sm">
                                <div class="col-lg-12">Groepsnaam</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">

                                    <div class="col-lg-12">
                                        <select class="select2 form-control" data-select_width="100%" name="USERGROUPS[]"
                                            multiple>
                                            {section name=j loop=$usergroups}
                                                {if $usergroups[j].IS_MEMBER == 'N'}
                                                <option value="{$usergroups[j].PLR__RECORDID}">{$usergroups[j].USERGROUPNAME}</option>
                                                {/if}
                                            {/section}
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Annuleren</button>
                <button type="button" class="btn btn-primary btnSave" id="btn-add-user-rolegroups">Groepen
                    toevoegen</button>
            </div>
        </div>
    </div>
</div>

<div class="modal inmodal" id="dlgChangeUserProps" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only">Sluiten</span></button>
                <h4 class="modal-title">Gebruiker wijzigen</h4>
            </div>

            <div class="modal-body">
                <form class="form-horizontal password-change-validation" id="frmChangeUserProps">
                    <input type="hidden" name="event" value="changeuser">
                    <input type="hidden" name="_hdnProcessedByModule" value="true">

                    <div class="form-group">
                        <label class="col-lg-4 control-label">Login naam</label>
                        <div class="col-lg-8">
                            <input type="text" autofocus placeholder="Login naam"
                                class="text validate nowhitespace required validation_allchars form-control"
                                name="USERGROUPNAME" value="{$item.USERGROUPNAME}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-4 control-label">Volledige naam</label>
                        <div class="col-lg-8">
                            <input type="text" placeholder="Volledige naam"
                                class="text validate required validation_allchars form-control"
                                name="FULLNAME" value="{$item.FULLNAME}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-4 control-label">Email</label>
                        <div class="col-lg-8">
                            <input type="email" placeholder="Email (optioneel)" class="form-control"
                                name="EMAILADDRESS" value="{$item.EMAILADDRESS}">
                        </div>
                    </div>

                    <hr>

                    <div class="form-group">
                        <label class="col-lg-4 control-label">Wachtwoord</label>
                        <div class="col-lg-8">
                            <input type="password" autocomplete="new-password" placeholder="Wachtwoord"
                                class="form-control validate cp_password" name="PASSWORD">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-4 control-label">Typ nogmaals</label>
                        <div class="col-lg-8">
                            <input type="password" autocomplete="new-password" placeholder="Wachtwoord"
                                class="form-control validate cp_password_comfirm"
                                name="PASSWORD_VALIDATE">
                            <span class="cp_retypecorrect"></span>
                        </div>
                    </div>

                    <div class="text-danger font-larger cp_password_strength"></div>
                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Annuleren</button>
                <button type="button" class="btn btn-primary btnSave" id="btn-change-user-props-save">Gebruiker wijzigen</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    /* initPasswordValidation */
    jQuery(function () {
        $('.password-change-validation').each(function() {
            var $this = $(this);
            Polaris.Base.initPasswordValidation($this, function(result) {
                log(result);
                if (result == 'passwords_ok') {
                    $(".cp_retypecorrect", $this).text('');
                    $("#btn-add-user, #btn-change-user-props-save").removeClass("disabled");
                } else {
                    $(".cp_retypecorrect", $this).text('Wachtwoorden zijn niet gelijk!');
                    $("#btn-add-user, #btn-change-user-props-save").addClass("disabled");
                }
            });
        });
    });
</script>
