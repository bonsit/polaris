{include file="shared.tpl.php"}

<input type="hidden" id="fldUSERHASH" value="{$grouphash}">

<div class="element-detail-box font-larger">
    <div class="row">
        <div class="col-md-10">
            <h1 class="font-bold no-margins">
                {$item.USERGROUPNAME}
            </h1>
            <p>{$item.DESCRIPTION}</p>
        </div>
        <div class="col-md-2">
            <div class="btn-group">
                <button class="btn btn-success">Groepsleden toevoegen</button>
                <div class="dropdown">
                    <a class="usergroup-user-actions" data-toggle="dropdown">{icon name="more_horiz" size="2x"}</a>
                    <div class="dropdown-menu dropdown-menu-right {if $editing_yourself}disabled{/if}">
                        <li class="dropdown-item {if $editing_yourself}disabled{/if} text-danger" {if $editing_yourself}data-toggle="tooltip" title="Je kunt jezelf niet uit de organisatie verwijderen"{/if}><a href="#" id="btnDeleteGroup">Groep verwijderen</a></li>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="ug-stats">
                <table>
                    <tr>
                        <td class="">
                            <div class="ug-stat m-r">
                                <div class="ug-stat-label">{lang members}</div>
                                <div class="ug-stat-value">{$members|count}</div>
                            </div>
                        </td>
                        <td>
                            <div class="ug-stat  m-r">
                                <div class="ug-stat-label">{lang productaccess}</div>
                                <div class="ug-stat-value">1{$active_users}</div>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <table id="datalistview" class="table table-striped table-hover dataTable group-table ug-overview" summary="overzicht">
                <thead>
                    <tr>
                        <th style="width:10%">{lang user}</th>
                        <th>{lang last_active}</th>
                        <th>{lang status}</th>
                        <th>{lang action}</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody class="">
                    {section name=i loop=$members}
                    <tr class="dr {if $members[i].ACCOUNTDISABLED == 'Y'}disabled{/if}" id="row@{$members[i].PLR__RECORDID}">
                        <td>
                            <div class="ug-username">
                                <a title="{lang edit}" href="{$callerquery}edit/{$members[i].PLR__RECORDID}/">
                                    <span class="ug-username-text">{$members[i].FULLNAME}</span>
                                </a>

                            </div>
                            <small class="text-muted">{$members[i].EMAILADDRESS}</small>
                        </td>
                        <td>{$members[i].LASTLOGINDATE|date_format:"%d-%m-%Y"}</td>
                        <td>{if $members[i].ACCOUNTDISABLED == 'Y'}<span class="label label-danger">Account opgeschort</span>{else}Actief{/if}</td>
                        <td><a href="{$callerquery}edit/{$members[i].PLR__RECORDID}/">{lang show_details}</a></td>
                        <td>
                            <div class="dropdown">
                                <a class="usergroup-user-actions" data-toggle="dropdown">{icon name="more_horiz" size="2x"}</a>
                                <div class="dropdown-menu dropdown-menu-right {if $editing_yourself}disabled{/if}">
                                    <li class="dropdown-item {if $editing_yourself}disabled{/if}" {if $editing_yourself}data-toggle="tooltip" title="Je kunt jezelf niet verwijderen als organisatiebeheerder"{/if}><a href="#" id="btnAssignAdminRole">Lid verwijderen van groep</a></li>
                                </div>
                            </div>
                        </td>
                    </tr>
                    {/section}
                </tbody>
            </table>

        </div>
    </div>
</div>

{include file="usergroup_modalforms.tpl.php"}