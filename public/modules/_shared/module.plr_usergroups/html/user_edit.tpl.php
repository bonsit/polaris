{include file="shared.tpl.php"}

<input type="hidden" id="fldUSERHASH" value="{$userhash}">

<div class="element-detail-box font-larger">
    <div class="row">
        <div class="col-md-11">
            <h1 class="font-bold no-margins">
                {$item.FULLNAME}
            </h1>
            <br>
            <p class="m-b-md">
                {if $item.ACCOUNTDISABLED == 'Y'}
                <span class="font-larger label label-danger">Account opgeschort</span>
                {else}
                    {if $item.LASTLOGINDATE}
                    Laatst actief op {$item.LASTLOGINDATE|date_format:"%d-%m-%Y"}.
                    {else}
                    Nog niet actief geweest.
                    {/if}
                {/if}
            </p>
        </div>
        <div class="col-md-1">
            <div class="dropdown">
                <a class="usergroup-user-actions" data-toggle="dropdown">{icon name="more_horiz" size="2x"}</a>
                <div class="dropdown-menu dropdown-menu-right {if $editing_yourself}disabled{/if}">

                    {if $item.CLIENTADMIN == 'N'}<li class="dropdown-item {if $editing_yourself}disabled{/if}" {if $editing_yourself}data-toggle="tooltip" title="Je kunt jezelf niet aanwijzen als organisatiebeheerder"{/if}><a class="{if $editing_yourself}disabled{/if}" href="#" id="btnAssignAdminRole">Organisatiebeheerdersrol toewijzen</a></li>{/if}
                    {if $item.CLIENTADMIN == 'Y'}<li class="dropdown-item {if $editing_yourself}disabled{/if}" {if $editing_yourself}data-toggle="tooltip" title="Je kunt jezelf niet verwijderen als organisatiebeheerder"{/if}><a class="{if $editing_yourself}disabled{/if}"  href="#" id="btnRevokeAdminRole">Organisatiebeheerdersrol verwijderen</a></li>{/if}
                    <li class="dropdown-item {if $editing_yourself}disabled{/if}" {if $editing_yourself}data-toggle="tooltip" title="Je kunt momenteel niet het wachtwoord voor jezelf opnieuw instellen"{/if}><a class="{if $editing_yourself}disabled{/if}" href="#" id="btnResetPassword">Vraag om wachtwoord opnieuw in te stellen</a></li>
                    {if $item.ACCOUNTDISABLED == 'N'}<li class="dropdown-item {if $editing_yourself}disabled{/if}" {if $editing_yourself}data-toggle="tooltip" title="Je kunt de toegang tot deze organisatie niet opschorten voor jezelf"{/if}><a href="#" id="btnSuspendAccess">Toegang opschorten</a></li>{/if}
                    {if $item.ACCOUNTDISABLED == 'Y'}<li class="dropdown-item"><a href="#" id="btnGrantAccess">Toegang herstellen</a></li>{/if}
                    <li class="dropdown-item {if $editing_yourself}disabled{/if} text-danger" {if $editing_yourself}data-toggle="tooltip" title="Je kunt jezelf niet uit de organisatie verwijderen"{/if}><a href="#" id="btnDeleteUser">Gebruiker verwijderen</a></li>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="usergroup-details-box">

                <div class="col-md-12">
                    <img src="/assets/images/profile-default.png" class="usergroup-profile-image m-b-md" alt="profile">
                </div>

                <div class="m-b-md">
                    <p>Volledige naam</p>
                    <p class="copy-clipboard font-bold">{$item.FULLNAME}</p>
                </div>

                <div class="m-b-md">
                    <p>E-mailadres</p>
                    <p class="copy-clipboard font-bold">{$item.EMAILADDRESS|default:'onbekend'}</p>
                </div>

                <div class="m-b-md">
                    <p>Inlognaam</p>
                    <p class="copy-clipboard font-bold">{$item.USERGROUPNAME}</p>
                </div>

                <button class="btn btn-default" id="btnChangeUserProps"
                {if $item.ACCOUNTDISABLED == 'Y' or $item.ROLE == 'API'}disabled
                data-toggle="tooltip" title="Je kunt deze gebruiker niet wijzigen"{/if}
                >Wijzigen</button>

            </div>
            <a href="{$callerquery}" class="btn btn-default">{icon name="md-arrow_back"} Terug naar overzicht</a>
        </div>
        <div class="col-md-8">
            <div class="usergroup-role-box usergroup-role-box-default">

                {if $item.CLIENTADMIN == 'Y'}
                <div class="alert alert-info">
                    <h3>{$item.FULLNAME} is een organisatiebeheerder</h3>
                    Hiermee krijg je het hoogste niveau van rechten voor het beheren van instellingen, gebruikers en groepen voor je organisatie.
                </div>
                {/if}
                <div class="usergroup-role-box-heading">
                    {if $item.ACCOUNTDISABLED == 'N'}<button id="btnAddUserToApplication" class="pull-right btn btn-default">Toegang geven</button>{/if}
                    <h3 class="usergroup-role-box-title">{lang product_access}</h3>
                    <p>Beheer bestaande applicatierollen en applicatietoegang. <a href="#">Meer informatie over hoe producttoegang werkt</a>.</p>
                </div>
                <div class="usergroup-role-box-body">

                    <table class="table">
                        <thead>
                            <tr>
                                <th width="20%">Applicatie</th>
                                <th width="20%">Productrollen</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        {section name=i loop=$userapps}
                            <tr>
                                <td>
                                    <span class="usergroup-applicatie-icon" style="background-color:{$userapps[i].APPTILECOLOR};" title="{$userapps[i].APPLICATIONNAME}">
                                        <i class="material-symbols-filled md-1x">{$userapps[i].IMAGE}</i>
                                    </span>
                                    {$userapps[i].APPLICATIONNAME}
                                </td>
                                <td>
                                    <div class="dropdown">
                                        <button type="button" class="btn btn-default input-block-level dropdown-toggle" data-toggle="dropdown">
                                        {$userapps[i].PERMISSIONS_LABEL|truncate:80:"...":true}
                                        {icon name="arrow_drop_down"}
                                        </button>

                                        <ul class="dropdown-menu">
                                        {section name=j loop=$userapps[i].ALL_PERMISSIONS}
                                            <li class="dropdown-item">
                                                <a href="#" class="">
                                                    <label>
                                                        <input type="checkbox" class="icheck ug-permissions" name="user[]" data-user="{$userapps[i].ALL_PERMISSIONS[j].USER}" data-group="{$userapps[i].ALL_PERMISSIONS[j].GROUP}" data-application="{$userapps[i].ALL_PERMISSIONS[j].APPLICATION}" value="checked" {if $userapps[i].ALL_PERMISSIONS[j].CURRENTLYSELECTED eq 1}checked="checked"{/if} />
                                                        {$userapps[i].ALL_PERMISSIONS[j].GROUPNAME}
                                                    </label>
                                                </a>
                                            </li>
                                        {/section}
                                        </ul>
                                    </div>
                                </td>
                                <td class="text-right">
                                    <div class="dropdown">
                                        <a class="usergroup-group-actions" data-toggle="dropdown">{icon name="more_horiz" size="2x"}</a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <li class="dropdown-item"><a href="#" class="ug-revoke-application" data-user="{$userapps[i].ALL_PERMISSIONS[0].USER}" data-application="{$userapps[i].ALL_PERMISSIONS[0].APPLICATION}">Toegang tot product intrekken</a></li>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        {/section}

                        </tbody>
                    </table>
                </div>
            </div>

            <div class="usergroup-role-box usergroup-role-box-default">
                <div class="usergroup-role-box-heading">
                    {if $item.ACCOUNTDISABLED == 'N'}<button id="btnAddUserToGroups" class="pull-right btn btn-default">Aan groepen toevoegen</button>{/if}
                    <h3 class="usergroup-role-box-title">{lang groupmembership}</h3>
                    <p>Gebruik groepen om rechten, beperkingen en producttoegang te beheren.</p>
                </div>
                <div class="usergroup-role-box-body">

                    <table class="table">
                        <thead>
                            <tr>
                                <th width="20%">Groep</th>
                                <th>Toegang tot</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        {section name=i loop=$usergroups}
                            {if $usergroups[i].IS_MEMBER == 'Y'}
                                <tr>
                                    <td>
                                        <div><a href="{$callerquery}edit/{$usergroups[i].PLR__RECORDID}/">{$usergroups[i].USERGROUPNAME}</a></div>
                                        <small class="text-muted">{$usergroups[i].DESCRIPTION}</small>
                                    </td>
                                    <td>
                                    </td>
                                    <td class="text-right">
                                        <div class="dropdown">
                                            <a class="usergroup-group-actions" data-toggle="dropdown">{icon name="more_horiz" size="2x"}</a>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <li class="dropdown-item"><a class="ug-revoke-role" data-group="{$usergroups[i].PLR__RECORDID}" href="#">Gebruiker uit deze groep verwijderen</a></li>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            {/if}
                        {/section}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

{include file="usergroup_modalforms.tpl.php"}