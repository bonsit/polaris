</form>
[[include file="shared.tpl.php"]]

<link rel="stylesheet" type="text/css" href="[[$moduleroot]]css/default.css">
<link rel="stylesheet" type="text/css" href="[[$moduleroot]]css/dropzone.css" />
<script src="[[$moduleroot]]js/dropzone.js" type="text/javascript"></script>
<script src="[[$moduleroot]]js/jquery.shuffleLetters.js" type="text/javascript"></script>
<script src="[[$moduleroot]]js/script.js" type="text/javascript"></script>

<div class="companies">

[[section name=i loop=$companies]]
<div class="company_dropzone">
<img class="company_logo" src="[[$moduleroot]]html/[[$companies[i].company_logo]]" />
<form action="[[$approot]]/module/st_platform/" class="dropzone" id="my-awesome-dropzone">
    <input type="hidden" name="_hdnProcessedByModule" value="true" />
    <input type="hidden" name="_hdnAction" value="scanfile" />
    <input type="hidden" name="company" value="[[$companies[i].company]]" />
    <input type="hidden" name="account" value="[[$companies[i].account]]" />

    <i class="fa fa-file-o fa-4x"></i><br><br>
</form>
</div>
[[/section]]

</div>

<!-- <p id="scannerresult" class="scannerresult"></p> -->
