</form>
[[include file="shared.tpl.php"]]
<script src="[[$moduleroot]]js/script.js" type="text/javascript"></script>

<form action="[[$callerquery]]" method="GET">

<p>Zoek transactie: <input type="text" name="transaction" value="[[$smarty.get.transaction]]" style="width:50vw;" />
<input type="submit" value="Zoek" />
</p>
<br/>

<table class="table table-striped">
    <tr>
        <td>transactionHash</td>
        <td class="">[[$receipt->transactionHash]]</td>
    </tr>
    <tr>
        <td>transactionIndex</td>
        <td>[[$receipt->transactionIndex]]</td>
    </tr>
    <tr>
        <td>blockHash</td>
        <td>[[$receipt->blockHash]]</td>
    </tr>
    <tr>
        <td>blockNumber</td>
        <td>[[$receipt->blockNumber]]</td>
    </tr>
    <tr>
        <td>gasUsed</td>
        <td>[[$receipt->gasUsed]]</td>
    </tr>
    <tr>
        <td>cumulativeGasUsed</td>
        <td>[[$receipt->cumulativeGasUsed]]</td>
    </tr>
    <tr>
        <td>contractAddress</td>
        <td>[[$receipt->contractAddress]]</td>
    </tr>
    <tr>
        <td>logs</td>
        <td>
            [[$receipt->logs|default:''|implode:"\n"]]
        </td>
    </tr>
</table>

