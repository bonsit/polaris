<?php
ini_set('max_execution_time', 100);

// Require the Composer autoloader.
require 'vendor/autoload.php';
require_once '_shared/module._base.php';
require_once 'includes/mainactions.inc.php';
require_once PLR_DIR.'/php_includes/ethereum/ethereum.php';

use Aws\S3\S3Client;

class Module extends _BaseModule {

    function Module($moduleid, $module) {
        global $_GVARS;
        global $polaris;

        parent::__construct($moduleid, $module);

        $this->processed = false;

        $this->isAngularApplication = true;

        /**
        * What client are we talking about? (nog doen: eventueel selecteerbaar maken)
        */
        $this->clientid = $_SESSION['clientid'];

        /* Load the user database */
        $_databaseid = 1;
        if (isset($_databaseid)) {
            $db = new plrDatabase($polaris);
            $db->LoadRecord(array($this->clientid, $_databaseid));
            $this->userdatabase = $db->connectUserDB();
        }
        if (isset($_POST['_hdnProcessedByModule'])) {
            $_allesRecord = $_POST;

            try {
                switch($_POST['_hdnAction']) {
                case 'scanfile':
                    $_tmpfile = $_FILES['file']['tmp_name'];
                    $_filename = $_FILES['file']['name'];
                    $_company = $_POST['company'];
                    $_account = $_POST['account'];
                    $_hash = hash_file('sha256', $_tmpfile);
                    $_bcInfo = $this->StoreHashOnBlockchain($_hash);
                    $_storageInfo = $this->StoreFileOnCloud($_hash, $_tmpfile, $_filename);
                    $this->LogAssetStorage($_company, $_account, $_filename, $_hash, $_bcInfo, $_storageInfo);
                    $result = arraY('account'=>$_account, 'hash'=>$_hash, 'blockchain'=>$_bcInfo, 'storage'=>$_storageInfo);
                break;
                }

                $resultArray = Array('result'=>$result, 'query'=>$_SERVER['REQUEST_URI'], 'error'=>false);
                echo json_encode($resultArray);
                exit;
            } catch (Exception $E) {
                echo $E;
                $detailedmessage = $E->completemsg."\r\n".parse_backtrace(debug_backtrace());

                $resultArray = Array('result'=>false, 'query'=>$_SERVER['REQUEST_URI'], 'error'=>$E->msg, 'detailed_error'=>$detailedmessage);
                echo  json_encode($resultArray);
            }
        }
    }

    function LogAssetStorage($_company, $_account, $_filename, $_hash, $_bcInfo, $_storageInfo) {
        $_sql = "INSERT INTO asset_logging (company, account, orifilename, hash, bcinfo, objecturl, create_datetime) VALUES (?,?,?,?,?,?,NOW())";

        $this->userdatabase->Execute($_sql, array($_company, $_account, $_filename, $_hash, $_bcInfo, $_storageInfo));
    }

    function awsGetProfile() {
        global $_CONFIG;

        return array(
            'region' => $_CONFIG['AWS_REGION'],
            'version' => 'latest',
            'credentials' => array(
                'key' => $_CONFIG['AWS_ACCESS_KEY_ID'],
                'secret'  => $_CONFIG['AWS_SECRET_ACCESS_KEY'],
            )
        );
    }

    function getTransactionReceipt($ethereum, $transaction) {
        if (isset($transaction)) {
            $_receipt = $ethereum->eth_getTransactionReceipt($transaction);
            return $_receipt;
        } else {
            return false;
        }
    }

    function dumpEthereumInfo($ethereum, $transaction, $from, $to) {
        var_dump('net_version '.$ethereum->net_version());
        var_dump('net_listening '.$ethereum->net_listening());
        var_dump('net_peerCount '.$ethereum->net_peerCount());
        var_dump('eth_coinbase '.$ethereum->eth_coinbase());
        var_dump('eth_mining '.$ethereum->eth_mining());
        var_dump('eth_hashrate '.$ethereum->eth_hashrate());
        var_dump('eth_gasPrice '.$ethereum->eth_gasPrice());
        var_dump('accounts ', $ethereum->eth_accounts());
        var_dump($ethereum->eth_getTransactionReceipt($transaction));

        var_dump(hexdec($ethereum->eth_getBalance($from)), $ethereum->eth_getBalance($from));
    }

    function StoreHashOnBlockchain($hash) {
        $ethereum = new Ethereum('ethereum', 8545);

        $_accounts = $ethereum->eth_accounts();
        $_from = $ethereum->eth_coinbase();
        $_to = $_accounts[1];

        $transaction = new Ethereum_Transaction($_from, $_to, $gas=null, $gasPrice=null, $value=100, $data=$hash);
        $_result = $ethereum->eth_sendTransaction($transaction);

        //$this->dumpEthereumInfo($ethereum, $_result, $_from, $_to);

        return $_result;
    }

    function StoreFileOnCloud($hash, $tmpfile, $filename) {
        global $_CONFIG;

        // Upload a publicly accessible file. The file size and type are determined by the SDK.
        try {
            // Instantiate an Amazon S3 client.
            $_s3 = S3Client::factory($this->awsGetProfile());

            $_awsBucket = 'elasticbeanstalk-eu-central-1-387268653687';
            $_folder = 'steeltrace';
            $_ext = pathinfo($filename, PATHINFO_EXTENSION);
            $_keyname = $_folder.'/'.$hash.'.'.$_ext;

            $_result = $_s3->putObject([
                'Bucket'    => $_awsBucket,
                'Key'       => $_keyname,
                'SourceFile'=> $tmpfile,
                'ACL'       => 'public-read',
                'Metadata'  => array(
                    'Company' => 'Kropman',
                    'OriFile' => $filename
                )
            ]);
            $_res = $_result->toArray();
            return $_res['ObjectURL'];
        } catch (Aws\S3\Exception\S3Exception $e) {
            echo "There was an error uploading the file.\n";
            var_dump($e);
        }
    }

    function ShowScanner($ethereum) {
        $_accounts = $ethereum->eth_accounts();

        $_companies = array(
            array(
                'company'       => 'MCL',
                'company_logo'  => 'mcl-logo.png',
                'account'       => $_accounts[0]
            ),
            array(
                'company'       => 'Kropman',
                'company_logo'  => 'kropman-logo.png',
                'account'       => $_accounts[1]
            ),
            array(
                'company'       => 'Shell',
                'company_logo'  => 'shell-logo.png',
                'account'       => $_accounts[2]
            ),
        );
        $this->smarty->assign('companies', $_companies);
        $this->smarty->display("scan.tpl.php");
    }

    function ShowBrowser($ethereum) {
        $_transaction = $_GET['transaction'];
        $this->smarty->assign('receipt', $this->getTransactionReceipt($ethereum, $_transaction));
        $this->smarty->display("browse.tpl.php");
    }

    function Process() {
        global $_GVARS;
    }

    function Show($outputformat='xhtml', $rec=false) {
        parent::Show($outputformat, $rec);

        switch($this->outputformat) {
            case 'xhtml':
                $this->showControls = false;
                $this->showDefaultButtons = false;

                $ethereum = new Ethereum('ethereum', 8545);

                switch ($this->moduleid) {
                    case '1':
                        $this->ShowScanner($ethereum);

                        break;

                    case '2':
                        $this->ShowBrowser($ethereum);

                        break;

                    default:
                        $this->smarty->display("main.tpl.php");
                        break;
                }
        }
    }

}