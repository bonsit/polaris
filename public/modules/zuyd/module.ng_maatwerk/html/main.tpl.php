<script type="text/javascript" src="/bower_components/angular/angular.js"></script>
<script type="text/javascript" src="https://cdn.rawgit.com/lorenzofox3/Smart-Table/master/dist/smart-table.js"></script>
<script type="text/javascript" src="[[$moduleroot]]/js/app.js"></script>

<div class='container' ng-app='smarttabledemo' ng-controller='smarttabledemo'>
  <h3>Minimal Angular/Smart-Table Demo</h3>
  <table st-table='data' st-select-row="row" st-select-mode="multiple" class='table table-striped'>
    <thead>
      <tr>
        <th colspan='999'>
          <input st-search class='form-control' type='search' placeholder='Search' ng-model='search' />
        </th>
      </tr>
      <tr>
        <th st-sort='OPLEIDINGTYPE'>opleidingtype</th>
        <th st-sort='OPLEIDINGNAAM'>opleidingnaam</th>
        <th st-sort='OPLEIDINGNAAMEN'>opleidingnaam Engels</th>
        <th st-sort='IS_DEELTIJD'>is_deeltijd</th>
      </tr>
    </thead>
    <tbody>
      <tr st-select-row='row' ng-repeat='row in data'>
        <td>{{row.OPLEIDINGTYPE}}</td>
        <td>{{row.OPLEIDINGNAAM}}</td>
        <td>{{row.OPLEIDINGNAAMEN}}</td>
        <td>{{row.IS_DEELTIJD}}</td>
      </tr>
    </tbody>
  </table>
</div>
