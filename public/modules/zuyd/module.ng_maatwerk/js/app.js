'use strict';

angular.module('smarttabledemo', ['smart-table'])
.run(function() {
  console.clear();
})
.controller('smarttabledemo', function($scope, $http) {
    let _url = _servicequery + window.location.search;
    $http({method: 'GET', url: _url}).success(function(data) {
        console.log(_servicequery, data);
        $scope.data = data;
    });

    $scope.save=function() {
        $http.post(_servicequery, $scope.user);
    }
});