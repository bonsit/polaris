<?php

require_once('module._base.php');

class Module extends _BaseModule {
	var $collecteditems;
	var $level;
	
	function Module($moduleid, $module) {
        $this->_BaseModule($moduleid, $module);
	}

	function Process() {

        parent::Process();
    
        if ($_POST['_hdnAction']) {
            $recordArray = $_POST;
            
            switch(strtolower($recordArray['_hdnAction'])) {
            case 'save':
                if (is_array($recordArray['_hdnRecordID'])) {
                    require_once('includes/mainactions.inc.php');
                    // array situation where multiple records get posted
                    foreach($recordArray['_hdnRecordID'] as $_hdnRecordID) {
                        if ($recordArray['PARAMVALUE:'.$_hdnRecordID] != $recordArray['OLDVALUE:'.$_hdnRecordID]) {
                            $oneRecord['_hdnRecordID'] = $_hdnRecordID;
                            $oneRecord['_hdnDatabase'] = $recordArray['_hdnDatabase'];
                            $oneRecord['_hdnState']    = ($recordArray['_hdnState'] != '') ? $recordArray['_hdnState'] : 'edit';
                            $oneRecord['_hdnTable']    = strtolower($recordArray['_hdnTable']);
                            $oneRecord['PARAMVALUE']   = $recordArray['PARAMVALUE:'.$_hdnRecordID];
                            saveRecord($oneRecord);
//                            __saveRecord($this->userdatabase, $oneRecord, $autosupervalues);
                        }
                    } 
                }	
                break;
            case 'delete':
                DeleteRecords($_POST);
                break;
            }
        }		
    }

	function Show() {
        global $_sqlUserWebParams;
        parent::Show();
    
        if ($this->processed) {
          $this->smarty->assign('processed', $this->processed);
        } 

		/** Webparam FormView **/
		$parray = extractParams($parameters);
		$webparamrecords = $this->userdatabase->Execute($_sqlUserWebParams);
		$webparamrecords = $webparamrecords->GetAll();
		$this->smarty->assign('webparams', $webparamrecords);
		return $this->smarty->Fetch('module.webparam.tpl');
	}

}

?>
