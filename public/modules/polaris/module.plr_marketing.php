<?php
require_once('includes/mainactions.inc.php');
require_once('module._base.php');

class Module extends _BaseModule {
    var $stylesheet;

    function __construct($moduleid, $module, $clientname=false) {
        global $_GVARS;
        parent::__construct($moduleid, $module, $clientname);

        $this->processed = false;
        $this->stylesheet = 'default.css';
    }

    function Process() {
        $this->userdatabase->SetFetchMode(ADODB_FETCH_ASSOC);
        $this->parray = extractParams($this->params);
        $masterfield = $this->parray['masterfield'];

        if (isset($_POST['_hdnAction'])) {
            switch ($_POST['_hdnAction']) {
            case 'publish':
                return $this->PublishArticle($_POST['_hdnRecordID']);
            break;
            case 'save':
                return SaveRecord($_POST);
            break;
            }
        }
    }

    function PublishArticle($rec) {
        $_tablename = $this->form->record->TABLENAME;
        $_key = $this->form->database->makeRecordID(array('ID'));
        $_sql = "UPDATE ".$_tablename." SET published = IF(published=1, 0, 1) WHERE ". $_key ." = '$rec'";
        $this->userdatabase->Execute($_sql);
        return true;
    }

    function ShowListView($state, $permission, $detail=false, $masterrecord=false) {
        $_customfilter = $this->__GetCustomFilter();
        $_rs = $this->form->GetViewContent($detail=false, $masterrecord=false, $limit=false, $_customfilter);
        $_rs = $_rs->GetAll();

        $this->smarty->assign('stylesheet', $this->stylesheet);
        $this->smarty->assign('items', $_rs);
        echo $this->smarty->Fetch('campagnelist.tpl.php');
    }

    function ShowFormView($state, $permission, $detail=false, $masterrecord=false) {
        $_customfilter = $this->__GetCustomFilter();
        $_rs = $this->form->GetViewContent($detail=false, $masterrecord=false, $limit=false, $_customfilter);
        $_rs = $_rs->GetAll();
        require_once('php_includes/markdownextra/markdown.php');
        $_rs[0]['BODY_PREVIEW'] = Markdown($_rs[0]['BODY']);
        $this->smarty->assign('stylesheet', $this->stylesheet);
        $this->smarty->assign('items', $_rs);
        echo $this->smarty->Fetch("blogedit.tpl.php");
    }

    function Show($outputformat='xhtml', $rec=false) {
        global $_GVARS;

        if ($this->form->outputformat == 'json') {
            echo json_encode($this->data);
        } elseif ($this->form->outputformat == 'php') {
            echo serialize($this->data);
        } else {
            parent::Show();
            switch($_GET['action']) {
            case '':
                $this->ShowListView($this->state, $this->permission, $detail=false, $masterrecord=false);
            break;
            case 'edit':
            case 'insert':
                $this->ShowFormView($this->state, $this->permission, $detail=false, $masterrecord=false);
            break;
            }
        }
    }

}
