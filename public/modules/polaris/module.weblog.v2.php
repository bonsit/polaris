<?php

include_once('module._base.php');

class Module extends _BaseModule {

	var $maxtextlength = 150;
  
	function Module($moduleid) {
    $this->_BaseModule($moduleid);
    $this->processed = false;
	}	

	function Process() {
    $this->processed = true;
  }

	function Show() {
    $this->Process();

		if (isset($_GET['action'])) {
			switch($_GET['action']) {
			case 'edit':
				$this->form->ShowFormView($this->state, $this->permission, $detail=false, $masterrecord=false);
				$this->form->ShowButtons($this->state, $this->permission);
				$this->form->ShowNavigationLinks($this->state, $this->permissiontype, $detail=false);
			break;
			case 'searchresult':
				$this->form->ShowSearchForm($this->state, $this->permission, $detail=false, $masterrecord=false);
				$this->form->ShowButtons($this->state, $this->permission);
				$this->form->ShowNavigationLinks($this->state, $this->permissiontype, $detail=false);
			break;
			}
		} else {
			$rs = $this->form->GetViewContent($detail=false, $masterrecord=false);
			
			$this->ShowQuickAddScripts();
			
			$this->form->ShowFormInfo($this->permission, $rs->RecordCount());
			$this->form->ShowEditWebPageLink();

			$this->form->ShowListViewTableHeader();
			$fieldcount = $this->form->pages->ShowListViewHeader($this->userdatabase, $this->permission, $repeatedheader = false, $this->form->masterfields);

			echo $this->GetQuickAddBox();		

			$this->form->pages->ShowListViewData($this->userdatabase, $rs, $this->permission, $this->form->masterfields);
			$this->form->pages->ShowListViewFooter($fieldcount, $this->permission, $this->masterfields);
			$this->form->ShowListViewTableFooter();
		}
	}

	function ShowQuickAddScripts() {
			echo '<script type="text/javascript" src="javascript/validation/wforms.js"></script>';
			echo '<script type="text/javascript" src="javascript/validation/localization-'.strtolower($GLOBALS['lang']).'.js"></script>';
			echo <<<EOHTML
			<script type="text/javascript">
			
				function quickEntryToggle(on) { 
					if (!on) {
						Element.removeClassName(document.body, 'insert');
						Element.addClassName(document.body, 'allitems');
						$('quickinsert').style.display = 'none';
					} else {
						Element.addClassName(document.body, 'insert');
						Element.removeClassName(document.body, 'search');
						Element.removeClassName(document.body, 'allitems');
						try {
							$('quickinsert').style.display = 'table-row-group';
						} catch(err) {
							$('quickinsert').style.display = 'inline';
						}
						if(navigator.appVersion.indexOf('AppleWebKit')>0) $('datalistview').innerHTML += ''; //force redraw on safari
					}
				}
				
				Event.observe('action_masterinsert', 'click', function(){ quickEntryToggle(true); return false; });
				$('action_masterinsert').setAttribute('href', 'javascript:void(0)');
			</script>
			<style>
				#quickinsert tr td {border-bottom: 0px !important}
			</style>
			<input type="hidden" name="_hdnState" value="insert" />
EOHTML;
	}
	
	function GetQuickAddBox() {
		$pages =& $this->form->pages->pages;
		for($i = 0; $i < count($pages); $i++)
			$pages[$i]->LoadRecord();

		$html .= "<tbody id=\"quickinsert\" style=\"display:none\"><tr>";
		foreach ( array_keys($this->form->pages->pages) as $index ) {
			$page =& $this->form->pages->pages[$index];

			foreach ( array_keys($page->lines->lines) as $lineindex ) {
				$line =& $page->lines->lines[$lineindex];

				if ( ($line->record->LINETYPE == 'COLUMN' or $line->record->LINETYPE == 'COMPONENT')
							and $line->record->COLUMNVISIBLE == 'Y'
						 and !isset($_SESSION[$this->form->owner->record->CONSTRUCTID.'.'.GROUP_PREFIX.$line->record->COLUMNNAME]) 
						 and ( !$masterfields or ($masterfields and !in_array($line->record->COLUMNNAME, $masterfields)))
				) {
					$gui = $line->GetGuiElement($this->userdatabase, $inlistview = true, $recordid, $state='insert', $originalfieldvalue, $line->record->LINEREQUIRED == 'Y' or $line->record->REQUIRED == 'Y', $line->record->READONLY == 'Y', $this->permission=254);
					if ($gui != '')
						$html .= "<td>$gui</td>";
					$fldidx++;
				}
			}
		}
		$html .= "<td></td>";
		$html .= '</tr>';
		$html .= '<tr style=""><td colspan="0" style="font-size:0.9em"> <input type="submit" onclick="this.form._hdnAction.value=\'save\';" value="Opslaan" /> <input type="button" value="Annuleren" onclick="javascript:quickEntryToggle(false)" /></td></tr>';
		$html .= '</tbody>';
		return $html;
	}

} 

?>