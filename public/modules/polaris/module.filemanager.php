<?php
set_time_limit(300);
//error_reporting(E_ERROR);

require_once('module._base.php');

function cmp_array($a, $b) {
    if ($a == $b) {
        return 0;
    }
    return ($a < $b) ? -1 : 1;
}

class Module extends _BaseModule {
    var $filepath;
    var $scriptname;
    var $readpath;
    var $handle;

    function Module($moduleid, $module) {
        $this->_BaseModule($moduleid, $module);
        
        $this->processed = false;
    }
        
    function DisplayFileManager() {
        // This started out as this � http://www.evoluted.net/community/code/directorylisting.php
        // It was then edited by Ryan McCue from http://cubegames.net/ to include file uploading
        // Then I took it and stripped it back to it's bare minimum of less than 150 lines of 
        // PHP and HTML and just 20 lines of CSS. My name is Jim Whimpey and you can
        // find me at valhallaisland.com
        
        // This code is released under GPL 3.0 which is included in the bundle
    
        // Files to hide in the directory listing
        // add and subtract as you please
        $hide = array(	'resources',
                        'index.php',
                        '.htaccess',
                        '.htpasswd',
                        '.DS_Store',
                        '.svn');
                
        $files = array();
        while ($file = readdir($this->handle)) { 
            if ($file == "." || $file == ".." || in_array($file, $hide))  continue;
            $key = @filemtime($file);
            if (!is_dir($file)) {
                $filesize = round(filesize($file)/1024);
            } else {    
                $filesize = false;
            }
            $files[] = array(
                'key' => $key,
                'filename' => $file,  
                'filesize' => $filesize,
                'filetime' => filemtime($file),
                'ext' => strtolower(substr($file, strrpos($file, '.')+1))
            );
        }
        closedir($this->handle); 
    
        // Sort our files
//        @ksort($files, SORT_NUMERIC);
//        $files = @array_reverse($files);

        $this->smarty->assign('files', $files);
        return $this->smarty->Fetch('filemanager.tpl.php');
    }
 
    function Process() {
        // When downloading force it to actually download
        // rather than just open it in the browser
        if ($_GET['download']) {
            $file = str_replace('/', '', $_GET['download']);
            $file = str_replace('..', '', $file);
    
            if (file_exists($file)) {
                header("Content-type: application/x-download");
                header("Content-Length: ".filesize($file)); 
                header('Content-Disposition: attachment; filename="'.$file.'"');
                readfile($file);
                die();
            }
        }

        $this->filepath = $_SERVER['SCRIPT_FILENAME'];
        $this->scriptname = basename($this->filepath);
        $this->readpath = str_replace($this->scriptname, "", $this->filepath);
        $this->handle = opendir($this->readpath);

        /**        
        * Delete a file
        */
        if (isset($_GET['rmfile'])) {
            $rmfile = $_GET['rmfile'];
            if (!unlink($readpath . $_GET['rmfile']))
                echo "Het bestand kon niet worden verwijderd. ($readpath${_GET['rmfile']})";
        }
        
        /**        
        * Upload a file
        */
        if ($_FILES['file']) {
            $success = move_uploaded_file($_FILES['file']['tmp_name'], $_FILES['file']['name']);
        }

    }
    
    function Show() {
        parent::Show();
        return $this->DisplayFileManager();
    }
  	
}

?>
	
<?php $baseurl = $_SERVER['PHP_SELF']; ?>
