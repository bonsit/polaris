<input type="hidden" name="_hdnRecordID" value="{$recset.recordid}" />
<input type="hidden" name="_hdnGroup" value="{$currentgroup|default:$smarty.get.group}" />
<input type="hidden" name="_hdnAction" value="save" />
<input type="hidden" name="_hdnState" value="edit" />
<input type="hidden" name="_hdnProcessedByModule" value="true" />

{literal}
<script language="JavaScript" type="text/javascript">
<!--
function UnselectLanguages(){
	var litems = document.getElementsByTagName('li');
	for (var i = 0; i < litems.length; i++){
		if (litems[i].id.substring(0,5) == 'i18n_') {
	      	var obj = litems[i];
            removeElementClass(obj, "current");
		}
	}
}

function i18n_SelectLanguage(i18nElement, lang) {
	UnselectLanguages();
	var newcurrent = $('i18n_'+lang);
	if (newcurrent) {
	    addElementClass(newcurrent, "current");
		var TextElem = $('_hdnvalue_'+i18nElement);
		if (TextElem) {
			TextElem.value = $(i18nElement + lang).value;
			TextElem.setAttribute('language', lang);
//			TextElem.focus();
		}
	}	else alert('language not found');
}

function i18n_ChangeCaption(i18nElement) {
	var TextElem = $('_hdnvalue_'+i18nElement);
	var currentlang = TextElem.getAttribute('language');
	if ($(i18nElement + currentlang))
		$(i18nElement + currentlang).value = TextElem.value;
}
//-->
</script>
{/literal}

<div class='panelhelp'>Hiermee kunt u de kenmerken van een menuitem instellen.</div>
<table id="formview" summary="data">
{*<tr><td width="30%">{lang menuid}:</td>
<td><input name="menuid" class="text" size="5" maxlength="5" type="text" value="{$recset.menuid}" disabled="disabled" /></td></tr>*}
<tr><td>{lang menuname}:</td>
<td>

<div class="i18n">
{section name=i loop=$recset.menunames}
<input type="hidden" id="caption_rsc_lang{$recset.menunames[i].language}" name="caption_rsc_lang[]" value="{$recset.menunames[i].language}" />
<input type="hidden" id="caption_rsc_{$recset.menunames[i].language}" name="caption_rsc_value[]" value="{$recset.menunames[i].menuname}" />
{/section}

<ul class="i18n_languages">
{section name=i loop=$recset.menunames}
<li {if $smarty.section.i.first}class="current" {/if}id="i18n_{$recset.menunames[i].language}"><a href="#" onclick="i18n_SelectLanguage('caption_rsc_','{$recset.menunames[i].language}')">{$recset.menunames[i].language}</a></li>
{/section}
</ul>
<input id="_hdnvalue_caption_rsc_" class="text i18n" size="50" language="{$recset.menunames[0].language}" onblur="i18n_ChangeCaption('caption_rsc_')" maxlength="255" type="text" value="{$recset.menunames[0].menuname}" />
<input id="caption_rsc_" name="caption_rsc_" type="hidden" value="{$recset.caption_rsc_}" />
</div>
</td></tr>
<tr><td colspan="2" class="textline"><input type="radio" name="_hdnLinkPage" id="optionlink" value="optionlink"{if $recset.url} checked="checked"{/if} /> Dit menuitem activeert een externe link:</td></tr>

<tr><td>{lang url}:</td>
<td><input name="url" id="url" class="text" size="50" maxlength="200" type="text" value="{$recset.url}"
onchange="$('optionlink').checked=true;$('pagename').value=''" /></td></tr>

<tr><td colspan="2" class="textline"><input type="radio" name="_hdnLinkPage" id="optionpage" value="optionpage"{if $recset.pagename} checked="checked"{/if} /> Dit menuitem toont een webpagina:</td></tr>
<tr><td>{lang pagename}:</td><td>
<select name="pagename" id="pagename" onchange="$('optionpage').checked=true;$('url').value=''">
<option value=""></option>
{section name=i loop=$webpages}
<option value="{$webpages[i].pagename}"{if $recset.pagename eq $webpages[i].pagename} selected="selected"{/if}>{$webpages[i].pagename}</option>
{/section}
</section>
</td></tr>
<tr><td colspan="2" class="textline">&nbsp;</td></tr>

<tr><td>{lang target}:</td><td>
<select name="target">
{html_options values=$targets output=$targets selected=$recset.target}
</select>
</td></tr>

<tr><td>{lang visible}:</td>
<td><input name="visible" id="VISY" class="radio" size="1" maxlength="1" type="radio" value="Y"{if $recset.visible eq 'Y'}checked="checked"{/if} /><label for="VISY">Ja</label> <input name="visible" id="VISN" class="radio" size="1" maxlength="1" type="radio" value="N"{if $recset.visible eq 'N'}checked="checked"{/if} /><label for="VISN">Nee</label></td></tr>
<tr><td>{lang parentmenuid}:</td>
<td>

<select name="parentmenuid">
<optgroup label="Menu's">
{section name=i loop=$menugroups}
<option value="{$menugroups[i].menuid}"{if $recset.parentmenuid eq $menugroups[i].menuid} selected="selected"{/if}>{$menugroups[i].menuname}</option>
{/section}
</optgroup>
<optgroup label="Menuitems">
{section name=i loop=$menu}
<option value="{$menu[i].menuid}"{if $recset.parentmenuid eq $menu[i].menuid} selected="selected"{/if}>{$menu[i].menuname}</option>
{/section}
</optgroup>
</select>
</td></tr>
<tr><td>{lang seo_keywords}:</td>
<td><input name="seokeywords" class="text" size="45" maxlength="255" type="text" value="{$recset.seokeywords}" /> {lang seo_keywords_help}</td></tr>
{*<tr><td>{lang orderindex}:</td>
<td><input name="orderindex" class="text" size="5" maxlength="5" type="text" value="{$recset.orderindex}" /></td></tr>*}
</table>
<input type="submit" value="{lang but_save}" />
<input type="button" value="{lang but_cancel}" class="protect" onClick="location.href='{$callerquery}?modaction=&group={$smarty.get.group}'" />