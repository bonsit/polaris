<input type="hidden" name="_hdnAction" value="delete" />
<input type="hidden" name="_hdnProcessedByModule" value="true" />
<input type="hidden" name="_hdnGroup" value="{$currentgroup|default:$smarty.get.group}" />

<div id="tabs-content">
<ul class="tabs left white">

{section name=i loop=$menugroups}
<li {if $currentgroup eq $menugroups[i].menuid}id="sel"{/if}><a href="{urlquery_add url=$callerquery group=$menugroups[i].menuid}">{$menugroups[i].menuname}</a></li>
{/section}
<li class="clean"><a href="{urlquery_add url=$callerquery modaction="add_menu"}">Nieuw menu toevoegen</a></li>
</ul>
</div>
<hr class="cleaner" />

{if $menugroups[0].menuid ne ''}
{section name=i loop=$menu}
{if $smarty.section.i.first}
<table class="data striped">
  <thead><tr><th class="action"><input type="checkbox" onClick='javascript:checkAllCheckBoxes(this)' class='checkbox' name='_hdnAllBox' /></th><th>Menuitem naam</th><th></th><th>Zichtbaar?</th><th>Is gekoppeld aan</th><th>SEO keywords</th><th class="action">Volgorde</th></tr></thead>
{/if}

<tr{if $menu[i].visible eq 'N'} class="invisible"{/if}>
<td class="action"><input type="checkbox" onClick="CheckBox(this)" name="rec:{$menu[i].recordid}" class="checkbox" value="Y" /></td>
<td>{''|indent:$menu[i].level:"&nbsp;&nbsp;"}{if $menu[i].level > 0}<img src="{$serverroot}/images/ftv2lastnode.gif" style="vertical-align:middle" />&nbsp;{/if}<a href="{urlquery_add url=$callerquery group=$smarty.get.group modaction=edit rec=$menu[i].recordid}">{$menu[i].menuname}</a></td>
<td class="action"><a href="{urlquery_add url=$callerquery group=$currentgroup modaction=edit rec=$menu[i].recordid}">[{lang edit}]</a></td>
<td>
<input type="radio" name="_hdnVisibleState_{$menu[i].recordid}" value="Y" {if $menu[i].visible eq 'Y'}checked="checked"{/if} onclick="visibleState('frmextra','VISIBLE', '{$menu[i].recordid}', 'Y')" /> Ja &nbsp;
<input type="radio" name="_hdnVisibleState_{$menu[i].recordid}" value="N" {if $menu[i].visible ne 'Y'}checked="checked"{/if} onclick="visibleState('frmextra','VISIBLE', '{$menu[i].recordid}', 'N')" /> Nee
<td>{if $menu[i].pagename}Pagina: {$menu[i].pagename}
{elseif $menu[i].url}Link: {$menu[i].url}
{else}&lt;niet gekoppeld&gt;
{/if}
</td>
<td>{$menu[i].seokeywords|truncate:30}</td>
<td class="action">
{*{if !$smarty.section.i.first}<a href="'.$moveup.'" title="{lang move_up}"><img src="{$serverroot}/images/up_blue.gif" alt="up" /></a>{/if}
{if !$smarty.section.i.last}<a href="'.$movedown.'" title="{lang move_down}"><img src="{$serverroot}/images/down_blue.gif" alt="down" /></a>{/if}
*}
</td>
</tr>

{if $smarty.section.i.last}
<tfoot><tr><td class="xaction" colspan="2"><input type="submit" name="_hdnDeleteButton" value="{lang but_delete}" class="sbttn" onclick="return CheckDel(this);" title="{lang but_delete_hint}" tabindex="2" /></td><td></td><td></td><td></td><td></td><td></td></tr></tfoot>
</table>
{/if}
{sectionelse}
<br /><p>Dit menu is nog leeg. U kunt nu menuitems gaan toevoegen.</p>
{/section}
</form>{* close this tag because there is another form tag on this page *}

{*<a href="{urlquery_add url=$callerquery group=$smarty.get.group action=deletegroup}">{$menugroups[i].menuid}verwijder menu</a>*}

<form method="post" action=".">
<input type="hidden" name="_hdnProcessedByModule" value="true" />
<input type="hidden" name="_hdnAction" value="add_menuitem" >
<input type="hidden" name="language" value="{$defaultlanguagecode}" />
<div class="moduleactions"><h1>Nieuw menuitem toevoegen</h1>
Menuitem naam: <input type="text" name="menuname" value="" size="20" /> ({$defaultlanguagecode})<br />
Gekoppeld aan pagina: 
<select name="page">
{section name=i loop=$webpages}
<option value="{$webpages[i].pagename}">{$webpages[i].pagename}</option>
{/section}
</select> 

<table>
<tr><td><input type="radio" name="menutype" id="addmenu" checked="checked" value="menu" /> <label for="addmenu">na het menuitem</label></td><td> 
<select name="addnormalmenuitem" onchange="getElementById('addmenu').checked=true"{if !$menu} disabled="disabled"{/if}>
{section name=i loop=$menu}
<option value="{$menu[i].recordid}"{if $smarty.section.i.last} selected="selected"{/if}>{$menu[i].menuname}</option>
{/section}
</select> 
</td></tr>
<tr><td><input type="radio" name="menutype" id="addsubmenu" value="submenu"{if !$menu} disabled="disabled"{/if}/> <label for="addsubmenu">als submenu van het menuitem</label></td>
<td>
<select name="addsubmenuitem" onchange="getElementById('addsubmenu').checked=true"{if !$menu} disabled="disabled"{/if}>
{section name=i loop=$menu}
<option value="{$menu[i].recordid}"{if $smarty.section.i.last} selected="selected"{/if}>{$menu[i].menuname}</option>
{/section}
</select> 
</td></tr>
</table>
<br /><input type="submit" name="submit" value="{lang but_add}" />
</div>
</form>
{/if}
<form>{* open a form tag because of the close form tag in the polaris class *}
