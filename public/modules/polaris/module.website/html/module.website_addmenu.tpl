<input type="hidden" name="_hdnRecordID" value="{$recset.recordid}" />
<input type="hidden" name="_hdnGroup" value="{$smarty.get.group}" />
<input type="hidden" name="_hdnAction" value="add_menu" />

<h2>Nieuw menu</h2>
<p>Hier kunt u een nieuw menu toevoegen aan uw website. Dit menu zal nog (handmatig) op de juiste plaats in uw website geplaatst moeten worden.</p>

<table id="formview" summary="data">
<tr><td>{lang menuname}:</td>
<td><input id="menuname" name="menuname" class="text" size="50" maxlength="255" type="text" value="{$recset.menuname}" /></td></tr>
</table>
<input type="hidden" name="language" value="{$defaultlanguagecode}" />
<input type="submit" value="{lang but_save}" /> 
<input type="button" value="{lang but_cancel}" class="protect" onClick="location.href='{$callerquery}?modaction=&group={$smarty.get.group}'" />