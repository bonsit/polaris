{include file="shared.tpl.php"}

{section loop=$items name=i}
<input type="hidden" class="campagne_published_value" value="{$items[i].PUBLISHED}" />

<div class="col-lg-4">
    <div class="widget navy-bg no-padding">
        <div class="p-m">
            <h1 class="m-xs">0 </h1><small>abonnees</small>

            <h3 class="font-bold no-margins">
                Annual income
            </h3>
            <small>Income form project Alpha.</small>
        </div>
        <div class="flot-chart">
            <div class="flot-chart-content" id="flot-chart1" style="padding: 0px; position: relative;">
                <canvas class="flot-base" width="1140" height="200" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 570px; height: 100px;"></canvas>
                <canvas class="flot-overlay" width="1140" height="200" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 570px; height: 100px;"></canvas>
            </div>
        </div>
    </div>
</div>


        <a href="{$serverroot}{$callerquery}edit/{$items[i].PLR__RECORDID}/"><h1>{$items[i].campagnenaam}</h1></a>
        <p class="pblog_published">
            Aangemaakt op: {$items[i].CREATED_AT|date_format:"%e %B %Y"}<br/>
            Gepubliceerd op: <span class="pblog_published_at">{if $items[i].PUBLISHED == 0}Nog niet{else}{$items[i].PUBLISHED_AT|date_format:"%e %B %Y"}{/if}</span><br/>
            Status:
            <select class="pblog_status" data-rec="{$items[i].PLR__RECORDID}">
                <option value="publish" {if $items[i].PUBLISHED == 1}selected="selected"{/if}>Gepubliceerd</option>
                <option value="concept" {if $items[i].PUBLISHED == 0}selected="selected"{/if}>Concept</option>
            </select>
        <p>
        <div class="buttons small">
        <a href="{$serverroot}{$callerquery}edit/{$items[i].PLR__RECORDID}/" class="active">Wijzig</a>
        <a href="#" data-rec="{$items[i].PLR__RECORDID}" class="btnDelete negative">Verwijder</a>
        </div>
{/section}

