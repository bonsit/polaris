<?php

include_once('module._base.php');

class Module extends _BaseModule {
  var $responce;
  var $from;
  var $subject;
  var $sendto;
  var $bodytext;
  var $keeptext;
  var $processed;

	function Module($moduleid) {
    $this->_BaseModule($moduleid);
    $this->processed = false;
	}

  function LoadBulkMailRecord() {
    global $plrdb, $_sqlBulkMail;

    $rs = $plrdb->Execute($_sqlBulkMail, array($this->form->owner->owner->record->RECORDID, $this->moduleid));    
    if ($rec = $rs->FetchNextObject(true)) {
      return $rec;
    } else {
      return false;
    }
  }

  function SaveBulkMailRecord() {
    global $plrdb, $_sqlUpdateBulkMail, $_sqlUpdateBulkMailText;

    $rs = $plrdb->Execute($_sqlUpdateBulkMail, array($this->from, $this->subject, $_GET['app'], $this->moduleid));    

    if ($this->keeptext == 'yes')
      $rs = $plrdb->Execute($_sqlUpdateBulkMailText, array($this->bodytext, $_GET['app'], $this->moduleid));    
  }

  function GetAddresses($clientid, $databaseid, $source, $addresscolumnname) {
    global $_DOCROOT;
    
    include_once($_DOCROOT.'/includes/basefunctions.inc.php');
    include_once($_DOCROOT.'/includes/miscfunc.inc.php');
    $userdb = ConnectUserDB($clientid, $databaseid);
    $sql = MakeSQLSelect($source);
    $addresses = $userdb->GetAll($sql);
    return $addresses;
  }

  function Process() {
    global $_DOCROOT;

    if ($_POST['submit']) {
      require_once($_DOCROOT.'/includes/func_email.inc.php');
      $this->from = stripslashes($_POST['fromaddress']);
      $this->subject = $_POST['subject'];
      $this->sendto = $_POST['emailaddresses'];
      $this->bodytext = $_POST['bodytext'];
      $this->keeptext = $_POST['keeptext'];
      $this->SaveBulkMailRecord();
      sendmail($responce, $this->from, $this->subject, $this->sendto, $this->bodytext);
      $this->responce = $responce;
      $this->processed = true;
    }
  }

	function Show() {

        $this->Process();
        if ($this->processed) {
          $this->smarty->assign('processed', $this->processed);
          $this->smarty->assign('responce', $this->responce);
        } 
    
        if ($rec = $this->LoadBulkMailRecord()) {
          $this->smarty->assign('addresses', $this->GetAddresses($rec->CLIENTID, $rec->DATABASEID, $rec->SQLSOURCE, $rec->ADDRESSCOLUMNNAME));
          $this->smarty->assign('addresscolumnname', $rec->ADDRESSCOLUMNNAME);
          $this->smarty->assign('fromaddress', stripslashes($rec->FROMADDRESS));
          $this->smarty->assign('subject', $rec->SUBJECT);
          $this->smarty->assign('bodytext', $rec->BODYTEXT);
          $this->smarty->assign('mailtype', $rec->MAILTYPE);
          $this->smarty->display('module.bulkmail.tpl.php');
        } else {
                echo 'Could not load Bulkmail record.';
		}
	}
  	
}

?>