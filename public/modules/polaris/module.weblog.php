<?php

include_once('module._base.php');

class Module extends _BaseModule {

	var $maxtextlength = 150;
	var $agrs;

	function Module($moduleid, $module) {
        $this->_BaseModule($moduleid, $module);
        $this->processed = false;
	}

	function Process() {
		if ($_GET['customaction']=='true') {
			$_SESSION['WEBLOG.TOONALLEITEMS'] = isset($_GET['alleitems']) and $_GET['alleitems'] == 'Y';
			$_SESSION['WEBLOG.TOONALLEGEBIEDEN'] = isset($_GET['allegebieden']) and $_GET['allegebieden'] == 'Y';
			$_SESSION['WEBLOG.TOONALLECOMMENTAAR'] = isset($_GET['allecommentaar']) and $_GET['allecommentaar'] == 'Y';
		}

    $this->processed = true;
  }

 	function __GetCustomFilter() {
 		$this->Process();
 		$customfilter = false;
		if ( !($_SESSION['WEBLOG.TOONALLEITEMS'] == true) ) 			{
			$customfilter = '(afgehandeld = "N")';
		}
		if ( !($_SESSION['WEBLOG.TOONALLEGEBIEDEN'] == true) ) 	{
			$focus_aandachtsgebieden = isset($_SESSION['FOCUS.AANDACHTSGEBIEDEN'])?$_SESSION['FOCUS.AANDACHTSGEBIEDEN']:0;

			if ($customfilter != '') $customfilter .= ' and ';
			$customfilter .= '('.$focus_aandachtsgebieden.' & aandachtsgebieden)';
		}
 		return $customfilter;
	}

	function Show() {
	   global $_GVARS;
		require_once('includes/str_highlight.inc.php');

		$monthsofyearshort_lang = array ('Jan','Feb','Mrt','Apr','Mei','Jun','Jul','Aug','Sep','Okt','Nov','Dec');
		$dayofweek = array("zo", "ma", "di", "wo", "do", "vr", "za");
		$highlighter = '<span style="background-color:yellow;">\1</span>';

		if (isset($_GET['action'])) {
			switch($_GET['action']) {
			case 'edit':
				$this->form->ShowFormView($this->state, $this->permission, $detail=false, $masterrecord=false);
				$this->form->ShowButtons($this->state, $this->permission);
				$this->form->ShowNavigationLinks($this->state, $this->permissiontype, $detail=false);
			break;
			case 'searchresult':
				$this->form->ShowSearchForm($this->state, $this->permission, $detail=false, $masterrecord=false);
				$this->form->ShowButtons($this->state, $this->permission);
				$this->form->ShowNavigationLinks($this->state, $this->permissiontype, $detail=false);
			break;
			}
		} else {

			$this->agrs = $this->userdatabase->GetAll('SELECT aandachtsgebiedid, aandachtsgebied FROM aandachtsgebied ORDER BY aandachtsgebied');

			$databasehash = encodeString($this->form->record->CLIENTID.'_'.$this->form->record->DATABASEID);
			$formactionscript = $this->form->MakeURLQuery(array('q','offset'), false);
			$customfilter = $this->__GetCustomFilter();
			$rs = $this->form->GetViewContent($detail=false, $masterrecord=false, $limit=false, $customfilter);
			$checked_alleitems		  = $_SESSION['WEBLOG.TOONALLEITEMS']?'checked="checked"':'';
			$checked_allegebieden   = $_SESSION['WEBLOG.TOONALLEGEBIEDEN']?'checked="checked"':'';
			$checked_allecommentaar = $_SESSION['WEBLOG.TOONALLECOMMENTAAR']?'checked="checked"':'';

			$rscomments = $this->userdatabase->GetAll('SELECT username, beschrijving, last, post, commentaarid, logboekid FROM logboekcommentaar ORDER BY logboekid DESC, last ASC');
			// change comments to more practical layout
			foreach($rscomments as $cm) {
				$comments[$cm['logboekid']][] = $cm;
			}
			echo '<input type="hidden" name="_hdnAction" value="save" />';
			echo '<input type="hidden" name="_hdnState" value="insert" />';
			echo '<div id="quickformview" style="display:none"><div class="logview_newentry">';
			$this->form->ShowFormView($this->state='insert', $this->permission, $detail=false, $masterrecord=false);
			$this->form->ShowButtons($this->state='insert', $this->permission);
			echo '<br /></div></div></form>';

			$searchfield = (!is_array($_GET['q']))?'<input type="hidden" name="q" value="'.$_GET['q'].'" />':'';
			echo <<<EOHTML
			<div id="weblog_extra_action" style="display:none">
			<form id="formflags" action="$formactionscript" method="get">
			<input type="hidden" name="maximize" value="{$_GET['maximize']}" />
			$searchfield
			<input type="hidden" name="customaction" value="true" />
			<label><input type="checkbox" name="allegebieden" value="Y" $checked_allegebieden onclick="this.form.submit()" /> Alle aandachtsgebieden</label>
			<label><input type="checkbox" name="alleitems" value="Y" $checked_alleitems onclick="this.form.submit()" /> Toon afgehandelde items</label>
			<label><input type="checkbox" name="allecommentaar" value="Y" $checked_allecommentaar onclick="this.form.submit()" /> Toon commentaar</label>
			</form></div>
EOHTML;

        $script = "
			var adjusted = false;

            /***
            * initialize the form validation
            */
            Polaris.FormValidator.onLoadHandler();

			function selectItemLink() {
  			     showElement('quickformview');
                 if (!adjusted) adjustLabelWidth();
                 adjusted = true;
                 Polaris.Base.masterItemSelect($('quickformview').style.display == 'block');
			}

			if (getLocationVariable('logaction') == 'insert') {
				$('quickformview').style.display = 'block';
				if (getLocationVariable('klant') != '' && $('_fldKLANTID'))
				    $('_fldKLANTID').value = getLocationVariable('klant');
				if (getLocationVariable('aandachtsgebied') != '' && $('_fldAANDACHTSGEBIEDEN')) {
					var aandachtsgebied = getLocationVariable('aandachtsgebied');
					if (parseInt(aandachtsgebied)) {
						 $('_fldAANDACHTSGEBIEDEN').value = aandachtsgebied;
					} else {
						var elems = getElementsByTagAndClassName('input', 'binarycheckbox', $('div__fldAANDACHTSGEBIEDEN'));
						for (var i=0;i<elems.length;i++) {
						    if (elems[i].getAttribute('screenvalue').toUpperCase() == aandachtsgebied.toUpperCase())
						        elems[i].checked = true;
						}
					}
					syncCheckboxValue('_fldAANDACHTSGEBIEDEN');
				 	updateCheckboxValue('_fldAANDACHTSGEBIEDEN');
				}
				if (getLocationVariable('beschrijving') != '' && $('_fldBESCHRIJVING'))
				 $('_fldBESCHRIJVING').value = '<code>'+getLocationVariable('beschrijving')+'</code><br /><br />';
				if (getLocationVariable('naam') != '' && $('_fldNAAM'))
				 $('_fldNAAM').value = getLocationVariable('naam');

			}

			var stretchers, togglers, cmnt_stretchers, cmnt_togglers;

            addLoadEvent(function() {
				connect('action_masterinsert', 'onclick', function(){ selectItemLink() ;return false; });
				$('action_masterinsert').setAttribute('href', 'javascript:void(0)');

				togglers = getElementsByTagAndClassName('a', 'display'); //a's where I click on
				stretchers = getElementsByTagAndClassName('div', 'stretcher'); //div's that stretches
    			for (var i=0;i<stretchers.length;i++) hideElement(stretchers[i]);
				Polaris.Base.mConnect(togglers, 'onclick', function(e) {
				    if (getStyle('addcomment_'+this.id, 'display') == 'none')
				        showElement('addcomment_'+this.id);
				    else
				        hideElement('addcomment_'+this.id);
				    return false;
			    } );

				cmnt_togglers = getElementsByTagAndClassName('a', 'display_cmnt'); //a's where I click on
				Polaris.Base.mConnect(cmnt_togglers, 'onclick', function(e) {
				    if (getStyle('comments_'+this.id, 'display') == 'none')
				        showElement('comments_'+this.id);
				    else
				        hideElement('comments_'+this.id);
				    return false;
				} );
            ";

    	    $cmnt_stretchersclass = 'show';
			if ( !$_SESSION['WEBLOG.TOONALLECOMMENTAAR'] ) {
			    $cmnt_stretchersclass = 'hide';
				$script .= " cmnt_stretchers = getElementsByTagAndClassName('div', 'stretcher_cmnt'); //div's that stretches \n\r";
				$script .= " for (var i=0;i<cmnt_stretchers.length;i++) hideElement(cmnt_stretchers[i]); \n\r";
			}
			$script .= "
			    $('extra_action_container').innerHTML = $('weblog_extra_action').innerHTML;
			});";

            $GLOBALS['extrapagescripts'][] = $script;

			$primkeys = $this->form->database->metaPrimaryKeys(strtolower($this->form->GetDataDestination()), $hashed = false);
			$i = 0;
			while($rec = $rs->FetchNextObject(true)) {
				$recordid = MakeRecordID($rec, $primkeys);
				$dt = strtotime($rec->POST);
				$logdate = date('Y-m-d H:i',$rec->GELDIGTOT_UTS);
				$logyear = date('Y',$dt);
				$logday = date('j',$dt);
				$logdayofweek = date('w', $dt);
				$logmonthnr = date('m',$dt);
				$logmonth = date('M',$dt);
				$logtime = date('H:i',$dt);
				$titel = $rec->NAAM;
				$beschrijving = strip_tags($rec->BESCHRIJVING, '<br><br/><span><code><p><strong><em><i>');
				if (isset($_GET['q']) and !is_array($_GET['q'])) {
					$titel = str_highlight($titel, $_GET['q'], STR_HIGHLIGHT_STRIPLINKS, $highlighter);
					$beschrijving = str_highlight($beschrijving, $_GET['q'], STR_HIGHLIGHT_STRIPLINKS, $highlighter);
				}

				$commentaarcount = isset($comments[$rec->LOGBOEKID])?count($comments[$rec->LOGBOEKID]):0;
				$aandachtsgebieden = $this->ProcessAGShort($rec->AANDACHTSGEBIEDEN);
				$aglang = $this->ProcessAG($rec->AANDACHTSGEBIEDEN);
				$afgehandeldclass = ($rec->AFGEHANDELD == 'Y')?' afgehandeld':'';
    			$editactionscript = $this->form->MakeURL(null, false);
    			$editactionquery = $this->form->MakeQuery(array('q','offset'), false);
				$editlink = "<input type=\"button\" onclick=\"javascript:window.location.href='{$editactionscript}edit/$recordid/{$editactionquery}'\" class=\"popbox\" value=\"Wijzig\" />";

				if ($rec->AFGEHANDELD == 'N')
					$afgehandeldhtml = "<input type=\"button\" xstyle=\"float:right\" class=\"cancelbubble\" name=\"_hdnHotUpdateButton_$recordid\" id=\"_hdnHotUpdateButton_$recordid\" onclick=\"hotRecordUpdate('frmhotupdate', '$recordid', 'afgehandeld', 'Y')\" value=\"Afgehandeld\" />";
				else
					$afgehandeldhtml = '';
				echo "<div class=\"logview$afgehandeldclass\"><a name=\"{$rec->LOGBOEKID}\"></a>";
				echo <<<EOHTML
				<div class="logdate"><p>
				<span class="day">{$dayofweek[$logdayofweek]} $logday</span>
				<span class="month">{$monthsofyearshort_lang[$logmonthnr-1]}</span>
				<span class="year">$logyear</span>
				<span class="time">$logtime</span></p></div>

				<div class="loginfobox"><h1>Log informatie</h1><div class="boxtext">
				Door: <span>$rec->USERNAME</span><br />Geldig tot: <span>$logdate</span><br />Klant: <span>{$rec->KLANTID_SSS9}</span><br />Aandachtsgebieden: <acronym title='$aglang'><span>$aandachtsgebieden</span></acronym><br />Incident: <span>$rec->INCIDENT</span><br/>{$editlink} {$afgehandeldhtml}
				</div></div>

             <div class="logcontent">
				<div class="logname">$titel</div>
				<div class="logtext">$beschrijving</div>
				<hr class="cleaner" />
EOHTML;

				echo "<div class=\"logcomments\"><a href=\"javascript:void(0)\" id=\"{$rec->LOGBOEKID}\" style=\"cursor:pointer\" title=\"{$rec->LOGBOEKID}\" class=\"display_cmnt\">";
				if ($commentaarcount != 0) {
					echo "$commentaarcount comment";
					echo ($commentaarcount == 1)?"aar":"aren";
				}
				echo "</a>";
				if ($commentaarcount != 0)
					echo " - laatste commentaar door <em>".$comments[$rec->LOGBOEKID][count($comments[$rec->LOGBOEKID])-1]['username']."</em>";
				echo "<div id=\"comments_{$rec->LOGBOEKID}\" class=\"stretcher_cmnt $cmnt_stretchersclass\">";
				if ($comments[$rec->LOGBOEKID]) {
					$rowidx = 1;
					foreach ($comments[$rec->LOGBOEKID] as $reccm) {
						$cmdt = strtotime($reccm['last']);
						$cmdate = date('Y-m-d H:i',$cmdt);
						if ( bcmod($rowidx,2)==0 ) $oddeven = ' even'; else $oddeven = ' odd';
						echo '<div class="comment'.$oddeven.'">';
						echo '<div class="info"><em>'.$reccm['username'].'</em> op '.$cmdate.'</div>';
						echo '<div class="text">'.$reccm['beschrijving'].'</div>';
						echo '</div>';
						$rowidx++;
					}
				}
       			$action = add_query_arg($formactionscript, '_fragment', $rec->LOGBOEKID);
				echo <<<EOHTML
				</div>
				<a href="javascript:void(0)" id="{$rec->LOGBOEKID}" title="{$rec->LOGBOEKID}" class="display">Voeg commentaar toe</a>
				<div class="addcomment stretcher" id="addcomment_{$rec->LOGBOEKID}">

				<form id="addcommentform_{$rec->LOGBOEKID}" class="validate" action="$action" method="post">
				<textarea name="BESCHRIJVING" class="required" id="_fldBESCHRIJVING_{$rec->LOGBOEKID}" cols="60" rows="8"></textarea><br />
				<input type="submit" name="_hdnSubmit" value="Opslaan" />
				<input type="button" onclick="hideElement('addcomment_{$rec->LOGBOEKID}')" value="Annuleren" />
				<input type="hidden" name="USERNAME" value="{$_SESSION['name']}" />
				<input type="hidden" name="LOGBOEKID" value="{$rec->LOGBOEKID}" />
				<input type="hidden" name="_hdnDatabase" value="$databasehash" />
				<input type="hidden" name="_hdnTable" value="logboekcommentaar" />
				<input type="hidden" name="_hdnAction" value="save" />
				<input type="hidden" name="_hdnState" value="insert" />
				</form><br/><br/><br/></div>

				</div>
                </div>
				</div>
EOHTML;
				$i++;
			}
			echo '<form>';
		}
	}

	function ProcessAG($agtotaal) {
		$result = '';
		foreach($this->agrs as $agrec) {
			if ((intval($agtotaal) & intval($agrec['aandachtsgebiedid'])) == intval($agrec['aandachtsgebiedid']))
				$result .= $agrec['aandachtsgebied'].', ';
		}
		if ($result == '') $result = '-';
		else $result = substr($result, 0, -2);
		return $result;
	}

	function ProcessAGShort($agtotaal) {
		$result = '';
		foreach($this->agrs as $agrec) {
			if ((intval($agtotaal) & intval($agrec['aandachtsgebiedid'])) == intval($agrec['aandachtsgebiedid'])) {
				$result .= $agrec['aandachtsgebied'].', ';
			}
		}
		if ($result == '') $result = '-';
		else {
			if (substr_count($result,', ') > 1) {
				$pos = strpos($result, ", ");
				$result = substr($result, 0, $pos+2);
				$result .= '...';
			} else {
				$result = substr($result, 0, -2);
			}
		}
		return $result;
	}

}

?>