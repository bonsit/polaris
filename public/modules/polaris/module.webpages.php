<?php

include_once('module._base.php');

class Module extends _BaseModule {
    var $param;

	function Module($moduleid, $module) {
        $this->_BaseModule($moduleid, $module);

        $this->processed = false;
	}

    function GetWebPages($db, $folders) {
        global $_sqlUserWebPagesBASE, $scramble;
        
        /**
        * Get the webpages from the database
        */
        $_sqlUserWebPages = $_sqlUserWebPagesBASE.' WHERE editable = "Y"';
        if (isset($_GET['q']))
            $_sqlUserWebPages .= ' AND pagename LIKE "%'.$_GET['q'].'%"';
        $_sqlUserWebPages .= ' ORDER BY pagename, language ';
        $webpagesrecords = $db->GetAll($_sqlUserWebPages);
        $i = 0;
        if ($webpagesrecords)
            foreach($webpagesrecords as $webpagesrecord) {
                if (($webpagesrecords[$i + 1]['pagename'] != $webpagesrecord['pagename'])) {
                    $subarray[] = array('recordid' => MD5($scramble.'_'.$webpagesrecord['pageid']), 'pageid' => $webpagesrecord['pageid'], 'html' => $webpagesrecord['html'], 'language' => $webpagesrecord['language'], 'timestamp' => $webpagesrecord['timestamp'], 'sqlsource' => $webpagesrecord['sqlsource']);
                    $webpages[] = array('pagename' => $webpagesrecord['pagename'], 'title' => $webpagesrecord['title'], 'type' => 'db', 'subpage' => $subarray );
                    unset($subarray);
                } else {
                    $subarray[] = array('recordid' => MD5($scramble.'_'.$webpagesrecord['pageid']), 'pageid' => $webpagesrecord['pageid'], 'html' => $webpagesrecord['html'], 'language' => $webpagesrecord['language'], 'timestamp' => $webpagesrecord['timestamp'], 'sqlsource' => $webpagesrecord['sqlsource']);
                }
                $i++;
            }
            
        /**
        * Get the webpages from the filesystem
        */
        if (isset($folders)) {
          $folderarray = explode(',', $folders);
          foreach($folderarray as $folder) {
            $webpagefiles = GetFileList($_SERVER['DOCUMENT_ROOT'].$folder, $filenameonly=true, $extensions=array('tpl', 'php'));
            if (isset($webpagefiles)) {
              foreach($webpagefiles as $webpagefile) {  
                $subarray[] = array('recordid' => MD5($scramble.'_'.$webpagefile), 'pageid' => $webpagefile, 'html' => '', 'language' => 'AA', 'timestamp' => '', 'sqlsource' => '');
                $webpages[] = array('pagename' => $folder.$webpagefile, 'type' => 'fs', 'subpage' => $subarray);
                unset($subarray);
              }
            }
          }
        }
        return $webpages;
    }
    
    function DisplayWebPages() {
        
        /** Webpages ListView **/
        $parray = extractParams($this->params);
        
        $webpages = $this->GetWebPages($this->userdatabase, $parray['folders']);

        $this->smarty->assign('state', $this->state);
        $this->smarty->assign('permission', $this->permission);
        $this->smarty->assign('webpages', $webpages);
        return $this->smarty->Fetch('module.webpages.tpl');
    }

    function DisplayWebPage($rec, $type, $command) {
        global $scramble, $_GVARS, $_sqlUserWebPage, $_sqlUserWebPageLanguages, $_sqlWebLanguages, $_sqlUnusedWebLanguages;

        $parray = extractParams($this->params);
        if ($_GET['action'] == 'edit') {
            $rs = $this->userdatabase->Execute($_sqlUserWebPage, array($rec));
            if ($rs and $rs->recordCount() > 0) { 
                $webpagesrecord = $rs->FetchRow();
            
                $subarray[] = array('recordid' => MD5($scramble.'_'.$webpagesrecord['pageid']), 'pageid' => $webpagesrecord['pageid'], 'html' => '', 'language' => '', 'timestamp' => '', 'sqlsource' => '');
                $webpagesrecord = array('pagename' => $webpagesrecord['pagename'], 'title' => $webpagesrecord['title'], 'type' => 'db', 'subpage' => $subarray);
    
                $languages = $this->userdatabase->GetAll($_sqlUserWebPageLanguages, array($webpagesrecord['pagename']));
                $this->smarty->assign('lang', $languages);
                foreach ($languages as $language) {
                  $usedlang[] = '"'.$language['language'].'"';
                }
                $whereclause = implode(',',$usedlang);
                $rsavaillang = $this->userdatabase->Execute($_sqlUnusedWebLanguages.'('.$whereclause.')');
            }
            if ($rsavaillang->RecordCount( ) == 0) {
              $availlang = 'Er zijn geen andere talen beschikbaar.';
              $showeditor = false;
            } else {
              $availlang = RecordSetAsSelectList($rsavaillang, $keycolumn='languagecode', $namecolumn='desc_'.strtolower($_GVARS['lang']), true, $id='langid', $selectname='language',  "class='required'");
            }
        } elseif ($_GET['action'] == 'insert') {
            $rsavaillang = $this->userdatabase->Execute($_sqlWebLanguages);
            $availlang = RecordSetAsSelectList($rsavaillang, $keycolumn='languagecode', $namecolumn='desc_'.strtolower($_GVARS['lang']), true, $id='langid', $selectname='language',  "class='required'");
        }
        $this->smarty->assign('availlang', $availlang);
    
        $showeditor = true;
        $GLOBALS['includeGreyBox'] = true;

        if ($this->form->servicecall or ($_GET['maximize'] == 'true'))
          $cancelAction = 'javascript:window.close()';
        else
          $cancelAction = 'javascript:window.location.href=\''.$this->smarty->callerquery_base.'\';return false;';
        $this->smarty->assign('cancelAction', $cancelAction);

        if ($type == '') $type = 'db';

        switch ($type) {
        case 'db':
            $rs = $this->userdatabase->Execute($_sqlUserWebPage, array($rec));
            if ($rs) $webpagesrecord = $rs->FetchRow();
            $webpagesrecord['timestamp'] = $webpagesrecord['lastedited'];

            include_once('classes/textEditor.class.php');
            $_pageProcessor = $webpagesrecord['pageprocessor']?$webpagesrecord['pageprocessor']:'markdown' ;
            $_columnName = 'HTML';
            $_readonly = $webpagesrecord['editable'] == 'N';
            $_textEditor = new TextEditor($_pageProcessor, $_columnName, false, '100%', '190px', false, $_readonly);
            $_textEditor->setValue($webpagesrecord['html']);
//                $_textEditor->_editor->BaseHRef = $parray['base'];
            if (isset($parray['css']))
                $_textEditor->_editor->Config['EditorAreaCSS'] = $parray['css'];

            $rs = $this->userdatabase->Execute($_sqlUserWebPageLanguages, array($webpagesrecord['pagename']));
            $languages = $rs->GetAll();
            $this->smarty->assign('lang', $languages);
            break;
        case 'fs':
            if (isset($folders)) {
              $folderarray = explode(',', $folders);
              foreach($folderarray as $folder) {
                  $webpagefiles = GetFileList($_SERVER['DOCUMENT_ROOT'].$folder, $filenameonly=true, $extensions=array('tpl', 'php'));
                  foreach($webpagefiles as $webpagefile) {  
                      if (MD5($scramble.'_'.$webpagefile) == $rec) {
                          $thefilename = $_SERVER['DOCUMENT_ROOT'].$folder.$webpagefile;
                          break;
                      }
                  }
              }
            }
            if (isset($thefilename)) {
                include_once('classes/textEditor.class.php');
                $_pageProcessor = 'FCK';
                $_textEditor = new TextEditor($_pageProcessor);

                $subarray[] = array('recordid' => MD5($scramble.'_'.$webpagefile), 'pageid' => $webpagefile, 'html' => '', 'language' => 'AA', 'timestamp' => '', 'sqlsource' => '');
                $webpagesrecord = array('pagename' => $folder.$webpagefile, 'type' => 'fs', 'subpage' => $subarray);
                $_textEditor->Value = file_get_contents($thefilename);
            }
            break;
        }
        $this->smarty->assign('state', $state);
        $this->smarty->assign('permission', $permissiontype);
        $this->smarty->assign('showeditor', $showeditor);
        $this->smarty->assign('pageprocessor', $_pageProcessor);
        $this->smarty->assign('webpage', $webpagesrecord);
        $this->smarty->assign_by_ref('editor', $_textEditor);
        $this->smarty->assign('filetype', $type);
        return $this->smarty->Fetch('module.webpage_edit.tpl');
    }

    function Process() {
        if ($_POST['submit']) {
            $this->processed = true;
        }
    }

    function Show() {
        parent::Show();
    
        if ($_GET['action'] == 'edit' or $_GET['action'] == 'insert') {
            return $this->DisplayWebPage($_GET['rec'], $_GET['type'], $_GET['cmd']);
        } else {
            return $this->DisplayWebPages();
        }
    }
  	
}

?>