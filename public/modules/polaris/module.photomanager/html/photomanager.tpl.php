<link rel="stylesheet" type="text/css" media='all' href="{$serverroot}/modules/module.photomanager/module.photomanager.css" />

<div id="pm_addphoto"><div id="pm_addphoto_content">
    </form>
    <strong>Individuele foto's toevoegen</strong>
    <br />
    &nbsp;&nbsp;&nbsp;&nbsp;{lang image_add_new}: 
    <form action="." method="post" enctype="multipart/form-data">
    <input type="hidden" name="MAX_FILE_SIZE" value="500000" />
    <input type="file" name="imagefile1" size="20" /> 
    <input type="hidden" name="_hdnProcessedByModule" value="true" />
    <input type="submit" name="upload" value="{lang but_upload}" />
    &nbsp;<input type="checkbox" name="overwrite" value="yes" /> huidig bestand overschrijven?
    </form>
    <div style="float:right"><div id="ajax_action" style="display:none;font-weight:bold">Volgorde is aangepast</div></div>
    <br />
    <hr />
    <strong>Meerdere foto's toevoegen</strong>
    <br />
    <form name="dataform" id="dataform" method="post" action=".">
        <input type="hidden" name="_hdnAction" value="import" />
        <input type="hidden" name="_hdnDatabase" value="{$database}" />
        <input type="hidden" name="_hdnTable" value="{$tablename}" />
        <input type="hidden" name="_hdnProcessedByModule" value="true" />
        &nbsp;&nbsp;&nbsp;&nbsp;Folder:
        <input type="text" name="fotofolder" value="{$folder}" size="20" />
        <input type="submit" value="Kies bestanden"> 
    </form>
</div></div>

<form name="dataform" id="dataform" method="post" action=".">
<input type="hidden" name="_hdnAction" value="{if $photosimport|@count > 0}importfiles{else}delete{/if}" />
<input type="hidden" name="_hdnDatabase" value="{$database}" />
<input type="hidden" name="_hdnTable" value="{$tablename}" />
<input type="hidden" name="_hdnProcessedByModule" value="true" />
{if $importaction}

{if $photosimport|@count > 0}
    <p>Aantal te importeren foto's: {$photosimport|@count}<p>
    <div style="height:200px;overflow:auto;padding:1em;border:1px solid #777;">
    <ul style="margin:0;list-style:none;padding:0;">
    {section name=k loop=$photosimport}
        <li style="float:left;display:inline;margin:5px 10px 5px 5px;">
        <input type="checkbox" name="importphotos[]" checked="checked" value="{$photosimport[k]}" /> {$photosimport[k]}&nbsp;
        <a href="/thumb/?src={$dir}/{$photosimport[k]}&w=120&h=90" class="popup" /><img src="/images/zoom.png" alt="preview" /></a>
        </li>
    {/section}
    </ul>
    </div>
    <input type="submit" value="{lang but_import}" />
{else}
    <p>Alle foto's zijn geimporteerd.</p>
{/if}

{else}

<ul id="pm_imagebox">
{section name=i loop=$photos}
<li id="pm_{$photos[i].$idfield}" class="pm_image" itemid="{$smarty.section.i.index}" >
<div class="handle" >
<img src="/thumb/?src={$dir}/{$photos[i].$photofield}&w=120&h=90" title="{$photos[i].$photofield}" />
</div>
<div class="pm_actions">
<div class="pm_imagename"><abbr title="{$photos[i].$photofield}">{$photos[i].$photofield|truncate:25}</abbr></div>
<a href="{$web}/{$photos[i].$photofield}"><img src="{$serverroot}/images/zoom.png" title="{lang image_view_normal_size}" alt="{lang image_view_normal_size}" style="vertical-align:top" /></a>
<input id="rec:{$photos[i].PLR__RECORDID}" name="rec:{$photos[i].PLR__RECORDID}" type="checkbox" class="checkbox" />
</div>
</li>
{/section}
</ul>
<hr class="fix" /><br />
<input type="submit" value="{lang but_delete}" id="pm_delete" /> &nbsp;<a href="#" onclick="javascript:selectAll()">Alles selecteren</a> &nbsp;<a href="#" onclick="javascript:selectNone()">Niets selecteren</a>

{literal}
<script type="text/javascript">
var _selectCheckboxes = function(onoff) {
    $('#pm_imagebox :checkbox').each(function(e) {this.checked = onoff});
}
var selectAll = function() {_selectCheckboxes(true)};
var selectNone = function() {_selectCheckboxes(false)};

/***
    Make the photo order persistent in de db via Ajax
**/
var gotResponseData = function (response) {
    $('#ajax_action').show('slow', function(){ alert('1');$(this).hide("slow") } );
};
var responseFetchFailed = function (err) {
  alert('De sortering kon niet worden toegepast. Controleer uw parameters. ('+err+')');
};

var saveOrder = function (event,ui) {
    var url = window.location.href+'?ajaxaction=pm_sortorder&'+ ui.item.parent().sortable("serialize");
    $.getJSON(url, gotResponseData);
}

_loadevents_[_loadevents_.length] = function() {
    $('#pm_imagebox').sortable({'forcePlaceholderSize':true, 'update':saveOrder});

    $('#pm_delete').click(function(event) {
        if (CheckDel(this)) 
            this.form.submit();
        else
            return false;
    });
}

</script>
{/literal}
{/if}
