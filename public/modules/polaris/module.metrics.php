<?php
require_once('includes/mainactions.inc.php');
require_once('_shared/module._base.php');

use Michelf\Markdown;

class ModuleMetrics extends _BaseModule {
    var $stylesheet;

    function __construct($moduleid, $module, $clientname=false) {
        global $_GVARS;
        parent::__construct($moduleid, $module, $clientname);

        $this->processed = false;
        $this->stylesheet = 'default.css';
    }

    function __GetCustomFilter() {
    }

    function FormatBytes(int $size, $precision = 2) {
        $base = log($size, 1024);
        $suffixes = array('', 'K', 'M', 'G', 'T');

        return round(pow(1024, $base - floor($base)), $precision) .' '. $suffixes[floor($base)];
    }

    function ProcessJson() {
        $_content = false;
        $this->form->database->userdb->SetFetchMode(ADODB_FETCH_ASSOC);
        $_func = $_GET['func'];
        if (!empty($_func)) {
            switch ($_func) {
                case 'load_average':
                    $_content = $this->GetLoadAverage();
                    break;
                default:
                    break;
            }
        }
        if ($_content !== false) {
            echo $_content;
            exit; // stop with the rest of the html
        }
        return false;
    }

    function ProcessRequest($event=false) {
        if (isset($_POST['_hdnAction'])) {
            switch ($_POST['_hdnAction']) {
            case 'publish':
                return $this->PublishArticle($_POST['_hdnRecordID']);
            break;
            case 'plr_save':
                $_POST['UPDATED_AT'] = date('Y-m-d H:i:s');
                $_POST['BLOG_ID'] = 1;
                $result = SaveRecord($_POST);
                if ($result == true) {
                    $_GET['action'] == '';
                }
            break;
            }
        }
    }

    function GetLoadAverage(){
        exec('top -n 1', $_top_output);
        $_load_average = explode(" ", $_top_output[2]);
        $_new_load[0] = $_load_average[2];
        $_new_load[1] = $_load_average[3];
        $_new_load[2] = $_load_average[4];

        return $_new_load[0];
    }

    function GetStats(){
        $_stats = array();

        $free = shell_exec('free');
        $free = (string)trim($free);
        $free_arr = explode("\n", $free);
        $_mem = explode(" ", $free_arr[1]);

        $_mem = array_filter($_mem);
        $_mem = array_merge($_mem);
        $_new_mem['total_perc'] = '100';
        $_new_mem['used_perc'] = $_mem[2]/$_mem[1]*100;
        $_new_mem['free_perc'] = $_mem[3]/$_mem[1]*100;
        $_new_mem['shared_perc'] = $_mem[4]/$_mem[1]*100;
        $_new_mem['buff_cache_perc'] = $_mem[5]/$_mem[1]*100;
        $_new_mem['available_perc'] = $_mem[6]/$_mem[1]*100;
        $_new_mem['total_fmt'] = $this->FormatBytes($_mem[1]);
        $_new_mem['used_fmt'] = $this->FormatBytes($_mem[2]);
        $_new_mem['free_fmt'] = $this->FormatBytes($_mem[3]);
        $_new_mem['shared_fmt'] = $this->FormatBytes($_mem[4]);
        $_new_mem['buff_cache_fmt'] = $this->FormatBytes($_mem[5]);
        $_new_mem['available_fmt'] = $this->FormatBytes($_mem[6]);
        $_stats['memory'] = $_new_mem;
        $_memory_usage = $_mem[2]/$_mem[1]*100;
        $_stats['mem_usage_perc'] = $_memory_usage;

        exec('top -n 1', $_top_output);
        $_cpu_usage = explode(" ", $_top_output[1]);
        $_cpu_usage = array_filter($_cpu_usage);
        $_cpu_usage = array_merge($_cpu_usage);
        array_shift($_cpu_usage);
        $_new_cpu_usage = array();
        for ($i = 0, $lim = sizeof($_cpu_usage); $i < $lim; $i += 2) {
          $_new_cpu_usage[$_cpu_usage[$i+1]] = isset($_cpu_usage[$i]) ? $_cpu_usage[$i] : null;
        }
        $_stats['cpu_usage'] = $_new_cpu_usage;

        $_load_average = explode(" ", $_top_output[2]);
        $_new_load[0] = $_load_average[2];
        $_new_load[1] = $_load_average[3];
        $_new_load[2] = $_load_average[4];
        $_stats['load_average'] = $_new_load;
        $_new_load_prec[0] = $_load_average[2]/1*100;
        $_new_load_prec[1] = $_load_average[3]/1*100;
        $_new_load_prec[2] = $_load_average[4]/1*100;
        $_stats['load_average_perc'] = $_new_load_prec;

        return $_stats;
    }

    function ShowListView($state, $permission, $detail=false, $masterrecord=false) {
        $_customfilter = $this->__GetCustomFilter();
        $_rs = $this->form->GetViewContent($detail=false, $masterrecord=false, $limit=false, $_customfilter);
        $this->smarty->assign('items', $_rs);
        //         echo "<pre>";
        // print_r($this->GetStats());
        //         echo "</pre>";
        $this->smarty->assign('stats', $this->GetStats());
        $this->smarty->assign('stylesheet', $this->stylesheet);
        $this->AddModuleJS(['main.js']);

        echo $this->smarty->Fetch('metrics.tpl.php');
    }

    function ShowFormView($state, $permission, $detail=false, $masterrecord=false) {
        $_customfilter = $this->__GetCustomFilter();
        $_rs = $this->form->GetViewContent($detail=false, $masterrecord=false, $limit=false, $_customfilter);

        $_rs[0]['BODY_PREVIEW'] = Markdown::defaultTransform($_rs[0]['BODY']);
        $this->smarty->assign('stylesheet', $this->stylesheet);
        $this->smarty->assign('items', $_rs);
        echo $this->smarty->Fetch("blogedit.tpl.php");
    }

}