<?php

require_once('_shared/module._base.php');
require_once('includes/mainactions.inc.php');

class ModuleWebmenu extends _BaseModule {

	var $collecteditems;
	var $level;

	function __construct($moduleid, $module) {
        global $_GVARS;
        parent::__construct($moduleid, $module);

        $this->processed = false;
        $this->stylesheet = 'default.css';
	}

	function Process() {
    global $dr;

    parent::Process();

    if ($_GET['group'] and $_GET['group'] < 0)
      $this->currentmenugroup = $_GET['group'];
    elseif ($_POST['_hdnGroup'])
      $this->currentmenugroup = $_POST['_hdnGroup'];
		else
      $this->currentmenugroup = -1;

		if ($_GET['modaction'])
			$this->action = $_GET['modaction'];
		else
			$this->action = '';
    if ($_REQUEST['_hdnAction']) {
	 		switch(strtolower($_REQUEST['_hdnAction'])) {
			case ACTION_ADD_MENU:
				$this->AddMenu($_POST['menuname'], 'mainmenu', -1, $_POST['language']);
				break;
			case ACTION_ADD_MENUITEM:
				if ($_POST['menutype'] == 'menu')
				  $relmenuitem = $_POST['addnormalmenuitem'];
				else
					$relmenuitem = $_POST['addsubmenuitem'];
				$this->AddMenu($_POST['menuname'], $_POST['menutype'], $relmenuitem, $_POST['language'], $_POST['page']);
				break;
			case 'deletegroup':
			    break;
			case 'save':
				require_once('includes/mainactions.inc.php');
				$FieldsWithoutResources = SaveResources($_POST);
				SaveRecord($FieldsWithoutResources);
				break;
			case 'delete':
				/***
					After the records have been deleted, make sure the child items
					are moved to the parent of the deleted item
				*/
				global $_sqlWebMenuItemsWithParent;
				// LATER deleteResources($this->userdatabase, $_POST);
				DeleteRecords($_POST);
                foreach($_POST as $recid => $onoff) {
                    if (substr($recid, 0, 4) == 'rec:') {
                        $recordid = substr($recid, 4, 32);
                            $rs = $this->userdatabase->Execute($_sqlWebMenuItemsWithParent,array($recordid));
                            while($rec = $rs->FetchNextObject(true)) {
                                $record['parentmenuid'] = $this->currentmenugroup;
                                $this->userdatabase->AutoExecute('web_menu',$record,'UPDATE', 'menuid='.$rec->MENUID);
                            }
                    }
                }
				break;
			}
        }
    }

	function Show($outputformat='xhtml', $rec=false) {
        global $dr, $_sqlWebLanguages;
        parent::Show();

        if ($this->processed) {
            $this->smarty->assign('processed', $this->processed);
        }

        $this->level = -1;
        $this->smarty->assign('currentgroup', $this->currentmenugroup);
        $this->smarty->assign('webpages', $this->LoadWebPages());

        $this->LoadMenu($this->currentmenugroup);
        $this->smarty->assign('menu', $this->collecteditems);
        $this->smarty->assign('menugroups', $this->LoadMenuGroups());

        $deflang = $this->userdatabase->GetAll($_sqlWebLanguages.' AND defaultlanguage = "Y"');
        $this->smarty->assign('defaultlanguagecode', $deflang[0]['languagecode']?$deflang[0]['languagecode']:'NL');
        $this->smarty->assign('defaultlanguagename', $deflang[0]['desc_nl']?$deflang[0]['desc_nl']:'Nederlands');

		switch ($this->action) {
        case ACTION_EDIT:
            $this->smarty->assign('targets', array('','_blank','_parent','_self','_top'));
            $mirecord = $this->GetMenuItem($_GET['rec'], true);
            $this->smarty->assign('recset', $mirecord);
            return $this->smarty->fetch('module.webmenu_edit.tpl');
        break;
        case ACTION_ADD_MENU:
            return $this->smarty->fetch('module.webmenu_addmenu.tpl');
        break;
        default:
            return $this->smarty->fetch('module.webmenu.tpl');
		}
	}

  function LoadMenuGroups() {
    global $_sqlWebMenuGroups;

    $rs = $this->userdatabase->GetAll($_sqlWebMenuGroups);
    if ($rs) {
			// assign the first menugroup to the currentmenugroup variable
			return $rs;
		} else {
			return false;
		}
  }

  function LoadMenu($menugroup) {
    global $_sqlWebMenu;

    $defaultlanguage = 'NL';
    $rs = $this->userdatabase->GetAll($_sqlWebMenu, array($menugroup,$defaultlanguage));
		$this->level++;
    if ($rs and ($this->level <  5)) {
			foreach($rs as $menuitem) {
				$menuitem['level'] = $this->level;
				$this->collecteditems[] = $menuitem;
				$this->LoadMenu($menuitem['menuid']);
				$this->level--;
			}
    } else {
      return false;
    }
  }

  function LoadWebPages() {
    global $_sqlAllUserWebPages;

    $rs = $this->userdatabase->GetAll($_sqlAllUserWebPages);
    if ($rs) {
      return $rs;
    } else {
      return false;
    }
  }

	function GetMenuItem($menuid, $uppercase=false) {
		global $_sqlWebMenuItem, $_sqlWebLanguages;

		$rs = $this->userdatabase->GetAll($_sqlWebMenuItem, array($menuid));
		if ($rs) {
			$langs = $this->userdatabase->GetAll($_sqlWebLanguages);
			$rec = $rs[0];
			$defaultlanguage = '';
			foreach($langs as $lang) {
				$found = false;
				if ($lang['defaultlanguage'] == 'Y')
					$defaultlanguage = $lang['languagecode'];
				foreach($rs as $rsitem) {
					if ($rsitem['language'] == $lang['languagecode']) {
						$captionarray[] = array('language' => $rsitem['language'], 'menuname' => $rsitem['menuname']);
						$found = true;
					}
				}
				if (!$found) {
					$captionarray[] = array('language' => $lang['languagecode'], 'menuname' => '');
				}
			}
			$rec['menunames'] = $captionarray;
			$rec['defaultlanguage'] = $defaultlanguage;
			return $rec;
		} else
			return false;
	}

	function GetLastOrderIndex($parentmenuid) {
		global $_sqlLastWebMenuItem;

		$rs = $this->userdatabase->Execute($_sqlLastWebMenuItem, array($parentmenuid));
		if ($rs) {
			$rec = $rs->FetchNextObject(false);
			return $rec->orderindex;
		} else
			return 0;
	}

	function GetNewMenuId() {
		global $_sqlMaxWebMenuItem;

		$rs = $this->userdatabase->Execute($_sqlMaxWebMenuItem);
		if ($rs) {
			$rec = $rs->FetchNextObject(false);
			if ($rec->maxmenuid > 0)
				return $rec->maxmenuid + 1;
			else
				return 1;
		} else
			return 1;
	}

	function GetNewMainMenuId() {
		global $_sqlMinWebMenuItem;

		$rs = $this->userdatabase->Execute($_sqlMinWebMenuItem);
		if ($rs) {
			$rec = $rs->FetchNextObject(false);
			if ($rec->minmenuid < 0)
				return $rec->minmenuid - 1;
			else
				return -1;
		} else
			return -1;
	}

	function MoveMenuitemsDown($parentmenuid, $orderindex, $neworderindex) {
		global $_sqlMenuItemsOrderIndexUpdate;
		$rs = $this->userdatabase->Execute($_sqlMenuItemsOrderIndexUpdate, array($parentmenuid, $orderindex)) or Die('Error: '.ErrorMsg());
		if ($rs) {
  		while($rec = $rs->FetchNextObject(true)) {
  	    $record["orderindex"] = $neworderindex;
      	$this->userdatabase->AutoExecute('web_menu', $record, 'UPDATE', 'menuid = '.$rec->MENUID) or Die('Error: '.ErrorMsg());
  			$neworderindex++;
  		}
		}
	}

	function AddMenu($menuname, $menutype, $relmenuitem, $language, $page=false) {
		global $_sqlInsertMenuItem, $_sqlInsertNewResource;

		// Insert menuname as a resource
		$this->userdatabase->Execute($_sqlInsertNewResource, array($language, $menuname));
		$caption_rsc_ = $this->userdatabase->Insert_ID();
		$relmenuitemobject = $this->GetMenuItem($relmenuitem);
		// Determine the orderindex and parentmenu
		if ($menutype == 'mainmenu') {
			/***
			   adds a main menu
			*/
			$visible = 'Y';
			$orderindex = 1;
			$parentmenuid = 0;
	  	// Get the last menuid
			$menuid = $this->GetNewMainMenuId();
		} elseif ($menutype == 'menu') {
			/***
			   adds a normal menuitem
			*/
			if ($relmenuitemobject) {
				$orderindex = $relmenuitemobject['orderindex'] + 1;
				$parentmenuid = $relmenuitemobject['parentmenuid'];
			} else {
				$orderindex = 1;
				$parentmenuid = $this->currentmenugroup;
			}
			// Move all higher menuitems one position down
			$neworderindex = $orderindex + 1;
			$this->MoveMenuitemsDown($parentmenuid, $orderindex, $neworderindex);
			$visible = 'N';
	  	// Get the last menuid
			$menuid = $this->GetNewMenuId();
		}	else {
			/***
			   adds a submenuitem
			*/
			$parentmenuid = $relmenuitemobject['menuid'];
			$orderindex = $this->GetLastOrderIndex($relmenuitem) + 1;
			$visible = 'N';
	  	// Get the last menuid
			$menuid = $this->GetNewMenuId();
		}
		if (!$page) $page = '';
		// Insert the menuitem
		$this->userdatabase->Execute($_sqlInsertMenuItem,
			array($menuid, $caption_rsc_, $orderindex, $visible, $parentmenuid, $page));
	}

}

?>