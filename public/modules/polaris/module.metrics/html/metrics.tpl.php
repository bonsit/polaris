<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">

        <div class="col-md-3">
            <div class="widget style1 navy-bg">
                <div class="row">
                    <div class="col-md-4">
                        <i class="fa-solid fa-memory fa-4x"></i>

                    </div>
                    <div class="col-md-8 text-right">
                        <span>Memory usage</span>
                        <h2 class="font-bold">{$stats.mem_usage_perc|string_format:"%.2f"}%</h2>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="widget style1 navy-bg">
                <div class="row">
                    <div class="col-md-4">
                        <i class="fa-solid fa-microchip fa-4x"></i>

                    </div>
                    <div class="col-md-8 text-right">
                        <span>CPU usage</span>
                        <h2 class="font-bold">{$stats.cpu_usage.usr}</h2>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="widget style1 navy-bg">
                <div class="row">
                    <div class="col-md-4">
                        <i class="fa-solid fa-display fa-4x"></i>

                    </div>
                    <div class="col-md-8 text-right">
                        <span>Load average</span>
                        <h2 class="font-bold" id="load_average">{$stats.load_average[0]}</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">

        <div class="col-md-3">
            <div class="ibox-content">
                <div>
                    <span>Total</span>
                    <small class="float-right">{$stats.memory.total_fmt}</small>
                </div>
                <div class="progress progress-small">
                    <div style="width: {$stats.memory.total_perc}%;" class="progress-bar"></div>
                </div>

                <div>
                    <span>Used</span>
                    <small class="float-right">{$stats.memory.used_fmt}</small>
                </div>
                <div class="progress progress-small">
                    <div style="width: {$stats.memory.used_perc}%;" class="progress-bar"></div>
                </div>

                <div>
                    <span>Free</span>
                    <small class="float-right">{$stats.memory.free_fmt}</small>
                </div>
                <div class="progress progress-small">
                    <div style="width: {$stats.memory.free_perc}%;" class="progress-bar"></div>
                </div>

                <div>
                    <span>Shared</span>
                    <small class="float-right">{$stats.memory.shared_fmt}</small>
                </div>
                <div class="progress progress-small">
                    <div style="width: {$stats.memory.shared_perc}%;" class="progress-bar"></div>
                </div>

                <div>
                    <span>Available</span>
                    <small class="float-right">{$stats.memory.available_fmt}</small>
                </div>
                <div class="progress progress-small">
                    <div style="width: {$stats.memory.available_perc}%;" class="progress-bar"></div>
                </div>
            </div>
        </div>


        <div class="col-md-3">
            <div class="ibox-content">
                <div>
                    <span>Userspace</span>
                    <small class="float-right">{$stats.cpu_usage.usr}</small>
                </div>
                <div class="progress progress-small">
                    <div style="width: {$stats.cpu_usage.usr};" class="progress-bar"></div>
                </div>

                <div>
                    <span>Kernel</span>
                    <small class="float-right">{$stats.cpu_usage.sys}</small>
                </div>
                <div class="progress progress-small">
                    <div style="width: {$stats.cpu_usage.sys};" class="progress-bar"></div>
                </div>

                <div>
                    <span>Nice</span>
                    <small class="float-right">{$stats.cpu_usage.nic}</small>
                </div>
                <div class="progress progress-small">
                    <div style="width: {$stats.cpu_usage.nic};" class="progress-bar"></div>
                </div>

                <div>
                    <span>Idle</span>
                    <small class="float-right">{$stats.cpu_usage.idle}</small>
                </div>
                <div class="progress progress-small">
                    <div style="width: {$stats.cpu_usage.idle};" class="progress-bar"></div>
                </div>

                <div>
                    <span>IO</span>
                    <small class="float-right">{$stats.cpu_usage.io}</small>
                </div>
                <div class="progress progress-small">
                    <div style="width: {$stats.cpu_usage.usr};" class="progress-bar"></div>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="ibox">
                <div class="ibox-content">
                    <h5>Load average</h5>
                    <div id="load_average_graph"><canvas width="230" height="60" style="display: inline-block; width: 230px; height: 60px; vertical-align: top;"></canvas>
                    </div>
                    <div>
                        <span>1 minute load average</span>
                        <small class="float-right">{$stats.load_average[0]}</small>
                    </div>
                    <div class="progress progress-small">
                        <div style="width: {$stats.load_average_perc[0]}%;" class="progress-bar"></div>
                    </div>

                    <div>
                        <span>5 minutes load average</span>
                        <small class="float-right">{$stats.load_average[1]}</small>
                    </div>
                    <div class="progress progress-small">
                        <div style="width: {$stats.load_average_perc[1]}%;" class="progress-bar"></div>
                    </div>

                    <div>
                        <span>15 minutes load average</span>
                        <small class="float-right">{$stats.load_average[2]}</small>
                    </div>
                    <div class="progress progress-small">
                        <div style="width: {$stats.load_average_perc[2]}%;" class="progress-bar"></div>
                    </div>

                </div>
            </div>
        </div>

    </div>


    <div class="row">

        <div class="col-md-3">
            <div class="widget style1 navy-bg">
                <div class="row">
                    <div class="col-md-4">
                        <i class="fa-solid fa-people-group fa-4x"></i>

                    </div>
                    <div class="col-md-8 text-right">
                        <span>Number of clients</span>
                        <h2 class="font-bold">{$items[0].CLIENT_TOTAL}</h2>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="widget style1 lazur-bg">
                <div class="row">
                    <div class="col-md-4">
                        <i class="fa-solid fa-display fa-4x"></i>

                    </div>
                    <div class="col-md-8 text-right">
                        <span>Number of apps</span>
                        <h2 class="font-bold">{$items[0].APP_TOTAL}</h2>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="widget style1 yellow-bg">
                <div class="row">
                    <div class="col-md-4">
                        <i class="fa-solid fa-person fa-4x"></i>

                    </div>
                    <div class="col-md-8 text-right">
                        <span>Number of users</span>
                        <h2 class="font-bold">{$items[0].USERGROUP_TOTAL}</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var load_avg = [{','|implode:$stats.load_average}];
</script>