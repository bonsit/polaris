$(document).ready(function() {

    $(function() {

        var sparklineDuration = 10; //seconds - value stay in view for how long
        var valuesList = [0]; // list of values
        var new_load = 0; // value should be replace with real value
        var sparklinePulsTime = 1000; //every second - how frequent to update line

        var sparklineLeadLine = function() {
            $('#load_average_graph').sparkline(valuesList, {
                type: 'line',
                width: '100%',
                height: '60',
                lineColor: '#1ab394',
                fillColor: "#ffffff",
                chartRangeMin: 0,
                chartRangeMax: 1
            });
        }

        window.setInterval(function(){
            getLoadAverage(function(new_load) {
                console.log(new_load);
                valuesList.push( new_load );
                valuesList = updateValueList(valuesList, sparklineDuration);
                sparklineLeadLine();
                $('#load_average').text(new_load);
            });
        }, sparklinePulsTime);

        var refreshSparklines;
        $(window).resize(function(e) {
            clearTimeout(refreshSparklines);
            refreshSparklines = setTimeout(sparklineLeadLine, 500);
        });
        sparklineLeadLine();

    })

    function getLoadAverage(callback) {
        $.getJSON(_servicequery, {
            'func': 'load_average',
            '_hdnProcessedByModule': 'true'
        },
        function(data, textStatus) {
            if (textStatus == 'success') {
                if (callback) callback(data);
            } else {
                console.log('Geen load_average gevonden: ' + textStatus);
            }
        });
    }

    function updateValueList(valuesList, sparklineDuration) {

        if(valuesList.length > sparklineDuration) {
            return valuesList.slice(valuesList.length - sparklineDuration,valuesList.length);
        }

        return valuesList;
    }

});
