<script language="JavaScript" type="text/javascript" src="{$serverroot}/modules/module.filemanager/js/swfupload.js"></script>
<script language="JavaScript" type="text/javascript" src="{$serverroot}/modules/module.filemanager/js/swfupload.queue.js"></script>
<script language="JavaScript" type="text/javascript" src="{$serverroot}/modules/module.filemanager/js/fileprogress.js"></script>
<script language="JavaScript" type="text/javascript" src="{$serverroot}/modules/module.filemanager/js/handlers.js"></script>
{literal}
<script type="text/javascript">
		var swfu;

		window.onload = function() {
			var settings = {
				flash_url : "../swfupload/swfupload_f9.swf",
				upload_url: "../simpledemo/upload.php",	// Relative to the SWF file
				post_params: {"PHPSESSID" : ""},
				file_size_limit : "100 MB",
				file_types : "*.*",
				file_types_description : "All Files",
				file_upload_limit : 100,
				file_queue_limit : 0,
				custom_settings : {
					progressTarget : "fsUploadProgress",
					cancelButtonId : "btnCancel"
				},
				debug: false,

				// The event handler functions are defined in handlers.js
				file_queued_handler : fileQueued,
				file_queue_error_handler : fileQueueError,
				file_dialog_complete_handler : fileDialogComplete,
				upload_start_handler : uploadStart,
				upload_progress_handler : uploadProgress,
				upload_error_handler : uploadError,
				upload_success_handler : uploadSuccess,
				upload_complete_handler : uploadComplete,
				queue_complete_handler : queueComplete	// Queue plugin event
			};

			swfu = new SWFUpload(settings);
	     };
</script>
{/literal}

<div id="filemanager">
<table id="fm_table" border="0" cellspacing="5" cellpadding="5">

{section name=i loop=$files}
<tr>
    <td>{if $files[i].filesize}<a href="?download={$files[i].filename};">{/if}{$files[i].filename|truncate:40}{if $files[i].filesize}</a>{/if}</td>
    <td>{if $files[i].filesize}{$files[i].filesize} KB{else}{/if}</td>
    <td>{$files[i].filetime|date_format:"%d/%m/%y"}</td>
    <td><a href="#" onclick="if (confirm('Verwijderen?')) window.location='./?rmfile={$files[i].filename}'">Delete</a></td>
</tr>
{/section}


</table>

<div id="fm_upload">

    <form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>" enctype="multipart/form-data">
        <p><input type="file" name="file" /></p>
        <p><input type="submit" value="Upload" /></p>
    </form>

</div>
</div>