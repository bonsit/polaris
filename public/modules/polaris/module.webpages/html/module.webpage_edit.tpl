<ul id="masteractions" class="mdactions">
<li class="insert"><a id="action_masterinsert" href="{$callerquery}insert/">Voeg een pagina toe</a></li>
<li id="action_masterall" class="allitems"><a href="{$callerquery}">Alle pagina's</a></li>
</ul>

<div id="formview" class="formview alignlabels">

<input type="hidden" name="_hdnState" value="{if $smarty.get.cmd eq 'add_language' or $smarty.get.action eq 'insert'}insert{else}edit{/if}" />
<input type="hidden" name="_hdnRecordID" value="{$webpage.recordid}" />
<input type="hidden" name="_hdnProcessedByModule" value="false" />
<input type="hidden" name="LASTEDITED" value="NOW()" />
<input type="hidden" name="PAGEPROCESSOR" value="{$pageprocessor}" />

<div id="_rowPAGINA" class="formviewrow"><label class="label">Pagina: </label><div class="formcolumn">

<input type='text' label="Pagina" id="_fldPAGENAME" {if $smarty.get.action ne 'insert'}class='readonly' readonly="readonly"{/if} name='PAGENAME' size='40' maxlength ='40' value='{$webpage.pagename}' />
 <span style="color:gray">Laatst gewijzigd op: {$webpage.timestamp|date_format:"%A %e %B %Y &nbsp; %H:%M:%S"}</span>
</div></div>
<div id="_rowTITEL" class="formviewrow"><label class="label">Titel: </label><div class="formcolumn">
<input type='hidden' id="_fldTITLEORIG" value='{$webpage.title}' />
<input type='text' id="_fldTITLE" label="Titel" name='TITLE' size='40' maxlength ='255' value='{$webpage.title}' /></div></div>

{if $lang|@count gt 0}
<hr class="fix" /><br />
    <div class="pagetabcontainer">
        <ul id="pagetabs">
            {section name=i loop=$lang}
                <li><a {if $lang[i].language eq $webpage.language}class="selected"{/if} href="{$callerquery}edit/{$lang[i].recordid}/?type={$webpage.type|default:$lang[i].type}">{$lang[i].language}{*php}global $smarty;echo lang_get('lang_'.strtolower($smarty->_tpl_vars['lang'][$smarty->_sections['i']['index']]['language'])){/php*}</a></li>
            {/section}
            {if $smarty.get.cmd ne 'add_language'}
                <li class="clean"><a href="{$callerquery}edit/{$smarty.get.rec}/?type={$webpage.type|default:$lang[i].type}&cmd=add_language">Nieuwe taal toevoegen</a></li>
            {else}
                <li id="sel"><a href="javascript:void(0)" onclick="return false">&lt;Nieuwe taal&gt;</a></li>
            {/if}
        </ul>
    </div>
{else}
<div id="_rowPAGINA" class="formviewrow"><label class="label">Taal: </label><div class="formcolumn">
    {$availlang}
</div></div>
{/if}

{if $smarty.get.cmd eq 'add_language'}
{/if}

{if $showeditor eq true}
    <div class="markdownpreviewcontainer">
        <div id="wmd-preview" class="wmd-preview markdownpreviewpane" style="width:400px;height:0px"></div>
        {$editor->Render()}
    </div {*markdownpreviewcontainer*}>
        
    <div id="_rowADVANCED" class="formviewrow">
        <label class="label"></label>
        <div class="formcolumn">
            <a href="javascript:void(0)" onclick="javascript:$('#advanced').toggle();">Geavanceerd &gt;&gt;</a>
            <div id="advanced" style="display:none">
                {if $filetype eq 'db'}
                Content source: <textarea label="Sqlsource" class='' name='SQLSOURCE' size='50' style="height:2em;" maxlength='5000'>{$webpage.sqlsource|escape:"html"}</textarea>
                {/if}
            </div>
        </div>
    </div>
{/if}
    <br />
</div>

{literal}
<script type="text/javascript">
$("#_fldPAGENAME").keyup(function() {
    if ($("#_fldTITLEORIG").val() == '')
        $("#_fldTITLE").val($(this).val().replace(/_/g,' '));
});
$("#_fldPAGENAME").blur(function() {
    $(this).val($(this).val().replace(/ /g,'_').toLowerCase());
});
</script>
{/literal}