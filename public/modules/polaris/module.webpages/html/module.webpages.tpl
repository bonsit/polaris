<ul id="masteractions">
<li class="insert"><a id="action_masterinsert" href="{$callerquery}insert/">Voeg een pagina toe</a></li>
<li id="action_masterall" class="allitems"><a href="{$callerquery}">Alle pagina's</a></li>
</ul>

<br />
{section name=i loop=$webpages}

{if $smarty.section.i.first}
<table class="data striped group-table right">
<thead><tr><th width="1%">{lang type}</th><th width="300px;">{lang webpage_name}</th><th width="300px;">{lang webpage_title}</th><th>{lang languages}</th>
{if $permission & DELETE == DELETE}
<th class="center group-table right">{lang delete}</th>
{/if}
</tr></thead>
{/if}
{section name=sub loop=$webpages[i].subpage}
{if $smarty.section.sub.first}
<tr><td>
{if $webpages[i].type eq 'fs'}
<img src="{$serverroot}/images/wp_fs2.png" width="16" height="16" alt="fs" title="File webpage" />
{else}
<img src="{$serverroot}/images/wp_db2.png" width="16" height="16" alt="db" title="Database webpage" />
{/if}
</td><td><a title="{lang edit}" href="{$callerquery}edit/{$webpages[i].subpage[sub].recordid}/?type={$webpages[i].type}">{$webpages[i].pagename}</a>
 <a href="{$callerquery}edit/{$webpages[i].subpage[sub].recordid}/?type={$webpages[i].type}">[{lang edit}]</a></td
>
<td>{$webpages[i].title}</td>

<td>
{/if}
{*<a href='{urlquery_add url=$callerquery}&action=edit&type={$webpages[i].type}&rec={$webpages[i].subpage[sub].recordid}'>*}
{$webpages[i].subpage[sub].language}
{*</a>*}&nbsp; 
{/section}
{if $smarty.section.sub.last}
</td>
{if $permission & DELETE == DELETE}
<td class="center"><input name="recordid[]" value="rec:{$webpages[i].pageid}" type="checkbox"></td>
{/if}
</tr>
{/if}
{if $smarty.section.i.last}
<tfoot>
<td></td><td></td><td></td><td></td>
{if $permission & DELETE == DELETE}
<td class="center"><input type="submit" class="protect" value="Verwijder" onclick="return CheckDel(this);" /></td>
{/if}
</tfoot>
</table>
</form>
{/if}
{sectionelse}
<p>{lang no_webpage_exists}</p>
<ul class="startitems">
{section name=j loop=$actions}
<li class="normalmenu"><a href="{$actions[j].location}"><img src="{$serverroot}/{#default_icon_url#}{$actions[j].icon}" height="{#large_icon_height#}" width="{#large_icon_width#}" alt="" class="im" />{$actions[j].caption}</a></li>
{/section}
</ul>
{/section} 
