<?php
set_time_limit(300);

require_once('includes/mainactions.inc.php');
require_once('module._base.php');

class Module extends _BaseModule {
    var $folder;
    var $folderfield;

    function Module($moduleid, $module) {
        $this->_BaseModule($moduleid, $module);

        $this->processed = false;
        $GLOBALS['includeSortable'] = true;
    }

    function DisplayPhotoManager() {
        global $_GVARS;

        $_GVARS['_includeJavascript'][] = $_GVARS['serverroot'].'/javascript/jquery/jquery-ui-personalized-1.6rc2.min.js';

        /** Webpages ListView **/
        $rs = $this->form->GetViewContent($detail=true, $this->masterrecord, $limit=50);
        $photos = $rs->getArray();
        if (isset($this->parray['folderfield'])) {
            $folderfield = $this->parray['folderfield'];
            $this->folder = $this->masterrecord->$folderfield;
            $this->smarty->assign('folderfield', $folderfield);
            $this->smarty->assign('folder', $this->folder);
            $this->folder = '/'.$this->folder;
        } else {
            $this->folder = '';
        }

        if ($_POST['_hdnAction'] == 'import') {
            $dh  = opendir($this->parray['dir'].$this->folder);
            while (false !== ($filename = readdir($dh))) {
                if ($filename[0] != '.')
                    $files[] = $filename;
            }
            foreach($photos as $photo) {
                $i = array_search($photo['FOTOURL'], $files);
                if ($i !== false) {
                    unset($files[$i]);
                }
            }
            sort($files);
            $this->smarty->assign('importaction', true);
            $this->smarty->assign('photosimport', $files);
        }

        $this->smarty->assign('state', $this->state);
        $this->smarty->assign('database', encodeString($this->form->record->CLIENTID.'_'.$this->form->record->DATABASEID));
        $this->smarty->assign('tablename', $this->form->GetDataDestination());
        $this->smarty->assign('permission', $this->permission);
        $this->smarty->assign('web', $this->parray['web'].$this->folder);
        $this->smarty->assign('dir', $this->parray['dir'].$this->folder);
        $this->smarty->assign('idfield', $this->parray['idfield']);
        $this->smarty->assign('orderfield', $this->parray['orderfield']);
        $this->smarty->assign('photofield', $this->parray['photofield']);

        $this->smarty->assign('photos', $photos);
        return $this->smarty->Fetch('photomanager.tpl.php');
    }

    function InsertImages($files) {
        $sql = 'SELECT MAX('.$this->parray['orderfield'].') + 1 as MAXVOLGORDE, MAX('.$this->parray['idfield'].') + 1 as MAXID FROM '.$this->form->GetDataDestination().' WHERE ';
        $this->form->GetKeyFields();
        $where = $this->form->MakeMasterWhereClause($this->masterrecord);
        $sql .= $where;
//        $mkeys = array_shift($this->form->keyfields);
//var_dump($mkeys);
//        foreach($mkeys as $keyfield) {
//            if (isset($this->masterrecord->$keyfield))
//                $sql .= "$keyfield = {$this->masterrecord->$keyfield} AND ";
//        }
        $maxims = $this->userdatabase->GetAll($sql);
        $maxorder = $maxims[0]['MAXVOLGORDE'];
        $maxid = $maxims[0]['MAXID'];

        if (!$maxorder) $maxorder = 1;
        if (!$maxid) $maxid = 1;
//        $this->userdatabase->debug=true;
        $insertsql = 'INSERT INTO '.$this->form->GetDataDestination().' ( ';
        $keys = $this->form->keyfields;

        $insertsql .= implode(',',$keys);
        $insertsql .= ', '.$this->parray['photofield'];
        $insertsql .= ', '.$this->parray['orderfield'];
        $insertsql .= ') VALUES ( ';
//        array_pop($keys);

        foreach($keys as $keyfield) {
            $keyfound = array_search($keyfield, $this->form->detailfields);
            if ($keyfound !== false) {
                $masterkeyfield = $this->form->masterfields[$keyfound];
                if (isset($this->masterrecord->$masterkeyfield))
                    $insertsql .= $this->masterrecord->$masterkeyfield.', ';
            }
        }
        $insertsql .= ' ?,?,?) ';
        foreach($files as $file) {
            $seppos = strrpos($file, '/');
            if ($seppos !== false) $file = substr($file, $seppos+1);
            $file = urlencode($file);
            $this->userdatabase->Execute($insertsql, array($maxid, $file, $maxorder) );
            $maxid++;
            $maxorder++;
        }
    }

    function UpdateFotoFolder($fotofolder) {
        $ff = $this->parray['folderfield'];
        if ($this->masterrecord->$ff != $fotofolder) {
            $this->form->GetKeyFields();
            $sql = 'UPDATE '.$this->form->owner->masterform->GetDataDestination().' SET '.$this->parray['folderfield'].' = ? WHERE '.$this->form->MakeMasterWhereClause($this->masterrecord, $type="mastercolumns");
//            $this->userdatabase->debug=true;
            $this->userdatabase->Execute($sql, array($fotofolder));
        }
    }

    function ImportFiles() {
        $this->InsertImages($_POST['importphotos']);
    }

    function ProcessUploadedImages() {
        require_once('includes/func_imageupload.inc.php');

        // if one or more images is being uploaded, move them
        // to the client image path and create thumbnails of them
        if ( isset($this->parray['dir']) ) $path = rawurldecode($this->parray['dir']);
        $movedfiles = moveuploadedimages($path, $_POST['overwrite']=='yes');
        $this->InsertImages($movedfiles);
    }

    function Process() {
        $this->parray = extractParams($this->params);
        $masterfield = $this->parray['masterfield'];
        $this->form->GetKeyFields();

        if (isset($_GET['ajaxaction']) and $_GET['ajaxaction'] == 'pm_sortorder') {
            // save the new photo order
            $photos = $_GET['pm'];
            $i=1;
            $sql = "UPDATE ".$this->form->GetDataDestination()." SET ".$this->parray['orderfield']." = ? WHERE  ";
            foreach($this->form->keyfields as $keyfield) {
                if (isset($this->masterrecord->$keyfield))
                    $sql .= "$keyfield = {$this->masterrecord->$keyfield} AND ";
                else
                    $sql .= "$keyfield = ? ";
            }
            foreach($photos as $photo) {
                $this->userdatabase->Execute($sql, array($i, $photo));
                $i++;
            }
            $response = array('result'=>true);
            echo json_encode($response);
            die();
        }

        if ($_POST['_hdnAction'] == 'delete') {
            DeleteRecords($_POST);
        }

        if ($_POST['_hdnAction'] == 'import') {
            $this->UpdateFotoFolder($_POST['fotofolder']);
        }

        if ($_POST['_hdnAction'] == 'importfiles') {
            $this->ImportFiles();
        }

        if ($_POST['upload']) {
            $this->ProcessUploadedImages();
            $this->processed = true;
        }
    }

    function Show() {
        parent::Show();
        return $this->DisplayPhotoManager();
    }

}

?>