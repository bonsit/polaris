<?php
require('_shared/module._base.php');

class Module extends _BaseModule {

    function Module($moduleid, $module) {
        global $_GVARS;
        global $polaris;

        parent::__construct($moduleid, $module);

        $this->processed = false;

        $this->isAngularApplication = false;

        /**
        * What client are we talking about? (nog doen: eventueel selecteerbaar maken)
        */
        $this->clientid = $_SESSION['clientid'];

        /* Load the user database */
        $_databaseid = null;
        if (isset($_databaseid)) {
            $db = new plrDatabase($polaris);
            $db->LoadRecord(array($this->clientid, $_databaseid));
            $this->userdatabase = $db->connectUserDB();
        }

        if (isset($_POST['_hdnProcessedByModule'])) {
            $_allesRecord = $_POST;

            try {
                switch($_POST['_hdnFORMTYPE']) {
                /***
                * Upload postcodebestand verwerken
                */
                case '...someaction...':
                break;
                }

                $resultArray = Array('result'=>$result, 'query'=>$_SERVER['REQUEST_URI'], 'error'=>false);
                echo json_encode($resultArray);
                exit;
            } catch (Exception $E) {
                $detailedmessage = $E->completemsg."\r\n".parse_backtrace(debug_backtrace());

                $resultArray = Array('result'=>false, 'query'=>$_SERVER['REQUEST_URI'], 'error'=>$E->msg, 'detailed_error'=>$detailedmessage);
                echo  json_encode($resultArray);
            }
        }
    }

    function Process() {
        global $_GVARS;
    }

    function Show($outputformat='xhtml', $rec=false) {
        parent::Show($outputformat, $rec);

        switch($this->outputformat) {
            case 'xhtml':
                $this->showControls = false;
                $this->showDefaultButtons = false;

                switch ($this->moduleid) {
                    case '1':
                        $this->smarty->display("documents.tpl.php");
                        break;
                    case '2':
                        $this->smarty->display("dashboard.tpl.php");
                        break;
                    default:
                        $this->smarty->display("main.tpl.php");
                        break;
                }
        }
    }

}