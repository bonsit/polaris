<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="file-manager">
                        <button class="btn btn-success btn-block">Upload Files</button>
                        <div class="hr-line-dashed"></div>
                        <h5>Folders</h5>
                        <ul class="folder-list" style="padding: 0">
                            <li><a href=""><i class="fa fa-folder"></i> Van Dort Letselschade</a></li>
                            <li><a href=""><i class="fa fa-folder"></i> Berichten</a></li>
                            <li><a href=""><i class="fa fa-folder"></i> Foto's</a></li>
                            <li><a href=""><i class="fa fa-folder"></i> Onkosten</a></li>
                            <li><a href=""><i class="fa fa-folder"></i> Medisch</a></li>
                        </ul>
                        <h5 class="tag-title">Tags</h5>
                        <ul class="tag-list" style="padding: 0">
                            <li><a href="">Rechtbank</a></li>
                            <li><a href="">Verzekeraar</a></li>
                            <li><a href="">Letselschade</a></li>
                            <li><a href="">Gezondheid</a></li>
                            <li><a href="">Arts</a></li>
                            <li><a href="">Specialist</a></li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-9">
            <div class="row">
                <div class="col-lg-12">
                    <div class="file-box">
                        <div class="file">
                            <a href="#">
                                <span class="corner"></span>
                                <div class="icon">
                                    <i class="fa fa-file-pdf-o"></i>
                                </div>
                                <div class="file-name">
                                    Document_2016.doc
                                    <br/>
                                    <small>Toegevoegd: Jan 11, 2016</small>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="file-box">
                        <div class="file">
                            <a href="#">
                                <span class="corner"></span>
                                <div class="icon">
                                    <i class="fa fa-file-pdf-o"></i>
                                </div>
                                <div class="file-name">
                                    Italy street.jpg
                                    <br/>
                                    <small>Toegevoegd: Jan 6, 2016</small>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="file-box">
                        <div class="file">
                            <a href="#">
                                <span class="corner"></span>
                                <div class="icon">
                                    <i class="fa fa-file-excel-o"></i>
                                </div>
                                <div class="file-name">
                                    My feel.png
                                    <br/>
                                    <small>Toegevoegd: Jan 7, 2016</small>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="file-box">
                        <div class="file">
                            <a href="#">
                                <span class="corner"></span>
                                <div class="icon">
                                    <i class="fa fa-file-audio-o"></i>
                                </div>
                                <div class="file-name">
                                    Michal Jackson.mp3
                                    <br/>
                                    <small>Toegevoegd: Jan 22, 2016</small>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="file-box">
                        <div class="file">
                            <a href="#">
                                <span class="corner"></span>
                                <div class="icon">
                                    <i class="fa fa-file-image-o"></i>
                                </div>
                                <div class="file-name">
                                    Document_2016.doc
                                    <br/>
                                    <small>Toegevoegd: Feb 11, 2016</small>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="file-box">
                        <div class="file">
                            <a href="#">
                                <span class="corner"></span>
                                <div class="icon">
                                    <i class="fa fa-file-text-o"></i>
                                </div>
                                <div class="file-name">
                                    Monica's birthday.mpg4
                                    <br/>
                                    <small>Toegevoegd: Feb 18, 2016</small>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="file-box">
                        <a href="#">
                            <div class="file">
                                <span class="corner"></span>
                                <div class="icon">
                                    <i class="fa fa-file-word-o"></i>
                                </div>
                                <div class="file-name">
                                    Annual report 2016.xls
                                    <br/>
                                    <small>Toegevoegd: Feb 22, 2016</small>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="file-box">
                        <div class="file">
                            <a href="#">
                                <span class="corner"></span>
                                <div class="icon">
                                    <i class="fa fa-file"></i>
                                </div>
                                <div class="file-name">
                                    Document_2016.doc
                                    <br/>
                                    <small>Toegevoegd: Jan 11, 2016</small>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>