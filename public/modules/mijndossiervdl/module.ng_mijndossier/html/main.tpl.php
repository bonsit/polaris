<link href="{$moduleroot}css/default.css" rel="stylesheet">
<link href="{$moduleroot}css/plugins/iCheck/custom.css" rel="stylesheet">
<link href="{$moduleroot}css/plugins/steps/jquery.steps.css" rel="stylesheet">
<link href="{$moduleroot}css/plugins/fullcalendar/fullcalendar.css" rel="stylesheet">
<link href="{$moduleroot}css/plugins/fullcalendar/fullcalendar.print.css" rel='stylesheet' media='print'>

<div class="ibox float-e-margins">
    <div class="ibox-contentxxx" id="ibox-content">

<button class="btn btn-success btn-rounded" type="button" data-toggle="modal" data-target="#myModal2"><i class="fa fa-comment"></i>&nbsp; <span>Stel vraag</span></button>
        <button class="btn btn-primary btn-rounded" type="button" data-toggle="modal" data-target="#myModal3"><i class="fa fa-file"></i>&nbsp; <span>Stuur document</span></button>


        <div id="vertical-timeline" class="vertical-container light-timeline center-orientation">





            <div class="vertical-timeline-block">
                <div class="vertical-timeline-icon blue-bg">
                    <i class="fa fa-file-text"></i>
                </div>

                <div class="vertical-timeline-content">
                    <h2>Gezondsheidsverloop bijhouden</h2>
                                                    <img src="{$moduleroot}/images/app.png" width="100px" class="pull-right" />

                    <p>Met een paar simpele tikken kunt u vanaf het eerste moment precies bijhouden welke klachten u heeft, welke beperkingen u ondervindt
                        en bijvoorbeeld welke afspraken u heeft gemaakt met uw huisarts of specialst. Wel zo handig om thuis nog eens alles na te kijken.
                    </p>
                    <a href="#" class="btn btn-sm btn-success"> Download app </a>
                    <span class="vertical-date">
                        Vandaag <br/>
                        <small>16 nov 2016</small>
                    </span>
                </div>
            </div>

            <div class="vertical-timeline-block">
                <div class="vertical-timeline-icon yellow-bg">
                    <i class="fa fa-coffee"></i>
                </div>

                <div class="vertical-timeline-content">
                    <h2>Afspraak kantoor Maastricht</h2>
                    <p>We zouden graag een afspraak maken bij ons op kantoor. Schikt het u donderdag 2 februari a.s. om 11.00 uur?
                    <div class="pull-right">
                        <button class="btn btn-secondary btn-sm" type="button">Nee</button>
                        <button class="btn btn-primary btn-sm" type="button">Ja</button>
                    </div>
                </p>
                    <span class="vertical-date">Een week geleden <br/><small>8 nov 2016</small></span>
                </div>
            </div>

            <div class="vertical-timeline-block">
                <div class="vertical-timeline-icon lazur-bg">
                    <i class="fa fa-file"></i>
                </div>

                <div class="vertical-timeline-content">
                    <h2>Onze afspraken op schrift</h2>
                    <p>U heeft gekozen de optie No Cure No Pay. In de bijlage vindt u de afspraken die we met u gemaakt hebben. Als u nog vragen heeft, neem gerust contact met ons op. </p>
                    <button class="btn btn-xs btn-primary">Open bijlage</button>
                    <span class="vertical-date">Twee weken geleden <br/><small>1 nov 2016</small></span>
                </div>
            </div>

            <div class="vertical-timeline-block">
                <div class="vertical-timeline-icon navy-bg">
                    <img src="{$moduleroot}/images/vandortletselschade_logo_wit.png" style="width:1.6vmax;padding-top:8px;" />
                </div>

                <div class="vertical-timeline-content">
                    <img src="{$moduleroot}/images/beeksma.png" class="img-circle pull-right" alt="profile" style="width:50px;">
                    <h2>Welkom Mevr. Janssen</h2>
                    <p>Van harte welkom bij <b>Van Dort Letselschade</b> - Kantoor Amsterdam.
                    In deze beveiligde persoonlijke omgeving heeft u altijd inzicht in de status van uw dossier.
                    Ook kunt u hier vragen stellen of documenten opsturen naar ons kantoor.<br/>
                    Heeft u vragen? U kunt mij altijd bereiken op 012-34567890.<br/>
                    <span class="pull-right">– Martine Beeksma</span></p>
                    <span class="vertical-date">Twee weken geleden <br/><small>1 nov 2016</small></span>
                </div>
            </div>
        </div>

    </div>
</div>


<div class="modal inmodal" id="myModal2" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated flipInY">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Stel een vraag</h4>
                <small class="font-bold">Wat wilt u ons vragen?</small>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group"><label>Bericht</label> <textarea rows="10" placeholder="Typ hier uw tekst" class="form-control"></textarea></div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Sluit</button>
                <button type="button" class="btn btn-primary">Verstuur</button>
            </div>

        </div>
    </div>
</div>

<div class="modal inmodal" id="myModal3" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated flipInY">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Stuur een document</h4>
            </div>
            <div class="modal-body">

            <form action="#" class="dropzone dz-clickable" id="dropzoneForm">

            <div class="dz-default dz-message"><span><strong>Drag &amp; Drop uw bestanden hier of klik om te uploaden. </strong><br> (Dit is een demo. Bestanden worden niet echt geüpload.)</span></div></form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Sluit</button>
                <button type="button" class="btn btn-primary">OK</button>
            </div>


        </div>
    </div>
</div>
