<div class="wrapper wrapper-conten">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-contentx">
                                    <div>
                                        <span class="pull-right text-right">
                                        <small>Webbezoek vandortletselschade.nl</small>
                                            <br/>
                                            Totaal aantal: 162,862
                                        </span>
                                        <h1 class="m-b-xs">1234</h1>
                                        <h3 class="font-bold no-margins">
                                            Webstatistieken
                                        </h3>
                                        <small>vandortletselschade.nl</small>
                                    </div>

                                <div>
                                    <canvas id="lineChart" height="70"></canvas>
                                </div>

                                <div class="m-t-md">
                                    <small class="pull-right">
                                        <i class="fa fa-clock-o"> </i>
                                        Update op 16.07.2017
                                    </small>
                                   <small>
                                       <strong>Aantal webbezoekers vandortletselschade.nl</strong>
                                   </small>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            <div class="row">

                    <div class="col-sm-4">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-primary pull-right">Vandaag</span>
                                <h5>Bezoekers</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins">64</h1>
                                <div class="stat-percent font-bold text-navy">20% <i class="fa fa-level-up"></i></div>
                                <small>Nieuwe bezoekers</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-info pull-right">Vandaag</span>
                                <h5>Aanmeldingen</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins">11</h1>
                                <div class="stat-percent font-bold text-info">4% <i class="fa fa-level-up"></i></div>
                                <small>Nieuwe aanmeldingen</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-warning pull-right">Maandelijks</span>
                                <h5>Vragen</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins">42</h1>
                                <div class="stat-percent font-bold text-warning">16% <i class="fa fa-level-up"></i></div>
                                <small>Nieuwe vragen</small>
                            </div>
                        </div>
                    </div>
                </div>



<div class="wrapper wrapper-content">
    <div class="row animatedx fadeInRight">
        <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content_xxx" id="ibox-content">

                <div class="row">
                    <div class="col-lg-12">
                        <div class="small pull-left col-md-3 m-l-lg m-t-md">
                            <strong>uw gezondsheidsdossier</strong> van de afgelopen periode.
                        </div>
                        <div class="flot-chart m-b-xl">
                            <div class="flot-chart-content" id="flot-dashboard5-chart"></div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    </div>
</div>
    {literal}
    <script>
        $(document).ready(function() {

            var data1 = [
                [1,8],[2,5],[3,10],[4,4],[5,16],[6,5],[7,11],[8,6],[9,11],[10,20],[11,10],[12,13],[13,4],[14,7],[15,8],[16,12]
            ];
            var data2 = [
                [1,2],[2,7],[3,4],[4,11],[5,4],[6,2],[7,5],[8,11],[9,5],[10,4],[11,1],[12,5],[13,2],[14,5],[15,2],[16,0]
            ];
            $("#flot-dashboard5-chart").length && $.plot($("#flot-dashboard5-chart"), [
                        data1,  data2
                    ],
                    {
                        series: {
                            lines: {
                                show: false,
                                fill: true
                            },
                            splines: {
                                show: true,
                                tension: 0.4,
                                lineWidth: 1,
                                fill: 0.4
                            },
                            points: {
                                radius: 0,
                                show: true
                            },
                            shadowSize: 2
                        },
                        grid: {
                            hoverable: true,
                            clickable: true,

                            borderWidth: 2,
                            color: 'transparent'
                        },
                        colors: ["#1ab394", "#1C84C6"],
                        xaxis:{
                        },
                        yaxis: {
                        },
                        tooltip: false
                    }
            );



            var lineData = {
                labels: ["January", "February", "March", "April", "May", "June", "July"],
                datasets: [
                    {
                        label: "Bezoekers",
                        backgroundColor: "rgba(26,179,148,0.5)",
                        borderColor: "rgba(26,179,148,0.7)",
                        pointBackgroundColor: "rgba(26,179,148,1)",
                        pointBorderColor: "#fff",
                        data: [28, 48, 40, 19, 86, 27, 90]
                    },
                    {
                        label: "Aanmeldingen",
                        backgroundColor: "rgba(220,220,220,0.5)",
                        borderColor: "rgba(220,220,220,1)",
                        pointBackgroundColor: "rgba(220,220,220,1)",
                        pointBorderColor: "#fff",
                        data: [65, 59, 80, 81, 56, 55, 40]
                    }
                ]
            };

            var lineOptions = {
                responsive: true
            };


            var ctx = document.getElementById("lineChart").getContext("2d");
            new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});


        });

    </script>
    {/literal}

<!-- Peity -->
<script src="{$moduleroot}/js/plugins/peity/jquery.peity.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="{$moduleroot}/js/inspinia.js"></script>
<script src="{$moduleroot}/js/plugins/pace/pace.min.js"></script>

<!-- Flot -->
<script src="{$moduleroot}/js/plugins/flot/jquery.flot.js"></script>
<script src="{$moduleroot}/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
<script src="{$moduleroot}/js/plugins/flot/jquery.flot.spline.js"></script>
<script src="{$moduleroot}/js/plugins/flot/jquery.flot.resize.js"></script>
<script src="{$moduleroot}/js/plugins/flot/jquery.flot.pie.js"></script>
<script src="{$moduleroot}/js/plugins/flot/jquery.flot.symbol.js"></script>
<script src="{$moduleroot}/js/plugins/flot/jquery.flot.time.js"></script>
<script src="{$moduleroot}/js/plugins/chartJs/Chart.min.js"></script>
