<?php
    require "header.php";
    require "nav.php";
?>


<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-3">
        <h2>Mijn dossier informatie</h2>
        <ol class="breadcrumb">
            <li>
                <a href="index.php">Home</a>
            </li>
            <li class="active">
                <strong>Dossier</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-9">



            <div class="row bs-wizard" style="border-bottom:0;">

                <div class="col-xs-3 bs-wizard-step complete">
                  <div class="text-center bs-wizard-stepnum">Intake</div>
                  <div class="progress"><div class="progress-bar"></div></div>
                  <a href="#" class="bs-wizard-dot"></a>
                  <div class="bs-wizard-info text-center">inventarisatie zaak, vastleggen gemaakte afspraken.</div>
                </div>

                <div class="col-xs-3 bs-wizard-step disabled"><!-- complete -->
                  <div class="text-center bs-wizard-stepnum">Informatie</div>
                  <div class="progress"><div class="progress-bar"></div></div>
                  <a href="#" class="bs-wizard-dot"></a>
                  <div class="bs-wizard-info text-center">Verzamelen informatie toedracht, schadeposten en benadeelden.</div>
                </div>

                <div class="col-xs-3 bs-wizard-step disabled"><!-- complete -->
                  <div class="text-center bs-wizard-stepnum">Aansprakelijkheid</div>
                  <div class="progress"><div class="progress-bar"></div></div>
                  <a href="#" class="bs-wizard-dot"></a>
                  <div class="bs-wizard-info text-center">Vaststellen aansprakelijkheid, verzenden documenten. </div>
                </div>

                <div class="col-xs-3 bs-wizard-step disabled"><!-- active -->
                  <div class="text-center bs-wizard-stepnum">Schade regelen</div>
                  <div class="progress"><div class="progress-bar"></div></div>
                  <a href="#" class="bs-wizard-dot"></a>
                  <div class="bs-wizard-info text-center">Berekenen omvang schade, overleg met alle partijen.</div>
                </div>
            </div>



    </div>
</div>

<div class="wrapper wrapper-content">
    <div class="row animated fadeInRight">
        <div class="col-lg-12">






        <div class="ibox float-e-margins">
            <div class="ibox-content_xxx" id="ibox-content">

                <button class="btn btn-default btn-rounded" type="button" data-toggle="modal" data-target="#myModal2"><i class="fa fa-comment"></i>&nbsp; <span>Stel vraag</span></button>
                <button class="btn btn-default btn-rounded" type="button" data-toggle="modal" data-target="#myModal3"><i class="fa fa-file"></i>&nbsp; <span>Stuur document</span></button>

                <div id="vertical-timeline" class="vertical-container light-timeline center-orientation">


                    <div class="vertical-timeline-block">
                        <div class="vertical-timeline-icon navy-bg">
                            <img src="images/vandortletselschade_logo_wit.png" style="width:23px;padding-top:8px;" />
                        </div>

                        <div class="vertical-timeline-content">
                            <img src="img/med2_1_circle.jpg" class="img-circle pull-right" alt="profile" style="width:50px;">
                            <h2>Welkom Mevr. Janssen</h2>
                            <p>Van harte welkom bij <b>Van Dort Letselschade</b>.<br/>
                            In deze beveiligde persoonlijke omgeving heeft u altijd inzicht in de status van uw dossier.
                            Ook kunt u hier vragen stellen of documenten opsturen naar ons kantoor.<br/>
                            Heeft u vragen? U kunt ons altijd bereiken op 012-34567890.<br/>
                            <span class="pull-right">– Raoul van Dort</span></p>
                            <span class="vertical-date">Vandaag <br/><small>1 nov 2016</small></span>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    </div>
</div>



<div class="modal inmodal" id="myModal2" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated flipInY">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Stel een vraag</h4>
                <small class="font-bold">Wat wilt u ons vragen?</small>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group"><label>Bericht</label> <textarea rows="10" placeholder="Typ hier uw tekst" class="form-control"></textarea></div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Sluit</button>
                <button type="button" class="btn btn-primary">Verstuur</button>
            </div>

        </div>
    </div>
</div>

<div class="modal inmodal" id="myModal3" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated flipInY">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Stuur een document</h4>
            </div>
            <div class="modal-body">

            <form action="#" class="dropzone dz-clickable" id="dropzoneForm">

            <div class="dz-default dz-message"><span><strong>Drag &amp; Drop uw bestanden hier of klik om te uploaden. </strong><br> (Dit is een demo. Bestanden worden niet echt geüpload.)</span></div></form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Sluit</button>
                <button type="button" class="btn btn-primary">OK</button>
            </div>


        </div>
    </div>
</div>

<?php
    require "footer.php";
?>

</body>

</html>

