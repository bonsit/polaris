<?php
require "header.php";
require "nav.php";
?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Mijn instellingen</h2>
        <ol class="breadcrumb">
            <li>
                <a href="index.php">Home</a>
            </li>
            <li class="active">
                <strong>Instellingen</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row animated fadeInRight">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content_xxx" id="ibox-content">
                    <div class="row">
                            <div class="col-md-6">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h2 class="panel-title">Accountgegevens</h2>
                                    </div>
                                    <table class="table display"><tbody><tr><th>Klantnummer</th><td id="CustomerId">13474</td></tr><tr><th>Naam</th><td id="CustomerCompanyName">Karin Janssen</td></tr><tr><th>Adres</th><td id="CustomerStreetAddress">Beekerweg 13</td></tr><tr><th>Postcode</th><td id="CustomerZipCode">6235 CA</td></tr><tr><th>Plaats</th><td id="CustomerCity">Ulestraten</td></tr><tr><th>Land</th><td id="CustomerCountry">Nederland</td></tr><tr><th>BSN-nummer</th><td id="CustomerBtwVat">NL855690185B01</td></tr></tbody></table>               <div class="panel-body">
                                    <div class="btn-group pull-right">
                                        <a href="/nl/customers/view" class="btn btn-default">Details</a>                    </div>
                                    </div>
                                </div>
                            </div>
                        <div class="col-md-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h2 class="panel-title">Financieel</h2>
                                </div>
                                <table class="table display"><tbody><tr><th>Factuur periode</th><td id="CustomerBillingPeriod">1 maand</td></tr><tr><th>Volgende factuur</th><td id="CustomerBillingInvoiceNext">17 nov 2016</td></tr><tr><th>Betaaltermijn</th><td id="CustomerTermOfPayment">14 dagen</td></tr><tr><th>Accountbalans</th><td id="CustomerPaymentBalance">€ 0.00</td></tr><tr><th>Betaalwijze</th><td id="CustomerInitialPayment.method">SEPA Automatische incasso</td></tr><tr><th>Account</th><td id="CustomerInitialPayment.account">NL13KNAB0254872700</td></tr></tbody></table>               <div class="panel-body">
                                <div class="btn-group pull-right">
                                    <a href="/nl/billing" class="btn btn-default">Details</a>                   </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h2 class="panel-title">Samenvatting</h2>
                                </div>
                                <table class="table">
                                    <thead><tr><th>Maand</th><th class="text-right">Aantal berichten</th></tr></thead>
                                    <tbody><tr><td>nov 2016</td><td class="text-right">8 </td></tr>
                                    <tr><td>okt 2016</td><td class="text-right">12 </td></tr>
                                    <tr><td>sep 2016</td><td class="text-right">11 </td></tr>
                                </tbody></table>
                                <div class="panel-body">
                                    <div class="btn-group pull-right">
                                        <a href="/nl/customer_io_statistics" class="btn btn-default">Details</a>                    </div>
                                    </div>
                                </div>
                            </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
        require "footer.php";
        ?>
    </body>
</html>