<?php
require('_shared/module._base.php');

class ModuleWvH extends _BaseModule {
    var $stylesheet;

    function __construct($moduleid, $module, $clientname=false) {
        global $_GVARS;
        parent::__construct($moduleid, $module, $clientname);

        $this->stylesheet = 'default.css';
        $this->showControls = true;
        $this->showDefaultButtons = false;
    }

    function ProcessJson() {
        return false;
    }

    function ProcessRequest($_allesRecord) {
        if (isset($_POST['_hdnAction'])) {
            switch ($_POST['_hdnAction']) {
            case '...someaction...':
            break;
            }
        }
    }

    function ShowListView($state, $permission, $detail=false, $masterrecord=false) {
        $_customfilter = $this->__GetCustomFilter();
        $_rs = $this->form->GetViewContent($detail=false, $masterrecord=false, $limit=false, $_customfilter);

        $this->smarty->assign('items', $_rs);
        $this->smarty->assign('stylesheet', $this->stylesheet);
        $this->AddModuleJS(['maxia.js']);

        switch($this->moduleid) {
        case 1:
            echo $this->smarty->Fetch('demo_module.tpl.php');
        break;
        }
    }

    function ShowFormView($state, $permission, $detail=false, $masterrecord=false) {
        $_customfilter = $this->__GetCustomFilter();

        $_gen_form = new base_plrForm($this->form->owner);
        $_gen_form->LoadRecord(array(12, 15));
        $_gen_form->ShowFormView($this->state, $this->permission, $detail=false, $masterrecord=false);
    }

    function Show($outputformat='xhtml', $rec=false) {
        global $_GVARS;

        if ($this->form->outputformat == 'json') {
            $this->ProcessJson();
        } else {
            parent::Show();
            switch($_GET['action']) {
            case '':
                $this->ShowListView($this->state, $this->permission, $detail=false, $masterrecord=false);
            break;
            case 'edit':
            case 'insert':
                $this->ShowFormView($this->state, $this->permission, $detail=false, $masterrecord=false);
            break;
            }
        }
    }

}
