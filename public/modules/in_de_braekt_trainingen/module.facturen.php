<?php
require('_shared/module._base.php');

class Module extends _BaseModule {

    function Module($moduleid, $module) {
        global $_GVARS;
        global $polaris;

        parent::_BaseModule($moduleid, $module);

        $this->processed = false;

        /**
        * What client are we talking about? (nog doen: eventueel selecteerbaar maken)
        */
        $this->clientid = $_SESSION['clientid'];

        $_databaseid = 1;
        /* Load the user database */
        $db = new plrDatabase($polaris);
        $db->LoadRecord(array($this->clientid, $_databaseid));
        $this->userdatabase = $db->connectUserDB();
    }

    function Process() {
        global $_GVARS;
    }

    function MaakFactuur() {
        global $polaris;

        $_sql = "INSERT INTO factuur (factuurnummer, factuurdatum, klant, bedragbruto, btw, notitie, betaaltermijn)
        VALUE (?, ?, ?, ?, ?, ?, ?)";
        $this->userdatabase->Execute($_sql, array());
    }

    function ExecuteProcedure($params, &$error) {
        $validrowids = $this->dbobject->customUrlDecode($_GET['ROWID']);
        if ($_GET['DEBUG'] == 'DEBUG') {
            //            echo '<input type="hidden" id="_fldDONTSHOWMESSAGE" value="true" />';
            echo "<div id='message' style='max-height:300px;overflow:auto;-webkit-user-select: auto !important;-khtml-user-select: auto !important;-moz-user-select: auto !important;-ms-user-select: auto !important;user-select: auto !important;'>";
        }

        switch($this->moduleid) {
        case 'ExecChangeBtvAmountArticleSale':
            $result = $this->ChangeBtvAmountArticleSale($params, $error);
            break;
        }
    }

    function Show($outputformat='xhtml', $rec=false) {
        parent::Show($outputformat, $rec);

        /* When there are parameters and they have no value, then show the parameter form */
        $displayDialogForFirstTime = !isset($_GET['notfirst']);
        if ($_GET['afterprint'] == 'true')
            $displayDialogForFirstTime = false;
        $customparamform = $_GET['customparamform']=='true';

        if ($customparamform) {
            $this->ParamsNeedValue($_GET);
            $paramvalues = $_GET;
        } else {
            $paramvalues = $this->ParamsNeedValue($this->currentaction->record);
        }

        $_executeImmediate = $this->currentaction->record->TITLE == '' and $this->currentaction->record->TEXT == '';
        if (!$_executeImmediate and ($displayDialogForFirstTime or !$paramvalues)) {
            if ($customparamform) {
                $this->DisplayCustomParameterForm($this->moduleid, $_GET);
            } else {
                $this->DisplayParameterForm();
            }
        } else {
            $result = $this->ExecuteProcedure($paramvalues, $error);
            if ($_GET['output'] != 'pdf') {
                $this->DisplayMessage($result, $error);
            }
        }

        $this->MaakFactuur();
    }

}