<?php
require_once('includes/mainactions.inc.php');
require_once('_shared/module._base.php');
require_once('nha/module.nha_macs/php/forms.class.php');

class Module extends _BaseModule {

    function Module($moduleid, $module) {
        global $_GVARS;
        parent::_BaseModule($moduleid, $module);

        $this->processed = false;

        /**
        * What client are we talking about? (nog doen: eventueel selecteerbaar maken)
        */
        $this->clientid = $_SESSION['clientid'];
    }

    function Process() {
        global $_GVARS;
        global $polaris;

        $_plrinstance = $this->form->plrInstance();
        $_plrinstance->SetFetchMode(ADODB_FETCH_ASSOC);
        $this->form->database->userdb->SetFetchMode(ADODB_FETCH_ASSOC);

        $this->form->database->SetOracleUserID($_SESSION['userid']);

        if (isset($_POST['_hdnProcessedByModule'])) {
            $_allesRecord = $_POST;

            $polaris->logUserAction('SQL logging', session_id(), $_SESSION['name'], var_export($_allesRecord, true));

            $forms = new Forms($this->form->database->userdb);

            try {
                switch($_allesRecord['_hdnFORMTYPE']) {
                /***
                * Nieuwe Inschrijvingen Opslaan
                */
                case 'nieuweinschrijving':
                    $result = $forms->BewaarNieuweInschrijvingen($_allesRecord);
                break;

                /***
                * Nieuwe Aanvraag Opslaan
                */
                case 'nieuweaanvraag':
                    $result = $forms->BewaarNieuweAanvraag($_allesRecord);
                break;

                /***
                * Artikel Verkoop Opslaan
                */
                case 'artikelverkoop':
                    $result = $forms->BewaarArtikelVerkoop($_allesRecord);
                break;

                /***
                * Geplande Actie (Telemarketing) Opslaan
                */
                case 'telemarketing':
                    $result = $forms->BewaarGeplandeActie($_allesRecord);
                break;

                /***
                * Betaalwijze Opslaan
                */
                case 'betaalwijzeaanpassen':
                    $result = $forms->BewaarBetaalwijze($_allesRecord);
                break;

                /***
                * Inschrijving Opzeggen
                */
                case 'inschrijvingopzeggen':
                    $result = $forms->BewaarInschrijvingOpzeggen($_allesRecord);
                break;

                /***
                * Inschrijving Termijnen wijzigen
                */
                case 'inschrijvingtermijnenwijzigen':
                    $result = $forms->BewaarInschrijvingTermijnWijzigen($_allesRecord);
                break;

                /***
                * Betalingsverzoeken samenvoegen
                */
                case 'betalingsverzoekensamenvoegen':
                    $result = $forms->BewaarBetalingsverzoekenSamenvoegen($_allesRecord);
                break;

                /***
                * Betalingsregeling
                */
                case 'betalingsregeling':
                    $result = $forms->BewaarBetalingsRegeling($_allesRecord);
                break;

                /***
                * Teveel betaald verwerken
                */
                case 'teveelbetaaldverwerken':
                    $result = $forms->BewaarTeveelBetaaldBedragenVerwerken($_allesRecord);
                break;

                /***
                * Losse factuur aanmaken
                */
                case 'lossefactuuraanmaken':
                    $result = $forms->BewaarLosseFactuurAanmaken($_allesRecord);
                break;

                /***
                * Cursusgegevens aanmaken
                */
                case 'cursusgegevens':
                    $_rsLessen = $this->GetSTTlessen($_allesRecord['CURSUSCODE_STT'][0]);
                    $result = $forms->BewaarCursusGegevens($_allesRecord, $_rsLessen);
                break;
                case 'cohortcursus':
                    $result = $forms->BewaarCohortCursus($_allesRecord);
                break;
                case 'kopieercohortcursus':
                    $result = $forms->KopieerCohortCursus($_allesRecord);
                break;
                case 'cohort':
                    $result = $forms->BewaarCohort($_allesRecord);
                break;
                case 'verwijdercursus':
                    $result = $forms->VerwijderCursus($_allesRecord);
                break;
                case 'verwijdercohort':
                    $result = $forms->VerwijderCursus($_allesRecord);
                break;
                case 'bulkpremiums':
                    $result = $forms->BewaarLeaflets($_allesRecord);
                break;
                /***
                * Nieuwe module aanmaken
                */
                case 'modulegegevens':
                    $result = $forms->BewaarModule($_allesRecord);
                break;
                case 'verwijdermodule':
                    $result = $forms->VerwijderModule($_allesRecord);
                break;
                case 'modulelessen':
                    $result = $forms->BewaarModuleLessen($_allesRecord);
                break;
                }

                $resultArray = Array('result'=>$result, 'query'=>$_SERVER['REQUEST_URI'], 'error'=>false);
                echo json_encode($resultArray);

            } catch (Exception $E) {
                $detailedmessage = $E->completemsg."\r\n".parse_backtrace(debug_backtrace());
                if ($result == null) $result = false;

                $resultArray = Array('result'=>$result, 'query'=>$_SERVER['REQUEST_URI'], 'error'=>$E->msg, 'detailed_error'=>$detailedmessage);
                echo  json_encode($resultArray);
            }
        }
    }

    function IsBackOfficeGroep($usergroupid) {
        return $usergroupid == '555' /* Medewerkers BO */ or $usergroupid == '553' /* Supervisors BO */ or $usergroupid == '789' /* Supervisors BO */;
    }

    function GetAdvertentieCodes() {
        /* Haal de advertentiecodes op, gecached */
        $_sqlAdvertentiecode = "
            SELECT A.Advertentiecode, A.Advertentiecode || ' - '|| S.Advertentiesoortnaam AS Omschrijving
            FROM NHA.Advertentie A, NHA.AdvertentieSoort S
            WHERE  A.Advertentiesoortcode = S.Advertentiesoortcode AND A.AdvertentieCode <> 'OBK' AND (A.datumgeldigvanaf <= TRUNC(sysdate) AND A.GeblokkeerdJaNee = 'N'
            AND (A.datumgeldigtotenmet is null or A.datumgeldigtotenmet >= TRUNC(sysdate)))
            ORDER BY 1
        ";
        // A.Advertentiecode <> 'SYS' AND  <-
        return $this->form->database->userdb->GetAssoc($_sqlAdvertentiecode);
    }

    function GetOntvangstmedia() {
        /* Haal de ontvangstmedia op, gecached */
        $_sqlOntvangstmedium = "SELECT ontvangstmediumcode as Code, omschrijving FROM NHA.l10n_ontvangstmedium WHERE ontvangstmediumcode NOT IN ('I', 'C')";
        return $this->form->database->userdb->GetAssoc($_sqlOntvangstmedium);
    }

    function GetLanden() {
        /* Haal de landcodes op, gecached */
        $_sqlLanden = "SELECT LANDCODE, LANDNAAM FROM NHA.land WHERE INGEBRUIK = 'J' ORDER BY LANDNAAM";
        return $this->form->database->userdb->CacheGetAssoc($_sqlLanden);
    }

    function GetGebruikteLanden() {
        /* Haal de landcodes op, gecached */
        $_sqlLanden = "SELECT LANDCODE, LANDNAAM FROM NHA.landgebruikt";
        $_rs = $this->form->database->userdb->CacheGetAssoc($_sqlLanden);
        $_rs[""] = "";
        return $_rs;
    }

    function GetEigenLand() {
        return $_COOKIE['Macs_Dataentry_landselectie'];
    }

    function DisplayNieuweAanvraag() {
        $GLOBALS['includeGreyBox'] = true;

        if ($this->outputformat == 'xhtml') {
            $this->showControls = false;
            if ($this->IsBackOfficeGroep($_SESSION['usergroup'])) {
                $_cursussource = 'cursusplusstudiegids_backoffice';
            } else {
                $_cursussource = 'cursusplusstudiegids';
            }
            $this->smarty->assign('cursussource', $_cursussource);
            $this->smarty->assign('advertentiecodes', $this->GetAdvertentieCodes());
            $this->smarty->assign('ontvangstmedia', $this->GetOntvangstmedia());
            $this->smarty->assign('landselectie', !$this->form->owner->HasParameter('geenlandselectie'));
            $this->smarty->assign('landen', $this->GetLanden());
            $this->smarty->assign('eigenland', $this->GetEigenLand());
            $this->smarty->display("nieuwe_aanvraag.tpl.php");
        }
    }

    function DisplayNieuweInschrijving() {
        if ($this->outputformat == 'xhtml') {
            $this->showControls = false;
            if ($this->IsBackOfficeGroep($_SESSION['usergroup'])) {
                $_cursussource = 'cursus_selectie_backoffice';
            } else {
                $_cursussource = 'cursus_selectie';
            }
            $this->smarty->assign('isbogroep', $this->IsBackOfficeGroep($_SESSION['usergroup']) ? 'J':'N');
            $this->smarty->assign('cursussource', $_cursussource);
            $this->smarty->assign('advertentiecodes', $this->GetAdvertentieCodes());
            $this->smarty->assign('ontvangstmedia', $this->GetOntvangstmedia());
            $this->smarty->assign('landselectie', !$this->form->owner->HasParameter('geenlandselectie'));

            $this->smarty->assign('landen', $this->GetLanden());
            $this->smarty->assign('eigenland', $this->GetEigenLand());
            $this->smarty->display("nieuwe_inschrijving.tpl.php");
        }
    }

    function GetJsonArtikelGegevens($artikelcode, $cursistnr=false) {
        $_sqlVerzendArtikelen = "
        SELECT ARTIKELCODE, OMSCHRIJVING, VOORRAAD, NVL(BEDRAGINEUROS,0) AS BEDRAGINEUROS, NVL(BEDRAGINEUROSLOS,0) AS BEDRAGINEUROSLOS, NVL(VERWIJDERINGSBIJDRAGE,0), VERZENDKOSTEN, DISPLAYCODE
        FROM ARTIKEL V WHERE V.ARTIKELCODE = :artikelcode AND INARTIKELVERKOOPJANEE = 'J'
        ";

        $_artikel = $this->form->database->userdb->GetRow($_sqlVerzendArtikelen, array('artikelcode'=>$artikelcode));
        $_artikel = Array('artikelcode'=>$_artikel['ARTIKELCODE'], 'omschrijving'=> $_artikel['OMSCHRIJVING']
            , 'bedragineuros'=>$_artikel['BEDRAGINEUROS'], 'bedragineuroslos'=>$_artikel['BEDRAGINEUROSLOS']
            , 'displaycode'=>$_artikel['DISPLAYCODE']
            , 'verwijderingsbijdrage'=>$_artikel['VERWIJDERINGSBIJDRAGE'], 'verzendkosten'=>$_artikel['VERZENDKOSTEN']
        );

        $_cursussen = Array();
        if ($cursistnr) {
            $_sqlCursusVanStudentVanArtikel = "
            SELECT c.CURSUSCODE, c.CURSUSNAAM
            FROM cursusartikelverkoop cav, inschrijving i, cursus c
            WHERE
              cav.artikelcode = :artikelcode AND
              i.cursuscode = cav.cursuscode AND
              cav.cursuscode = c.cursuscode AND
              i.cursistnr = :cursistnr
            GROUP BY c.cursuscode, c.cursusnaam
            ";

            $_rs = $this->form->database->userdb->GetAll($_sqlCursusVanStudentVanArtikel, array('artikelcode'=>$artikelcode, 'cursistnr'=>$cursistnr));
            if ($_rs) {
                foreach($_rs as $_record) {
                    $_cursussen[] = Array('cursuscode'=>$_record['CURSUSCODE'], 'cursusnaam'=>$_record['CURSUSNAAM']);
                }
            }
        }

        // return the result as JSON
        $result = array('artikel' => $_artikel, 'cursussen' => $_cursussen);
        return json_encode($result);
    }

    function GetJsonPersoonlijkeInschrijvingen($medewerkernr) {
        $_sqlAantalInschrijvingen = "
        SELECT COUNT(*)  AANTAL
        FROM NHA.Inschrijving
        WHERE OntvangstMedium = 'TM'
        AND DatumInschrijving = TRUNC(SysDate)
        AND MedewerkerNr = :medewerkernr
        ";

        $_aantal = $this->form->database->userdb->GetOne($_sqlAantalInschrijvingen, array('medewerkernr'=>$medewerkernr));

        // return the result as JSON
        return json_encode(array('aantal'=>$_aantal, 'medewerkernr'=>$medewerkernr));
    }

    function RuimOpActiesInBehandeling() {
        // zet alle acties die al langer dan 30 minuten in behandeling zijn en nog niet zijn verwerkt
        $_sqlUpdateStatus = "
            update geplandeactie set inbehandeling = null, inbehandelingdoor = null
            where inbehandeling is not null and InBehandelingDoor is not null
            and GEREALISEERDEDATUM IS NULL
            and trunc((86400*(sysdate-inbehandeling))/60) > 30
        ";
        $_rs = $this->form->database->userdb->Execute($_sqlUpdateStatus);
    }

    function GetJsonAutoSelectie($medewerkernr) {
        $rs = $this->form->GetViewContent($detail=true, $masterrecord=false, $limit=false, $customfilter = 'INBEHANDELING IS NULL');
        $actienr = $this->BepaalFocus($rs->GetAll(), $medewerkernr, $returnactienr=true);

        // return the result as JSON
        return json_encode(array('automatische_selectie'=>$actienr, 'medewerkernr'=>$medewerkernr));
    }

    function GetJsonOorzaakGeenInteresse() {
        $_sql = "SELECT OORZAAKGEENINTERESSE_ORI, OORZAAKGEENINTERESSE FROM NHA.l10n_OORZAAKGEENINTERESSE";

        $_rs = $this->form->database->userdb->GetAll($_sql);
        // return the result as JSON
        return json_encode($_rs);
    }

    function GeplandeActieOnderbehandeling($actienr) {
        $_sqlUpdateStatus = "
            UPDATE NHA.GeplandeActie
            SET InBehandeling = sysdate, InBehandelingDoor = :medewerkernr
            WHERE Actienr = :actienr
        ";
        $medewerkernr = $_SESSION['userid'];
        $_rs = $this->form->database->userdb->Execute($_sqlUpdateStatus, array("actienr"=>$actienr,"medewerkernr"=>$medewerkernr));
    }

    function GeplandeActieNIETOnderbehandeling($actienr) {
        $_sqlUpdateStatus = "
            UPDATE NHA.GeplandeActie
            SET InBehandeling = null, InBehandelingDoor = null
            WHERE Actienr = :actienr
        ";
        $_rs = $this->form->database->userdb->Execute($_sqlUpdateStatus, array("actienr"=>$actienr));

        // return the result as JSON
        return json_encode(array('result'=>true));
    }

    function GetJsonTelemarketingInfo($actienr, $relatienr, $oorzaaknr) {
        global $polaris;

        $_sqlInbehandeling = "
        SELECT InBehandeling, InBehandelingDoor, Fullname AS Medewerkernaam
        FROM NHA.GeplandeActie G, NHA.PLR_USERGROUP U
        WHERE G.InBehandelingDoor = U.usergroupid and actienr = :actienr
        ";

        $_geplandeactie = $this->form->database->userdb->GetRow($_sqlInbehandeling, array("actienr"=>$actienr));
        $_inbehandeling[] = Array('inbehandeling' => count($_geplandeactie)==0?'NEE': 'JA', 'inbehandelingdoor' => $_geplandeactie['MEDEWERKERNAAM']?$_geplandeactie['MEDEWERKERNAAM']:'', 'medewerkernr'=>$_geplandeactie['INBEHANDELINGDOOR']?$_geplandeactie['INBEHANDELINGDOOR']:'');

        $_sqlActieHistorie = "
            SELECT ACTIENR, GEREALISEERDEDATUM, ACTIEOMSCHRIJVING, OUDACTIENR
            FROM NHA.GeplandeActie
            WHERE ActieNr <> :actienr
            START WITH ActieNr = :actienr
            CONNECT BY PRIOR OudActieNr = ActieNr
            ORDER BY ActieNr DESC
        ";

        // markeer de actie als zijnde 'onder behandeling' met het huidige tijdstip, indien niet onder behandeling
        $polaris->logUserAction('TM logging', session_id(), $_SESSION['name'], var_export($_geplandeactie, true));

        if (count($_geplandeactie)==0)
            $this->GeplandeActieOnderbehandeling($actienr);

        $_actiehistorie = Array();
        $_result = '';
        $_rs = $this->form->database->userdb->Execute("ALTER SESSION SET NLS_DATE_FORMAT = 'RRRR-MM-DD HH24:MI'");
        $_rs = $this->form->database->userdb->GetAll($_sqlActieHistorie, array("actienr"=>$actienr));
        if ($_rs) {
            foreach($_rs as $_record) {
                $_actiehistorie[] = Array('actienr'=>$_record['ACTIENR'], 'gerealiseerdedatum'=>$_record['GEREALISEERDEDATUM'], 'actieomschrijving'=>$_record['ACTIEOMSCHRIJVING']);
            }
        }
        $_rs = $this->form->database->userdb->Execute("ALTER SESSION SET NLS_DATE_FORMAT = 'YYYY-MM-DD'");
        $_sqlOpmerkingen = "
            SELECT R.RowID, R.INSCHRIJFNR, R.DATUM, R.OPMERKING, R.CURSUSCODE
            FROM Opmerking R
            WHERE R.RELATIENR = :relatienr
            ORDER BY R.DATUM DESC
        ";

        $_opmerkingen = Array();
        $_result = '';
        $_rs = $this->form->database->userdb->GetAll($_sqlOpmerkingen, array("relatienr"=>$relatienr));

        if ($_rs) {
            foreach($_rs as $_record) {
                $_opmerkingen[] = Array('inschrijfnr'=>$_record['INSCHRIJFNR'], 'datum'=>$_record['DATUM'], 'opmerking'=>$_record['OPMERKING'], 'cursuscode'=>$_record['CURSUSCODE']);
            }
        }

        $_sqlAanvraagCursusInfo = "
            SELECT DISTINCT
              C.CURSUSSTUDIEGIDSCODE,
              C.CURSUSNAAM,
              C.ACADEMIEOFPW,
              C.STUDIEGIDSJANEE,
              C.BELLENNAGEENREACTIEJANEE,
              V.DATUMTIJD
            FROM Verzoek V, VerzoekCursusInfo VCI, CursusEnStudieGids C
            WHERE V.Verzoeknr = VCI.Verzoeknr
            AND VCI.VerzoekNr IN
            (
            SELECT GA.OorzaakNr
            FROM GeplandeActie GA
            WHERE GA.GerealiseerdeDatum IS NULL
            AND GA.ActieSoort = 'B'
            AND GA.OorzaakSoort = 'V'
            AND GA.RelatieNr = :relatienr
            AND GA.OorzaakNr = :oorzaaknr
            )
            AND C.CursusStudieGidsCode = VCI.CursusStudieGidsCode
        ";

        $_aanvraagcursisinfo = Array();
        $_rs = $this->form->database->userdb->GetAll($_sqlAanvraagCursusInfo, array("relatienr"=>$relatienr, "oorzaaknr"=>$oorzaaknr));
        if ($_rs) {
            foreach($_rs as $_record) {
                $_aanvraagcursisinfo[] = Array('cursusstudiegidscode'=>$_record['CURSUSSTUDIEGIDSCODE'], 'cursusnaam'=>$_record['CURSUSNAAM'], 'academieofpw'=>$_record['ACADEMIEOFPW'], 'studiegids'=>$_record['STUDIEGIDSJANEE'], 'bellennageenreactie'=>$_record['BELLENNAGEENREACTIEJANEE'], 'datumverzoek'=>$_record['DATUMTIJD']);
            }
        }

        // return the result as JSON
        return json_encode(array('actiehistorie'=>$_actiehistorie, 'aanvraagcursusinfo'=>$_aanvraagcursisinfo, 'opmerkingen'=>$_opmerkingen, 'inbehandeling'=>$_inbehandeling));
    }

    function GetJsonBetaalwijze($landcode) {
        if ($landcode == 'DE') {
            $_sqlBetaalWijze = "
            SELECT DISTINCT BETAALWIJZECODE
            , DECODE(BT.BETAALWIJZECODE, 'AUT', BT.OMSCHRIJVING, BT.OMSCHRIJVING) AS OMSCHRIJVING
            FROM BETAALWIJZE BT
            WHERE BT.TEGEBRUIKBIJINSCHRJANEE = 'J'
            ";
        } else {
            $_sqlBetaalWijze = "
            SELECT DISTINCT DECODE(BT.BETAALWIJZECODE, 'AUT', 'AUT_'||AD.AUTOBETAALDAG, 'ACP', 'ACP', 'AEL', 'AEL') AS BETAALWIJZECODE
            , DECODE(BT.BETAALWIJZECODE, 'AUT', BT.OMSCHRIJVING||' op '||AD.AUTOBETAALDAG||'e', BT.OMSCHRIJVING) AS OMSCHRIJVING
            FROM BETAALWIJZE BT
            , AUTOMATISCHEBETAALDAG AD
            WHERE BT.TEGEBRUIKBIJINSCHRJANEE = 'J'
            ";
        }

        $_betaalwijze = Array();
        $_result = '';
        $_rs = $this->form->database->userdb->GetAll($_sqlBetaalWijze);
        if ($_rs) {
            foreach($_rs as $_record) {
                $_betaalwijze[] = Array($_record['BETAALWIJZECODE'], $_record['OMSCHRIJVING']);
            }
            $_betaalwijze = arrayUnique($_betaalwijze);
            sort($_betaalwijze);
            $_result = $_betaalwijze;
        }
        // return the result as JSON
        return json_encode($_result);
    }

    function GetJsonCheckSpamPostcode($postcode) {
        $_sqlCheckSpam = "
        SELECT POSTCODE FROM ISM.ISMSPAMPOSTCODE
        WHERE POSTCODE = :POSTCODE
        ";
        $_rs = $this->form->database->userdb->GetOne($_sqlCheckSpam, array('POSTCODE' => $postcode));
        $_result = isset($_rs);
        return json_encode($_result);
    }

    function GetCursusGegevens($cursuscode) {
        $_result = '';

        $_sqlCursus = "
            SELECT C.CURSUSGROEP, C.MBOHBONIVEAU, G.CURSUSSENBLOKKERENJANEE, C.INSCHRIJVINGENBLOKKERENJANEE
            , C.MATERIAALCOMPLEETJANEE, C.MATERIAALVERWACHTDATUM
            FROM NHA.Cursus C, NHA.CursusGroep G
            WHERE C.Cursusgroep = G.Cursusgroepcode AND C.CursusCode = :cursuscode
        ";

        $_cursus = $this->form->database->userdb->GetRow($_sqlCursus, array('cursuscode'=>$cursuscode));

        $_sqlStudieBetaalTermijnen = "
            SELECT DISTINCT CSB.StudieTempoCode, CSB.AantalBetaalTermijnen
            FROM NHA.CursusStudieTempBetaalTer CSB
            WHERE CSB.CursusCode = :cursuscode
        ";

        $_studietempobetaaltermijnen = Array();
        $_rs = $this->form->database->userdb->GetAll($_sqlStudieBetaalTermijnen, array('cursuscode'=>$cursuscode));
        if ($_rs) {
            foreach($_rs as $_record) {
                $_studietempobetaaltermijnen[] = Array('studietempocode'=>$_record['STUDIETEMPOCODE'], 'aantalbetaaltermijnen'=>$_record['AANTALBETAALTERMIJNEN']);
            }
        }

        $_sqlStudieTempoTermijnen = "
        SELECT DISTINCT MIN(CST.STUDIETEMPOCODE) AS STUDIETEMPOCODE, MIN(ST.STUDIETEMPONAAM) AS STUDIETEMPONAAM
        FROM CURSUSSTUDIETEMPOTERMIJN CST, L10N_STUDIETEMPO ST
        WHERE CST.STUDIETEMPOCODE = ST.STUDIETEMPOCODE
		AND CST.CURSUSCODE = :cursuscode
        GROUP BY CST.CURSUSCODE, CST.STUDIETEMPOCODE, CST.TERMIJNNR
        ";

        $_studietempo = Array();
        $_rs = $this->form->database->userdb->GetAll($_sqlStudieTempoTermijnen, array('cursuscode'=>$cursuscode));
        if ($_rs) {
            foreach($_rs as $_record) {
                $_studietempo[] = Array($_record['STUDIETEMPOCODE'], $_record['STUDIETEMPONAAM']);
            }
            $_studietempo = arrayUnique($_studietempo);
            sort($_studietempo);
        }

        $_sqlGratisVerzendOnderdelen = "SELECT C.VERZENDONDERDEELGROEPNAAM, V.VERZENDONDERDEELCODE, O.KORTEOMSCHRIJVING, C.VERPLICHTJANEE
        FROM CURSUSGRATISVERZENDONDERDEEL C, VERZENDONDERDEELGROEPONDERDEEL V, VERZENDONDERDEEL O
        WHERE C.VERZENDONDERDEELGROEPNAAM = V.VERZENDONDERDEELGROEPNAAM
        AND V.VERZENDONDERDEELCODE = O.VERZENDONDERDEELCODE AND CURSUSCODE = :cursuscode
        ORDER BY C.VERZENDONDERDEELGROEPNAAM, C.DISPLAYVOLGORDE
        ";

        $_gratisonderdelen = Array();
        $_rs = $this->form->database->userdb->GetAll($_sqlGratisVerzendOnderdelen, array('cursuscode'=>$cursuscode));
        if ($_rs) {
            foreach($_rs as $_record) {
                $_gratisonderdelen[$_record['VERZENDONDERDEELGROEPNAAM']][] = Array($_record['VERZENDONDERDEELCODE'], $_record['KORTEOMSCHRIJVING'], $_record['VERPLICHTJANEE']);
            }
        }

        $_sqlArtikelen = "SELECT A.ARTIKELCODE, A.OMSCHRIJVING, A.VOORRAAD, A.BEDRAGINEUROS, A.BEDRAGINEUROSLOS, A.VERWIJDERINGSBIJDRAGE,A.VERZENDKOSTEN
        FROM CURSUSARTIKELVERKOOP V, ARTIKEL A WHERE V.ARTIKELCODE = A.ARTIKELCODE AND V.CURSUSCODE = :cursuscode
        ";

        $_artikelen = Array();
        $_rs = $this->form->database->userdb->GetAll($_sqlArtikelen, array('cursuscode'=>$cursuscode));
        if ($_rs) {
            foreach($_rs as $_record) {
                $_artikelen[] = Array('artikelcode'=>$_record['ARTIKELCODE'], 'omschrijving'=>$_record['OMSCHRIJVING']
                    , 'bedragineuros'=>$_record['BEDRAGINEUROS'], 'bedragineuroslos'=>$_record['BEDRAGINEUROSLOS']
                    , 'verwijderingsbijdrage'=>$_record['VERWIJDERINGSBIJDRAGE'], 'verzendkosten'=>$_record['VERZENDKOSTEN']
                );
            }
        }

        $_sqlCursusLesVoorraad = "SELECT T.CursusCode, T.StudieTempoCode, T.LesNr, V.Voorraad
        FROM NHA.CursusStudieTempoTermijn T, NHA.CursusLes L, NHA.CursusSealPakketVoorraad V
        WHERE T.CursusCode = :cursuscode
        AND L.CursusCode = T.CursusCode AND L.CursusLesNr = T.LesNr AND T.TermijnNr = 1 AND V.CursusCode = T.CursusCode
        AND V.SealPakketNr = L.SealPakketNr
        ORDER BY T.StudieTempoCode, T.LesNr
        ";

        $_lesvoorraad = Array();
        $_rs = $this->form->database->userdb->GetAll($_sqlCursusLesVoorraad, array('cursuscode'=>$cursuscode));
        if ($_rs) {
            foreach($_rs as $_record) {
                $_lesvoorraad[$_record['STUDIETEMPOCODE']][] = Array($_record['LESNR'], $_record['VOORRAAD']);
            }
        }

        $_result = array_merge(
            Array('cursuscode' => $cursuscode),
            Array('cursusgroep' => $_cursus['CURSUSGROEP']),
            Array('mbohboniveau' => $_cursus['MBOHBONIVEAU']),
            Array('cursussenblokkeren' => $_cursus['CURSUSSENBLOKKERENJANEE']),
            Array('inschrijvingenblokkeren' => $_cursus['INSCHRIJVINGENBLOKKERENJANEE']),
            Array('materiaalcompleet' => $_cursus['MATERIAALCOMPLEETJANEE']),
            Array('materiaalverwachtdatum' => $_cursus['MATERIAALVERWACHTDATUM']),
            Array('studietempobetaaltermijnen' => $_studietempobetaaltermijnen),
            Array('studietempos' => $_studietempo),
            Array('gratisonderdelen' => $_gratisonderdelen),
            Array('artikelen' => $_artikelen),
            Array('lesvoorraad' => $_lesvoorraad)
        );

        return $_result;
    }

    function GetJsonCursusGegevens($cursuscode) {
        $_result = $this->GetCursusGegevens($cursuscode);
        // return the result as JSON
        return json_encode($_result);
    }

    function GetBetaalwijzen() {
        $_sql = "
        SELECT BETAALWIJZECODE, OMSCHRIJVING, TEGEBRUIKBIJINSCHRJANEE
        FROM l10n_BETAALWIJZE
        WHERE TEGEBRUIKBIJINSCHRJANEE = 'J'
        ";
        $_rs = $this->form->database->userdb->GetAll($_sql);
        return $_rs;
    }

    function DisplayArtikelVerkoop() {
        if ($this->outputformat == 'xhtml') {
            $this->showControls = false;
            $this->smarty->assign('landen', $this->GetGebruikteLanden());
            $this->smarty->assign('eigenland', $this->GetEigenLand());
            $this->smarty->display("artikel_verkoop.tpl.php");
        }
    }

    function MoetNuVerwerktWorden($begintijd, $eindtijd) {
        return (
            $begintijd != ''
            and (strnatcmp($begintijd, date('H:i')) <= 0)
            and (!isset($eindtijd) or (isset($eindtijd) and strnatcmp($eindtijd, date('H:i')) >= 0))
        );
    }

    function BepaalFocus($data, $medewerker, $returnactienr=false) {
        $found = $actienr = false;
        // Eerst de dringende acties van medewerker.
        foreach($data as $k => $item) {
            if ($item['GEPLANDEMEDEWERKER'] == $medewerker and $item['PERIODE'] >= 2 ) {
                if ($item['PERIODE'] == 3 and $this->MoetNuVerwerktWorden($item['BEGINTIJD'], $item['EINDTIJD'])
                ) {
                    $found = true;
                    $data[$k]['FOCUS'] = true;
                    $actienr = $item['ACTIENR'];
                    break;
                }

                if ($item['PERIODE'] == 2) {
                    $found = true;
                    $data[$k]['FOCUS'] = true;
                    $actienr = $item['ACTIENR'];
                    break;
                }
            }
        }

        // Dan de dringende, niet-persoonlijke.
        if (!$found)
        foreach($data as $k => $item) {
            if ($item['PERIODE'] >= 2 ) {

                if ($item['PERIODE'] == 3 and $this->MoetNuVerwerktWorden($item['BEGINTIJD'], $item['EINDTIJD'])
                ) {
                    $found = true;
                    $data[$k]['FOCUS'] = true;
                    $actienr = $item['ACTIENR'];
                    break;
                }

                if ($item['PERIODE'] == 2) {
                    $data[$k]['FOCUS'] = true;
                    $found = true;
                    $actienr = $item['ACTIENR'];
                    break;
                }
            }
        }

        // Vervolgens niet dringende acties van medewerker.
        if (!$found)
        foreach($data as $k => $item) {
            if ($item['GEPLANDEMEDEWERKER'] == $medewerker and $item['PERIODE'] != 3) {
                $data[$k]['FOCUS'] = true;
                $found = true;
                $actienr = $item['ACTIENR'];
                break;
            }
        }

        // Dan de niet-dringende, niet-persoonlijke.
        if (!$found)
        foreach($data as $k => $item) {
            if ($item['GEPLANDEMEDEWERKER'] != 491 and $item['PERIODE'] != 3 ) {
                $data[$k]['FOCUS'] = true;
                $found = true;
                $actienr = $item['ACTIENR'];
                break;
            }
        }

        // ...en niet actueel. Maak de eerste item actief
        if (!$found) {
            $data[0]['FOCUS'] = true;
            $found = true;
            $actienr = $data[0]['ACTIENR'];
        }

        if ($returnactienr)
            return $actienr;
        else
            return $data;
    }

    function DisplayTelemarketing() {
        if ($this->outputformat == 'xhtml') {
            $this->showControls = false;
            $this->showDefaultButtons = false;
            $this->smarty->assign('landen', $this->GetLanden());
            $this->smarty->display("telemarketing.tpl.php");
        } elseif ($this->outputformat == 'json') {
            $rs = $this->form->GetViewContent($detail=true, $masterrecord=false, $limit=$_GET['limit']?$_GET['limit']:25);
            $rs = $rs->GetAll();
            // return the result as JSON
            echo json_encode($rs);
        }
    }

    function DisplayBetaalwijzeAanpassen() {
        if ($this->outputformat == 'xhtml') {
            $this->showControls = false;
            $this->showDefaultButtons = false;
            $this->smarty->assign('landen', $this->GetGebruikteLanden());
            $this->smarty->assign('betaalwijze', $this->GetBetaalwijzen());
            $this->smarty->display("betaalwijzeaanpassen.tpl.php");
        }
    }

    function DisplayInschrijvingOpzeggen() {
        if ($this->outputformat == 'xhtml') {
            $this->showControls = false;
            $this->showDefaultButtons = false;
            $this->smarty->assign('landen', $this->GetGebruikteLanden());
            $this->smarty->assign('redenopzegging', $this->GetRedenOpzegging());
            $this->smarty->display("inschrijving_opzeggen.tpl.php");
        }
    }

    function GetRedenOpzegging() {
        $_sql = "
        SELECT REDENOPZEGGING
        FROM l10n_REDENOPZEGGING
        ";
        //$this->form->database->userdb->debug=true;
        $_rs = $this->form->database->userdb->GetAll($_sql);
        return $_rs;
    }

    function GetOpzeggingInfo($inschrijfnr) {
        $db = $this->form->database->userdb;
        $stmt = $db->PrepareSP("BEGIN NHA.PackOpzegging.GeefOpzeggingsInfo(
              :inschrijfnr
            , :datum
            , :huidigAantalLessen
            , :huidigAantalVerzLessen
            , :huidigAantalTeVerzLessen
            , :huidigTotaalBedrag
            , :huidigBedragBetaald
            , :huidigBedragTeBetalen
            , :opzegAantalLessen
            , :opzegAantalVerzLessen
            , :opzegAantalTeVerzLessen
            , :opzegTotaalBedrag
            , :opzegBedragBetaald
            , :opzegBedragTeBetalen
            , :verschilBedrag
            , :hoogsteBedrag
            );
        END;");
        $db->InParameter($stmt,$inschrijfnr,'inschrijfnr');
        $db->InParameter($stmt,date('d-m-Y'),'datum');
        $db->OutParameter($stmt,$huidigAantalLessen,'huidigAantalLessen');
        $db->OutParameter($stmt,$huidigAantalVerzLessen,'huidigAantalVerzLessen');
        $db->OutParameter($stmt,$huidigAantalTeVerzLessen,'huidigAantalTeVerzLessen');
        $db->OutParameter($stmt,$huidigTotaalBedrag,'huidigTotaalBedrag');
        $db->OutParameter($stmt,$huidigBedragBetaald,'huidigBedragBetaald');
        $db->OutParameter($stmt,$huidigBedragTeBetalen,'huidigBedragTeBetalen');
        $db->OutParameter($stmt,$opzegAantalLessen,'opzegAantalLessen');
        $db->OutParameter($stmt,$opzegAantalVerzLessen,'opzegAantalVerzLessen');
        $db->OutParameter($stmt,$opzegAantalTeVerzLessen,'opzegAantalTeVerzLessen');
        $db->OutParameter($stmt,$opzegTotaalBedrag,'opzegTotaalBedrag');
        $db->OutParameter($stmt,$opzegBedragBetaald,'opzegBedragBetaald');
        $db->OutParameter($stmt,$opzegBedragTeBetalen,'opzegBedragTeBetalen');
        $db->OutParameter($stmt,$verschilBedrag,'verschilBedrag');
        $db->OutParameter($stmt,$hoogsteBedrag,'hoogsteBedrag');

        $ok = $db->Execute($stmt);

        if ($ok) {
            $result = array(
            'huidigAantalLessen' => $huidigAantalLessen,
            'huidigAantalVerzLessen' => $huidigAantalVerzLessen,
            'huidigAantalTeVerzLessen' => $huidigAantalTeVerzLessen,
            'huidigTotaalBedrag' => money_format( '%!n', $huidigTotaalBedrag),
            'huidigBedragBetaald' => money_format( '%!n', $huidigBedragBetaald),
            'huidigBedragTeBetalen' => money_format( '%!n', $huidigBedragTeBetalen),
            'opzegAantalLessen' => $opzegAantalLessen,
            'opzegAantalVerzLessen' => $opzegAantalVerzLessen,
            'opzegAantalTeVerzLessen' => $opzegAantalTeVerzLessen,
            'opzegTotaalBedrag' => money_format( '%!n', $opzegTotaalBedrag),
            'opzegBedragBetaald' => money_format( '%!n', $opzegBedragBetaald),
            'opzegBedragTeBetalen' => money_format( '%!n', $opzegBedragTeBetalen),
            'verschilBedrag' => money_format( '%!n', $verschilBedrag),
            'hoogsteBedrag' => money_format( '%!n', $hoogsteBedrag),
            );
        } else {
            $result = false;
        }

        // return the result as JSON
        echo json_encode($result);
    }

    function DisplayInschrijvingTermijnenWijzigen() {
        if ($this->outputformat == 'xhtml') {
            $this->showControls = false;
            $this->showDefaultButtons = false;
            $this->smarty->assign('landen', $this->GetGebruikteLanden());
            $this->smarty->display("inschrijving_termijnen_wijzigen.tpl.php");
        }
    }

    function GetStudieBetaalInfo($inschrijfnr, $zelfstudie) {
        $_sqlZelfStudie = "
            SELECT DISTINCT ZelfStudieJaNee
            FROM NHA.InschrijvingBetaalTempoTermijn
            WHERE InschrijfNr = :inschrijfnr
        ";

        $_zelfstudie = Array();
        $_rs = $this->form->database->userdb->GetAll($_sqlZelfStudie, array('inschrijfnr'=>$inschrijfnr));
        if ($_rs) {
            foreach($_rs as $_record) {
                $_zelfstudie[] = Array('zelfstudie'=>$_record['ZELFSTUDIEJANEE']);
            }
        }

        $_sqlStudieTempos = "
            SELECT StudieTempoCode, StudieTempoNaam
            FROM NHA.StudieTempo
            WHERE StudieTempoCode IN
            (SELECT DISTINCT StudieTempoCode
            FROM NHA.InschrijvingStudieTempoTermijn
            WHERE InschrijfNr = :inschrijfnr)
        ";

        $_studietempos = Array();
        $_rs = $this->form->database->userdb->GetAll($_sqlStudieTempos, array('inschrijfnr'=>$inschrijfnr));
        if ($_rs) {
            foreach($_rs as $_record) {
                $_studietempos[] = Array('studietempocode'=>$_record['STUDIETEMPOCODE'], 'studietemponaam'=>$_record['STUDIETEMPONAAM']);
            }
        }

        $_sqlAantalInschrijvingBetaalTermijnen = "
            SELECT DISTINCT AantalTermijnen
            FROM NHA.InschrijvingBetaalTempoTermijn
            WHERE InschrijfNr = :inschrijfnr
            AND ZelfStudieJaNee = :zelfstudie
        ";

        $_inschrijvingbetaaltermijnen = Array();
        $_rs = $this->form->database->userdb->GetAll($_sqlAantalInschrijvingBetaalTermijnen, array('inschrijfnr'=>$inschrijfnr,'zelfstudie'=>$zelfstudie));
        if ($_rs) {
            foreach($_rs as $_record) {
                $_inschrijvingbetaaltermijnen[] = Array('aantaltermijnen'=>$_record['AANTALTERMIJNEN']);
            }
        }

        $_sqlStudieBetaalTermijnen = "
            SELECT DISTINCT CSB.StudieTempoCode, CSB.AantalBetaalTermijnen
            FROM NHA.CursusStudieTempBetaalTer CSB,
            NHA.Inschrijving I,
            NHA.InschrijvingBetaalTempoTermijn IB
            WHERE I.InschrijfNr = :inschrijfnr
            AND IB.InschrijfNr = I.InschrijfNr
            AND CSB.CursusCode = I.CursusCode
            AND  IB.ZelfStudieJaNee = :zelfstudie
        ";

        $_studietempobetaaltermijnen = Array();
        $_rs = $this->form->database->userdb->GetAll($_sqlStudieBetaalTermijnen, array('inschrijfnr'=>$inschrijfnr,'zelfstudie'=>$zelfstudie));
        if ($_rs) {
            foreach($_rs as $_record) {
                $_studietempobetaaltermijnen[] = Array('studietempocode'=>$_record['STUDIETEMPOCODE'], 'aantalbetaaltermijnen'=>$_record['AANTALBETAALTERMIJNEN']);
            }
        }

        $_result = array_merge(
            Array('zelfstudies' => $_zelfstudie),
            Array('studietempos' => $_studietempos),
            Array('inschrijvingbetaaltermijnen' => $_inschrijvingbetaaltermijnen),
            Array('studietempobetaaltermijnen' => $_studietempobetaaltermijnen)
        );

        // return the result as JSON
        return json_encode($_result);
    }

    function DisplayBetalingsverzoekenSamenvoegen() {
        if ($this->outputformat == 'xhtml') {
            $this->showControls = false;
            $this->showDefaultButtons = false;
            $this->smarty->assign('landen', $this->GetGebruikteLanden());
            $this->smarty->display("betalingsverzoeken_samenvoegen.tpl.php");
        }
    }

    /**
    * Teveel Betaald Saldo
    */
    function GetBetalingsverzoekenTBS($inschrijfnr, $relatienr, $zondernulbedragen=false) {
        $_sql = "
        SELECT ROWIDTOCHAR(BV.ROWID) AS PLR__RECORDID,
        BV.BETALINGSVERZOEKNR,BV.RELATIENR,BV.INSCHRIJFNR,BV.STATUS,to_char(BV.BEDRAGINEUROS, 'FM99990.00') as BEDRAGINEUROS,to_char(BV.BEDRAGBETAALDINEUROS, 'FM99990.00') as BEDRAGBETAALDINEUROS
        ,BV.BETALINGSKENMERK,BV.TERMIJNNR,BV.ORDERPICKLIJSTNR,BV.VERZENDDATUM,BV.FACTUURDATUM,BV.BETAALWIJZECODE
        ,BV.BETALINGSREGELINGJANEE,BV.SYSTEEMVERVALDATUM,to_char(BV.BEDRAGNOGTEBETALENINEUROS, 'FM99990.00') as BEDRAGNOGTEBETALENINEUROS,BV.CURSISTVERVALDATUM,BV.FACTUURBOEKINGDATUMTIJD,BV.HERINNERINGJANEE
        ,BV.ORDERPICKLIJSTREGELNR,BV.VERWERKTINBETALINGSREGELING,BV.ONDERDEELVANBETALINGSREGELING
        ,OPL.VERWERKDATUM, DECODE(BV.TermijnNr, NULL, 'B' ||TO_CHAR(BV.betalingsverzoeknr, 'FM00000000000')
        , 'A' || TO_CHAR(BV.TermijnNr, 'FM0009')|| BV.BETALINGSKENMERK) AS ord
        FROM NHA.BetalingsVerzoek BV
        , NHA.OrderPickLijst OPL
        WHERE ((:inschrijfnr is not null and BV.InschrijfNr = :inschrijfnr) or (BV.Inschrijfnr is null and BV.Relatienr = :relatienr))
        AND BV.Status in ('IB','GB','VE','GP','GV')
        AND OPL.OrderPickLijstNr (+) = BV.OrderPickLijstNr
        ORDER BY ORD ASC";
        $_rs = $this->form->database->userdb->GetAll($_sql, array('inschrijfnr'=>$inschrijfnr,'relatienr'=>$relatienr));

        // return the result as JSON
        return json_encode($_rs);
    }

    /**
    * Losse Factuur Aanmaken
    */
    function GetBetalingsverzoekenLFA($inschrijfnr, $zondernulbedragen=false) {
        $_sql = "
        SELECT ROWIDTOCHAR(BV.ROWID) AS PLR__RECORDID,
        BV.BETALINGSVERZOEKNR,BV.RELATIENR,BV.INSCHRIJFNR,BV.STATUS,to_char(BV.BEDRAGINEUROS, 'FM99990.00') as BEDRAGINEUROS,to_char(BV.BEDRAGBETAALDINEUROS, 'FM99990.00') as BEDRAGBETAALDINEUROS
        ,BV.BETALINGSKENMERK,BV.TERMIJNNR,BV.ORDERPICKLIJSTNR,BV.VERZENDDATUM,BV.FACTUURDATUM,BV.BETAALWIJZECODE
        ,BV.BETALINGSREGELINGJANEE,BV.SYSTEEMVERVALDATUM,to_char(BV.BEDRAGNOGTEBETALENINEUROS, 'FM99990.00') as BEDRAGNOGTEBETALENINEUROS,BV.CURSISTVERVALDATUM,BV.FACTUURBOEKINGDATUMTIJD,BV.HERINNERINGJANEE
        ,BV.ORDERPICKLIJSTREGELNR,BV.VERWERKTINBETALINGSREGELING,BV.ONDERDEELVANBETALINGSREGELING
        ,OPL.VERWERKDATUM
        FROM NHA.BetalingsVerzoek BV
        , NHA.OrderPickLijst OPL
        WHERE BV.InschrijfNr = :inschrijfnr
        AND OPL.OrderPickLijstNr (+) = BV.OrderPickLijstNr";

        if ($zondernulbedragen) {
            $_sql .= " AND BEDRAGNOGTEBETALENINEUROS <> 0 ";
        }

        $_sql .= "
        ORDER BY BV.TermijnNr, BV.BetalingsKenmerk DESC";

        $_rs = $this->form->database->userdb->GetAll($_sql, array('inschrijfnr'=>$inschrijfnr));

        // return the result as JSON
        return json_encode($_rs);
    }

    function DisplayBetalingsRegeling() {
        if ($this->outputformat == 'xhtml') {
            $this->showControls = false;
            $this->showDefaultButtons = false;
            $this->smarty->assign('landen', $this->GetGebruikteLanden());
            $this->smarty->display("betalingsregeling.tpl.php");
        }
    }

    function GetBetalingsRegeling($inschrijfnr) {
        $_sqlOpenstaand = "
        SELECT SUM(BedragInEuros - BedragBetaaldInEuros) AS OPENSTAANDINEUROS
        FROM NHA.VERVLOGENBETALINGSVERZOEKEN
        WHERE inschrijfnr = :inschrijfnr
        AND soortkosten = 'B'";
        $_openstaand = $this->form->database->userdb->GetOne($_sqlOpenstaand, array('inschrijfnr'=>$inschrijfnr));
        if (!isset($_openstaand)) $_openstaand = 0;

        $_sqlOpenstaandAdminKosten = "
        SELECT SUM(BedragInEuros - BedragBetaaldInEuros) AS OPENSTAANDINEUROS
        FROM NHA.VERVLOGENBETALINGSVERZOEKEN
        WHERE inschrijfnr = :inschrijfnr
        AND soortkosten = 'A'";
        $_openstaandAdminKosten = $this->form->database->userdb->GetOne($_sqlOpenstaandAdminKosten, array('inschrijfnr'=>$inschrijfnr));
        if (!isset($_openstaandAdminKosten)) $_openstaandAdminKosten = 0;

        // return the result as JSON
        return json_encode(array('OPENSTAANDINEUROS'=>$_openstaand, 'OPENSTAANDADMINKOSTENINEUROS'=>$_openstaandAdminKosten));
    }

    function DisplayTeveelBetaaldVerwerken() {
        if ($this->outputformat == 'xhtml') {
            $this->showControls = false;
            $this->showDefaultButtons = false;
            $this->smarty->assign('landen', $this->GetGebruikteLanden());
            $this->smarty->display("teveelbetaaldverwerken.tpl.php");
        }
    }

    function GetTeveelBetaaldRecords($inschrijfnr) {
        $_sqlTeveelBetaald = "
        SELECT i.inschrijfnr, i.cursistnr, itk.inkomendtransactienr, itk.bedragineuros, it.aanmaakdatum_1, itk.volgnr
        FROM NHA.INSCHRIJVING i, NHA.FACTUUR f, NHA.INKOMENDETRANSACTIE it, NHA.INKOMENDETRANSACTIEKOPPELING itk
        WHERE f.inschrijfnr = :inschrijfnr
        AND f.factuurnr = it.FACTUURNR_4
        AND f.INSCHRIJFNR = i.INSCHRIJFNR
        AND it.INKOMENDTRANSACTIENR = itk.INKOMENDTRANSACTIENR
        AND itk.terugbetalingnr IS NULL
        AND itk.aanmaningskostenopFACTUURNR IS NULL
        AND itk.betalingsverzoeknr IS NULL
        AND itk.alsbuitengewonebatenboeken IS NULL

        UNION

        SELECT i.inschrijfnr, i.cursistnr, itk.inkomendtransactienr, itk.bedragineuros, it.aanmaakdatum_1, itk.volgnr
        FROM NHA.INSCHRIJVING i, NHA.INKOMENDETRANSACTIE it, NHA.INKOMENDETRANSACTIEKOPPELING itk
        WHERE i.inschrijfnr = :inschrijfnr
        AND (i.FACTUURRELATIE = it.RELATIENR_4 OR i.cursistnr = it.RELATIENR_4)
        AND it.FACTUURNR_4 IS NULL
        AND it.INKOMENDTRANSACTIENR = itk.INKOMENDTRANSACTIENR
        AND itk.terugbetalingnr IS NULL
        AND itk.aanmaningskostenopFACTUURNR IS NULL
        AND itk.betalingsverzoeknr IS NULL
        AND itk.alsbuitengewonebatenboeken IS NULL
        ";

        $_rs = $this->form->database->userdb->GetAll($_sqlTeveelBetaald, array("inschrijfnr"=>$inschrijfnr));
        if ($_rs) {
            foreach($_rs as $_rec) {
                $totaalbedrag = $_rec['BEDRAGINEUROS'];
            }
        } else {
            $totaalbedrag = 0;
        }

        // return the result as JSON
        return json_encode(array('TOTAALBEDRAG'=>$totaalbedrag));
    }

    function GetTeveelBetaaldAlleRecords($inschrijfnr) {
        $_sqlTeveelBetaaldAlles = "
        SELECT i.inschrijfnr, i.cursistnr, itk.inkomendtransactienr, itk.bedragineuros, it.aanmaakdatum_1, itk.volgnr
        FROM NHA.INSCHRIJVING i, NHA.FACTUUR f, NHA.INKOMENDETRANSACTIE it, NHA.INKOMENDETRANSACTIEKOPPELING itk
        WHERE f.inschrijfnr = :inschrijfnr
        AND f.factuurnr = it.FACTUURNR_4
        AND f.INSCHRIJFNR = i.INSCHRIJFNR
        AND it.INKOMENDTRANSACTIENR = itk.INKOMENDTRANSACTIENR
        AND itk.terugbetalingnr IS NULL
        AND itk.aanmaningskostenopFACTUURNR IS NULL
        AND itk.betalingsverzoeknr IS NULL
        AND itk.alsbuitengewonebatenboeken IS NULL

        UNION

        SELECT i.inschrijfnr, i.cursistnr, itk.inkomendtransactienr, itk.bedragineuros, it.aanmaakdatum_1, itk.volgnr
        FROM NHA.INSCHRIJVING i, NHA.INKOMENDETRANSACTIE it, NHA.INKOMENDETRANSACTIEKOPPELING itk
        WHERE i.inschrijfnr = :inschrijfnr
        AND (i.FACTUURRELATIE = it.RELATIENR_4 OR i.cursistnr = it.RELATIENR_4)
        AND it.INKOMENDTRANSACTIENR = itk.INKOMENDTRANSACTIENR
        AND itk.terugbetalingnr IS NULL
        AND itk.aanmaningskostenopFACTUURNR IS NULL
        AND itk.betalingsverzoeknr IS NULL
        AND itk.alsbuitengewonebatenboeken IS NULL
        ";

        $totaalbedrag = 0;

        if (isset($inschrijfnr)) {
            $_rs = $this->form->database->userdb->GetAll($_sqlTeveelBetaaldAlles, array("inschrijfnr"=>$inschrijfnr));
            if ($_rs) {
                foreach($_rs as $_rec) {
                    $totaalbedrag = $totaalbedrag + floatval($_rec['BEDRAGINEUROS']);
                }
                $totaalbedrag = str_replace(',','.',$totaalbedrag);
            } else {
                $totaalbedrag = 0;
            }
        }

        // return the result as JSON
        return json_encode(array('TOTAALBEDRAG'=>$totaalbedrag));
    }

    function GetBriefInhoud($verzendonderdeelcode, $landcode='NL') {
        $_sql = "SELECT DECODE(:landcode, 'DE', NVL(INHOUDBRIEF_PLR_DE, INHOUDBRIEF_PLR), 'BE', NVL(INHOUDBRIEF_PLR_BE, INHOUDBRIEF_PLR), INHOUDBRIEF_PLR) AS INHOUDBRIEF_PLR
        FROM nha.verzendonderdeel WHERE verzendonderdeelcode = :verzendonderdeelcode";
        $_inhoud = $this->form->database->userdb->GetOne($_sql, array('landcode'=>$landcode,'verzendonderdeelcode'=>$verzendonderdeelcode));
        return json_encode(array("INHOUDBRIEF_PLR"=>$_inhoud));
    }

    function GetLandcodeRelatie($inschrijfnr) {
        $result = 'NL';
        if ($inschrijfnr) {
            $db = $this->form->database->userdb;
            $_sp = $db->PrepareSP(
            "DECLARE RETVAL VARCHAR2(2); BEGIN :RETVAL := NHA.PackInschrijving.BepaalAcademieLand(:inschrijfnr); END;"
            );
            $db->InParameter($_sp, $inschrijfnr, 'inschrijfnr');
            $db->OutParameter($_sp, $result, 'RETVAL');
            $resultSQL = $db->Execute($_sp);
        }
        return $result;
    }

    function DisplayLosseFactuurAanmaken() {
        global $_GVARS;

        if ($this->outputformat == 'xhtml') {
            echo "<script src='{$_GVARS['serverpath']}/guicomponents/ckeditor/ckeditor.js'></script>";
            $this->showControls = false;
            $this->showDefaultButtons = false;
            $this->smarty->assign('landen', $this->GetGebruikteLanden());
            $this->smarty->display("lossefactuuraanmaken.tpl.php");
        }
    }

    function GetCursusGroepen() {
        $_sql = "
            SELECT g.CURSUSGROEPCODE, g.CURSUSGROEPCODE || ' - ' || g.OMSCHRIJVING
            FROM CURSUSGROEP g
            ORDER BY 1
        ";
        $_rs = $this->form->database->userdb->GetAssoc($_sql);
        return $_rs;
    }

    function GetCursusCodes($cursusgroep, $inclusiefinactief=false) {
        $geblokkeerdjanee = $inclusiefinactief?"'J','N'":"'N'";

        $_sql = "
            SELECT c.CURSUSCODE, c.CURSUSNAAM, count(s.SEALPAKKETNR) AS AANTALSEALPAKKETTEN
            , c.ISCOHORTCURSUSVAN, c.SOORT_CURSUS
            FROM CURSUS c, CURSUSSEALPAKKETVOORRAAD s
            WHERE c.CURSUSGROEP = :CURSUSGROEP
            AND c.GEBLOKKEERDJANEE IN ($geblokkeerdjanee)
            AND c.CURSUSCODE = s.CURSUSCODE (+)
            GROUP BY c.CURSUSCODE, c.CURSUSNAAM, c.ISCOHORTCURSUSVAN, c.SOORT_CURSUS
            ORDER BY NVL(c.ISCOHORTCURSUSVAN, c.CURSUSCODE), c.SOORT_CURSUS, c.CURSUSCODE
        ";
//            c.GEBLOKKEERDJANEE = 'N'

        $_rs = $this->form->database->userdb->GetAll($_sql, array("CURSUSGROEP"=>$cursusgroep));
        // return the result as JSON
        return json_encode($_rs);
    }

    function GetAcademieOfPw() {
        $_sql = "
            SELECT ttt.ACADEMIEOFPW, ttt.ACADEMIEOFPW  || ' - ' || ttt.OMSCHRIJVING
            FROM ACADEMIEOFPW ttt
            ORDER BY ttt.ACADEMIEOFPW
        ";
        $_rs = $this->form->database->userdb->GetAssoc($_sql);
        return $_rs;
    }

    function GetAdministratie() {
        $_sql = "
            SELECT ttt.ACADEMIEOFPW||'-'||e.ADRESNR
            , e.LANDCODE ||' '|| ttt.ACADEMIEOFPW  ||' - '|| ttt.OMSCHRIJVING ||' '|| l.LANDNAAM
            FROM ACADEMIEOFPW ttt, EIGENADRES e, LAND l
            WHERE ttt.ACADEMIEOFPW = e.ACADEMIEOFPW
            AND e.LANDCODE = l.LANDCODE
            ORDER BY ttt.ACADEMIEOFPW, l.landnaam DESC
                    ";
        $_rs = $this->form->database->userdb->GetAssoc($_sql);
        return $_rs;
    }

    function GetStudieGidsen() {
        $_sql = "
            SELECT verzendonderdeelcode, verzendonderdeelcode omschrijving
            FROM VERZENDONDERDEEL
            WHERE soort = 'S'
            ORDER BY Verzendonderdeelcode
        ";
        $_rs = $this->form->database->userdb->GetAssoc($_sql);
        return $_rs;
    }

    function GetLeaflets() {
        $_sql = "
            SELECT verzendonderdeelcode, verzendonderdeelcode omschrijving
            FROM VERZENDONDERDEEL
            WHERE soort = 'L'
            ORDER BY Verzendonderdeelcode
        ";
        $_rs = $this->form->database->userdb->GetAssoc($_sql);
        return $_rs;
    }

    function GetCursusClusters() {
        $_sql = "
            SELECT CURSUSCLUSTERNR, CURSUSCLUSTERNAAM omschrijving
            FROM CURSUSCLUSTER
            ORDER BY CURSUSCLUSTERNAAM
        ";
        $_rs = $this->form->database->userdb->GetAssoc($_sql);
        return $_rs;
    }



    function GetStudieTempos() {
        $_sql = "
            SELECT studietempocode, studietemponaam
            FROM STUDIETEMPO
            WHERE studietempocode <> 'C'
            ORDER BY STUDIETEMPOCODE
        ";
        $_rs = $this->form->database->userdb->GetAssoc($_sql);
        return $_rs;
    }

    function GetKwalificatieDossiers() {
        $_sql = "
            SELECT KDCODE, KDCODE || ' - ' || OMSCHRIJVING
            FROM KWALIFICATIEDOSSIER
            WHERE RELEVANTVOORNHA = 'J'
            ORDER BY KDCODE
        ";
        $_rs = $this->form->database->userdb->GetAssoc($_sql);
        return $_rs;
    }

    function GetCohorten() {
        $_sql = "
            SELECT COHORTCODE, COHORTCODE OMSCHRIJVING
            FROM COHORT
            WHERE SOORTCOHORT = 'O'
            ORDER BY COHORTCODE DESC
        ";
        $_rs = $this->form->database->userdb->GetAssoc($_sql);
        return $_rs;
    }

    function GetDossierCohorten() {
        $_sql = "
            SELECT COHORTCODE, COHORTCODE OMSCHRIJVING
            FROM COHORT
            WHERE SOORTCOHORT = 'D'
            ORDER BY COHORTCODE DESC
        ";
        $_rs = $this->form->database->userdb->GetAssoc($_sql);
        return $_rs;
    }

    function GetSTTlessen($cursuscode) {
        $_sqlLessen = "
            SELECT C.CURSUSLESNR, C.OMSCHRIJVING
            FROM CURSUSLES C
            WHERE C.CURSUSCODE = :CURSUSCODE
            ORDER BY 1
        ";
        $_rsLessen = $this->form->database->userdb->GetAll($_sqlLessen, array("CURSUSCODE"=>$cursuscode));
        return $_rsLessen;
    }

    function GetSTTgegevens($cursuscode) {
        $_sqlSTTgegevens = "
            SELECT C.CURSUSCODE, C.STUDIETEMPOCODE, S.STUDIETEMPONAAM, C.TERMIJNNR, C.LESNR
            FROM CURSUSSTUDIETEMPOTERMIJN C, STUDIETEMPO S
            WHERE C.CURSUSCODE = :CURSUSCODE
            AND C.STUDIETEMPOCODE = S.STUDIETEMPOCODE
            ORDER BY 2,4,5
        ";
        $_rsSTTgegevens = $this->form->database->userdb->GetAll($_sqlSTTgegevens, array("CURSUSCODE"=>$cursuscode));
        $_nieuwetermijn = FALSE;
        $_STT = array();
        foreach($_rsSTTgegevens as $_index => $_rec) {
            if ($_rec['TERMIJNNR'] != $_rsSTTgegevens[$_index-1]['TERMIJNNR']
            or $_nieuwetermijn) {
                $_beginlesnr = $_rec['LESNR'];
                $_nieuwetermijn = FALSE;
            }

            $_verschil = intval($_rsSTTgegevens[$_index+1]['LESNR']) - intval($_rsSTTgegevens[$_index]['LESNR']);
            if (($_rsSTTgegevens[$_index]['TERMIJNNR'] == $_rsSTTgegevens[$_index+1]['TERMIJNNR']
            and ($_verschil > 1 or $_verschil < 0))
            or $_rsSTTgegevens[$_index]['TERMIJNNR'] != $_rsSTTgegevens[$_index+1]['TERMIJNNR']
            ) {
                $_eindlesnr = $_rsSTTgegevens[$_index]['LESNR'];
                $_nieuwetermijn = TRUE;
            }

            if ($_nieuwetermijn) {
                $_STT[] = array('CURSUSCODE_STT'=>$_rec['CURSUSCODE'],'STUDIETEMPOCODE'=>$_rec['STUDIETEMPOCODE'],'STUDIETEMPONAAM'=>$_rec['STUDIETEMPONAAM'],'TERMIJNNR'=>$_rec['TERMIJNNR'],'BEGINLES'=>$_beginlesnr,'EINDLES'=>$_eindlesnr);
            }
        }
        $_rsLessen = $this->GetSTTlessen($cursuscode);

        // return the result as JSON
        return json_encode(array("STT"=>$_STT, "LESSEN"=>$_rsLessen));
    }

    function GetBTTgegevens($cursuscode) {
        global $polaris;

        $_sqlBTTgegevens = "
            SELECT CURSUSCODE AS CURSUSCODE_BTT, AANTALTERMIJNEN, ZELFSTUDIEJANEE, BEDRAGINEUROS, BEDRAGINEUROS_BELGIE, BEDRAGINEUROS_BELGIE_OORSPR
            FROM CURSUSBETAALTEMPOTERMIJN
            WHERE CURSUSCODE = :CURSUSCODE
            GROUP BY CURSUSCODE, AANTALTERMIJNEN, ZELFSTUDIEJANEE, BEDRAGINEUROS, BEDRAGINEUROS_BELGIE, BEDRAGINEUROS_BELGIE_OORSPR
            ORDER BY 1, 3, 2
        ";

        $_rsBTTgegevens = $this->form->database->userdb->GetAll($_sqlBTTgegevens, array("CURSUSCODE"=>$cursuscode));
        foreach($_rsBTTgegevens as $_index => $_rec) {
            $_rsBTTgegevens[$_index]['BEDRAGINEUROS'] = str_replace('.',',',$_rsBTTgegevens[$_index]['BEDRAGINEUROS']);
            $_rsBTTgegevens[$_index]['BEDRAGINEUROS_BELGIE'] = str_replace('.',',',$_rsBTTgegevens[$_index]['BEDRAGINEUROS_BELGIE']);
            $_rsBTTgegevens[$_index]['BEDRAGINEUROS_BELGIE_OORSPR'] = str_replace('.',',',$_rsBTTgegevens[$_index]['BEDRAGINEUROS_BELGIE_OORSPR']);
        }

        // return the result as JSON
        return json_encode(array("BTT"=>$_rsBTTgegevens));
    }

    function GetCohortgegevens($cursuscode, $inclusiefinactief=false) {
        $geblokkeerdjanee = $inclusiefinactief?"'J','N'":"'N'";

        $_sql = "SELECT  ttt.CURSUSCODE AS CURSUSCODE_CH, ttt.COHORT ,  ttt.ISCOHORTCURSUSVAN ,  ttt.KDCODE
        ,  ttt.DOSSIERCOHORT ,  ttt.CNUMMER ,  ttt.DATUMGELDIGTM AS COHORT_DATUMGELDIGTM ,  ttt.DATUMGELDIGVANAF AS COHORT_DATUMGELDIGVANAF
        , TO_CHAR(ttt.DATUMGELDIGVANAF, 'YYYY') AS JAARVANAF
        , TO_CHAR(ttt.DATUMGELDIGTM, 'YYYY') AS JAARTM
        , COUNT(I.INSCHRIJFNR) AS AANTALINSCHRIJVINGEN
        FROM NHA.CURSUS ttt, NHA.INSCHRIJVING I
        WHERE ttt.ISCOHORTCURSUSVAN = :CURSUSCODE
        AND ttt.CURSUSCODE = I.CURSUSCODE (+)
        AND ttt.GEBLOKKEERDJANEE IN ($geblokkeerdjanee)
        GROUP BY ttt.CURSUSCODE, ttt.COHORT ,  ttt.ISCOHORTCURSUSVAN ,  ttt.KDCODE
        ,  ttt.DOSSIERCOHORT ,  ttt.CNUMMER ,  ttt.DATUMGELDIGTM,  ttt.DATUMGELDIGVANAF
        , TO_CHAR(ttt.DATUMGELDIGVANAF, 'YYYY')
        , TO_CHAR(ttt.DATUMGELDIGTM, 'YYYY')
        ORDER BY ttt.DATUMGELDIGVANAF, ttt.DATUMGELDIGTM";
//$this->form->database->userdb->debug=true;
        $_rsCohorten = $this->form->database->userdb->GetAll($_sql, array("CURSUSCODE"=>$cursuscode));
        return json_encode($_rsCohorten);
    }

    function GetCursusBasisGegevens($cursuscode, $inclusiefinactief=false) {
        $geblokkeerdjanee = $inclusiefinactief?"'J','N'":"'N'";

        $_sql = "SELECT  ttt.CURSUSCODE ,  ttt.CURSUSNAAM ,  ttt.WEBOMSCHRIJVING
        , ttt.CURSUSGROEP ,  ttt.ACADEMIEOFPW||'-'||ttt.EIGENADRES ADMINISTRATIE,  ttt.EXAMENGELD ,  ttt.AANTALLESSENOPZEGGING
        , ttt.AANTALMAANDENONDERBREKING ,  ttt.AANTALRINGBANDEN ,  ttt.BEGELEIDINGMETOFZONDER
        , ttt.BELLENNAGEENREACTIEJANEE ,  ttt.EXTERNINTERNEXAMEN ,  ttt.MIDDELBAARONDERWIJSSOORT
        , ttt.MBOHBONIVEAU ,  ttt.TOTAALAANTALLESSEN ,  ttt.COHORT ,  ttt.ISCOHORTCURSUSVAN ,  ttt.KDCODE
        , ttt.DOSSIERCOHORT ,  ttt.CNUMMER ,  ttt.DATUMGELDIGTM ,  ttt.DATUMGELDIGVANAF ,  ttt.STUDIEGIDSCODE
        , ttt.LEAFLETCODE ,  ttt.STANDAARDSTUDIETEMPO ,  ttt.HBOOVERIGE
        , ttt.RETOURNEERBAARJANEE ,  ttt.STANDAARDBETAALTEMPOTERMIJNEN ,  ttt.ZENDINGINDIENBETAALDJANEE
        , ttt.STICKERVENSTERENVELOPE1 ,  ttt.STICKERVENSTERENVELOPEN ,  ttt.OPZEGBAARJANEE
        , ttt.LIGTOPSORTEERPOSITIE ,  ttt.AANTALLESSENPERMAANDBIJOPZEG ,  ttt.CURSUSDUURMAANDEN
        , ttt.LESBLOKPLUSENVELOP ,  ttt.GEBLOKKEERDJANEE ,  ttt.ALLEENZICHTBAARVOORBACKOFF
        , ttt.INSCHRIJVINGENBLOKKERENJANEE ,  ttt.MATERIAALCOMPLEETJANEE ,  ttt.MATERIAALVERWACHTDATUM
        , ttt.TEGEMOETKOMINGWACHTTIJD ,  ROWIDTOCHAR( ttt.ROWID) AS PLR__RECORDID
        , ttt.SOORT_CURSUS, ttt.LEERWEG, ttt.SOORTONDERWIJS, ttt.CURSUSCLUSTERNR, ttt.GETUIGSCHRIFTJANEE
        , ttt.ONDERTEK_VOOR_VERSTUREN_JN, ttt.INSCHRIJFFORM_PER_EMAIL_JN
        , (SELECT COUNT(I.INSCHRIJFNR) FROM NHA.INSCHRIJVING I WHERE I.CURSUSCODE = ttt.CURSUSCODE) AS AANTALINSCHRIJVINGEN
        , (SELECT COUNT(s.SEALPAKKETNR) FROM NHA.CURSUSSEALPAKKETVOORRAAD s WHERE s.CURSUSCODE = ttt.CURSUSCODE) AS AANTALSEALPAKKETTEN
        , NVL2(C.CURSUSCODE, 'J', 'N') AS GEBUFFERDJANEE
        , ttt.PLATHOSID
        FROM CURSUS ttt, GEBUFFERDEMBOCURSUSSEN c
        WHERE ttt.CURSUSCODE = :CURSUSCODE
        AND ttt.CURSUSCODE = c.CURSUSCODE (+)
        AND ttt.GEBLOKKEERDJANEE IN ($geblokkeerdjanee)
        ";
        //GEBLOKKEERDJANEE='N'

        $_rsBasisGegevens = $this->form->database->userdb->GetAll($_sql, array("CURSUSCODE"=>$cursuscode));
        return json_encode($_rsBasisGegevens);
    }

    function GetAlleCursusGegevens($cursuscode, $inclusiefinactief) {
        $result = '{"Basisgegevens":'.$this->GetCursusBasisGegevens($cursuscode, $inclusiefinactief).',';
        $result .= '"Cohortgegevens":'.$this->GetCohortgegevens($cursuscode, $inclusiefinactief).',';
        $result .= '"STTgegevens":'.$this->GetSTTgegevens($cursuscode).',';
        $result .= '"BTTgegevens":'.$this->GetBTTgegevens($cursuscode).'}';
        return $result;
    }

    /*****
    * Cursus checklist
    */
    function RunChecklist($_cursusCode) {
        $_sqlCheck['Aantal cursuslessen'] = "SELECT COUNT(*) AS AANTAL FROM NHA.CURSUSLES WHERE CURSUSCODE = :CURSUSCODE";
        $_sqlCheck['Aantal sealpakketten'] = "SELECT COUNT(*) AS AANTAL FROM NHA.CURSUSSEALPAKKETVOORRAAD WHERE CURSUSCODE = :CURSUSCODE";
        $_sqlCheck['Aantal studietempotermijnen'] = "SELECT COUNT(*) AS AANTAL FROM NHA.CURSUSSTUDIETEMPOTERMIJN WHERE CURSUSCODE = :CURSUSCODE";
        $_sqlCheck['Aantal betaaltempotermijnen'] = "SELECT COUNT(*) AS AANTAL FROM NHA.CURSUSBETAALTEMPOTERMIJN WHERE CURSUSCODE = :CURSUSCODE";
        $_sqlCheck['Huiswerkschema'] = "SELECT COUNT(*) AS AANTAL FROM ONDERWIJS.MODULELESHUISWERK
            WHERE MODULECODE IN (SELECT DISTINCT MODULECODE FROM NHA.CURSUSLES WHERE CURSUSCODE = :CURSUSCODE)";

        foreach($_sqlCheck as $index => $_sql) {
            $_aantal = $this->form->database->userdb->GetOne($_sql, array("CURSUSCODE"=>$_cursusCode));
            $_checklist[] = array('check'=>$index.': '.$_aantal,'status'=>$_aantal > 0);
        }

        $_result = '{"checks":'.json_encode($_checklist).'}';
        return $_result;
    }

    /*****
    * Cursus checklist
    */
    function GetFoutieveStudieTempo($cursuscode) {
        $_sql = "
            SELECT INSCHRIJFNR, CURSISTNR, STUDIETEMPOCODE
            FROM INSCHRIJVING_MATINCOMPLEET M
            WHERE CURSUSCODE = :CURSUSCODE
            AND M.STUDIETEMPOCODE NOT IN (
                SELECT DISTINCT C.STUDIETEMPOCODE
                FROM CURSUSSTUDIETEMPOTERMIJN C
                WHERE C.CURSUSCODE = M.CURSUSCODE
            )
        ";
        $_sqlStudeTempos = "
            SELECT DISTINCT C.STUDIETEMPOCODE
            FROM CURSUSSTUDIETEMPOTERMIJN C
            WHERE C.CURSUSCODE = :CURSUSCODE
        ";
        $_rs = $this->form->database->userdb->GetAll($_sql, array("CURSUSCODE"=>$cursuscode));
        $_rs2 = $this->form->database->userdb->GetAll($_sqlStudeTempos, array("CURSUSCODE"=>$cursuscode));

        $_result = '{"foutenlijst":'.json_encode($_rs);
        $_result .= ',"studietempos":'.json_encode($_rs2);
        $_result .= '}';
        return $_result;

    }

    function DisplayCursusGegevens() {
        global $_GVARS;
        global $polaris;

        if ($this->outputformat == 'xhtml') {
            $this->showControls = false;
            $this->showDefaultButtons = false;
            $this->smarty->assign("cursusgroepen", $this->GetCursusGroepen($_SESSION['pinrecords'][$polaris->polarissession]['NHA.CURSUS'][0]['CURSUSCODE']));
            $this->smarty->assign("administratie", $this->GetAdministratie());
            $this->smarty->assign("studiegidsen", $this->GetStudieGidsen());
            $this->smarty->assign("leaflets", $this->GetLeaflets());
            $this->smarty->assign("studietempos", $this->GetStudieTempos());
            $this->smarty->assign("kdcodes", $this->GetKwalificatieDossiers());
            $this->smarty->assign("cohorten", $this->GetCohorten());
            $this->smarty->assign("dossiercohorten", $this->GetDossierCohorten());
            $this->smarty->assign("cohortgeldigvanaf", strtotime('+1 year'));
            $this->smarty->assign("cohortgeldigtm", strtotime('+2 years'));
            $this->smarty->assign("cursusclusters", $this->GetCursusClusters());

            $this->smarty->assign("gepindecursus", $_SESSION['pinrecords'][$polaris->polarissession]['NHA.CURSUS'][0]['CURSUSCODE']);
            $this->smarty->display("cursusgegevensplus.tpl.php");
        }
    }

    function DisplayOverzichtCursussen() {
        global $_GVARS;
        global $polaris;

        if ($this->outputformat == 'xhtml') {
            $this->form->LoadViewContent($detail=false, $masterrecord=false, false, false);
            echo "</form><div id=\"scherm_cursusoverzicht\" class=\"listview\">";
            $this->form->ShowListView($state='view', $permissiontype=2);
            echo "</div>";
        }
    }

    function GetModuleCodes() {
        $_sql = "
            SELECT c.MODULECODE, c.NAAM, count(s.LESNR) AS AANTALLESSEN
            FROM MODULE c, MODULELES s
            WHERE s.MODULECODE (+)= c.MODULECODE
            GROUP BY c.MODULECODE, c.NAAM
            ORDER BY c.MODULECODE
        ";
//        $_rs = $this->form->database->userdb->GetAll($_sql);

        $_rs = $this->form->GetViewContent($detail=false, $masterrecord=false, $limit=5000);
        $_rs = $_rs->GetAll();
        sort($_rs);
        // return the result as JSON
        return json_encode($_rs);
    }

    function GetModuleLessen($modulecode) {
        $_sql = "
            SELECT s.MODULECODE, s.LESNR, s.OMSCHRIJVING, 'VIEW' AS STATE
            FROM MODULELES s
            WHERE s.MODULECODE = :MODULECODE
            ORDER BY s.LESNR
        ";
        $_rs = $this->form->database->userdb->GetAll($_sql, array('MODULECODE'=>$modulecode));
        // return the result as JSON
        return json_encode(array("lessen"=>$_rs));
    }

    function DisplayModuleGegevens() {
        global $_GVARS;
        global $polaris;

        if ($this->outputformat == 'xhtml') {
            $this->showControls = false;
            $this->showDefaultButtons = false;
            $this->smarty->assign("gepindecursus", $_SESSION['pinrecords'][$polaris->polarissession]['NHA.CURSUS'][0]['CURSUSCODE']);
            $this->smarty->display("modulegegevensplus.tpl.php");
        }
    }

    function GetPremiums() {
        $_sql = "SELECT VERZENDONDERDEELCODE, VERZENDONDERDEELCODE ||' - '|| OMSCHRIJVING AS OMSCHRIJVING FROM VERZENDONDERDEEL WHERE ISPREMIUM = 'J' ORDER BY VERZENDONDERDEELCODE";
        $_rs = $this->form->database->userdb->GetAssoc($_sql);

        return $_rs;
    }

    function GetInschrijfInfo($_inschrijfnr) {
        $_sql = "
        SELECT RELATIENR, VOORLETTERS, TUSSENVOEGSEL, ACHTERNAAM, CURSUSCODE, PLAATS, ONTVANGENLEAFLET, DATUMONTVANGENLEAFLET,
        (SELECT LISTAGG(VERZENDONDERDEELCODE, ',') WITHIN GROUP (ORDER BY VERZENDONDERDEELCODE)
         FROM ORDERPICKLIJSTREGEL R, ORDERPICKLIJST O, VERZENDONDERDEEL V
         WHERE I.INSCHRIJFNR = O.INSCHRIJFNR (+)
         AND O.ORDERPICKLIJSTNR = R.ORDERPICKLIJSTNR (+)
         AND (R.PICKTYPESOORT = 'A' OR R.PICKTYPESOORT IS NULL)
         AND R.PICKTYPECODE = V.VERZENDONDERDEELCODE (+)
         AND (V.ISPREMIUM = 'J' OR V.ISPREMIUM IS NULL)) LEAFLET
        FROM INSCHRIJVING I, RELATIE R
        WHERE I.CURSISTNR = R.RELATIENR
        AND I.INSCHRIJFNR = :INSCHRIJFNR
        ";
        $_rs = $this->form->database->userdb->GetRow($_sql, array('INSCHRIJFNR'=>$_inschrijfnr));

        return json_encode($_rs);
    }

    function DisplayBulkPremiums() {
        global $_GVARS;
        global $polaris;

        if ($this->outputformat == 'xhtml') {
            $this->showControls = false;
            $this->showDefaultButtons = false;
            $_maxLeafletsPerKeer =  20;
            $this->smarty->assign('loopBulk', range(1, $_maxLeafletsPerKeer));
            $this->smarty->assign('premiums', $this->GetPremiums());
            $this->smarty->display("bulkpremiums.tpl.php");
        }
    }

    function GetOndertekenenInschrijvingen($_inschrijfnr) {
        $_sql = "
        SELECT RELATIENR, VOORLETTERS, TUSSENVOEGSEL, ACHTERNAAM, CURSUSCODE, PLAATS, ONDERTEKENDJANEE
        FROM INSCHRIJVING I, RELATIE R
        WHERE I.CURSISTNR = R.RELATIENR
        AND I.INSCHRIJFNR = :INSCHRIJFNR
        ";
        $_rs = $this->form->database->userdb->GetRow($_sql, array('INSCHRIJFNR'=>$_inschrijfnr));

        return json_encode($_rs);
    }

    function DisplayBulkOndertekenen() {
        global $_GVARS;
        global $polaris;

        if ($this->outputformat == 'xhtml') {
            $this->showControls = false;
            $this->showDefaultButtons = false;
            $_maxOndertekenenPerKeer =  20;
            $this->smarty->assign('loopBulk', range(1, $_maxOndertekenenPerKeer));
            //$this->smarty->assign('ondertekenen', $this->GetOndertekenenInschrijvingen());
            $this->smarty->display("bulkondertekenen.tpl.php");
        }
    }

    function GetJsonIncassoDatum($betaaldag, $inschrijfnr) {
        $retourneerdefault = 'N';
        $db = $this->form->database->userdb;
        $stmt = $db->PrepareSP("
        DECLARE
            RETVAL DATE;
        BEGIN
            :RETVAL := NHA.PackInschrijvingAanpassing.BepaalEersteVolgIncassoQuick(
              :incassodag
            , :inschrijfnr
            , SYSDATE
            , :retourneerdefault
            );
        END;");

        $db->InParameter($stmt, $betaaldag, 'incassodag');
        $db->InParameter($stmt, $inschrijfnr, 'inschrijfnr');
        $db->InParameter($stmt, $retourneerdefault, 'retourneerdefault');
        $db->OutParameter($stmt, $result, 'RETVAL');

        $ok = $db->Execute($stmt);

        return json_encode(array('incassodatum'=>$result));
    }

    function Show($outputformat='xhtml', $rec=false) {
        global $_GVARS;
        global $polaris;

        parent::Show($outputformat, $rec);

        if (isset($_GET['action'])) {
            /**
            * Show the Polaris generated forms when the user wants to insert or edit an item
            */
            switch($_GET['action']) {
            case 'edit':
            case 'insert':
                $this->form->ShowFormView($this->state, $this->permission, $detail=false, $masterrecord=false);
                $this->form->ShowButtons($this->state, $this->permission);
                $this->form->ShowNavigationLinks($this->state, $this->permission, $detail=false);
            break;
            case 'searchext':
                $this->form->ShowSearchForm($this->state, $this->permission, $detail=false, $masterrecord=false);
                $this->form->ShowButtons($this->state, $this->permission);
                $this->form->ShowNavigationLinks($this->state, $this->permission, $detail=false);
            break;
            }
        } else {
            /**
            * Show the custommade forms when user views a list of items
            */
            // alleen als een relatie als 'relatie' is gepind, dan invullen in Data Entry schermen
            // dus niet als een relatie onzichtbaar bij een inschrijving is gepind
            if ($_SESSION['pinrecords'][$polaris->polarissession]['NHA.RELATIE'][0]['RELATIENR'])
            //if ($_SESSION['pinrecords'][$polaris->polarissession]['NHA.RELATIE'][0]['VISIBLE'] !== false and $_SESSION['pinrecords'][$polaris->polarissession]['NHA.RELATIE'][0]['RELATIENR'])
                $this->smarty->assign('pinrelatie', $_SESSION['pinrecords'][$polaris->polarissession]['NHA.RELATIE'][0]['RELATIENR']);

            if ($_SESSION['pinrecords'][$polaris->polarissession]['NHA.INSCHRIJVING'][0]['INSCHRIJFNR'])
                $this->smarty->assign('pininschrijving', $_SESSION['pinrecords'][$polaris->polarissession]['NHA.INSCHRIJVING'][0]['INSCHRIJFNR']);

            switch ($this->moduleid) {
            case 1:
                if ($_GET['func'] != '') {
                    if ($_GET['func'] == 'checkspampostcode') {
                        /* Check of de postcode een potentiele 'spamwijk' is */
                        echo $this->GetJsonCheckSpamPostcode($_GET['postcode']);
                    }
                } else {
                    /***
                    * Nieuwe Aanvraag
                    */
                    $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/sharedlib.js';
                    $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/relatieselectie_form.js';
                    $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/nieuwe_aanvraag.js';
                    $this->DisplayNieuweAanvraag();
                }
            break;
            case 2:
                /***
                * Nieuwe Inschrijving
                */
                if ($_GET['func'] != '') {
                    if ($_GET['func'] == 'cursusgegevens') {
                        /* Geef de cursusgegevens terug voor de betreffende cursuscode: betaaltermijnen, studietempos, enz */
                        echo $this->GetJsonCursusGegevens($_GET['cursuscode'], $_GET['zelfstudie'], $_GET['belgie']);
                    }
                    if ($_GET['func'] == 'betaalwijze') {
                        /* Geef de betaalwijze terug (hetzelfde voor alle cursussen) */
                        echo $this->GetJsonBetaalwijze($_GET['landcode']);
                    }
                    if ($_GET['func'] == 'checkspampostcode') {
                        /* Check of de postcode een potentiele 'spamwijk' is */
                        echo $this->GetJsonCheckSpamPostcode($_GET['postcode']);
                    }
                } else {
                    $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/sharedlib.js';
                    $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/relatieselectie_form.js';
                    $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/artikelverkoop_form.js';
                    $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/nieuwe_inschrijving_resultaat.js';
                    $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/nieuwe_inschrijving.js';
                    $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/iban.js';
                    $this->DisplayNieuweInschrijving();
                }
            break;
            case 3:
                /***
                * Artikel Verkoop
                */
                if ($_GET['func'] != '') {
                    if ($_GET['func'] == 'artikelgegevens') {
                        /* Geef de cursusgegevens terug voor de betreffende cursuscode: betaaltermijnen, studietempos, enz */
                        echo $this->GetJsonArtikelGegevens($_GET['artikelcode'], $_GET['cursistnr']);
                    }
                    if ($_GET['func'] == 'checkspampostcode') {
                        /* Check of de postcode een potentiele 'spamwijk' is */
                        echo $this->GetJsonCheckSpamPostcode($_GET['postcode']);
                    }
                } else {
                    $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/sharedlib.js';
                    $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/relatieselectie_form.js';
                    $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/artikel_verkoop_resultaat.js';
                    $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/artikel_verkoop.js';
                    $this->DisplayArtikelVerkoop();
                }
            break;
            case 4:
                /***
                * Telemarketing
                */
                if ($_GET['func'] != '') {
                    if ($_GET['func'] == 'persinschr') {
                        // ruim de acties op die te lang in behandeling zijn
                        $this->RuimOpActiesInBehandeling();
                        /* Geef het aantal pers. inschrijvingen voor vandaag */
                        echo $this->GetJsonPersoonlijkeInschrijvingen($_SESSION['userid']);
                    }
                    if ($_GET['func'] == 'autoselectie') {
                        /* Geef het actienummer terug van de automatisch geselecteerde geplande actie */
                        echo $this->GetJsonAutoSelectie($_SESSION['userid']);
                    }
                    if ($_GET['func'] == 'annuleeronderbehandeling') {
                        /* Geef het actienummer terug van de automatisch geselecteerde geplande actie */
                        echo $this->GeplandeActieNIETOnderbehandeling($_GET['actienr']);
                    }
                    if ($_GET['func'] == 'telemarketinginfo') {
                        /* Geef de cursusgegevens terug voor de betreffende cursuscode: betaaltermijnen, studietempos, enz */
                        echo $this->GetJsonTelemarketingInfo($_GET['actienr'], $_GET['relatienr'], $_GET['oorzaaknr']);
                    }
                    if ($_GET['func'] == 'oorzaakgeeninteresse') {
                        /* Geef de cursusgegevens terug voor de betreffende cursuscode: betaaltermijnen, studietempos, enz */
                        echo $this->GetJsonOorzaakGeenInteresse();
                    }
                } elseif (!$_POST['_hdnFORMTYPE']) {
                    $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/sharedlib.js';
                    $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/relatieselectie_form.js';
                    $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/telemarketing.js';
                    $this->DisplayTelemarketing();
                }
            break;
            case 5:
                /***
                * Betaalwijze aanpassen
                */
                if ($_GET['func'] != '') {
                    if ($_GET['func'] == 'bepaalincassodatum') {
                        /* Geef de opzegging totaalbedragen/info terug voor de betreffende inschrijving */
                        echo $this->GetJsonIncassoDatum($_GET['betaaldag'], $_GET['inschrijfnr']);
                    }
                } else {
                    $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/sharedlib.js';
                    $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/inschrijvingselectie_form.js';
                    $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/betaalwijzeaanpassen.js';
                    $this->DisplayBetaalwijzeAanpassen();
                }
            break;
            case 6:
                /***
                * Inschrijving opzeggen
                */
                if ($_GET['func'] != '') {
                    if ($_GET['func'] == 'opzegginginfo') {
                        /* Geef de opzegging totaalbedragen/info terug voor de betreffende inschrijving */
                        echo $this->GetOpzeggingInfo($_GET['inschrijfnr']);
                    }
                } else {
                    $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/sharedlib.js';
                    $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/inschrijvingselectie_form.js';
                    $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/inschrijvingopzeggen.js';
                    $this->DisplayInschrijvingOpzeggen();
                }
            break;
            case 7:
                /***
                * Inschrijving Termijnen wijzigen
                */
                if ($_GET['func'] != '') {
                    if ($_GET['func'] == 'studiebetaalinfo') {
                        /* Geef de opzegging totaalbedragen/info terug voor de betreffende inschrijving */
                        echo $this->GetStudieBetaalInfo($_GET['inschrijfnr'], $_GET['zelfstudie']);
                    }
                } else {
                    $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/sharedlib.js';
                    $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/inschrijvingselectie_form.js';
                    $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/inschrijvingtermijnwijzigen.js';
                    $this->DisplayInschrijvingTermijnenWijzigen();
                }
            break;
            case 8:
                /***
                * Betalingsverzoeken Samenvoegen
                */
                if ($_GET['func'] != '') {
                    if ($_GET['func'] == 'betalingsverzoeken') {
                        /* Geef de opzegging totaalbedragen/info terug voor de betreffende inschrijving */
                        echo $this->GetBetalingsverzoekenBVS($_GET['inschrijfnr']);
                    }
                } else {
                    $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/sharedlib.js';
                    $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/inschrijvingselectie_form.js';
                    $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/betalingsverzoeken_samenvoegen.js';
                    $this->DisplayBetalingsverzoekenSamenvoegen();
                }
            break;
            case 9:
                /***
                * Betalingsregeling
                */
                if ($_GET['func'] != '') {
                    if ($_GET['func'] == 'betalingsregeling') {
                        /* Geef de opzegging totaalbedragen/info terug voor de betreffende inschrijving */
                        echo $this->GetBetalingsRegeling($_GET['inschrijfnr']);
                    }
                } else {
                    $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/sharedlib.js';
                    $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/inschrijvingselectie_form.js';
                    $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/betalingsregeling.js';
                    $this->DisplayBetalingsRegeling();
                }
            break;
            case 10:
                /***
                * Teveel betaald bedrag verwijderen
                */
                if ($_GET['func'] != '') {
                    if ($_GET['func'] == 'betalingsverzoeken') {
                        /* Geef de opzegging totaalbedragen/info terug voor de betreffende inschrijving */
                        echo $this->GetBetalingsverzoekenTBS($_GET['inschrijfnr'], $_GET['relatienr'], $zondernulbedragen=true);
                    }
                    /* Deze wordt niet meer gebruikt: zie knopje 'Overige bedragen meenemen', MARIELLE
                    if ($_GET['func'] == 'totaalteveelbetaald') {
                        /* Geef de opzegging totaalbedragen/info terug voor de betreffende inschrijving
                        echo $this->GetTeveelBetaaldRecords($_GET['inschrijfnr']);
                    }
                    */
                    if ($_GET['func'] == 'totaalteveelbetaald_alles') {
                        /* Geef de opzegging totaalbedragen/info terug voor de betreffende inschrijving */
                        echo $this->GetTeveelBetaaldAlleRecords($_GET['inschrijfnr']);
                    }
                } else {
                    $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/sharedlib.js';
                    $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/inschrijvingselectie_form.js';
                    $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/teveelbetaaldverwerken.js';
                    $this->DisplayTeveelBetaaldVerwerken();
                }
            break;
            case 11:
                /***
                * Losse Factuur Aanmaken
                */
                if ($_GET['func'] != '') {
                    if ($_GET['func'] == 'betalingsverzoeken') {
                        /* Geef de opzegging totaalbedragen/info terug voor de betreffende inschrijving */
                        echo $this->GetBetalingsverzoekenLFA($_GET['inschrijfnr'], $zondernulbedragen=true);
                    }
                    if ($_GET['func'] == 'inhoudbrief') {
                        if ($_GET['inschrijfnr']) {
                            $_landcode = $this->GetLandcodeRelatie($_GET['inschrijfnr']);
                            echo $this->GetBriefInhoud($_GET['verzendonderdeelcode'], $_landcode);
                        }
                    }
                } else {
                    //$_GVARS['_includeJavascript'][] = $_GVARS['serverpath'].'/guicomponents/ckeditor/adapters/jquery.js';
                    $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/sharedlib.js';
                    $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/inschrijvingselectie_form.js';
                    $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/lossefactuuraanmaken.js';
                    $this->DisplayLosseFactuurAanmaken();
                }
            break;
            case 12:
                /***
                * Cursusgegevens invullen (versneld)
                */
                if ($_GET['func'] != '') {
                    switch ($_GET['func']) {
                        case 'cursuscodes':
                            echo $this->GetCursusCodes($_GET['cursusgroep'], $_GET['inclusiefinactief'] == 'true');
                            break;
                        case 'allecursusgegevens':
                            echo $this->GetAlleCursusGegevens($_GET['cursuscode'], $_GET['inclusiefinactief']);
                            break;
                        case 'cursusbasisgegevens':
                            echo $this->GetCursusBasisGegevens($_GET['cursuscode'], $_GET['inclusiefinactief']);
                            break;
                        case 'cursuscohorten':
                            echo $this->GetCohortgegevens($_GET['cursuscode'], $_GET['inclusiefinactief']);
                            break;
                        case 'sttgegevens':
                            echo $this->GetSTTgegevens($_GET['cursuscode']);
                            break;
                        case 'bttgegevens':
                            echo $this->GetBTTgegevens($_GET['cursuscode']);
                            break;
                        case 'checklist':
                            echo $this->RunChecklist($_GET['cursuscode']);
                            break;
                        case 'foutievestudietempo':
                            echo $this->GetFoutieveStudieTempo($_GET['cursuscode']);
                            break;
                    }
                } else {
                    $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/sharedlib.js';
                    $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/cursusgegevens.js';
                    $this->DisplayCursusGegevens();
                    if (isset($_GET['tag']) or isset($_GET['qc']) or isset($_GET['q'])) {
                        $this->DisplayOverzichtCursussen();
                    }
                }
            break;
            case 13:
                /***
                * Modulegegevens invullen (versneld)
                */
                if ($_GET['func'] != '') {
                    if ($_GET['func'] == 'modulecodes') {
                        echo $this->GetModuleCodes();
                    }
                    if ($_GET['func'] == 'modulelessen') {
                        echo $this->GetModuleLessen($_GET['modulecode']);
                    }
                } else {
                    $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/sharedlib.js';
                    $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/modulegegevens.js';
                    $this->DisplayModuleGegevens();
                }
            break;
            case 14:
                /***
                * Bulkpremiums invullen (versneld)
                */
                if ($_GET['func'] != '') {
                    if ($_GET['func'] == 'getinschrijfinfo') {
                        echo $this->GetInschrijfInfo($_GET['inschrijfnr']);
                    }
                } else {
                    $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/sharedlib.js';
                    $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/bulkpremiums.js';
                    $this->DisplayBulkPremiums();
                }
            break;
            case 15:
                /***
                * Bulkondertekenen invullen (versneld)
                */
                if ($_GET['func'] != '') {
                    if ($_GET['func'] == 'getinschrijfinfo') {
                        echo $this->GetOndertekenenInschrijvingen($_GET['inschrijfnr']);
                    }
                } else {
                    $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/sharedlib.js';
                    $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/bulkondertekenen.js';
                    $this->DisplayBulkOndertekenen();
                }
            break;
            }
        }
    }

}