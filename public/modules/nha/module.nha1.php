<?php
require_once('_shared/module.plr_executeaction.php');

class Module extends ModulePLR_ExecuteAction {

    function __construct($moduleid, $module) {
        parent::__construct($moduleid, $module);

        $this->processed = false;

        /**
        * What client are we talking about? (nog doen: eventueel selecteerbaar maken)
        */
        $this->clientid = $_SESSION['clientid'];

        $this->moduleid = $_GET['moduleid'];
    }

    function Process($method) {
        if (isset($_POST['_hdnProcessedByModule'])) {
            /* ... */
        }
    }

    function GetRowFilter($ROWIDS) {
        if ($ROWIDS != '') {
            $rowid = $this->dbobject->customUrlDecode($ROWIDS);
            $rowid = str_replace(',','\',\'',$rowid);
            $filter = ' I.ROWID IN (\''.$rowid.'\') ';
        } else {
            $filter = FALSE;
        }

        return $filter;
    }

    function GetTeAccorderenInschr($ROWID) {
        $_teAccorderenInschrSQL = '
        SELECT i.inschrijfnr
        , r.VOORLETTERS || \' \' || r.ACHTERNAAM
        , CASE
            WHEN trunc((months_between(sysdate, r.gebdatum))/12) < 18
            AND (NVL(i.MINDERJARIGAKKOORDJANEE, \'N\') = \'N\' )
            THEN \'Relatie is minderjarig! (\' || r.gebdatum || \')\'
            ELSE NULL
          END
        FROM inschrijving i, relatie r
        WHERE i.cursistnr = r.relatienr AND
        ';

        $rowid = $this->dbobject->customUrlDecode($ROWID);
        $rowid = str_replace(',','\',\'',$rowid);
        $filter = 'i.ROWID IN (\''.$rowid.'\')';
        $orderby = ' ORDER BY 3 ASC, 1 ASC';

        $inschr = $this->userdatabase->Execute($_teAccorderenInschrSQL.$filter.$orderby);

        include('tohtml.inc.php');
        $inschrtabel = '<div class="bodyscroll scrollarea"><br/>';
        $inschrtabel .= rs2html($inschr, 'id="datalistview" class="data striped nowrap group-table right" cellspacing="0" cellpadding="0"',$zheaderarray=array('Inschrijfnr -','Relatie -',' Opmerking',''),$htmlspecialchars=true,$echo = false);
        $inschrtabel .= '</div>';

        return $inschrtabel;
    }

    function GetCheckAndereInschr($ROWID) {
        $rowid = $this->dbobject->customUrlDecode($ROWID);

        $_teZoekInschrSQL = '
        SELECT i.INSCHRIJFNR
        , r.relatienr || \' - \' || r.VOORLETTERS || \' \' || r.ACHTERNAAM as NAAM
        , i.cursuscode || \' \' || c.cursusnaam as CURSUS
        FROM INSCHRIJVING i, RELATIE r, CURSUS c
        WHERE i.cursistnr = r.relatienr
        AND i.cursuscode = c.cursuscode
        AND i.inschrijfnr <> (SELECT INSCHRIJFNR FROM INSCHRIJVING i4 WHERE i4.ROWID IN (\''.$rowid.'\'))
        AND i.cursistnr = (SELECT CURSISTNR FROM INSCHRIJVING i3 WHERE i3.ROWID IN (\''.$rowid.'\'))
        AND i.DATUMINSCHRIJVING >= (SELECT DATUMINSCHRIJVING FROM INSCHRIJVING i2 WHERE i2.ROWID IN (\''.$rowid.'\'))
        ORDER BY 3 ASC, 1 ASC
        ';

        $inschr = $this->userdatabase->GetAll($_teZoekInschrSQL);
        if (count($inschr) == 0) {
            echo '<br/><h2>Er zijn geen nieuwe inschrijvingen gevonden</h2>';
            $inschrtabel = false;
        } else {
            $inschrtabel = '<div class="bodyscroll scrollarea"><br/>
                    <table cols="3" id="datalistview" class="data striped nowrap group-table right" cellspacing="0" cellpadding="0"><tbody><tr>
                    <th></th><th>Inschrijfnr</th><th>Relatie -</th><th> Cursus</th></tr>';
            foreach($inschr as $rec) {
                $inschrtabel .= '<tr valign="top"><td align="left"><input type="radio" name="param1" value="'.$rec['INSCHRIJFNR'].'" /></td><td align="right">'.$rec['INSCHRIJFNR'].'&nbsp;</td><td>'.$rec['NAAM'].'</td><td>'.$rec['CURSUS'].'</td></tr>';
            }
            $inschrtabel .= '</tbody></table></div>';
        }

        return $inschrtabel;
    }

    function GetInschrTabel($ROWID) {
        $rowid = $this->GetRowFilter($ROWID);
        if ($rowid) {
            $_sql = 'SELECT i.INSCHRIJFNR
            , r.VOORLETTERS || \' \' || r.ACHTERNAAM || \' (\' || r.relatienr || \')\' as NAAM
            , i.cursuscode || \' \' || c.cursusnaam as CURSUS
            , c.CURSUSCODE, c.ISCOHORTCURSUSVAN
            FROM INSCHRIJVING i, RELATIE r, CURSUS c
            WHERE i.cursistnr = r.relatienr
            AND i.CURSUSCODE = c.CURSUSCODE
            AND '.$rowid;
            $inschr = $this->userdatabase->GetAll($_sql);
            foreach($inschr as $rec) {
                $result .= 'Inschrijving: '.$rec['INSCHRIJFNR'].' &nbsp; Relatie: '.$rec['NAAM'].'<br/>';
            }
        }
        $result .= '<br/>';
        return $result;
    }

    function GetInschrWijzigen($ROWID) {
        $rowid = $this->GetRowFilter($ROWID);
        $_sql = 'SELECT i.INSCHRIJFNR
        , r.relatienr || \' - \' || r.VOORLETTERS || \' \' || r.ACHTERNAAM as NAAM
        , i.cursuscode || \' \' || c.cursusnaam as CURSUS
        , c.CURSUSCODE, c.ISCOHORTCURSUSVAN
        FROM INSCHRIJVING i, RELATIE r, CURSUS c
        WHERE i.cursistnr = r.relatienr
        AND i.CURSUSCODE = c.CURSUSCODE
        AND '.$rowid;
        $inschr = $this->userdatabase->GetAll($_sql);
        $_sql = 'SELECT CURSUSCODE FROM CURSUS WHERE ISCOHORTCURSUSVAN = :CURSUSCODE ORDER BY CURSUSCODE';
        $crs = $this->userdatabase->GetAll($_sql, array('CURSUSCODE'=>$inschr[0]['ISCOHORTCURSUSVAN']));
        if (count($crs) == 0) {
            $result = '<br/> &ndash; Er zijn geen records gevonden.';
        } else {
            $result = '<br/>Inschrijving: '.$inschr[0]['INSCHRIJFNR'].'<br/>Relatie: '.$inschr[0]['NAAM'].'<br/>Cohort: <select name="param2">';
            foreach($crs as $rec) {
                $selected = $inschr[0]['CURSUSCODE'] == $rec['CURSUSCODE'] ? 'selected=\'selected\'' : '';
                $result .= "<option value=\"{$rec['CURSUSCODE']}\" $selected>{$rec['CURSUSCODE']}</option>";
            }
            $result .= '</select>';
        }
        return $result;
    }

    function GetInschrijvingen($relatienr) {
        $_sql = 'SELECT INSCHRIJFNR, CURSUSCODE, DATUMINSCHRIJVING FROM INSCHRIJVING WHERE CURSISTNR = :RELATIENR ORDER BY DATUMINSCHRIJVING DESC, INSCHRIJFNR DESC ';
        $inschr = $this->userdatabase->GetAll($_sql, array('RELATIENR'=>$relatienr));
        $result = '';
        foreach($inschr as $rec) {
            $result .= "<option value=\"{$rec['INSCHRIJFNR']}\">{$rec['INSCHRIJFNR']} - {$rec['CURSUSCODE']} - {$rec['DATUMINSCHRIJVING']}</option>";
        }
        return $result;
    }

    function GetRelatie($inschrijfnr) {
        $_sql = 'SELECT CURSISTNR FROM INSCHRIJVING WHERE INSCHRIJFNR = :INSCHRIJFNR ';
        $relnr = $this->userdatabase->GetOne($_sql, array('INSCHRIJFNR'=>$inschrijfnr));
        return $relnr;
    }

    function GetBetalingsVerzoek($ROWID) {
        $rowid = $this->GetRowFilter($ROWID);
        $_sql = 'SELECT BETALINGSVERZOEKNR, BETAALWIJZECODE, LANDCODE
        FROM BETALINGSVERZOEK I, INSCHRIJVING I2
        WHERE I.INSCHRIJFNR = I2.INSCHRIJFNR AND
        '.$rowid;
        $_result = $this->userdatabase->GetRow($_sql);
        return $_result;
    }

    function GetBetaaldagen() {
        $_sql = "SELECT AUTOBETAALDAG FROM AUTOMATISCHEBETAALDAG";
        $_rs = $this->userdatabase->GetAll($_sql);
        foreach($_rs as $_rec) {
            $_result .= $_rec['AUTOBETAALDAG'].', ';
        }
        $_result = substr($_result, 0, -2);
        return $_result;
    }

    function UnsubscribeLettersPrinted() {
        $_delOplEnBTV = "
            DELETE FROM NHA.OPLENBTV_VOOR_BRIEF
            WHERE inschrijfNr IN (SELECT INSCHRIJFNR FROM NHA.TEPRINTEN_BRIEVEN B WHERE B.PRINTDATUMOPZEGBRIEF is NULL)
        ";

        $this->userdatabase->Execute($_delOplEnBTV);

        $_updateTePrintenBrieven = "
            UPDATE NHA.TEPRINTEN_BRIEVEN
            SET PRINTDATUMOPZEGBRIEF = SYSDATE
            WHERE PRINTDATUMOPZEGBRIEF IS NULL
        ";

        $this->userdatabase->Execute($_updateTePrintenBrieven);
    }

    function PrintUnsubscribeLetters() {
        global $_CONFIG;

        // PRINTER : PrinterOpzegRetourBrief
        // RAPPORT : 'opzegretourbrief.rpt'

        $_zoekTePrintenBrieven = 'SELECT B.RowId, B.* FROM TEPRINTEN_BRIEVEN B WHERE B.PRINTDATUMOPZEGBRIEF is NULL';
        // print in batch
        $reporturl = str_replace("%REPORTNAME%",'/reports/Macs/OpzegRetourBrief',$_CONFIG["reportserverurl"]);
        $reporturl = str_replace("%REPORTPARAMS%",'',$reporturl);
        $reporturl .= '&output=pdf&j_username=jasperadmin&j_password=jasperadmin';
        //        $linkhref = $_po->ReplaceDynamicVariables($reporturl, '', $rec)

        header("Content-type:application/pdf");
        header("Content-Disposition:attachment;filename='teprintenbrieven.pdf'");

        readfile($reporturl);
    }

    function ExecTplTotalenPickLijstRapportPrinted() {
        $datumVeld = 'PRINTDATUMTOTALENPICKLIJST';
        $this->userdatabase->Execute("UPDATE TOTALENPICKLIJST SET $datumVeld = SYSDATE WHERE $datumVeld IS NULL");
        return true;
    }

    function ExecTplVervolgRapportenBeforePrint() {
        $datumVeld = 'PRINTDATUMCURSISTENPICKLIJST';

/*
      ExecOracleProcedure(
    DMCursistAdmin.OracleSession,
    GetDbSchema() + '.Packlogistiek',
    'VulOplDatumVerstuurd',
    [ARowId]
  );

*/
//        $this->userdatabase->Execute("UPDATE TOTALENPICKLIJST SET $datumVeld = SYSDATE WHERE $datumVeld IS NULL");
        return true;
    }

    function ChangeBtvAmountArticleSale($params, &$error) {
        global $polaris;
        $rsOPL = $this->GetOrderPickLijstRegel($params['ROWID'], $error);

        if (!$error) {
            try {
                $result = $this->dbobject->updateRecord($tablename='ORDERPICKLIJSTREGEL'
                    , array("ORDERPICKLIJSTNR"=>$rsOPL['ORDERPICKLIJSTNR'], "ORDERPICKLIJSTREGELNR"=>$rsOPL['ORDERPICKLIJSTREGELNR'])
                    , array(
                        "AANTAL"=>$params['param1'],
                        "PRIJSPERSTUKINEUROS"=>$polaris->ConvertFloatValue($params['param2'])
                      )
                );
            } catch (Exception $e) {
//                echo $polaris->getPrettyErrorMessage($e->errno, $e->errmsg);
            }
            return $result;
        } else {
            $error = "U heeft niet de juiste OPL gekozen.";
            return false;
        }
    }

    function GetOrderPickLijstRegel($ROWID, &$error) {
        $rowid = $this->dbobject->customUrlDecode($ROWID);

        $_zoekBTV = 'SELECT B.BetalingsKenmerk, B.OrderPickLijstNr, B.OrderPickLijstRegelNr
        FROM BETALINGSVERZOEK B
        WHERE B.ROWID = CHARTOROWID(\''.$rowid.'\') ';

        $rsBTV = $this->userdatabase->GetRow($_zoekBTV);

        if (empty($rsBTV)) {
            $error = 'Betalingsverzoek niet gevonden';
            return false;
        }

        if ($rsBTV['BETALINGSKENMERK'] != 'A') {
            $error = 'Dit is geen betalingsverzoek van een artikelverkoop';
            return false;
        }

        $_zoekOPL = 'SELECT R.RowId, R.* FROM OrderPickLijstRegel R
        WHERE R.OrderPickLijstNr = ' .$rsBTV['ORDERPICKLIJSTNR']. ' AND
        R.OrderPickLijstRegelNr = ' .$rsBTV['ORDERPICKLIJSTREGELNR'];

        $rsOPL = $this->userdatabase->GetRow($_zoekOPL);
        if (empty($rsOPL)) {
            $error = 'Bijbehorende orderpicklijstregel niet gevonden';
            return false;
        }

        if ($rsOPL['PICKTYPESOORT'] != 'A') {
            $error = 'Bijbehorende orderpicklijstregel is geen artikelverkoop (PickTypeSoort <> A)';
            return false;
        }
        return $rsOPL;
    }

    function GetVooropleidingOverzicht($ROWID) {
        $rowid = $this->GetRowFilter($ROWID);
        $_sql = 'SELECT i.INSCHRIJFNR
        , r.relatienr || \' - \' || r.VOORLETTERS || \' \' || r.ACHTERNAAM as NAAM
        , i.cursuscode || \' \' || c.cursusnaam as CURSUS
        , c.CURSUSCODE
        , v.VOOROPLEIDING, v.DIPLOMAJANEE
        FROM INSCHRIJVING i, RELATIE r, CURSUS c, VOOROPLEIDING v
        WHERE i.cursistnr = r.relatienr
        AND r.relatienr = v.relatienr
        AND i.CURSUSCODE = c.CURSUSCODE
        AND '.$rowid;
        $inschr = $this->userdatabase->GetAll($_sql);
        $result = '<br/>';
        $_vooropleidingen = array(
              'LBO_VBO' => 'LBO/VBO'
            , 'MAVO_VMBO' => 'MAVO/VMBO'
            , 'HAVO' => 'HAVO (t/m jr 3)'
            , 'VWO' => 'VWO (t/m jr 3)'
            , 'MBO' => 'MBO'
            , 'HBO' => 'HBO'
            , 'BACH' => 'Bachelor'
        );
        $_vooropleidingenKeys = array_keys($_vooropleidingen);

        $result = '<div class="vooropleidingen">';
        foreach($_vooropleidingen as $_k => $_vooropleiding) {
            $result .= '<div>';
            $_vooroplChecked = '';
            $_diplomaChecked = '';
            $_found = FALSE;
            $_name = $_k;
            $_label = $_vooropleiding;
            foreach($inschr as $rec) {
                if ( in_array(strtolower($rec['VOOROPLEIDING']), explode('_', strtolower($_k))) !== FALSE
                or strtolower($rec['VOOROPLEIDING']) === strtolower($_k) ) {
                    $_found = TRUE;
                    $_vooroplChecked = 'checked="checked"';
                    if ($rec['DIPLOMAJANEE'] == 'J') {
                        $_diplomaChecked = 'checked="checked"';
                    }
                    break;
                } else {
                }
            }
            $result .= '<span style="display:inline-block;min-width:13em"><input type="checkbox" '.$_vooroplChecked.' class="OPL" name="VOOROPLEIDINGEN[]" value="'.$_name.'">'.$_label.'</span>';
            $result .= '<span>Diploma? <input type="checkbox" '.$_diplomaChecked.' class="DPL" name="DIPLOMAS[]" value="'.$_name.'"></span>';
            $result .= '</div>';
        }
        foreach($inschr as $rec) {
            $_vooroplChecked = '';
            $_diplomaChecked = '';
            $_missing = TRUE;
            foreach($_vooropleidingen as $_k => $_vooropleiding) {
                $_name = $_k;
                $_label = $_vooropleiding;
                if ( in_array(strtolower($rec['VOOROPLEIDING']), explode('_', strtolower($_k))) == TRUE
                or strtolower($rec['VOOROPLEIDING']) == strtolower($_k)
                ) {
                    $_missing = FALSE;
                    break;
                } else {

                }
            }
            if ($_missing) {
                $_checked = $rec['DIPLOMAJANEE'] == 'J' ? 'checked="checked"' : '';
                $result .= 'Anders: <input type="text" name="VOOROPLEIDING_ANDERS" style="width:100px" value="'.$rec['VOOROPLEIDING'].'"> Diploma? <input type="checkbox" name="VOOROPLDIPLOMA_ANDERS" '.$_checked.' value="'.$rec['VOOROPLEIDING'].'" /><br/>';
                break; // maximaal één ANDERS vooropleiding
            }
        }
        $result .= '</div>';
        $result .= '<script type="text/javascript">
        $(document).ready(function() {
            $(".DPL").change(function() {
                if ($(this).prop("checked")) {
                    $(this).closest("div").find(".OPL").prop("checked", true);
                }
            });
            $(".OPL").change(function() {
                if (!$(this).prop("checked"))
                    $(this).closest("div").find(".DPL").prop("checked", false);
            });
        });
        </script>';
        return $result;
    }

    function _GetInschrijfSetOverzicht($ROWID, $kolomNaam, $single=FALSE) {
        if ($single === TRUE) {
            $_table = 'INSCHRIJVING_PRE';
        } else {
            $_table = 'INSCHRIJVING_BO_ONDERTEK';
        }

        $rowid = $this->GetRowFilter($ROWID);
        $_sql = 'SELECT i.INSCHRIJFNR
        , r.relatienr || \' - \' || r.VOORLETTERS || \' \' || r.ACHTERNAAM as NAAM
        , i.cursuscode || \' \' || c.cursusnaam as CURSUS
        , NVL(r.email, \'onbekend\') email
        FROM '.$_table.' i, RELATIE r, CURSUS c
        WHERE i.cursistnr = r.relatienr
        AND i.CURSUSCODE = c.CURSUSCODE
        AND NVL(i.'.$kolomNaam.', \'N\') = \'N\'';
        if (isset($rowid) and $rowid !== FALSE) {
            $_sql .= ' AND '.$rowid;
        }
        return $this->userdatabase->Execute($_sql);
    }

    function GetEPostOverzicht($ROWID) {
        $inschr = $this->_GetInschrijfSetOverzicht($ROWID, 'INSFRM_AFGEDRUKT');

        include('tohtml.inc.php');
        $result = '<div class="bodyscroll scrollarea"><br/>';
        $result .= rs2html($inschr, 'id="datalistview" class="data striped nowrap group-table right" cellspacing="0" cellpadding="0"',$zheaderarray=array('Inschrijfnr','Relatie','Cursus', 'Emailadres'),$htmlspecialchars=true,$echo = false);
        $result .= '</div>';
        return $result;
    }

    function GetMailRelatie($ROWID) {
        $rowid = $this->GetRowFilter($ROWID);
        if (isset($rowid) and $rowid !== FALSE) {
            $_sql = "SELECT INSCHRIJFNR, INSFRM_GEMAILD, INSFRM_MAILDATUM, INSCHRIJFFORM_PER_EMAIL_JN, EMAIL, NETTENAAM
            FROM INSCHRIJVING_PRE I
            WHERE $rowid";

            $inschr = $this->userdatabase->Execute($_sql);

            $result = '<br/>';
            $result .= "<div style=\"max-height:200px;overflow:auto\">";
            foreach($inschr as $rec) {
                if ($rec['EMAIL'] == '') {
                    $result .= "<p><b>{$rec['NETTENAAM']} heeft geen emailaders.</b></p>";
                    $this->actionsmarty->assign("okbuttondisabled",'disabled');
                } else {
                    $result .= "<table class=\" datax\" >";
                    $result .= "<thead><tr><th>Inschrijfnr</ht><th>Naam</ht><th>Mailadres</ht><th>Al verstuurd?</th><th>Verstuurd op</th></tr></thead>";
                    $result .= "<tbody>";
                    $result .= "<tr>";
                    $_IsAlGemailed = ($rec['INSFRM_GEMAILD'] == 'J')?'Ja':'Nee';

                    $result .= "<td>{$rec['INSCHRIJFNR']}</td><td>{$rec['NETTENAAM']}</td><td>{$rec['EMAIL']}</td><td>{$_IsAlGemailed}</td><td>{$rec['INSFRM_MAILDATUM']}</td>";
                    $result .= "</tr>";
                    $result .= "</tbody>";
                    $result .= "</table>";
                }
            }
            if ($rec['INSCHRIJFFORM_PER_EMAIL_JN'] !== 'J') {
                $result .= "<p><b>Deze cursist hoeft geen inschrijfformulier per mail te ontvangen. Weet u zeker dat u door wilt gaan?</b></p>";
            }
            $result .= "</div>";

            return $result;
        }
    }

    function GetMailOverzicht($ROWID) {
        $inschr = $this->_GetInschrijfSetOverzicht($ROWID, 'INSFRM_GEMAILD');

        $result = '<br/>';
        $result .= "<div style=\"max-height:200px;overflow:auto\">";
        $result .= "<table class=\" datax\" >";
        $result .= "<thead><tr><th>Inschrijfnr</ht><th>Naam</ht><th>Mailadres</ht><th><input type=\"checkbox\" id=\"_fldVERWERKLEGEEMAIL\" name=\"VERWERKLEGEEMAILALL\" /> <label for=\"_fldVERWERKLEGEEMAIL\">Geen mailadres, toch verwerken? </label></ht></tr></thead>";
        $result .= "<tbody>";
        foreach($inschr as $rec) {
            $result .= "<tr>";
            $_input = '';
            if ($rec['EMAIL'] == 'onbekend') {
                $_input  = '<input type="checkbox" name="VERWERKLEGEEMAIL[]" value="'.$rec['INSCHRIJFNR'].'" />';
            }
            $result .= "<td>{$rec['INSCHRIJFNR']}</td><td>{$rec['NAAM']}</td><td>{$rec['EMAIL']}</td><td>$_input</td>";
            $result .= "</tr>";
        }
        $result .= "</tbody>";
        $result .= "</table>";
        $result .= "</div>";

        $result .= '<script>
        $("#_fldVERWERKLEGEEMAIL").change(function(elem) {
            $("input[name^=VERWERKLEGEEMAIL]").prop("checked", $(this).prop("checked"));
        })
        </script>
        ';

        return $result;
    }

    function TPLDatumSelect(&$totaal) {
        $_sql = "
        SELECT DISTINCT DATUM, COUNT(*) AS AANTAL
        FROM TOTALENPICKLIJST T
        WHERE PRINTDATUMTOTALENPICKLIJST IS NULL
        GROUP BY DATUM
        ORDER BY 1
        ";
        $totaal = 0;
        $_rs = $this->userdatabase->GetAll($_sql);
        $_result = '<div class="bodyscroll">';
        foreach($_rs as $_rec) {
            $_result .= "<input type=\"checkbox\" name=\"param3[]\" class=\"dat_cbx\" value=\"${_rec['DATUM']}\" data=\"${_rec['AANTAL']}\" checked=\"checked\" /> ${_rec['DATUM']} (${_rec['AANTAL']} stuks)<br />";
            $totaal += intval($_rec['AANTAL']);
        }
        $_result .= '<script>$("input.dat_cbx").change(function() {
            var tot = 0;
            $("input.dat_cbx:checked").each(function() {
                tot += parseInt($(this).attr("data"), 10);
            });
            $("input[name=param2]").val(tot);
        });
        </script></div>';
        return $_result;
    }

    function GetAutoPrintTPL() {
        require_once('module.nha1/php/totalenpicklijst.class.php');
        $tpl = new TotalenPickLijst($this, $this->dbobject);

        return $tpl->AutoPrintActief();
    }

    function GetTotalenPickLijsten($ROWID, $printKolom, $extraFilter=false, &$error) {
        if ($ROWID == '') {
            if ($printKolom != '')
                $filter = $printKolom.' IS NULL';
        } else {
            $rowid = $this->dbobject->customUrlDecode($ROWID);
            $rowid = str_replace(',','\',\'',$rowid);
            $filter = 'T.ROWID IN (\''.$rowid.'\')';
        }
        if ($extraFilter)
            $filter .= " AND $extraFilter";

        if ($printKolom != '')
            $printKolom = ', '.$printKolom;
        $_zoekTPL = 'SELECT Datum, Rapportnr '.$printKolom.'
        FROM TOTALENPICKLIJST T
        WHERE '.$filter;

        $rsTPL = $this->userdatabase->GetAll($_zoekTPL);
        return $rsTPL;
    }

    function CustomParamsNeedValue($rec) {
        return true;
    }

    function GetDefaultLogischePrinter($kolomNaam) {
        $_sql = "SELECT $kolomNaam FROM PARAMETERS";
        $printer = $this->userdatabase->GetOne($_sql);

        return $printer;
    }

    function GetLogischePrinters() {
        global $_CONFIG;

        if ($_CONFIG['ostype'] == 'WINDOWS') {
            $_printerOS = 'PRINTER_NAAM_WIN';
        } else {
            $_printerOS = 'PRINTER_NAAM';
        }
        $_printerSQL = "SELECT NAAM, NAAM||'  &nbsp; --> &nbsp; '||NVL($_printerOS, '<ONBEKEND>')||' '||PAPIERBRON||'' AS OMSCHRIJVING FROM PRINTER_PLR_CUPS";
        $rsPrinters = $this->userdatabase->GetAssoc($_printerSQL);
        return $rsPrinters;
    }

    function VolgendTerugkoppelBestand() {
        $_sp = $this->userdatabase->PrepareSP(
        "DECLARE RETVAL VARCHAR2(20); BEGIN :RETVAL := BRON.ImportExport.VolgendTerugkoppelBestand(); END;"
        );
        $result = $this->userdatabase->OutParameter($_sp,$returnvalue,'RETVAL');
        $result = $this->userdatabase->Execute($_sp);
        return $returnvalue;
    }

    function ExecuteProcedure($params, &$error) {
        $validrowids = $this->dbobject->customUrlDecode($_GET['ROWID']);
        if ($_GET['DEBUG'] == 'DEBUG') {
//            echo '<input type="hidden" id="_fldDONTSHOWMESSAGE" value="true" />';
            echo "<div id='message' style='max-height:300px;overflow:auto;-webkit-user-select: auto !important;-khtml-user-select: auto !important;-moz-user-select: auto !important;-ms-user-select: auto !important;user-select: auto !important;'>";
        }

        switch($this->moduleid) {
        case 'ExecChangeBtvAmountArticleSale':
            $result = $this->ChangeBtvAmountArticleSale($params, $error);
            break;
        case 'ExecUnsubscribe':
            $result = $this->PrintUnsubscribeLetters();
            break;
        case 'ExecUnsubscribePrinted':
            $result = $this->UnsubscribeLettersPrinted();
            break;
        case 'ExecAcceptGiro':
        case 'ExecAcceptGiroPreview':
            require_once('module.nha1/php/factuur.class.php');
            $nhafactuur = new nha1_Factuur($this, $this->dbobject);

            $_selectedPrinter3 = $nhafactuur->GetPrinterObject($_GET['SELECTEDPRINTER3']); // NL
            $_selectedPrinter4 = $nhafactuur->GetPrinterObject($_GET['SELECTEDPRINTER4']); // BE
            $_selectedPrinter5 = $nhafactuur->GetPrinterObject($_GET['SELECTEDPRINTER5']); // DE

            if ($this->moduleid == 'ExecAcceptGiroPreview') {
                $nhafactuur->outputFactuur($validrowids, $_GET['param1'] /* Met of zonder ACP */);
            } else {
                $nhafactuur->printFactuur($validrowids, $_GET['param1'] /* Met of zonder ACP */, $_selectedPrinter3, $_selectedPrinter4, $_selectedPrinter5 );
            }
            $result = true;
            break;
        case 'ExecTplTotalenPickLijstRapport':
            require_once('module.nha1/php/totalenpicklijst.class.php');
            $tpl = new TotalenPickLijst($this, $this->dbobject);
            $result = $tpl->Afdrukken($_GET, $validrowids);
            break;
        case 'ExecTplVervolgRapporten':
        case 'ExecTplVervolgRapportenQuick':
            require_once('module.nha1/php/vervolgrapporten.class.php');
            if ($this->moduleid == 'ExecTplVervolgRapportenQuick') {
                $_rapportKolom = 'PLR_PRINTERORDERPICKLIJST';
                $_rapportKolom2 = 'PLR_PRINTERORDERPICKSTICKERLST';
                $_rapportKolom3 = 'PLR_PRINTERACCEPTGIRO';
                $_rapportKolom4 = 'PLR_PRINTERACCEPTGIROBE';
                $_rapportKolom5 = 'PLR_PRINTERACCEPTGIRODE';
                $_rapportKolom6 = 'PLR_PRINTERORDERPICKLIJSTBE';
                $_rapportKolom7 = 'PLR_PRINTERORDERPICKLIJSTDE';

                $_GET['SELECTEDPRINTER'] = $this->GetDefaultLogischePrinter($_rapportKolom);
                $_GET['SELECTEDPRINTER2'] = $this->GetDefaultLogischePrinter($_rapportKolom2);
                $_GET['SELECTEDPRINTER3'] = $this->GetDefaultLogischePrinter($_rapportKolom3);
                $_GET['SELECTEDPRINTER4'] = $this->GetDefaultLogischePrinter($_rapportKolom4);
                $_GET['SELECTEDPRINTER5'] = $this->GetDefaultLogischePrinter($_rapportKolom5);
                $_GET['SELECTEDPRINTER6'] = $this->GetDefaultLogischePrinter($_rapportKolom6);
                $_GET['SELECTEDPRINTER7'] = $this->GetDefaultLogischePrinter($_rapportKolom7);
//                $_GET['DEBUG'] = 'DEBUG';
            }

            $vr = new VervolgRapporten($this, $this->dbobject);
            $result = $vr->AllesAfdrukken($_GET, $validrowids);
            break;
        case 'ExecInschrijfformulierenOnly':
            require_once('module.nha1/php/vervolgrapporten.class.php');
            $vr = new VervolgRapporten($this, $this->dbobject);
            $result = $vr->PrintInschrijfFormulieren($_GET, $validrowids);
            break;
        case 'ExecLosseAcceptGiros':
        case 'ExecLosseAcceptGirosHerhaling':
        case 'ExecLosseAcceptGirosDrukkerij':
        case 'ExecLosseAcceptGirosHerhalingDrukkerij':
            require_once('module.nha1/php/vervolgrapporten.class.php');
            $vr = new VervolgRapporten($this, $this->dbobject);
            $result = $vr->PrintLosseFacturen($_GET);
            break;
        case 'ExecAcceptGirosOnly':
            require_once('module.nha1/php/vervolgrapporten.class.php');
            $vr = new VervolgRapporten($this, $this->dbobject);
            $result = $vr->PrintFacturen($_GET, $validrowids);
            break;
        case 'ExecTplTotalenPickLijstGroepRapport':
            require_once('module.nha1/php/vervolgrapporten.class.php');
            $vr = new VervolgRapporten($this, $this->dbobject);
            $result = $vr->PrintTotalenPicklijstGroepen($_GET, $validrowids);
            break;
        case 'BriefMateriaalIncompleet':
            /* rechtstreeks afdrukken via CUPS */
            require_once('module.nha1/php/bufferinschrijvingen.class.php');
            $br = new BufferInschrijvingen($this, $this->dbobject);
            $result = $br->MateriaalIncompleetAfdrukken($_GET, $validrowids);
            break;
        case 'BriefCohortWachtkamer':
            /* rechtstreeks afdrukken via CUPS */
            require_once('module.nha1/php/bufferinschrijvingen.class.php');
            $br = new BufferInschrijvingen($this, $this->dbobject);
            $result = $br->CohortWachtkamerBriefAfdrukken($_GET, $validrowids);
            break;
        case 'MBOHBOBrieven':
            /* rechtstreeks afdrukken via CUPS */
            require_once('module.nha1/php/bufferinschrijvingen.class.php');
            $br = new BufferInschrijvingen($this, $this->dbobject);
            switch ($_GET['param1']) {
            case 'VOOROPLEIDING':
                $result = $br->BriefVooropleidingAfdrukken($_GET, $validrowids);
                break;
            case 'VOOROPLEIDING_BO':
                $result = $br->BriefVooropleidingBackOfficeAfdrukken($_GET, $validrowids);
                break;
            case 'DIPLOMA':
                $result = $br->BriefDiplomaAfdrukken($_GET, $validrowids);
                break;
            case 'WERKERVARING':
                $result = $br->BriefWerkervaringAfdrukken($_GET, $validrowids);
                break;
            case '21BRIEF':
                $result = $br->Brief21JaarOfOuderAfdrukken($_GET, $validrowids);
                break;
            }
            break;
        case 'MBOHBOFONabellen':
            require_once('module.nha1/php/bufferinschrijvingen.class.php');
            $br = new BufferInschrijvingen($this, $this->dbobject);
            $result = $br->FONabellen($_GET, $validrowids);
            break;
        case 'MBOHBOFOVooropleiding':
            require_once('module.nha1/php/bufferinschrijvingen.class.php');
            $br = new BufferInschrijvingen($this, $this->dbobject);
            $result = $br->FOVooropleidingAanpassen($_GET, $validrowids);
            break;
        case 'MBOHBOVrijstellingOngedaanMaken':
            require_once('module.nha1/php/bufferinschrijvingen.class.php');
            $br = new BufferInschrijvingen($this, $this->dbobject);
            $result = $br->VrijstellingOngedaanMaken($_GET, $validrowids);
            break;
        case 'MBOHBOFOAfkeuren':
            require_once('module.nha1/php/bufferinschrijvingen.class.php');
            $br = new BufferInschrijvingen($this, $this->dbobject);
            $result = $br->FOAfkeuren($_GET, $validrowids);
            break;
        case 'MBOHBOEDRCheck':
            require_once('module.nha1/php/bufferinschrijvingen.class.php');
            $br = new BufferInschrijvingen($this, $this->dbobject);
            $result = $br->EDRCheck($_GET, $validrowids);
            break;
        case 'MBOHBOToetsingAkkoord':
            require_once('module.nha1/php/bufferinschrijvingen.class.php');
            $br = new BufferInschrijvingen($this, $this->dbobject);
            $result = $br->EDRCheck($_GET, $validrowids);
            break;
        case 'MBOHBOToetsingAfgekeurd':
            require_once('module.nha1/php/bufferinschrijvingen.class.php');
            $br = new BufferInschrijvingen($this, $this->dbobject);
            $result = $br->ToetsingAfgekeurd($_GET, $validrowids);
            break;
        case 'MBOHBOCheckAndereOpleiding':
            require_once('module.nha1/php/bufferinschrijvingen.class.php');
            $br = new BufferInschrijvingen($this, $this->dbobject);
            $result = $br->VolgtAndereOpleiding($_GET, $validrowids);
            break;
        case 'MBOHBOToetsingDefinitiefAfgekeurd':
            require_once('module.nha1/php/bufferinschrijvingen.class.php');
            $br = new BufferInschrijvingen($this, $this->dbobject);
            $result = $br->ToetsingDefinitiefAfgekeurd($_GET, $validrowids);
            break;
        case 'MBOHBOWijzigCohort':
            require_once('module.nha1/php/bufferinschrijvingen.class.php');
            $br = new BufferInschrijvingen($this, $this->dbobject);
            $result = $br->WijzigCohort($_GET, $validrowids);
            break;
        case 'ExecCallOpmerking':
            require_once('module.nha1/php/callopmerkingen.class.php');
            $call = new CallOpmerkingen($this, $this->dbobject);
            $result = $call->Opslaan($_GET, $error);
            break;
        case 'ImporteerTerugkoppel':
            require_once('module.nha_bron/php/bron.class.php');
            $bron = new Bron($this, $this->dbobject);
            $result = $bron->DoImportVanBron($_GET, $error);
            break;
        case 'ExecDatumOpschuiven':
            require_once('module.nha1/php/bufferinschrijvingen.class.php');
            $br = new BufferInschrijvingen($this, $this->dbobject);
            $result = $br->ExecDatumOpschuiven($_GET, $error);
            break;
        case 'ExecAutoPrint':
            require_once('module.nha1/php/totalenpicklijst.class.php');
            $tpl = new TotalenPickLijst($this, $this->dbobject);
            $tpl->AutoPrintOptie($_GET['param1'] == 'AUTOPRINTJA');
            $result = true;
            break;
        case 'ExecAutoInschrFormEmailen':
            require_once('module.nha1/php/bufferinschrijvingen.class.php');
            $tpl = new BufferInschrijvingen($this, $this->dbobject);
            $tpl->AutoInschrijfFormEmailenOptie($_GET['param1'] == 'AUTOEMAILJA');
            $result = true;
            break;
        case 'InschrijfformAfdrukken':
            /* rechtstreeks afdrukken via CUPS */
            require_once('module.nha1/php/bufferinschrijvingen.class.php');
            $br = new BufferInschrijvingen($this, $this->dbobject);
            //$result = $br->BriefInschrijfFormsAfdrukken($_GET, $validrowids);
            $result = $br->BriefInschrijfFormsOpslaan($_GET, $validrowids);
            break;
        case 'InschrijfformMailen':
        case 'InschrijfformMailenSingle':
            require_once('module.nha1/php/bufferinschrijvingen.class.php');
            $br = new BufferInschrijvingen($this, $this->dbobject);
            $result = $br->BriefInschrijfFormsMailen($_GET, $validrowids, $this->moduleid, $error);
            break;
        case 'InschrijfformOndertekend':
            require_once('module.nha1/php/bufferinschrijvingen.class.php');
            $br = new BufferInschrijvingen($this, $this->dbobject);
            $result = $br->ExecHandtekeningOntvangen($_GET, $validrowids, $error);
            break;

        case 'ExecStandaardBriefVoorvertoning':
            require_once('module.nha1/php/briefvoorvertoning.class.php');
            $br = new BriefVoorvertoning($this, $this->dbobject);
            $result = $br->Voorvertoning($_GET, $validrowids, $error);
            echo $result;
            $_GET['DEBUG'] = 'true';
            $result = false;
            break;
/*
        case 'ExecMateriaalIncompleetAfgedrukt':
            $br = new BriefMateriaalIncompleet($this, $this->dbobject);
            $_belgie = html_entity_decode($_GET['LANDCODE']) == '=\'BE\'';
            $result = $br->BrievenZijnAfgedrukt($_belgie);
            break;
        case 'ExecTplTotalenPickLijstRapportPrinted':
            $result = $this->ExecTplTotalenPickLijstRapportPrinted();
            break;
*/
        }
        return $result;
    }

    function DisplayCustomParameterForm($moduleid, $params) {
        global $_GVARS;
        global $polaris;

        $this->actionsmarty->assign("debugchecked", $_SERVER['SERVER_ADDR'] == '10.211.55.3'?'checked="checked"':'');

        switch($moduleid) {
        case 'ExecChangeBtvAmountArticleSale':
            $rsOPL = $this->GetOrderPickLijstRegel($params['ROWID'], $error);

            $prijsperstuk = $rsOPL['PRIJSPERSTUKINEUROS'];
            $prijsperstuk = floatval(str_replace(',','.',$prijsperstuk));
            $prijsperstuk = number_format(floatval($prijsperstuk), 2, ',','.');

            // >> vraag om Aantal en PrijsPerStukInEuros (en toon ze ook via $_zoekOPL)

            $this->actionsmarty->assign("error",$error);

            $this->actionsmarty->assign("dialogtitle",$this->currentaction->record->TITLE);
            $this->actionsmarty->assign("dialogtext",$this->currentaction->record->TEXT);
            $this->actionsmarty->assign("moduleid",$this->currentaction->record->MODULEID);

            $this->actionsmarty->assign("label1","Aantal");
            $this->actionsmarty->assign("param1","<input type=\"text\" class=\"required form-control\" name=\"param1\" autofocus=\"autofocus\" value=\"{$rsOPL['AANTAL']}\" />");
            $this->actionsmarty->assign("label2","Prijs per stuk");
            $this->actionsmarty->assign("param2","<input type=\"text\" class=\"required form-control validation_float\" data-v-max=\"9999999.99\" data-m-dec=\"2\" name=\"param2\" value=\"{$prijsperstuk}\" />");

            $this->actionsmarty->display("parameterform.tpl.php");

            break;
        case 'ExecLosseAcceptGiros':
        case 'ExecLosseAcceptGirosHerhaling':
        case 'ExecLosseAcceptGirosDrukkerij':
        case 'ExecLosseAcceptGirosHerhalingDrukkerij':
            $_printKolom = 'PRINTDATUMFACTUREN';

            if ($moduleid == 'ExecLosseAcceptGirosHerhalingDrukkerij' or $moduleid == 'ExecLosseAcceptGirosDrukkerij') {
                $_rapportKolomNL = 'PLR_PRINTERACCEPTGIRO_DR';
                $_rapportKolomBE = 'PLR_PRINTERACCEPTGIROBE_DR';
                $_rapportKolomDE = 'PLR_PRINTERACCEPTGIRODE_DR';
            } else {
                $_rapportKolomNL = 'PLR_PRINTERACCEPTGIRO';
                $_rapportKolomBE = 'PLR_PRINTERACCEPTGIROBE';
                $_rapportKolomDE = 'PLR_PRINTERACCEPTGIRODE';
            }
            $_extraFilter = '';

            $this->actionsmarty->assign("label1","Soort facturen");
            $this->actionsmarty->assign("param1","<input type=\"radio\" class=\"required\" name=\"param1\" id=\"radVervallen\" value=\"VERVALLEN\" /> <label for=\"radVervallen\">Vervallen facturen</label>
            &nbsp; <input type=\"radio\" class=\"required\" name=\"param1\" id=\"radZuiver\" value=\"ZUIVERE\" checked=\"checked\" /> <label for=\"radZuiver\">Zuivere facturen</label>
            &nbsp; <input type=\"radio\" class=\"required\" name=\"param1\" id=\"radVorderingen\" value=\"VORDERINGEN\" /> <label for=\"radVorderingen\">Vorderingen</label>
            &nbsp; <input type=\"radio\" class=\"required\" name=\"param1\" id=\"radAlles\" value=\"ALLES\" /> <label for=\"radAlles\">Alles</label> <br /><br />");

            $this->actionsmarty->assign("label2","Regio");
            $this->actionsmarty->assign("param2","<input type=\"radio\" name=\"param2\" id=\"radRegioBE\" value=\"BELGIE\" /> <label for=\"radRegioBE\">Belgie</label>
            &nbsp; <input type=\"radio\" name=\"param2\" id=\"radRegioDE\" value=\"DUITSLAND\" xdisabled=\"disabled\" checked=\"checked\" /> <label for=\"radRegioDE\" disabled=\"disabled\"><strike>Duitsland</strike></label>
            &nbsp; <input type=\"radio\" name=\"param2\" id=\"radRegioNL\" value=\"NEDERLAND\" checked=\"checked\" /> <label for=\"radRegioNL\">Nederland en rest</label>
            &nbsp; <input type=\"radio\" name=\"param2\" id=\"radRegioAlles\" value=\"ALLES\" /> <label for=\"radRegioAlles\">Alles</label><br /><br />");

            $this->actionsmarty->assign("label3","Soort academie");
            $this->actionsmarty->assign("param3","<input type=\"radio\" class=\"required\" name=\"param3\" id=\"radAcaNHA\" checked=\"checked\" value=\"NHA\" /> <label for=\"radAcaNHA\">NHA</label>
            &nbsp; <input type=\"radio\" class=\"required\" name=\"param3\" id=\"radAcaHA\" value=\"HA\" /> <label for=\"radAcaHA\">HA</label>
            &nbsp; <input type=\"radio\" class=\"required\" name=\"param3\" id=\"radAcaAlles\" value=\"ALLES\" /> <label for=\"radAcaAlles\">Alles</label><br /><br />");

            $this->actionsmarty->assign("error",$error);

            $_title = $this->currentaction->record->TITLE;
            if ($moduleid == 'ExecLosseAcceptGirosHerhalingDrukkerij' or $moduleid == 'ExecLosseAcceptGirosDrukkerij') {
                $_title .= " als Batch-opdracht";
            }
            $this->actionsmarty->assign("dialogtitle",$_title);
            $this->actionsmarty->assign("dialogtext",$this->currentaction->record->TEXT);
            $this->actionsmarty->assign("moduleid",$this->currentaction->record->MODULEID);
            $this->actionsmarty->assign("printerselectie",$this->GetLogischePrinters());
            $this->actionsmarty->assign("defaultprinter3",$this->GetDefaultLogischePrinter($_rapportKolomNL));
            $this->actionsmarty->assign("defaultprinter4",$this->GetDefaultLogischePrinter($_rapportKolomBE));
            $this->actionsmarty->assign("defaultprinter5",$this->GetDefaultLogischePrinter($_rapportKolomDE));

            $this->actionsmarty->assign("okbuttondisabled",$_okButtonDisabled?'disabled':'');

            if ($moduleid == 'ExecLosseAcceptGirosHerhaling' or $moduleid == 'ExecLosseAcceptGirosHerhalingDrukkerij') {
                $this->actionsmarty->assign("label4","Af te drukken datum");
                $this->actionsmarty->assign("param4","<input type=\"text\" name=\"param4\" value=\"".date("d-m-Y")."\" />");
                $this->actionsmarty->assign("label5","Nogmaals afdrukken?");
                $this->actionsmarty->assign("param5","<input type=\"hidden\" name=\"param5\" value=\"opnieuwafdrukken\" />Ja");
            }

            if ($moduleid == 'ExecLosseAcceptGirosHerhalingDrukkerij' or $moduleid == 'ExecLosseAcceptGirosDrukkerij') {
                $this->actionsmarty->assign("label6","Afdrukken in Batches?");
                $this->actionsmarty->assign("param6","<input type=\"hidden\" name=\"param6\" value=\"batch\" /> Ja");
            }

            $this->actionsmarty->display("parameterform.tpl.php");

            break;
        case 'ExecAcceptGiroPreview':
            $this->actionsmarty->assign("dialogtitle",$this->currentaction->record->TITLE);
            $this->actionsmarty->assign("dialogtext",$this->currentaction->record->TEXT);
            $this->actionsmarty->assign("moduleid",$this->currentaction->record->MODULEID);

            $this->actionsmarty->display("parameterform.tpl.php");
            break;
        case 'ExecAcceptGiro':
//            $rsOPL = $this->GetOrderPickLijstRegel($params['ROWID'], $error);
            $_rapportKolomNL = 'PLR_PRINTERACCEPTGIRO';
            $_rapportKolomBE = 'PLR_PRINTERACCEPTGIROBE';
            $_rapportKolomDE = 'PLR_PRINTERACCEPTGIRODE';

            $this->actionsmarty->assign("error",$error);

//            $this->actionsmarty->assign("target","_blank");

            $this->actionsmarty->assign("dialogtitle",$this->currentaction->record->TITLE);
            $this->actionsmarty->assign("dialogtext",$this->currentaction->record->TEXT);
            $this->actionsmarty->assign("moduleid",$this->currentaction->record->MODULEID);

            $this->actionsmarty->assign("label1","Met of zonder Acceptgiro?");
            $this->actionsmarty->assign("param1","<input type=\"radio\" class=\"required\" name=\"param1\" value=\"METACP\" checked=\"checked\" /> Met &nbsp; <input type=\"radio\" class=\"required\" name=\"param1\" value=\"ZONDERACP\" /> Zonder");

            $this->actionsmarty->assign("label2","Druk ook automatische incasso af.");
            $this->actionsmarty->assign("param2","<input type=\"hidden\" name=\"param2\" value=\"INCAUT\" /> Ja");

            $this->actionsmarty->assign("printerselectie",$this->GetLogischePrinters());
            $this->actionsmarty->assign("defaultprinter3",$this->GetDefaultLogischePrinter($_rapportKolomNL));
            $this->actionsmarty->assign("defaultprinter4",$this->GetDefaultLogischePrinter($_rapportKolomBE));
            $this->actionsmarty->assign("defaultprinter5",$this->GetDefaultLogischePrinter($_rapportKolomDE));

            $this->actionsmarty->display("parameterform.tpl.php");

            break;
        case 'ExecTplTotalenPickLijstRapport':
        case 'ExecTplVervolgRapporten':
        case 'ExecInschrijfformulierenOnly':
        case 'ExecAcceptGirosOnly':
        case 'ExecTplTotalenPickLijstGroepRapport':
            /**
            * Specifieke toewijzingen
            */
            if ($moduleid == 'ExecTplTotalenPickLijstRapport') {
                $_soortLijstLabel = 'totalenpicklijst';
                $_soortLijstenLabel = 'totalenpicklijsten';
                $_printKolom = 'PRINTDATUMTOTALENPICKLIJST';
                $_rapportKolom = 'PLR_PRINTERTOTALENPICKLIJST';
                $_rapportKolom6 = 'PLR_PRINTERTOTALENPICKLIJSTBE';
                $_rapportKolom7 = 'PLR_PRINTERTOTALENPICKLIJSTDE';
                $_extraFilter = '';
            } elseif ($moduleid == 'ExecTplVervolgRapporten') {
                $_soortLijstLabel = 'deze vervolglijst';
                $_soortLijstenLabel = 'deze vervolglijsten';
                $_printKolom = 'PRINTDATUMCURSISTENPICKLIJST';
                $_rapportKolom = 'PLR_PRINTERORDERPICKLIJST';
                $_rapportKolom2 = 'PLR_PRINTERORDERPICKSTICKERLST';
                $_rapportKolom3 = 'PLR_PRINTERACCEPTGIRO';
                $_rapportKolom4 = 'PLR_PRINTERACCEPTGIROBE';
                $_rapportKolom5 = 'PLR_PRINTERACCEPTGIRODE';
                $_rapportKolom6 = 'PLR_PRINTERORDERPICKLIJSTBE';
                $_rapportKolom7 = 'PLR_PRINTERORDERPICKLIJSTDE';
                $_extraFilter = 'PRINTDATUMTOTALENPICKLIJST IS NOT NULL';
            } elseif ($moduleid == 'ExecInschrijfformulierenOnly') {
                $_soortLijstLabel = 'dit inschrijfformulier';
                $_soortLijstenLabel = 'deze inschrijfformulieren';
                $_printKolom = 'PRINTDATUMINSCHRIJFFORMULIEREN';
                $_rapportKolom = 'PLR_PRINTERINSCHRIJFFORMULIER';
                $_extraFilter = '';
            } elseif ($moduleid == 'ExecAcceptGirosOnly') {
                $_soortLijstLabel = 'deze accepgiro';
                $_soortLijstenLabel = 'deze acceptgiro\'s';
                $_printKolom = 'PRINTDATUMFACTUREN';
                $_rapportKolom3 = 'PLR_PRINTERACCEPTGIRO';
                $_rapportKolom4 = 'PLR_PRINTERACCEPTGIROBE';
                $_rapportKolom5 = 'PLR_PRINTERACCEPTGIRODE';
                $_extraFilter = '';
            } elseif ($moduleid == 'ExecTplTotalenPickLijstGroepRapport') {
                $_soortLijstLabel = 'dit groeprapport';
                $_soortLijstenLabel = 'deze groeprapporten';
                $_printKolom = '';
                $_rapportKolom = 'PLR_PRINTERTOTALENPICKLIJSTGRP';
                $_extraFilter = '';
            }

            $rsTPL = $this->GetTotalenPickLijsten($params['ROWID'], $_printKolom, $_extraFilter, $error);
            $this->actionsmarty->assign("error",$error);

//            $this->actionsmarty->assign("target","_blank");

            $this->actionsmarty->assign("dialogtitle",$this->currentaction->record->TITLE);
            $this->actionsmarty->assign("moduleid",$this->currentaction->record->MODULEID);

            if (count($rsTPL) == 0) {
                if ($params['ROWID'] == '')
                   $dialogtext = "<span class=\"att\">Er zijn geen totalenpicklijsten om af te drukken.</span>";
                else
                   $dialogtext = "<span class=\"att\">Druk eerst de totalenpicklijst af.</span>";
            } else {
                $afgedruktcount = 0;
                foreach($rsTPL as $_rec) {
                    if (isset($_rec[$_printKolom])) $afgedruktcount++;
                }

                $count = count($rsTPL);
                if ($afgedruktcount > 0) {
                    if ($count == 1 and $afgedruktcount == 1) {
                        $dialogtext = ucfirst($_soortLijstLabel.' is al eens afgedrukt op '.$rsTPL[0][$_printKolom].'.');
                    } elseif ($count == $afgedruktcount) {
                        $dialogtext .= $afgedruktcount.' '.$_soortLijstenLabel.' '.($afgedruktcount==1?'is':'zijn').' al eens afgedrukt.';
                    } else {
                        $dialogtext = "U gaat $count $_soortLijstLabel$meervoud afdrukken.<br />";
                        $dialogtext .= $afgedruktcount.' van deze '.$_soortLijstenLabel.' '.($afgedruktcount==1?'is':'zijn').' al eens afgedrukt.';
                    }
                    $this->actionsmarty->assign("label1","Wilt u deze nog eens afdrukken?");
                    $this->actionsmarty->assign("param1","<input type=\"radio\" class=\"required\" name=\"param1\" value=\"NOGEENS\" checked=\"checked\" /> Ja &nbsp; <input type=\"radio\" class=\"required\" name=\"param1\" value=\"NIETNOGEENS\" /> Nee");
                    if ($moduleid == 'ExecTplVervolgRapporten') {
                        $this->actionsmarty->assign("label4","Facturen apart afdrukken?");
                        $this->actionsmarty->assign("param4","<input type=\"checkbox\" name=\"param4\" checked=\"checked\" value=\"FACTURENLOS\" />");
                    }

                    if ($moduleid == 'ExecTplVervolgRapporten') {
                        $this->actionsmarty->assign("label2","Wilt u de informatieformulieren afdrukken?");
                        $this->actionsmarty->assign("param2","<input type=\"radio\" class=\"required\" name=\"param2\" checked=\"checked\" value=\"PRINTINFOFORM\" /> Ja &nbsp; <input type=\"radio\" class=\"required\" name=\"param2\" value=\"NOPRINTINFOFORM\" /> Nee");
                    }

                    $this->actionsmarty->assign("param5","<input type=\"hidden\" name=\"param5\" value=\"REPRINT\" />");
                } else {
                    if ($params['ROWID'] == '') {
                        if ($moduleid == 'ExecTplVervolgRapporten') {
                            $_okButtonDisabled = true;
                            $dialogtext = "U dient eerst een of meerdere picklijsten aan te vinken.";
                        } else {
                            $dialogtext = $this->currentaction->record->TEXT." ({$count} stuks)";

                            if ($moduleid == 'ExecTplTotalenPickLijstRapport') {
                                $dialogtext = "";
                                $this->actionsmarty->assign("label3","Welke data wilt u afdrukken?");
                                $this->actionsmarty->assign("param3",$this->TPLDatumSelect($_totaal));
                                $this->actionsmarty->assign("label2","Hoeveel totaalpicklijsten wilt u nu afdrukken?");
                                $this->actionsmarty->assign("param2","<input type=\"text\" name=\"param2\" checked=\"checked\" value=\"$_totaal\" placeholder=\"aantal\" />");
                            }
                        }
                    } else {
                        $count = count($rsTPL);
                        $meervoud = $_soortLijstLabel;
                        if ($count > 1) {
                            $counttext = $count;
                            $meervoud = $_soortLijstenLabel;
                        } else {
                            $itemtext = "({$rsTPL[0]['DATUM']} - {$rsTPL[0]['RAPPORTNR']})";
                        }
                        if ($moduleid == 'ExecTplVervolgRapporten') {
                            $this->actionsmarty->assign("label4","Facturen apart afdrukken?");
                            $this->actionsmarty->assign("param4","<input type=\"checkbox\" name=\"param4\" checked=\"checked\" value=\"FACTURENLOS\" />");
                        }
                        $dialogtext = "Wilt u $counttext $meervoud $itemtext afdrukken?";
                    }
                }
            }
            $this->actionsmarty->assign("dialogtext",$dialogtext);
            $this->actionsmarty->assign("printerselectie",$this->GetLogischePrinters());

            // Indien het de VervolgRapporten zijn dan kunnen deze op vier printers worden afgedrukt (sticker, normaal A4, Factuur, FactuurBE)
            if ($_rapportKolom) { $this->actionsmarty->assign("defaultprinter",$this->GetDefaultLogischePrinter($_rapportKolom)); }
            if ($_rapportKolom2) { $this->actionsmarty->assign("defaultprinter2",$this->GetDefaultLogischePrinter($_rapportKolom2)); }
            if ($_rapportKolom3) { $this->actionsmarty->assign("defaultprinter3",$this->GetDefaultLogischePrinter($_rapportKolom3)); }
            if ($_rapportKolom4) { $this->actionsmarty->assign("defaultprinter4",$this->GetDefaultLogischePrinter($_rapportKolom4)); }
            if ($_rapportKolom5) { $this->actionsmarty->assign("defaultprinter5",$this->GetDefaultLogischePrinter($_rapportKolom5)); }
            if ($_rapportKolom6) { $this->actionsmarty->assign("defaultprinter6",$this->GetDefaultLogischePrinter($_rapportKolom6)); }
            if ($_rapportKolom7) { $this->actionsmarty->assign("defaultprinter7",$this->GetDefaultLogischePrinter($_rapportKolom7)); }

            $this->actionsmarty->assign("okbuttondisabled",$_okButtonDisabled?'disabled':'');
            $this->actionsmarty->display("parameterform.tpl.php");

            break;
        case 'BriefMateriaalIncompleet':
            $this->actionsmarty->assign("error",$error);

            $this->actionsmarty->assign("dialogtitle",$this->currentaction->record->TITLE);
            $this->actionsmarty->assign("dialogtext",$this->currentaction->record->TEXT);
            $this->actionsmarty->assign("moduleid",$this->currentaction->record->MODULEID);

            $_rowid = $this->dbobject->customUrlDecode($params['ROWID']);
            if ($_rowid != '') {
                $_rowid = str_replace(',','\',\'',$_rowid);
                $_filter = 'AND I.ROWID IN (\''.$_rowid.'\')';
            } else {
                $_filter = '';
            }

            $tellers = $this->TelTePrintenMatIncomplBrieven($_filter);

            $this->actionsmarty->assign("label1","Kies het land");
            $this->actionsmarty->assign("param1","
                <input type=\"radio\" id=\"rd_nietbelgie\" class=\"required\" name=\"param1\" checked=\"checked\" value=\"NL\" /> <label for=\"rd_nietbelgie\">Nederland en overige ({$tellers['AANTAL_NL']} stuks)</label><br>
                <input type=\"radio\" id=\"rd_belgie\" class=\"required\" name=\"param1\" value=\"BE\" /> <label for=\"rd_belgie\">Belgie ({$tellers['AANTAL_BE']} stuks)</label><br>
                <input type=\"radio\" id=\"rd_duitsland\" class=\"required\" name=\"param1\" value=\"DE\" /> <label for=\"rd_duitsland\">Duitsland (+ CH, AT) ({$tellers['AANTAL_DE']} stuks)</label>
                "
                );

            $this->actionsmarty->assign("printerselectie",$this->GetLogischePrinters());
            $this->actionsmarty->assign("defaultprinter",'BackOffice2');

            $this->actionsmarty->display("parameterform.tpl.php");

            break;
        case 'BriefCohortWachtkamer':
            $this->actionsmarty->assign("error",$error);

            $this->actionsmarty->assign("dialogtitle",$this->currentaction->record->TITLE);
            $this->actionsmarty->assign("dialogtext",$this->currentaction->record->TEXT);
            $this->actionsmarty->assign("moduleid",$this->currentaction->record->MODULEID);

            $_rowid = $this->dbobject->customUrlDecode($params['ROWID']);
            if ($_rowid != '') {
                $_rowid = str_replace(',','\',\'',$_rowid);
                $_filter = 'AND INSCHRIJVING_COHORTWACHTKAMER.ROWID IN (\''.$_rowid.'\')';
            } else {
                $_filter = '';
            }

            $tellers = $this->TelTePrintenCohortBrieven($_filter);

            $this->actionsmarty->assign("label1","Soort brief");
            $this->actionsmarty->assign("param1",
               "<input type=\"radio\" class=\"required\" id=\"param1_1\" name=\"param1\" value=\"COHORTWACHTKAMER\" checked=\"checked\" /> <label for=\"param1_1\">Cohortwachtkamer</label> &nbsp <b>({$tellers['AANTAL_COHORTWACHTKAMER']} stuks)</b>
               "
            );

            $this->actionsmarty->assign("printerselectie",$this->GetLogischePrinters());
            $this->actionsmarty->assign("defaultprinter",'BackOffice2');

            $this->actionsmarty->display("parameterform.tpl.php");

            break;
        case 'MBOHBOBrieven':
            $this->actionsmarty->assign("error",$error);

            $this->actionsmarty->assign("dialogtitle",$this->currentaction->record->TITLE);
            $this->actionsmarty->assign("dialogtext",$this->currentaction->record->TEXT);
            $this->actionsmarty->assign("moduleid",$this->currentaction->record->MODULEID);

            $_rowid = $this->dbobject->customUrlDecode($params['ROWID']);
            if ($_rowid != '') {
                $_rowid = str_replace(',','\',\'',$_rowid);
                $_filter = 'AND INSCHRIJVING_TOETSEN.ROWID IN (\''.$_rowid.'\')';
            } else {
                $_filter = '';
            }

            $tellers = $this->TelTePrintenMBOHBOBrieven($_filter);

            if ($tellers['AANTAL_VOOROPLEIDING'] > 0) {
                $_vooroplChecked = 'checked="checked"';
            }
            elseif ($tellers['AANTAL_VOOROPLEIDING_BO'] > 0) {
                $_vooroplBOChecked = 'checked="checked"';
            }
            elseif ($tellers['AANTAL_WERKERVARING'] > 0) {
                $_werkervaringChecked = 'checked="checked"';
            }
            elseif ($tellers['AANTAL_DIPLOMA'] > 0) {
                $_DiplChecked = 'checked="checked"';
            }
            elseif ($tellers['AANTAL_VERKLARING21JAAR'] > 0) {
                $_21pChecked = 'checked="checked"';
            }
            $this->actionsmarty->assign("label1","Soort brief");
            $this->actionsmarty->assign("param1",
               "<input type=\"radio\" class=\"required\" $_vooroplChecked id=\"param1_1\" name=\"param1\" value=\"VOOROPLEIDING_BO\" /> <label for=\"param1_1\">Vooropleiding brief voor Backoffice</label> &nbsp <b>({$tellers['AANTAL_VOOROPLEIDING']} stuks)</b>
                <br /><input type=\"radio\" class=\"required\" $_vooroplBOChecked id=\"param1_5\" name=\"param1\" value=\"VOOROPLEIDING\" /> <label for=\"param1_5\">Vooropleiding diploma's MBO/HBO/Master</label> &nbsp; <b>({$tellers['AANTAL_VOOROPLEIDING_BO']} stuks)</b>
                <br /><input type=\"radio\" class=\"required\" $_werkervaringChecked id=\"param1_2\" name=\"param1\" value=\"WERKERVARING\" /> <label for=\"param1_2\">Vrijstelling obv werkervaring</label> &nbsp; <b>({$tellers['AANTAL_WERKERVARING']} stuks)</b>
                <br /><input type=\"radio\" class=\"required\" $_DiplChecked id=\"param1_3\" name=\"param1\" value=\"DIPLOMA\" /> <label for=\"param1_3\">Vrijstelling obv diploma</label> &nbsp; <b>({$tellers['AANTAL_DIPLOMA']} stuks)</b>
                <br /><input type=\"radio\" class=\"required\" $_21pChecked id=\"param1_4\" name=\"param1\" value=\"21BRIEF\" /> <label for=\"param1_4\">21 jaar of ouder verklaring/21+ toetsen</label> &nbsp; <b>({$tellers['AANTAL_VERKLARING21JAAR']} stuks)</b>
               "
               // geen automatische Quickscan 150 euro BTV meer maken
               //  &nbsp; &nbsp;<input type=\"checkbox\" name=\"param2\" value=\"BTVWERKERVARING\" checked=\"checked\"> Incl. aanmaken betalingsverzoek
            );

            $this->actionsmarty->assign("printerselectie",$this->GetLogischePrinters());
            $this->actionsmarty->assign("defaultprinter",'BackOffice2');

            $this->actionsmarty->display("parameterform.tpl.php");
            break;
        case 'MBOHBOToetsingAkkoord':
        case 'MBOHBOToetsingAfgekeurd':
        case 'MBOHBOToetsingDefinitiefAfgekeurd':
        case 'MBOHBOCheckAndereOpleiding':
        case 'MBOHBOWijzigCohort':
//            $this->actionsmarty->assign("dialogtext",$this->currentaction->record->TEXT);
            $this->actionsmarty->assign("moduleid",$this->currentaction->record->MODULEID);

            if ($params['ROWID'] == '') {
                $_okButtonDisabled = true;
                $error = "U dient eerst een of meerdere inschrijvingen aan te vinken.";
            } else {
                $_okButtonDisabled = false;
                $dialogtext = $this->currentaction->record->TEXT;
            }

            if ($moduleid == 'MBOHBOToetsingAkkoord') {
                $_inschrTabel = $this->GetTeAccorderenInschr($params['ROWID']);
                $dialogtext .= $_inschrTabel;
                $this->actionsmarty->assign("label1","Kredietwaardig?");
                $this->actionsmarty->assign("param1",
                   "<input type='radio' name=\"param1\" id=\"param1_J\" value=\"J\" checked=\"checked\"><label for=\"param1_J\"> Ja </label><br/> <input type='radio' name=\"param1\" id=\"param1_N\" value=\"N\"><label for=\"param1_N\"> Nee (+ start WANBET procedure)</label>"
                );
            }

            if ($moduleid == 'MBOHBOCheckAndereOpleiding') {
                if ($params['ROWID'] == '') {
                    $_inschrTabel = false;
                } else {
                    $_inschrTabel = $this->GetCheckAndereInschr($params['ROWID']);
                }
                if ($_inschrTabel !== FALSE) {
                    $dialogtext .= $_inschrTabel;
                } else {
                    $_okButtonDisabled = true;
                }
            }

            if ($moduleid == 'MBOHBOWijzigCohort') {
                $_inschrTabel = $this->GetInschrWijzigen($params['ROWID']);
                $dialogtext .= $_inschrTabel;
            }

            $this->actionsmarty->assign("dialogtitle",$this->currentaction->record->TITLE);
            $this->actionsmarty->assign("dialogtext",$dialogtext);
            $this->actionsmarty->assign("error",$error);

            $_icon = false;
            switch($moduleid) {
            case 'MBOHBOToetsingAkkoord':
                $_icon = 'function/thumbs_up_48.png';
                break;
            case 'MBOHBOToetsingAfgekeurd':
                $_icon = 'function/thumbs_down_48.png';
                break;
            case 'MBOHBOToetsingDefinitiefAfgekeurd':
                $_icon = 'oxygen/filesystems/trashcan_full.png';
                break;
            case 'MBOHBOCheckAndereOpleiding':
                $_icon = 'oxygen/actions/system_switch_user.png';
                break;
            }
            if ($_icon)
                $this->actionsmarty->assign("param2","<img style=\"position:absolute;top:0px;right:100px;\" src=\"{$_GVARS['serverpath']}/i/{$_icon}\">");

            $this->actionsmarty->assign("okbuttondisabled",$_okButtonDisabled?'disabled':'');
            $this->actionsmarty->display("parameterform.tpl.php");
            break;
        case 'ExecCallOpmerking':
            $this->actionsmarty->assign("error",$error);

//            $this->actionsmarty->assign("dialogtext",$this->currentaction->record->TEXT);
            $this->actionsmarty->assign("moduleid",$this->currentaction->record->MODULEID);

            $_okButtonDisabled = false;
            $dialogtext = $this->currentaction->record->TEXT;

            $this->actionsmarty->assign("dialogtitle",$this->currentaction->record->TITLE);
            $this->actionsmarty->assign("dialogtext",$dialogtext);

            $_relatienr = ($_GET['RELATIENR']!='')?$_GET['RELATIENR']:$_GET['CURSISTNR'];
            if ($_relatienr == '') $_relatienr = $_GET['ZOEKRELATIE'];

            $_inschrijfnr = ($_GET['INSCHRIJFNR']!='')?$_GET['INSCHRIJFNR']:$_GET['ZOEKINSCHRIJVING'];

            if ($_relatienr == '' and $_inschrijfnr != '') {
                $_relatienr = $this->GetRelatie($_inschrijfnr);
            }
            $_readonlyRelatie = $_relatienr == ''?'':'readonly';
            $this->actionsmarty->assign("label1","Relatienummer");
            $this->actionsmarty->assign("param1","<input type=\"text\" class=\"{$_readonlyRelatie}\" name=\"param1\" value=\"{$_relatienr}\" />");

            if ($_relatienr != '' and $_inschrijfnr == '') {
                $options = $this->GetInschrijvingen($_relatienr);
                $this->actionsmarty->assign("label2","Inschrijfnr");
                $this->actionsmarty->assign("param2","<select name=\"param2\">{$options}</select>");
            } else {
                $_readonlyInschrijfnr = $_inschrijfnr == ''?'':'readonly';
                $this->actionsmarty->assign("label2","Inschrijfnr");
                $this->actionsmarty->assign("param2","<input type=\"text\" class=\"{$_readonlyInschrijfnr}\" name=\"param2\" value=\"{$_inschrijfnr}\" />");
            }
            $this->actionsmarty->assign("label3","Opmerking");
            $this->actionsmarty->assign("param3","<textarea autofocus=\"autofocus\" style=\"width:400px;height:20px;\" name=\"param3\"></textarea>");

            $this->actionsmarty->assign("okbuttondisabled",$_okButtonDisabled?'disabled':'');
            $this->actionsmarty->display("parameterform.tpl.php");
            break;
        case 'ImporteerTerugkoppel':
            $this->actionsmarty->assign("error",$error);

            $this->actionsmarty->assign("dialogtitle",$this->currentaction->record->TITLE);
            $this->actionsmarty->assign("moduleid",$this->currentaction->record->MODULEID);

            $dialogtext = $this->currentaction->record->TEXT.'<br /><h2>'.$this->VolgendTerugkoppelBestand().'</h2>';

            $this->actionsmarty->assign("label1","debuglevel");
            $this->actionsmarty->assign("param1",
               "<select name=\"param1\"><option>0</option><option>1</option><option>2</option></select>"
            );

            $this->actionsmarty->assign("dialogtext",$dialogtext);
            $this->actionsmarty->display("parameterform.tpl.php");
            break;
        case 'MBOHBOFONabellen':
            if ($this->SelectieBevatVrijst($params['ROWID'])) {
                $error = "U kunt alleen relaties met onvoldoende vooropleiding doorzetten naar Front Office.";
                $_okButtonDisabled = true;
            }

            $this->actionsmarty->assign("dialogtitle",$this->currentaction->record->TITLE);
            $this->actionsmarty->assign("moduleid",$this->currentaction->record->MODULEID);
            $this->actionsmarty->assign("okbuttondisabled",$_okButtonDisabled?'disabled':'');

            $this->actionsmarty->assign("error",$error);
            $this->actionsmarty->assign("dialogtext", $this->currentaction->record->TEXT);
            $this->actionsmarty->display("parameterform.tpl.php");
            break;
        case 'MBOHBOFOVooropleiding':
            $this->actionsmarty->assign("dialogtitle",$this->currentaction->record->TITLE);
            $this->actionsmarty->assign("moduleid",$this->currentaction->record->MODULEID);
            $this->actionsmarty->assign("okbuttondisabled",$_okButtonDisabled?'disabled':'');

            $dialog = $this->currentaction->record->TEXT;
            $dialog .= $this->GetVooropleidingOverzicht($params['ROWID']);

            $this->actionsmarty->assign("error",$error);
            $this->actionsmarty->assign("dialogtext", $dialog);
            $this->actionsmarty->display("parameterform.tpl.php");
            break;
        case 'MBOHBOVrijstellingOngedaanMaken':
            $this->actionsmarty->assign("dialogtitle",$this->currentaction->record->TITLE);
            $this->actionsmarty->assign("moduleid",$this->currentaction->record->MODULEID);
            $this->actionsmarty->assign("okbuttondisabled",$_okButtonDisabled?'disabled':'');

            $_inschrTabel = $this->GetInschrTabel($params['ROWID']);
            $dialogtext = $_inschrTabel;
            $dialogtext .= $this->currentaction->record->TEXT;

            $this->actionsmarty->assign("error",$error);
            $this->actionsmarty->assign("dialogtext", $dialogtext);
            $this->actionsmarty->display("parameterform.tpl.php");
            break;
        case 'MBOHBOFOAfkeuren':
            $this->actionsmarty->assign("dialogtitle",$this->currentaction->record->TITLE);
            $this->actionsmarty->assign("moduleid",$this->currentaction->record->MODULEID);
            $this->actionsmarty->assign("okbuttondisabled",$_okButtonDisabled?'disabled':'');

            $this->actionsmarty->assign("error",$error);
            $this->actionsmarty->assign("dialogtext", $this->currentaction->record->TEXT);
            $this->actionsmarty->display("parameterform.tpl.php");
            break;
        case 'MBOHBOEDRCheck':
            $_inschrTabel = $this->GetInschrTabel($params['ROWID']);

            if ($params['ROWID'] == '') {
                $error = 'U dient een inschrijving aan te vinken.';
            }
            $dialogtext = $_inschrTabel;
            $dialogtext .= $this->currentaction->record->TEXT;

            $this->actionsmarty->assign("dialogtitle", $this->currentaction->record->TITLE);
            $this->actionsmarty->assign("moduleid",$this->currentaction->record->MODULEID);
            $this->actionsmarty->assign("okbuttondisabled",$_okButtonDisabled?'disabled':'');

            $this->actionsmarty->assign("error",$error);
            $this->actionsmarty->assign("dialogtext", $dialogtext);
            $this->actionsmarty->assign("label1","Kredietwaardig?");
            $this->actionsmarty->assign("param1",
               "<input type='radio' name=\"param1\" id=\"param1_J\" value=\"J\" checked=\"checked\"><label for=\"param1_J\"> Ja </label><br/> <input type='radio' name=\"param1\" id=\"param1_N\" value=\"N\"><label for=\"param1_N\"> Nee (+ start WANBET procedure)</label>"
            );
            $this->actionsmarty->display("parameterform.tpl.php");
            break;
        case 'ExecDatumOpschuiven':
            $_btv = $this->GetBetalingsVerzoek($params['ROWID']);
            $_betaaldagen = $this->GetBetaaldagen();
            $_betalingsverzoeknr = $_btv['BETALINGSVERZOEKNR'];
            $_dialogtext = $this->currentaction->record->TEXT;
            if ($_btv['BETAALWIJZECODE'] == 'AUT' and $_btv['LANDCODE'] !== 'DE') {
                $_autvalidation = "aut_check";
                $_dialogtext .= '<script type="text/javascript">
                    var betaaldagen = ['.$_betaaldagen.'];
                    jQuery.validator.addMethod("valtOpBetaalDag", function(value, element) {
                        var dag = parseInt(value.substr(0,2));
                        return ($.inArray(dag, betaaldagen) > -1);
                    }, "Automatische betaaldag moet op "+betaaldagen+" vallen.");
                    $("#parameterform").validate({
                        rules : {
                            param2 : { valtOpBetaalDag : true }
                        }
                    });
                    </script>';
                $_dialogtext .= '<h3>De verzenddatum/factuurdatum dient op automatische betaaldag te vallen ('.$_betaaldagen.').</h3>';
            } else {
                $_autvalidation = "";
            }
            $_input1 = "<input type=\"text\" class=\"appear_required $_autvalidation date_input\" name=\"param2\" value=\"\" />";
            $_input2 = "<input type=\"text\" class=\"appear_required date_input\" name=\"param3\" value=\"\" />";
            $this->actionsmarty->assign("label1", "BetalingsverzoekNr");
            $this->actionsmarty->assign("param1", "<input type=\"text\" class=\"readonly\" name=\"param1\" value=\"$_betalingsverzoeknr\" />");
            $this->actionsmarty->assign("label2", "Verzenddatum");
            $this->actionsmarty->assign("param2", $_input1);
            $this->actionsmarty->assign("label3", "Factuurdatum");
            $this->actionsmarty->assign("param3", $_input2);
            $this->actionsmarty->assign("label4", "Cursist vervaldatum");
            $this->actionsmarty->assign("param4", "<input type=\"text\" class=\"appear_required date_input\" name=\"param4\" value=\"\" />");
            $this->actionsmarty->assign("label5", "Systeem vervaldatum");
            $this->actionsmarty->assign("param5", "<input type=\"text\" class=\"appear_required date_input\" name=\"param5\" value=\"\" />");

            $this->actionsmarty->assign("dialogtitle", $this->currentaction->record->TITLE);
            $this->actionsmarty->assign("error", $error);

            $this->actionsmarty->assign("dialogtext", $_dialogtext);
            $this->actionsmarty->display("parameterform.tpl.php");
            break;
        case 'ExecAutoPrint':
            $_dialogtext = $this->currentaction->record->TEXT;

            $this->actionsmarty->assign("label1","Automatisch afdrukken actief?");
            if ($this->GetAutoPrintTPL()) {
                $_checkedJa = 'checked="checked"';
            } else {
                $_checkedNee = 'checked="checked"';
            }

            $this->actionsmarty->assign("param1","<input type=\"radio\" class=\"required\" name=\"param1\" value=\"AUTOPRINTJA\" $_checkedJa /> Ja &nbsp; <input type=\"radio\" class=\"required\" name=\"param1\" value=\"AUTOPRINTNEE\" $_checkedNee /> Nee");

            $this->actionsmarty->assign("dialogtitle",$this->currentaction->record->TITLE);
            $this->actionsmarty->assign("error",$error);

            $this->actionsmarty->assign("dialogtext", $_dialogtext);
            $this->actionsmarty->display("parameterform.tpl.php");
            break;
        case 'ExecAutoInschrFormEmailen':
            $_dialogtext = $this->currentaction->record->TEXT;

            $this->actionsmarty->assign("label1","Automatisch emailen van inschrijfformulieren?");
            if ($this->GetAutoPrintTPL()) {
                $_checkedJa = 'checked="checked"';
            } else {
                $_checkedNee = 'checked="checked"';
            }

            $this->actionsmarty->assign("param1","<input type=\"radio\" class=\"required\" name=\"param1\" value=\"AUTOEMAILJA\" $_checkedJa /> Ja &nbsp; <input type=\"radio\" class=\"required\" name=\"param1\" value=\"AUTOEMAILNEE\" $_checkedNee /> Nee");

            $this->actionsmarty->assign("dialogtitle",$this->currentaction->record->TITLE);
            $this->actionsmarty->assign("error",$error);

            $this->actionsmarty->assign("dialogtext", $_dialogtext);
            $this->actionsmarty->display("parameterform.tpl.php");
            break;

        case 'InschrijfformAfdrukken':
            global $g_lang_strings;
            $this->actionsmarty->assign("error",$error);

            $dialog = $this->currentaction->record->TEXT;
            $dialog .= $this->GetEPostOverzicht($params['ROWID']);
            $this->actionsmarty->assign("dialogtext",$dialog);
            $this->actionsmarty->assign("dialogtitle",$this->currentaction->record->TITLE);
            $this->actionsmarty->assign("moduleid",$this->currentaction->record->MODULEID);

            $_rowid = $this->dbobject->customUrlDecode($params['ROWID']);
            if ($_rowid != '') {
                $_rowid = str_replace(',','\',\'',$_rowid);
                $_filter = 'AND INSCHRIJVING_BO_ONDERTEK.ROWID IN (\''.$_rowid.'\')';
            } else {
                $_filter = '';
            }

            $tellers = $this->TelTePrintenInschrijfformBrieven($_filter);

            $this->actionsmarty->assign("label1",$g_lang_strings['soortbrief']);
            $this->actionsmarty->assign("param1",
               "<input type=\"radio\" class=\"required\" id=\"param1_1\" name=\"param1\" value=\"INSCHRIJFFORMULIEREN\" checked=\"checked\" /> <label for=\"param1_1\">".$g_lang_strings['forms']."</label> &nbsp <b>({$tellers['AANTAL_INSCHRIJFFORM']} ".$g_lang_strings['pieces'].")</b>
               "
            );

            $this->actionsmarty->display("parameterform.tpl.php");
            break;

        case 'InschrijfformMailen':
            global $g_lang_strings;

            $this->actionsmarty->assign("error",$error);
            $this->actionsmarty->assign("dialogtitle",$this->currentaction->record->TITLE);
            $this->actionsmarty->assign("moduleid",$this->currentaction->record->MODULEID);

            $dialog = $this->currentaction->record->TEXT;
            $dialog .= $this->GetMailOverzicht($params['ROWID']);
            $this->actionsmarty->assign("dialogtext",$dialog);

            $_rowid = $this->dbobject->customUrlDecode($params['ROWID']);
            if ($_rowid != '') {
                $_rowid = str_replace(',','\',\'',$_rowid);
                $_filter = 'AND INSCHRIJVING_BO_ONDERTEK.ROWID IN (\''.$_rowid.'\')';
            } else {
                $_filter = '';
            }

            $tellers = $this->TelTePrintenInschrijfformMails($_filter);

            $this->actionsmarty->assign("label1",$g_lang_strings['soortbrief']);
            $this->actionsmarty->assign("param1",
               "<input type=\"radio\" class=\"required\" id=\"param1_1\" name=\"param1\" value=\"INSCHRIJFFORMULIEREN\" checked=\"checked\" /> <label for=\"param1_1\">".$g_lang_strings['forms']."</label> &nbsp <b>({$tellers['AANTAL_INSCHRIJFFORM']} ".$g_lang_strings['pieces'].")</b>
               "
            );

            $this->actionsmarty->display("parameterform.tpl.php");
            break;

        case 'InschrijfformMailenSingle':
            global $g_lang_strings;

            $this->actionsmarty->assign("error",$error);
            $this->actionsmarty->assign("dialogtitle",$this->currentaction->record->TITLE);
            $this->actionsmarty->assign("moduleid",$this->currentaction->record->MODULEID);

            $dialog = $this->currentaction->record->TEXT;
            $dialog .= $this->GetMailRelatie($params['ROWID']);
            $this->actionsmarty->assign("dialogtext",$dialog);

            $_rowid = $this->dbobject->customUrlDecode($params['ROWID']);
            if ($_rowid != '') {
                $_rowid = str_replace(',','\',\'',$_rowid);
                $_filter = 'AND INSCHRIJVING_PRE.ROWID IN (\''.$_rowid.'\')';
            } else {
                $_filter = '';
            }

            $tellers = $this->TelTePrintenInschrijfformMails($_filter, $single=true);

            $this->actionsmarty->assign("label1",$g_lang_strings['soortbrief']);
            $this->actionsmarty->assign("param1",
               "<input type=\"radio\" class=\"required\" id=\"param1_1\" name=\"param1\" value=\"INSCHRIJFFORMULIEREN\" checked=\"checked\" /> <label for=\"param1_1\">".$g_lang_strings['forms']."</label> &nbsp <b>({$tellers['AANTAL_INSCHRIJFFORM']} ".$g_lang_strings['pieces'].")</b>
               "
            );

            $this->actionsmarty->display("parameterform.tpl.php");
            break;

        case 'InschrijfformMailPreview':
            global $g_lang_strings;
            global $_GVARS;

            require_once 'module.nha1/php/bufferinschrijvingen.class.php';
            $br = new BufferInschrijvingen($this, $this->dbobject);
            $_response = $br->BriefInschrijfFormsMailPreview($_GET, $params['ROWID'], $error);
            $this->actionsmarty->assign("error",$error);
            $this->actionsmarty->assign("dialogtitle",$this->currentaction->record->TITLE);
            $this->actionsmarty->assign("moduleid",$this->currentaction->record->MODULEID);
            $_attachments = '<a xtarget="x_blank" href="'.$_GVARS['serverpath'].'/app/macs/module/nha1/?customparamform=true&action=cbf9e44d84eaf34b09d2f70cc3362de2&moduleid=downloadinschrijfformpdf&ROWID='.$params['ROWID'].'"><i class="fa fa-file-pdf-o fa-2x"></i></a>';

            $this->actionsmarty->assign("fromaddress", $_response->VanAdres);
            $this->actionsmarty->assign("toaddress", $_response->NaarAdres);
            $this->actionsmarty->assign("subject", $_response->Onderwerp);
            $this->actionsmarty->assign("attachments", $_attachments);
            $this->actionsmarty->assign("bodytext", $_response->Inhoud);

            $this->actionsmarty->display("preview.tpl.php");
            break;

        case 'downloadinschrijfformpdf':
            require_once 'module.nha1/php/bufferinschrijvingen.class.php';

            $_br = new BufferInschrijvingen($this, $this->dbobject);
            $_output = $_br->OutputInschrijfformPDF($_GET['ROWID']);

            if ($_output) {
                header("Content-Disposition: attachment; filename=file.pdf");
                header('Content-Type: application/pdf');
                echo $_output;
            } else {
                echo "PDF heeft geen inhoud.";
            }

            break;

        case 'InschrijfformOndertekend':
            global $g_lang_strings;

            if ($params['ROWID'] !== '') {
                $_inschrTabel = $this->GetInschrTabel($params['ROWID']);
                $dialogtext = $this->currentaction->record->TEXT.'<br/>';
                $dialogtext .= $_inschrTabel;

                $this->actionsmarty->assign("error",$error);
                $this->actionsmarty->assign("dialogtitle",$this->currentaction->record->TITLE);
                $this->actionsmarty->assign("moduleid",$this->currentaction->record->MODULEID);
                $this->actionsmarty->assign("dialogtext",$dialogtext);
            } else {
                $this->actionsmarty->assign("error","Kruis een of meerdere inschrijvingen aan.");
            }
            $this->actionsmarty->display("parameterform.tpl.php");
            break;

        case 'ExecStandaardBriefVoorvertoning':
            global $g_lang_strings;

            if ($params['ROWID'] !== '') {
                $_inschrTabel = $this->GetInschrTabel($params['ROWID']);
                $dialogtext = '>';

                $this->actionsmarty->assign("error",$error);
                $this->actionsmarty->assign("dialogtitle",$this->currentaction->record->TITLE);
                $this->actionsmarty->assign("moduleid",$this->currentaction->record->MODULEID);
                $this->actionsmarty->assign("dialogtext",$dialogtext);
            } else {
                $this->actionsmarty->assign("error","Selecteer een brief.");
            }
            $this->actionsmarty->display("parameterform.tpl.php");

            break;

        default:
            echo "Moduleid niet gevonden $moduleid";
            break;
        }
    }

    function SelectieBevatVrijst($ROWID) {
        $_rowid = $this->GetRowFilter($ROWID);

        $_sql = 'SELECT COUNT(*) FROM INSCHRIJVING i WHERE '.$_rowid.' AND PRESTATUS = \'BO_VRIJST\'';
        $_status = intval($this->userdatabase->GetOne($_sql));
        return ($_status > 0);
    }

    function TelTePrintenMatIncomplBrieven($filter) {
        $_sql = "
        SELECT 'AANTAL_BE' AS SOORT, COUNT(I.INSCHRIJFNR) AS AANTAL FROM INSCHRIJVING_MATINCOMPLEET I, CURSUS C WHERE I.CURSUSCODE = C.CURSUSCODE AND NVL(MATCOMPL_AFGEDRUKT, 'N') = 'N' AND NVL(MATERIAALVERWACHTDATUM, ' ') <> ' ' AND LANDCODE = 'BE' $filter
        UNION
        SELECT 'AANTAL_NL' AS SOORT, COUNT(I.INSCHRIJFNR) AS AANTAL FROM INSCHRIJVING_MATINCOMPLEET I, CURSUS C WHERE I.CURSUSCODE = C.CURSUSCODE AND NVL(MATCOMPL_AFGEDRUKT, 'N') = 'N' AND NVL(MATERIAALVERWACHTDATUM, ' ') <> ' ' AND LANDCODE = 'NL' $filter
        UNION
        SELECT 'AANTAL_DE' AS SOORT, COUNT(I.INSCHRIJFNR) AS AANTAL FROM INSCHRIJVING_MATINCOMPLEET I, CURSUS C WHERE I.CURSUSCODE = C.CURSUSCODE AND NVL(MATCOMPL_AFGEDRUKT, 'N') = 'N' AND NVL(MATERIAALVERWACHTDATUM, ' ') <> ' ' AND LANDCODE = 'DE' $filter
        ";
        return $this->userdatabase->GetAssoc($_sql);
    }

    function TelTePrintenMBOHBOBrieven($filter) {
        $_sql = "
        SELECT 'AANTAL_WERKERVARING' AS SOORT, COUNT(*) AS AANTAL FROM INSCHRIJVING_TOETSEN WHERE VRIJSTELLINGWERKERVARING = 'J' AND NVL(TO_CHAR(VRIJSTWERKERV_AFGEDRUKTDATUM), ' ') = ' ' $filter
        UNION
        SELECT 'AANTAL_DIPLOMA' AS SOORT, COUNT(*) AS AANTAL FROM INSCHRIJVING_TOETSEN WHERE VRIJSTELLINGDIPLOMA = 'J' AND NVL(TO_CHAR(VRIJSTDIPLOMA_AFGEDRUKTDATUM), ' ') = ' ' $filter
        UNION
        SELECT 'AANTAL_VERKLARING21JAAR' AS SOORT, COUNT(*) AS AANTAL FROM INSCHRIJVING_TOETSEN WHERE VERKLARING21JAAROFOUDER = 'J' AND NVL(TO_CHAR(VERKLARING21_AFGEDRUKTDATUM), ' ') = ' ' $filter
        UNION
        SELECT 'AANTAL_VOOROPLEIDING' AS SOORT, COUNT(*) AS AANTAL FROM INSCHRIJVING_TOETSEN WHERE HEEFTVOOROPLEIDING = 'J' AND NVL(TO_CHAR(HEEFTVOOROPL_AFGEDRUKTDATUM), ' ') = ' ' AND VRIJSTELLINGDIPLOMA <> 'J' AND VRIJSTELLINGWERKERVARING <> 'J' $filter
        UNION
        SELECT 'AANTAL_VOOROPLEIDING_BO' AS SOORT, COUNT(*) AS AANTAL FROM INSCHRIJVING_TOETSEN WHERE HEEFTVOOROPLEIDING = 'J' AND HEEFTVOOROPL_AFGEDRUKTDATUM IS NOT NULL AND NVL(TO_CHAR(HEEFTVOOROPL_AFGEDRUKTDATUM2), ' ') = ' ' $filter
        ";
        return $this->userdatabase->GetAssoc($_sql);
    }

    function TelTePrintenCohortBrieven($filter) {
        $_sql = "
        SELECT 'AANTAL_COHORTWACHTKAMER' AS SOORT, COUNT(*) AS AANTAL FROM INSCHRIJVING_COHORTWACHTKAMER WHERE NVL(TO_CHAR(WACHTTIJD_AFGEDRUKT), 'N') = 'N' $filter
        ";
        return $this->userdatabase->GetAssoc($_sql);
    }

    function TelTePrintenInschrijfformBrieven($filter) {
        $_sql = "
        SELECT 'AANTAL_INSCHRIJFFORM' AS SOORT, COUNT(*) AS AANTAL FROM INSCHRIJVING_BO_ONDERTEK WHERE NVL(INSFRM_AFGEDRUKT, 'N') = 'N' $filter
        ";
        return $this->userdatabase->GetAssoc($_sql);
    }

    function TelTePrintenInschrijfformMails($filter, $single=FALSE) {
        if ($single === TRUE) {
            $_table = 'INSCHRIJVING_PRE';
        } else {
            $_table = 'INSCHRIJVING_BO_ONDERTEK';
        }
        $_sql = "
        SELECT 'AANTAL_INSCHRIJFFORM' AS SOORT, COUNT(*) AS AANTAL FROM $_table WHERE 1=1 $filter
        ";
        return $this->userdatabase->GetAssoc($_sql);
    }

    function Show($outputformat='xhtml', $rec=false) {
        parent::Show($outputformat, $rec);

        /* When there are parameters and they have no value, then show the parameter form */
        $displayDialogForFirstTime = !isset($_GET['notfirst']);
        if ($_GET['afterprint'] == 'true')
            $displayDialogForFirstTime = false;
        $customparamform = $_GET['customparamform']=='true';

        if ($customparamform) {
            $this->ParamsNeedValue($_GET);
            $paramvalues = $_GET;
        } else {
            $paramvalues = $this->ParamsNeedValue($this->currentaction->record);
        }

        $_executeImmediate = $this->currentaction->record->TITLE == '' and $this->currentaction->record->TEXT == '';
        if (!$_executeImmediate and ($displayDialogForFirstTime or !$paramvalues)) {
            if ($customparamform) {
                $this->DisplayCustomParameterForm($this->moduleid, $_GET);
            } else {
                $this->DisplayParameterForm();
            }
        } else {
            $result = $this->ExecuteProcedure($paramvalues, $error);
            if ($_GET['output'] != 'pdf') {
                $this->DisplayMessage($result, $error);
            }
        }
    }
}