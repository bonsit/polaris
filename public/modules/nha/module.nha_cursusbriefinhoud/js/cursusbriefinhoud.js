var NHA = {};

NHA.CBI = {
    dirtyform : false,
    initialize: function() {
        var Self = NHA.CBI;
        $("#constructtitle").appendTo($("#movedmasteractions"));
        $("#masteractions").appendTo($("#movedmasteractions"));

        Self.vulBriefkoppelingen();

        $("#btnNIEUWECBI").click(Self.nieuweBriefKoppeling);
        $("#btnVERWIJDERCBI").click(Self.verwijderBriefKoppeling);

        $("#_hdnSaveAnalyse").click(Self.saveAnalyse);
    },
    _refreshLocation: function(cbi) {
        window.location = Polaris.Base.addLocationVariable('cbi', cbi);
    },
    refreshLocation: function(e) {
        e.preventDefault();
        NHA.CBI._refreshLocation($(this).val());
    },
    huidigeCBIObject: function() {
        return $("#_fldBRIEFKOPPELING option:selected").data();
    },
    bepaalSessieCBI: function() {
        return ($("#_hdnSessionCBI").val());
    },
    vulBriefkoppelingenVeld: function() {
        var Self = NHA.CBI;

        var advobj = Self.huidigeCBIObject();
    },
    vulBriefkoppelingen: function(e) {
        var Self = NHA.CBI;

        $.getJSON(_servicequery, {
            'func': 'briefkoppelingen'
        }, function(data, textStatus) {
            if (textStatus == 'success') {
                log(data);
                var $bk = $("#_fldBRIEFKOPPELING");
                $bk
                .empty()
                .addOptions(data, 'BRIEFKOPPELING', 'BRIEFKOPPELING', Self.bepaalSessieCBI())
                //.chosen({disable_search_threshold: 10, include_group_label_in_selected: true}) // maak er een mooiere dropdown van
                .change(Self.refreshLocation);
                //Self.vulBriefkoppelingenVeld();
            }
        });
    },
    vulBriefcodes: function(e) {
        var Self = NHA.CBI;

        $.getJSON(_servicequery, {
            'func': 'briefcodes'
        }, function(data, textStatus) {
            if (textStatus == 'success') {
                var $bc = $("#_fldBRIEFCODE");
                $bc
                .empty()
                .addOptions(data, 'VERZENDONDERDEELCODE', 'VERZENDONDERDEELCODE');
            }
        });
    },
    saveBriefKoppeling: function(data) {
        var Self = NHA.CBI;

        var senddata = data;
        senddata.push({name: '_hdnFORMTYPE', value: 'nieuwebriefkoppeling'});
        senddata.push({name: '_hdnProcessedByModule', value: 'true'});
        senddata.push({name: 'BRIEFKOPPELING', value: $("#_fldNIEUWEBRIEFKOPPELING").val()});
        senddata.push({name: 'BRIEFCODE', value: $("#_fldBRIEFCODE").val()});

        var message = "Nieuwe briefkoppeling wordt opgeslagen.";
        var feedback_ok = "Nieuwe briefkoppeling is opgeslagen.";
        Polaris.Ajax.postJSON(_servicequery, senddata, message, feedback_ok, function(data) {
            Self.vulBriefkoppelingen();
        });
        return true;
    },
    nieuweBriefKoppeling: function(e) {
        var Self = NHA.CBI;

        Polaris.Window.showModuleForm('/modules/nha/module.nha_cursusbriefinhoud/html/briefkoppeling_nieuw.tpl.php'
        , /* on Accept */ Self.saveBriefKoppeling
        , /* on Show   */ function() {
            Self.vulBriefcodes();
        });
    },
    verwijderBriefKoppeling: function() {
        var Self = NHA.CBI;

        var $cbi = $("#_fldBRIEFKOPPELING");
        var cbi = $cbi.val();
        if (cbi != '' && confirm("Weet u zeker dat u briefkoppeling \"" + cbi + "\" en bijbehorende regels wilt verwijderen?")) {
            var senddata = [];
            senddata.push({name: 'BRIEFKOPPELING', value: cbi});
            senddata.push({name: '_hdnFORMTYPE', value: 'verwijderbriefkoppeling'});
            senddata.push({name: '_hdnProcessedByModule', value: 'true'});

            Polaris.Ajax.postJSON(_servicequery, senddata, null, 'Briefkoppeling "' + cbi + '" is verwijderd',
            function() {
                $("option[value='"+cbi+"']", $cbi).remove();
                $cbi.trigger("chosen:updated");
            });
        }
    },
    updateNieuweAdvCode: function() {
        var landcode = $("input[name=LAND]:checked").val().substring(0,1);
        if (landcode == 'N') landcode = '';
        var adcode = 'HAH' + landcode;
        adcode += $("#_fldMAAND").val() + $("#_fldJAAR").val().substring(2);
        $("#_lblADVERTENTIECODE").text(adcode);
        $("#_fldNIEUWEADVERTENTIECODE").val(adcode);
    }
}

$(document).ready(function(){
    NHA.CBI.initialize();
});
