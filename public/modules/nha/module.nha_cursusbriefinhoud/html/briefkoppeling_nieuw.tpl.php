<h1>Nieuwe briefkoppeling</h1>

<div id="_rowBRIEFKOPPELING" class="formviewrow">
    <label class="label">Briefkoppeling:</label>
    <div class="formcolumn">
        <input type="text" id="_fldNIEUWEBRIEFKOPPELING" name="NIEUWEBRIEFKOPPELING" value="" class="required validateUpperCase" placeholder="Koppelingnaam" />
    </div>
</div>
<div id="_rowBRIEFCODES" class="formviewrow">
    <label class="label">Brief:</label>
    <div class="formcolumn">
        <select id="_fldBRIEFCODE" name="BRIEFCODE">
        </select>
    </div>
</div>

<br />
<div class="buttons">
    <button type="button" tabindex="8800" id="btnSAVEANDCLOSE" class="jqmAccept positive">OK</button>
    <button type="button" tabindex="8810" id="btnCLOSE" class="jqmClose secondary">Annuleer</button>
</div>
