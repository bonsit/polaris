<link rel="stylesheet" type="text/css" media='all' href="{$serverpath}/min/f={$modulepath}/css/default.css" />

<input type="hidden" name="_hdnRecordID" value="{$items[0].PLR__RECORDID}" />
<input type="hidden" name="_hdnProcessedByModule" value="false" />
<input type="hidden" name="_hdnKeepOpen" value="true" />
<input type="hidden" name="_hdnExportType" value="csv" />
<input type="hidden" name="medewerkernr" value="{if $smarty.get.action == 'insert' and $smarty.get.rec != ''}{$smarty.session.userid}{else}{$items[0].MEDEWERKERNR|default:$smarty.session.userid}{/if}" />
<input type="hidden" name="querynr" value="{$items[0].QUERYNR}" />

<div class="alignlabels" id="querygenerator">
    <div class="formviewrow">
        <label class="label">{lang query_name}: </label>
        <div class="formcolumn">
            <input type="text" name="querynaam" size="40" value="{$items[0].QUERYNAAM}" />
        </div>
    </div>

    <div class="formviewrow">
        <label class="label">{lang description}: </label>
        <div class="formcolumn">
            <textarea name="omschrijving" style="height:3em;width:500px;">{$items[0].OMSCHRIJVING}</textarea>
        </div>
    </div>
    <hr class="fix" />
    <br />

    <div class="pagetabcontainer">
        <ul id="pagetabs">
            <li><a id="tab_sql" href="#page_sql">SQL</a></li>
            <li><a id="tab_parameters" href="#page_parameters">{lang parameters}</a></li>
        </ul>
    </div>

    <div id="page_sql" class="pagecontents">
        <textarea id="sqlquery" name="queryinhoud">{$items[0].QUERYINHOUD}</textarea>
    </div>

    <div id="page_parameters" class="pagecontents">
        <div id="parameters" style="overflow:auto;">&lt;{lang none}&gt;</div>
    </div>

    <div class="buttons small" id="sqlbuttons">
        <button id="ExecuteSQL" accesskey="F11">{lang execute}</button>
        <button id="ExportXSL" type="submit" class="xsl" accesskey="">{lang export} XSL</button>
        <button id="ExportCSV" type="submit" class="csv" accesskey="">{lang export} CSV</button>
        {*<button id="ExportXML" type="submit" class="xml" accesskey="">Exporteer XML&hellip;</button>*}
        <button id="printHTML" type="submit" class="xhtml" accesskey="">{lang print}</button>
        <input type="checkbox" name="_hdnFLUSHCACHE" value="yes" /> {lang refresh_data}
    </div>

    <div id="sqlresult" style="overflow:auto;">&lt;{lang no_results}&gt;</div>

</div>

<iframe name="qg_iframe" style="display:none;"></iframe>