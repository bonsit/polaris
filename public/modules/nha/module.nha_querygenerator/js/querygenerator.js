$.fn.serializeObject = function() {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

var objectToKeyValuePairs = function(a) {
    var o = Array();

    for (var key in a) {
        o[o.length] = {'name':key, 'value':a[key]}
    }
    return o;
};

var NHA = {};

NHA.QueryGenerator = {
    initialize: function() {
        $("#ExportCSV,#ExportXML,#ExportXSL,#printHTML").click(function(e) {
            e.preventDefault();

            if ($("body.insert").length > 0) {
                alert('U dient eerst de query op te slaan.');
            } else {
                var $dataform = $("#dataform");
                var oldaction = $dataform.attr('action');
                var oldexporttype = $("input[name=_hdnExportType]", $dataform).val();
                var oldtarget = $dataform.attr('target');

                $dataform.attr('action', _servicequery.replace('/json/','/csv/')+'edit/'+Polaris.Base.customUrlEncode($("input[name=_hdnRecordID]").val())+'/');
                $("input[name=_hdnProcessedByModule]", $dataform).val('true');
                $("input[name=_hdnExportType]", $dataform).val($(this).attr('class'));
                if ($(this).attr('id') == 'printHTML')
                    $dataform.attr('target','qg_iframe');
                $dataform[0].submit(); // via de DOM ([0]) en niet via jQuery ivm de validator -> ajax post

                $dataform.attr('action', oldaction);
                $dataform.attr('target', oldtarget);
                $("input[name=_hdnExportType]", $dataform).val(oldexporttype);
                $("input[name=_hdnAction]", $dataform).val('save');
                $("input[name=_hdnProcessedByModule]", $dataform).val('false');
            }
        });

        $("#ExecuteSQL").click(function(e) {
            e.preventDefault();
            NHA.QueryGenerator.executeSQL();
        });

        $("#tab_parameters").click(function() {
            $("#page_parameters :input[value='']:first").focus(function(){
                // Select input field contents
                this.select();
            });
        });

        NHA.QueryGenerator.lookforParameters();
    },
    executeSQL: function(justparams) {
        justparams = justparams || false;

        var url = _serverroot + '/app/macs/module/nha_querygenerator/';
        var values = $("#dataform").serializeObject();
        values._hdnAction = "execute";
        values._hdnProcessedByModule = true;
        values.justParameters = justparams;
        values = objectToKeyValuePairs(values);
        $("#sqlresult").html("<img src=\""+_serverroot+"/images/wait.gif\" />");
        $.post(url, values, function(data, textStatus) {
            if (textStatus == 'success') {
                if (data) {
                    try {
                        var data = jQuery.parseJSON(data);
                    } catch(error) {

                    }
                    $("#sqlresult img").remove();
                    if (typeof data == 'object' && data.showparameterform) {
                        $("#parameters").html(data.form);
                        /* Toont het parameter tabblad */
                        if (!justparams) {
                            $("#tab_parameters").click();
                            $("#page_parameters :input[value='']:first").focus();
                        }
                    } else {
                        $("#sqlresult").html(data);
                    }
                } else {
                    if (justparams)
                        $("#sqlresult").html("");
                    else
                        $("#sqlresult").html("Geen resultaat gevonden");
                }
            } else {
                alert("Fout bij uitvoeren van query. Foutmelding: "+textStatus);
            }
        });
    },
    lookforParameters: function() {
        NHA.QueryGenerator.executeSQL(true);
    },
    resizePage: function() {
        $scroll = $("#sqlresult");
        if ($scroll.length > 0) {
            var height = $(window).height() - $scroll.offset().top;
            height = height - $("#sqlbuttons").outerHeight() - $(".buttons").outerHeight() - 40;
            height = Math.max(100,height);
            $scroll.css(($.browser.msie && $.browser.version < 7 ? '' : 'max-') + 'height', height + 'px');
            $scroll.height(height);
        }
    }
}

$(document).ready(function(){
    NHA.QueryGenerator.initialize();

    NHA.QueryGenerator.resizePage();
    $(window).resize(NHA.QueryGenerator.resizePage);

    $("#medewerkers").change(function() {
        window.location = Polaris.Base.addLocationVariable('medewerker', this.value);
    });


});
