var optionsHuidig = {
    chart: {
        animation:true,
        renderTo: 'aantal_inschr_huidig_bar',
        defaultSeriesType: 'area',
        backgroundColor: '#fff'
    },
    title: {
        text: 'Huidige TM'
    },
    xAxis: {
                   labels: {
   //				align: 'center',
                   style: {
                       font: 'normal 10px Verdana, sans-serif'
                   }
                   },
                   type: 'datetime',
                   minPadding: 0.02,
                   maxPadding: 0.02,
      },
    yAxis: {
        min: 0,
        title: {
            text: 'Huidige acties'
        }
    },
    tooltip: {
        formatter: function() {
            var d = new Date(this.x);
            return '<b>'+ d.toDateString() +'</b><br/>'+
                 this.series.name +': '+ this.y;
        }
    },
    plotOptions: {
        area: {
           dataLabels: {
              enabled: true
           },
           enableMouseTracking: true
        },
        series: {
            animation:true
        }
    }
};    

var optionsPlanning = {
    chart: {
        animation:true,
        renderTo: 'aantal_inschr_planning_bar',
        defaultSeriesType: 'area',
        backgroundColor: '#fff'
    },
    title: {
        text: 'Planning TM'
    },
    xAxis: {
                   labels: {
   //				align: 'center',
                   style: {
                       font: 'normal 10px Verdana, sans-serif'
                   }
                   },
                   type: 'datetime',
                   minPadding: 0.02,
                   maxPadding: 0.02,
      },
    yAxis: {
        min: 0,
        title: {
            text: 'Geplande acties'
        }
    },
    tooltip: {
        formatter: function() {
            var d = new Date(this.x);
            return '<b>'+ d.toDateString() +'</b><br/>'+
                 this.series.name +': '+ this.y;
        }
    },
    plotOptions: {
        area: {
           dataLabels: {
              enabled: true
           },
           enableMouseTracking: true
        },
        series: {
            animation:true
        }
    }
};

var chartHuidig = new Highcharts.Chart(optionsHuidig);
var chartPlanning = new Highcharts.Chart(optionsPlanning);

$(document).ready(function() {
    chartHuidig.addSeries({
        name:'NL Huidig', data: TM_NL_HUIDIG
    });
    
    chartHuidig.addSeries({
        name:'BE Huidig', data: TM_BE_HUIDIG
    });

    chartPlanning.addSeries({
        name:'NL Planning', data: TM_NL_PLANNING        
    });
    
    chartPlanning.addSeries({
        name:'BE Planning', data: TM_BE_PLANNING        
    });
});
