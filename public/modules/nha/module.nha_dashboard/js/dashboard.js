var globaldata = [];

var getVakantie = function(land, jaar, dag) {
    dag = parseInt(dag.substring(0, 1));
console.log(land, jaar, dag);
    var data = 0;
    if (jaar == 2) {
        data = globaldata['aantal_inschrijvingen_ditjaar'+land];
    } else if (jaar == 1) {
        data = globaldata['aantal_inschrijvingen_1jaar'+land];
    } else {
        data = globaldata['aantal_inschrijvingen_2jaar'+land];
    }

    return data[dag].vakantie || '';
}

var optionsNL = {
    chart: {
        animation:true,
        renderTo: 'aantal_inschr_bar_NL',
        backgroundColor: '#000',
        type: 'column'
    },
    title: {
        text: 'Inschrijvingen Nederland - afgelopen dagen'
    },
    xAxis: {
        categories: ['.', '.', '.', 'eergisteren', 'gisteren'],
        title: {
            text: null
        }
    },
/*
    xAxis: {
        type: 'datetime',
        maxZoom: 48 * 3600 * 1000
    },
*/
    yAxis: {
        min: 0,
        title: {
            text: 'Aantal inschrijvingen'
        }
    },
    legend: {
        align: 'right',
        x: -100,
        verticalAlign: 'top',
        y: 20,
        floating: true,
        color: '#222',
        backgroundColor: '#eee',
        borderColor: '#ccc',
        borderWidth: 1,
        shadow: false
    },
    tooltip: {
        formatter: function() {
            return '<b>'+ this.x + '</b><br/>' +
                 this.series.name + ': '+ this.y + '<br>'
                 + getVakantie('NL', this.series.index, this.x);
        }
    },
    plotOptions: {
        column: {
           dataLabels: {
              enabled: true,
              color: '#FFFFFF',
           }
        }
    },
    series: [
            {
                name: 'jaar -2',
                data: [0, 0, 0, 0, 0],
                vakantie: ['','','','','']
            }, {
                name: 'jaar -1',
                data: [0, 0, 0, 0, 0],
                vakantie: ['','','','','']
            }, {
                name: 'jaar',
                data: [0, 0, 0, 0, 0],
                vakantie: ['','','','','']
            }
    ]
};

var optionsBE = {
    chart: {
        animation:true,
        renderTo: 'aantal_inschr_bar_BE',
        backgroundColor: '#000',
        type: 'column'
    },
    title: {
        text: 'Inschrijvingen België - afgelopen dagen'
    },
    xAxis: {
        categories: ['.', '.', '.', 'eergisteren', 'gisteren'],
        title: {
            text: null
        }
    },
/*
    xAxis: {
        type: 'datetime',
        maxZoom: 48 * 3600 * 1000
    },
*/
    yAxis: {
        min: 0,
        title: {
            text: 'Aantal inschrijvingen'
        }
    },
    legend: {
        align: 'right',
        x: -100,
        verticalAlign: 'top',
        y: 20,
        floating: true,
        color: '#222',
        backgroundColor: '#eee',
        borderColor: '#ccc',
        borderWidth: 1,
        shadow: false
    },
    tooltip: {
        formatter: function() {
            return '<b>'+ this.x + '</b><br/>' +
                 this.series.name + ': '+ this.y + '<br>'
                 + getVakantie('BE', this.series.index, this.x);
        }
    },
    plotOptions: {
        column: {
           dataLabels: {
              enabled: true,
              color: '#FFFFFF',
           }
        }
    },
    series: [
            {
                name: 'jaar -2',
                data: [0, 0, 0, 0, 0],
                vakantie: ['','','','','']
            }, {
                name: 'jaar -1',
                data: [0, 0, 0, 0, 0],
                vakantie: ['','','','','']
            }, {
                name: 'jaar',
                data: [0, 0, 0, 0, 0],
                vakantie: ['','','','','']
            }
    ]
};

var gaugeOptions = {
        chart: {
            renderTo: 'container',
            type: 'gauge',
            plotBackgroundColor: null,
            plotBackgroundImage: null,
            plotBorderWidth: 0,
            plotShadow: false
        },

        title: {
            text: 'Speedometer'
        },

        pane: {
            startAngle: -150,
            endAngle: 150,
            background: [{
                backgroundColor: {
                    linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                    stops: [
                        [0, '#FFF'],
                        [1, '#333']
                    ]
                },
                borderWidth: 0,
                outerRadius: '109%'
            }, {
                backgroundColor: {
                    linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                    stops: [
                        [0, '#333'],
                        [1, '#FFF']
                    ]
                },
                borderWidth: 1,
                outerRadius: '107%'
            }, {
                // default background
            }, {
                backgroundColor: '#DDD',
                borderWidth: 0,
                outerRadius: '105%',
                innerRadius: '103%'
            }]
        },

        // the value axis
        yAxis: {
            min: 0,
            max: 200,

            minorTickInterval: 'auto',
            minorTickWidth: 1,
            minorTickLength: 10,
            minorTickPosition: 'inside',
            minorTickColor: '#666',

            tickPixelInterval: 30,
            tickWidth: 2,
            tickPosition: 'inside',
            tickLength: 10,
            tickColor: '#666',
            labels: {
                step: 2,
                rotation: 'auto'
            },
            title: {
                text: 'km/h'
            },
            plotBands: [{
                from: 0,
                to: 120,
                color: '#55BF3B' // green
            }, {
                from: 120,
                to: 160,
                color: '#DDDF0D' // yellow
            }, {
                from: 160,
                to: 200,
                color: '#DF5353' // red
            }]
        },

        series: [{
            name: 'Speed',
            data: [80],
            tooltip: {
                valueSuffix: ' km/h'
            }
        }]
}

var addXlabel = function(options, data, index) {

    var ret = options.xAxis.categories[5-index];
    if (ret == '.')
        ret = data[index].label;
    ret = index + ' ' + ret;
    return ret;
}

var reloadStats = function() {
    var url = _serverroot+"/services/json/app/dashboard/const/dashboardmain/";
    if (window.location.search) {
        url = url + window.location.search;
    }
    $.getJSON( url, { "func": "getstats" }, function(data, textStatus) {
        if (textStatus == 'success') {
            globaldata = data;
            insch_nl_laatsteuur.incrementTo(parseFloat(data.aantal_nl_inschrijvingen_laatsteuur.aantal),5,200);
            insch_nl_vandaag.incrementTo(parseFloat(data.aantal_nl_inschrijvingen_vandaag.aantal),5,200);
            insch_nl_eenjaargeleden.incrementTo(parseFloat(data.aantal_nl_inschrijvingen_eenjaargeleden.aantal),5,200);
            insch_nl_tweejaargeleden.incrementTo(parseFloat(data.aantal_nl_inschrijvingen_tweejaargeleden.aantal),5,200);

            insch_be_laatsteuur.incrementTo(parseFloat(data.aantal_be_inschrijvingen_laatsteuur.aantal),5,200);
            insch_be_vandaag.incrementTo(parseFloat(data.aantal_be_inschrijvingen_vandaag.aantal),5,200);
            insch_be_eenjaargeleden.incrementTo(parseFloat(data.aantal_be_inschrijvingen_eenjaargeleden.aantal),5,200);
            insch_be_tweejaargeleden.incrementTo(parseFloat(data.aantal_be_inschrijvingen_tweejaargeleden.aantal),5,200);

            actieve_gebruikers.incrementTo(parseFloat(data.aantal_actieve_gebruikers),5,200);

            optionsNL.xAxis.categories[0] = addXlabel(optionsNL, data.aantal_inschrijvingen_ditjaarNL, 5);
            optionsNL.xAxis.categories[1] = addXlabel(optionsNL, data.aantal_inschrijvingen_ditjaarNL, 4);
            optionsNL.xAxis.categories[2] = addXlabel(optionsNL, data.aantal_inschrijvingen_ditjaarNL, 3);
            optionsNL.xAxis.categories[3] = addXlabel(optionsNL, data.aantal_inschrijvingen_ditjaarNL, 2);
            optionsNL.xAxis.categories[4] = addXlabel(optionsNL, data.aantal_inschrijvingen_ditjaarNL, 1);

            optionsBE.xAxis.categories[0] = addXlabel(optionsBE, data.aantal_inschrijvingen_ditjaarBE, 5);
            optionsBE.xAxis.categories[1] = addXlabel(optionsBE, data.aantal_inschrijvingen_ditjaarBE, 4);
            optionsBE.xAxis.categories[2] = addXlabel(optionsBE, data.aantal_inschrijvingen_ditjaarBE, 3);
            optionsBE.xAxis.categories[3] = addXlabel(optionsBE, data.aantal_inschrijvingen_ditjaarBE, 2);
            optionsBE.xAxis.categories[4] = addXlabel(optionsBE, data.aantal_inschrijvingen_ditjaarBE, 1);

            var theDate = new Date();
            optionsNL.series[0].name = theDate.getFullYear() - 2;
            optionsNL.series[1].name = theDate.getFullYear() - 1;
            optionsNL.series[2].name = theDate.getFullYear();
            optionsBE.series[0].name = theDate.getFullYear() - 2;
            optionsBE.series[1].name = theDate.getFullYear() - 1;
            optionsBE.series[2].name = theDate.getFullYear();

            if (typeof chartNL == 'undefined') {
                chartNL = new Highcharts.Chart(optionsNL);
            }
            if (typeof chartBE == 'undefined') {
                chartBE = new Highcharts.Chart(optionsBE);
            }

            insertVakanties(data);

            plotChart(data);
        }
    });
}

var voegVakantieToe = function(elem, vakantie) {
    if (vakantie) {
        $span = $("<span class=\"vakantie\">");
        $img = $("<img>");
    //    $img.attr('src', 'images/icon_arrow.gif');
        $span.append($img);
        $span.html(vakantie);
        $(elem).parent().append($span);
    }
}

var insertVakanties = function(data) {
    $(".vakantie").remove();

    voegVakantieToe("#insc_nl_laatsteuur", data.aantal_nl_inschrijvingen_laatsteuur.vakantie);
    voegVakantieToe("#insc_nl_vandaag", data.aantal_nl_inschrijvingen_vandaag.vakantie);
    voegVakantieToe("#insc_nl_eenjaargeleden", data.aantal_nl_inschrijvingen_eenjaargeleden.vakantie);
    voegVakantieToe("#insc_nl_tweejaargeleden", data.aantal_nl_inschrijvingen_tweejaargeleden.vakantie);

    voegVakantieToe("#insc_be_laatsteuur", data.aantal_be_inschrijvingen_laatsteuur.vakantie);
    voegVakantieToe("#insc_be_vandaag", data.aantal_be_inschrijvingen_vandaag.vakantie);
    voegVakantieToe("#insc_be_eenjaargeleden", data.aantal_be_inschrijvingen_eenjaargeleden.vakantie);
    voegVakantieToe("#insc_be_tweejaargeleden", data.aantal_be_inschrijvingen_tweejaargeleden.vakantie);
}

var plotChart = function(aantallen) {
    if (aantallen) {
        for(i=1;i<=5;i++) {
            // Dit jaar
            chartNL.series[2].data[i-1].update(parseFloat(aantallen.aantal_inschrijvingen_ditjaarNL[6-i].aantal));
            chartBE.series[2].data[i-1].update(parseFloat(aantallen.aantal_inschrijvingen_ditjaarBE[6-i].aantal));

            // vorig jaar
            chartNL.series[1].data[i-1].update(parseFloat(aantallen.aantal_inschrijvingen_1jaarNL[6-i].aantal));
            chartNL.series[1].options.vakantie[i-1] = aantallen.aantal_inschrijvingen_1jaarNL[6-i].vakantie;
            chartBE.series[1].data[i-1].update(parseFloat(aantallen.aantal_inschrijvingen_1jaarBE[6-i].aantal));
            chartBE.series[1].options.vakantie[i-1] = aantallen.aantal_inschrijvingen_1jaarBE[6-i].vakantie;

            // twee jaar geleden
            chartNL.series[0].data[i-1].update(parseFloat(aantallen.aantal_inschrijvingen_2jaarNL[6-i].aantal));
            chartNL.series[0].options.vakantie[i-1] = aantallen.aantal_inschrijvingen_2jaarNL[6-i].vakantie;
            chartBE.series[0].data[i-1].update(parseFloat(aantallen.aantal_inschrijvingen_2jaarBE[6-i].aantal));
            chartBE.series[0].options.vakantie[i-1] = aantallen.aantal_inschrijvingen_2jaarBE[6-i].vakantie;
        }
    };
};

//var actieveGebruikers = new Highcharts.Chart(gaugeOptions);

var insch_nl_laatsteuur = new flipCounter('insc_nl_laatsteuur', {value:0, auto:false});
var insch_nl_vandaag = new flipCounter('insc_nl_vandaag', {value:0, auto:false});
var insch_nl_eenjaargeleden = new flipCounter('insc_nl_eenjaargeleden', {value:0, auto:false});
var insch_nl_tweejaargeleden = new flipCounter('insc_nl_tweejaargeleden', {value:0, auto:false});

var insch_be_laatsteuur = new flipCounter('insc_be_laatsteuur', {value:0, auto:false});
var insch_be_vandaag = new flipCounter('insc_be_vandaag', {value:0, auto:false});
var insch_be_eenjaargeleden = new flipCounter('insc_be_eenjaargeleden', {value:0, auto:false});
var insch_be_tweejaargeleden = new flipCounter('insc_be_tweejaargeleden', {value:0, auto:false});

var actieve_gebruikers = new flipCounter('aantal_actievegebruikers', {value:0, auto:false});

$(document).ready(function() {

    $("#refresh").click( function(e) {
        e.preventDefault();
        reloadStats();
    });

    setInterval(reloadStats, 2000*60);
    reloadStats();

    //Polaris.Dataview.enableTooltips($("#metingenvandaag"));
});
