<?php
  // Juli 2010, Rian Rietveld, RRWD web development
  // Input: in te voeren adres, als get/post-variable in te voeren
  $adres = urlencode("6225HD, Nederland");

  // Aanroep functie
  $coordinates = getCoord($adres);

  // Output: de coordinaten longitude, latitude, altitude, voor verwerking in de rest van de code
  echo "coordinates zijn: ".$coordinates[0]." ".$coordinates[1];

  function getCoord($adres, $timeout=10) {
 // Roep Google Maps aan met het adres en de soort output
    $url = "http://maps.google.com/maps/geo?q=".$adres."&output=xml";

 // defineer een header
    $parts = parse_url($url);
    $host = $parts['host'];
    $path = $parts['path'];
    $query = $parts['query'];
    $header = "GET $path"."?"."$query HTTP/1.0\r\n";
    $header .= "Host: $host\r\n";
    $header .= "User-Agent: {$_SERVER['SERVER_NAME']}{$_SERVER['REQUEST_URI']} - RRWD\r\n\r\n";

 // open socket naar Google en lees output in
    if (gethostbyname($host) != $host) {
      $socket = @fsockopen($host, 80, $errno, $errstr, $timeout);
      if ($socket) {
        fwrite($socket, $header);
        unset($http_response);
        while (!feof($socket)) {
          $http_response .= fread($socket, 256);
        }
        fclose($socket);
        if (strpos($http_response, "200 OK")) {
        // mik de header weer weg
          $pos1 = stripos($http_response, "<?xml");
          $http_response = substr("$http_response",$pos1);
        // lees de xml in en pik de coordinates eruit
          $xml = new SimpleXMLElement($http_response);
          $coordinates = $xml->Response->Placemark->Point->coordinates;
          $coordinates = explode(',',$coordinates);
          return $coordinates;
        }
      }
    }
  }

?>