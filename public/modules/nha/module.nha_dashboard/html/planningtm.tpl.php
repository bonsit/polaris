<link rel="stylesheet" type="text/css" media='all' href="{$moduleroot}/css/default.css" />
<script type="text/javascript">
var TM_BE_HUIDIG = [
{section name=i loop=$tm_be_huidig}
[Date.UTC({$tm_be_huidig[i].GEPLANDEDATUM}), {$tm_be_huidig[i].AANTAL}]
{if !($smarty.section.i.last)},{/if}
{/section}
];

var TM_NL_HUIDIG = [
{section name=j loop=$tm_nl_huidig}
[Date.UTC({$tm_nl_huidig[j].GEPLANDEDATUM}), {$tm_nl_huidig[j].AANTAL}]
{if !($smarty.section.j.last)},{/if}
{/section}
];

var TM_BE_PLANNING = [
{section name=i loop=$tm_be_planning}
[Date.UTC({$tm_be_planning[i].GEPLANDEDATUM}), {$tm_be_planning[i].AANTAL}]
{if !($smarty.section.i.last)},{/if}
{/section}
];

var TM_NL_PLANNING = [
{section name=j loop=$tm_nl_planning}
[Date.UTC({$tm_nl_planning[j].GEPLANDEDATUM}), {$tm_nl_planning[j].AANTAL}]
{if !($smarty.section.j.last)},{/if}
{/section}
];

</script>
<div style="color:#333;">Totaal huidige acties België: {$tm_be_totaal} &nbsp; &nbsp; Totaal huidige acties Nederland: {$tm_nl_totaal}</div>
<div id="aantal_inschr_huidig_bar" style="width: 500px; height: 250px; margin: 10px auto;"></div>
<br /><br />
<div id="aantal_inschr_planning_bar" style="width: 800px; height: 300px; margin: 0px auto;"></div>
