<link rel="stylesheet" type="text/css" media='all' href="{$moduleroot}/css/default.css?r=2" />

<div id="metingenvandaag">

<div class="land">NL</div>
<div class="meting"><span id="insc_nl_tweejaargeleden"></span><p class="subtitel">2 jaar geleden</p></div>
<div class="meting"><span id="insc_nl_eenjaargeleden"></span><p class="subtitel">1 jaar geleden</p></div>
<div class="meting"><span id="insc_nl_vandaag"></span><p class="subtitel">Vandaag</p></div>
<div class="meting"><span id="insc_nl_laatsteuur"></span><p class="subtitel">Afgelopen uur</p></div>
<hr class="cleaner" style="clear:left;" />

<div class="land">BE</div>
<div class="meting"><span id="insc_be_tweejaargeleden"></span></div>
<div class="meting"><span id="insc_be_eenjaargeleden"></span></div>
<div class="meting"><span id="insc_be_vandaag"></span></div>
<div class="meting"><span id="insc_be_laatsteuur"></span></div>

</div>

<div id="actieve" class="meting meting2" xstyle="clear:both;"><span id="aantal_actievegebruikers"></span><p class="subtitel">Aantal ingelogde gebruikers</p></div>

<div id="aantal_inschr_bar_NL" class="chart"></div>

<div id="aantal_inschr_bar_BE" class="chart"></div>


<div id="counter"></div>

<div class="buttons dashboard small"><button id="refresh">Refresh Now</button> Auto refresh: 2 mins </div>