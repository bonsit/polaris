<?php
require_once('includes/mainactions.inc.php');
require_once('_shared/module._base.php');

define('frmAanmeldingExamen', '59c4a0a3e7df353cf6d978faaeeeb448');
define('frmExamenCijfers', '2358316d5e6b253477e3744429864b99');

class Module extends _BaseModule {

    function Module($moduleid, $module) {
        global $_GVARS;
        parent::_BaseModule($moduleid, $module);

        $this->processed = false;
        /**
        * What client are we talking about? (nog doen: eventueel selecteerbaar maken)
        */
        $this->clientid = $_SESSION['clientid'];
    }

    function Process() {
        global $_GVARS;
        global $polaris;

        $_plrinstance = $this->form->plrInstance();
        $_plrinstance->SetFetchMode(ADODB_FETCH_ASSOC);
        $this->form->database->userdb->SetFetchMode(ADODB_FETCH_ASSOC);

        if (isset($_POST['_hdnProcessedByModule'])) {
            try {
                if ($_POST['_hdnAction'] == 'delete') {
                    DeleteOneRecord($_POST);
                } else {
                    // hier code wanneer de gebruiker op Save drukt
                    if ($_POST['_hdnForm'] == frmAanmeldingExamen) {
                        $_aanmeldingsnr = $this->saveAanmelding($_POST);
                        $result = $this->saveExamenAanmeldingen($_POST, $_aanmeldingsnr);
                    }

                    if ($_POST['_hdnForm'] == frmExamenCijfers) {
                        $result = $this->saveExamenCijfers($_POST);
                    }

                    $resultArray = Array('result'=>$result, 'query'=>$_SERVER['REQUEST_URI'], 'error'=>false);
                    echo json_encode($resultArray);
                    exit;
                }
            } catch (Exception $E) {
                $detailedmessage = $E->completemsg."\r\n".parse_backtrace(debug_backtrace());
                if ($result == null) $result = false;

                $resultArray = Array('result'=>$result, 'query'=>$_SERVER['REQUEST_URI'], 'error'=>$E->msg, 'detailed_error'=>$detailedmessage);
                echo json_encode($resultArray);
                exit;
            }
        }
    }

    function saveAanmelding($record) {
        global $polaris;

        $_relatie = $this->form->database->userdb->GetRow("SELECT CURSISTNR FROM NHA.INSCHRIJVING WHERE INSCHRIJFNR = {$record['INSCHRIJFNR']}");

        // bewaar de geboortedatum en geboorteplaats
        $this->form->database->userdb->Execute("UPDATE NHA.RELATIE SET GEBDATUM = :GEBDATUM, GEBPLAATS = :GEBPLAATS, BSN = :BSN WHERE RELATIENR = :RELATIENR"
        , array('GEBDATUM'=>$record['GEBDATUM'], 'GEBPLAATS'=>$record['GEBPLAATS'], 'BSN'=>$record['BSN'], 'RELATIENR'=>$_relatie['CURSISTNR']));

        $_aanmelding['_hdnDatabase'] = $record['_hdnDatabase'];
        $_aanmelding['_hdnTable'] = 'ONDERWIJS.AANMELDINGEN';
        $_aanmelding['_hdnRecordID'] = $record['_hdnRecordID'];
        $_aanmelding['_hdnState'] = $record['_hdnRecordID'] == ''?'insert':'edit';
        $_aanmelding['NR'] = $record['NR']!='' ? $record['NR'] : '__auto_increment__';
        $_aanmelding['DATUMS_NR'] = $record['DATUMS_NR'];
        $_aanmelding['INSCHRIJFNR'] = $record['INSCHRIJFNR'];
        $_aanmelding['DATUM_AANMELDING'] = $record['DATUM_AANMELDING'];
        $_aanmelding['EXAMENNR'] = $record['EXAMENNR'];
        $_aanmelding['EXTRATIJDINMIN'] = $record['EXTRATIJDINMIN'];
        $_aanmelding['FACTUURRELATIENRBETAALDJANEE'] = $record['FACTUURRELATIENRBETAALDJANEE'] == 'J'?'J':'N';
        $_aanmelding['REKENINGNROFGIRONRRELATIE'] = $record['REKENINGNROFGIRONRRELATIE'];
        $_aanmelding['BETAALWIJZE'] = $record['BETAALWIJZE']=='AUT'?'AUT':'ACC';
        $_aanmelding['AANWEZIGHEID'] = $record['AANWEZIGHEID'] == 'J'?'J':'N';
        $_aanmelding['UITZONDERINGJANEE'] = $record['UITZONDERINGJANEE'] == 'J'?'J':'N';
// Toch uitzonderingtekst opslaan:        $_aanmelding['UITZONDERINGOPMERKING'] = $_aanmelding['UITZONDERINGJANEE'] == 'J'?$record['UITZONDERINGOPMERKING']:'';
        $_aanmelding['UITZONDERINGOPMERKING'] = $record['UITZONDERINGOPMERKING'];
        $_aanmelding['BEDRAGEXAMENGELD'.FLOATCONVERT] = $record['BEDRAGEXAMENGELD'];
        $_aanmelding['MEDEWERKERNR'] = $_SESSION['userid'];
        if ($record['DATUM_ANNULERING'] != '') {
            $_aanmelding['DATUM_ANNULERING'] = $record['DATUM_ANNULERING'];
            $_aanmelding['REDEN_ANNULERING'] = $record['REDEN_ANNULERING'];
            $_aanmelding['GEGRONDE_ANNULERING'] = $record['GEGRONDE_ANNULERING'] == 'J'?'J':'N';
        } else {
            $_aanmelding['DATUM_ANNULERING'] = '';
            $_aanmelding['REDEN_ANNULERING'] = '';
            $_aanmelding['GEGRONDE_ANNULERING'] = '';
        }

        $_autovalues = SaveRecord($_aanmelding);

        if ($record['NR'] == '') {
            return $_autovalues['NR'];
        } else {
            return $record['NR'];
        }
    }

    function saveExamenAanmeldingen($record, $aanmeldingsnr) {
        $_baserecord['_hdnDatabase'] = $record['_hdnDatabase'];
        $_baserecord['_hdnTable'] = 'ONDERWIJS.EXAMEN_AANMELDINGEN';

        $_recordids = $_POST['rec'];
        $_datums_examennr = $_POST['datums_examennr'];

        $_aangemeld = $_POST['aangemeld'];
        if ($_aangemeld == NULL) $_aangemeld = array();
        foreach($_datums_examennr as $_key=> $_examennr) {
            $_aanmelding = $_baserecord;

            $debug = false;

            if ($_recordids[$_key] != '' and array_search($_examennr, $_aangemeld)===FALSE) {
                /* plr__recordid exists, but not checked:
                   record needs to be DELETED */
                $_aanmelding['_hdnRecordID'] = str_replace('rec@','',$_recordids[$_key]);
                DeleteOneRecord($_aanmelding, $debug);

            } elseif ($_recordids[$_key] != '' and array_search($_examennr, $_aangemeld)!==FALSE) {
                /* plr__recordid exists, but checked:
                   record needs to be UPDATED */
                $_aanmelding['_hdnState'] = 'edit';
                $_aanmelding['_hdnRecordID'] = str_replace('rec@','',$_recordids[$_key]);
                $_aanmelding['aanmeldingnr'] = $aanmeldingsnr;
                $_aanmelding['datums_examennr'] = $_datums_examennr[$_key];
                SaveRecord($_aanmelding, $debug);

            } elseif ($_recordids[$_key] == '' and array_search($_examennr, $_aangemeld)!==FALSE) {
                /* plr__recordid does not exists, but checked:
                   record needs to be INSERTED */
                $_aanmelding['_hdnState'] = 'insert';
                $_aanmelding['nr'] = '__auto_increment__';
                $_aanmelding['aanmeldingnr'] = $aanmeldingsnr;
                $_aanmelding['datums_examennr'] = $_datums_examennr[$_key];
                SaveRecord($_aanmelding, $debug);

            }
        }
    }

    function saveExamenCijfers($record) {
        global $polaris;

        $debug = false;

        $_baserecord['_hdnDatabase'] = $record['_hdnDatabase'];
        $_baserecord['_hdnTable'] = 'ONDERWIJS.EXAMEN_AANMELDINGEN';

        $_recordids = $record['rec'];
        $_examenaanmeldingnr = $record['examenaanmeldingnr'];
        $_blanco = $_POST['blancoingeleverd'];
        if ($_blanco == NULL) $_blanco = array();

        foreach($_recordids as $_key=> $_rec) {
            $_examencijfer = $_baserecord;
            $_examencijfer['_hdnState'] = 'edit';
            $_examencijfer['_hdnRecordID'] = str_replace('rec@','',$_rec);

            $_cijfer = $record['examencijfer'][$_key];
            if ( in_array(substr($_cijfer,0,1), array('o','g','v')) ) {
                $_cijfer = substr($_cijfer,0,1);
            } else {
                $_cijfer = $polaris->ConvertFloatValue($record['examencijfer'][$_key]);
                if ($_cijfer < 0 or $_cijfer > 10) {
                    $_cijfer = false;
                }
            }

            $_blancoIngeleverd = array_search($_examenaanmeldingnr[$_key], $_blanco)!==FALSE;
            if ($_blancoIngeleverd) {
                $_cijfer = '';
            }

            if ($_cijfer !== false) {
                $_examencijfer['examenpunt'] = str_replace('.',',',$_cijfer);
                $_examencijfer['examenpunt'] = str_replace(',0','',$_examencijfer['examenpunt']);
//                $_examencijfer['examenpunt'] = $_cijfer;
                $_examencijfer['blancoingeleverd'] = $_blancoIngeleverd?'J':'N';
                SaveRecord($_examencijfer, $debug);
            }
        }
    }

    function GetLanden() {
        /* Haal de landcodes op, gecached */
        $_sqlLanden = "SELECT LANDCODE, LANDNAAM FROM NHA.land";
        return $this->form->database->userdb->CacheGetAssoc($_sqlLanden);
    }

    function GetExamenAanmeldingInfo($cursuscode, $datumsnr, $inschrijfnr) {
        $_sql = "
            SELECT /*_SELECTSTART_*/ MAX(EXAMENAANMELDINGNR) AS EXAMENAANMELDINGNR, MAX(CURSUSCODE) AS CURSUSCODE, MAX(DEELKWALIFICATIECODE) AS DEELKWALIFICATIECODE, MAX(OMSCHRIJVING) AS OMSCHRIJVING
            , MAX(EXAMENNR) AS EXAMENNR, MAX(EXAMENCODE) AS EXAMENCODE, MAX(EXAMENOMSCHRIJVING) AS EXAMENOMSCHRIJVING
            , MAX(AANGEMELD) AS AANGEMELD, MAX(AANMELDINGNR) AS AANMELDINGNR, MAX(DATUM_ANNULERING) AS DATUM_ANNULERING, MAX(GEGRONDE_ANNULERING) AS GEGRONDE_ANNULERING
            , MAX(REDEN_ANNULERING) AS REDEN_ANNULERING, MAX(INSCHRIJFNR) AS INSCHRIJFNR, MAX(PLR__RECORDID) AS PLR__RECORDID
            FROM (
                SELECT EA.NR AS EXAMENAANMELDINGNR, CURSUSCODE, CE.DEELKWALIFICATIECODE , D.OMSCHRIJVING, DAE.NR AS EXAMENNR
                , DAE.EXAMENCODE, E.EXAMENOMSCHRIJVING  , NVL2(A.NR,'JA','NEE') AS AANGEMELD, A.NR AS AANMELDINGNR, DATUM_ANNULERING , GEGRONDE_ANNULERING
                , REDEN_ANNULERING, INSCHRIJFNR , ROWIDTOCHAR( EA.ROWID) AS PLR__RECORDID
                /*_SELECTEND_*/
                FROM   ONDERWIJS.CURSUSEXAMEN CE
                , ONDERWIJS.DEELKWALIFICATIE D
                , ONDERWIJS.VW_AANMELDINGEXAMEN E
                , ONDERWIJS.DATUMS_EXAMEN DAE
                , ONDERWIJS.DATUMS DT
                , ONDERWIJS.EXAMEN_AANMELDINGEN EA , ONDERWIJS.AANMELDINGEN A
                WHERE CE.DEELKWALIFICATIECODE = D.DEELKWALIFICATIECODE
                AND CE.EXAMENCODE = E.EXAMENCODE
                AND E.EXAMENCODE = DAE.EXAMENCODE
                AND DAE.NR = EA.DATUMS_EXAMENNR (+) AND EA.AANMELDINGNR = A.NR (+)
                AND DT.NR = DAE.DATUMS_NR
                AND ((CE.CURSUSCODE = :cursuscode)
                AND (DT.NR = :datumsnr)
                AND (A.INSCHRIJFNR = :inschrijfnr)
                )
				AND E.GELDIG_VANAF <= DT.DATUM
				AND E.GELDIG_TM >= DT.DATUM
				AND D.GELDIG_VANAF <= DT.DATUM
				AND D.GELDIG_TM >= DT.DATUM

                UNION

                SELECT /*_SELECTSTART_*/ DISTINCT TO_NUMBER(NULL) AS EXAMENAANMELDINGNR, CURSUSCODE, CE.DEELKWALIFICATIECODE
                , D.OMSCHRIJVING, DAE.NR AS EXAMENNR
                , DAE.EXAMENCODE, E.EXAMENOMSCHRIJVING , '' AS AANGEMELD, TO_NUMBER(NULL) AS AANMELDINGNR
                , TO_DATE(NULL) AS DATUM_ANNULERING , '' AS GEGRONDE_ANNULERING
                , NULL AS REDEN_ANNULERING, TO_NUMBER(NULL) AS INSCHRIJFNR , NULL  AS PLR__RECORDID
                /*_SELECTEND_*/
                FROM   ONDERWIJS.CURSUSEXAMEN CE
                , ONDERWIJS.DEELKWALIFICATIE D
                , ONDERWIJS.VW_AANMELDINGEXAMEN E
                , ONDERWIJS.DATUMS_EXAMEN DAE
                , ONDERWIJS.DATUMS DT
                WHERE CE.DEELKWALIFICATIECODE = D.DEELKWALIFICATIECODE
                AND CE.EXAMENCODE = E.EXAMENCODE
                AND E.EXAMENCODE = DAE.EXAMENCODE
                AND DT.NR = DAE.DATUMS_NR
                AND ((CE.CURSUSCODE = :cursuscode)
                AND (DT.NR = :datumsnr))
				AND E.GELDIG_VANAF <= DT.DATUM
				AND E.GELDIG_TM >= DT.DATUM
				AND D.GELDIG_VANAF <= DT.DATUM
				AND D.GELDIG_TM >= DT.DATUM
		    )
            GROUP BY EXAMENCODE
            ORDER BY EXAMENCODE
        ";

        $rs = $this->form->database->userdb->GetAll($_sql, array('cursuscode'=>$cursuscode, 'datumsnr'=>$datumsnr, 'inschrijfnr'=>$inschrijfnr));

        // return the result as JSON
        return json_encode($rs);
    }

    function GetExamenCijfers($cursuscode, $datumsnr, $inschrijfnr) {
        $_sql = "
            SELECT EA.NR AS EXAMENAANMELDINGNR, EA.EXAMENPUNT, DECODE(EA.BLANCOINGELEVERD,'J','JA','NEE') AS BLANCOINGELEVERD, CE.CURSUSCODE, CE.DEELKWALIFICATIECODE , D.OMSCHRIJVING, DAE.NR AS EXAMENNR
            , DAE.EXAMENCODE, E.EXAMENOMSCHRIJVING  , NVL2(A.NR,'JA','NEE') AS AANGEMELD, A.NR AS AANMELDINGNR
            , INSCHRIJFNR , ROWIDTOCHAR( EA.ROWID) AS PLR__RECORDID
            /*_SELECTEND_*/
            FROM   ONDERWIJS.CURSUSEXAMEN CE
            , ONDERWIJS.DEELKWALIFICATIE D
            , ONDERWIJS.VW_AANMELDINGEXAMEN E
            , ONDERWIJS.DATUMS_EXAMEN DAE
            , ONDERWIJS.DATUMS DT
            , ONDERWIJS.EXAMEN_AANMELDINGEN EA , ONDERWIJS.AANMELDINGEN A
            WHERE CE.DEELKWALIFICATIECODE = D.DEELKWALIFICATIECODE
            AND CE.EXAMENCODE = E.EXAMENCODE
            AND E.EXAMENCODE = DAE.EXAMENCODE
            AND DAE.NR = EA.DATUMS_EXAMENNR (+) AND EA.AANMELDINGNR = A.NR (+)
            AND DT.NR = DAE.DATUMS_NR
            AND ((CE.CURSUSCODE = :cursuscode)
            AND (DT.NR = :datumsnr)
            AND (A.INSCHRIJFNR = :inschrijfnr)
            )
            AND E.GELDIG_VANAF <= DT.DATUM
            AND E.GELDIG_TM >= DT.DATUM
            AND D.GELDIG_VANAF <= DT.DATUM
            AND D.GELDIG_TM >= DT.DATUM
        ";
        $rs = $this->form->database->userdb->GetAll($_sql, array('cursuscode'=>$cursuscode, 'datumsnr'=>$datumsnr, 'inschrijfnr'=>$inschrijfnr));

        // return the result as JSON
        return json_encode($rs);
    }

    function GetExamenDatums($examennr) {
        $_sql = "
            SELECT DISTINCT NR AS DATUMS_NR, DATUM FROM ONDERWIJS.DATUMS WHERE NR IN (
                SELECT DATUMS_NR FROM ONDERWIJS.AANMELDINGEN WHERE EXAMENNR = :examennr
            )
            ORDER BY DATUM DESC
        ";

        $rs = $this->form->database->userdb->GetAll($_sql, array('examennr'=>$examennr));

        // return the result as JSON
        return json_encode($rs);
    }

    function GetAlleExamenDatums() {
        $_sql = "
            SELECT DISTINCT TO_CHAR(D.DATUM,'dd-mm-yyyy') , D.DATUM AS DATUMVALUE
            FROM ONDERWIJS.DATUMS D
            WHERE NR IN (
                SELECT DATUMS_NR FROM ONDERWIJS.AANMELDINGEN
            )
            AND D.SOORT_DATUM = 'E'
            ORDER BY D.DATUM DESC
        ";

        // return the result as JSON
        return $this->form->database->userdb->GetAssoc($_sql);
    }

    function GetAlleCursusCodes() {
        /*
        $_sql = "
            SELECT DISTINCT C.CURSUSCODE, C.CURSUSCODE || ' - '  || C.CURSUSNAAM AS CURSUSNAAM
            FROM ONDERWIJS.CURSUSEXAMEN CE, NHA.CURSUS C
            WHERE CE.CURSUSCODE = C.CURSUSCODE
        ";
        */
        $_sql = "
            SELECT DISTINCT C.CURSUSCODE, C.CURSUSCODE || ' - '  || C.CURSUSNAAM AS CURSUSNAAM
            FROM NHA.CURSUS C
            , NHA.INSCHRIJVING I
            , ONDERWIJS.AANMELDINGEN A
            WHERE I.INSCHRIJFNR = A.INSCHRIJFNR
            AND I.CURSUSCODE = C.CURSUSCODE
            ORDER BY C.CURSUSCODE
        ";

        // return the result as JSON
        $_result = $this->form->database->userdb->GetAssoc($_sql);
        return $_result;
    }

    function GetDatumCursusCodes($datum) {
        $_sql = "
            SELECT DISTINCT C.CURSUSCODE, C.CURSUSCODE || ' - '  || C.CURSUSNAAM AS CURSUSNAAM
            FROM NHA.CURSUS C
            , NHA.INSCHRIJVING I
            , ONDERWIJS.AANMELDINGEN A
            , ONDERWIJS.DATUMS D
            WHERE I.INSCHRIJFNR = A.INSCHRIJFNR
            AND I.CURSUSCODE = C.CURSUSCODE
            AND A.DATUMS_NR = D.NR
            AND D.DATUM = NVL(:datum, D.DATUM)
            ORDER BY C.CURSUSCODE
        ";

        // return the result as JSON
        $_result = $this->form->database->userdb->GetAll($_sql, array('datum'=>$datum));
        return json_encode($_result);
    }

    function GetExamenInschrijvingen($examennr, $datumsnr) {
        $_sql = "
            SELECT A.INSCHRIJFNR, I.CURSUSCODE, NHA.PACKTOOLS.NetteGeadresseerde(R.GESLACHT, R.VOORLETTERS, R.TUSSENVOEGSEL, R.ACHTERNAAM) AS NAAM
            , R.POSTCODE, R.STRAAT || ' ' || R.HUISNR AS ADRES
            , R.PLAATS, R.LANDCODE, I.LANDCODE AS INSCHR_LANDCODE
            , R.RELATIENR, I.DATUMINSCHRIJVING, I.OPZEGGINGSDATUM, I.OPMERKING, I.AANMANINGSCODE, I.DATUMGERETOURNEERD
            , R.VOORLETTERS, R.TUSSENVOEGSEL, R.ACHTERNAAM, R.GESLACHT
            , F.VOORLETTERS AS FACTUUR_VOORLETTERS
            , F.TUSSENVOEGSEL AS FACTUUR_TUSSSENVOEGSEL , F.ACHTERNAAM AS FACTUUR_ACHTERNAAM
            , F.BEDRIJFSNAAM AS FACTUUR_BEDRIJFSNAAM, F.RELATIENR AS FACTUUR_RELATIENR
            , I.VOOROPLEIDINGAKKOORD, I.BINNENKOMSTOOVK, I.ONDERTEKENDJANEE
            FROM ONDERWIJS.AANMELDINGEN A, NHA.INSCHRIJVING I, NHA.RELATIE R, NHA.RELATIE F
            WHERE EXAMENNR = :examennr AND DATUMS_NR = :datumsnr
            AND A.INSCHRIJFNR = I.INSCHRIJFNR
            AND I.CURSISTNR = R.RELATIENR (+)
            AND I.FACTUURRELATIE = F.relatienr (+)
        ";
        $rs = $this->form->database->userdb->GetAll($_sql, array('examennr'=>$examennr, 'datumsnr'=>$datumsnr));

        // return the result as JSON
        return json_encode($rs);
    }

    function DisplayAanmeldingen() {
        global $_GVARS;

        /***
        * Aanmelding
        */
        $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/inschrijvingselectie_form.js';
        $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/examenaanmelding_form.js';
        $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/aanmelding.js';

        $this->smarty->assign('landen', $this->GetLanden());

        if ($this->outputformat == 'xhtml') {
            $this->smarty->display("aanmeldingen.tpl.php");
        }
    }

    function DisplayExamenCijfers() {
        global $_GVARS;

        /***
        * Examencijfers
        */
        $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/inschrijvingselectie_form.js';
        $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/examenaanmelding_form.js';
        $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/examencijfers.js';

        $this->smarty->assign('landen', $this->GetLanden());

        if ($this->outputformat == 'xhtml') {
            $this->smarty->display("examencijfers.tpl.php");
        }
    }

    function DisplayExamensAfdrukken() {
        global $_GVARS;

        /***
        * Examen Afdrukken
        */
        $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/resultatenoverzicht.js';

        if ($this->outputformat == 'xhtml') {
            $this->smarty->assign('examendatums', $this->GetAlleExamenDatums());
//            $this->smarty->assign('cursuscodes', $this->GetAlleCursusCodes());
            $this->smarty->display("examenafdrukken.tpl.php");
        }
    }

    function GetAantalInschrijvingen($soort, $aantaldagen, $opzegtermijn) {
        $_landcode = 'NL';
        return $this->form->database->userdb->CacheGetOne('
            SELECT COUNT(*)
            FROM NHA.INSCHRIJVING_NETTO
            WHERE LANDCODE = :LANDCODE
            AND CURSUSCODE NOT LIKE \'%BAS%\'
            AND CURSUSNAAM NOT LIKE \'%leergang%\'
            AND CURSUSGROEP = :CURSUSGROEP
            AND DATUMINSCHRIJVING >= TRUNC(SYSDATE) - :AANTALDAGEN - :OPZEGTERMIJN
            AND DATUMINSCHRIJVING <= TRUNC(SYSDATE) - :OPZEGTERMIJN'
            , array('CURSUSGROEP'=>$soort, 'LANDCODE'=>$_landcode, 'AANTALDAGEN'=>$aantaldagen, 'OPZEGTERMIJN'=>$opzegtermijn));
    }

    function DisplayDashboardOnderwijs() {
        $_aantaldagen = 30;
        $_opzegtermijn = 15;
        $_totaal = $_aantaldagen + $_opzegtermijn;
        $this->smarty->assign('vanaf', strftime("%e %b", strtotime("-{$_totaal} day")));
        $this->smarty->assign('tm', strftime("%e %b", strtotime("-{$_opzegtermijn} day")));
        $this->smarty->assign('aantalinschrijvingen_mbo',$this->GetAantalInschrijvingen('MBO', $_aantaldagen, $_opzegtermijn));
        $this->smarty->assign('aantalinschrijvingen_hbo',$this->GetAantalInschrijvingen('HBO', $_aantaldagen, $_opzegtermijn));
        $this->smarty->display('onderwijs_dashboard.tpl.php');
    }

    function Show($outputformat='xhtml', $rec=false) {
        global $_GVARS;

        parent::Show($outputformat, $rec);

        $_GVARS['_includeJavascript'][] = $_GVARS['serverpath'].'/javascript/pure/pure2.js';
        $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/sharedlib.js';
        $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/onderwijs.sharedlib.js';
        $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/iban.js';

        switch ($this->moduleid) {
        case 1:
            if ($_GET['func'] != '') {
                if ($_GET['func'] == 'examenaanmeldinginfo') {
                    /* Geef de examenaanmeldinginfo terug voor de betreffende aanmelding */
                    echo $this->GetExamenAanmeldingInfo($_GET['cursuscode'], $_GET['datumsnr'], $_GET['inschrijfnr']);
                }
            } else {
                $this->DisplayAanmeldingen();
            }
            break;
        case 2:
            if ($_GET['func'] != '') {
                if ($_GET['func'] == 'examencijfers') {
                    /* Geef de examencijfers terug voor de betreffende aanmelding */
                    echo $this->GetExamenCijfers($_GET['cursuscode'], $_GET['datumsnr'], $_GET['inschrijfnr']);
                }
                if ($_GET['func'] == 'examendatums') {
                    /* Geef de examencijfers terug voor de betreffende aanmelding */
                    echo $this->GetExamenDatums($_GET['examennr']);
                }
                if ($_GET['func'] == 'exameninschrijvingen') {
                    /* Geef de examencijfers terug voor de betreffende aanmelding */
                    echo $this->GetExamenInschrijvingen($_GET['examennr'], $_GET['datumsnr']);
                }

            } else {
                $this->DisplayExamenCijfers();
            }
            break;
        case 3:
            if ($_GET['func'] != '') {
                if ($_GET['func'] == 'datumcursuscodes') {
                    /* Geef de examencijfers terug voor de betreffende aanmelding */
                    echo $this->GetDatumCursusCodes($_GET['datum']);
                }
            } else {
                $this->DisplayExamensAfdrukken();
            }
            break;
        case 4:
            if ($_GET['func'] != '') {
                if ($_GET['func'] == 'datumcursuscodes') {
                    /* Geef de examencijfers terug voor de betreffende aanmelding */
                    echo $this->GetDatumCursusCodes($_GET['datum']);
                }
            } else {
                $this->DisplayDashboardOnderwijs();
            }
            break;
        }
     }

}