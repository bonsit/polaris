<?php
require_once('includes/mainactions.inc.php');
require_once('includes/datefuncs.inc.php');
require_once('_shared/module._base.php');

class Module extends _BaseModule {
    var $database; // plr database object
    var $cacheTimeOut;
    var $_nrOfDesc = 5;

    function Module($moduleid, $module) {
        global $_GVARS;
        global $polaris;
        global $ADODB_FETCH_MODE;

        parent::_BaseModule($moduleid, $module);

        $this->cacheTimeOut = 5*60;

        $this->processed = false;
        $this->clientid = $_SESSION['clientid'];
        $this->currentdb = 15;

        $this->database = new plrDatabase($polaris);
        $this->database->LoadRecord(array($this->clientid, $this->currentdb));
        $this->userdatabase = $this->database->connectUserDB();
//        $ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
    }

    /**
     * Module function __GetCustomFilter:
     * Op deze manier kunnen we de geselecteerde advertentiecode doorsturen aan Polaris, zodat deze de juiste
     * recordcount kan bepalen
     */
    function __GetCustomFilter() {
        $_customfilter = 'BRIEFKOPPELING = \''. $this->GetHuidigeCBI() .'\'';
        return $_customfilter;
    }

    function Process() {
        if ($this->database)
            $this->database->SetOracleUserID($_SESSION['userid']);
        if (isset($_POST['_hdnProcessedByModule'])) {
            $_allesRecord = $_POST;
            try {
                switch($_allesRecord['_hdnFORMTYPE']) {
                case 'nieuwebriefkoppeling':
                    $result = $this->BewaarBriefKoppeling($_allesRecord);
                break;
                }
                $resultArray = Array('result'=>$result, 'query'=>$_SERVER['REQUEST_URI'], 'error'=>false);
                echo json_encode($resultArray);
                exit;
            } catch (Exception $E) {
                $detailedmessage = $E->completemsg."\r\n".parse_backtrace(debug_backtrace());
                if ($result == null) $result = false;

                $resultArray = Array('result'=>$result, 'query'=>$_SERVER['REQUEST_URI'], 'error'=>$E->msg, 'detailed_error'=>$detailedmessage);
                echo json_encode($resultArray);
                exit;
            }
        }
    }

    function BewaarBriefKoppeling($_allesRecord) {
        $_result = $this->userdatabase->Execute(
            "INSERT INTO NHA.CURSUSBRIEFINHOUD (BRIEFKOPPELING, BRIEFCODE, ACTIEFJN) VALUES(:BRIEFKOPPELING, :BRIEFCODE, 'N')"
            , array('BRIEFKOPPELING'=>$_allesRecord['BRIEFKOPPELING'], 'BRIEFCODE'=>$_allesRecord['BRIEFCODE'])
        );
        return $_result;
    }

    function VerwijderBriefKoppeling($_allesRecord) {
        $_sql = "DELETE FROM NHA.ADVERTENTIECODE WHERE ADVERTENTIECODE = :ADV";
        $this->userdatabase->Execute($_sql, array('ADV' => $_allesRecord['ADVERTENTIECODE']));
        $_sql = "DELETE FROM MARKETING.FT_VERSPREIDING WHERE ADVERTENTIECODE = :ADV";
        $this->userdatabase->Execute($_sql, array('ADV' => $_allesRecord['ADVERTENTIECODE']));
        $_sql = "DELETE FROM MARKETING.FT_VERSPREIDINGADV WHERE ADVERTENTIECODE = :ADV";
        return $this->userdatabase->Execute($_sql, array('ADV' => $_allesRecord['ADVERTENTIECODE']));
    }

    function ShowItemView() {
        $rowid = $this->form->database->customUrlDecode($_GET['rec']);
        $this->form->startno = 0;
        $_rs = $this->form->GetViewContent($detail=false, $masterrecord=false, false, $filter="ttt.ROWID = '{$rowid}'");
        $items = $_rs->GetAll();
        $_inserting = isset($items[0]) ? false : true;
        if ($_inserting) {
            $_landcode = $this->BepaalLandCode($_SESSION['mod.mark.huidigecbi']);
            $_eersteitem = $this->GetEersteItem($_SESSION['mod.mark.huidigecbi']);
            $this->smarty->assign("eersteitem", $_eersteitem);
        } else {
            $_landcode = $items[0]['LANDCODE'];
        };
        $items[0]['VERSPREIDKOSTEN'] = number_format((float) str_replace(',','.',$items[0]['VERSPREIDKOSTEN']), $this->_nrOfDesc, ',','.');
        $this->smarty->assign("items", $items);
        $this->smarty->assign("landenopties", $this->GetLanden());
        $this->smarty->assign("regioopties", $this->GetRegios($_landcode));
        $this->smarty->assign("herhalingvan", $this->GetHerhalingVan($rowid, $_SESSION['mod.mark.huidigecbi']));
//        $this->smarty->assign("advertentiecodes", $this->GetAdvertentieCodes());
        $this->smarty->assign("advertentiecode", $_SESSION['mod.mark.huidigecbi']);
        $this->smarty->assign("verspreideropties", $this->GetVerspreiders($_landcode));
        $this->smarty->display("verspreiding_insert.tpl.php");
    }

    function GetHuidigeCBI() {
        if ($_GET['cbi'] != '') {
            $_cbi = $_GET['cbi'];
        } elseif (isset($_SESSION['mod.mark.huidigecbi'])) {
            $_cbi = $_SESSION['mod.mark.huidigecbi'];
        } else {
            if (isset($this->advcodes)) {
                $_cbi = array_keys($this->advcodes);
                $_cbi = $_cbi[0];
            }
        }
        $_SESSION['mod.mark.huidigecbi'] = $_cbi;
        return $_cbi;
    }

    function GetHuidigeRecordID($codes, $advcode) {
        foreach($codes as $_taalgroep) {
            foreach($_taalgroep as $_advcode) {
                if ($_advcode['ADVERTENTIECODE'] == $advcode) {
                    return $_advcode['PLR__RECORDID'];
                }
            }
        }
    }

    function GetBriefkoppelingen() {
        $this->userdatabase->SetFetchMode(ADODB_FETCH_ASSOC);
        $_sql = "SELECT DISTINCT BRIEFKOPPELING FROM CURSUSBRIEFINHOUD";
        $rs = $this->userdatabase->GetAll($_sql);
        $this->userdatabase->SetFetchMode(ADODB_FETCH_NUM);
        return $rs;
    }

    function GetBriefCodes() {
        $this->userdatabase->SetFetchMode(ADODB_FETCH_ASSOC);
        $_sql = "SELECT DISTINCT VERZENDONDERDEELCODE FROM VERZENDONDERDEEL WHERE SOORT = 'BB'";
        $rs = $this->userdatabase->GetAll($_sql);
        $this->userdatabase->SetFetchMode(ADODB_FETCH_NUM);
        return $rs;
    }

    function ShowListView() {
        $this->form->recordcount = -1;
        $this->smarty->display("briefkoppeling.tpl.php");
        $this->form->LoadViewContent($detail=false, $masterrecord=false, $limit=false, $this->__GetCustomFilter());
        $this->form->ShowListView($state='view', $this->permission);
    }

    function GetVerspreiderItems($_scores) {
        if (is_array($_scores))
            return array_keys($_scores);
        else
            return FALSE;
    }

    function Show($outputformat='xhtml', $rec=false) {
        global $_GVARS;

        parent::Show($outputformat, $rec);

        $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/cursusbriefinhoud.js';

        if ($_GET['func'] != '') {
            if ($_GET['func'] == 'briefkoppelingen') {
                echo json_encode($this->GetBriefkoppelingen());
                exit;
            }
            if ($_GET['func'] == 'briefcodes') {
                echo json_encode($this->GetBriefCodes());
                exit;
            }
        } else {
            switch ($this->moduleid) {
            case 1:
                /* Show the Delete button in list view */
                $this->showDefaultButtons = true;
                switch($_GET['action']) {
                case 'edit':
                case 'insert':
                    $this->ShowItemView();
                    break;
                default:
                    $this->ShowListView();
                    break;
                }
            break;
            case 2:
                $this->showDefaultButtons = false;
                $this->ShowScoreOverzicht();
            break;
            }
        }
    }

}