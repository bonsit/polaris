<?php
require_once('includes/mainactions.inc.php');
require_once('_shared/module._base.php');

class Module extends _BaseModule {
    var $database; // plr database object
    var $cacheTimeOut;

    function Module($moduleid, $module) {
        global $_GVARS;
        global $polaris;

        parent::_BaseModule($moduleid, $module);

        $this->cacheTimeOut = 5*60;

        $this->processed = false;
        /**
        * What client are we talking about? (nog doen: eventueel selecteerbaar maken)
        */
        $this->clientid = $_SESSION['clientid'];

        $this->currentdb = 3;

        $this->database = new plrDatabase($polaris);
        $this->database->LoadRecord(array($this->clientid, $this->currentdb));
        $this->userdatabase = $this->database->connectUserDB();

        /* Show the Delete button in list view */
        $this->showDefaultButtons = true;

        if (isset($_GET['medewerker']))
            $_SESSION['sqlquery_currentuserid'] = $_GET['medewerker'];
        elseif (isset($_SESSION['sqlquery_currentuserid']))
            $_SESSION['sqlquery_currentuserid'] = $_SESSION['sqlquery_currentuserid'];
        else
            $_SESSION['sqlquery_currentuserid'] = $_SESSION['userid'];
    }

    function Process() {
        if ($this->form->database)
            $this->form->database->SetOracleUserID($_SESSION['userid']);
    }

    function GetSQLParameters($sql, &$params) {
        $sqlready = true;

        $vars = $this->database->SQLHasParams($sql);

        if ($vars) {
            foreach($vars as $var) {
                $name = str_replace(":","",$var);
                $pargetname = '_hdn'.$name;
                $parvalue = $_POST[$pargetname];
                $params[$name] = $parvalue;
                $sqlready = ($parvalue != '');
            }
        }
        return $sqlready;
    }

    function ShowQueryResult($justparams=false) {
        global $gSQLBlockRows;
        global $polaris;

        $_params = null;

        $_sql = $_REQUEST['queryinhoud'];
        if ($_sql) {
            $sqlready = $this->GetSQLParameters($_sql, $_params);
            if (!$sqlready) {
                $form = '';
                $vars = $this->database->SQLHasParams($_sql);

                if ($vars) {
                    $vars = array_unique($vars);
                    foreach($vars as $var) {
                        $name = str_replace(":","",$var);
                        $pargetname = '_hdn'.$name;
                        $parvalue = $_POST[$pargetname];
                        $form .= $name." <input type='text' name='$pargetname' value='$parvalue' /><br />";
                    }
                    $result = array('showparameterform'=>true, 'form'=>$form, 'params'=>$vars);
                    $result = json_encode($result);
                } else {
                    if ($justparams)
                        $sqlready = false;
                    else
                        $sqlready = true;

                }
            }
            if ($sqlready and !$justparams) {
                $gSQLBlockRows = 1000;
                try {
                    $this->userdatabase->SetFetchMode(ADODB_FETCH_NUM);
                    $_sql = str_replace(array("Â","\r\n","\n","\r"),"\r\n",$_sql);
                    $_sql = str_replace(array(chr(160),chr(194))," ",$_sql);

                    if ($_REQUEST['_hdnFLUSHCACHE'] == 'yes') {
                        $rs = $this->userdatabase->Execute($_sql, $_params);
                    } else {
                        $rs = $this->userdatabase->CacheExecute($this->cacheTimeOut, $_sql, $_params); // five minutes
                    }

/* IDENTIFIER TOO LONG problem:
                    if ($_REQUEST['_hdnFLUSHCACHE'] == 'yes') {
                        $rs = $this->userdatabase->SelectLimit($_sql, -1, -1, $_params);
                    } else {
                        $rs = $this->userdatabase->CacheSelectLimit($this->cacheTimeOut, $_sql, -1, -1, $_params); // five minutes
                    }
*/

                    $this->DoStats('execute');
                    include('tohtml.inc.php');
                    $result = rs2html($rs, 'id="datalistview" class="data striped nowrap group-table right" cellspacing="0" cellpadding="0"',$zheaderarray=false,$htmlspecialchars=true,$echo = false);
                } catch (Exception $E) {
                    $result =  $E->msg.'<br />SQL: '.$_sql;
                }
            }
            echo $result;
        }
    }

    function DoStats($type) {
        if (isset($_REQUEST['querynr']) and $_REQUEST['querynr'] != '') {
            $_insertSQLQueryStats = "INSERT INTO NHA.SQLQUERY_STATS (QUERYNR, ACTIONTYPE) VALUES('{$_REQUEST['querynr']}', '{$type}') ";
            $this->userdatabase->Execute($_insertSQLQueryStats);
        }
    }

    function ExportQueryResult() {
        global $polaris;

        $_params = null;

        $_sql = $_REQUEST['queryinhoud'];
        $sqlready = $this->GetSQLParameters($_sql, $_params);

        if ($_sql and $sqlready) {
            try {
                switch($_POST['_hdnExportType']) {
                case 'csv':
                case 'xsl':
                    $this->userdatabase->SetFetchMode(ADODB_FETCH_ASSOC);
                    break;
                default:
                    $this->userdatabase->SetFetchMode(ADODB_FETCH_NUM);
                    break;
                }

                $_sql = str_replace(array("\r\n","\n","\r"),"\r\n",$_sql);
                $_sql = str_replace(array(chr(160),chr(194))," ",$_sql);

                if ($_REQUEST['_hdnFLUSHCACHE'] !== 'yes') {
                    $rs = $this->userdatabase->Execute($_sql, $_params);
                } else {
                    $rs = $this->userdatabase->CacheExecute($this->cacheTimeOut, $_sql, $_params); // five minutes
                }
                $this->DoStats($_POST['_hdnExportType']);

                switch($_POST['_hdnExportType']) {
                case 'csv':
                    header("Content-type: text/x-csv\n");
                    header("Content-Disposition: attachment; filename=".$_POST['querynaam'].".csv");
                    require_once('toexport.inc.php');
                    $result = rs2csv($rs);

                    break;
                case 'xsl':
                    require_once('includes/excel.inc.php');
                    $filename = sanitize_filename($_POST['querynaam'], 'xls');
                    $excel = new ExcelExport($rs, $_POST['querynaam']);
                    $excel->download_with_dialog($filename);

                    break;
                case 'xml':
//                    header("Content-type: text/xml\n");
//                    header("Content-Disposition: attachment; filename=querygenerator.xml");
                    require_once('includes/adodb_toxml.inc.php');
                    $result = rs2xml($rs, 'querygeneratorexport');
                    break;
                case 'xhtml':
                    header("Content-type: text/html; charset=utf8; \r\n");

                    include('tohtml.inc.php');
                    global $gSQLBlockRows;
                    $gSQLBlockRows = 10000;

//                    require_once('toexport.inc.php');
                    $result = '<html><body><style>table{ font-size:0.95em;} table th{text-align:left;} table td {border-bottom:1px solid #ccc; padding:2px;min-width:80px;max-width:500px}</style>
                    <h3>'.$_POST['querynaam'].'</h3>
                    <pre style="">';
                    $zheaderarray = array();
                    for ($i=0;$i<=$rs->FieldCount()-1;$i++) {
                        $field = $rs->FetchField($i);
//                        $zheaderarray[] = $field->name;
                        if ($field->max_length == -1) {
                            $maxlength = 10;
                        } else {
                            $maxlength = round($field->max_length * 0.80);
                        }
                        $zheaderarray[] = substr($field->name,0,$maxlength);
                    }
                    $result .= rs2html($rs, 'id="xdatalistview" class=""',$zheaderarray,$htmlspecialchars=true,$echo = false);
//                    $result .= rs2tab($rs);
                    $result .= '</pre><script>window.print();</script></body></html>';
                    break;
                }
                echo utf8_decode($result);
            } catch (Exception $E) {
                echo $E->msg;
            }
        }
    }

    function ShowQueryGenerator() {
        $rowid = $this->form->database->customUrlDecode($_GET['rec']);
        $this->form->startno = 0;
        $_rs = $this->form->GetViewContent($detail=false, $masterrecord=false, false, $filter="ttt.ROWID = '{$rowid}'");
        $items = $_rs->GetAll();
        $this->smarty->assign("items", $items);
        $this->smarty->display("querygenerator.tpl.php");
    }

    function __GetCustomFilter() {
        if ($_SESSION['sqlquery_currentuserid'] != "") {
            $customfilter = 'MEDEWERKERNR='.$_SESSION['sqlquery_currentuserid'];
        } else {
            $customfilter = false;
        }
        return $customfilter;
    }

    function ShowQueryList() {
        $_sql = "SELECT USERGROUPID, USERGROUPNAME FROM NHA.PLR_USERGROUP ORDER BY USERGROUPNAME";
        $this->smarty->assign('medewerkers', $this->userdatabase->GetAssoc($_sql));
        $this->smarty->assign('huidigemedewerker', $_SESSION['sqlquery_currentuserid']);
        $this->smarty->display("queries.tpl.php");

        $this->form->record->VIEWTYPEPARAMS .= ';showactions=false';
        $this->form->LoadViewContent($detail=false, $masterrecord=false, $limit=false, $this->__GetCustomFilter());
        $this->form->recordcount = -1;
        $this->form->ShowListView($state='view', $this->permission);
    }

    function Show($outputformat='xhtml', $rec=false) {
        global $_GVARS;

        parent::Show($outputformat, $rec);

        if ($outputformat == 'csv' or $outputformat == 'xsl') {
            $this->ExportQueryResult();
        } else {
            if ($_REQUEST['_hdnAction'] == 'execute') {
                $this->ShowQueryResult($_REQUEST['justParameters']=='true');
            } else {
                $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/querygenerator.js';

                switch($_GET['action']) {
                case 'edit':
                case 'insert':
                    $this->ShowQueryGenerator();
                    break;
                default:
                    $this->ShowQueryList();
                    break;
                }
            }
        }
    }

}