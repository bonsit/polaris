var editorOn = false;

var nhaTrans = {
    initialize: function() {
        $("#contextselection").change(function() {
            $("input[name=_hdnAction]").val('__nothing__');
            this.form.submit();
        });
        $("input[name=submit_form]").click(function() {
            $("input[name=_hdnAction]").val('save');
            this.form.submit();
        });

        $("#translations :input:not([readonly='readonly']):first").focus();

        if (feedback) {
            Polaris.Base.modalMessage('Records opgeslagen', 1000);
        }
    }
}

$(document).ready(function(){
    nhaTrans.initialize();
});
