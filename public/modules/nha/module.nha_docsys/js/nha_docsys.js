var editorOn = false;

var nhaDocSys = {
    saveLink: function(elem, method) {
        var callback = function(data, status) {
            if (status == 'success') {
                elem.attr('rel', data /* store the RecordID */);
            } else {
                alert(data);
            }
        };
        //id:elem.attr('id'), domainid:Polaris.Base.getLocationVariable('domain'), uiid:Polaris.Base.getLocationVariable('ui')
        var elem = $(elem);
        var pos = elem.position();
        var postmethod = method || "savelink";
        if (elem.attr('rel')) {
            var linkrecordid = elem.attr('rel').split('#')[1];
        }
        
        if (postmethod == 'savelink') {
            jQuery.post(_servicequery+'?output=json', {_hdnProcessedByModule:true, method:postmethod
            , uiid: $('#masterRecordId').val()
            , recordid:linkrecordid
            , top:pos.top, left:pos.left, width:elem.width(), height:elem.height()
            }, callback );
        } else if (postmethod == 'createlink') {
            jQuery.post(_servicequery+'?output=json', {_hdnProcessedByModule:true, method:postmethod
            , uiid: $('#masterRecordId').val()
            , top:pos.top, left:pos.left, width:elem.width(), height:elem.height()
            }, callback );
        }
    },

    addDomain: function(values) {
        var params = {_hdnProcessedByModule:true, method:"adddomain", domainname: values[0].value};
        jQuery.post(_servicequery, params, function(data, status) {
            if (status == 'success') {
                $("#domainselector").addOptions(data, 'DOMAINID', 'DOMAINNAME', values[0].value);
            }
        });
    },

    saveElement: function(e) {
        var params = $(this.form).serializeArray();

        jQuery.post(_servicequery+'?output=json', params );
        e.preventDefault();
    },

    createNewLink: function(elem) {
        $("#uielements li").resizable('destroy');
        if ($("#uielements li:last").length)
            var linkId = parseInt($("#uielements li:last").attr('id').substr(3))+1;
        else
            var linkId = 1;
        var li = $("<li id='id_"+linkId+"'></li>");
        $("#uielements").append(li);
        nhaDocSys.saveLink(li, method='createlink');
        nhaDocSys.initFunctionLink(li);
    },
    
    removeLinks: function() {
        var isDeleted = function() {
            $("#uielements li.ui-selected").remove();
        };
        
        if (($("#uielements li.ui-selected").length > 0)) {
            $("#uielements li.ui-selected").addClass('confirm');
            if (confirm('Weet u zeker dat u deze link wilt verwijderen?')) {
                $("#uielements li.ui-selected").each(function(){
                    jQuery.post(_callerquery, {_hdnProcessedByModule:true, method:"deletelink", recordid:$(this).attr('rel').split('#')[1]}, isDeleted);
                })
            } else {
                $("#uielements li.ui-selected").removeClass('confirm');
            }
        }
    },
    
    initUIElements: function(elems) {
        elems.each(function() {
            nhaDocSys.initFunctionLink(this)
        });
    },
    
    initFunctionLink: function(elem) {
        if (editorOn) {
            nhaDocSys.initEditMode(elem);
        } else {
            nhaDocSys.initViewMode(elem);
        }
    },
 
    clearElementFields: function() {
        /* Initialize the detail info */
        $('#ui_action').text('');
        $('#actionTypeSP').attr('checked','');
        $('#actionTypeDLL').attr('checked','');
        $('#actionTypeRPT').attr('checked','');
        $('#linkedObjectSP').val('');
        $('#linkName').val('');
    },
    
    fillElementFields: function(elem) {
        var result = '';
        $(ui_data).each(function() {
            /* Lookup the current element  */
            if (this.PLR__RECORDID == $(elem).attr('rel').split('#')[1]) {
                /* and display the detail information */
                $('#linkName').val(this.NAME);

                if (this.ACTIONTYPE == 'SP')
                    $('#actionTypeSP').attr('checked', 'checked');
                else if (this.ACTIONTYPE == 'DLL')
                    $('#actionTypeDLL').attr('checked', 'checked')
                else if (this.ACTIONTYPE == 'RPT')
                    $('#actionTypeRPT').attr('checked', 'checked');
                $('#ui_action').text(this.ACTION);
                $('#linkedObjectSP').val(this.LINKEDOBJECT);
                $('#elementRecordId').val(this.PLR__RECORDID);

                $('#linkedObjectContainer').hide();
                if ($('#actionTypeSP').attr('checked'))
                    $('#linkedObjectContainer').show();
        
                /* Enable the submit button */
                $('#elementSubmit').attr('disabled','');
                result = this.LINKEDOBJECT;
            }
        });
        return result;
    },

    initViewMode: function(elem) {
        $(elem).click(function() {
            nhaDocSys.clearElementFields();

            $("#uielements li").not(this).removeClass("ui-selected");
            $(this).addClass("ui-selected");
            nhaDocSys.fillElementFields(this);
        })
        .dblclick(function() {
            var linkedObject = nhaDocSys.fillElementFields(this);
            window.location = _serverroot+'/app/docsys/const/packages/?itemname='+linkedObject;
        });
    }, 

    initEditMode: function(elem) {
        $(elem).resizable({
            handles:'all',
            autoHide:true,
            stop: function(e, ui) { 
                nhaDocSys.saveLink(this);
            } 
        })
        .draggable({
            cursor: 'pointer',
            stop: function(e, ui) {
                nhaDocSys.saveLink(this);
            }
        })
        .unbind('click')
        .click(function() {
            nhaDocSys.clearElementFields();
            $("#uielements li").not(this).removeClass("ui-selected");
            $(this).toggleClass("ui-selected");
            if ($(this).hasClass("ui-selected"))
                nhaDocSys.fillElementFields();
        });
    },
    
    toggleEditorMode: function(e) {
        $("#editorpanel").slideToggle("fast").hide();
        $(this).toggleClass('switchOn');
        editorOn = !editorOn;
        if (editorOn) {
            $(this).html("Editor Uit");
            nhaDocSys.initUIElements($("#uielements li"));
        } else {
            $("#uielements li").resizable('destroy').draggable('destroy').removeClass('editing');
            $(this).html("Editor Aan");
        }
        e.preventDefault();
    },

    /***
    * Modeling functions 
    */
    drawMethode: function(canvas, i, methodtype, methodname, options) {
        // beschrijving, parameters, gebruikt, onclick
        var m = Polaris.Modeler;
        var spDefaultHeight = 100;
        var spOffsetX = 310;
        var spOffsetY = 100;
        var packX = procX = funcX = 0;
        
        var isExpanded = Polaris.Base.getLocationVariable('expand') == 'true';
        if (isExpanded) spOffsetX = 100;
        var boxWidth = (isExpanded)?m.activityBoxExpandWidth:m.activityBoxWidth;
        var boxHeight = (isExpanded)?m.activityBoxExpandHeight:Math.max(m.activityBoxHeight, $(options.parameters).length * (m.paramBoxHeight + 10));
        boxHeight = boxHeight + 2;

        var paper = Raphael($('#'+canvas+' .diagram')[0], '99%', boxHeight + spDefaultHeight);

        var docCount = pckCount = procCount = funcCount = 0;
        if (options.uses) {
            $(options.uses).each(function(i) {
                var self = this;
                switch(self[0]) {
                case 'TABLE': docCount++;
                    var clickEvent = function(){
                        window.location = _serverroot+'/app/docsys/const/ftd/?qc%5B%5D=FACTTYPENAME&q%5B%5D=%22'+self[1]+'%22&autoexpand=true';
                    };
                    var table = m.drawSubDocument(paper, spOffsetX+10 + ((docCount - 1) * 60), 0, self[1], 1, {onclick: clickEvent});
                break;
                case 'PACKAGE': pckCount++;
                    if (isExpanded) {
                        var clickEvent = function() { window.location = _serverroot+'/app/docsys/const/packages/?pack='+self[1]; }
                        var pack = m.drawSubActivity(paper, spOffsetX*2 + 20 + packX, spOffsetY+boxHeight*1.8, self[0], self[1], collapsed=false, {scale:0.5, onclick: clickEvent});
                        packX = packX + pack[1];
                    }
                break;
                case 'PROCEDURE': procCount++; 
                    if (isExpanded) {
                        var clickEvent = function() { window.location = _serverroot+'/app/docsys/const/storedprocs/?itemname='+self[1]+'&pack='+Polaris.Base.getLocationVariable('pack'); }
                        var proc = m.drawSubActivity(paper, spOffsetX*2 + 20 + procX, spOffsetY+boxHeight*1.4, self[0], self[1], collapsed=false, {scale:0.5, onclick: clickEvent});
                        procX = procX + proc[1];
                    }
                break;
                case 'FUNCTION': funcCount++; 
                    if (isExpanded) {
                        var clickEvent = function() { window.location = _serverroot+'/app/docsys/const/functions/?itemname='+self[1]+'&pack='+Polaris.Base.getLocationVariable('pack'); }
                        var func = m.drawSubActivity(paper,  spOffsetX*2 + 20 + funcX, spOffsetY+boxHeight*0.4, self[0], self[1], collapsed=false, {scale:0.5, onclick: clickEvent});
                        funcX = funcX + func[1];
                    }
                break;
                }
            });
        }

        /* Even niet: beetje druk op het scherm
        if (options.usedby) {
            var usedByY = 20;
            paper.text(0, usedByY, 'Deze methode wordt aangeroepen door:').attr({"text-anchor":"left", fontSize:"12px", fill:"#73AFB9"});
            $(options.usedby).each(function(i) {
                var ub = m.drawUsedBy(paper, 0, usedByY + 30 + i * 25, this[0], this[1]);
            })
        }
        */
        
        var c = m.drawActivity(paper, spOffsetX, spOffsetY, methodtype, methodname, collapsed=(procCount > 0 || pckCount > 0 || funcCount > 0), {expanded:isExpanded}, $(options.parameters).length);
        c[0].toBack();
        if (methodtype == 'function')
            c[0].attr({fill:'#E6F5EB'});
        if (options.remark == 'J')
            c[0].attr({fill:'#ccc'});
        
//        var converter = new Attacklab.showdown.converter();
        if (options.description) {
            var beschrijving = "<h3>Beschrijving</h3><p>"+options.description.replace(new RegExp( "\\n", "g" ),"<br />")+"</p>"; // replace to force NewLine in markdown   //converter.makeHtml()
            $('#'+canvas+' .description').html(beschrijving);
        }
        
        if (options.usedby || options.uielements) {
            var beschrijving = "<h4>Deze methode wordt aangeroepen door</h4><ul>";
            $(options.usedby).each(function(i) {
                if (this[0])
                    beschrijving = beschrijving + '<li>'+this[0]+' <a href="'+_serverroot+'/app/docsys/const/triggers/?itemname='+this[1]+'">'+this[1]+'</a></li>'; 
            })

            $(options.uielements).each(function(i) {
                if (this[0])
                    beschrijving = beschrijving + '<li>Macs Formulier <a href="'+_serverroot+'/app/docsys/const/overview/?domain=1&ui='+this[0]+'">'+this[1]+'</a></li>'; 
            })

            beschrijving = beschrijving + '</ul>'; 
            $('#'+canvas+' .extrainfo').html(beschrijving);
        }

        if (options.detaildescription) {
            $(options.detaildescription).each(function(i) {
                var beschrijving = "<h4>Detail beschrijving</h4><p>"+this.replace(new RegExp( "\\n", "g" ),"<br />")+"</p>";  //converter.makeHtml(
                $(beschrijving).appendTo($('#'+canvas+' .description'));
            })
        }
        
        $('#'+canvas+' .diagram').height(120 + c[0][0].height.animVal.value);
        if (options.parameters)
            $(options.parameters).each(function(i) {
                m.drawActivityParam(paper, spOffsetX, spOffsetY, this[2], this[0]+' '+this[1], {expanded:isExpanded}, $(options.parameters).length);
            });

        if (options.sourcecode) {
            var sourcecode = '<code><pre>'+options.sourcecode+'</pre></code>';
            $('#'+canvas+' .sourcecode').html(sourcecode);
        }

    },
    
    drawEvent: function(canvas, i, eventtype, event, options) {
        // beschrijving, parameters, gebruikt, onclick
        var m = Polaris.Modeler;

        var spDefaultHeight = 100;
        var spOffsetX = 310;
        var spOffsetY = 100;

        var boxWidth = m.activityBoxWidth;
        var boxHeight = m.activityBoxHeight;

        var paper = Raphael($('#'+canvas)[0], '99%', boxHeight + spDefaultHeight);
        m.drawEvent(paper, 100, 100, 30, eventtype, {});
        
        paper.text(150,150, event.EVENTNAME);
    }
}

initOverview = function() {
    /**
    * Functie acties
    */
    $("#editorpanel").hide();
    $("#toggleEditor").click( nhaDocSys.toggleEditorMode );
    nhaDocSys.initUIElements($("#uielements li"));

    $('#addLink').click(function(){ nhaDocSys.createNewLink() });
    $('#removeLink').click(function(){ nhaDocSys.removeLinks() });

//    $('#functionlinks').selectable({stop: function(){
//        alert($('#functionlinks li.ui-selected').length)
//    }});

    $('#elementSubmit').attr('disabled','disabled').click(nhaDocSys.saveElement);
    
    $('#ajaxfeedback')
    .ajaxStart(function() { $(this).fadeIn('slow') })
    .ajaxStop(function() { $(this).fadeOut('slow') });
}


var initStoredProcView = function(storedProcedure) {
    $(storedProcedure.object).each(function(i) {
        var self = this;
        nhaDocSys.drawMethode(storedProcedure.canvasID, i, this.METHODTYPE, this.METHODNAME, 
            {description: this.DESCRIPTION
            , detaildescription: this.DETAILDESCRIPTION
            , sourcecode: this.SOURCECODE
            , parameters: this.PARAMETERS
            , uses: this.USES
            , usedby: this.USEDBY
            , uielements: this.UIELEMENTS
            , conversion: this.REMARK
            , onclick: function() { window.location='?item='+self.METHODNAME }
            });

    })
}

var initEventsView = function(events) {
    var m = Polaris.Modeler;
    $(events.object).each(function(i) {
        nhaDocSys.drawEvent(events.canvasID, i, 'TIMER',  this);
    })
}

var toggleFactTypeDiagram = function(elem) {
    var self = elem; 
    var container = $(self).parent("td").children(".ftdcontent");
    if (container.children(".sentencepattern").text() != '') {
        container.toggle('fast');
        $(elem).toggleClass('down');
    } else {
        var ft = $(self).attr("rel");
        var canvas = container[0];
        $.getJSON(_serverroot+'/services/json/app/docsys/const/ftd/', 
            {method:'facttypestruct', _hdnProcessedByModule: 'true', facttype: ft}, function(data) {
                Polaris.Modeler.drawFTD(canvas, data);
                container.toggle('fast');
                $(elem).toggleClass('down');
            });
    }
}

var initProgressView = function(initPerc) {
    $(".progressBar").each(function() {
        if (initPerc == true) {
            var perc = 0;
        } else {
            var perc = parseInt($(this).attr('value'));
        }
        var color = '';
        if (perc < 25)
            color = 'red';
        else if (perc >= 25 && perc < 70)
            color = 'orange';
        else if (perc >= 70 && perc < 100)
            color = 'yellow';
        else
            color = 'green';
        $(this).progressBar(perc, { barImage: _serverroot+'/javascript/jquery/images/progressbg_'+color+'.gif'});
        $('#importPLSQL span').remove();
        $('#importPLSQL').attr('disabled','');
    });
}

$(document).ready(function(){
    $('#constructtitle').append($('#domainselect'));
    if ($("#editorpanel").length)
        initOverview();

    $('.ui_group h1').click(function(){ $('ul.userinterface',$(this).parent()).slideToggle(); $(this).toggleClass('down'); }).css({'cursor':'pointer'});
    $('.ui_group a.ui_image').hover(function(){ $('.ui_elements', $(this).parent()).slideDown('fast') }, function(){ $('.ui_elements', $(this).parent()).slideUp('fast') });

    if (typeof(spInit) != "undefined")
        initStoredProcView(spInit);

    $("#methodlist.group .head").click(function() {
        $(this).next().slideToggle("fast");
    }).next().hide();
    $('#methodlist.group ul.currentpack').show();
    $("#methodlist.group li.current").parent().show();
    $("#methodlist.group li.hassearch").parent().show();
    if ($("#methodlist.group ul:visible").length == 0)
        $("#methodlist.group ul:first").show();

    if (typeof(eventsInit) != "undefined")
        initEventsView(eventsInit);
    
    $('.toggleSwitch').click(function(){ $(this).siblings(".toggler").slideToggle() });
    
    $('.toggleFTD').click( function () { toggleFactTypeDiagram(this) } );
    $('.toggleFTD.autoexpand').each( function() { toggleFactTypeDiagram(this) } );
    
    if ($('#progress').length) initProgressView();
    $('#importPLSQL').click(function(e) {
        initProgressView(true);
        $('#importPLSQL').append('<span>&nbsp; Even geduld a.u.b...</span>').attr('disabled','true');
        $("#importresult").load(window.location.protocol+'//'+window.location.host+"/nha/import.php", function() {initProgressView();});
        e.preventDefault();
    });

    $("#btnAddDomain").click(function() {
        Polaris.Window.showModuleForm('/modules/nha/module.nha_docsys/html/form_adddomain.tpl.php', function(data) {
            nhaDocSys.addDomain(data);
        });
    });
});
