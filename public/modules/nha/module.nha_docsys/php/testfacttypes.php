<?php
require_once('../../../includes/app_includes.inc.php');
/**
 * Create the Polaris object
 */
$polaris = new Polaris();
$polaris->initialize();
$polaris->instance->SetFetchMode(ADODB_FETCH_ASSOC);

$_sql = '
SELECT f.*, group_concat(p.PLACEHOLDERNAME) AS PLACEHOLDERS 
FROM mod_facttype f 
LEFT JOIN mod_facttype_placeholder p 
ON f.clientid = p.clientid AND f.domainid = p.domainid AND f.facttypeid = p.facttypeid
WHERE COMMENT <> "VIEW"
GROUP BY f.facttypeid';
$_facttypes = $polaris->instance->GetAll($_sql);
foreach($_facttypes as $_k => $_facttype) {
    $_placeholders = explode(',',$_facttype['PLACEHOLDERS']);

    echo "<h4>".$_facttype['FACTTYPENAME']."</h4>";    
    foreach($_placeholders as $_placeholder) {
        if (stripos($_facttype['SENTENCEPATTERN'], "<".$_placeholder.">") === FALSE) {
            echo $_facttype['FACTTYPENAME'] . " => " .$_placeholder . " niet aanwezig in zinssjabloon.<br />";
        }
    }

    preg_match_all("/<[^>]+>/",$_facttype['SENTENCEPATTERN'], $_sp_placeholders);
    foreach($_sp_placeholders[0] as $_sp_placeholder) {
        $_sp_placeholder = str_replace(array("<",">"),array("",""),$_sp_placeholder);
        if (!in_array($_sp_placeholder, $_placeholders)) {
            var_dump($_facttype['SENTENCEPATTERN']);
            echo $_facttype['FACTTYPENAME'] . " => " .$_sp_placeholder . " niet aanwezig als invulplaats.<br />";
        }
    }
}
?>