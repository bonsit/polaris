<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

/**
 * Smarty count_words modifier plugin
 *
 * Type:     modifier<br>
 * Name:     hyperlinks<br>
 * Purpose:  transform sentence pattern markups to  hyperlinks
 * @author   Bart Bons
 * @param string
 * @return string
 */
function smarty_modifier_hyperlinks($string, $name) {
    $pattern = '/\[([a-zA-Z0-9]+)\]/';
    $replacement = '<a href="#concept_${1}">${1}</a>';
    $string = preg_replace($pattern, $replacement, $string);

    $pattern = '/\{([a-zA-Z0-9]+)\}/';
    $replacement = '<span class="concept">${1}</span>';
    $string = preg_replace($pattern, $replacement, $string);
    return $string;
}

/* vim: set expandtab: */

?>