{include file="shared.tpl.php"}

<div id="instellingen">
<p>Door de PL/SQL broncode te importeren worden alle stored procedures, triggers, packages, functions en bijbehorende beschrijvingen in het DocumentatieSysteem opgenomen. 
Tevens worden van alle processen de in- en uitgaande parameters bepaald en de door processen gebruikte objecten (tabellen, packages en procedures) vastgesteld.</p>

<button id="importPLSQL">Importeer</button>
<br /><br />
<h3>Importeer resultaat</h3>
<div id="importresult"></div>

<p></p>
<h2>Importeer tabellen/views</h2>
<p>Alle tabellen en views van de gekozen database worden geïmporteerd in het DocumentatieSysteem.</p>
<form method="POST" action=".">
<input type="hidden" name="_hdnProcessedByModule" value="true" />
<input type="hidden" name="method" value="importtables" />
Database: {$databaseSelect}<br/>
<input id="importTabellen" type="submit" value="Importeer" /> &nbsp; <input type="checkbox" name="overwritefacttypes" value="Y" /> Verwijder bestaande feittypen
</form>
</div>

<div id="progress">
    <h2>Documentatiesyteem statistieken</h2>
    <table>
        <tr><td>Userinterfaces</td><td><span class="progressBar" id="pb_ui" value="{math equation=floor(100*(x/y)) x=$progress[0].ui_done y=$progress[0].ui_total}">{math equation=floor(100*(x/y)) x=$progress[0].ui_done y=$progress[0].ui_total}</span> ({$progress[0].ui_done} van {$progress[0].ui_total})</td></tr>
        <tr><td>Scheduled tasks</td><td><span class="progressBar" id="pb_ui" value="{math equation=floor(100*(x/y)) x=$progress[0].events_done y=$progress[0].events_total}">{math equation=floor(100*(x/y)) x=$progress[0].events_done y=$progress[0].events_total}</span> ({$progress[0].events_done} van {$progress[0].events_total})</td></tr>
        <tr><td>Begrippen</td><td><span class="progressBar" id="pb_concepts" value="{math equation=floor(100*(x/y)) x=$progress[0].concepts_done y=$progress[0].concepts_total}">{math equation=floor(100*(x/y)) x=$progress[0].concepts_done y=$progress[0].concepts_total}</span> ({$progress[0].concepts_done} van {$progress[0].concepts_total})</td></tr>
        <tr><td>Feittypen</td><td><span class="progressBar" id="pb_ftds" value="{math equation=floor(100*(x/y)) x=$progress[0].ftd_done y=$progress[0].ftd_total}">{math equation=floor(100*(x/y)) x=$progress[0].ftd_done y=$progress[0].ftd_total}</span> ({$progress[0].ftd_done} van {$progress[0].ftd_total})</td></tr>
        <tr><td>Packages</td><td><span class="progressBar" id="pb_functions" value="{math equation=floor(100*(x/y)) x=$progress[0].packs_done y=$progress[0].packs_total}">{math equation=floor(100*(x/y)) x=$progress[0].packs_done y=$progress[0].packs_total}</span> ({$progress[0].packs_done} van {$progress[0].packs_total})</td></tr>
        <tr><td>Stored procedures</td><td><span class="progressBar" id="pb_storedprocs" value="{math equation=floor(100*(x/y)) x=$progress[0].storedprocs_done y=$progress[0].storedprocs_total}">{math equation=floor(100*(x/y)) x=$progress[0].storedprocs_done y=$progress[0].storedprocs_total}</span> ({$progress[0].storedprocs_done} van {$progress[0].storedprocs_total})</td></tr>
        <tr><td>Functions</td><td><span class="progressBar" id="pb_functions" value="{math equation=floor(100*(x/y)) x=$progress[0].functions_done y=$progress[0].functions_total}">{math equation=floor(100*(x/y)) x=$progress[0].functions_done y=$progress[0].functions_total}</span> ({$progress[0].functions_done} van {$progress[0].functions_total})</td></tr>
        <tr><td>Triggers</td><td><span class="progressBar" id="pb_functions" value="{math equation=floor(100*(x/y)) x=$progress[0].triggers_done y=$progress[0].triggers_total}">{math equation=floor(100*(x/y)) x=$progress[0].triggers_done y=$progress[0].triggers_total}</span> ({$progress[0].triggers_done} van {$progress[0].triggers_total})</td></tr>
    </table>
    
    <h2>Opmerkingen</h2>
    <form action="{$callerquery}" method="POST">
    <input type="hidden" name="_hdnDatabase" value="{$module->form->databasehash}" />
    <input type="hidden" name="_hdnTable" value="{$commenttablename}" />
    <input type="hidden" name="_hdnAction" value="save" />
    <input type="hidden" name="_hdnState" value="{if $comment.PLR__RECORDID}edit{else}insert{/if}" />
    <input type="hidden" name="_hdnRecordID" value="{$comment.PLR__RECORDID}" />
    <input type="hidden" name="CLIENTID" value="%session[clientid]%" />
    <input type="hidden" name="DOMAINID" value="%session[domainid]%" />
    <input type="hidden" name="SUBJECTID" value="DOCSYS.CONFIG" />
    <textarea name="COMMENT" rows="15" cols="50">{$comment.COMMENT}</textarea><br />
    <input type="submit" value="{lang but_save}" />
    </form>
</div>
