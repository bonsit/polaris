{include file="shared.tpl.php"}
<script type="text/javascript" src="{$serverpath}/javascript/master-raphael.js"></script>

{if $smarty.get.const == 'packages'}
<ul id="methodlist" class="group">
    {section name=i loop=$methoditems}
        {if $methoditems[i].PACKAGE != $methoditems[$smarty.section.i.index_prev].PACKAGE}
        {if !$smarty.section.i.first}</ul></li>{/if}
        <li><a href="javascript:void(0)" class="head">{$methoditems[i].PACKAGE}</a>
        <ul {if $currentpackage == $methoditems[i].PACKAGE}class="currentpack"{/if}>
        <li class="{if $currentmethod == $methoditems[i].PACKAGEID}current {/if}{if $methoditems[i].PACKAGEHASSEARCH}hassearch{/if}"><a href="{urlquery_add item=$methoditems[i].PACKAGEID pack=$methoditems[i].PACKAGE}">&#8212; <b>DE GEHELE PACKAGE</b> &#8212;</a></li>
        {/if}
        <li class="{if $currentmethod == $methoditems[i].METHODID or $currentmethod == $methoditems[i].METHODNAME}current {/if}{if $methoditems[i].HASSEARCH}hassearch{/if}"><a href="{urlquery_add item=$methoditems[i].METHODID pack=$methoditems[i].PACKAGE}">{$methoditems[i].METHODNAME}</a></li>
    {/section}
    </ul>
</ul>
{elseif $smarty.get.const == 'triggers'}
<ul id="methodlist" class="group">
    {section name=i loop=$methoditems}
        {if $methoditems[i].TRIGGERTABLE != $methoditems[$smarty.section.i.index_prev].TRIGGERTABLE}
        {if !$smarty.section.i.first}</ul></li>{/if}
        <li><a href="javascript:void(0)" class="head">{$methoditems[i].TRIGGERTABLE}</a>
        <ul>
        {/if}
        <li class="{if $currentmethod == $methoditems[i].METHODID or $currentmethod == $methoditems[i].METHODNAME}current {/if}{if $methoditems[i].HASSEARCH}hassearch{/if}"><a href="{urlquery_add item=$methoditems[i].METHODID}">{$methoditems[i].TRIGGERTYPE} {$methoditems[i].TRIGGEREVENT}</a></li>
    {/section}
    </ul>
</ul>

{else}
<ul id="methodlist" class="group">
    <li class="{if $methoditems[i].HASSEARCH} hassearch{/if}">
    <a href="javascript:void(0)" class="head">ALGEMEEN</a>
    <ul>
    {section name=i loop=$methoditems}
        {if $methoditems[i].REMARK == 'N'}
        <li class="{if $currentmethod == $methoditems[i].METHODID or $currentmethod == $methoditems[i].METHODNAME}current {/if}{if $methoditems[i].HASSEARCH}hassearch{/if}"><a href="{urlquery_add item=$methoditems[i].METHODID}">{$methoditems[i].METHODNAME}</a></li>
        {/if}
    {/section}
    </ul>
    </li>
    <li class="{if $methoditems[i].HASSEARCH} hassearch{/if}">
    <a href="javascript:void(0)" class="head">CONVERSIE</a>
    <ul>
    {section name=i loop=$methoditems}
        {if $methoditems[i].REMARK == 'J'}
        <li class="{if $currentmethod == $methoditems[i].METHODID or $currentmethod == $methoditems[i].METHODNAME}current {/if}{if $methoditems[i].HASSEARCH}hassearch{/if}"><a href="{urlquery_add item=$methoditems[i].METHODID}">{$methoditems[i].METHODNAME}</a></li>
        {/if}
    {/section}
    </ul>
    </li>
</ul>
{/if}

{if $currentitem}
<div id="methodcontainer">

    <div class="pagetabcontainer">
        <ul id="pagetabs">
            <li ><a id="tab_method_diagram" href="#method_diagram">Diagram</a></li>
            <li ><a id="tab_method_specification" href="#method_specification">Specificaties</a></li>
            <li ><a id="tab_method_sourcecode" href="#method_sourcecode">Broncode</a></li>
        </ul>
    </div>

    <div id="method_diagram" class="pagecontents">
        <div class="diagram"></div>
        <div class="extrainfo"></div>
        <div class="description">Geen beschrijving</div>

    </div>

    <div id="method_sourcecode" class="pagecontents">
        <div class="sourcecode">Geen beschrijving</div>
    </div>

    <div id="method_specification" class="pagecontents">
        <div class="">
            <h1>{$currentitem.METHODNAME}</h1>
            <table class="specs">
                <tr><td>Type:</td><td>{if $currentitem.METHODTYPE=='packproc'}Package procedure{elseif $currentitem.METHODTYPE=='packfunc'}Package function{else}{$currentitem.METHODTYPE|ucwords}{/if}</td></tr>
                {if $currentitem.METHODTYPE == 'trigger'}
                    <tr><td>Werkt op tabel: &nbsp;</td><td>{$currentitem.TRIGGERTABLE}</td></tr>
                    <tr><td>Type: &nbsp;</td><td>{$currentitem.TRIGGERTYPE}</td></tr>
                    <tr><td>Soort impuls: &nbsp;</td><td>{$currentitem.TRIGGEREVENT}</td></tr>
                    {if $columns}
                    <tr><td>Trigger reageert op kolommen:</td><td>
                        {section name=i loop=$columns}
                            {if $columns[i].ISTRIGGERCOLUMN == 'NO'}
                                    {$columns[i].COLUMNNAME|lower},
                            {/if}
                        {/section}
                        </td>
                    </tr>

                    <tr><td>Kolommen die uitgelezen worden:</td><td>
                        {section name=i loop=$columns}
                            {section name=j loop=$columns[i].COLUMNUSAGE}
                            {if strpos($columns[i].COLUMNUSAGE[j], "IN") > 0}
                                {if $columns[i].COLUMNUSAGE[j][0] == 'O'}:OLD{else}:NEW{/if}.{$columns[i].COLUMNNAME|lower}<br />
                            {/if}
                            {/section}
                        {/section}
                        </td>
                    </tr>

                    <tr><td>Kolommen die een waarde krijgen:</td><td>
                        {section name=i loop=$columns}
                            {section name=j loop=$columns[i].COLUMNUSAGE}
                            {if strpos($columns[i].COLUMNUSAGE[j], "OUT") > 0}
                                :NEW.{$columns[i].COLUMNNAME|lower}<br />
                            {/if}
                            {/section}
                        {/section}
                        </td>
                    </tr>
                    {/if}

                {/if}

                {if $currentitem.METHODTYPE != 'package'}
                <tr><td>Parameters: &nbsp;</td><td>
                    {section name=i loop=$currentitem.PARAMETERS}
                        <strong>{$currentitem.PARAMETERS[i][0]}</strong>
                        {$currentitem.PARAMETERS[i][1]}
                        {$currentitem.PARAMETERS[i][2]}<br />
                    {/section}
                    </td>
                </tr>
                {/if}
                <tr><td>Gebruikt:</td><td>
                    {section name=i loop=$currentitem.USES}
                        {if $currentitem.USES[i][0] == 'TABLE'}Tabel <a href="{$serverroot}/app/docsys/const/ftd/?qc[]=facttypename&qv[]='{$currentitem.USES[i][1]}'&autoexpand=true">{$currentitem.USES[i][1]}</a>
                        {elseif $currentitem.USES[i][0] == 'PACKAGE'}Package  <a href="{$serverroot}/app/docsys/const/packages/?pack={$currentitem.USES[i][1]}">{$currentitem.USES[i][1]}</a>
                        {elseif $currentitem.USES[i][0] == 'FUNCTION'}Functie  <a href="{$serverroot}/app/docsys/const/function/?itemname={$currentitem.USES[i][1]}">{$currentitem.USES[i][1]}</a>
                        {elseif $currentitem.USES[i][0] == 'SEQUENCE'}Sequence  {$currentitem.USES[i][1]}
                        {elseif $currentitem.USES[i][0] == 'PROCEDURE'}Stored procedure  <a href="{$serverroot}/app/docsys/const/storedprocs/?itemname={$currentitem.USES[i][1]}">{$currentitem.USES[i][1]}</a>
                        {/if}
                        <br />
                    {sectionelse}
                        n.v.t.
                    {/section}
                    </td>
                </tr>
                <tr><td>Wordt gebruikt door: &nbsp;</td><td>
                    {section name=i loop=$currentitem.USEDBY}
                        {if ($currentitem.USEDBY[i][1] != $currentitem.METHODNAME)}
                            {if $currentitem.USEDBY[i][0] == 'TABLE'}Tabel
                            {else}{$currentitem.USEDBY[i][0]|lower|capitalize}
                            {/if}
                            <strong>{$currentitem.USEDBY[i][1]}</strong><br />
                        {/if}
                    {sectionelse}
                        n.v.t.
                    {/section}
                    </td>
                </tr>
                <tr><td>Wordt gebruikt in Macs formulier:</td><td>
                    {section name=i loop=$currentitem.UIELEMENTS}
                        <a href="{$serverroot}/app/docsys/const/overview/?domain=1&ui={$currentitem.UIELEMENTS[i][0]}">{$currentitem.UIELEMENTS[i][1]}</a>
                        <br />
                    {sectionelse}
                        n.v.t.
                    {/section}
                    </td>
                </tr>
                <tr><td>Gebruikt bij conversie: &nbsp;</td><td>{if $currentitem.REMARK =='N'}Nee{else}Ja{/if}</td></tr>
            </table>

        </div>
    </div>


</div>

<script>
{if $currentitem}
var data = {json array=$currentitem};
{else}
var data = [];
{/if}
spInit = {ldelim}'object': data, 'canvasID': 'methodcontainer' {rdelim}
</script>

{/if}