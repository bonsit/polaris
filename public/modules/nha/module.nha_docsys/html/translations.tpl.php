{include file="shared.tpl.php"}
<input type="hidden" name="_hdnProcessedByModule" value="true" />
<input type="hidden" name="method" value="savetranslation" />

{if $feedback}
<script type="text/javascript">var feedback = true;</script>
{/if}

Context: {$contextselection}

<table id="translations">
    <thead>
        <tr>
            <th>ID</th>
            <th>Nederlands</th>
            <th>Duits</th>
        </tr>
    </thead>
<tbody>
{section name=i loop=$records}
<tr>
    <td>
        <input type="hidden" readonly="readonly" name="id[]" value="{$records[i]._KEYVALUE_}" />
        <input type="text" readonly="readonly" value="{$records[i]._KEYVALUE_}" tabindex="-1" />
    </td>
    <td>
        <input type="text" size="70" name="{$records[i]._KEYVALUE_}_NL" value="{$records[i].TRANS_NL}" />
    </td>
    <td {if $records[i].TRANS_DE_PENDING}class="pending"{/if}>
        <input type="text" size="70" name="{$records[i]._KEYVALUE_}_DE" value="{$records[i].TRANS_DE}" />
    </td>
</tr>
{if ($smarty.section.i.index mod 20 eq 0 and $smarty.section.i.index > 4)
or $smarty.section.i.last}
<tr>
    <td></td>
    <td colspan="2">
        <input type="submit" name="submit_form" value="Opslaan" />
    </td>
</tr>
{/if}
{/section}
</tbody>
</table>