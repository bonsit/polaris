{include file="shared.tpl.php"}

{if $smarty.get.maximize != 'true'}
<script type="text/javascript" src="{$serverpath}/javascript/jquery/jquery.livesearch.js"></script>
{literal}<script>
_loadevents_[_loadevents_.length] = function() {
    $('#q').liveUpdate('#concepts').focus();
}
</script>{/literal}


<form method="get" autocomplete="off">
    <div>
        Snelzoeken: <input type="search" value="" name="q" id="q" />
    </div> 
</form>
{/if}

<ul id="concepts">
{section name=i loop=$items}
<li>
<h2>
<input id="rec@{$items[i].PLR__RECORDID}" name="rec@{$items[i].PLR__RECORDID}" type="checkbox" class="checkbox deletebox" />
<a name="concept_{$items[i].CONCEPT}"></a>{$items[i].CONCEPT} &nbsp;<a class="edit_action" href="{$serverpath}{$callerquery}edit/{$items[i].PLR__RECORDID}/">[{lang edit}]</a></h2>

{$items[i].DEFINITION|hyperlinks:$items[i].CONCEPT|markdown}
</li>
{/section}
</ul>
