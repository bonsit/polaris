{include file="shared.tpl.php"}
<script type="text/javascript" src="{$serverpath}/javascript/master-raphael.js"></script>

<ul id="storedprocs">

{section name=i loop=$items}
<li>
<h2>{$items[i].methodenaam}</h2>
<p>{$items[i].methodebeschrijving}</p>

{assign var=parameters value=$items[i].parameters}
{if $parameters[0]}
<ul class="params">
{foreach from=$parameters item=param}
<li>{$param}</li>
{/foreach}
</ul>
{/if}

{assign var=gebruikt value=$items[i].gebruikt}
{if $gebruikt[0]}
<h3>SP Gebruikt</h3>
<ul class="gebruikt">
{foreach from=$gebruikt item=object}
<li>{$object}</li>
{/foreach}
</ul>
{/if}

{assign var=wordgebruiktdoor value=$items[i].wordgebruiktdoor}
{if $wordgebruiktdoor[0]}
<h3>SP Wordt gebruikt door</h3>
<ul class="wordgebruiktdoor">
{foreach from=$wordgebruiktdoor item=object}
<li>{$object}</li>
{/foreach}
</ul>
{/if}

<a href="{$callerquery}?sp={$items[i].id}">
</a></li>
{/section}

</ul>