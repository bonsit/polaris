<link rel="stylesheet" type="text/css" media='all' href="{$moduleroot}/css/default.css" />

{if $domains}
<span id="domainselect">
 &nbsp; Kennisdomein: <select id="domainselector" {if $domains|@count == 1}disabled="disabled{/if}" onchange="window.location='{$serverpath}{$callerquery}?domain='+this.value">
{section name=i loop=$domains}
    <option value="{$domains[i].DOMAINID}" {if $smarty.session.domainid == $domains[i].DOMAINID}selected="selected"{/if}>{$domains[i].DOMAINNAME}</option>
{/section}
</select>
<button id="btnAddDomain">+</button>
</span>
{/if}