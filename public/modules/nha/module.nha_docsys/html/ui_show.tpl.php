{include file="shared.tpl.php"}
<div id="functionmain">
    <div id="ajaxfeedback">Wijzigingen opgeslagen <img src="{$serverroot}/images/wait.gif" /></div>
    
    <h1>{$items[0].NAME} <span><a class="popbox" href="{$serverpath}{$callerquery}edit/{$items[0].PLR__RECORDID}/?maximize=true&doautoclose=true">wijzig</a></span></h1>
    
    <div class="pagetabcontainer">
        <ul id="pagetabs">
            <li ><a id="tab_ui_processdescription" href="#ui_processdescription">Functionele beschrijving</a></li>
            <li ><a id="tab_ui_ui" href="#ui_ui">Gebruikersinterface</a></li>
        </ul>
    </div>

    <div id="ui_processdescription" class="pagecontents ui_processdescription">
        <img src="{$serverroot}/thumb/600/400/docsys/media/{$items[0].IMAGE|escape:'url'}?f=png)" class="smallui" />
        <div class="content">
        <h2>{$items[0].NAME}</h2>
        <h3>Functionele beschrijving</h3>
        {$items[0].PROCESSDESCRIPTION|default:"Geen procesbeschrijving bekend."|markdown}

        <hr />
        
        <h2>Procesbeschrijving vanuit broncode</h2>
        {if $processlist}
            {section name=i loop=$processlist}
            <h3>Procedure {$processlist[i].name}</h3>
            {$processlist[i].description|markdown}
            {/section}
        {else}
            <p>Geen beschrijving bekend.</p>
        {/if}

        <hr />

        {if $elements}
        <h2>Schermfuncties</h2>
        {section name=k loop=$elements}
        <h3>Knop: {$elements[k].NAME}</h3>
        {$elements[k].DESCRIPTION|default:$elements[k].UIDESCRIPTION|markdown}
        {if $elements[k].ACTIONTYPE == 'SP'}
        {/if}
        {/section}
        <hr />
        {/if}

        {if $facttypes}
            <h2>Hoofdtabellen</h2>
            {section name=i loop=$facttypes}
                {if $facttypes[i].facttype}<a href="{$serverroot}/app/docsys/const/ftd/?qc%5B%5D=FACTTYPENAME&q%5B%5D=%22{$facttypes[i].facttype}%22&autoexpand=true">{$facttypes[i].facttype}</a> {if $facttypes[i].action}({$facttypes[i].action}){/if}<br />{/if}        
                {if $facttypes[i].sentencepattern}
                    <div class="zinssjabloon">
                    <h3>Zinssjabloon (verwoording van de tabel):</h3>
                    <p>{$facttypes[i].sentencepattern|htmlspecialchars|markdown}</p>
                    </div>
                {/if}
            {/section}
    
            {if $facttypes[0].triggers}
                <h2>Bedrijfsregels (triggers)</h2>
                {section name=i loop=$facttypes}
                    {if $facttypes[i].facttype}<h2><em>Hoofdtabel: <a href="{$serverroot}/app/docsys/const/ftd/?qc%5B%5D=FACTTYPENAME&q%5B%5D=%22{$facttypes[i].facttype}%22&autoexpand=true">{$facttypes[i].facttype}</a> {if $facttypes[i].action}({$facttypes[i].action}){/if}</em></h2>{/if}        
                    {section name=j loop=$facttypes[i].triggers}
                        <h3>{$facttypes[i].triggers[j].TRIGGERTYPE_TRANS|replace:"%s":$facttypes[i].facttype} {$facttypes[i].triggers[j].TRIGGEREVENT_TRANS}:</h3>
                    
                        {$facttypes[i].triggers[j].DESCRIPTION|replace:"   ":""|markdown}
                    {/section}
                {/section}
            {/if}
        {/if}
        
        {if $items[0].COMMENT != ''}
        <hr />
        <h3>Commentaar</h3>
        <p>{$items[0].COMMENT|markdown}</p>
        {/if}
        </div>
    </div>

{*    
    <div id="ui_description" class="pagecontents ui_description">
        <img src="{$serverroot}/thumb/600/400/docsys/media/{$items[0].IMAGE|escape:'url'}?f=png)" class="smallui" />
        {$items[0].DESCRIPTION|default:"Nog geen beschrijving."|markdown}<br />
        
        {if $items[0].COMMENT != ''}
        <h3>Commentaar</h3>
        <p>{$items[0].COMMENT}</p>
        {/if}
    </div>
*}    
    <div id="ui_ui" class="pagecontents">
        <div class="docsyspanel" id="UI_panel">
            <a href="#" id="toggleEditor">Editor Aan</a>
            <ul id="editorpanel">
                <li><a href="#" id="addLink">Voeg een kader toe</a></li>
                <li><a href="#" id="removeLink">Verwijder kader</a></li>
            </ul>
            
            <ul>
                {if $items[0].FACTTYPE}<li>Hoofd feittype: <a href="{$serverroot}/app/docsys/const/ftd/?qc%5B%5D=FACTTYPENAME&q%5B%5D=%22{$items[0].FACTTYPE}%22&autoexpand=true">{$items[0].FACTTYPE}</a></li>
                <li><hr /></li>{/if}
                <li>Link Detail informatie</li>
                <li id="linkinfo">
                    <form method="POST" action=".">
                        <input type="hidden" name="_hdnProcessedByModule" value="true" />
                        <input type="hidden" name="method" value="saveelement" />
                        <input type="hidden" name="uiid" id="masterRecordId" value="{$uiid}" />
                        <input type="hidden" name="recordid" id="elementRecordId" value="" />
                        
                        <input type="text" name="linkname" size="35" id="linkName" value="" /><br />
                        
                        <input type="radio" name="actiontype" id="actionTypeSP" value="SP" /> StoredProc <br />
                        <input type="radio" name="actiontype" id="actionTypeDLL" value="DLL" /> DLL call <br />
                        <input type="radio" name="actiontype" id="actionTypeRPT" value="RPT" /> Rapport<br />
    
                        Aanroep: <textarea id="ui_action" name="action"></textarea><br />
                    
                        <div id="linkedObjectContainer" style="display:none;">Stored procedure: 
                        <select name="linkedobject" id="linkedObjectSP">
                        {section name=j loop=$storedprocs}
                            {if $storedprocs[j].GROUP != $storedprocs[$smarty.section.j.index_prev].GROUP}
                            {if !$smarty.section.j.first}</optgroup>{/if}
                                <optgroup LABEL="{$storedprocs[j].GROUP}">
                            {/if}
                            <option value="{if $storedprocs[j].PACKAGE}{$storedprocs[j].PACKAGE}.{/if}{$storedprocs[j].METHODNAME}" {if $storedprocs[j].PACKAGE+"."+$storedprocs[j].METHODNAME == $elements[i].LINKEDOBJECT}selected="selected"{/if}>{$storedprocs[j].METHODNAME}</option>
                        {/section}
                        </select>
                        </div>
                        <br /><input type="submit" id="elementSubmit" value="{lang but_save}" />
                     </form>
                </li>
            </ul>
        </div>
    
        <div id="uicontainer" style="background:url({$serverroot}/thumb/1024/900/docsys/media/{$items[0].IMAGE|escape:'url'}?f=png) no-repeat top left">
            <ul id="uielements">
            {section name=i loop=$elements}
            <li id="id_{$elements[i].ID}" style="top:{$elements[i].TOP}px;left:{$elements[i].LEFT}px;width:{$elements[i].WIDTH}px;height:{$elements[i].HEIGHT}px"
            rel="{$elements[i].UIID}#{$elements[i].PLR__RECORDID}" ></li>
            {/section}
            </ul>
        </div>
    
    </div>
</div>

<script>var ui_data = {if $elements}{json array=$elements}{else}{ldelim}{rdelim}{/if}</script>
