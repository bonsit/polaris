{include file="shared.tpl.php"}
<script type="text/javascript" src="{$serverpath}/javascript/master-raphael.js"></script>

<table id="datalistview" class="data striped group-table right {if $module->form->FormHasSearchResult() == 'true'}searchresult{/if}" cellspacing="0" cellpadding="0">
<thead><tr><th></th><th>Feittype</th><th>Commentaar</th><th>(!)</th></tr></thead>
{section name=i loop=$items}
<tr>
<td><input id="rec@{$items[i].PLR__RECORDID}" name="rec@{$items[i].PLR__RECORDID}" type="checkbox" class="checkbox deletebox" /></td>
<td><p class="toggleFTD{if $smarty.get.autoexpand == 'true'} autoexpand{/if}" rel="{$items[i].FACTTYPENAME}">
{$items[i].FACTTYPENAME}</p>
&nbsp;<a class="edit_action" href="{$serverroot}{$callerquery}edit/{$items[i].PLR__RECORDID}/{$callerqueryparams}">[{lang edit}] </a> {if $items[i].SENTENCEPATTERN|strip_tags|trim == ''}(nog aanpassen){/if}

<div class="ftdcontent" style="display:none;"><div class="diagram"></div><div class="sentencepattern"></div></div>
</td>
<td>{$items[i].COMMENT}</td>
<td>{if $items[i].SENTENCEPATTERN|strip_tags|trim == ''}Zinsjabloon aanvullen{/if}</td>
</tr>
{/section}
</table>
