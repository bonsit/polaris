<link rel="stylesheet" type="text/css" media='all' href="{$moduleroot}/css/default.css" />

<div class="ui_group_overview _{$items[i].ORDERINDEX}">
<h1 class="main">Functionele beschrijving MACS</h1>
<p>Gegenereerd op {$smarty.now|date_format:"%e %B %Y om %H:%M uur."}
<br /><br /><br /><br /><br /><p>

{section name=i loop=$items}
{if $items[i].GROUPNAME != $items[$smarty.section.i.index_prev].GROUPNAME}
{if !$smarty.section.i.first }</ul><hr class="fix" /></div>{/if}

<h1>Onderdeel {$items[i].GROUPNAME}</h1>
{/if}

<h2>Scherm: {$items[i].NAME}</h2>

<img src="{$serverroot}/thumb/700/400/docsys/media/{$items[i].IMAGE}?f=png" width="700px" style="margin:20px 0 20px 0;float:none" />

<h3>Functionele beschrijving</h3>
{$items[i].PROCESSDESCRIPTION|default:"Geen procesbeschrijving bekend."|markdown}<br />

{if $items[i].processlist}
<h3>Procesbeschrijving vanuit broncode</h3>
    {section name=j loop=$items[i].processlist}
    <h3>Procedure {$items[i].processlist[j].name}</h3>
    {$items[i].processlist[j].description|markdown}
    {sectionelse}
    Geen beschrijving bekend.
    {/section}
{/if}

{if $items[i].facttypes}
<h3>Hoofdtabellen</h3>
<p>
{section name=j loop=$items[i].facttypes}
    {if $items[i].facttypes[j].facttype}{$items[i].facttypes[j].facttype} {if $items[i].facttypes[j].action}({$items[i].facttypes[j].action}){/if}<br />{/if}        

    {if $items[i].facttypes[j].sentencepattern}
        <div class="zinssjabloon">
        <h3>Zinssjabloon (verwoording van de tabel):</h3>
        <p>{$items[i].facttypes[j].sentencepattern|htmlspecialchars|markdown}</p>
        </div>
    {/if}

{/section}
</p>

<h3>Bedrijfsregels (triggers)</h3>
{if $items[i].facttypes}
    {section name=j loop=$items[i].facttypes}
        {if $items[i].facttypes[j].facttype}<h4><em>Hoofdtabel: {$items[i].facttypes[j].facttype} {if $items[i].facttypes[j].action}({$items[i].facttypes[j].action}){/if}</em></h4>{/if}        
        {section name=k loop=$items[i].facttypes[j].triggers}
            <h5>{$items[i].facttypes[j].triggers[k].TRIGGERTYPE_TRANS|replace:"%s":$items[i].facttypes[j].facttype} {$items[i].facttypes[j].triggers[k].TRIGGEREVENT_TRANS}:</h5>
        
            {$items[i].facttypes[j].triggers[k].DESCRIPTION|replace:"   ":""|markdown}
        {/section}
    {/section}
{else}
    Geen regels bekend.
{/if}
{/if}

{if $elements}
<h3>Schermfuncties</h3>
{section name=k loop=$elements}
{$elements[k].NAME}
{sectionelse}
Geen schermfuncties bekend.
{/section}
{/if}

<hr style="page-break-after:always;clear:both" /><br />
{/section}

</div>
