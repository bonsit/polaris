<link rel="stylesheet" type="text/css" media='all' href="{$moduleroot}/css/dashboard.css?{#BuildVersion#}" />

<input type="hidden" name="_hdnProcessedByModule" value="true" />

<span id="ajaxfeedback"></span>
<div id="errorMessage"></div>

<div class="grid">

<div id="dashboard_mbo" class="widget widget-number mbo">
    <h1 class="title">MBO</h1>

    <h2 class="value">{$aantalinschrijvingen_mbo}</h2>

    <p class="change-rate">
        inschrijvingen
        <div class="subtitle">exclusief basisopleidingen</div>
    </p>

    <p class="updated-at">{$vanaf} t/m {$tm}</p>
</div>

<div id="dashboard_hbo" class="widget widget-number hbo">
    <h1 class="title">HBO</h1>

    <h2 class="value">{$aantalinschrijvingen_hbo}</h2>

    <p class="change-rate">
        inschrijvingen
        <div class="subtitle">exclusief basisopleidingen<br/> en leergangen</div>
    </p>

    <p class="updated-at">{$vanaf} t/m {$tm}</p>
</div>

</div>