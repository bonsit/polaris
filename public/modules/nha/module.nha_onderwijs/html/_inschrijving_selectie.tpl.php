<div id="inschrijving_selectie">
    <div class="pagetabcontainer">
        <ul id="pagetabs">
            <li><b>Zoek inschrijving:</b></li>
            <li><a id="tab_inschrijving" href="#page_inschrijving">Via inschrijving</a></li>
            <li><a id="tab_relatie" href="#page_relatie">Via relatie</a></li>
            {if $examenform}
            <li><a id="tab_examen" href="#page_examen">Via examen</a></li>
            {/if}
        </ul>
    </div>

    <div id="page_inschrijving" class="pagecontents">
        <div class="content">
            <div id="inschrijving_relatie" class="alignlabels">

                <table style="width:100%;vertical-align:top;" class="tblinschrijving">
                    <tr>
                        <td style="width:25%;">
                            <div class="formviewrow"><label class="label">Inschrijfnr: </label>
                                <div class="formcolumn"><input tabindex="1" type="search" id="_fldZOEKINSCHRIJVING" size="10" value="" /></div>
                            </div>

                            <div class="formviewrow"><label class="label">Cursuscode: </label>
                                <div class="formcolumn"><label class="CURSUSCODE"></label></div>
                            </div>
                            <div class="formviewrow"><label class="label">Relatienr: </label>
                                <div class="formcolumn"><label class="CURSISTNR"></label></div>
                            </div>
                            <div class="formviewrow"><label class="label">Datum inschrijving: </label>
                                <div class="formcolumn"><label class="DATUMINSCHRIJVING"></label></div>
                            </div>
                            <div class="formviewrow"><label class="label">Opzeggingsdatum: </label>
                                <div class="formcolumn"><label class="OPZEGGINGSDATUM"></label></div>
                            </div>
                            <div class="formviewrow"><label class="label">Inschr. ondertekend?: </label>
                                <div class="formcolumn"><label class="ONDERTEKENDJANEE"></label></div>
                            </div>
                            <div class="formviewrow"><label class="label">Vooropl. akkoord?: </label>
                                <div class="formcolumn"><label class="VOOROPLEIDINGAKKOORD"></label></div>
                            </div>
                            <div class="formviewrow"><label class="label">OOVK ontvangen: </label>
                                <div class="formcolumn"><label class="BINNENKOMSTOOVK"></label></div>
                            </div>
                        </td>
                        <td style="width:30%;">
                            <div class="formviewrow"><label class="label">Relatienaam: </label>
                                <div class="formcolumn"><label class="VOORLETTERS"></label> <label class="TUSSENVOEGSEL"></label> <label class="ACHTERNAAM"></label></div>
                            </div>
                            <div class="formviewrow"><label class="label">Adres: </label>
                                <div class="formcolumn"><label class="STRAAT"></label> <label class="HUISNR"></label></div>
                            </div>
                            <div class="formviewrow"><label class="label">&nbsp;</label>
                                <div class="formcolumn"><label class="POSTCODE"></label>&nbsp; <label class="PLAATS"></label></div>
                            </div>
                            <div class="formviewrow"><label class="label">BSN:</label>
                                <div class="formcolumn">
                                {if $readonly==false}
                                <input tabindex="4" class="BSN validate_integer" name="BSN" size="20" />
                                {else}
                                <label class="BSN"></label>
                                {/if}
                                </div>
                            </div>
                            <div class="formviewrow"><label class="label">Geboortedatum:</label>
                                <div class="formcolumn">
                                {if $readonly==false}
                                <input tabindex="5" class="GEBDATUM date_input" name="GEBDATUM" size="10" />
                                {else}
                                <label class="GEBDATUM"></label>
                                {/if}
                                </div>
                            </div>
                            <div class="formviewrow"><label class="label">Geboorteplaats:</label>
                                <div class="formcolumn">
                                {if $readonly==false}
                                <input tabindex="10" class="GEBPLAATS" name="GEBPLAATS" size="15" />
                                {else}
                                <label class="GEBPLAATS"></label>
                                {/if}
                                </div>
                            </div>
                            <div class="formviewrow"><label class="label">Opmerking: </label>
                                <div class="formcolumn"><label class="OPMERKING"></label></div>
                            </div>
                        </td>
                        <td>
                            <div class="formviewrow"><label class="label">Aanmaningscode: </label>
                                <div class="formcolumn"><label class="AANMANINGSCODE"></label></div>
                            </div>
                            <div class="formviewrow"><label class="label">Datum geretourneerd:</label>
                                <div class="formcolumn"><label class="DATUMGERETOURNEERD"></label></div>
                            </div>
                            <div class="formviewrow"><em><label class="label">Factuurelatie</label> <label class="FACTUUR_RELATIENR"></label></em>
                                <div class="formcolumn"></div>
                            </div>
                            <div class="formviewrow"><label class="label">Achternaam: </label>
                                <div class="formcolumn"><label class="FACTUUR_VOORLETTERS"></label> <label class="FACTUUR_TUSSENVOEGSEL"></label> <label class="FACTUUR_ACHTERNAAM"></label></div>
                            </div>
                            <div class="formviewrow"><label class="label">Bedrijf: </label>
                                <div class="formcolumn"><label class="FACTUUR_BEDRIJFSNAAM"></label></div>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

    <div id="page_relatie" class="pagecontents">
        <div class="searchbox alignlabels">
            <div class="formviewrow"><label class="label">Relatienr: </label><div class="formcolumn"><input tabindex="3" type="search" id="_fldZOEKRELATIE" size="15" xvalue="1154223" /></div></div>
            <div class="formviewrow"><label class="label">Gebdatum: </label><div class="formcolumn"><input tabindex="6" type="search" id="_fldZOEKGEBOORTEDATUM" placeholder="DD-MM-JJJJ" size="15" value="" /></div></div>
            <div class="formviewrow"><label class="label">Postcode: </label><div class="formcolumn"><input tabindex="9" type="search" id="_fldZOEKPOSTCODE" placeholder="9999XX" size="15" class="validation_uppercase" xvalue="6225HD" /></div></div>
            <div class="formviewrow"><label class="label">Huisnr: </label><div class="formcolumn"><input tabindex="12" type="search" id="_fldZOEKHUISNR" size="15" xvalue="14" /></div></div>
            <div class="formviewrow"><label class="label">Land: </label><div class="formcolumn"><select tabindex="15" type="search" id="_fldZOEKLAND" style="width:150px;">{html_options options=$landen selected="NL"}</select></div></div>
            <div class="formviewrow"><label class="label">&nbsp;</label>
                <div class="buttons small formcolumn">
                    <button id="btnSEARCHRELATIE" type="button" class="searchbutton">Zoek</button>
                </div>
            </div>
        </div>

        <div class="searchresult selectable">
            <div class="content">
                <table id="relaties" class="relaties">
                  <thead>
                    <tr>
                      <th>Relatienr</th>
                      <th>Naam</th>
                      <th>Straat</th>
                      <th>Postcode</th>
                      <th>Plaats</th>
                      <th>Land</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr class="relatie">
                      <td class="nummer"></td>
                      <td class="naam"></td>
                      <td class="straat"></td>
                      <td class="postcode"></td>
                      <td class="plaats"></td>
                      <td class="land"></td>
                    </tr>
                  </tbody>
                </table>
            </div>
        </div>
    </div>

    {if $examenform}
    <div id="page_examen" class="pagecontents">
        <div class="searchbox alignlabels">
            <div class="formviewrow"><label class="label">Examennr: </label><div class="formcolumn"><input tabindex="3" type="search" id="_fldZOEKEXAMENNR" size="15" class="validation_uppercase" /></div></div>
            <div class="formviewrow"><label class="label">Datum: </label><div class="formcolumn"><select tabindex="6" size="8" id="_fldZOEKEXAMENDATUM"></select></div></div>
        </div>

        <div class="searchresult selectable">
            <div class="content">
                <table id="ex_inschrijvingen" class="ex_inschrijvingen">
                  <thead>
                    <tr>
                      <th>Inschrijfnr</th>
                      <th>Cursus</th>
                      <th>Naam</th>
                      <th>Adres</th>
                      <th>Postcode</th>
                      <th>Plaats</th>
                      <th>Land</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr class="aanmelding">
                      <td class="inschrijfnr"></td>
                      <td class="cursus"></td>
                      <td class="naam"></td>
                      <td class="adres"></td>
                      <td class="postcode"></td>
                      <td class="plaats"></td>
                      <td class="land"></td>
                    </tr>
                  </tbody>
                </table>
            </div>
        </div>
    </div>
    {/if}
</div>

<div id="inschrijvingen" class="jqmWindow shadowbox selectable" id="" style="display:none;">
  <h2>Kies de gewenste Inschrijving.</h2>
  <table class="inschrijvingen" style="width:100%;">
  <thead>
    <tr>
      <th>Inschrijvingnr</th>
      <th>Cursuscode</th>
      <th>Datum inschrijving</th>
    </tr>
  </thead>
  <tbody>
    <tr class="inschrijving">
      <td class="inschrijfnr"></td>
      <td class="cursuscode"></td>
      <td class="datuminschrijving"></td>
    </tr>
  </tbody>
  </table>
    <div class="content">
    </div>
</div>
