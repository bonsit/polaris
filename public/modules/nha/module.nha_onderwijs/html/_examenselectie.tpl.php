<table id="examens" class="examens striped scrollarea">
  <thead>
    <tr>
      <th style="width:20px;"></th>
      <th colspan="2">Examen</th>
    </tr>
  </thead>
  <tbody style="font-size:1em;">
    <tr class="examen">
      <td>
          <input type="hidden" class="plr__record" name="rec[]" />
          <input type="hidden" class="aanmeldingnr" name="aanmeldingnr[]" />
          <input type="hidden" class="datums_examennr" name="datums_examennr[]" />
          <input type="checkbox" class="aangemeld" name="aangemeld[]" />
      </td>
      <td class="examencode"></td>
      <td>
        <span class="deelkwalificatie" title="..."><i class="fa fa-info-circle fa-lg fa-fw"></i></span>&nbsp;
        <span class="examenomschrijving"></span>
      </td>
    </tr>
  </tbody>
</table>
