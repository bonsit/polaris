<table id="examencijfers" class="examens striped scrollarea">
  <thead>
    <tr>
      <th style="width:0px;"></th>
      <th colspan="2">Examen</th>
      <th colspan="1">Cijfer</th>
      <th colspan="1">Blanco ingeleverd?</th>
    </tr>
  </thead>
  <tbody style="font-size:1em;">
    <tr class="examen">
      <td>
          <input type="hidden" class="plr__record" name="rec[]" />  
          <input type="hidden" class="examenaanmeldingnr" name="examenaanmeldingnr[]" />  
          <input type="hidden" class="aanmeldingnr" name="aanmeldingnr[]" />  
      </td>
      <td class="examencode"></td>
      <td class="examenomschrijving"></td>
      <td>
        <input type="text" name="examencijfer[]" class="examencijfer" size="10" /></td>
      <td>
        <input type="checkbox" name="blancoingeleverd[]" class="blancoingeleverd" />
      </td>
    </tr>
  </tbody>
</table>
