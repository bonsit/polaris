{$debug_message}
{include file="_shared.tpl.php"}

{literal}
<style>
/* Onderdrukken plr_actions */
#formcontrols {display:none;}
</style>
{/literal}

<div id="formview" class="frmExamenAfdrukken">

    <p>Hier kunt u examenoverzichten afdrukken.</p>
    <p>Kies de examendatum en eventueel de cursuscode of inschrijfnr.</p>
    <p>
        <div class="alignlabels">
            <div class="formviewrow"><label class="label">Examendatum: </label>
                <div class="formcolumn"><select tabindex="2" id="_fldEXAMENDATUM"><option value="">Alle data</option>{html_options options=$examendatums}</select></div>
            </div>
            <div class="formviewrow"><label class="label">Cursuscode: </label>
                <div class="formcolumn"><select tabindex="5" id="_fldCURSUSCODE"><option value="">Alle cursussen</option>{html_options options=$cursuscodes}</select></div>
            </div>
            <div class="formviewrow"><label class="label">Inschrijfnr: </label>
                <div class="formcolumn"><input tabindex="10" type="search" id="_fldINSCHRIJFNR" size="15" /></div>
            </div>
        </div>
    </p>
    <div class="buttons">
        <button id="btnPRINT" type="button" class="secondary"><i class="fa fa-cogs"></i> Genereer rapport</button>
    </div>
</div>
