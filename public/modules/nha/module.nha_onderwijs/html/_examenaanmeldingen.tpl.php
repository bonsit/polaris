<h3 id="lblRELATIE">Examendatum</h3>

<select tabindex="90" id="datumselect" disabled="disabled" name="DATUMS_NR"></select>
&nbsp;&nbsp;<a href="#" id="verwijder_examenaanmelding">Verwijder deze aanmelding</a>

<div id="aanmeldinginfo" class="">
    <input type="hidden" class="PLR__RECORDID" name="_hdnRecordID" />
    <input type="hidden" class="NR" name="NR" />
    <input type="hidden" class="INSCHRIJFNR" name="INSCHRIJFNR" />

    <table style="width:100%;" class="tblAanmelding">
        <tr style="vertical-align:top;" >
            <td class="column1">

                <div class="formviewrow"><label class="label">Datum aanmelding: </label>
                    <div class="formcolumn"><input tabindex="100" type="text" class="DATUM_AANMELDING date_input required" format="dd-mm-yy" name="DATUM_AANMELDING" size="12" /></div>
                </div>
                <div class="formviewrow"><label class="label">Examennr: </label>
                    <div class="formcolumn"><input tabindex="110" type="text" class="EXAMENNR" name="EXAMENNR" size="10" /></div>
                </div>

                <div class="formviewrow"><label class="label" for="_fldAANWEZIGHEID" >Aanwezig op examen? </label>
                    <div class="formcolumn"><input tabindex="120" type="checkbox" id="_fldAANWEZIGHEID" class="AANWEZIGHEID" name="AANWEZIGHEID" value="J" checked="checked" /></div>
                </div>
                <div class="formviewrow"><label class="label" for="_fldEXTRATIJDINMIN">Extra tijd in min.</label>
                    <div class="formcolumn"><input tabindex="125" type="text" id="_fldEXTRATIJDINMIN" class="EXTRATIJDINMIN" name="EXTRATIJDINMIN" /></div>
                </div>
            </td>
            <td class="column2">
                <div class="formviewrow"><label class="label">Betaalwijze: </label>
                    <div class="formcolumn"><select tabindex="150" name="BETAALWIJZE" id="_fldBETAALWIJZE" class="BETAALWIJZE"><option value="AUT">Automatische incasso</option><option value="ACC">Acceptgiro</option></select></div>
                </div>
                <div class="formviewrow"><label class="label">Rekeningnr: </label>
                    <div class="formcolumn"><input tabindex="160" type="text" class="REKENINGNROFGIRONRRELATIE required" id="_fldREKENINGNROFGIRONRRELATIE" name="REKENINGNROFGIRONRRELATIE" size="34" /><br />
                    <select tabindex="-1" id="bankrekeningnr_select"></select>
                    </div>
                </div>
                <div class="formviewrow"><label class="label">Examengeld: &euro; </label>
                    <div class="formcolumn"><input tabindex="170" type="text" class="BEDRAGEXAMENGELD required" id="_fldBEDRAGEXAMENGELD" name="BEDRAGEXAMENGELD" size="12" /></div>
                </div>
                <div class="formviewrow"><label class="label" for="_fldFACTUURRELATIENRBETAALDJANEE">Betaalt factuurrelatie? </label>
                    <div class="formcolumn"><input tabindex="180" type="checkbox" id="_fldFACTUURRELATIENRBETAALDJANEE" class="FACTUURRELATIENRBETAALDJANEE" name="FACTUURRELATIENRBETAALDJANEE" value="J" /></div>
                </div>
                <div class="formviewrow"><label class="label" for="_fldUITZONDERINGJANEE">Uitzondering? </label>
                    <div class="formcolumn">
                        <input tabindex="190" type="checkbox" id="_fldUITZONDERINGJANEE" class="UITZONDERINGJANEE" name="UITZONDERINGJANEE" value="J" />
                        <textarea tabindex="200" style="vertical-align:top;" class="UITZONDERINGOPMERKING" id="_fldUITZONDERINGOPMERKING" name="UITZONDERINGOPMERKING" cols="40" rows="2" maxlength="255" placeholder="Opmerking"></textarea>
                    </div>
                </div>
                {*
                <div class="formviewrow"><label class="label">Aan/afmeldingbrief afgedrukt op: </label>
                    <div class="formcolumn"><label class="PRINTDATUM"></label></div>
                </div>
                *}
            </td>
            <td class="column3">
                <div class="formviewrow"><em><label class="label">Annulering</label></em>
                    <div class="formcolumn"></div>
                </div>
                <div class="formviewrow"><label class="label">Geannuleerd op: </label>
                    <div class="formcolumn"><input tabindex="300" type="text" class="DATUM_ANNULERING date_input" name="DATUM_ANNULERING" size="10" /></div>
                </div>
                <div class="formviewrow"><label class="label">Reden: </label>
                    <div class="formcolumn"><input tabindex="310" type="text" class="REDEN_ANNULERING" name="REDEN_ANNULERING" size="20" /></div>
                </div>
                <div class="formviewrow"><label class="label" for="_fldGEGRONDE_ANNULERING">Gegrond? </label>
                    <div class="formcolumn"><input tabindex="320" type="checkbox" id="_fldGEGRONDE_ANNULERING" class="GEGRONDE_ANNULERING" name="GEGRONDE_ANNULERING" value="J" /></div>
                </div>
            </td>
         </tr>
    </table>

    <hr class="fix" />

</div>
