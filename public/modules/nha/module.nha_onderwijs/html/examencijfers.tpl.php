{$debug_message}
{include file="_shared.tpl.php"}

<div id="formview" class="frmExamencijfers">

{include file="_inschrijving_selectie.tpl.php" readonly=true examenform=true}
{*include file="_examenaanmeldingen.tpl.php" type=""*}

<h3 id="lblRELATIE">Examendatum</h3>
<select tabindex="90" id="datumselect" disabled="disabled" name="DATUMS_NR"></select>

<div id="examenselectiearea" class="scrollarea">
{include file="_examencijfers.tpl.php"}
</div>

<div class="buttons">
<button id="btnSAVE" type="button" class="savebutton positive disabled" accesskey="F9">Opslaan</button>
<button id="btnCANCEL" type="button" class="cancelbutton negative" accesskey="F2">Annuleren</button>
</div>

</div>
