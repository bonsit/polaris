{$debug_message}
{include file="_shared.tpl.php"}

<div id="formview" class="frmAanmeldingen">

{include file="_inschrijving_selectie.tpl.php" readonly=false}
{include file="_examenaanmeldingen.tpl.php" type=""}

<div id="examenselectiearea" class="scrollarea">
{include file="_examenselectie.tpl.php"}
</div>

<div class="buttons">
<button id="btnNEW" type="button" class="newbutton secondary" accesskey="F8">Nieuw</button>
<button id="btnSAVE" type="button" class="savebutton positive disabled" accesskey="F9">Opslaan</button>
<button id="btnCANCEL" type="button" class="cancelbutton negative" accesskey="F2">Annuleren</button>
</div>

</div>
