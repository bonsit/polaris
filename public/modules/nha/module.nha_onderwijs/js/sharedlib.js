var NHA = {};

NHA.Messages = {
    geenRelatieGeselecteerd: 'U heeft nog geen relatie geselecteerd.',
    foutZoekActie: 'De zoekopdracht kon niet uitgevoerd worden. Error: ',
    geenExamensGevonden: 'Er zijn geen examens gevonden voor deze cursus/inschrijving.'
}

NHA.SharedLib = {
    _macs_servicequery: _servicequery.substr(0,_servicequery.substr(0,_servicequery.length-1).lastIndexOf('/'))+"/%construct%/",

    modalMessage: function(str, customtimeout) {
        if (typeof customtimeout == 'undefined') customtimeout = 1500;
        $.blockUI({message: str, timeout: customtimeout,
             showOverlay: true,
             css: {
                 border: 'none',
                 padding: '1em',
                 backgroundColor: '#000',
                 '-webkit-border-radius': '10px',
                 '-moz-border-radius': '10px',
                 opacity: .8,
                 color: '#ededed',
                 fontSize: '1.2em',
                 letterSpacing: '1px'
             }
         });
    },
    closeModalMessage: function() {
       $.unblockUI();
    },
    easyFillForm: function (item, fields, form) {
        form = form ||document;
        $(fields).each(function(i, elem) {
            $('label.'+elem, $(form)).text(item[elem]||'');
            $(':input.'+elem, $(form)).val(item[elem]||'').change();
            $(':checkbox.'+elem, $(form)).attr('checked',false);
            $('span#_lbl'+elem).html(($('span#_lbl'+elem).attr('title')||'')+(item[elem]||'&nbsp;'));
        });
    },
    ajaxFeedback: function(message) {
        Polaris.Visual.yellowFade($('#ajaxfeedback').html("<span>"+message+"</span>")[0]
        , function() {
            $('#ajaxfeedback span').fadeOut("slow");
        });
    },
    zoekAdres: function(landcode, postcode, huisnummer, callback) {
        //  Alleen nedelandse adressen worden opgezocht
        if (landcode == 'NL') {

            $("#btnZOEKADRES").hide();
            $("#btnZOEKADRES").after($("<img id=\"imgwait\" />").attr('src', _serverroot+'/images/wait.gif'));
            var zoekAdresKlaar = function() {
                $("#imgwait").remove();
                $("#btnZOEKADRES").show();
            }

            var url = NHA.SharedLib._macs_servicequery.replace("%construct%","postcodetabelnederland")
            +"?output=json&limit=10"
            +"&qc[]=POSTCODE&qv[]="+$('#_fldPOSTCODE').val().replace(' ','')
            +"&qc[]=HUISNR&qv[]="+$('#_fldHUISNR').val();
            $.getJSON(url, function(data, textStatus) {
                if (textStatus == 'success') {
                    huisnummer = parseInt(huisnummer);
                    $(data).each(function(i, elem) {
                        var nrvan = parseInt(elem.HUISNRVAN.replace(/0+/g,''));
                        var nrtm = parseInt(elem.HUISNRTM.replace(/0+/g,''));
                        if ((huisnummer >= nrvan) && (huisnummer <= nrtm) &&
                           ((huisnummer % 2) == 0) && ((nrvan % 2) == 0)) {
                            callback(elem);
                            return true;
                        }

                    });
                } else {
                    alert('De zoekopdracht kon niet uitgevoerd worden. Error: '+textStatus);
                }
                zoekAdresKlaar();
            });
        } else {
            alert("Alleen Nederlandse adressen kunnen worden opgezocht.");
        }
    },
    bepaalAanhef: function(geslacht) {
        return ['Dhr','Mevr','Dhr/Mevr'][['M','V','O'].indexOf(geslacht)]
    },
    clearExamples: function(form) {
        $(form).find('.' + $.fn.example.defaults.className).val('');
    }
}

$(document).ready(function(){
    if ($.example)
        $('input[title]').example(function() { return $(this).attr('title'); });
//    $("#_fldGEBOORTEDATUM").mask("99/99/9999");

});