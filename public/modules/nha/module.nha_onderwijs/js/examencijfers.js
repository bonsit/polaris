Onderwijs.ExamenCijfers = {
    bewaarCijfers: function() {
//$('#dataform')[0].submit();
//return;
		/***
		 * Controleer de standaard velden
		 */
		formValid = true;

		var validator = $("#dataform").validate({
			onsubmit: false,
			debug: false,
			validateHiddenFields: false,
			rules: {
			    'examencijfer[]': {
			        examencijfer: [0,10]
			    }
			}
		});
		if (!$("#dataform").valid()) {
			formValid = false;
			Polaris.Base.modalMessage('U heeft ' + validator.numberOfInvalids() + ' velden niet correct ingevuld.', 1000, false);
		}

        if (formValid) {
            $('#btnSAVE').addClass('disabled');
            jQuery.post(_servicequery, $('#dataform').serializeArray(), function(data, textStatus){
                if (textStatus == 'success') {
                    NHA.SharedLib.ajaxFeedback('Wijzigingen zijn opgeslagen.');
                } else {
                    NHA.SharedLib.ajaxFeedback('Nieuwe cijfers niet aangepast. Fout: '+data);
                }
                /* vul de datum Select list opnieuw en selecteer de huidige datum */
                Onderwijs.InschrijvingSelectieForm.vulDatums(Onderwijs.InschrijvingSelectieForm.inschrijvingData.INSCHRIJFNR, $("#datumselect").val());
                $("#_fldZOEKINSCHRIJVING").focus().select();
                $('#btnSAVE').removeClass('disabled');
            });
        }
    },
    annuleerForm: function() {
        /* Maak de formulieren/velden leeg */
        NHA.SharedLib.easyFillForm([], Onderwijs.InschrijvingSelectieForm.inschrijvingVelden);
//        NHA.SharedLib.easyFillForm([], Onderwijs.ExamenAanmeldingForm.examenVelden);
        Onderwijs.ExamenCijfers.annuleerExamens();
        Onderwijs.InschrijvingSelectieForm.focusEerstePageVeld();
    },
    annuleerExamens: function(cleardatums) {
        /* Maak de formulieren/velden leeg */
        if (typeof cleardatums == 'undefined') cleardatums = true;
        if (cleardatums)
            Onderwijs.InschrijvingSelectieForm.vulDatums(0);
        Onderwijs.ExamenAanmeldingForm.vulExamenCijfers([]);
    },
    cijferValid: function(cijfer) {
        var result = false;
        if (cijfer == '') return true;
        var cijferFloat = Polaris.Base.floatValue(cijfer);
        if (cijfer == 'o' || cijfer == 'onvoldoende') {
            result = 'onvoldoende';
        } else if (cijfer == 'g' || cijfer == 'goed') {
            result = 'goed';
        } else if (cijfer == 'v' || cijfer == 'voldoende') {
            result = 'voldoende';
        } else if (Polaris.Base.isInteger(cijferFloat) || Polaris.Base.isFloat(cijferFloat)) {
            if (cijferFloat >= 0.0 && cijferFloat <= 10.0) {
                result = Polaris.Base.formatFloat(cijfer, 1);
            }
        }
        return result;
    },
    checkCijfers: function() {
        var Self = Onderwijs.ExamenCijfers;
        $(".examencijfer")
        .change(function() {
            var cijfer = $(this).val();
            var result = Self.cijferValid(cijfer);
            if (result) {
                $(this).val(result);
                $('.blancoingeleverd', $(this).parents('tr')).attr('checked',false);
            }
        })
        .focus(function() {
            if ($(this).val().length >= 4) {
                $(this).select();
            }
        });
    }
}

$(document).ready(function(){
    Onderwijs.InschrijvingSelectieForm.initialiseer();
    Onderwijs.ExamenAanmeldingForm.initialiseer();

    /* Form actions */
    $('#dataform').submit(function(e) {
        e.preventDefault();
    });
    $('#btnSAVE').click(function(e) {
        e.preventDefault();
        if (!$(this).hasClass('disabled'))
            Onderwijs.ExamenCijfers.bewaarCijfers();
    });
    $('#btnCANCEL').click(function(e) {
        e.preventDefault();
        if (!$(this).hasClass('disabled'))
            Onderwijs.ExamenCijfers.annuleerForm();
    });

    $("#datumselect").change(function() {
        var _record = $("option:selected",this).data('record');
        var _datum = $(this).val();
        Onderwijs.ExamenAanmeldingForm.zoekExamenCijfers(Onderwijs.InschrijvingSelectieForm.inschrijvingData.CURSUSCODE, _datum, function() {
            Onderwijs.ExamenCijfers.checkCijfers();
        });
    });

    Onderwijs.ExamenCijfers.checkCijfers();

    // luister naar events vanuit de formulier secties (examenaanmelding_form.js)
    $(".frmExamencijfers").bind('annuleerexamencijfer', function() { Onderwijs.ExamenCijfers.annuleerForm() });
});

jQuery.validator.addMethod("examencijfer", function(value, element, params) {
    return Onderwijs.ExamenCijfers.cijferValid(value) != false;
}, jQuery.format("U kunt een cijfer invullen of de waarden o(nvoldoende), v(oldoende), g(oed)."));
