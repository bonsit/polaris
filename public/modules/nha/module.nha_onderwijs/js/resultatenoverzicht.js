Onderwijs.ResultatenOverzicht = {

    zoekCursusCodes: function(datum, callback) {
        var Self = Onderwijs.ResultatenOverzicht;
        var inschrijfnr = $("#_fldZOEKINSCHRIJVING").val();

        $("#_fldCURSUSCODE")
        .empty()
        .html('<option value="">- even geduld aub -</option>');

        $.getJSON(_servicequery, {'func':'datumcursuscodes', 'datum': datum}, function(data, textStatus) {
            /* Leg data vast als lokale/globale var */
            if (textStatus == 'success') {
                $("#_fldCURSUSCODE")
                .empty()
                .html('<option value="">Alle cursussen</option>')
                .addOptions(data, 'CURSUSCODE', 'CURSUSNAAM');
                if (typeof callback != 'undefined') {
                    callback();
                }
            } else {
                NHA.SharedLib.modalMessage(NHA.Messages.foutZoekActie + textStatus);
            }
        });
    }

}

$(document).ready(function(){
    $("#dataform").submit(function(e) {
        e.preventDefault();
        $("#btnPRINT").click();
    })

    $("#btnPRINT").click(function() {
        var datum = $("#_fldEXAMENDATUM").val();
        var cursuscode = $("#_fldCURSUSCODE").val();
        var inschrijfnr = $("#_fldINSCHRIJFNR").val();

        var src = _serverroot+'/app/onderwijs/module/plr_execjasperreport/?action=e30f9628132d0358e7961e5e6dac4927&&jr=/reports/Macs/ResultatenOverzicht';
        var params = params2 = '';

        params2 = ' AND 1=1';
        if (cursuscode != '') {
            params2 += ' AND I.CURSUSCODE=\''+cursuscode+'\'';
        }
        if (inschrijfnr != '') {
            params2 += ' AND I.INSCHRIJFNR='+inschrijfnr;
        }
        
        log(src+'&EXAMENDATUM='+datum+'&WHEREFILTERMAIN='+Polaris.Base.encodeUrlValue(params2));
        Polaris.Base.rawPopup(src+'&EXAMENDATUM='+datum+'&WHEREFILTERMAIN='+Polaris.Base.encodeUrlValue(params2));
    });
    
    $("#_fldEXAMENDATUM").change(function() {
        Onderwijs.ResultatenOverzicht.zoekCursusCodes($("#_fldEXAMENDATUM").val());
    });
    $("#_fldEXAMENDATUM option:eq(1)").attr('selected', 'selected');
    
    Onderwijs.ResultatenOverzicht.zoekCursusCodes($("#_fldEXAMENDATUM").val());
});
