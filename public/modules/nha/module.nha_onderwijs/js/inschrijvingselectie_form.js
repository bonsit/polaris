Onderwijs.InschrijvingSelectieForm = {
    inschrijvingenData: [],

    inschrijvingData: [],

    htmlInschrijvingen: '',
    templateInschrijvingen: '',
    inschrijvingVelden: ['CURSUSCODE','DATUMINSCHRIJVING','VOORLETTERS','TUSSENVOEGSEL','ACHTERNAAM','STRAAT','HUISNR','POSTCODE','PLAATS'
    ,'OPZEGGINGSDATUM','DATUMGERETOURNEERD','MENING','AANMANINGSCODE','OPMERKING','CURSISTNR','GEBDATUM','GEBPLAATS', 'BSN'
    ,'VOOROPLEIDINGAKKOORD', 'BINNENKOMSTOOVK'],

    relatieData: [],
    htmlRelaties: '',
    templateRelaties: '',

    inschrijvingenData: [],
    htmlInschrijvingen: '',
    templateInschrijvingen: '',

    datumsData: [],
    examenDatumsData: [],

    examenInschrijvingenData: [],
    htmlExamenInschrijvingen: '',
    templateExamenInschrijvingen: '',

    initialiseer: function() {
        var Self = Onderwijs.InschrijvingSelectieForm;

        Self.htmlRelaties = $( '#relaties tbody' );
        //directive to render the template
        var directivesRelaties = {
            'tr' : {
                'relatie<-': {
                    'td.nummer': 'relatie.RELATIENR',
                    'td.naam': function(arg) { return NHA.SharedLib.bepaalAanhef(arg.item.GESLACHT)+' '+(arg.item.VOORLETTERS||'')+' '+arg.item.ACHTERNAAM },
                    'td.straat': function(arg) { return arg.item.STRAAT+' '+arg.item.HUISNR },
                    'td.postcode': 'relatie.POSTCODE',
                    'td.plaats': 'relatie.PLAATS',
                    'td.land': 'relatie.LANDCODE',
                    '@onclick': function(arg){
                        return 'Onderwijs.InschrijvingSelectieForm.selecteerRelatie(this,'+arg.pos+');'
                    },
                    '@onmouseover': '"$(this).toggleClass(\'active\', true);"',
                    '@onmouseout' : '"$(this).toggleClass(\'active\', false);"',
                    '@style':"'cursor:pointer'",
                    '@class': function(arg){
                        //arg => {data:data, items:items, pos:pos, item:items[pos]};
                        var oddEven =  (arg.pos % 2 == 0) ? 'even ' : 'odd ';
                        var firstLast = (arg.pos == 0) ? 'first ': (arg.pos == arg.items.length -1) ? 'last ':'';
                        var opvallend = arg.item.HANDELINGSONBEKWAAMJANEE == 'J'? 'alert' : '';
                        return ' ' + oddEven + firstLast + opvallend;
                    }
                }
            }
        };
        Self.templateRelaties = Self.htmlRelaties.compile( directivesRelaties );

        Self.htmlInschrijvingen = $( '#inschrijvingen tbody' );
        //directive to render the template
        var directivesInschrijvingen = {
            'tr' : {
                'inschrijving<-': {
                    'td.inschrijfnr': 'inschrijving.INSCHRIJFNR',
                    'td.cursuscode': 'inschrijving.CURSUSCODE',
                    'td.datuminschrijving': 'inschrijving.DATUMINSCHRIJVING',
                    '@onclick': function(arg){
                        return 'Onderwijs.InschrijvingSelectieForm.selecteerInschrijving(this,'+arg.pos+');'
                    },
                    '@onmouseover': '"$(this).toggleClass(\'active\', true);"',
                    '@onmouseout' : '"$(this).toggleClass(\'active\', false);"',
                    '@style':"'cursor:pointer'",
                    '@class': function(arg){
                        //arg => {data:data, items:items, pos:pos, item:items[pos]};
                        var oddEven =  (arg.pos % 2 == 0) ? 'even ' : 'odd ';
                        var firstLast = (arg.pos == 0) ? 'first ': (arg.pos == arg.items.length -1) ? 'last ':'';
                        return ' ' + oddEven + firstLast;
                    }
                }
            }
        };
        Self.templateInschrijvingen = Self.htmlInschrijvingen.compile( directivesInschrijvingen );

        Self.htmlExamenInschrijvingen = $( '#ex_inschrijvingen tbody' );
        //directive to render the template
        var directivesExamenInschrijvingen = {
            'tr' : {
                'aanmelding<-': {
                    'td.inschrijfnr': 'aanmelding.INSCHRIJFNR',
                    'td.cursus': 'aanmelding.CURSUSCODE',
                    'td.naam': 'aanmelding.NAAM',
                    'td.adres': 'aanmelding.ADRES',
                    'td.postcode': 'aanmelding.POSTCODE',
                    'td.plaats': 'aanmelding.PLAATS',
                    'td.land': 'aanmelding.LANDCODE',
                    '@onclick': function(arg){
                        return 'Onderwijs.InschrijvingSelectieForm.selecteerInschrijving(this,'+arg.pos+');'
                    },
                    '@onmouseover': '"$(this).toggleClass(\'active\', true);"',
                    '@onmouseout' : '"$(this).toggleClass(\'active\', false);"',
                    '@style':"'cursor:pointer'",
                    '@class': function(arg){
                        //arg => {data:data, items:items, pos:pos, item:items[pos]};
                        var oddEven =  (arg.pos % 2 == 0) ? 'even ' : 'odd ';
                        var firstLast = (arg.pos == 0) ? 'first ': (arg.pos == arg.items.length -1) ? 'last ':'';
                        return ' ' + oddEven + firstLast;
                    }
                }
            }
        };
        if (Self.htmlExamenInschrijvingen.length > 0)
            Self.templateExamenInschrijvingen = Self.htmlExamenInschrijvingen.compile( directivesExamenInschrijvingen );
    },
    initialiseerEvents: function() {
        var Self = Onderwijs.InschrijvingSelectieForm;

        /* Zoek Inschrijving events */
        $('#btnSEARCHINSCHRIJVING').click(function(e) {
            e.preventDefault();
            Self.zoekInschrijving();
        });
        $("#_fldZOEKINSCHRIJVING").keyup(function(event) {
            if (event.keyCode == 13 /*Enter*/) {
                event.preventDefault();
                Self.zoekInschrijving();
            }
        });

        $("#pagetabs").bind('switched', function(event, pageid, alltabs, container, settings) {
            Self.focusEerstePageVeld();
        });

        /* Zoek relatie events */
        var searchboxRelatieAction = function(e) {
            e.preventDefault();
            Self.zoekRelatie();
        }
        $("#page_relatie .searchbox input").keydown(function(event) {
            if (event.keyCode == 13 /*Enter*/) {
                searchboxRelatieAction(event);
            }
        });
        $('#btnSEARCHRELATIE').click(searchboxRelatieAction);
    },
    selecteerRelatie: function(elem,pos) {
        var Self = Onderwijs.InschrijvingSelectieForm;
        Self.zoekInschrijvingen(Self.relatieData[pos].RELATIENR);
    },
    _selecteerInschrijving: function(tableid, pos) {
        var Self = Onderwijs.InschrijvingSelectieForm;

        /* Verwijder dialoog venster/ Inschrijvingen */
        $.unblockUI();

        if (tableid == 'ex_inschrijvingen') {
            /* Leg de gekozen inschrijving vast als lokale/globale var */
            Self.inschrijvingData = Self.examenInschrijvingenData[pos];
        } else {
            /* Toont het Inschrijving tabblad */
            $("#tab_inschrijving").click();
            /* Leg de gekozen inschrijving vast als lokale/globale var */
            Self.inschrijvingData = Self.inschrijvingenData[pos];
        }
        /* Vul de gevonden inschrijving */
        Self.vulInschrijving();

    },
    selecteerInschrijving: function(elem, pos) {
        var Self = Onderwijs.InschrijvingSelectieForm;

        var tableid = $(elem).parents('table').attr('id');

        $('tr', $(elem).parents('table')).removeClass('selected');
        $(elem).addClass('selected');

        Self._selecteerInschrijving(tableid, pos);
    },
    focusEerstePageVeld: function() {
        $($("#pagetabs a.selected").attr('href')+" :input:visible:first").focus();
    },
    vulInschrijving: function() {
        var Self = Onderwijs.InschrijvingSelectieForm;

        var _data = Self.inschrijvingData;

        /* Als er een factuurrelatie is, dan bankrekeningnr ophalen van die relatie */
        if (_data.FACTUURRELATIE != null && _data.FACTUURRELATIE != '') {
            Self.bepaalFactuurBankRekening(_data.FACTUURRELATIE, function(bankrekening){
                _data.FACTUURRELATIEBANKREKENINGNR = bankrekening;
            });
        }
        $('#inschrijving_relatie').autoRender(_data);

        /* reset het inschrijfnr, omdat deze overschreven wordt door Pure (!) */
        $("#_fldZOEKINSCHRIJVING").val(_data.INSCHRIJFNR);

        /* leg het inschrijfnr vast voor het scherm Examencijfers */
        sessionStorage.nha_onderwijs_inschrijfnr = _data.INSCHRIJFNR;

        /* Reconnect the events, because Pure deletes the events(!) */
        Self.initialiseerEvents();

        /* Geselecteerde datum bij 'Via examen' */
        var datumsnr = $("#_fldZOEKEXAMENDATUM:visible").val();

        Self.vulDatums(_data.INSCHRIJFNR, datumsnr);
        $("#_fldZOEKINSCHRIJVING").focus();
    },
    vulExamenInschrijvingen: function(data) {
        var Self = Onderwijs.InschrijvingSelectieForm;
        /* Zet de data in de ExamenInschrijvingen tabel */
        Self.htmlExamenInschrijvingen = Self.htmlExamenInschrijvingen.render( data, Self.templateExamenInschrijvingen );
    },
    zoekInschrijving: function() {
        var Self = Onderwijs.InschrijvingSelectieForm;
        var _inschrijvingnr = $('#_fldZOEKINSCHRIJVING').val();

        /* leg het inschrijfnr vast voor het scherm Examencijfers */
        sessionStorage.nha_onderwijs_inschrijfnr = _inschrijvingnr;

        if (_inschrijvingnr) {
            var url = NHA.SharedLib._macs_servicequery.replace("%construct%","inschrijvingrelaties").replace("const","form")
              +"?limit=20"
              +"&qc[]=INSCHRIJFNR&qv[]='" + _inschrijvingnr+"'";
            $.getJSON(url, function(data, textStatus) {
                if (textStatus == 'success' && data.length > 0) {
                    /* Leg data vast als lokale/globale var */
                    Self.inschrijvingData = data[0];
                    /* Vul de gevonden inschrijving */
                    Self.vulInschrijving();
                } else {
                    var _error = data.length == 0?'geen inschrijvingen gevonden':'Ajax call niet uitvoerbaar';
                    NHA.SharedLib.modalMessage(NHA.Messages.foutZoekActie + _error);
                }
            });
        }
    },
    zoekInschrijvingen: function(cursistnr) {
        var Self = Onderwijs.InschrijvingSelectieForm;
        var url = NHA.SharedLib._macs_servicequery.replace("%construct%","inschrijvingrelaties").replace("const","form")
          +"?limit=20"
          +"&qc[]=CURSISTNR&qv[]='" + cursistnr + "'";
        $.getJSON(url, function(data, textStatus) {
            /* Leg data vast als lokale/globale var */
            if (textStatus == 'success' && data.length > 0) {
                if (data.length == 1) {
                    Self.inschrijvingData = data[0];
                    /* Vul de gevonden inschrijving */
                    Self.vulInschrijving();
                    /* Toont het Inschrijving tabblad */
                    $("#tab_inschrijving").click();
                } else {
                    /* Leg data vast als lokale/globale var */
                    Self.inschrijvingenData = data;
                    /* Laat gebruiker kiezen uit de gevonden inschrijvingen */

                    /* Zet de data in de Inschrijvingen tabel */
                    Self.htmlInschrijvingen = Self.htmlInschrijvingen.render( Self.inschrijvingenData, Self.templateInschrijvingen );

                    /* Toon Inschrijvingen tabel en laat de gebruiker kiezen */
                    $.blockUI({ message: $("#inschrijvingen")});
                    $('.blockOverlay').attr('title','Klik om venster te sluiten').click($.unblockUI);
                }
            } else {
                var _error = data.length == 0?'geen inschrijvingen gevonden':'Ajax call niet uitvoerbaar';
                NHA.SharedLib.modalMessage(NHA.Messages.foutZoekActie + _error);
            }
        });
    },
    zoekExamenInschrijvingen: function(examennr, datumsnr) {
        var Self = Onderwijs.InschrijvingSelectieForm;

        $.getJSON(_servicequery, {'func':'exameninschrijvingen', 'examennr': examennr, 'datumsnr': datumsnr}, function(data, textStatus) {
            /* Leg data vast als lokale/globale var */
            if (textStatus == 'success') {
                Self.examenInschrijvingenData = data;
                if (Self.examenInschrijvingenData.length == 0) {
                    Self.vulExamenInschrijvingen([]);
                    NHA.SharedLib.modalMessage(NHA.Messages.geenExamensGevonden);
                } else {
                    Self.vulExamenInschrijvingen(Self.examenInschrijvingenData);
                    if (Self.examenInschrijvingenData.length == 1) {
                        Self._selecteerInschrijving('ex_inschrijvingen', 0);
                    }
                }
            } else {
                NHA.SharedLib.modalMessage(NHA.Messages.foutZoekActie + textStatus);
            }
        });

    },
    bepaalFactuurBankRekening: function(factuurrelatienr, callback) {
        var Self = Onderwijs.InschrijvingSelectieForm;

        var url = NHA.SharedLib._macs_servicequery.replace("%construct%","laatsterelatiebankrekening").replace("const","form")
          +"?limit=20"
          +"&qo[]=EQ&qc[]=RELATIENR&qv[]='" + factuurrelatienr + "'";
        $.getJSON(url, function(data, textStatus) {
            /* Leg data vast als lokale/globale var */
            if (textStatus == 'success' && data.length > 0) {
                callback(data[0].BANKGIROREKENINGNR);
            } else {
                return false;
            }
        });
    },
    vulRelaties: function(data) {
        var Self = Onderwijs.InschrijvingSelectieForm;

        // rendering but reusing the compiled function template
        Self.htmlRelaties = Self.htmlRelaties.render( data, Self.templateRelaties );
    },
    zoekRelatieOpNummer: function(relatienr, callback) {
        var Self = Onderwijs.InschrijvingSelectieForm;

        $('#_fldZOEKPOSTCODE,#_fldZOEKHUISNR,#_fldZOEKLAND,#_fldZOEKGEBOORTEDATUM').val('');

        var _url = NHA.SharedLib._macs_servicequery.replace("%construct%","relatie").replace("const","form")
          +"?limit=20"
          +"&qo[]=EQ&qc[]=RELATIENR&qv[]=" + relatienr;
        $.getJSON(_url, function(data, textStatus) {
            /* Leg data vast als lokale/globale var */
            if (textStatus == 'success') {
                callback(data);
            } else {
                callback(false);
            }
        });
    },
    zoekRelatieOpAdres: function(callback) {
        var Self = Onderwijs.InschrijvingSelectieForm;

        $('#_fldZOEKPOSTCODE').val($('#_fldZOEKPOSTCODE').val().toUpperCase());

        var _postcode = $('#_fldZOEKPOSTCODE').val().replace(' ','');
        if (_postcode.length > 0 && _postcode.length != 6) _postcode += "%";
        if (_postcode != '' && _postcode.charAt(_postcode.length-1) != "%") _postcode = "'"+_postcode+"'";
        var _huisnr = $('#_fldZOEKHUISNR').val();
        if (_huisnr != '') _huisnr = "'"+_huisnr+"'";
        var _land = $('#_fldZOEKLAND').val();
        if (_land != '') _land = "'"+_land+"'";
        var _gebdatum = $('#_fldZOEKGEBOORTEDATUM').val();
        if (_gebdatum != '') _gebdatum = "'"+_gebdatum+"'";

        $('.searchresult .content').addClass('loading');
        var _url = NHA.SharedLib._macs_servicequery.replace("%construct%","relatie").replace("const","form")
          +"?limit=20"
          +"&qc[]=GEBDATUM&qv[]=" + _gebdatum
          +"&qc[]=POSTCODE&qv[]=" + _postcode
          +"&qc[]=HUISNR&qv[]=" + _huisnr
          +"&qc[]=LAND&qv[]="+_land;
        $.getJSON(_url, function(data, textStatus) {
            $('.searchresult .content').removeClass('loading');
            /* Leg data vast als lokale/globale var */
            if (textStatus == 'success') {
                callback(data);
            } else {
                callback(false);
            }
        });
    },
    zoekExamenDatums: function(examennr, callback) {
        var Self = Onderwijs.InschrijvingSelectieForm;

        $.getJSON(_servicequery, {'func':'examendatums', 'examennr': examennr}, function(data, textStatus) {
            /* Leg data vast als lokale/globale var */
            if (textStatus == 'success') {
                Self.examenDatumsData = data;
                if (Self.examenDatumsData.length == 0) {
                    NHA.SharedLib.modalMessage(NHA.Messages.geenExamensGevonden);
                } else {
                    Self.vulExamenDatumsSelect(Self.examenDatumsData);
                    /*
                    if ($("#_fldZOEKEXAMENDATUM option").length > 0 ) {
                        $("#_fldZOEKEXAMENDATUM").change();
                    }
                    */
//                    if (typeof callback != 'undefined')
//                        callback();
                }
            } else {
                NHA.SharedLib.modalMessage(NHA.Messages.foutZoekActie + textStatus);
            }
        });
    },
    zoekRelatie: function() {
        var Self = Onderwijs.InschrijvingSelectieForm;

        var _relatienr = $('#_fldZOEKRELATIE').val();

        var callback = function(data) {
            Self.relatieData = data;
            if (Self.relatieData)
                Self.vulRelaties(data);
            else
                NHA.SharedLib.modalMessage(NHA.Messages.foutZoekActie);
        }

        if (_relatienr)
            Self.relatieData = Self.zoekRelatieOpNummer(_relatienr, callback);
        else
            Self.relatieData = Self.zoekRelatieOpAdres(callback);
    },
    zoekExamens: function(e) {
        var Self = Onderwijs.InschrijvingSelectieForm;

        var _examennr = $('#_fldZOEKEXAMENNR').val();
        var _datumsnr = $('#_fldZOEKEXAMENDATUM').val();

        if (_examennr && !_datumsnr) {
            Self.zoekExamenDatums(_examennr);
        } else if (_examennr && _datumsnr) {
            Self.zoekExamenInschrijvingen(_examennr, _datumsnr);
        }
        Self.vulExamenInschrijvingen([]);
        $(".frmExamencijfers").trigger('annuleerexamencijfer');
    },
    vulExamenDatumsSelect: function(data) {
        $("#_fldZOEKEXAMENDATUM").empty().addOptions(data, 'DATUMS_NR', 'DATUM');
    },
    vulDatums: function(inschrijfnummer, huidigedatum) {
        var Self = Onderwijs.InschrijvingSelectieForm;

        if (inschrijfnummer == 0) {
            $("#datumselect option").remove();
        } else {
            var datumbron = ($(".frmExamencijfers").length > 0)?'aanmeldingsdataexamen':'aanmeldingsdata';
            var _url = NHA.SharedLib._macs_servicequery.replace("%construct%",datumbron).replace("const","form")
              +"?limit=100"
              +"&qo[]=OJ"
              +"&qc[]=INSCHRIJFNR&qv[]='" + inschrijfnummer + "'";
            $.getJSON(_url, function(data, textStatus) {
                /* Leg data vast als lokale/globale var */
                if (textStatus == 'success') {
                    Self.datumsData = data;
                    $("#datumselect option").remove();
                    var _list = $("#datumselect")[0];
                    var _option = new Option("Kies een datum...", "");
                    _list.options.add(_option);
                    $.each(data, function(index, itemData) {
                        var _showvalue = itemData.DATUMVALUE;
                        if (itemData.DATUM_AANMELDING) _showvalue += ' (Aanmelding van '+itemData.DATUM_AANMELDING+')';
                        var _option = new Option(_showvalue, itemData.DATUMS_NR);
                        $(_option).data('record', itemData);
                        _list.options.add(_option);
                    });
                    $("#datumselect").attr('disabled',false);

                    if (typeof huidigedatum != 'undefined') {
                        // Select the passed date
                        $("#datumselect").val(huidigedatum);
                        // and trigger the event
                        $("#datumselect").change();
                    } else {
                        if (datumbron == 'aanmeldingsdataexamen') {
                        // Select the first date (= second option)
                            $("#datumselect option:eq(1)").attr('selected', 'selected');
                            // and trigger the event
                            $("#datumselect").change();
                        }
                    }

                    /* Enable the Save button */
                    $('#btnSAVE').removeClass('disabled');
                } else {
                    NHA.SharedLib.modalMessage(NHA.Messages.foutZoekActie + textStatus);
                }
            });
        }
    }
}

$(document).ready(function(){
    var Self = Onderwijs.InschrijvingSelectieForm;

    if ($("#_fldZOEKINSCHRIJVING").val() == '' && typeof sessionStorage.nha_onderwijs_inschrijfnr != 'undefined') {
        $("#_fldZOEKINSCHRIJVING").val(sessionStorage.nha_onderwijs_inschrijfnr);
        Onderwijs.InschrijvingSelectieForm.zoekInschrijving();
    }

    /* Zoek examen events */
    var searchboxExamenAction = function(e) {
        e.preventDefault();
        Self.zoekExamens(e);
    }
    $("#_fldZOEKEXAMENNR").keydown(function(event) {
        if (event.keyCode == 13 /*Enter*/) {
            $("#_fldZOEKEXAMENDATUM option").remove();
            searchboxExamenAction(event);
        }
    });
    $('#btnSEARCHEXAMEN').click(searchboxExamenAction);
    $("#_fldZOEKEXAMENDATUM").click(searchboxExamenAction);

    Onderwijs.InschrijvingSelectieForm.initialiseerEvents();
 });