var Onderwijs = {
    resize: function() {
        $bs = $("#examenselectiearea");
        var height = $(window).height() - $bs.offset().top;
        if ($(".buttons").length > 0) height = Math.max(100,height - $(".buttons").outerHeight() - 50);
        $bs.css(($.browser.msie && $.browser.version < 7 ? '' : 'max-') + 'height', height + 'px');
        $("#examenselectiearea").height(height);
    }
};
