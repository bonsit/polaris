Onderwijs.ExamenAanmeldingForm = {
    examensData: [],
    htmlExamens: '',
    templateExamens: '',
    directivesAanmeldingInfo: '',
    templateAanmeldingInfo: '',
    examenVelden: ['DATUM_AANMELDING', 'REKENINGNROFGIRONRRELATIE', 'BETAALWIJZE', 'PRINTDATUM', 'AANWEZIGHEID'
    ,'DATUM_ANNULERING','REDEN_ANNULERING','GEGRONDE_ANNULERING','EXAMENNR','BEDRAGEXAMENGELD','AANWEZIGHEID','FACTUURRELATIENRBETAALDJANEE'
    ,'UITZONDERINGJANEE','UITZONDERINGOPMERKING','EXTRATIJDINMIN'],

    tekstCijfers: {'o':'onvoldoende', 'g':'goed', 'v':'voldoende'},
    formatCijfer: function(cijfer) {
        var result = '';
        if (typeof Onderwijs.ExamenAanmeldingForm.tekstCijfers[cijfer] != 'undefined') {
            result = Onderwijs.ExamenAanmeldingForm.tekstCijfers[cijfer];
        } else {
            result = cijfer ? Polaris.Base.formatFloat(cijfer, 1) : '';
        }
        return result;
    },
    initialiseer: function() {
        var Self = Onderwijs.ExamenAanmeldingForm;

        Self.htmlAanmeldingInfo = $('#aanmeldinginfo');
        Self.directivesAanmeldingInfo = {
            '.PLR__RECORDID@value':'PLR__RECORDID',
            '.NR@value':'NR',
            '.DATUM_AANMELDING@value':'DATUM_AANMELDING',
            '.INSCHRIJFNR@value':function(arg) {return Onderwijs.InschrijvingSelectieForm.inschrijvingData.INSCHRIJFNR},
//            '.PRINTDATUM':'PRINTDATUM',
            '.EXAMENNR@value':'EXAMENNR',
            '.EXTRATIJDINMIN@value':'EXTRATIJDINMIN',
            '.REKENINGNROFGIRONRRELATIE@value':'REKENINGNROFGIRONRRELATIE',
            '.BETAALWIJZE option@selected': function(arg){ return arg.context.BETAALWIJZE == 'AUT' ? 'true' : '' },
            '.AANWEZIGHEID@checked': function(arg){ return arg.context.AANWEZIGHEID == 'J' ? 'checked' : ''},
            '.FACTUURRELATIENRBETAALDJANEE@checked': function(arg){ return arg.context.FACTUURRELATIENRBETAALDJANEE == 'J' ? 'checked' : ''},
            '.BEDRAGEXAMENGELD@value':'BEDRAGEXAMENGELD',
            '.UITZONDERINGJANEE@checked': function(arg){ return arg.context.UITZONDERINGJANEE == 'J' ? 'checked' : ''},
            '.UITZONDERINGOPMERKING@text':'UITZONDERINGOPMERKING',
            '.DATUM_ANNULERING@value':'DATUM_ANNULERING',
            '.REDEN_ANNULERING@value':'REDEN_ANNULERING',
            '.GEGRONDE_ANNULERING@checked':function(arg){ return arg.context.GEGRONDE_ANNULERING == 'J' ? 'checked' : ''}
        };
        if (Self.htmlAanmeldingInfo.length > 0)
            Self.templateAanmeldingInfo = Self.htmlAanmeldingInfo.compile( Self.directivesAanmeldingInfo );

        Self.htmlExamens = $('#examens tbody');
        //directive to render the template
        var directivesExamens = {
            'tr' : {
                'examen<-': {
                    'input.aangemeld@id': function(arg){ return arg.item.PLR__RECORDID?'rec@' + Polaris.Base.customUrlEncode(arg.item.PLR__RECORDID):'' },
                    'input.aangemeld@value': 'examen.EXAMENNR',
                    'input.aangemeld@checked': function(arg){ return arg.item.AANGEMELD == 'JA'?"checked":"" },
                    'input.plr__record@value': function(arg){ return (arg.item.AANGEMELD == 'JA')&&(arg.item.PLR__RECORDID!=null)?'rec@' + Polaris.Base.customUrlEncode(arg.item.PLR__RECORDID):'' },
                    'input.aanmeldingnr@value': function(arg){ return (arg.item.AANGEMELD == 'JA')?arg.item.EXAMENAANMELDINGNR:'' },
                    'input.datums_examennr@value': 'examen.EXAMENNR',
                    'td.examencode': 'examen.EXAMENCODE',
                    'span.examenomschrijving': 'examen.EXAMENOMSCHRIJVING',
                    'span.deelkwalificatie@title': function(arg){ return 'Deelkwalificatie: ' + arg.item.DEELKWALIFICATIECODE + ' ' + arg.item.OMSCHRIJVING },
//                    'input.datumannulering@value': 'examen.DATUM_ANNULERING',
//                    'input.gegrondeannulering@value': function(arg){ return arg.item.PLR__RECORDID?'rec@' + Polaris.Base.customUrlEncode(arg.item.PLR__RECORDID):'' },
//                    'input.gegrondeannulering@checked': function(arg){ return arg.item.GEGRONDE_ANNULERING == 'J'?"checked":"" },
//                    'input.redenannulering@value': 'examen.REDEN_ANNULERING',
                    '@class': function(arg){
                        //arg => {data:data, items:items, pos:pos, item:items[pos]};
                        var oddEven =  (arg.pos % 2 == 0) ? 'even' : 'odd';
                        var firstLast = (arg.pos == 0) ? 'first': (arg.pos == arg.items.length -1) ? 'last':'';
                        return ' '+oddEven+' '+firstLast;
                    }
                },
            }
        };
        if (Self.htmlExamens.length > 0)
            Self.templateExamens = Self.htmlExamens.compile( directivesExamens );

        Self.htmlExamenCijfers = $('#examencijfers tbody');
        //directive to render the template
        var directivesExamenCijfers = {
            'tr' : {
                'examen<-': {
//                    'input.aangemeld@id': function(arg){ return arg.item.PLR__RECORDID?'rec@' + Polaris.Base.customUrlEncode(arg.item.PLR__RECORDID):'' },
//                    'input.aangemeld@value': 'examen.EXAMENNR',
//                    'input.aangemeld@checked': function(arg){ return arg.item.AANGEMELD == 'JA'?"checked":"" },
                    'input.plr__record@value': function(arg){ return (arg.item.PLR__RECORDID!=null)?'rec@' + Polaris.Base.customUrlEncode(arg.item.PLR__RECORDID):'' },
                    'input.examenaanmeldingnr@value': function(arg){ return arg.item.EXAMENAANMELDINGNR },
//                    'input.datums_examennr@value': 'examen.EXAMENNR',
                    'td.examencode': 'examen.EXAMENCODE',
                    'input.examencijfer@value': function(arg) { return Self.formatCijfer( arg.item.EXAMENPUNT ) },
                    'input.blancoingeleverd@id': function(arg){ return arg.item.PLR__RECORDID?'rec@' + Polaris.Base.customUrlEncode(arg.item.PLR__RECORDID):'' },
                    'input.blancoingeleverd@value': 'examen.EXAMENAANMELDINGNR',
                    'input.blancoingeleverd@checked': function(arg){ return arg.item.BLANCOINGELEVERD == 'JA'?"checked":"" },
                    'td.examenomschrijving': 'examen.EXAMENOMSCHRIJVING',
//                    'span.deelkwalificatie@title': function(arg){ return 'Deelkwalificatie: ' + arg.item.DEELKWALIFICATIECODE + ' ' + arg.item.OMSCHRIJVING },
                    '@class': function(arg){
                        //arg => {data:data, items:items, pos:pos, item:items[pos]};
                        var oddEven =  (arg.pos % 2 == 0) ? 'even' : 'odd';
                        var firstLast = (arg.pos == 0) ? 'first': (arg.pos == arg.items.length -1) ? 'last':'';
                        return ' '+oddEven+' '+firstLast;
                    }
                },
            }
        };
        if (Self.htmlExamenCijfers.length > 0)
            Self.templateExamenCijfers = Self.htmlExamenCijfers.compile( directivesExamenCijfers );
    },
    initialiseerBankRekeningSelectbox: function(record) {
        var _select = $("#bankrekeningnr_select");
        _select.append($("<option>Geen rekeningnr bekend</option>"));
        if (record.BANKGIROREKENINGNR != null || record.FACTUURRELATIEBANKREKENINGNR != null) {
            $("option", _select).remove();
            if (record.BANKGIROREKENINGNR != null)
                _select.append($("<option></option>").attr("value",record.BANKGIROREKENINGNR).text("<-- "+ record.BANKGIROREKENINGNR +" (inschrijving)"));
            if (record.FACTUURRELATIEBANKREKENINGNR != null)
                _select.append($("<option></option>").attr("value",record.FACTUURRELATIEBANKREKENINGNR).text("<-- "+ record.FACTUURRELATIEBANKREKENINGNR +" (factuurrelatie)"));
            $("#bankrekeningnr_select:has(option)").click(function() {
                $(".REKENINGNROFGIRONRRELATIE").val($(this).val());
                $('select.BETAALWIJZE').val("AUT").change();
            });
            if ($(".REKENINGNROFGIRONRRELATIE").val() == '' && $("option", _select).length==1)
                $(".REKENINGNROFGIRONRRELATIE").val($("option", _select).val());
        }
    },
    vulAanmeldingInfo: function(data) {
        var Self = Onderwijs.ExamenAanmeldingForm;
        if (data) {
            $("#aanmeldinginfo").render(data, Self.templateAanmeldingInfo);

            /* als de datum niet is ingevuld, dan de huidige dag invullen */
            if ($('input.DATUM_AANMELDING').val() == '') {
                $('input.DATUM_AANMELDING').val(Polaris.Base.dateToString(new Date()));
                $('input.AANWEZIGHEID').attr('checked','checked'); /* standaard aanwezig */
            }

            /* betaalwijze lukt niet via Pure... nog uitzoeken, nu maar even met de hand */
            $('select.BETAALWIJZE').val(data.BETAALWIJZE||"AUT");

            // probleem met Pure. Er worden rare ?? tekens toegevoegd aan strings met spaties
            $("input.REDEN_ANNULERING").val(data.REDEN_ANNULERING);
            $(":input.UITZONDERINGOPMERKING").val(data.UITZONDERINGOPMERKING);

            /* Vul de bankrekening selectbox met de juiste waarden */
            Self.initialiseerBankRekeningSelectbox(Onderwijs.InschrijvingSelectieForm.inschrijvingData);

            /* Opnieuw de events koppelen. Pure.js verwijderd deze */
            $("#_fldBETAALWIJZE").change(function() {
                $(".REKENINGNROFGIRONRRELATIE").toggleClass('required', $(this).val() == 'AUT');
                if ($(this).val() == 'ACC')
                    $(".REKENINGNROFGIRONRRELATIE").val('');
            }).change();
        }
    },
    zoekExamens: function(cursuscode, datum, callback) {
        var Self = Onderwijs.ExamenAanmeldingForm;
        var inschrijfnr = $("input[name=INSCHRIJFNR]").val();

        $.getJSON(_servicequery, {'func':'examenaanmeldinginfo', 'cursuscode': cursuscode, 'datumsnr': datum, 'inschrijfnr': inschrijfnr}, function(data, textStatus) {
            /* Leg data vast als lokale/globale var */
            if (textStatus == 'success') {
                Self.examensData = data;
                if (Self.examensData.length == 0) {
                    NHA.SharedLib.modalMessage(NHA.Messages.geenExamensGevonden);
                    $(".frmAanmeldingen").trigger('annuleeraanmelding');
                } else {
                    Self.vulExamens(Self.examensData);
                    if (typeof callback != 'undefined')
                        callback();
                }
            } else {
                NHA.SharedLib.modalMessage(NHA.Messages.foutZoekActie + textStatus);
            }
        });
    },
    zoekExamenCijfers: function(cursuscode, datum, callback) {
        var Self = Onderwijs.ExamenAanmeldingForm;
        var inschrijfnr = $("#_fldZOEKINSCHRIJVING").val();

        $.getJSON(_servicequery, {'func':'examencijfers', 'cursuscode': cursuscode, 'datumsnr': datum, 'inschrijfnr': inschrijfnr}, function(data, textStatus) {
            /* Leg data vast als lokale/globale var */
            if (textStatus == 'success') {
                Self.examensData = data;
                if (Self.examensData.length == 0) {
                    NHA.SharedLib.modalMessage(NHA.Messages.geenExamensGevonden);
//                    $(".frmExamencijfers").trigger('annuleerexamencijfer');
                } else {
                    Self.vulExamenCijfers(Self.examensData);
                    if (typeof callback != 'undefined')
                        callback();
                }
            } else {
                NHA.SharedLib.modalMessage(NHA.Messages.foutZoekActie + textStatus);
            }
        });
    },
    controleerIBAN: function() {
        if ($("#_fldREKENINGNROFGIRONRRELATIE").val().length < 11) {
            var err = CheckBankRekeningNr($("#_fldREKENINGNROFGIRONRRELATIE").val());
        } else {
            var err = CheckIBAN($("#_fldREKENINGNROFGIRONRRELATIE").val());
        }
        log(err);
        if (err) {
            Polaris.Base.modalMessage(err, 2000, false);
        }
    },
    vulExamens: function(data) {
        var Self = Onderwijs.ExamenAanmeldingForm;
        Self.htmlExamens = Self.htmlExamens.render( data, Self.templateExamens );
    },
    vulExamenCijfers: function(data) {
        var Self = Onderwijs.ExamenAanmeldingForm;
        Self.htmlExamenCijfers = Self.htmlExamenCijfers.render( data, Self.templateExamenCijfers );
    }
}

$(document).ready(function(){
    /* De betaalwijze event koppelen */
    $("#_fldBETAALWIJZE").change(function() {
        $(".REKENINGNROFGIRONRRELATIE").toggleClass('required', $(this).val() == 'AUT');
    });
    $("#_fldREKENINGNROFGIRONRRELATIE").val('').blur(Onderwijs.ExamenAanmeldingForm.controleerIBAN);
});
