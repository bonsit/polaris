Onderwijs.Aanmelding = {
    verwijderAanmelding: function() {
        if (confirm('Weet u zeker dat u de aanmelding van examen op '+$("#datumselect :selected").text()+' wilt verwijderen?')) {
            var _data = {
                '_hdnDatabase':$('#dataform input[name=_hdnDatabase]').val(),
                '_hdnTable':'ONDERWIJS.AANMELDINGEN',
                '_hdnAction':'delete',
                '_hdnProcessedByModule':'true',
                '_hdnRecordID':$('#dataform input[name=_hdnRecordID]').val()
            };
            jQuery.post(_servicequery, _data, function(data, textStatus) {
                if (textStatus == 'success') {
                    NHA.SharedLib.ajaxFeedback('Aanmelding is verwijderd.');
                    Onderwijs.Aanmelding.annuleerForm();
                    /* vul de datum Select list opnieuw */
//                    Onderwijs.InschrijvingSelectieForm.vulDatums(Onderwijs.InschrijvingSelectieForm.inschrijvingData.INSCHRIJFNR);
                } else {
                    NHA.SharedLib.ajaxFeedback('Aanmelding niet verwijderd. Fout: '+data);
                    $("#datumselect").change();
                }
            });
        }
    },
    bewaarAanmelding: function() {
//$('#dataform')[0].submit();
//return;
		/***
		 * Controleer de standaard velden
		 */
		formValid = true;
		var validator = $("#dataform").validate({
			onsubmit: false,
			debug: false,
			validateHiddenFields: false
		});
		if (!$("#dataform").valid()) {
			formValid = false;
			Polaris.Base.modalMessage('U heeft ' + validator.numberOfInvalids() + ' velden niet correct ingevuld.', 1000, false);
		}
		if ($("#dataform .aangemeld:checked").length==0) {
		    formValid = false;
			Polaris.Base.modalMessage('U dient minimaal een examen aan te vinken.', 1000, false);
		}

        if (formValid) {
            $('#btnSAVE').addClass('disabled');
            Polaris.Ajax.postJSON(_servicequery, $('#dataform').serializeArray(), null, 'Wijzigingen zijn opgeslagen.'
            , function(data) {
                /* vul de datum Select list opnieuw en selecteer de huidige datum */
                Onderwijs.InschrijvingSelectieForm.vulDatums(Onderwijs.InschrijvingSelectieForm.inschrijvingData.INSCHRIJFNR, $("#datumselect").val());
                $('#btnSAVE').removeClass('disabled');
            }
            , function(data) {
                $('#btnSAVE').removeClass('disabled');
            });
        }
    },
    annuleerForm: function(allesleeg) {
        if (typeof allesleeg == 'undefined') allesleeg = false;
        /* Maak de formulieren/velden leeg */
        NHA.SharedLib.easyFillForm([], Onderwijs.InschrijvingSelectieForm.inschrijvingVelden);
        NHA.SharedLib.easyFillForm([], Onderwijs.ExamenAanmeldingForm.examenVelden);
        Onderwijs.Aanmelding.annuleerExamens();
        $("#bankrekeningnr_select").empty();
        if (allesleeg)
            $("#_fldZOEKINSCHRIJVING").val('');
        $("#_fldZOEKINSCHRIJVING").focus();
    },
    annuleerExamens: function(cleardatums) {
        /* Maak de formulieren/velden leeg */
        if (typeof cleardatums == 'undefined') cleardatums = true;
        if (cleardatums)
            Onderwijs.InschrijvingSelectieForm.vulDatums(0);
        Onderwijs.ExamenAanmeldingForm.vulExamens([]);
    },
    resize: function() {
        $bs = $("#examenselectiearea");
        var height = $(window).height() - $bs.offset().top;
        if ($(".buttons").length > 0) height = Math.max(100,height - $(".buttons").outerHeight() - 50);
        $bs.css(($.browser.msie && $.browser.version < 7 ? '' : 'max-') + 'height', height + 'px');
        $("#examenselectiearea").height(height);
    }
}

$(document).ready(function(){
    Onderwijs.InschrijvingSelectieForm.initialiseer();
    Onderwijs.ExamenAanmeldingForm.initialiseer();

    /* Form actions */
    $('#dataform').submit(function(e) {
        e.preventDefault();
    });
    $("#btnNEW").click(function(e) {
        e.preventDefault();
        if (!$(this).hasClass('disabled'))
            Onderwijs.Aanmelding.annuleerForm(true);
    });
    $('#btnSAVE').click(function(e) {
        e.preventDefault();
        if (!$(this).hasClass('disabled'))
            Onderwijs.Aanmelding.bewaarAanmelding();
    });
    $('#btnCANCEL').click(function(e) {
        e.preventDefault();
        if (!$(this).hasClass('disabled'))
            Onderwijs.Aanmelding.annuleerForm();
    });

    $("#verwijder_examenaanmelding").click(function(e) {
        e.preventDefault();
        Onderwijs.Aanmelding.verwijderAanmelding();
    });

    $("#datumselect").change(function() {
        var _record = $("option:selected",this).data('record');
        var _datum = $(this).val();
        Onderwijs.ExamenAanmeldingForm.vulAanmeldingInfo(_record);
        Onderwijs.ExamenAanmeldingForm.zoekExamens(Onderwijs.InschrijvingSelectieForm.inschrijvingData.CURSUSCODE, _datum, function() {
            /* Maak de date input velden opnieuw, omdat Pure de events niet mee overneemt bij het renderen */
            Polaris.Form.initializeDatePickers();
        });
    });

    $("#_fldUITZONDERINGOPMERKING").keyup(function(e) {
        $("#_fldUITZONDERINGJANEE").attr('checked', $(this).val() != '');
    });

    $(window).resize(Onderwijs.resize);
    $(".pagetabcontainer a").click(Onderwijs.resize);
    Onderwijs.resize();

    $("#_fldZOEKINSCHRIJVING").focus();

    // luister naar events vanuit de formulier secties (examenaanmelding_form.js)
    $(".frmAanmeldingen").bind('annuleeraanmelding', function() { Onderwijs.Aanmelding.annuleerExamens(false) });

});