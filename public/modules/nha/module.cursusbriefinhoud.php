<?php
require_once('includes/mainactions.inc.php');
require_once('includes/datefuncs.inc.php');
require_once('_shared/module._base.php');

class Module extends _BaseModule {
    var $database; // plr database object
    var $cacheTimeOut;
    var $_nrOfDesc = 5;

    function Module($moduleid, $module) {
        global $_GVARS;
        global $polaris;
        global $ADODB_FETCH_MODE;

        parent::_BaseModule($moduleid, $module);

        $this->cacheTimeOut = 5*60;

        $this->processed = false;
        $this->clientid = $_SESSION['clientid'];
        $this->currentdb = 15;

        $this->database = new plrDatabase($polaris);
        $this->database->LoadRecord(array($this->clientid, $this->currentdb));
        $this->userdatabase = $this->database->connectUserDB();
//        $ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
    }

    /**
     * Module function __GetCustomFilter:
     * Op deze manier kunnen we de geselecteerde advertentiecode doorsturen aan Polaris, zodat deze de juiste
     * recordcount kan bepalen
     */
    function __GetCustomFilter() {
        $_customfilter = '';
        return $_customfilter;
    }

    function Process() {
        if ($this->database)
            $this->database->SetOracleUserID($_SESSION['userid']);
        if (isset($_POST['_hdnProcessedByModule'])) {
            $_allesRecord = $_POST;
            try {
                switch($_allesRecord['_hdnFORMTYPE']) {
                case '...':
                    //$result = $this->Bewaar...($_allesRecord);
                break;
                }
                $resultArray = Array('result'=>$result, 'query'=>$_SERVER['REQUEST_URI'], 'error'=>false);
                echo json_encode($resultArray);
                exit;
            } catch (Exception $E) {
                $detailedmessage = $E->completemsg."\r\n".parse_backtrace(debug_backtrace());
                if ($result == null) $result = false;

                $resultArray = Array('result'=>$result, 'query'=>$_SERVER['REQUEST_URI'], 'error'=>$E->msg, 'detailed_error'=>$detailedmessage);
                echo json_encode($resultArray);
                exit;
            }
        }
    }

    function BewaarNieuweAdvertentiecode($_allesRecord) {
        $_result = $this->userdatabase->Execute(
            "INSERT INTO NHA.ADVERTENTIECODE (ADVERTENTIECODE) VALUES(:CODE)"
            , array('CODE'=>$_allesRecord['NIEUWEADVERTENTIECODE'])
        );
        $_result = $this->userdatabase->Execute(
            "INSERT INTO MARKETING.FT_VERSPREIDINGADV (ADVERTENTIECODE) VALUES(:CODE)"
            , array('CODE'=>$_allesRecord['NIEUWEADVERTENTIECODE'])
        );
        return $this->GetGroepAdvcodes($_allesRecord['NIEUWEADVERTENTIECODE']);
    }

    function VerwijderCampagne($_allesRecord) {
        $_sql = "DELETE FROM NHA.ADVERTENTIECODE WHERE ADVERTENTIECODE = :ADV";
        $this->userdatabase->Execute($_sql, array('ADV' => $_allesRecord['ADVERTENTIECODE']));
        $_sql = "DELETE FROM MARKETING.FT_VERSPREIDING WHERE ADVERTENTIECODE = :ADV";
        $this->userdatabase->Execute($_sql, array('ADV' => $_allesRecord['ADVERTENTIECODE']));
        $_sql = "DELETE FROM MARKETING.FT_VERSPREIDINGADV WHERE ADVERTENTIECODE = :ADV";
        return $this->userdatabase->Execute($_sql, array('ADV' => $_allesRecord['ADVERTENTIECODE']));
    }

    function BewaarVerspreiding($_allesRecord) {
        global $polaris;

        $_allesRecord = $polaris->ConvertFloatValues($_allesRecord);
        $_result = $this->userdatabase->Execute(
            "UPDATE MARKETING.FT_VERSPREIDINGADV SET DRUKKOSTEN = :DRUKKOSTEN
            WHERE ADVERTENTIECODE = :ADVERTENTIECODE"
            , array(
                'DRUKKOSTEN'=>$_allesRecord['DRUKKOSTEN'],
                'ADVERTENTIECODE'=>$_allesRecord['ADVERTENTIECODE']
            )
        );
        return $_result;
    }

    function GetLanden() {
        return $this->userdatabase->GetAssoc(
            "SELECT LANDCODEVANINSCHRIJVING, LAND AS OMSCHRIJVING FROM NHA.LANDVANINSCHRIJVING"
        );
    }

    function GetVerspreiders($landcode) {
        $filter = 'WHERE LANDCODE = \''.$landcode.'\'';
        if ($_GET['action'] !== 'edit') {
            $filter .= ' AND ACTIEF = \'J\'';
        }
        return $this->userdatabase->GetAssoc(
            "SELECT VERSPREIDER, CONCAT(CONCAT(LANDCODE, ' - '), NAAM) AS OMSCHRIJVING
            FROM MARKETING.FT_VERSPREIDER
            $filter
            ORDER BY LANDCODE, NAAM"
        );
    }

    function GetAdvertentieCodes($landcode=FALSE, $advertentiecode=FALSE) {
//            AND (V.PINNED = 'J' OR V.PINNED = :PINNED)
        if (!isset($this->advcodes)) {
            $_sql = sprintf("
                SELECT MAX(A.ADVERTENTIECODE), MAX(A.ADVERTENTIECODE) || ' ' || NVL2(V.ADVERTENTIECODE, '', '<nieuw>') AS OMSCHRIJVING
                FROM NHA.ADVERTENTIECODE A, MARKETING.FT_VERSPREIDING V
                WHERE A.ADVERTENTIECODE = V.ADVERTENTIECODE (+)
                %s
                GROUP BY V.LANDCODE, V.ADVERTENTIECODE, V.PERIODE_VANAF
                ORDER BY NVL2(V.LANDCODE, V.LANDCODE, 0), SUBSTR(V.ADVERTENTIECODE, -2, 2) || SUBSTR(V.ADVERTENTIECODE, -4, 2) || V.ADVERTENTIECODE DESC, PERIODE_VANAF DESC
                ", ($landcode ? "AND A.ADVERTENTIECODE <> '$advertentiecode' AND LANDCODE = '$landcode'" : ""));
            $rs = $this->userdatabase->GetAssoc( $_sql, array('PINNED'=>($landcode?'J':'N')) );
            $this->advcodes = $rs;
        }
        return $this->advcodes;
    }

    function GetAdvertentieSoortCode() {
        return json_encode($this->userdatabase->GetAll(
            "SELECT ADVERTENTIESOORTCODE, ADVERTENTIESOORTCODE AS OMSCHRIJVING FROM NHA.ADVERTENTIESOORT WHERE SOORTBLADNAAM = 'HUIS AAN HUIS BLAD'"
        ));
    }

    function GetVerspreidplanPeriodes($advcode) {
        return $this->userdatabase->GetAll(
            "SELECT ID, PERIODE_VANAF, PERIODE_TM, ADVERTENTIECODE, MUTATIEDATUM, LANDCODE, VERSPREIDER
            , VARIANT, COMMENTAAR, AANTALBRIEVENBUSSEN, WEEKNR, PINNED, NIVEAU, TELLENINANALYSE
            FROM MARKETING.FT_VERSPREIDING
            WHERE ADVERTENTIECODE = :ADVERTENTIECODE", array("ADVERTENTIECODE"=>$advcode)
        );
    }

    function GetEersteItem($advertentiecode) {
        $_sql = "SELECT * FROM (SELECT * FROM FT_VERSPREIDING WHERE ADVERTENTIECODE = :ADVERTENTIECODE ORDER BY PERIODE_VANAF) WHERE ROWNUM <= 1 ";
        $_result = $this->userdatabase->GetRow($_sql, array("ADVERTENTIECODE" => $advertentiecode));
        return $_result;
    }

    function BepaalLandCode($advcode) {
        switch(substr($advcode, 3,1)) {
        case 'B':
            $_landcode = 'BE';
            break;
        case 'D':
            $_landcode = 'DE';
            break;
        default:
            $_landcode = 'NL';
        }
        return $_landcode;
    }

    function ShowItemView() {
        $rowid = $this->form->database->customUrlDecode($_GET['rec']);
        $this->form->startno = 0;
        $_rs = $this->form->GetViewContent($detail=false, $masterrecord=false, false, $filter="ttt.ROWID = '{$rowid}'");
        $items = $_rs->GetAll();
        $_inserting = isset($items[0]) ? false : true;
        if ($_inserting) {
            $_landcode = $this->BepaalLandCode($_SESSION['mod.mark.huidigeadvcode']);
            $_eersteitem = $this->GetEersteItem($_SESSION['mod.mark.huidigeadvcode']);
            $this->smarty->assign("eersteitem", $_eersteitem);
        } else {
            $_landcode = $items[0]['LANDCODE'];
        };
        $items[0]['VERSPREIDKOSTEN'] = number_format((float) str_replace(',','.',$items[0]['VERSPREIDKOSTEN']), $this->_nrOfDesc, ',','.');
        $this->smarty->assign("items", $items);
        $this->smarty->assign("landenopties", $this->GetLanden());
        $this->smarty->assign("regioopties", $this->GetRegios($_landcode));
        $this->smarty->assign("herhalingvan", $this->GetHerhalingVan($rowid, $_SESSION['mod.mark.huidigeadvcode']));
//        $this->smarty->assign("advertentiecodes", $this->GetAdvertentieCodes());
        $this->smarty->assign("advertentiecode", $_SESSION['mod.mark.huidigeadvcode']);
        $this->smarty->assign("verspreideropties", $this->GetVerspreiders($_landcode));
        $this->smarty->display("verspreiding_insert.tpl.php");
    }

    function GetHuidigeAdvCode() {
        if ($_GET['adv'] != '') {
            $_advcode = $_GET['adv'];
        } elseif (isset($_SESSION['mod.mark.huidigeadvcode'])) {
            $_advcode = $_SESSION['mod.mark.huidigeadvcode'];
        } else {
            if (isset($this->advcodes)) {
                $_advcode = array_keys($this->advcodes);
                $_advcode = $_advcode[0];
            }
        }
        $_SESSION['mod.mark.huidigeadvcode'] = $_advcode;
        return $_advcode;
    }

    function GetHuidigeRecordID($codes, $advcode) {
        foreach($codes as $_taalgroep) {
            foreach($_taalgroep as $_advcode) {
                if ($_advcode['ADVERTENTIECODE'] == $advcode) {
                    return $_advcode['PLR__RECORDID'];
                }
            }
        }
    }

    function GetGroepAdvcodes($advcode=false) {
        $this->userdatabase->SetFetchMode(ADODB_FETCH_ASSOC);
        $_sql = "SELECT * FROM VW_GROEPADVERTENTIECODES";
        if ($advcode) {
            $_sql .= " WHERE ADVERTENTIECODE = :ADVERTENTIECODE";
            $rs = $this->userdatabase->GetAll($_sql, array("ADVERTENTIECODE" => $advcode));
        } else {
            $rs = $this->userdatabase->GetAll($_sql);
        }
        $this->userdatabase->SetFetchMode(ADODB_FETCH_NUM);
        $_data = array();
        foreach($rs as $k => $rec) {
            $_data[$rec['LAND']][] = $rec;
        }
        return $_data;
    }

    function ShowListView() {
        $this->form->recordcount = -1;
//        $_huidigeadvcode = $this->GetHuidigeAdvCode();
        //$this->smarty->assign("advertentiecode", $_huidigeadvcode);
        $this->smarty->display("briefkoppeling.tpl.php");
        $this->form->LoadViewContent($detail=false, $masterrecord=false, $limit=false, $this->__GetCustomFilter());
        $this->form->ShowListView($state='view', $this->permission);
    }

    function SplitScores($data) {
        $_data = Array();
        foreach($data as $i => $_row) {
            $_section[] = $_row;
            $_nxtVers = $data[$i+1]['VERSPREIDER'];
            $_nxtVariant = $data[$i+1]['VARIANT'];

            if ($i > 0 and $_row['VERSPREIDER'] !== $_nxtVers and $_section) {
                if ($_row['VARIANT'] !== 'TOTAAL')
                    $_data[$_row['VERSPREIDER']][$_row['VARIANT']] = $_section;
                unset($_section);
            }

            if ($i > 0 and $_row['VARIANT'] !== $_nxtVariant and $_section) {
                if ($_row['VARIANT'] !== 'TOTAAL')
                    $_data[$_row['VERSPREIDER']][$_row['VARIANT']] = $_section;
                unset($_section);
            }

            // Totalen per verspreider
            if ($_row['ADVERTENTIECODE_ADV'] == 'TOTAAL') {
                $_data[$_row['VERSPREIDER']]['TOTAAL'][] = $_row;
            }

            // Hoofdtotalen
            if ($_row['ADVERTENTIECODE_ADV'] == 'TOTAAL') {
                $_data['TOTAAL'][$_row['VERSPREIDER']][$_row['VARIANT']][] = $_row;
            }
        }
        foreach($_data as $k => $v) {
            ksort($_data[$k]);
        }
        ksort($_data);
        unset($_data['TOTAAL']['TOTAAL']['TOTAAL']['1']);
// echo "<pre>";
// do_dump($_data['TOTAAL']);
// echo "</pre>";
        return $_data;
    }

    function GetScoreOverzicht($advertentiecode) {
        $_sql = "
        select NVL2(v.weeknr, v.advertentiecode, 'TOTAAL') as advertentiecode_adv
        , NVL(v.verspreider, 'TOTAAL') as verspreider, NVL(v.variant, 'TOTAAL') AS VARIANT, NVL(v.weeknr, 0) as weeknr_a1
        , sum(r.aantalbussen) as som_aantalbussen_b1, sum(r.brutototaal) as som_brutototaal_c1
        , sum(r.brutototaal) / sum(r.aantalbussen) as conversie_d1
        , sum(r.brutoadv) as som_brutoadv_e1, sum(r.brutoadv) / sum(r.aantalbussen) as conversie_f1
        , MIN(va.drukkosten) as drukkosten_l1, MIN(v.verspreidkosten) as verspreidkosten_m1
        , NVL2(v.weeknr, MIN(vs.naam), 'TOTAAL') as verspreidernaam
        from MARKETING.FT_VERSPREIDINGADV va, MARKETING.FT_VERSPREIDING v, MARKETING.FT_VERSPREIDINGREGELS r, MARKETING.FT_VERSPREIDER vs
        where va.advertentiecode = v.advertentiecode AND v.id = r.verspreidingid
        and vs.verspreider = v.verspreider
        and v.advertentiecode = :ADVERTENTIECODE
        group by rollup(v.advertentiecode, v.verspreider, v.variant, v.weeknr)
        --having group_id() < 1
        --having v.weeknr is null
        order by v.advertentiecode, DECODE(v.verspreider, 'TNT', '0', v.verspreider), NVL(v.variant, ''), v.weeknr
        ";
        $this->userdatabase->SetFetchMode(ADODB_FETCH_ASSOC);
        $_data = $this->userdatabase->GetAll($_sql, array('ADVERTENTIECODE' => $advertentiecode));
        $this->userdatabase->SetFetchMode(ADODB_FETCH_NUM);
        return $_data;
    }

    function GetVerspreiderItems($_scores) {
        if (is_array($_scores))
            return array_keys($_scores);
        else
            return FALSE;
    }

    function GetAnalyse($advcode) {
        return $this->userdatabase->GetOne("SELECT ANALYSE FROM FT_VERSPREIDINGADV WHERE ADVERTENTIECODE = :advertentiecode"
        , array("advertentiecode" => $advcode));
    }

    function BewaarAnalyse($_allesRecord) {
        $_result = $this->userdatabase->Execute(
            "UPDATE MARKETING.FT_VERSPREIDINGADV SET ANALYSE = :ANALYSE
            WHERE ADVERTENTIECODE = :ADVERTENTIECODE"
            , array(
                'ANALYSE'=>$_allesRecord['ANALYSE'],
                'ADVERTENTIECODE'=>$_allesRecord['ADVERTENTIECODE']
            )
        );
        return $_result;
    }

    function ShowScoreOverzicht() {
        $this->GetAdvertentieCodes();
        $_huidigeadvcode = $this->GetHuidigeAdvCode();
        $_data = $this->GetScoreOverzicht($_huidigeadvcode);
        $_scores = $this->SplitScores($_data);
        $_verspreiders = $this->GetVerspreiderItems($_scores);

        $this->smarty->assign("advertentiecodes", $this->GetGroepAdvcodes());
        $this->smarty->assign("advertentiecode", $_huidigeadvcode);
        $this->smarty->assign("analyse", $this->GetAnalyse($_huidigeadvcode));
        $this->smarty->assign("scores", $_scores);
        $this->smarty->assign("verspreiders", $_verspreiders);
        $this->smarty->display('mt_totalen.tpl.php');
    }

    function Show($outputformat='xhtml', $rec=false) {
        global $_GVARS;

        parent::Show($outputformat, $rec);

        $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/cursusbriefinhoud.js';

        if ($_GET['func'] != '') {
            if ($_GET['func'] == 'advertentiesoorten') {
                echo $this->GetAdvertentieSoortCode();
                exit;
            }
            if ($_GET['func'] == 'groepadvcodes') {
                echo json_encode($this->GetGroepAdvcodes());
                exit;
            }
        } else {
            switch ($this->moduleid) {
            case 1:
                /* Show the Delete button in list view */
                $this->showDefaultButtons = true;
var_dump($_GET['action']);
                switch($_GET['action']) {
                case 'edit':
                case 'insert':
                    $this->ShowItemView();
                    break;
                default:
                    $this->ShowListView();
                    break;
                }
            break;
            case 2:
                $this->showDefaultButtons = false;
                $this->ShowScoreOverzicht();
            break;
            }
            }
        }
    }

}