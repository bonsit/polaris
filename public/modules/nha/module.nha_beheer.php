<?php
require_once('includes/mainactions.inc.php');
require_once('_shared/module._base.php');
require_once('module.nha_macs/php/forms.class.php');

class Module extends _BaseModule {

    function Module($moduleid, $module) {
        global $_GVARS;
        parent::_BaseModule($moduleid, $module);

        $this->processed = false;

        /**
        * What client are we talking about? (nog doen: eventueel selecteerbaar maken)
        */
        $this->clientid = $_SESSION['clientid'];
    }

    function Process() {
        global $_GVARS;
        global $polaris;

        $_plrinstance = $this->form->plrInstance();
        $_plrinstance->SetFetchMode(ADODB_FETCH_ASSOC);
        $this->form->database->userdb->SetFetchMode(ADODB_FETCH_ASSOC);

        $this->form->database->SetOracleUserID($_SESSION['userid']);

        if (isset($_POST['_hdnProcessedByModule'])) {
            $_allesRecord = $_POST;

            $forms = new Forms($this->form->database->userdb);

            try {
                switch($_POST['_hdnFORMTYPE']) {
                /***
                * Upload postcodebestand verwerken
                */
                case 'postcodeimporteren':
                    if ($_allesRecord['_hdnAction'] == 'btnVerwerkBE') {
                        $result = $this->VerwerkPostcodeTabel('be');
                    }
                    if ($_allesRecord['_hdnAction'] == 'btnVerwerkNL') {
                        $result = $this->VerwerkPostcodeTabel('nl');
                    }
                    if ($_allesRecord['_hdnAction'] == 'butDeleteImportbe') {
                        $result = $this->VerwijderPostcodeBestand('be');
                    }
                    $result = $this->VerwerkPostcodeBestand($_FILES);
                break;
                }

//                $resultArray = Array('result'=>$result, 'query'=>$_SERVER['REQUEST_URI'], 'error'=>false);
//                echo json_encode($resultArray);

            } catch (Exception $E) {
                $detailedmessage = $E->completemsg."\r\n".parse_backtrace(debug_backtrace());

                $resultArray = Array('result'=>false, 'query'=>$_SERVER['REQUEST_URI'], 'error'=>$E->msg, 'detailed_error'=>$detailedmessage);
                echo  json_encode($resultArray);
            }

        }
    }

    function VerwijderPostcodeBestand($landcode) {
        $_sp = "DELETE FROM POSTCODEIMPORTSCHEDULE WHERE landcode = :landcode AND DATUM IN (SELECT MAX(DATUM) FROM POSTCODEIMPORTSCHEDULE WHERE landcode = :landcode)";
        $this->form->database->userdb->Execute($_sp, array('landcode'=>strtoupper($landcode)));
    }

    function VerwerkPostcodeTabel($landcode) {
        $landcode = strtoupper($landcode);

        $_sp = $this->form->database->userdb->Prepare("BEGIN PackNHAPostcode.Verwerk${landcode}Postcodes(); END;");
        $this->form->database->userdb->Execute($_sp);
    }

    function BestandsOpmaakInOrde($landcode, $bestandsnaam) {
        $result = FALSE;

        $aantalrecords = count(file($bestandsnaam));

        if (($handle = fopen($bestandsnaam, "r")) !== FALSE) {
            // Bekijk een regel
            $data = fgets($handle);
            //if (strpos($data, 'Provincie - Province;') !== FALSE and $landcode == 'BE') {
            if ($landcode == 'BE') {
                $result = TRUE;
            } elseif (strpos($data, '*** POSTCODETABEL PTT REEKS') !== FALSE and $landcode == 'NL') {
                $result = TRUE;
            }
        }
        return $result;
    }

    function OpdrachtToevoegenAanLog($landcode, $bestand, $bestandsnaam, $grootte, $aantalrecords) {
        $result = TRUE;
        $this->form->database->userdb->debug=false;

        if ($aantalrecords != -1) {
            $_sqlVorigeNietVerwerkteOpdrachtenNegeren = "UPDATE POSTCODEIMPORTSCHEDULE SET STATUS = 'Niet verwerkt', VERWERKT = 'O' WHERE VERWERKT = 'N' AND LANDCODE = '$landcode'";
            $this->form->database->userdb->Execute($_sqlVorigeNietVerwerkteOpdrachtenNegeren);

            $_sql = "INSERT INTO POSTCODEIMPORTSCHEDULE (LANDCODE, BESTANDSNAAM, BESTANDSNAAMTEMP, AANTALRECORDS, GROOTTE)
            VALUES (:landcode, :bestand, :bestandsnaam, :aantalrecords, :grootte)";

            $this->form->database->userdb->Execute($_sql,
                array('landcode'=>$landcode, 'bestand'=>$bestand, 'bestandsnaam'=>$bestandsnaam, 'aantalrecords'=>$aantalrecords, 'grootte'=>$grootte));
        }

        $this->form->database->userdb->debug=false;
        return $result;
    }

    function VerwerkBestand($landcode, $bestandsnaam) {
        $row = 0;
        if (($handle = fopen($bestandsnaam, "r")) !== FALSE) {
            // Doorloop alle regels
            if ($landcode == 'BE')
                $_sql = "INSERT INTO POSTCODETABEL".$landcode."_VERWERKEN (TAALCODE, PROVINCIE, GEMEENTE, DEELGEMEENTE, POSTCODE, BRIEVENBUSSEN) VALUES (:TAALCODE, :PROVINCIE, :GEMEENTE, :DEELGEMEENTE, :POSTCODE, :AANTALBUSSEN)";
            else
                $_sql = "INSERT INTO POSTCODETABEL".$landcode."_VERWERKEN (POSTCODE, STRAAT, PLAATS, HUISNRVAN, HUISNRTM) VALUES (:POSTCODE, :STRAAT, :PLAATS, :HUISNRVAN, :HUISNRTM)";

            $transTaalCode = array(' & ' => "_", "(" => "", ")" => "");
//            $_sqlParsed = $this->form->database->userdb->Prepare($_sql);
            while (($data = fgets($handle, 4096)) !== FALSE) {
                $data = iconv('macintosh', 'UTF-8', $data);
                // Is het een postcode regel?
                if ($landcode == 'BE') {
                    /**
                    *  Belgische postcode
                    */
                    $data = explode(';', $data);
                    $_array = FALSE;
                    if (trim($data[1]) != '' and ($data[1] == strtoupper($data[1]))) {
                        $_taalcode = str_replace(' ','_',strtr(trim($data[5]), $transTaalCode));
                        $_provincie = explode(' / ', $data[0]);
                        $_provincie = trim($_provincie[0]);
                        $_gemeente = explode(' / ', $data[1]);
                        $_gemeente = trim($_gemeente[0]);
                        $_deelgemeente = explode(' / ', $data[2]);
                        $_deelgemeente = trim($_deelgemeente[0]);
                        $_aantalbussen = trim($data[6]);
                        if (!is_numeric($_aantalbussen)) {
                            $_aantalbussen = 0;
                        }

                        if (in_array($_taalcode, array('FR','NL','DE','FR_NL','NL_FR','FR_DE','DE_FR'))) {
                            $_array = array('TAALCODE'=>$_taalcode, 'PROVINCIE'=>$_provincie, 'GEMEENTE'=>$_gemeente, 'DEELGEMEENTE'=>$_deelgemeente, 'POSTCODE'=>trim($data[3]), 'AANTALBUSSEN'=>$_aantalbussen);
                        }
                    } else {
                        //var_dump($data);
                    }
                } else {
                    /**
                    *  Nederlandse postcode
                    */
                    $_array = FALSE;
                    if ($data[0] != '*') {
                        $_postcode = trim(substr($data, 0, 6));
                        $_huisvan = trim(substr($data, 7, 5));
                        $_huistm = trim(substr($data, 12, 5));
                        $_plaats = trim(substr($data, 35, 24));
                        $_straat = trim(substr($data, 76, 24));

                        if (strlen($_postcode) == 6) {
                            $_array = array('POSTCODE'=>$_postcode, 'STRAAT'=>$_straat, 'PlAATS'=>$_plaats, 'HUISNRVAN'=>$_huisvan, 'HUISNRTM'=>$_huistm);
                        }
                    }
                }

                if ($_array) {
                    // Correcte regel? Voeg de regel toe aan tabel postcodetabel<landcode>_verwerken
                    try {
                        $this->form->database->userdb->Execute($_sql, $_array);
                    } catch (Exception $E) {
                        var_dump($_array);
                        throw $E;
                    }
                    $row++;
                }
            }
//            fclose($handle);
        } else {
            $row = -1;
        }
        return $row;
    }

    function MakeFileChunks( $srcFilename, $chunkSize, $filePrefix) {
        $splitcmd = sprintf("split -a5 -l %u %s %s 2>&1", $chunkSize, $srcFilename, $filePrefix);
        $output = null;

        exec($splitcmd, $output, $return_code);
        if ($return_code) {
            die("Chunk split failed, code: ".$return_code." message: ".implode("\n", $output));
        }

        $chunkfiles = glob($filePrefix.'*');
        sort($chunkfiles);

        var_dump($chunkfiles);
    }

    function TijdelijkePCRecordsAanmaken($landcode, $bestandsnaam) {
        $this->form->database->userdb->debug=false;

        $_sql = "DELETE FROM POSTCODETABEL".$landcode."_VERWERKEN";
        $this->form->database->userdb->Execute($_sql);

        $this->form->database->userdb->debug=false;

//        $this->MakeFileChunks( $bestandsnaam, $chunkSize='20 m', $filePrefix='pc_chunk' );

        // Lees Bestand
        $_aantalrecords = $this->VerwerkBestand($landcode, $bestandsnaam);

        return $_aantalrecords;
    }

    function _VerwerkPostcodeBestand($allesRecord, $landcode) {
        $landcode = strtoupper($landcode);
        $_entry = strtolower($landcode).'_bestand';

        if ($allesRecord[$_entry]['name'] != '') {
            // belgische postcode bestand verwerken
            $_bestand = $allesRecord[$_entry]['name'];
            $_bestandsNaam = $allesRecord[$_entry]['tmp_name'];
            if ($this->BestandsOpmaakInOrde($landcode, $_bestandsNaam)) {
                $_grootte = $allesRecord[$_entry]['size'];
                $_aantalrecords = $this->TijdelijkePCRecordsAanmaken($landcode, $_bestandsNaam);
                $this->OpdrachtToevoegenAanLog($landcode, $_bestand, $_bestandsNaam, $_grootte, $_aantalrecords);
            } else {
                echo "<p style='color:red'>Het bestand is niet correct opgemaakt en kan niet verwerkt worden. Upload een ander bestand.</p>";
            }
        }
    }

    function VerwerkPostcodeBestand($allesRecord) {
        $landcode = 'BE';
        $this->_VerwerkPostcodeBestand($allesRecord, $landcode);

        $landcode = 'NL';
        $this->_VerwerkPostcodeBestand($allesRecord, $landcode);
    }

    function GetJsonNHALogging($ernst, $tijd, $datum, $context) {
        $_error = false;
        $_sql = "SELECT LOGID, MASTERID, ERNST, CONTEXT, TO_CHAR(DATUM, 'DD-MM-YYYY HH24:MI:SS') AS DATUM, MELDING, ORA_ERROR
        FROM NHA.LOGGING ttt
        WHERE BITAND(:ERNST, ERNSTID) = ERNSTID
        ";
        if ($context != '') {
            $_sql .= " AND (CONTEXT = '$context')";
        }
        if ($tijd > 0) {
            $_sql = $this->form->owner->AddMasterFilter($_sql);
            $_sql = $this->form->AddTagFilter($_sql);
            /*
            1 => Vandaag
            2 => Gisteren
            4 => Alles
            8 => Specifieke datum
             */
            if (4 & $tijd == 4) {
                // no date filtering
            } else {
                if (($tijd & 1) == 1) {
                    // vandaag
                    $_where[] = " DATUM >= TRUNC(SYSDATE)";
                }
                if (($tijd & 2) == 2)
                    // 24 uur
                    $_where[] = " (DATUM > SYSDATE - 1)";
                if ((($tijd & 16) == 16))
                    // afgelopen 30 dagen
                    $_where[] = " (DATUM >= TRUNC(SYSDATE) - 30)";
                if ((($tijd & 8) == 8) and $datum != '')
                    if (strtotime($datum) === FALSE) {
                        $_error = 'Verkeerde datum formaat';
                    } else {
                        // specifieke datum
                        $_where[] = " (DATUM >= TO_DATE('$datum', 'DD-MM-YYYY') AND DATUM < TO_DATE('$datum', 'DD-MM-YYYY') + 1)";
                    }
            }
            if (is_array($_where)) {
                $_wheredatum = implode(" OR ", $_where);
                $_sql .= " AND ($_wheredatum)";
            }
            $_sql .= " ORDER BY LOGID DESC NULLS LAST, ttt.DATUM DESC";
            $_rs = $this->form->database->userdb->SelectLimit($_sql, 2000, 0, array('ERNST'=>$ernst));
            $_rs = $_rs->GetArray();
        } else {
            $_rs = false;
        }
        $resultArray = Array('result'=>$_rs, 'query'=>$_sql, 'error'=>$_error);

        return json_encode($resultArray);
    }

    function GetImportLog($landcode) {
        /* Haal de import logs op */
        $_sqlLogs = "
        SELECT TO_CHAR(DATUM, 'DD-MM-YYYY HH24:MI') AS DATUM_FORMAT, BESTANDSNAAM, AANTALRECORDS, AANTALRECORDSVERWERKT, GROOTTE, VERWERKT, STATUS
        FROM POSTCODEIMPORTSCHEDULE WHERE LANDCODE = :LANDCODE
        ORDER BY DATUM DESC
        ";
        return $this->form->database->userdb->GetAll($_sqlLogs, array('LANDCODE'=>$landcode));
    }

    function GetCountPC($landcode) {
        /* Haal de import logs op */
        $_landnaam = ($landcode == 'BE')?"BELGIE":"NEDERLAND";
        $_sqlCount = "SELECT COUNT(*) FROM POSTCODETABEL$_landnaam";
        return $this->form->database->userdb->GetOne($_sqlCount);
    }

    function DisplayImporteerPCBestanden() {
        if ($this->outputformat == 'xhtml') {
            $this->showControls = false;
            $this->smarty->assign('import_be', $this->GetImportLog('BE'));
            $this->smarty->assign('import_nl', $this->GetImportLog('NL'));
            $this->smarty->assign('be_count', $this->GetCountPC('BE'));
            $this->smarty->assign('nl_count', $this->GetCountPC('NL'));
            $this->smarty->display("importeer_pcbestanden.tpl.php");
        }
    }

    function GetLogContexts() {
        $_sql = "SELECT DISTINCT CONTEXT, CONTEXT AS OMSCHRIJVING FROM NHA.LOGGING WHERE CONTEXT IS NOT NULL ORDER BY CONTEXT";
        return $this->form->database->userdb->CacheGetAssoc($_sql);
    }

    function DisplayNHALogging() {
        if ($this->outputformat == 'xhtml') {
            $this->showControls = false;
            $this->smarty->assign('contextselect', $this->GetLogContexts());
            $this->smarty->display("nha_logging.tpl.php");
        }
    }

    function Show($outputformat='xhtml', $rec=false) {
        global $_GVARS;
        global $polaris;

        parent::Show($outputformat, $rec);

        if (isset($_GET['action'])) {
            /**
            * Show the Polaris generated forms when the user wants to insert or edit an item
            */
            switch($_GET['action']) {
            case 'edit':
            case 'insert':
            break;
            case 'searchext':
            break;
            }
        } else {
            /**
            * Show the custommade forms when user views a list of items
            */
            switch ($this->moduleid) {
            case 1:
                /***
                * Importeren van postcode bestanden
                */
                if ($_GET['func'] != '') {
                    if ($_GET['func'] == '....') {
                        //echo $this->GetJsonCursusGegevens($_GET['cursuscode'], $_GET['zelfstudie'], $_GET['belgie']);
                    }
                } else {
                    $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/sharedlib.js';
                    $this->DisplayImporteerPCBestanden();
                }
            break;
            case 2:
                /***
                * Tonen van logging items
                */
                if ($_GET['func'] != '') {
                    if ($_GET['func'] == 'nhalogging') {
                        echo $this->GetJsonNHALogging($_GET['ernst'], $_GET['tijd'], $_GET['datum'], $_GET['context']);
                    }
                } else {
                    $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/sharedlib.js';
                    $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/nhalogging.js';
                    $this->DisplayNHALogging();
                }
            break;
            }
        }
    }

}