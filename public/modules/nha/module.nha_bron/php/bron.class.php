<?php
require "nha/module.nha1/php/_base.class.php";

class Bron extends NHA_BaseObject  {

    function Bron($owner, $databaseobject='') {
        $this->NHA_BaseObject($owner, $databaseobject);
    }
    
    function DoImportVanBron($params, &$error) {
        global $polaris;
        
        $_sp = $this->database->Prepare("
        BEGIN
            Bron.ImportExport.DoImportVanBron(:FORCEERIMPORT, :DEBUGLEVEL);
        END;
        ");

        $this->database->InParameter($_sp, $params['param2'], 'FORCEERIMPORT');
        $this->database->InParameter($_sp, $params['param1'], 'DEBUGLEVEL');
        try {
            $result = true;
            $this->database->Execute($_sp);
        } catch (Exception $e) {
            $result = false;
            $error = $polaris->getPrettyErrorMessage($e->errno, $e->errmsg);
        }
        return $result;
    }
}

?>