var Bron = {};

Bron = {
    exportResults: [],

    initialize: function() {
        var Self = Bron;

        Polaris.Dataview.sizeToFitCells($(".listview"));

        $("#batchnummer_select").change(function() {
            this.form.submit();
        });

        $("#btnAfgehandeld").click(Self.batchVerwerkt);
        Self.toggleAllesVerwerktButton();

        $("input[type=radio]").click(Self.signaalVerwerkt);

        if ($("#bronoverzicht").length > 0) {
            Self.bronOverzicht();
            Self.refreshOverzicht();
        }

        $(".btnNogmaalsDoorsturen").click(Self.nogmaalsDoorsturen);
    },
    toggleAllesVerwerktButton: function() {
        $("#btnAfgehandeld").toggleClass('disabled', $("input.signaalverwerkt_y").length == $("input.signaalverwerkt_y:checked").length);
    },
    nogmaalsDoorsturen: function(e) {
        e.preventDefault();

        var senddata = [];
        senddata.push({name: '_hdnFORMTYPE', value: 'nogmaalsdoorsturen'});
        senddata.push({name: '_hdnProcessedByModule', value: 'true'});
        var recordid = $(this).attr('name');
        senddata.push({name: 'SIGNAAL_ROWID', value: recordid});

        log(senddata);

        $input = $(this);

        Polaris.Ajax.postJSON(_servicequery, senddata, null, 'Wijziging opgeslagen', function (data) {
            $("#signaalverwerkt_y_"+recordid).attr('checked','checked');
            $input.attr('disabled', 'disabled');
        });
    },
    signaalVerwerkt: function(e) {
        var Self = Bron;

        var senddata = [];
        senddata.push({name: '_hdnFORMTYPE', value: 'signaalverwerken'});
        senddata.push({name: '_hdnProcessedByModule', value: 'true'});
        senddata.push({name: 'SIGNAAL_ROWID', value: $(this).parents("tr").find("input.recordid").val()});
        senddata.push({name: 'SIGNAAL_ONOFF', value: $(this).val()});

        log(senddata);

        Polaris.Ajax.postJSON(_servicequery, senddata, null, null, function (data) {
            Self.toggleAllesVerwerktButton();
        });
    },
    batchVerwerkt: function(e) {
        var Self = Bron;

        var senddata = [];
        e.preventDefault();
        senddata.push({name: '_hdnFORMTYPE', value: 'batchverwerken'});
        senddata.push({name: '_hdnProcessedByModule', value: 'true'});
        senddata.push({name: 'BATCHID', value: $("#batchnummer_select").val()});
        senddata.push({name: 'SIGNAAL_ONOFF', value: 'Y'});

        Polaris.Ajax.postJSON(_servicequery, senddata, null, null, function (data) {
            $("input.signaalverwerkt_y").attr('checked', 'checked');
            Self.toggleAllesVerwerktButton();
        });
    },
    bronOverzicht: function() {
        var queries = [
              'Aantal naar BRON gestuurd'
            , 'Aantal goedgekeurd'
            , 'Aantal afgekeurd'
            , 'Aantal nog te keuren'
            , ''
            , 'Nog te sturen (eerste keer)'
            , ' - Met BSN'
            , ' - Zonder BSN'
            , 'Nog te sturen (wijzigingen)'
            , ''
            , 'Inschrijving zonder Onderwijsovereenkomst'
        ];

        $("#bronoverzicht").empty();
        $(queries).each(function(i, elem) {
            if (elem) {
                elem = elem + ':<div><i class="fa fa-refresh"></i></div>';
            } else {
                elem = elem + '&nbsp;';
            }
            var $li = $('<li id="elem'+i+'">'+elem+'</li>');
            $("#bronoverzicht").append($li);
        });
    },
    refreshOverzicht: function(e){
        var Self = Bron;

        var senddata = [];
        var basesenddata = [];
        if (e) e.preventDefault();
        basesenddata.push({name: '_hdnFORMTYPE', value: 'bronoverzicht'});
        basesenddata.push({name: '_hdnProcessedByModule', value: 'true'});
        $("#bronoverzicht li:has(div)").each(function(i, elem) {
            senddata = basesenddata.slice(0); // copy in stead of reference
            senddata.push({name: 'query', value: 'elem_'+i});

            $("div i", elem).addClass('fa-spin');
            Polaris.Ajax.postJSON(_servicequery, senddata, null, null, function (data) {
                $("div", elem).text(data.result.count).removeClass('working').addClass('ready');
            });
        });
    }
};

$(document).ready(function(){
    var Self = Bron;
    Self.initialize();

});
