var BronGBA = {};

BronGBA = {
    gbaResults: [],
    currentIndex: 0,
    fields: ['RELATIENR', 'GEBDATUM', 'DATUMINGANGADRES', 'BSN', 'ONDERWIJSNUMMER', 'POSTCODE', 'VOORLETTERS', 'VOORNAMEN', 'TUSSENVOEGSEL', 'ACHTERNAAM', 'STRAAT', 'HUISNUMMER', 'HUISNUMMERTOEVOEGING', 'PLAATS'],

    initialize: function() {
        var Self = BronGBA;

        $("td.gbafield").click(function() {
            Self.toggleCheckbox($(this).parents('tr').find(':checkbox'));
        });
        $(":checkbox").click(Self.syncCheckbox);

        $("#btnRefresh").click(function(e) {
            e.preventDefault();
            Self.getGBAgegevens();
        });
        $("#btnAccept").click(function(e) {
            if (!$(this).hasClass('disabled')) {
                Self.acceptForm(e);
            }
        });
        $("#btnPrev").click(Self.prevRelatie);
        $("#btnNext").click(Self.nextRelatie);

        $(document).keydown(function(e) {
            if (e.which == 37) {
                Self.prevRelatie(e);
            }
            if (e.which == 39) {
                Self.nextRelatie(e);
            }
            if (e.which == 13) {
                if (confirm('Weet u zeker dat de GBA gegevens wilt overnemen?')) {
                    Self.acceptForm(e);
                }
            }
        });

        Self.getGBAgegevens();
    },
    toggleCheckbox: function($checkbox, on) {
        var Self = BronGBA;

//        if ($checkbox.attr('disabled')) return;

        if (typeof on == 'undefined') on = !$checkbox.is(":checked");
        if (on) {
            $checkbox.prop('checked', true);
            $("#btnAccept").removeClass('disabled');
        } else {
            $checkbox.prop('checked', false);
        }
        Self.syncCheckbox($checkbox);
    },
    syncCheckbox: function(e) {
        var elem = e.target;
        if (typeof e.target == 'undefined') {
            elem = e;
        }
        $(elem).parents('tr').find('td.gbafield').toggleClass('active', $(elem).is(":checked"));
    },
    acceptForm: function(e) {
        var Self = BronGBA;

        e.preventDefault();

        $("#btnAccept").addClass('disabled');

        if (typeof Self.gbaResults[Self.currentIndex].REL_RELATIENR == 'undefined') return;

        var senddata = [];
        senddata.push({name: '_hdnFORMTYPE', value: 'gbagegevensverwerken'});
        senddata.push({name: '_hdnProcessedByModule', value: 'true'});
        senddata.push({name: '_hdnCurrentIndex', value: Self.currentIndex});
        senddata.push({name: '_hdnRelatieNr', value: Self.gbaResults[Self.currentIndex].REL_RELATIENR});
        senddata.push({name: '_hdnBatchNummerAanleverbestand', value: Self.gbaResults[Self.currentIndex].BATCHNUMMER_AANLEVERBESTAND});
        senddata.push({name: '_hdnMeldingNr', value: Self.gbaResults[Self.currentIndex].MELDINGNUMMER});

        $(Self.fields).each(function(i, elem) {
            if ($("#CHK_"+elem).is(":checked")) {
                senddata.push({name: elem, value: Self.gbaResults[Self.currentIndex]['GBA_'+elem]});
            }
        });

        jQuery.postJSON(_servicequery, senddata, function(data, textStatus) {
            Polaris.Base.closeModalMessage();
            $("#btnAccept").removeClass('disabled');
            if (textStatus == 'success') {
                log('GBA gegevens verwerkt');
                if (data.error === false) {
                    Self.updateChanges(data.result);
                } else {
                    Polaris.Base.errorDialog(data.error, data.detailed_error);
                }
            } else {
                Macs.SharedLib.ajaxFeedback('Fout opgetreden bij JSON post.');
            }
        });
    },
    updateChanges: function(data) {
        var Self = BronGBA;

        Self.gbaResults[data._hdnCurrentIndex].GBAOVERGEZET = true;

        for (var k in data) {
            if (k.substr(0,4) != '_hdn') {
                Self.gbaResults[data._hdnCurrentIndex]['REL_'+k] = data[k];
            }
        }

        if (data._hdnCurrentIndex == Self.currentIndex)
            Self.displayRelatie(data._hdnCurrentIndex);
    },
    gotoRelatie: function(direction) {
        var Self = BronGBA;

        Self.currentIndex += direction;
        if (Self.currentIndex > Self.gbaResults.length - 1) {
            Self.currentIndex = 0;
        }
        if (Self.currentIndex < 0) {
            Self.currentIndex = Self.gbaResults.length - 1;
        }
        Self.displayRelatie(Self.currentIndex);
        // update counter label
        $("#counter").text((Self.currentIndex + 1) + ' van ' + Self.gbaResults.length);
    },
    prevRelatie: function(e) {
        e.preventDefault();

        BronGBA.gotoRelatie(-1);
    },
    nextRelatie: function(e) {
        e.preventDefault();

        BronGBA.gotoRelatie(1);
    },
    getGBAgegevens: function() {
        var Self = BronGBA;
        var senddata = [];

        $("#btnRefresh i").addClass('fa-spin');
        var fetchKlaar = function() {
            $("#btnRefresh i").removeClass('fa-spin');
        };

        jQuery.getJSON(_servicequery, {'func': 'gbagegevens'}, function(data, textStatus) {
            fetchKlaar();
            if (textStatus == 'success') {
                if (data.error === false) {
                    Self.currentIndex = 0;
                    Self.gbaResults = data.result;
                    Self.refreshGBAform(Self.gbaResults);
                } else {
                    Polaris.Base.errorDialog(data.error, data.detailed_error);
                }
            } else {
                Macs.SharedLib.ajaxFeedback('Fout opgetreden bij JSON get: BronGBA.getGBAgegevens');
            }
        });

    },
    displayRelatie: function(id) {
        var Self = BronGBA;

        var d = Self.gbaResults;

        if (typeof d[id].REL_RELATIENR == 'undefined') return;

        $("input[name=RELATIENR]").val(d[id].REL_RELATIENR);
        $("input[name=MELDINGNUMMER]").val(d[id].MELDINGNUMMER);
        $("input[name=BATCHNUMMER_AANLEVERBESTAND]").val(d[id].BATCHNUMMER_AANLEVERBESTAND);

        var link = _serverroot+'/app/macs/const/relatie4/edit/'+Polaris.Base.customUrlEncode(d[id].ROW_ID)+'/?_hdnPlrSes=newsession&autopin=true';
        $("#RELATIENR").html('<a href="'+link+'" target="_blank">'+d[id].REL_RELATIENR+'</a>');
        $("#DATUMINGANGADRES").html('Adres ingegaan op '+d[id].DATUMINGANGADRES);

        $(Self.fields).each(function(i, elem) {
            var rel_value = d[id]['REL_'+elem]||'';
            var gba_value = d[id]['GBA_'+elem]||'';
            var $checkbox = $("#CHK_"+elem);

            $("#REL_"+elem).text(rel_value);
            $("#GBA_"+elem).text(gba_value);

            if (!gba_value) {
                $checkbox.prop('disabled', true);
                Self.toggleCheckbox($checkbox, false);
            } else {
                $checkbox.prop('disabled', false);
                Self.toggleCheckbox($checkbox, (rel_value != gba_value));
            }
        });

        $("#btnAccept").toggleClass('disabled', Self.gbaResults[id].GBAOVERGEZET == 'J');
    },
    refreshGBAform: function(data) {
        var Self = BronGBA;

        Self.gotoRelatie(Self.currentIndex);
    }
};

$(document).ready(function(){
    var Self = BronGBA;
    Self.initialize();
});
