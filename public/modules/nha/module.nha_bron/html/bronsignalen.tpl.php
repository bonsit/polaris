{include file="shared.tpl.php"}

Aanleverbestand: <select id="batchnummer_select" name="batchnummer_select">
{section name=i loop=$batchnummers}
<option {if $batchnummers[i].BATCHID == $current_batchnummer}selected="selected"{/if} value="{$batchnummers[i].BATCHID}">
    {$batchnummers[i].BATCHID} - Ontvangst {$batchnummers[i].DATUMONTVANGST} (Niet verwerkt: {$batchnummers[i].NIET_VERWERKT})
</option>
{/section}
</select> <span class="buttons small" style="margin-left:20px"><a href="#" id="btnAfgehandeld" class="positive">Alles verwerkt</a></span>

{section name=i loop=$batchnummers}
{if $batchnummers[i].BATCHID == $current_batchnummer}
<div id="batchinfo">
    <table>
        <tr>
            <td width="50%">Aantal meldingen: {$batchnummers[i].AANTALMELDINGEN}</td>
            <td {if $batchnummers[i].CUM_AANTAL_FOUTE_MELDINGEN > 0}style="color:red"{/if}>Cummulatief aantal foute meldingen: {$batchnummers[i].CUM_AANTAL_FOUTE_MELDINGEN}</td>
        </tr>
        <tr>
            <td>Resterend aantal meldingen: {$batchnummers[i].RESTEREND_AANTAL_MELDINGEN}</td>
            <td {if $batchnummers[i].CUM_AANTAL_GOEDE_MELDINGEN > 0}style="color:green"{/if}>Cummulatief aantal goede meldingen: {$batchnummers[i].CUM_AANTAL_GOEDE_MELDINGEN}</td>
        </tr>
    </table>
</div>
{/if}
{/section}

<div class="headerscroll scrollarea">
    <table id="dataheaderview" class="data striped group-table right"  cellspacing="0" cellpadding="0">
        <thead>
        <tr>
            <th>Verwerkt?&nbsp;</th>
            <th class="right">Meldingnr&nbsp;</th>
            <th>Omschrijving&nbsp;</th>
            <th>Ernst&nbsp;</th>
            <th>Inschrijfnr&nbsp;</th>
            <th>Relatienr&nbsp;</th>
            <th>Oplossing&nbsp;</th>
            <th>Inschrijving volgnummer&nbsp;</th>
            <th>Startdatum&nbsp;</th>
            <th>BPV Volgnummer&nbsp;</th>
            <th>Recordsoort&nbsp;</th>
            <th>Actie&nbsp;</th>
        <tr>
        </thead>
    </table>
</div>

<div class="bodyscroll scrollarea">
    <table id="datalistview" class="data striped group-table right">
        {section name=i loop=$signalen}
            <tr class="ernst_{$signalen[i].ERNST}">
                <td>
                    <input type="radio" id="signaalverwerkt_y_{$signalen[i].SIGNAAL_ROWID}" class="signaalverwerkt_y" name="signaalverwerkt_{$signalen[i].SIGNAAL_ROWID}" value="Y" {if $signalen[i].VERWERKT_YN == 'Y'}checked="checked"{/if} /> Ja &nbsp;
                    <input type="radio" id="signaalverwerkt_i_{$signalen[i].SIGNAAL_ROWID}" class="signaalverwerkt_i" name="signaalverwerkt_{$signalen[i].SIGNAAL_ROWID}" value="I" {if $signalen[i].VERWERKT_YN == 'I'}checked="checked"{/if} /> In behandeling &nbsp;
                    <input type="radio" id="signaalverwerkt_n_{$signalen[i].SIGNAAL_ROWID}" class="signaalverwerkt_n" name="signaalverwerkt_{$signalen[i].SIGNAAL_ROWID}" value="N" {if $signalen[i].VERWERKT_YN == 'N'}checked="checked"{/if} /> Nee
                </td>
                <td><input type="hidden" class="recordid" name="SIGNAAL_RECORDID_{$signalen[i].SIGNAAL_ROWID}" value="{$signalen[i].SIGNAAL_ROWID}" /> {if $signalen[i.index_prev].MELDINGNUMMER != $signalen[i].MELDINGNUMMER}{$signalen[i].MELDINGNUMMER}{/if}</td>
                <td>({$signalen[i].SIGNAALCODE}) {$signalen[i].OMSCHRIJVINGSIGNAAL|htmlentities}</td>
                <td>{if $signalen[i].ERNST == 'S'}Signaal{else}Afkeur{/if}</td>
                {if $signalen[i].INSCHRIJFNR == -1}
                <td></td>
                {else}
                <td><a target="_blank" href="{$serverroot}/app/macs/const/inschrijving/edit/{$signalen[i].INSCHRIJVING_ROWID}/?_hdnPlrSes=newsession&autopin=true">{$signalen[i].INSCHRIJFNR}</a></td>
                {/if}
                <td><a target="_blank" href="{$serverroot}/app/macs/const/relatie4/edit/{$signalen[i].RELATIE_ROWID}/?_hdnPlrSes=newsession&autopin=true">{$signalen[i].RELATIENR}</a></td>
                <td class="hoverpop"><input type="hidden" value="{$signalen[i].OPLOSSING|default:'Geen oplossing bekend.'|htmlentities}"> <img src="modules/nha/module.nha_macs/html/info.png"> </td>
                <td>{$signalen[i].RI_INSCHRIJVINGVOLGNUMMER}</td>
                <td>{$signalen[i].RI_STARTDATUM}</td>
                <td>{$signalen[i].RI_BPVVOLGNUMMER}</td>
                <td>{$signalen[i].RECORDSOORT_TERUGKOPPEL}</td>
                <td>
                    <input type="button" id="signaalverwerkt_x_{$signalen[i].SIGNAAL_ROWID}" class="btnNogmaalsDoorsturen" {if $signalen[i].REL_WIJZIGINGNAEXPORT == 'J'}disabled="disabled"{/if} name="{$signalen[i].SIGNAAL_ROWID}" value="Nogmaals doorsturen" />
                </td>
            </tr>
        {/section}
    </table>
</div>