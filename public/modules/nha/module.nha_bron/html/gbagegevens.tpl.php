{include file="shared.tpl.php"}
<div id="gbaframe">
    <div id="counter"></div>
    <table id="gbatabel" style="width:700px;">
    <thead>
    <tr><th style="width:100px"></th><th style="width:250px">Macs gegevens</th><th style="width:250px">GBA gegevens</th><th>Overzetten?</th></tr>
    </thead>
    <tr><td>Relatie</td>
        <td id="RELATIENR"></td><td></td><td></td></tr>
    <tr><td>BSN</td>
        <td id="REL_BSN"></td><td id="GBA_BSN" class="gbafield"></td><td><input type="checkbox" id="CHK_BSN" /></td></tr>
    <tr><td>Onderwijsnr</td>
        <td id="REL_ONDERWIJSNUMMER"></td><td id="GBA_ONDERWIJSNUMMER" class="gbafield"></td><td><input type="checkbox" id="CHK_ONDERWIJSNUMMER" /></td></tr>
    <tr><td>Geboortedatum</td>
        <td id="REL_GEBDATUM"></td><td id="GBA_GEBDATUM" class="gbafield"></td><td><input type="checkbox" id="CHK_GEBDATUM" /></td></tr>
    <tr><td>Voorletters</td>
        <td id="REL_VOORLETTERS"></td><td id="GBA_VOORLETTERS" class="gbafield"></td><td><input type="checkbox" id="CHK_VOORLETTERS" /></td></tr>
    <tr><td>Voornamen</td>
        <td id="REL_VOORNAMEN"></td><td id="GBA_VOORNAMEN" class="gbafield"></td><td><input type="checkbox" id="CHK_VOORNAMEN" /></td></tr>
    <tr><td>Voorvoegsels</td>
        <td id="REL_TUSSENVOEGSEL"></td><td id="GBA_TUSSENVOEGSEL" class="gbafield"></td><td><input type="checkbox" id="CHK_TUSSENVOEGSEL" /></td></tr>
    <tr><td>Achternaam</td>
        <td id="REL_ACHTERNAAM"></td><td id="GBA_ACHTERNAAM" class="gbafield"></td><td><input type="checkbox" id="CHK_ACHTERNAAM" /></td></tr>
    <tr><th>Adres</td><th></td><td id="DATUMINGANGADRES"></td></tr>
    <tr><td>Straatnaam</td>
        <td id="REL_STRAAT"></td><td id="GBA_STRAAT" class="gbafield"></td><td><input type="checkbox" id="CHK_STRAAT" /></td></tr>
    <tr><td>Huisnummer</td>
        <td id="REL_HUISNUMMER"></td><td id="GBA_HUISNUMMER" class="gbafield"></td><td><input type="checkbox" id="CHK_HUISNUMMER" /></td></tr>
    <tr><td>Huisnr-toev</td>
        <td id="REL_HUISNUMMERTOEVOEGING"></td><td id="GBA_HUISNUMMERTOEVOEGING" class="gbafield"></td><td><input type="checkbox" id="CHK_HUISNUMMERTOEVOEGING" /></td></tr>
    <tr><td>Postcode</td>
        <td id="REL_POSTCODE"></td><td id="GBA_POSTCODE" class="gbafield"></td><td><input type="checkbox" id="CHK_POSTCODE" /></td></tr>
    <tr><td>Plaatsnaam</td>
        <td id="REL_PLAATS"></td><td id="GBA_PLAATS" class="gbafield"></td><td><input type="checkbox" id="CHK_PLAATS" /></td></tr>
    </table>
</div>

<input type="hidden" name="RELATIENR" value="" />
<input type="hidden" name="MELDINGNUMMER" value="" />
<input type="hidden" name="BATCHNUMMER_AANLEVERBESTAND" value="" />

<div class="buttons">
    <button id="btnRefresh" class="secondary"><i class="fa fa-refresh fa-fw"></i> Vernieuwen</button>
    <button id="btnPrev"><i class="fa fa-chevron-left fa-fw"></i> Vorige</button>
    <button id="btnNext">Volgende <i class="fa fa-chevron-right fa-fw"></i></button>
    <button id="btnAccept" class="positive" accesskey="F9">Accepteren</button>
</div>