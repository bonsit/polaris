<?php
require_once('_shared/module._base.php');

class Module extends _BaseModule {
    var $domains;
    var $domainid;

    function Module($moduleid, $module) {
        global $_GVARS;

        parent::_BaseModule($moduleid, $module);

        $this->processed = false;
    }

    function Process() {
        global $_GVARS;

    }

    function x_week_range($date) {
//        $ts = strtotime($date);
        $ts = $date;
        $start = (date('w', $ts) == 0) ? $ts : strtotime('last sunday', $ts);
        return array($start,
                     strtotime('next saturday', $start));
    }

    function dagVorigJaar($aantaljarenterug, $datum=false) {
        if (!$datum)
            $datum = strtotime('today');
        $dagnr = date('N', $datum);
        $xjaargeleden = strtotime("-$aantaljarenterug year");
        for($i=0;$i<7;$i++) {
            $idatum = strtotime("+$i days", $xjaargeleden);
            $idag = date('N', $idatum);
            if ($idag == $dagnr) {
                $datumplus = strtotime("+$i days", $xjaargeleden);
                $deltaplus = $i;
                break;
            }
        }
        for($i=0;$i<7;$i++) {
            $idatum = strtotime("-$i days", $xjaargeleden);
            $idag = date('N', $idatum);
            if ($idag == $dagnr) {
                $datumminus = strtotime("-$i days", $xjaargeleden);
                $deltaminus = $i;
                break;
            }
        }

        if ($deltaminus > $deltaplus) {
            $dedatum = $datumplus;
        } else {
            $dedatum = $datumminus;
        }

        return $dedatum;
    }

    function BepaalVakantie($land, $datum) {
        $_cachetimeout = 0 * 60; // 5 minuten

        $_sql = "
        SELECT LISTAGG(V.VAKANTIEFEESTDAGEN, '<br/>') WITHIN GROUP (ORDER BY V.DATUM_TM)
        FROM VAKANTIEFEESTDAGEN V
        WHERE V.LAND = :LAND
        AND V.DATUM_VANAF <= TO_DATE(:DATUM, 'DD-MM-YYYY')
        AND V.DATUM_TM >= TO_DATE(:DATUM, 'DD-MM-YYYY')
        ";
        $vakanties = $this->form->database->userdb->CacheGetOne($_cachetimeout, $_sql, array("LAND" => $land, "DATUM" => $datum));
        return $vakanties;
    }

    function GetStats() {
        global $polaris;

        $_cachetimeout = 5 * 60; // 5 minuten
        $_cachetimeoutLong = 30 * 60 * 60 * 24; // 1 maand

        $nettedagweergave = array(1=>'Maandag', 2=>'Dinsdag', 3=>'Woensdag', 4=>'Donderdag', 5=>'Vrijdag', 6=>'Zaterdag', 7=>'Zondag');

        $vandaag = date("d/m/Y");
        $ditjaar = date("Y");
        $weeknr = date("W");
        $dagnr = date("N");
        $vandaag2jaargeleden = date('d-m-Y', $this->dagVorigJaar(2));
        $vandaag1jaargeleden = date('d-m-Y', $this->dagVorigJaar(1));

        // 1378548, 1379887 zijn proefrelaties
        $_sql = "SELECT COUNT(*) FROM INSCHRIJVING_NETTO I WHERE ";

        $_NLsql = $_sql . " I.LANDCODE <> 'BE' AND I.LANDCODE <> 'DE' ";
        $_BEsql = $_sql . " I.LANDCODE = 'BE'";
        $_DEsql = $_sql . " I.LANDCODE = 'DE'";

        $NL_aantalinschrijving_tweejaargeleden = array(
            "aantal" => $this->form->database->userdb->CacheGetOne($_cachetimeoutLong, $_NLsql."
                        AND DATUMINSCHRIJVING = TO_DATE(:dag,'DD-MM-YYYY')
                        ", array("dag"=>$vandaag2jaargeleden))
            ,"vakantie" => $this->BepaalVakantie('NL', $vandaag2jaargeleden)
        );

        $NL_aantalinschrijving_eenjaargeleden = array(
            "aantal" => $this->form->database->userdb->CacheGetOne($_cachetimeoutLong, $_NLsql."
                        AND DATUMINSCHRIJVING = TO_DATE(:dag,'DD-MM-YYYY')
                        ", array("dag"=>$vandaag1jaargeleden))
            ,"vakantie" => $this->BepaalVakantie('NL', $vandaag1jaargeleden)
        );

        $NL_aantalinschrijving_vandaag = array(
            "aantal" => $this->form->database->userdb->CacheGetOne($_cachetimeout, $_NLsql."
                        AND DATUMINSCHRIJVING = TRUNC(SYSDATE)
                        AND I.OPZEGGINGSDATUM IS NULL
                        AND I.DATUMGERETOURNEERD IS NULL
                        ")
            ,"vakantie" => $this->BepaalVakantie('NL', date('d-m-Y'))
        );

        $NL_aantalinschrijving_tweejaargeleden_totnu = $this->form->database->userdb->CacheGetOne($_cachetimeout, $_NLsql."
                        AND DATUMINSCHRIJVING = TO_DATE(:dag,'DD-MM-YYYY')
                        AND (CREATEDATE - TRUNC(CREATEDATE)) <= (SYSDATE - TRUNC(SYSDATE))
                        ", array("dag"=>$vandaag2jaargeleden));

        $NL_aantalinschrijving_eenjaargeleden_totnu = $this->form->database->userdb->CacheGetOne($_cachetimeout, $_NLsql."
                        AND DATUMINSCHRIJVING = TO_DATE(:dag,'DD-MM-YYYY')
                        AND (CREATEDATE - TRUNC(CREATEDATE)) <= (SYSDATE - TRUNC(SYSDATE))
                        ", array("dag"=>$vandaag1jaargeleden));

        $NL_aantalinschrijving_laatsteuur = array(
            "aantal" => $this->form->database->userdb->CacheGetOne($_cachetimeout, "
                        SELECT COUNT(*) FROM INSCHRIJVING I WHERE CREATEDATE >= SYSDATE - 1/24 AND I.LANDCODE <> 'BE'
                        ")
            ,"vakantie" => ""
        );

        $BE_aantalinschrijving_tweejaargeleden = array(
            "aantal" => $this->form->database->userdb->CacheGetOne($_cachetimeoutLong, $_BEsql."
                        AND DATUMINSCHRIJVING = TO_DATE(:dag,'DD-MM-YYYY')
                        ", array("dag"=>$vandaag2jaargeleden))
            ,"vakantie" => $this->BepaalVakantie('BE', $vandaag2jaargeleden)
        );

        $BE_aantalinschrijving_eenjaargeleden = array(
            "aantal" => $this->form->database->userdb->CacheGetOne($_cachetimeoutLong, $_BEsql."
                        AND DATUMINSCHRIJVING = TO_DATE(:dag,'DD-MM-YYYY')
                        ", array("dag"=>$vandaag1jaargeleden))
            ,"vakantie" => $this->BepaalVakantie('BE', $vandaag1jaargeleden)
        );

        $BE_aantalinschrijving_vandaag = array(
            "aantal" => $this->form->database->userdb->CacheGetOne($_cachetimeout, $_BEsql."
                        AND DATUMINSCHRIJVING = TRUNC(SYSDATE)
                        AND I.OPZEGGINGSDATUM IS NULL
                        AND I.DATUMGERETOURNEERD IS NULL
                        ")
            ,"vakantie" => $this->BepaalVakantie('BE', date('d-m-Y'))
        );

        $BE_aantalinschrijving_laatsteuur = array(
            "aantal" => $this->form->database->userdb->CacheGetOne($_cachetimeout, "
                        SELECT COUNT(*) FROM INSCHRIJVING I WHERE CREATEDATE >= SYSDATE - 1/24 AND I.LANDCODE <> 'NL'
                        ")
            ,"vakantie" => ""
        );

        $DE_aantalinschrijving_tweejaargeleden = array(
            "aantal" => $this->form->database->userdb->CacheGetOne($_cachetimeoutLong, $_DEsql."
                        AND DATUMINSCHRIJVING = TO_DATE(:dag,'DD-MM-YYYY')
                        ", array("dag"=>$vandaag2jaargeleden))
            ,"vakantie" => $this->BepaalVakantie('DE', $vandaag2jaargeleden)
        );

        $DE_aantalinschrijving_eenjaargeleden = array(
            "aantal" => $this->form->database->userdb->CacheGetOne($_cachetimeoutLong, $_DEsql."
                        AND DATUMINSCHRIJVING = TO_DATE(:dag,'DD-MM-YYYY')
                        ", array("dag"=>$vandaag1jaargeleden))
            ,"vakantie" => $this->BepaalVakantie('DE', $vandaag1jaargeleden)
        );

        $DE_aantalinschrijving_vandaag = array(
            "aantal" => $this->form->database->userdb->CacheGetOne($_cachetimeout, $_DEsql."
                        AND DATUMINSCHRIJVING = TRUNC(SYSDATE)
                        AND I.OPZEGGINGSDATUM IS NULL
                        AND I.DATUMGERETOURNEERD IS NULL
                        ")
            ,"vakantie" => $this->BepaalVakantie('DE', date('d-m-Y'))
        );

        $DE_aantalinschrijving_laatsteuur = array(
            "aantal" => $this->form->database->userdb->CacheGetOne($_cachetimeout, "
                        SELECT COUNT(*) FROM INSCHRIJVING I WHERE CREATEDATE >= SYSDATE - 1/24 AND I.LANDCODE = 'DE'
                        ")
            ,"vakantie" => ""
        );

        $_cachetimeout2 = 60 * 60 * 8; // 8 uur
        $dezedag = strtotime("-1 days");

        for($day = 1;$day <= 5;$day++) {
            $NLaantalinschrijving_ditjaar[$day] = array(
                "aantal" => $this->form->database->userdb->CacheGetOne($_cachetimeout2, $_NLsql." AND DATUMINSCHRIJVING = TRUNC(SYSDATE) - :dag", array("dag"=>$day))
               ,"label" => $nettedagweergave[date('N', $dezedag)]
               ,"vakantie" => $this->BepaalVakantie('NL', date('d-m-Y', $dezedag))
            );

            $BEaantalinschrijving_ditjaar[$day] = array(
                "aantal" => $this->form->database->userdb->CacheGetOne($_cachetimeout2, $_BEsql." AND DATUMINSCHRIJVING = TRUNC(SYSDATE) - :dag", array("dag"=>$day))
               ,"label" => $nettedagweergave[date('N', $dezedag)]
               ,"vakantie" => $this->BepaalVakantie('BE', date('d-m-Y', $dezedag))
            );

            $DEaantalinschrijving_ditjaar[$day] = array(
                "aantal" => $this->form->database->userdb->CacheGetOne($_cachetimeout2, $_DEsql." AND DATUMINSCHRIJVING = TRUNC(SYSDATE) - :dag", array("dag"=>$day))
               ,"label" => $nettedagweergave[date('N', $dezedag)]
               ,"vakantie" => $this->BepaalVakantie('DE', date('d-m-Y', $dezedag))
            );

            $dezedag = strtotime("-1 days", $dezedag);
        }

        $dag1jaargeleden = $this->dagVorigJaar(1, strtotime("-1 days"));
        for($day = 1;$day <= 5;$day++) {
            $dagen1jaargeleden[$day] = date('d-m-Y', $dag1jaargeleden);
            $NLaantalinschrijving_dag_eenjaargeleden[$day] = array(
                "aantal" => $this->form->database->userdb->CacheGetOne($_cachetimeoutLong, $_NLsql." AND DATUMINSCHRIJVING = TO_DATE(:dag,'DD-MM-YYYY')", array("dag"=>$dagen1jaargeleden[$day]))
               ,"label" => date('j M', $dag1jaargeleden)
               ,"vakantie" => $this->BepaalVakantie('NL', date('d-m-Y', $dag1jaargeleden))
            );

            $BEaantalinschrijving_dag_eenjaargeleden[$day] = array(
                "aantal" => $this->form->database->userdb->CacheGetOne($_cachetimeoutLong, $_BEsql." AND DATUMINSCHRIJVING = TO_DATE(:dag,'DD-MM-YYYY')", array("dag"=>$dagen1jaargeleden[$day]))
               ,"label" => date('j M', $dag1jaargeleden)
               ,"vakantie" => $this->BepaalVakantie('BE', date('d-m-Y', $dag1jaargeleden))
            );

            $DEaantalinschrijving_dag_eenjaargeleden[$day] = array(
                "aantal" => $this->form->database->userdb->CacheGetOne($_cachetimeoutLong, $_DEsql." AND DATUMINSCHRIJVING = TO_DATE(:dag,'DD-MM-YYYY')", array("dag"=>$dagen1jaargeleden[$day]))
               ,"label" => date('j M', $dag1jaargeleden)
               ,"vakantie" => $this->BepaalVakantie('DE', date('d-m-Y', $dag1jaargeleden))
            );

            $dag1jaargeleden = strtotime('-1 days', $dag1jaargeleden);
        }

        $dag2jaargeleden = $this->dagVorigJaar(2, strtotime("-1 days"));
        for($day = 1;$day <= 5;$day++) {
            $dagen2jaargeleden[$day] = date('d-m-Y', $dag2jaargeleden);
            $NLaantalinschrijving_dag_tweejaargeleden[$day] = array(
                "aantal" => $this->form->database->userdb->CacheGetOne($_cachetimeoutLong, $_NLsql." AND DATUMINSCHRIJVING = TO_DATE(:dag,'DD-MM-YYYY')", array("dag"=>$dagen2jaargeleden[$day]))
               ,"label" => date('j M', $dag2jaargeleden)
               ,"vakantie" => $this->BepaalVakantie('NL', date('d-m-Y', $dag2jaargeleden))
            );

            $BEaantalinschrijving_dag_tweejaargeleden[$day] = array(
                "aantal" => $this->form->database->userdb->CacheGetOne($_cachetimeoutLong, $_BEsql." AND DATUMINSCHRIJVING = TO_DATE(:dag,'DD-MM-YYYY')", array("dag"=>$dagen2jaargeleden[$day]))
               ,"label" => date('j M', $dag2jaargeleden)
               ,"vakantie" => $this->BepaalVakantie('BE', date('d-m-Y', $dag2jaargeleden))
            );

            $DEaantalinschrijving_dag_tweejaargeleden[$day] = array(
                "aantal" => $this->form->database->userdb->CacheGetOne($_cachetimeoutLong, $_DEsql." AND DATUMINSCHRIJVING = TO_DATE(:dag,'DD-MM-YYYY')", array("dag"=>$dagen2jaargeleden[$day]))
               ,"label" => date('j M', $dag2jaargeleden)
               ,"vakantie" => $this->BepaalVakantie('DE', date('d-m-Y', $dag2jaargeleden))
            );

            $dag2jaargeleden = strtotime('-1 days', $dag2jaargeleden);
        }

        $actieve_gebruikers = $polaris->instance->CacheGetOne($_cachetimeout, "select count(distinct usergroupname)
        FROM plr_session2 s LEFT JOIN plr_usergroup u
        ON substr(sessdata FROM LOCATE('%22',sessdata)+ 3 FOR LOCATE('%22',sessdata, LOCATE('%22',sessdata)+ 4)-LOCATE('%22',sessdata) - 3) = u.usergroupname
        WHERE DATE_FORMAT(modified, '%Y-%m-%d') = DATE_FORMAT(now(), '%Y-%m-%d')
        ORDER BY 1");
        $tweejaargeleden_totnu = intval($NL_aantalinschrijving_tweejaargeleden_totnu);
        $eenjaargeleden_totnu = intval($NL_aantalinschrijving_eenjaargeleden_totnu);
        $vandaag_totnu = intval($NL_aantalinschrijving_vandaag['aantal']);
        $gemiddeld_totnu = (($tweejaargeleden_totnu + $eenjaargeleden_totnu) / 2);
        //$gemiddeld_totnu = $tweejaargeleden_totnu;
        if ($gemiddeld_totnu == 0) {
            $score_nl_vandaag = 0;
        } else {
            $score_nl_vandaag = 100 - (($vandaag_totnu * 100) / $gemiddeld_totnu);
        }
        $score_nl_vandaag = number_format($score_nl_vandaag, 2);

        $result = array(
            'aantal_nl_inschrijvingen_tweejaargeleden'=>$NL_aantalinschrijving_tweejaargeleden,
            'aantal_nl_inschrijvingen_eenjaargeleden'=>$NL_aantalinschrijving_eenjaargeleden,
            'aantal_nl_inschrijvingen_vandaag'=>$NL_aantalinschrijving_vandaag,
            'aantal_nl_inschrijvingen_laatsteuur'=>$NL_aantalinschrijving_laatsteuur,
            'score_nl_vandaag'=>$gemiddeld_totnu,
//            'aantal_nl_inschrijvingen_tweejaargeleden_totnu'=>$NL_aantalinschrijving_tweejaargeleden_totnu,
//            'aantal_nl_inschrijvingen_eenjaargeleden_totnu'=>$NL_aantalinschrijving_eenjaargeleden_totnu,

            'aantal_be_inschrijvingen_tweejaargeleden'=>$BE_aantalinschrijving_tweejaargeleden,
            'aantal_be_inschrijvingen_eenjaargeleden'=>$BE_aantalinschrijving_eenjaargeleden,
            'aantal_be_inschrijvingen_vandaag'=>$BE_aantalinschrijving_vandaag,
            'aantal_be_inschrijvingen_laatsteuur'=>$BE_aantalinschrijving_laatsteuur,

            'aantal_de_inschrijvingen_tweejaargeleden'=>$DE_aantalinschrijving_tweejaargeleden,
            'aantal_de_inschrijvingen_eenjaargeleden'=>$DE_aantalinschrijving_eenjaargeleden,
            'aantal_de_inschrijvingen_vandaag'=>$DE_aantalinschrijving_vandaag,
            'aantal_de_inschrijvingen_laatsteuur'=>$DE_aantalinschrijving_laatsteuur,

            'aantal_actieve_gebruikers'=>$actieve_gebruikers,

            'aantal_inschrijvingen_ditjaarNL'=>$NLaantalinschrijving_ditjaar,
            'aantal_inschrijvingen_1jaarNL'=>$NLaantalinschrijving_dag_eenjaargeleden,
            'aantal_inschrijvingen_2jaarNL'=>$NLaantalinschrijving_dag_tweejaargeleden,
            'aantal_inschrijvingen_ditjaarBE'=>$BEaantalinschrijving_ditjaar,
            'aantal_inschrijvingen_1jaarBE'=>$BEaantalinschrijving_dag_eenjaargeleden,
            'aantal_inschrijvingen_2jaarBE'=>$BEaantalinschrijving_dag_tweejaargeleden,
            'aantal_inschrijvingen_ditjaarDE'=>$DEaantalinschrijving_ditjaar,
            'aantal_inschrijvingen_1jaarDE'=>$DEaantalinschrijving_dag_eenjaargeleden,
            'aantal_inschrijvingen_2jaarDE'=>$DEaantalinschrijving_dag_tweejaargeleden
        );

        echo json_encode($result);
    }

    function GetGeoStats($minutes, $test=false) {
        $this->form->database->userdb->SetFetchMode(ADODB_FETCH_ASSOC);
        if ($test) {
            $_aantal = $this->form->database->userdb->GetAssoc("
            SELECT G.POSTCODE, TO_CHAR(SYSDATE, 'DD-MM-YYYY HH24:II:SS') AS CREATEDATE, MAX(LATITUDE) AS LATITUDE, MAX(LONGITUDE) AS LONGITUDE
                FROM GEOCODES G
                WHERE G.POSTCODE IN (
                '1019','2211','3211','5670','3411','2491','5121','5663','6235','6228',
                '1020','2212','3212','5671','3412','2492','5122','5664','6236','6229',
                '2020','2114','3222','5674','3413','2493','5109','5464','6111','5111'
                )
                GROUP BY G.POSTCODE
                ORDER BY 2
            ");
        } else {
            if ($minutes == NULL) $minutes = 5;
            $_aantal = $this->form->database->userdb->GetAssoc("
                SELECT R.POSTCODE, MAX(TO_CHAR(I.CREATEDATE, 'DD-MM-YYYY HH24:II:SS')) AS CREATEDATE, MAX(LATITUDE) AS LATITUDE, MAX(LONGITUDE) AS LONGITUDE
                FROM INSCHRIJVING I, RELATIE R, GEOCODES G
                WHERE I.CURSISTNR = R.RELATIENR
                AND SUBSTR(R.POSTCODE, 1, 4) = G.POSTCODE
                AND I.LANDCODE = G.LAND
                AND I.CREATEDATE > SYSDATE - INTERVAL '$minutes' MINUTE
                AND R.LANDCODE = 'NL'
                GROUP BY R.POSTCODE
                ORDER BY 2
                ");
        }
        $laatste_inschrijvingen = array("postcodes" => $_aantal);
        echo json_encode($laatste_inschrijvingen);
    }

    function GetGeoAggrStats($minutes, $test=false) {
        $this->form->database->userdb->SetFetchMode(ADODB_FETCH_ASSOC);
        if ($test) {
            // $_aantal = $this->form->database->userdb->GetAssoc("
            //     SELECT SUBSTR(G.POSTCODE, 1, 4) AS POSTCODE
            //     , 1 AANTAL, MAX(LATITUDE) AS LATITUDE, MAX(LONGITUDE) AS LONGITUDE
            //     FROM GEOCODES G
            //     WHERE G.POSTCODE IN (
            //     '1019','2211','3211','5670','3411','2491','5121','5663','6235','6228',
            //     '1020','2212','3212','5671','3412','2492','5122','5664','6236','6229',
            //     '2020','2114','3222','5674','3413','2493','5109','5464','6111','5111'
            //     )
            //     GROUP BY G.POSTCODE
            //     ORDER BY 1
            // ");
            $_aantal = $this->form->database->userdb->GetAssoc("
                SELECT SUBSTR(G.POSTCODE, 1, 4) AS POSTCODE
                , 1 AANTAL, MAX(LATITUDE) AS LATITUDE, MAX(LONGITUDE) AS LONGITUDE
                FROM GEOCODES G
                WHERE G.POSTCODE IN ('1019','6228')
                GROUP BY G.POSTCODE
                ORDER BY 1
            ");

        } else {
            if ($minutes != '') {
                $_filter = "AND I.CREATEDATE > SYSDATE - INTERVAL '$minutes' MINUTE";
            } else {
                $_filter = "AND I.DATUMINSCHRIJVING = TRUNC(SYSDATE)";
            }
            $_sql = "
                SELECT SUBSTR(R.POSTCODE, 1, 4) AS POSTCODE
                , COUNT(I.INSCHRIJFNR) AANTAL, MAX(LATITUDE) AS LATITUDE, MAX(LONGITUDE) AS LONGITUDE
                FROM INSCHRIJVING I, RELATIE R, GEOCODES G
                WHERE I.CURSISTNR = R.RELATIENR
                AND SUBSTR(R.POSTCODE, 1, 4) = G.POSTCODE
                AND R.LANDCODE = G.LAND
                $_filter
                AND R.LANDCODE = 'NL'
                AND I.OPZEGGINGSDATUM IS NULL
                AND I.DATUMGERETOURNEERD IS NULL
                GROUP BY SUBSTR(R.POSTCODE, 1, 4)
                ORDER BY 1
            ";
            $_aantal = $this->form->database->userdb->GetAssoc($_sql);
        }
        $laatste_inschrijvingen = array("postcodes" => $_aantal);
        echo json_encode($laatste_inschrijvingen);
    }

    function DisplayNHADashboard() {
        $this->smarty->display("dashboard.tpl.php");
    }

    function GetHuidigeActiesLand($landcode) {
        $_cachetimeout = 60 * 60 * 8; // 8 uur

        $_sql = "
        select
            to_char(trunc(g.geplandedatum), 'YYYY,')
          || to_char(to_number(to_char(trunc(g.geplandedatum), 'MM')) - 1, '00')
          || to_char(trunc(g.geplandedatum), ',DD')
          || ',0'
        geplandedatum
        , count(g.relatienr) as aantal
        from nha.geplandeactie g, nha.relatie r
        where g.relatienr = r.relatienr
        and r.landcode = '$landcode'
        and g.actiesoort = 'B'
        and g.geplandedatum >= trunc(sysdate-4) and g.geplandedatum <= trunc(sysdate)
        group by g.geplandedatum
        ";

        $result = $this->form->database->userdb->CacheGetAll($_cachetimeout, $_sql);
        return $result;
    }

    function GetPlanningLand($landcode) {
        $_cachetimeout = 60 * 60 * 8; // 8 uur

        $result = $this->form->database->userdb->CacheGetAll($_cachetimeout, "
        select
               to_char(trunc(v.datumtijd) + p.aantaldagennaverzoekzonderreac, 'YYYY,')
            || to_char(to_number(to_char(trunc(v.datumtijd) + p.aantaldagennaverzoekzonderreac, 'MM')) - 1, '00')
            || to_char(trunc(v.datumtijd) + p.aantaldagennaverzoekzonderreac, ',DD')
            || ',0'
        geplandedatum
        , count(*) as aantal
        from nha.verzoek v
        , nha.parameters p
        , nha.relatie r
        where reedsgereageerdjanee = 'N'
        and trunc(v.datumtijd) + p.aantaldagennaverzoekzonderreac >= trunc(sysdate)
        and trunc(v.datumtijd) + p.aantaldagennaverzoekzonderreac <= trunc(sysdate+7)
        and v.relatienr = r.relatienr
        and (r.telefoonoverdag is not null or r.telefoonsavonds is not null)
        and r.landcode = '$landcode'
        and r.bellenivmtmactiejanee = 'J'

        and (v.verzoeknr in (
            select g.verzoeknr
            from nha.verzoekcursusinfo g, nha.cursus c
            where v.verzoeknr = g.verzoeknr
            and c.cursuscode = g.cursusstudiegidscode
            and c.bellennageenreactiejanee = 'J')
        or  v.verzoeknr in (
            select y.verzoeknr
            from nha.verzoekcursusinfo y, nha.verzendonderdeel d
            where y.verzoeknr = v.verzoeknr
            and y.cursusstudiegidscode = d.verzendonderdeelcode
            and d.verzendonderdeelcode not in ('AHBO')
            and d.soort = 'S'))
        and v.verzoeknr not in (
            select x.oorzaaknr
            from nha.geplandeactie x
            where v.verzoeknr = x.oorzaaknr
            and x.actiesoort = 'B')
        and v.verzoeknr not in (
            select x.oorzaaknr
            from nha.historiegeplandeactie x
            where v.verzoeknr = x.oorzaaknr
            and x.actiesoort = 'B')
        group by trunc(v.datumtijd) + p.aantaldagennaverzoekzonderreac
    ");
        return $result;
    }

    function DisplayPlanningTM() {
        $day = strtotime("today");
        $vandaag = date("Y,", $day);
        $vandaag .= sprintf("%02s", date("m", $day)-1);
        $vandaag .= date(",d,0", $day);

        $day = strtotime("-1 month");
        $vorigeperiode = date("Y,", $day);
        $vorigeperiode .= sprintf("%02s", date("m", $day)-1);
        $vorigeperiode .= date(",d,0", $day);

        $huidige_be = $this->GetHuidigeActiesLand('BE');
        $huidige_nl = $this->GetHuidigeActiesLand('NL');
        $planning_be = $this->GetPlanningLand('BE');
        $planning_nl = $this->GetPlanningLand('NL');
        $_totaalBE = $_totaalNL = 0;

        foreach($huidige_be as $huidig_rec) {
            if (str_replace(',','',str_replace(' ','',$huidig_rec['GEPLANDEDATUM'])) >= str_replace(',','',$vorigeperiode)) {
                $_totaalBE = $_totaalBE + intval($huidig_rec['AANTAL']);
            }
        }
        foreach($huidige_nl as $huidig_rec) {
            if (str_replace(',','',str_replace(' ','',$huidig_rec['GEPLANDEDATUM'])) >= str_replace(',','',$vorigeperiode)) {
                $_totaalNL = $_totaalNL + intval($huidig_rec['AANTAL']);
            }
        }

        $_subtotaal = 0;
        array_unshift($planning_be, array("GEPLANDEDATUM"=>$vandaag,"AANTAL"=>$_totaalBE));
        foreach($planning_be as $key => $planning_rec) {
            if (($key != 0) and str_replace(',','',str_replace(' ','',$planning_rec['GEPLANDEDATUM'])) >= str_replace(',','',$vorigeperiode)) {
                $_vorige = intval($planning_be[$key-1]['ORI_AANTAL']);
                $planning_be[$key]['ORI_AANTAL'] = intval($planning_be[$key]['AANTAL']);
                $_subtotaal = $_subtotaal + $_vorige;
                $planning_be[$key]['AANTAL'] = $planning_be[$key]['ORI_AANTAL'] + $_subtotaal + $_totaalBE;
            }
        }

        $_subtotaal = 0;
        array_unshift($planning_nl, array("GEPLANDEDATUM"=>$vandaag,"AANTAL"=>$_totaalNL));
        foreach($planning_nl as $key => $planning_rec) {
            if (($key != 0) and str_replace(',','',str_replace(' ','',$planning_rec['GEPLANDEDATUM'])) >= str_replace(',','',$vorigeperiode)) {
                $_vorige = intval($planning_nl[$key-1]['ORI_AANTAL']);
                $planning_nl[$key]['ORI_AANTAL'] = intval($planning_nl[$key]['AANTAL']);
                $_subtotaal = $_subtotaal + $_vorige;
                $planning_nl[$key]['AANTAL'] = $planning_nl[$key]['ORI_AANTAL'] + $_subtotaal + $_totaalNL;
            }
        }

        $this->smarty->assign('tm_be_totaal', $_totaalBE);
        $this->smarty->assign('tm_nl_totaal', $_totaalNL);
        $this->smarty->assign('tm_be_huidig', $huidige_be);
        $this->smarty->assign('tm_nl_huidig', $huidige_nl);
        $this->smarty->assign('tm_be_planning', $planning_be);
        $this->smarty->assign('tm_nl_planning', $planning_nl);
        $this->smarty->display("planningtm.tpl.php");
    }

    function Show($outputformat='xhtml', $rec=false) {
        global $_GVARS;

        parent::Show($outputformat, $rec);

        switch ($this->moduleid) {
        case 1:
            $_GVARS['_includeJavascript'][] = $_GVARS['serverpath'].'/javascript/highcharts/js/highcharts.js';
            $_GVARS['_includeJavascript'][] = $_GVARS['serverpath'].'/javascript/jquery/flipcounter.js';
            $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/dashboard.js';
            /***
            * Dashboard
            */
            if ($_GET['func'] == 'getstats') {
                $this->GetStats();
                exit;
            }
            if ($_GET['func'] == 'getgeostats') {
                $this->GetGeoStats($_GET['minutes'], isset($_GET['test']));
                exit;
            }
            if ($_GET['func'] == 'getgeoaggrstats') {
                $this->GetGeoAggrStats($_GET['minutes'], isset($_GET['test']));
                exit;
            }

            $this->showControls = false;
            $this->DisplayNHADashboard();
        break;
        case 2:
            $_GVARS['_includeJavascript'][] = $_GVARS['serverpath'].'/javascript/highcharts/js/highcharts.js';
            $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/planningtm.js';
            /***
            * Planning TM
            */
            $this->showControls = false;
            $this->DisplayPlanningTM();
        break;
        }
    }

}