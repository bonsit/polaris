<?php
require_once(dirname(__FILE__).'/../../includes/app_includes.inc.php');

/**
 * Create the Polaris object
 */
$polaris = new Polaris();
$polaris->initialize();

$_sql = 'SELECT USERGROUPID, USERGROUPNAME, USERORGROUP, FULLNAME FROM plr_usergroup WHERE USERORGROUP = "USER" AND CLIENTID = 2';
$polaris->instance->debug = true;
$rs = $polaris->instance->GetAll($_sql);
if (count($rs) > 0) {
    $clientid = 2;
    $databaseid = 3;
    $database = new plrDatabase( $polaris, array('CLIENTID' => $clientid, 'DATABASEID' => $databaseid) );
    $database->LoadRecord();
    $database->connectUserDB();

    $database->userdb->Execute('DELETE FROM PLR_USERGROUP');

    $_updatesql = 'INSERT INTO PLR_USERGROUP (USERGROUPID, USERGROUPNAME, USERORGROUP, FULLNAME) VALUES (:USERGROUPID, :USERGROUPNAME, :USERORGROUP, :FULLNAME)';
    foreach($rs as $rec) {
        $database->userdb->Execute($_updatesql
            , array('USERGROUPID'=>$rec['USERGROUPID'], 'USERGROUPNAME'=>$rec['USERGROUPNAME'], 'USERORGROUP'=>$rec['USERORGROUP'], 'FULLNAME'=>$rec['FULLNAME']));
    }
}
?>