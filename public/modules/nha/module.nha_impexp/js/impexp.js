var ImpExp = {};

ImpExp = {
    exportResults: [],
    
    initialize: function() {
        var Self = ImpExp;
        
        Self.showExportSets();
    },
    switchView: function(view) {
        $(".view").hide();
        $("."+view).show();
    },
    
    getCustomExportResult: function(callback, theFunction, params) {
        var Self = ImpExp;
        
        $.extend(params, {'func':theFunction});
        $.getJSON(_servicequery, params, function(data, textStatus) {
			if (textStatus == 'success') {
	    		/* Leg data vast als lokale/globale var */
    			Self.exportResults[theFunction] = data;
    			if (callback) callback(data);
            }
        });
    },
    getGenericExportResult: function(callback, form, params) {
        var Self = ImpExp;

        var url = Macs.SharedLib._macs_servicequery.replace("%construct%", form).replace('/const/','/form/');
        $.getJSON(url, params, function(data, textStatus) {
			if (textStatus == 'success') {
	    		/* Leg data vast als lokale/globale var */
    			Self.exportResults[form] = data;
    			if (callback) callback(data);
            }
        });
    },
    addToRecordGroup: function(exportrecord, html) {
        $("#record_list li").each(function() {
            if ($(this).data('obj').RECORDID == exportrecord) {
                if ($('ul', this).length == 0) {
                    $(this).append('<ul></ul>');
                }
                $(this).find('ul').append(html);
            }
        });
    },
    expandRecord: function() {
        var Self = ImpExp;

        var $li = $(this).parents('li');
        var obj = $li.data('obj');
        var _expandExportRecord = function(data) {
            $li.remove();
            $li.append('<ul class="exportfield"></ul>');
            var $ul = $li.find('ul');
            $.each(data, function() {
                var html = '';
                html = '<li><div>' + this.FIELDID + ' ' + this.DESTINATION + ' ' + this.SOURCE 
                + '</div></li>';
                
                var $html = $(html);
                $html.data('obj', this);
                $ul.append($html);
            });
        }

        Self.getCustomExportResult(_expandExportRecord, 'exportfields', {
		    'exportset': obj.EXPORTSET,
		    'recordid': obj.RECORDID
        });
    },
    showExportRecords: function(e) {
        var Self = ImpExp;

        e.preventDefault();

        var _showExportRecord = function(data) {
            $.each(data, function() {
                var html = '';
                html = '<li><div>' + this.RECORDID + ' ' + this.RECORDTYPE 
                + '<div class="button">></button>'
                + '</div></li>';
                
                var $html = $(html);
                $html.data('obj', this).find(".button").click(Self.expandRecord);
                if (this.BELONGSTOGROUP) {
                    Self.addToRecordGroup(this.BELONGSTOGROUP, $html);
                } else {
                    $("#record_list").append($html);
                }
            });
        }

        var exportset = $(e.currentTarget).data('obj');
        Self.getCustomExportResult(_showExportRecord, 'exportrecords', {
		    'exportset': exportset.ID
        });
        
        Self.switchView('exportrecords');
    },
    showExportSets: function() {
        var Self = ImpExp;
        
        var items = [];
        var _showExportSets = function(data) {
            $.each(data, function() {
                var html = '<li><a href="javascript:void()">';
                html += '<div class="apps_content">';
                html += '<span class="appname">' + this.EXPORTNAME + '</span>';
                html += '<span class="version">laatste uitvoerdatum: 12-09-2012</span>';
                html += '<img class="appicon" src="http://localhost/polarisnha/i/simplistica/mail_sent.png" alt="Import Export: '+this.EXPORTNAME+'" />';
                html += '<p>what is this</p>';
                html += '</div>';
                html += '</a></li>';

                var $html = $(html);
                $html.data('obj', this);
                $("#impexp_list").append($html);
            });
            $("#impexp_list li").click(Self.showExportRecords);
        }
        Self.getGenericExportResult(_showExportSets, 'exportset', {});
    }
}

$(document).ready(function(){
    var Self = ImpExp;
    Self.initialize();

});
