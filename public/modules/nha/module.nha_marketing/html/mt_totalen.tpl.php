{include file="shared.tpl.php"}
<input type="hidden" id="_hdnSessionAdvCode" value="{$advertentiecode}" />

<label class="label" title="ADVERTENTIECODE">Verspreidplan:</label>
<select id="_fldADVERTENTIECODE" name="ADVERTENTIECODE"></select>

<div class="pagetabcontainer">
    <ul id="pagetabs">
        {section name=i loop=$verspreiders}
        <li>
            <a id="tab_formscore_page{$verspreiders[i]}" href="#formscore_page{$verspreiders[i]}" >{$verspreiders[i]}</a>
        </li>
        {/section}
    </ul>
</div>

{foreach from=$scores key=k item=score}

<div id="formscore_page{$k}" class="pagecontents">
    {if $k != 'TOTAAL'}
        {foreach from=$score key=i item=variant}
            <div class="{if $variant[i].VARIANT !== NULL}variant{else}totaalvariant{/if}">
                <h2 class="tableheader">{if $variant[i].VARIANT !== NULL}Variant {$variant[i].VARIANT}{else}Totaal verspreider{/if}</h2>
                <table class="scoreoverzicht data">
                    <thead>
                        <tr>
                            <th>Verspreiding</th>
                            <th>Aantal verspr.</th>
                            <th class="right">PC</th>
                            <th class="right">%</th>
                            <th class="right">AC</th>
                            <th class="right">%</th>
                        </tr>
                    </thead>
                    <tbody>
                    {foreach from=$variant key=j item=sc}
                    <tr {if ($sc.WEEKNR_A1 == 0 and $variant[i].VARIANT !== NULL) or ($sc.VARIANT == 'TOTAAL')}class="totaal"{/if}>
                        <td>
                        {if $sc.ADVERTENTIECODE_ADV == 'TOTAAL'}
                            {if $sc.VARIANT == 'TOTAAL' and $variant[i].VARIANT == 'TOTAAL'}
                                Totaal
                            {else}
                                {$sc.VARIANT}
                            {/if}
                        {else}
                            {if $sc.WEEKNR_A1 == 0}
                                Bereik
                            {else}
                                wk {$sc.WEEKNR_A1}
                            {/if}
                        {/if}
                        </td>
                        <td class="right">{$sc.SOM_AANTALBUSSEN_B1}</td>
                        <td class="right">{$sc.SOM_BRUTOTOTAAL_C1}</td>
                        <td class="right">{$sc.CONVERSIE_D1|number_format:4}</td>
                        <td class="right">{$sc.SOM_BRUTOADV_E1}</td>
                        <td class="right">{$sc.CONVERSIE_F1|number_format:4}</td>
                        {if ($sc.WEEKNR_A1 == 0 and $variant[i].VARIANT !== NULL) or ($sc.VARIANT == 'TOTAAL')}
                            {assign var=h1 value=$sc.SOM_BRUTOTOTAAL_C1}
                            {assign var=j1 value=$sc.SOM_BRUTOADV_E1}
                        {/if}
                    </tr>
                    {/foreach}
                    </tbody>
                </table>
                {if $variant[i].VARIANT !== 'TOTAAL'}
                <table class="totaal data">
                    <tr>
                        <td>Drukkosten</td>
                        <td>{$sc.SOM_AANTALBUSSEN_B1}</td>
                        <td class="right">&times; &nbsp; &euro; {$sc.DRUKKOSTEN_L1|number_format:5}</td>
                        <td class="right">{math equation=x*y x=$sc.SOM_AANTALBUSSEN_B1|default:0 y=$sc.DRUKKOSTEN_L1|default:0 assign=n1}
                            = &nbsp; &euro; {$n1|number_format:2}
                        </td>
                    </tr>
                    </tr>
                        <td>Verspreidkosten</td>
                        <td>{$sc.SOM_AANTALBUSSEN_B1}</td>
                        <td class="right">&times; &nbsp; &euro; {$sc.VERSPREIDKOSTEN_M1|number_format:5}</td>
                        <td class="right">{math equation=x*y x=$sc.SOM_AANTALBUSSEN_B1|default:0 y=$sc.VERSPREIDKOSTEN_M1|default:0 assign=o1}
                             = &nbsp; &euro; {$o1|number_format:2}</td>
                    </tr>
                    <tr class="totaal">
                        <td>Totaal (incl BTW)</td>
                        <td></td>
                        <td></td>
                        <td class="right">{math equation=x+y x=$n1 y=$o1 assign=p1}
                            &euro; {$p1|number_format:2}</td>
                    </tr>
                    <tr class="totaal">
                        <td>Kosten per conversie</td>
                        <td>Postcode</td>
                        <td>Adv.code</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>&euro; {if $h1 eq 0}n.v.t.{else}{math equation=x/y x=$p1 y=$h1 format="%.2f"}{/if}</td>
                        <td>&euro; {if $j1 eq 0}n.v.t.{else}{math equation=x/y x=$p1 y=$j1 format="%.2f"}{/if}</td>
                        <td></td>
                    </tr>
                    <tr class="totaal">
                        <td colspan="4"></td>
                    </tr>
                </table>
                {/if}
            </div>
        {/foreach}
    {else}
        {*
            Totalen Tabblad
        *}
        <div class="analyse">
            <h3>Analyse</h3>
            <textarea class="analysebox" id="analysetext" name="analyse">{$analyse}</textarea>
            <div class="buttons">
                <button type="button" class="button primary" id="_hdnSaveAnalyse" type="submit">{lang but_save}</button>
            </div>
        </div>
        <h2 class="tableheader">{if $verspreider[i].VARIANT}Variant {$verspreider[i].VARIANT}{else}Totaal{/if}</h2>
        <table class="scoreoverzicht data totaal">
            <thead>
                <tr>
                    <th>Verspreider</th>
                    <th>Variant</th>
                    <th>Aantal verspr.</th>
                    <th class="right">PC</th>
                    <th class="right">%</th>
                    <th class="right">AC</th>
                    <th class="right">%</th>
                    <th class="right">Kosten/postcode</th>
                    <th class="right">Kosten/advcode</th>
                </tr>
            </thead>
            <tbody>
           {foreach from=$score key=i item=verspreider}
                {foreach from=$verspreider key=j item=variant}
                    {if $variant[0]|is_array}
                    {foreach from=$variant key=l item=sc}
                    <tr {if ($sc.WEEKNR_A1 == 0 and $variant[i].VARIANT !== NULL) or ($sc.VARIANT == 'TOTAAL')}class="totaal"{/if}>
                        <td>{$sc.VERSPREIDER}</td>
                        <td>
                        {if !$variant[i].VARIANT}
                            {$sc.VARIANT}
                        {elseif $sc.WEEKNR_A1 == 0}
                            Bereik
                        {else}
                            wk {$sc.WEEKNR_A1}
                        {/if}
                        </td>
                        <td class="right">{$sc.SOM_AANTALBUSSEN_B1}</td>
                        <td class="right">{$sc.SOM_BRUTOTOTAAL_C1}</td>
                        <td class="right">{$sc.CONVERSIE_D1|number_format:4}</td>
                        <td class="right">{$sc.SOM_BRUTOADV_E1}</td>
                        <td class="right">{$sc.CONVERSIE_F1|number_format:4}</td>
                        <td class="right">
                            {math equation=(x1*y1)+(x1*y2) x1=$sc.SOM_AANTALBUSSEN_B1|default:0 y1=$sc.DRUKKOSTEN_L1|default:0 x1=$sc.SOM_AANTALBUSSEN_B1|default:0 y2=$sc.VERSPREIDKOSTEN_M1|default:0 assign=p1}

                            {if $sc.SOM_BRUTOTOTAAL_C1 eq 0}
                            n.v.t.
                            {else}
                            {math equation=x/y x=$p1 y=$sc.SOM_BRUTOTOTAAL_C1 assign=r1}
                            € {$r1|number_format:2}
                            {/if}</td>
                        <td class="right">
                            {if $sc.SOM_BRUTOADV_E1 eq 0}
                            n.v.t.
                            {else}
                            {math equation=x/y x=$p1 y=$sc.SOM_BRUTOADV_E1 assign=s1}
                            € {$s1|number_format:2}
                            {/if}</td>
                    </tr>
                    {/foreach}
                    {/if}
                {/foreach}
            {/foreach}
            </tbody>
        </table>

    {/if}
</div>
{foreachelse}
    <p>Er is helaas geen score bekend voor dit verspreidplan.</p>
{/foreach}
<br />