{include file="shared.tpl.php"}
<input type="hidden" name="_hdnProcessedByModule" value="false" />
<input type="hidden" id="_hdnSessionAdvCode" value="{$advertentiecode}" />

<br/>
<div id="verspreidplan" class="columngroup">
    <div class="formviewrow">
        <label class="label" title="ADVERTENTIECODE">Campagne:</label>
        <div class="formcolumn campagne">
            <select id="_fldADVERTENTIECODE" name="ADVERTENTIECODE"></select>
            <button class="btnAdd" type="button" id="btnNIEUWEADVERTENTIECODE" value="">+</button>
            <button class="btnRemove" type="button" id="btnVERWIJDERADVERTENTIECODE" value="">-</button>
        </div>
    </div>

    <div class="formviewrow">
        <label class="label" title="DRUKKOSTEN">Drukkosten: &euro;</label>
        <div class="formcolumn">
            <input type="text" label="Drukkosten" class="text validation_float" data-m-dec="5" id="_fldDRUKKOSTEN" name='DRUKKOSTEN' size="7" maxlength='12' value="" /> per folder
        </div>
    </div>
</div>
<div class="columngroupreset"></div>
<div id="movedmasteractions"></div>
<br/>