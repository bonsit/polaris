<h1>Nieuwe campagne <span id="_lblADVERTENTIECODE">bla</span></h1>

<input type="hidden" name="NIEUWEADVERTENTIECODE" id="_fldNIEUWEADVERTENTIECODE" value="">

<div id="_rowADVERTENTIESOORT" class="formviewrow">
    <label class="label">Distributieland:</label>
    <div class="formcolumn">
        <input type="radio" name="LAND" id="_fldLAND_NL" value="NL" checked="checked" /><label for="_fldLAND_NL"> Nederland &nbsp;</label>
        <input type="radio" name="LAND" id="_fldLAND_BE" value="BE" /><label for="_fldLAND_BE"> Belgie &nbsp;</label>
        <input type="radio" name="LAND" id="_fldLAND_DE" value="DE" /><label for="_fldLAND_DE"> Duitsland</label>
    </div>
</div>
<div id="_rowMAAND" class="formviewrow">
    <label class="label">Maand:</label>
    <div class="formcolumn">
        <select id="_fldMAAND" name="MAAND">
            <option value="01">Januari</option>
            <option value="02">Februari</option>
            <option value="03">Maart</option>
            <option value="04">April</option>
            <option value="05">Mei</option>
            <option value="06">Juni</option>
            <option value="07">Juli</option>
            <option value="08">Augustus</option>
            <option value="09">September</option>
            <option value="10">Oktober</option>
            <option value="11">November</option>
            <option value="12">December</option>
        </select>
    </div>
</div>
<div id="_rowJAAR" class="formviewrow">
    <label class="label">Jaar:</label>
    <div class="formcolumn">
        <input type="text" label="Jaar" class="text" name="JAAR" id="_fldJAAR" size="8" maxlength='11' value="" />
    </div>
</div>

<br />
<div class="buttons">
    <button type="button" tabindex="8800" id="btnSAVEANDCLOSE" class="jqmAccept positive">OK</button>
    <button type="button" tabindex="8810" id="btnCLOSE" class="jqmClose secondary">Annuleer</button>
</div>
