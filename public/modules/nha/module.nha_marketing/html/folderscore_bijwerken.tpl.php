<input type="hidden" name="ADVERTENTIECODE" value="{$item[0].ADVERTENTIECODE}" />

<h1>Folderscore bijwerken</h1>
<p>Met deze actie bij u de folderscore bij van advertentie <b>{$item[0].ADVERTENTIECODE}</b>.</p>

<h2>Parameters</h2>
<p>
    <div id="_rowMeetperiode" class="formviewrow">
        <label class="label" title="Meetperiode">Meet periode:</label>
        <div class="formcolumn">
             <input type="text" class="" name="MEETPERIODE" value="{$meetperiode}" size="5" /> dagen<br/>
        </div>
    </div>
</p>

<div class="buttons">
    <button id="btnSAVE" type="button" class="jqmAccept positive {if isset($error)}disabled{/if}" accesskey="F9">Bijwerken</button>
    <button id="btnCANCEL" type="button" class="jqmClose negative" accesskey="F2">Annuleren</button>
</div>

{literal}
<script type="text/javascript">

$(document).ready(function(){
});

$("#btnSAVE").click(function() {
    var senddata = $("#ajaxdialog :input").serializeArray();
    senddata.push({name: '_hdnFORMTYPE', value: 'folderscorebijwerken'});
    senddata.push({name: '_hdnProcessedByModule', value: 'true'});
    $("#ajaxdialog").jqmHide();
    Polaris.Ajax.postJSON(_servicequery, senddata, 'Folderscore bijwerken...', 'De score is bijgewerkt.', function(data) {
        // pass
    });
});
</script>
{/literal}
