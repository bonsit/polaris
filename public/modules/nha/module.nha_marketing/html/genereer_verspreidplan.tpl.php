<input type="hidden" name="ADVERTENTIECODE" value="{$item[0].ADVERTENTIECODE}" />
<input type="hidden" name="LANDCODE" value="{$item[0].LANDCODE}" />
<input type="hidden" name="SOORTSELECTIE" value="{$formule.basisSelectie}" />
<input type="hidden" name="FORMULE" value="{$formule.basisFormule}" />
<h1>Genereren verspreidplannen</h1>
<p>Met deze actie genereert u de {$formulemaand} verspreidplannen van advertentie <b>{$item[0].ADVERTENTIECODE}</b>.</p>

<p>We gaan voor deze verspreiding uit van <b>{if $formule.basisSelectie eq 'topdown'}de best scorende{else}een aselecte verzameling{/if}</b> postbussen van:<br/>

    {if $formule.basisJaar1 neq FALSE}
            Maand
            {html_options name="MAAND1" options=$maanden selected=$formule.basisMaand1}

            Jaar
            {html_options name="JAAR1" options=$jaren selected=$formule.basisJaar1}

            {if $formule.basisJaar2 neq FALSE}
                <p><b>{$formule.basisFormule}</b> de postbussen van de verspreiding van:<br />
                Maand
                {html_options name="MAAND2" options=$maanden selected=$formule.basisMaand2}

                Jaar
                {html_options name="JAAR2" options=$jaren selected=$formule.basisJaar2}
                </p>
            {/if}
    {else}
    <b>Alle postcodes van het land</b>
    {/if}
</p>

<h2>Postcode uitsluiting</h2>

<div id="uitsluitingBlock">
    <p>
        De volgende steden of postcodes worden uitgesloten van de verspreiding:<br/>
        <input type="text" class="taginator" name="UITSLUITING_POSTCODEGEBIED_{$item[0].LANDCODE}" id="uitsluiting_postcodegebieden" value="{','|implode:$uitsluitingen}" style="width:100%" />
    </p>
</div>

<h2>Controlepostcodes</h2>
<div id="controleBlock">
    <p>
        De volgende controle postcodes worden meegenomen bij de verspreiding:<br/>
        <input type="text" class="taginator" name="CONTROLE_POSTCODEGEBIED_{$item[0].LANDCODE}" id="controle_postcodegebieden" value="{','|implode:$controlepostcodes}" style="width:100%" />
    </p>
</div>

{if $laatstescoreupdate neq ''}
<div class="warning">
    <p>Deze scores van deze campagne zijn op {$laatstescoreupdate} geüpdated.
    Het is niet verstandig de verspreidregels opnieuw aan te maken.</p>
    <p>Weet u zeker dat u deze verspreiding opnieuw wilt genereren? &nbsp;
    <input type="radio" name="OPNIEUWGENEREREN" value="J" /> Ja &nbsp; <input type="radio" name="OPNIEUWGENEREREN" value="N" checked="checked" /> Nee
    </p>
</div>
{/if}

<h2>Parameters</h2>
<p>
    <div id="_rowMarge" class="formviewrow">
        <label class="label" title="Afwijking">Verspreidmarge:</label>
        <div class="formcolumn">
             <input type="text" class="" name="VERSPREIDMARGE" value="10" size="5" /> %<br/>
        </div>
    </div>
</p>

<div class="buttons">
    <button id="btnSAVE" type="button" class="jqmAccept positive {if isset($error)}disabled{/if}" accesskey="F9">Genereer</button>
    <button id="btnCANCEL" type="button" class="jqmClose negative" accesskey="F2">Annuleren</button>
</div>

{literal}
<script type="text/javascript">

$(document).ready(function(){
    $(".taginator").tagator({
        prefix: 'tagator_',           // CSS class prefix
        height: 'auto',               // auto or element
        useDimmer: false,             // dims the screen when result list is visible
        showAllOptionsOnFocus: false, // shows all options even if input box is empty
        autocomplete: []              // this is an array of autocomplete options
    });
});

$("#btnSAVE").click(function() {
    var senddata = $("#ajaxdialog :input").serializeArray();
    senddata.push({name: '_hdnFORMTYPE', value: 'genereerverspreidplan'});
    senddata.push({name: '_hdnProcessedByModule', value: 'true'});
    $("#ajaxdialog").jqmHide();
    Polaris.Ajax.postJSON(_servicequery, senddata, 'Bestanden genereren...', 'Het verspreidplan is gegenereerd.', function(data) {
        // pass
    });
});
</script>
{/literal}
