{include file="shared.tpl.php"}
<input type="hidden" name="_hdnRecordID" value="{$items[0].PLR__RECORDID}" />
<input type="hidden" name="_hdnProcessedByModule" value="false" />

<div id="formview" class="formview alignlabels groups_1">
<div class="columngroup">
    <div id="_rowADVERTENTIECODE" class="formviewrow">
        <label class="label" title="ADVERTENTIECODE">Advertentiecode:</label>
        <div class="formcolumn">
            <input type="hidden" id="_fldADVERTENTIECODEx" name="ADVERTENTIECODE" value="{$advertentiecode}" />
            {$advertentiecode}
        </div>
    </div>
    <div id="_rowLANDCODE" class="formviewrow">
        <label class="label" title="LANDCODE">Landcode:</label>
        <div class="formcolumn">
            <input type="hidden" id="_fldLANDCODE" name="LANDCODE" value="{$items[0].LANDCODE}" />
            <input type="text" tabindex="-1" class="readonly" id="_lblLANDCODE" value="{$items[0].LANDCODE_SSS1}" />
        </div>
    </div>
    <div id="_rowVERSPREIDER" class="formviewrow">
        <label class="label" title="VERSPREIDER">Verspreider:</label>
        <div class="formcolumn">
            <select size='1' name='VERSPREIDER' id='_fldVERSPREIDER' style="max-width:400px" label="Verspreider" class="required">
                {html_options options=$verspreideropties selected=$items[0].VERSPREIDER|default:$eersteitem.VERSPREIDER}
            </select>
        </div>
    </div>
    <div id="_rowPERIODE_VANAF" class="formviewrow">
        <label class="label" title="PERIODE_VANAF">Periode vanaf:</label>
        <div class="formcolumn">
            <input type='text' id='_fldPERIODE_VANAF' name='PERIODE_VANAF' label="Periode vanaf" format='d-m-yy' class='text validation_date date_input required' value='{$items[0].PERIODE_VANAF|default:$eersteitem.PERIODE_VANAF}' size='11'>
        </div>
    </div>
    <div id="_rowPERIODE_TM" class="formviewrow">
        <label class="label" title="PERIODE_TM">Periode t/m:</label>
        <div class="formcolumn">
            <input type='text' id='_fldPERIODE_TM' name='PERIODE_TM' label="Periode t/m" format='d-m-yy' class='text validation_date date_input required' value='{$items[0].PERIODE_TM}' size='11'>
        </div>
    </div>
    <div id="_rowWEEKNR" class="formviewrow">
        <label class="label" title="WEEKNR">Weeknr:</label>
        <div class="formcolumn">
            <input type="text" label="Weeknr" class="text required validation_integer" data-v-max="53" id="_fldWEEKNR" name='WEEKNR' size="8" value="{$items[0].WEEKNR}">
        </div>
    </div>
    <div id="_rowREGIO" class="formviewrow">
        <label class="label" title="REGIO">Regio:</label>
        <div class="formcolumn">
            <select size='1' name='REGIO' id='_fldREGIO' style="max-width:400px" label="Regio" xclass="required">
                <option value="">Alles</option>
                {html_options options=$regioopties selected=$items[0].REGIO|default:$eersteitem.REGIO}
            </select>
        </div>
    </div>
    <div id="_rowAANTALBRIEVENBUSSEN" class="formviewrow">
        <label class="label" title="AANTALBRIEVENBUSSEN">Aantal brievenbussen:</label>
        <div class="formcolumn">
            <input type="text" label="Aantal brievenbussen" class="text required validation_integer" data-v-max="9999999999" data-m-dec="0" id="_fldAANTALBRIEVENBUSSEN" name='AANTALBRIEVENBUSSEN' size="10" maxlength='10' value="{$items[0].AANTALBRIEVENBUSSEN|default:$eersteitem.AANTALBRIEVENBUSSEN}" />
        </div>
    </div>
    <div id="_rowVARIANT" class="formviewrow">
        <label class="label" title="VARIANT">Variant:</label>
        <div class="formcolumn">
            <input type="text" label="Variant" class="text validation_allchars" id="_fldVARIANT" name='VARIANT' size="50" maxlength='150' value="{$items[0].VARIANT|default:$eersteitem.VARIANT}">
        </div>
    </div>
    <div id="_rowVERSPREIDKOSTEN" class="formviewrow">
        <label class="label" title="VERSPREIDKOSTEN">Verspreidkosten:</label>
        <div class="formcolumn">
            <input type="text" label="Variant" class="text validation_allchars" id="_fldVERSPREIDKOSTEN" name='VERSPREIDKOSTEN@@FLOATCONVERT' size="20" maxlength='10' value="{$items[0].VERSPREIDKOSTEN|default:$eersteitem.VERSPREIDKOSTEN}">
        </div>
    </div>
    <div id="_rowCOMMENTAAR" class="formviewrow">
        <label class="label" title="COMMENTAAR">Commentaar:</label>
        <div class="formcolumn">
            <input type="text" label="Commentaar" class="text validation_allchars" id="_fldCOMMENTAAR" name='COMMENTAAR' size="50" maxlength='250' value="{$items[0].COMMENTAAR}">
        </div>
    </div>
    <div id="_rowISHERHALINGVAN" class="formviewrow">
        <label class="label" title="ISHERHALINGVAN">Is herhaling van:</label>
        <div class="formcolumn">
            <select size='1' name='ISHERHALINGVAN' id='_fldISHERHALINGVAN' style="max-width:400px" label="Is herhaling van">
                <option value=""></option>
                {html_options options=$herhalingvan selected=$items[0].ISHERHALINGVAN}
            </select>
        </div>
    </div>
</div>
<div class="columngroupreset"></div>
<br/>
