var NHA = {};

NHA.Marketing = {
    dirtyform : false,
    initialize: function() {
        var Self = NHA.Marketing;
        if ($("body").hasClass('insert') || $("body").hasClass('edit')) {
            Self.updateLandNaam($("#_fldADVERTENTIECODEx").val());
        }

        Self.vulAdvertentieCodes();

        $("#btnNIEUWEADVERTENTIECODE").click(Self.nieuweAdvertentieCode);
        $("#btnVERWIJDERADVERTENTIECODE").click(Self.verwijderAdvertentieCode);

        $("#_fldPERIODE_VANAF").change(function() {
            $("#_fldWEEKNR").val(Polaris.Base.getWeekNumber( Polaris.Base.stringToDate($(this).val()) ));
            if ($("#_fldPERIODE_TM").val() == '') {
                $("#_fldPERIODE_TM").val($("#_fldPERIODE_VANAF").val()).change();
            }
        });

        $("#_fldDRUKKOSTEN")
        .autoNumeric({aSep: '.', aDec: ',', mDec: '5'})
        .change(Self.saveCampagne)
        .keyup(function(e) {
            var code = (e.keyCode ? e.keyCode : e.which);
            if (code==13) {
                Self.saveCampagne();
            } else {
                Self.dirtyform = true;
            }
        });

        $("#constructtitle").appendTo($("#movedmasteractions"));
        $("#masteractions").appendTo($("#movedmasteractions"));

        $("#_hdnSaveAnalyse").click(Self.saveAnalyse);
    },
    saveAnalyse: function() {
        var Self = NHA.Marketing;

        var senddata = [];
        senddata.push({name: '_hdnFORMTYPE', value: 'saveanalyse'});
        senddata.push({name: '_hdnProcessedByModule', value: 'true'});
        senddata.push({name: 'ADVERTENTIECODE', value: Self.bepaalSessieAdvCode()})
        senddata.push({name: 'ANALYSE', value: $("#analysetext").val()})
        var message = "Analyse wordt opgeslagen.";
        var feedback_ok = "Analyse is opgeslagen.";
        Polaris.Ajax.postJSON(_servicequery, senddata, message, feedback_ok, function(data) {
        });
        return true;
    },
    _refreshLocation: function(advcode) {
        window.location = Polaris.Base.addLocationVariable('adv', advcode);
    },
    refreshLocation: function(e) {
        e.preventDefault();
        NHA.Marketing._refreshLocation($(this).val());
    },
    huidigeAdvertentieObject: function() {
        return $("#_fldADVERTENTIECODE option:selected").data();
    },
    bepaalSessieAdvCode: function() {
        return ($("#_hdnSessionAdvCode").val());
    },
    vulAdvertentieForm: function() {
        var Self = NHA.Marketing;

        var advobj = Self.huidigeAdvertentieObject();
        if (advobj != null && advobj.DRUKKOSTEN != null)
            $("#_fldDRUKKOSTEN").val(advobj.DRUKKOSTEN.trim());
    },
    vulAdvertentieCodes: function(e) {
        var Self = NHA.Marketing;

        $.getJSON(_servicequery, {
            'func': 'groepadvcodes'
        }, function(data, textStatus) {
            if (textStatus == 'success') {
                var $adv = $("#_fldADVERTENTIECODE");
                $adv
                .empty()
                .addOptionsOptGroup(data, 'ADVERTENTIECODE', 'JAAR,MAAND', Self.bepaalSessieAdvCode())
                .chosen({disable_search_threshold: 10, include_group_label_in_selected: true}) // maak er een mooiere dropdown van
                .change(Self.refreshLocation);
                Self.vulAdvertentieForm();
            }
        });
    },
    saveAdvertentiecode: function(data) {
        var Self = NHA.Marketing;

        var senddata = data;
        senddata.push({name: '_hdnFORMTYPE', value: 'nieuweadvertentiecode'});
        senddata.push({name: '_hdnProcessedByModule', value: 'true'});

        var message = "Nieuwe advertentiecode wordt opgeslagen.";
        var feedback_ok = "Nieuwe advertentiecode is opgeslagen.";
        Polaris.Ajax.postJSON(_servicequery, senddata, message, feedback_ok, function(data) {
            // goto the first advcode in the advcodes of groups
            $.each(data.result, function(key, group) {
                Self._refreshLocation(group[0].ADVERTENTIECODE);
            });
        });
        return true;
    },
    nieuweAdvertentieCode: function(e) {
        var Self = NHA.Marketing;

        Polaris.Window.showModuleForm('/modules/nha/module.nha_marketing/html/advertentiecode_nieuw.tpl.php'
        , /* on Accept */ Self.saveAdvertentiecode
        , /* on Show   */ function() {
            $("#_fldMAAND,#_fldJAAR").change(Self.updateNieuweAdvCode);
            $("input[name=LAND]").click(Self.updateNieuweAdvCode);
            var currdate = new Date();
            currdate.setMonth(currdate.getMonth() + 1);
            $("#_fldJAAR").val(currdate.getFullYear());
            $("#_fldMAAND option:eq("+currdate.getMonth()+")").attr('selected', 'selected');
            Self.updateNieuweAdvCode();
        });
    },
    verwijderAdvertentieCode: function() {
        var Self = NHA.Marketing;

        var $adv = $("#_fldADVERTENTIECODE");
        var advCode = $adv.val();
        var landinfo = Self.getLandNaamISO(advCode);
        var campagne = $("option:selected", $adv).text().trim();
        if (advCode != '' && confirm("Weet u zeker dat u campagne \"" + landinfo.landnaam + " " + campagne + "\" en bijbehorende verspreidingen wilt verwijderen?")) {
            var senddata = [];
            senddata.push({name: 'ADVERTENTIECODE', value: advCode});
            senddata.push({name: '_hdnFORMTYPE', value: 'verwijdercampagne'});
            senddata.push({name: '_hdnProcessedByModule', value: 'true'});

            Polaris.Ajax.postJSON(_servicequery, senddata, null, 'Campagne "' + landinfo.landnaam + ' ' + campagne + '" is verwijderd',
            function() {
                $("option[value='"+advCode+"']", $adv).remove();
                $adv.trigger("chosen:updated");
            });
        }
    },
    getLandNaamISO: function(advertentiecode) {
        var landcode = advertentiecode.substring(3,4);
        var landcombi = {
            'B': {landnaam: 'België', landcode: 'BE'},
            'D': {landnaam: 'Duitsland', landcode: 'DE'},
            'N': {landnaam: 'Nederland', landcode: 'NL'}
        }
        // Nederland heeft geen landcode in de advertentiecode (bijv. HAH0415)
        if (landcode != 'B' && landcode != 'D') landcode = 'N';
        return landcombi[landcode];
    },
    updateLandNaam: function(advertentiecode) {
        var Self = NHA.Marketing;

        var landinfo = Self.getLandNaamISO(advertentiecode);
        $("#_lblLANDCODE").val(landinfo.landnaam);
        $("#_fldLANDCODE").val(landinfo.landcode);
    },
    saveCampagne: function(e) {
        var Self = NHA.Marketing;

        if (Self.dirtyform) {
            var senddata = [];
            senddata.push({name: '_hdnFORMTYPE', value: 'saveverspreiding'});
            senddata.push({name: '_hdnProcessedByModule', value: 'true'});
            senddata.push({name: 'ADVERTENTIECODE', value: $("#_fldADVERTENTIECODE").val()});
            senddata.push({name: 'DRUKKOSTEN@@FLOATCONVERT', value: $('#_fldDRUKKOSTEN').val()});
            var message = null;
            var feedback_ok = "Wijzigingen zijn opgeslagen.";
            Polaris.Ajax.postJSON(_servicequery, senddata, message, feedback_ok, Self.refreshVerspreiding);
            Self.dirtyform = false;
            return true;
        }
    },
    refreshVerspreiding: function(data) {
        log(data);
    },
    updateNieuweAdvCode: function() {
        var landcode = $("input[name=LAND]:checked").val().substring(0,1);
        if (landcode == 'N') landcode = '';
        var adcode = 'HAH' + landcode;
        adcode += $("#_fldMAAND").val() + $("#_fldJAAR").val().substring(2);
        $("#_lblADVERTENTIECODE").text(adcode);
        $("#_fldNIEUWEADVERTENTIECODE").val(adcode);
    }
}

$(document).ready(function(){
    NHA.Marketing.initialize();
});
