<?php
/**
    crontab: elke 6 minuten
    * / 6 * * * * /usr/local/bin/cronic php /var/www/html/polaris-test/modules/nha/module.nha1/php/cli_inschrijfforms_emailen.php
*/
require_once(dirname(__FILE__).'/../../../../includes/app_includes.inc.php');
require_once(dirname(__FILE__).'/bufferinschrijvingen.class.php');

global $_GVARS;
$_GVARS['docroot'] = dirname(__FILE__).'/../../';

/**
 * Create the Polaris object
 */
$polaris = new Polaris();
$polaris->initialize();

$clientid = 2;
$databaseid = 3;
$database = new plrDatabase($polaris, array('CLIENTID' => $clientid, 'DATABASEID' => $databaseid) );
$database->LoadRecord();
$database->connectUserDB();

$_sql = '';
$bfr = new BufferInschrijvingen($polaris, $database);
if ($bfr->AutoInschrijfFormEmailenActief()) {
    $validrowids = $database->userdb->GetCol('SELECT ROWIDTOCHAR(I.ROWID) FROM INSCHRIJVING I, RELATIE R
    WHERE I.CURSISTNR = R.RELATIENR AND NVL(INSFRM_GEMAILD, \'N\') = \'N\' AND I.INSCHRIJFFORM_PER_EMAIL_JN = \'J\'
    AND R.EMAIL IS NOT NULL AND (I.PRESTATUS NOT IN (\'MATINCOMPLEET\', \'COHORTWACHTKAMER\') OR I.PRESTATUS IS NULL)
    ');
    $validrowids = implode(',', $validrowids);
    $error = '';
    $result = FALSE;
    if ($validrowids !== '') {
        $result = $bfr->BriefInschrijfFormsMailen(array(), $validrowids, 'InschrijfformMailenSingle', $error);
    }
    var_dump($result);
} else {
    var_dump('Automatisch emailen niet actief.');
}