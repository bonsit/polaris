<?php

ini_set("error_reporting", E_ALL & ~E_NOTICES & ~E_DEPRECATED);
require 'vendor/ros/ezpdf/src/Cezpdf.php';

class Creport extends Cezpdf {

    function Creport($papersize,$orientation){
        global $_GVARS;

        $this->Cezpdf($papersize,$orientation);
        $this->mainFont = '/Library/WebServer/Documents/polaris/php_includes/ezPDF/fonts/Helvetica.afm';

        $euro_diff = array(94=>'Euro');
        $this->selectFont($this->mainFont,
            array('encoding'=>'WinAnsiEncoding','differences'=>$euro_diff));
    }
}

class Academie {
    var $NaamKort = '';
    var $Naam = '';
}

class InschrijfRelatie {
    var $RelatieNr;
    var $Geslacht;
    var $TussenVoegsel;
    var $AchterNaam;
    var $Plaats;
}

class Postbus {
    var $AdresNr = '';
    var $Postbus = '';
    var $Postcode = '';
    var $Plaats = '';
    var $LandCode = '';
}

class BankVerbinding {
    var $BankNr = '';
    var $BankNaam = '';
    var $Plaats = '';
    var $RekeningNr = '';
    var $LandCode = '';
    var $IBANCode = '';
    var $BICCode = '';
}

class EigenAdres {
    var $AdresNr = '';
    var $Postcode = '';
    var $Plaats = '';
    var $LandCode = '';
    var $TaalCode = '';
    var $Straat = '';
    var $HuisNummer = '';
}

class Registratie {
    var $LandCode = '';
    var $Registratie = '';
    var $BTWNummer = '';
}

class Afdeling {
    var $LandCode = '';
    var $AfdelingNr = '';
    var $AfdelingNaam = '';
    var $DoorKiesNr = '';
    var $HandtekeningBestand = '';
}

class Relatie {
    var $RelatieNr = '';
    var $BedrijfsNaam = '';
    var $AfdelingsNaam = '';
    var $Aanhef = '';
    var $Voorletters = '';
    var $TussenVoegsel = '';
    var $Achternaam = '';
    var $Straat = '';
    var $HuisNummer = '';
    var $Postcode = '';
    var $Plaats = '';
    var $LandCode = '';
    var $Geslacht = '';
    var $RekeningNr = '';
    var $LandNaam = '';
    var $BTWNummer = '';
}

class BegeleidendeBrief {
    var $Inhoud = '';
}

class SpecificatieItem {
    var $Omschrijving = '';
    var $TermijnNr = '';
    var $Aantal = '';
    var $VervalDatum = '';
    var $SubTotaal = '';
    var $PrijsPerStuk = '';
    var $VerwijderingsBijdragePerStuk = '';
}

class Specificatie {
    var $items = array();
    var $totaal = '';

    function Specificatie() {
    }

    function &add() {
        $this->items[] = new SpecificatieItem();
        return $this->items[count($this->items)-1];
    }
}

class Factuur {
    var $Academie;
    var $Landcode;
    var $Postbus;
    var $BankVerbinding;
    var $EigenAdres;
    var $Registratie;
    var $Afdeling;
    var $Relatie;
    var $BegeleidendeBrief;

    var $FactuurNr;
    var $printDateTime;
    var $DateringDatum = '';
    var $InschrijfNr = '';
    var $Inzake = '';
    var $VervalDatum = '';
    var $KoppelNr = '';
    var $AcceptGiroNr = '';
    var $Bedrag = '';
    var $BetalingsKenmerk = '';
    var $ZuivereFactuur = '';
    var $FormulierCode = '';

    var $TotaalGewogen = '';

    var $_pdf = null;

    var $OCRFontSize = 10;
    var $OCRFontSizeBE = 9.5;
    var $OCRFontName = '';
    var $BasicFontName = '';

    var $RelatieFontSize = 8; // relatie acceptgiro
    var $RelatieFontSizeBE = 11;
    var $InzakeFontSize = 8; // inzake acceptgiro
    var $leftmargin = 50;
    var $topmargin = 685;

    var $acceptNL_leftmargin = 0;
    var $acceptNL_topmargin = -2;

    var $OLDLOCAL = 'nl_NL';

    var $localstings = array(
        'DE' => array(
            'specificatie' => 'Spezifikation',
            'omschrijving' => 'Umschreibung',
            'termijnnr' => 'Termin',
            'vervaldatum' => 'Zahlungstermin',
            'bedrag' => 'Betrag',
            'totaal' => 'Gesamte',
            'tav' => 'z. Hd. von',
            'dhr' => 'Herr',
            'mevr' => 'Frau',
            'relatienr' => 'Kundennr',
            'btwnummer' => 'USt-IdNr',
            'factuurnr' => 'Rechnungnr',
            'inzake' => 'Betreff',
            'telefoonnr' => 'Rufnummer',
            'doorkiesnr' => 'Direktwahl',
            'postcode' => 'Postleitzahl',
            'postbus' => 'Postfach',
            'btw' => 'MwSt',
            'bank' => 'BANK',
            'betreft' => 'Betreff'
            ),
        'NL' => array(
            'specificatie' => 'Specificatie',
            'omschrijving' => 'Omschrijving',
            'termijnnr' => 'Termijnnr',
            'vervaldatum' => 'Vervaldatum',
            'bedrag' => 'Bedrag',
            'totaal' => 'Totaal',
            'tav' => 'T.a.v.',
            'dhr' => 'Dhr',
            'mevr' => 'Mevr',
            'relatienr' => 'Relatienr',
            'btwnummer' => 'BTW-nummer',
            'factuurnr' => 'Factuurnr',
            'inzake' => 'Inzake',
            'telefoonnr' => 'Telefoonnr',
            'doorkiesnr' => 'Doorkiesnr',
            'postcode' => 'Postcode',
            'postbus' => 'Postbus',
            'btw' => 'BTW',
            'bank' => 'BANK',
            'betreft' => 'Betreft'
        ));

    function Factuur() {
        global $_CONFIG;

        $this->Academie = new Academie();
        $this->InschrijfRelatie = new InschrijfRelatie();
        $this->Postbus = new Postbus();
        $this->BankVerbinding = new BankVerbinding();
        $this->EigenAdres = new EigenAdres();
        $this->Registratie = new Registratie();
        $this->Afdeling = new Afdeling();
        $this->Relatie = new Relatie();
        $this->BegeleidendeBrief = new BegeleidendeBrief();
        $this->Specificatie = new Specificatie();

        if ($_SERVER['SERVER_ADDR']=='::1') {
            $this->BasicFontName = '/var/www/html/polarisnha/php_includes/ezPDF/fonts/Helvetica.afm';
            $this->OCRFontName = '/var/www/html/polarisnha/php_includes/ezPDF/fonts/ocrb10.afm';
            $this->KIXFontName = '/var/www/html/polarisnha/php_includes/ezPDF/fonts/KIX-BRG_.afm';
        } else {
            if ($_CONFIG['ostype'] == 'WINDOWS') {
                $this->BasicFontName = 'php_includes/ezPDF/fonts/Helvetica.afm';
                $this->OCRFontName = 'php_includes/ezPDF/fonts/ocrb10.afm';
                $this->KIXFontName = 'php_includes/ezPDF/fonts/KIX-BRG_.afm';
//                $this->KIXFontName = 'php_includes/ezPDF/fonts/Helvetica.afm';
            } else {
                $this->BasicFontName = 'php_includes/ezPDF/fonts/Helvetica.afm';
                $this->OCRFontName = '/usr/share/fonts/nhatype1/ocrb10.afm';
                $this->KIXFontName = '/usr/share/fonts/nhatype1/KIX-BRG_.afm';
            }
        }
    }

    function AlsBetreftWeergave() {
        if ($this->InschrijfRelatie->Geslacht == 'M') {
            $_geslacht = $this->Translate('dhr').'. ';
        } elseif ($this->Geslacht == 'V') {
            $_geslacht = $this->Translate('mevr').'. ';
        } else {
            $_geslacht = '';
        }
        $string = $_geslacht;
        if ($this->InschrijfRelatie->TussenVoegsel != '') {
            $string .= $this->InschrijfRelatie->TussenVoegsel.' ';
        }
        $string .= $this->InschrijfRelatie->AchterNaam.', '.$this->InschrijfRelatie->Plaats;
        return substr($string, 0, 40);
    }

    function TaalCode() {
        // indien het een factuurrelatie betreft
        // dan kijken naar de relatie landcode, anders naar de factuur landcode
        if ($this->InschrijfRelatie->RelatieNr > 0) {
            $_landcode = $this->Relatie->LandCode;
        } else {
            $_landcode = $this->Landcode;
        }
        if ($this->IsDuitseRegio($_landcode)) {
            return 'DE';
        } else {
            return 'NL';
        }
    }

    function AcademieLandCode() {
        return $this->EigenAdres->LandCode;
    }

    function Translate($text) {
        $localtext = $this->localstings[$this->TaalCode()][$text];
        if ($localtext == '') {
            $localtext = $text;
        }
        return $localtext;
    }

    function PrintKoppelNr() {
        $fontsize = 9;
        $leftmargin = $this->leftmargin+490;
        $topmargin = $this->topmargin+100;
        $this->_pdf->addText($leftmargin, $topmargin, $fontsize, $this->KoppelNr);
    }

    function PrintLogo() {
        global $_CONFIG;

        if ($_CONFIG['ostype'] == 'WINDOWS') {
//            $foto = 'C:\\htdocs\\jasperreports\\images\\'.$this->EigenAdres->LogoBestand;
        } else {
            $foto = '/var/www/macs/images/'.$this->EigenAdres->LogoBestand;
        }
        if (file_exists($foto) and is_file($foto)) {
            list($width, $height) = getimagesize($foto);
            if ($this->EigenAdres->LandCode == 'DE') {
                $newwidth = 180;
            } else {
                $newwidth = 150;
            }
            $this->_pdf->addJpegFromFile($foto,270-(($width*0.1)/2),730,$newwidth);
        }
    }

    function PrintNHAAdres() {
        global $polaris;

        $fontsize = 10;

        $leftmargin = $this->leftmargin;
        $topmargin = $this->topmargin;
        $delta = 80;
        $deltaX = 11;
        $this->_pdf->addText($leftmargin, $topmargin, $fontsize, $this->EigenAdres->Plaats);
        $this->_pdf->addText($leftmargin+$delta, $topmargin, $fontsize, ": ".$this->printDateTime);
        $topmargin -= $deltaX;
        $this->_pdf->addText($leftmargin, $topmargin, $fontsize, $this->Translate('relatienr'));
        $this->_pdf->addText($leftmargin+$delta, $topmargin, $fontsize, ": ".$this->Relatie->RelatieNr);
        $topmargin -= $deltaX;
        if ($this->Relatie->LandCode == 'BE' and trim($this->Relatie->BTWNummer) != '') {
            $this->_pdf->addText($leftmargin, $topmargin, $fontsize, $this->Translate('btwnummer'));
            $this->_pdf->addText($leftmargin+$delta, $topmargin, $fontsize, ": ".$this->Relatie->BTWNummer);
            $topmargin -= $deltaX;
        }
        $this->_pdf->addText($leftmargin, $topmargin, $fontsize, $this->Translate('factuurnr'));
        $this->_pdf->addText($leftmargin+$delta, $topmargin, $fontsize, ": ".$this->FactuurNr);
        if (trim($this->Inzake) != '') {
            $topmargin -= $deltaX;
            $this->_pdf->addText($leftmargin, $topmargin, $fontsize, $this->Translate('inzake'));
            $this->_pdf->addText($leftmargin+$delta, $topmargin, $fontsize, ": ".$this->Inzake);
        }
        $topmargin -= $deltaX;
        if ($this->Relatie->LandCode == 'BE') {
            $telefoon_tekst = $this->Translate('telefoonnr');
        } else {
            $telefoon_tekst = $this->Translate('doorkiesnr');
        }
        $this->_pdf->addText($leftmargin, $topmargin, $fontsize, $telefoon_tekst);
        $this->_pdf->addText($leftmargin+$delta, $topmargin, $fontsize, ": ".$this->Afdeling->DoorKiesNr);
        if ($this->InschrijfRelatie->RelatieNr > 0) {
            $topmargin -= $deltaX;
            $this->_pdf->addText($leftmargin, $topmargin, $fontsize, $this->Translate('betreft'));
            $fontsize = 8;
            $this->_pdf->addText($leftmargin+$delta, $topmargin, $fontsize, ": ".$this->AlsBetreftWeergave());
        }
    }

    function PrintFactuurRelatieAdres() {
        $fontsize = 10;
        $leftmargin = $this->leftmargin + 245;
        $topmargin = $this->topmargin;
        $delta = 11;

        if ($this->Relatie->BedrijfsNaam == '') {
            $naam = "{$this->Relatie->Aanhef}";
        } else {
            $bedrijf = $this->Relatie->BedrijfsNaam;
            $this->_pdf->addText($leftmargin,$topmargin,$fontsize,"{$bedrijf}");
            $topmargin = $topmargin - $delta;

            if ($this->Relatie->AfdelingsNaam != '') {
                $this->_pdf->addText($leftmargin,$topmargin,$fontsize,"{$this->Relatie->AfdelingsNaam}");
                $topmargin = $topmargin - $delta;
            }

            if ($this->Relatie->Achternaam !== '.') {
                $naam = $this->Translate('tav');
            }
        }

        if ($this->Relatie->Achternaam !== '.') {
            $naam .= " {$this->Relatie->Voorletters}";

            if ($this->Relatie->TussenVoegsel != '')
                $naam .= " {$this->Relatie->TussenVoegsel}";
            $naam .= " {$this->Relatie->Achternaam}";
        }
        $this->_pdf->addText($leftmargin,$topmargin,$fontsize,$naam);
        $topmargin = $topmargin - $delta;
        $this->_pdf->addText($leftmargin,$topmargin,$fontsize,"{$this->Relatie->Straat} {$this->Relatie->HuisNummer}");
        $topmargin = $topmargin - $delta;
        $plaats = strtoupper($this->Relatie->Plaats);
        $this->_pdf->addText($leftmargin,$topmargin,$fontsize,"{$this->Relatie->Postcode} {$plaats}");
        if ($this->Relatie->LandNaam != '') {
            $topmargin = $topmargin - $delta;
            $this->_pdf->addText($leftmargin,$topmargin,$fontsize,"{$this->Relatie->LandNaam}");
        }

        // KIX barcode
        $this->_pdf->selectFont($this->KIXFontName, 'none');
        $topmargin = $topmargin - $delta;
        $fontsize = 10;
        $this->_pdf->addText($leftmargin,$topmargin,$fontsize,"{$this->Relatie->KIXCode}");
        $this->_pdf->selectFont($this->BasicFontName);
        /*
        */
    }

    function PrintBriefInhoud() {
        $fontsize = 9;
        $this->_pdf->ezSetY($this->topmargin - 67);
        $tekst = str_replace("\t","",$this->BegeleidendeBrief->Inhoud);
        $tekst = str_replace("\r\n&nbsp;","",$tekst);
        $tekst = strip_tags($tekst,'<b><i>');
        $tekst = str_replace("\r\n\r\n\r\n","\r\n\r\n",html_entity_decode(trim($tekst)));
        $this->_pdf->ezText($tekst, $fontsize, array('justification'=>'full', 'spacing' => 1.1));
    }

    function PrintSpecificatie() {
        global $polaris;

        $fontsize = 10;
        $this->_pdf->ezText("", $fontsize, array('justification'=>'full', 'left'=>0, 'spacing' => 1.1));
        $this->_pdf->ezText($this->Translate('specificatie').":", $fontsize, array('justification'=>'full', 'left'=>0, 'spacing' => 1.1));
        $fontsize = 4;
        $y = $this->_pdf->ezText("", $fontsize, array('justification'=>'full', 'left'=>0, 'spacing' => 1.1));
        $fontsize = 8;

        $tableleft = $this->leftmargin + 0;

        $_rowdelta = 10;
        $_termijnDelta = 150;
        $_vervalDelta = 200;
        $_bedragDelta = 270;

        $y = $y - $_rowdelta;

        $this->_pdf->addText($tableleft, $y, $fontsize, "<b>".$this->Translate('omschrijving')."</b>");
        $this->_pdf->addText($tableleft+$_termijnDelta, $y, $fontsize, "<b>".$this->Translate('termijnnr')."</b>");
        $this->_pdf->addText($tableleft+$_vervalDelta, $y, $fontsize, "<b>".$this->Translate('vervaldatum')."</b>");
        $this->_pdf->addText($tableleft+$_bedragDelta, $y, $fontsize, "<b>".$this->Translate('bedrag')."</b>");
        $y = $y - $_rowdelta;

        foreach($this->Specificatie->items as $_item) {
            $this->_pdf->addText($tableleft, $y, $fontsize, $_item['OMSCHRIJVING']);
            $this->_pdf->addText($tableleft+$_termijnDelta, $y, $fontsize, $_item['TERMIJNNR']);
            $this->_pdf->addText($tableleft+$_vervalDelta, $y, $fontsize, $_item['VERVALDATUM']);
            $this->_pdf->addText($tableleft+$_bedragDelta, $y, $fontsize, $_item['SUBTOTAAL']);
            $y = $y - $_rowdelta;
        }

/*
        $y = $this->_pdf->ezTable($this->Specificatie->items,
            $cols=array('OMSCHRIJVING'=>'Omschrijving                           ','TERMIJNNR'=>'Termijn','VERVALDATUM'=>'Vervaldatum','SUBTOTAAL'=>'         Bedrag'),
            $title='',
            $options=array('shaded'=>0,'showLines'=>0,'fontSize'=>8,'rowGap'=>'1','xOrientation'=>'left','xPos'=>$tableleft));
*/
        $this->_pdf->setStrokeColor(0,0,0);
        $this->_pdf->setLineStyle($width=1);
        $y = $y + 6;
        $this->_pdf->line($tableleft+270,$y,$tableleft+330,$y);
        $y = $y - 12;
        $this->_pdf->addText($tableleft+230, $y, $fontsize, "<b>".$this->Translate('totaal')."</b>");
        $this->_pdf->addText($tableleft+270, $y, $fontsize, "".$this->Specificatie->totaal."");

        if ($this->BTWtonen) {
            $y = $y - 20;
            $this->_pdf->addText($tableleft+220, $y, $fontsize, $this->Translate('btw')." {$this->BTWpercentage}% ");
            $this->_pdf->addText($tableleft+270, $y, $fontsize, $this->Specificatie->btwbedrag);
        }
    }

    function PositieFormule($string) {
        $len = strlen($string);
        return 270 - intval($len*1.5);
    }

    function IsDuitseRegio($landcode) {
        return ($landcode == 'DE' or $landcode == 'AT' or $landcode == 'CH');
    }

    function IsCaribischeRegio($landcode) {
        return ($landcode == 'CW' or $landcode == 'AW' or $landcode == 'BQ');
    }

    function PrintFooter() {
        global $polaris;

        $fontsize = 7.5;
        $_reg = $this->Registratie->Registratie;
        //if (trim($_reg) != '')
        //    $_reg .= '.';

        if ($this->IsDuitseRegio($this->Relatie->LandCode) or $this->IsCaribischeRegio($this->Relatie->LandCode)) {
//        if ($this->IsDuitseRegio($this->Registratie->LandCode)) {
//        if ($this->AcademieLandCode() == 'DE') {
            $_ypos = 20;
        } else {
            $_ypos = 300;
        }

        $_btw = $this->Registratie->BTWNummer;
        if ($this->Postbus->Postbus != '0') {
            $regel = $this->Translate('postbus');
            $regel .= " {$this->Postbus->Postbus}, {$this->Postbus->Postcode} {$this->Postbus->Plaats}  |  ";
        } else {
            $regel = " {$this->EigenAdres->Straat} {$this->EigenAdres->HuisNummer}, {$this->EigenAdres->Postcode} {$this->EigenAdres->Plaats}  |  ";
        }
//        $regel .= $this->Translate('bank');
        $regel .= " {$_reg} {$_btw}  |  ";
        $regel .= " {$this->BankVerbinding->BankNaam} {$this->BankVerbinding->RekeningTekst}";

        $this->_pdf->addText($this->PositieFormule($regel),$_ypos, $fontsize, $regel);
    }

    function PrintNietSturen() {
        $fontsize = 16;
        $regel = "NIET INSTUREN";
        $this->_pdf->addText(180,155,$fontsize, $regel);
    }

    function PrintAcceptGiro() {
        $this->_pdf->selectFont($this->BasicFontName);
        if ($this->IsDuitseRegio($this->Relatie->LandCode) or $this->IsCaribischeRegio($this->Relatie->LandCode)) {
            // geen acceptgiro voor Duitsland
        } elseif ($this->Relatie->LandCode == 'BE') {
            $this->PrintAcceptGiro_BE();
        } else {
            $this->PrintAcceptGiro_NL();
        }
    }

    function PrintAcceptGiro_NL() {
        $this->PrintAGNLControleStrook();
        $this->PrintAGNLBody();
    }

    function PrintAGNLControleStrook() {
        $this->PrintAGNLControleStrookBetalingsKenmerk();
        $this->PrintAGNLControleStrookBedrag();
        $this->PrintAGNLControleStrookInzake();
        $this->PrintAGNLControleStrookNHAGegevens();
    }

    function PrintAGNLControleStrookBetalingsKenmerk() {
        $fontsize = $this->OCRFontSize;
        $offsetX = 21 + $this->acceptNL_leftmargin;
        $offsetY = 243 + $this->acceptNL_topmargin;
        $delta = 13;
        $this->_pdf->selectFont($this->OCRFontName, 'none');

        $this->_pdf->addText($offsetX, $offsetY, $fontsize, substr($this->BetalingsKenmerk,0,4));
        $this->_pdf->addText($offsetX+37, $offsetY, $fontsize, substr($this->BetalingsKenmerk,4,4));
        $this->_pdf->addText($offsetX, $offsetY-$delta, $fontsize, substr($this->BetalingsKenmerk,8,4));
        $this->_pdf->addText($offsetX+37, $offsetY-$delta, $fontsize, substr($this->BetalingsKenmerk,12,4));
    }

    function PrintAGNLControleStrookBedrag() {
        $fontsize = $this->OCRFontSize;
        $bedragX = 25 + $this->acceptNL_leftmargin;
        $bedragY = 194 + $this->acceptNL_topmargin;
        $fracX = 80;
        $fracY = $bedragY;
        $this->_pdf->selectFont($this->OCRFontName, 'none');
        $this->_pdf->addText($bedragX, $bedragY, $fontsize, $this->totaal_bedrag);
        $this->_pdf->addText($fracX, $fracY, $fontsize, $this->totaal_frac);

    }

    function PrintAGNLControleStrookInzake() {
        $fontsize = $this->InzakeFontSize;
        $inzakeX = 20 + $this->acceptNL_leftmargin;
        $inzakeY = 154 + $this->acceptNL_topmargin;
        $inzakeDeltaX = 60;
        $inzakeDeltaY = 12;
        $this->_pdf->selectFont($this->BasicFontName);

        $_tekst = $this->InschrijfNr == ''?'Factuurnr':'Inschrijfnr';
        $this->_pdf->addText($inzakeX,$inzakeY,$fontsize, $_tekst.':');
        $_streepje = $this->InschrijfNr == ''?'':'/';
        $this->_pdf->addText($inzakeX,$inzakeY-$inzakeDeltaY,$fontsize+2, $this->InschrijfNr.$_streepje.$this->FactuurNr);

        $this->_pdf->addText($inzakeX,$inzakeY-$inzakeDeltaY*2,$fontsize, 'Betalen voor:');
        $this->_pdf->addText($inzakeX,$inzakeY-$inzakeDeltaY*3,$fontsize+2, $this->VervalDatum);
    }

    function PrintAGNLControleStrookNHAGegevens() {
        $offsetX = 20 + $this->acceptNL_leftmargin;
        $offsetY = 97 + $this->acceptNL_topmargin;
        $delta = 11;
        $this->_pdf->selectFont($this->BasicFontName);

        $fontsize = $this->RelatieFontSize;
        $this->_pdf->addText($offsetX+27, $offsetY, $fontsize, $this->BankVerbinding->RekeningNr);
        $fontsize = $this->RelatieFontSize - 1;
        $this->_pdf->addText($offsetX, $offsetY-$delta, $fontsize, "<b>{$this->Academie->NaamKort}</b>");
        $this->_pdf->addText($offsetX, $offsetY-$delta*2, $fontsize, "<b>".$this->Translate('postbus')." {$this->Postbus->Postbus}</b>");
        $this->_pdf->addText($offsetX, $offsetY-$delta*3, $fontsize, "<b>{$this->Postbus->Postcode}  {$this->Postbus->Plaats}</b>");
    }

    function PrintAGNLBodyInzake() {
        $fontsize = $this->InzakeFontSize;
        $inzakeX = 290 + $this->acceptNL_leftmargin;
        $inzakeY = 273 + $this->acceptNL_topmargin;
        $inzakeDeltaX = 60;
        $inzakeDeltaY = 12;
        $this->_pdf->selectFont($this->BasicFontName);
        if (trim($this->Inzake) != '') {
            $this->_pdf->addText($inzakeX,$inzakeY,$fontsize, 'Inzake');
            $this->_pdf->addText($inzakeX+$inzakeDeltaX,$inzakeY,$fontsize, ": ".$this->Inzake);
            $inzakeY -= $inzakeDeltaY;
        }
        $this->_pdf->addText($inzakeX,$inzakeY,$fontsize, 'Betalen voor');
        $this->_pdf->addText($inzakeX+$inzakeDeltaX,$inzakeY,$fontsize, ": ".$this->VervalDatum);
        $inzakeY -= $inzakeDeltaY;

        $_tekst = $this->InschrijfNr == ''?'Factuurnr':'Inschrijfnr';
        $_streepje = $this->InschrijfNr == ''?'':'/';
        $this->_pdf->addText($inzakeX,$inzakeY,$fontsize, $_tekst);

        $this->_pdf->addText($inzakeX+$inzakeDeltaX,$inzakeY,$fontsize, ": ".$this->InschrijfNr.$_streepje.$this->FactuurNr);
    }

    function PrintAGNLBodyBedrag() {
        $fontsize = $this->OCRFontSize;
        $bedragX = 125 + $this->acceptNL_leftmargin;
        $bedragY = 230 + $this->acceptNL_topmargin;
        $fracX = 238 + $this->acceptNL_leftmargin;
        $fracY = $bedragY;
        $this->_pdf->selectFont($this->OCRFontName, 'none');
        $this->_pdf->addText($bedragX+(8-strlen($this->totaal_bedrag))*7, $bedragY, $fontsize, $this->totaal_bedrag);
        $this->_pdf->addText($fracX, $fracY, $fontsize, $this->totaal_frac);

    }

    function PrintAGNLBodyBetalingsKenmerk() {
        $fontsize = $this->OCRFontSize;
        $this->_pdf->selectFont($this->OCRFontName, 'none');
        $deltaBK = 36;
        $offsetX = 284 + $this->acceptNL_leftmargin;
        $offsetY = 228 + $this->acceptNL_topmargin;
        $this->_pdf->addText($offsetX, $offsetY, $fontsize, substr($this->BetalingsKenmerk,0,4));
        $this->_pdf->addText($offsetX+$deltaBK, $offsetY, $fontsize, substr($this->BetalingsKenmerk,4,4));
        $this->_pdf->addText($offsetX+$deltaBK*2, $offsetY, $fontsize, substr($this->BetalingsKenmerk,8,4));
        $this->_pdf->addText($offsetX+$deltaBK*3, $offsetY, $fontsize, substr($this->BetalingsKenmerk,12,4));

    }

    function PrintAGNLBodyRekeningNr() {
        $fontsize = $this->OCRFontSize;
        $offsetX = 180 + $this->acceptNL_leftmargin;
        $offsetY = 194 + $this->acceptNL_topmargin;
        $this->_pdf->selectFont($this->OCRFontName, 'none');
        $this->_pdf->addText($offsetX, $offsetY, $fontsize, $this->Relatie->RekeningNr);
    }

    function PrintAGNLBodyRelatie() {
        $fontsize = $this->RelatieFontSize;
        $offsetX = 163 + $this->acceptNL_leftmargin;
        $offsetY = 158 + $this->acceptNL_topmargin;
        $delta = 17;
        $this->_pdf->selectFont($this->BasicFontName);

        if ($this->Relatie->BedrijfsNaam == '') {
//            $naam = $this->Relatie->Aanhef;
            $naam = "{$this->Relatie->Voorletters}";
            if ($this->Relatie->TussenVoegsel != '')
                $naam .= " {$this->Relatie->TussenVoegsel}";
            $naam .= " {$this->Relatie->Achternaam}";
        } else {
            $naam = $this->Relatie->BedrijfsNaam;
        }

        $this->_pdf->addText($offsetX, $offsetY, $fontsize, $naam);
        $this->_pdf->addText($offsetX, $offsetY-$delta, $fontsize, "{$this->Relatie->Straat} {$this->Relatie->HuisNummer}");
        $this->_pdf->addText($offsetX, $offsetY-$delta*2, $fontsize, "{$this->Relatie->Postcode}  {$this->Relatie->Plaats}");
    }

    function PrintAGNLBodyNHAGegevens() {
        global $polaris;

        $offsetX = 160 + $this->acceptNL_leftmargin;
        $offsetY = 97 + $this->acceptNL_topmargin;
        $delta = 11;
        $this->_pdf->selectFont($this->BasicFontName);

        $fontsize = $this->RelatieFontSize;
        $this->_pdf->addText($offsetX, $offsetY, $fontsize, $this->BankVerbinding->RekeningNr);
        $fontsize = $this->RelatieFontSize - 1;
        $this->_pdf->addText($offsetX, $offsetY-$delta, $fontsize, "<b>{$this->Academie->Naam}</b>");
        $this->_pdf->addText($offsetX, $offsetY-$delta*2, $fontsize, "<b>".$this->Translate('postbus')." {$this->Postbus->Postbus}, {$this->Postbus->Postcode}  {$this->Postbus->Plaats}</b>");
    }

    function PrintAGNLBodyControleRegel() {
        $fontsize = $this->OCRFontSize;
        $offsetX = 124 + $this->acceptNL_leftmargin;
        $offsetY = 24 + $this->acceptNL_topmargin;
        $this->_pdf->selectFont($this->OCRFontName, 'none');

        $this->_pdf->addText($offsetX, $offsetY, $fontsize, sprintf('%016s+',$this->BetalingsKenmerk));
        if (trim($this->Relatie->GewogenRekeningNr) != '')
            $this->_pdf->addText($offsetX+131, $offsetY, $fontsize, sprintf('%010s<',$this->Relatie->GewogenRekeningNr));
        $this->_pdf->addText($offsetX+218, $offsetY, $fontsize, sprintf('%09s+',$this->TotaalBedragGewogen));
        $this->_pdf->addText($offsetX+340, $offsetY, $fontsize, sprintf('%010s+',$this->BankVerbinding->RekeningNr));
        $this->_pdf->addText($offsetX+427, $offsetY, $fontsize, sprintf('%02s>',$this->FormulierCode));
    }

    function PrintAGNLBody() {
        $this->PrintAGNLBodyInzake();

        // OCR deel
        $this->PrintAGNLBodyBedrag();
        $this->PrintAGNLBodyBetalingsKenmerk();
        $this->PrintAGNLBodyRekeningNr();

        // Relatie deel
        $this->PrintAGNLBodyRelatie();

        // NHA deel
        $this->PrintAGNLBodyNHAGegevens();

        $this->PrintAGNLBodyControleRegel();
    }

    function PrintAcceptGiro_BE() {
        $this->PrintAGBEControleStrook();
        $this->PrintAGBEBody();
    }

    function PrintAGBEControleStrook() {
        $this->PrintAGBEControleStrookBedrag();
        $this->PrintAGBEControleStrookNHAGegevens();
        $this->PrintAGBEControleStrookBetalingsKenmerk();
    }

    function PrintAGBEBody() {
        // OCR deel
        $this->PrintAGBEBodyBedrag();
        $this->PrintAGBEBodyBetalingsKenmerk();
        $this->PrintAGBEBodyRekeningNr();

        // Relatie deel
        $this->PrintAGBEBodyRelatie();

        // NHA deel
        $this->PrintAGBEBodyNHAGegevens();
    }

    function PrintAGBEBodyBedrag() {
        $fontsize = $this->OCRFontSize;
        $bedragX = 440;
        $bedragY = 192;
        $fracX = 533;
        $fracY = $bedragY;
        $this->_pdf->selectFont($this->OCRFontName, 'none');
        $this->_pdf->addText($bedragX+(8-strlen($this->totaal_bedrag))*7, $bedragY, $fontsize, $this->totaal_bedrag);
        $this->_pdf->addText($fracX, $fracY, $fontsize, $this->totaal_frac);
    }

    function PrintAGBEBodyBetalingsKenmerk() {
        $fontsize = $this->RelatieFontSizeBE;
        $inzakeX = 196;
        $inzakeY = 71;
        $this->_pdf->selectFont($this->BasicFontName);
        $this->_pdf->addText($inzakeX,$inzakeY,$fontsize, $this->Inzake);

        $fontsize = $this->RelatieFontSizeBE;
//        $this->_pdf->selectFont($this->OCRFontName);
        $this->_pdf->selectFont($this->BasicFontName);
        $deltaBK = 34;
        $offsetX = 196;
        $offsetY = 46;

        $this->_pdf->addText($offsetX, $offsetY, $fontsize, substr($this->BetalingsKenmerk,0,4));
        $this->_pdf->addText($offsetX+$deltaBK, $offsetY, $fontsize, substr($this->BetalingsKenmerk,4,4));
        $this->_pdf->addText($offsetX+$deltaBK*2, $offsetY, $fontsize, substr($this->BetalingsKenmerk,8,4));
        $this->_pdf->addText($offsetX+$deltaBK*3, $offsetY, $fontsize, substr($this->BetalingsKenmerk,12,4));

        $offsetX = 300;
        $this->_pdf->addText($offsetX+$deltaBK*3, $offsetY, $fontsize, 'Betalen voor: '.$this->VervalDatum);
    }

    function PrintAGBEBodyRekeningNr() {
        $fontsize = $this->OCRFontSizeBE;
        $offsetX = 192;
        $offsetY = 170;
        $this->_pdf->selectFont($this->OCRFontName, 'none');
        $this->_pdf->addText($offsetX, $offsetY, $fontsize, $this->Relatie->RekeningNr);
    }

    function PrintAGBEBodyRelatie() {
        $fontsize = $this->RelatieFontSizeBE-1;
        $offsetX = 195;
        $offsetY = 136;
        $delta = 13;
        $this->_pdf->selectFont($this->BasicFontName);

        $naam = "{$this->Relatie->Voorletters}";
        if ($this->Relatie->TussenVoegsel != '')
            $naam .= " {$this->Relatie->TussenVoegsel}";
        $naam .= " {$this->Relatie->Achternaam}";

        $this->_pdf->addText($offsetX, $offsetY, $fontsize, $naam);
        $this->_pdf->addText($offsetX, $offsetY-$delta, $fontsize, "{$this->Relatie->Straat} {$this->Relatie->HuisNummer}");
        $this->_pdf->addText($offsetX, $offsetY-$delta*2, $fontsize, "{$this->Relatie->Postcode}  {$this->Relatie->Plaats}");
    }

    function PrintAGBEBodyNHAGegevens() {
        $delta = 13;
        $this->_pdf->selectFont($this->BasicFontName);

        $fontsize = $this->RelatieFontSizeBE;
        $offsetX = 430;
        $offsetY = 169;
        $this->_pdf->addText($offsetX, $offsetY, $fontsize, $this->BankVerbinding->RekeningNr);

        $fontsize = $this->RelatieFontSizeBE-1;
        $offsetX = 400;
        $offsetY = 136;
        $this->_pdf->addText($offsetX, $offsetY, $fontsize, "{$this->Academie->Naam}");
        $this->_pdf->addText($offsetX, $offsetY-$delta, $fontsize, $this->Translate('postbus')." {$this->Postbus->Postbus}");
        $this->_pdf->addText($offsetX, $offsetY-$delta*2, $fontsize, "{$this->Postbus->Postcode}  {$this->Postbus->Plaats}");
    }

    function PrintAGBEControleStrookBetalingsKenmerk() {
        $fontsize = $this->RelatieFontSizeBE;
        $offsetX = 25;
        $offsetY = 70;
        $this->_pdf->selectFont($this->BasicFontName);
        $this->_pdf->addText($offsetX,$offsetY,$fontsize, $this->Inzake);

        $offsetX = 25;
        $offsetY = 49;
        $fontsize = $this->OCRFontSizeBE;
        $this->_pdf->selectFont($this->OCRFontName, 'none');
        $this->_pdf->addText($offsetX, $offsetY, $fontsize, $this->BetalingsKenmerk);
    }

    function PrintAGBEControleStrookBedrag() {
        $fontsize = $this->OCRFontSizeBE;
        $bedragX = 25;
        $bedragY = 193;
        $this->_pdf->selectFont($this->OCRFontName, 'none');
        $bedrag = $this->totaal_bedrag.','.$this->totaal_frac;
        $this->_pdf->addText($bedragX+(120-strlen($bedrag)*8), $bedragY, $fontsize, $bedrag);

    }

    function PrintAGBEControleStrookNHAGegevens() {
        $this->_pdf->selectFont($this->BasicFontName);
        $fontsize = $this->RelatieFontSizeBE;

        $offsetX = 20;
        $offsetY = 144;
        $this->_pdf->addText($offsetX, $offsetY, $fontsize, $this->BankVerbinding->RekeningNr);

        $offsetY = 120;
        $this->_pdf->addText($offsetX, $offsetY-$delta, $fontsize, "{$this->Academie->Naam}");
    }

    function _PrintPage($metOfZonderACP) {
        if (($this->_pdf == null)) {
            $this->_pdf = new Creport('a4','portrait');
        }

        $this->_pdf->mainFont = $this->BasicFontName;

        $euro_diff = array(94=>'Euro');
        $this->_pdf->selectFont($this->BasicFontName,
            array('encoding'=>'WinAnsiEncoding','differences'=>$euro_diff));

        $this->_pdf->ezSetMargins(50,50,50,50);
        $this->_pdf->setColor(0,0,0);
        $this->_pdf->selectFont($this->BasicFontName);

        $this->PrintKoppelNr();
        $this->PrintLogo();
        $this->PrintNHAAdres();
        $this->PrintFactuurRelatieAdres();
        $this->PrintBriefInhoud();

        if ($this->Specificatie->totaalFloat > 0) {
            $this->PrintSpecificatie();
        }
        $this->PrintFooter();

        if ($metOfZonderACP == 'ZONDERACP') {
            $this->PrintNietSturen();
        } else {
            $this->PrintAcceptGiro();
        }

        return true;
    }

    function PrintPage($metOfZonderACP, $returnPage=false) {
        // Prevent sprintf from displaying , instead of . (PDF's don't like it)
        $OLDLOCAL = $this->OLDLOCAL;
		setlocale(LC_ALL, null);

        $this->_PrintPage($metOfZonderACP);

        if (isset($_GET['d']) && $_GET['d']){
            $pdfcode = $this->_pdf->output(1);
        //  $end_time = getmicrotime();
    //      $pdfcode = str_replace("\n","\n<br>",htmlspecialchars($pdfcode));
            echo '<html><body><textarea>';
            echo trim($pdfcode);
            echo '</textarea></body></html>';
        } else {
            if ($returnPage) {
//                header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0 Pragma: no-cache');
//                header('Content-Description: File Transfer');
//                header('Content-Type: application/x-download');
//                header('Content-Disposition: attachment; filename=factuur.pdf');
                $result = $this->_pdf->output();
            } else {
                $this->_pdf->ezStream();
                $result = true;
            }
        }

		setlocale(LC_ALL, $OLDLOCAL);
        return $result;
    }

    function BatchPrintPage($metOfZonderACP) {
        $result = $this->_PrintPage($metOfZonderACP);
        return $result;
    }

    function OutputBatchPrint() {
        $result = $this->_pdf->output();
        return $result;
    }

    function ShowPrintPage($metOfZonderACP) {
        $this->PrintPage($metOfZonderACP, $return=false);
        return true;
    }

    function OutputPrintPage($metOfZonderACP) {
        $result = $this->PrintPage($metOfZonderACP, $return=true);
        unset($this->_pdf);
        return $result;
    }

    function NextPage() {
        if (isset($this->_pdf))
            $this->_pdf->ezNewPage();
    }
}

?>
