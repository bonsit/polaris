<?php
require_once "pdffactuurnieuw.class.php";
require_once "pdffactuurNL_IBAN.class.php";
require_once "includes/miscfunc.inc.php";
//require_once "pdffactuur.class.php";
require_once "_rapport.class.php";

define('ntAcceptGiro' , 'ACCEPTGIRO');
define('ntRembours' , 'REMBOURS');
define('ntFranco' , 'FRANCO');
define('ntAutomatisch' , 'AUTOMATISCH');
define('ntOnbekend' , 'ONBEKEND');

class nha1_Factuur extends NHA_RapportObject {
    var $factuur;
    var $factuurrecord;

    //var $cFormuliercode = '13'; // Acceptgiro Bijlageloos, stortingskosten voor betaler
    var $cFormuliercode = '17'; // IBAN ACP -- Acceptgiro Bijlageloos, stortingskosten voor betaler

    function nha1_Factuur($owner, $database='') {
        global $_CONFIG;

        $this->NHA_RapportObject($owner, $database);

        if ($_CONFIG['gebruikibanaccept'] == TRUE) {
            $this->factuur = new FactuurIBAN();
        } else {
            $this->factuur = new FactuurNieuw();
        }
        //$this->factuur = new Factuur();
        $this->factuur->FormulierCode = $this->cFormuliercode;
//        $this->factuur->currentDateTime = $this->currentDateTime;
    }

    function berekenAcceptGiroElfProefCijfer($nummer) {
        $gewichten = array(2,4,8,5,10,9,7,3,6,1);
        $som = 0; $teller = 0; $index = 0; $cijfer = 0; $rest = 0; $controle = 0;
        $nummer = (string) $nummer;
        for ($teller=(strlen($nummer)-1);$teller>=0;$teller--) {
            $cijfer = intval($nummer[$teller]);
            $som = $som + ( $cijfer * $gewichten[$index] );
            $index++;

            if ($index == 10) $index = 0;
        }

        $rest = $som % 11;
        $controle = 11 - $rest;
        if ($controle == 10)
            $controle = 1;
        elseif ($controle == 11)
            $controle = 0;
        $result = $controle;
        return (string) $result;
    }

    function loadFactuur($rowid) {
        if ($rowid != '') {
            $this->database->SetFetchMode(ADODB_FETCH_ASSOC);
            $_sqlFactuur = "SELECT F.*
            , DECODE(F.LANDCODE, 'DE', BTWPERCENTAGE_DE, 'BE', BTWPERCENTAGE_BE, BTWPERCENTAGE_NL) AS BTWPERCENTAGE
            FROM NHA.FACTUUR F, NHA.PARAMETERS WHERE F.ROWID = :RIJ";
            $this->factuurrecord = $this->database->GetRow($_sqlFactuur, array("RIJ"=>$rowid));
        } else {
            $this->factuurrecord = false;
        }
    }

    function outputFactuur($rowid, $metOfZonderACP) {
        $this->loadFactuur($rowid);
        $this->printFactuurBody($metOfZonderACP);
    }

    function printFactuur($rowid, $metOfZonderACP, $printerObjectNL, $printerObjectBE, $printerObjectDE) {
        $this->loadFactuur($rowid);

        if ($this->factuurrecord) {
            if (($this->factuurrecord['BETAALWIJZECODE'] != 'AUT') or ($_GET['param2'] == 'INCAUT')) {
                if ($this->factuurrecord['LANDCODE_RELATIE'] == 'BE')
                    $_printerObject = $printerObjectBE;
                elseif ($this->factuurrecord['LANDCODE'] == 'DE')
                    $_printerObject = $printerObjectDE;
                else
                    $_printerObject = $printerObjectNL;
                $this->printFactuurBody($metOfZonderACP, $_printerObject);
            } else {
                $this->msg("Deze factuur wordt niet afgedrukt. Is een Automatische incasso. Fnr: ".$this->factuurrecord['FACTUURNR']);
            }
        } else {
            $this->msg("Geen factuur gevonden.");
        }
    }

    function printFacturen($rs, $metOfZonderACP, $printerObject, $printInBatch=false) {
        $result = true;

        if ($printInBatch) {
            // Prevent sprintf from displaying , instead of . (PDF's don't like it)
            $OLDLOCAL = $this->factuur->OLDLOCAL;
		    setlocale(LC_ALL, null);
        }

        $_count = count($rs);
        $_index = 0;
        foreach($rs as $_rec) {
            $_index++;
            $this->loadFactuur($_rec['PLR__RECORDID']);
            $_tmpResult = $this->printFactuurBody($metOfZonderACP, $printerObject, $printInBatch);
            if ($_index < $_count)
                $this->factuur->NextPage();
        }

        if ($printInBatch) {

            $_page = $this->factuur->OutputBatchPrint();
            $filename = $printInBatch;
            $filename = $this->SavePage($_page, $filename);
            setlocale(LC_ALL, $OLDLOCAL);
        }

        return $result;
    }

    function getVreemdeValuta($inschrijfnr) {
        $_sql = "SELECT VREEMDEVALUTACODE, DISPLAYVALUTACODE, KOERS
        FROM INSCHRIJVING I, VALUTA_CODE V, VALUTA_KOERS K
        WHERE I.INSCHRIJFNR = :INSCHRIJFNR AND I.VREEMDEVALUTACODE = V.VALUTACODE
        AND V.VALUTACODE = K.VALUTACODE";
        return $this->database->GetRow($_sql, array('INSCHRIJFNR'=>$inschrijfnr));
    }

    function formatBedrag($bedrag, $bold=false, $vreemdevaluta=false, $koers=false) {
        setlocale(LC_MONETARY,'nl_NL');
        if ($vreemdevaluta) {
            $_replacedSign = $vreemdevaluta;
            $bedrag = $bedrag * $koers;
        } else {
            $_replacedSign = '^';
        }
        $_result = str_replace("EUR", $_replacedSign, money_format('%#8n', $bedrag));
        if ($bold) {
            $_result = str_replace("$_replacedSign", "$_replacedSign<b>", $_result);
            $_result = $_result."</b>";
        }
        return $_result;
    }

    function formatRekeningCode($rekening) {
        if ($rekening != '') {
            if (strlen($rekening) < 9) {
                $result = sprintf('%07s', $rekening);
                $result = $this->berekenAcceptGiroElfProefCijfer($result).$result;
            } else {
                $result = sprintf('%010s', $rekening);
            }
        } else {
            $result = '          ';
        }
        return $result;
    }

    function transformUTF8($str) {
        return iconv("UTF-8", "ISO-8859-1//TRANSLIT",$str);
    }

    function printFactuurBody($metOfZonderACP, $printerObject=false, $printInBatch=false) {
        global $polaris;

        $_errorFactuur = false;

        if (!$this->factuurrecord) {
            $_errorFactuur = true;
        }

        $f =& $this->factuur;

        $f->FactuurNr = $this->factuurrecord['FACTUURNR'];
        $f->Landcode = $this->factuurrecord['LANDCODE'];

        /*
        *   Academie gegevens
        */
        if ($this->factuurrecord['ACADEMIEOFPW'] == '') {
            $this->factuurrecord['ACADEMIEOFPW'] = 'NHA';
        }

        if ($this->factuurrecord['ACADEMIEOFPW'] == 'PW') {
            $f->Academie->NaamKort = 'HA';
            $f->Academie->Naam = 'Hobby Academie';
        } elseif ($this->factuurrecord['ACADEMIEOFPW'] == 'HA') {
            $f->Academie->NaamKort = 'NHA';
//            $f->Academie->Naam = 'Nationale Handels Academie';
        } else {
            $f->Academie->NaamKort = $this->factuurrecord['ACADEMIEOFPW'];
        }

        $_sqlAcademie = "
        SELECT ACADEMIEOFPW, OMSCHRIJVING, LOGO_PLR FROM NHA.ACADEMIEOFPW
        WHERE ACADEMIEOFPW = :ACADEMIEOFPW";
        $_academieRow = $this->database->GetRow($_sqlAcademie, array("ACADEMIEOFPW"=>$f->Academie->NaamKort));

        $f->Academie->Naam = $_academieRow['OMSCHRIJVING'];
        $f->Academie->LogoBestand = $_academieRow['LOGO_PLR'];

        /*
        * InschrijfRelatie ophalen
        */
        $_sqlinschrijfRelatie = "
        SELECT R.RELATIENR, R.GESLACHT, R.TUSSENVOEGSEL, R.ACHTERNAAM, R.PLAATS
        FROM NHA.relatie R, NHA.inschrijving I
        WHERE I.inschrijfNr = :inschrijfnr AND
        I.FactuurRelatie is not null and I.CursistNr = R.RelatieNr";
        $_relRow = false;
        if ($this->factuurrecord['INSCHRIJFNR']) {
            $_relRow = $this->database->GetRow($_sqlinschrijfRelatie, array("INSCHRIJFNR"=>$this->factuurrecord['INSCHRIJFNR']));
        }
        if ($_relRow) {
            $f->InschrijfRelatie->RelatieNr = $_relRow['RELATIENR'];
            $f->InschrijfRelatie->Geslacht = $_relRow['GESLACHT'];
            $f->InschrijfRelatie->Tussenvoegsel = $_relRow['TUSSENVOEGSEL'];
            $f->InschrijfRelatie->AchterNaam = $this->transformUTF8($_relRow['ACHTERNAAM']);
            $f->InschrijfRelatie->Plaats = $this->transformUTF8($_relRow['PLAATS']);
        } else {
            $f->InschrijfRelatie->RelatieNr = 0;
        }

        /*
        *   Postbus gegevens
        */
        $_sqlPostbus = "
        SELECT ACADEMIEOFPW, POSTBUSADRESNR, POSTBUS, POSTCODE, PLAATS, LANDCODE FROM NHA.POSTBUSADRES
        WHERE ACADEMIEOFPW = :ACADEMIEOFPW AND POSTBUSADRESNR = :POSTBUSADRESNR";

        $_postbusRow = false;
        if ($this->factuurrecord['POSTBUSADRESNR']) {
            try {
                $_postbusRow = $this->database->GetRow($_sqlPostbus, array("ACADEMIEOFPW"=>$this->factuurrecord['ACADEMIEOFPW'], "POSTBUSADRESNR"=>$this->factuurrecord['POSTBUSADRESNR']));
            } catch (Exception $e) {
                echo "Postbus kon niet opgehaald worden";
                var_dump($this->factuurrecord);
            }
        }
        if (!$_postbusRow) {
            $_sqlPostbusFailback = "
            SELECT ACADEMIEOFPW, POSTBUSADRESNR, POSTBUS, POSTCODE, PLAATS, LANDCODE FROM NHA.POSTBUSADRES
            WHERE ACADEMIEOFPW = :ACADEMIEOFPW AND LANDCODE = :LANDCODE";

            $_postbusRow = $this->database->GetRow($_sqlPostbusFailback, array("ACADEMIEOFPW"=>'NHA', "LANDCODE"=>$this->factuur->AcademieLandCode()));
        }
        $f->Postbus->AdresNr = $_postbusRow['POSTBUSADRESNR'];
        $f->Postbus->Postbus = $_postbusRow['POSTBUS'];
        $f->Postbus->Postcode = $_postbusRow['POSTCODE'];
        $f->Postbus->Plaats = $this->transformUTF8($_postbusRow['PLAATS']);
        $f->Postbus->LandCode = $_postbusRow['LANDCODE'];

        /*
        *   Bankverbinding gegevens
        */
        $_sqlBank = "
        SELECT BANKNR, BANKNAAM, REKENINGNR, PLAATS, LANDCODE, IBANCODE, BICCODE
        FROM NHA.BANKVERBINDING
        WHERE ACADEMIEOFPW = :ACADEMIEOFPW AND BANKNR = :BANKNR";
        $_bankRow = false;
        if ($this->factuurrecord['BANKNR']) {
            $_bankRow = $this->database->GetRow($_sqlBank, array("ACADEMIEOFPW"=>$this->factuurrecord['ACADEMIEOFPW'], "BANKNR"=>$this->factuurrecord['BANKNR']));
        }
        if (!$_bankRow) {
            // not found then
            $_sqlBankFailback = "
            SELECT BANKNR, BANKNAAM, REKENINGNR, PLAATS, LANDCODE, IBANCODE, BICCODE
            FROM NHA.BANKVERBINDING
            WHERE ACADEMIEOFPW = :ACADEMIEOFPW AND LANDCODE = :LANDCODE";
            $_bankRow = $this->database->GetRow($_sqlBankFailback, array("ACADEMIEOFPW"=>'NHA', "LANDCODE"=>$this->factuur->AcademieLandCode()));
        }
        $f->BankVerbinding->BankNr = $_bankRow['BANKNR'];
        $f->BankVerbinding->BankNaam = $this->transformUTF8($_bankRow['BANKNAAM']);
        $f->BankVerbinding->Plaats = $this->transformUTF8($_bankRow['PLAATS']);
        $f->BankVerbinding->RekeningNr = $_bankRow['REKENINGNR'];
        $f->BankVerbinding->LandCode = $_bankRow['LANDCODE'];
        $f->BankVerbinding->IBANCode = $_bankRow['IBANCODE'];
        $f->BankVerbinding->BICCode = $_bankRow['BICCODE'];
        if ($f->BankVerbinding->IBANCode != '') {
            $f->BankVerbinding->RekeningTekst = 'IBAN '.$f->BankVerbinding->IBANCode.'   BIC '.$f->BankVerbinding->BICCode;
        } else {
            $f->BankVerbinding->RekeningTekst = 'Reknr '.$f->BankVerbinding->RekeningNr;
        }

        /*
        *   Eigenadres (NHA) gegevens
        */
        $_eigenadresRow = false;
        $_sqlEigenAdres = "
        SELECT ACADEMIEOFPW, ADRESNR, STRAAT, HUISNR, POSTCODE, PLAATS, LANDCODE, TAALCODE, LOGO_PLR
        FROM NHA.EIGENADRES
        WHERE ACADEMIEOFPW = :ACADEMIEOFPW AND ADRESNR = :EIGENADRESNR";
        if ($this->factuurrecord['EIGENADRESNR']) {
            $_eigenadresRow = $this->database->GetRow($_sqlEigenAdres, array("ACADEMIEOFPW"=>$this->factuurrecord['ACADEMIEOFPW'], "EIGENADRESNR"=>$this->factuurrecord['EIGENADRESNR']));
        }
        if (!$_eigenadresRow) {
            // not found then
            $_sqlEigenAdres = "
            SELECT ACADEMIEOFPW, ADRESNR, STRAAT, HUISNR, POSTCODE, PLAATS, LANDCODE, TAALCODE
            FROM NHA.EIGENADRES
            WHERE ACADEMIEOFPW = :ACADEMIEOFPW AND LANDCODE = :LANDCODE";
            $_eigenadresRow = $this->database->GetRow($_sqlEigenAdresFailback, array("ACADEMIEOFPW"=>'NHA', "LANDCODE"=>$this->factuur->AcademieLandCode()));
        }
        $f->EigenAdres->AdresNr = $_eigenadresRow['ADRESNR'];
        $f->EigenAdres->Postcode = $_eigenadresRow['POSTCODE'];
        $f->EigenAdres->Plaats = $this->transformUTF8($_eigenadresRow['PLAATS']);
        $f->EigenAdres->LandCode = $_eigenadresRow['LANDCODE'];
        $f->EigenAdres->TaalCode = $_eigenadresRow['TAALCODE'];
        $f->EigenAdres->LogoBestand = $_eigenadresRow['LOGO_PLR'];
        $f->EigenAdres->Straat = $this->transformUTF8($_eigenadresRow['STRAAT']);
        $f->EigenAdres->HuisNummer = $_eigenadresRow['HUISNR'];

        /*
        *   Registratie (NHA) gegevens
        */
        $_sqlRegistratie = "
        SELECT ACADEMIEOFPW, LANDCODE, REGISTRATIE, BTWNUMMER FROM NHA.REGISTRATIE
        WHERE ACADEMIEOFPW = :ACADEMIEOFPW AND LANDCODE = :LANDCODE";
        $_registratieRow = $this->database->GetRow($_sqlRegistratie, array("ACADEMIEOFPW"=>$this->factuurrecord['ACADEMIEOFPW'], "LANDCODE"=>$this->factuur->AcademieLandCode()));

        $f->Registratie->LandCode = $this->factuurrecord['LANDCODE'];
        if ($_registratieRow) {
            $f->Registratie->Registratie = $_registratieRow['REGISTRATIE'];
            $f->Registratie->BTWNummer = $_registratieRow['BTWNUMMER'];
        } else {
            $f->Registratie->Registratie = '';
        }

        /*
        *   Afdeling (NHA) gegevens
        */
        $_sqlAfdeling = "
        SELECT LANDCODE, AFDELINGNR, AFDELINGNAAM, DOORKIESNR, HANDTEKENING_PLR FROM NHA.AFDELING
        WHERE LANDCODE = :LANDCODE AND AFDELINGNR = :AFDELINGNR";
        $_afdelingRow = $this->database->GetRow($_sqlAfdeling, array("AFDELINGNR"=>$this->factuurrecord['AFDELINGNR'], "LANDCODE"=>$this->factuur->AcademieLandCode()));

        $f->Afdeling->LandCode = $_afdelingRow['LANDCODE'];
        $f->Afdeling->AfdelingNr = $_afdelingRow['AFDELINGNR'];
        $f->Afdeling->AfdelingNaam = $_afdelingRow['AFDELINGNAAM'];
        $f->Afdeling->DoorKiesNr = $_afdelingRow['DOORKIESNR'];
        $f->Afdeling->HandtekeningBestand = $_afdelingRow['HANDTEKENING_PLR'];
        try {
            $stmt = $this->database->PrepareSP("
            BEGIN
                NHA.PackFacturen.Maakbegleidendebrieftekst_PLR(:factuurnr, :briefinhoud);
            END;");

            $factuurnr = $this->factuurrecord['FACTUURNR'];
            $this->database->InParameter($stmt,$factuurnr,'factuurnr');
            $this->database->OutParameter($stmt,$briefinhoud,'briefinhoud');
            $this->database->Execute($stmt);

            $briefinhoud = $this->transformUTF8($briefinhoud);
        } catch (Exception $e) {
            $this->msg("Factuur {$this->factuurrecord['FACTUURNR']} heeft geen briefinhoud");
            $briefinhoud = '';
        }
        $f->BegeleidendeBrief->Inhoud = $briefinhoud;
        /*
        * Relatie gegevens
        */
        $f->Relatie->RelatieNr = $this->factuurrecord['RELATIENR'];
        $f->Relatie->BedrijfsNaam = $this->transformUTF8($this->factuurrecord['BEDRIJFSNAAM']);
        $f->Relatie->AfdelingsNaam = $this->transformUTF8($this->factuurrecord['AFDELINGNAAM']);
        $f->Relatie->Aanhef = $this->factuurrecord['AANHEF_RELATIE'];
        $f->Relatie->Voorletters = $this->factuurrecord['VOORLETTERS_RELATIE'];
        $f->Relatie->TussenVoegsel = $this->factuurrecord['TUSSENVOEGSEL_RELATIE'];
        $f->Relatie->Achternaam = $this->transformUTF8($this->factuurrecord['ACHTERNAAM_RELATIE']);
        $f->Relatie->Straat = $this->transformUTF8($this->factuurrecord['STRAAT_RELATIE']);
        $f->Relatie->HuisNummer = $this->factuurrecord['HUISNR_RELATIE'];
        $f->Relatie->Postcode = $this->factuurrecord['POSTCODE_RELATIE'];
        $f->Relatie->Plaats = $this->transformUTF8($this->factuurrecord['PLAATS_RELATIE']);
        $f->Relatie->LandCode = $this->factuurrecord['LANDCODE_RELATIE'];
        if ($f->Relatie->LandCode == 'NL' and $f->Relatie->Postcode != '' and $f->Relatie->HuisNummer != '') {
            $f->Relatie->KIXCode = $f->Relatie->Postcode.$f->Relatie->HuisNummer;
        } else {
            $f->Relatie->KIXCode = '';
        }
        $f->Relatie->Geslacht = 'O';
        if (strpos('/', $f->Relatie->Aanhef) === false) {
            if (strpos('vr', $f->Relatie->Aanhef) === false and strpos('fr', $f->Relatie->Aanhef) === false) {
                $f->Relatie->Geslacht = 'M';
            } else {
                $f->Relatie->Geslacht = 'V';
            }
        }
        // $f->Relatie->RekeningNr = intval($this->factuurrecord['REKENINGOFGIRONR_RELATIE']) > 0 ? $this->factuurrecord['REKENINGOFGIRONR_RELATIE'] : '';
        // rfc 33
        $f->Relatie->RekeningNr = $this->factuurrecord['REKENINGOFGIRONR_RELATIE'];
        $f->Relatie->GewogenRekeningNr = $this->formatRekeningCode($f->Relatie->RekeningNr);

        if ($f->Relatie->LandCode != 'NL' and $f->Relatie->LandCode != 'BE' and $f->Relatie->LandCode != 'DE') {
            $_sqlLandCode = "
            SELECT LANDCODE, LANDNAAM FROM NHA.LAND
            WHERE LANDCODE = :LANDCODE";
            $_landRow = $this->database->GetRow($_sqlLandCode, array("LANDCODE"=>$f->Relatie->LandCode));

            $f->Relatie->LandNaam = $this->transformUTF8($_landRow['LANDNAAM']);
        } else {
            $f->Relatie->LandNaam = '';
        }

        /*
        * Inschrijving Relatie gegevens
        */
        $_sqlFactuurRelatie = 'SELECT R.RELATIENR, R.BTWNUMMER
        FROM NHA.relatie R, NHA.inschrijving I
        WHERE I.inschrijfNr = :INSCHRIJFNR AND I.FactuurRelatie = R.RelatieNr';
        $_factuurRelatieRow = $this->database->GetRow($_sqlFactuurRelatie, array("INSCHRIJFNR"=>$this->factuurrecord['INSCHRIJFNR']));
        if (!$_factuurRelatieRow) {
            $_sqlBetalendeRelatie = 'SELECT R.BTWNUMMER
            FROM NHA.relatie R
            WHERE R.RELATIENR = :RELATIENR';
            $_factuurRelatieRow = $this->database->GetRow($_sqlBetalendeRelatie, array("RELATIENR"=>$this->factuurrecord['RELATIENR']));
        }
        if ($_factuurRelatieRow) {
            $f->Relatie->BTWNummer = $_factuurRelatieRow['BTWNUMMER'];
        } else {
            $f->Relatie->BTWNummer = '';
        }

        $f->DateringDatum = date("d-m-Y");
        $f->InschrijfNr = $this->factuurrecord['INSCHRIJFNR'];
        $f->Inzake = $this->factuurrecord['INZAKE'];
        $f->VervalDatum = $this->factuurrecord['VERVALDATUM'];
        $f->KoppelNr = $this->factuurrecord['KOPPELNR'];
        $f->AcceptGiroNr = $this->factuurrecord['ACCEPTGIRONR'];
        $f->Bedrag = $this->factuurrecord['BEDRAGINEUROS'];
        $f->BetalingsKenmerk = sprintf("%07s",   $f->Relatie->RelatieNr)."".sprintf("%08s",   $f->FactuurNr);
        $f->BetalingsKenmerk = $this->berekenAcceptGiroElfProefCijfer($f->BetalingsKenmerk).$f->BetalingsKenmerk;
        $f->ZuivereFactuur = $this->factuurrecord['AANMANINGSKOSTENINEUROS'] == '';

        $f->printDateTime = isset($this->factuurrecord['VERWERKDATUMTIJD'])?$this->factuurrecord['VERWERKDATUMTIJD']:$this->getCurrentDateTime();

        if ($this->factuurrecord['BETAALWIJZECODE'] == 'ACP')
            $f->NotaType = ntAcceptGiro;
        elseif ($this->factuurrecord['BETAALWIJZECODE'] == 'AUT')
            $f->NotaType = ntAutomatisch;
        elseif ($this->factuurrecord['BETAALWIJZECODE'] == 'REM')
            $f->NotaType = ntRembours;
        elseif ($this->factuurrecord['BETAALWIJZECODE'] == 'FRA')
            $f->NotaType = ntFranco;
        else
            $f->NotaType = ntOnbekend;

        $this->database->BeginTrans(); // do a transaction because without it the Select cannot see the data which was Inserted by the Stored Proc (auto-commit)

        $stmt = $this->database->PrepareSP("
        BEGIN
            NHA.PackComplexQueryspeed.FillFactuurSpecificatie(:FACTUURNR);
        END;");
        $factuurnr = $this->factuurrecord['FACTUURNR'];
        $this->database->InParameter($stmt, $factuurnr, 'FACTUURNR');
        $result = $this->database->Execute($stmt);

        $_vvs = $this->getVreemdeValuta($this->factuurrecord['INSCHRIJFNR']);
        $_vv = $_vvs['DISPLAYVALUTACODE'];
        $_krs = $_vvs['KOERS'];

        $_sqlFactuurSpecificatie = "
        SELECT OMSCHRIJVING, TERMIJNNR, VERVALDATUM, SUBTOTAAL, AANTAL, BETALINGSVERZOEKNR, PRIJSPERSTUKINEUROS, VERWIJDERINGSBIJDRAGEPERSTUK
        FROM NHA.FACTUURSPECIFICATIE
        WHERE FACTUURNR = :FACTUURNR
        ORDER BY TERMIJNNR, NVL(BETALINGSVERZOEKNR,0), OMSCHRIJVING";
        $this->database->SetFetchMode(ADODB_FETCH_ASSOC);
        $_specsData = $this->database->GetAll($_sqlFactuurSpecificatie, array("FACTUURNR"=>$this->factuurrecord['FACTUURNR']));
        $totaalSpecs = 0.0;
        foreach($_specsData as $_i => $_rec) {
            $_subtotaal = floatval($_rec['SUBTOTAAL']);
            $totaalSpecs = $totaalSpecs + $_subtotaal;
            $_rec['SUBTOTAAL'] = $this->formatBedrag($_subtotaal, false, $_vv, $_krs);
            $_rec['OMSCHRIJVING'] = $this->transformUTF8($_rec['OMSCHRIJVING']);
            $_specsData[$_i] = $_rec;
        }
        $f->Specificatie->items = $_specsData;

        $f->Specificatie->totaalFloat = $totaalSpecs;
        $f->Specificatie->totaal = $this->formatBedrag($totaalSpecs, $bold=true, $_vv, $_krs);
        $_totaal = $this->factuurrecord['BEDRAGINEUROS'];
        if ($_krs)
            $_totaal = $_totaal * $_krs;
        $f->TotaalBedragGewogen = sprintf('%08s',intval(round($_totaal*100)));
        $f->TotaalBedragGewogen = $f->TotaalBedragGewogen.$this->berekenAcceptGiroElfProefCijfer($f->TotaalBedragGewogen);

        $_totfloatvalue = floatval($_totaal);
        $f->totaal_bedrag = floor($_totfloatvalue);
        $f->totaal_frac = sprintf('%02s', round($_totfloatvalue - floor($_totfloatvalue), 2) * 100);

        $f->BTWpercentage = $this->factuurrecord['BTWPERCENTAGE'];
        $f->Specificatie->btwbedrag = $this->formatBedrag($totaalSpecs / (100 + $f->BTWpercentage) * $f->BTWpercentage, false, $_vv, $_krs);
        $f->BTWtonen = ($this->factuurrecord['LANDCODE'] == 'DE' && $this->factuurrecord['ACADEMIEOFPW'] == 'HA');

        $this->database->CommitTrans();

        try {
            if ($printerObject) {
                if ($printInBatch) {
                    $f->BatchPrintPage($metOfZonderACP);
                } else {
                    $_page = $f->OutputPrintPage($metOfZonderACP);

                    $filename = $this->SavePage($_page);

                    // print the file
                    $this->_DrukBestandAf($filename, $printerObject);

                    if (!$this->debug) {
                        // delete the temporary file
                        unlink($filename);
                    }
                }
            } else {
                $f->ShowPrintPage($metOfZonderACP);
            }
        } catch (Exception $e) {
            $_skippedFacturen[] = $this->factuurrecord['FACTUURNR'];
            $_errorFactuur = true;
        }

        if ($this->debug) {
            if ($_errorFactuur) {
                $this->msg("Factuurnr: ".$this->factuurrecord['FACTUURNR']." niet afgedrukt ($filename).");
            } else {
                $this->msg("Factuurnr: ".$this->factuurrecord['FACTUURNR']." afgedrukt ($filename).");
                $this->ProcessFactuurVerwerktJaNee();
            }
        } else {
            if (!$_errorFactuur) {
                if ($printerObject) {
                    $this->ProcessFactuur();
                } else {
                    $this->ProcessFactuurVerwerktJaNee();
                }
            }
        }

        if ($printerObject) {
            if ($_errorFactuur) {
                $polaris->logQuick("Factuurnr fout: ".$this->factuurrecord['FACTUURNR']." niet afgedrukt ($filename).");
            }
        }

//        $f->NextPage();

        return true;
    }

    function SavePage($page, $filename=false) {
        if (!$filename) {
            // create a temporary file
            $filename = tempnam(sys_get_temp_dir(), "PLRPRINT");
        }

        if (file_exists($filename))
            chmod($filename, 0755);
        $handle = fopen($filename, "w");
        fwrite($handle, $page);
        fclose($handle);
        return $filename;
    }

    function ProcessFactuur() {
        global $polaris;

        $_sql = "UPDATE FACTUUR SET BETALINGSKENMERKACCEPTGIRO = :KENMERK, VERWERKDATUMTIJD = SYSDATE WHERE FACTUURNR = :FACTUURNR AND VERWERKDATUMTIJD IS NULL";
        $this->database->Execute($_sql, array("FACTUURNR"=>$this->factuurrecord['FACTUURNR'], "KENMERK"=>$this->factuur->BetalingsKenmerk));
        $_sql = "UPDATE FACTUUR SET AANTALKERENVERWERKT = AANTALKERENVERWERKT + 1, SPOOLDATUMTIJD = SYSDATE WHERE FACTUURNR = :FACTUURNR";
        $this->database->Execute($_sql, array("FACTUURNR"=>$this->factuurrecord['FACTUURNR']));
    }

    function ProcessFactuurVerwerktJaNee() {
        global $polaris;

        $_sql = "UPDATE FACTUUR SET VERWERKTJANEE = 'J', VERWERKDATUMTIJD = SYSDATE WHERE FACTUURNR = :FACTUURNR";
        $this->database->Execute($_sql, array("FACTUURNR"=>$this->factuurrecord['FACTUURNR']));
    }

}

?>