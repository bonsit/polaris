<?php
require "_rapport.class.php";
set_time_limit(1440); // rapporten kunnen lang duren, max 1440 seconden = 24 mins

class TotalenPickLijst extends NHA_RapportObject  {

    var $aantalInBatch;
    var $inBatch = true;

    function TotalenPickLijst($owner, $databaseobject='') {
        $this->NHA_RapportObject($owner, $databaseobject);
    }

    function AutoPrintActief() {
        $_sql = 'SELECT TPL_AUTOPRINT FROM PARAMETERS WHERE ROWNUM <= 1';
        $_result = $this->database->GetOne($_sql);
        return ($_result == 'Y');
    }

    function AutoPrintOptie($_enable) {
        $_YN = ($_enable ? 'Y' : 'N');
        $_sql = 'UPDATE PARAMETERS SET TPL_AUTOPRINT = :AANUIT';
        $_result = $this->database->Execute($_sql, array('AANUIT'=>$_YN));
        return ($_result);
    }

    function GetDataSource($ROWIDS, $afgedruktKolomNaam, $datasourceFilter, $afTeDrukkenData=false, $inBatch=false) {
        if ($ROWIDS != '') {
            /***
            * Meerdere rowids of een enkele
            */
            $rowid = $this->dbobject->customUrlDecode($ROWIDS);
            $rowid = str_replace(',','\',\'',$rowid);
            $filter = 'T.ROWID IN (\''.$rowid.'\')';
        }
        if (!$this->nogEensAfdrukken) {
            if ($filter != '') {
                $filter .= " AND ";
            }
            $filter .= " $afgedruktKolomNaam IS NULL";
        }

        if (isset($afTeDrukkenData) and $afTeDrukkenData !== FALSE) {
            if ($filter != '') {
                $filter .= " AND ";
            }
        }
        if (isset($afTeDrukkenData) and $afTeDrukkenData !== FALSE) {
            $filter .= " ( ";
            foreach($afTeDrukkenData as $_datum) {
                $filter .= " Datum = TO_DATE('{$_datum}', 'DD-MM-YYYY') OR ";
            }
            $filter = substr($filter, 0, -3);
            $filter .= " ) ";
        }

        $_zoekTPL = "SELECT DATUM, RAPPORTNR, EERSTEVERVOLGOFARTIKEL, $afgedruktKolomNaam , ROWIDTOCHAR(ROWID) AS PLR__RECORDID
        FROM TOTALENPICKLIJST T
        WHERE ".$filter." ORDER BY LANDCODE ASC, EERSTEVERVOLGOFARTIKEL, PRIORITEIT, ACADEMIEOFPW DESC, DATUM ASC, RAPPORTNR";
        $rsTPL = $this->database->GetAll($_zoekTPL);

        if ($this->debug) $this->msg($_zoekTPL);

        $_resultTPL = null;
        $i = 1;
        foreach($rsTPL as $k => $row) {
            if ($this->$datasourceFilter($k, $row)) {
                if ($inBatch) {
                    $_insertTPL = "INSERT INTO TEPRINTEN_TOTALENPICKLIJSTEN (Datum, Rapportnr, Volgnr)
                    VALUES('{$row['DATUM']}', {$row['RAPPORTNR']}, $i)";
                    $rsTPL = $this->database->Execute($_insertTPL);
                    if ($this->debug) $this->msg($_insertTPL);
                    $i++;
                } else {
                    $_resultTPL[] = $row;
                }
            }
        }
        return $_resultTPL;
    }

    function totalenPickLijstFilter($index, $row) {
        if ($this->aantalInBatch !== FALSE) {
            if ($index <= $this->aantalInBatch - 1)
                return true;
            else
                return false;
        } else {
            return true;
        }
    }

    function Afdrukken($params, $rowids) {
        $_result = true;
        $this->afgedruktKolomNaam = 'PRINTDATUMTOTALENPICKLIJST';

        parent::Afdrukken($params);

        $this->selectedPrinterNL = $this->GetPrinterObject($params['SELECTEDPRINTER']);
        $this->selectedPrinterBE = $this->GetPrinterObject($params['SELECTEDPRINTER6']);
        $this->selectedPrinterDE = $this->GetPrinterObject($params['SELECTEDPRINTER7']);
        $this->nogEensAfdrukken = ($params['param1'] == 'NOGEENS');

        if (isset($params['param2']))
            $this->aantalInBatch = intval($params['param2']);
        else
            $this->aantalInBatch = false;

        $this->inBatch = TRUE; //($params['param4'] == 'INBATCH');

        $this->afTeDrukkenData = $params['param3'];
        $this->rapportNaam = 'Dagpicklijst';

        /**
        *   Haal alle picklijsten op die afgedrukt moeten worden (alles of aangevinkte records)
        *   Op de gevonden picklijsten wordt nog een filtering toegepast: alleen 'Eerste' zendingen worden afgedrukt
        */

        /* Tijdelijke tabel legen */
        $_sqlTijdelijkeTabelLegen = "DELETE FROM TEPRINTEN_TOTALENPICKLIJSTEN";

        $ok = $this->database->Execute($_sqlTijdelijkeTabelLegen);

        /* Bepaalt welke records worden afgedrukt */
        $_datasourceFilter = "totalenPickLijstFilter";
        $_rsTPL = $this->GetDataSource($rowids, $this->afgedruktKolomNaam, $_datasourceFilter, $this->afTeDrukkenData, $this->inBatch);

        if ($this->inBatch) {
            $_sqlCounter = "SELECT COUNT(*) FROM TEPRINTEN_TOTALENPICKLIJSTEN";
            if ($this->debug) {
                $_countTijdelijk = $this->database->GetOne($_sqlCounter);
                $this->msg('Aantal tijdelijke records: '.$_countTijdelijk);
            }

            $_landCodes = $this->getActieveLandcodes();
            foreach($_landCodes as $_landcode) {
                $_printerNaam = 'selectedPrinter'.$_landcode;
                $_theFilter = "LANDCODE = '$_landcode' AND (TOTALENPICKLIJST.DATUM, TOTALENPICKLIJST.RAPPORTNR) IN (SELECT DATUM, RAPPORTNR FROM TEPRINTEN_TOTALENPICKLIJSTEN)";

                $_countLandRecords = $this->database->GetOne("SELECT COUNT(*) FROM TOTALENPICKLIJST WHERE $_theFilter");
                $this->msg("Aantal tijdelijke records $_landcode: ".$_countLandRecords);
                if ($_countLandRecords > 0) {
                    $_whereFilter = 'DUPLEX=TRUE&WHEREFILTER='.rawurlencode('AND '.$_theFilter); // .' ORDER BY VOLGNR'
                    if (!$_alAfgedrukt or ($_alAfgedrukt and $this->nogEensAfdrukken)) {

                        $_goedAfgedrukt = $this->_DrukRapportAf($this->rapportNaam, $_whereFilter, $this->$_printerNaam);

                        if ($_goedAfgedrukt and !$this->debug) {
                            $_result = true;

                            $_sqlUpdateAfgedrukt = "UPDATE TOTALENPICKLIJST SET {$this->afgedruktKolomNaam} = SYSDATE WHERE $_theFilter";
                            $ok = $this->database->Execute($_sqlUpdateAfgedrukt);

                            $this->database->CompleteTrans();

                            // post print procedure
                            // ... (niks)
                        } else {
                            $_result = false;
                        }
                    }
                }
            }
        } else {
            if (count($_rsTPL) > 0)
            foreach($_rsTPL as $_TPL) {
                $this->database->StartTrans();

                $_alAfgedrukt = $_TPL[$this->afgedruktKolomNaam] != '';

                $_whereFilter = 'DUPLEX=FALSE&WHEREFILTER='.rawurlencode("AND TOTALENPICKLIJST.ROWID = CHARTOROWID('{$_TPL['PLR__RECORDID']}')");

                if (!$_alAfgedrukt or ($_alAfgedrukt and $this->nogEensAfdrukken)) {
                    /**
                    * Afdrukken van rapport
                    */
                    $_goedAfgedrukt = $this->DrukRapportAf($_whereFilter);

                    if ($_goedAfgedrukt) {
                        $_result = true;

                        $this->LijstIsAfgedrukt($this->afgedruktKolomNaam, $_TPL['PLR__RECORDID']);
                        // pre print procedure
                        // ... (niks)

                        $this->database->CompleteTrans();

                        // post print procedure
                        // ... (niks)
                    } else {
                        $_result = false;
                    }
                }
            }
        }

        return $_result;
    }

}

?>