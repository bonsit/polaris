<?php
require "_rapport.class.php";
set_time_limit(1440); // rapporten kunnen lang duren, max 1440 seconden = 24 mins

class BriefMateriaalIncompleet extends NHA_RapportObject  {

    function BriefMateriaalIncompleet($owner, $databaseobject='') {
        $this->NHA_RapportObject($owner, $databaseobject);
    }

    function Filter($row) {
        return true; // Alles kan worden afgedrukt
    }

    function BrievenZijnAfgedrukt($belgie) {
        global $polaris;

        $_whereFilter = $this->GetLandFilter($belgie);

        $_sql = "
        UPDATE INSCHRIJVING SET MATCOMPL_AFGEDRUKT = 'J', MATCOMPL_AFGEDRUKTDATUM = SYSDATE
        WHERE MATERIAALCOMPLEETJANEE = 'N' AND (MATCOMPL_AFGEDRUKT = 'N' OR MATCOMPL_AFGEDRUKT IS NULL) AND $_whereFilter
        ";
//        $polaris->logUserAction('test', session_id(), $_SESSION['name'], $_sql);
        $this->database->Execute($_sql);
        return TRUE;
    }

    function GetLandFilter($belgie, $encode=false) {
        $_whereFilter = "LANDCODE";
        if ($belgie) {
            if ($encode)
                $_whereFilter .= '='.rawurlencode("='BE'");
            else
                $_whereFilter .= "='BE'";
        } else {
            if ($encode)
                $_whereFilter .= '='.rawurlencode("<>'BE'");
            else
                $_whereFilter .= "<>'BE'";
        }
        return $_whereFilter;
    }

    function Afdrukken($params, $rowids) {
        $_result = false;
        $this->afgedruktKolomNaam = 'PRINTDATUMTOTALENPICKLIJST';

        parent::Afdrukken($params);

        $this->selectedPrinter = $this->GetPrinterObject($params['SELECTEDPRINTER']);
        $this->belgie = ($params['param1'] == 'BELGIE');
        $this->nederland = ($params['param1'] == 'NIETBELGIE');
        $this->rapportNaam = 'MateriaalIncompleetNLBE';
        $_datasourceFilter = "Filter";
        $_whereFilter = $this->GetLandFilter($this->belgie, true);
        if ($this->DrukRapportAf($_whereFilter)) {
            $_result = true;
            $this->BrievenZijnAfgedrukt($this->belgie);
        }
        return $_result;
    }

}

?>