<?php
require "_rapport.class.php";

class CallOpmerkingen extends NHA_BaseObject  {

    function CallOpmerkingen($owner, $databaseobject='') {
        $this->NHA_BaseObject($owner, $databaseobject);
    }
    
    function Opslaan($params, &$error) {
        global $polaris;
        
        $_sql = 'INSERT INTO OPMERKING(RELATIENR, INSCHRIJFNR, OPMERKING, MEDEWERKERNR) VALUES (
            :RELATIENR, :INSCHRIJFNR, :OPMERKING, :MEDEWERKERNR)';
        $_relatienr = $params['param1'];
        $_inschrijfnr = $params['param2'];
//        $_datum = $this->getCurrentDate(); -- geen datum, wordt door db afgehandeld (+time)
        $_opmerking = $params['param3'];
        if (strtoupper(substr($_opmerking, 0, 4)) != 'CALL') {
            $_opmerking = 'CALL: '.$_opmerking;
        }
        $_medewerkernr = $_SESSION['userid'];

        try {
            $result = $this->database->Execute($_sql, array(
                'RELATIENR'=>$_relatienr, 'INSCHRIJFNR'=>$_inschrijfnr, 'OPMERKING'=>$_opmerking, 'MEDEWERKERNR'=>$_medewerkernr
            ));
        } catch (Exception $e) {
            $error = $polaris->getPrettyErrorMessage($e->errno, $e->errmsg);
        }
        return $result;
    }
}

?>