<?php
require_once "_rapport.class.php";
set_time_limit(1440); // rapporten kunnen lang duren, max 1440 seconden = 24 mins

class VervolgRapporten extends NHA_RapportObject  {

    function VervolgRapporten($owner, $databaseobject='') {
        $this->NHA_RapportObject($owner, $databaseobject);
    }

    function GetDataSource($landcode, $ROWIDS, $afgedruktKolomNaam, $extraWhereFilter, $datasourceFilter) {
        if ($ROWIDS != '') {
            $rowid = $this->dbobject->customUrlDecode($ROWIDS);
            $rowid = str_replace(',','\',\'',$rowid);
            $filter = 'T.ROWID IN (\''.$rowid.'\') ';
        } else {
            $filter = ' 1 = 2 ';
        }
        if (!$this->nogEensAfdrukken) {
            if ($filter != '') $filter .= ' AND ';
            $filter .= $afgedruktKolomNaam.' IS NULL';
        }

        if ($extraWhereFilter) {
            if ($filter != '') $filter .= ' AND ';
            $filter .= $extraWhereFilter;
        }

        $filter .= " AND LANDCODE = '$landcode' ";

        $_zoekTPL = "SELECT Datum, Rapportnr, Code, AcademieOfPw, EersteVervolgOfArtikel
        , PrintDatumInschrijfFormulieren, PrintDatumFacturen, Prioriteit, ROWIDTOCHAR(ROWID) AS PLR__RECORDID
        , $afgedruktKolomNaam
        FROM TOTALENPICKLIJST T
        WHERE ".$filter."
        ORDER BY T.DATUM DESC, T.RAPPORTNR DESC
        ";

        $this->database->debug = $this->debug;

        $rsTPL = $this->database->GetAll($_zoekTPL);
        $this->database->debug = false;

        $_resultTPL = null;
        foreach($rsTPL as $k => $row) {
            if ($this->$datasourceFilter($row))
                $_resultTPL[] = $row;
        }
        return $_resultTPL;
    }

    function vervolgRapportFilter($row) {
        // als het een studiegids is, dan record is okee.
        $_result = $row['CODE'] == 'S';

        // als het geen studiegids is, dan controleren of er orderpickregels zijn
        if (!$_result) {
            $_sqlControleerRegels = "
              SELECT * FROM ORDERPICKLIJSTREGEL
              WHERE RAPPORTNR = :RAPPORTNR AND GEREALISEERDEDATUM = :DATUM AND ROWNUM < = 1";
            $_regelsAanwezig = $this->database->Execute($_sqlControleerRegels, array('RAPPORTNR'=>$row['RAPPORTNR'],'DATUM'=>$row['DATUM']));
            $_result = !($_regelsAanwezig->EOF);
        }
        return $_result;
    }

    function PostCursistenOplRapport($params, $tplrowid) {
        $this->msg("40 PostCursistenOplRapport");
        $this->executeStoredProc('Packlogistiek.VulOplDatumVerstuurd', array($tplrowid));
    }

    function PrintInschrijfFormulieren($params, $rowids) {
        $rowids = explode(',',$rowids);
        foreach($rowids as $rowid) {
            $this->msg("80 PrintInschrijfFormulierenVanTotalenPicklijst $rowid");
            $result = $this->PrintInschrijfFormulierenVanTotalenPicklijst(false, $params, $rowid, null, null);
        }
        return $result;
    }

    function PrintFacturen($params, $rowids) {
        $rowids = explode(',',$rowids);
        $result = true;
        foreach($rowids as $rowid) {
            $this->msg("90 PrintFacturenVanTotalenPicklijst $rowid");
            $result = $this->PrintFacturenVanTotalenPicklijst(false, $params, $rowid, null, null);
        }
        return $result;
    }

    function PrintInschrijfFormulierenVanTPLPerLand($params, $tplrowid, $landcode) {
        $result = true;
        $this->msg("Landcode ".$landcode);

        $_countryFilter = "A.LANDCODE IN ('$landcode')";
        if ($landcode == 'DE' or $landcode == 'AT' or $landcode == 'CH') {
            $_title = 'Duitsland/Oostenrijk';
            $_rapportNaam = 'inschrijfformulier_DE';
            $_printerObjectIF = $this->GetPrinterObject('InschrijfformulierDuitslandPrinter');
            $_printerObjectWB = $this->GetPrinterObject('WelkomstBriefDEPrinter');
            $_printerObjectPG = $this->GetPrinterObject('PersoonlijkeGegevensDEPrinter');
        } elseif ($landcode == 'BE') {
            $_title = 'België';
            $_rapportNaam = 'inschrijfformulier';
            $_printerObjectIF = $this->GetPrinterObject('InschrijfformulierBelgiePrinter');
            $_printerObjectWB = $this->GetPrinterObject('WelkomstBriefBEPrinter');
            $_printerObjectPG = $this->GetPrinterObject('PersoonlijkeGegevensBEPrinter');
        } else {
            $_title = 'Nederland en rest';
            $_rapportNaam = 'inschrijfformulier';
            $_printerObjectIF = $this->GetPrinterObject('InschrijfformulierPrinter');
            $_printerObjectWB = $this->GetPrinterObject('WelkomstBriefNLPrinter');
            $_printerObjectPG = $this->GetPrinterObject('PersoonlijkeGegevensNLPrinter');
        }

        $_sql = "
            SELECT DISTINCT T3.ONDERTEKENDJANEE, T1.GEREALISEERDEDATUM AS DATUM, T1.RAPPORTNR, T3.INSCHRIJFNR, T3.CURSUSCODE, T2.TERMIJNNR
            , CASE WHEN T3.CURSUSCODE IN (SELECT CURSUSCODE FROM NIEUWEDOCSETCURSUS) THEN 'J' ELSE 'N' END AS NIEUWEDOCSET
            FROM TOTALENPICKLIJST A, ORDERPICKLIJSTREGEL T1, ORDERPICKLIJST T2, INSCHRIJVING T3
            WHERE A.ROWID = CHARTOROWID(:RIJNR)
            AND $_countryFilter
            AND T1.RAPPORTNR = A.RAPPORTNR
            AND T1.GEREALISEERDEDATUM = A.DATUM
            AND T1.ORDERPICKLIJSTNR = T2.ORDERPICKLIJSTNR
            AND T2.VERZOEKNR IS NULL
            --AND T2.TERMIJNNR = 1
            AND T3.INSCHRIJFNR = T2.INSCHRIJFNR
            --AND T3.ONDERTEKENDJANEE = 'N'
            AND STUDIETEMPOCODE <> 'C' AND ROWNUM <= 1";

        $this->msg("30 InschrijfFormulieren voor execute");
        $this->msg("30.2 $_sql ".$tplrowid);
        $_rs = $this->database->Execute($_sql, array('RIJNR'=>"{$tplrowid}"));
        $this->msg("31 InschrijfFormulieren na execute");

        if (!$_rs->EOF) {
            $_rec = $_rs->FetchRow();

            $_filter = "Rapportnr={$_rec['RAPPORTNR']}&Datum={$_rec['DATUM']}";
            if ($_rec['NIEUWEDOCSET'] == 'N') {
                if ($_rec['TERMIJNNR'] == 1) {
                    if ($_rec['ONDERTEKENDJANEE'] == 'N') {
                        $result = $this->_DrukRapportAf($_rapportNaam, $_filter, $_printerObjectIF);
                        $this->msg("45.1 Rapport InschrijfFormulier afgedrukt: $_title");
                    } else {
                        $this->msg("45.1b Rapport InschrijfFormulier NIET afgedrukt omdat al ondertekend is");
                    }
                } else {
                    $this->msg("45.1 Rapport InschrijfFormulier NIET afgedrukt omdat termijnnr > 1");
                }
            } else {
                $_rapportNaamWelkomstBrief = 'WelkomstBrief';
                //$_rapportNaamOrderPickLijstenViaRapport = 'OrderPickLijstenViaRapport';
                $_rapportNaamInschrijfForm = 'inschrijfformulier2015';
                $_rapportNaamPersoonlijkeGegevens = 'PersoonlijkeGegevens';

                $result = $this->_DrukRapportAf($_rapportNaamWelkomstBrief, $_filter, $_printerObjectWB);
                $this->msg("45.5 Rapport WelkomstBrief afgedrukt: $_title");

                if ($_rec['TERMIJNNR'] == 1) {
                    $result = $this->_DrukRapportAf($_rapportNaamPersoonlijkeGegevens, $_filter, $_printerObjectPG);
                    $this->msg("45.4 Rapport PersoonlijkeGegevens afgedrukt: $_title");

                    if ($_rec['ONDERTEKENDJANEE'] == 'N') {
                        $result = $this->_DrukRapportAf($_rapportNaamInschrijfForm, $_filter, $_printerObjectIF);
                        $this->msg("45.3 Rapport Inschrijfformulier2015 afgedrukt: $_title");
                    }
                }
            }

        } else {
            $this->msg("46 Geen inschrijfformulier om af te drukken");
        }
        if ($result)
            $this->LijstIsAfgedrukt('PRINTDATUMINSCHRIJFFORMULIEREN', $tplrowid, $landcode);
        return $result;
    }

    function PrintInschrijfFormulierenVanTotalenPicklijst($landcode, $params, $tplrowid, $rapportnr, $datum) {
        if (!isset($param['INSCHRIJFFORMULIERENAFDRUKKEN']) or (isset($param['INSCHRIJFFORMULIERENAFDRUKKEN']) and $param['INSCHRIJFFORMULIERENAFDRUKKEN'] == 'Y')) {
            if ($landcode) {
                $this->PrintInschrijfFormulierenVanTPLPerLand($params, $tplrowid, $landcode);
            } else {
                $this->PrintInschrijfFormulierenVanTPLPerLand($params, $tplrowid, 'NL'); // Niet Duitsland, Oostenrijk en Zwitserland)
            }

            // Geen inschrijfformulieren meer via verzendafdeling, gaat nu via E-Post en mail
            // $this->PrintInschrijfFormulierenVanTPLPerLand($params, $tplrowid, 'DE'); // Duitsland
            // $this->PrintInschrijfFormulierenVanTPLPerLand($params, $tplrowid, 'AT'); // Oostenrijk
            // $this->PrintInschrijfFormulierenVanTPLPerLand($params, $tplrowid, 'CH'); // Zwitserland
        }
    }

    function PrintFacturenVanTotalenPicklijst($landcode, $params, $tplrowid, $rapportnr, $datum) {
        if (!isset($param['FACTURENAFDRUKKEN']) or (isset($param['FACTURENAFDRUKKEN']) and $param['FACTURENAFDRUKKEN'] == 'Y')) {
            $this->database->BeginTrans();

            $this->msg("52 Maak ZuiverFacturen");
            $this->executeStoredProc('Packfacturen.MaakZuivereFacturen', array($this->getCurrentDate(), $tplrowid));

            $this->database->CommitTrans();

            if ($landcode !== FALSE) {
                $this->msg("55 PrintFacturenVanTotalenPickLijstPerLand $landcode: $tplrowid");
                $result = $this->PrintFacturenVanTotalenPickLijstPerLand($landcode, $tplrowid);

                $this->database->CommitTrans();

                $this->LijstIsAfgedrukt('PRINTDATUMFACTUREN', $tplrowid, $landcode);
            } else {
                foreach($this->getActieveRelatieLandcodes() as $_landcode) {
                    $this->msg("57 PrintFacturenOfTotalenPickLijstPerLand $_landcode: $tplrowid");
                    $result = $this->PrintFacturenVanTotalenPickLijstPerLand($_landcode, $tplrowid);
                    $this->database->CommitTrans();
                    $this->LijstIsAfgedrukt('PRINTDATUMFACTUREN', $tplrowid, $_landcode);
                }
            }
        }
        return $result;
    }

    function PrintInschrijfFormulierenAndFacturen($landcode, $params, $tplrowid, $rapportnr, $datum) {
        //PRINT InschrijfFormulier.rpt [ROWID]
        $this->PrintInschrijfFormulierenVanTotalenPicklijst($landcode, $params, $tplrowid, $rapportnr, $datum);

        $this->PrintFacturenVanTotalenPicklijst($landcode, $params, $tplrowid, $rapportnr, $datum);
    }

    function PostCursistenOplRapportStudy($param, $tplrowid) {
        $this->executeStoredProc('Packlogistiek.VulOplDatumVerstuurd', array($tplrowid));
        $this->executeStoredProc('Packlogistiek.VulVerzoekDatumVerstuurd', array($tplrowid));
    }

    function PrintInfoFormulieren($landcode, $param, $tplrowid, $rapportnr, $datum) {
        $result = true;
        if (!$this->infoFormulierenOverslaan) {
            $_sql = "
                SELECT A.DATUM, A.RAPPORTNR, A.ROWID, A.LANDCODE
                FROM TOTALENPICKLIJST A, TEPRINTENAANVRAGEN I
                WHERE A.DATUM = :DATUM AND A.RAPPORTNR = :RAPPORTNR AND I.RAPPORTNR = A.RAPPORTNR AND I.DATUM = A.DATUM
                AND ROWNUM <= 1
            ";
            $this->msg("131 PrintInfoFormulieren voor execute");

            $_rs = $this->database->GetAll($_sql, array('RAPPORTNR'=>$rapportnr,'DATUM'=>$datum));

            $this->msg("132 PrintInfoFormulieren na execute");

            if (count($_rs) > 0) {
                $_printernaam = 'PLR_PRINTERAANVRAAGFORMULIER';
                if ($landcode == 'BE' or $landcode == 'DE') $_printernaam .= $landcode;
                $_printerObject = $this->GetPrinterObjectViaLogischeNaam($_printernaam);
                $_rapportNaam = 'AanvragenViaRapport';
                $_filter = 'Datum='.$_rs[0]['DATUM'].'&Rapportnr='.$_rs[0]['RAPPORTNR'];
                $result = $this->_DrukRapportAf($_rapportNaam, $_filter, $_printerObject);
            } else {
                $this->msg("38 Geen infoformulier om af te drukken");
            }
        } else {
            $this->msg("39 Infoformulieren worden niet afgedrukt");
        }
        return $result;
    }

    function PrintTotalenPicklijstGroepen($param, $tplrowid) {
        $_sql = "
          SELECT T.RAPPORTNR, T.DATUM
          FROM TOTALENPICKLIJST T
          WHERE T.ROWID = CHARTOROWID(:RIJNR)
          AND T.CODE <> 'S'
          AND T.EERSTEVERVOLGOFARTIKEL = 'V'
          AND T.ACADEMIEOFPW in ('HA', 'PW')";

        $_rs = $this->database->GetAll($_sql, array('RIJNR'=>"{$tplrowid}"));

        $this->msg("31 PrintTotalenPicklijstGroepen na execute");

        if (count($_rs) > 0) {
            $_printerObject = $this->GetPrinterObjectViaLogischeNaam('PLR_PRINTERTOTALENPICKLIJSTGRP');
            $_rapportNaam = 'TotalenpicklijstGroepen';
            $_filter = 'Datum='.$_rs[0]['DATUM'].'&Rapportnr='.$_rs[0]['RAPPORTNR'];
            $this->_DrukRapportAf($_rapportNaam, $_filter, $_printerObject);
        } else {
            if ($params['interactie']) {
            } else {
                echo "Er zijn geen totalenpicklijst groepen!";
            }
            $this->msg("38 Geen TotalenPicklijstGroepen om af te drukken");
        }

// GEEN PRINT DATUM KOLOM  ??      $this->LijstIsAfgedrukt('PRINTDATUMINSCHRIJFFORMULIEREN', $record['PLR__RECORDID']);
    }

    function BepaalOPLPrinterKolomNaam($rec, $landcode) {
        $_sql = "
          SELECT
            DECODE(code, 'A', 'J',
              DECODE(code, 'S', 'N',
                DECODE(EersteVervolgOfArtikel, 'E',
                  DECODE(c.STICKERVENSTERENVELOPE1,'ST','J','N'),
                  DECODE(c.STICKERVENSTERENVELOPEN,'ST','J','N')
                )
              )
            ) AS stickerjanee
          FROM NHA.TOTALENPICKLIJST T, NHA.CURSUS C
          WHERE
            T.ROWID = CHARTOROWID(:RIJNR)
            AND T.code = C.CURSUSCODE (+)
          ";
        $stickerjanee = $this->database->GetOne($_sql, array('RIJNR'=>"{$rec['PLR__RECORDID']}"));

        if ($rec['ACADEMIEOFPW'] == 'PW') {
            // als het een PW lijst is dan afdrukken op speciale PW sticker printer
            $printerObject = $this->GetPrinterObject('PWOrderPickLijstStickerPrinter');
        } else {
            switch ($landcode) {
                case 'DE':
                    $printerObject = $this->selectedPrinter7;
                    break;
                case 'BE':
                    $printerObject = $this->selectedPrinter6;
                    break;
                case 'NL':
                    if ($stickerjanee == 'J') {
                        // als het een sticker lijst is dan afdrukken op sticker papier (maar die wordt nu standaard op gewoon papier gedrukt)
                        $printerObject = $this->selectedPrinter2;
                    } else {
                        // als het geen sticker lijst is dan afdrukken op gewone nederlandse printer
                        $printerObject = $this->selectedPrinter;
                    }
                    break;
                default:
                    $printerObject = $this->selectedPrinter;
                    break;
            }
        }

        return $printerObject;
    }

    function _Afdrukken($landcode, $rs, $afgedruktKolomNaam, $preCommitProcessing, $postCommitProcessing, $params) {
        $_printKolomUpdatenDEBUG = true; // printkolom updaten?
        $_result = true;

        foreach($rs as $_rec) {
            $this->database->BeginTrans();

            $_alAfgedrukt = $_rec[$afgedruktKolomNaam] != '';

            $_reportFilter = "Rapportnr={$_rec['RAPPORTNR']}&Datum={$_rec['DATUM']}";

            if (!$_alAfgedrukt or ($_alAfgedrukt and $this->nogEensAfdrukken)) {

                /**
                * Bepaald naar welke printer/lade het rapport afgedrukt moet worden (ivm stickervellen e.d.)
                */
                $_selectedPrinter = $this->BepaalOPLPrinterKolomNaam($_rec, $landcode);

                /**
                * Afdrukken van enkel rapport naar printer/lade
                */
                $_goedAfgedrukt = $this->_DrukRapportAf($this->rapportNaam, $_reportFilter, $_selectedPrinter);

                if ($_goedAfgedrukt) {
                    /**
                    * Pre Commit procedure
                    */
                    if ($preCommitProcessing != NULL) {
                        $this->$preCommitProcessing($params, $_rec['PLR__RECORDID']);
                    }

                    /**
                    * Pre Commit procedure
                    */
                    if ($_printKolomUpdatenDEBUG) {
                        $this->LijstIsAfgedrukt($afgedruktKolomNaam, $_rec['PLR__RECORDID'], $landcode);
                    }
                    $this->database->CommitTrans();

                    /**
                    * Post Commit procedure
                    */
                    if ($postCommitProcessing != NULL)
                        $this->$postCommitProcessing($landcode, $params, $_rec['PLR__RECORDID'], $_rec['RAPPORTNR'], $_rec['DATUM']);
                    $this->database->CommitTrans();

                } else {
                    $_aantalFoutAfgedrukt++;
                }
            }
        }
        return $_result;
    }

    function AllesAfdrukken($params, $tplrowids) {
        $_result = true;

        $this->msg("00 Start vervolglijsten");

        $this->afgedruktKolomNaam = 'PRINTDATUMCURSISTENPICKLIJST';

        parent::Afdrukken($params);

        $this->nogEensAfdrukken = ($params['param1'] == 'NOGEENS');
        $this->infoFormulierenOverslaan = ($params['param2'] == 'NOPRINTINFOFORM');
        $this->printFacturenLos = ($params['param4'] == 'FACTURENLOS');

        $this->selectedPrinter = $this->GetPrinterObject($params['SELECTEDPRINTER']);
        $this->selectedPrinter2 = $this->GetPrinterObject($params['SELECTEDPRINTER2']);
        $this->selectedPrinter3 = $this->GetPrinterObject($params['SELECTEDPRINTER3']);
        $this->selectedPrinter4 = $this->GetPrinterObject($params['SELECTEDPRINTER4']);
        $this->selectedPrinter5 = $this->GetPrinterObject($params['SELECTEDPRINTER5']);
        $this->selectedPrinter6 = $this->GetPrinterObject($params['SELECTEDPRINTER6']);
        $this->selectedPrinter7 = $this->GetPrinterObject($params['SELECTEDPRINTER7']);

        /**
        *   Haal alle picklijsten op die afgedrukt moeten worden (alles of aangevinkte records)
        *   Op de gevonden picklijsten wordt nog een filtering toegepast: picklijsten moeten gevuld zijn (behalve bij studiegidsen)
        */

        /*  Bepaalt welke records worden afgedrukt */
        $_datasourceFilter = "vervolgRapportFilter";

        foreach($this->getActieveLandcodes() as $_landcode) {
            $this->msg("01 Landcode $_landcode");

            /**
            * Alles behalve de StudieGidsen afdrukken
            */
            $this->rapportNaam = 'OrderPickLijstenViaRapport';
            $_rsTPLGeenStudieGids = $this->GetDataSource($_landcode, $tplrowids, $this->afgedruktKolomNaam, 'CODE <> \'S\'', $_datasourceFilter);
            if ($_rsTPLGeenStudieGids) {
                $_preCommitProces = 'PostCursistenOplRapport';

                //$_postCommitProces = $this->printFacturenLos ? '' : 'PrintInschrijfFormulierenAndFacturen';
                $_postCommitProces = $this->printFacturenLos ? 'PrintInschrijfFormulierenVanTotalenPicklijst' : 'PrintInschrijfFormulierenAndFacturen';
                $_result = $this->_Afdrukken($_landcode, $_rsTPLGeenStudieGids, $this->afgedruktKolomNaam, $_preCommitProces, $_postCommitProces, $params);
            }

            if ($_rsTPLGeenStudieGids and $this->printFacturenLos and ($_landcode !== 'DE')) {
            //if ($_rsTPLGeenStudieGids and $this->printFacturenLos) {
                foreach($_rsTPLGeenStudieGids as $_rec) {
                    $this->PrintFacturenVanTotalenPicklijst($_landcode, $params, $_rec['PLR__RECORDID'], $_rec['RAPPORTNR'], $_rec['DATUM']);
                }
            }

            /**
            * De StudieGidsen afdrukken
            */
            $this->rapportNaam = 'OrderPickLijstenViaRapport';
            $_rsTPLStudieGids = $this->GetDataSource($_landcode, $tplrowids, $this->afgedruktKolomNaam, 'CODE = \'S\'', $_datasourceFilter);
            if ($_rsTPLStudieGids) {
                $_preCommitProces = 'PostCursistenOplRapportStudy';
                $_postCommitProces = 'PrintInfoFormulieren';
                $_result = $this->_Afdrukken($_landcode, $_rsTPLStudieGids, $this->afgedruktKolomNaam, $_preCommitProces, $_postCommitProces, $params);
            }

            if (!isset($_rsTPLGeenStudieGids) and !isset($_rsTPLStudieGids)) {
                // als er geen vervolgrapporten zijn gevonden
                // dan Totalenpicklijst als verwerkt aanduiden
                $this->LijstIsAfgedrukt($this->afgedruktKolomNaam, $tplrowids, $_landcode);
            }
        }

        return $_result;
    }

    function PrintFacturenVanTotalenPickLijstPerLand($landcode, $rowid) {
        if ($landcode == 'BE' or $landcode == 'DE' or $landcode == 'AT' or $landcode == 'CH') {
            $_country .= "A.LANDCODE_RELATIE IN ('$landcode')";
            $_title = "$landcode facturen";
        } else {
            $_country = "A.LANDCODE_RELATIE NOT IN ('BE','DE','AT','CH')";
            $_title = 'Facturen';
        }

        $_sqlFacturen =
          "SELECT DISTINCT A.FACTUURNR, ROWIDTOCHAR(A.ROWID) AS PLR__RECORDID, T2.groepjes_id, A.LANDCODE_RELATIE, A.KOPPELNR
          FROM TOTALENPICKLIJST T, ORDERPICKLIJSTREGEL T1, ORDERPICKLIJST T2, FACTUUR A
          WHERE T.ROWID = CHARTOROWID(:RIJNR) AND $_country AND
          A.KOPPELNR IS NOT NULL AND      /* geen losse facturen factuurrelatie */
          A.ORDERPICKLIJSTNR = T1.ORDERPICKLIJSTNR AND
          T1.ORDERPICKLIJSTNR = T2.ORDERPICKLIJSTNR AND
          T1.RAPPORTNR = T.RAPPORTNR AND
          T1.gerealiseerdedatum = T.DATUM AND
          T2.VERZOEKNR IS NULL
          ORDER BY T2.Groepjes_id, A.LANDCODE_RELATIE, A.KOPPELNR ";

        $this->msg($_sqlFacturen);
        $_rsFacturen = $this->database->GetAll($_sqlFacturen, array('RIJNR'=>"$rowid"));

        if ($this->GetTotalenPickLijst($rowid, $_datum, $_rapportnr)) {
            $_title .= " totalenpicklijst $_datum/$_rapportnr";
        }
        return $this->PrintFacturenPerLandBody($_rsFacturen, $landcode, $_title);
    }

    function PrintFacturenPerLandBody($rs, $landcode, $title, $printInBatchFileName=false, $opnieuwAfdrukken=false) {
        global $polaris;

        require_once('factuur.class.php');

        $_metACP = 'METACP';

        if ($landcode == 'BE')
            $_printerObject = $this->selectedPrinter4;
        elseif ($landcode == 'DE' or $landcode == 'AT' or $landcode == 'CH')
            $_printerObject = $this->selectedPrinter5;
        else
            $_printerObject = $this->selectedPrinter3;

        $nhafactuur = new nha1_Factuur($this, $this->dbobject);

        $result = $nhafactuur->printFacturen($rs, $_metACP, $_printerObject, $printInBatchFileName);

        if ($printInBatchFileName) {
            // zet PDF om naar PS
            $_ps_filename = $this->ConvertReportFile($printInBatchFileName);

            $_deletePDFFiles = true;

            // druk de PS af op de gekozen printer
            $_printerOpties = $this->getPrintOptions($_printerObject);
            $this->PrintReportFile($_ps_filename, $_printerObject['PRINTER_NAAM'], $_printerOpties, $convertToPS=false);

            if (!$opnieuwAfdrukken) {
                if ($this->GetFTPParameters($server, $username, $passw, $remotepath)) {
                    if ($server != '' and $username != '') {
                        if ($this->BackupViaFTP($printInBatchFileName, $server, $username, $passw, $remotepath)) {
                            // alles goed gegaan
                            $this->msg("Backup gelukt: $printInBatchFileName, ${username}@${server}/${remotepath}");
                        } else {
                            $polaris->logQuick("FTP backup ERROR: kon bestand niet wegschrijven: $printInBatchFileName, ${username}@${server}/${remotepath}");
                            $_deletePDFFiles = false;
                        }
                    } else {
                        echo "Er wordt geen backup gemaakt.";
                    }
                } else {
                    $polaris->logQuick("FTP backup ERROR: geen parameters gevonden");
                    $_deletePDFFiles = false;
                }
            }

            // delete the temporary files
            if (!$this->debug) {
                if ($_deletePDFFiles) {
                    // delete the temporary file
                    unlink($printInBatchFileName);
                }
                unlink($_ps_filename);
            }
        }

        return $result;
    }

    function GetFTPParameters(&$server, &$username, &$passw, &$remotepath) {
        $_sqlParameters = 'SELECT FACBCK_FTPSERVER, FACBCK_FTPUSER, FACBCK_FTPPASSW, FACBCK_FTPREMOTEPATH FROM PARAMETERS';

        $params = $this->database->GetRow($_sqlParameters);
        if ($params) {
            // give back the parameters
            $server = $params['FACBCK_FTPSERVER'];
            $username = $params['FACBCK_FTPUSER'];
            $passw = $params['FACBCK_FTPPASSW'];
            $remotepath = $params['FACBCK_FTPREMOTEPATH'];

            return true;
        } else {
            return false;
        }
    }

    function GetTotalenPickLijst($rowid, &$_datum, &$_rapportnr) {
        $_sqlZoekTotalenPicklijst = "
         SELECT DATUM, RAPPORTNR FROM TOTALENPICKLIJST
         WHERE ROWID = CHARTOROWID(:RIJNR)";

        $tpl = $this->database->GetRow($_sqlZoekTotalenPicklijst, array('RIJNR'=>"$rowid"));
        if ($tpl) {
            $_datum = $tpl['DATUM'];
            $_rapportnr = $tpl['RAPPORTNR'];
            $result = true;
        } else {
            echo "";
            $result = true;
        }
        return $result;
    }

    function _PrintLosseFacturen($params, $landcode, $soortAcademie, $soortFactuur, $opnieuwAfdrukken, $opnieuwAfdrukkenDatum, $printInBatch) {
        $_academieFilters = array("NHA" => "= 'NHA'", "HA" => "<> 'NHA' OR A.ACADEMIEOFPW IS NULL"); //"= 'BEL'", "= 'MAX'",

        foreach($_academieFilters as $_soortAc => $_soortFilter) {
            if (($_soortAc == $soortAcademie) or ($soortAcademie == 'ALLES')) {
                $_soortFilter = '(A.ACADEMIEOFPW '.$_soortFilter.')';
                if ($soortFactuur == 'ALLES') {
                    $this->PrintLosseFacturenPerCountry($_filter.$_soortFilter, $landcode, 'ZUIVERE', $_soortAc, $opnieuwAfdrukken, $opnieuwAfdrukkenDatum, $printInBatch);
                    $this->PrintLosseFacturenPerCountry($_filter.$_soortFilter, $landcode, 'VERVALLEN', $_soortAc, $opnieuwAfdrukken, $opnieuwAfdrukkenDatum, $printInBatch);
                    $this->PrintLosseFacturenPerCountry($_filter.$_soortFilter, $landcode, 'VORDERINGEN', $_soortAc, $opnieuwAfdrukken, $opnieuwAfdrukkenDatum, $printInBatch);
                } else {
                    $this->PrintLosseFacturenPerCountry($_filter.$_soortFilter, $landcode, $soortFactuur, $_soortAc, $opnieuwAfdrukken, $opnieuwAfdrukkenDatum, $printInBatch);
                }
            }
        }
    }

    function PrintLosseFacturen($params) {
        $result = true;

        $_soortFactuur = $params['param1'];
        $_regioBE = (($params['param2'] == 'BELGIE') or ($params['param2'] == 'ALLES'));
        $_regioDE = (($params['param2'] == 'DUITSLAND') or ($params['param2'] == 'ALLES'));
        $_regioNL = (($params['param2'] == 'NEDERLAND') or ($params['param2'] == 'ALLES'));
        $_soortAcademie = $params['param3'];

        if ($params['param5'] == 'opnieuwafdrukken') {
            $_opnieuwAfdrukken = true;
            $_opnieuwAfdrukkenDatum = $params['param4'];
        } else {
            $_opnieuwAfdrukken = false;
            $_opnieuwAfdrukkenDatum = false;
        }
        $_printInBatch = ($params['param6'] == 'batch');

        $this->selectedPrinter3 = $this->GetPrinterObject($params['SELECTEDPRINTER3']); // NL
        $this->selectedPrinter4 = $this->GetPrinterObject($params['SELECTEDPRINTER4']); // BE
        $this->selectedPrinter5 = $this->GetPrinterObject($params['SELECTEDPRINTER5']); // DE

        $this->database->BeginTrans();

        /**
        * Eerst de Belgen
        */
        if ($_regioBE) {
            $this->_PrintLosseFacturen($params, 'BE', $_soortAcademie, $_soortFactuur, $_opnieuwAfdrukken, $_opnieuwAfdrukkenDatum, $_printInBatch);
        }

        /**
        * Dan de Duitsers en Oostenrijkers
        * Duitse facturen worden voortaan via Datev afgedrukt (laat code staan in het geval dat er toch via Polaris afgedrukt moet worden)
        */
        if ($_regioDE) {
            $this->_PrintLosseFacturen($params, 'DE', $_soortAcademie, $_soortFactuur, $_opnieuwAfdrukken, $_opnieuwAfdrukkenDatum, $_printInBatch);
        }

        /**
        * Dan de Nederlanders en de rest
        */
        if ($_regioNL) {
            $this->_PrintLosseFacturen($params, 'NL', $_soortAcademie, $_soortFactuur, $_opnieuwAfdrukken, $_opnieuwAfdrukkenDatum, $_printInBatch);
        }

        $this->database->CommitTrans();

        return $result;
    }

    function PrintLosseFacturenPerCountry($filter, $landcode, $soortFactuur, $soortAcademie, $opnieuwAfdrukken, $opnieuwAfdrukkenDatum = false, $printInBatch = false) {
        global $polaris;

        $_vervallenOfVorderingen = ($soortFactuur == 'VERVALLEN' or $soortFactuur == 'VORDERINGEN');

        if ($_vervallenOfVorderingen) {
            if ($soortFactuur == 'VORDERINGEN') {
                $_extraFilter = " A.BEGELEIDENDEBRIEFCODE = 'BRIEFAK4'";
            } else {
                $_extraFilter = " (A.BEGELEIDENDEBRIEFCODE <> 'BRIEFAK4' OR A.BEGELEIDENDEBRIEFCODE IS NULL)";
            }
            $_extraFilter .= ' AND ';
        }

        if ($_vervallenOfVorderingen) {
            $_extraFilter .= '(A.AANMANINGSKOSTENINEUROS IS NOT NULL)'; // AND A.AANMANINGSKOSTENINEUROS <> 0
        } else {
            $_extraFilter .= 'A.AANMANINGSKOSTENINEUROS IS NULL';
        }

        $_extraFilter .= ' AND ';

        if ($landcode == 'DE' or $landcode == 'BE') {
            $_extraFilter .= "(A.LANDCODE_RELATIE IN ('$landcode') OR A.LANDCODE_RELATIE IN (SELECT DISTINCT SPECIAALLANDCODE FROM SPECIAALACADEMIELAND WHERE LANDCODE = '$landcode'))";
        } else {
            $_extraFilter .= "NOT (A.LANDCODE_RELATIE IN ('DE', 'BE') OR A.LANDCODE_RELATIE IN (SELECT DISTINCT SPECIAALLANDCODE FROM SPECIAALACADEMIELAND WHERE LANDCODE IN ('DE', 'BE')))";
        }

        if ($filter != '')
            $_extraFilter .= ' AND '.$filter;

        if ($opnieuwAfdrukken) {
            $_extraFilter .= " AND TRUNC(VERWERKDATUMTIJD) = '$opnieuwAfdrukkenDatum' AND ORDERPICKLIJSTNR IS NULL AND verzenddatum >= (sysdate - 70) and verzenddatum <= sysdate";
            $_sql = "SELECT A.*, ROWIDTOCHAR(A.ROWID) AS PLR__RECORDID FROM NHA.FACTUUR A WHERE $_extraFilter ORDER BY A.FACTUURNR";
        } else {
            $_sql = "SELECT A.*, ROWIDTOCHAR(A.ROWID) AS PLR__RECORDID FROM NHA.FACTUUR A, NHA.LOSSEOPENSTAANDEFACTUREN_SNEL L WHERE A.FACTUURNR = L.FACTUURNR AND $_extraFilter ORDER BY A.FACTUURNR";
        }
        if ($this->debug) $this->msg($_sql);
        $_rs = $this->database->GetAll($_sql);
        $_strvervallen = $_vervallenOfVorderingen?'vervallen/vorderingen':'';
        $_strlandcode = $landcode;

        $this->msg("Aantal facturen: ".count($_rs)." ($filter $_strvervallen $_strlandcode, $_extraFilter)");
        if ($printInBatch) {
            $_tmpsoortfactuur = substr($soortFactuur,0,2);
            $_tmpland = $landcode;
            $_tmpsoortacademie = $soortAcademie;
            $_datum = strtoupper(date('Ymd_His'));
            $_aantalfacturen = count($_rs);
            $printInBatchFileName = sys_get_temp_dir()."/_${_datum}_${_tmpsoortfactuur}_${_tmpsoortacademie}_${_tmpland}_p${_aantalfacturen}.pdf";
        } else {
            $printInBatchFileName = false;
        }

        if (count($_rs) > 0) {
            $this->PrintFacturenPerLandBody($_rs, $landcode, $_title='', $printInBatchFileName, $opnieuwAfdrukken);
        }
    }

}

?>