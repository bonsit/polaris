<?php

require_once "pdffactuurnieuw.class.php";

class FactuurIBAN extends FactuurNieuw {

    function FactuurIBAN() {
        parent::FactuurNieuw();
    }

    /* ACTIVEREN BIJ NIEUWE IBAN ACCEPTGIRO */

    function PrintAGNLBodyControleRegel() {
        $fontsize = $this->OCRFontSize;
        $offsetX = 124 + $this->acceptNL_leftmargin;
        $offsetY = 24 + $this->acceptNL_topmargin;
        $this->_pdf->selectFont($this->OCRFontName, 'none');

        $this->_pdf->addText($offsetX, $offsetY, $fontsize, sprintf('%016s+',$this->BetalingsKenmerk));
        // Op nieuwe Acceptgiro GEEN REKENINGNR VAN RELATIE AFDRUKKEN
        // if (trim($this->Relatie->GewogenRekeningNr) != '')
        //    $this->_pdf->addText($offsetX+101, $offsetY, $fontsize, sprintf('%010s<',$this->Relatie->GewogenRekeningNr));
        $this->_pdf->addText($offsetX+131, $offsetY, $fontsize, sprintf('%09s+',$this->TotaalBedragGewogen));
        $this->_pdf->addText($offsetX+209, $offsetY, $fontsize, sprintf('%08s+',$this->BankVerbinding->BICCode));
        $this->_pdf->addText($offsetX+281, $offsetY, $fontsize, sprintf('%18s+',$this->BankVerbinding->IBANCode));
        $this->_pdf->addText($offsetX+427, $offsetY, $fontsize, sprintf('%02s>',$this->FormulierCode));
    }

    function PrintAGNLBodyRekeningNr() {
        $fontsize = $this->OCRFontSize;
        $offsetX = 180 + $this->acceptNL_leftmargin;
        $offsetY = 194 + $this->acceptNL_topmargin;
        $this->_pdf->selectFont($this->OCRFontName, 'none');
        // GEEN REKENINGNR VAN RELATIE AFDRUKKEN
        // $this->_pdf->addText($offsetX, $offsetY, $fontsize, $this->Relatie->RekeningNr);
    }

    function PrintAGNLControleStrookNHAGegevens() {
        $offsetX = 20 + $this->acceptNL_leftmargin;
        $offsetY = 83 + $this->acceptNL_topmargin;
        $delta = 8;
        $this->_pdf->selectFont($this->BasicFontName);

        $fontsize = $this->RelatieFontSize - 1;
        $this->_pdf->addText($offsetX, $offsetY, $fontsize, $this->BankVerbinding->IBANCode);
        $this->_pdf->addText($offsetX, $offsetY-$delta, $fontsize, "<b>{$this->Academie->NaamKort}</b>");
        $this->_pdf->addText($offsetX, $offsetY-$delta*2, $fontsize, "<b>Postbus {$this->Postbus->Postbus}</b>");
        $this->_pdf->addText($offsetX, $offsetY-$delta*3, $fontsize, "<b>{$this->Postbus->Postcode}  {$this->Postbus->Plaats}</b>");
    }

    function PrintAGNLBodyNHAGegevens() {
        $offsetX = 160 + $this->acceptNL_leftmargin;
        $offsetY = 97 + $this->acceptNL_topmargin;
        $delta = 11;
        $this->_pdf->selectFont($this->BasicFontName);

        $fontsize = $this->RelatieFontSize;
        $this->_pdf->addText($offsetX, $offsetY, $fontsize, $this->BankVerbinding->IBANCode);
        $fontsize = $this->RelatieFontSize - 1;
        $this->_pdf->addText($offsetX, $offsetY-$delta, $fontsize, "<b>{$this->Academie->Naam}</b>");
        $this->_pdf->addText($offsetX, $offsetY-$delta*2, $fontsize, "<b>Postbus {$this->Postbus->Postbus}, {$this->Postbus->Postcode}  {$this->Postbus->Plaats}</b>");
    }

}

?>
