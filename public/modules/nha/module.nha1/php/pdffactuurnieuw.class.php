<?php

require_once "pdffactuur.class.php";

class FactuurNieuw extends Factuur {

    function FactuurNieuw() {
        parent::Factuur();
    }

    function PrintAcceptGiro_BE() {
        $this->PrintAGBEBody();
    }

    function PrintAGBEBody() {
        // OCR deel
        $this->PrintAGBEBodyBedrag();
        $this->PrintAGBEBodyBetalingsKenmerk();

        $this->PrintAGBEBodyRekeningNr();

        // Relatie deel
        $this->PrintAGBEBodyRelatie();

        // NHA deel
        $this->PrintAGBEBodyNHAGegevens();
    }

    function PrintAGBEBodyBedrag() {
        $fontsize = $this->OCRFontSize;
        $bedragX = 478;
        $bedragY = 204;
        $fracX = 562;
        $fracY = $bedragY;
        $this->_pdf->selectFont($this->OCRFontName, 'none');
        $this->_pdf->addText($bedragX+(8-strlen($this->totaal_bedrag))*7, $bedragY, $fontsize, $this->totaal_bedrag);
        $this->_pdf->addText($fracX, $fracY, $fontsize, $this->totaal_frac);
    }

    function PrintAGBEBodyBetalingsKenmerk() {
    /*
        $fontsize = $this->RelatieFontSizeBE;
        $inzakeX = 196;
        $inzakeY = 71;
        $this->_pdf->selectFont($this->BasicFontName);
        $this->_pdf->addText($inzakeX,$inzakeY,$fontsize, $this->Inzake);
    */
        $fontsize = $this->RelatieFontSizeBE;
//        $this->_pdf->selectFont($this->OCRFontName);
        $this->_pdf->selectFont($this->BasicFontName);
        $deltaBK = 34;
        $offsetX = 100;
        $offsetY = 13;

        $this->_pdf->addText($offsetX, $offsetY, $fontsize, substr($this->BetalingsKenmerk,0,4));
        $this->_pdf->addText($offsetX+$deltaBK, $offsetY, $fontsize, substr($this->BetalingsKenmerk,4,4));
        $this->_pdf->addText($offsetX+$deltaBK*2, $offsetY, $fontsize, substr($this->BetalingsKenmerk,8,4));
        $this->_pdf->addText($offsetX+$deltaBK*3, $offsetY, $fontsize, substr($this->BetalingsKenmerk,12,4));
    }

    function PrintAGBEBodyRekeningNr() {
        // alleen IBAN rekeningnr's afdrukken, alle andere negeren
        if (substr($this->Relatie->RekeningNr, 0, 2) == 'BE') {
            $fontsize = $this->OCRFontSizeBE;
            $this->_pdf->selectFont($this->OCRFontName, 'none');
            $offsetX = 100;
            $offsetY = 180;
            $this->_pdf->addText($offsetX, $offsetY, $fontsize, $this->Relatie->RekeningNr);
        }
    }

    function PrintAGBEBodyRelatie() {
        $fontsize = $this->RelatieFontSizeBE-1;
        $offsetX = 100;
        $offsetY = 158;
        $delta = 12;
        $this->_pdf->selectFont($this->BasicFontName);

        $naam = "{$this->Relatie->Voorletters}";
        if ($this->Relatie->TussenVoegsel != '')
            $naam .= " {$this->Relatie->TussenVoegsel}";
        $naam .= " {$this->Relatie->Achternaam}";

        $this->_pdf->addText($offsetX, $offsetY, $fontsize, $naam);
        $this->_pdf->addText($offsetX, $offsetY-$delta, $fontsize, "{$this->Relatie->Straat} {$this->Relatie->HuisNummer}");
        $this->_pdf->addText($offsetX, $offsetY-$delta*2, $fontsize, "{$this->Relatie->Postcode}  {$this->Relatie->Plaats}");
    }

    function PrintAGBEBodyNHAGegevens() {
        $delta = 12;
        $fontsize = $this->OCRFontSizeBE;
        $this->_pdf->selectFont($this->OCRFontName, 'none');

        $offsetX = 100;
        $offsetY = 110;
        $this->_pdf->addText($offsetX, $offsetY, $fontsize, $this->BankVerbinding->IBANCode);

        $offsetY = 85;
        $this->_pdf->addText($offsetX, $offsetY, $fontsize, $this->BankVerbinding->BICCode);

        $fontsize = $this->RelatieFontSizeBE-1;
        $this->_pdf->selectFont($this->BasicFontName);
        $offsetY = 62;
        $this->_pdf->addText($offsetX, $offsetY, $fontsize, "{$this->Academie->Naam}");
        $this->_pdf->addText($offsetX, $offsetY-$delta, $fontsize, "Postbus {$this->Postbus->Postbus}");
        $this->_pdf->addText($offsetX, $offsetY-$delta*2, $fontsize, "{$this->Postbus->Postcode}  {$this->Postbus->Plaats}");
    }

}

?>
