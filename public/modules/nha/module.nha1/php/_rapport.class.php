<?php
require "_base.class.php";
require PLR_DIR."/classes/ftp.class.php";

class NHA_RapportObject extends NHA_BaseObject  {
    var $debug = false;
    var $rapportNaam = '';
    var $selectedPrinter = null;
    var $nogEensAfdrukken = false;
    var $afgedruktKolomNaam;
    var $updateAfgedruktSTMT;
    var $previousTime = null;

    var $ipp;

    function NHA_RapportObject($owner, $databaseobject='') {
        $this->NHA_BaseObject($owner, $databaseobject);

        $this->debug = ($_GET['DEBUG'] == 'DEBUG');
    }

    function microtime_float() {
        list($usec, $sec) = explode(" ", microtime());
        return ((float)$usec + (float)$sec);
    }

    function msg($message) {
        if ($this->debug) {
            $tijd = $this->microtime_float();
            $t = floor(($tijd - ($this->previousTime?$this->previousTime:$tijd)) * 100000);
            echo "<pre style='color:blue'>$message ({$t} millisec)</pre>";
            $this->previousTime = $tijd;
        }
    }

    function getActieveLandcodes() {
        return array('NL', 'BE', 'DE');
    }

    function getActieveRelatieLandcodes() {
        return array('NL', 'BE', 'DE', 'CH', 'AT');
    }

    function getAsFile($url) {
        $_tmpPDF = sys_get_temp_dir()."/_tmp_PDF".$this->microtime_float().".pdf";
        @unlink($_tmpPDF);
        $output = shell_exec('curl -s "'.$url.'" --output '.$_tmpPDF);
        if (trim($output) != '')
            echo trim($output);

        return $_tmpPDF;
    }

    function getAsData($url) {
        return file_get_contents($this->getAsFile($url));
    }

    function GetLogischePrinterNaam($kolomNaam) {
        $_sql = "SELECT $kolomNaam FROM PARAMETERS";
        $printer = $this->database->GetOne($_sql);

        return $printer;
    }

    function GetPrinterObject($printernaam) {
        global $_CONFIG;

        $_printerSQL = "SELECT NAAM, PRINTER_NAAM, PRINTER_NAAM_WIN, PAPIERBRON, PAPIERFORMAAT, OPTIES, OPTIES_CUPS FROM PRINTER_PLR_CUPS WHERE NAAM = :NAAM";
        // geen CacheGetRow ivm het aanroepen van een script via php-cli en de user nha
        // /tmp/84 is aangemaakt door apache, niet door nha
        $rsPrinter = $this->database->GetRow($_printerSQL, array('NAAM'=>$printernaam));
        if ($_CONFIG['ostype'] == 'WINDOWS') {
            $rsPrinter['PRINTER_NAAM'] = $rsPrinter['PRINTER_NAAM_WIN'];
        }

        return $rsPrinter;
    }

    function GetPrinterObjectViaLogischeNaam($kolomNaam) {
        $_printerNaam = $this->GetLogischePrinterNaam($kolomNaam);
        if ($_printerNaam)
            $result = $this->GetPrinterObject($_printerNaam);
        else {
            $result = false;
            echo "Geen Logische printer gevonden.";
        }
        return $result;
    }

    function CombinePDF() {
        $_tmpCompletePDF = sys_get_temp_dir()."/_tmp_PDF_POLARIS".$this->microtime_float().".pdf";

        $_command = "pdfunite";
        foreach (func_get_args() as $pdf) {
            $acc += $n;
            $_command .= " $pdf ";
        }
        $_command .= " $_tmpCompletePDF";

        if ($this->debug) {
            echo "$_command";
        } else {
            shell_exec("$_command");
        }
        return $_tmpCompletePDF;
    }

    function ConvertReportFile($filename) {
        shell_exec("pdf2ps -r600 $filename $filename.ps > /dev/null 2>&1");
        return "$filename.ps";
    }

    function BackupViaFTP($localfilename, $ftpserver, $ftpusername, $ftppassw, $ftpremotepath) {
        $result = true;

        if ($this->debug) {
            echo "Backup: $localfilename on server $ftpserver/$ftpremotepath using $ftpusername";
        } else {
            $ftp = new ftp();

            $ftp->debug = false;

            $_remotefilename = basename($localfilename);
            $_remotefile = $ftpremotepath.$_remotefilename;

            try {
                if ($ftp->ftp_connect($ftpserver)) {
                    if ($ftp->ftp_login($ftpusername,$ftppassw)) {
                        if (!$ftp->ftp_put($_remotefile, $localfilename)) {
                            $result = false;
                        }
                    } else {
                        $result = false;
                    }
                }  else {
                    $result = false;
                }
            } catch (Exception $E) {
                echo "Time out of ander FTP probleem.";
            }
        }
        return $result;
    }

    function PrintReportFile($filename, $printer, $printoptions, $convertToPS=false) {
        global $_CONFIG;

        if ($printoptions)
            $printoptions = "-o media=$printoptions";

        if ($_CONFIG['ostype'] == 'WINDOWS') {
            if ($convertToPS) // wat doen we met convertToPS? voor nu, hetzelfde...
                $_cmd = $_CONFIG['windows_printpdf_path']." -printer \"$printer\" $filename";
            else
                $_cmd = $_CONFIG['windows_printpdf_path']." -printer \"$printer\" $filename";
        } else {
            if ($convertToPS)
                $_cmd = "pdf2ps -r600 $filename - | lpr -P $printer $printoptions";
            else
                $_cmd = "lpr -P $printer $printoptions $filename";
        }

        if ($this->debug) {
            echo "$_cmd  \r\n<br/>";
        } else {
            shell_exec($_cmd);
        }

        return true;
    }

    function _GetGenericJasperReportUrl($rapportnaam, $filter, $jasperreporturl) {
        $rapportnaam = '/reports/Macs/'.$rapportnaam;
        $reporturl = str_replace("%REPORTNAME%",$rapportnaam,$jasperreporturl);
        $reporturl = str_replace("%REPORTPARAMS%",$filter,$reporturl);

        return $reporturl;
    }

    function GetJasperReportUrl($rapportnaam, $filter) {
        global $_CONFIG;
        return $this->_GetGenericJasperReportUrl($rapportnaam, $filter, $_CONFIG["polarisreporturl"]);
    }

    function GetLocalJasperReportUrl($rapportnaam, $filter) {
        global $_CONFIG;
        return $this->_GetGenericJasperReportUrl($rapportnaam, $filter, $_CONFIG["reportserverurl"]);
    }

    function PrintJasperReportURI($rapportnaam, $filter, $printer, $printoptions, $extraprintoptions, $nrofcopies=false) {
        global $_CONFIG;
        // EXAMPLE    $ret = shell_exec("curl \"$reporturl\" | lpr -P HP_LaserJet_P2015_Series_172.16.2.5 -o media=Tray4Optional");

        $reporturl = $this->GetJasperReportUrl($rapportnaam, $filter);

        if ($printoptions)
            $printoptions = "-o media=$printoptions";

        if ($nrofcopies) {
            $_copiesoptions = '-#'.$nrofcopies.' -o Collate=True';
        }

/** EVEN GEEN phpprintipp
        $this->ipp->setHost($_CONFIG["cupsprinterhost"]);
        $this->ipp->setPrinterURI($printer);
        $this->ipp->setData("./testfiles/test-utf8.txt"); // Path to file.
        $this->ipp->printJob();
*/

        if ($_CONFIG['ostype'] == 'WINDOWS') {
            $temp_file = tempnam(sys_get_temp_dir(), 'nh1');
//            $_tempcmd = $_CONFIG['windows_curl_path']." \"$reporturl\" 2 > $temp_file";
            $_tempcmd = $_CONFIG['windows_curl_path']." \"$reporturl\" -o $temp_file";
            $_printcmd = $_CONFIG['windows_printpdf_path']." -printer \"$printer\" $temp_file";

            if ($this->debug) {
                echo "$_tempcmd \r\n<br/>";
                echo "$_printcmd \r\n<br/>";
            } else {
                $_out = shell_exec($_tempcmd); // curl naar temp bestand
                $_ret = shell_exec($_printcmd);
                //unlink($temp_file); // verwijder het tijdelijk bestand
            }
        } else {
            if ($this->debug) {
                echo 'curl -s "'.$reporturl.'" 2>/dev/null | lpr -P '.$printer.' '.$printoptions.' '.$extraprintoptions.' '.$_copiesoptions.' 2>&1 1>/dev/null';
                echo "\r\n<br/>";
            } else {
                $output = shell_exec('curl -s "'.$reporturl.'" 2>/dev/null | lpr -P '.$printer.' '.$printoptions.' '.$extraprintoptions.' '.$_copiesoptions.' 2>&1 1>/dev/null');
                if (trim($output) != '')
                    echo trim($output);
            }
        }

        return true;
    }

    function GetPrintOptions($printerobject) {
        $_printerOpties = $printerobject['PAPIERBRON'];
        if ($printerobject['PAPIERFORMAAT'] != '')
            $_printerOpties .= ','.$printerobject['PAPIERFORMAAT'];
        return $_printerOpties;
    }

    function GetExtraPrintOptions($printerobject) {
        $_extraPrinterOpties = $printerobject['OPTIES_CUPS'];
        return $_extraPrinterOpties;
    }

    function DrukRapportAf($filter, $nrofcopies=false) {
        return $this->_DrukRapportAf($this->rapportNaam, $filter, $this->selectedPrinter, $nrofcopies);
    }

    function _DrukRapportAf($rapportnaam, $filter, $printerobject, $nrofcopies=false) {
        global $_CONFIG;

        $_printerOpties = $this->getPrintOptions($printerobject);
        $_extraPrinterOpties = $this->GetExtraPrintOptions($printerobject);

        if ($printerobject and $_printerOpties) {
            return $this->PrintJasperReportURI($rapportnaam, $filter, $printerobject['PRINTER_NAAM'], $_printerOpties, $_extraPrinterOpties, $nrofcopies);
        } else {
            return false;
        }
    }

    function _DrukBestandAf($filename, $printerobject) {
        $_printerOpties = $this->getPrintOptions($printerobject);
        $_convertToPS = $printerobject['OPTIES'] == 'CONVERTPS';
        return $this->PrintReportFile($filename, $printerobject['PRINTER_NAAM'], $_printerOpties, $_convertToPS);
    }

    function LijstIsAfgedrukt($kolomnaam, $rowid, $landcode) {
        if ($this->debug) {
            $this->msg("Lijst afgedrukt: $kolomnaam: $rowid");
        }
        $rowid = $this->dbobject->customUrlDecode($rowid);
        $rowid = str_replace(',','\',\'',$rowid);
        $filter = 'ROWID IN (\''.$rowid.'\') ';
        $filter .= " AND LANDCODE = '$landcode' ";
        $_sqlUpdateAfgedrukt = "UPDATE TOTALENPICKLIJST SET {$kolomnaam} = SYSDATE WHERE $filter";
        $ok = $this->database->Execute($_sqlUpdateAfgedrukt);
    }

    function Afdrukken($params) {
        // abstract function
    }

    function GetDataSource($ROWID, $afgedruktKolomNaam, $datasourceFilter) {
        // abstract function
    }

    function GetBriefInhoud($briefcode, $rec) {
        $_vanadres = '';
        $_onderwerp = '';
        $_sp = $this->database->PrepareSP("BEGIN NHA.PackLogistiek.MaakStandaardMail(:pRelatieNr, :pInschrijfNr, :pBriefCode, :pBrief, :pVanAdres, :pOnderwerp); END;");
        $this->database->InParameter($_sp, $rec['CURSISTNR'], 'pRelatieNr');
        $this->database->InParameter($_sp, $rec['INSCHRIJFNR'], 'pInschrijfNr');
        $this->database->InParameter($_sp, $briefcode, 'pBriefCode');
        $this->database->OutParameter($_sp, $_brief, 'pBrief');
        $this->database->OutParameter($_sp, $_vanadres, 'pVanAdres');
        $this->database->OutParameter($_sp, $_onderwerp, 'pOnderwerp');
        $_result = $this->database->Execute($_sp);

        $_return = new stdClass();
        $_return->VanAdres = $_vanadres;
        $_return->Onderwerp = $_onderwerp;
        $_return->Inhoud = $_brief;

        return $_return;
    }

    function GetJasperReportAsPDF($rapportnaam, $filter, $output='pdf') {
        $reporturl = $this->GetLocalJasperReportUrl($rapportnaam, $filter);
        $reporturl = str_replace("%REPORTOUTPUT%",$output,$reporturl);

        return $this->getAsFile($reporturl);
    }

    function GetJasperReportAsData($rapportnaam, $filter, $output='pdf') {
        return file_get_contents($this->GetJasperReportAsPDF($rapportnaam, $filter, $output));
    }

    function MailBriefPlusAttachments($mailadres, $vanadres, $onderwerp, $bodytekst, $attachments) {
        global $_CONFIG;

        require_once PLR_DIR.'/php_includes/postmark/postmark_mailer.php';

        echo "Brief $briefnaam met bijlage $reporturl wordt verstuurd naar $mailadres <br/>";
        $postmark = new Postmark($_CONFIG['postmark_api_token'], $vanadres);
        $postmark = $postmark->to($mailadres)
            ->subject($onderwerp)
            ->plain_message($bodytekst);
        foreach($attachments as $_bestandsnaam => $_attachment) {
            $postmark = $postmark->attachment($_bestandsnaam, $_attachment['data'], $_attachment['type']);
        }
        $result = $postmark->send();

        return $result;
    }

}
