<?php
require "nha/module.nha1/php/_base.class.php";

class BriefVoorvertoning extends NHA_BaseObject  {

    function BriefVoorvertoning($owner, $databaseobject='') {
        $this->NHA_BaseObject($owner, $databaseobject);
    }

    function Voorvertoning($params, &$error) {
        global $polaris;

        $_inschrijfnr = $params['param1'];
        $_brief = $params['param2'];

        try {
            $_relatienr = $this->database->GetOne("SELECT CURSISTNR FROM INSCHRIJVING WHERE INSCHRIJFNR = :INSCHRIJFNR", array('INSCHRIJFNR'=>$_inschrijfnr));
            if ($_relatienr == '')
                adodb_plr_throw($this->database, $fn, $errno, 'Vul een bestaand inschrijfnummer in.', $p1, $p2, $this->database, $completemsg);
            $_sp = $this->database->Prepare("
            DECLARE
                BriefTekst VARCHAR2(4000);
            BEGIN
                :BriefTekst := NHA.PackLogistiek.MaakStandaardBrief(:RELATIENR, :INSCHRIJFNR, :CURSUSBRIEF);
            END;
            ");

            $_brieftekst = '';
            $this->database->InParameter($_sp, $_relatienr, 'RELATIENR');
            $this->database->InParameter($_sp, $_inschrijfnr, 'INSCHRIJFNR');
            $this->database->InParameter($_sp, $_brief, 'CURSUSBRIEF');
            $this->database->OutParameter($_sp, $_brieftekst, 'BriefTekst');
            try {
                $this->database->Execute($_sp);
                $result = "<p style=\"font-family: monospace;white-space: pre-wrap;\">".$_brieftekst."</p>";
            } catch (ADODB_Exception $e) {
                $result = false;
                $error = $polaris->getPrettyErrorMessage($e->errno, $e->errmsg);
            }

        } catch (Exception $e) {
            $result = false;
            $error = $polaris->getPrettyErrorMessage($e->errno, $e->errmsg);
            return $result;
        }

        return $result;
    }
}
