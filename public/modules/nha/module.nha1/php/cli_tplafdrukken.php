<?php
/**
    crontab: elke ma, di, wo, do, vr om 07:00 uur
    0 7 * * 1,2,3,4,5 sudo -u apache php /var/www/html/polaris-test/modules/nha/module.nha1/php/cli_tplafdrukken.php
*/
require_once(dirname(__FILE__).'/../../../../includes/app_includes.inc.php');
require_once(dirname(__FILE__).'/totalenpicklijst.class.php');

$_CONFIG["polarisreporturl"] = $_CONFIG["polarisreporthost"].'/showreport/pdf%REPORTNAME%/?%REPORTPARAMS%';

parse_str(implode('&', array_slice($argv, 1)), $_GET);

/**
 * Create the Polaris object
 */
$polaris = new Polaris();
$polaris->initialize();

$clientid = 2;
$databaseid = 3;
$database = new plrDatabase($polaris, array('CLIENTID' => $clientid, 'DATABASEID' => $databaseid) );
$database->LoadRecord();
$database->connectUserDB();

$_sql = '';
$tpl = new TotalenPickLijst($polaris, $database);
if ($tpl->AutoPrintActief()) {
    $params = array('ROWID'=>'', 'param2'=>NULL, 'param4'=>'INBATCH', 'SELECTEDPRINTER'=>'TotalenPickLijstPrinter', 'SELECTEDPRINTER6'=>'TotalenPickLijstPrinterBelgie', 'SELECTEDPRINTER7'=>'TotalenPickLijstPrinterDuitsland', 'DEBUG'=>'DEBUG');
    //$tpl->debug=true;
    $result = $tpl->Afdrukken($params, '');
    var_dump($result);
} else {
    var_dump('Automatisch afdrukken niet actief.');
}