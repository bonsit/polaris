<?php
require "_rapport.class.php";
set_time_limit(1440); // rapporten kunnen lang duren, max 1440 seconden = 24 mins

class BufferInschrijvingen extends NHA_RapportObject  {

    function BufferInschrijvingen($owner, $databaseobject='') {
        $this->NHA_RapportObject($owner, $databaseobject);
    }

    function Filter($row) {
        return true; // Alles kan worden afgedrukt
    }

    function AutoInschrijfFormEmailenActief() {
        $_sql = 'SELECT TPL_AUTOINSCHRFORMEMAIL FROM PARAMETERS WHERE ROWNUM <= 1';
        $_result = $this->database->GetOne($_sql);
        return ($_result == 'J');
    }

    function AutoInschrijfFormEmailenOptie($_enable) {
        $_YN = ($_enable ? 'J' : 'N');
        $_sql = 'UPDATE PARAMETERS SET TPL_AUTOINSCHRFORMEMAIL = :AANUIT';
        $_result = $this->database->Execute($_sql, array('AANUIT'=>$_YN));
        return ($_result);
    }

    function GetRowFilter($ROWIDS) {
        if ($ROWIDS != '') {
            $rowid = $this->dbobject->customUrlDecode($ROWIDS);
            $rowid = str_replace(',','\',\'',$rowid);
            $filter = ' INSCHRIJVING.ROWID IN (\''.$rowid.'\') ';
        } else {
            $filter = FALSE;
        }

        return $filter;
    }

    function GetLandFilter($landcode, $encode=false) {
        $_whereFilter = "LANDCODE";
        if ($encode)
            $_whereFilter .= '='.rawurlencode("='$landcode'");
        else
            $_whereFilter .= "='$landcode'";
        return $_whereFilter;
    }

    function IncompleetBrievenZijnAfgedrukt($landcode) {
        global $polaris;

        $_whereFilter = $this->GetLandFilter($landcode);

        $_sqlSelect = "
        SELECT I.INSCHRIJFNR, I.CURSISTNR, C.MATERIAALVERWACHTDATUM FROM INSCHRIJVING I, CURSUS C
        WHERE I.CURSUSCODE = C.CURSUSCODE
        AND I.MATERIAALCOMPLEETJANEE = 'N' AND (I.MATCOMPL_AFGEDRUKT = 'N' OR I.MATCOMPL_AFGEDRUKT IS NULL) AND $_whereFilter
        ";

        $_rs = $this->database->GetAll($_sqlSelect);

        $_UpdateSQL = "
        UPDATE INSCHRIJVING SET MATCOMPL_AFGEDRUKT = 'J', MATCOMPL_AFGEDRUKTDATUM = SYSDATE
        WHERE INSCHRIJFNR = :inschrijfnr
        ";

        foreach($_rs as $_rec) {
            $this->database->Execute($_UpdateSQL, array('inschrijfnr'=>$_rec['INSCHRIJFNR']));

            $opmerking = 'MATINCOMP Materiaal niet voorradig, materiaal wordt verwacht: '.$_rec['MATERIAALVERWACHTDATUM'].'. Brief verstuurd.';
            $this->OpmerkingMaken($_rec['INSCHRIJFNR'], $_rec['CURSISTNR'], $opmerking);
        }
        return TRUE;
    }

    function MateriaalIncompleetAfdrukken($params, $rowids) {
        $_result = false;

        parent::Afdrukken($params);

        $this->selectedPrinter = $this->GetPrinterObject($params['SELECTEDPRINTER']);
        $this->landcode = $params['param1'];
        $this->rapportNaam = 'MateriaalIncompleetNLBE';
        $_filter = $this->GetRowFilter($rowids);
        $_whereFilter = $this->GetLandFilter($this->landcode, true);
        if ($_filter != '')
            $_whereFilter .= ' AND '.$_filter;
        if ($this->DrukRapportAf($_whereFilter)) {
            $_result = true;
            $this->IncompleetBrievenZijnAfgedrukt($this->landcode);
        }
        return $_result;
    }

    function CohortWachtkamerBriefAfdrukken($params, $rowids) {
        $_result = false;

        $this->rapportNaam = 'BriefCohortWachtkamer';
        $_kolomnaamKeuze = 'WACHTTIJD_AFGEDRUKT';
        $_kolomnaamDatum = 'WACHTTIJD_AFGEDRUKTDATUM';

        $_filter = $this->GetRowFilter($rowids);

        $result = $this->StandaardBriefAfdrukken($params, $_kolomnaamKeuze, $_kolomnaamDatum, $_filter);
        return $result;
    }

    function IsSpecialKolom($kolom) {
        return ($kolom == 'WACHTTIJD_AFGEDRUKT' or $kolom == 'INSFRM_AFGEDRUKT');
    }

    function StandaardBriefAfdrukken($params, $kolomnaamKeuze, $kolomnaamDatum, $filter, $nrofcopies=false) {
        $_result = false;

        parent::Afdrukken($params);

        $this->selectedPrinter = $this->GetPrinterObject($params['SELECTEDPRINTER']);

        if ($filter) $filter = ' AND '.$filter;

        if ($filter)
            $_whereFilter = "WHEREFILTER=".urlencode($filter);
        else
            $_whereFilter = "WHEREFILTER=+AND+1=1"; // where filter die altijd true is

        if ($this->DrukRapportAf($_whereFilter, $nrofcopies)) {
            $_result = true;

            // ingeval Voorlopleiding en Vrijstelling Diploma beide zijn aangevinkt,
            // dan als Vrijstelling Diploma verwerken
            if ($kolomnaamKeuze == 'HEEFTVOOROPLEIDING') {
                $_extrafilter = ' AND NVL(VRIJSTELLINGDIPLOMA,\'N\') = \'N\'';
            }
            if ($kolomnaamKeuze == 'INSFRM_AFGEDRUKT') {
                $_extrafilter = ' AND PRESTATUS = \'BO_ONDERTEKENEN\'';
            }

            if ($this->IsSpecialKolom($kolomnaamKeuze)) {
                $_kolomKeuzeFilter = "";
            } else {
                $_kolomKeuzeFilter = "AND $kolomnaamKeuze = 'J'";
            }

            $filter = str_replace('INSCHRIJVING.ROWID','I.ROWID',$filter);
            $_sql = "
                SELECT I.INSCHRIJFNR, C.CURSUSGROEP, I.CURSISTNR, C.CURSUSCODE
                FROM INSCHRIJVING I, CURSUS C
                WHERE I.CURSUSCODE = C.CURSUSCODE
                $_extrafilter
                $_kolomKeuzeFilter AND NVL(TO_CHAR($kolomnaamDatum), ' ') = ' '
                $filter
            ";

            $_rs = $this->database->GetAll($_sql);
            foreach($_rs as $_rec) {
                /**
                * Vastleggen dat brief is afgedrukt
                */
                $this->BriefIsAfgedrukt($_rec['INSCHRIJFNR'], $kolomnaamKeuze, $kolomnaamDatum);

                /**
                * Opmerkingen aanmaken
                */
                switch ($kolomnaamKeuze) {
                case 'VRIJSTELLINGWERKERVARING':
                    $_opmerking = "VRSTWE Inschrijving met vrijstellingsverzoek obv werkervaring ontvangen, documentatie gestuurd";
                    break;
                case 'VRIJSTELLINGDIPLOMA':
                    if (strpos($_rec['CURSUSCODE'], 'HBO1XOP') === FALSE) {
                        // ingeval van niet OPC
                        $_opmerking = "AANVRST Aanvraagformulier vrijstelling met begeleidende brief en bijlage toegestuurd.";
                    } else {
                        // ingeval van OPC
                        $_opmerking = "VRSTDPL Inschrijving met vrijstellingsverzoek obv diploma's ontvangen, brief gestuurd met vraag naar gewaarmerkte kopieën diploma's.";
                    }
                    break;
                case 'VERKLARING21JAAROFOUDER':
                    if ($_rec['CURSUSGROEP'] == 'HBO') {
                        $_opmerking = "21+Toets Inschrijving ontvangen, op verzoek cursist 21+ toets gestuurd";
                    } else {
                        $_opmerking = "WERKERV Verzoek om toelating op basis van 21+ verklaring ontvangen, brief gestuurd met vraag naar schriftelijke verklaring werkgever.";
                    }
                    break;
                case 'HEEFTVOOROPLEIDING':
                    if ($kolomnaamDatum == 'HEEFTVOOROPL_AFGEDRUKTDATUM2') { // cursist
                        $_opmerking = "DPLCHECK Brief met vraag naar kopieën diploma’s, inschrijfformulier etc. verstuurd.";
                    } else { // backoffice
                        $_opmerking = "DPLCHECK Lesmateriaal wordt verstuurd.";
                    }
                    break;
                case 'WACHTTIJD_AFGEDRUKT':
                    $_opmerking = 'COHORTWACHT Cohortcursus nog niet klaar. Brief verstuurd.';
                }
                $this->OpmerkingMaken($_rec['INSCHRIJFNR'], $_rec['CURSISTNR'], $_opmerking);
            }

        }
        return $_result;
    }

    function StandaardMailVersturen($params, $briefnaam, $kolomnaamKeuze, $kolomnaamDatum, $rec, $attachments, &$error) {
        $result = true;

        $tochverwerken = array();
        if (is_array($params['VERWERKLEGEEMAIL'])) {
            $tochverwerken = array_values($params['VERWERKLEGEEMAIL']);
        }
        $sendResult = FALSE;
        if ($rec['EMAIL'] != '') {
            $filter = 'Inschrijfnr='.$rec['INSCHRIJFNR'];

            $_returnObject = $this->GetBriefInhoud($briefnaam, $rec);
            if ($_returnObject->Inhoud == '') {
                $error = "Mailtekst niet kunnen vinden: $briefnaam";
                $result = false;
                return $result;
            }

            $sendResult = $this->MailBriefPlusAttachments($rec['EMAIL'], $_returnObject->VanAdres, $_returnObject->Onderwerp, $_returnObject->Inhoud, $attachments, $rec);
        }

        $_inschrTochVerwerken = in_array($rec['INSCHRIJFNR'], $tochverwerken) === TRUE;
        if (($sendResult !== FALSE and $sendResult->ErrorCode == 0) or $_inschrTochVerwerken) {
            $_UpdateSQL = "UPDATE INSCHRIJVING SET INSFRM_MAILDATUM = SYSDATE, INSFRM_GEMAILD = 'J' WHERE INSCHRIJFNR = :INSCHRIJFNR";
            $this->database->Execute($_UpdateSQL, array("INSCHRIJFNR"=>$rec['INSCHRIJFNR']));
            if ($_inschrTochVerwerken) {
                $_opmerking = "Emailadres niet bekend, toch verwerkt.";
            } else {
                $_opmerking = "Postmark: Email is correct verstuurd op {$sendResult->SubmittedAt} (MessageID: {$sendResult->MessageID})";
            }
        } else {
            if ($sendResult !== FALSE)
                $_opmerking = "Postmark: ".$sendResult->Message;
            $result = false;
        }
        $this->OpmerkingMaken($rec['INSCHRIJFNR'], $rec['CURSISTNR'], $_opmerking);
        return $result;
    }

    function WerkervaringBTVMaken($relatienr, $inschrijfnr, $verzendonderdeelcode) {
        // BTV aanmaken
        $_sp = $this->database->PrepareSP("
        BEGIN
            PackOrderpickBTV.MaakBTVVanVerzendonderdeel(
                :RelatieNr,
                :InschrijfNr,
                :Verzendonderdeelcode
            );
        END;
        ");

        // rfc45 - Werkervaring niet meer op inschrijfnr, alleen relatienr
        // rfc45 ONGEDAAN GEMAAKT, FACTUREN KWAMEN ER NIET UIT (ZONDER INSCHRIJFNR)
        //$inschrijfnr = null;

        $this->database->InParameter($_sp, $relatienr, 'RelatieNr');
        $this->database->InParameter($_sp, $inschrijfnr, 'InschrijfNr');
        $this->database->InParameter($_sp, $verzendonderdeelcode, 'Verzendonderdeelcode');

        $this->database->Execute($_sp);
    }

    function OpmerkingMaken($inschrijfnr, $relatienr, $opmerking) {
        if (isset($opmerking) and $opmerking !== '') {
            $_sql = "
            INSERT INTO OPMERKING (INSCHRIJFNR, RELATIENR, OPMERKING)
            VALUES (:INSCHRIJFNR, :RELATIENR, :OPMERKING)
            ";
            $this->database->Execute($_sql, array('INSCHRIJFNR'=>$inschrijfnr, 'RELATIENR'=>$relatienr, 'OPMERKING'=>$opmerking));
        }
    }

    function GetRelatieInschrijfNr($rowids) {
        $_filter = $this->GetRowFilter($rowids);
        $_sql = "SELECT INSCHRIJFNR, CURSISTNR AS RELATIENR FROM INSCHRIJVING WHERE $_filter";
        $_rs = $this->database->GetAll($_sql);
        return $_rs;
    }

    function WanbetalingProcedure($rowids) {
        $_rs = $this->GetRelatieInschrijfNr($rowids);
        foreach($_rs as $_rec) {
            $this->OpmerkingMaken($_rec['INSCHRIJFNR'], $_rec['RELATIENR'], 'Screenen WANBET, naar inschrijven. MACS');
        }
        $_filter = $this->GetRowFilter($rowids);
        $_sqlUpdateInsc = 'UPDATE INSCHRIJVING SET OPMERKING = \'WANBET\' WHERE '.$_filter;
        $this->database->Execute($_sqlUpdateInsc);
    }

    function EDRControleAkkoord($rowids) {
        $_rs = $this->GetRelatieInschrijfNr($rowids);
        foreach($_rs as $_rec) {
            $this->OpmerkingMaken($_rec['INSCHRIJFNR'], $_rec['RELATIENR'], 'Screenen oke, naar inschrijven. MACS');
        }
        $_filter = $this->GetRowFilter($rowids);
        $_sqlUpdateInsc = 'UPDATE INSCHRIJVING SET OPMERKING = \'CURSIST\' WHERE '.$_filter;
        $this->database->Execute($_sqlUpdateInsc);
    }

    function BriefIsAfgedrukt($inschrijfnr, $kolomnaamKeuze, $kolomnaamDatum) {
        $this->database->debug=true;
        if ($this->IsSpecialKolom($kolomnaamKeuze)) {
            $_sql = "
            UPDATE INSCHRIJVING SET $kolomnaamDatum = SYSDATE, $kolomnaamKeuze = 'J' WHERE INSCHRIJFNR = :INSCHRIJFNR
            ";
        } else {
            $_sql = "
            UPDATE INSCHRIJVING SET $kolomnaamDatum = SYSDATE WHERE $kolomnaamKeuze = 'J' AND INSCHRIJFNR = :INSCHRIJFNR
            ";
        }
        $this->database->Execute($_sql, array('INSCHRIJFNR'=>$inschrijfnr));
    }

    function Brief21JaarOfOuderAfdrukken($params, $rowids) {
        $this->rapportNaam = 'Brief21OfOuder';
        $_kolomnaamKeuze = 'VERKLARING21JAAROFOUDER';
        $_kolomnaamDatum = 'VERKLARING21_AFGEDRUKTDATUM';

        $_filter = $this->GetRowFilter($rowids);

        $result = $this->StandaardBriefAfdrukken($params, $_kolomnaamKeuze, $_kolomnaamDatum, $_filter);
        return $result;
    }

    function BriefDiplomaAfdrukken($params, $rowids) {
        $this->rapportNaam = 'BriefDiplomas';
        $_kolomnaamKeuze = 'VRIJSTELLINGDIPLOMA';
        $_kolomnaamDatum = 'VRIJSTDIPLOMA_AFGEDRUKTDATUM';

        $_filter = $this->GetRowFilter($rowids);

        $_nrofcopies = 2;
        $result = $this->StandaardBriefAfdrukken($params, $_kolomnaamKeuze, $_kolomnaamDatum, $_filter, $_nrofcopies);
        return $result;
    }

    function BriefWerkervaringAfdrukken($params, $rowids) {
        $this->rapportNaam = 'BriefWerkervaring';
        $_kolomnaamKeuze = 'VRIJSTELLINGWERKERVARING';
        $_kolomnaamDatum = 'VRIJSTWERKERV_AFGEDRUKTDATUM';

        $_filter = $this->GetRowFilter($rowids);

        $result = $this->StandaardBriefAfdrukken($params, $_kolomnaamKeuze, $_kolomnaamDatum, $_filter);

        return $result;
    }

    function BriefVooropleidingAfdrukken($params, $rowids) {
        $this->rapportNaam = 'BriefVooropleiding';
        $_kolomnaamKeuze = 'HEEFTVOOROPLEIDING';
        $_kolomnaamDatum = 'HEEFTVOOROPL_AFGEDRUKTDATUM2';

        $_filter = $this->GetRowFilter($rowids);

        $_nrofcopies = 1;
        $result = $this->StandaardBriefAfdrukken($params, $_kolomnaamKeuze, $_kolomnaamDatum, $_filter, $_nrofcopies);
        return $result;
    }

    function BriefVooropleidingBackOfficeAfdrukken($params, $rowids) {
        $this->rapportNaam = 'BriefVooropleiding';
        $_kolomnaamKeuze = 'HEEFTVOOROPLEIDING';
        $_kolomnaamDatum = 'HEEFTVOOROPL_AFGEDRUKTDATUM';

        $_filter = $this->GetRowFilter($rowids);

        $_nrofcopies = 1;
        $result = $this->StandaardBriefAfdrukken($params, $_kolomnaamKeuze, $_kolomnaamDatum, $_filter, $_nrofcopies);
        return $result;
    }

    function _ToetsenAccorderen($resultaat, $rowids) {
        $_resultaatstring = $resultaat?'J':'N';
        $_filter = $this->GetRowFilter($rowids);

        if ($_filter) {
            $_sql = "
            UPDATE INSCHRIJVING
            SET TOETSINGAFGEHANDELD = 'J' , TOETSINGRESULTAAT = '$_resultaatstring'
            , DEFINITIEFAFGEKEURD = 'N'
            WHERE $_filter
            ";
            $this->database->Execute($_sql);
        }
        return true;
    }

    function ToetsingAkkoord($params, $rowids) {
        $result = $this->_ToetsenAccorderen(true, $rowids);
        if ($result) {
            // indien inschrijvingen worden geaccordeerd, dan worden deze meteen verwerkt (OPL en BTV aamaken)
            $_sp = $this->database->PrepareSP(
                "BEGIN NHA.PackInschrijving.VerwerkGeaccordeerdeInschr(); END;"
            );
            $this->database->Execute($_sp);
        }
        return $result;
    }

    function ToetsingAfgekeurd($params, $rowids) {
        return $this->_ToetsenAccorderen(false, $rowids);
    }

    function VolgtAndereOpleiding($params, $rowids) {
        if (isset($params['param1'])) {
            $_sql = '
            UPDATE INSCHRIJVING
            SET OMGEZETNAARINSCHRIJVING = '.$params['param1'].'
            WHERE '.$this->GetRowFilter($rowids);
            return $this->database->Execute($_sql);
        } else {
            echo "<h1>U dient een inschrijving te selecteren!</h1>";
            return false;
        }
    }

    function VrijstellingOngedaanMaken($params, $rowids) {
        $_filter = $this->GetRowFilter($rowids);
        if ($_filter) {
            $_rs = $this->GetRelatieInschrijfNr($rowids);
            foreach($_rs as $_rec) {
                $this->OpmerkingMaken($_rec['INSCHRIJFNR'], $_rec['RELATIENR'], 'MACS: Cursist ziet af van vrijstelling.');
            }

            // Vrijstellingsaanvraag ongedaan maken
            $_sql = "
            UPDATE INSCHRIJVING
            SET VRIJSTELLINGDIPLOMA = 'N', VRIJSTELLINGWERKERVARING = 'N'
            , HEEFTVOOROPLEIDING = CASE
                WHEN VERKLARING21JAAROFOUDER = 'N' THEN 'J'
                ELSE HEEFTVOOROPLEIDING
                END
            WHERE $_filter
            AND (VRIJSTELLINGDIPLOMA = 'J' OR VRIJSTELLINGWERKERVARING = 'J')
            ";
            $this->database->Execute($_sql);
        }
        return true;
    }

    function ToetsingDefinitiefAfgekeurd($params, $rowids) {
        $_filter = $this->GetRowFilter($rowids);

        if ($_filter) {
            // Afkeuren en PRESTATUS resetten door hem op NULL te zetten
            $_sql = "
            UPDATE INSCHRIJVING
            SET DEFINITIEFAFGEKEURD = 'J', PRESTATUS = ''
            , TOETSINGAFGEHANDELD = 'J', TOETSINGRESULTAAT = 'N'
            WHERE $_filter
            ";
            $this->database->Execute($_sql);
        }
        return true;
    }

    function WijzigCohort($params, $rowids) {
        if (isset($params['param2'])) {
            $_sql = '
            UPDATE INSCHRIJVING
            SET CURSUSCODE = :CURSUS
            WHERE '.$this->GetRowFilter($rowids);
            return $this->database->Execute($_sql, array('CURSUS'=>$params['param2']));
        } else {
            echo "<h1>U dient een cohort te selecteren!</h1>";
            return false;
        }
    }

    function FONabellen($params, $rowids) {
        $_sql = 'UPDATE INSCHRIJVING SET PRESTATUS = \'FO_DPL\' WHERE '.$this->GetRowFilter($rowids);
        return $this->database->Execute($_sql);
    }

    function FOVooropleidingAanpassen($params, $rowids) {
        $_rs = $this->GetRelatieInschrijfNr($rowids);
        $_sqlUpdateInschr = 'UPDATE INSCHRIJVING SET PRESTATUS = \'BO_DPL\' WHERE INSCHRIJFNR = :INSCHRIJFNR';
        $_sqlDelete = 'DELETE FROM VOOROPLEIDING WHERE RELATIENR = :RELATIENR';
        $_sqlInsert = 'INSERT INTO VOOROPLEIDING (RELATIENR, VOOROPLEIDING, DIPLOMAJANEE, GEACCORDEERDJANEE)
        VALUES (:RELATIENR, :VOOROPLEIDING, :DIPLOMAJANEE, \'N\')';
        $_diplomas = array_values($params['DIPLOMAS']);
        foreach($_rs as $_rec) {
            $this->database->Execute($_sqlDelete, array('RELATIENR'=>$_rec['RELATIENR']));
            foreach($params['VOOROPLEIDINGEN'] as $_vooropleiding) {
                $_diploma = in_array($_vooropleiding, $_diplomas)? 'J' : 'N';
                $result = $this->database->Execute($_sqlInsert, array(
                    'RELATIENR' => $_rec['RELATIENR']
                    , 'VOOROPLEIDING' => $_vooropleiding
                    , 'DIPLOMAJANEE' => $_diploma
                ));
            }
            $this->database->Execute($_sqlUpdateInschr, array('INSCHRIJFNR'=>$_rec['INSCHRIJFNR']));
        }
        if (isset($params['VOOROPLEIDING_ANDERS'])) {
            $result = $this->database->Execute($_sqlInsert, array(
                'RELATIENR' => $_rec['RELATIENR']
                , 'VOOROPLEIDING' => $params['VOOROPLEIDING_ANDERS']
                , 'DIPLOMAJANEE' => isset($params['VOOROPLDIPLOMA_ANDERS']) ? 'J' : 'N'
            ));
        }
        return $result;
    }

    function FOAfkeuren($params, $rowids) {
        $_sql = 'UPDATE INSCHRIJVING SET PRESTATUS = \'AFTEKEUREN\' WHERE '.$this->GetRowFilter($rowids);
        $this->database->debug=false;
        return $this->database->Execute($_sql);
    }

    function EDRCheck($params, $rowids) {
        if ($params['param1'] == 'N') {
            $this->WanbetalingProcedure($rowids);
        } else {
            $this->EDRControleAkkoord($rowids);
        }
        $_result = $this->ToetsingAkkoord($params, $rowids);
        return $_result;
    }

    function ExecDatumOpschuiven($params, &$error) {
        global $polaris;

        $_sp = $this->database->PrepareSP("
        BEGIN
            NHA.Packinschrijvingaanpassing.DatumBTVSchuiven(
                :BetalingsVerzoeknr,
                :Verzenddatum,
                :Factuurdatum,
                :Cursistvervaldatum,
                :Systeemvervaldatum
            );
        END;
        ");
        $this->database->InParameter($_sp, $params['param1'], 'BetalingsVerzoeknr');
        $this->database->InParameter($_sp, $params['param2'], 'Verzenddatum');
        $this->database->InParameter($_sp, $params['param3'], 'Factuurdatum');
        $this->database->InParameter($_sp, $params['param4'], 'Cursistvervaldatum');
        $this->database->InParameter($_sp, $params['param5'], 'Systeemvervaldatum');

        try {
            $result = true;
            $this->database->Execute($_sp);
        } catch (Exception $e) {
            $result = false;
            $error = $polaris->getPrettyErrorMessage($e->errno, $e->errmsg);
        }
        return $result;
    }

    function BriefInschrijfFormsAfdrukken($params, $rowids) {
        $_result = true;

        $this->selectedPrinter = $this->GetPrinterObject($params['SELECTEDPRINTER']);

        $_formAchterkant = $this->GetInschrijfformAchterkant();

        $_filter = $this->GetRowFilter($rowids);
        if ($_filter) $_filter .= " AND ";
        $_filter = str_replace('INSCHRIJVING.ROWID','I.ROWID',$_filter);
        $_filter .= " NVL(INSFRM_AFGEDRUKT, 'N') = 'N'";
        $_sqlSelect = "SELECT INSCHRIJFNR FROM INSCHRIJVING_BO_ONDERTEK I WHERE $_filter";
        $_sqlUpdate = "UPDATE INSCHRIJVING SET INSFRM_AFGEDRUKT = 'J', INSFRM_AFGEDRUKTDATUM = SYSDATE WHERE INSCHRIJFNR = :INSCHRIJFNR";

        $_rs = $this->database->GetAll($_sqlSelect);
        foreach($_rs as $_rec) {
            $_begeleidendeBrief = $this->GetInschrijfBegeleidendeBrief($_rec['INSCHRIJFNR']);
            $_formVoorkant = $this->GetInschrijfformVoorkant($_rec['INSCHRIJFNR']);
            $_combinedPDF = $this->CombinePDF($_begeleidendeBrief, $_formVoorkant, $_formAchterkant, $_formVoorkant, $_formAchterkant);

            $_result = $this->_DrukBestandAf($_combinedPDF, $this->selectedPrinter);
            if ($_result) {
                $this->database->Execute($_sqlUpdate, array('INSCHRIJFNR'=>$_rec['INSCHRIJFNR']));
            } else {
                break;
            }
        }

        return $_result;
    }

    function BriefInschrijfFormsOpslaan($params, $rowids) {
        global $_CONFIG;

        $_result = true;
        $_opmerking = "Inschrijfformuliersetje is klaargezet voor E-Post.";
        $_formAchterkant = $this->GetInschrijfformAchterkant();

        $_filter = $this->GetRowFilter($rowids);
        if ($_filter) $_filter .= " AND ";
        $_filter = str_replace('INSCHRIJVING.ROWID','I.ROWID',$_filter);
        $_filter .= " NVL(INSFRM_AFGEDRUKT, 'N') = 'N'";

        $_sqlSelect = "SELECT INSCHRIJFNR FROM INSCHRIJVING_BO_ONDERTEK I WHERE $_filter";
        $_sqlUpdate = "UPDATE INSCHRIJVING SET INSFRM_AFGEDRUKT = 'J', INSFRM_AFGEDRUKTDATUM = SYSDATE WHERE INSCHRIJFNR = :INSCHRIJFNR";

        $_rs = $this->database->GetAll($_sqlSelect);
        foreach($_rs as $_rec) {
            $_begeleidendeBrief = $this->GetInschrijfBegeleidendeBrief($_rec['INSCHRIJFNR']);
            $_formVoorkant = $this->GetInschrijfformVoorkant($_rec['INSCHRIJFNR']);
            $_combinedPDF = $this->CombinePDF($_begeleidendeBrief, $_formVoorkant, $_formAchterkant, $_formVoorkant, $_formAchterkant);

            $_date = date('Y-m-d-Hi');
            if (rename($_combinedPDF, $_CONFIG['inschrijfform_folder'].$_date.'_inschrijfformset_'.$_rec['INSCHRIJFNR'].'.pdf')) {
                $this->OpmerkingMaken($_rec['INSCHRIJFNR'], $_rec['CURSISTNR'], $_opmerking);
                $this->database->Execute($_sqlUpdate, array('INSCHRIJFNR'=>$_rec['INSCHRIJFNR']));
            } else {
                break;
            }
        }

        return $_result;
    }

    function GetInschrijfBriefCode($moduleid) {
        if ($moduleid == 'InschrijfformMailenSingle') {
            return 'INFRAUTODE';
        } else {
            return 'INFRMAILDE';
        }
    }

    function GetInschrijfBegeleidendeBrief($inschrijfnr) {
        $_rapportnaam = 'inschrijfformulier_DE2_brief';
        $_reportFilter = 'Inschrijfnr='.$inschrijfnr;
        return $this->GetJasperReportAsPDF($_rapportnaam, $_reportFilter);
    }

    function GetInschrijfformVoorkant($inschrijfnr) {
        $_rapportnaam = 'inschrijfformulier_DE2';
        $_reportFilter = 'Inschrijfnr='.$inschrijfnr;
        return $this->GetJasperReportAsPDF($_rapportnaam, $_reportFilter);
    }

    function GetInschrijfformAchterkant() {
        global $_GVARS;

        return dirname($_GVARS['docroot']).$this->owner->modulepath.'/attachments/Einschreibform_Kursbedingungen_de230516.pdf';
    }

    function BriefInschrijfFormsMailPreview($params, $rowids, &$error) {
        global $_GVARS;

        $_filter = $this->GetRowFilter($rowids);
        if ($_filter) $_filter = ' AND '.$_filter;
        $_filter = str_replace('INSCHRIJVING.ROWID','I.ROWID',$_filter);

        $_sql = "SELECT * FROM INSCHRIJVING_BO_ONDERTEK I WHERE 1=1 $_filter";
        $_rs = $this->database->GetAll($_sql);

        $_briefnaam = $this->GetInschrijfBriefCode($this->moduleid);

        $_inhoud = $this->GetBriefInhoud($_briefnaam, $_rs[0]);
        $_inhoud->NaarAdres = $_rs[0]['EMAIL'];

        return $_inhoud;
    }

    function OutputInschrijfformPDF($rowids) {
        $_rs = $this->GetRelatieInschrijfNr($rowids);
        $_formVoorkant = $this->GetInschrijfformVoorkant($_rs[0]['INSCHRIJFNR']);
        $_formAchterkant = $this->GetInschrijfformAchterkant();

        $_combinedPDF = $this->CombinePDF($_formVoorkant, $_formAchterkant);
        if (file_exists($_combinedPDF))
            return file_get_contents($_combinedPDF);
        else
            return FALSE;
    }

    function BriefInschrijfFormsMailen($params, $rowids, $moduleid, &$error) {
        global $_GVARS;

        $_kolomnaamKeuze = 'INSFRM_GEMAILD';
        $_kolomnaamDatum = 'INSFRM_MAILDATUM';

        $_filter = $this->GetRowFilter($rowids);
        if ($_filter) $_filter = ' AND '.$_filter;
        $_filter = str_replace('INSCHRIJVING.ROWID','I.ROWID',$_filter);

        $_sql = "SELECT * FROM INSCHRIJVING_PRE I WHERE NVL(INSFRM_GEMAILD, 'N') = 'N' $_filter";
        $_rs = $this->database->GetAll($_sql);
        $_briefnaam = $this->GetInschrijfBriefCode($moduleid);

        // Een keer de kursbedingungen ophalen, want die blijft toch hetzelfde voor elke inschr.
        $_formAchterkant = $this->GetInschrijfformAchterkant();

        foreach($_rs as $_rec) {
            $_formVoorkant = $this->GetInschrijfformVoorkant($_rec['INSCHRIJFNR']);
            if ($_formVoorkant === FALSE) {
                $error = "Bijvoegsel bevat geen data: ".$_rec['INSCHRIJFNR'];
                $result = false;
            } else {
                $_combinedPDF = $this->CombinePDF($_formVoorkant, $_formAchterkant);
                if (file_exists($_combinedPDF))
                    $_pdfdata = file_get_contents($_combinedPDF);

                $_attachments = array(
                    'Kursinformation.pdf' => array('data'=>$_pdfdata, 'type'=>'application/pdf')
                );

                try {
                    $result = $this->StandaardMailVersturen($params, $_briefnaam, $_kolomnaamKeuze, $_kolomnaamDatum, $_rec, $_attachments, $error);
                } catch (Exception $E) {
                    $result = new stdClass;
                    $result->ErrorCode = 9991;
                    $result->Message = $E->getMessage();
                }
            }
        }

        return TRUE;
    }

}
