<?php

class NHA_BaseObject {
    var $owner;
    var $dbobject;
    var $database;
    
    function NHA_BaseObject($owner, $databaseobject='') {
        $this->owner = $owner;

        if ($databaseobject!='') {
            $this->dbobject = $databaseobject;
            $this->database = $this->dbobject->userdb;
        }
        $this->currentDateTime = $this->getCurrentDateTime();
    }

    function getCurrentDateTime() {
        $_sqlCurrentDateTime = "SELECT SysDate FROM DUAL";
        return $this->database->GetOne($_sqlCurrentDateTime);
    }
    
    function getCurrentDate() {
        return $this->GetCurrentDateTime();
    }
    
    function executeStoredProc($sp, $params) {
        try {
            $paramstext = implode("','",$params);
            $_sp = $this->database->PrepareSP("BEGIN $sp('$paramstext'); END;");
            $this->database->Execute($_sp);
        } catch(exception $E) {
        }
    }

}

?>