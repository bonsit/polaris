<div class="componentblock" id="nieuwe_inschrijving_cursusselectie">
    <div class="componentcontent" id="componentcontent1">
        <table>
            <thead style="font-size:0.9em;">
                <tr>
                    <th></th>
                    <th style="width:30%">{lang cursus}</th>
                    <th>{lang zelfstandig}</th>
                    <th>{lang studietempo}</th>
                    <th>{lang betaalwijze}</th>
                    <th>{lang betaaltermijnen}</th>
                    <th>{lang korting}</th>
                    <th>{lang kortingomschrijving}</th>
                    <th><abbr title="{lang inschrijfgeld_berekenen}"></abbr>{lang inschrijfgeld_kort}&nbsp;</th>
                    <th>{lang termijn_bedrag} </th>
                    <th>{lang startdatum}</th>
                    <th>{lang premium}</th>
                    <th id="mbhhboheader" nowrap>{lang mbo_hbo_opties}</th>
                    <th></th>
                </tr>
            </thead>
            <tbody class="componentitems">
                <tr class="componentitem" style="white-space: nowrap;">
                    <td style="text-align:center">
                        {if $smarty.get.actienr == ''}
                        <input type="hidden" class="AANGEVINK" name="INSCHRIJVING!AANGEVINK[]" value=".cursuscode." />
                        {else}
                        <input tabindex="10" class="AANGEVINK" type="checkbox" name="INSCHRIJVING!AANGEVINK[]" value=".cursuscode." />
                        {/if}
                    </td>
                    <td nowrap>
                        <input type="hidden" class="hdnACTION" name="INSCHRIJVING!_hdnAction[]" value="save" />
                        <input type="hidden" class="hdnSTATE" name="INSCHRIJVING!_hdnState[]" value="insert" />
                        <input type="hidden" class="hdnINSCHRIJFNR" name="INSCHRIJVING!INSCHRIJFNR[]" value="@READGEN.seqInschrijving" />
                        <input tabindex="11" type="hidden" class="extsearch CURSUSCODE" style="width:90%" name="INSCHRIJVING!CURSUSCODE[]" ajaxurl_ori="{$source}" ajaxurl="{$source}" selectmasterfield="{$cursuskolom}" captionflds="{$cursuskolom},CURSUSNAAM" searchflds="{$cursuskolom},CURSUSNAAM"  />
                    </td>
                    <td style="text-align:center"><input tabindex="12" class="ZELFSTUDIEJANEE" type="checkbox" name="INSCHRIJVING!ZELFSTUDIEJANEE[]" value="cursuscode" /></td>
                    <td><select tabindex="13" class="STUDIETEMPOCODE" name="INSCHRIJVING!STUDIETEMPOCODE[]" style="width:75px"></select></td>
                    <td><select tabindex="15" class="BETAALWIJZE" name="INSCHRIJVING!BETAALWIJZE[]" style="width:139px"></select></td>
                    <td><select tabindex="17" class="AANTALBETAALTERMIJNEN" name="INSCHRIJVING!AANTALBETAALTERMIJNEN[]" style="width:45px"></select>&nbsp; <i class="VOORRAAD fa fa-lg"></i></div>
                    </td>
                    <td><input tabindex="21" alt="99" class="KORTINGSPERCENTAGE" type="text" size="3" style="width:30px;" name="INSCHRIJVING!KORTINGSPERCENTAGE[]" />%</td>
                    <td><input tabindex="23" class="KORTINGOMSCHRIJVING" type="text" maxlength="50" style="min-width:130px;" name="INSCHRIJVING!KORTINGOMSCHRIJVING[]" /></td>
                    <td><input tabindex="-1" class="BEPAALINSCHRIJFGELD" type="checkbox" checked="checked" name="INSCHRIJVING!BEPAALINSCHRIJFGELD[]" value="cursuscode" /></td>
                    <td style="text-align:right;padding-right:4px;"><span class="TERMIJNBEDRAG"></span></td>
                    <td><input tabindex="25" alt="date_nl" class="STARTDATUM date_input" type="text" size="10" name="INSCHRIJVING!STARTDATUM[]" /></td>
                    <td><select tabindex="26" class="GRATISONDERDEEL" name="GRATISONDERDEEL!GRATISONDERDEEL[]" style="width:170px"></select></td>
                    <td style="font-size:0.9em;">
                        <div class="nietmbohbo">N.v.t.</div>
                        <span class="hbo mbo2 mbo34"><input tabindex="-1" type="checkbox" id="HEEFTVOOROPLEIDING" class="HEEFTVOOROPLEIDING" name="INSCHRIJVING!HEEFTVOOROPLEIDING[]" value="cursuscode" /> <label for="HEEFTVOOROPLEIDING">Vooropleidingen</label><br/></span>
                        <span class="hbo mbo2 mbo34"><input tabindex="38" type="checkbox" id="VRIJSTELLINGDIPLOMA" class="VRIJSTELLINGDIPLOMA" name="INSCHRIJVING!VRIJSTELLINGDIPLOMA[]" value="cursuscode" /> <label for="VRIJSTELLINGDIPLOMA">Vrijstelling obv diploma's? </label><br/></span>
                        <span class="hbo mbo2 mbo34"><input tabindex="39" type="checkbox" id="VRIJSTELLINGWERKERVARING" class="VRIJSTELLINGWERKERVARING" name="INSCHRIJVING!VRIJSTELLINGWERKERVARING[]" value="cursuscode" /> <label for="VRIJSTELLINGWERKERVARING">Vrijstelling obv werkervaring (&euro;150)? </label></span>
                        <span class="hbo mbo2 mbo34"><input tabindex="40" type="checkbox" id="VERKLARING21JAAROFOUDER" class="VERKLARING21JAAROFOUDER" name="INSCHRIJVING!VERKLARING21JAAROFOUDER[]" value="cursuscode" /> <label for="VERKLARING21JAAROFOUDER">Verklaring 21 jaar of ouder? </label><br/></span>
                        <span class="directinschr"><input tabindex="41" type="checkbox" id="DIRECTINSCHRIJVING" class="DIRECTINSCHRIJVING" name="INSCHRIJVING!DIRECTINSCHRIJVING[]" value="cursuscode" /> <label for="DIRECTINSCHRIJVING"><b>Direct inschrijven?</b> </label><br/></span>
                        <span class="hbo mbo2 mbo34"><hr /></span>
                    </td>
                    <td> &nbsp; <i class="REMOVEROW fa fa-minus-circle fa-lg"></i></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>