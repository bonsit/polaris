<h3 id="_lblGEPLANDEACTIE">{lang geplande_actie} <span id="_lblACTIENR">&nbsp;</span></h3>

<table id="geplandeactieform">
    <tr>
        <td>
            {lang geplande_datum}:
        </td>
        <td>
            <input type="text" id="_fldGEPLANDEDATUM" class="readonly required dateNL date_input" readonly="readonly" style="width:70px" name="GEPLANDEDATUM" /> &nbsp;
            {lang begintijd}: <input type="text" id="_fldBEGINTIJD" class="readonly " style="width:40px" name="BEGINTIJD" /> &nbsp;
            {lang eindtijd}: <input type="text" id="_fldEINDTIJD" class="readonly " style="width:40px" name="EINDTIJD" />&nbsp;
        </td>
    </tr>
    <tr>
        <td>
            {lang omschrijving}:
        </td>
        <td>
            <input type="text" id="_fldACTIEOMSCHRIJVING" class="readonly required" style="width:400px;" name="ACTIEOMSCHRIJVING" />
        </td>
    </tr>
</table>