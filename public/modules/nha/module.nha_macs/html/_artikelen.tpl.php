<h2>{lang selecteer_artikelen} <div class="buttons xsmall" style="display:inline-block;"><button tabindex="-1" style="float:none;" id="artikelrijtoevoegen" accesskey="F4" type="button">{lang extra}</button></div></h2>

<div class="componentblock" id="artikelverkoop_artikelselectie">
    <div class="componentcontent" id="componentcontent1">
        <table id="artikelentabel">
            <thead>
                <tr>
                    <th>{lang artikel}</th>
{*                    <th>Cursus</th>*}
                    <th>{lang aantal}</th>
                    <th>{lang stukprijs}</th>
                    <th>{lang verw_bijdrage}</th>
                    <th>{lang subtotaal}</th>
                </tr>
            </thead>
            <tbody class="componentitems">
                <tr class="componentitem">
                    <td nowrap>
                    <input type="hidden" class="" name="ORDERPICKLIJSTREGEL!_hdnAction[]" value="save" />
                    <input type="hidden" class="recordstate" name="ORDERPICKLIJSTREGEL!_hdnState[]" value="insert" />
                    <input type="hidden" class="OMSCHRIJVING" name="ORDERPICKLIJSTREGEL!OMSCHRIJVING[]" value="" />
                    <input type="hidden" class="VERZENDKOSTEN" value="" />
                    <input tabindex="510" type="hidden" class="extsearch validation_uppercase PICKTYPECODE" style="width:400px" name="ORDERPICKLIJSTREGEL!PICKTYPECODE[]" ajaxurl="/app/macs/form/{$source}/" selectmasterfield="{$artikelkolom}" captionflds="{$artikelkolom},OMSCHRIJVING" searchflds="{$artikelkolom},OMSCHRIJVING" /></td>
{*                    <td><select tabindex="513" class="CURSUSCODE" name="ORDERPICKLIJSTREGEL!CURSUSCODE[]" style="width:200px"></select></td>*}
                    <td><input tabindex="515" class="AANTAL" name="ORDERPICKLIJSTREGEL!AANTAL[]" style="width:30px;text-align:right" /></td>
                    <td><input tabindex="519" alt="decimal_nl" class="PRIJSPERSTUKINEUROS" type="text" size="10" name="ORDERPICKLIJSTREGEL!PRIJSPERSTUKINEUROS[]" style="width:45px;text-align:right" /></td>
                    <td><input tabindex="521" class="VERWIJDERINGSBIJDRAGEPERSTUK" type="text" size="10" name="ORDERPICKLIJSTREGEL!VERWIJDERINGSBIJDRAGEPERSTUK[]" class="readonly" readonly="readonly" style="width:45px;text-align:right" /></td>
                    <td style="text-align:right;padding-right:4px;"><span class="SUBTOTAAL"></span></td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="5" style="text-align:right">
                        <label id="_lblFRANKEERKOSTEN" style="margin-left:45px;">{lang verzendkosten}</label>: &euro;
                        <input tabindex="930" alt="decimal_nl" type="text" id="_fldTOTAALVERZENDKOSTEN" name="ORDERPICKLIJST!VERZENDKOSTEN" style="width:50px;text-align:right;" value="" />
                    </td>
                </tr>
                <tr>
                    <td colspan="4"></td>
                    <td style="text-align:right;">{lang totaal}: &euro; <span style="border-top:2px solid #333;width:50px;" id="_fldTOTAAL">xxx,xx</span></td>
                </tr>
            </tfoot>
        </table>
    </div>
</div>