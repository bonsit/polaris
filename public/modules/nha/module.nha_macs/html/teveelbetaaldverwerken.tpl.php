{include file="shared.tpl.php"}

<div id="formview" class="frmTeveelBetaaldVerwerken">
    <input type="hidden" name="_hdnFORMTYPE" value="teveelbetaaldverwerken" />
    <input type="hidden" id="_hdnRecordID" name="_hdnRecordID" value="" />
    
    <input type="hidden" name="INSCHRIJFNR" id="_fldINSCHRIJFNR_NIEUW" value="" />
    <input type="hidden" name="RELATIENR" id="_fldRELATIENR_NIEUW" value="" />
    
    {include file="_inschrijving_selectie.tpl.php" firsttab="RELATIE"}
    
    Selecteer inschrijving: <select id="inschrijvingenselect" size="3" style="height:4em;width:50%;"></select>
    
    {* KimP hoeft geen filters  ---   include file="_betalingsverzoeken_filter.tpl.php"*}
    {include file="_teveelbetaaldverwerken_betalingsverzoeken.tpl.php"}

    <br />
    <div class="buttons">
        <button type="button" id="btnSAVE" class="savebutton positive" accesskey="F9">Opslaan</button>
        <button type="button" id="btnCANCEL" class="cancelbutton negative" accesskey="F2">Annuleren</button>
    </div>
    
</div{* .frmTeveelBetaaldVerwerken *}>