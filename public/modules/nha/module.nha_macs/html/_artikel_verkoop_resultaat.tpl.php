<div id="resultaatformulier" class="jqmWindow shadowbox" style="display:none">
    <h2>{lang resultaat_artikelverkoop}</h2>
    <table id="resultaattabel">
        <tbody>
            <tr>
                <td class="column1">
                    <h3>{lang relatie} (<span class="RELATIENR">9999999</span>)</h3>
                    <span class="BEDRIJF"></span><span class="NAAM"></span><br />
                    <span class="ADRES"></span><br />
                    <span class="WOONPLAATS"></span><br />
                    <span class="LAND"></span><br />
                </td>
                <td class="column2">
                    <h3>{lang factuurrelatie} (<span class="FACTUURRELATIENR">9999999</span>)</h3>
                    <span class="FACTUURNAAM"></span><br />
                    <span class="FACTUURADRES"></span><br />
                    <span class="FACTUURWOONPLAATS"></span><br />
                    <span class="FACTUURLAND"></span><br />
                </td>
            </tr>
            <tr><td colspan="2">&nbsp;</td></tr>
            <tr>
                <td colspan="2">
                    <h3>{lang bestelde_artikelen}</h3>
                    <table id="">
                        <thead>
                            <tr>
                                <th style="width:20%;">{lang displaycode}</th>
                                <th style="width:80%;">{lang omschrijving}</th>
                                <th>{lang aantal}</th>
                            </tr>
                        </thead>
                        <tbody class="componentitems">
                            <tr class="artikel componentitem">
                                <td><span class="DISPLAYCODE"></span></td>
                                <td><span class="OMSCHRIJVING"></span></td>
                                <td><span class="AANTAL">1</span></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <br/>
    <div class="buttons">
        <button tabindex="1200" id="btnCLOSE" class="jqmClose closebutton positive">{lang but_close}</button>
    </div>
</div>
