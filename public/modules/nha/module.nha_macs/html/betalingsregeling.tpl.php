{include file="shared.tpl.php"}

<div id="formview" class="frmBetalingsRegeling">
    <input type="hidden" name="_hdnFORMTYPE" value="betalingsregeling" />

    {include file="_inschrijving_selectie.tpl.php" firsttab="RELATIE"}

    {lang inschrijving}: <select id="inschrijvingenselect"></select>
    <br />
    <br />

    {include file="_regelingoverzicht.tpl.php"}
    {include file="_betalingsregeling.tpl.php"}

    <br />
    <div class="buttons">
        <button type="button" id="btnSAVE" class="savebutton positive disabled" accesskey="F9">{lang but_save}</button>
        <button type="button" id="btnCANCEL" class="cancelbutton negative" accesskey="F2">{lang but_cancel}</button>
    </div>

</div{* .frmBetalingsRegeling *}>