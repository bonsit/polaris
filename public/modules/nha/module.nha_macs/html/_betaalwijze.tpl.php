<table>
    <tr>
        <td>{lang betaalwijze}: </td>
        <td>
            <select id="_fldBETAALWIJZE" name="BETAALWIJZE">
                <option value=""></option>
                {section name=i loop=$betaalwijze}
                <option value="{$betaalwijze[i].BETAALWIJZECODE}">{$betaalwijze[i].OMSCHRIJVING}</option>
                {/section}
            </select>
        </td>
    </tr>
    <tr>
        <td>{lang bankgiro}: </td>
        <td><input type="text" class="" data-v-max="" id="_fldBANKGIROREKENINGNR" name="BANKGIROREKENINGNR" size="34" /></td>
    </tr>
    <tr>
        <td>{lang betaaldag}: </td>
        <td>
            <select id="_fldBETAALDAG" name="BETAALDAG">
            </select>
            <span id="_fldINCASSODAG"></span>
        </td>
    </tr>
</table>
