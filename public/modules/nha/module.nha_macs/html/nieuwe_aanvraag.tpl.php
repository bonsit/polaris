{include file="shared.tpl.php"}

<div id="formview" class="ibox-content formview frmNieuweAanvraag">
<input type="hidden" name="_hdnFORMTYPE" value="nieuweaanvraag" />

{include file="_dataentry_landselectie.tpl.php"}
<h2>{lang selecteer_cursussen}
    <div class="btn-group-sm" style="display:inline-block;">
        <button class="btn btn-default" tabindex="-1" id="cursusrijtoevoegen" accesskey="F4" type="button">Extra</button>
    </div>
</h2>

{include file="_nieuwe_aanvraag_cursus_selectie.tpl.php" source=$cursussource cursuskolom=CURSUSSTUDIEGIDSCODE}
<hr />
{include file="_relatie_selectie.tpl.php"}
<br />
<hr />

<h2 id="lblRELATIE"><em>{lang new}</em> {lang relatie}</h2>

<div style="position:relative;">
{include file="_relatie_statistieken.tpl.php"}
{include file="_relatie_form.tpl.php" type="RELATIE!"}
</div>

<hr class="fix" /><br />
    <button type="button" id="btnSAVE" accesskey="F9" class="btn btn-success savebutton positive disabled">{lang but_save}</button>
    <button type="button" id="btnCANCEL" accesskey="F2" class="btn btn-danger cancelbutton negative">{lang but_cancel}</button>
<div class="xbuttons">
</div>

</div /* frmNieuweAanvraag */>