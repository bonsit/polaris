{include file="shared.tpl.php"}

<div id="formview" class="frmBulkPremiums">
<input type="hidden" name="_hdnFORMTYPE" value="bulkpremiums" />

<div class=""><p>U kunt hier per inschrijving (max {$loopBulk|@count} per keer) aangeven welke leaflet is ontvangen.</p></div>

<table id="tblBulkPremium" class="data">
    <thead>
        <tr>
            <th style="width:100px">Inschrijfnummer</th>
            <th>Leaflet</th>
            <th>Relatiegegevens</th>
        </tr>
    </thead>
    <tbody>
    {foreach from=$loopBulk item=n}
    <tr>
        <td>
            <input type="hidden" class="status" tabindex="-1" name="_hdnStatus[]" size="2" value="" />
            <input type="text" class="inschrijfnr" size="9" tabindex="{math equation=x+1 x=$n*$n-1}" name="inschrijfnr[]" value="" />
        </td>
        <td>
            <select type="text" class="leaflet" tabindex="-1" style="width:200px" name="leaflet[]">
                <option value="">-Kies leaflet-</option>
                {html_options options=$premiums}
            </select>
        </td>
        <td>
            <span class="relatieinfo"></span> <span class="melding"></span>
        </td>
    </tr>
    {/foreach}
    </tbody>
</table>

<br />
<div class="buttons">
    <button type="button" id="btnSAVE" class="savebutton positive disabled" accesskey="F9">{lang verwerkleaflets}</button>
    <button type="button" id="btnCANCEL" class="cancelbutton negative" accesskey="F2">{lang clean}</button>
</div>

</div{* .frmBulkPremiums *}>
