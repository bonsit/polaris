<div id="artikelenformulier" class="jqmWindow shadowbox" style="display:none">

    <div class="componentblock" id="nieuwe_inschrijving_artikelen">
        <div class="componentcontent" id="componentcontent2">
            <table id="artikelentabel" style="width:100%;">
                <tbody>
                    <tr style="border-bottom:1px solid #ccc;" class="cursus">
                        <td class="CURSUSNAAM" colspan="2" style="width:150px;font-size:1.2em;text-align:left;vertical-align:top;">Cursus Keyboard</td>
                        <td style="font-size:1em;text-align:left;">
                            <input type="hidden" class="CURSUSCODE" />
                            <table id="artikelentabel2" style="width:100%;">
                                <thead style="font-size:0.9em;">
                                    <tr>
                                        <th>Artikel</th>
                                        <th>Aantal</th>
                                        <th>Stukprijs</th>
                                        <th>Verw.bijdrage</th>
                                        <th>Subtotaal</th>
                                    </tr>
                                </thead>
                                <tbody class="componentitems">
                                    <tr class="artikel componentitem">
                                        <td style="width:160px;">
                                        <input type="hidden" class="" name="ORDERPICKLIJSTREGEL!_hdnAction[]" value="save" />
                                        <input type="hidden" class="recordstate" name="ORDERPICKLIJSTREGEL!_hdnState[]" value="insert" />
                                        <input type="hidden" class="PICKTYPECODE" name="ORDERPICKLIJSTREGEL!PICKTYPECODE[]" />
                                        <input type="hidden" class="OMSCHRIJVING" name="ORDERPICKLIJSTREGEL!OMSCHRIJVING[]" value="" />
                                        <input type="hidden" class="VERZENDKOSTEN" value="" />
                                        <span class="OMSCHRIJVING">aaa</span>
                                        </td>
                                        <td><input alt="99" tabindex="8000" type="text" tabindex="315" class="AANTAL" name="ORDERPICKLIJSTREGEL!AANTAL[]" style="width:20px" value="0" /></td>
                                        <td><input tabindex="8003" type="text" tabindex="" class="validation_float PRIJSPERSTUKINEUROS" data-m-dec="2" name="ORDERPICKLIJSTREGEL!PRIJSPERSTUKINEUROS[]" style="width:40px" value="" /></td>
                                        <td><input type="text" tabindex="" class="VERWIJDERINGSBIJDRAGE" name="ORDERPICKLIJSTREGEL!VERWIJDERINGSBIJDRAGE[]" style="width:39px" /></td>
                                        <td style="text-align:right;padding-right:4px;">&euro; <span class="SUBTOTAAL"></span></td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="2">
                        </td>
                        <td style="text-align:right">
                        {*
                            <select tabindex="8100" id="_fldBETAALWIJZECODE" name="ORDERPICKLIJST!BETAALWIJZECODE">
                                <option value="ACP">Acceptgiro</option>
                                <option value="AUT">Automatisch</option>
                                <option value="FRA">Franco</option>
                                <option value="REM">Rembours</option>
                            </select>
                        *}
                            <label id="_lblFRANKEERKOSTEN" style="margin-left:45px;">Verzendkosten</label>: &euro;
                            <input alt="decimal_nl" tabindex="8110" type="text" id="_fldTOTAALVERZENDKOSTEN" name="ORDERPICKLIJST!VERZENDKOSTEN" style="width:30px;" value="" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"></td>
                        <td style="text-align:right;">Totaal: &euro; <span style="border-top:2px solid #333;width:50px;" id="_fldTOTAAL">xxx,xx</span></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>

    <div class="buttons">
        <button tabindex="8800" id="btnSAVEANDCLOSE" class="jqmClose closebutton positive">Opslaan</button>
        <button tabindex="8810" id="btnCLOSE" class="jqmClose closebutton secondary">Sluiten</button>
    </div>
</div>
