{include file="shared.tpl.php"}

<div id="formview" class="formview frmNieuweInschrijving">
<input type="hidden" name="_hdnFORMTYPE" value="nieuweinschrijving" />

<input type="hidden" name="_hdnISBOGROEP" value="{$isbogroep}" />


<input type="hidden" name="TM_ACTIENR" value="{$smarty.get.actienr}" />
<div id="divRELATIEOPMERKING" class="buttons small" style="display:none;position:absolute;top:10px;left:300px;"><button id="_btnRELATIEOPMERKING" class="positive">Plaats Opmerking (relatie)</button></div>

{if $landselectie}
{include file="_dataentry_landselectie.tpl.php"}
{/if}
<h2>{lang selecteer_cursussen} <div class="buttons xsmall" style="display:inline-block;"><button style="float:none;" id="cursusrijtoevoegen" accesskey="F4" type="button">Extra</button></div></h2>

{include file="_nieuwe_inschrijving_cursus_selectie.tpl.php" source=$cursussource cursuskolom=CURSUSCODE}

<span id="bankgiro_label" style="display:inline-block;width:100px;text-align:right">{lang bankgiro}</span>: <input type="text" tabindex="100" id="_fldBANKGIROREKENING" maxlength="34" name="BANKGIROREKENINGNR" /> &nbsp;
<input tabindex="101" id="_fldONDERTEKEND" type="checkbox" name="ONDERTEKENDJANEE" value="J" /> <label for="_fldONDERTEKEND">{lang ondertekend}? </label>

<hr />

{include file="_relatie_selectie.tpl.php"}

<br />

<div class="pagetabcontainer">
    <ul id="pagetabs">
        <li><h3 id="lblRELATIE">{lang new} {lang relatie}</h3></li>
        <li><a id="tab_relatie" href="#page_relatie" class="selected">{lang relatie}</a></li>
        <li><a id="tab_factuurrelatie" href="#page_factuurrelatie">{lang factuurrelatie}</a></li>
        <li style="display:none" id="tabli_vooropleiding"><a id="tab_vooropleiding" href="#page_vooropleiding"><i class="fa fa-exclamation-triangle fa-lg"></i> {lang vooropleiding}</a></li>
    </ul>
</div>

{include file="_relatie_statistieken.tpl.php" type=""}

<div id="page_relatie" class="pagecontents" style="display:none">
{include file="_relatie_form.tpl.php" type="RELATIE!"}
</div{* pagetab *}>

<div id="page_factuurrelatie" class="pagecontents" style="display:none">
{include file="_relatie_form.tpl.php" type="FACTUURRELATIE!"}
</div{* pagetab *}>

<div id="page_vooropleiding" class="pagecontents" style="display:none">
{include file="_relatie_vooropleidingen.tpl.php"}
</div{* pagetab *}>

{include file="_nieuwe_inschrijving_artikelen.tpl.php"}
{include file="_nieuwe_inschrijving_resultaat.tpl.php"}

<div class="buttons">
    <button type="button" tabindex="500" id="btnSAVE" accesskey="F9" class="savebutton positive disabled">{lang but_save}</button>
    <button type="button" tabindex="505" id="btnCANCEL" accesskey="F2" class="cancelbutton negative">{lang but_cancel}</button>
    <button type="button" tabindex="510" id="btnARTIKELEN" accesskey="Ctrl+F2" class="artikelenbutton disabled"><i class="fa fa-gift fa-lg"></i> Artikelen</button>
</div>

</div{* .frmNieuweInschrijving *}>

<div id="opmerkingForm" class="jqmWindow" style="display:none;cursor:default">
    {lang vul_opmerking_in}:<br />
    <textarea id="_fldRELATIEOPMERKING" name="RELATIEOPMERKING" autofocus style="height:25px;width:100%;"></textarea>
    <div class="buttons" style="font-size:0.8em;">
        <button type="button" class="jqmClose positive savebutton">{lang but_close}</button>
    </div>
</div>
