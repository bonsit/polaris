{include file="shared.tpl.php"}
<input type="hidden" name="_hdnFORMTYPE" value="telemarketing" />

<div class="tm_listview">
    <div class="telemarketinggrid bodyscroll">
        <table id="geplandeacties" class="selectable geplandeacties" tabindex="1">
          <thead>
            <tr>
              <th colspan="2">&nbsp;</th>
              <th>{lang geplande_datum}</th>
              <th>{lang begintijd}</th>
              <th>{lang eindtijd}</th>
              <th>{lang naam}</th>
              <th>{lang omschrijving}</th>
              <th>{lang medewerker}</th>
            </tr>
          </thead>
          <tbody>
            <tr class="geplandeactie">
              <td><input type="hidden" class="actienr" /></td>
              <td class="periode"></td>
              <td class="geplandedatum"></td>
              <td class="begintijd"></td>
              <td class="eindtijd"></td>
              <td class="achternaam"></td>
              <td class="actieomschrijving"></td>
              <td class="naammedewerker"></td>
            </tr>
          </tbody>
        </table>
    </div>

    <br />
    <div class="buttons">
        <button id="btnREFRESH" class="refreshbutton positive"><span><i class="fa fa-refresh"></i> {lang but_refresh}</span></button>
        <button id="btnAUTOSELECT" class="searchbutton" accesskey="F10"><span><i class="fa fa-magic"></i> {lang auto_selectie}</span></button>
        <button id="btnCOUNTITEMS" class="countbutton "><span><i class="fa fa-tachometer"></i> {lang count_records}</span></button>
        <div id="persinschr">{lang aantal_pers_inschr}: <span id="persinschrvalue"></span></div>
    </div>
</div>

<div class="tm_formview" style="display:none;position:relative;width:100%;">
    <input type="hidden" id="_fldACTIENR" name="ACTIENR" value="telemarketing" />
    <input type="hidden" id="_hdnRecordID" name="_hdnRecordID" value="" />

    <div class="column" style="min-width:700px;">
        <div class="pagetabcontainer">
            <ul id="pagetabs">
                <li><a id="tab_relatie" href="#page_relatie" class="selected">{lang relatie}</a></li>
                <li><a id="tab_opmerkingen" href="#page_opmerkingen">{lang opmerkingen}</a></li>
            </ul>
        </div>

        <div id="page_relatie" class=" pagecontents">
            <input type="hidden" id="relatietype" value="RELATIE" />
            {include file="_relatie_form.tpl.php" type="RELATIE!" formtype="TM"}
        </div>

        <div id="page_opmerkingen" class="pagecontents">
            {include file="tm_opmerkingen.tpl.php" tableid=relatieopmerkingen}
        </div>

        <div id="sub_geplandeactie" class="banner pagecontents">
            {include file="tm_geplandeactie.tpl.php"}
        </div>

        <div id="sub_resultaten" class="banner pagecontents">
            {include file="tm_resultaten.tpl.php"}
        </div>
    </div>

    <div class="column" style="width:280px;">
        {include file="tm_aanvraagcursusinfo.tpl.php"}
        {include file="tm_actiehistorie.tpl.php"}
    </div>

    <hr class="fix"/><br />
    <div class="buttons">
        <button type="button" id="btnSAVE" class="savebutton positive disabled" accesskey="F9"><span>{lang but_save}</span></button>
        <button type="button" id="btnCANCEL" class="cancelbutton negative" accesskey="F2"><span>{lang but_cancel}</span></button>
    </div>

</div>
