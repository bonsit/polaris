<h2>{lang factuur}</h2>
<div class="pagecontents">
<table style="width:100%;margin:8px 0 8px 0;">
    <tr>
        <td style="width:20%;">{lang betaalwijze}: <span style="font-weight:bold">{lang acceptgiro}</span></td>
        <td style="width:23%;">{lang inzake}: <input type="text" id="_fldINZAKE" name="INZAKE" value="" size="15" /></td>
        <td style="width:25%;">{lang verzenddatum}:&nbsp;<input type="text" id="_fldVERZENDDATUM" name="VERZENDDATUM" size="12" value="" class="date_input required" /> </td>
        <td style="width:20%;">{lang op_verstuurd_zetten}? <input type="checkbox" id="_fldVERSTUURDJANEE" name="VERSTUURDJANEE" value="J" /> </td>
        <td style="width:30%;text-align:right;"><div class="buttons medium"><button type="button" id="_btnWIJZIGBRIEF" class="secondary">{lang wijzig_brief}</button></div></td>
    </tr>
</table>
</div>