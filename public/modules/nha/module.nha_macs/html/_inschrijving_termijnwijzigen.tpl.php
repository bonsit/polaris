<table>
    <tr>
        <td>{lang zelfstudie}: </td>
        <td>
            <input tabindex="100" type="checkbox" id="_fldZELFSTUDIE" name="ZELFSTUDIE" value="J" />
        </td>
    </tr>
    <tr>
        <td>{lang studietempo}: </td>
        <td><select tabindex="110" class="cleanup" id="_fldSTUDIETEMPO" name="STUDIETEMPO" style="width:150px"></select>
        &nbsp;
        <label for="_btnKOPPEL">{lang koppelen}?</label><input type="checkbox" id="_btnKOPPEL" />
        </td>
    </tr>
    <tr>
        <td>{lang betaaltermijnen}: </td>
        <td><select tabindex="120" class="cleanup" id="_fldBETAALTERMIJNEN" name="BETAALTERMIJNEN" style="width:150px"></select></td>
    </tr>
    <tr>
        <td colspan="2"><br /></td>
    </tr>
    <tr>
        <td>CALL {lang opmerking}: &nbsp;</td>
        <td><input type="text" tabindex="130" class="cleanup" id="_fldCALLOPMERKING" name="CALLOPMERKING" style="width:450px" value="" /></td>
    </tr>
</table>