<div class="componentblock" id="nieuwe_aanvraag_cursusselectie">
    <div class="componentcontent">
        <input type="hidden" class="recordstate" value="insert" name="VERZOEKCURSUSINFO!_hdnState[]" />
        <input type="hidden" value="__auto_increment__" name="VERZOEKCURSUSINFO!VERZOEKNR[]" />
        <ul class="componentitems">
            <li class="componentitem">
                {lang code} / {lang omschrijving}: <input type="hidden" tabindex="10" class="extsearch validate-uppercase CURSUSSTUDIEGIDSCODE" style="width:400px"  name="VERZOEKCURSUSINFO!CURSUSSTUDIEGIDSCODE[]" ajaxurl_ori="{$source}" ajaxurl="{$source}" selectfield="{$cursuskolom}" selectmasterfield="{$cursuskolom}" captionflds="{$cursuskolom},CURSUSNAAM" searchflds="{$cursuskolom},CURSUSNAAM"  />
            </li>
        </ul>
    </div>
</div>