<div id="inschrijving_selectie">
    <div class="pagetabcontainer">
        <ul class="nav nav-tabs">
            <li><b>{lang search} {lang inschrijving}:</b></li>
            {if $firsttab == 'RELATIE'}
                <li class="active"><a data-toggle="tab" id="tab_relatie" href="#page_relatie">{lang via} {lang factuurrelatie_relatie}</a></li>
                <li ><a data-toggle="tab" id="tab_inschrijving" href="#page_inschrijving">{lang via} {lang inschrijving}</a></li>
            {else}
                <li class="active"><a data-toggle="tab" id="tab_inschrijving" href="#page_inschrijving">{lang via} {lang inschrijving}</a></li>
                <li ><a data-toggle="tab" id="tab_relatie" href="#page_relatie">{lang via} {lang factuurrelatie_relatie}</a></li>
            {/if}
        </ul>
    </div>

    <div class="panel-body">
        <div class="tab-content">
            <div id="page_inschrijving" class="tab-pane active">
                <div id="inschrijving_relatie" class="alignlabelsx row">

                    <div class="form-group form-group-sm">
                        <label class="control-label col-lg-2">{lang inschrijfnr}: </label>
                        <div class="col-lg-2">
                            <input tabindex="1" class="form-control" type="search" id="_fldZOEKINSCHRIJVING" size="12" value="{$pininschrijving}" />
                        </div>
                    </div>

                    <div class="form-group form-group-sm">
                        <label class="control-label col-lg-2">{lang cursus}: </label>
                        <div class="col-lg-2">
                            <label class="form-control-static CURSUSNAAM"></label>
                        </div>
                    </div>

                    <table style="width:100%;vertical-align:top;" class="tblinschrijving">
                        <tr>
                            <td style="width:30%;">
                                <div class="formviewrow"><label class="label">{lang inschrijfnr}: </label>
                                    <div class="formcolumn"><input tabindex="1" type="search" id="_fldZOEKINSCHRIJVING" size="12" value="{$pininschrijving}" /></div>
                                </div>

                                <div class="formviewrow"><label class="label">{lang cursus}: </label>
                                    <div class="formcolumn"><label class="CURSUSNAAM"></label></div>
                                </div>
                                <div class="formviewrow"><label class="label">{lang cursuscode}: </label>
                                    <div class="formcolumn"><label class="CURSUSCODE"></label></div>
                                </div>
                                <div class="formviewrow"><label class="label">{lang cursistnr}: </label>
                                    <div class="formcolumn"><label class="CURSISTNR"></label></div>
                                </div>
                                <div class="formviewrow"><label class="label">{lang datum_inschrijving}: </label>
                                    <div class="formcolumn"><label class="DATUMINSCHRIJVING"></label></div>
                                </div>
                            </td>
                            <td style="width:30%;">
                                <div class="formviewrow"><label class="label">{lang relatienaam}: </label>
                                    <div class="formcolumn"><label class="VOORLETTERS"></label> <label class="TUSSENVOEGSEL"></label> <label class="ACHTERNAAM"></label></div>
                                </div>
                                <div class="formviewrow"><label class="label">{lang adres}: </label>
                                    <div class="formcolumn"><label class="STRAAT"></label> <label class="HUISNR"></label></div>
                                </div>
                                <div class="formviewrow"><label class="label">{lang postcode}: </label>
                                    <div class="formcolumn"><label class="POSTCODE"></label></div>
                                </div>
                                <div class="formviewrow"><label class="label">{lang plaats}:</label>
                                    <div class="formcolumn"><label class="PLAATS"></label></div>
                                </div>
                            </td>
                            <td style="width:33%;">
                                <div class="formviewrow"><label class="label">{lang opzeggingsdatum}: </label>
                                    <div class="formcolumn"><label class="OPZEGGINGSDATUM"></label></div>
                                </div>
                                <div class="formviewrow"><label class="label">{lang geretourneerd_op}:</label>
                                    <div class="formcolumn"><label class="DATUMGERETOURNEERD"></label></div>
                                </div>
                                <div class="formviewrow"><label class="label">{lang opmerking}: </label>
                                    <div class="formcolumn"><label class="OPMERKING"></label></div>
                                </div>
                                <div class="formviewrow"><label class="label">{lang aanmaningscode}: </label>
                                    <div class="formcolumn"><label class="AANMANINGSCODE"></label></div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

            <div id="page_relatie" class="tab-pane">
                <div class="searchbox alignlabels">
                    <div class="formviewrow"><label class="label">{lang relatienummer}: </label><div class="formcolumn"><input tabindex="3" type="search" id="_fldZOEKRELATIE" size="15" value="{$pinrelatie}" /></div></div>
                    <div class="formviewrow"><label class="label">{lang postcode}: </label><div class="formcolumn"><input tabindex="4" type="search" id="_fldZOEKPOSTCODE" size="15" placeholder="9999XX" alt="9999ZZ" class="validation_uppercase" xvalue="" /></div></div>
                    <div class="formviewrow"><label class="label">{lang huisnummer}: </label><div class="formcolumn"><input tabindex="5" type="search" id="_fldZOEKHUISNR" size="15" xvalue="" /></div></div>
                    <div class="formviewrow"><label class="label">{lang land}: </label><div class="formcolumn"><select tabindex="6" type="search" id="_fldZOEKLAND" style="width:110px;">{html_options options=$landen selected="NL"}</select></div></div>
                    <div class="formviewrow"><label class="label">&nbsp;</label>
                        <div class="buttons small">
                            <button id="btnSEARCHRELATIE" type="button" class="searchbutton">{lang but_search}</button>
                        </div>
                    </div>
                </div>

                <div class="searchresult selectable">
                    <div class="content">
                        <table id="relaties" class="relaties">
                          <thead>
                            <tr>
                              <th>{lang relatienummer}</th>
                              <th>{lang naam}</th>
                              <th>{lang bedrijf}</th>
                              <th>{lang straat}</th>
                              <th>{lang postcode}</th>
                              <th>{lang plaats}</th>
                              <th>{lang land}</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr class="relatie">
                              <td class="nummer"></td>
                              <td class="naam"></td>
                              <td class="bedrijf"></td>
                              <td class="straat"></td>
                              <td class="postcode"></td>
                              <td class="plaats"></td>
                              <td class="land"></td>
                            </tr>
                          </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="inschrijvingen" class="jqmWindow shadowbox selectable" id="" style="display:none;">
    <h2>{lang kies_gewenste_inschrijving}.</h2>
    <div class="inschrijvingcontent">
        <table class="inschrijvingen" style="width:100%;">
          <thead>
            <tr>
              <th>{lang inschrijfnr}</th>
              <th>{lang relatie}</th>
              <th>{lang relatienummer}</th>
              <th>{lang cursuscode}</th>
              <th>{lang aanmaningscode}</th>
              <th>{lang datum_inschrijving}</th>
            </tr>
          </thead>
          <tbody>
            <tr class="inschrijving">
              <td class="inschrijfnr"></td>
              <td class="relatie"></td>
              <td class="relatienr"></td>
              <td class="cursuscode"></td>
              <td class="aanmaningscode"></td>
              <td class="datuminschrijving"></td>
            </tr>
          </tbody>
        </table>
    </div>
</div>