<h2>Betalingsverzoeken</h2>
<div id="betalingsverzoeken" class="bv_tabel">
    <table id="betalingsverzoekentabel" class="data scrollable">
        <thead>
            <tr>
                <th>Nr</th>
                <th>Termijn</th>
                <th>Kenmerk</th>
                <th>Status</th>
                <th>Bedrag</th>
                <th>Betaald</th>
                <th>Te betalen</th>
                <th>Toegewezen</th>
                <th>Verzenddatum</th>
                <th>Herinnering</th>
                <th>Regeling</th>
                <th>Volgorde</th>
                <th>Betalen</th>
            </tr>
        </thead>
        <tbody id="betalingsverzoekarea">
            <tr class="betalingsverzoek">
                <td>
                <input type="hidden" name="PLR__RECORDID[]" class="PLR__RECORDID" value="" />
                <input type="hidden" name="BETALINGSVERZOEKNR[]" class="BETALINGSVERZOEKNR" value="" />
                <input type="hidden" name="TOEGEWEZEN[]" class="TOEGEWEZEN" value="" />
                <span class="NR"></span></td>
                <td><span class="TERMIJN"></span></td>
                <td><span class="KENMERK"></span></td>
                <td><span class="STATUS"></span></td>
                <td class="right"><span class="BEDRAG"></span></td>
                <td class="right"><span class="BEDRAGBETAALD"></span></td>
                <td class="right"><span class="TEBETALEN"></span></td>
                <td class="middle"><span class="TOEGEWEZEN_DISPLAY"></span></td>
                <td><span class="VERZENDDATUM"></span></td>
                <td class="middle"><input type="checkbox" disabled="disabled" class="HERINNERING" value="J" /></td>
                <td class="middle"><input type="checkbox" disabled="disabled" class="REGELING" value="J" /></td>
                <td><span class="VOLGORDE"></span></td>
                <td class="middle"><input type="checkbox" name="BETALEN[]" class="BETALEN" value="" /></td>
            </tr>
        </tbody>
    </table>

    <table id="teveelbetaaldfooter">
        <tfoot>
            <tr>
                <td colspan="1" class="right" style="width:120px;">Maximaal toe te wijzen:</td>
                <td colspan="1" class="right" style="width:60px;"><span id="_fldMAXTOETEWIJZEN"></span></td>
                <td colspan="1" class="right" style="width:120px;">Totaal toe te wijzen:</td>
                <td colspan="1" class="right" style="width:70px;"><input type="text" id="_fldTOTAALBEDRAG" class="totaalbedrag required validation_float" value="" /></td>
                <td colspan="1" class="right totaaltoegewezen" style="width:80px;"><input type="text" id="_fldTOEGEWEZEN" class="totaalbedrag" disabled value="" /></td>
                <td colspan="1" class="right">Restant</td>
                <td colspan="1" class="right"><input type="text" id="_fldRESTANT" class="totaalbedrag" disabled value="" /></td>
                <td colspan="1" class="right"><input type="checkbox" name="BUITENGEWONEBATEN" id="_chbBUITENGEWONEBATEN" value="J" /> Buitengewone baten</td>
                <td colspan="1" class="right"><input type="text" id="_fldBUITENGEWONEBATENBEDRAG" name="BUITENGEWONEBATENBEDRAG" class="totaalbedrag validation_float" value="" /></td>
            </tr>
        </tfoot>
    </table>

</div>
