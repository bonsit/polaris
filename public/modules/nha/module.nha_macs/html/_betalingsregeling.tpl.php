<input type="hidden" name="INSCHRIJFNR" id="_fldINSCHRIJFNR" value="" />
<input type="hidden" name="RESTBEDRAG" id="_fldRESTBEDRAG" value="" />
<input type="hidden" name="AANTALMAANDEN" id="_fldAANTALMAANDEN" value="" />
<input type="hidden" name="OPENSTAAND" id="_fldOPENSTAAND" value="" />
<input type="hidden" name="OPENSTAANDADMINKOSTEN" id="_fldOPENSTAANDADMINKOSTEN" value="" />

<table id="betalingsregelingtable">
    <tr>
        <td>{lang achterstallig_saldo}: </td>
        <td><span id="_lblOPENSTAAND"></span></td>
    </tr>
    <tr>
        <td>{lang achterstallige_administratiekosten}: </td>
        <td><span id="_lblOPENSTAANDADMINKOSTEN"></span></td>
    </tr>
    <tr>
        <td>{lang gewenst_maandbedrag}: </td>
        <td><input type="text" name="MAANDBEDRAG" id="_fldMAANDBEDRAG" class="validation_float required" value="0,00" /></td>
    </tr>
    <tr>
        <td>{lang administratiekosten_kwijtschelden}: </td>
        <td><input type="checkbox" name="ADMINKOSTENKWIJTSCHELDEN" id="_fldADMINKOSTENKWIJTSCHELDEN" value="J" /></td>
    </tr>
    <tr>
        <td>{lang datum_ingang}: </td>
        <td><input type="text" name="INGANGSDATUM" id="_fldINGANGSDATUM" class="date_input" /></td>
    </tr>
    <tr>
        <td>{lang toelichting}: </td>
        <td><input type="text" name="TOELICHTING" id="_fldTOELICHTING" size="40" class="required" /></td>
    </tr>
</table>