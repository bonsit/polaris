{if $type == "FACTUURRELATIE!"}
{assign var=requiredtype value="appear_required"}
{else}
{assign var=requiredtype value="required"}
{/if}

{assign var=relatietype value=$type|replace:"!":""}

{if $type == "FACTUURRELATIE!"}
    {assign var=requiredtype value='appear_required'}
{else}
    {assign var=requiredtype value='required'}
{/if}
{if $formtype == 'BETAALWIJZEAANPASSEN'}
{else}
<div id="relatieformbuttons" class="buttons small">
    {if $formtype == 'TM'}
    <a tabindex="-1" id="btn{$relatietype}EDIT" accesskey="Shift+F6" href="javascript:void(0)">{lang edit}</a>
    {else}
    <a tabindex="-1" id="btn{$relatietype}EDIT" accesskey="Shift+F6" href="javascript:void(0)">{lang edit}</a>
    {if $type == "FACTUURRELATIE!"}
    <a tabindex="-1" id="btn{$relatietype}ERASE" accesskey="Shift+F7" href="javascript:void(0)">{lang clean}</a>
    {/if}
    <a tabindex="-1" id="btn{$relatietype}COPY" accesskey="Shift+F11" href="javascript:void(0)">{lang copy} &amp; {lang edit}</a>
    <a tabindex="-1" id="btn{$relatietype}NEW" accesskey="Shift+F12" href="javascript:void(0)">{lang new}</a>
    {/if}
</div>
{/if}

<div id="frm{$relatietype}" class="frmrelatieform">
    <input type="hidden" id="{$relatietype}_hdnRecordID" name="{$type}_hdnRecordID" value="" />
    <input type="hidden" name="{$type}_hdnTable" value="NHA.RELATIE" />
    <input type="hidden" id="{$relatietype}_hdnState" name="{$type}_hdnState" value="" />
    <input type="hidden" id="{$relatietype}_hdnAction" name="{$type}_hdnAction" value="..nothing.." />
    <input type="hidden" id="{$relatietype}_fldRELATIENR" name="{$type}RELATIENR" value="" />

    <div class="viewmode">
        <div class="formviewrow"><label class="label">{lang relatie}: </label><div class="formcolumn">
            <span id="{$type}_lblGESLACHT">&nbsp;</span>
            <span id="{$type}_lblVOORLETTERS">&nbsp;</span>
            <span id="{$type}_lblTUSSENVOEGSEL">&nbsp;</span>
            <span id="{$type}_lblACHTERNAAM">&nbsp;</span>
        </div></div>
        <div class="formviewrow"><label class="label">{lang bedrijf}: </label><div class="formcolumn">
            <span id="{$type}_lblBEDRIJFSNAAM">&nbsp;</span>
            <span id="{$type}_lblAFDELINGNAAM"></span>
        </div></div>
        <div class="formviewrow"><label class="label">{lang adres}: </label><div class="formcolumn">
            <span id="{$type}_lblSTRAAT">&nbsp;</span>
            <span id="{$type}_lblHUISNR">&nbsp;</span><br />
            <span id="{$type}_lblPOSTCODE">&nbsp;</span> &nbsp;<span id="{$type}_lblPLAATS">&nbsp;</span><span id="{$type}_lblLANDCODE" title=", ">&nbsp;</span>
        </div></div>

            <div class="formviewrow captions">
                <table class="rowtable">
                    <tr>
                        <td style="min-width:11em;">{lang tel_overdag}</td>
                        <td style="min-width:11em;">{lang tel_avond}</td>
                        <td style="min-width:11em;">{lang tel_mobiel}</td>
                        <td>{lang email}</td>
                    </tr>
                </table>
            </div>
            <div class="formviewrow {if $formtype == 'TM'}largecontact{/if}"><label class="label">{lang contact}: </label><div class="formcolumn">
                <table class="rowtable contact"><tr>
                <td style="min-width:7em;" class="larger"><span id="{$type}_lblTELEFOONOVERDAG">&nbsp;</span></td>
                <td style="min-width:7em;" class="larger"><span id="{$type}_lblTELEFOONSAVONDS">&nbsp;</span></td>
                <td style="min-width:7em;" class="larger"><span id="{$type}_lblMOBIELNR">&nbsp;</span></td>
                <td><a tabindex="-1" href="mailto:"><span id="{$type}_lblEMAIL">&nbsp;</span></a></td>
                </tr></table>
            </div></div>
        {if $type != "FACTUURRELATIE!"}
            <div class="formviewrow"><label class="label">{lang geboortedatum}:</label><div class="formcolumn">
                <span id="{$type}_lblGEBDATUM">&nbsp;</span>
                <label class="label">{lang geboorteplaats}:</label>
                <span id="{$type}_lblGEBPLAATS">&nbsp;</span>
            </div></div>
        {/if}
        <hr class="fix" /><br />
    </div{* .viewmode *}>

    <div class="editmode" style="display:none;">
        <div class="formviewrow"><label class="label">{lang relatie}: </label><div class="formcolumn">
            <select tabindex="250" name="{$type}GESLACHT" id="{$type}_fldGESLACHT"><option value="O">{lang dhr_mevr}</option><option value="M">{lang dhr}</option><option value="V">{lang mevr}</option></select>
            <input tabindex="251" size="8" name="{$type}VOORNAAM" id="{$type}_fldVOORNAAM" placeholder="{lang voornaam}" />
            <input tabindex="252" size="20" name="{$type}ACHTERNAAM" id="{$type}_fldACHTERNAAM" placeholder="{lang achternaam}" class="{$requiredtype}" />
            <input tabindex="253" size="8" name="{$type}TUSSENVOEGSEL" id="{$type}_fldTUSSENVOEGSEL" placeholder="{lang tussenvoegsel}" />
        </div></div>
        <div class="formviewrow"><label class="label">{lang bedrijf}: </label><div class="formcolumn">
            <input tabindex="-1" type="text" name="{$type}BEDRIJFSNAAM" id="{$type}_fldBEDRIJFSNAAM" size="20" placeholder="{lang bedrijfsnaam}" />
            <input tabindex="-1" type="text" name="{$type}AFDELINGNAAM" id="{$type}_fldAFDELINGNAAM" size="18" placeholder="{lang afdeling}" />
            <input tabindex="-1" type="text" name="{$type}BTWNUMMER" id="{$type}_fldBTWNUMMER" size="20" placeholder="{lang btw_nummer}" />
        </div></div>
        <div class="formviewrow"><label class="label">{lang adres}: </label><div class="formcolumn">
            <input tabindex="264" type="text" name="{$type}POSTCODE" id="{$type}_fldPOSTCODE" size="9" placeholder="{lang postcode}" class="validation_uppercase pcfield {$requiredtype}" value="" />
            <input tabindex="265" type="text" name="{$type}HUISNUMMER" id="{$type}_fldHUISNUMMER" size="7" placeholder="{lang huisnummer}" class="validation_uppercase validation_integer {$requiredtype}" />
            <input tabindex="267" type="text" name="{$type}HUISNUMMERTOEVOEGING" id="{$type}_fldHUISNUMMERTOEVOEGING" size="5" placeholder="{lang huisnummertoevoeging}" class="validation_uppercase" />
            <select tabindex="268" name="{$type}LANDCODE" id="{$type}_fldLANDCODE" style="width:150px;">{html_options options=$landen selected="NL"}</select>
            <button tabindex="269" class="btnZOEKADRES">{lang search_active} {lang adres}</button>
        </div></div>
        <div class="formviewrow"><label class="label">&nbsp;</label><div class="formcolumn">
            <input tabindex="270" type="text" name="{$type}STRAAT" id="{$type}_fldSTRAAT" size="34" placeholder="{lang straat}" class="{$requiredtype}" />
            <input tabindex="271" type="text" name="{$type}PLAATS" id="{$type}_fldPLAATS" size="34" placeholder="{lang plaats}" class="pcsearch {$requiredtype}" />
        </div></div>
            <div class="formviewrow captions">
                <table class="rowtable">
                    <tr>
                        <td>{lang tel_overdag}</td>
                        <td>&nbsp;{lang tel_avond}</td>
                        <td>&nbsp;&nbsp;{lang tel_mobiel}</td>
                        <td>{lang email}</td>
                    </tr>
                </table>
            </div>
            <div class="formviewrow"><label class="label">{lang contact}: </label><div class="formcolumn">
                <input tabindex="282" type="text" name="{$type}TELEFOONOVERDAG" id="{$type}_fldTELEFOONOVERDAG" class="telefoon" size="12" />
                <input tabindex="283" type="text" name="{$type}TELEFOONSAVONDS" id="{$type}_fldTELEFOONSAVONDS" class="telefoon" size="12" />
                <input tabindex="284" type="text" name="{$type}MOBIELNR" id="{$type}_fldMOBIELNR" class="telefoon" size="12" />
                <input tabindex="285" type="text" name="{$type}EMAIL" class="email" id="{$type}_fldEMAIL" maxlength="50" size="25" />
            </div></div>
        {if $type != "FACTUURRELATIE!"}
            <div class="formviewrow"><label class="label">{lang geboortedatum}: </label><div class="formcolumn">
                <input tabindex="286" type="text" name="{$type}GEBDATUM" class="gebdatum validation_date" id="{$type}_fldGEBDATUM" size="16"  />

                {lang geboorteplaats}:&nbsp;
                <input tabindex="287" type="text" name="{$type}GEBPLAATS" class="gebplaats" id="{$type}_fldGEBPLAATS" size="16"  />
            </div></div>
        {/if}
    </div{* .editmode *}>
</div{* #frmrelatie *}>

{literal}
<script type="text/javascript">
$(document).ready(function(){
    if (Macs.RelatieSelectieForm)
        var reltype = Macs.RelatieSelectieForm.relatieType();
    else
        var reltype = 'RELATIE';
    $(".btnZOEKADRES").click(function(e) {
        e.preventDefault();
        if (Macs.RelatieSelectieForm)
            var reltype = Macs.RelatieSelectieForm.relatieType();
        else
            var reltype = 'RELATIE';
        Macs.SharedLib.zoekFormAdres(reltype);
    });

    var reltype = 'RELATIE';
    $huisnr = $("#"+reltype+"\\!_fldHUISNUMMER");
    $("#"+reltype+"\\!_fldHUISNUMMER").keyup(function(event) { if (($("#"+reltype+"\\!_fldPOSTCODE").val() != '' || this.value != '') && event.keyCode == 13 /*Enter*/) { event.preventDefault(); Macs.SharedLib.zoekFormAdres(reltype); } });
    // kopieer het telefoonnr naar mobiel indien mobiel niet is ingevuld en telefoonnr een 06 nummer is
    $("#"+reltype+"\\!_fldTELEFOONOVERDAG").blur(function() {
        $mnr = $("#"+reltype+"\\!_fldMOBIELNR");
        if ($(this).val().substr(0,2) == '06' && ($mnr.val() == '')) $mnr.val($(this).val());
    });
    $huisnrtoevoeging = $("#"+reltype+"\\!_fldHUISNUMMERTOEVOEGING");
    $huisnr.blur(function() {
        Macs.RelatieSelectieForm.switchHuisNr($huisnr, $huisnrtoevoeging, $("#"+reltype+"\\!_fldLANDCODE").val());
    });

    var factuurreltype = 'FACTUURRELATIE';
    $frhuisnr = $("#"+factuurreltype+"\\!_fldHUISNUMMER");
    $frhuisnrtoevoeging = $("#"+factuurreltype+"\\!_fldHUISNUMMERTOEVOEGING");
    $("#"+factuurreltype+"\\!_fldHUISNUMMER").keyup(function(event) { if (($("#"+factuurreltype+"\\!_fldPOSTCODE").val() != '' || this.value != '') && event.keyCode == 13 /*Enter*/) { event.preventDefault(); Macs.SharedLib.zoekFormAdres(factuurreltype); } });

    $("#"+reltype+"\\!_fldLANDCODE").change(function() {
        // ingeval het land BE is, dan moet de plaatsnaam kiesbaar zijn uit een lijst
        Macs.RelatieSelectieForm.switchBEPlaatsnaam(reltype);
        // ingeval het land NL is, dan is het huisnummer een getal
        Macs.RelatieSelectieForm.switchHuisNr($huisnr, $huisnrtoevoeging, $("#"+reltype+"\\!_fldLANDCODE").val());
    });
    $("#"+factuurreltype+"\\!_fldLANDCODE").change(function() {
        Macs.RelatieSelectieForm.switchBEPlaatsnaam(factuurreltype);
        Macs.RelatieSelectieForm.switchHuisNr($frhuisnr, $frhuisnrtoevoeging, $("#"+factuurreltype+"\\!_fldLANDCODE").val());
    });
    $frhuisnr.blur(function() {
        Macs.RelatieSelectieForm.switchHuisNr($frhuisnr, $frhuisnrtoevoeging, $("#"+factuurreltype+"\\!_fldLANDCODE").val());
    });
});
</script>
{/literal}
