<h2>Betalingsverzoeken</h2>
<div id="betalingsverzoeken" class="bv_tabel">
    <table id="betalingsverzoekentabel" class="striped data scrollable">
        <thead>
            <tr>
                <th>Termijn</th>
                <th>Kenmerk</th>
                <th>Status</th>
                <th>Bedrag</th>
                <th>Betaald</th>
                <th>Te betalen</th>
                <th>Betaalwijze</th>
                <th>Verzenddatum</th>
                <th>Vervaldata cursist/systeem</th>
                <th>Herinnering</th>
                <th>Regeling</th>
                <th>Samen</th>
            </tr>
        </thead>
        <tbody>
            <tr class="betalingsverzoek">
                <td><input type="hidden" name="PLR__RECORDID[]" class="PLR__RECORDID" value="" />
                <span class="TERMIJN"></span></td>
                <td><span class="KENMERK"></span></td>
                <td><span class="STATUS"></span></td>
                <td class="right"><span class="BEDRAG"></span></td>
                <td class="right"><span class="BEDRAGBETAALD"></span></td>
                <td class="right"><span class="TEBETALEN"></span></td>
                <td class="middle"><span class="BETAALWIJZE"></span></td>
                <td><span class="VERZENDDATUM"></span></td>
                <td><span class="VERVALDATA"></span></td>
                <td class="middle"><input type="checkbox" disabled="disabled" class="HERINNERING" value="J" /></td>
                <td class="middle"><input type="checkbox" disabled="disabled" class="REGELING" value="J" /></td>
                <td class="middle"><input type="checkbox" name="SAMEN[]" class="SAMEN" value="" /></td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="3" class="right">Totaal</td>
                <td colspan="1" class="right"><input id="_fldTOTAALBEDRAG" class="totaalbedrag" disabled value="" /></td>
                <td colspan="1" class="right"><input id="_fldTOTAALBETAALD" class="totaalbedrag" disabled value="" /></td>
                <td colspan="1" class="right"><input id="_fldTOTAALBEDRAGTEBETALEN" class="totaalbedrag" disabled value="" /></td>
            </tr>
            <tr class="betalingsverzoektotaal">
                <td><span class="TERMIJN_NIEUW"></span></td>
                <td><input type="text" name="BETALINGSKENMERK" id="_fldBETALINGSKENMERK_NIEUW" disabled style="width:50px;" value="C" /></td>
                <td><span class="STATUS_NIEUW">GP</span></td>
                <td class="right"><input type="text" name="BEDRAGINEUROS" id="_fldBEDRAG_NIEUW" class="required totaalbedrag" value="" /></td>
                <td class="right"><span id="_fldBEDRAGBETAALD_NIEUW"></span></td>
                <td class="right"><span id="_fldTEBETALEN_NIEUW"></span></td>
                <td class="middle"><span id="_fldBETAALWIJZE_NIEUW"></span></td>
                <td><input type="text" name="VERZENDDATUM" id="_fldVERZENDDATUM_NIEUW" style="width:80px;" class="required date_input" value="" /></td>
                <td><span id="_fldVERVALDATA_NIEUW"></span></td>
                <td class="middle"><input type="checkbox" disabled="disabled" id="_fldHERINNERING_NIEUW" value="J" /></td>
                <td class="middle"><input type="checkbox" disabled="disabled" class="_fldREGELING_NIEUW" value="J" /></td>
                <td></td>
            </tr>                        
        </tfoot>
    </table>
</div>
