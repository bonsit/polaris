{include file="shared.tpl.php"}

<div id="formview" class="formview frmArtikelVerkoop">
<input type="hidden" name="_hdnFORMTYPE" value="artikelverkoop" />
{include file="_dataentry_landselectie.tpl.php"}

{include file="_relatie_selectie.tpl.php" form="artikelverkoop"}
<br />
<div class="pagetabcontainer">
    <ul id="pagetabs">
        <li><h3 id="lblRELATIE">{lang new} {lang relatie}</h3></li>
        <li><a id="tab_relatie" href="#page_relatie" class="selected">{lang relatie}</a></li>
        <li><a id="tab_factuurrelatie" href="#page_factuurrelatie">{lang factuurrelatie}</a></li>
    </ul>
</div>

<div id="page_relatie" class="pagecontents" style="display:none">
{*include file="_artikelverkoop_info.tpl.php" type="ARTIKELVERKOOP"*}

{include file="_relatie_form.tpl.php" type="RELATIE!"}
</div{* pagetab *}>

<div id="page_factuurrelatie" class="pagecontents" style="display:none">
{include file="_relatie_form.tpl.php" type="FACTUURRELATIE!"}
</div{* pagetab *}>

{include file="_artikelen.tpl.php" source=artikel artikelkolom=ARTIKELCODE}
{include file="_artikel_verkoop_resultaat.tpl.php"}

<div class="buttons">
    <button type="button" tabindex="1000" id="btnSAVE" class="savebutton positive disabled" accesskey="F9">{lang but_save}</button>
    <button type="button" tabindex="1010" id="btnCANCEL" class="cancelbutton negative" accesskey="F2">{lang but_cancel}</button>
</div>

</div{* .frmNieuweAanvraag *}>
