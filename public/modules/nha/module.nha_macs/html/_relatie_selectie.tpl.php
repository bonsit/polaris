<div id="relatie_selectie">
    <input type="hidden" id="relatietype" value="RELATIE" />
    <div class="searchbox alignlabels">
        <h2 id="lblzoekrelatie">{lang search_active} {lang relatie}</h2>
        <div class="form-group form-group-sm">
          <label for="_fldZOEKRELATIE" class="col-sm-3 control-label">{lang relatienummer}:</label>
          <div class="col-sm-6">
            <input class="form-control" {if $form == 'artikelverkoop'}tabindex="112"{else}nofocus tabindex="-1"{/if} type="search" id="_fldZOEKRELATIE" size="17" value="{$pinrelatie}" />
          </div>
        </div>

        <div class="form-group form-group-sm">
          <label class="col-sm-3 control-label">{lang postcode_huisnummer}: </label>
          <div class="col-sm-6">
            <input class="form-control validation_uppercase" tabindex="113" type="search" id="_fldZOEKPOSTCODE" style="width:75px;" xvalue="" />
            <input class="form-control validation_uppercase" tabindex="114" type="search" id="_fldZOEKHUISNR" style="width:48px;"  xvalue="14" />
          </div>
        </div>

        <div class="form-group form-group-sm">
          <label for="_fldZOEKRELATIE" class="col-sm-3 control-label">{lang land}:</label>
          <div class="col-sm-6">
            <select class="form-control" tabindex="115" type="search" id="_fldZOEKLAND" style="width:130px;">{html_options options=$landen selected=$lang}</select>
          </div>
        </div>

        <div class="formviewrow"><label class="label">&nbsp;</label>
            <div class="formcolumn">
                <div class="buttons small">
                    <button tabindex="117" id="btnSEARCH" type="button" class="" accesskey="F7">{lang but_search}</button>
                </div>
            </div>
        </div>
    </div>

    <div class="searchresult">
        <div class="content bodyscroll">
            <table id="relaties" class="selectable relaties" tabindex="130">
              <thead>
                <tr>
                  <th>{lang relatienummer}</th>
                  <th>{lang naam}</th>
                  <th>{lang straat}</th>
                  <th>{lang postcode}</th>
                  <th>{lang plaats}</th>
                  <th>{lang land}</th>
                  <th>{lang handelingsonbekwaam}</th>
                </tr>
              </thead>
              <tbody>
                <tr class="relatie row">
                  <td class="nummer"></td>
                  <td class="naam"></td>
                  <td class="straat"></td>
                  <td class="postcode"></td>
                  <td class="plaats"></td>
                  <td class="land"></td>
                  <td class="handelingsonbekwaam"></td>
                </tr>
              </tbody>
            </table>
        </div>
    </div>
</div>
