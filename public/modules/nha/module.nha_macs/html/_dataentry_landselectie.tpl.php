<div class="btn-group btn-group-sm pull-right">
    <button tabindex="-1" name="_hdnLandSelectie" class="btn btn-{if $eigenland == 'NL'}primary{else}white{/if}" type="button">NL</button>
    <button tabindex="-1" name="_hdnLandSelectie" class="btn btn-{if $eigenland == 'BE'}primary{else}white{/if}" type="button">BE</button>
    <button tabindex="-1" name="_hdnLandSelectie" class="btn btn-{if $eigenland == 'DE'}primary{else}white{/if}" type="button">DE</button>
</div>

<!-- <div class="switcher switcher-three candy blue">
    <input id="NL_land" name="_hdnLandSelectie" type="radio" value="NL" tabindex="-1" {if $eigenland == 'NL'}checked{/if}>
    <label for="NL_land">NL</label>

    <input id="BE_land" name="_hdnLandSelectie" type="radio" value="BE" tabindex="-1" {if $eigenland == 'BE'}checked{/if}>
    <label for="BE_land">BE</label>

    <input id="DE_land" name="_hdnLandSelectie" type="radio" value="DE" tabindex="-1" {if $eigenland == 'DE'}checked{/if}>
    <label for="DE_land">DE</label>

    <span class="slide-button"></span>
</div>
 -->