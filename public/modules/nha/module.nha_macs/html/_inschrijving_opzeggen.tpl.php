<div id="opzegginginfocontainer" class="rounded">
<table id="opzegginginfo">
    <thead>
        <tr>
            <th>{lang totale_lesgeld}</th>
            <th>{lang lessen}</th>
            <th>{lang verzonden}</th>
            <th>{lang te_verzenden}</th>
            <th>{lang bedrag}</th>
            <th>{lang betaald}</th>
            <th>{lang te_betalen}</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>{lang huidige}</td>
            <td class="huidigAantalLessen"></td>
            <td class="huidigAantalVerzLessen"></td>
            <td class="huidigAantalTeVerzLessen"></td>
            <td class="huidigTotaalBedrag"></td>
            <td class="huidigBedragBetaald"></td>
            <td class="huidigBedragTeBetalen"></td>
        </tr>
        <tr>
            <td>{lang opzegging}</td>
            <td class="opzegAantalLessen"></td>
            <td class="opzegAantalVerzLessen"></td>
            <td class="opzegAantalTeVerzLessen"></td>
            <td class="opzegTotaalBedrag"></td>
            <td class="opzegBedragBetaald"></td>
            <td class="opzegBedragTeBetalen"></td>
        </tr>
        <tr class="total">
            <td>{lang verschil}</td>
            <td></td>
            <td></td>
            <td></td>
            <td class="verschilBedrag"></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>{lang hoogste}</td>
            <td></td>
            <td></td>
            <td></td>
            <td class="hoogsteBedrag"></td>
            <td></td>
            <td></td>
        </tr>
    </tbody>
</table>
</div>

<table>
    <tr>
        <td>{lang reden}: </td>
        <td>
            <select id="_fldREDENOPZEGGING" name="REDENOPZEGGING">
                <option></option>
                {section name=i loop=$redenopzegging}
                    <option value="{$redenopzegging[i].REDENOPZEGGING}">{$redenopzegging[i].REDENOPZEGGING}</option>
                {/section}
            </select>
        </td>
    </tr>
    <tr>
        <td>{lang toelichting}: </td>
        <td><input type="text" id="_fldTOELICHTINGREDENOPZEGGING" name="TOELICHTINGREDENOPZEGGING" style="width:300px" /></td>
    </tr>
    <tr>
        <td>{lang datum}: </td>
        <td><input type="text" id="_fldOPZEGGINGSDATUM" class="date_input required" name="OPZEGGINGSDATUM" style="width:80px" /></td>
    </tr>
</table>
