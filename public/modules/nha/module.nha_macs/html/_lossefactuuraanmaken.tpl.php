<h2>{lang betalingsverzoeken}</h2>
<div id="betalingsverzoeken" class="bv_tabel">
    <table id="betalingsverzoekentabel" class="striped data scrollable">
        <thead>
            <tr>
                <th>{lang termijn}</th>
                <th>{lang kenmerk}</th>
                <th>{lang status}</th>
                <th>{lang bedrag}</th>
                <th>{lang betaald}</th>
                <th>{lang te_betalen}</th>
                <th>{lang betaalwijze}</th>
                <th>{lang verzenddatum}</th>
                <th>{lang vervaldata_cursist_systeem}</th>
                <th>{lang herinnering}</th>
                <th>{lang regeling}</th>
                <th>{lang factuur}</th>
            </tr>
        </thead>
        <tbody>
            <tr class="betalingsverzoek">
                <td><input type="hidden" name="PLR__RECORDID[]" class="PLR__RECORDID" value="" />
                <span class="TERMIJN"></span></td>
                <td><span class="KENMERK"></span></td>
                <td><span class="STATUS"></span></td>
                <td class="right"><span class="BEDRAG"></span></td>
                <td class="right"><span class="BEDRAGBETAALD"></span></td>
                <td class="right"><span class="TEBETALEN"></span></td>
                <td class="middle"><span class="BETAALWIJZE"></span></td>
                <td><span class="VERZENDDATUM"></span></td>
                <td><span class="VERVALDATA"></span></td>
                <td class="middle"><input type="checkbox" disabled="disabled" class="HERINNERING" value="J" /></td>
                <td class="middle"><input type="checkbox" disabled="disabled" class="REGELING" value="J" /></td>
                <td class="middle"><input type="checkbox" name="FACTUUR[]" class="FACTUUR" value="" /></td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="3" class="right">Totaal</td>
                <td colspan="1" class="right"><input id="_fldTOTAALBEDRAG" class="totaalbedrag" disabled value="" /></td>
                <td colspan="1" class="right"><input id="_fldTOTAALBETAALD" class="totaalbedrag" disabled value="" /></td>
                <td colspan="1" class="right"><input id="_fldTOTAALBEDRAGTEBETALEN" class="totaalbedrag" disabled value="" /></td>
            </tr>
        </tfoot>
    </table>
</div>
