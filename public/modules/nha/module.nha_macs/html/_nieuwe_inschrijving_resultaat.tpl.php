<div id="resultaatformulier" class="jqmWindow shadowbox" style="display:none">
    <h2 style="text-transform:capitalize">{lang new} {lang inschrijving}</h2>
    <table id="resultaattabel">
        <tbody>
            <tr>
                <td class="column1">
                    <h3>{lang relatie} (<span class="RELATIENR">12345999</span>)</h3>
                    <span class="BEDRIJF"></span><span class="NAAM">Dhr. A.J.J. Bons</span><br />
                    <span class="ADRES">Cederhoven 14</span><br />
                    <span class="WOONPLAATS">6225HD  Maastricht</span><br />
                    <span class="LAND">Nederland</span><br />
                </td>
                <td class="column2">
                    <h3>{lang factuurrelatie} (<span class="FACTUURRELATIENR">12345999</span>)</h3>
                    <span class="FACTUURNAAM">Dhr. A.J.J. Bons</span><br />
                    <span class="FACTUURADRES">Cederhoven 14</span><br />
                    <span class="FACTUURWOONPLAATS">6225HD  Maastricht</span><br />
                    <span class="FACTUURLAND">Nederland</span><br />
                </td>
            </tr>
            <tr><td colspan="2">&nbsp;</td></tr>
            <tr>
                <td colspan="2">
                    <h3>{lang ingeschreven_voor_cursus}</h3>
                    <table id="">
                        <thead>
                            <tr>
                                <th style="width:12%;">{lang inschrijfnr}</th>
                                <th style="width:20%;">{lang cursus}</th>
                                <th style="width:50%;">{lang omschrijving}</th>
                                <th>{lang inschrijfgeld}</th>
                                <th>{lang premium}</th>
                            </tr>
                        </thead>
                        <tbody class="componentitems">
                            <tr class="inschrijving componentitem">
                                <td><span class="INSCHRIJFNR">123</span></td>
                                <td><span class="CURSUSCODE">aaa</span></td>
                                <td><span class="OMSCHRIJVING">aaa</span></td>
                                <td><span class="INSCHRIJFGELD">&euro; 25,00</span></td>
                                <td><span class="ACTIE"></span></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <br/>
    <div class="buttons">
        <button tabindex="1200" id="btnCLOSE" class="jqmClose closebutton positive">{lang but_close}</button>
    </div>
</div>
