<h1>Inschrijvingen met een foutieve studietempo</h1>
<p>Hieronder zie je de inschrijvingen met een foutieve studietempo. Je kunt deze studietempo's in een keer wijzigen.</p>

<div id="_rowONJUISTTEMPOLIJST" class="formviewrow">
  <div class="overzicht">
    <div class="content bodyscroll">
      <ul id="ONJUISTTEMPOLIJST"></ul>
    </div>
  </div>
</div>

<div id="_rowJUISTSTUDIETEMPO" class="formviewrow">
    <div class="formcolumn">
        Selecteer het juiste studietempo:
        <select id="selJUISTESTUDIETEMPO" name="JUISTESTUDIETEMPO"></select>
    </div>
</div>

<br />
<div class="buttons">
    <button type="button" tabindex="8800" id="btnSAVEANDCLOSEFOUTSTUDIETEMPO" value="yes" class="jqmAccept positive">OK</button>
    <button type="button" tabindex="8810" id="btnCLOSE" value="no" class="jqmClose secondary">Annuleer</button>
</div>
