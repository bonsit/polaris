{include file="shared.tpl.php"}

<input type="hidden" id="gepindecursus" value="{$gepindecursus}" />
<input type="hidden" id="_hdnMODULECODE" value="" />
<h2>
  Module: <select id="modulecodes" tabindex="-1"></select>
  &nbsp; <a href="#" id="deleteModule">Verwijder</a>
</h2>

<div id="cursusgegevens">
    <p>Modulenaam: <input type="text" name="MODULENAAM" size="50" /></p>
    <div class="pagetabcontainer">
        <ul id="pagetabs">
            <li><a id="MG" class="selected "href="#page_mg">Modulelessen</a></li>
        </ul>
    </div>

    <div id="page_mg" class="pagecontents" style="display:block;">
        <div class="overzicht">
            <div class="bodyscroll content">
                <table id="modulelestabel" class="selectable sttrijen" tabindex="10">
                  <thead>
                    <tr>
                      <th>Lesnr</th>
                      <th>Omschrijving</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr class="stt row">
                      <td class="lesnr"></td>
                      <td class="omschrijving"></td>
                    </tr>
                  </tbody>
                </table>
            </div>
            <div class="buttons small">
                <a class="btnVoegtoeItem" accesskey="CTRL+N">Voeg les toe</a>
                <a class="btnVerwijderItem" accesskey="CTRL+D">Verwijder les</a>
                <a class="btnRefreshItemList">Laad opnieuw</a>
            </div>
        </div>
    </div>

    <br />
    <div class="buttons">
        <a id="btnBuild" class="secondary"><img src="i/oxygen/actions/tools_wizard.png" /> Modulegegevens opslaan</a>
    </div>
</div>

<div id="nieuwemodule" class="jqmWindow shadowbox" style="display:none">
    <h1>Nieuwe module</h1>

    <div id="_rowMODULECODE" class="formviewrow">
        <label class="label">Modulecode: </label>
        <div class="formcolumn">
            <input type="text" label="Modulecode" class="text" id="_fldMODULECODE" name='NEW_MODULECODE' size="12" maxlength='10' value=""  />
        </div>
    </div>

    <div id="_rowMODULENAAM" class="formviewrow">
        <label class="label">Modulenaam: </label>
        <div class="formcolumn">
            <input type="text" label="Modulenaam" class="text" id="_fldMODULENAAM" name='NEW_MODULENAAM' size="60" maxlength='50' value=""  />
        </div>
    </div>

    <div id="_rowAANTALLESSEN" class="formviewrow">
        <label class="label">Aantal lessen: </label>
        <div class="formcolumn">
            <input type="text" label="Aantal lessen" class="text validation_integer {ldelim}aNeg: '', mNum: 3, mDec: 0{rdelim}" id="_fldAANTALLESSEN" name='NEW_AANTALLESSEN' size="5" maxlength='3' value=""  />
        </div>
    </div>
    <br />
    <div class="buttons">
        <button type="button" tabindex="8800" id="btnSAVEANDCLOSE" class="jqmClose closebutton positive">OK</button>
        <button type="button" tabindex="8810" id="btnCLOSE" class="jqmClose closebutton secondary">Annuleer</button>
    </div>
</div>
