{include file="shared.tpl.php"}

<div id="formview" class="frmInschrijvingOpzeggen">
<input type="hidden" name="_hdnFORMTYPE" value="inschrijvingopzeggen" />
<input type="hidden" id="_hdnRecordID" name="_hdnRecordID" value="" />

{include file="_inschrijving_selectie.tpl.php"}
{include file="_inschrijving_opzeggen.tpl.php"}

<br />
<div class="buttons">
    <button type="button" id="btnSAVE" class="savebutton positive disabled" accesskey="F9">{lang but_save}</button>
    <button type="button" id="btnCANCEL" class="cancelbutton negative" accesskey="F2">{lang but_cancel}</button>
</div>

</div{* .frmInschrijvingOpzeggen *}>
