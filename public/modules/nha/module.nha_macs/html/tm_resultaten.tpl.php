<h3>{lang resultaat}</h3>

<input type="hidden" id="_fldRESULTAAT" name="RESULTAAT" />

<div class="buttons">
    <button type="button" id="btnINSCHRIJVING" class="" accesskey="Ctrl+F7"><span><i class="fa fa-thumbs-o-up"></i> {lang inschrijving}</span></button>
    <button type="button" id="btnSTUDIEGIDSOPSTUREN" class="" accesskey="Ctrl+F8"><span><i class="fa fa-envelope"></i> {lang stuur_studiegids}</span></button>
    <button type="button" id="btnNABELLEN" class="" accesskey="Ctrl+F9" title="{lang nabellen_hint}"><span><i class="fa fa-phone-square"></i> {lang nabellen}</span></button>
    <br /><br />
    <button type="button" id="btnGEENINTERESSE" class="" accesskey="Ctrl+F10"><span><i class="fa fa-ban"></i> {lang geen_interesse}</span></button>
    <button type="button" id="btnAFSPRAAKMAKEN" class="" accesskey="Ctrl+F11" title="{lang afspraak_maken_hint}"><span><i class="fa fa-calendar"></i> {lang afspraak_maken}</span></button>
    <button type="button" id="btnLATERNABELLEN" class="" accesskey="Ctrl+F12" title="{lang later_nabellen_hint}"><span><i class="fa fa-phone"></i> {lang later_nabellen}</span></button>
    <br />
</div>
<hr class="fix" />
<br />
<div id="pnlGEENINTERESSE">
    <hr/>
    <table>
        <tr>
            <td>{lang reden}: </td>
            <td><select id="_fldOORZAAKGEENINTERESSE" class="OORZAAKGEENINTERESSE" name="OORZAAKGEENINTERESSE" tabindex="500"></select>
                {lang of}: <input type="text" id="_fldANDERRESULTAAT" class="ANDERRESULTAAT" name="ANDERRESULTAAT" tabindex="505" />
            </td>
        </tr>
        <tr>
            <td>{lang toelichting}: </td><td><input type="text" id="_fldTOELICHTINGGEENINTERESSE" class="TOELICHTINGGEENINTERESSE" name="TOELICHTINGGEENINTERESSE" style="width:400px;" tabindex="510" />
            &nbsp; <label for="_fldMAILINGSTUREN">{lang mailing_sturen}? </label><input type="checkbox" id="_fldMAILINGSTUREN" class="" name="MAILINGSTUREN" value="J" tabindex="540" checked="checked" />
            </td>
        </tr>
    </table>
</div>

<div id="pnlSTUDIEGIDSOPSTUREN">
    <input type="hidden" name="STUDIEGIDSNOGMAALSOPSTUREN" value="J" />
    <hr/>
    {lang studiegids_niet_ontvangen}
</div>