{include file="shared.tpl.php"}

<div id="formview" class="frmLosseFactuurAanmaken">
    <input type="hidden" name="_hdnFORMTYPE" value="lossefactuuraanmaken" />
    <input type="hidden" id="_hdnRecordID" name="_hdnRecordID" value="" />

    <input type="hidden" name="INSCHRIJFNR" id="_fldINSCHRIJFNR_NIEUW" value="" />
    <input type="hidden" name="RELATIENR" id="_fldRELATIENR_NIEUW" value="" />

    {include file="_inschrijving_selectie.tpl.php"}

    {lang selecteer_inschrijving}: <select id="inschrijvingenselect"></select>

    {include file="_factuur_gegevens.tpl.php"}

    {* KimP, ElizaS hoeven geen filters  ---   include file="_betalingsverzoeken_filter.tpl.php"*}
    {include file="_lossefactuuraanmaken.tpl.php"}

    <div class="xbuttons">
        <button type="button" id="btnSAVE" class="btn btn-success savebutton positive" accesskey="F9">{lang but_save}</button>
        <button type="button" id="btnCANCEL" class="btn btn-danger cancelbutton negative" accesskey="F2">{lang but_cancel}</button>
    </div>

</div{* .frmLosseFactuurAanmaken *}>

<div id="briefForm" class="jqmWindow" style="display:none;cursor:default" style="height:325px;width:600px;">

    <textarea id="_fldINHOUDBRIEF" name="INHOUDBRIEF_PLR" cols="115" rows="20" maxlength="4000" autofocus="autofocus"></textarea>
    <div>
        <div style="float:right">
            <select id="_fldSTANDAARDBRIEF" name="BEGELEIDENDEBRIEFCODE"></select>
            <button type="button" id="_btnINIT">{lang haal_brieftekst_op}</button>
        </div>
        <div class="buttons" style="font-size:0.8em;margin-top:10px;width:200px;">
            <button type="button" class="jqmClose positive savebutton">{lang but_close}</button>
        </div>
    </div>
</div>
