<div id="vooroplblok" class="VOOROPLEIDING">
    <p>Kies de vooropleiding(en) van de cursist en eventueel of hij/zij het diploma heeft gehaald.</p>
    <div>
        <input tabindex="-1" id="opl_lbovbo" type="checkbox" class="OPL" name="VOOROPLEIDING!VOOROPLEIDING[]" value="LBO_VBO" />
        <label for="opl_lbovbo">LBO/VBO</label>
        <input tabindex="-1" id="dpl_lbovbo" type="checkbox" class="DPL" class="OPL" name="VOOROPLEIDING!VOOROPLDIPLOMA[]" value="LBO_VBO" /><label for="dpl_lbovbo"> Diploma?</label>
    </div>
    <div>
        <input tabindex="-1" id="opl_mavovmbo" type="checkbox" class="OPL" name="VOOROPLEIDING!VOOROPLEIDING[]" value="MAVO_VMBO" />
        <label for="opl_mavovmbo">MAVO/VMBO</label>
        <input tabindex="-1" id="dpl_mavovmbo" type="checkbox" class="DPL" name="VOOROPLEIDING!VOOROPLDIPLOMA[]" value="MAVO_VMBO" /><label for="dpl_mavovmbo"> Diploma?</label>
    </div>
    <div>
        <input tabindex="-1" id="opl_havo" type="checkbox" class="OPL" name="VOOROPLEIDING!VOOROPLEIDING[]" value="HAVO" />
        <label for="opl_havo"> HAVO  (t/m jr 3)</label>
        <input tabindex="-1" id="dpl_havo" type="checkbox" class="DPL" name="VOOROPLEIDING!VOOROPLDIPLOMA[]" value="HAVO" /><label for="dpl_havo"> Diploma?</label>
    </div>
    <div>
        <input tabindex="-1" id="opl_vwo" type="checkbox" class="OPL" name="VOOROPLEIDING!VOOROPLEIDING[]" value="VWO" />
        <label for="opl_vwo">VWO (t/m jr 3)</label>
        <input tabindex="-1" id="dpl_vwo" type="checkbox" class="DPL" name="VOOROPLEIDING!VOOROPLDIPLOMA[]" value="VWO" /><label for="dpl_vwo"> Diploma?</label>
    </div>
    <div>
        <input tabindex="-1" id="opl_mbo" type="checkbox" class="OPL" name="VOOROPLEIDING!VOOROPLEIDING[]" value="MBO" />
        <label for="opl_mbo">MBO</label>
        <input tabindex="-1" id="dpl_mbo" type="checkbox" class="DPL" name="VOOROPLEIDING!VOOROPLDIPLOMA[]" value="MBO" /><label for="dpl_mbo"> Diploma?</label>
    </div>
    <div>
        <input tabindex="-1" id="opl_hbo" type="checkbox" class="OPL" name="VOOROPLEIDING!VOOROPLEIDING[]" value="HBO" />
        <label for="opl_hbo">HBO</label>
        <input tabindex="-1" id="dpl_hbo" type="checkbox" class="DPL" name="VOOROPLEIDING!VOOROPLDIPLOMA[]" value="HBO" /><label for="dpl_hbo"> Diploma?</label>
    </div>
    <div>
        <input tabindex="-1" id="opl_bach" type="checkbox" class="OPL" name="VOOROPLEIDING!VOOROPLEIDING[]" value="BACH" />
        <label for="opl_bach">Bachelor</label>
        <input tabindex="-1" id="dpl_bach" type="checkbox" class="DPL" name="VOOROPLEIDING!VOOROPLDIPLOMA[]" value="BACH" /><label for="dpl_bach"> Diploma?</label>
    </div>
    <div>
        Anders: <input tabindex="-1" id="opl_anders" type="text" style="width:122px;" name="VOOROPLEIDING!VOOROPLEIDING_ANDERS" value="" />

        <input tabindex="-1" id="dpl_anders" type="checkbox" class="DPL" name="VOOROPLEIDING!VOOROPLDIPLOMA_ANDERS" value="ANDERS" /><label for="dpl_anders"> Diploma?</label>
    </div>
</div>

{literal}
<script type="text/javascript">
    $(".DPL").change(function() {
        if ($(this).prop('checked'))
            $(this).parent().find('.OPL').prop('checked', true);
    });
    $(".OPL").change(function() {
        if (!$(this).prop('checked'))
            $(this).parent().find('.DPL').prop('checked', false);
    });
</script>
{/literal}