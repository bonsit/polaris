{include file="shared.tpl.php"}

<div id="formview" class="frmBetalingsVerzoekenSamenvoegen">
    <input type="hidden" name="_hdnFORMTYPE" value="betalingsverzoekensamenvoegen" />
    <input type="hidden" id="_hdnRecordID" name="_hdnRecordID" value="" />
    
    <input type="hidden" name="INSCHRIJFNR" id="_fldINSCHRIJFNR_NIEUW" value="" />
    <input type="hidden" name="RELATIENR" id="_fldRELATIENR_NIEUW" value="" />
    <input type="hidden" name="BETAALWIJZECODE" id="_fldBETAALWIJZECODE_NIEUW" value="ACP" />
    
    {include file="_inschrijving_selectie.tpl.php"}
    
    {* KimP zegt geen filter --- include file="_betalingsverzoeken_filter.tpl.php"*}
    {include file="_betalingsverzoeken_samenvoegen.tpl.php"}

    <br />
    <div class="buttons">
        <button type="button" id="btnSAVE" class="savebutton positive" accesskey="F9">Opslaan</button>
        <button type="button" id="btnCANCEL" class="cancelbutton negative" accesskey="F2">Annuleren</button>
    </div>
    
</div{* .frmBetalingsverzoekenSamenvoegen *}>