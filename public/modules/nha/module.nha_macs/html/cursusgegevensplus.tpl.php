{include file="shared.tpl.php"}

<div id="scherm_cursusgegevens" class="listview" style="display:none">
<input type="hidden" id="gepindecursus" value="{$gepindecursus}" />
<input type="hidden" id="_hdnCURSUSCODE" value="" />
<input type="hidden" id="_hdnRecordID" name="_hdnRecordID" value="" />

<h2 style="margin-left:40p">
<a href="javascript:void(0)" id="pinrecord_single" style="float:left;font-size:0.7em" class="pinrecord large" title="Pin dit record">
<span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-tag fa-stack-1x  fa-inverse"></i></span>
</a> &nbsp; &nbsp;
Cursusgroep: <select id="cursusgroepen" tabindex="-1">{html_options options=$cursusgroepen}</select>
&nbsp; Cursus: <select id="cursuscodes" tabindex="-1"></select>  </h2>

<div id="cursusgegevens">
    <div class="pagetabcontainer">
        <ul id="pagetabs">
            <li id="itemBG"><a id="BG" class="selected "href="#page_bg">Basisgegevens</a></li>
            <li id="itemCH"><a id="CH" href="#page_ch">Cohortgegevens</a></li>
            <li id="itemSTT"><a id="STT" href="#page_stt">Studietempotermijnen</a></li>
            <li id="itemBTT"><a id="BTT" href="#page_btt">Betaaltempotermijnen</a></li>
            <li id="itemCLS"><a id="CLS" href="#page_cls">Cursuslessen</a></li>
            <li id="itemCKL"><a id="CKL" href="#page_ckl">Checklist</a></li>
        </ul>
    </div>

    <div id="page_bg" class="pagecontents" style="display:block;">
        <div class="content">

            <div class="columngroup">
                <div id="_rowCURSUSCODE" class="formviewrow">
                  <label class="label" title="CURSUSCODE">Cursuscode:</label>
                  <div class="formcolumn">
                    <input type="text" label="Cursuscode" class="text readonly required validation_uppercase" id="_fldCURSUSCODE" name='CURSUSCODE' size="12" maxlength='15' value="">
                    <div class="buttons small" style="display:inline-block">
                        <a rel="CURSUSCODE" id="_btnWijzigCursusCode" class="button secondary storedprocedure"  href="{$serverroot}/app/macs/module/plr_execstoredproc/?action=302f86216142c590f7204c041652fa7c&firsttime=true" >Wijzig...</a>
                    </div>
                  </div>
                </div>

                <div id="_rowAANTALINSCHRIJVINGEN" class="formviewrow">
                  <label class="label" title="AANTALINSCHRIJVINGEN">Aantal inschrijvingen:</label>
                  <div class="formcolumn">
                    <input type="text" tabindex=-1 label="Aantal inschrijvingen" class="text readonly" id="_fldAANTALINSCHRIJVINGEN" size="2" maxlength='5' value="">
                  </div>
                </div>

                <div id="_rowCURSUSNAAM" class="formviewrow">
                  <label class="label" title="CURSUSNAAM">Cursusnaam:</label>
                  <div class="formcolumn">
                    <input type="text" label="Cursusnaam" class="text required validation_allchars" id="_fldCURSUSNAAM" name='CURSUSNAAM' size="32" maxlength='50' value="">
                  </div>
                </div>

                <div id="_rowWEBOMSCHRIJVING" class="formviewrow">
                  <label class="label" title="WEBOMSCHRIJVING">Webomschrijving:</label>
                  <div class="formcolumn">
                    <input type="text" label="Webomschrijving" class="text required validation_allchars" id="_fldWEBOMSCHRIJVING" name='WEBOMSCHRIJVING' size="32" maxlength='255' value="">
                  </div>
                </div>

                <div id="_rowCURSUSGROEP" class="formviewrow">
                  <label class="label" title="CURSUSGROEP">Cursusgroep:</label>
                  <div class="formcolumn">
                    <select size='1' name='CURSUSGROEP' id='_fldCURSUSGROEP' style="max-width:200px" label="Cursusgroep" class="required">
                    {html_options options=$cursusgroepen}
                    </select>
                  </div>
                </div>

                <div id="_rowADMINISTRATIE" class="formviewrow">
                  <label class="label" title="ADMINISTRATIE">Administratie:</label>
                  <div class="formcolumn">
                    <select size='1' name='ADMINISTRATIE' id='_fldADMINISTRATIE' style="max-width:200px" label="Administratie" class="required">
                    {html_options options=$administratie}
                    </select>
                  </div>
                </div>

                <div id="_rowSTUDIEGIDSCODE" class="formviewrow">
                  <label class="label" title="STUDIEGIDSCODE">Studiegids:</label>
                  <div class="formcolumn">
                    <select size='1' name='STUDIEGIDSCODE' id='_fldSTUDIEGIDSCODE' style="max-width:200px" label="StudiegidsCode" class="required">
                    {html_options options=$studiegidsen}
                    </select>
                  </div>
                </div>

                <div id="_rowLEAFLETCODE" class="formviewrow">
                  <label class="label" title="LEAFLETCODE">Leaflet:</label>
                  <div class="formcolumn">
                    <select size='1' name='LEAFLETCODE' id='_fldLEAFLETCODE' style="max-width:200px" label="Leafletcode">
                        <option value=""></option>
                        {html_options options=$leaflets}
                    </select>
                  </div>
                </div>

                <div id="_rowCURSUSDUURMAANDEN" class="formviewrow">
                  <label class="label" title="CURSUSDUURMAANDEN">Cursusduur:</label>
                  <div class="formcolumn">
                    <input type="text" label="Cursusduur" class="text validation_integer" data-v-max="999" id="_fldCURSUSDUURMAANDEN" name='CURSUSDUURMAANDEN' size="2" maxlength='3' value=""> maanden
                  </div>
                </div>

                <div id="_rowSTANDAARDSTUDIETEMPO" class="formviewrow">
                  <label class="label" title="STANDAARDSTUDIETEMPO">Standaard studietempo:</label>
                  <div class="formcolumn">
                    <select size='1' name='STANDAARDSTUDIETEMPO' id='_fldSTANDAARDSTUDIETEMPO' style="max-width:200px" label="Standaardstudietempo" class="required">
                    {html_options options=$studietempos}
                    </select>
                  </div>
                </div>

                <div id="_rowSTANDAARDBETAALTEMPOTERMIJNEN" class="formviewrow">
                  <label class="label" title="STANDAARDBETAALTEMPOTERMIJNEN">Standaard aantal betaaltempotermijnen:</label>
                  <div class="formcolumn">
                    <input type="text" label="Standaardbetaaltempotermijnen" class="text required validation_integer" data-v-max="999" id="_fldSTANDAARDBETAALTEMPOTERMIJNEN" name='STANDAARDBETAALTEMPOTERMIJNEN' size="2" maxlength='3' value="4">
                  </div>
                </div>

                <div id="_rowAANTALSEALPAKKETTEN" class="formviewrow">
                  <label class="label" title="AANTALSEALPAKKETTEN">Aantal sealpakketten:</label>
                  <div class="formcolumn">
                    <input type="text" label="Aantal sealpakketten" class="text" id="_fldAANTALSEALPAKKETTEN" name='AANTALSEALPAKKETTEN' size="2" maxlength='5' value="">
                  </div>
                </div>

                <div id="_rowAANTALMAANDENONDERBREKING" class="formviewrow">
                  <label class="label" title="AANTALMAANDENONDERBREKING">Aantal maanden onderbreking:</label>
                  <div class="formcolumn">
                    <input type="text" label="Aantal maanden onderbreking" class="text required validation_integer" data-v-max="99"  id="_fldAANTALMAANDENONDERBREKING" name='AANTALMAANDENONDERBREKING' size="2" maxlength='2' value="4">
                  </div>
                </div>

                <div id="_rowZENDINGINDIENBETAALDJANEE" class="formviewrow">
                  <label class="label" title="ZENDINGINDIENBETAALDJANEE">Krijgt zending indien betaald?</label>
                  <div class="formcolumn">
                    <select size='1' name='ZENDINGINDIENBETAALDJANEE' id='_fldZENDINGINDIENBETAALDJANEE' class=''>
                        <option value=""></option>
                        <option value="J">Ja</option>
                    </select>
                  </div>
                </div>

                <div id="_rowBEGELEIDINGMETOFZONDER" class="formviewrow">
                  <label class="label" title="BEGELEIDINGMETOFZONDER">Zelfstudie?</label>
                  <div class="formcolumn">
                    <input type="radio" name='BEGELEIDINGMETOFZONDER' id='_fldBEGELEIDINGMETOFZONDER_M' value="M" class="required" /><label for="_fldBEGELEIDINGMETOFZONDER_M"> Nee </label>&nbsp;
                    <input type="radio" name='BEGELEIDINGMETOFZONDER' id='_fldBEGELEIDINGMETOFZONDER_Z' value="Z" /><label for="_fldBEGELEIDINGMETOFZONDER_Z">  Ja </label>&nbsp;
                    <input type="radio" name='BEGELEIDINGMETOFZONDER' id='_fldBEGELEIDINGMETOFZONDER_MZ' value="MZ" /><label for="_fldBEGELEIDINGMETOFZONDER_MZ">  Beide</label>
                  </div>
                </div>

                <div id="_rowBELLENNAGEENREACTIEJANEE" class="formviewrow">
                  <label class="label" title="BELLENNAGEENREACTIEJANEE">Bellen na geen reactie?</label>
                  <div class="formcolumn">
                    <div class="required">
                      <input type="radio" id="_fldBELLENNAGEENREACTIEJANEE_J" checked="checked" name="BELLENNAGEENREACTIEJANEE" value="J" class="required" label="Bellen na geen reactie?">
                      &nbsp;<label for="_fldBELLENNAGEENREACTIEJANEE:J">Ja&nbsp;</label>
                      <input type="radio" id="_fldBELLENNAGEENREACTIEJANEE:N" name="BELLENNAGEENREACTIEJANEE" value="N" class="" label="Bellen na geen reactie?">
                      &nbsp;<label for="_fldBELLENNAGEENREACTIEJANEE:N">Nee&nbsp;</label>
                    </div>
                  </div>
                </div>

                <div id="_rowEXTERNINTERNEXAMEN" class="formviewrow">
                  <label class="label" title="EXTERNINTERNEXAMEN">Extern/Intern-examen:</label>
                  <div class="formcolumn">
                    <select size='1' name='EXTERNINTERNEXAMEN' id='_fldEXTERNINTERNEXAMEN' class=''>
                      <option value="E">Extern</option>
                      <option value="I" selected>Intern</option>
                      <option value="EI">Beide</option>
                      <option value="G">Geen</option>
                    </select>
                  </div>
                </div>

                <div id="_rowEXAMENGELD" class="formviewrow">
                  <label class="label" title="EXAMENGELD">Examengeld:</label>
                  <div class="formcolumn">
                    <input type="text" label="Examengeld" class="text validation_float" data-v-max="9999999.99" data-m-dec="2" id="_fldEXAMENGELD" name='EXAMENGELD@@FLOATCONVERT' size="9" maxlength='9' value=""> euro
                  </div>
                </div>

                <div id="_rowTOTAALAANTALLESSEN" class="formviewrow">
                  <label class="label" title="TOTAALAANTALLESSEN">Totaal aantal lessen:</label>
                  <div class="formcolumn">
                    <input type="text" label="Totaal aantal lessen" readonly="readonly" tabindex="-1" class="text readonly validation_integer" data-v-max="999" id="_fldTOTAALAANTALLESSEN" name='TOTAALAANTALLESSEN' size="2" maxlength='3' value="">
                  </div>
                </div>

                <div id="_rowSOORT_CURSUS" class="formviewrow">
                  <label class="label" title="SOORT_CURSUS">Soort Macs cursus:</label>
                  <div class="formcolumn">
                    <select size='1' name='SOORT_CURSUS' id='_fldSOORT_CURSUS' class='' >
                        <option value="" selected>Normaal</option>
                        <option value="A">Algemeen</option>
                        <option value="C">Cohort</option>
                    </select>
                  </div>
                </div>

                <div id="_rowMBOHBONIVEAU" class="formviewrow">
                  <label class="label" title="MBOHBONIVEAU">Niveau MBO/HBO:</label>
                  <div class="formcolumn">
                    <select size='1' name='MBOHBONIVEAU' id='_fldMBOHBONIVEAU' class=''>
                      <option value=""></option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="3/4">3/4 (nog omzetten)</option>
                    </select>
                  </div>
                </div>

                <div id="_rowSOORTONDERWIJS" class="formviewrow">
                  <label class="label" title="SOORTONDERWIJS">Soort MBO/HBO onderwijs:</label>
                  <div class="formcolumn">
                    <select size='1' name='SOORTONDERWIJS' id='_fldSOORTONDERWIJS' class='' >
                        <option value=''></option>
                        <option value="DK">Deelkwalificaties</option>
                        <option value="BGO">Beroepsgericht Onderwijs</option>
                    </select>
                  </div>
                </div>

                <div id="_rowLEERWEG" class="formviewrow">
                  <label class="label" title="LEERWEG">MBO/HBO Leerweg:</label>
                  <div class="formcolumn">
                    <select size='1' name='LEERWEG' id='_fldLEERWEG' class='' >
                        <option value=''></option>
                        <option value="BOL">BOL</option>
                        <option value="BBL">BBL</option>
                        <option value="OVO">OVO</option>
                    </select>
                  </div>
                </div>

                <div id="_rowGETUIGSCHRIFTJANEE" class="formviewrow">
                  <label class="label" title="Heeft de cursist de mogelijkheid om na afronden van zijn cursus een Getuigschrift via Studenten Plaza aan te vragen?">Kan cursist getuigschrift aanvragen?</label>
                  <div class="formcolumn">
                      <input type="radio" id="_fldGETUIGSCHRIFTJANEE_J" checked="checked" name="GETUIGSCHRIFTJANEE" value="J" class="required" label="Getuigschrift?">
                      &nbsp;<label for="_fldGETUIGSCHRIFTJANEE:J">Ja&nbsp;</label>
                      <input type="radio" id="_fldGETUIGSCHRIFTJANEE_N" name="GETUIGSCHRIFTJANEE" value="N" class="" label="Getuigschrift?">
                      &nbsp;<label for="_fldGETUIGSCHRIFTJANEE:N">Nee&nbsp;</label>
                  </div>
                </div>

                <div id="_rowCURSUSCLUSTERNR" class="formviewrow">
                  <label class="label" title="Deze cursus behoort tot de cluster">Cursuscluster:</label>
                  <div class="formcolumn">
                    <select size='1' name='CURSUSCLUSTERNR' id='_fldCURSUSCLUSTERNR' style="max-width:200px" label="Deze cursus behoort tot de cluster">
                        <option value=""></option>
                        {html_options options=$cursusclusters}
                    </select>
                  </div>
                </div>

                {*
                <div id="" class="formviewrow">
                  <label class="label" title="">
                  <br />
                  <input id="_fldPASTOECOHORTEN" type="checkbox" name="pastoecohorten" value="J" /> <label for="_fldPASTOECOHORTEN">Pas toe op alle toekomstige cohorten</label>
                  </label>
                </div>
                *}

            </div>

            <div class="columngroup">

                <div id="_rowDATUMGELDIGVANAF" class="formviewrow">
                  <label class="label" title="DATUMGELDIGVANAF">Geldig vanaf:</label>
                  <div class="formcolumn">
                    <input type='text' id='_fldDATUMGELDIGVANAF' name='DATUMGELDIGVANAF' label="Datum geldig vanaf" class='text validation_date date_input required' size='11'>
                  </div>
                </div>

                <div id="_rowDATUMGELDIGTM" class="formviewrow">
                  <label class="label" title="DATUMGELDIGTM">Geldig t/m:</label>
                  <div class="formcolumn">
                    <input type='text' id='_fldDATUMGELDIGTM' name='DATUMGELDIGTM' label="Datum geldig t/m" class='text date_input' value='' size='11'>
                  </div>
                </div>

                <div id="_rowCNUMMER" class="formviewrow">
                  <label class="label" title="CNUMMER">C-nummer:</label>
                  <div class="formcolumn">
                    <input type='text' id='_fldCNUMMER' name='CNUMMER' label="C-nummer" class='text' value='' size='11'>
                  </div>
                </div>

                <div id="_rowPLATHOSID" class="formviewrow">
                  <label class="label" title="PLATHOSID">Plathos-id:</label>
                  <div class="formcolumn">
                    <input type='text' id='_fldPLATHOSID' name='PLATHOSID' label="Plathos-id" class='text' value='' size='11'>
                  </div>
                </div>

                <div id="_row" class="formviewrow">
                    <label class='label'>
                        <span class='textline'>Retourneren / Opzeggen</span>
                    </label>
                </div>

                <div id="_rowRETOURNEERBAARJANEE" class="formviewrow">
                  <label class="label" title="RETOURNEERBAARJANEE">Retourneerbaar?</label>
                  <div class="formcolumn">
                    <div class="required">
                      <input type="radio" id="_fldRETOURNEERBAARJANEE_J" checked="checked" name="RETOURNEERBAARJANEE" value="J" class="required" label="Retourneerbaar?">&nbsp;<label for="_fldRETOURNEERBAARJANEE_J">Ja&nbsp;</label> <input type="radio" id="_fldRETOURNEERBAARJANEE_N" name="RETOURNEERBAARJANEE" value="N" class="" label="Retourneerbaar?">&nbsp;<label for="_fldRETOURNEERBAARJANEE_N">Nee&nbsp;</label>
                    </div>
                  </div>
                </div>

                <div id="_rowOPZEGBAARJANEE" class="formviewrow">
                  <label class="label" title="OPZEGBAARJANEE">Opzegbaar?</label>

                  <div class="formcolumn">
                    <div class="required">
                      <input type="radio" id="_fldOPZEGBAARJANEE_J" checked="checked" name="OPZEGBAARJANEE" value="J" class="required" label="Opzegbaar?">&nbsp;<label for="_fldOPZEGBAARJANEE_J">Ja&nbsp;</label> <input type="radio" id="_fldOPZEGBAARJANEE_N" name="OPZEGBAARJANEE" value="N" class="" label="Opzegbaar?">&nbsp;<label for="_fldOPZEGBAARJANEE_N">Nee&nbsp;</label>
                    </div>
                  </div>
                </div>

                <div id="_rowAANTALLESSENOPZEGGING" class="formviewrow">
                  <label class="label" title="AANTALLESSENOPZEGGING">Aantal lessen opzegging:</label>
                  <div class="formcolumn">
                    <input type="text" label="Aantal lessen opzegging" class="text validation_integer {ldelim}aNeg: '', mNum: 2, mDec: 0{rdelim}" id="_fldAANTALLESSENOPZEGGING" name='AANTALLESSENOPZEGGING' size="2" maxlength='2' value="">
                  </div>
                </div>

                <div id="_rowAANTALLESSENPERMAANDBIJOPZEG" class="formviewrow">
                  <label class="label" title="AANTALLESSENPERMAANDBIJOPZEG">Aantal lessen p/m bij opzegging:</label>

                  <div class="formcolumn">
                    <input type="text" label="Aantal lessen p/m bij opzegging" class="text validation_integer {ldelim}aNeg: '', mNum: 2, mDec: 0{rdelim}" id="_fldAANTALLESSENPERMAANDBIJOPZEG" name='AANTALLESSENPERMAANDBIJOPZEG' size="2" maxlength='2' value="">
                  </div>
                </div>

                <div id="_row" class="formviewrow">
                    <label class='label'>
                        <br />
                        <span class='textline'>Logistiek</span>
                    </label>
                </div>

                <div id="_rowSTICKERVENSTERENVELOPE1" class="formviewrow">
                  <label class="label" title="STICKERVENSTERENVELOPE1">Eerste zending:</label>
                  <div class="formcolumn">
                      <input type="radio" id="_fldSTICKERVENSTERENVELOPE1_ST" name="STICKERVENSTERENVELOPE1" value="ST" class="required default" label="Soort afdruk 1">&nbsp;<label for="_fldSTICKERVENSTERENVELOPE1_ST">Sticker&nbsp;</label>
                      <input type="radio" id="_fldSTICKERVENSTERENVELOPE1_VE" name="STICKERVENSTERENVELOPE1" value="VE" class="" label="Soort afdruk 1">&nbsp;<label for="_fldSTICKERVENSTERENVELOPE1_VE">Vensterenvelop&nbsp;</label>
                  </div>
                </div>

                <div id="_rowSTICKERVENSTERENVELOPEN" class="formviewrow">
                  <label class="label" title="STICKERVENSTERENVELOPEN">Vervolg zendingen:</label>
                  <div class="formcolumn">
                      <input type="radio" id="_fldSTICKERVENSTERENVELOPEN_ST" name="STICKERVENSTERENVELOPEN" value="ST" class="required default" label="Soort afdruk Vervolg">&nbsp;<label for="_fldSTICKERVENSTERENVELOPEN_ST">Sticker&nbsp;</label>
                      <input type="radio" id="_fldSTICKERVENSTERENVELOPEN_VE" name="STICKERVENSTERENVELOPEN" value="VE" class="" label="Soort afdruk Vervolg">&nbsp;<label for="_fldSTICKERVENSTERENVELOPEN_VE">Vensterenvelop&nbsp;</label>
                  </div>
                </div>

                <div id="_rowLIGTOPSORTEERPOSITIE" class="formviewrow">
                  <label class="label" title="LIGTOPSORTEERPOSITIE">Ligt op sorteerpositie:</label>
                  <div class="formcolumn">
                    <input type="text" label="Ligt op sorteerpositie" class="text required validation_integer {ldelim}aNeg: '', mNum: 5, mDec: 0{rdelim}" id="_fldLIGTOPSORTEERPOSITIE" name='LIGTOPSORTEERPOSITIE' size="2" maxlength='5' value="">
                  </div>
                </div>

                <div id="_rowLESBLOKPLUSENVELOP" class="formviewrow">
                  <label class="label" title="LESBLOKPLUSENVELOP">Lesblok incl. envelop:</label>
                  <div class="formcolumn">
                    <div class="required">
                      <input type="radio" id="_fldLESBLOKPLUSENVELOP_J" checked="checked" name="LESBLOKPLUSENVELOP" value="J" class="default" label="Lesblok incl. envelop">&nbsp;<label for="_fldLESBLOKPLUSENVELOP_J">Ja&nbsp;</label> <input type="radio" id="_fldLESBLOKPLUSENVELOP:N" name="LESBLOKPLUSENVELOP" value="N" class="" label="Lesblok incl. envelop">&nbsp;<label for="_fldLESBLOKPLUSENVELOP:N">Nee&nbsp;</label>
                    </div>
                  </div>
                </div>

                <div id="_rowAANTALRINGBANDEN" class="formviewrow">
                  <label class="label" title="AANTALRINGBANDEN">Aantal ringbanden:</label>
                  <div class="formcolumn">
                    <input type="text" label="Aantal ringbanden" class="text required validation_integer {ldelim}aNeg: '', mNum: 2, mDec: 0{rdelim}" id="_fldAANTALRINGBANDEN" name='AANTALRINGBANDEN' size="2" maxlength='2' value="">
                  </div>
                </div>

                <div id="_row" class="formviewrow">
                    <label class='label'>
                        <br />
                        <span class='textline'>Activeren / Inactiveren</span>
                    </label>
                </div>

                <div id="_rowGEBLOKKEERDJANEE" class="formviewrow">
                  <label class="label" title="GEBLOKKEERDJANEE">Cursus verborgen?</label>
                  <div class="formcolumn">
                    <div class="required">
                      <input type="radio" id="_fldGEBLOKKEERDJANEE_J" name="GEBLOKKEERDJANEE" value="J" class="required default" label="Cursus verborgen?">&nbsp;<label for="_fldGEBLOKKEERDJANEE_J">Ja&nbsp;</label> <input type="radio" id="_fldGEBLOKKEERDJANEE:N" checked="checked" name="GEBLOKKEERDJANEE" value="N" class="" label="Cursus verborgen?">&nbsp;<label for="_fldGEBLOKKEERDJANEE:N">Nee&nbsp;</label>
                    </div>
                  </div>
                </div>

                <div id="_rowALLEENZICHTBAARVOORBACKOFF" class="formviewrow">
                  <label class="label" title="ALLEENZICHTBAARVOORBACKOFF">Alleen zichtbaar voor Backoffice?</label>
                  <div class="formcolumn">
                    <div class="required">
                      <input type="radio" id="_fldALLEENZICHTBAARVOORBACKOFF_J" checked="checked" name="ALLEENZICHTBAARVOORBACKOFF" value="J" class="required" label="Alleen zichtbaar voor Backoffice?">&nbsp;<label for="_fldALLEENZICHTBAARVOORBACKOFF_J">Ja&nbsp;</label> <input type="radio" id="_fldALLEENZICHTBAARVOORBACKOFF_N" name="ALLEENZICHTBAARVOORBACKOFF" value="N" class="default" label="Alleen zichtbaar voor Backoffice?">&nbsp;<label for="_fldALLEENZICHTBAARVOORBACKOFF_N">Nee&nbsp;</label>
                    </div>
                  </div>
                </div>

                <div id="_rowINSCHRIJVINGENBLOKKERENJANEE" class="formviewrow">
                  <label class="label" title="INSCHRIJVINGENBLOKKERENJANEE">Inschrijvingen toetsen?</label>
                  <div class="formcolumn">
                    <div class="required">
                      <input type="radio" id="_fldINSCHRIJVINGENBLOKKERENJANEE_J" name="INSCHRIJVINGENBLOKKERENJANEE" value="J" class="required" label="Inschrijvingen toetsen?">&nbsp;<label for="_fldINSCHRIJVINGENBLOKKERENJANEE_J">Ja&nbsp;</label> <input type="radio" id="_fldINSCHRIJVINGENBLOKKERENJANEE_N" checked="checked" name="INSCHRIJVINGENBLOKKERENJANEE" value="N" class="default" label="Inschrijvingen toetsen?">&nbsp;<label for="_fldINSCHRIJVINGENBLOKKERENJANEE_N">Nee&nbsp;</label>
                    </div>
                  </div>
                </div>

                <!--
                <div id="_rowGEBUFFERDJANEE" class="formviewrow">
                  <label class="label" title="GEBUFFERDJANEE">Inschrijvingen bufferen?</label>
                  <div class="formcolumn">
                    <div class="required">
                      <input type="radio" id="_fldGEBUFFERDJANEE_J" name="GEBUFFERDJANEE" value="J" class="required" label="Inschrijvingen bufferen?">&nbsp;<label for="_fldGEBUFFERDJANEE_J">Ja&nbsp;</label> <input type="radio" id="_fldGEBUFFERDJANEE_N" checked="checked" name="GEBUFFERDJANEE" value="N" class="default" label="Inschrijvingen bufferen?">&nbsp;<label for="_fldGEBUFFERDJANEE_N">Nee&nbsp;</label>
                    </div>
                  </div>
                </div>
                -->

                <div id="_row" class="formviewrow">
                    <label class='label'>
                    </label>
                </div>

                <div id="_rowMATERIAALCOMPLEETJANEE" class="formviewrow">
                  <label class="label" title="MATERIAALCOMPLEETJANEE">Materiaal compleet?</label>
                  <div class="formcolumn">
                    <div class="required">
                      <input type="radio" id="_fldMATERIAALCOMPLEETJANEE_J" checked="checked" name="MATERIAALCOMPLEETJANEE" value="J" class="required default" label="Materiaal compleet?">&nbsp;<label for="_fldMATERIAALCOMPLEETJANEE_J">Ja&nbsp;</label> <input type="radio" id="_fldMATERIAALCOMPLEETJANEE_N" name="MATERIAALCOMPLEETJANEE" value="N" class="" label="Materiaal compleet?">&nbsp;<label for="_fldMATERIAALCOMPLEETJANEE_N">Nee&nbsp;</label>
                    </div>
                  </div>
                </div>

                <div id="_rowMATERIAALVERWACHTDATUM" class="formviewrow">
                  <label class="label" title="MATERIAALVERWACHTDATUM">Materiaal verwacht op:</label>
                  <div class="formcolumn">
                    <input type="text" label="Materiaal verwacht op" class="text validation_allchars" id="_fldMATERIAALVERWACHTDATUM" name='MATERIAALVERWACHTDATUM' size="30" maxlength='100' value="">
                  </div>
                </div>

                <div id="_rowTEGEMOETKOMINGWACHTTIJD" class="formviewrow">
                  <label class="label" title="TEGEMOETKOMINGWACHTTIJD">Tegemoetkoming wachttijd tekst:</label>
                  <div class="formcolumn">
                    <input type="text" label="Tegemoetkoming wachttijd tekst" class="text validation_allchars" id="_fldTEGEMOETKOMINGWACHTTIJD" name='TEGEMOETKOMINGWACHTTIJD' size="40" maxlength='255' value="">
                    <button class="button" type="button" onclick="$('#_fldTEGEMOETKOMINGWACHTTIJD').val('P.S. Als tegemoetkoming voor de wachttijd hoeft u geen 25 euro inschrijfgeld te betalen.')"><- Standaard tekst</button>
                  </div>
                </div>

                <div id="_row" class="formviewrow">
                    <label class='label'>
                    </label>
                </div>

                <div id="_rowONDERTEK_VOOR_VERSTUREN_JN" class="formviewrow">
                  <label class="label" title="ONDERTEK_VOOR_VERSTUREN_JN">Inschrijfform. ondertekenen, dan verzenden?</label>
                  <div class="formcolumn">
                      <input type="radio" id="ONDERTEK_VOOR_VERSTUREN_JN_J" name="ONDERTEK_VOOR_VERSTUREN_JN" value="J" class="required" label="Eerst ondertekenen, dan verzenden?">&nbsp;<label for="_fldONDERTEK_VOOR_VERSTUREN_JN_J">Ja&nbsp;</label> <input type="radio" id="_fldONDERTEK_VOOR_VERSTUREN_JN_N" name="ONDERTEK_VOOR_VERSTUREN_JN" value="N" class="default" label="Eerst ondertekenen, dan verzenden?">&nbsp;<label for="_fldONDERTEK_VOOR_VERSTUREN_JN_N">Nee&nbsp;</label>
                  </div>
                </div>

                <div id="_rowINSCHRIJFFORM_PER_EMAIL_JN" class="formviewrow">
                  <label class="label" title="INSCHRIJFFORM_PER_EMAIL_JN">Inschrijfform meteen emailen?</label>
                  <div class="formcolumn">
                      <input type="radio" id="INSCHRIJFFORM_PER_EMAIL_JN_J" name="INSCHRIJFFORM_PER_EMAIL_JN" value="J" class="required" label="Inschrijfform meteen emailen?">&nbsp;<label for="_fldINSCHRIJFFORM_PER_EMAIL_JN_J">Ja&nbsp;</label> <input type="radio" id="_fldINSCHRIJFFORM_PER_EMAIL_JN_N" name="INSCHRIJFFORM_PER_EMAIL_JN" value="N" class="default" label="Inschrijfform meteen emailen?">&nbsp;<label for="_fldINSCHRIJFFORM_PER_EMAIL_JN_N">Nee&nbsp;</label>
                  </div>
                </div>

            </div>

            <div class="columngroupreset"></div>
        </div>
    </div>

    <div id="page_ch" class="pagecontents" style="display:block;">
        <div class="columngroup" id="cohortColumnGroup" style="float:right;width:600px;">
           <div class="content">

            <div id="" class="formviewrow">
              <label class="label">Cursisten kunnen zich voor deze</label>
              <div class="formcolumn">
              </div>
            </div>

            <div id="_rowDATUMGELDIGVANAF" class="formviewrow">
              <label class="label" title="DATUMGELDIGVANAF">cohort inschrijven vanaf:</label>
              <div class="formcolumn">
                <input type='text' id='_fldCOHORT_DATUMGELDIGVANAF' name='COHORT_DATUMGELDIGVANAF' label="Datum geldig vanaf" format='d-m-yy' class='text readonly' value='' size='11'>
              </div>
            </div>

            <div id="_rowDATUMGELDIGTM" class="formviewrow">
              <label class="label" title="DATUMGELDIGTM">t/m:</label>
              <div class="formcolumn">
                <input type='text' id='_fldCOHORT_DATUMGELDIGTM' name='COHORT_DATUMGELDIGTM' label="Datum geldig t/m" format='d-m-yy' class='text readonly' value='' size='11'>
              </div>
            </div>

            <div id="_row" class="formviewrow">
                <label class='label'>
                    <span class='textline'>Cohortgegevens</span>
                </label>
            </div>

            <div id="_rowCOHORT" class="formviewrow">
              <label class="label" title="COHORT">Cohort:</label>
              <div class="formcolumn">
                <select name="COHORT_COHORT" label="Cohort" id="_fldCOHORT_COHORT">
                <option value=""></option>
                {html_options options=$cohorten}
                <option value="NIEUWCOHORT">&lt;Nieuw cohort...&gt;</option>
                </select>
              </div>
            </div>

            <div id="_row" class="formviewrow">
                <label class='label'>
                    <span class='textline'></span>
                </label>
            </div>

            <div id="_rowDOSSIERCOHORT" class="formviewrow">
              <label class="label" title="DOSSIERCOHORT">Dossiercohort:</label>
              <div class="formcolumn">
                <select name="COHORT_DOSSIERCOHORT" label="Cohort" id="_fldCOHORT_DOSSIERCOHORT">
                <option value=""></option>
                {html_options options=$dossiercohorten}
                <option value="NIEUWDOSSIERCOHORT">&lt;Nieuw dossiercohort...&gt;</option>
                </select>
              </div>
            </div>

            <div id="_rowCNUMMER" class="formviewrow">
              <label class="label" title="CNUMMER">C-nummer:</label>
              <div class="formcolumn">
                <input type="text" label="C-nummer" class="text validation_allchars" id="_fldCOHORT_CNUMMER" name='COHORT_CNUMMER' size="10" maxlength='10' value="">
              </div>
            </div>

            <div id="_rowKDCODE" class="formviewrow">
              <label class="label" title="KDCODE">Kwalificatiedossier:</label>
              <div class="formcolumn">
                <select name="COHORT_KDCODE" label="KD-code" id="_fldCOHORT_KDCODE">
                <option value=""></option>
                {html_options options=$kdcodes}
                </select>
              </div>
            </div>

          </div>
        </div>

        <div class="overzicht cohortenlijst">
            <div class="content xbodyscroll">

                <table id="chtabel" class="selectable chrijen" tabindex="10">
                  <thead>
                    <tr>
                      <th style="width:40%" colspan="1">Cohorten <span style="font-weight:normal"> - geldigheidsperiode</span></th>
                      <th style="width:20%" colspan="1">Aantal inschr.</th>
                      <th style="width:60%" colspan="1"></th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr class="ch row">
                      <td class="cohort"></td>
                      <td class="aantalinschrijvingen"></td>
                      <td class="acties"><div class="buttons small">{*<button class="wijzigPeriode">Wijzig periode...</button> *}<button class="toonCohort">Tonen</button></div></td>
                    </tr>
                  </tbody>
                </table>
            </div>

            <div class="buttons small">
                <a class="btnVoegtoeItemCohort" accesskey="CTRL+N">Voeg toe</a>
                <a class="btnVerwijderItemCohort" accesskey="CTRL+D">Verwijder</a>
                <a class="btnKopieerItemCohort" accesskey="CTRL+K">Kopieer</a>
                <a class="btnRefreshItemList">Laad opnieuw</a>
            </div>
        </div>

        <div class="columngroupreset"></div>

    </div>

    <div id="page_stt" class="pagecontents" style="display:none;">
        <div class="overzicht">
            <div class="content xbodyscroll">
                <table id="stttabel" class="selectable sttrijen" tabindex="10">
                  <thead>
                    <tr>
                      <th style="width:60px">Studietempo</th>
                      <th style="width:40px">Termijn</th>
                      <th style="width:80px">Lessen</th>
                      <th style="width:10px">Omschr.</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr class="stt row">
                      <td class="studietempo"></td>
                      <td class="termijn"></td>
                      <td class="lessen"></td>
                      <td class="hoverpop"></td>
                    </tr>
                  </tbody>
                </table>
                <div class="opmerking">
                    <p>U kunt alleen van Cohortcursussen de studietempotermijnen aanpassen.</p>
                </div>
            </div>
            <div class="buttons small">
                <a class="btnVoegtoeItem" accesskey="CTRL+N">Voeg toe</a>
                <a class="btnVerwijderItem" accesskey="CTRL+D">Verwijder</a>
                <a class="btnRefreshItemList">Laad opnieuw</a>
            </div>
        </div>
    </div>

    <div id="page_btt" class="pagecontents" style="display:none;">
        <div class="overzicht">
            <div class="content xbodyscroll">
                <table id="btttabel" class="selectable bttrijen" tabindex="10">
                  <thead>
                    <tr>
                      <th style="width:60px">Aantal termijnen</th>
                      <th style="width:20px">Zelfstudie?</th>
                      <th style="width:60px">Bedrag</th>
                      <th style="width:60px">Bedrag Belgi&euml;</th>
                      <th style="width:60px">Bedrag Belgi&euml; Oorspr.</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr class="btt row">
                      <td class="aantaltermijnen"></td>
                      <td class="zelfstudie"></td>
                      <td class="bedrag"></td>
                      <td class="bedragbe"></td>
                      <td class="bedragbeoorspr"></td>
                    </tr>
                  </tbody>
                </table>
                <div class="opmerking">
                    <p>U kunt alleen van Cohortcursussen de betaaltempotermijnen aanpassen.</p>
                </div>
            </div>
            <div class="buttons small">
                <a class="btnVoegtoeItem" accesskey="CTRL+N">Voeg toe</a>
                <a class="btnVerwijderItem" accesskey="CTRL+D">Verwijder</a>
                <a class="btnRefreshItemList">Laad opnieuw</a>
            </div>
        </div>
    </div>

    <div id="page_cls" class="pagecontents" style="display:none;">
        <div class="overzicht">
            <h2>Hier kun je in de toekomst de Cursuslessen/sealpakketten aanmaken.</h2>
            <p><br /><br /><br /><br /><br /><br /><br /><br /></p>
        </div>
    </div>

    <div id="page_ckl" class="pagecontents" style="display:none;">
        <div class="overzicht">
            <h2>Checklist van deze cursus</h2>
            <ul id="cursuschecklist">
                <li class="ok">Aantal cursuslessen: 3</li>
                <li class="ok">Aantal sealpakketten: 54</li>
                <li class="notok">Huiswerkschema compleet?</li>
            </ul>
        </div>
    </div>

    <div class="buttons">
        <a id="btnBuild" class="positive" accesskey="F9">Opslaan</a>
        <a id="btnCancel" class="negative" accesskey="F2">Annuleren</a>
        <a id="btnDelete" class="secondary" style="margin-right:40px;">Verwijder</a>
        <a rel="CURSUSCODE" class="button  storedprocedure"  href="{$serverroot}/app/macs/module/plr_execstoredproc/?action=f1fcce1278962f386208e506f4025c6a&firsttime=true" >Maak kopie...</a>

        <span style="margin-left:150px;"><input type="checkbox" id="_fldINCLUSIEFINACTIEF" name="_hdnINCLUSIEFINACTIEF" value="J" /> Inclusief verborgen cursussen?</span>
    </div>
</div>
</div>
