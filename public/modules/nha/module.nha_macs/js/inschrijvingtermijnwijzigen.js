Macs.InschrijvingTermijnWijzigen = {
	zelfStudies: null,
	studieTempos: null,
	inschrijvingBetaalTermijnen: null,
	studieTempoBetaalTermijnen: null,
	termijnWijzigenVelden: ['ZELFSTUDIE', 'STUDIETEMPO', 'BETAALTERMIJNEN', 'CALLOPMERKING'],
	bepaalTermijnWijzigInfo: function(zelfstudie) {
		zelfstudie = zelfstudie || Macs.InschrijvingSelectieForm.inschrijvingData.ZELFSTUDIEJANEE;

		$.getJSON(_servicequery, {
			'func': 'studiebetaalinfo',
			'inschrijfnr': Macs.InschrijvingSelectieForm.inschrijvingData.INSCHRIJFNR,
			'zelfstudie': zelfstudie
		},
		function(data, textStatus) {
			if (textStatus == 'success') {
				Macs.InschrijvingTermijnWijzigen.zelfStudies = data.zelfstudies;
				Macs.InschrijvingTermijnWijzigen.studieTempos = data.studietempos;
				Macs.InschrijvingTermijnWijzigen.inschrijvingBetaalTermijnen = data.inschrijvingbetaaltermijnen;
				Macs.InschrijvingTermijnWijzigen.studieTempoBetaalTermijnen = data.studietempobetaaltermijnen;

				$("#_fldZELFSTUDIE")
				.prop('checked', zelfstudie == 'J')
				.prop('disabled', Macs.InschrijvingTermijnWijzigen.zelfStudies.length <= 1)
				;

				$("#_fldSTUDIETEMPO")
				.empty()
				.addOptions(Macs.InschrijvingTermijnWijzigen.studieTempos, 'studietempocode', 'studietemponaam', Macs.InschrijvingSelectieForm.inschrijvingData.STUDIETEMPOCODE)
				//.attr('disabled', Macs.InschrijvingTermijnWijzigen.studieTempos.length <= 1)
				;

				$("#_fldBETAALTERMIJNEN")
				.empty()
				.addOptions(Macs.InschrijvingTermijnWijzigen.inschrijvingBetaalTermijnen, 'aantaltermijnen', 'aantaltermijnen', Macs.InschrijvingSelectieForm.inschrijvingData.AANTALBETAALTERMIJNEN)
				//.attr('disabled', Macs.InschrijvingTermijnWijzigen.inschrijvingBetaalTermijnen.length <= 1)
				;

				var gekoppeldEnabled = Macs.InschrijvingTermijnWijzigen.studieTempoBetaalTermijnen.length > 0;

				$("#_btnKOPPEL").prop('disabled', !gekoppeldEnabled);

				var gekoppeld = gekoppeldEnabled;

				if ($("#_fldSTUDIETEMPO option").length <= 1 || $("#_fldBETAALTERMIJNEN option").length <= 1) {
					gekoppeld = false;
					$("#_btnKOPPEL").prop('disabled', true);
				}
				$("#_btnKOPPEL").prop('checked', gekoppeld);

			} else {
				alert('De opzegredenen kon niet geladen worden. Error: ' + textStatus);
			}
		});
	},
	selecteerInschrijving: function() {
		var item = Macs.InschrijvingSelectieForm.inschrijvingData;

		/**
		 *  Toon de velden e.d. in het InschrijvingOpzeggen formulier
		 */
		Macs.SharedLib.easyFillForm(item, Macs.InschrijvingTermijnWijzigen.termijnWijzigenVelden);
		$("#_hdnRecordID").val(item.PLR__RECORDID);
		Macs.InschrijvingTermijnWijzigen.bepaalTermijnWijzigInfo();
		$("#_fldZELFSTUDIE,#_fldSTUDIETEMPO").filter(":enabled").focus();
	},
	zoekRelatie: function() {
		Macs.SharedLib.easyFillForm([], Macs.InschrijvingTermijnWijzigen.termijnWijzigenVelden);
		$("#_hdnRecordID").val('');
	},
	zoekGekoppeldeWaarde: function(field) {
		var searchfieldname = '';

		if (field.id == '_fldSTUDIETEMPO') {
			searchfieldname = 'studietempocode';
			resultfieldname = 'aantalbetaaltermijnen';
		} else {
			searchfieldname = 'aantalbetaaltermijnen';
			resultfieldname = 'studietempocode';
		}

		var searchvalue = $(field).val();

		var result = false;
		$(Macs.InschrijvingTermijnWijzigen.studieTempoBetaalTermijnen).each(function(index, elem) {
			if (elem[searchfieldname] == searchvalue) {
				result = elem[resultfieldname];
			}
		});
		return result;
	},
	bewaarWijziging: function() {
		// Ga er vanuit dat er geen problemen met het formulier zijn
		var formValid = true;

		/***
		 * Controleer de standaard velden
		 */
		var validator = $("#dataform").validate({
			onsubmit: false,
			debug: false,
			validateHiddenFields: false
		});
		if (!$("#dataform").valid()) {
			formValid = false;
    		var counter = $("#dataform").validate().numberOfInvalids();
			Polaris.Base.modalMessage($.t('validation.incorrect_field', {count: counter}), 1000, false);
		}

		/***
		 * Controleer of er een relatie is geselecteerd
		 */
		if ($('#RELATIE_hdnRecordID').val() == '') {
			formValid = false;
			log("geen relatie geselecteerd");
			Polaris.Base.modalMessage(Macs.Messages.geenRelatieGeselecteerd);
			return;
		}

		if (formValid) {
	        Polaris.Ajax.postJSON(_servicequery, $('#dataform'), $.t('nha.macs.informatieWordtOpgeslagen'), $.t('plr.changes_saved'), function(data) {
				validator.resetForm();
        		Polaris.Form.userIsEditing = false;
				Macs.InschrijvingTermijnWijzigen.annuleerForm();
	        }, function() {
				validator.resetForm();
	        });
		}
	},
	annuleerForm: function() {
		Macs.InschrijvingSelectieForm.maakInschrijvingLeeg();
		Macs.SharedLib.easyFillForm([], Macs.InschrijvingTermijnWijzigen.termijnWijzigenVelden);
		$('#_hdnRecordID').val('');

		$("#_fldZOEKINSCHRIJVING").focus();
		$('#btnSAVE').addClass('disabled');

		Polaris.Form.userIsEditing = false;
	}
};

// begin meteen met het ophalen van gui ONafhankelijke informatie
Macs.SharedLib.laadParameters();

$(document).ready(function() {
	Macs.InschrijvingSelectieForm.initialiseer({
		'funcSelecteerInschrijving': Macs.InschrijvingTermijnWijzigen.selecteerInschrijving,
		'funcZoekRelatie': Macs.InschrijvingTermijnWijzigen.zoekRelatie
	});

	/* Form actions */
	$('#dataform').submit(function(e) {
		e.preventDefault();
	});
	$('#btnCANCEL').click(function(e) {
		e.preventDefault();
		Macs.InschrijvingTermijnWijzigen.annuleerForm();
	});
	$('#btnSAVE').click(function(e) {
		e.preventDefault();
		if (!$(this).hasClass('disabled')) Macs.InschrijvingTermijnWijzigen.bewaarWijziging();
	});

	$('#_fldZELFSTUDIE,#_fldSTUDIETEMPO,#_fldBETAALTERMIJNEN').change(function(e) {
		// Geef aan Polaris door dat de gebruiker het formulier aan het invullen is, zodat
		// er een extra bevestiging wordt gevraagd zodra de gebruiker een ander formulier kiest
		Polaris.Form.userIsEditing = true;

		/* Save knop aanzetten */
		$('#btnSAVE').removeClass('disabled');

		/* record state omzetten naar Edit */
		$("#_hdnAction").val('save');
		$("#_hdnState").val('edit');
	});
	$("#_fldZELFSTUDIE").change(function(e) {
		Macs.InschrijvingTermijnWijzigen.bepaalTermijnWijzigInfo($("#_fldZELFSTUDIE").prop('checked') == true ? 'J' : 'N');
	});
	$('#_fldSTUDIETEMPO,#_fldBETAALTERMIJNEN,#_btnKOPPEL').change(function(e) {
		if ($("#_btnKOPPEL").prop('checked') == true) {
			var val = false;
			if (this.id == '_fldBETAALTERMIJNEN') {
				val = Macs.InschrijvingTermijnWijzigen.zoekGekoppeldeWaarde($("#_fldBETAALTERMIJNEN")[0]);
				if (val) $("#_fldSTUDIETEMPO").val(val);
			} else if (this.id == '_fldSTUDIETEMPO') {
				val = Macs.InschrijvingTermijnWijzigen.zoekGekoppeldeWaarde($("#_fldSTUDIETEMPO")[0]);
				if (val) $("#_fldBETAALTERMIJNEN").val(val);
			} else {
				val = Macs.InschrijvingTermijnWijzigen.zoekGekoppeldeWaarde($("#_fldBETAALTERMIJNEN")[0]);
				if (val) $("#_fldSTUDIETEMPO").val(val);

				val = Macs.InschrijvingTermijnWijzigen.zoekGekoppeldeWaarde($("#_fldSTUDIETEMPO")[0]);
				if (val) $("#_fldBETAALTERMIJNEN").val(val);
			}
		}
	});

	// Initialiseer het formulier (alsof er geannuleerd wordt)
	Macs.InschrijvingTermijnWijzigen.annuleerForm();
});
