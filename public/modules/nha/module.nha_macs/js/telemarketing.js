Macs.Telemarketing = {
	grid: null,
	geselecteerdeActie: null,
	currentIndex: null,
	huidigeMedewerker: null,
	geplandeActiesData: [],
	telemarketingInfo: [],

	relatieVelden: ['RELATIENR', 'VOORLETTERS', 'ACHTERNAAM', 'TUSSENVOEGSEL', 'GESLACHT', 'GEBDATUM', 'BEDRIJFSNAAM', 'AFDELINGNAAM', 'STRAAT', 'HUISNR', 'HUISNUMMER', 'HUISNUMMERTOEVOEGING', 'POSTCODE', 'PLAATS', 'LANDCODE', 'TELEFOONOVERDAG', 'DISPLAYLANDCODE', 'TELEFOONSAVONDS', 'MOBIELNR', 'EMAIL', 'RELATIE__RECORDID'],
	geplandeActieVelden: ['PLR__RECORDID', 'GEPLANDEDATUM', 'BEGINTIJD', 'EINDTIJD', 'ACTIEOMSCHRIJVING', 'INBEHANDELINGDOORNAAM'],

	htmlGeplandeActies: '',
	templateGeplandeActies: '',
	htmlCursusinfo: '',
	templateCursusinfo: '',
	htmlActieHistorie: '',
	templateActieHistorie: '',
	htmlOpmerkingen: '',
	templateOpmerkingen: '',

	initialiseer: function(callbacks) {
		var Self = Macs.Telemarketing;

		Self.htmlGeplandeActies = $('#geplandeacties tbody');

		//directive to render the template
		var directivesGeplandeActies = {
			'tr': {
				'geplandeactie<-': {
					'td.periode': function(arg) {
						var icon = 'exclamation-circle';
						var color = 'red';
						if (arg.item.PERIODE == 2) {
							icon = 'phone-square';
							color = 'orange';
						} else if (arg.item.PERIODE == 3) {
							icon = 'share-square';
							color = 'blue';
						}
						return '<i style="color:'+color+'" class="fa fa-lg fa-'+icon+'"></i>';
					},
					'td.geplandedatum': 'geplandeactie.GEPLANDEDATUM',
					'td.begintijd': 'geplandeactie.BEGINTIJD',
					'td.eindtijd': 'geplandeactie.EINDTIJD',
					'td.achternaam': function(arg) {
						return Macs.SharedLib.bepaalAanhef(arg.item.GESLACHT) + ' ' + (arg.item.VOORLETTERS || '') + ' ' + arg.item.ACHTERNAAM;
					},
					'td.actieomschrijving': function(arg) {
						if (arg.item.ACTIEOMSCHRIJVING.length > 80) return arg.item.ACTIEOMSCHRIJVING.substr(0, 80) + '...';
						else return arg.item.ACTIEOMSCHRIJVING;
					},
					//'td.inbehandelingdoor': 'geplandeactie.INBEHANDELINGNAAM',
					'td.naammedewerker': function(arg) {
					    var value = arg.item.MEDEWERKERNAAM;
					    if (arg.item.INBEHANDELINGDOOR == arg.item.GEPLANDEMEDEWERKER)
					        value += ' (+)';
					    return value;
				    },

                    'input.actienr@value': 'geplandeactie.ACTIENR',
					'@style': "'cursor:pointer'",
					'@class': function(arg) {
						//arg => {data:data, items:items, pos:pos, item:items[pos]};
						var oddEven = (arg.pos % 2 == 0) ? 'even' : 'odd';
						var firstLast = (arg.pos == 0) ? 'first' : (arg.pos == arg.items.length - 1) ? 'last' : '';
						return ' ' + oddEven + ' ' + firstLast;
					}
				}
			}
		};
		Self.templateGeplandeActies = Self.htmlGeplandeActies.compile(directivesGeplandeActies);

		Self.htmlCursusinfo = $('#cursusinformatie tbody');
		var directivesCursusinfo = {
			'tr': {
				'cursusinfo<-aanvraagcursusinfo': {
					'td.cursusstudiegidscode': 'cursusinfo.cursusstudiegidscode',
					'td.cursusnaam': 'cursusinfo.cursusnaam'
				}
			}
		};
		Self.templateCursusinfo = Self.htmlCursusinfo.compile(directivesCursusinfo);

		Self.htmlActieHistorie = $('#actiehistorietabel tbody');
		var directivesActieHistorie = {
			'tr': {
				'actie<-actiehistorie': {
					'td.gerealiseerdedatum': 'actie.gerealiseerdedatum',
					'td.actieomschrijving': 'actie.actieomschrijving'
				}
			}
		};
		Self.templateActieHistorie = Self.htmlActieHistorie.compile(directivesActieHistorie);

		Self.htmlOpmerkingen = $('#relatieopmerkingen tbody');
		var directivesOpmerkingen = {
			'tr': {
				'opmerking<-opmerkingen': {
					'td.datum': 'opmerking.datum',
					'td.opmerking': 'opmerking.opmerking'
				}
			}
		};
		Self.templateOpmerkingen = Self.htmlOpmerkingen.compile(directivesOpmerkingen);
	},
	bepaalFocus: function(callback) {
		var Self = Macs.Telemarketing;

		$.getJSON(_servicequery, {
			'func': 'autoselectie'
		},
		function(data, textStatus) {
			if (textStatus == 'success') {
                var $table = $("#geplandeacties");
                $table.data('input').focus();
                log('after bepaalfocus');
                if (data.automatische_selectie) {
            		var tmpindex = $("#geplandeacties tbody tr:has(input.actienr[value="+data.automatische_selectie+"])").index();
            		Macs.Telemarketing.grid.setFocus(Macs.Telemarketing.grid.cellFromCoords(1, tmpindex), true);
                    if (callback) callback();
                }
			}
		});
	},
	vulGeplandeActies: function(data) {
		var Self = Macs.Telemarketing;

		/* rendering but reusing the compiled function template */
		log('before render');
		Self.htmlGeplandeActies = Self.htmlGeplandeActies.render(data, Self.templateGeplandeActies);
		log('after render');

		/* Vul de tabel op basis van de template */
        var focusregel = 0;
		log('before maakTabelSelecteerbaar');
		Self.maakTabelSelecteerbaar("#geplandeacties", focusregel);
		log('before bepaalfocus');
		Self.bepaalFocus();
	},
	vulActieHistorie: function(data) {
		var Self = Macs.Telemarketing;

		if (data.actiehistorie.length > 0) {
			$("#_lblActieHistorieCaption span").text('');
			$('#actiehistorietabel').show();
			Self.htmlActieHistorie = Self.htmlActieHistorie.render(data, Self.templateActieHistorie);
		} else {
			$("#_lblActieHistorieCaption span").text("- "+$.t('nha.macs.geen_historie'));
			$('#actiehistorietabel').hide();
		}
	},
	vulAanvraagCursusinfo: function(data) {
		var Self = Macs.Telemarketing;

		if (data.aanvraagcursusinfo.length > 0) {
			$("#_lblAANVRAAGDATUM").text(data.aanvraagcursusinfo[0].datumverzoek);
			Self.htmlCursusinfo = Self.htmlCursusinfo.render(data, Self.templateCursusinfo);
		} else {
			$("#_lblAANVRAAGDATUM").text('geen aanvragen');
			$("#cursusinformatie tbody").empty();
		}
	},
	vulOpmerkingen: function(data, table) {
		var Self = Macs.Telemarketing;
		table = table || $("#relatieopmerkingen");

		if (data.aanvraagcursusinfo.length > 0) {
			Self.htmlOpmerkingen = Self.htmlOpmerkingen.render(data, Self.templateOpmerkingen);
		} else {
			$("tbody", table).empty();
		}
	},
	maakTabelSelecteerbaar: function(tableid, focusregel) {
		var $table = $(tableid);

		Macs.Telemarketing.grid = new KeyTable({
			'table': $table[0],
			'selectRow': true,
			'keepSelection': true,
			'continueScroll': false,
			'initScroll': true,
			//            'focus': [1,0],
			'form': true
		});
		$table.data('input').focus();

		Macs.Telemarketing.grid.setFocus(Macs.Telemarketing.grid.cellFromCoords(1, focusregel), true);

		/* Selecteer de rij wanneer gebruiker op Enter drukt (action) */
		$('tbody tr', $table).each(function() {
			Macs.Telemarketing.grid.event.action(this, function(nCell) {
				Macs.Telemarketing.selecteerGeplandeActie();
			});
			Macs.Telemarketing.grid.event.open(this, function(nCell) {
				Macs.Telemarketing.selecteerGeplandeActie();
			});
		});
	},
	selecteerGeplandeActie: function(elem) {
		var tmpindex = $("#geplandeacties tbody tr.focus").index();

		if (tmpindex == -1) return;

		Macs.Telemarketing.currentIndex = tmpindex;
		var item = Macs.Telemarketing.geplandeActiesData[Macs.Telemarketing.currentIndex];
		Macs.Telemarketing.geselecteerdeActie = item;
		log('voor telemarketinginfo');
		$.getJSON(_servicequery, {
			'func': 'telemarketinginfo',
			'actienr': item.ACTIENR,
			'relatienr': item.RELATIENR,
			'oorzaaknr': item.OORZAAKNR
		},
		function(data, textStatus) {
			if (textStatus == 'success') {
			    Macs.Telemarketing.telemarketingInfo = data;
				Macs.Telemarketing.vulActieHistorie(data);
				Macs.Telemarketing.vulAanvraagCursusinfo(data);
				Macs.Telemarketing.vulOpmerkingen(data);
                var inbehandeling = data.inbehandeling[0];
				if (inbehandeling.inbehandeling == 'JA'
				&& Macs.Telemarketing.huidigeMedewerker != Macs.Telemarketing.telemarketingInfo.inbehandeling[0].medewerkernr
				) {
        			Polaris.Base.modalDialog('Deze actie wordt momenteel behandeld door '+inbehandeling.inbehandelingdoor // + ".<br />Wilt u de actie DEBLOKKEREN?"
        			, {
        			    'close': Macs.Telemarketing.annuleerForm
//        			    , 'yes': function() { Macs.Telemarketing.annuleerActieOnderbehandeling(item.ACTIENR) }
        			  });
				}
			}
		});

		/**
		 *  Toon de geplande actie in het geplande actie formulier
		 */
		Macs.SharedLib.easyFillForm(item, Macs.Telemarketing.relatieVelden, 'RELATIE!');
		Macs.SharedLib.easyFillForm(item, Macs.Telemarketing.geplandeActieVelden, '');

		/* Velden die wat meer aandacht vergen */
		//        $('#_hdnRecordID').val(item.PLR__RECORDID); // Geplande actie recordid
		$('#_fldACTIENR').val(item.ACTIENR);
		$('#RELATIE_hdnRecordID').val(item.RELATIE__RECORDID);
		$('#RELATIE_fldRELATIENR').val(item.RELATIENR);

		/* Save knop aanzetten */
		$('#btnSAVE').removeClass('disabled');
		/* record state omzetten naar Edit */
		$('input[name=_hdnAction]').val('save');
		$('input[name=_hdnState]').val('edit');
		/* Zet geslacht om naar nette Aanhef */
		document.getElementById('RELATIE!_lblGESLACHT').innerHTML = Macs.SharedLib.bepaalAanhef(item.GESLACHT);

		$("#dataform input.date_input").date_input();

        var scope = Macs.RelatieSelectieForm.bepaalFormScope();
        Macs.RelatieSelectieForm.switchModus('view', scope);

		Macs.Telemarketing.toggleFormMode('formview');

		// initialiseer 'geen interesse' resultaat
		$("#_fldOORZAAKGEENINTERESSE option:first").attr('selected', true);
		$("#_fldANDERRESULTAAT,#_fldTOELICHTINGGEENINTERESSE,#_fldRELATIEOPMERKING").val('');
		$("#btnGEENINTERESSE").click(); // roep de onclick aan omdat deze meteen de juiste 'this' meegeeft aan de functie
		$("#tab_relatie").click();
	},
	bewaarGeplandeActie: function(callback) {
		// Ga er vanuit dat er geen problemen met het formulier zijn
		var formValid = true;

		/***
		 * Controleer de standaard velden
		 */
		if (!$("#dataform").valid()) {
			formValid = false;
			Polaris.Base.modalMessage('U heeft ' + $("#dataform").validate().numberOfInvalids() + ' velden niet correct ingevuld.', 1000, false);

            var scope = Macs.RelatieSelectieForm.bepaalFormScope();
            Macs.RelatieSelectieForm.switchModus('edit', scope);

			// zet focus op eerste foutieve veld
			//            $("#dataform input:blank").focus();
		}

		/***
		 * Controleer of er een relatie is geselecteerd
		 */
		if ($('#RELATIE_hdnRecordID').val() == '') {
			formValid = false;
			log("geen relatie geselecteerd");
			Polaris.Base.modalMessage(Macs.Messages.geenRelatieGeselecteerd);
			return;
		}

//$('#dataform')[0].submit();
//return;
		if (formValid) {
			log('before bewaar');
			jQuery.postJSON(_servicequery, $('#dataform'), function(data, textStatus) {
				log('after bewaar');
				if (textStatus == 'success') {
					if (data.result != false) {
						Macs.SharedLib.ajaxFeedback('Record opgeslagen');
						if (Macs.Telemarketing.currentIndex != null) {
							Macs.Telemarketing.toonGeplandeActies(Macs.Telemarketing.annuleerForm);
//							Macs.Telemarketing.annuleerForm();
						}
					} else {
						Polaris.Base.errorDialog(data.error, data.detailed_error);
					}
    				if (typeof callback == 'function') callback();
				} else {
					Macs.SharedLib.ajaxFeedback('Fout opgetreden bij JSON post.');
				}
			});
		} else {
			console.log("TM Form not valid");
		}
	},
	annuleerActieOnderbehandeling: function(actienr) {
		$.getJSON(_servicequery,
		{
		    'func':'annuleeronderbehandeling',
		    'actienr': actienr
		},
		function(data, textStatus) {
			if (textStatus == 'success') {
                log("Actie is niet meer onder behandeling "+Macs.Telemarketing.geselecteerdeActie.ACTIENR);
			} else {
			    log('Actie '+actienr+' is nog steeds onderbehandeling.');
			}
		});
	},
	annuleerForm: function() {
		/* Maak de velden leeg */
		Macs.SharedLib.easyFillForm([], Macs.Telemarketing.geplandeActieVelden, 'RELATIE!');
		Macs.SharedLib.easyFillForm([], Macs.Telemarketing.geplandeActieVelden, '');

        var scope = Macs.RelatieSelectieForm.bepaalFormScope();
        Macs.RelatieSelectieForm.switchModus('view', scope);
	    if ((Macs.Telemarketing.telemarketingInfo.inbehandeling[0].inbehandeling == 'JA'
	    && Macs.Telemarketing.huidigeMedewerker == Macs.Telemarketing.telemarketingInfo.inbehandeling[0].medewerkernr)
	    || Macs.Telemarketing.telemarketingInfo.inbehandeling[0].medewerkernr == '') {
	    	log('voor annuleerActieOnderbehandeling');
            Macs.Telemarketing.annuleerActieOnderbehandeling(Macs.Telemarketing.geselecteerdeActie.ACTIENR);
        }

        $("#_fldMAILINGSTUREN").prop('checked', true);

		Macs.Telemarketing.toggleFormMode('listview');
		setTimeout(function() {
			$("#geplandeacties").data('input').focus();
			Macs.Telemarketing.grid.setFocus(Macs.Telemarketing.grid.cellFromCoords(1, Macs.Telemarketing.currentIndex), true);
		},
		5);
	},
	toggleFormMode: function(mode) {
		if (mode == 'listview') {
			$(".tm_listview").show();
			$(".tm_formview").hide();
		} else {
			$(".tm_listview").hide();
			$(".tm_formview").show();

			// Haal Auto refresh weg wanneer gebruiker aan het invullen is
			if (typeof tmTimer != 'undefined') clearTimeout(tmTimer);
		}
	},
	toonGeplandeActies: function(callback) {
    	$("#btnREFRESH i").addClass("fa-spin");
    	log('Before toonGeplandeActies');
		$.getJSON(_servicequery + '?limit=5000', function(data, textStatus) {
			/* Leg data vast als lokale/globale var */
			Macs.Telemarketing.geplandeActiesData = data;
			log('After toonGeplandeActies');
			if (textStatus == 'success') {
				Macs.Telemarketing.vulGeplandeActies(data);
				// Auto refresh de geplande acties
				/* 5 minuten refresh */
//				tmTimer = setTimeout(Macs.Telemarketing.toonGeplandeActies, 5 * 60 * 1000);
				log('After vulGeplandeActies');
				if (typeof callback == 'function') callback();
			} else {
				Macs.SharedLib.modalMessage(Macs.Messages.foutZoekActie + textStatus);
			}
    		$("#btnREFRESH i").removeClass("fa-spin");
		});

		$.getJSON(_servicequery + '?func=persinschr', function(data, textStatus) {
			/* Leg data vast als lokale/globale var */
			if (textStatus == 'success') {
	            $("#persinschrvalue").text(data.aantal);
			} else {
				Macs.SharedLib.modalMessage(Macs.Messages.foutZoekActie + textStatus);
			}
		});
	},
	bepaalOorzaakGeenInteresse: function() {
		$.getJSON(_servicequery + '?func=oorzaakgeeninteresse', function(data, textStatus) {
			/* Leg data vast als lokale/globale var */
			if (textStatus == 'success') {
				$("#_fldOORZAAKGEENINTERESSE").empty();
				if (data) {
					Macs.Telemarketing.oorzaakGeenInteresse = data;
					$("#_fldOORZAAKGEENINTERESSE").addOptions(data, 'OORZAAKGEENINTERESSE_ORI', 'OORZAAKGEENINTERESSE');
				}
				$("#_fldOORZAAKGEENINTERESSE").append(new Option('', ''));
			} else {
				console.log('Geen oorzaakgeeninteresse gevonden: ' + textStatus);
			}
		});
	},
	bepaalHuidigeMedewerker: function() {
		$.getJSON(_ajaxquery, {
			'event': 'getuserid'
		},
		function(data, textStatus) {
			if (textStatus == 'success') {
				if (data) {
					Macs.Telemarketing.huidigeMedewerker = data.result;
				}
			} else {
				console.log('Geen huidige medewerker gevonden: ' + textStatus);
			}
		});
	},
	resultaat_GeenInteresse: function(e) {
		if ($(this).val() != '') {
			$("#_fldTOELICHTINGGEENINTERESSE").attr('disabled', false).removeClass('readonly');
			$("#_fldSTUDIEGIDSNOGMAALSOPSTUREN").attr('checked', false);
		}

		// leg het resultaattype vast
		$("#_fldRESULTAAT").val('GEENINTERESSE');

		// disable het geplande actie/vervolg actie form
		$("#_lblGEPLANDEACTIE").text($.t('nha.macs.geplande_actie'));
		$("#geplandeactieform input").attr('disabled', true).attr('readonly', true).addClass('readonly');

		// maak de huidige functie button actief
		$('#btnINSCHRIJVING,#btnSTUDIEGIDSOPSTUREN,#btnNABELLEN,#btnLATERNABELLEN,#btnAFSPRAAKMAKEN').removeClass('active');
		$(this).addClass('active');

		// toon het 'geen interesse' panel en zet focus op eerste veld
		$("#pnlGEENINTERESSE").show();
		$("#pnlSTUDIEGIDSOPSTUREN").hide();

		$("#_fldOORZAAKGEENINTERESSE").focus();
	},
	resultaat_StudiegidsOpsturen: function(e) {
		$("#_fldSTUDIEGIDSNOGMAALSOPSTUREN").attr('checked', true);
		$("#_fldRESULTAAT").val('STUDIEGIDSOPSTUREN');
		$("#_fldOORZAAKGEENINTERESSE,#_fldANDERRESULTAAT").val('');
		$("#_fldTOELICHTINGGEENINTERESSE").attr('disabled', true).addClass('readonly');

		// maak de huidige functie button actief
		$('#btnINSCHRIJVING,#btnGEENINTERESSE,#btnNABELLEN,#btnLATERNABELLEN,#btnAFSPRAAKMAKEN').removeClass('active');
		$(this).addClass('active');

		$("#pnlGEENINTERESSE").hide();
		$("#pnlSTUDIEGIDSOPSTUREN").show();
	},
	resultaat_Inschrijving: function(e) {
		e.preventDefault();
		$("#_fldRESULTAAT").val('INSCHRIJVING');
		var url = _serverroot + '/app/macs/const/nieuwe_inschrijving/';
		url += '?relatienr=' + Macs.Telemarketing.geselecteerdeActie.RELATIENR + '&oorzaaknr=' + Macs.Telemarketing.geselecteerdeActie.OORZAAKNR + '&actienr=' + Macs.Telemarketing.geselecteerdeActie.ACTIENR + '&maximize=true';


		var iframe = $("#inschrijvingframe");
        if (iframe.length == 0) {
    		iframe = $('<iframe id="inschrijvingframe">');
        }

		var $content = $("#content");

		iframe.appendTo($content);
		iframe.attr('src', url);
		iframe.css({border:0, position:"absolute", top:0, left:0, width: $content.width(), height: $content.height(), 'margin-left':$content.css("margin-left")});

		// maak de huidige functie button actief
		$('#btnGEENINTERESSE,#btnSTUDIEGIDSOPSTUREN,#btnNABELLEN,#btnLATERNABELLEN,#btnAFSPRAAKMAKEN').removeClass('active');
		$(this).addClass('active');

		// 'geen interesse', 'studiegids sturen' verbergen
		$("#pnlGEENINTERESSE,#pnlSTUDIEGIDSOPSTUREN").hide();
	},
	resultaat_VervolgActie: function(e) {
		e.preventDefault();

		// 'geen interesse' verbergen
		$("#pnlGEENINTERESSE,#pnlSTUDIEGIDSOPSTUREN").hide();

		// juiste button actief maken
		$('#btnINSCHRIJVING,#btnGEENINTERESSE,#btnSTUDIEGIDSOPSTUREN,#btnNABELLEN,#btnLATERNABELLEN,#btnAFSPRAAKMAKEN').removeClass('active');
		$(this).addClass('active');

		$("#_lblGEPLANDEACTIE").text($.t('nha.macs.vervolgactie'));
		$("#geplandeactieform input").attr('disabled', false).attr('readonly', false).removeClass('readonly');

		var text = '';
		switch (this.id) {
		case 'btnNABELLEN':
			// leg het resultaattype vast
			$("#_fldRESULTAAT").val('NABELLEN');

			text = $.t('nha.macs.nabellen');
			var d = new Date();
			var delta = 1;
			if (d.getDay() == 5) delta = 3;
			var datum = Macs.SharedLib.bepaalHuidigeDatum(delta);
			$("#_fldBEGINTIJD").val('');
			$("#_fldGEPLANDEDATUM").val(datum).change().focus();
			break;
		case 'btnLATERNABELLEN':
			// leg het resultaattype vast
			$("#_fldRESULTAAT").val('LATERNABELLEN');

			text = $.t('nha.macs.later_nabellen');
			var datum = Macs.SharedLib.bepaalHuidigeDatum();
			$("#_fldGEPLANDEDATUM").val(datum).change();
			var tijd = Macs.SharedLib.bepaalHuidigeTijd(30);
			$("#_fldBEGINTIJD").val(tijd).focus().select();
			break;
		case 'btnAFSPRAAKMAKEN':
			// leg het resultaattype vast
			$("#_fldRESULTAAT").val('AFSPRAAKMAKEN');

			text = $.t('nha.macs.afspraak_maken');
			var tijd = Macs.SharedLib.bepaalHuidigeTijd(30);
			$("#_fldBEGINTIJD").val('');
			$("#_fldGEPLANDEDATUM").val('').change().focus();
			break;
		}
		$("#_fldACTIEOMSCHRIJVING").val(text);
	}
};


Macs.Telemarketing.bepaalOorzaakGeenInteresse(); // Laad de betaalwijze in een variable, en begin hier meteen mee (niet in de ready() function)
Macs.Telemarketing.bepaalHuidigeMedewerker();

$(document).ready(function() {
	Macs.Telemarketing.initialiseer();

	Macs.Telemarketing.toonGeplandeActies();

	/* Form actions */
	$('#dataform').submit(function(e) {
		e.preventDefault();
	});
	$('#btnSAVE').click(function(e) {
		e.preventDefault();
		Macs.Telemarketing.bewaarGeplandeActie();
	});
	$('#btnCANCEL').click(function(e) {
		e.preventDefault();
		Macs.Telemarketing.annuleerForm();
	});
	$('#btnREFRESH').click(function(e) {
		e.preventDefault();
		Macs.Telemarketing.toonGeplandeActies();
	});
	$('#btnAUTOSELECT').click(function(e) {
		e.preventDefault();

		var focusregel = Macs.Telemarketing.bepaalFocus(Macs.Telemarketing.selecteerGeplandeActie);
	});
	$("#btnCOUNTITEMS").click(function(e) {
		e.preventDefault();
		alert("Aantal geplande acties: " + Macs.Telemarketing.geplandeActiesData.length);
	});

	$("#_fldANDERRESULTAAT").keyup(function() {
		if ($(this).val() != '') $("#_fldOORZAAKGEENINTERESSE").val('');
	});

	$("#_fldOORZAAKGEENINTERESSE").change(Macs.Telemarketing.resultaat_GeenInteresse);

	$('#btnGEENINTERESSE').click(Macs.Telemarketing.resultaat_GeenInteresse);
	$('#btnINSCHRIJVING').click(Macs.Telemarketing.resultaat_Inschrijving);
	$('#btnSTUDIEGIDSOPSTUREN').click(Macs.Telemarketing.resultaat_StudiegidsOpsturen);
	$('#btnNABELLEN,#btnLATERNABELLEN,#btnAFSPRAAKMAKEN').click(Macs.Telemarketing.resultaat_VervolgActie);

	$(window).resize(function() {
		var $grid = $(".telemarketinggrid");
		$grid.css('height', ($(window).height() - $grid.offset().top - 70) + 'px');
	});
	$(window).resize();
});
