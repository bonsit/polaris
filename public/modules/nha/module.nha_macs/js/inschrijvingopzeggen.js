Macs.InschrijvingOpzeggen = {
	opzeggingsVelden: ['REDENOPZEGGING', 'TOELICHTINGREDENOPZEGGING', 'OPZEGGINGSDATUM'],
	redenenOpzegging: null,
	opzeggingInfo: null,
	bepaalOpzeggenInfo: function() {
		$.getJSON(_servicequery, {
			'func': 'redenopzegging'
		},
		function(data, textStatus) {
			if (textStatus == 'success') {
				Macs.InschrijvingOpzeggen.redenenOpzegging = data;
				$("#_fldREDENOPZEGGING").empty().addOptions([''], '', '').addOptions(data, 'REDENOPZEGGING', 'REDENOPZEGGING');
			} else {
				alert('De redenenen kon niet geladen worden. Error: ' + textStatus);
			}
		});
	},
	selecteerInschrijving: function() {
		var item = Macs.InschrijvingSelectieForm.inschrijvingData;

		/**
		 *  Toon de velden e.d. in het InschrijvingOpzeggen formulier
		 */
		Macs.SharedLib.easyFillForm(item, Macs.InschrijvingOpzeggen.opzeggingsVelden);

		$("#_hdnRecordID").val(item.PLR__RECORDID);

		$("#_fldREDENOPZEGGING").focus();

		var editable = item && (item.OPZEGGINGSDATUM == '' || item.OPZEGGINGSDATUM == null) && item.OPZEGBAARJANEE == 'J';
		if (editable) {
			Macs.InschrijvingOpzeggen.toonOpzeggingInfo(item.INSCHRIJFNR);
		} else {
			$('#_fldREDENOPZEGGING,#_fldTOELICHTINGREDENOPZEGGING,#_fldOPZEGGINGSDATUM').attr('disabled', true);
    		/* Save knop uitzetten */
	    	$('#btnSAVE').addClass('disabled');
		}
	},
	zoekRelatie: function() {
		$('#_fldREDENOPZEGGING').val('');
		$('#_fldTOELICHTINGREDENOPZEGGING,#_fldOPZEGGINGSDATUM').val('');
		$("#_hdnRecordID").val('');
	},
	toonOpzeggingInfo: function(inschrijfnr) {
		$('#_fldREDENOPZEGGING,#_fldTOELICHTINGREDENOPZEGGING,#_fldOPZEGGINGSDATUM').attr('disabled', false);
		$.getJSON(_servicequery, {
			'func': 'opzegginginfo',
			'inschrijfnr': inschrijfnr
		},
		function(data, textStatus) {
			if (textStatus == 'success') {
				Macs.InschrijvingOpzeggen.opzeggingInfo = data;
				$("#opzegginginfo").autoRender(data);
			} else {
				alert('De opzegredenen kon niet geladen worden. Error: ' + textStatus);
			}
		});
	},
	bewaarOpzegging: function() {
		// Ga er vanuit dat er geen problemen met het formulier zijn
		var formValid = true;

		/***
		 * Controleer de standaard velden
		 */
		var validator = $("#dataform").validate({
			onsubmit: false,
			debug: false,
			validateHiddenFields: false
		});
		if (!$("#dataform").valid()) {
			formValid = false;
			Polaris.Base.modalMessage('U heeft ' + validator.numberOfInvalids() + ' velden niet correct ingevuld.', 1000, false);
		}

		/***
		 * Controleer of er een relatie is geselecteerd
		 */
		if ($('#RELATIE_hdnRecordID').val() == '') {
			formValid = false;
			log("geen relatie geselecteerd");
			Polaris.Base.modalMessage(Macs.Messages.geenRelatieGeselecteerd);
			return;
		}

		if (formValid) {
			//$('#dataform')[0].submit();
			//return;
			jQuery.postJSON(_servicequery, $('#dataform'), function(data, textStatus) {
				if (textStatus == 'success') {
					validator.resetForm();
					if (data.result != false) {
						Macs.SharedLib.ajaxFeedback('Record opgeslagen');
						Macs.InschrijvingOpzeggen.annuleerForm();
					} else {
						Polaris.Base.errorDialog(data.error, data.detailed_error);
					}
				} else {
					Macs.SharedLib.ajaxFeedback('Fout opgetreden bij JSON post.');
				}
			});
		}
	},
	annuleerForm: function() {
		Polaris.Form.userIsEditing = false;

		Macs.InschrijvingSelectieForm.maakInschrijvingLeeg();
		Macs.SharedLib.easyFillForm([], Macs.InschrijvingOpzeggen.opzeggingsVelden);
		$('#_hdnRecordID').val('');

		$("#_fldZOEKINSCHRIJVING").focus();
	}
};

// begin meteen met het ophalen van gui ONafhankelijke informatie
Macs.SharedLib.laadParameters();

$(document).ready(function() {
	Macs.InschrijvingSelectieForm.initialiseer({
		'funcSelecteerInschrijving': Macs.InschrijvingOpzeggen.selecteerInschrijving,
		'funcZoekRelatie': Macs.InschrijvingOpzeggen.zoekRelatie
	});

	/* Form actions */
	$('#dataform').submit(function(e) {
		e.preventDefault();
	});
	$('#btnCANCEL').click(function(e) {
		e.preventDefault();
		Macs.InschrijvingOpzeggen.annuleerForm();
	});
	$('#btnSAVE').click(function(e) {
		e.preventDefault();
		if (!$(this).hasClass('disabled')) Macs.InschrijvingOpzeggen.bewaarOpzegging();
	});

	$("#_fldOPZEGGINGSDATUM").date_input();
	Macs.InschrijvingOpzeggen.bepaalOpzeggenInfo();

	$('#_fldREDENOPZEGGING,#_fldTOELICHTINGREDENOPZEGGING,#_fldOPZEGGINGSDATUM,#_fldOPZEGGINGSDATUM').change(function(e) {
		// Geef aan Polaris door dat de gebruiker het formulier aan het invullen is, zodat
		// er een extra bevestiging wordt gevraagd zodra de gebruiker een ander formulier kiest
		Polaris.Form.userIsEditing = true;

		/* Save knop aanzetten */
		$('#btnSAVE').removeClass('disabled');

		/* record state omzetten naar Edit */
		$("#_hdnAction").val('save');
		$("#_hdnState").val('edit');
	});

	$('#_fldREDENOPZEGGING').change(function(e) {
		$("#_fldOPZEGGINGSDATUM").val(Macs.SharedLib.bepaalHuidigeDatum());
	});

	// Initialiseer het formulier (alsof er geannuleerd wordt)
	Macs.InschrijvingOpzeggen.annuleerForm();
});
