Macs.ModuleGegevens = {
	modulelesgegevensData: [],
	modulecodesData: [],

	htmlML: '',
	templateML: '',

/*
	STTGrid: null,
	BTTGrid: null,
*/
	zoekwaarden: ['RELATIE', 'FACTUURRELATIE'],

    studietempos: [
        {"CODE":"A","NAAM":"alles"}
       ,{"CODE":"H","NAAM":"hoog"}
       ,{"CODE":"L","NAAM":"laag"}
       ,{"CODE":"N","NAAM":"normaal"}
    ],
	initialiseer: function() {
		var Self = Macs.ModuleGegevens;

		Self.htmlML = $('#modulelestabel tbody');
		//directive to render the template
		var MLdirectives = {
			'tr': {
				'ml<-': {
					'td.lesnr': 'ml.LESNR',
					'td.omschrijving': 'ml.OMSCHRIJVING',
					'@onclick': function(arg) {
						return 'Macs.ModuleGegevens._wijzigRegel(' + arg.pos + ');';
					},
					'@style': function(arg) {
					    var style = 'cursor:pointer';
					    if (arg.item.STATE == 'DELETE')
					        style += ' display:none';
					    return style;
					},
					'@class': function(arg) {
						//arg => {data:data, items:items, pos:pos, item:items[pos]};
						var oddEven = (arg.pos % 2 == 0) ? 'even ' : 'odd ';
						var firstLast = (arg.pos == 0) ? 'first ' : (arg.pos == arg.items.length - 1) ? 'last ' : '';
						return ' ' + oddEven + firstLast;
					}
				}
			}
		};
        if (Self.htmlML.length > 0) Self.templateML = Self.htmlML.compile(MLdirectives);
	},
	huidigeRij: function(type) {
		var Self = Macs.ModuleGegevens;

	    type = type || 'focus';
	    var tn = Self.huidigeItemsTabelNaam();
		return $('#'+tn+' tbody tr.'+type);
	},
	selecteerRij: function(tabelnaam) {
		var Self = Macs.ModuleGegevens;

		Self.resetRijen(Self.huidigeRij());
	},
	wijzigModuleLes: function(row) {
		var Self = Macs.ModuleGegevens;

	    row = row || Self.huidigeRij();
        var item = Self.getGeselecteerdeItem();
        var _state = $.data(item, 'state');

	    if (item && (_state == 'view' || _state == null)) {
            $.data(item, 'state', 'edit');

            Self.resetRijen(row);

            var doEnterKey = function() {
                var code = e.which; // recommended to use e.which, it's normalized across browsers
                if (code==13) {
                    Self.bewaarRij();
                }
            }

            var inputOmschrijving = $('<input tabindex="103" class="omschrijving" style="width:380px" maxlength="100" type="text" value="'+item.OMSCHRIJVING+'">');
            inputOmschrijving.change(Self.bewaarRij).keydown(doEnterKey);
            $(".omschrijving", row).html(inputOmschrijving);
            inputOmschrijving.focus();

            if (item.STATE == 'INSERT') {
                var inputLesnr = $('<input tabindex="102" class="lesnr" style="width:25px" type="text" value="'+item.LESNR+'">');
                inputLesnr.change(Self.bewaarRij).keydown(doEnterKey);
                $(".lesnr", row).html(inputLesnr);
                inputLesnr.focus();
            }

        }
	},
	formIsDirty: function(dirty) {
	    Polaris.Form.userIsEditing = dirty;
        $("#btnBuild").toggleClass("disabled", !dirty);
	},
	resetRijen: function(huidigeRij) {
		var Self = Macs.ModuleGegevens;

        var tn = Self.huidigeItemsTabelNaam();
	    $('#'+tn+' tbody tr').each(function() {
	        if (typeof(huidigeRij) == 'undefined' || huidigeRij[0] != this)
	            Self.resetRij($(this));
	    });
	},
	resetRij: function(row) {
		var Self = Macs.ModuleGegevens;

		var rowObject = Self.getRijObject(row);
	    $.data(rowObject, 'state', 'view');

        $(".lesnr", row).html(rowObject.LESNR);
        $(".omschrijving", row).html(rowObject.OMSCHRIJVING);
	},
	getRijObject: function(row) {
		var Self = Macs.ModuleGegevens;

        var tn = Self.huidigeItemsTabelNaam();
        var ds = Self.huidigeItemsDataSet();
		var pos = $('#'+tn+' tbody tr').index($(row)[0]);
		return ds[pos] || null;
	},
	getGeselecteerdeItem: function() {
		var Self = Macs.ModuleGegevens;

		/* Geeft de geselecteerde item terug */
		var result = false;

		var row = Macs.ModuleGegevens.huidigeRij();
        if (row.length > 0) {
            var tn = Self.huidigeItemsTabelNaam();
            var ds = Self.huidigeItemsDataSet();
    		var pos = $('#'+tn+' tbody tr').index(row[0]);
            result = ds[pos];
        }
		return result;
	},
	maakTabelSelecteerbaar: function(tableid, focusregel) {
//		var $table = $(tableid);
        Macs.ModuleGegevens._selecteerRegel(tableid, focusregel);
	},
	_selecteerRegel: function(tableid, pos) {
		/* Highlite de nieuwe selectie */
		$($('#'+tableid+' tbody tr').removeClass('focus').get(pos)).toggleClass('focus', true);
		Macs.ModuleGegevens.selecteerRij(tableid);
	},
	_wijzigRegel: function(pos) {
		var Self = Macs.ModuleGegevens;

	    var tabelnaam = Self.huidigeItemsTabelNaam();
        Macs.ModuleGegevens._selecteerRegel(tabelnaam, pos);
      	Macs.ModuleGegevens.wijzigModuleLes();
	},
	vulModuleCodes: function(defaultmodule) {
		var Self = Macs.ModuleGegevens;
        defaultmodule = defaultmodule || $("#gepindecursus").val();

        var q = $("#searchquery").val();
		$.getJSON(_servicequery, {
            'func': 'modulecodes',
            'q': q
        }, function(data, textStatus) {
			Self.modulecodesData = data;

			if (textStatus == 'success') {
        	    $("#modulecodes").empty().addOptions(Macs.ModuleGegevens.modulecodesData, "MODULECODE", "MODULECODE,NAAM", defaultmodule);
			} else {
				Polaris.Base.modalMessage($.t('nha.macs.foutZoekActie') + textStatus);
			}

			$("#modulecodes").change(); /* trigger the change event */
		});
	},
    haalMLGegevens: function(modulecode) {
        Macs.ModuleGegevens.getMLGegevens(modulecode, function() {
            Macs.ModuleGegevens.vulModuleLessen(modulecode);
        });
    },
	getMLGegevens: function(modulecode, callback) {
		var Self = Macs.ModuleGegevens;

        Self.modulelesgegevensData = [];
        Self.htmlML = Self.htmlML.render(Self.modulelesgegevensData, Self.templateML);

		$.getJSON(_servicequery, {
            'func': 'modulelessen',
            'modulecode': modulecode
        }, function(data, textStatus) {
			/* Leg data vast als lokale/globale var */
			if (textStatus == 'success') {
    			Macs.SharedLib.ajaxFeedback('Gegevens geladen');
    			if (data.lessen && data.lessen.length == 0) {
    			} else {
            		Self.modulelesgegevensData = data.lessen;
    			    callback();
    			}
			} else {
				Polaris.Base.modalMessage($.t('nha.macs.foutZoekActie') + textStatus);
			}
		});
	},
	vulModuleLessen: function(modulecode) {
		var Self = Macs.ModuleGegevens;

		modulecode = modulecode || $("#modulecodes").val();
    	$('#modulelestabel tbody').empty();
        Self.refreshTabel('modulelestabel');
        $("#MG").text('Modulelessen ('+Self.modulelesgegevensData.length+')');
	},
	refreshTabel: function(tabelnaam, selected) {
		var Self = Macs.ModuleGegevens;

        selected = selected || 0;
        /* Vul de tabel op basis van de template */
        Self.htmlML = Self.htmlML.render(Self.modulelesgegevensData, Self.templateML);
        Self.maakTabelSelecteerbaar(Self.huidigeItemsTabelNaam(), selected);
	},
    bewaarRij: function() {
        var Self = Macs.ModuleGegevens;
	    var $row = $(this).parents('tr');
	    var item = Self.getRijObject($row);

        if (item.STATE != 'INSERT')
            item.STATE = 'UPDATE';
        $lesnr = $("input.lesnr", $row);
        if ($lesnr.length > 0) {
            item.LESNR = $lesnr.val();
        }
        item.OMSCHRIJVING = $("input.omschrijving", $row).val();

        Self.resetRijen();

        Self.formIsDirty(true);
	},
	huidigePageTab: function() {
	    return $("#pagetabs a.selected").attr('id');
	},
	huidigeItemsDataSet: function() {
		var Self = Macs.ModuleGegevens;

	    return Self.modulelesgegevensData;
	},
	huidigeItemsTabelNaam: function() {
	    return 'modulelestabel';
	},
	voegRijToe: function(e) {
		var Self = Macs.ModuleGegevens;

	    e.preventDefault();

	    var ds = Self.modulelesgegevensData;
	    var tabelnaam = Self.huidigeItemsTabelNaam();

	    var vorigerij = [];
	    if (ds && ds.length > 0) {
	        vorigerij = ds[ds.length-1];
	    }
        var lesnr = parseInt(vorigerij.LESNR, 10) + 1;
        ds.push({
            'STATE':'INSERT',
            'MODULECODE':$("#modulecodes").val(),
            'LESNR':lesnr||'',
            'OMSCHRIJVING':vorigerij.OMSCHRIJVING||''
        });

        Self.refreshTabel(tabelnaam);
        Self._wijzigRegel(ds.length-1);
        Self.formIsDirty(true);
	},
	verwijderRij: function(e) {
		var Self = Macs.ModuleGegevens;
	    e.preventDefault();

        var item = Self.getGeselecteerdeItem();
        if (item) {
            item.STATE = 'DELETE';
            var tn = Self.huidigeItemsTabelNaam();
    		$('#'+tn+' tbody tr.focus').css('display','none');
            Macs.ModuleGegevens._selecteerRegel(tn, 0);
            Self.formIsDirty(true);
        }
	},
	laadItemListOpnieuw: function(e) {
		var Self = Macs.ModuleGegevens;
	    e.preventDefault();

  	    var tabelnaam = Self.huidigeItemsTabelNaam();
	    if (confirm($.t('nha.macs.bevestigingRefresh'))) {
	        Macs.ModuleGegevens.haalMLGegevens($("#modulecodes").val());
    	}
	},
	gegevensInOrde: function() {
	    return true;
	},
	convertToPostData: function(objects) {
	    data = [];
	    $(objects).each(function(obj) {
	        for(prop in this) {
	            data.push({name: prop+'[]', value: this[prop]});
	        }
	    });
	    return data;
	},
	showNewForm: function() {
		var Self = Macs.ModuleGegevens;
		var $nieuwform = $("#nieuwemodule");

        $nieuwform
        .jqm({
            modal: true,
            onHide: Macs.ModuleGegevens.hideNewForm
        })
        .jqmShow();

		$("#_fldMODULECODE").focus();
	},
	hideNewForm: function(hash) {
		hash.w.hide();
		hash.o.remove();
	},
	bewaarNieuweModule: function() {
        Polaris.Base.modalMessage("Module wordt aangemaakt.", 0);

        var senddata = [];
        senddata.push({name: 'MODULECODE', value: $(":input[name=NEW_MODULECODE]").val()});
        senddata.push({name: 'MODULENAAM', value: $(":input[name=NEW_MODULENAAM]").val()});
        senddata.push({name: 'AANTALLESSEN', value: $(":input[name=NEW_AANTALLESSEN]").val()});
        senddata.push({name: '_hdnFORMTYPE', value: 'modulegegevens'});
        senddata.push({name: '_hdnProcessedByModule', value: 'true'});
        jQuery.postJSON(_servicequery, senddata, function(data, textStatus) {
            Polaris.Base.closeModalMessage();
            if (textStatus == 'success') {
                log('Modulegegevens aangemaakt');
                if (data.error == false) {
                    Macs.SharedLib.ajaxFeedback('Nieuwe module aangemaakt');
                    Macs.ModuleGegevens.vulModuleCodes($(":input[name=NEW_MODULECODE]").val().toUpperCase());
                    Macs.ModuleGegevens.formIsDirty(false);
                } else {
                    Polaris.Base.errorDialog(data.error, data.detailed_error);
                }
            } else {
                Macs.SharedLib.ajaxFeedback('Fout opgetreden bij JSON post.');
            }
        });
	},
	verwijderModule: function(e) {
	    e.preventDefault();

	    var modulecode = $("#_hdnMODULECODE").val();

	    if (modulecode != '' && confirm("Weet u zeker dat u module " + $("#_hdnMODULECODE").val() + " en bijbehorende modulelessen wilt verwijderen?")) {

            var senddata = [];
            senddata.push({name: 'MODULECODE', value: modulecode});
            senddata.push({name: '_hdnFORMTYPE', value: 'verwijdermodule'});
            senddata.push({name: '_hdnProcessedByModule', value: 'true'});
            jQuery.postJSON(_servicequery, senddata, function(data, textStatus) {
                if (textStatus == 'success') {
                    log('Module verwijderd');
                    if (data.error == false) {
                        Macs.SharedLib.ajaxFeedback('Module is verwijderd');
                        Macs.ModuleGegevens.vulModuleCodes();
                        Macs.ModuleGegevens.formIsDirty(false);
                    } else {
                        Polaris.Base.errorDialog(data.error, data.detailed_error);
                    }
                } else {
                    Macs.SharedLib.ajaxFeedback('Fout opgetreden bij JSON post.');
                }
            });
	    }
	},
	maakGegevens: function(e) {
		var Self = Macs.ModuleGegevens;

	    e.preventDefault();
	    if (Self.gegevensInOrde()) {
			Polaris.Base.modalMessage("Modulegegevens worden opgeslagen.", 0);

            var senddata = Self.convertToPostData(Self.modulelesgegevensData);
            senddata.push({name: 'MODULECODE', value: $("#_hdnMODULECODE").val()});
            senddata.push({name: 'MODULENAAM', value: $(":input[name=MODULENAAM]").val()});
            senddata.push({name: '_hdnFORMTYPE', value: 'modulelessen'});
            senddata.push({name: '_hdnProcessedByModule', value: 'true'});
			jQuery.postJSON(_servicequery, senddata, function(data, textStatus) {
				Polaris.Base.closeModalMessage();
				if (textStatus == 'success') {
					log('Modulegegevens aangemaakt');
					if (data.result == true) {
						Macs.SharedLib.ajaxFeedback('Modulegegevens aangemaakt');
                        Macs.ModuleGegevens.haalMLGegevens($("#modulecodes").val());
                        Macs.ModuleGegevens.formIsDirty(false);
					} else {
						Polaris.Base.errorDialog(data.error, data.detailed_error);
					}
				} else {
					Macs.SharedLib.ajaxFeedback('Fout opgetreden bij JSON post.');
				}
			});
	    }
	}
};

$(document).ready(function() {
    $("#action_masterinsert").click(function(e) {
        e.preventDefault();
        Macs.ModuleGegevens.showNewForm();
    });

    $("#deleteModule").click(Macs.ModuleGegevens.verwijderModule);

    /* Nieuwe module (met popup scherm) */
    $("#btnSAVEANDCLOSE").click(Macs.ModuleGegevens.bewaarNieuweModule);

    $("#dataform").submit(function(e) {
        Macs.ModuleGegevens.maakGegevens(e);
    });

    $("input[name=MODULENAAM]").change(function() {
        Macs.ModuleGegevens.formIsDirty(true);
    });

    Macs.ModuleGegevens.formIsDirty(false);

    Macs.ModuleGegevens.vulModuleCodes();

	Macs.ModuleGegevens.initialiseer();

    $("#modulecodes").change(function() {
        var modulecode = $(this).val();
        $("#_hdnMODULECODE").val(modulecode);
        var module = Macs.SharedLib.filter(Macs.ModuleGegevens.modulecodesData,
            function(item) {
                if (item['MODULECODE'] == modulecode) {
                    return true;
                }
                return false;
            }
        );
        if (module.length == 0) {
            return null;
        } else {
            $(":input[name=MODULENAAM]").val(module[0].NAAM);
        }

        Macs.ModuleGegevens.haalMLGegevens(modulecode);
    });

	$("#pagetabs").bind('switched', function(event, pageid, alltabs, container, settings) {
	    // niks
    });

    $(".btnVoegtoeItem").click(Macs.ModuleGegevens.voegRijToe);
    $(".btnVerwijderItem").click(Macs.ModuleGegevens.verwijderRij);
    $(".btnRefreshItemList").click(Macs.ModuleGegevens.laadItemListOpnieuw);
    $("#btnBuild").click(Macs.ModuleGegevens.maakGegevens);
});
