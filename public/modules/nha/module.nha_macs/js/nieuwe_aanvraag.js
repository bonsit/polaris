Macs.NieuweAanvraag = {
	cursusRijTemplate: '',
	prefix: 'RELATIE!',
	jqprefix: 'RELATIE\\!',
	onSelecteerRelatie: function(item, prefix) {
		if ($('input.CURSUSSTUDIEGIDSCODE[value!=""]').length == 0) {
            if (item.TELEFOONOVERDAG == null && item.TELEFOONSAVONDS == null && item.MOBIELNR == null) {
                Macs.RelatieSelectieForm.switchModus('edit');
                $('#' + prefix + '\\!_fldTELEFOONOVERDAG').focus();
            } else {
                Macs.RelatieSelectieForm.switchModus('view');
                $("#_fldADVERTENTIECODE").focus();
            }
        }
	},
	bewaarRelatie: function() {
		// Ga er vanuit dat er geen problemen met het formulier zijn
		var formValid = true;

		/***
		 * Controleer de standaard velden
		 */
		$("#dataform").validate();

		if (!$("#dataform").valid()) {
			formValid = false;
    		Macs.RelatieSelectieForm.switchModus('edit');
    		var counter = $("#dataform").validate().numberOfInvalids();
			Polaris.Base.modalMessage($.t('validation.incorrect_field', {count: counter}), 1000, false);
			// zet focus op eerste foutieve veld
			//            $("#dataform input:blank").focus();
		}

		/***
		 * Controleer op dubbele cursussen bij de inschrijvingen
		 */
		var cursuscodes = $.map($("input.CURSUSSTUDIEGIDSCODE[value!=]"), function(a) {
			return a.value;
		});
		if (cursuscodes.length > 1 && Macs.SharedLib.uniqueArray(cursuscodes).length != cursuscodes.length) {
			formValid = false;
			log("dubbele cursussen");
			Polaris.Base.modalMessage(Macs.Messages.dubbeleCursussen);
			return;
		}

		/***
		 * Controleer of cursussen zijn ingevuld
		 */
		if ($("input.CURSUSSTUDIEGIDSCODE[value!=]").length == 0) {
			formValid = false;
			log("geen cursussen");
			Polaris.Base.modalMessage(Macs.Messages.geenCursussen);
			return;
		}

		/***
		 * Controleer of er een relatie is geselecteerd
		 */
		if ($('input[name=RELATIE\\!_hdnState]').val() == '') {
			formValid = false;
			log("geen relatie geselecteerd");
			Polaris.Base.modalMessage(Macs.Messages.geenRelatieGeselecteerd);
			return;
		}

		//$('input[name=RELATIE!_hdnAction]').val('save');
		//$('#dataform')[0].submit();
		//return;
		if (formValid) {
			$('input[name=_hdnAction]').val('save');
			$('#btnSAVE').addClass('disabled');

	        Polaris.Ajax.postJSON(_servicequery, $('#dataform'), $.t('nha.macs.aanvraagWordtOpgeslagen'), $.t('plr.changes_saved'),
		        function (data) {
					Macs.NieuweAanvraag.annuleerForm();
		        },
	        	function (data) {
					$('#btnSAVE').removeClass('disabled');
	        	}
	        );
		}
	},
	annuleerForm: function() {
		Polaris.Form.userIsEditing = false;

		/* Maak de velden leeg */
		Macs.RelatieSelectieForm.maakLeeg('RELATIE!');

		$('#RELATIE_hdnRecordID').val('');
		$('#RELATIE_fldRELATIENR').val('');

		$("#_fldNIEUWSBRIEF").attr('checked', false);

		if (typeof sessionStorage.ontvangstmedium != 'undefined')
    		$("#_fldONTVANGSTMEDIUM").val(sessionStorage.ontvangstmedium);

		$("#_fldZOEKLAND").val(Macs.RelatieSelectieForm.huidigLandSelectie());

		/* Verwijder alle aanvraagregels behalve de eerste twee (0 en1) */
		$("#nieuwe_aanvraag_cursusselectie .componentitem:gt(1)").remove();

		$("input.as_showfield,input.extsearch").val('');
		Macs.RelatieSelectieForm.switchModus('view');
		$("input.as_showfield:first").focus();

		Macs.RelatieSelectieForm.resetLandSelectie();
	},
	cursusRijToevoegen: function(event) {
		if ($("#nieuwe_aanvraag_cursusselectie .componentitem").length < 15) {
			var rij = Macs.NieuweAanvraag.cursusRijTemplate.clone();
			// Rij toevoegen
			$("#nieuwe_aanvraag_cursusselectie .componentitems").append(rij);
			// Landcode toevoegen aan autosuggest fields
			Macs.RelatieSelectieForm.updateAjaxAttributes();
			// Event handlers toevoegen (autosuggest en date input)
			Polaris.Form.setAutoSuggest3($("input.extsearch", rij), {
				'onItemSelect': function(elem, value, vis_elem) {
				    if (!value) return;

					var lines = $("#nieuwe_aanvraag_cursusselectie input.as_showfield");
					var index = lines.index(vis_elem);
					var nextline = lines.get(index + 1);
					if (nextline) {
						nextline.focus();
					} else {
						setTimeout(function() {
							$("#_fldZOEKPOSTCODE").focus();
						},
						200); // focus event 'direct' gaat mis; focus springt over naar huisnr
					}
				}
			});
			$("input.date_input", rij).date_input();
			// Focus zetten op cursus veld
			$("#nieuwe_aanvraag_cursusselectie input.as_showfield").focus();
		} else {
			$(event.target).attr('disabled', 'true');
		}
	}
};

$(document).ready(function() {
	Macs.RelatieSelectieForm.initLandSelectie();

	// Form actions
	$('#dataform').submit(function(e) {
		e.preventDefault();
	});
	$('#btnSAVE').click(function(e) {
		e.preventDefault();
		if (!$(this).hasClass('disabled')) Macs.NieuweAanvraag.bewaarRelatie();
	});
	$('#btnCANCEL').click(function(e) {
		e.preventDefault();
		Macs.NieuweAanvraag.annuleerForm();
	});

	$("#_fldONTVANGSTMEDIUM").change(function() {
	    sessionStorage.ontvangstmedium = $(this).val();
	});

	$("#formview").bind('selecteerrelatie', function(event, item, prefix) {
		Macs.NieuweAanvraag.onSelecteerRelatie(item, prefix);
	});

	// Nieuwe aanvraag: Selecteer cursus(sen)
	// Bewaar een niet-ingevulde rij als template rij
	Macs.NieuweAanvraag.cursusRijTemplate = $("#nieuwe_aanvraag_cursusselectie .componentitem:first").remove().clone(true);
	// Knop 'Extra' koppelen aan event handler
	$("#cursusrijtoevoegen").click(Macs.NieuweAanvraag.cursusRijToevoegen);

	// Standaard zijn er twee cursus rijen om in te vullen
	Macs.NieuweAanvraag.cursusRijToevoegen();
	Macs.NieuweAanvraag.cursusRijToevoegen();

    if (typeof sessionStorage.ontvangstmedium != 'undefined')
        $("#_fldONTVANGSTMEDIUM").val(sessionStorage.ontvangstmedium);

	var $fldZoekRelatie = $("#_fldZOEKRELATIE");
	if ($fldZoekRelatie.val() != '') {
    	$("#nieuwe_aanvraag_cursusselectie input.as_showfield:first").focus();
	} else {
		// Initialiseer het formulier (alsof er geannuleerd wordt)
		Macs.NieuweAanvraag.annuleerForm();
	}
});
