Macs.ArtikelVerkoopFormulier = {
    visible: false,
    betaalwijze:'ACP',
    totaalverzendkosten: 0,
    htmlArtikelen: '',
    templateArtikelen: '',
    initialize: function() {
        var Self = Macs.ArtikelVerkoopFormulier;

        Self.schermBezocht = false;
        
        var directive = {
          "tr.cursus": {
            "cursus <- ": {
              ".CURSUSCODE@value": "cursus.cursusnaam",
              "td.CURSUSNAAM": "cursus.cursusnaam",
              "tr.artikel": {
                "artikel <- cursus.artikelen": {
                  ".PICKTYPECODE@value": "artikel.artikelcode",
                  "INPUT.OMSCHRIJVING@value": "artikel.omschrijving",
                  "SPAN.OMSCHRIJVING": "artikel.omschrijving",
                  ".AANTAL@value": function(args){ return args.item.aantal || 0 },
                  ".PRIJSPERSTUKINEUROS@value": function(args){ return Macs.SharedLib.formatFloat(args.item.bedragineuros) },
                  ".VERZENDKOSTEN@value":"artikel.verzendkosten",
                  ".VERWIJDERINGSBIJDRAGE@value":"artikel.verwijderingsbijdrage",
                  ".VERWIJDERINGSBIJDRAGE@readonly": function(args){ return args.item.verwijderingsbijdrage ? '' : 'readonly'; }
                }
              }
            }
          }
        }

        Self.htmlArtikelen = $("#artikelentabel");
        Self.templateArtikelen = Self.htmlArtikelen.compile(directive);
        
        $("#btnSAVEANDCLOSE").click(function() { $("#btnSAVE").trigger('click'); });

        $("#artikelenformulier").jqm({modal:true, onHide: Self.hide});
    },
    show: function(cursusseninfo) {
        var Self = Macs.ArtikelVerkoopFormulier;
        
        Self.schermBezocht = true;
        Self.htmlArtikelen = Self.htmlArtikelen.render(cursusseninfo, Self.templateArtikelen);
        
        var $artikelenformulier = $("#artikelenformulier");

        $(".AANTAL,.PRIJSPERSTUKINEUROS", $artikelenformulier).change(function() { Macs.ArtikelVerkoopFormulier.totaalverzendkosten = 0; Macs.ArtikelVerkoopFormulier.updateTotalen(cursusseninfo) });
        $("#_fldTOTAALVERZENDKOSTEN").change(function() { Self.totaalverzendkosten = $(this).val(); Macs.ArtikelVerkoopFormulier.updateTotalen(cursusseninfo) });
        Macs.ArtikelVerkoopFormulier.updateTotalen(cursusseninfo); // initialiseer de artikelen bedragen   
        $("#_fldBETAALWIJZECODE").val(Self.betaalwijze||'ACP');
        
        $("#_fldBETAALWIJZECODE").change(function() {
            Self.betaalwijze = $(this).val();
            if ($(this).val() == 'REM') {
                $("#_lblFRANKEERKOSTEN").text('Rembourskosten');
                $("#_fldTOTAALVERZENDKOSTEN").attr('readonly','readonly').val(Macs.SharedLib.formatFloat(Macs.SharedLib.parameters.KOSTENREMBOURS));
            } else {
                $("#_lblFRANKEERKOSTEN").text('Verzendkosten');
                $("#_fldTOTAALVERZENDKOSTEN").attr('readonly','');
                Macs.ArtikelVerkoopFormulier.updateTotalen(cursusseninfo);
            }
            Macs.NieuweInschrijving.updateBetaalwijze();
        });
        $('input:text', $artikelenformulier).setMask(); // apply the input masks
                                   
        Macs.ArtikelVerkoopFormulier.visible = true;
        $artikelenformulier.jqmShow();
        Polaris.Form.setFocusFirstField("#artikelenformulier");
    },
    hide: function(hash) {
        Macs.ArtikelVerkoopFormulier.visible = false;
        hash.w.hide();
        hash.o.remove();
    },
    updateTotalen: function(cursusseninfo) {
        var Self = Macs.ArtikelVerkoopFormulier;
        var totaal = tmptotaalverzendkosten = 0;
        $("#artikelentabel .cursus").each(function(i, cursus) {
            $(".artikel", this).each(function(j, artikel) {
                var aantal = parseFloat($(".AANTAL",this).val().replace(',','.'));
                var stukprijs = parseFloat($(".PRIJSPERSTUKINEUROS",this).val().replace(',','.'));
                var verzendkosten = parseFloat($(".VERZENDKOSTEN",this).val().replace(',','.'));
                
                // bewaar waarden in het object model
                cursusseninfo[i].artikelen[j].aantal = aantal;
                cursusseninfo[i].artikelen[j].bedragineuros = stukprijs;

                // update de subtotalen
                var subtotaal = aantal * stukprijs;
                $(".SUBTOTAAL",this).text(Macs.SharedLib.formatFloat(subtotaal));
                
                // en hou de totalen bij voor na de loop
                totaal = totaal + subtotaal;
                if (aantal > 0)
                    tmptotaalverzendkosten = tmptotaalverzendkosten + verzendkosten;
            });
        });
        if (parseFloat(Self.totaalverzendkosten) != 0)
            tmptotaalverzendkosten = parseFloat(Self.totaalverzendkosten);

        $("#_fldTOTAALVERZENDKOSTEN").val(tmptotaalverzendkosten);
        $("#_fldTOTAAL").text(Macs.SharedLib.formatFloat(totaal + tmptotaalverzendkosten));
    }
}

$(document).ready(function(){
    Macs.ArtikelVerkoopFormulier.initialize();    
});