Macs.Betaalwijze = {
	betaalVelden: ['BETAALWIJZE', 'BANKGIROREKENINGNR', 'BETAALDAG', 'PLR__RECORDID'],
	artikelRijTemplate: null,
	betaalDagen: null,
	bepaalBetaalDag: function() {
		var url = Macs.SharedLib._macs_servicequery.replace("%construct%", "automatische_betaaldag");
		$.getJSON(url, {
			limit: 10
		},
		function(data, textStatus) {
            if (data != null) {
    			if (textStatus == 'success') {
	    			Macs.Betaalwijze.betaalDagen = data;
		    		Macs.Betaalwijze.vulBetaalDag();
    			} else {
    				alert('De betaaldagen kon niet geladen worden. Error: ' + textStatus);
	    		}
            } else {
                alert('De betaaldagen kon niet geladen worden. Error: geen data. Niet genoeg rechten?');
            }
		});
	},
	bepaalIncassoDatum: function() {
		$("#_fldINCASSODAG").text('');
		if ($("#_fldBETAALWIJZE").val() == 'AUT') {
			$.getJSON(_servicequery, {
				func: 'bepaalincassodatum'
			  , betaaldag: $("#_fldBETAALDAG").val()
			  , inschrijfnr: Macs.InschrijvingSelectieForm.inschrijvingData.INSCHRIJFNR
			},
			function(data, textStatus) {
	            if (data != null) {
	    			if (textStatus == 'success' && data.incassodatum != null) {
	    				$("#_fldINCASSODAG").text('Eerst mogelijke incassodatum: '+data.incassodatum);
	    			} else {
	    				$("#_fldINCASSODAG").text('De incassodatum kan niet bepaald worden');
		    		}
	            } else {
	                alert('De incassodatum kan niet geladen worden. Error: geen data. Niet genoeg rechten?');
	            }
			});
		}
	},
	vulBetaalDag: function() {
		$("#_fldBETAALDAG").empty();
		if ($("#_fldBETAALWIJZE").val() == 'AUT' && Macs.InschrijvingSelectieForm.inschrijvingData.LANDCODE !== 'DE') {
			$("#_fldBETAALDAG").addOptions(Macs.Betaalwijze.betaalDagen, 'AUTOBETAALDAG', 'AUTOBETAALDAG', Macs.InschrijvingSelectieForm.inschrijvingData.BETAALDAG);
		} else {
			var lvd = Macs.InschrijvingSelectieForm.inschrijvingData.LESSENVERZENDDAG || '';
			$("#_fldBETAALDAG").html("<option value='" + lvd + "'>" + lvd + "</option>");
		}
	},
	selecteerInschrijving: function() {
		var item = Macs.InschrijvingSelectieForm.inschrijvingData;

		/**
		 *  Toon de betaalwijze e.d. in het Betaalwijze formulier
		 */
		Macs.SharedLib.easyFillForm(item, Macs.Betaalwijze.betaalVelden);
		if (item.BETAALWIJZE == 'AUT')
			$("#_fldBETAALDAG,#_fldBANKGIROREKENINGNR").addClass('required');
		else
			$("#_fldBETAALDAG,#_fldBANKGIROREKENINGNR").removeClass('required');

		$("#_hdnRecordID").val(item.PLR__RECORDID);

		Macs.Betaalwijze.vulBetaalDag();
		Macs.Betaalwijze.bepaalIncassoDatum();

		$("#_fldBETAALWIJZE").focus();
	},
	zoekRelatie: function() {
		$("#_fldBETAALWIJZE").val('ACP');
		$("#_fldBANKGIROREKENINGNR").val('');
		$("#_fldBETAALDAG").val('1');
		$("#_hdnRecordID").val('');
	},
	bewaarBetaalwijze: function() {
		// Ga er vanuit dat er geen problemen met het formulier zijn
		var formValid = true;

		/***
		 * Controleer de standaard velden
		 */
		var validator = $("#dataform").validate({
			onsubmit: false,
			debug: false,
			validateHiddenFields: false
		});
		if (!$("#dataform").valid()) {
			formValid = false;
    		var counter = $("#dataform").validate().numberOfInvalids();
			Polaris.Base.modalMessage($.t('validation.incorrect_field', {count: counter}), 1000, false);
		}

		/***
		 * Controleer of er een relatie is geselecteerd
		 */
		if ($('#RELATIE_hdnRecordID').val() === '') {
			formValid = false;
			log("geen relatie geselecteerd");
			Polaris.Base.modalMessage(Macs.Messages.geenRelatieGeselecteerd);
			return;
		}

		if (formValid) {
	        Polaris.Ajax.postJSON(_servicequery, $('#dataform'), null, $.t('plr.changes_saved'), Macs.Betaalwijze.annuleerForm);
		}
	},
	annuleerForm: function() {
		Polaris.Form.userIsEditing = false;

		/* Save knop uitzetten */
		$('#btnSAVE').addClass('disabled');

		Macs.InschrijvingSelectieForm.maakInschrijvingLeeg();
		Macs.SharedLib.easyFillForm([], Macs.Betaalwijze.betaalVelden);
		$("#_fldBETAALDAG,#_fldBANKGIROREKENINGNR").removeClass('required');
		$("#_fldBETAALDAG").empty();
		$("#_hdnRecordID").val('');

		$("#_fldZOEKINSCHRIJVING").focus();
	}
};

// begin meteen met het ophalen van gui ONafhankelijke informatie
Macs.SharedLib.laadParameters();
Macs.Betaalwijze.bepaalBetaalDag();

$(document).ready(function() {
	Macs.InschrijvingSelectieForm.initialiseer({
		'funcSelecteerInschrijving': Macs.Betaalwijze.selecteerInschrijving,
		'funcZoekRelatie': Macs.Betaalwijze.zoekRelatie
	});

	/* Form actions */
	$('#dataform').submit(function(e) {
		e.preventDefault();
	});
	$('#btnCANCEL').click(function(e) {
		e.preventDefault();
		Macs.Betaalwijze.annuleerForm();
	});
	$('#btnSAVE').click(function(e) {
		e.preventDefault();
		if (!$(this).hasClass('disabled')) Macs.Betaalwijze.bewaarBetaalwijze();
	});

	$("#_fldBETAALWIJZE").change(function() {
		Macs.Betaalwijze.vulBetaalDag();
	});

	$("#_fldBETAALWIJZE,#_fldBETAALDAG,#_fldBANKGIROREKENINGNR").change(function() {
		var auto = $("#_fldBETAALWIJZE").val() == 'AUT';
		$("#_fldBETAALDAG,#_fldBANKGIROREKENINGNR").toggleClass('required', auto);

		Macs.Betaalwijze.bepaalIncassoDatum();

		// Geef aan Polaris door dat de gebruiker het formulier aan het invullen is, zodat
		// er een extra bevestiging wordt gevraagd zodra de gebruiker een ander formulier kiest
		Polaris.Form.userIsEditing = true;

		/* Save knop aanzetten */
		$('#btnSAVE').removeClass('disabled');

		/* record state omzetten naar Edit */
		$("#_hdnAction").val('save');
		$("#_hdnState").val('edit');
	});

	// Initialiseer het formulier (alsof er geannuleerd wordt)
	Macs.Betaalwijze.annuleerForm();
});
