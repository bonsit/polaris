Macs.InschrijvingSelectieForm = {
	inschrijvingenData: [],

	inschrijvingData: [],

	htmlInschrijvingen: '',
	templateInschrijvingen: '',
	inschrijvingVelden: ['CURSUSCODE', 'CURSUSNAAM', 'DATUMINSCHRIJVING', 'VOORLETTERS', 'TUSSENVOEGSEL', 'ACHTERNAAM', 'STRAAT', 'HUISNR', 'POSTCODE', 'PLAATS', 'OPZEGGINGSDATUM', 'DATUMGERETOURNEERD', 'MENING', 'AANMANINGSCODE'],

	relatiesData: [],
	relatieData: [],
	htmlRelaties: '',
	templateRelaties: '',

	htmlInschrijvingen: '',
	templateInschrijvingen: '',

	datumsData: [],

	funcSelecteerInschrijving: function() {},
	funcAfterZoekRelatie: function() {},

	initialiseer: function(callbacks) {
		var Self = Macs.InschrijvingSelectieForm;

		if (callbacks.funcSelecteerInschrijving) Self.funcSelecteerInschrijving = callbacks.funcSelecteerInschrijving;
		if (callbacks.funcZoekRelatie) Self.funcAfterZoekRelatie = callbacks.funcZoekRelatie;
		if (callbacks.funcOnZoekRelatie) Self.funcOnZoekRelatie = callbacks.funcOnZoekRelatie;

		Self.htmlRelaties = $('#relaties tbody');
		//directive to render the template
		var directivesRelaties = {
			'tr': {
				'relatie<-': {
					'td.nummer': 'relatie.RELATIENR',
					'td.naam': function(arg) {
						return Macs.SharedLib.bepaalAanhef(arg.item.GESLACHT) + ' ' + (arg.item.VOORLETTERS || '') + ' ' + arg.item.ACHTERNAAM;
					},
					'td.bedrijf': 'relatie.BEDRIJFSNAAM',
					'td.straat': function(arg) {
						return arg.item.STRAAT + ' ' + arg.item.HUISNR;
					},
					'td.postcode': 'relatie.POSTCODE',
					'td.plaats': 'relatie.PLAATS',
					'td.land': 'relatie.LANDCODE',
					'@onclick': function(arg) {
						return 'Macs.InschrijvingSelectieForm.selecteerRelatie(this,' + arg.pos + ');';
					},
					'@onmouseover': '"$(this).toggleClass(\'active\', true);"',
					'@onmouseout': '"$(this).toggleClass(\'active\', false);"',
					'@style': "'cursor:pointer'",
					'@class': function(arg) {
						//arg => {data:data, items:items, pos:pos, item:items[pos]};
						var oddEven = (arg.pos % 2 == 0) ? 'even ' : 'odd ';
						var firstLast = (arg.pos == 0) ? 'first ' : (arg.pos == arg.items.length - 1) ? 'last ' : '';
						var opvallend = arg.item.HANDELINGSONBEKWAAMJANEE == 'J' ? 'alert' : '';
						return ' ' + oddEven + firstLast + opvallend;
					}
				}
			}
		};

		Self.templateRelaties = Self.htmlRelaties.compile(directivesRelaties);

		Self.htmlInschrijvingen = $('#inschrijvingen tbody');
		//directive to render the template
		var directivesInschrijvingen = {
			'tr': {
				'inschrijving<-': {
					'td.inschrijfnr': 'inschrijving.INSCHRIJFNR',
					'td.relatie': 'inschrijving.ACHTERNAAM',
					'td.relatienr': 'inschrijving.CURSISTNR',
					'td.cursuscode': 'inschrijving.CURSUSCODE',
					'td.aanmaningscode': 'inschrijving.AANMANINGSCODE',
					'td.datuminschrijving': 'inschrijving.DATUMINSCHRIJVING',
					'@onclick': function(arg) {
						return 'Macs.InschrijvingSelectieForm.selecteerInschrijving(this,' + arg.pos + ');';
					},
					'@onmouseover': '"$(this).toggleClass(\'active\', true);"',
					'@onmouseout': '"$(this).toggleClass(\'active\', false);"',
					'@style': "'cursor:pointer'",
					'@class': function(arg) {
						//arg => {data:data, items:items, pos:pos, item:items[pos]};
						var oddEven = (arg.pos % 2 == 0) ? 'even ' : 'odd ';
						var firstLast = (arg.pos == 0) ? 'first ' : (arg.pos == arg.items.length - 1) ? 'last ' : '';
						return ' ' + oddEven + firstLast;
					}
				}
			}
		};

		Self.templateInschrijvingen = Self.htmlInschrijvingen.compile(directivesInschrijvingen);
	},
	initialiseerEvents: function() {
		var Self = Macs.InschrijvingSelectieForm;

		/* Zoek Inschrijving events */
		$('#btnSEARCHINSCHRIJVING').click(function(e) {
			e.preventDefault();
			Self.zoekInschrijving();
		});
		$("#_fldZOEKINSCHRIJVING").keyup(function(event) {
			if (event.keyCode == 13 /*Enter*/ ) {
				event.preventDefault();
				Self.zoekInschrijving();
			}
		});

		/* Zoek relatie events */
		var searchboxAction = function(e) {
			e.preventDefault();
			Self.zoekRelatie();
		};
		$(".searchbox input").keydown(function(event) {
			if (event.keyCode == 13 /*Enter*/ ) {
				searchboxAction(event);
			}
		});
		$('#btnSEARCHRELATIE').click(searchboxAction);
	},
	selecteerRelatie: function(elem, pos) {
		var Self = Macs.InschrijvingSelectieForm;

        Self.relatieData = Self.relatiesData[pos];

        if (Self.funcOnZoekRelatie) {
            Self.funcOnZoekRelatie(Self.relatieData.RELATIENR);
        } else {
    		Self.zoekInschrijvingen(Self.relatieData.RELATIENR);
        }
	},
	selecteerInschrijving: function(elem, pos) {
		var Self = Macs.InschrijvingSelectieForm;

		/* Verwijder dialoog venster/ Inschrijvingen */
		$("#inschrijvingen").jqmHide();

		/* Toont het Inschrijving tabblad */
		$("#tab_inschrijving").click();
		/* Leg de gekozen inschrijving vast als lokale/globale var */
		Self.inschrijvingData = Self.inschrijvingenData[pos];

		/* Vul de gevonden inschrijving */
		Self.vulInschrijving();
	},
	vulInschrijving: function() {
		var Self = Macs.InschrijvingSelectieForm;

		var _data = Self.inschrijvingData;
		/* Als er een factuurrelatie is, dan bankrekeningnr ophalen van die relatie */
		if (_data.FACTUURRELATIE != null && _data.FACTUURRELATIE != '') {
			Self.bepaalFactuurBankRekening(_data.FACTUURRELATIE, function(bankrekening) {
				_data.FACTUURRELATIEBANKREKENINGNR = bankrekening;
			});
		}
		$('#inschrijving_relatie').autoRender(_data);

		/* reset het inschrijfnr, omdat deze overschreven wordt door Pure (!) */
		$("#_fldZOEKINSCHRIJVING").val(_data.INSCHRIJFNR);

		/* Reconnect the events, because Pure deletes the events(!) */
		Self.initialiseerEvents();

		Self.funcSelecteerInschrijving(_data.INSCHRIJFNR);
	},
	maakInschrijvingLeeg: function() {
    	$('#relaties tbody tr').empty();
		$('#inschrijving_relatie .formcolumn label').text('');
	},
	zoekInschrijving: function() {
		var Self = Macs.InschrijvingSelectieForm;
		var _inschrijvingnr = $('#_fldZOEKINSCHRIJVING').val();
		if (Macs.SharedLib.isInteger(_inschrijvingnr)) {
            var url = Macs.SharedLib._macs_servicequery.replace("%construct%", "inschrijvingrelaties").replace("const", "form") + "?limit=30" + "&qc[]=INSCHRIJFNR&qv[]='" + _inschrijvingnr + "'";
            $.getJSON(url, function(data, textStatus) {
                if (textStatus == 'success' && data.length > 0) {
                    /* Leg data vast als lokale/globale var */
                    Self.inschrijvingData = data[0];
                    /* Vul de gevonden inschrijving */
                    Self.vulInschrijving();
                } else {
                    var _error = data.length == 0 ? $.t('nha.macs.geenInschrijvingGevonden') : 'Ajax call error 1';
                    Polaris.Base.modalMessage($.t('nha.macs.foutZoekActie') + _error);
                }
            });
        } else {
            alert('Inschrijfnr "' + _inschrijvingnr + '" is not a number. Please contact Admin (bartbons@debster.nl).' );
            log(_inschrijvingnr);
        }
	},
	zoekInschrijvingen: function(cursistnr, callback) {
		var Self = Macs.InschrijvingSelectieForm;
		var url = Macs.SharedLib._macs_servicequery.replace("%construct%", "inschrijvingrelaties").replace("const", "form") + "?limit=30" + "&qc[]=CURSISTNR^FACTUURRELATIE&qv[]='" + cursistnr + "'^'" + cursistnr + "'";
		var defaultAction = function(data, textStatus) {
			/* Leg data vast als lokale/globale var */
			if (textStatus == 'success' && data.length > 0) {
				if (data.length == 1) {
					Self.inschrijvingData = data[0];

                    /* Vul de gevonden inschrijving */
                    Self.vulInschrijving();
                    /* Toont het Inschrijving tabblad */
                    $("#tab_inschrijving").click();
				} else {
					/* Leg data vast als lokale/globale var */
					Self.inschrijvingenData = data;
					/* Laat gebruiker kiezen uit de gevonden inschrijvingen */

                    /* Zet de data in de Inschrijvingen tabel */
                    Self.htmlInschrijvingen = Self.htmlInschrijvingen.render(Self.inschrijvingenData, Self.templateInschrijvingen);

                    /* Toon Inschrijvingen tabel en laat de gebruiker kiezen */
                    $("#inschrijvingen").jqm().jqmShow();
				}
			} else {
				var _error = data.length == 0 ? $.t('nha.macs.geenInschrijvingGevonden') : 'Ajax call error 2';
				Polaris.Base.modalMessage($.t('nha.macs.foutZoekActie') + _error);
			}
		};

		if (typeof callback == 'function')
    		$.getJSON(url, callback);
        else
    		$.getJSON(url, defaultAction);
	},
	bepaalFactuurBankRekening: function(factuurrelatienr, callback) {
		var Self = Macs.InschrijvingSelectieForm;

		var url = Macs.SharedLib._macs_servicequery.replace("%construct%", "laatsterelatiebankrekening").replace("const", "form") + "?limit=30" + "&qo[]=EQ&qc[]=RELATIENR&qv[]='" + factuurrelatienr + "'";
		$.getJSON(url, function(data, textStatus) {
			/* Leg data vast als lokale/globale var */
			if (textStatus == 'success' && data.length > 0) {
				callback(data[0].BANKGIROREKENINGNR);
				return true;
			} else {
				return false;
			}
		});
	},
	vulRelaties: function(data) {
		var Self = Macs.InschrijvingSelectieForm;

		// rendering but reusing the compiled function template
		Self.htmlRelaties = Self.htmlRelaties.render(data, Self.templateRelaties);
	},
	zoekRelatieOpNummer: function(relatienr, callback) {
		var Self = Macs.InschrijvingSelectieForm;

		var _url = Macs.SharedLib._macs_servicequery.replace("%construct%", "relatie").replace("const", "form") + "?limit=30" + "&qo[]=EQ&qc[]=RELATIENR&qv[]=" + relatienr;
		$.getJSON(_url, function(data, textStatus) {
			/* Leg data vast als lokale/globale var */
			if (textStatus == 'success') {
				callback(data);
			} else {
				callback(false);
			}
		});
	},
	zoekRelatieOpAdres: function(callback) {
		var Self = Macs.InschrijvingSelectieForm;

		$('#_fldZOEKPOSTCODE').val($('#_fldZOEKPOSTCODE').val().toUpperCase());
		var _postcode = $('#_fldZOEKPOSTCODE').val().replace(' ', '');
		if (_postcode) _postcode += '%';
		var _huisnr = $('#_fldZOEKHUISNR').val();
		if (_huisnr) _huisnr += '%';

		var _url = Macs.SharedLib._macs_servicequery.replace("%construct%", "relatie").replace("const", "form") + "?limit=50" + "&qc[]=POSTCODE&qv[]=" + _postcode + "&qc[]=HUISNR&qv[]=" + _huisnr + "&qc[]=LAND&qv[]=" + $('#_fldZOEKLAND').val();
		$.getJSON(_url, function(data, textStatus) {
			/* Leg data vast als lokale/globale var */
			if (textStatus == 'success') {
				callback(data);
			} else {
				callback(false);
			}
		});
	},
	zoekRelatie: function() {
		var Self = Macs.InschrijvingSelectieForm;

		var _relatienr = $('#_fldZOEKRELATIE').val();

		var callback = function(data) {
			Self.relatiesData = data;
			if (Self.relatiesData) {
			    Self.vulRelaties(Self.relatiesData);
                if (Self.relatiesData.length == 1) {
                    Self.relatieData = Self.relatiesData[0];
                }

			    if (Self.funcOnZoekRelatie) {
       			    Self.funcOnZoekRelatie(_relatienr);
                } else {
                    if (Self.relatiesData.length == 1) {
                        Self.zoekInschrijvingen(Self.relatieData.RELATIENR);
                    }
                }
			} else {
			    Polaris.Base.modalMessage($.t('nha.macs.foutZoekActie'));
            }

			// voer eventuele extra stappen uit vanuit het oproepende formulier
			Self.funcAfterZoekRelatie();
		};

		if (_relatienr)
		    Self.zoekRelatieOpNummer(_relatienr, callback);
		else
		    Self.zoekRelatieOpAdres(callback);
	},
	focusZoekVeld: function() {
        $(".pagecontents:visible :input:first").focus();
	}
};

$(document).ready(function() {
	Macs.InschrijvingSelectieForm.initialiseerEvents();

	$("#pagetabs").bind('switched', function(event, pageid, alltabs, container, settings) {
		$(pageid + " :input:first").focus();
	});

    // zet focus op eerste zichtbare zoekveld
    Macs.InschrijvingSelectieForm.focusZoekVeld();

	if ($("#_fldZOEKINSCHRIJVING").val() != '') {
	    Macs.InschrijvingSelectieForm.zoekInschrijving();
	}

	if ($("#_fldZOEKRELATIE").val() != '') {
		Macs.InschrijvingSelectieForm.zoekRelatie();
	}


});
