Macs.BetalingsverzoekenSamenvoegen = {
	betalingsverzoeken: null,
	initialiseer: function() {
		var Self = Macs.BetalingsverzoekenSamenvoegen;

		var directive = {
			"tr.betalingsverzoek": {
				"betalingsverzoek <- ": {
					".PLR__RECORDID@value": "betalingsverzoek.PLR__RECORDID",
					".TERMIJN": "betalingsverzoek.TERMIJNNR",
					".KENMERK": "betalingsverzoek.BETALINGSKENMERK",
					".STATUS": "betalingsverzoek.STATUS",
					".BEDRAG": function(arg) {
						return Macs.SharedLib.formatFloat(arg.item.BEDRAGINEUROS, 2);
					},
					".BEDRAGBETAALD": function(arg) {
						return Macs.SharedLib.formatFloat(arg.item.BEDRAGBETAALDINEUROS, 2);
					},
					".TEBETALEN": function(arg) {
						return Macs.SharedLib.formatFloat(arg.item.BEDRAGNOGTEBETALENINEUROS, 2);
					},
					".BETAALWIJZE": "betalingsverzoek.BETAALWIJZECODE",
					".VERZENDDATUM": "betalingsverzoek.VERZENDDATUM",
					".VERVALDATA": "#{betalingsverzoek.CURSISTVERVALDATUM} / #{betalingsverzoek.SYSTEEMVERVALDATUM}",
					".HERINNERING@checked": function(arg) {
						return arg.item.HERINNERINGJANEE == 'J' ? 'checked' : '';
					},
					".REGELING@checked": function(arg) {
						return arg.item.BETALINGSREGELINGJANEE == 'J' ? 'checked' : '';
					},
					".SAMEN@value": "betalingsverzoek.PLR__RECORDID"
				}
			}
		};

		Self.htmlBetalingsverzoeken = $("#betalingsverzoekentabel");
		Self.templateBetalingsverzoeken = Self.htmlBetalingsverzoeken.compile(directive);

		// filter de verzoeken-set op basis van de aangeklikte filter(s)
		$("#betalingsverzoek_filters input").click(function(e) {
			Macs.BetalingsverzoekenSamenvoegen.vulBetalingsverzoeken();
		});

		$(window).resize(Macs.BetalingsverzoekenSamenvoegen.resize).trigger('resize');
		$(".pagetabcontainer a").click(Macs.BetalingsverzoekenSamenvoegen.resize);
	},
	bepaalBetalingsverzoeken: function(inschrijfnr) {
		$.getJSON(_servicequery, {
			'func': 'betalingsverzoeken',
			'inschrijfnr': inschrijfnr
		},
		function(data, textStatus) {
			if (textStatus == 'success') {
				Macs.BetalingsverzoekenSamenvoegen.betalingsverzoeken = data;
				Macs.BetalingsverzoekenSamenvoegen.vulBetalingsverzoeken();
        		$(window).trigger('resize');
			} else {
				alert('De betaaldagen kon niet geladen worden. Error: ' + textStatus);
			}
		});
	},
	bepaalTotalen: function(verzoeken) {
		var Self = Macs.BetalingsverzoekenSamenvoegen;

		var ids = $("#betalingsverzoekentabel tr:has(.SAMEN:checked)").map(function(i, item) {
			return $(item).find(".PLR__RECORDID").val();
		});

		var totaalbedrag = totaalbetaald = totaaltebetalen = 0;
		$(Self.betalingsverzoeken).each(function(i, elem) {
			if ($.inArray(elem.PLR__RECORDID, ids) != -1) {
				totaalbedrag = totaalbedrag + parseFloat(elem.BEDRAGINEUROS);
				totaalbetaald = totaalbetaald + parseFloat(elem.BEDRAGBETAALDINEUROS);
				totaaltebetalen = totaaltebetalen + parseFloat(elem.BEDRAGNOGTEBETALENINEUROS);
			}
		});

		// vul de totalen in
		$("#_fldTOTAALBEDRAG").val(Macs.SharedLib.formatFloat(totaalbedrag, 2));
		$("#_fldTOTAALBETAALD").val(Macs.SharedLib.formatFloat(totaalbetaald, 2));
		$("#_fldTOTAALBEDRAGTEBETALEN").val(Macs.SharedLib.formatFloat(totaaltebetalen, 2));

		// vul de nieuwe waarden in
		$("#_fldBEDRAG_NIEUW").val(Macs.SharedLib.formatFloat(totaaltebetalen, 2)).focus();
		$("#_fldBEDRAGBETAALD_NIEUW").text(Macs.SharedLib.formatFloat(0, 2));
		$("#_fldTEBETALEN_NIEUW").text(Macs.SharedLib.formatFloat(0, 2));
	},
	vulBetalingsverzoeken: function() {
		var Self = Macs.BetalingsverzoekenSamenvoegen;
		var filteredVerzoeken = Self.filterVerzoeken(Self.betalingsverzoeken);
		Self.htmlBetalingsverzoeken = Self.htmlBetalingsverzoeken.render(filteredVerzoeken, Self.templateBetalingsverzoeken);

		$("#_fldINSCHRIJFNR_NIEUW").val(Self.betalingsverzoeken[0].INSCHRIJFNR);
		$("#_fldRELATIENR_NIEUW").val(Self.betalingsverzoeken[0].RELATIENR);
		$("#_fldVERZENDDATUM_NIEUW").val(Macs.SharedLib.bepaalHuidigeDatum(1));
		$("#_fldBETAALWIJZE_NIEUW").text('ACP');

		// bereken de totalen wanneer op een Samen checkbox wordt geklikt
		$("#betalingsverzoekentabel .SAMEN").click(function(e) {
            // Geef aan Polaris door dat de gebruiker het formulier aan het invullen is, zodat 
            // er een extra bevestiging wordt gevraagd zodra de gebruiker een ander formulier kiest
            Polaris.Form.userIsEditing = true;
    
			Polaris.Dataview._checkCheckBox(this);
			Self.bepaalTotalen(filteredVerzoeken);

		});
	},
	filterVerzoeken: function(items) {
		var filters = $("#betalingsverzoek_filters input").map(function(index, elem) {
			if ($(elem).attr('checked')) return $(elem).val();
		});

		var result = [];
		if (filters.length > 0) {
			$(items).each(function(index, item) {
				var found = $.inArray(item.STATUS, filters);
				items[index].FIL = found;
				if (found != -1) {
					result.push(item);
				}
			});
		} else {
			result = items;
		}
		return result;
	},
	selecteerInschrijving: function() {
		Macs.BetalingsverzoekenSamenvoegen.bepaalBetalingsverzoeken(Macs.InschrijvingSelectieForm.inschrijvingData.INSCHRIJFNR);
	},
	zoekRelatie: function() {},
	bewaarForm: function() {
		// Ga er vanuit dat er geen problemen met het formulier zijn
		var formValid = true;

		/***
		 * Controleer de standaard velden
		 */
		var validator = $("#dataform").validate({
			onsubmit: false,
			debug: false,
			validateHiddenFields: false
		});
		if (!$("#dataform").valid()) {
			formValid = false;
			Polaris.Base.modalMessage('U heeft ' + validator.numberOfInvalids() + ' velden niet correct ingevuld.', 1000, false);
		}

		if (formValid) {
			//$('#dataform')[0].submit();
			//return;
			jQuery.postJSON(_servicequery, $('#dataform'), function(data, textStatus) {
				if (textStatus == 'success') {
					validator.resetForm();
					if (data.error == false) {
						if (data.result == true) {
							Macs.SharedLib.ajaxFeedback('Record opgeslagen');
						}
						Macs.BetalingsverzoekenSamenvoegen.bepaalBetalingsverzoeken(Macs.InschrijvingSelectieForm.inschrijvingData.INSCHRIJFNR);
					} else {
						Polaris.Base.errorDialog(data.error, data.detailed_error);
					}
				} else {
					Macs.SharedLib.ajaxFeedback('Fout opgetreden bij JSON post.');
				}
			});
		}
	},
	annuleerForm: function() {
		Polaris.Form.userIsEditing = false;

		Macs.InschrijvingSelectieForm.maakInschrijvingLeeg();
	},
	resize: function() {
		$bs = $(".scrollable");
		var height = $(window).height() - $bs.offset().top;
		if ($(".buttons").length > 0) height = Math.max(100, height - $(".buttons").outerHeight() - 0);
		
		height = 100;
		$bs.css(($.browser.msie && $.browser.version < 7 ? '' : 'max-') + 'height', height + 'px');
		$bs.height(height);
	}
};

$(document).ready(function() {
	Macs.InschrijvingSelectieForm.initialiseer({
		'funcSelecteerInschrijving': Macs.BetalingsverzoekenSamenvoegen.selecteerInschrijving,
		'funcZoekRelatie': Macs.BetalingsverzoekenSamenvoegen.zoekRelatie
	});

	/* Form actions */
	$('#dataform').submit(function(e) {
		e.preventDefault();
	});
	$('#btnCANCEL').click(function(e) {
		e.preventDefault();
		Macs.BetalingsverzoekenSamenvoegen.annuleerForm();
	});
	$('#btnSAVE').click(function(e) {
		e.preventDefault();
		if (!$(this).hasClass('disabled')) Macs.BetalingsverzoekenSamenvoegen.bewaarForm();
	});

	// initialiseer het formulier
	Macs.BetalingsverzoekenSamenvoegen.initialiseer();

	// Maak het formulier leeg (alsof er geannuleerd wordt)
	Macs.BetalingsverzoekenSamenvoegen.annuleerForm();
});