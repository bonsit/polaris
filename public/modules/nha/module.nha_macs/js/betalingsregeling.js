Macs.BetalingsRegeling = {
	betalingsverzoeken: null,
	initialiseer: function() {
		var Self = Macs.BetalingsRegeling;

		$("#_fldMAANDBEDRAG, #_fldADMINKOSTENKWIJTSCHELDEN").change(function() {
			Macs.BetalingsRegeling.regelingBerekenen();
		});
  		$("#_fldINGANGSDATUM").date_input();
	},
	bepaalBetalingsRegeling: function(inschrijfnr) {
		$("#betalingsregelingtable :input").val('');

		$.getJSON(_servicequery, {
			'func': 'betalingsregeling',
			'inschrijfnr': inschrijfnr
		},
		function(data, textStatus) {
			if (textStatus == 'success') {
				Macs.BetalingsRegeling.betalingsregeling = data;
				Macs.BetalingsRegeling.vulBetalingsRegeling();
			} else {
				alert('De betaaldagen kon niet geladen worden. Error: ' + textStatus);
			}
		});
	},
	vulBetalingsRegeling: function() {
		var Self = Macs.BetalingsRegeling;

		$("#_fldOPENSTAAND").val(Macs.SharedLib.formatFloat(Macs.BetalingsRegeling.betalingsregeling.OPENSTAANDINEUROS));
		$("#_lblOPENSTAAND").text(Macs.SharedLib.formatFloat(Macs.BetalingsRegeling.betalingsregeling.OPENSTAANDINEUROS, 2));
		$("#_fldOPENSTAANDADMINKOSTEN").val(Macs.SharedLib.formatFloat(Macs.BetalingsRegeling.betalingsregeling.OPENSTAANDADMINKOSTENINEUROS));
		$("#_lblOPENSTAANDADMINKOSTEN").text(Macs.SharedLib.formatFloat(Macs.BetalingsRegeling.betalingsregeling.OPENSTAANDADMINKOSTENINEUROS, 2));
		$("#_fldINGANGSDATUM").val(Macs.SharedLib.bepaalHuidigeDatum());

		if (Macs.BetalingsRegeling.betalingsregeling.OPENSTAANDINEUROS == 0) {
			$("#betalingsregelingtable :input").attr('disabled', true).addClass('readonly');
			/* Save knop aanzetten */
			$('#btnSAVE').addClass('disabled');
			$("#_fldZOEKINSCHRIJVING").focus();
		} else {
			$("#betalingsregelingtable :input").attr('disabled', false).removeClass('readonly');
			$("#_fldMAANDBEDRAG").val("0,00").focus();
			/* Save knop aanzetten */
			$('#btnSAVE').removeClass('disabled');
		}
	},
	selecteerInschrijving: function(inschrijfnr) {
//Macs.InschrijvingSelectieForm.inschrijvingData.INSCHRIJFNR
		$("#_fldINSCHRIJFNR").val(inschrijfnr);
		Macs.BetalingsRegeling.bepaalBetalingsRegeling(inschrijfnr);
	},
	vulInschrijvingenSelect: function(inschrijvingenData) {
	    $("#inschrijvingenselect").change(function() {
			var obj = Macs.SharedLib.findObjectInArray(Macs.InschrijvingSelectieForm.inschrijvingenData, 'INSCHRIJFNR', $(this).val());
			if (obj.CURSUSCODE[0] == 'Q') {
				alert($.t('nha.macs.duitseBetalingsRegelingViaDatev'));
				$('#btnSAVE').addClass('disabled');
			} else {
		        Macs.BetalingsRegeling.selecteerInschrijving($(this).val());
			}
	    });
	    $("#inschrijvingenselect").empty().addOptions(inschrijvingenData, "INSCHRIJFNR", "INSCHRIJFNR,CURSUSCODE,CURSUSNAAM,# - Aanmaningscode:,AANMANINGSCODE")
	    if ($("#_fldZOEKINSCHRIJVING").val() != '')
    	    $("#inschrijvingenselect").val($("#_fldZOEKINSCHRIJVING").val());
	    $("#inschrijvingenselect").change();
	},
	zoekRelatie: function() {},
	onZoekRelatie: function(cursistnr) {
		var Self = Macs.BetalingsRegeling;

	    var callback = function(data, textStatus) {
	        if (textStatus == 'success') {
        		Macs.InschrijvingSelectieForm.inschrijvingenData = data;
                Self.vulInschrijvingenSelect(Macs.InschrijvingSelectieForm.inschrijvingenData);
	        }
	    }

	    if (Macs.InschrijvingSelectieForm.relatieData.RELATIENR) {
            Macs.InschrijvingSelectieForm.zoekInschrijvingen(cursistnr, callback);
        }
	},
	regelingBerekenen: function() {
		var sl = Macs.SharedLib;

		var maandbedrag = sl.floatValue($("#_fldMAANDBEDRAG").val());
		var openstaand = sl.floatValue($("#_fldOPENSTAAND").val());
		var openstaandadminkosten = sl.floatValue($("#_fldOPENSTAANDADMINKOSTEN").val());

		var subtotaal = openstaand;
		if ($("#_fldADMINKOSTENKWIJTSCHELDEN").attr('checked') == false) {
			subtotaal = subtotaal + openstaandadminkosten;
		}
		var aantalmaandenbetalen = Math.floor((subtotaal) / maandbedrag);

		var txt = '';

		var maandtxt = $.t('nha.macs.maand');
		if (aantalmaandenbetalen > 0) {
			var restbedrag = Math.floor((subtotaal - (aantalmaandenbetalen * maandbedrag)) * 100) / 100;
			$("#_fldRESTBEDRAG").val(sl.formatFloat(restbedrag, 2));

			if (aantalmaandenbetalen > 1) {
				txt = maandtxt+' 1/' + aantalmaandenbetalen + ': € ' + sl.formatFloat(maandbedrag, 2) + "\n\n";
			} else {
				txt = maandtxt+' 1: € ' + sl.formatFloat(maandbedrag, 2) + "\n\n";
			}
			if (restbedrag > 0) {
//				aantalmaandenbetalen = aantalmaandenbetalen + 1;
				txt += maandtxt+' ' + (aantalmaandenbetalen + 1) + ': € ' + sl.formatFloat(restbedrag, 2);
			}

			$("#_fldAANTALMAANDEN").val(aantalmaandenbetalen);

			$("#regeling").val(txt);
		} else if (maandbedrag > 0) {
			txt = maandtxt+' 1: € ' + maandbedrag;
			$("#_fldAANTALMAANDEN").val(1);

			$("#regeling").val(txt);
		}
	},
	bewaarForm: function() {
		// Ga er vanuit dat er geen problemen met het formulier zijn
		var formValid = true;

		/***
		 * Controleer de standaard velden
		 */
		var validator = $("#dataform").validate({
			onsubmit: false,
			debug: false,
			validateHiddenFields: false
		});
		if (!$("#dataform").valid()) {
			formValid = false;
    		var counter = $("#dataform").validate().numberOfInvalids();
			Polaris.Base.modalMessage($.t('validation.incorrect_field', {count: counter}), 1000, false);
		}

		if (Macs.SharedLib.floatValue($("#_fldMAANDBEDRAG").val()) <= 0) {
			formValid = false;
			Polaris.Base.modalMessage($.t('nha.macs.maandBedragGroterNul'), 1000, false);
		}

		var m = /([0-9]{2})-([0-9]{2})-([0-9]{4})/.exec($("#_fldINGANGSDATUM").val());
		var dat = new Date(m[3]+'-'+m[2]+'-'+m[1]);
		var mindat = Macs.SharedLib.bepaalHuidigeDatumObject(-1);
		var maxdat = Macs.SharedLib.bepaalHuidigeDatumObject(60);

		if (dat < mindat || dat > maxdat) {
			formValid = false;
			$("#_fldINGANGSDATUM").focus();
			Polaris.Base.modalMessage($.t('nha.macs.regelingBinnenTweeMaanden'), 1000, false);
		}

		if (formValid) {
//$('#dataform')[0].submit();
//return;
	        Polaris.Ajax.postJSON(_servicequery, $('#dataform'), $.t('nha.macs.aanvraagWordtOpgeslagen'), $.t('plr.changes_saved'),
		        function (data) {
            		Polaris.Form.userIsEditing = false;
					Macs.BetalingsRegeling.bepaalBetalingsRegeling(Macs.InschrijvingSelectieForm.inschrijvingData.INSCHRIJFNR);
		        },
	        	function (data) {
					Macs.BetalingsRegeling.annuleerForm();
	        	}
	        );
		}
	},
	annuleerForm: function() {
		Polaris.Form.userIsEditing = false;

		/* Save knop aanzetten */
		$('#btnSAVE').addClass('disabled');

		Macs.InschrijvingSelectieForm.maakInschrijvingLeeg();
		$("#betalingsregelingtable :input").val('');

		$("#regeling").val('');
	}
};

$(document).ready(function() {
	Macs.InschrijvingSelectieForm.initialiseer({
		'funcSelecteerInschrijving': Macs.BetalingsRegeling.selecteerInschrijving,
		'funcZoekRelatie': Macs.BetalingsRegeling.zoekRelatie,
		'funcOnZoekRelatie': Macs.BetalingsRegeling.onZoekRelatie
	});

	/* Form actions */
	$('#dataform').submit(function(e) {
		e.preventDefault();
	});
	$('#btnCANCEL').click(function(e) {
		e.preventDefault();
		Macs.BetalingsRegeling.annuleerForm();
	});
	$('#btnSAVE').click(function(e) {
		e.preventDefault();
		if (!$(this).hasClass('disabled')) Macs.BetalingsRegeling.bewaarForm();
	});

	$("#betalingsregelingtable :input").change(function() {
		// Geef aan Polaris door dat de gebruiker het formulier aan het invullen is, zodat
		// er een extra bevestiging wordt gevraagd zodra de gebruiker een ander formulier kiest
		Polaris.Form.userIsEditing = true;

		/* Save knop aanzetten */
		$('#btnSAVE').removeClass('disabled');

		/* record state omzetten naar Edit */
		$("#_hdnAction").val('save');
		$("#_hdnState").val('edit');
	});

	// initialiseer het formulier
	Macs.BetalingsRegeling.initialiseer();

	if ($("#_fldZOEKRELATIE").val() == '' || $("#_fldZOEKINSCHRIJVING").val() == '') {
    	// Maak het formulier leeg (alsof er geannuleerd wordt)
		Macs.BetalingsRegeling.annuleerForm();
	}
});
