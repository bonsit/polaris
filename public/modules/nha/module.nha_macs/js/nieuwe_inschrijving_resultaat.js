Macs.InschrijvingResultaat = {
	htmlResultaat: '',
	templateResultaat: '',
	initialize: function() {
		var Self = Macs.InschrijvingResultaat;

		var directive = {
		    ".RELATIENR": "<a target='_top' href='javascript:location.href=_serverroot+\"/app/macs/const/relatie4/?qc[]=RELATIENR&qv[]=%27#{relatie.RELATIENR}%27\"'>#{relatie.RELATIENR}</a>",
			".NAAM": function(arg) {
				var res = arg.context.relatie.NAAM;
				if (arg.context.relatie.BEDRIJF != null && arg.context.relatie.BEDRIJF != '') res = arg.context.relatie.BEDRIJF + ' &nbsp; t.a.v. ' + res;
				return res;
			},
			".ADRES": "relatie.ADRES",
			".WOONPLAATS": "relatie.WOONPLAATS",
			".LAND": "relatie.LAND",

		    ".FACTUURRELATIENR": "<a target='_top' href='javascript:location.href=_serverroot+\"/app/macs/const/relatie4/?qc[]=RELATIENR&qv[]=%27#{factuurrelatie.RELATIENR}%27\"'>#{factuurrelatie.RELATIENR}</a>",
			".FACTUURNAAM": function(arg) {
				if (arg.context.factuurrelatie.length == 0) return '';
				var res = arg.context.factuurrelatie.NAAM;
				if (arg.context.factuurrelatie.BEDRIJF != null && arg.context.factuurrelatie.BEDRIJF != '') res = arg.context.factuurrelatie.BEDRIJF + ' &nbsp; t.a.v. ' + res;
				return res;
			},
			".FACTUURADRES": "factuurrelatie.ADRES",
			".FACTUURWOONPLAATS": "factuurrelatie.WOONPLAATS",
			".FACTUURLAND": "factuurrelatie.LAND",
			"tr.inschrijving": {
				"inschrijving <- inschrijving": {
					".INSCHRIJFNR": "<a target='_top' href='javascript:location.href=_serverroot+\"/app/macs/const/inschrijving/?qc[]=INSCHRIJFNR&qv[]=%27#{inschrijving.INSCHRIJFNR}%27\"'>#{inschrijving.INSCHRIJFNR}</a>",
					".CURSUSCODE": "inschrijving.CURSUSCODE",
					".OMSCHRIJVING": function(arg) {
						if (typeof(arg.item.CURSUSCODE) == 'undefined') return "&ndash; " + arg.item.OMSCHRIJVING;
						else return arg.item.OMSCHRIJVING;
					},
					".INSCHRIJFGELD": "inschrijving.INSCHRIJFGELD"
//					".ACTIE": "<a target='_top' href='javascript:location.href=_serverroot+\"/app/macs/const/inschrijving/?qc[]=INSCHRIJFNR&qv[]=%27#{inschrijving.INSCHRIJFNR}%27\"'>Maak opmerking</a>"
				}
			}
		};
		Self.htmlResultaat = $("#resultaattabel");
		Self.templateResultaat = Self.htmlResultaat.compile(directive);

		$("#resultaatformulier").jqm({
			modal: true,
			onHide: Self.hide
		});
	},
	show: function(resultaatinfo) {
		var Self = Macs.InschrijvingResultaat;
		if (typeof resultaatinfo != 'undefined')
			Self.htmlResultaat = Self.htmlResultaat.render(resultaatinfo.result, Self.templateResultaat);
		var $resultaatformulier = $("#resultaatformulier");
		$resultaatformulier.jqmShow();
		$("#btnCLOSE").focus();
	},
	hide: function(hash) {
		hash.w.hide();
		hash.o.remove();
		Macs.NieuweInschrijving.annuleerForm();

        // ingeval van TM oproep, dan frame weghalen bij Bewaren
        var iframe = parent.document.getElementById("inschrijvingframe");

        if ($(iframe).length > 0) {
            $(iframe).remove();
        }

	}
};

$(document).ready(function() {
	Macs.InschrijvingResultaat.initialize();
});
