var Macs = {};

String.prototype.replaceAll = function(a, b) {
    return this.replace(new RegExp(a.replace(/([.?*+^$[\]\\(){}|-])/ig, "\\$1"), 'ig'), b)
}

Macs.SharedLib = {
	_macs_servicequery: _servicequery.substr(0, _servicequery.substr(0, _servicequery.length - 1).lastIndexOf('/')) + "/%construct%/",

	parameters: '',

	initializeLocalization: function() {
        $.jsperanto.init(function(t){
    //        Polaris.Base.afterLanguageResourceLoaded(t);
        }, {
            fallbackLang: 'nl',
            dicoPath: _serverroot+'/modules/nha/module.nha_macs/languages',
            setDollarT: true,
            async : false,
            lang:g_lang.toLowerCase()
        });
    },

	// Return new array with duplicate values removed
	uniqueArray: function(arr) {
		var o = {},
			i, l = arr.length,
			r = [];
		for (i = 0; i < l; i++) o[arr[i]] = arr[i];
		for (i in o) r.push(o[i]);
		return r;
	},
	findObjectInArray: function (objectArray, attribute, attributeValue) {
	    return $.grep(objectArray, function(n, i) {
	    	return n[attribute] == attributeValue;
	    })[0];
	},
    filter: function(array, fn, scope) {
        array = array || [];
        scope = scope || this;

        var result = [];

        for (var i = 0, len = array.length; i < len; ++i) {
            if (fn.call(scope, array[i])) {
                result.push(array[i]);
            }
        }

        return result;
    },
	easyFillForm: function(item, fields, prefix) {
		prefix = prefix || "";
		$(fields).each(function(i, elem) {
			var input = document.getElementById(prefix + '_fld' + elem); // Dont use jQuery, because it does not work with special chars like in the prefix (#RELATIE!VOORLETTERS)
            if (input && typeof item[elem] == 'undefined') {
                if (input.tagName == 'SELECT' && $(input).hasClass('cleanup')) {
                    $(input).empty();
                } else {
                    $(input).val('');
                    $(input).attr('checked',false);
                }
            }
			if (input) {
				input.value = item[elem] || '';
				//                $(input).change().example(function() { return $(this).attr('title') });
			}
			var span = document.getElementById(prefix + '_lbl' + elem); // Dont use jQuery, because it does not work with special chars like in the prefix (#RELATIE!VOORLETTERS)
			if (span) {
				span.innerHTML = (span.title || '') + (item[elem] || '&nbsp;');
			}
		});
	},
	isNumber: function(x) {
		return ((typeof x === typeof 1) && (null !== x) && isFinite(x));
	},
	isInteger: function(val) {
		return (val == null || isNaN(val)) ? false : (((1.0 * val) == Math.floor(val)) && (val.indexOf(".") == -1));
	},
	isFloat: function(val) {
		//        if (Macs.SharedLib.isNumber(val)) return true;
		//        if (isNaN(val)) return false;
		var n = val.replace(/^\s+|\s+$/g, "");
		return n.length > 0 && !(/[+-][^0-9.]/).test(n) && (/\.\d/).test(n);
	},
	floatValue: function(str) {
		str = str || "0";
		str = (str + '').replace(',', '.');
		return parseFloat(str);
	},
	formatFloat: function(val, decs, decsep) {
	    decsep = decsep || ',';
        if (decsep == '.')
      		val = (val + '').replace(',', '.');

	    if (typeof val == 'undefined' || val == null || isNaN(val) || val == '' ) return '';

		decs = decs || 2;

  		val = (val + '').replace(',', '.');
		if (Macs.SharedLib.isFloat(val) || Macs.SharedLib.isInteger(val)) {
			var value = parseFloat(val).toFixed(decs);
            if (decsep == ',') {
	    		return value.replace('.', ',')
            } else {
	    		return value.replace('.', ',')
	    	}
		} else {
			return 'NaN';
		}
	},
	ajaxFeedback: function(message) {
		$('#ajaxfeedback').html("<span>" + message + "</span>");
		$('#ajaxfeedback').fadeIn(300, function() {
			$(this).fadeOut(2000);
		});
	},
	laadParameters: function() {
		var url = Macs.SharedLib._macs_servicequery.replace("%construct%", "parameters").replace("const", "form");
		$.getJSON(url, {
			limit: 1
		},
		function(data, textStatus) {
			if (textStatus == 'success') {
				Macs.SharedLib.parameters = data[0];
			} else {
				alert('The parameters could not be loaded. Error: ' + textStatus);
			}
		});
	},
	zoekCursus: function(cursuscode, callback) {
		var url = Macs.SharedLib._macs_servicequery.replace("%construct%", "cursus");
		$.getJSON(url, {
			limit: 1,
			"qc[]": "CURSUSCODE",
			"qv[]": "'" + cursuscode + "'"
		},
		function(data, textStatus) {
			if (textStatus == 'success') {
				if (callback) callback(data);
			} else {
				alert('The search failed. Error: ' + textStatus);
			}
		});
	},
	zoekFormAdres: function(prefix) {
		Macs.SharedLib.zoekAdres($(":input[name=" + prefix + "\\!LANDCODE]").val(), $("#" + prefix + "\\!_fldPOSTCODE").val(), $("#" + prefix + "\\!_fldHUISNUMMER").val(), $("#" + prefix + "\\!_fldHUISNUMMERTOEVOEGING").val(), function(data) {
			$("#" + prefix + "\\!_fldPLAATS").val(data.PLAATS).change();
			$("#" + prefix + "\\!_fldSTRAAT").val(data.STRAAT).change();
		});
	},
	checkSpamPostcode: function(postcode) {
		if (postcode !== '') {
			$.getJSON(_servicequery, {
				func: 'checkspampostcode',
				postcode: postcode
			},
			function(data, textStatus) {
				if (textStatus == 'success') {
					if (data) {
						Polaris.Base.modalDialog('Deze postcodewijk is SPAM gevoelig. Overleg eerst met debiteuren!', {close: function() {
							//nothing
						}});
					}
				}
			});
		}
	},
	zoekAdres: function(landcode, postcode, huisnummer, huisnummertoevoeging, callback) {
		if (landcode == '' || postcode == '' || huisnummer == '') return false;

		//  Alleen nedelandse adressen worden opgezocht
		if (landcode == 'NL') {
			$("#btnZOEKADRES").hide();
			$("#btnZOEKADRES").after($("<img id=\"imgwait\" />").attr('src', _serverroot + '/images/wait.gif'));
			var zoekAdresKlaar = function() {
				$("#imgwait").remove();
				$("#btnZOEKADRES").show();
			};

			var url = Macs.SharedLib._macs_servicequery.replace("%construct%", "postcodetabelnederland").replace("const", "form") + "?output=json&limit=10" + "&qc[]=POSTCODE&qv[]=" + postcode.replace(' ', '');
    		var found = false;
			$.getJSON(url, function(data, textStatus) {
				if (textStatus == 'success') {
					huisnummer = parseInt(huisnummer, 10);
					$(data).each(function(i, elem) {
						var nrvan = parseInt(elem.HUISNRVAN, 10);
						var nrtm = parseInt(elem.HUISNRTM, 10);
						// if huisnummer is in range
						if ((huisnummer >= nrvan) && (huisnummer <= nrtm) && ((huisnummer % 2) == (nrvan % 2))) {
							// and if huisnummer is in range
							callback(elem);
							found = true;
							return false;
						} else {
							elem.STRAAT = '';
							elem.PLAATS = '';
							found = false;
						}
					});
				} else {
					alert('The search failed. Error: ' + textStatus);
				}
				zoekAdresKlaar();
				// geen adres gevonden: maak velden leeg
    			if (!found) callback({'PLAATS':'','STRAAT':''});
			});
		} else {
//			alert("Alleen Nederlandse adressen kunnen worden opgezocht.");
		};
	},
	bepaalAanhef: function(geslacht) {
		return [$.t('nha.macs.dhr'), $.t('nha.macs.mevr'), $.t('nha.macs.dhr_mevr')][
			['M', 'V', 'O'].indexOf(geslacht)];
	},
	bepaalHuidigeTijd: function(plusminuten) {
		plusminuten = plusminuten || 0;

		var d = new Date();
		d.setTime(d.getTime() + (plusminuten * 60 * 1000));
		return d.toLocaleTimeString().substr(0, 5);
	},
	bepaalHuidigeDatum: function(plusdagen) {
		d = Macs.SharedLib.bepaalHuidigeDatumObject(plusdagen);
		var day = "00" + (d.getDate());
		day = String(day).substring(day.length, day.length - 2);
		var month = "00" + (d.getMonth() + 1);
		var iLen = month.length;
        month = String(month).substring(iLen, iLen - 2);
		return day + "-" + month + "-" + d.getFullYear();
	},
	bepaalHuidigeDatumObject: function(plusdagen) {
		plusdagen = plusdagen || 0;

		var d = new Date();
		d.setTime(d.getTime() + (plusdagen * 24 * 60 * 60 * 1000));
		return d;
	},
	escapeSel: function(value) {
		return value.replaceAll('+', '\\+').replaceAll('/', '\\/');
	}
};

$(document).ready(function() {
	log('Macs.SharedLib.initializeLocalization');
	Macs.SharedLib.initializeLocalization();
});