jQuery.fn.hasValue = function(thevalue) {
	var count = 0;

	this.each(function() {
		if (typeof thevalue == 'object') {
			var elem = this;
			$.each(thevalue, function(i) {
				if ($(elem).val() == this) {
					count++;
				}
			});
		} else {
			if ($(this).val() == thevalue) {
				count++;
			}
		}
	});
	return count;
};

Macs.NieuweInschrijving = {
	betaalwijze: '',
	cursusRijTemplate: '',
	bepaalBetaalWijze: function() {
		$.getJSON(_servicequery, {
			'func': 'betaalwijze',
			'landcode': Macs.RelatieSelectieForm.huidigLandSelectie()
		},
		function(data, textStatus) {
			if (textStatus == 'success') {
				Macs.NieuweInschrijving.betaalwijze = data;
			} else {
				console.log('Geen betaalwijze gevonden: ' + textStatus);
			}
		});
	},
	onSelecteerRelatie: function(item, prefix) {
  		var scope = Macs.RelatieSelectieForm.bepaalFormScope();

        if (item.TELEFOONOVERDAG == null && item.TELEFOONSAVONDS == null && item.MOBIELNR == null) {
            Macs.RelatieSelectieForm.switchModus('edit', scope);
        }
		if ($('input.CURSUSCODE[value!=""]').length == 0) {
            if (item.TELEFOONOVERDAG == null && item.TELEFOONSAVONDS == null && item.MOBIELNR == null) {
                $('#' + prefix + '\\!_fldTELEFOONOVERDAG').focus();
            } else {
                Macs.RelatieSelectieForm.switchModus('view', scope);
                $("#_fldADVERTENTIECODE").focus();
            }
        }
	},
	vooropleidingNodig: function(cursuscodes) {
		result = true;
		if (cursuscodes.length == 1) {
			if ($.inArray('MBOXBEV', cursuscodes) >= 0 || $.inArray('PRMBAGM', cursuscodes) >= 0) {
				result = false;
			}
		}
		return result;
	},
	bewaarInschrijvingen: function() {
		// Ga er vanuit dat er geen problemen met het formulier zijn
		var formValid = true;

		var prefix = Macs.RelatieSelectieForm.relatieType();

		/***
		 * Controleer de standaard velden
		 */
		if (!$("#dataform").valid()) {
			formValid = false;
    		Macs.RelatieSelectieForm.switchModus('edit');
    		var counter = $("#dataform").validate().numberOfInvalids();
			Polaris.Base.modalMessage($.t('validation.incorrect_field', {count: counter}), 1000, false);
		}

		/***
		 * Controleer op dubbele cursussen bij de inschrijvingen
		 */
		var cursuscodes = $.map($("input.CURSUSCODE"), function(a) {
			if (a.value != '') return a.value;
		});
		if (cursuscodes.length > 1 && Macs.SharedLib.uniqueArray(cursuscodes).length != cursuscodes.length) {
			formValid = false;
			log("dubbele cursussen");
			Polaris.Base.modalMessage($.t('nha.macs.dubbeleCursussen'));
			return;
		}

		/***
		 * Controleer of cursussen zijn ingevuld
		 */
		if ($("input.CURSUSCODE[value!=]").length == 0) {
			formValid = false;
			log("geen cursussen");
			Polaris.Base.modalMessage($.t('nha.macs.geenCursussen'));
			return;
		}

		/***
		 * Controleer of er een relatie is geselecteerd
		 */
		if ($('input[name=RELATIE\\!_hdnState]').val() == '') {
			formValid = false;
			log("geen relatie geselecteerd");
			Polaris.Base.modalMessage($.t('nha.macs.geenRelatieGeselecteerd'));
			return;
		}


		/***
		 * Controleer of er minimaal een van de vier MBO/HBO/Master opties zijn aangevinkt
		 */
		if ($(':input.HEEFTVOOROPLEIDING:visible').length > 0
		&& !$(':input.HEEFTVOOROPLEIDING:visible').prop('checked')
		&& !$(':input.VRIJSTELLINGDIPLOMA:visible').prop('checked')
		&& !$(':input.VRIJSTELLINGWERKERVARING:visible').prop('checked')
		&& !$(':input.VERKLARING21JAAROFOUDER:visible').prop('checked')) {
			formValid = false;
			Polaris.Base.modalMessage($.t('nha.macs.geenMBOHBOoptieAangevinkt'));
			return;
		}

		/***
		 * Controleer of er de vooropleidingen zijn ingevuld bij MBO/HBO/MBAGM
		 */
		if ($(':input.HEEFTVOOROPLEIDING').prop('checked')
		&& !$(':input.VRIJSTELLINGDIPLOMA').prop('checked')
		&& !$(':input.VRIJSTELLINGWERKERVARING').prop('checked')
		&& !$(':input.VERKLARING21JAAROFOUDER').prop('checked')
		&& $("#vooroplblok input:checked").length == 0) {
			if (Macs.NieuweInschrijving.vooropleidingNodig(cursuscodes)) {
				formValid = false;
				Polaris.Base.modalMessage($.t('nha.macs.geenVooropleiding'));
				Polaris.Visual.showTab('#pagetabs', '#page_vooropleiding');
				return;
			}
		}

		// indien er checkboxes zijn bij inschrijvingen (via TM), dan moet er minimaal 1 aangevinkt zijn
		if ($(":checkbox.AANGEVINK").length > 0) {
		    if ($(":checkbox.AANGEVINK:checked").length == 0) {
    			formValid = false;
    			Polaris.Base.modalMessage($.t('nha.macs.minimaalEenInschrijvingAanvinken'));
    			return;
		    }
		}

		// Kortingpercentage
		if ($(":input.KORTINGSPERCENTAGEx").length > 0) {
		    $(":input.KORTINGSPERCENTAGEx").each(function(i) {
		        $(this).val($.trim($(this).val()));
//		        var fv = $(this).val();
		        var fv = Macs.SharedLib.formatFloat($(this).val(), 1);
		        alert(fv);
//		        var fv = Macs.SharedLib.floatValue($(this).val());
		        $(this).val(fv);
		        if (isNaN(fv) || fv > 100) {
                    formValid = false;
                    $(this).focus();
                    Polaris.Base.modalMessage($.t('nha.macs.kortingPercentageMoetFloatZijn'));
		            return false;
		        }
		    });
		}

		/***
		 * Controleer of het scherm "Artikelen" al is bekeken
		 */
		var cursusmetartikelen = $.map(Macs.NieuweInschrijving.bepaalCursussen(), function(elem, i) {
			return ($(elem).data('cursusgegevens') && $(elem).data('cursusgegevens').artikelen && $(elem).data('cursusgegevens').artikelen.length > 0);
		});
		if (cursusmetartikelen[0] == true && !Macs.ArtikelVerkoopFormulier.schermBezocht) {
			log("artikelen bestellen");
			formValid = false;
			Macs.NieuweInschrijving.toonArtikelFormulier();
		}

		/***
		 * Toon een melding wanneer cursuslessen niet op voorraad zijn

        EVEN GEEN VOORRAAD meldingen, omdat er geen voorraad wordt bijgehouden

		var $om = $("#_fldONTVANGSTMEDIUM");
		if ($om.val() == 'T' || $om.val() == 'TM') {
			var msg = '';
			$(".VOORRAAD.welvoorraad").each(function() {
				msg = msg + $(this).attr('title');
			});
			if (msg != '') Polaris.Base.modalDialog(msg);
		}
    	*/

// $('#dataform')[0].submit();
// return;
		if (formValid) {
			Polaris.Base.modalMessage($.t('nha.macs.inschrijvingWordtOpgeslagen'), 0);
			jQuery.postJSON(_servicequery, $('#dataform'), function(data, textStatus) {
				Polaris.Base.closeModalMessage();
				if (textStatus == 'success') {
					log('Nieuwe inschrijving opgeslagen');
					if (data.result != null && data.result != false) {
						Macs.SharedLib.ajaxFeedback($.t('plr.changes_saved'));
						Macs.NieuweInschrijving.toonResultaatFormulier(data);

                        // ingeval van TM oproep, dan frame weghalen bij Bewaren
                        var iframe = parent.document.getElementById("inschrijvingframe");
                        if ($(iframe).length > 0) {
                            if (window.parent.Macs) {
                                window.parent.Macs.Telemarketing.bewaarGeplandeActie();
                            }
                        }

					} else {
						Polaris.Base.errorDialog(data.error, data.detailed_error);
					}
				} else {
					Macs.SharedLib.ajaxFeedback('JSON Post error');
				}
			});
		}
	},
	annuleerForm: function() {
		Polaris.Form.userIsEditing = false;

		/* Maak de velden leeg */
		Macs.RelatieSelectieForm.maakLeeg('RELATIE!');
		Macs.RelatieSelectieForm.wisFactuurRelatieForm();

		Macs.ArtikelVerkoopFormulier.schermBezocht = false;

		$('#RELATIE_hdnRecordID').val('');
		$('#FACTUURRELATIE_hdnRecordID').val('');

		/* Verwijder alle inschrijvingregels behalve de eerste (0) */
		$("#nieuwe_inschrijving_cursusselectie .componentitem:gt(0)").remove();
		Macs.NieuweInschrijving.maakCursusGegevensLeeg($("#nieuwe_inschrijving_cursusselectie .componentitem:eq(0)"));

		/* Verwijder alle artikelregels */
		$("#nieuwe_inschrijving_artikelen .componentitem:gt(0)").remove();
		Macs.NieuweInschrijving.maakArtikelGegevensLeeg($("#nieuwe_inschrijving_artikelen .componentitem:eq(0)"));

		Macs.RelatieSelectieForm.switchModus('view');

		/* MBOHBO resetten */
		Macs.NieuweInschrijving.resetMBOHBOVelden();
		$("#vooroplblok input").prop('checked', false);

		// advertentiecode standaard op SYS
		$("#_fldADVERTENTIECODE").val('SYS');

		$("#_fldZOEKLAND").val(Macs.RelatieSelectieForm.huidigLandSelectie());
		Macs.RelatieSelectieForm.resetLandSelectie();

		$("#_fldNIEUWSBRIEF").prop('checked', false);

		$("#_fldBANKGIROREKENING").val('');

		Macs.NieuweInschrijving.updateBetaalwijze(false);
		$("#_fldONDERTEKEND").prop('checked', false);
		$("input.CURSUSCODE").val('');

		$("#tab_relatie").click();

		$("input.as_showfield").val('');
		$("input.as_showfield:first").focus();

        // ingeval van TM oproep, dan frame weghalen bij Annuleren
        var iframe = parent.document.getElementById("inschrijvingframe");
        if ($(iframe).length > 0) {
            $(iframe).remove();
        }
	},
	bepaalCursussen: function() {
		return $("#nieuwe_inschrijving_cursusselectie tbody tr.componentitem");
	},
	updateTermijnBedragen: function() {
		Macs.NieuweInschrijving.bepaalCursussen().each(function() {
			Macs.NieuweInschrijving.bepaalTermijnBedrag(this);
		});
	},
	updateBetaalwijze: function(onoff) {
	    var bankGiroVerplicht = false;
	    if (typeof onoff != 'undefined')
	        bankGiroVerplicht = onoff;
	    else
		    bankGiroVerplicht = ($(".BETAALWIJZE").hasValue(['AUT_1', 'AUT_15']) > 0) || ($("#_fldBETAALWIJZECODE").val() == 'AUT');
		$("#_fldBANKGIROREKENING").toggleClass('required', bankGiroVerplicht).removeClass('valid');
		//        .attr('required', bankGiroVerplicht?'required':'');
	},
	updateLesVoorraad: function() {
		var cursusrij = $(this).parents("tr"); // neem de TR van het betreffende input field
		var studietempo = this.value;
		var specialtooltip = false;

		var cursus = cursusrij.data('cursus');
		var cursusgegevens = cursusrij.data('cursusgegevens');
		var $voorraad = cursusrij.find(".VOORRAAD").text('');
		var msg = '';
		if (cursusgegevens.lesvoorraad[studietempo]) {
			var aantalopvoorraad = function() {
				var count = 0;
				$.each(cursusgegevens.lesvoorraad[studietempo], function(i) {
					if (parseInt(this[1], 10) > 0) {
						count++;
					}
				});
				return count;
			};
			var aantal = cursusgegevens.lesvoorraad[studietempo].length;
			var aov = aantalopvoorraad();

			if (aov == 0) {
				msg = $.t('nha.macs.cursusAlleLessenGeenVoorraad', {'cursus':cursus.CURSUSCODE});
			} else if (aov < aantal) {
				lessengeenvoorraad = '';
				$.each(cursusgegevens.lesvoorraad[studietempo], function(i) {
					if (this[1] <= 0) lessengeenvoorraad = lessengeenvoorraad + this[0] + ',';
				});
				msg = $.t('nha.macs.cursusSommigeLessenGeenVoorraad', {cursus:cursus.CURSUSCODE, lessen: lessengeenvoorraad});
			} else {
				msg = $.t('nha.macs.cursusAlleLessenOpVoorraad', {cursus: cursus.CURSUSCODE});
			}
		}

		if (cursus.MATERIAALCOMPLEETJANEE != 'J') {
		    msg = $.t('nha.macs.materiaalIncompleet', {datum: (cursus.MATERIAALVERWACHTDATUM || $.t('nha.macs.binnenkort'))});
		    specialtooltip = true;
		    var aantal = 1;
		    var aov = 0;
		}
        if (specialtooltip && aov < aantal) {
    		$voorraad.append('<span class="ui-tooltip" style="position:absolute;margin-left:-127px;top:34px">'+msg.replace("\n",'<br/>')+'</span>');
    		$(".ui-tooltip",cursusrij).delay(3000).fadeOut(500);
		}
		$voorraad.toggleClass('fa-exclamation', (aov < aantal)).toggleClass('fa-check', (aov == aantal)).attr('title', msg);
	},
	bepaalTermijnBedrag: function(elem) {
		elem = $(elem);
		if (elem[0].tagName != 'TR') elem = elem.parents("tr");
		var belgisch = elem.find(":input.CURSUSCODE").val()[0] == 'X';
		var $zelfstudie = elem.find(":input.ZELFSTUDIEJANEE");
		var zelfstudie = $zelfstudie.prop('checked') ? 'J' : 'N';
		var $betaaltermijnen = elem.find(":input.AANTALBETAALTERMIJNEN");
		var $termijnbedrag = elem.find("span.TERMIJNBEDRAG");
		var $korting = elem.find(":input.KORTINGSPERCENTAGE");

		var betaaltempos = $(elem).data("betaaltempos");
		var termijnbedrag = 0;

		if (betaaltempos) {
			var iszelfstudieaanwezig = false;
			$.each(betaaltempos, function(i, val) {
				if (zelfstudie == val.ZELFSTUDIEJANEE) {
					iszelfstudieaanwezig = true;
					if ($betaaltermijnen.val() == val.AANTALTERMIJNEN) {
						if (belgisch == true) {
							termijnbedrag = val.BEDRAGINEUROS_BELGIE;
						} else {
							termijnbedrag = val.BEDRAGINEUROS;
						}
						if (termijnbedrag != null) termijnbedrag = termijnbedrag.replace(',', '.');
						return true;
					}
				}
			});
			if (parseFloat(termijnbedrag) > 0) {
			    $korting.val($korting.val().replace('.',','));
			    var kortingval = Macs.SharedLib.floatValue($korting.val());
				if (kortingval > 0) termijnbedrag = parseFloat(termijnbedrag) - (parseFloat(termijnbedrag) * (kortingval / 100));
				var termijnbedragFormat = new Number(parseFloat(termijnbedrag));
				$termijnbedrag.text(Macs.SharedLib.formatFloat(termijnbedragFormat.toFixed(2)));
			} else {
				if (!iszelfstudieaanwezig && $zelfstudie.prop('checked') != false) {
					Polaris.Base.modalMessage($.t('nha.macs.geenZelfStudieMogelijk'));
					$zelfstudie.prop('checked', false);
				}
				$termijnbedrag.text('');
			}
		}
	},
	zoekGekoppeldeWaarde: function(field, studietempobetaaltermijnen) {
		var searchfieldname = '';

		searchfieldname = 'studietempocode';
		resultfieldname = 'aantalbetaaltermijnen';

		var searchvalue = $(field).val();

		var result = false;
		$(studietempobetaaltermijnen).each(function(index, elem) {
			if (elem[searchfieldname] == searchvalue) {
				result = elem[resultfieldname];
			}
		});
		return result;
	},
	maakArtikelGegevensLeeg: function(elem) {
        regel = $(elem);
        if (regel.length > 0 && regel[0].tagName != 'TR') regel = $(elem).parents("tr"); // neem de TR van het betreffende input field
		regel.find(":input.PICKTYPECODE").val('');
		regel.find(":input.AANTAL").val('');
	},
	maakCursusGegevensLeeg: function(elem) {
        regel = $(elem);
        if (regel.length > 0 && regel[0].tagName != 'TR') regel = $(elem).parents("tr"); // neem de TR van het betreffende input field
		// maak de velden leeg
		regel.find(":input.CURSUSCODE").val('');
		regel.find(":input.ZELFSTUDIEJANEE").prop('checked', false).val('');
		regel.find(":input.AANTALBETAALTERMIJNEN").empty();
		regel.find(":input.STUDIETEMPOCODE").empty();
		var $gratisonderdelen = regel.find(":input.GRATISONDERDEEL:first").empty();
		regel.find(":input.GRATISONDERDEEL").not($gratisonderdelen).remove();
		regel.find(":input.BETAALWIJZE").empty();
		regel.find(":input.STARTDATUM").val('');
		regel.find(":input.KORTINGSPERCENTAGE").val('');
		regel.find(":input.KORTINGOMSCHRIJVING").val('');
		regel.find(":input.BEPAALINSCHRIJFGELD").prop('checked', true).val('');
		regel.find("span.TERMIJNBEDRAG").text('');
		regel.find("input.extsearch").val('');
		Macs.NieuweInschrijving.updateBetaalwijze(false);

		// Verwijder de data
		regel.removeData('cursus');
		regel.removeData('cursusgegevens');
		regel.removeData('betaaltempos');
	},
	setVooropleidingTab: function(onoff) {
		$("#tabli_vooropleiding").toggle(onoff);
	},
	resetMBOHBOVelden: function(elem) {
		var self = Macs.NieuweInschrijving;

		elem = elem || $(document);
		elem.find(":input.HEEFTVOOROPLEIDING").prop('checked', false);
		elem.find(":input.VRIJSTELLINGDIPLOMA").prop('checked', false);;
		elem.find(":input.VRIJSTELLINGWERKERVARING").prop('checked', false);;
		elem.find(":input.VERKLARING21JAAROFOUDER").prop('checked', false);;
		elem.find(":input.DIRECTINSCHRIJVING").prop('checked', false);;
	    elem.addClass('niet_mbohboinschrijving');
        self.setVooropleidingTab(false);
		Polaris.Visual.showTab('#pagetabs', '#page_relatie');
	},
	vulCursusGegevens: function(elem, cursuscode) {
		var self = Macs.NieuweInschrijving;

		elem = $(elem).parents("tr"); // neem de TR van het betreffende input field

		self.maakCursusGegevensLeeg(elem);

		if (typeof cursuscode == 'undefined') return;

		// maak de velden leeg
		var $cursuscode = elem.find(":input.CURSUSCODE");
		var $zelfstudie = elem.find(":input.ZELFSTUDIEJANEE");
		var $studietempo = elem.find(":input.STUDIETEMPOCODE");
		var $betaaltermijnen = elem.find(":input.AANTALBETAALTERMIJNEN");
		var $gratisonderdelen = elem.find(":input.GRATISONDERDEEL:first");
		elem.find(":input.GRATISONDERDEEL").not($gratisonderdelen).remove();
		var $betaalwijze = elem.find(":input.BETAALWIJZE");
		var $startdatum = elem.find(":input.STARTDATUM");
		var $korting = elem.find(":input.KORTINGSPERCENTAGE");
		var $kortingomschrijving = elem.find(":input.KORTINGOMSCHRIJVING");
		var $inschrijfgeldbepalen = elem.find(":input.BEPAALINSCHRIJFGELD");
		var $termijnbedrag = elem.find("span.TERMIJNBEDRAG");
		var $aangevink = elem.find(":input.AANGEVINK");
		var $heeftvooropleiding = elem.find(":input.HEEFTVOOROPLEIDING");
		var $vrijstdiplomas = elem.find(":input.VRIJSTELLINGDIPLOMA");
		var $vrijstwerkervaring = elem.find(":input.VRIJSTELLINGWERKERVARING");
		var $verklaring21jaar = elem.find(":input.VERKLARING21JAAROFOUDER");
		var $directinschrijving = elem.find(":input.DIRECTINSCHRIJVING");

		$aangevink.attr('value', cursuscode);
		$zelfstudie.attr('value', cursuscode);
		$inschrijfgeldbepalen.attr('value', cursuscode);
		$heeftvooropleiding.attr('value', cursuscode);
		$vrijstdiplomas.attr('value', cursuscode);
		$vrijstwerkervaring.attr('value', cursuscode);
		$verklaring21jaar.attr('value', cursuscode);
		$directinschrijving.attr('value', cursuscode);

		// Geef aan Polaris door dat de gebruiker het formulier aan het invullen is, zodat
		// er een extra bevestiging wordt gevraagd zodra de gebruiker een ander formulier kiest
		Polaris.Form.userIsEditing = true;

        var vulBetaalTermijnen = function($betaaltermijnen, betaaltempos, zelfstudie, defaultaantalbetaaltermijnen) {
            var uniekebetaaltermijnen = [];

            var aantalbetaaltermijnen = $betaaltermijnen.val();
            if (typeof aantalbetaaltermijnen == 'undefined' || aantalbetaaltermijnen == 0) aantalbetaaltermijnen = defaultaantalbetaaltermijnen || 4;

            $betaaltermijnen.empty();
            var defaultgevonden = false;
            $.each(betaaltempos, function(i, val) {
                if ((zelfstudie == val.ZELFSTUDIEJANEE) && $.inArray(val.AANTALTERMIJNEN, uniekebetaaltermijnen) == -1) {
                    uniekebetaaltermijnen.push(val.AANTALTERMIJNEN);
                    if (aantalbetaaltermijnen == val.AANTALTERMIJNEN) defaultgevonden = true;
                }
            });

            if (!defaultgevonden) aantalbetaaltermijnen = 4;
            $.each(uniekebetaaltermijnen, function(i, val) {
                $betaaltermijnen.append(new Option(val, val, false
                /*defaultSelected*/
                , aantalbetaaltermijnen == parseInt(val, 10)));
            });
    		$betaaltermijnen.change();
        }

		var bepaalCursusGegevens = function(cursuscode, zelfstudie, landcode) {
			if ((typeof cursuscode == 'undefined') || (cursuscode == '')) {
				// niks
			} else {
                // Maak een callstack zodat we meerdere ajax calls parallel(!) kunnen uitvoeren
                var Stack = new Callstack();

                // Wat gaan we doen wanneer alle ajax calls klaar zijn met uitvoeren?
                Stack.onComplete = function(stack) {
                    // Wat gaan we doen wanneer alle ajax calls klaar zijn met uitvoeren?
                    var betaaltempos = stack.betaaltempos.arguments[0];
                    var cursusgegevens = stack.cursusgegevens.arguments[0];

                    $(elem).data("betaaltempos", betaaltempos);
                    $(elem).data("cursusgegevens", cursusgegevens);

                    if (cursusgegevens.result != false) {
                        var cursus = $(elem).data('cursus');

                        // ingeval de cursuscode via (oorzaaknummer van) TM wordt doorgegeven, dan de cursuscode/cursusnaam handmatig invullen
                        if ($cursuscode.val() == '') {
                            $cursuscode.val(cursus.CURSUSCODE);
                            elem.find(":input.as_showfield").val(cursus.CURSUSCODE + ' ' + cursus.CURSUSNAAM);
                        }

                        /**
                         * Zoek de betaaltermijnen en zet de unieke termijnen in de dropdown select
                         */
                        var defaultaantalbetaaltermijnen = cursus.STANDAARDBETAALTEMPOTERMIJNEN;
                        $zelfstudie.change(function() {
                            var zelfstudie = $(this).prop('checked')?'J':'N';
                            vulBetaalTermijnen($betaaltermijnen, betaaltempos, zelfstudie, defaultaantalbetaaltermijnen);
                        });
                        $zelfstudie.change();
                        // er zijn geen studietempo's, dus "geblokkeerd": rood veld
                        $betaaltermijnen.css('background-color', $betaaltermijnen.find('option').length == 0?'red':'');

                        /**
                         * Vul de Studietempo select dropdown
                         */
                        $studietempo.empty();
                        $.each(cursusgegevens.studietempos, function(i, val) {
                            $studietempo.append(new Option(val[1], val[0], false
                            /*defaultSelected*/
                            , cursus.STANDAARDSTUDIETEMPO == val[0]));
                        });
                        // er zijn geen studietempo's, dus "geblokkeerd": rood veld
                        $studietempo.css('background-color', $studietempo.find('option').length == 0?'red':'');

                        $studietempo.change(function(e) {
                            $betaaltermijnen.val(
                            Macs.NieuweInschrijving.zoekGekoppeldeWaarde($studietempo, cursusgegevens.studietempobetaaltermijnen) ||
                            /* just in case er geen studtempbetter zijn */
                            defaultaantalbetaaltermijnen);
                            $betaaltermijnen.change();
                        });
                        $studietempo.change(); // trigger het change event

                        /**
                         * Vul de Gratisonderdelen select dropdowns
                         */
                        $gratisonderdelen.empty();
                        var $_currentdropdown = $gratisonderdelen;
                        var index = 0;
                        for (var i in cursusgegevens.gratisonderdelen) {
                            if (index > 0) {
                                $_currentdropdown = $_currentdropdown.clone().empty();
                                $gratisonderdelen.after($_currentdropdown);
                                $_currentdropdown.before("<br />");
                            }

                            $_group = $(document.createElement("optgroup")).attr('label', i);
                            $_currentdropdown.append($_group);

                            // add empty option
                            $_group.append(new Option('', ''));
                            var _cursusstudiegidscode = elem.find(":input.CURSUSCODE").val();
                            $.each(cursusgegevens.gratisonderdelen[i], function(i, val) {
                                $_group.append(new Option(val[0] + " | " + val[1], _cursusstudiegidscode + '!' + val[0] + '!' + val[1]));
                            });
                            index++;
                        }

                        /**
                         * Vul de Betaalwijze select dropdown
                         */
                        $betaalwijze.empty();
                        $.each(self.betaalwijze, function(i, val) {
                            $betaalwijze.append(new Option(val[1], val[0]));
                        });

                        /**
                         * Zet de datum van vandaag in Startdatum
                         */
                        var d = new Date();
                        $startdatum.val(d.getDate() + '-' + (d.getMonth() + 1) + '-' + d.getFullYear());

                        /**
                         * Bereken het Termijnbedrag
                         */
                        Macs.NieuweInschrijving.bepaalTermijnBedrag(elem);

                        /**
                         * Duitse inschrijvingen hebben geen inschrijfgeld, dus uitzetten
                         */
                        if (cursuscode[0] == 'Q') {
                            $inschrijfgeldbepalen.prop('checked', false).attr('disabled', true);
                        } else {
                            $inschrijfgeldbepalen.attr('disabled', false);
                        }

                        /**
                         * Toon de gewenste checkboxes ivm HBO/MBO
                         */
                        elem.removeClass('mbo2inschrijving');
                        elem.removeClass('mbo34inschrijving');
                        elem.removeClass('hboinschrijving');
                        elem.removeClass('niet_mbohboinschrijving');
                        elem.removeClass('directinschrijven');
                        if (cursusgegevens.inschrijvingenblokkeren == 'J') {
                            if (cursusgegevens.cursusgroep == 'HBO') {
                                elem.addClass('hboinschrijving');
                            } else {
                                if (cursusgegevens.mbohboniveau == '2')
                                    elem.addClass('mbo2inschrijving');
                                else
                                    elem.addClass('mbo34inschrijving');
                            }
                            if ($(":input[name=_hdnISBOGROEP]").val() == 'J') {
                                elem.addClass('directinschrijven');
                            }

                            if (cursus.CURSUSCODE == 'HBO1OPC') // Hbo psychosociaal counselor jr.1: geen vrijst obv werkervaring
                                elem.find(":input.VRIJSTELLINGWERKERVARING:visible").attr('disabled', true);
                            else
                                elem.find(":input.VRIJSTELLINGWERKERVARING:visible").removeAttr('disabled');


                            $heeftvooropleiding.click(function() {
                                self.setVooropleidingTab($(this).prop('checked'));
                            });
                            elem.find(":input.HEEFTVOOROPLEIDING:visible").prop('checked', true);
                            self.setVooropleidingTab(elem.find(":input.HEEFTVOOROPLEIDING:visible").prop('checked'));
                            elem.find(":input.VRIJSTELLINGDIPLOMA:visible").prop('checked', false);
                            elem.find(":input.VRIJSTELLINGWERKERVARING:visible").prop('checked', false);
                            elem.find(":input.VERKLARING21JAAROFOUDER:visible").prop('checked', false);

                            elem.find(":input.VRIJSTELLINGDIPLOMA:visible,:input.VRIJSTELLINGWERKERVARING:visible").click(function() {
                                if (elem.find(":input.VRIJSTELLINGDIPLOMA").attr('checked')
//                                   || elem.find(":input.VRIJSTELLINGWERKERVARING").attr('checked')
                                ) {
                                    elem.find(":input.HEEFTVOOROPLEIDING:visible").attr('checked', false);
                                }
                                if (elem.find(":input.VRIJSTELLINGDIPLOMA:visible").attr('checked')) {
                                    elem.find(":input.VERKLARING21JAAROFOUDER:visible").attr('checked', false);
                                }
                            });
                            elem.find(":input.VRIJSTELLINGDIPLOMA:visible,:input.VRIJSTELLINGWERKERVARING:visible,:input.VERKLARING21JAAROFOUDER:visible,:input.HEEFTVOOROPLEIDING:visible").click(function() {
                                var total = 0;
                                elem.find(":input.VRIJSTELLINGDIPLOMA:visible,:input.VRIJSTELLINGWERKERVARING:visible,:input.VERKLARING21JAAROFOUDER:visible").each(function(i, elem) {
                                    if ($(elem).attr('checked')) {
                                        total++;
                                    }
                                });
                                if (total == 0) {
                                    //elem.find(":input.HEEFTVOOROPLEIDING:visible").attr('checked', true);
                                }
                            });
                            elem.find(":input.VERKLARING21JAAROFOUDER:visible").click(function() {
                                if ($(this).attr('checked')) {
                                    //elem.find(":input.HEEFTVOOROPLEIDING:visible").attr('checked', false);
                                    elem.find(":input.VRIJSTELLINGDIPLOMA:visible").attr('checked', false);
                                }
                            });
                            elem.find(":input.DIRECTINSCHRIJVING:visible").click(function() {
                                if ($(this).attr('checked')) {
                                    //elem.find(":input.HEEFTVOOROPLEIDING").attr('checked', false);
                                    elem.find(":input.VRIJSTELLINGDIPLOMA:visible").attr('checked', false);
                                    elem.find(":input.VRIJSTELLINGWERKERVARING:visible").attr('checked', false);
                                    elem.find(":input.VERKLARING21JAAROFOUDER:visible").attr('checked', false);
                                } else {
                                    elem.find(":input.HEEFTVOOROPLEIDING").attr('checked', true);
                                }
                            });

                        } else {
                            self.resetMBOHBOVelden(elem);
                        }

                        /**
                         * Als er artikelen zijn, zet dan de Artikelen knop aan
                         */
                        var cursusmetartikelen = $.map(Macs.NieuweInschrijving.bepaalCursussen(), function(elem, i) {
                            return ($(elem).data('cursusgegevens') && $(elem).data('cursusgegevens').artikelen && $(elem).data('cursusgegevens').artikelen.length > 0);
                        });
                        $("#btnARTIKELEN").toggleClass('disabled', cursusmetartikelen[0] == false);

                        if (elem.find(".STUDIETEMPOCODE OPTION").length == 1) elem.find(".STUDIETEMPOCODE").attr('tabindex', '-1');

                        // zet focus op het juiste veld
                        if (elem.find(".ZELFSTUDIEJANEE:enabled").length > 0) elem.find(".ZELFSTUDIEJANEE").focus();
                        else if (elem.find(".STUDIETEMPOCODE:enabled").length > 0 && elem.find(".STUDIETEMPOCODE:enabled OPTION").length > 1) elem.find(".STUDIETEMPOCODE").focus();
                        else elem.find(".BETAALWIJZE").focus();
                    }
                };

                var url = Macs.SharedLib._macs_servicequery.replace("%construct%", "cursusbetaaltempotermijn").replace('/const/','/form/');
                $.getJSON(url, {
                    "qc[]": "CURSUSCODE",
                    "qv[]": "'" + cursuscode + "'"
                }, Stack.add('betaaltempos'));
                $.getJSON(_servicequery, {
                    'func': 'cursusgegevens',
                    'cursuscode': cursuscode
                }, Stack.add('cursusgegevens'));

			}
		};

		if (!isUndefined(cursuscode)) {
			// Update het termijn bedrag wanneer de cursus gegevens wijzigen
			$betaaltermijnen.add($zelfstudie).change(function(event) {
				Macs.NieuweInschrijving.bepaalTermijnBedrag(event.target);
			});
			$korting.keyup(function(event) {
				Macs.NieuweInschrijving.bepaalTermijnBedrag(event.target);
//    		    $kortingomschrijving.attr('readonly', this.value == '');
			});

			Macs.SharedLib.zoekCursus(cursuscode, function(data) {
				// cursus JSON record vastleggen bij INPUT veld van cursuscode
				var cursus = data[0];
				$(elem).data('cursus', cursus);

				// Zelfstudie is actief afhankelijk van of het met of zonder begeleiding is
				$zelfstudie.prop('disabled', cursus.BEGELEIDINGMETOFZONDER != 'MZ');
				$zelfstudie.prop('checked', cursus.BEGELEIDINGMETOFZONDER == 'Z');

				// haal de rest van de cursus gegevens op (dropdown waarden)
				bepaalCursusGegevens(cursuscode, $zelfstudie.prop('checked') == true ? 'J' : 'N', Macs.RelatieSelectieForm.huidigLandSelectie());
			});
		}
	},
	refreshCursusRijen: function() {
		var aantal = $("#nieuwe_inschrijving_cursusselectie tbody:first tr.componentitem").length;
		$("#nieuwe_inschrijving_cursusselectie tbody:first tr.componentitem").remove();
		Macs.NieuweInschrijving.cursusRijToevoegen();
	},
	updateArtikelenButton: function(cursusmetartikelen) {},
	cursusRijToevoegen: function(cursus) {
		if ($("#nieuwe_inschrijving_cursusselectie tbody:first tr.componentitem").length < 15) {
			var aantalrijen = $("#nieuwe_inschrijving_cursusselectie tbody:first tr.componentitem").length;
			var rij = Macs.NieuweInschrijving.cursusRijTemplate.clone(true);

			// tabindex ophogen zodat er per regel getabbed kan worden
			rij.find(":input").each(function() {
				if ($(this).attr('tabindex') > 0) $(this).attr('tabindex', parseInt($(this).attr('tabindex'), 10) + 15 * aantalrijen);
			});

			// Rij toevoegen
			$("#nieuwe_inschrijving_cursusselectie tbody:first").append(rij);

			var zoekveld = $("input.extsearch", rij);

			// werkt alleen met een 'echte' event, omdat hij getriggerd wordt vanuit een ander frame
			zoekveld[0].onchange = function(event) {
				Macs.NieuweInschrijving.vulCursusGegevens(this, this.value);
			};

			Macs.RelatieSelectieForm.updateAjaxAttributes();
		    $("input[name=_hdnLandSelectie]:checked").change();

			// Event handlers toevoegen (autosuggest, date input en tiptip)
			Polaris.Form.setAutoSuggest3(zoekveld, {
				toUpperCase: true,
				onItemSelect: Macs.NieuweInschrijving.vulCursusGegevens
			});

			$("input.date_input", rij).date_input();

			// via TM doorgegeven cursus invullen
			if (typeof cursus == 'string' && cursus != '') Macs.NieuweInschrijving.vulCursusGegevens(zoekveld, cursus);

            // zorg ervoor dat rijen expliciet verwijderd kunnen worden
			$(".REMOVEROW", rij).click(function() {
			    var rij = $(this).parents('TR');
			    rij.css('background-color', 'red');
			    if ($("input.CURSUSCODE", rij).val() == '' || confirm($.t('nha.macs.bevestigingVerwijderCursus'))) {
    			    $(this).parents('TR').remove();
    			} else {
    			    rij.css('background-color', '');
    			}
			});

			// Focus zetten op cursus veld
			$("#nieuwe_inschrijving_cursusselectie input.as_showfield").focus();

		} else {
			if (typeof cursus == 'event') $(cursus.target).attr('disabled', 'true');
		}
	},
	toonArtikelFormulier: function() {
		var cursusseninfo = $.map(Macs.NieuweInschrijving.bepaalCursussen(), function(elm, i) {
			if ((typeof $(elm).data('cursusgegevens') == 'undefined') || ($(elm).data('cursusgegevens') == null) || $(elm).data('cursusgegevens').artikelen.length == 0) return false;
			var cs = $(elm).data('cursus');
			if (!cs) return false;
			var c = {
				"cursuscode": cs.CURSUSCODE,
				"cursusnaam": cs.CURSUSNAAM,
				"artikelen": $(elm).data('cursusgegevens').artikelen
			};
			return c;
		});
		Macs.ArtikelVerkoopFormulier.show(cursusseninfo);
	},
	toonResultaatFormulier: function(resultaatinfo) {
		Macs.InschrijvingResultaat.show(resultaatinfo);
	},
	controleerIBAN: function() {
		if ($("#_fldBANKGIROREKENING").val().length < 11) {
		    var err = CheckBankRekeningNr($("#_fldBANKGIROREKENING").val());
		} else {
		    var err = CheckIBAN($("#_fldBANKGIROREKENING").val());
		}
	    log(err);
	    if (err) {
            Polaris.Base.modalMessage(err, 2000, false);
	    }
	},
	updateLandCode: function() {
		var landcode = Macs.RelatieSelectieForm.huidigLandSelectie();
		//Macs.NieuweInschrijving.updateTermijnBedragen();
		$("#_fldBANKGIROREKENING").val('').blur(Macs.NieuweInschrijving.controleerIBAN);
		$("#_fldZOEKLAND").val(landcode);
    	if (landcode == 'BE') {
		    $("#bankgiro_label").text('IBAN Code');
    	} else {
	    	$("#bankgiro_label").text($.t('nha.macs.bankgiro'));
    	}
	}
};

Macs.NieuweInschrijving.bepaalBetaalWijze(); // Laad de betaalwijze in een variable, en begin hier meteen mee (niet in de ready() function)
Macs.SharedLib.laadParameters();

$(document).ready(function() {
	Macs.RelatieSelectieForm.initLandSelectie();
	/* Form actions */
	$('#dataform').submit(function(e) {
		e.preventDefault();
	});
	$('#btnSAVE').click(function(e) {
		e.preventDefault();
    	if (!$(this).hasClass('disabled')) Macs.NieuweInschrijving.bewaarInschrijvingen();
	});
	$('#btnCANCEL').click(function(e) {
		e.preventDefault();

		Macs.NieuweInschrijving.annuleerForm();
		if (Polaris.Base.getLocationVariable('maximize') == 'true') {
			window.close();
		}
	});
	$('#btnARTIKELEN').click(function(e) {
		e.preventDefault();
		Macs.NieuweInschrijving.toonArtikelFormulier();
	});

	$("#formview").bind('selecteerrelatie', function(event, item, prefix) {
		Macs.NieuweInschrijving.onSelecteerRelatie(item, prefix);
	});

	$("#_fldONTVANGSTMEDIUM").change(function() {
	    sessionStorage.ontvangstmedium = $(this).val();
	});

    $('.gebdatum').change(function() {
        if (!Polaris.Base.isMinimumAge($(this).val(), 18)) {
            Polaris.Base.modalMessage($.t('nha.macs.minimumLeeftijdRelatie'), 2500);
        }
    });

	$('#dataform .BETAALWIJZE').change(Macs.NieuweInschrijving.updateBetaalwijze);
	$('#dataform .STUDIETEMPOCODE').change(Macs.NieuweInschrijving.updateLesVoorraad);

	// Bewaar een niet-ingevulde rij als template rij
	var tmpRij = $("#nieuwe_inschrijving_cursusselectie .componentitem:first");
	Macs.NieuweInschrijving.cursusRijTemplate = tmpRij.clone(true);
	tmpRij.remove();

	// Knop 'Extra' koppelen aan event handler
	$("#cursusrijtoevoegen").click(Macs.NieuweInschrijving.cursusRijToevoegen);

	if ($("#_fldZOEKRELATIE").val() == '') {
		// Initialiseer het formulier (alsof er geannuleerd wordt)
		Macs.NieuweInschrijving.annuleerForm();
	}

	// inschrijving via TM
	var verzoeknr = Polaris.Base.getLocationVariable('oorzaaknr');
	if (typeof verzoeknr != 'undefined') {

	    $("#divRELATIEOPMERKING").show();

	    $("#_btnRELATIEOPMERKING").click(function(e) {
	        $("#opmerkingForm").jqm({modal:true}).jqmShow();
	    });

		var url = Macs.SharedLib._macs_servicequery.replace("%construct%", "verzoekcursusinfo").replace("const", "form");
		url = url + "?qc[]=BELLENNAGEENREACTIEJANEE&qv[]='J'&qc[]=GEBLOKKEERDJANEE&qv[]='N'&qc[]=STUDIEGIDSJANEE&qv[]='N'&qc[]=VERZOEKNR&qv[]='" + verzoeknr + "'";
		$.getJSON(url,
		function(data, textStatus) {
			if (textStatus == 'success') {
                if (data.length == 0) {
                    Macs.NieuweInschrijving.cursusRijToevoegen(); // lege rij toevoegen
                } else {
                    $.each(data, function(i, row) {
                        // realtime werkt niet, dus pauze tussen elke rij-toevoeging: nog oplossen
                        //                Macs.NieuweInschrijving.cursusRijToevoegen(row.CURSUSSTUDIEGIDSCODE);
                        setTimeout(function() {
                        	log(row.CURSUSSTUDIEGIDSCODE);
                            Macs.NieuweInschrijving.cursusRijToevoegen(row.CURSUSSTUDIEGIDSCODE);
                        },
                        1000 * (i+1));
                    });
                }
			}
		});
		$("#_fldADVERTENTIECODE").val('SYS');
		$("#_fldONTVANGSTMEDIUM").val('TM');
	} else {
		/* Standaard is er een cursus rij om in te vullen*/
		Macs.NieuweInschrijving.cursusRijToevoegen();
		if (typeof sessionStorage.ontvangstmedium != 'undefined')
    		$("#_fldONTVANGSTMEDIUM").val(sessionStorage.ontvangstmedium);

	}
	Macs.NieuweInschrijving.updateLandCode();
});
