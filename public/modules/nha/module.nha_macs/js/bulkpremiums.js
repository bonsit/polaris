Macs.BulkPremium = {
    //setup before functions
    typingTimer: 0,
    doneTypingInterval: 500,

    init: function() {
        var self = Macs.BulkPremium;

        var inschrijfnr = null;
        $('.inschrijfnr').keyup(function(e){
            if (e.which == 8 || (e.which >= 32 && e.which <= 122)) {
                inschrijfnr = $(this).val();
                self.timedinschrijfnrIngevuld(this, inschrijfnr);
            }
            if ($(this).val() == '') {
                $('.status', $(this).parents('tr')).val('');
                var $row = $(this).parents('tr');
                self.toonRijMelding($row, "");
                $row.find("*").removeClass('att');
                $row.find(".leaflet").val('');
            }

        }).bind("paste", function(e){
            inschrijfnr = $(this).val();
            self.timedinschrijfnrIngevuld(this, inschrijfnr);
        });

        //on keydown, clear the countdown
        $('.inschrijfnr').keydown(function(e){
            if (e.which == 8 || (e.which >= 32 && e.which <= 122))
                clearTimeout(self.typingTimer);
        });

        $('#dataform').submit(function(e) {
            e.preventDefault();
        });
        $('#btnCANCEL').click(function(e) {
            e.preventDefault();
            Macs.BulkPremium.annuleerForm();
        });
        $('#btnSAVE').click(function(e) {
            e.preventDefault();
            if (!$(this).hasClass('disabled')) Macs.BulkPremium.bewaarLeaflets();
        });

        $('.inschrijfnr:first').focus();
    },
    toonRijMelding: function($row, melding) {
        if (!isUndefined(melding) && (melding !== '')) {
            $(".melding", $row).html("<p>"+melding+"</p>").find("p").addClass('att');
        } else {
            $(".melding p", $row).html('').removeClass('att');
        }
    },
    timedinschrijfnrIngevuld: function(elem, inschrijfnr) {
        var self = Macs.BulkPremium;
        var $row = $(elem).parents("tr");
        self.typingTimer = setTimeout(function(){ self.inschrijfnrIngevuld($row, inschrijfnr) }, self.doneTypingInterval);
    },
    inschrijfnrIngevuld: function($row, inschrijfnr) {
        var self = Macs.BulkPremium;

        if (!isUndefined(inschrijfnr) && inschrijfnr !== '') {
            $.getJSON(_servicequery, {
                'func': 'getinschrijfinfo',
                'inschrijfnr': inschrijfnr
                },
                function(data, textStatus) {
                    // cursus JSON record vastleggen bij INPUT veld van artikelcode
                    $($row).data('record', data);
                    $relinfo = $(".relatieinfo", $row);
                    $status = $(".status", $row);
                    if (isUndefined(data.ACHTERNAAM)) {
                        self.toonRijMelding($row, "Onbekende inschrijving");
                        $status.val('inschrijving_onbekend');
                    } else {
                        $relinfo.text(data.RELATIENR + ": " + data.ACHTERNAAM + " (" + data.PLAATS + ")");
                        if (data.ONTVANGENLEAFLET !== null) {
                            self.toonRijMelding($row, "Leaflet is al eerder ingevuld ("+ data.DATUMONTVANGENLEAFLET +")");
                            $status.val('leaflet_bekend');
                        } else {
                            self.toonRijMelding('');
                            if (data.LEAFLET == null) {
                                self.toonRijMelding($row, "Geen premium gevonden; kies een uit lijst.");
                                $(".leaflet", $row).val('');
                                $status.val('ok');
                            } else {
                                if (data.LEAFLET.indexOf(',') !== -1) {
                                    // multiple leaflets
    //                                alert('Meerdere permiums gevonden: ' + data.LEAFLET + '. Kies een uit de lijst.');
                                    self.toonRijMelding($row, "Meerdere premiums gevonden: "+ data.LEAFLET);
                                } else {
                                    // just one leaflet
                                    $(".leaflet", $row).val(data.LEAFLET);
                                    self.toonRijMelding($row, '');
                                }
                                $status.val('ok');
                            }

                            $("#btnSAVE").removeClass('disabled');
                        }
                    }
                    Polaris.Form.userIsEditing = true;
                }
            );
        }
    },
    bewaarLeaflets: function() {
        // Ga er vanuit dat er geen problemen met het formulier zijn
        var formValid = true;

        /***
         * Controleer op dubbele cursussen bij de inschrijvingen
         */
        var inschrijfnrs = $.map($("input.inschrijfnr"), function(a) {
            if (a.value !== '')
                return a.value;
        });
        if (inschrijfnrs.length > 1 && Macs.SharedLib.uniqueArray(inschrijfnrs).length != inschrijfnrs.length) {
            formValid = false;
            log("dubbele inschrijvingen");
            Polaris.Base.modalMessage($.t('nha.macs.dubbeleInschrijvingen'));
            return;
        }

        $("input.inschrijfnr").each(function(index, elem) {
            if ($(elem).val() !== '') {
                $row = $(elem).parents('tr');
                log($row);
                if ($(".status", $row).val() == 'inschrijving_onbekend') {
                    $(".inschrijfnr", $row).addClass('att');
                } else if ($(".leaflet", $row).val() == null || $(".leaflet", $row).val() == '') {
                    $(".leaflet", $row).addClass('att');
                    formValid = false;
                } else {
                    $row.find("*").removeClass('att');
                }
            }
        });

        if (formValid) {
            //Polaris.Base.modalMessage($.t('nha.macs.leafletsWordenOpgeslagen'), 0);

            Polaris.Ajax.postJSON(_servicequery, $('#dataform'), $.t('nha.macs.leafletsWordenOpgeslagen'), $.t('plr.changes_saved'), function (data) {
                $(data.result).each(function(index, inschrijfnr) {
                    log(index);
                    log(inschrijfnr);
                    log($(".inschrijfnr:field-value(="+inschrijfnr+")"));
                    $(".inschrijfnr:field-value(="+inschrijfnr+")").parent().addClass('checked');
                });
                Polaris.Form.userIsEditing = false;
                //Macs.BulkPremium.annuleerForm(data);
            }, function(data) {
                log('fout?');
                log(data);
            });
        } else {
            Polaris.Base.modalMessage('Vul het formulier juist in...', 1000);
            return;
        }
    },
    annuleerForm: function() {
        $('.inschrijfnr').val('');
        $("#tblBulkPremium *").removeClass('att').removeClass('checked');
        $(".relatieinfo,.melding").text('');
        $(".status,.leaflet").val('');

        $('.inschrijfnr:first').focus();
    }
};

// begin meteen met het ophalen van gui ONafhankelijke informatie
Macs.SharedLib.laadParameters();

$(document).ready(function() {
    Macs.BulkPremium.init();

    Polaris.Form.userIsEditing = false;
});
