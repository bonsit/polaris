Macs.ArtikelVerkoop = {
	artikelRijTemplate: null,
	onSelecteerRelatie: function(item, prefix) {
		$("input.as_showfield:first").focus();
	},
	bewaarArtikelen: function() {
		// Ga er vanuit dat er geen problemen met het formulier zijn
		var formValid = true;

		/***
		 * Controleer de standaard velden
		 */
		if (!$("#dataform").valid()) {
			formValid = false;
    		Macs.RelatieSelectieForm.switchModus('edit');

    		var counter = $("#dataform").validate().numberOfInvalids();
			Polaris.Base.modalMessage($.t('validation.incorrect_field', {count: counter}), 1000, false);
		}

		/***
		 * Controleer op dubbele cursussen bij de inschrijvingen
		 */
		var artikelen = $.map($("input.PICKTYPECODE"), function(a) {
			if (a.value != '') return a.value;
		});
		if (artikelen.length > 1 && Macs.SharedLib.uniqueArray(artikelen).length != artikelen.length) {
			formValid = false;
			log("dubbele artikelen");
			Polaris.Base.modalMessage($.t('nha.macs.dubbeleArtikelen'));
			$("input.as_showfield:first").focus();
			return;
		}

		/***
		 * Controleer of cursussen zijn ingevuld
		 */
		if ($("input.PICKTYPECODE[value!=]").length == 0) {
			formValid = false;
			log("geen artikelen");
			Polaris.Base.modalMessage($.t('nha.macs.geenArtikelen'));
			$("input.as_showfield:first").focus();
			return;
		}

		/***
		 * Controleer of er een relatie is geselecteerd
		 */
		var reltype= Macs.RelatieSelectieForm.relatieType()
		if ($('#RELATIE_hdnRecordID').val() == '' && $("#"+reltype+"\\!_fldLANDCODE").val() != 'BE') {
			formValid = false;
			log("geen relatie geselecteerd");
			Polaris.Base.modalMessage($.t('nha.macs.geenArtikelRelatieGeselecteerd'));
			return;
		}

		if (formValid) {
			Polaris.Base.modalMessage($.t('nha.macs.artikelVerkoopWordtOpgeslagen'), 0);

            Polaris.Ajax.postJSON(_servicequery, $('#dataform'), $.t('nha.macs.artikelVerkoopWordtOpgeslagen'), $.t('plr.changes_saved'), function (data) {
				Macs.ArtikelVerkoop.toonResultaatFormulier(data);
            	Macs.ArtikelVerkoop.annuleerForm();
            }, function(data) {
            	log(data);
            });
		}
	},
	annuleerForm: function() {
		/* Maak de velden leeg */
		Macs.RelatieSelectieForm.maakLeeg('RELATIE!');
		Macs.RelatieSelectieForm.wisFactuurRelatieForm();

		$('#RELATIE_hdnRecordID').val('');
		$('#FACTUURRELATIE_hdnRecordID').val('');

		Macs.RelatieSelectieForm.switchModus('view');

		// advertentiecode standaard op SYS
		$("#_fldADVERTENTIECODE").val('SYS');
		$("#_fldZOEKLAND").val(Macs.RelatieSelectieForm.huidigLandSelectie());

		/* Verwijder alle aanvraagregels behalve de eerste twee (0 en1) */
		$cs = $("#artikelverkoop_artikelselectie");
		$(".componentitem:gt(1)", $cs).remove();
		$("input", $cs).val('');
		$("span", $cs).text('');

		Macs.RelatieSelectieForm.resetLandSelectie();

        Polaris.Form.userIsEditing = false;
	},
	toonResultaatFormulier: function(resultaatinfo) {
		Macs.ArtikelVerkoopResultaat.show(resultaatinfo);
	},
	vulArtikelGegevens: function(elem, artikelcode) {
		var self = Macs.ArtikelVerkoop;
		var item = Macs.RelatieSelectieForm.geselecteerdeRelatie();

		elem = $(elem).parents("tr"); // neem de TR van het betreffende input field
		// Verwijder de data
		elem.removeData('artikel');

		var $cursus = elem.find(":input.CURSUSCODE").empty();
		var $omschrijving = elem.find(":input.OMSCHRIJVING").empty();
		var $aantal = elem.find(":input.AANTAL");
		var $prijsperstuk = elem.find(":input.PRIJSPERSTUKINEUROS");
		var $verzendkosten = elem.find(":input.VERZENDKOSTEN");
		var $verwijderingsbijdrage = elem.find(":input.VERWIJDERINGSBIJDRAGEPERSTUK");

		if (!isUndefined(artikelcode)) {
			// Update het termijn bedrag wanneer de cursus gegevens wijzigen
			//            $betaaltermijnen.change(function(event) { Macs.ArtikelVerkoop.bepaalTermijnBedrag(event.target) });
			var cursistnr = 0;
			if (item !== null) cursistnr = item.RELATIENR;
			$.getJSON(_servicequery, {
				'func': 'artikelgegevens',
				'artikelcode': artikelcode,
				'cursistnr': cursistnr
			},
			function(data, textStatus) {
				// cursus JSON record vastleggen bij INPUT veld van artikelcode
				var artikel = data;
				$(elem).data('artikel', artikel);

				// vul de rest van de artikel velden in
				/* Geen Cursus veld
                    if (artikel.cursussen != null)
                        $.each(artikel.cursussen, function(i, val){ $cursus.append(new Option(val.cursusnaam, val.cursuscode, false )) })
                    */
				var pps = artikel.artikel.bedragineuroslos;
				if (artikel.cursussen.length > 0) pps = artikel.artikel.bedragineuros;
				$omschrijving.val(artikel.artikel.omschrijving);
				$prijsperstuk.val(Macs.SharedLib.formatFloat(pps));
				$verzendkosten.val(Macs.SharedLib.formatFloat(artikel.artikel.verzendkosten));
				$verwijderingsbijdrage.val(Macs.SharedLib.formatFloat(artikel.artikel.verwijderingsbijdrage || 0));
				$aantal.val(1).change().focus().select();

                // Geef aan Polaris door dat de gebruiker het formulier aan het invullen is, zodat
                // er een extra bevestiging wordt gevraagd zodra de gebruiker een ander formulier kiest
                Polaris.Form.userIsEditing = true;
			});
		}
	},
	updateTotalen: function() {
		var totaal = totaalverzendkosten = 0;
		$("#artikelentabel .componentitem:has(.PICKTYPECODE[value!=])").each(function(i, artikel) {
			var aantal = parseFloat($(".AANTAL", this).val().replace(',', '.'));
			var stukprijs = parseFloat($(".PRIJSPERSTUKINEUROS", this).val().replace(',', '.'));
			var verwijderingsbijdrage = parseFloat($(".VERWIJDERINGSBIJDRAGEPERSTUK", this).val().replace(',', '.')) || 0;

			// update de subtotalen
			var subtotaal = aantal * stukprijs + verwijderingsbijdrage;
			$(".SUBTOTAAL", this).text(Macs.SharedLib.formatFloat(subtotaal));

			// en hou de totalen bij voor na de loop
			totaal = totaal + subtotaal;
		});
		var totaalverzendkosten = parseFloat($("#_fldTOTAALVERZENDKOSTEN").val().replace(',', '.')) || 0;
		$("#_fldTOTAALVERZENDKOSTEN").val(Macs.SharedLib.formatFloat(totaalverzendkosten))
		$("#_fldTOTAAL").text(Macs.SharedLib.formatFloat(totaal + totaalverzendkosten));
	},
	artikelRijToevoegen: function(event) {
		var tbody = $("#artikelverkoop_artikelselectie tbody");
		if ($("tr.componentitem", tbody[0]).length <= 20) {
			var aantalrijen = $("tr.componentitem", tbody.first()[0]).length;
			var rij = Macs.ArtikelVerkoop.artikelRijTemplate.clone(true);

			// tabindex ophogen zodat er per regel getabbed kan worden
			rij.find(":input").each(function() {
				if ($(this).attr('tabindex') > 0) $(this).attr('tabindex', parseInt($(this).attr('tabindex'), 10) + 15 * aantalrijen);
			});

			// werkt alleen met een 'echte' event, omdat hij getriggerd wordt vanuit een ander frame
			var zoekveld = $("input.extsearch", rij);
			zoekveld[0].onchange = function(event) {
				Macs.ArtikelVerkoop.vulArtikelGegevens(this, this.value);
			};

			// Event handlers toevoegen (autosuggest, date input en tiptip)
			Polaris.Form.setAutoSuggest3($("input.extsearch", rij), {
				onItemSelect: Macs.ArtikelVerkoop.vulArtikelGegevens,
				toUpperCase: true,
				extraParams: {
					'': ''
				}
			});
			$("input.date_input", rij).date_input();

			// Rij toevoegen
			tbody.first().append(rij);
			// Focus zetten op cursus veld
			$("input.as_showfield:first", tbody).focus();

		} else {
			$(event.target).attr('disabled', 'true');
		}
	}
};

// begin meteen met het ophalen van gui ONafhankelijke informatie
Macs.SharedLib.laadParameters();

$(document).ready(function() {
	Macs.RelatieSelectieForm.initLandSelectie();
    Macs.RelatieSelectieForm.HandelingsOnbekwaamToelaten = true;
//	Macs.RelatieSelectieForm.initialiseer();

	/* Form actions */
	$('#dataform').submit(function(e) {
		e.preventDefault();
	});
	$('#btnCANCEL').click(function(e) {
		e.preventDefault();
		Macs.ArtikelVerkoop.annuleerForm();
	});
	$('#btnSAVE').click(function(e) {
		e.preventDefault();
		if (!$(this).hasClass('disabled')) Macs.ArtikelVerkoop.bewaarArtikelen();
	});

	$("#formview").bind('selecteerrelatie', function(event, item, prefix) {
		Macs.ArtikelVerkoop.onSelecteerRelatie(item, prefix);
	});

	$artikelverkoop_artikelselectie = $("#artikelverkoop_artikelselectie");
	$(".AANTAL,.PRIJSPERSTUKINEUROS,.VERWIJDERINGSBIJDRAGEPERSTUK", $artikelverkoop_artikelselectie[0]).add("#_fldTOTAALVERZENDKOSTEN").change(function() {
		Macs.ArtikelVerkoop.updateTotalen();
	});
	Macs.ArtikelVerkoop.updateTotalen(); // initialiseer de artikelen bedragen
	$("#_fldBETAALWIJZECODE").change(function() {
		if ($(this).val() == 'REM') {
			$("#_lblFRANKEERKOSTEN").text('Rembourskosten');
			$("#_fldTOTAALVERZENDKOSTEN").attr('readonly', 'readonly').val(Macs.SharedLib.formatFloat(Macs.SharedLib.parameters.KOSTENREMBOURS));
		} else {
			$("#_lblFRANKEERKOSTEN").text('Verzendkosten');
			$("#_fldTOTAALVERZENDKOSTEN").attr('readonly', '');
			Macs.ArtikelVerkoop.updateTotalen();
		}
		if ($(this).val() == 'REM' || $(this).val() == 'FRA') $("#_fldBANKGIROREKENING").attr('readonly', 'readonly');
		else $("#_fldBANKGIROREKENING").removeAttr('readonly');
		$("#_fldBANKGIROREKENING").toggleClass('required', $(this).val() == 'AUT');
	});

	// Bewaar een niet-ingevulde rij als template rij
	var tmpRij = $("#artikelverkoop_artikelselectie .componentitem:first");
	Macs.ArtikelVerkoop.artikelRijTemplate = tmpRij.clone(true);
	tmpRij.remove();

	// Knop 'Extra' koppelen aan event handler
	$("#artikelrijtoevoegen").click(Macs.ArtikelVerkoop.artikelRijToevoegen);

	/* Standaard zijn er drie artikel rijen om in te vullen*/
	Macs.ArtikelVerkoop.artikelRijToevoegen();
	Macs.ArtikelVerkoop.artikelRijToevoegen();


	if ($("#_fldZOEKRELATIE").val() == '') {
		// Initialiseer het formulier (alsof er geannuleerd wordt)
		Macs.ArtikelVerkoop.annuleerForm();
	}
    Polaris.Form.userIsEditing = false;
});
