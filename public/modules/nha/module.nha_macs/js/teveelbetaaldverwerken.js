Macs.TeveelBetaaldVerwerken = {
    TotaalBedrag: 0,
    MaxTotaalBedrag: 0,
    RestantBedrag: 0,
    ToegewezenTotaal: 0,
	betalingsverzoeken: null,
	initialiseer: function() {
		var Self = Macs.TeveelBetaaldVerwerken;

		var directive = {
			"tr.betalingsverzoek": {
				"betalingsverzoek <- ": {
					".PLR__RECORDID@value": "betalingsverzoek.PLR__RECORDID",
					".NR": "betalingsverzoek.BETALINGSVERZOEKNR",
					".BETALINGSVERZOEKNR@value": "betalingsverzoek.BETALINGSVERZOEKNR",
					".TERMIJN": "betalingsverzoek.TERMIJNNR",
					".KENMERK": "betalingsverzoek.BETALINGSKENMERK",
					".STATUS": "betalingsverzoek.STATUS",
					".BEDRAG": function(arg) {
						return Macs.SharedLib.formatFloat(arg.item.BEDRAGINEUROS, 2);
					},
					".BEDRAGBETAALD": function(arg) {
						return Macs.SharedLib.formatFloat(arg.item.BEDRAGBETAALDINEUROS, 2);
					},
					".TEBETALEN": function(arg) {
						return Macs.SharedLib.formatFloat(arg.item.BEDRAGNOGTEBETALENINEUROS, 2);
					},
					".VERZENDDATUM": "betalingsverzoek.VERZENDDATUM",
//					".VERVALDATA": "#{betalingsverzoek.CURSISTVERVALDATUM} / #{betalingsverzoek.SYSTEEMVERVALDATUM}",
					".HERINNERING@checked": function(arg) {
						return arg.item.HERINNERINGJANEE == 'J' ? 'checked' : '';
					},
					".REGELING@checked": function(arg) {
						return arg.item.BETALINGSREGELINGJANEE == 'J' ? 'checked' : '';
					},
					".BETALEN@value": "betalingsverzoek.PLR__RECORDID"
				}
			}
		};

		Self.htmlBetalingsverzoeken = $("#betalingsverzoekentabel");
		Self.templateBetalingsverzoeken = Self.htmlBetalingsverzoeken.compile(directive);

		// filter de verzoeken-set op basis van de aangeklikte filter(s)
		$("#betalingsverzoek_filters input").click(function(e) {
			Macs.TeveelBetaaldVerwerken.vulBetalingsverzoeken();
		});
/*
		$(window).resize(Macs.TeveelBetaaldVerwerken.resize);
		$(".pagetabcontainer a").click(Macs.TeveelBetaaldVerwerken.resize);
		Macs.TeveelBetaaldVerwerken.resize();
*/
		$("#_fldTOTAALBEDRAG").change(function(e) {
		    if (parseFloat($(this).val()) > Macs.TeveelBetaaldVerwerken.MaxTotaalBedrag) {
		        alert('U kunt maximaal '+Macs.TeveelBetaaldVerwerken.MaxTotaalBedrag+' toewijzen.');
		        $("#_fldTOTAALBEDRAG").val(Macs.TeveelBetaaldVerwerken.MaxTotaalBedrag);
		    }
            Macs.TeveelBetaaldVerwerken.TotaalBedrag = parseFloat($(this).val());
            Macs.TeveelBetaaldVerwerken.RestantBedrag = 0;

            Macs.TeveelBetaaldVerwerken.eersteVerdelingBedragen(true);
		});

		$("#_fldBUITENGEWONEBATENBEDRAG").change(function() {
		    $("#_chbBUITENGEWONEBATEN").prop('checked', true);
		});
	},
	bepaalBetalingsverzoeken: function(inschrijfnr, relatienr) {
		Polaris.Form.userIsEditing = false;
		$("#_chbBUITENGEWONEBATEN").prop('checked', false);
		$("#_fldBUITENGEWONEBATENBEDRAG").val('');
		$.getJSON(_servicequery, {
			'func': 'betalingsverzoeken',
			'inschrijfnr': inschrijfnr,
			'relatienr': relatienr
		},
		function(data, textStatus) {
			if (textStatus == 'success') {
				Macs.TeveelBetaaldVerwerken.betalingsverzoeken = data;
        		Macs.TeveelBetaaldVerwerken.bepaalBetaalTotalen(inschrijfnr);
				Macs.TeveelBetaaldVerwerken.vulBetalingsverzoeken(inschrijfnr);
			} else {
				alert('De betalingsverzoeken kon niet geladen worden. Error: ' + textStatus);
			}
		});
	},
	vulBetalingsverzoeken: function(inschrijfnr) {
		var Self = Macs.TeveelBetaaldVerwerken;

        $("#_fldINSCHRIJFNR_NIEUW").val(inschrijfnr);

		if (Self.betalingsverzoeken.length == 0) {
    		Self.htmlBetalingsverzoeken.empty();
		} else {

            var filteredVerzoeken = Self.filterVerzoeken(Self.betalingsverzoeken);
            Self.htmlBetalingsverzoeken = Self.htmlBetalingsverzoeken.render(filteredVerzoeken, Self.templateBetalingsverzoeken);

            $("#_fldRELATIENR_NIEUW").val(Self.betalingsverzoeken[0].RELATIENR);

            // bereken de totalen wanneer op een Samen checkbox wordt geklikt
            $("#betalingsverzoekentabel .BETALEN").click(function(e) {

                Self.verdeelBedragen(this);
            });
        }
	},
	bepaalBetaalTotalen: function(inschrijfnr) {
	    var allebedragen = '_alles'; // MARIELLE zegt: altijd overige bedragen meenemen $("_chkOVERIGEBEDRAGENMEENEMEN").attr('checked')?'_alles':'';
		$.getJSON(_servicequery, {
                'func': 'totaalteveelbetaald'+allebedragen,
                'inschrijfnr': inschrijfnr
            },
            function(data, textStatus) {
                if (textStatus == 'success') {
                    Macs.TeveelBetaaldVerwerken.TotaalBedrag = Macs.SharedLib.floatValue(data.TOTAALBEDRAG);
                    $("#_fldTOTAALBEDRAG").val(Macs.TeveelBetaaldVerwerken.TotaalBedrag);
                    Macs.TeveelBetaaldVerwerken.MaxTotaalBedrag = Macs.TeveelBetaaldVerwerken.TotaalBedrag;
                    $("#_fldMAXTOETEWIJZEN").text('€ ' + Macs.SharedLib.formatFloat(Macs.TeveelBetaaldVerwerken.TotaalBedrag,2));

                    Macs.TeveelBetaaldVerwerken.eersteVerdelingBedragen();
                } else {
                    alert('De totalen kon niet geladen worden. Error: ' + textStatus);
                }
            }
        );
	},
	zoekBetalingsVerzoekObject: function(recordid) {
		var Self = Macs.TeveelBetaaldVerwerken;

        var found = false;
		$(Self.betalingsverzoeken).each(function(i, elem) {
		    if (elem.PLR__RECORDID == recordid) {
		        found = elem;
		        return false;
		    }
		});
		return found;
	},
	eersteVerdelingBedragen: function(nieuwbedrag) {
	    nieuwbedrag = nieuwbedrag || false;

		var Self = Macs.TeveelBetaaldVerwerken;

        var rest = Macs.TeveelBetaaldVerwerken.TotaalBedrag;
        var totaaltoegewezen = 0;
        var index = 1;
		$(Self.betalingsverzoeken).each(function(i, elem) {
            log(Macs.SharedLib.escapeSel(elem.PLR__RECORDID));
            var row = $("#betalingsverzoekentabel .PLR__RECORDID[value="+Macs.SharedLib.escapeSel(elem.PLR__RECORDID)+"]").parents("tr");

            if (nieuwbedrag && !$(".BETALEN", row).prop('checked')) return true;

		    var nogtebetalen = parseFloat(elem.BEDRAGNOGTEBETALENINEUROS);
            elem.INDEX = 0;
            elem.TOEGEWEZEN = 0;
		    if (nogtebetalen > 0 && nogtebetalen <= rest) {
		        elem.TOEGEWEZEN = nogtebetalen;
	            $(".TOEGEWEZEN", row).val(nogtebetalen, 2);
	            $(".TOEGEWEZEN_DISPLAY", row).text(Macs.SharedLib.formatFloat(nogtebetalen, 2));
	            totaaltoegewezen = totaaltoegewezen + nogtebetalen;
                $(".VOLGORDE", row).text(index);
                elem.INDEX = index;
	            $(".BETALEN", row).prop('checked', true);
		        rest = rest - nogtebetalen;
		    } else if (rest > 0 && nogtebetalen > rest) {
		        elem.TOEGEWEZEN = rest;
	            $(".TOEGEWEZEN", row).val(elem.TOEGEWEZEN, 2);
	            $(".TOEGEWEZEN_DISPLAY", row).text(Macs.SharedLib.formatFloat(elem.TOEGEWEZEN, 2));
	            totaaltoegewezen = totaaltoegewezen + elem.TOEGEWEZEN;
                $(".VOLGORDE", row).text(index)
                elem.INDEX = index;
	            $(".BETALEN", row).prop('checked', true);
		        rest = 0;
		    } else {
  		        elem.TOEGEWEZEN = 0;
		        if ($(".BETALEN", row).prop('checked')) {
    	            $(".TOEGEWEZEN", row).val(elem.TOEGEWEZEN, 2);
	                $(".TOEGEWEZEN_DISPLAY", row).text(Macs.SharedLib.formatFloat(elem.TOEGEWEZEN, 2));
                    $(".VOLGORDE", row).text(index)
                    elem.INDEX = index;
		        } else {
    	            $(".TOEGEWEZEN", row).val('', 2);
	                $(".TOEGEWEZEN_DISPLAY", row).text('');
                    $(".VOLGORDE", row).text('')
                    elem.INDEX = 0;
		        }
		    }
		    index++;
		});
		Self.ToegewezenTotaal = totaaltoegewezen;
		Self.RestantBedrag = Self.MaxTotaalBedrag - Self.ToegewezenTotaal;

		Self.bepaalTotalen();
	},
	verdeelBedragen: function(checkbox) {
		var Self = Macs.TeveelBetaaldVerwerken;

	    var sortByIndex = function(a, b) {
	        return a.INDEX - b.INDEX;
	    }

        var toegewezentotaal = 0;
  		var currentrow = $(checkbox).parents('tr');

		if ($(checkbox).prop('checked')) {
		    if (Self.RestantBedrag > 0) {
                /* Huidige rij AANgevinkt:
                    - Hoogste index bepalen
                    - Huidige rij cijferen naar hoogste index + 1
                    - Bedrag toekennen aan huidige regel
                    - totalen weergeven
                */
                var hoogsteindex = 0;
                $(Self.betalingsverzoeken).each(function(i, elem) {
                    if (elem.INDEX > hoogsteindex) hoogsteindex = elem.INDEX;
                    toegewezentotaal = toegewezentotaal + elem.TOEGEWEZEN;
                });

                var elem = Self.zoekBetalingsVerzoekObject($(checkbox).val());
                elem.INDEX = hoogsteindex + 1;
                $(".VOLGORDE", currentrow).text(elem.INDEX);

                var nogtebetalen = parseFloat(elem.BEDRAGNOGTEBETALENINEUROS);
                if (nogtebetalen > 0 && nogtebetalen <= Self.RestantBedrag) {
                    $(".TOEGEWEZEN", currentrow).val(nogtebetalen);
                    $(".TOEGEWEZEN_DISPLAY", currentrow).text(Macs.SharedLib.formatFloat(nogtebetalen, 2));
                    elem.TOEGEWEZEN = nogtebetalen;
                    toegewezentotaal = toegewezentotaal + elem.TOEGEWEZEN;
                    Self.RestantBedrag = Self.RestantBedrag - nogtebetalen;
                } else if (Self.RestantBedrag > 0 && nogtebetalen > Self.RestantBedrag) {
                    $(".TOEGEWEZEN", currentrow).val(Self.RestantBedrag);
                    $(".TOEGEWEZEN_DISPLAY", currentrow).text(Macs.SharedLib.formatFloat(Self.RestantBedrag, 2));
                    elem.TOEGEWEZEN = Self.RestantBedrag;
                    toegewezentotaal = toegewezentotaal + elem.TOEGEWEZEN;
                    Self.RestantBedrag = 0;
                }
            } else {
                $(checkbox).prop('checked', false);
            }
		} else {
		    /* Huidige rij UITgevinkt:
		        - huidige rij resetten
		        - opnieuw afcijferen
		        - totaal bepalen
		        - totalen weergeven
		    */
            Macs.TeveelBetaaldVerwerken.eersteVerdelingBedragen(true);

            var elem = Self.zoekBetalingsVerzoekObject($(checkbox).val());
            if (elem) {
                elem.INDEX = 0;
                elem.TOEGEWEZEN = 0;
                $(".TOEGEWEZEN", currentrow).val(0);
                $(".TOEGEWEZEN_DISPLAY", currentrow).text(Macs.SharedLib.formatFloat(0,2));
                $(".VOLGORDE", currentrow).text('');
                $(".BETALEN", currentrow);
            } else {
                alert('Error: intern betalingsverzoek-object niet gevonden');
            }

            var sortedbetalingsverzoeken = Self.betalingsverzoeken.slice(0); // maak een copy van het array met slice(0) =alles
            sortedbetalingsverzoeken.sort(sortByIndex);

            var index = 1;
            $(sortedbetalingsverzoeken).each(function(i, elem) {
                var row = $("#betalingsverzoekentabel .PLR__RECORDID[value="+Macs.SharedLib.escapeSel(elem.PLR__RECORDID)+"]").parents("tr");
                var cb = $(".BETALEN", row);
                if (cb.prop('checked')) {
                    $(".VOLGORDE", row).text(index);
                    var original_elem = Self.zoekBetalingsVerzoekObject(elem.PLR__RECORDID);
                    original_elem.INDEX = index;
                    toegewezentotaal = toegewezentotaal + elem.TOEGEWEZEN;
                    index++;
                }
            });

            var restant =
    		Self.RestantBedrag = Macs.TeveelBetaaldVerwerken.MaxTotaalBedrag - toegewezentotaal;
		}
		Self.ToegewezenTotaal = toegewezentotaal;
		Self.bepaalTotalen(toegewezentotaal);
    },
    bepaalTotalen: function() {
		var Self = Macs.TeveelBetaaldVerwerken;

		$("#_fldTOEGEWEZEN").val(Macs.SharedLib.formatFloat(Self.ToegewezenTotaal, 2));
  		$("#_fldRESTANT").val(Macs.SharedLib.formatFloat(Self.RestantBedrag, 2));

        if (Self.RestantBedrag > 0) {
            $("#_fldBUITENGEWONEBATENBEDRAG").val(Macs.SharedLib.formatFloat(Self.RestantBedrag, 2));
        } else {
            $("#_fldBUITENGEWONEBATENBEDRAG").val('');
        }
    },
	filterVerzoeken: function(items) {
		var filters = $("#betalingsverzoek_filters input").map(function(index, elem) {
			if ($(elem).prop('checked')) return $(elem).val();
		});

		var result = [];
		if (filters.length > 0) {
			$(items).each(function(index, item) {
				var found = $.inArray(item.STATUS, filters);
				items[index].FIL = found;
				if (found != -1) {
					result.push(item);
				}
			});
		} else {
			result = items;
		}
		return result;
	},
	vulInschrijvingenSelect: function(inschrijvingenData) {
		Macs.TeveelBetaaldVerwerken.htmlBetalingsverzoeken.empty();
		$("#teveelbetaaldfooter input[type=text]").val('');
		$("#teveelbetaaldfooter span").text('');

	    $("#inschrijvingenselect").empty().addOptions(inschrijvingenData, "INSCHRIJFNR", "INSCHRIJFNR,CURSUSCODE,CURSUSNAAM");
	    if ($("#_fldZOEKINSCHRIJVING").val() != '') {
    	    $("#inschrijvingenselect").val($("#_fldZOEKINSCHRIJVING").val());
        }

        if ($("#inschrijvingenselect option:first").length > 0)
    	    $("#inschrijvingenselect").val($("#inschrijvingenselect option:first").val()).change();
	},
	selecteerInschrijving: function(inschrijfnr) {
	    var Self = Macs.TeveelBetaaldVerwerken;

  	    $("#inschrijvingenselect").val(inschrijfnr);

		Self.bepaalBetalingsverzoeken(inschrijfnr, Macs.InschrijvingSelectieForm.relatieData.RELATIENR);
	},
	zoekRelatie: function() {
	},
	onZoekRelatie: function(cursistnr) {
		var Self = Macs.TeveelBetaaldVerwerken;

	    var callback = function(data, textStatus) {
	        if (textStatus == 'success') {
        		Macs.InschrijvingSelectieForm.inschrijvingenData = data;
                Self.vulInschrijvingenSelect(Macs.InschrijvingSelectieForm.inschrijvingenData);
	        }
	    }
	    if (Macs.InschrijvingSelectieForm.relatieData.RELATIENR) {
	        $("#inschrijvingenselect").empty();
            Macs.InschrijvingSelectieForm.zoekInschrijvingen(cursistnr, callback);
        }
	},
	bewaarForm: function() {
		// Ga er vanuit dat er geen problemen met het formulier zijn
		var formValid = true;

		/***
		 * Controleer de standaard velden
		 */
		var validator = $("#dataform").validate({
			onsubmit: false,
			debug: false,
			validateHiddenFields: false
		});
		if (!$("#dataform").valid()) {
			formValid = false;
			Polaris.Base.modalMessage('U heeft ' + validator.numberOfInvalids() + ' velden niet correct ingevuld.', 1000, false);
		}

		if (formValid) {
//$('#dataform')[0].submit();
//return;
			Polaris.Base.modalMessage("Record wordt opgeslagen.", 0);
			jQuery.postJSON(_servicequery, $('#dataform'), function(data, textStatus) {
				Polaris.Base.closeModalMessage();
				if (textStatus == 'success') {
					validator.resetForm();
					if (data.error == false) {
						if (data.result == true) {
							Macs.SharedLib.ajaxFeedback('Record opgeslagen');
						}
						Macs.TeveelBetaaldVerwerken.bepaalBetalingsverzoeken(Macs.InschrijvingSelectieForm.inschrijvingData.INSCHRIJFNR || $("#inschrijvingenselect").val(), Macs.InschrijvingSelectieForm.relatieData.RELATIENR);
					} else {
						Polaris.Base.errorDialog(data.error, data.detailed_error);
					}
				} else {
					Macs.SharedLib.ajaxFeedback('Fout opgetreden bij JSON post.');
				}
			});
		}
	},
	annuleerForm: function() {
		Polaris.Form.userIsEditing = false;

		Macs.InschrijvingSelectieForm.maakInschrijvingLeeg();
		Macs.TeveelBetaaldVerwerken.htmlBetalingsverzoeken.empty();
		$("#teveelbetaaldfooter input[type=text]").val('');

		$("#teveelbetaaldfooter span").text('');
		$("#inschrijvingenselect").empty();
		Macs.InschrijvingSelectieForm.focusZoekVeld();
	},
	resize: function() {
		$bs = $("#betalingsverzoekarea");
		if ($bs.length > 0) {
			var height = $(window).height() - $bs.offset().top;
			if ($(".buttons").length > 0) height = Math.max(100, height - $(".buttons").outerHeight() - 30);
			$bs.css(($.browser.msie && $.browser.version < 7 ? '' : 'max-') + 'height', height + 'px');
log(height);
			$bs.height(height);
		}
	}
};

$(document).ready(function() {
	Macs.InschrijvingSelectieForm.initialiseer({
		'funcSelecteerInschrijving': Macs.TeveelBetaaldVerwerken.selecteerInschrijving,
		'funcZoekRelatie': Macs.TeveelBetaaldVerwerken.zoekRelatie,
		'funcOnZoekRelatie': Macs.TeveelBetaaldVerwerken.onZoekRelatie
	});

	/* Form actions */
	$('#dataform').submit(function(e) {
		e.preventDefault();
	});
	$('#btnCANCEL').click(function(e) {
		e.preventDefault();
		Macs.TeveelBetaaldVerwerken.annuleerForm();
	});
	$('#btnSAVE').click(function(e) {
		e.preventDefault();
		if (!$(this).hasClass('disabled')) Macs.TeveelBetaaldVerwerken.bewaarForm();
	});

    $("#inschrijvingenselect").change(function() {
        Macs.TeveelBetaaldVerwerken.selecteerInschrijving($("#inschrijvingenselect").val());
    });

	// initialiseer het formulier
	Macs.TeveelBetaaldVerwerken.initialiseer();

	if ($("#_fldZOEKRELATIE").val() == '' || $("#_fldZOEKINSCHRIJVING").val() == '') {
    	// Maak het formulier leeg (alsof er geannuleerd wordt)
		Macs.TeveelBetaaldVerwerken.annuleerForm();
	}
});