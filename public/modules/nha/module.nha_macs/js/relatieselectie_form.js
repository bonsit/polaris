Macs.RelatieSelectieForm = {
	relatiesData: [],

	relatieVelden: ['RELATIENR', 'VOORLETTERS', 'VOORNAAM', 'ACHTERNAAM', 'TUSSENVOEGSEL', 'GESLACHT', 'GEBDATUM', 'GEBPLAATS', 'BEDRIJFSNAAM', 'AFDELINGNAAM', 'STRAAT', 'HUISNR', 'HUISNUMMER', 'HUISNUMMERTOEVOEGING', 'POSTCODE', 'PLAATS', 'LANDCODE', 'TELEFOONOVERDAG', 'TELEFOONSAVONDS', 'MOBIELNR', 'EMAIL', 'BTWNUMMER', 'HANDELINGSONBEKWAAMJANEE'],

	html: '',
	template: '',

	relatieGrid: null,

	zoekwaarden: ['RELATIE', 'FACTUURRELATIE'],

	HandelingsOnbekwaamToelaten: false,

	initialiseer: function(callbacks) {
		var Self = Macs.RelatieSelectieForm;

		Self.html = $('#relaties tbody');
		//directive to render the template
		var directives = {
			'tr': {
				'relatie<-': {
					'td.nummer': 'relatie.RELATIENR',
					'td.naam': function(arg) {
						return Macs.SharedLib.bepaalAanhef(arg.item.GESLACHT) + ' ' + (arg.item.VOORLETTERS || '') + ' ' + (arg.item.TUSSENVOEGSEL || ' ') + ' ' + arg.item.ACHTERNAAM;
					},
					'td.straat': function(arg) {
						return arg.item.STRAAT + ' ' + arg.item.HUISNR;
					},
					'td.postcode': 'relatie.POSTCODE',
					'td.plaats': 'relatie.PLAATS',
					'td.land': 'relatie.LANDCODE',
					'td.handelingsonbekwaam': function(arg) { if (arg.item.HANDELINGSONBEKWAAMJANEE == 'J') return 'Ja'; else return ''; },
					'@onclick': function(arg) {
						return 'Macs.RelatieSelectieForm._selecteerRelatie(' + arg.pos + ');';
					},
					'@onmouseover': '"$(this).toggleClass(\'active\', true);"',
					'@onmouseout': '"$(this).toggleClass(\'active\', false);"',
					'@style': "'cursor:pointer'",
					'@class': function(arg) {
						//arg => {data:data, items:items, pos:pos, item:items[pos]};
						var oddEven = (arg.pos % 2 == 0) ? 'even ' : 'odd ';
						var firstLast = (arg.pos == 0) ? 'first ' : (arg.pos == arg.items.length - 1) ? 'last ' : '';
						var opvallend = arg.item.HANDELINGSONBEKWAAMJANEE == 'J' ? 'alert' : '';
						return ' ' + oddEven + firstLast + opvallend;
					}
				}
			}
		};

        if (Self.html.length > 0) {
    		Self.template = Self.html.compile(directives);
    	}
	},
	toggleVerplichteFactuurVelden: function(onoff) {
		var fields = $("#frmFACTUURRELATIE :input.appear_required").add("#frmFACTUURRELATIE :input.required");
		if (onoff == 'on') fields.removeClass("appear_required").addClass("required");
		else fields.removeClass("required").addClass("appear_required");
	},
	selecteerRelatie: function() {
		var self = Macs.RelatieSelectieForm;
		var prefix = Macs.RelatieSelectieForm.relatieType();
		var item = Macs.RelatieSelectieForm.geselecteerdeRelatie();
		/**
		 *  Toon de relatie in het Relatie formulier
		 */
		Macs.SharedLib.easyFillForm(item, Macs.RelatieSelectieForm.relatieVelden, prefix + '!');
		/* Velden die wat meer aandacht vergen */
		$('#' + prefix + '_hdnRecordID').val(item.PLR__RECORDID);
		$('#' + prefix + '_fldRELATIENR').val(item.RELATIENR);

		/* Label aanpassen en oplichten, zodat gebruiker weet wat gaande is */
		Polaris.Visual.yellowFade($('#lblRELATIE').html("Bestaande relatie")[0]);

		/* Save knop aanzetten */
		$('#btnSAVE').removeClass('disabled');
		/* record state omzetten naar 'Niet opslaan' */
		$("#" + prefix + "_hdnAction").val('nothing');
		$("#" + prefix + "_hdnState").val('clean');

		/* Zet geslacht om naar nette Aanhef */
		document.getElementById(prefix + '!_lblGESLACHT').innerHTML = Macs.SharedLib.bepaalAanhef(item.GESLACHT);
		var scope = Macs.RelatieSelectieForm.bepaalFormScope();

		$('select[name=GESLACHT]', scope).val(item.GESLACHT);
		// trigger the event 'selecteerrelatie'
		$('#formview').trigger('selecteerrelatie', [item, prefix]);
		log(item);
	},
	kopieerRelatie: function() {
		var prefix = Macs.RelatieSelectieForm.relatieType();

		if (Macs.RelatieSelectieForm.geselecteerdeRelatie() == null) {
			Polaris.Base.modalMessage(Macs.Messages.geenRelatieGeselecteerd);
			return;
		}

		Macs.RelatieSelectieForm.selecteerRelatie();

		/* Maak de huidige selectie ongedaan */
		$('table.relaties tbody tr').removeClass('selected');

		/* Label aanpassen en oplichten, zodat gebruiker weet wat gaande is */
		$('#lblRELATIE').html("Nieuwe relatie (gekopieerd)");
		Polaris.Visual.yellowFade($('#lblRELATIE')[0]);

		/* Save knop aanzetten */
		$('#btnSAVE').removeClass('disabled');

		/* nieuw record, dus relatienummer moet gegenereerd worden en recordid moet leeg worden */
		$('#' + prefix + '_fldRELATIENR').val('__auto_increment__');
		$('#' + prefix + '_hdnRecordID').val('');

		/* Switch naar edit mode */
		var scope = Macs.RelatieSelectieForm.bepaalFormScope();
		Macs.RelatieSelectieForm.switchModus('insert', scope);

		Polaris.Form.setFocusFirstField(".editmode");
	},
	nieuweRelatie: function() {
		var prefix = Macs.RelatieSelectieForm.relatieType();

		/* Maak de velden leeg */
		Macs.SharedLib.easyFillForm([], Macs.RelatieSelectieForm.relatieVelden, prefix + '!');

		/* Label aanpassen en oplichten, zodat gebruiker weet wat gaande is */
		$('#lblRELATIE').html("Nieuwe relatie");
		Polaris.Visual.yellowFade($('#lblRELATIE')[0]);

		/* Save knop aanzetten */
		$('#btnSAVE').removeClass('disabled');

		/* nieuw record, dus relatienummer moet gegenereerd worden en recordid moet leeg worden */
		$('#' + prefix + '_fldRELATIENR').val('__auto_increment__');
		$('#' + prefix + '_hdnRecordID').val('');

		/* Neem zoekvelden over in de nieuwe relatie */
		$("input[name=" + prefix + "\\!POSTCODE]").val($("#_fldZOEKPOSTCODE").val()).change();
		$("input[name=" + prefix + "\\!HUISNUMMER]").val($("#_fldZOEKHUISNR").val()).change();
		$(":input[name=" + prefix + "\\!LANDCODE]").val($("#_fldZOEKLAND").val()).change();

		if ($("#_fldZOEKLAND").val() == 'NL') {
			Macs.SharedLib.zoekFormAdres(prefix);
		};

		/* Switch naar edit mode */
		var scope = Macs.RelatieSelectieForm.bepaalFormScope();
		Macs.RelatieSelectieForm.switchModus('insert', scope);

		/* zet focus op Aanhef veld */
		$(':input[name=' + prefix + '\\!GESLACHT]').focus();

		Polaris.Form.setFocusFirstField(".editmode");
	},
	bewerkRelatie: function() {
       if ($('table.relaties tbody tr.selected').length > 0 || Macs.Telemarketing) {
            var prefix = Macs.RelatieSelectieForm.relatieType();
            var scope = Macs.RelatieSelectieForm.bepaalFormScope();
            Macs.RelatieSelectieForm.switchModus('edit', scope);

            Polaris.Form.setFocusFirstField($(".editmode", scope));
        } else {
            Polaris.Base.modalMessage(Macs.Messages.geenRelatieGeselecteerd);
        }
	},
	wisFactuurRelatieForm: function() {
		var self = Macs.RelatieSelectieForm;

		self.toggleVerplichteFactuurVelden('off');

		/* Maak de velden leeg */
		Macs.RelatieSelectieForm.maakLeeg('FACTUURRELATIE!');

		$("#FACTUURRELATIE_hdnAction").val('');
		$("#FACTUURRELATIE_hdnState").val('');
		$("#FACTUURRELATIE_hdnRecordID").val('');
		$('#FACTUURRELATIE_fldRELATIENR').val('');

        $("#_fldZOEKLAND").val(g_lang).attr('disabled', false);

		/* Zet het factuurrelatie form in View mode */
		Macs.RelatieSelectieForm.switchModus('view', $("#frmFACTUURRELATIE"));
	},
	maakLeeg: function(prefix) {
		var self = Macs.RelatieSelectieForm;

		Macs.SharedLib.easyFillForm([], self.relatieVelden, prefix);

		Macs.RelatieSelectieForm.zoekwaarden[prefix.replace('!', '')] = null;

		/* Zoek panel leegmaken */
		$(".searchbox input[type=search]").val('');
		/* Landveld standaard op NL */
		$("#_fldZOEKLAND").val(g_lang);

		$('#relaties tbody').empty();
	},
    switchBEPlaatsnaam: function(reltype) {
        var landfield = $("#"+reltype+"\\!_fldLANDCODE");
        var plaatsfield = $("input.pcsearch", landfield.parent().parent().parent());
        if (landfield.val() == 'BE') {
            var url = _serverroot+'/services/list/app/macs/form/postcodetabelbelgie/';

            var postcodefield = $("input.pcfield", landfield.parent().parent().parent());

            plaatsfield.autocomplete(url,
                {onItemSelect: null, selectFirst: false, selectOnly: false, openOnFocus: true, columnSearch: 'DEELGEMEENTE'
                , matchCase: 0, autoFill: false, minChars: 1, cacheLength: false, maxItemsToShow: 200
                , mustMatch: 0, delay: 100, extraParams:
                    function() {
                        return {'qc[]':'POSTCODE', 'qv[]':postcodefield.val() }
                    }
                }
            );
        } else {
            // remove autocomplete ingeval van 'niet BE'
            plaatsfield.unbind();
        }
    },
    switchHuisNr: function(huisnrveld, huisnrtoevoeging, landcode) {
        $(huisnrveld[0].form).validate();
        if (landcode == 'NL') {
            huisnrveld.rules("add", {'huisnr': true});
        } else {
            huisnrveld.rules("remove", 'huisnr');
        }
    },
	bepaalFormScope: function() {
		return $("#frm" + $("#relatietype").val())[0];
	},
	relatieType: function() {
		return $("#relatietype").val();
	},
	maakTabelSelecteerbaar: function(tableid, focusregel) {
		var $table = $(tableid);

		Macs.RelatieSelectieForm.relatieGrid = new KeyTable({
			'table': $table[0],
			'tabIndex': 130,
			'selectRow': true,
			'keepSelection': true,
			'continueScroll': false,
			'initScroll': true,
			//            'focus': [1,0],
			'form': true
		});

		Macs.RelatieSelectieForm.relatieGrid.setFocus(Macs.RelatieSelectieForm.relatieGrid.cellFromCoords(1, focusregel));

		/* Selecteer de rij wanneer gebruiker op Enter drukt (action) */
		$('tbody td', $table).each(function() {
			Macs.RelatieSelectieForm.relatieGrid.event.action(this, function(nCell) {
				var row = $(this.nCell).parents("tr");
				var pos = $('#relaties tbody tr').index(row[0]);
				Macs.RelatieSelectieForm._selecteerRelatie(pos);
			});
		});
	},
	vulRelatie: function(data) {
		var Self = Macs.RelatieSelectieForm;

		/* Vul de tabel op basis van de template */
		Self.html = Self.html.render(data, Self.template);

		/* Vul de tabel op basis van de template */
		Self.maakTabelSelecteerbaar("#relaties", 0);

		if (data.length == 1) {
			Macs.RelatieSelectieForm._selecteerRelatie(0);
			//            $("#relaties").data('input').blur();
		} else {
			$("#relaties").data('input').focus();
			$(".searchresult .content").scrollTop(0);
		}
	},
	switchModus: function(mode, scope) {
		var self = Macs.RelatieSelectieForm;
		var prefix = self.relatieType();

		scope = scope || document;
		if (mode == 'edit' || mode == 'insert') {
            $("#" + prefix + "_hdnAction").val('save');
            $("#" + prefix + "_hdnState").val(mode);

			$(".viewmode", scope).hide();
			$(".editmode", scope).show();

			// zet de Factuurrelatie velden op 'required', als er inderdaad een factuurrelatie gebruikt wordt
			if (prefix == 'FACTUURRELATIE') self.toggleVerplichteFactuurVelden('on');

		} else if (mode == 'view') {
			$(".viewmode", scope).show();
			$(".editmode", scope).hide();

			// zet de Factuurrelatie velden op 'required', als er inderdaad een factuurrelatie gebruikt wordt
			if (prefix == 'FACTUURRELATIE') self.toggleVerplichteFactuurVelden('off');
		} else if (mode == 'toggle') {
			$(".viewmode", scope).toggle();
			$(".editmode", scope).toggle();
		}
	},
	geselecteerdeRelatie: function() {
		/* Geeft de geselecteerde item terug */
		var row = $('table.relaties tbody tr.selected');
		var pos = $('#relaties tbody tr').index(row[0]);
		return Macs.RelatieSelectieForm.relatiesData[pos] || null;
	},
	_selecteerRelatie: function(pos) {
		/* Highlite de nieuwe selectie */
		$($('table.relaties tbody tr').removeClass('selected').get(pos)).toggleClass('selected', true);
		var item = Macs.RelatieSelectieForm.geselecteerdeRelatie();
		if (Macs.RelatieSelectieForm.HandelingsOnbekwaamToelaten || (!Macs.RelatieSelectieForm.HandelingsOnbekwaamToelaten && item.HANDELINGSONBEKWAAMJANEE == 'N')) {
			Macs.RelatieSelectieForm.selecteerRelatie();
		} else {
			Polaris.Base.modalMessage(Macs.Messages.relatieHandelingsOnbekwaam);
		}
		/* Maak de huidige selectie ongedaan */
		Macs.RelatieSelectieForm.relatieGrid.blur();
	},
	zoekRelatie: function() {
		$('#_fldZOEKPOSTCODE').val($('#_fldZOEKPOSTCODE').val());
		var _postcode = '';
		var _huisnr = '';
		var _huisnrveld = '';
		var _landcode = '';

		var _relatienr = $('#_fldZOEKRELATIE').val().trim();
		if (_relatienr) {
			_relatienr = "'" + _relatienr + "'";
//			$('#_fldZOEKLAND').val('');
		} else {
            _landcode = $('#_fldZOEKLAND').val();
            if (_landcode == null) _landcode = '';
            if (_landcode != '') _landcode = "'" + _landcode + "'";
            _postcode = $('#_fldZOEKPOSTCODE').val().replace(' ', '');
            if (_postcode) _postcode += '%';

		    if ($('#_fldZOEKLAND').val() == 'BE') {
		        _huisnrveld = 'HUISNRBE';
                _huisnr = "'"+$('#_fldZOEKHUISNR').val()+"'";
		    } else {
		        _huisnrveld = 'HUISNR';
                _huisnr = $('#_fldZOEKHUISNR').val();
                if (_huisnr) _huisnr += '%';
            }
		}

		Macs.SharedLib.checkSpamPostcode($('#_fldZOEKPOSTCODE').val().replace(' ', ''));

		$('#relaties tbody').empty();
		$('.searchresult .content').addClass('loading');
		var url = Macs.SharedLib._macs_servicequery.replace("%construct%", "relatie_search").replace("const", "form") + "?skippinfilter=true&output=json&limit=2000" + "&qc[]=RELATIENR&qv[]=" + _relatienr + "&qc[]=POSTCODE&qv[]=" + _postcode + "&qc[]=" + _huisnrveld + "&qv[]=" + _huisnr + "&qc[]=LANDCODE&qv[]=" + _landcode;
		$.getJSON(url, function(data, textStatus) {
			$('.searchresult .content').removeClass('loading');

			/* Leg data vast als lokale/globale var */
			Macs.RelatieSelectieForm.relatiesData = data;

			if (textStatus == 'success') {
				if (data.length == 0) {
					//                    Macs.RelatieSelectieForm.funcNieuweRelatie();
					Macs.RelatieSelectieForm.nieuweRelatie();
				} else {
					Macs.RelatieSelectieForm.vulRelatie(data);
				}
			} else {
				Polaris.Base.modalMessage(Macs.Messages.foutZoekActie + textStatus);
			}
		});
	},
	initLandSelectie: function(callback) {
	    $("input[name=_hdnLandSelectie]").click(function() {
	    	var sel_land = $(this).val();
	    	$.cookies.set('Macs.Dataentry.landselectie', sel_land);
	    	log('cookie logged');
	    	location.reload();
	    });
	    Macs.RelatieSelectieForm.resetLandSelectie();
	},
	resetLandSelectie: function() {
	    var landselectie = $.cookies.get('Macs.Dataentry.landselectie');
		if (landselectie == null) {
			if (g_lang == 'DE') {
				$("#DE_land").prop('checked', true);
				$.cookies.set('Macs.Dataentry.landselectie', 'DE');
		//		location.reload();
			} else {
				$("#NL_land").prop('checked', true);
				$.cookies.set('Macs.Dataentry.landselectie', 'NL');
			}
		} else {
			$("input[name=_hdnLandSelectie][value="+landselectie+']').prop('checked', true);
			$.cookies.set('Macs.Dataentry.landselectie', $("input[name=_hdnLandSelectie]:checked").val());
		}
	},
	huidigLandSelectie: function() {
		return $("input[name=_hdnLandSelectie]:checked").val()||'NL';
	},
	updateAjaxAttributes: function() {
		var landcode = Macs.RelatieSelectieForm.huidigLandSelectie();
		$("input.extsearch").each(function() {
			$(this).attr('ajaxurl', '/app/macs/form/'+$(this).attr('ajaxurl_ori')+'_'+landcode+'/');
		});
	}
};

$(document).ready(function() {

	Macs.RelatieSelectieForm.initialiseer();

	$('#btnSEARCH').click(function(e) {
		e.preventDefault();
		Macs.RelatieSelectieForm.zoekRelatie();
	});
	$('#btnRELATIECOPY').add("#btnFACTUURRELATIECOPY").click(function(e) {
		e.preventDefault();
		//Macs.RelatieSelectieForm.funcKopieerRelatie();
		Macs.RelatieSelectieForm.kopieerRelatie();
	});
	$('#btnRELATIENEW').add("#btnFACTUURRELATIENEW").click(function(e) {
		e.preventDefault();
		//Macs.RelatieSelectieForm.funcNieuweRelatie();
		Macs.RelatieSelectieForm.nieuweRelatie();
	});
	$("#btnRELATIEEDIT").add("#btnFACTUURRELATIEEDIT").click(function(e) {
		e.preventDefault();
		//Macs.RelatieSelectieForm.funcBewerkRelatie();
		Macs.RelatieSelectieForm.bewerkRelatie();
	});
	$("#btnFACTUURRELATIEERASE").click(function(e) {
		e.preventDefault();
		Macs.RelatieSelectieForm.wisFactuurRelatieForm();
	});

	/**
	 * Wijzig de tekst van 'Zoek relatie' naar 'Zoek factuurrelatie' en
	 *
	 */
	$("#pagetabs").bind('switched', function(event, pageid, alltabs, container, settings) {
		var relatietype = $(container).find("[href=" + pageid + "]").text();
		$("#lblzoekrelatie").text($.t('nha.macs.zoek') + ' ' + relatietype);

		/***
		 * leg de zoekwaarden vast per relatie (relatie en factuurrelatie)
		 */
		var reltype = pageid == '#page_factuurrelatie' ? 'FACTUURRELATIE' : 'RELATIE';
		$("#relatietype").val(reltype);

		var vorigereltype = reltype == 'RELATIE' ? 'FACTUURRELATIE' : 'RELATIE';

		Macs.RelatieSelectieForm.zoekwaarden[vorigereltype] = {
			'relatienr': $("#_fldZOEKRELATIE").val(),
			'postcode': $("#_fldZOEKPOSTCODE").val(),
			'huisnr': $("#_fldZOEKHUISNR").val(),
			'land': $("#_fldZOEKLAND").val()
		};

		var huidigewaarde = Macs.RelatieSelectieForm.zoekwaarden[reltype];
		if (typeof huidigewaarde != 'undefined' && huidigewaarde != null) {
			$("#_fldZOEKRELATIE").val(huidigewaarde.relatienr);
			$("#_fldZOEKPOSTCODE").val(huidigewaarde.postcode);
			$("#_fldZOEKHUISNR").val(huidigewaarde.huisnr);

			$("#_fldZOEKLAND").val(huidigewaarde.land);
		} else {
			$("#_fldZOEKRELATIE").val('');
			$("#_fldZOEKPOSTCODE").val('');
			$("#_fldZOEKHUISNR").val('');
		}

        $(".searchbox input:first").focus();
	});

	var $fldZoekRelatie = $("#_fldZOEKRELATIE");
	// Enter in het veld Postcode springt naar veld Huisnummer
	$("#_fldZOEKPOSTCODE").keyup(function(event) {
		if (event.keyCode == 13
		/*Enter*/
		) {
			$("#_fldZOEKHUISNR").focus();
		}
	});
	// Enter in het veld Huisnummer zoekt de relatie op
	$("#_fldZOEKHUISNR").keyup(function(event) {
		if (($("#_fldZOEKPOSTCODE").val() != '' || this.value != '') && event.keyCode == 13
		/*Enter*/
		) {
			Macs.RelatieSelectieForm.zoekRelatie();
		}
	});
	$("#_fldZOEKLAND").keyup(function(event) {
		if (($("#_fldZOEKPOSTCODE").val() != '' || this.value != '') && event.keyCode == 13
		/*Enter*/
		) {
			Macs.RelatieSelectieForm.zoekRelatie();
		}
	})
	.change(function(event) {
	    Macs.RelatieSelectieForm.switchBEPlaatsnaam(Macs.RelatieSelectieForm.relatieType());
	});
	// Enter in het veld Relatienr zoekt de relatie op
	$fldZoekRelatie.keyup(function(event) {
		if (this.value != '' && event.keyCode == 13
		/*Enter*/
		) {
			Macs.RelatieSelectieForm.zoekRelatie();
		}
	});

	var relatienr = Polaris.Base.getLocationVariable('relatienr');
	if (typeof relatienr != 'undefined') {
		$fldZoekRelatie.val(relatienr);
	}

	if ($fldZoekRelatie.length > 0 && $fldZoekRelatie.val() != '') {
		Macs.RelatieSelectieForm.zoekRelatie();
	}

	$("#RELATIE\\!_fldPOSTCODE,#FACTUURRELATIE\\!_fldPOSTCODE").blur(function() {
		Macs.SharedLib.checkSpamPostcode($(this).val());
	});

});
