Macs.DataEntry = {
    selecteerRelatie: function(elem,pos) {
        /* Maak de huidige selectie ongedaan */
        $('table.relaties tbody tr').removeClass('selected');
        /* Highlite de nieuwe selectie */
        $(elem).toggleClass('selected', true);

        var item = Macs.RelatieSelectieForm.relatiesData[pos];
        
        /**
        *  Toon de relatie in het Relatie formulier 
        */
        Macs.SharedLib.easyFillForm(item, this.relatieVelden);
        /* Velden die wat meer aandacht vergen */    
        $('#_hdnRecordID').val(item.PLR__RECORDID);

        /* Label aanpassen en oplichten, zodat gebruiker weet wat gaande is */
        Polaris.Visual.yellowFade($('#lblRELATIE').html("Bestaande relatie")[0]);
        /* Save knop aanzetten */
        $('#btnSAVE').removeClass('disabled');
        /* record state omzetten naar Edit */
        $('input[name=_hdnState]').val('edit');
    },
    kopieerRelatie: function() {
        /* Maak de huidige selectie ongedaan */
        $('table.relaties tbody tr').removeClass('selected');
        /* Label aanpassen en oplichten, zodat gebruiker weet wat gaande is */
        $('#lblRELATIE').html("Nieuwe relatie (gekopieerd)");
        Polaris.Visual.yellowFade($('#lblRELATIE')[0]);
        /* Save knop aanzetten */
        $('#btnSAVE').removeClass('disabled');
        /* record state omzetten naar Insert */
        $('input[name=_hdnState]').val('insert');
        /* nieuw record, dus relatienummer moet gegenereerd worden */
        $('input[name=RELATIENR]').val('__auto_increment__');
    },
    nieuweRelatie: function() {
        /* Maak de velden leeg */
        Macs.SharedLib.easyFillForm([], this.relatieVelden);    
        /* Label aanpassen en oplichten, zodat gebruiker weet wat gaande is */
        $('#lblRELATIE').html("Nieuwe relatie");
        Polaris.Visual.yellowFade($('#lblRELATIE')[0]);
        /* Save knop aanzetten */
        $('#btnSAVE').removeClass('disabled');
        /* record state omzetten naar Insert */
        $('input[name=_hdnState]').val('insert');
        /* nieuw record, dus relatienummer moet gegenereerd worden */
        $('input[name=RELATIENR]').val('__auto_increment__');
    },
    bewaarRelatie: function() {
        var url = _servicequery+"?output=json";
        jQuery.post(url, $('#dataform').serializeArray(), function(data, textStatus){ Macs.SharedLib.ajaxFeedback('Record opgeslagen'); $('#errorMessage').html(data+ ' ' +textStatus);});
    },
    annuleerForm: function() {
        /* Maak de velden leeg */
        Macs.SharedLib.easyFillForm([], this.relatieVelden);
        $('#relaties tbody').empty();
    }
};