Macs.CursusGegevens = {
    cursusgegevensDataBasis: [],
    cursusgegevensDataCohorten: [],
	cursusgegevensDataSTT: [],
	cursusgegevensDataBTT: [],
	cursusgroepenData: [],
	cursuscodesData: [],

	cursusVelden: ['CURSUSCODE', 'CURSUSNAAM', 'WEBOMSCHRIJVING', 'CURSUSGROEP', 'ADMINISTRATIE', 'STUDIEGIDSCODE', 'LEAFLETCODE'
	, 'CURSUSDUURMAANDEN', 'STANDAARDSTUDIETEMPO', 'STANDAARDBETAALTEMPOTERMIJNEN', 'AANTALSEALPAKKETTEN', 'AANTALMAANDENONDERBREKING'
	, 'ZENDINGINDIENBETAALDJANEE', 'BEGELEIDINGMETOFZONDER@radio', 'BELLENNAGEENREACTIEJANEE@radio', 'EXTERNINTERNEXAMEN', 'EXAMENGELD'
	, 'MIDDELBAARONDERWIJSSOORT', 'MBOHBONIVEAU', 'TOTAALAANTALLESSEN', 'RETOURNEERBAARJANEE@radio', 'OPZEGBAARJANEE@radio'
	, 'AANTALLESSENOPZEGGING', 'AANTALLESSENPERMAANDBIJOPZEG', 'STICKERVENSTERENVELOPE1@radio', 'STICKERVENSTERENVELOPEN@radio'
	, 'LIGTOPSORTEERPOSITIE', 'LESBLOKPLUSENVELOP@radio', 'AANTALRINGBANDEN', 'GEBLOKKEERDJANEE@radio'
    , 'ALLEENZICHTBAARVOORBACKOFF@radio', 'GETUIGSCHRIFTJANEE@radio', 'CURSUSCLUSTERNR'
	, 'INSCHRIJVINGENBLOKKERENJANEE@radio', 'MATERIAALCOMPLEETJANEE@radio', 'MATERIAALVERWACHTDATUM', 'TEGEMOETKOMINGWACHTTIJD'
	, 'DATUMGELDIGVANAF', 'DATUMGELDIGTM', 'SOORT_CURSUS', 'AANTALINSCHRIJVINGEN', 'CNUMMER', 'LEERWEG', 'SOORTONDERWIJS'
    , 'ONDERTEK_VOOR_VERSTUREN_JN@radio', 'INSCHRIJFFORM_PER_EMAIL_JN@radio', 'PLATHOSID'
	],

	htmlCohorten: '',
	htmlSTT: '',
	htmlBTT: '',
	templateBasis: '',
	templateCohorten: '',
	templateSTT: '',
	templateBTT: '',

/*
	STTGrid: null,
	BTTGrid: null,
*/
	zoekwaarden: ['RELATIE', 'FACTUURRELATIE'],

    studietempos: [
        {"CODE":"A","NAAM":"alles"}
       ,{"CODE":"H","NAAM":"hoog"}
       ,{"CODE":"L","NAAM":"laag"}
       ,{"CODE":"N","NAAM":"normaal"}
    ],

	initialiseer: function() {
		var Self = Macs.CursusGegevens;

		Self.htmlCohorten = $('#chtabel tbody');
		var Cohortdirectives = {
			'tr': {
				'cohort<-': {
					'td.cohort': function(arg) {
					    return arg.item.JAARVANAF + ' t/m ' + arg.item.JAARTM;
					},
					'td.aantalinschrijvingen': 'cohort.AANTALINSCHRIJVINGEN',
					'@onclick': function(arg) {
						return 'Macs.CursusGegevens._wijzigCohort(' + arg.pos + ');';
					},
					'@style': "'cursor:pointer'",
					'@class': function(arg) {
						//arg => {data:data, items:items, pos:pos, item:items[pos]};
						var oddEven = (arg.pos % 2 == 0) ? 'even ' : 'odd ';
						var firstLast = (arg.pos == 0) ? 'first ' : (arg.pos == arg.items.length - 1) ? 'last ' : '';
						return ' ' + oddEven + firstLast;
					}
				}
			}
		};
		if (Self.htmlCohorten.length > 0) Self.templateCohorten = Self.htmlCohorten.compile(Cohortdirectives);

		Self.htmlSTT = $('#stttabel tbody');
		//directive to render the template
		var STTdirectives = {
			'tr': {
				'stt<-': {
					'td.lessen': function(arg) {
					    var result = arg.item.BEGINLES;
					    if (arg.item.BEGINLES != arg.item.EINDLES)
					        result += ' t/m ' + arg.item.EINDLES;
					    return result;
					},
					'td.studietempo': 'stt.STUDIETEMPONAAM',
					'td.termijn': 'stt.TERMIJNNR',
					'td.hoverpop': function(arg) {
					    var omschr = '';
					    var eersteles = '';
					    var laatsteles = '';
					    var beginles = parseInt(arg.item.BEGINLES, 10);
					    var eindles = parseInt(arg.item.EINDLES, 10);
					    $(Macs.CursusGegevens.cursusgegevensDataSTT.LESSEN).each(function(i, elem) {
    					    var cursusles = parseInt(elem.CURSUSLESNR, 10);
					        if (cursusles >= beginles && cursusles <= eindles) {
                                if (eersteles == '') eersteles = cursusles;

                                if ((typeof Macs.CursusGegevens.cursusgegevensDataSTT.LESSEN[i + 1] != 'undefined'
                                    && elem.OMSCHRIJVING != Macs.CursusGegevens.cursusgegevensDataSTT.LESSEN[i + 1].OMSCHRIJVING)
                                || (typeof Macs.CursusGegevens.cursusgegevensDataSTT.LESSEN[i + 1] == 'undefined')
                                || cursusles == eindles
                                ) {
        					        laatsteles = cursusles;
        					        omschr += '<b>';
        					        if (eersteles != laatsteles)
        					            omschr += eersteles + ' t/m ';
      					            omschr += laatsteles + '</b> - ' + elem.OMSCHRIJVING + "\r\n";
    					            eersteles = '';
                                }
					        }
					    });
					    return '<input type="hidden" value="'+omschr+'" /><img src="modules/nha/module.nha_macs/html/info.png" />';
					},
					'@onclick': function(arg) {
						return 'Macs.CursusGegevens._wijzigRegel(' + arg.pos + ');';
					},
					'@style': "'cursor:pointer'",
					'@class': function(arg) {
						//arg => {data:data, items:items, pos:pos, item:items[pos]};
						var oddEven = (arg.pos % 2 == 0) ? 'even ' : 'odd ';
						var firstLast = (arg.pos == 0) ? 'first ' : (arg.pos == arg.items.length - 1) ? 'last ' : '';
						return ' ' + oddEven + firstLast;
					}
				}
			}
		};
        if (Self.htmlSTT.length > 0) Self.templateSTT = Self.htmlSTT.compile(STTdirectives);

		Self.htmlBTT = $('#btttabel tbody');
		//directive to render the template
		var BTTdirectives = {
			'tr': {
				'btt<-': {
				    'td.aantaltermijnen': 'btt.AANTALTERMIJNEN',
					'td.zelfstudie': function(arg) {
				        return arg.item.ZELFSTUDIEJANEE == 'J' ? 'Ja' : 'Nee';
					},
					'td.bedrag': function(arg) {
					    return Macs.SharedLib.formatFloat(arg.item.BEDRAGINEUROS,2,'.');
					},
					'td.bedragbe': function(arg) {
					    return Macs.SharedLib.formatFloat(arg.item.BEDRAGINEUROS_BELGIE,2,'.');
					},
					'td.bedragbeoorspr': function(arg) {
					    return Macs.SharedLib.formatFloat(arg.item.BEDRAGINEUROS_BELGIE_OORSPR,2,'.');
					},
					'@onclick': function(arg) {
						return 'Macs.CursusGegevens._wijzigRegel(' + arg.pos + ');';
					},
					'@style': "'cursor:pointer'",
					'@class': function(arg) {
						//arg => {data:data, items:items, pos:pos, item:items[pos]};
						var oddEven = (arg.pos % 2 == 0) ? 'even ' : 'odd ';
						var firstLast = (arg.pos == 0) ? 'first ' : (arg.pos == arg.items.length - 1) ? 'last ' : '';
						return ' ' + oddEven + firstLast;
					}
				}
			}
		};
        if (Self.htmlBTT.length > 0) Self.templateBTT = Self.htmlBTT.compile(BTTdirectives);

        /* Popup schermen */
        $("#_fldCOHORT_DOSSIERCOHORT").change(function() {
            if ($(this).val() == 'NIEUWDOSSIERCOHORT') {
                var onshow = function() {
                    $("#_fldSOORTCOHORT").val('D');
                    $(this).val('');
                }
                Polaris.Window.showModuleForm('/modules/nha/module.nha_macs/html/cursusgegevensplus_cohorttoevoegen.tpl.php', Self.bewaarNieuwePeriode, onshow);
            }
        });

        $("#_fldCOHORT_COHORT").change(function() {
            if ($(this).val() == 'NIEUWCOHORT') {
                var onshow = function() {
                    $("#_fldSOORTCOHORT").val('O');
                    $(this).val('');
                }
                Polaris.Window.showModuleForm('/modules/nha/module.nha_macs/html/cursusgegevensplus_cohorttoevoegen.tpl.php', Self.bewaarNieuwePeriode, onshow);
            }
        });

        $("#btnDelete").click(Self.verwijderCursus);

        $("#_fldINCLUSIEFINACTIEF").click(function() {
            $.cookies.set('Macs.Cursusgegevens.inclusiefinactief', $(this).is(':checked'));
            Self.vulCursusCodes($("#cursusgroepen").val(), $(this).is(':checked'));
        });
        $("#_fldINCLUSIEFINACTIEF").prop('checked', $.cookies.get('Macs.Cursusgegevens.inclusiefinactief')=='true'?true:false );

        $("#action_masterinsert").click(function(e) {
            e.preventDefault();
            Self.cursusRecordToevoegen();
        });

        $("#action_mastersearch").unbind('click');
        $("#action_mastersearch").click(function(e) {
            e.preventDefault();
            Self.switchView(true);
        });

        $("#cursusgroepen").change(function() {
            $.cookies.set('Macs.Cursusgegevens.cursusgroep', $(this).val());
            Self.vulCursusCodes($(this).val(), $("#_fldINCLUSIEFINACTIEF").prop('checked'));
        });

        $("#cursusgroepen").val($.cookies.get('Macs.Cursusgegevens.cursusgroep') || $("#cursusgroepen option:first").val());

        Self.vulCursusCodes($("#cursusgroepen").val(), $("#_fldINCLUSIEFINACTIEF").prop('checked'));

        $("#cursuscodes").change(Self.cursusCodeGewijzigd);

        // zorg dat de 'Bekijk' link wordt omgezet naar een js event
        $("a.edit_but.jump").click(function(e) {
            e.preventDefault();
            Self.onSelectRowHandler(this, e);
        });
        Polaris.Dataview.onSelectRowHandler = Self.onSelectRowHandler;

        $("#pagetabs").bind('switched', function(event, pageid, alltabs, container, settings) {
            Polaris.Form.adjustLabelWidth($(pageid));
            if (pageid == '#page_ckl') {
                Self.runCheckList();
            }
        });
        Polaris.Form.adjustLabelWidth($('#page_bg'));

        $(".btnVoegtoeItem").click(Self.voegRijToe);
        $(".btnVerwijderItem").click(Self.verwijderRij);

        $(".btnVoegtoeItemCohort").click(Self.voegCohortRijToe);
        $(".btnVerwijderItemCohort").click(Self.verwijderCohortRij);
        $(".btnKopieerItemCohort").click(Self.KopieerCohortRij);

        $(".btnRefreshItemList").click(Self.laadItemListOpnieuw);
        $("#btnBuild").click(Self.bewaarCursus);
        $("#btnCancel").click(Self.annuleerForm);

        if (typeof Polaris.Base.getLocationVariable('tag') != 'undefined'
        || typeof Polaris.Base.getLocationVariable('q') != 'undefined'
        || typeof Polaris.Base.getLocationVariable('qc[]') != 'undefined') {
            $("#scherm_cursusgegevens").hide();
            $("#scherm_cursusoverzicht").show();
        } else {
            $("#scherm_cursusoverzicht").hide();
            $("#scherm_cursusgegevens").show();
        }

        // alle cohort velden
        $("#_fldCOHORT_DATUMGELDIGVANAF,#_fldCOHORT_DATUMGELDIGTM,#_fldCOHORT_COHORT,#_fldCOHORT_DOSSIERCOHORT,#_fldCOHORT_CNUMMER,#_fldCOHORT_KDCODE"
        ).change(Self.bewaarCohortLokaal);

        // ingeval van materiaalcompleet dan scherm tonen met foutieve inschrijvingen (studietempo)
        $("#_fldMATERIAALCOMPLEETJANEE_J").click(Self.scanFoutieveStudieTempo);
	},
    scanFoutieveStudieTempo: function(e) {
        var Self = Macs.CursusGegevens;

        $.getJSON(_servicequery, {
            'func': 'foutievestudietempo'
            ,'cursuscode': $("#cursuscodes").val()
        }, function(data, textStatus) {
            if (textStatus == 'success') {
                log(data);
                if (data.foutenlijst.length > 0) {
                    Self.toonFouteInschrijvingen(data);
                }
            }
        });
    },
    toonFouteInschrijvingen: function(data) {
        var Self = Macs.CursusGegevens;
        var afterShow = function() {
            $(data.foutenlijst).each(function(i, elem) {
                log($("#ONJUISTTEMPOLIJST"));
                $("#ONJUISTTEMPOLIJST").append('<li><span>Inschrijfnr: '+elem.INSCHRIJFNR+'</span><span> Studietempocode: '+elem.STUDIETEMPOCODE+'</span></li>');
            });
            $("#selJUISTESTUDIETEMPO").addOptions(data.studietempos, 'STUDIETEMPOCODE', 'STUDIETEMPOCODE');
        };

        Polaris.Window.showModuleForm('/modules/nha/module.nha_macs/html/cursusgegevensplus_foutstudietempo.tpl.php', Self.updateFoutieveStudieTempos, afterShow);
    },
    updateFoutieveStudieTempos: function() {
        var Self = Macs.CursusGegevens;

        Self.cursusgegevensDataBasis.JUISTSTUDIETEMPO = $("#selJUISTESTUDIETEMPO").val();
    },
	huidigeRij: function(type) {
		var Self = Macs.CursusGegevens;

	    type = type || 'focus';
	    var tn = Self.huidigeItemsTabelNaam();
		return $('#'+tn+' tbody tr.'+type);
	},
	selecteerRij: function(tabelnaam) {
		var Self = Macs.CursusGegevens;

		Self.resetRijen(Self.huidigeRij());
	},
	wijzigSTT: function(row) {
		var Self = Macs.CursusGegevens;

	    row = row || Self.huidigeRij();
        var item = Self.getGeselecteerdeItem();
        var _state = $.data(item, 'state');

	    if (item && (_state == 'view' || _state == null)) {
            $.data(item, 'state', 'edit');

            Self.resetRijen(row);

            var selectStudieTempo = $('<select class="studietempo" tabindex="100"></select>');
            selectStudieTempo.change(Self.bewaarRij);
            $(".studietempo", row).html(selectStudieTempo.addOptions(Self.studietempos, 'CODE','NAAM', item.STUDIETEMPOCODE));

            var inputTermijn = $('<input tabindex="102" class="termijn" style="width:20px" type="text" value="'+item.TERMIJNNR+'">');
            inputTermijn.change(Self.bewaarRij);
            $(".termijn", row).html(inputTermijn);

            var selectBeginLessen = $('<select style="width:50px"  tabindex="104" class="beginles"></select>');
            selectBeginLessen.change(Self.bewaarRij).change(Self.controleerLessen);
            selectBeginLessen.addOptions(Macs.CursusGegevens.cursusgegevensDataSTT.LESSEN, 'CURSUSLESNR','CURSUSLESNR,OMSCHRIJVING', item.BEGINLES);
            var selectEindLessen = $('<select style="width:50px" tabindex="106" class="eindles"></select>');
            selectEindLessen.change(Self.bewaarRij).change(Self.controleerLessen);;
            selectEindLessen.addOptions(Macs.CursusGegevens.cursusgegevensDataSTT.LESSEN, 'CURSUSLESNR','CURSUSLESNR,OMSCHRIJVING', item.EINDLES);
            $(".lessen", row).empty().append(selectBeginLessen).append(' t/m ').append(selectEindLessen);
            selectStudieTempo.focus();
        }
	},
	wijzigBTT: function(row) {
		var Self = Macs.CursusGegevens;

	    row = row || Self.huidigeRij();
        var item = Self.getGeselecteerdeItem();
        var _state = $.data(item, 'state');

	    if (item && (_state == 'view' || _state == null)) {
            $.data(item, 'state', 'edit');

            Self.resetRijen(row);

            var inputAantalTermijnen = $('<input tabindex="100" class="aantaltermijnen validation_integer" style="width:20px" type="text" value="'+item.AANTALTERMIJNEN+'">');
            inputAantalTermijnen.change(Self.bewaarRij);
            $(".aantaltermijnen", row).html(inputAantalTermijnen);

            var radioZelfStudieJa = $('<input type="radio" tabindex="104" name="zelfstudiejanee" class="zelfstudiejanee" value="J">');
            radioZelfStudieJa.change(Self.bewaarRij);
            var radioZelfStudieNee = $('<input type="radio" tabindex="105" name="zelfstudiejanee" class="zelfstudiejanee" value="N"> Nee ');
            radioZelfStudieNee.change(Self.bewaarRij);
            if (item.ZELFSTUDIEJANEE == 'J') {
                radioZelfStudieJa.prop('checked',true);
            } else {
                radioZelfStudieNee.prop('checked',true);
            }
            $(".zelfstudie", row).html(radioZelfStudieJa).append(' Ja &nbsp;').append(radioZelfStudieNee).append(' Nee');

            var inputBedrag = $('<input tabindex="108" class="validation_float bedrag" style="width:50px" type="text" value="'+Macs.SharedLib.formatFloat(item.BEDRAGINEUROS,2,'.')+'">');
            inputBedrag.change(Self.bewaarRij);
            $(".bedrag", row).html(inputBedrag);

            var inputBedragBE = $('<input tabindex="110" class="validation_float bedragbe" style="width:50px" type="text" value="'+Macs.SharedLib.formatFloat(item.BEDRAGINEUROS_BELGIE,2,'.')+'">');
            inputBedragBE.change(Self.bewaarRij);
            $(".bedragbe", row).html(inputBedragBE);

            var inputBedragBEoorspr = $('<input tabindex="111" class="validation_float bedragbeoorspr" style="width:50px" type="text" value="'+Macs.SharedLib.formatFloat(item.BEDRAGINEUROS_BELGIE_OORSPR,2,'.')+'">');
            inputBedragBEoorspr.change(Self.bewaarRij);
            $(".bedragbeoorspr", row).html(inputBedragBEoorspr);

            $('input:text.validation_float').autoNumeric({aSep: '.', aDec: ','});
            $('input:text.validation_integer').autoNumeric({aSep: '', aDec: ',', mDec: 0});

            inputAantalTermijnen.focus();
        }
	},
	controleerLessen: function() {
		var Self = Macs.CursusGegevens;

	    var row = Self.huidigeRij();
	    var beginles = parseInt($(".beginles", row).val(), 10);
	    var eindles = parseInt($(".eindles", row).val(), 10);
	    if (eindles < beginles) {
	        $(".eindles", row).val(beginles);
	        var rijObject = Self.getRijObject(row);
	        rijObject.EINDLES = beginles;
	    }
	},
	resetRijen: function(huidigeRij) {
		var Self = Macs.CursusGegevens;

        var tn = Self.huidigeItemsTabelNaam();
	    $('#'+tn+' tbody tr').each(function() {
	        if (typeof(huidigeRij) == 'undefined' || huidigeRij[0] != this)
	            Self.resetRij($(this));
	    });
	},
	resetRij: function(row) {
		var Self = Macs.CursusGegevens;

		var rowObject = Self.getRijObject(row);
	    $.data(rowObject, 'state', 'view');

        var tn = Self.huidigeItemsTabelNaam();

        if (tn == 'stttabel') {
            $(".studietempo", row).html(rowObject.STUDIETEMPONAAM);
            $(".termijn", row).html(rowObject.TERMIJNNR);
            $(".lessen", row).html(rowObject.BEGINLES);
            if (rowObject.BEGINLES != rowObject.EINDLES) {
                $(".lessen", row).append(' t/m '+rowObject.EINDLES);
            }
        } else {
            $(".aantaltermijnen", row).html(rowObject.AANTALTERMIJNEN);
            $(".zelfstudie", row).html(rowObject.ZELFSTUDIEJANEE=='J'?'Ja':'Nee');
            $(".bedrag", row).html(Macs.SharedLib.formatFloat(rowObject.BEDRAGINEUROS,2,'.'));
            $(".bedragbe", row).html(Macs.SharedLib.formatFloat(rowObject.BEDRAGINEUROS_BELGIE,2,'.'));
            $(".bedragbeoorspr", row).html(Macs.SharedLib.formatFloat(rowObject.BEDRAGINEUROS_BELGIE_OORSPR,2,'.'));
        }
	},
	getRijObject: function(row) {
		var Self = Macs.CursusGegevens;

        var tn = Self.huidigeItemsTabelNaam();
        var ds = Self.huidigeItemsDataSet();
		var pos = $('#'+tn+' tbody tr').index($(row)[0]);
		return ds[pos] || null;
	},
	getGeselecteerdeItem: function(type) {
		var Self = Macs.CursusGegevens;

		/* Geeft de geselecteerde item terug */
		var result = false;

		var row = Macs.CursusGegevens.huidigeRij(type);
        if (row.length > 0) {
            var tn = Self.huidigeItemsTabelNaam();
            var ds = Self.huidigeItemsDataSet();
    		var pos = $('#'+tn+' tbody tr').index(row[0]);
            result = ds[pos];
        }
		return result;
	},
	maakTabelSelecteerbaar: function(tableid, focusregel) {
        Macs.CursusGegevens._selecteerRegel(tableid, focusregel);

/*
		Macs.CursusGegevens.STTGrid = new KeyTable({
			'table': $table[0],
			'tabIndex': 130,
			'selectRow': true,
			'keepSelection': true,
			'continueScroll': false,
			'initScroll': true,
			//            'focus': [1,0],
			'form': true
		});

		Macs.CursusGegevens.STTGrid.setFocus(Macs.CursusGegevens.STTGrid.cellFromCoords(1, focusregel));
*/
		/* Selecteer de rij wanneer gebruiker op Enter drukt (action)
		$('tbody td', $table).each(function(elem) {
			Macs.CursusGegevens.STTGrid.event.action(this, function(nCell) {
				var row = $(this.nCell).parents("tr");
				var pos = $('table.sttrijen tbody tr').index(row[0]);
				Macs.CursusGegevens._wijzigRegel(this.nCell);
			});
		});
		*/
	},
	_selecteerRegel: function(tableid, pos) {
		/* Highlite de nieuwe selectie */
		$($('#'+tableid+' tbody tr').removeClass('focus').get(pos)).toggleClass('focus', true);
		Macs.CursusGegevens.selecteerRij(tableid);
	},
	_wijzigRegel: function(pos) {
		var Self = Macs.CursusGegevens;

        if (Self.cursusgegevensDataBasis.SOORT_CURSUS != 'A') {
            var tabelnaam = Self.huidigeItemsTabelNaam();
            Macs.CursusGegevens._selecteerRegel(tabelnaam, pos);
            if (tabelnaam == 'stttabel') {
                Macs.CursusGegevens.wijzigSTT();
            } else {
                Macs.CursusGegevens.wijzigBTT();
            }
        }
	},
	_wijzigCohort: function(pos) {
		var Self = Macs.CursusGegevens;

	    var tabelnaam = Self.huidigeItemsTabelNaam();
        Macs.CursusGegevens._selecteerRegel(tabelnaam, pos);

        var item = Self.cursusgegevensDataCohorten[pos];
        Self.wijzigCohort(item);
	},
	wijzigCohort: function(item) {
        if (typeof item != 'undefined') {
            $("#_fldCOHORT_DATUMGELDIGVANAF").val(item.COHORT_DATUMGELDIGVANAF||item.DATUMGELDIGVANAF);
            $("#_fldCOHORT_DATUMGELDIGTM").val(item.COHORT_DATUMGELDIGTM||item.DATUMGELDIGTM);
            $("#_fldCOHORT_COHORT").val(item.COHORT);
            $("#_fldCOHORT_DOSSIERCOHORT").val(item.DOSSIERCOHORT);
            $("#_fldCOHORT_CNUMMER").val(item.CNUMMER);
            $("#_fldCOHORT_KDCODE").val(item.KDCODE);
        }
	},
	bewaarCohortLokaal: function(e) {
		var Self = Macs.CursusGegevens;

        var row = Self.huidigeRij();
        var rowObject = Self.getRijObject(row);

        if (rowObject) {
    //        rowObject.DATUMGELDIGVANAF = $("#_fldCOHORT_DATUMGELDIGVANAF").val();
    //        rowObject.DATUMGELDIGTM = $("#_fldCOHORT_DATUMGELDIGVANAF").val();
            rowObject.COHORT = $("#_fldCOHORT_COHORT").val();
            rowObject.DOSSIERCOHORT = $("#_fldCOHORT_DOSSIERCOHORT").val();
            rowObject.CNUMMER = $("#_fldCOHORT_CNUMMER").val();
            rowObject.KDCODE = $("#_fldCOHORT_KDCODE").val();
            rowObject.DOSSIERCOHORT = $("#_fldCOHORT_DOSSIERCOHORT").val();

            Polaris.Form.userIsEditing = true;
        }
	},
	vulCursusCodes: function(cursusgroep, inclusiefinactief) {
		var Self = Macs.CursusGegevens;

		$.getJSON(_servicequery, {
            'func': 'cursuscodes'
            ,'cursusgroep': cursusgroep
            ,'inclusiefinactief': inclusiefinactief?'true':'false'
        }, function(data, textStatus) {
//			$('.searchresult .content').removeClass('loading');

			/* Leg data vast als lokale/globale var */
			Self.cursuscodesData = data;

			if (textStatus == 'success') {
			    var defaultcursus = $.cookies.get('Macs.Cursusgegevens.cursuscode') || "";
			    var optgroep = null;
//			    $("#gepindecursus").val() || "";
                var optie = new Option('< Nieuwe cursus toevoegen... >', '' );
                $("#cursuscodes").empty().append( optie );

                $(Self.cursuscodesData).each(function(i, elem) {
                    if (typeof Self.cursuscodesData[i-1] != 'undefined'
                    && Self.cursuscodesData[i-1].CURSUSCODE == Self.cursuscodesData[i].ISCOHORTCURSUSVAN
                    && optgroep == null) {
                        optgroep = document.createElement('optgroup');
                        $(optgroep).prop('label', 'Cohorten');
                    }
                    if (optgroep) {
                        $("#cursuscodes").append( optgroep );
                        var optie = new Option(' - ' + elem.CURSUSCODE, elem.CURSUSCODE, false, defaultcursus ==  elem.CURSUSCODE);
                        $(optgroep).append( optie );
                    } else {
                        var optie = new Option(elem.CURSUSCODE + ' ' + elem.CURSUSNAAM, elem.CURSUSCODE, false, defaultcursus ==  elem.CURSUSCODE );
                        $("#cursuscodes").append( optie );
                    }

                    if (typeof Self.cursuscodesData[i+1] != 'undefined'
                    && Self.cursuscodesData[i+1].ISCOHORTCURSUSVAN == null) {
                        optgroep = null;
                    }
                });
                if ( $("#cursuscodes option:selected").val() == '' ) {
                    $("#cursuscodes option:eq(1)").prop('selected', 'selected');
                }
			} else {
				Polaris.Base.modalMessage($.t('nha.macs.foutZoekActie') + textStatus);
			}

			$("#cursuscodes").change();
		});
	},
	clearAlleGegevens: function() {
		var Self = Macs.CursusGegevens;
		Self.clearBasisGegevens();
		Self.clearSTTGegevens();
		Self.clearBTTGegevens();
		Self.clearCohortGegevens();
	},
	clearBasisGegevens: function() {
		var Self = Macs.CursusGegevens;
        Self.cursusgegevensDataBasis = [];
        $("#page_bg input:text").val('');
        $("#page_bg input:radio").prop('checked', false);
	},
	clearSTTGegevens: function() {
		var Self = Macs.CursusGegevens;
        Self.cursusgegevensDataSTT = [];
        Self.htmlSTT = Self.htmlSTT.render(Self.cursusgegevensDataSTT, Self.templateSTT);
	},
	clearBTTGegevens: function() {
		var Self = Macs.CursusGegevens;
        Self.cursusgegevensDataBTT = [];
        Self.htmlBTT = Self.htmlBTT.render(Self.cursusgegevensDataBTT, Self.templateBTT);
	},
	clearCohortGegevens: function() {
		var Self = Macs.CursusGegevens;
        Self.cursusgegevensDataCohorten = [];
        Self.htmlCohorten = Self.htmlCohorten.render(Self.cursusgegevensDataCohorten, Self.templateCohorten);
        $("#page_ch :input").val('');

//        Polaris.Visual.showTab('#pagetabs', '#page_bg');
	},
	_getGegevens: function(cursuscode, lijst, callback) {
		var Self = Macs.CursusGegevens;

        if (lijst == 'allecursusgegevens') {
            Self.clearAlleGegevens();
        } else if (lijst == 'cursusbasisgegevens') {
            Self.clearBasisGegevens();
        } else if (lijst == 'cursuscohorten') {
            Self.clearCohortGegevens();
        } else if (lijst == 'sttgegevens') {
            Self.clearSTTGegevens();
        } else {
            Self.clearBTTGegevens();
        }

		$.getJSON(_servicequery, {
            'func': lijst,
            'cursuscode': cursuscode,
            'inclusiefinactief': $("#_fldINCLUSIEFINACTIEF").prop('checked') ? 'true':'false'
        }, function(data, textStatus) {
			/* Leg data vast als lokale/globale var */
			if (lijst == 'allecursusgegevens') {
    			Self.cursusgegevensDataBasis = data.Basisgegevens[0];
    			Self.cursusgegevensDataCohorten = data.Cohortgegevens;
    			Self.cursusgegevensDataSTT = data.STTgegevens;
    			Self.cursusgegevensDataBTT = data.BTTgegevens;
    		} else if (lijst == 'cursusbasisgegevens') {
    			Self.cursusgegevensDataBasis = data;
    		} else if (lijst == 'cursuscohorten') {
    			Self.cursusgegevensDataCohorten = data;
    		} else if (lijst == 'sttgegevens') {
    			Self.cursusgegevensDataSTT = data;
    		} else {
    			Self.cursusgegevensDataBTT = data;
    		}
			if (textStatus == 'success') {
    			//Macs.SharedLib.ajaxFeedback('Gegevens geladen');
			    callback();
    			if (data.LESSEN && data.LESSEN.length == 0) {
    			    alert('Er zijn nog geen cursuslessen gemaakt.');
    			}
			} else {
				Polaris.Base.modalMessage($.t('nha.macs.foutZoekActie') + textStatus);
			}
		});
	},
	getAlleCursusGegevens: function(cursuscode, callback) {
		Macs.CursusGegevens._getGegevens(cursuscode, 'allecursusgegevens', callback);
	},
	getCursusGegevens: function(cursuscode, callback) {
		Macs.CursusGegevens._getGegevens(cursuscode, 'cursusbasisgegevens', callback);
	},
	getSTTGegevens: function(cursuscode, callback) {
		Macs.CursusGegevens._getGegevens(cursuscode, 'sttgegevens', callback);
	},
	getBTTGegevens: function(cursuscode, callback) {
		Macs.CursusGegevens._getGegevens(cursuscode, 'bttgegevens', callback);
	},
	getCohortGegevens: function(cursuscode, callback) {
		Macs.CursusGegevens._getGegevens(cursuscode, 'cursuscohorten', callback);
	},
	vulCursusBasisGegevens: function() {
		var Self = Macs.CursusGegevens;

        $(Self.cursusVelden).each(function(i, elem) {
            if (elem.indexOf('@radio') > 0) {
                var radioElem = elem.replace('@radio','');
                $("input[name="+radioElem+"]").filter("[value="+Self.cursusgegevensDataBasis[radioElem]+"]").prop('checked', true);
            } else {
                $("#_fld"+elem).val(Self.cursusgegevensDataBasis[elem]||"");
            }
        });

        $("#_btnWijzigCursusCode").toggleClass('readonly', Self.cursusgegevensDataBasis.AANTALINSCHRIJVINGEN == 0 );
        $("#_hdnRecordID").val(Polaris.Base.customUrlEncode(Self.cursusgegevensDataBasis.PLR__RECORDID));

        Polaris.Form.adjustLabelWidth($('#page_bg'));
	},
	vulLijst: function(lijst) {
		var Self = Macs.CursusGegevens;

//		cursuscode = cursuscode || $("#cursuscodes").val();
        var tabelnaam = '';
        var ds = '';

        if (lijst == 'STT') {
            tabelnaam = 'stttabel';
            ds = Self.cursusgegevensDataSTT.STT;
        } else if (lijst == 'BTT') {
            tabelnaam = 'btttabel';
            ds = Self.cursusgegevensDataBTT.BTT;
        } else if (lijst == 'COHORT') {
            tabelnaam = 'chtabel';
            ds = Self.cursusgegevensDataCohorten;
        }
    	$('#'+tabelnaam+' tbody').empty();
        Self.refreshTabel(ds, tabelnaam);
	},
	initCohortEvents: function() {
		var Self = Macs.CursusGegevens;

        /* WORDT EVEN NIET GEBRUIKT
        $("button.wijzigPeriode").click(function(e) {
            e.preventDefault();
            var pos = $("button.wijzigPeriode").index(e.target);
            var item = Self.cursusgegevensDataCohorten[pos];
        });
        */

        $("button.toonCohort").click(function(e) {
            e.preventDefault();
            var pos = $("button.toonCohort").index(e.target);
            var item = Self.cursusgegevensDataCohorten[pos];
            $("#cursuscodes").val(item.CURSUSCODE_CH).change();
        });
	},
	vulCohorten: function() {
		var Self = Macs.CursusGegevens;

		Self.vulLijst('COHORT');
		Self.initCohortEvents();

		var huidigeRij = Self.huidigeRij();
		var item = Self.getGeselecteerdeItem();
   		Macs.CursusGegevens.wijzigCohort(item);
	},
	vulSTT: function() {
		Macs.CursusGegevens.vulLijst('STT');
	},
	vulBTT: function() {
		Macs.CursusGegevens.vulLijst('BTT');
	},
	vulAlleCursusGegevens: function() {
		var Self = Macs.CursusGegevens;

	    Macs.CursusGegevens.vulCursusBasisGegevens();
		Macs.CursusGegevens.vulLijst('STT');
		Macs.CursusGegevens.vulLijst('BTT');

        // Toon of verberg de Cohort paginatab
        // Alleen Cohort paginatab tonen indien het een Algemene cursus is
        if (Self.cursusgegevensDataBasis.SOORT_CURSUS == 'C') {
            $(".cohortenlijst").hide();
            $("#CH").text('Cohortgegevens');
            $("#cohortColumnGroup").css({'float':'none'});
       		Self.wijzigCohort(Self.cursusgegevensDataBasis);
       		$("#_fldDATUMGELDIGVANAF,#_fldDATUMGELDIGTM").addClass("readonly");
            //overbodig veld 7-08-2014 $("#_rowGEBUFFERDJANEE").show();
        } else {
            $(".cohortenlijst").show();
            $("#CH").text('Cohorten');
            $("#cohortColumnGroup").css({'float':'right'});
    	    Self.vulCohorten();
       		$("#_fldDATUMGELDIGVANAF,#_fldDATUMGELDIGTM").removeClass("readonly");
            //overbodig veld 7-08-2014 $("#_rowGEBUFFERDJANEE").hide();
        }
   		$("#_fldCNUMMER").toggleClass("readonly", Self.cursusgegevensDataBasis.SOORT_CURSUS != null);

        // BTT en STT zijn alleen wijzigbaar indien het GEEN algemene cursuscode is
        $("#page_stt .buttons, #page_btt .buttons").toggle(Self.cursusgegevensDataBasis.SOORT_CURSUS != 'A');
        $("#page_stt .opmerking, #page_btt .opmerking").toggle(Self.cursusgegevensDataBasis.SOORT_CURSUS == 'A');

        Polaris.Form.initializeDatePickers();
	},
	refreshTabel: function(dataset, tabelnaam, selected) {
		var Self = Macs.CursusGegevens;

        selected = selected || 0;

        /* Vul de tabel op basis van de template */
        if (tabelnaam == 'stttabel') {
            Self.htmlSTT = Self.htmlSTT.render(dataset, Self.templateSTT);
        } else if (tabelnaam == 'btttabel') {
            Self.htmlBTT = Self.htmlBTT.render(dataset, Self.templateBTT);
        } else {
            Self.htmlCohorten = Self.htmlCohorten.render(dataset, Self.templateCohorten);
        }
        Polaris.Dataview.enableTooltips($("#"+tabelnaam), {contentPosition: 'rightStatic'});
        Self.maakTabelSelecteerbaar(Self.huidigeItemsTabelNaam(), selected);
	},
	bewaarRij: function() {
		var Self = Macs.CursusGegevens;
	    var $row = $(this).parents('tr');
	    var item = Self.getRijObject($row);
  	    var tabelnaam = Self.huidigeItemsTabelNaam();

        if (tabelnaam == 'stttabel') {
            item.STUDIETEMPOCODE = $("select.studietempo", $row).val();
            item.STUDIETEMPONAAM = $("select.studietempo option:selected", $row).text();
            item.TERMIJNNR = $("input.termijn", $row).val();
            item.BEGINLES = $("select.beginles", $row).val();
            item.EINDLES = $("select.eindles", $row).val();
        } else if (tabelnaam == 'btttabel') {
            item.AANTALTERMIJNEN = $("input.aantaltermijnen", $row).val();
            item.ZELFSTUDIEJANEE = $("input.zelfstudiejanee:checked", $row).val();
            item.BEDRAGINEUROS = $("input.bedrag", $row).val();
            item.BEDRAGINEUROS_BELGIE = $("input.bedragbe", $row).val();
            item.BEDRAGINEUROS_BELGIE_OORSPR = $("input.bedragbeoorspr", $row).val();
        }
	},
	huidigePageTab: function() {
	    return Polaris.Visual.activeTab('#pagetabs').prop('id');
	},
	huidigeItemsDataSet: function() {
		var Self = Macs.CursusGegevens;

	    var ds;
	    if (Self.huidigePageTab() == 'STT') {
	        ds = Macs.CursusGegevens.cursusgegevensDataSTT.STT;
	    } else if (Self.huidigePageTab() == 'BTT') {
	        ds = Macs.CursusGegevens.cursusgegevensDataBTT.BTT;
	    } else if (Self.huidigePageTab() == 'CH') {
	        ds = Macs.CursusGegevens.cursusgegevensDataCohorten;
	    }
	    return ds;
	},
	huidigeItemsTabelNaam: function() {
		var Self = Macs.CursusGegevens;

	    var tn;
	    var pageTab = Self.huidigePageTab();
	    if (pageTab == 'STT') {
	        tn = 'stttabel';
	    } else if (pageTab == 'BTT') {
	        tn = 'btttabel';
	    } else if (pageTab == 'CH') {
	        tn = 'chtabel';
	    }
	    return tn;
	},
	voegRijToe: function(e) {
		var Self = Macs.CursusGegevens;

	    e.preventDefault();

	    var ds = Self.huidigeItemsDataSet();
	    var tabelnaam = Self.huidigeItemsTabelNaam();

	    var vorigerij = [];
	    if (ds && ds.length > 0) {
	        vorigerij = ds[ds.length-1];
	    }

	    if (tabelnaam == 'stttabel') {
            var eindles = parseInt(vorigerij.EINDLES,10)+1;
            if (!$.inArray(eindles,Self.cursusgegevensDataSTT.LESSEN)) eindles = vorigerij.EINDLES;
            ds.push({
                'CURSUSCODE_STT':$("#cursuscodes").val(),
                'STUDIETEMPOCODE':vorigerij.STUDIETEMPOCODE||'',
                'STUDIETEMPONAAM':vorigerij.STUDIETEMPONAAM||'',
                'TERMIJNNR':vorigerij.TERMIJNNR||'',
                'BEGINLES':eindles||'',
                'EINDLES':eindles||''
            });
        } else {
            ds.push({
                'CURSUSCODE_BTT':$("#cursuscodes").val(),
                'AANTALTERMIJNEN':vorigerij.AANTALTERMIJNEN||'',
                'ZELFSTUDIEJANEE':vorigerij.ZELFSTUDIEJANEE||'',
                'BEDRAGINEUROS':vorigerij.BEDRAGINEUROS||'',
                'BEDRAGINEUROS_BELGIE':vorigerij.BEDRAGINEUROS_BELGIE||'',
                'BEDRAGINEUROS_BELGIE_OORSPR':vorigerij.BEDRAGINEUROS_BELGIE_OORSPR||''
            });
        }

        Self.refreshTabel(ds, tabelnaam);
        Self._wijzigRegel(ds.length-1);
	},
	verwijderRij: function(e) {
		var Self = Macs.CursusGegevens;
	    e.preventDefault();

        var item = Self.getGeselecteerdeItem();
        if (item) {
    	    var ds = Self.huidigeItemsDataSet();
    	    var tabelnaam = Self.huidigeItemsTabelNaam();
            var pos = ds.indexOf(item);
            ds.splice(pos, 1);
            if (pos > ds.length - 1) {
                pos = ds.length - 1;
            }
            Self.refreshTabel(ds, tabelnaam, pos);
        }
	},
	voegCohortRijToe: function(e) {
		var Self = Macs.CursusGegevens;

	    e.preventDefault();
        Polaris.Window.showModuleForm('/modules/nha/module.nha_macs/html/cursusgegevensplus_cohortcursustoevoegen.tpl.php', Self.bewaarNieuweCohort, function() {
            var item = Self.getGeselecteerdeItem();
            if (item) {
                $("#_fldCOHORT_JAARGELDIGVANAF").val(parseInt(item.JAARVANAF)+1);
                $("#_fldCOHORT_JAARGELDIGTM").val(parseInt(item.JAARTM)+1);
            } else {
                $("#_fldCOHORT_NEEMINFOVANALGEMENE").prop('checked', true);
            }
            $("#cohortCursusToevoegen h1").text('Nieuwe cohortcursus toevoegen');
        });
	},
    KopieerCohortRij: function(e) {
        var Self = Macs.CursusGegevens;

        e.preventDefault();

        var item = Self.getGeselecteerdeItem();
        if (item) {
            Polaris.Window.showModuleForm('/modules/nha/module.nha_macs/html/cursusgegevensplus_cohortcursustoevoegen.tpl.php', Self.kopieerCohortCursus, function() {
                $("#_rowCOHORT_NEEMINFOVANALGEMENE").hide();
                $("#_fldCOHORT_NEEMINFOVANALGEMENE").prop('checked', false);
                $("#cohortCursusToevoegen h1").text('Cohortcursus kopiëren');
                $("#_fldCOHORT_JAARGELDIGVANAF").val(parseInt(item.JAARVANAF)+1);
                $("#_fldCOHORT_JAARGELDIGTM").val(parseInt(item.JAARTM)+1);
            });
        } else {
            Polaris.Base.modalMessage('U dient eerst de cohort te selecteren die u wilt kopiëren.', 1000);
        }
    },
	verwijderCohortRij: function(e) {
		var Self = Macs.CursusGegevens;

	    e.preventDefault();
        var item = Self.getGeselecteerdeItem();
        if (item) {
            if (confirm($.t('nha.macs.bevestigingVerwijderCohortCursus'))) {
                Self.verwijderCohort(item);
            }
        }
	},
	laadItemListOpnieuw: function(e) {
		var Self = Macs.CursusGegevens;
	    e.preventDefault();

  	    var tabelnaam = Self.huidigeItemsTabelNaam();
	    if (confirm($.t('nha.macs.bevestigingRefresh'))) {
	        if (tabelnaam == 'stttabel') {
                Self.getSTTGegevens($("#cursuscodes").val(), Self.vulSTT);
            } else if (tabelnaam == 'btttabel') {
                Self.getBTTGegevens($("#cursuscodes").val(), Self.vulBTT);
            } else {
                Self.getCohortGegevens($("#cursuscodes").val(), Self.vulCohorten);
            }
    	}
	},
	gegevensInOrde: function() {
        var $dataform = $("#dataform");
        /*
        $dataform.validate({
            rules: {
                MATERIAALVERWACHTDATUM: {
                    required: {
                        depends: function() {
                            return ($("input[name=MATERIAALCOMPLEETJANEE]:checked").val() == 'N' );
                        }
                    }
                }
            }
        });
        */
        var valid = $dataform.valid();
        if (!valid) {
            Polaris.Base.modalMessage('U heeft ' + $dataform.validate().numberOfInvalids() + ' velden niet correct ingevuld.', 1000, false);
            // zet focus op eerste foutieve veld
            //            $("#dataform input:blank").focus();
        }

        return valid;
	},
	convertToPostData: function(objects) {
	    data = [];
	    $(objects).each(function(obj) {
	        for(prop in this) {
	            data.push({name: prop+'[]', value: this[prop]});
	        }
	    });
	    return data;
	},
	bewaarCursus: function(e) {
		var Self = Macs.CursusGegevens;

	    e.preventDefault();
	    if (Self.gegevensInOrde()) {
			Polaris.Base.modalMessage("Cursusgegevens worden opgeslagen.", 0);

            var senddata = [];
            var nieuweCursusCode = $("#_fldCURSUSCODE").val();

            senddata.push({name: '_hdnFORMTYPE', value: 'cursusgegevens'});
            senddata.push({name: '_hdnProcessedByModule', value: 'true'});
            senddata.push({name: 'JUISTSTUDIETEMPO', value: Self.cursusgegevensDataBasis.JUISTSTUDIETEMPO});
            $($("#dataform").serializeArray()).each(function(i, obj) {
                senddata.push({name: obj.name, value: obj.value});
            });
            $.merge(senddata, Self.convertToPostData(Self.cursusgegevensDataSTT.STT));
            $.merge(senddata, Self.convertToPostData(Self.cursusgegevensDataBTT.BTT));
            $.merge(senddata, Self.convertToPostData(Self.cursusgegevensDataCohorten));

            Polaris.Ajax.postJSON(_servicequery, senddata, "Cursus wordt opgeslagen.", 'Cursus opgeslagen.', function () {
                $("#pinrecord_single").click();
                $.cookies.set('Macs.Cursusgegevens.cursuscode', nieuweCursusCode);
                Self.vulCursusCodes($("#cursusgroepen").val(), $("#_fldINCLUSIEFINACTIEF").prop('checked'));
            });
	    }
	},
	verwijderCursus: function(e) {
	    var Self = Macs.CursusGegevens;

        e.preventDefault();
        if ($("#_fldAANTALINSCHRIJVINGEN").val() == 0) {

            if (confirm($.t('nha.macs.bevestigingVerwijderCursus'))) {
                var senddata = [];
                senddata.push({name: '_hdnFORMTYPE', value: 'verwijdercursus'});
                senddata.push({name: '_hdnProcessedByModule', value: 'true'});
                senddata.push({name: 'CURSUSCODE', value: $("#_fldCURSUSCODE").val()});

                Polaris.Ajax.postJSON(_servicequery, senddata, "Cursus wordt verwijderd.", 'Cursus '+ $("#_fldCURSUSCODE").val() +' is verwijderd', function () {
                    Self.vulCursusCodes($("#cursusgroepen").val(), $("#_fldINCLUSIEFINACTIEF").prop('checked'));
                });
            }
        } else {
            alert($.t('nha.macs.cursusHeeftInschrijvingen'));
        }
	},
	cursusCodeGewijzigd: function(e) {
	    var Self = Macs.CursusGegevens;

        var cursuscode = $(e.target).val();

        if (cursuscode == '') {
            Self.cursusRecordToevoegen();
        } else {
            // de status is van een gewoon record is edit
            $("input[name=_hdnState]").val('edit');
            $("#_hdnCURSUSCODE").val(cursuscode);
            var cursus = Macs.SharedLib.filter(Self.cursuscodesData,
             function(item) {
                if (item['CURSUSCODE'] == cursuscode) {
                    return true;
                }
                return false;
            });
            if (cursus.length == 0) {
                return null;
            } else {
                $(":input[name=AANTALSEALPAKKETTEN]").val(cursus[0].AANTALSEALPAKKETTEN);
            }
            $.cookies.set('Macs.Cursusgegevens.cursuscode', cursuscode);


            Self.getAlleCursusGegevens($("#cursuscodes").val(), Self.vulAlleCursusGegevens);

            if (Self.huidigePageTab() == 'CKL') {
                Self.runCheckList();
            }
        }
	},
    onSelectRowHandler: function(nCell, event) {
//        event.preventDefault();
        var cursusGroep = $(nCell).parents('tr').find('._fldCURSUSGROEP').val();
        var cursusCode = $(nCell).parents('tr').find('._fldCURSUSCODE').val();
        $.cookies.set('Macs.Cursusgegevens.cursuscode', cursusCode);

        $("#cursusgroepen").val(cursusGroep).change();
        $("#scherm_cursusgegevens").show();
        $("#scherm_cursusoverzicht").hide();
    },
    cursusRecordToevoegen: function() {
	    var Self = Macs.CursusGegevens;
        Self.switchView(false);
        // zorg dat het juist menuitem wordt geselecteerd
        $("body").removeClass('allitems').addClass('insert');
        // nieuw record klaarzetten om te insert-en
        $("input[name=_hdnState]").val('insert');

        Self.clearAlleGegevens();
        $("#_fldCURSUSCODE").removeClass('readonly').focus();

        // default waardes in cursus record
        $("#cursuscodes").val('');
        var row = Polaris.Dataview.focusedRow();
        var groepcode = $("._fldCURSUSGROEP", row).val();
        if (!isUndefined(groepcode)) {
            $("#cursusgroepen").val(groepcode);
            $("#_fldCURSUSGROEP").val(groepcode);
        } else {
            $("#_fldCURSUSGROEP").val($("#cursusgroepen").val());
        }
        $("#_fldDATUMGELDIGVANAF").val(Polaris.Base.dateToString(new Date()));
        $("input.default:radio").prop('checked', true);
    },
    annuleerForm: function(e) {
	    var Self = Macs.CursusGegevens;

        e.preventDefault();
        Self.vulAlleCursusGegevens();
    },
	bewaarNieuwePeriode: function() {
	    var formData = [];

	    var soortCohort = $("#_fldSOORTCOHORT").val();
	    var jaarPeriode = $("#_fldCOHORT_JAARPERIODE").val();
        formData.push({name: '_hdnProcessedByModule', value: 'true'});
        formData.push({name: '_hdnFORMTYPE', value: 'cohort'});
        formData.push({name: 'soortcohort', value: soortCohort});
        formData.push({name: 'jaarperiode', value: jaarPeriode});

        Polaris.Ajax.postJSON(_servicequery, formData, null, 'Periode aangemaakt', function () {
            if (soortCohort == 'O') {
                $select = $('#_fldCOHORT_COHORT');
            } else {
                $select = $('#_fldCOHORT_DOSSIERCOHORT');
            }
            $select.find("option:eq(0)").after($('<option>', {value: jaarPeriode, text: jaarPeriode, selected: 'selected'}));
        });
	},
	cohortCursusOpslaan: function(soortactie, cursuscode) {
		var Self = Macs.CursusGegevens;

	    var formData = [];

	    var jaarGeldigVanaf = $("#_fldCOHORT_JAARGELDIGVANAF").val();
	    var jaarGeldigTm = $("#_fldCOHORT_JAARGELDIGTM").val();
        var neemInfoOver = $("#_fldCOHORT_NEEMINFOVANALGEMENE").prop('checked');
        log(neemInfoOver);
        formData.push({name: '_hdnProcessedByModule', value: 'true'});
        formData.push({name: '_hdnFORMTYPE', value: soortactie});
        formData.push({name: 'CURSUSCODE', value: cursuscode});
        formData.push({name: 'JAARGELDIGVANAF', value: jaarGeldigVanaf});
        formData.push({name: 'JAARGELDIGTM', value: jaarGeldigTm});
        formData.push({name: 'NEEMINFOVANALGEMENE', value: neemInfoOver ? 'J' : 'N'});

        Polaris.Ajax.postJSON(_servicequery, formData, 'Cohort wordt opgeslagen.', 'Cohortcursus aangemaakt', function () {
            Self.getCohortGegevens($("#cursuscodes").val(), Self.vulCohorten);
        });
	},
    bewaarNieuweCohort: function() {
        var Self = Macs.CursusGegevens;

        Self.cohortCursusOpslaan('cohortcursus', $("#_hdnCURSUSCODE").val());
    },
    kopieerCohortCursus: function() {
        var Self = Macs.CursusGegevens;

        var item = Self.getGeselecteerdeItem();
        if (item) {
            Self.cohortCursusOpslaan('kopieercohortcursus', item.CURSUSCODE_CH);
        }
        return true;
    },
	verwijderCohort: function(item) {
		var Self = Macs.CursusGegevens;

	    var formData = [];
        formData.push({name: '_hdnProcessedByModule', value: 'true'});
        formData.push({name: '_hdnFORMTYPE', value: 'verwijdercohort'});
        formData.push({name: 'CURSUSCODE', value: item.CURSUSCODE_CH});

        Polaris.Base.modalMessage("Cohort wordt verwijderd.", 0);

        Polaris.Ajax.postJSON(_servicequery, formData, 'Cohort wordt verwijderd.', 'Cohortcursus verwijderd', function () {
            Self.getCohortGegevens($("#cursuscodes").val(), Self.vulCohorten);
        });
	},
	runCheckList: function() {
	    $("#cursuschecklist").empty();
	    jQuery.getJSON(_servicequery, {
	        func: 'checklist',
	        cursuscode: $("#_hdnCURSUSCODE").val()
	    }, function(data, textStatus) {
	        if (textStatus == 'success') {
	            $(data.checks).each(function(i, elem) {
	                var className = elem.status?'ok':'notok';
    	            $("#cursuschecklist").append($("<li>"+elem.check+"</li>").addClass(className));
	            });
	        }
	    });
	},
	switchView: function(overzicht) {
        if (overzicht) {
            $("#scherm_cursusgegevens").hide();
            $("#scherm_cursusoverzicht").show();
        } else {
            $("#scherm_cursusgegevens").show();
            $("#scherm_cursusoverzicht").hide();
        }
	}
};

$(document).ready(function() {

	Macs.CursusGegevens.initialiseer();

});
