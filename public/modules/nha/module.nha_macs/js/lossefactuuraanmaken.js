Macs.LosseFactuurAanmaken = {
	betalingsverzoeken: null,
	initialiseer: function() {
		var Self = Macs.LosseFactuurAanmaken;

		var directive = {
			"tr.betalingsverzoek": {
				"betalingsverzoek <- ": {
					".PLR__RECORDID@value": "betalingsverzoek.PLR__RECORDID",
					".TERMIJN": "betalingsverzoek.TERMIJNNR",
					".KENMERK": "betalingsverzoek.BETALINGSKENMERK",
					".STATUS": "betalingsverzoek.STATUS",
					".BEDRAG": function(arg) {
						return Macs.SharedLib.formatFloat(arg.item.BEDRAGINEUROS, 2);
					},
					".BEDRAGBETAALD": function(arg) {
						return Macs.SharedLib.formatFloat(arg.item.BEDRAGBETAALDINEUROS, 2);
					},
					".TEBETALEN": function(arg) {
						return Macs.SharedLib.formatFloat(arg.item.BEDRAGNOGTEBETALENINEUROS, 2);
					},
					".BETAALWIJZE": "betalingsverzoek.BETAALWIJZECODE",
					".VERZENDDATUM": "betalingsverzoek.VERZENDDATUM",
					".VERVALDATA": "#{betalingsverzoek.CURSISTVERVALDATUM} / #{betalingsverzoek.SYSTEEMVERVALDATUM}",
					".HERINNERING@checked": function(arg) {
						return arg.item.HERINNERINGJANEE == 'J' ? 'checked' : '';
					},
					".REGELING@checked": function(arg) {
						return arg.item.BETALINGSREGELINGJANEE == 'J' ? 'checked' : '';
					},
					".FACTUUR@value": "betalingsverzoek.PLR__RECORDID"
				}
			}
		};

		Self.htmlBetalingsverzoeken = $("#betalingsverzoekentabel");
		Self.templateBetalingsverzoeken = Self.htmlBetalingsverzoeken.compile(directive);

		// filter de verzoeken-set op basis van de aangeklikte filter(s)
		$("#betalingsverzoek_filters input").click(function(e) {
			Macs.LosseFactuurAanmaken.vulBetalingsverzoeken();
		});

        /* EVEN GEEN MARKUP
        $(function() {
            // Initialize the editor.
            // Callback function can be passed and executed after full instance creation.
            CKEDITOR.config.disableNativeSpellChecker = true;
            $('#_fldINHOUDBRIEF').ckeditor( {
                toolbar:'Basic',
                disableNativeSpellChecker:true,
                scayt_autoStartup:false,
            } );
        });
        */

		$(window).resize(Macs.LosseFactuurAanmaken.resize).trigger('resize');
		$(".pagetabcontainer a").click(Macs.LosseFactuurAanmaken.resize);
	},
	haalStandaardBrievenOp: function() {
	    var url = Macs.SharedLib._macs_servicequery.replace("%construct%",'briefselectie').replace('const','form');
		$.getJSON(url,
		function(data, textStatus) {
			if (textStatus == 'success') {
			    $("#_fldSTANDAARDBRIEF").addOptions(data,'VERZENDONDERDEELCODE','VERZENDONDERDEELCODE', 'STANDAARD');
			} else {
				alert('The letters could not be retrieved. Error: ' + textStatus);
			}
		});
	},
	haalBriefInhoudOp: function(verzendonderdeelcode) {
	    if (typeof verzendonderdeelcode != 'undefined' && verzendonderdeelcode != ''
        && (Macs.LosseFactuurAanmaken.betalingsverzoeken != null)) {

            $.getJSON(_servicequery, {
                'func': 'inhoudbrief',
                'verzendonderdeelcode': verzendonderdeelcode,
                'inschrijfnr': Macs.LosseFactuurAanmaken.betalingsverzoeken[0].INSCHRIJFNR
            },
            function(data, textStatus) {
                if (textStatus == 'success') {
                    $("#_fldINHOUDBRIEF").val(data.INHOUDBRIEF_PLR?data.INHOUDBRIEF_PLR:'');
                } else {
                    alert('The data could not be retrieved. Error: ' + textStatus);
                }
            });
        } else {
            Polaris.Base.modalMessage($.t('nha.macs.geenInschrijvingGevonden'));
        }
	},
	bepaalBetalingsverzoeken: function(inschrijfnr) {
		$.getJSON(_servicequery, {
			'func': 'betalingsverzoeken',
			'inschrijfnr': inschrijfnr
		},
		function(data, textStatus) {
			if (textStatus == 'success') {
				Macs.LosseFactuurAanmaken.betalingsverzoeken = data;
				Macs.LosseFactuurAanmaken.vulBetalingsverzoeken();
        		$(window).trigger('resize');
        		$("#_fldVERZENDDATUM").val(Macs.SharedLib.bepaalHuidigeDatum()).date_input();
        		$("#_fldINZAKE").focus();
			} else {
				alert('The data could not be retrieved. Error: ' + textStatus);
			}
		});
	},
	bepaalTotalen: function(verzoeken) {
		var Self = Macs.LosseFactuurAanmaken;

		var ids = $("#betalingsverzoekentabel tr:has(.FACTUUR:checked)").map(function(i, item) {
			return $(item).find(".PLR__RECORDID").val();
		});

        if (ids.length > 0) {
    	    Polaris.Form.userIsEditing = true;
        	$('#btnSAVE').removeClass('disabled');
        }

		var totaalbedrag = totaalbetaald = totaaltebetalen = 0;
		$(Self.betalingsverzoeken).each(function(i, elem) {
			if ($.inArray(elem.PLR__RECORDID, ids) != -1) {
				totaalbedrag = totaalbedrag + parseFloat(elem.BEDRAGINEUROS);
				totaalbetaald = totaalbetaald + parseFloat(elem.BEDRAGBETAALDINEUROS);
				totaaltebetalen = totaaltebetalen + parseFloat(elem.BEDRAGNOGTEBETALENINEUROS);
			}
		});

		// vul de totalen in
		$("#_fldTOTAALBEDRAG").val(Macs.SharedLib.formatFloat(totaalbedrag, 2));
		$("#_fldTOTAALBETAALD").val(Macs.SharedLib.formatFloat(totaalbetaald, 2));
		$("#_fldTOTAALBEDRAGTEBETALEN").val(Macs.SharedLib.formatFloat(totaaltebetalen, 2));

		// vul de nieuwe waarden in
		$("#_fldBEDRAG_NIEUW").val(Macs.SharedLib.formatFloat(totaaltebetalen, 2)).focus();
		$("#_fldBEDRAGBETAALD_NIEUW").text(Macs.SharedLib.formatFloat(0, 2));
		$("#_fldTEBETALEN_NIEUW").text(Macs.SharedLib.formatFloat(0, 2));
	},
	vulBetalingsverzoeken: function() {
		var Self = Macs.LosseFactuurAanmaken;

		if (Self.betalingsverzoeken.length > 0) {
            var filteredVerzoeken = Self.filterVerzoeken(Self.betalingsverzoeken);
            Self.htmlBetalingsverzoeken = Self.htmlBetalingsverzoeken.render(filteredVerzoeken, Self.templateBetalingsverzoeken);

            $("#_fldINSCHRIJFNR_NIEUW").val(Self.betalingsverzoeken[0].INSCHRIJFNR);
            $("#_fldRELATIENR_NIEUW").val(Self.betalingsverzoeken[0].RELATIENR);
            $("#_fldVERZENDDATUM_NIEUW").val(Macs.SharedLib.bepaalHuidigeDatum(1));

            // bereken de totalen wanneer op een Factuur checkbox wordt geklikt
            $("#betalingsverzoekentabel .FACTUUR").click(function(e) {
                Polaris.Dataview._checkCheckBox(this);
                Self.bepaalTotalen(filteredVerzoeken);
            });
        }
	},
	filterVerzoeken: function(items) {
		var filters = $("#betalingsverzoek_filters input").map(function(index, elem) {
			if ($(elem).attr('checked')) return $(elem).val();
		});

		var result = [];
		if (filters.length > 0) {
			$(items).each(function(index, item) {
				var found = $.inArray(item.STATUS, filters);
				items[index].FIL = found;
				if (found != -1) {
					result.push(item);
				}
			});
		} else {
			result = items;
		}
		return result;
	},
	vulInschrijvingenSelect: function(inschrijvingenData) {
	    $("#inschrijvingenselect").change(function() {
	        Macs.LosseFactuurAanmaken.selecteerInschrijving($(this).val());
	    });

		Macs.LosseFactuurAanmaken.htmlBetalingsverzoeken.empty();
		$("#teveelbetaaldfooter :input").val('');
		$("#teveelbetaaldfooter span").text('');

	    $("#inschrijvingenselect").empty().addOptions(inschrijvingenData, "INSCHRIJFNR", "INSCHRIJFNR,CURSUSCODE,CURSUSNAAM");
	    if ($("#_fldZOEKINSCHRIJVING").val() != '')
    	    $("#inschrijvingenselect").val($("#_fldZOEKINSCHRIJVING").val());

	    $("#inschrijvingenselect").change();
	},
	selecteerInschrijving: function(inschrijfnr) {
	    var Self = Macs.LosseFactuurAanmaken;

	    $("#inschrijvingenselect").val(inschrijfnr);
		Self.bepaalBetalingsverzoeken(inschrijfnr);
	},
	zoekRelatie: function() {},
	onZoekRelatie: function(cursistnr) {
		var Self = Macs.LosseFactuurAanmaken;

	    var callback = function(data, textStatus) {
	        if (textStatus == 'success') {
        		Macs.InschrijvingSelectieForm.inschrijvingenData = data;
                Self.vulInschrijvingenSelect(Macs.InschrijvingSelectieForm.inschrijvingenData);
	        }
	    }

	    if (Macs.InschrijvingSelectieForm.relatieData.RELATIENR) {
	        $("#inschrijvingenselect").empty();
            Macs.InschrijvingSelectieForm.zoekInschrijvingen(cursistnr, callback);
        }
	},
	bewaarForm: function() {
		// Ga er vanuit dat er geen problemen met het formulier zijn
		var formValid = true;

		/***
		 * Controleer de standaard velden
		 */
		var validator = $("#dataform").validate({
			onsubmit: false,
			debug: false,
			validateHiddenFields: false
		});
		if (!$("#dataform").valid()) {
			formValid = false;
            var counter = $("#dataform").validate().numberOfInvalids();
            Polaris.Base.modalMessage($.t('validation.incorrect_field', {count: counter}), 1000, false);
		}

		/***
		* Is er een regel aangevinkt?
		*/
		if ($("#betalingsverzoekentabel .FACTUUR:checked").length == 0) {
		    formValid = false;
			Polaris.Base.modalMessage($.t('nha.macs.geenBetalingsVerzoekAangevinkt'), 1000, false);
		}

		if (formValid) {
           	$('#btnSAVE').addClass('disabled');
            Polaris.Ajax.postJSON(_servicequery, $('#dataform'), null, $.t('plr.changes_saved')
            , function(data) {
                validator.resetForm();
                Macs.LosseFactuurAanmaken.bepaalBetalingsverzoeken(Macs.InschrijvingSelectieForm.inschrijvingData.INSCHRIJFNR);
                $("#_fldVERSTUURDJANEE").attr('checked',false);
                Polaris.Form.userIsEditing = false;
            }, function(data) {
               $('#btnSAVE').removeClass('disabled');
            });
		}
	},
	annuleerForm: function() {
		Polaris.Form.userIsEditing = false;

		Macs.InschrijvingSelectieForm.maakInschrijvingLeeg();
		$("#inschrijvingenselect").empty();
		$("#_fldZOEKINSCHRIJVING").focus();
		$("#_fldINZAKE").val('');
		$("#_fldINHOUDBRIEF").val('');
		$("#_fldVERSTUURDJANEE").prop('checked',false);
	},
	resize: function() {
		$bs = $(".scrollable");
		var height = $(window).height() - $bs.offset().top;
		if ($(".buttons").length > 0) height = Math.max(100, height - $(".buttons").outerHeight() - 40);
		$bs.css(($.browser.msie && $.browser.version < 7 ? '' : 'max-') + 'height', height + 'px');
		$bs.height(height);
	}
};

Macs.LosseFactuurAanmaken.haalStandaardBrievenOp();
// GEEN INITIELE BriefTekst//    Macs.LosseFactuurAanmaken.haalBriefInhoudOp('STANDAARD');

$(document).ready(function() {
	Macs.InschrijvingSelectieForm.initialiseer({
		'funcSelecteerInschrijving': Macs.LosseFactuurAanmaken.selecteerInschrijving,
		'funcZoekRelatie': Macs.LosseFactuurAanmaken.zoekRelatie,
		'funcOnZoekRelatie': Macs.LosseFactuurAanmaken.onZoekRelatie
	});

	/* Form actions */
	$('#dataform').submit(function(e) {
		e.preventDefault();
	});
	$('#btnCANCEL').click(function(e) {
		e.preventDefault();
		Macs.LosseFactuurAanmaken.annuleerForm();
	});
	$('#btnSAVE').click(function(e) {
		e.preventDefault();
		if (!$(this).hasClass('disabled')) Macs.LosseFactuurAanmaken.bewaarForm();
	});
	$('#btnSAVE').addClass('disabled');
	$("#_fldINZAKE,#_fldVERZENDDATUM").change(function() {
    	Polaris.Form.userIsEditing = true;
    	$('#btnSAVE').removeClass('disabled');
	});

    $("#_btnWIJZIGBRIEF").click(function(e) {
        $("#briefForm").jqm({modal:true}).jqmShow();
    });

    $("#_fldSTANDAARDBRIEF").change(function(e) {
        $("#_fldINHOUDBRIEF").val('');
    });

    $("#_btnINIT").click(function(e) {
        e.preventDefault();
        Macs.LosseFactuurAanmaken.haalBriefInhoudOp($("#_fldSTANDAARDBRIEF").val());
    });


	// initialiseer het formulier
	Macs.LosseFactuurAanmaken.initialiseer();

	// Maak het formulier leeg (alsof er geannuleerd wordt)
	Macs.LosseFactuurAanmaken.annuleerForm();
});