<?php
## Nieuwe inschrijving
$s_selecteer = 'wählen';
$s_inschrijving = 'Einschreibung';
$s_cursus = 'Kurs';
$s_cursussen = 'Kurs(e)';
$s_zelfstandig = 'Selbst?';
$s_zelfstudie = 'Selbststudie';
$s_studietempo = 'Studietempo';
$s_betaalwijze = 'Zahlung';
$s_betaaltermijnen = 'Zahlungsfristen';
$s_korting = 'Rabatt';
$s_kortingomschrijving = 'Rabatt Beschreib.';
$s_selecteer_cursussen = 'Kurse wählen';
$s_selecteer_inschrijving = 'Einschreibung wählen';
$s_bankgiro = 'Bankkonto';
$s_ondertekend = 'Unterzeichnet';
$s_relatie = 'Kunde';
$s_factuurrelatie = 'Rechnungsstellung';
$s_vooropleiding = 'Vooropleidingen';
$s_vul_opmerking_in = 'Geben Sie den Kommentar.';
$s_statistiek = 'Statistik';
$s_advertentiecode = 'ad-Code';
$s_ontvangstmedium = 'Empfangen Medium';
$s_nieuwsbrief_ontvangen = 'Möchte den Newsletter erhalten?';
$s_geboortedatum = 'Geb.datum';
$s_geboorteplaats = 'Geburtsort';
$s_relatienummer = 'Kundennummer';
$s_postcode_huisnummer = 'PLZ/Hausnr';
$s_termijn_bedrag = 'Rate';
$s_startdatum = 'Startdatum';
$s_premium = 'Prämie';
$s_mbo_hbo_opties = 'MBO/HBO Optionen';
$s_inschrijfgeld = 'Anmeldegebühr';
$s_inschrijfgeld_berekenen = 'Gebühr für diesem Einschreibung berechnen?';
$s_inschrijfgeld_kort = 'Gebühr';

$s_naam = 'Name';
$s_voorletters = 'Initialen';
$s_voornaam = 'Vorname';
$s_achternaam = 'Nachname';
$s_tussenvoegsel = 'Einfügung';
$s_adres = 'Adresse';
$s_straat = 'Straße';
$s_huisnummer = 'Hausnr';
$s_huisnummertoevoeging = 'Suffix';
$s_postcode = 'Postleitzahl';
$s_plaats = 'Stadt';
$s_land = 'Land';
$s_handelingsonbekwaam = 'Geschäftsunfähig';

$s_contact = 'Kontakt';
$s_tel_overdag = 'Tagestelefon';
$s_tel_avond = 'Telefon abends';
$s_tel_mobiel = 'Handy';
$s_email = 'E-Mail';

$s_dhr_mevr = 'Herr/Frau';
$s_dhr = 'Herr';
$s_mevr = 'Frau';

$s_bedrijf = 'Unternehmen';
$s_bedrijfsnaam = 'Firmennamen';
$s_afdeling = 'Abteilung';
$s_btw_nummer = 'USt-IdNr';

$s_omschrijving = 'Beschreibung';
$s_code = 'Kode';
$s_via = 'Durch';

$s_ingeschreven_voor_cursus = 'Registriert für Kurse';

# Artikelverkoop
$s_selecteer_artikelen = 'Artikel(n) auswählen';
$s_artikel = 'Artikel';
$s_aantal = 'Anzahl';
$s_stukprijs = 'Stückpreis';
$s_verw_bijdrage = 'Ents.gebühr';
$s_totaal = 'Gesamte';
$s_subtotaal = 'Zwischensumme';
$s_verzendkosten = 'Versandkosten';
$s_resultaat_artikelverkoop = 'Ergebnis Artikel-Marketing';
$s_bestelde_artikelen = 'Bestellte Artikel';
$s_displaycode = 'AnzeigeCode';
$s_zoek = 'Suche';

# Telemarketing
$s_geplande_datum = 'Geplanter Termin';
$s_begintijd = 'Startzeit';
$s_eindtijd = 'Endzeit';
$s_auto_selectie = 'Automatische Auswahl';
$s_count_records = 'Zeile tellen';
$s_aantal_pers_inschr = 'Persönlich Anzahl Einschreibungen';
$s_medewerker = 'Mitarbeiter';
$s_opmerking = 'Bemerkung';
$s_opmerkingen = 'Bemerkungen';
$s_geplande_actie = 'Geplante Aktion';
$s_vervolgactie = 'Follow-up';
$s_reden = 'Grund';
$s_resultaat = 'Folge';
$s_stuur_studiegids = 'Studienführer';
$s_nabellen = 'Follow-up-Anrufe';
$s_nabellen_hint = 'Indien er morgen teruggebeld dient te worden';
$s_geen_interesse = 'Nicht interessiert';
$s_afspraak_maken = 'Termin vereinb.';
$s_afspraak_maken_hint = 'Indien er bijvoorbeeld volgende week teruggebeld dient te worden';
$s_later_nabellen = 'Später anrufen';
$s_later_nabellen_hint = 'Indien er later op de dag teruggebeld dient te worden (standaard een half uur later)';
$s_studiegids_niet_ontvangen = 'Studienführer nicht empfangen? Erneut zu senden.';
$s_mailing_sturen = 'Mailing schicken';
$s_toelichting = 'Erklärung';
$s_of = 'oder';
$s_geen_historie = 'Keine History';
$s_actiehistorie_aanvraag = 'Actionshistory Anfrag';
$s_aangevraagde_cursus_studiegids = 'Angefragte Kurse/Studienführer';
$s_datum = 'Datum';
$s_inbehandelingdoor = 'In behandeling';

# Betalingsregeling
$s_factuurrelatie_relatie = '(Rechnung)Kunde';
$s_kies_gewenste_inschrijving = 'Wählen Sie die Einschreibung';
$s_inschrijfnr = 'Einschreibnr';
$s_cursuscode = 'Kursnummer';
$s_cursistnr = 'Kursteilnehmernr';
$s_datum_inschrijving = 'Registrierungdatum';
$s_relatienaam = 'Kundename';
$s_opzeggingsdatum = 'Kündigungstermin';
$s_geretourneerd_op = 'Rückgabe am';
$s_aanmaningscode = 'Mahnungcode';
$s_nieuwe_regeling = 'Neue Regelung';
$s_achterstallig_saldo = 'Überfälligen Betrag';
$s_achterstallige_administratiekosten = 'Mahngebühren';
$s_gewenst_maandbedrag = 'Gewünschte Monatsbetrag';
$s_administratiekosten_kwijtschelden = 'Administratiekosten kwijtschelden';
$s_datum_ingang = 'Datum des Eintrags';
$s_maand = 'Monat';

# Betaalwijze
$s_betaaldag = 'Zahltag';

# Inschrijving Termijn Wijzigen
$s_koppelen = 'Kupplung';

# Inschrijving Opzeggen (kort houden)
$s_totale_lesgeld = 'Total gebühr';
$s_lessen = 'Lektionen';
$s_verzonden = 'Verschickt';
$s_te_verzenden = 'Senden an';
$s_bedrag = 'Betrag';
$s_betaald = 'Bezahlt';
$s_te_betalen = 'Zahlenden Betrag';
$s_huidige = 'Aktuelle';
$s_opzegging = 'Absage';
$s_verschil = 'Unterschied';
$s_hoogste = 'Höchste';

# Losse factuur aanmaken
$s_factuur = 'Rechnung';
$s_acceptgiro = 'Giro';
$s_inzake = 'Auf';
$s_verzenddatum = 'Versanddatum';
$s_op_verstuurd_zetten = 'Auf Gesendet setzen';
$s_wijzig_brief = 'Brief ändern';
$s_betalingsverzoeken = 'Bezahlungsgesuchen';
$s_termijn = 'Termin';
$s_kenmerk = 'Typ';
$s_status = 'Status';
$s_vervaldata_cursist_systeem = 'Verfallsdaten Kunde/System';
$s_herinnering = 'Erinnerung';
$s_regeling = 'Regelung';
$s_haal_brieftekst_op = 'Brieftext erhalten';

# Bulk premiums
$s_verwerkleaflets = 'Verarbeiten leaflets';

# Messages
$s_geenRelatieGeselecteerd = 'Sie haben keine Kunde ausgewahlt.';
$s_geenArtikelRelatieGeselecteerd = 'Alleen ingeschreven relaties kunnen artikelen bestellen.';
$s_geenVooropleiding = 'Sie haben keine \'vooropleidingen\' ausgewahlt.';
$s_foutZoekActie = 'Die Suche konnte nicht ausgeführt werden. Error: ';
$s_geenZelfStudieMogelijk = 'Dieser Kurs ist nicht "Zelfstudie" möglich.';
$s_dubbeleInschrijvingen = 'Sie haben doppelte Einschreibnummer eingetragen.';
$s_dubbeleCursussen = 'Sie haben doppelte Kurse eingetragen.';
$s_geenCursussen = 'Sie müssen einen oder mehrere Kurse auswählen.';
$s_geenRelatieGevonden = 'Es gibt keine Kunde.';
$s_artikelenBestellen = 'Artikelen bestellen?';
$s_geenArtikelen = 'U dient een of meerdere artikelen in te vullen.';
$s_dubbeleArtikelen = 'U heeft dubbele artikelen ingevuld.';
$s_relatieHandelingsOnbekwaam = 'Deze relatie is handelingsonbekwaam';
$s_geenBelgischeCursusVoorRelatie = 'Relaties woonachtig buiten Belgi&euml; kunnen niet inschrijven op Belgische cursussen.';
$s_minimaalEenInschrijvingAanvinken = 'U dient minimaal één inschrijving aan te vinken';
$s_kortingPercentageMoetFloatZijn = 'Het kortingpercentage dient een getal te zijn tussen 0 en 100';
$s_minimumLeeftijdRelatie = 'Let op: Om een minderjarige cursist in te schrijven dient ook een ouder/voogd te ondertekenen.';
$s_bevestigingRefresh = 'Weet u zeker dat u deze lijst opnieuw wilt laden?';
$s_bevestigingVerwijderCohort = 'Weet u zeker dat u de geselecteerde cohort wilt verwijderen?';
$s_bevestigingVerwijderCohortCursus = 'Weet u zeker dat u de geselecteerde cohortcursus wilt verwijderen?';
$s_bevestigingVerwijderCursus = 'Sind Sie sicher das Sie diesen Kurse löschen wollen?';
$s_cursusHeeftInschrijvingen = 'U kunt deze cursus niet verwijderen. Er zijn al cursisten ingeschreven voor de cursus.';
$s_cursusWordtOpgeslagen = "Inschrijving wordt opgeslagen.";
$s_cursusAlleLessenGeenVoorraad = "Der erste Periode von Kurs __cursus__ hat unvollständige Lektionen.";
$s_cursusSommigeLessenGeenVoorraad = 'Die Lektionen mit nummer __lessen__ \n von der erste Periode von Kurse __cursus__ sind unvollständig,\n die übrigen Lektionen sins auf Lager verfügbar.';
$s_cursusAlleLessenOpVoorraad = "Alle Lektionen von __cursus__ sind auf Lager.";
$s_materiaalIncompleet = 'Das Kursmaterial ist derzeit unvollständig.\n Voraussichtlicher Starttermin: __datum__';
$s_binnenkort = "in Kürze";
$s_inschrijvingWordtOpgeslagen = "Die Einschreibung wird gespeichert.";
$s_aanvraagWordtOpgeslagen = "Die Anfrage wird gespeichert.";
$s_artikelVerkoopWordtOpgeslagen = "Artikel-Marketing wird gespeichert.";
$s_maandbedragGroterNul = 'Der monatliche Betrag muss größer als 0 sein.';
$s_regelingBinnenTweeMaanden = 'Die Regelung muss innerhalb von zwei Monaten beginnen.';
$s_geenInschrijvingGevonden = 'Keine Einschreibung gefunden';
$s_bankgiroIsVerplicht = 'Girokonto-nummer muss eingegeben werden.';
$s_informatieWordtOpgeslagen = "Datei wird gesicherd.";
$s_geenBetalingsVerzoekAangevinkt = 'Sie haben kein Bezahlungsgesuch ausgewahlt.';
$s_geenMBOHBOoptieAangevinkt = 'U dient minimaal één MBO/HBO optie te kiezen';
$s_duitseBetalingsRegelingViaDatev = 'Betalingsregeling voor Duitse inschrijving gaat via Datev';
$s_leafletsWordenOpgeslagen = "Leaflets werden gespeichert.";
