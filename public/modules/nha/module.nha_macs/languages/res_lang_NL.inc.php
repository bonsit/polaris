<?php
## Nieuwe inschrijving
$s_selecteer = 'Selecteer';
$s_inschrijving = 'Inschrijving';
$s_cursus = 'Cursus';
$s_cursussen = 'Cursus(sen)';
$s_zelfstandig = "Zelfst?";
$s_zelfstudie = 'Zelfstudie';
$s_studietempo = "Studietempo";
$s_betaalwijze = "Betaalwijze";
$s_betaaltermijnen = "Betaaltermijnen";
$s_korting = "Korting";
$s_kortingomschrijving = "Kortingomschrijving";
$s_selecteer_cursussen = 'Selecteer cursus(sen)';
$s_selecteer_inschrijving = 'Selecteer inschrijving';
$s_bankgiro = 'Bankrekening';
$s_ondertekend = 'Ondertekend';
$s_relatie = 'Relatie';
$s_factuurrelatie = 'Factuurrelatie';
$s_vooropleiding = 'Vooropleidingen';
$s_vul_opmerking_in = 'Vul hier de opmerking in';
$s_statistiek = 'Statistieken';
$s_advertentiecode = 'Advertentiecode';
$s_ontvangstmedium = 'Ontvangstmedium';
$s_nieuwsbrief_ontvangen = 'Wil nieuwsbrief ontvangen?';
$s_geboortedatum = 'Geb. datum';
$s_geboorteplaats = 'Geboorteplaats';
$s_relatienummer = 'Relatienr';
$s_postcode_huisnummer = 'Postcode/Hsnr';
$s_termijn_bedrag = 'Term.bedr';
$s_startdatum = 'Startdatum';
$s_premium = 'Premium';
$s_mbo_hbo_opties = 'MBO/HBO Opties';
$s_inschrijfgeld = 'Inschrijfgeld';
$s_inschrijfgeld_berekenen = 'Wel of geen inschrijfgeld voor deze inschrijving berekenen?';
$s_inschrijfgeld_kort = 'I&nbsp;geld';

$s_naam = 'Naam';
$s_voorletters = 'Voorletters';
$s_voornaam = 'Voornaam';
$s_achternaam = 'Achternaam';
$s_tussenvoegsel = 'Tussenvgsl';
$s_adres = 'Adres';
$s_straat = 'Straat';
$s_huisnummer = 'Huisnr';
$s_huisnummertoevoeging = 'Toev';
$s_postcode = 'Postcode';
$s_plaats = 'Plaats';
$s_land = 'Land';
$s_handelingsonbekwaam = 'Handelingsonbekwaam';

$s_contact = 'Contact';
$s_tel_overdag = 'Tel. overdag';
$s_tel_avond = 'Tel. \'s avonds';
$s_tel_mobiel = 'Mobiel';
$s_email = 'Email';

$s_dhr_mevr = 'Dhr/Mevr';
$s_dhr = 'Dhr';
$s_mevr = 'Mevr';

$s_bedrijf = 'Bedrijf';
$s_bedrijfsnaam = 'Bedrijfsnaam';
$s_afdeling = 'Afdeling';
$s_btw_nummer = 'BTW nummer';

$s_omschrijving = 'Omschrijving';
$s_code = 'Code';
$s_via = 'Via';

$s_ingeschreven_voor_cursus = 'Ingeschreven voor cursus';

# Artikelverkoop
$s_selecteer_artikelen = 'Selecteer artikel(en)';
$s_artikel = 'Artikel';
$s_aantal = 'Aantal';
$s_stukprijs = 'Stukprijs';
$s_verw_bijdrage = 'Verw.bijdrage';
$s_totaal = 'Totaal';
$s_subtotaal = 'Subtotaal';
$s_verzendkosten = 'Verzendkosten';
$s_resultaat_artikelverkoop = 'Resultaat Artikelverkoop';
$s_bestelde_artikelen = 'Bestelde artikelen';
$s_displaycode = 'Displaycode';
$s_zoek = 'Zoek';

# Telemarketing
$s_geplande_datum = 'Geplande datum';
$s_begintijd = 'Begintijd';
$s_eindtijd = 'Eindtijd';
$s_auto_selectie = 'Automatische selectie';
$s_count_records = 'Records tellen';
$s_aantal_pers_inschr = 'Aantal persoonlijke inschrijvingen';
$s_medewerker = 'Medewerker';
$s_opmerking = 'Opmerking';
$s_opmerkingen = 'Opmerkingen';
$s_geplande_actie = 'Geplande actie';
$s_vervolgactie = 'Vervolgactie';
$s_reden = 'Reden';
$s_resultaat = 'Resultaat';
$s_stuur_studiegids = 'Stuur studiegids';
$s_nabellen = 'Nabellen';
$s_nabellen_hint = 'Indien er morgen teruggebeld dient te worden';
$s_geen_interesse = 'Geen interesse';
$s_afspraak_maken = 'Afspraak maken';
$s_afspraak_maken_hint = 'Indien er bijvoorbeeld volgende week teruggebeld dient te worden';
$s_later_nabellen = 'Later nabellen';
$s_later_nabellen_hint = 'Indien er later op de dag teruggebeld dient te worden (standaard een half uur later)';
$s_studiegids_niet_ontvangen = 'Studiegids niet ontvangen? Nogmaals opsturen.';
$s_mailing_sturen = 'Mailing sturen';
$s_toelichting = 'Toelichting';
$s_of = 'of';
$s_geen_historie = 'Geen historie';
$s_actiehistorie_aanvraag = 'Actiehistorie aanvraag';
$s_aangevraagde_cursus_studiegids = 'Aangevraagde cursussen/studiegidsen';
$s_datum = 'Datum';
$s_inbehandelingdoor = 'In behandeling';

# Betalingsregeling
$s_factuurrelatie_relatie = '(factuur)relatie';
$s_kies_gewenste_inschrijving = 'Kies de gewenste Inschrijving';
$s_inschrijfnr = 'Inschrijfnr';
$s_cursuscode = 'Cursuscode';
$s_cursistnr = 'Cursistnr';
$s_datum_inschrijving = 'Datum inschrijving';
$s_relatienaam = 'Relatienaam';
$s_opzeggingsdatum = 'Opzeggingsdatum';
$s_geretourneerd_op = 'Geretourneerd op';
$s_aanmaningscode = 'Aanm.code';
$s_nieuwe_regeling = 'Nieuwe regeling';
$s_achterstallig_saldo = 'Achterstallig saldo';
$s_achterstallige_administratiekosten = 'Achterstallige administratiekosten';
$s_gewenst_maandbedrag = 'Gewenst maandbedrag';
$s_administratiekosten_kwijtschelden = 'Administratiekosten kwijtschelden';
$s_datum_ingang = 'Datum ingang';
$s_maand = 'maand';

# Betaalwijze
$s_betaaldag = 'Betaaldag';

# Inschrijving Termijn Wijzigen
$s_koppelen = 'Koppelen';

# Inschrijving Opzeggen
$s_totale_lesgeld = 'Totale lesgeld';
$s_lessen = 'Lessen';
$s_verzonden = 'Verzonden';
$s_te_verzenden = 'Te verzenden';
$s_bedrag = 'Bedrag';
$s_betaald = 'Betaald';
$s_te_betalen = 'Te betalen';
$s_huidige = 'Huidige';
$s_opzegging = 'Opzegging';
$s_verschil = 'Verschil';
$s_hoogste = 'Hoogste';

# Losse factuur aanmaken
$s_factuur = 'Factuur';
$s_acceptgiro = 'Acceptgiro';
$s_inzake = 'Inzake';
$s_verzenddatum = 'Verzenddatum';
$s_op_verstuurd_zetten = 'Op verstuurd zetten';
$s_wijzig_brief = 'Wijzig brief';
$s_betalingsverzoeken = 'Betalingsverzoeken';
$s_termijn = 'Termijn';
$s_kenmerk = 'Kenmerk';
$s_status = 'Status';
$s_vervaldata_cursist_systeem = 'Vervaldata cursist/systeem';
$s_herinnering = 'Herinnering';
$s_regeling = 'Regeling';
$s_haal_brieftekst_op = 'Haal brieftekst op';

# Bulk premiums
$s_verwerkleaflets = 'Verwerk leaflets';

# Messages
$s_geenRelatieGeselecteerd = 'U heeft nog geen relatie geselecteerd.';
$s_geenArtikelRelatieGeselecteerd = 'Alleen ingeschreven relaties kunnen artikelen bestellen.';
$s_geenVooropleiding = 'U heeft geen vooropleidingen aangegeven.';
$s_foutZoekActie = 'De zoekopdracht kon niet uitgevoerd worden. Error = ';
$s_geenZelfStudieMogelijk = 'Bij deze cursus is geen zelfstudie mogelijk.';
$s_dubbeleCursussen = 'U heeft dubbele cursussen ingevuld.';
$s_dubbeleInschrijvingen = 'U heeft dubbele inschrijfnummers ingevuld';
$s_geenCursussen = 'U dient een of meerdere cursussen in te vullen.';
$s_geenRelatieGevonden = 'Er is geen relatie gevonden.';
$s_artikelenBestellen = 'Artikelen bestellen?';
$s_geenArtikelen = 'U dient een of meerdere artikelen in te vullen.';
$s_dubbeleArtikelen = 'U heeft dubbele artikelen ingevuld.';
$s_relatieHandelingsOnbekwaam = 'Deze relatie is handelingsonbekwaam.';
$s_geenBelgischeCursusVoorRelatie = 'Relaties woonachtig buiten Belgi&euml; kunnen niet inschrijven op Belgische cursussen.';
$s_minimaalEenInschrijvingAanvinken = 'U dient minimaal één inschrijving aan te vinken.';
$s_kortingPercentageMoetFloatZijn = 'Het kortingpercentage dient een getal te zijn tussen 0 en 100.';
$s_minimumLeeftijdRelatie = 'Let op: Om een minderjarige cursist in te schrijven dient ook een ouder/voogd te ondertekenen. ';
$s_bevestigingRefresh = 'Weet u zeker dat u deze lijst opnieuw wilt laden? ';
$s_bevestigingVerwijderCohort = 'Weet u zeker dat u de geselecteerde cohort wilt verwijderen? ';
$s_bevestigingVerwijderCohortCursus = 'Weet u zeker dat u de geselecteerde cohortcursus wilt verwijderen? ';
$s_bevestigingVerwijderCursus = 'Weet u zeker dat u deze cursus wilt verwijderen? ';
$s_cursusHeeftInschrijvingen = 'U kunt deze cursus niet verwijderen. Er zijn al cursisten ingeschreven voor de cursus.';
$s_cursusWordtOpgeslagen = "Inschrijving wordt opgeslagen.";
$s_cursusAlleLessenGeenVoorraad = "Er is van termijn 1 van cursus __cursus__ geen les op voorraad.";
$s_cursusSommigeLessenGeenVoorraad = 'De lessen met nummers __lessen__ \n van termijn 1 van cursus __cursus__ zijn niet op voorraad,\n de resterende lessen zijn wel op voorraad.';
$s_cursusAlleLessenOpVoorraad = "Alle lessen van __cursus__ zijn op voorraad.";
$s_materiaalIncompleet = 'Het cursusmateriaal is momenteel incompleet.\n Verwachte startdatum: __datum__';
$s_binnenkort = "binnenkort";
$s_inschrijvingWordtOpgeslagen = "De inschrijving wordt opgeslagen";
$s_aanvraagWordtOpgeslagen = "De aanvraag wordt opgeslagen";
$s_artikelVerkoopWordtOpgeslagen = "Artikelverkoop wordt opgeslagen";
$s_maandbedragGroterNul = 'Het maandbedrag moet groter dan 0 zijn.';
$s_regelingBinnenTweeMaanden = 'De regeling moet binnen twee maanden beginnen.';
$s_geenInschrijvingGevonden = 'Geen inschrijvingen gevonden';
$s_bankgiroIsVerplicht = 'Bankgirorekeningnr moet ingevuld worden.';
$s_informatieWordtOpgeslagen = "Informatie wordt opgeslagen.";
$s_geenBetalingsVerzoekAangevinkt = 'U heeft geen betalingsverzoek aangevinkt.';
$s_geenMBOHBOoptieAangevinkt = 'U dient minimaal één MBO/HBO optie te kiezen';
$s_duitseBetalingsRegelingViaDatev = 'Betalingsregeling voor Duitse inschrijving gaat via Datev';
$s_leafletsWordenOpgeslagen = "Leaflets worden opgeslagen.";
