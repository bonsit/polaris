<?php
include_once("../../../../languages/lang_api.php");
$f = explode('.', basename(__file__));
$_language_directory = 'modules'.DIRECTORY_SEPARATOR.'nha'.DIRECTORY_SEPARATOR.'module.nha_macs';
lang_load_mod($f[0], dirname(dirname(__FILE__)));
require("_generic_.json.php");
