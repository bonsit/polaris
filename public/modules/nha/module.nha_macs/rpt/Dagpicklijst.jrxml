<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="Dagpicklijst" language="groovy" pageWidth="595" pageHeight="842" columnWidth="555" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20">
	<property name="ireport.jasperserver.reportUnit" value="/reports/Macs/Dagpicklijst"/>
	<property name="ireport.jasperserver.url" value="http://172.16.1.16:8080/jasperserver/services/repository"/>
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="WHEREFILTER" class="java.lang.String">
		<defaultValueExpression><![CDATA["AND PRINTDATUMTOTALENPICKLIJST IS NULL"]]></defaultValueExpression>
	</parameter>
	<parameter name="DUPLEX" class="java.lang.String">
		<defaultValueExpression><![CDATA["FALSE"]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT
        TOTALENPICKLIJST.DATUM,
 		TOTALENPICKLIJST.RAPPORTNR,
		TOTALENPICKLIJSTREGEL.SOORT,
		CURSUS.ACADEMIEOFPW,
		TOTALENPICKLIJSTREGEL.OMSCHRIJVING,
		TOTALENPICKLIJSTREGEL.SEALPAKKETNR,
		TOTALENPICKLIJSTREGEL.TOTAAL,
		TOTALENPICKLIJSTREGEL.VERZENDONDERDEEL,
		TOTALENPICKLIJSTREGEL.DISPLAYCODE,
		TOTALENPICKLIJSTREGEL.CODE,
		TOTALENPICKLIJST.PRIORITEIT,
		CURSUS.CURSUSNAAM,
		TOTALENPICKLIJST.LANDCODE,
		TOTALENPICKLIJST.ACADEMIEOFPW
 FROM   TOTALENPICKLIJST, TOTALENPICKLIJSTREGEL, CURSUS, TEPRINTEN_TOTALENPICKLIJSTEN TP
 WHERE  TOTALENPICKLIJST.DATUM=TOTALENPICKLIJSTREGEL.DATUM AND
 		TOTALENPICKLIJST.RAPPORTNR=TOTALENPICKLIJSTREGEL.RAPPORTNR AND
        TOTALENPICKLIJST.DATUM=TP.DATUM AND
        TOTALENPICKLIJST.RAPPORTNR=TP.RAPPORTNR AND
		TOTALENPICKLIJSTREGEL.CODE=CURSUS.CURSUSCODE (+)
        $P!{WHEREFILTER}
        ORDER BY LANDCODE ASC, PRIORITEIT, TOTALENPICKLIJST.ACADEMIEOFPW DESC
        , DATUM ASC, RAPPORTNR, SOORT DESC, EERSTEVERVOLGOFARTIKEL, SEALPAKKETNR, OMSCHRIJVING
        , PackTools.GetNumber(TOTALENPICKLIJSTREGEL.VERZENDONDERDEEL)]]>
	</queryString>
	<field name="DATUM" class="java.sql.Timestamp"/>
	<field name="RAPPORTNR" class="java.math.BigDecimal"/>
	<field name="SOORT" class="java.lang.String"/>
	<field name="ACADEMIEOFPW" class="java.lang.String"/>
	<field name="OMSCHRIJVING" class="java.lang.String"/>
	<field name="SEALPAKKETNR" class="java.math.BigDecimal"/>
	<field name="TOTAAL" class="java.math.BigDecimal"/>
	<field name="VERZENDONDERDEEL" class="java.lang.String"/>
	<field name="DISPLAYCODE" class="java.lang.String"/>
	<field name="CODE" class="java.lang.String"/>
	<field name="PRIORITEIT" class="java.math.BigDecimal"/>
	<field name="CURSUSNAAM" class="java.lang.String"/>
	<field name="LANDCODE" class="java.lang.String"/>
	<variable name="SoortOmschrijving" class="java.lang.String" resetType="Group" resetGroup="Datum" incrementType="Report">
		<variableExpression><![CDATA[$F{SOORT} + ": " +
($F{SOORT}.equals("LE")? "Lessen":
($F{SOORT}.equals("A")? "Artikelen":
($F{SOORT}.equals("BB")? "Begeleidende brieven":
($F{SOORT}.equals("S")? "Studiegidsen":
($F{SOORT}.equals("L")? "Leaflets":
($F{SOORT}.equals("M")? "Mailing":
($F{SOORT}.equals("AL")? "AdressenLijst":"")))))))]]></variableExpression>
		<initialValueExpression><![CDATA[]]></initialValueExpression>
	</variable>
	<variable name="Kopje" class="java.lang.String" resetType="Group" resetGroup="Datum">
		<variableExpression><![CDATA[(!$F{SOORT}.equals("LE")?
$F{OMSCHRIJVING} + ": " + $F{CODE} : $F{ACADEMIEOFPW} + ": " + $F{CURSUSNAAM} + " / " + $F{CODE}
)]]></variableExpression>
	</variable>
	<variable name="Seal" class="java.lang.String">
		<variableExpression><![CDATA[($F{SEALPAKKETNR} == 0 ? "": "SEAL")]]></variableExpression>
	</variable>
	<variable name="Module" class="java.lang.String">
		<variableExpression><![CDATA[($F{SOORT}.equals("LE")? "MODULECODE         MODULELESNR" : "VERZENDONDERDEEL        CODE")]]></variableExpression>
	</variable>
	<variable name="Code" class="java.lang.String">
		<variableExpression><![CDATA[($F{SOORT}.equals("LE") ?
    ($F{VERZENDONDERDEEL} > "10" ? "0" + $F{VERZENDONDERDEEL} + "  " + $F{DISPLAYCODE} : $F{VERZENDONDERDEEL} + "  " + $F{DISPLAYCODE})
    : $F{DISPLAYCODE}
)]]></variableExpression>
	</variable>
	<variable name="p0" class="java.lang.Integer">
		<variableExpression><![CDATA[$V{Code}.indexOf(" ")]]></variableExpression>
	</variable>
	<variable name="p1" class="java.lang.Integer">
		<variableExpression><![CDATA[$V{Code}.indexOf("-")]]></variableExpression>
	</variable>
	<variable name="p2" class="java.lang.Integer">
		<variableExpression><![CDATA[$V{Code}.indexOf("/")]]></variableExpression>
	</variable>
	<variable name="CodeSplitsen" class="java.lang.String">
		<variableExpression><![CDATA[($V{p0} < $V{p1} ?
    ($V{p2} > $V{p1} ?
    "  " + $V{Code}.substring($V{p1} + 1,$V{p2}) + "                              " + $V{Code}.substring($V{p2} + 1,$V{Code}.length()):
    "  " + $V{Code}.substring($V{p0} + 1,$V{p1}) + "                              " + $V{Code}.substring($V{p1} + 1,$V{Code}.length())
)
:$V{Code}
)]]></variableExpression>
	</variable>
	<group name="Datum" isStartNewPage="true" isResetPageNumber="true" isReprintHeaderOnEachPage="true">
		<groupExpression><![CDATA[$F{DATUM}]]></groupExpression>
		<groupHeader>
			<band height="36">
				<textField pattern="dd/MM/yyyy">
					<reportElement x="40" y="0" width="468" height="20"/>
					<textElement textAlignment="Center" lineSpacing="Single">
						<font fontName="SansSerif" size="15" isBold="true"/>
					</textElement>
					<textFieldExpression class="java.lang.String"><![CDATA["TOTALENPICKLIJST VOOR " + String.format("%td-%<tm-%<tY",$F{DATUM})]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="1"/>
		</groupFooter>
	</group>
	<group name="Rapportnr" isStartNewPage="true" isResetPageNumber="true">
		<groupExpression><![CDATA[$F{RAPPORTNR}]]></groupExpression>
		<groupHeader>
			<band height="23">
				<staticText>
					<reportElement x="32" y="0" width="100" height="20"/>
					<textElement lineSpacing="Single">
						<font fontName="SansSerif" size="15" isBold="true" isUnderline="false"/>
					</textElement>
					<text><![CDATA[RAPPORTNR]]></text>
				</staticText>
				<textField>
					<reportElement x="141" y="0" width="100" height="20"/>
					<textElement lineSpacing="Single">
						<font fontName="SansSerif" size="15" isBold="true"/>
					</textElement>
					<textFieldExpression class="java.math.BigDecimal"><![CDATA[$F{RAPPORTNR}]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement x="384" y="0" width="158" height="20"/>
					<textElement textAlignment="Right" lineSpacing="Single">
						<font fontName="SansSerif" size="15" isBold="true"/>
					</textElement>
					<textFieldExpression class="java.lang.String"><![CDATA["PRIORITEIT = " + $F{PRIORITEIT}]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="20">
				<printWhenExpression><![CDATA[new Boolean($V{PAGE_NUMBER}.intValue()==1) && $P{DUPLEX}.toUpperCase().equals('TRUE')]]></printWhenExpression>
				<break>
					<reportElement x="0" y="-4" width="100" height="1"/>
				</break>
			</band>
			<band height="20">
				<printWhenExpression><![CDATA[new Boolean($V{PAGE_NUMBER}.intValue()==2) && $P{DUPLEX}.toUpperCase().equals('TRUE')]]></printWhenExpression>
				<break>
					<reportElement x="0" y="2" width="100" height="1"/>
				</break>
			</band>
			<band height="20">
				<printWhenExpression><![CDATA[new Boolean($V{PAGE_NUMBER}.intValue()==3) && $P{DUPLEX}.toUpperCase().equals('TRUE')]]></printWhenExpression>
				<break>
					<reportElement x="0" y="8" width="100" height="1"/>
				</break>
			</band>
		</groupFooter>
	</group>
	<group name="Soort">
		<groupExpression><![CDATA[$F{SOORT}]]></groupExpression>
		<groupHeader>
			<band height="27">
				<rectangle>
					<reportElement x="32" y="1" width="510" height="23" backcolor="#E3E3E3"/>
				</rectangle>
				<textField evaluationTime="Group" evaluationGroup="Soort">
					<reportElement x="39" y="6" width="100" height="14"/>
					<textElement lineSpacing="Single">
						<font fontName="SansSerif" size="10" isBold="true"/>
					</textElement>
					<textFieldExpression class="java.lang.String"><![CDATA[$F{SOORT}]]></textFieldExpression>
				</textField>
				<textField evaluationTime="Group" evaluationGroup="Soort">
					<reportElement x="132" y="6" width="399" height="14"/>
					<textElement lineSpacing="Single">
						<font fontName="SansSerif" size="10" isBold="true"/>
					</textElement>
					<textFieldExpression class="java.lang.String"><![CDATA[$V{SoortOmschrijving}]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
		<groupFooter>
			<band/>
		</groupFooter>
	</group>
	<group name="AcademieOfPW">
		<groupExpression><![CDATA[$F{ACADEMIEOFPW}]]></groupExpression>
		<groupHeader>
			<band/>
		</groupHeader>
		<groupFooter>
			<band/>
		</groupFooter>
	</group>
	<group name="SOORTGROEP" isReprintHeaderOnEachPage="true">
		<groupExpression><![CDATA[(($F{SOORT} != "LE")?$F{OMSCHRIJVING}+ ": "+$F{CODE}
:$F{ACADEMIEOFPW}+": "+$F{CURSUSNAAM}+ " / "+$F{CODE})]]></groupExpression>
		<groupHeader>
			<band height="41">
				<printWhenExpression><![CDATA[$F{SOORT} != "A"]]></printWhenExpression>
				<textField evaluationTime="Group" evaluationGroup="SOORTGROEP">
					<reportElement positionType="Float" x="32" y="27" width="75" height="14" isRemoveLineWhenBlank="true">
						<printWhenExpression><![CDATA[$F{SOORT} != "A"]]></printWhenExpression>
					</reportElement>
					<textElement lineSpacing="Single">
						<font fontName="SansSerif" size="10" isBold="true" isUnderline="false"/>
					</textElement>
					<textFieldExpression class="java.lang.String"><![CDATA[$V{Seal}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement positionType="Float" x="441" y="27" width="98" height="14" isRemoveLineWhenBlank="true"/>
					<textElement textAlignment="Right" lineSpacing="Single">
						<font fontName="SansSerif" size="8" isBold="true" isUnderline="false"/>
					</textElement>
					<text><![CDATA[Werkelijk aanwezig]]></text>
				</staticText>
				<textField isStretchWithOverflow="true" evaluationTime="Group" evaluationGroup="SOORTGROEP">
					<reportElement x="32" y="0" width="543" height="27"/>
					<textElement lineSpacing="Single">
						<font fontName="SansSerif" size="14"/>
					</textElement>
					<textFieldExpression class="java.lang.String"><![CDATA[$V{Kopje}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" evaluationTime="Group" evaluationGroup="SOORTGROEP">
					<reportElement positionType="Float" x="129" y="27" width="255" height="14" isRemoveLineWhenBlank="true">
						<printWhenExpression><![CDATA[$F{SOORT} != "A"]]></printWhenExpression>
					</reportElement>
					<textElement lineSpacing="Single">
						<font fontName="SansSerif" size="10" isBold="true" isUnderline="false"/>
					</textElement>
					<textFieldExpression class="java.lang.String"><![CDATA[$V{Module}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement positionType="Float" x="384" y="27" width="44" height="14" isRemoveLineWhenBlank="true"/>
					<textElement textAlignment="Right" lineSpacing="Single">
						<font fontName="SansSerif" size="10" isBold="true" isUnderline="false"/>
					</textElement>
					<text><![CDATA[TOTAAL]]></text>
				</staticText>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="6"/>
		</groupFooter>
	</group>
	<group name="Seal" keepTogether="true">
		<groupExpression><![CDATA[$F{SEALPAKKETNR}]]></groupExpression>
		<groupFooter>
			<band height="6">
				<printWhenExpression><![CDATA[$F{SOORT} != "A"]]></printWhenExpression>
				<staticText>
					<reportElement x="40" y="0" width="100" height="6" isRemoveLineWhenBlank="true">
						<printWhenExpression><![CDATA[$F{SOORT} != "A"]]></printWhenExpression>
					</reportElement>
					<textElement lineSpacing="Single"/>
					<text><![CDATA[ ]]></text>
				</staticText>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band splitType="Stretch"/>
	</title>
	<pageHeader>
		<band height="26" splitType="Stretch">
			<rectangle>
				<reportElement mode="Transparent" x="0" y="2" width="555" height="15"/>
				<graphicElement>
					<pen lineWidth="1.0" lineColor="#999999"/>
				</graphicElement>
			</rectangle>
			<textField isBlankWhenNull="true">
				<reportElement mode="Transparent" x="90" y="3" width="377" height="14" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Center" lineSpacing="Single">
					<font fontName="Arial" size="12" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{ACADEMIEOFPW}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement mode="Opaque" x="6" y="4" width="84" height="12" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement lineSpacing="Single">
					<font size="9"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{LANDCODE}]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report" pattern="dd-MM-yyyy" isBlankWhenNull="true">
				<reportElement mode="Opaque" x="475" y="4" width="73" height="12" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Right" lineSpacing="Single">
					<font fontName="SansSerif" size="9"/>
				</textElement>
				<textFieldExpression class="java.util.Date"><![CDATA[new java.util.Date()]]></textFieldExpression>
			</textField>
		</band>
	</pageHeader>
	<columnHeader>
		<band splitType="Stretch"/>
	</columnHeader>
	<detail>
		<band height="14" splitType="Prevent">
			<printWhenExpression><![CDATA[$F{SOORT} != "A"]]></printWhenExpression>
			<textField isBlankWhenNull="true">
				<reportElement isPrintRepeatedValues="false" x="35" y="0" width="75" height="14">
					<printWhenExpression><![CDATA[($V{Seal_COUNT}.equals( 1 ) && $F{SEALPAKKETNR} > 0)]]></printWhenExpression>
				</reportElement>
				<textElement lineSpacing="Single">
					<font fontName="SansSerif" size="12"/>
				</textElement>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$F{SEALPAKKETNR}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="132" y="0" width="252" height="14"/>
				<textElement lineSpacing="Single">
					<font fontName="SansSerif" size="12"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$V{CodeSplitsen}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="384" y="0" width="47" height="14"/>
				<textElement textAlignment="Right" lineSpacing="Single">
					<font fontName="SansSerif" size="12"/>
				</textElement>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$F{TOTAAL}]]></textFieldExpression>
			</textField>
			<line>
				<reportElement x="467" y="12" width="76" height="1" printWhenGroupChanges="SOORTGROEP">
					<printWhenExpression><![CDATA[($V{Seal_COUNT}.equals( 1 ))]]></printWhenExpression>
				</reportElement>
			</line>
		</band>
		<band height="26">
			<printWhenExpression><![CDATA[$F{SOORT} == "A"]]></printWhenExpression>
			<textField>
				<reportElement x="384" y="8" width="46" height="17"/>
				<textElement textAlignment="Right" lineSpacing="Single">
					<font fontName="SansSerif" size="13"/>
				</textElement>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$F{TOTAAL}]]></textFieldExpression>
			</textField>
			<line>
				<reportElement x="468" y="25" width="75" height="1" printWhenGroupChanges="SOORTGROEP">
					<printWhenExpression><![CDATA[($V{Seal_COUNT}.equals( 1 ))]]></printWhenExpression>
				</reportElement>
			</line>
			<textField isStretchWithOverflow="true" evaluationTime="Group" evaluationGroup="SOORTGROEP">
				<reportElement stretchType="RelativeToTallestObject" x="30" y="0" width="354" height="24"/>
				<textElement lineSpacing="Single">
					<font fontName="SansSerif" size="15" isBold="false" isItalic="false"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$V{Kopje}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement positionType="Float" x="384" y="0" width="43" height="14" isRemoveLineWhenBlank="true"/>
				<textElement textAlignment="Right" lineSpacing="Single">
					<font fontName="SansSerif" size="7" isBold="true" isUnderline="false"/>
				</textElement>
				<text><![CDATA[TOTAAL]]></text>
			</staticText>
			<staticText>
				<reportElement positionType="Float" x="442" y="0" width="97" height="14" isRemoveLineWhenBlank="true"/>
				<textElement textAlignment="Right" lineSpacing="Single">
					<font fontName="SansSerif" size="8" isBold="true" isUnderline="false"/>
				</textElement>
				<text><![CDATA[Werkelijk aanwezig]]></text>
			</staticText>
		</band>
	</detail>
	<columnFooter>
		<band height="10" splitType="Stretch">
			<printWhenExpression><![CDATA[true]]></printWhenExpression>
		</band>
	</columnFooter>
	<pageFooter>
		<band height="23" splitType="Stretch">
			<textField>
				<reportElement x="455" y="0" width="100" height="20"/>
				<textElement textAlignment="Right" lineSpacing="Single">
					<font fontName="SansSerif" size="9"/>
				</textElement>
				<textFieldExpression class="java.lang.Integer"><![CDATA[$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
