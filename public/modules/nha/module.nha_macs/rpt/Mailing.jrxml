<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="Mailing" language="groovy" pageWidth="595" pageHeight="842" columnWidth="555" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20">
	<parameter name="Mailingrelatienr" class="java.math.BigDecimal">
		<defaultValueExpression><![CDATA[0]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT GEMAILDERELATIE.RELATIENR,
 		LAND.LANDNAAM,
		RELATIE.VOORLETTERS,
		RELATIE.TUSSENVOEGSEL,
		RELATIE.BEDRIJFSNAAM,
		RELATIE.GESLACHT,
		RELATIE.POSTCODE,
		RELATIE.STRAAT,
		RELATIE.HUISNR,
		RELATIE.PLAATS,
		RELATIE.AFDELINGNAAM,
		RELATIE.ACHTERNAAM,
		LAND.LANDCODE,
		GEMAILDERELATIE.MAILINGRELATIENR,
		GEMAILDERELATIE.MAILINGNR,
		GEMAILDERELATIE.MAILINGSOORTCODE,
		GEMAILDERELATIE.ACTIENR,
		RELATIE.KIXBARCODE,
		GEMAILDERELATIE.PRINTDATUM,
		AANTALDAGENGELDIGOPMAILING.GELDIGTOT
 FROM   AANTALDAGENGELDIGOPMAILING,
 		GEMAILDERELATIE,
		LAND,
		MAILING,
		RELATIE
 WHERE  GEMAILDERELATIE.MAILINGRELATIENR = AANTALDAGENGELDIGOPMAILING.MAILINGRELATIENR AND
 		GEMAILDERELATIE.RELATIENR = RELATIE.RELATIENR AND
		GEMAILDERELATIE.MAILINGNR = MAILING.MAILINGNR AND
		RELATIE.LANDCODE = LAND.LANDCODE AND
		GEMAILDERELATIE.PRINTDATUM IS  NULL and
                GEMAILDERELATIE.MAILINGRELATIENR =$P!{Mailingrelatienr}
 ORDER BY GEMAILDERELATIE.RELATIENR]]>
	</queryString>
	<field name="RELATIENR" class="java.math.BigDecimal">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="LANDNAAM" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="VOORLETTERS" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="TUSSENVOEGSEL" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="BEDRIJFSNAAM" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="GESLACHT" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="POSTCODE" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="STRAAT" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="HUISNR" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="PLAATS" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="AFDELINGNAAM" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="ACHTERNAAM" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="LANDCODE" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="MAILINGRELATIENR" class="java.math.BigDecimal">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="MAILINGNR" class="java.math.BigDecimal">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="MAILINGSOORTCODE" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="ACTIENR" class="java.math.BigDecimal">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="KIXBARCODE" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="PRINTDATUM" class="java.sql.Timestamp">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="GELDIGTOT" class="java.sql.Timestamp">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<variable name="Kixcode" class="java.lang.String">
		<variableExpression><![CDATA[($F{LANDCODE}.compareToIgnoreCase("NL") ? $F{KIXBARCODE} : "")]]></variableExpression>
	</variable>
	<variable name="Volnaam2" class="java.lang.String">
		<variableExpression><![CDATA[(!$F{BEDRIJFSNAAM}.isEmpty()?
    ($F{AFDELINGNAAM}.isEmpty()?
        ($F{BEDRIJFSNAAM}.length() > 40?
            $F{BEDRIJFSNAAM} + ",  " + $F{AFDELINGNAAM}:
            $F{BEDRIJFSNAAM} + "/n" + $F{AFDELINGNAAM}):
        $F{BEDRIJFSNAAM}): $F{ACHTERNAAM})]]></variableExpression>
	</variable>
	<variable name="NAWDeel1_1" class="java.lang.String">
		<variableExpression><![CDATA[(!($F{GESLACHT} == null)?
    ($F{GESLACHT}.equals("M")? "Dhr. " :
     ($F{GESLACHT}.equals("V")? "Mevr. " :
         (!$F{ACHTERNAAM}.equals("Administratie")? "Dhr./Mevr. ":""))):"") +
(!($F{VOORLETTERS} == null ) ? $F{VOORLETTERS} + " ": "") +
(!($F{TUSSENVOEGSEL} == null) ? $F{TUSSENVOEGSEL} + " ": "")]]></variableExpression>
	</variable>
	<variable name="NAWDeel1" class="java.lang.String">
		<variableExpression><![CDATA[(!($F{GESLACHT} == null)?
    ($F{GESLACHT}.equalsIgnoreCase("M")? "heer " :
     ($F{GESLACHT}.equalsIgnoreCase("V")? "mevrouw " : "heer/mevrouw ")
    ): " "
)]]></variableExpression>
	</variable>
	<variable name="NAWDeel2" class="java.lang.String">
		<variableExpression><![CDATA[(!($F{TUSSENVOEGSEL} == null ) ? $F{TUSSENVOEGSEL} + " " : "")]]></variableExpression>
	</variable>
	<variable name="Bedrijfsnaam" class="java.lang.String">
		<variableExpression><![CDATA[(!($F{BEDRIJFSNAAM} == null) ?
    (!($F{AFDELINGNAAM} == null) ?
        ($F{BEDRIJFSNAAM}.length() > 40?
            $F{BEDRIJFSNAAM} + ",  " + $F{AFDELINGNAAM} + "\n" + "T.a.v. ":
            $F{BEDRIJFSNAAM} + "\n" + $F{AFDELINGNAAM} +"\n" + "T.a.v. "):
        ($F{BEDRIJFSNAAM}.length() > 40?
            $F{BEDRIJFSNAAM} + ",  " + "\n" + "T.a.v. ":
            $F{BEDRIJFSNAAM} + "\n" + "\n" + "T.a.v. ")):"")]]></variableExpression>
	</variable>
	<variable name="NAW" class="java.lang.String" resetType="Group" resetGroup="Relatienr">
		<variableExpression><![CDATA[$V{Bedrijfsnaam} +
$V{NAWDeel1_1} +  $F{ACHTERNAAM} + "\n" + $F{STRAAT} + " " + $F{HUISNR} + "\n"
+
($F{LANDCODE}.equals("NL") ? $F{POSTCODE}.substring(0, 4) + " " + $F{POSTCODE}.substring(4,6) + " " + $F{PLAATS}:
($F{LANDCODE}.equals("BE") ? $F{POSTCODE} + " " + $F{PLAATS}: $F{POSTCODE} + " " + $F{PLAATS} + "\n" + $F{LANDNAAM})
)]]></variableExpression>
	</variable>
	<group name="Relatienr" isStartNewPage="true">
		<groupExpression><![CDATA[$F{RELATIENR}]]></groupExpression>
		<groupHeader>
			<band height="233">
				<textField isStretchWithOverflow="true" evaluationTime="Group" evaluationGroup="Relatienr">
					<reportElement x="294" y="20" width="250" height="110"/>
					<textElement verticalAlignment="Bottom">
						<font fontName="Arial"/>
					</textElement>
					<textFieldExpression class="java.lang.String"><![CDATA[$V{NAW}]]></textFieldExpression>
				</textField>
				<textField evaluationTime="Group" evaluationGroup="Relatienr">
					<reportElement x="294" y="140" width="250" height="20"/>
					<textElement>
						<font fontName="Arial"/>
					</textElement>
					<textFieldExpression class="java.lang.String"><![CDATA[$V{Kixcode}]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
		<groupFooter>
			<band/>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band splitType="Stretch"/>
	</title>
	<pageHeader>
		<band height="101" splitType="Stretch">
			<textField pattern="dd-MM-yyyy">
				<reportElement x="424" y="67" width="131" height="20"/>
				<textElement>
					<font fontName="Times New Roman" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.sql.Timestamp"><![CDATA[$F{GELDIGTOT}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="455" y="0" width="100" height="20"/>
				<textElement>
					<font fontName="Times New Roman" size="7"/>
				</textElement>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$F{MAILINGRELATIENR}]]></textFieldExpression>
			</textField>
		</band>
	</pageHeader>
	<columnHeader>
		<band splitType="Stretch"/>
	</columnHeader>
	<detail>
		<band splitType="Stretch"/>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band height="77" splitType="Stretch">
			<textField>
				<reportElement x="424" y="42" width="131" height="20">
					<printWhenExpression><![CDATA[$F{MAILINGSOORTCODE}.equals("HBO")]]></printWhenExpression>
				</reportElement>
				<textElement>
					<font fontName="Times New Roman" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.sql.Timestamp"><![CDATA[$F{GELDIGTOT}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="38" y="11" width="100" height="20">
					<printWhenExpression><![CDATA[$F{MAILINGSOORTCODE}.equals("HBO")]]></printWhenExpression>
				</reportElement>
				<textElement>
					<font fontName="Times New Roman"/>
				</textElement>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$F{RELATIENR}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement x="38" y="31" width="359" height="46">
					<printWhenExpression><![CDATA[$F{MAILINGSOORTCODE}.equals("HBO")]]></printWhenExpression>
				</reportElement>
				<textElement>
					<font fontName="Times New Roman"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$V{Volnaam2}]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
	<summary>
		<band height="42" splitType="Stretch"/>
	</summary>
</jasperReport>
