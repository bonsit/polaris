<?php

function LessenCallback(&$item, $key) {
    $item = intval($item['CURSUSLESNR']);
}

class Forms {
    var $database = nil;

    function Forms($db) {
        $this->database = $db;
    }

    function GetLeeftijd($geboortedatum) {
        list($day,$month,$year) = explode("-",$geboortedatum);
        $year_diff  = date("Y") - $year;
        $month_diff = date("m") - $month;
        $day_diff   = date("d") - $day;
        if ($day_diff < 0 || $month_diff < 0)
            $year_diff--;
        return $year_diff;
    }

    function GetRelatieInfo($relatieid) {
        /**
        * Bepaal de informatie voor op het Info (resultaat) scherm
        */
        $_sql = "
            SELECT RELATIENR
            , PackTools.NetteGeadresseerde(GESLACHT, VOORLETTERS, TUSSENVOEGSEL, ACHTERNAAM) AS NAAM
            , STRAAT||' '||HUISNR AS ADRES
            , POSTCODE||' '||PLAATS AS WOONPLAATS
            , LANDNAAM AS LAND
            , NVL(BEDRIJFSNAAM, '') AS BEDRIJF
            FROM NHA.RELATIE ttt, NHA.LAND l
            WHERE ttt.LANDCODE = l.LANDCODE
            AND RELATIENR = :relatienr
        ";
        return $this->database->GetRow($_sql, array('relatienr'=>$relatieid));
    }

    function _BewaarRelatieRecord($_relatieRecord, $_taalcode=false) {
        global $polaris;

        $_result = true;

        if ($_relatieRecord['_hdnAction'] == 'save') {
            /* Als het een nieuwe relatie is, dan auto_increment toevoegen */
            if ($_relatieRecord['RELATIENR'] == '__auto_increment__') {
                $_relatieRecord['RELATIENR'] = '@GEN.seqRelatie';
            }
            /* Duitsland heeft taalcode 2, de rest krijgt taalcode 1 (NL)) */
            if ($_taalcode !== false) {
                $_relatieRecord['TAALCODE'] = $_taalcode;
            }
            /* Relatie record opslaan in database */
            $_result = SaveRecord($_relatieRecord);
        }

        if (is_array($_result))
            return $_result['RELATIENR'];
        elseif ($_result === true)
            return $_relatieRecord['RELATIENR'];
    }

    function BewaarRelatieRecord($record, $taalcode=false) {
        global $polaris;

        $relatierecord = ExtractTableFields($record, 'RELATIE');
        $relatierecord['_hdnDatabase'] = $record['_hdnDatabase'];
        $r = $this->_BewaarRelatieRecord($relatierecord, $taalcode);
        return $r;
    }

    function BewaarFactuurRelatieRecord($record, $_relatieId=false, $taalcode=false) {
        if ($record['FACTUURRELATIE!RELATIENR'] == '__auto_increment__' or (intval($record['FACTUURRELATIE!RELATIENR']) > 0 and $record['FACTUURRELATIE!RELATIENR'] != $_relatieId)) {
            $factuurrelatierecord = ExtractTableFields($record, 'FACTUURRELATIE');
            $factuurrelatierecord['_hdnDatabase'] = $record['_hdnDatabase'];
            return $this->_BewaarRelatieRecord($factuurrelatierecord, $taalcode);
        } else {
            return '';
        }
    }

    function BepaalEersteOPLTermijn($inschrijfnr) {
        $_sql = "
            SELECT R.ORDERPICKLIJSTNR, R.TERMIJNNR
            FROM ORDERPICKLIJST L, ORDERPICKLIJSTREGEL R
            WHERE L.InschrijfNr = $inschrijfnr
            AND R.ORDERPICKLIJSTNR = L.ORDERPICKLIJSTNR
            AND ROWNUM < 2
            ORDER BY R.TERMIJNNR, R.orderpicklijstnr
        ";
        $rs = $this->database->GetRow($_sql);
        return $rs;
    }

    function _BewaarOPL($artikelebufferen, $db, $orderpicklijst, $inschrijfnr, $relatieId, $factuurRelatieId, $termijnnr, $viainschrijving, $startdatum) {
        global $polaris;

        $_verzending['_hdnDatabase'] = $db;
        $_verzending['_hdnAction'] = 'save';
        $_verzending['_hdnState'] = 'insert';

        if ($artikelebufferen) {
            $_verzending['_hdnTable'] = 'NHA.PRE_VERZENDING';
            $_keycolumn = 'VERZENDINGNR';
            $_sequence = 'seqPRE_VERZENDING';
        } else {
            $_verzending['_hdnTable'] = 'NHA.ORDERPICKLIJST';
            $_keycolumn = 'ORDERPICKLIJSTNR';
            $_sequence = 'SEQORDERPICKLIJST';
        }
        $_verzending[$_keycolumn] = '@READGEN.'.$_sequence;

        $_verzending['RELATIENR'] = $relatieId;
        if ($termijnnr)
            $_verzending['TERMIJNNR'] = $termijnnr;
        if ($factuurRelatieId != '')
            $_verzending['FACTUURRELATIE'] = $factuurRelatieId;
        if ($inschrijfnr)
            $_verzending['INSCHRIJFNR'] = $inschrijfnr;

        if ($viainschrijving and $startdatum) {
            $_verzending['GEPLANDEDATUM'] = $startdatum;
        } else {
            $_verzending['GEPLANDEDATUM'] = date('d-m-Y');
        }

        $_verzending['ARTIKELVERKOOPJANEE'] = 'J';
        $_verzending['MEDEWERKERNR'] = $_SESSION['userid'];
        $_verzending['VREEMDEVALUTACODE'] = $orderpicklijst['VREEMDEVALUTACODE'] != '' ? $orderpicklijst['VREEMDEVALUTACODE'] : 'EUR';
        $_verzending['BETAALWIJZECODE'] = isset($orderpicklijst['BETAALWIJZECODE']) ? $orderpicklijst['BETAALWIJZECODE'] : 'ACP'; // default ACP
        $_verzending['VERZENDKOSTEN'.FLOATCONVERT] = $orderpicklijst['VERZENDKOSTEN'];
        if (intval($orderpicklijst['BANKGIRONUMMER']) > 0)
            $_verzending['BANKGIRONUMMER'] = $orderpicklijst['BANKGIRONUMMER'];
        else
            $_verzending['BANKGIRONUMMER'] = '';

        $_asv = SaveRecord($_verzending);

        $_nummer = $_asv[$_keycolumn];
        return $_nummer;
    }

    function _BewaarOPLRegel($artikelebufferen, $db, $verzendingnr, $termijnnr, $aantal, $pictypesoort, $picktypecode, $omschrijving
        , $displaycode, $inschrijfnr=false, $vreemdevaluta=false, $prijsperstuk=false, $prijsperstukvreemdevaluta=false, $verwijderingsbijdrage=false) {
        global $polaris;

        if (($picktypecode == '') or (intval($aantal) < 1)) return false;

        $_verzendonderdeel['_hdnDatabase'] = $db;
        $_verzendonderdeel['_hdnState'] = 'insert';

        if ($artikelebufferen) {
            $_verzendonderdeel['_hdnTable'] = 'NHA.PRE_VERZENDONDERDELEN';
            $_keycolumn = 'VERZENDINGNR';
        } else {
            $_verzendonderdeel['_hdnTable'] = 'NHA.ORDERPICKLIJSTREGEL';
            $_keycolumn = 'ORDERPICKLIJSTNR';
        }

        $_verzendonderdeel[$_keycolumn] = $verzendingnr;
        if ($termijnnr)
            $_verzendonderdeel['TERMIJNNR'] = $termijnnr;
        $_verzendonderdeel['AANTAL'] = $aantal;
        $_verzendonderdeel['PICKTYPESOORT'] = $pictypesoort;
        $_verzendonderdeel['PICKTYPECODE'] = $picktypecode;
        $_verzendonderdeel['OMSCHRIJVING'] = html_entity_decode($omschrijving);
        $_verzendonderdeel['DISPLAYCODE'] = $displaycode;

        if ($inschrijfnr !== FALSE)
            $_verzendonderdeel['INSCHRIJFNR'] = $inschrijfnr;

        if ($vreemdevaluta !== FALSE) {
            if ($vreemdevaluta == 'EUR')
                $_verzendonderdeel['PRIJSPERSTUKINEUROS'.FLOATCONVERT] = $prijsperstuk;
            else
                $_verzendonderdeel['PRIJSPERSTUKINVREEMDEVALUTA'.FLOATCONVERT] = $prijsperstukvreemdevaluta;
        }

        if ($verwijderingsbijdrage !== FALSE and intval($verwijderingsbijdrage) <> 0)
            $_verzendonderdeel['VERWIJDERINGSBIJDRAGEPERSTUK'.FLOATCONVERT] = $verwijderingsbijdrage;

        $_asv = SaveRecord($_verzendonderdeel);

        return true;
    }

    function BewaarGratisOnderdelen($inschrijfnr, $cursuscode, $relatieId, $factuurRelatieId, $allesRecord) {
        $result = false;

        $_artikelenbufferen = true;

        $_onderdelen = ExtractTableFields($allesRecord, 'GRATISONDERDEEL');

        $_verzendingnummer = $this->_BewaarOPL(
              $_artikelenbufferen
            , $allesRecord['_hdnDatabase']
            , FALSE //orderpicklijst
            , $inschrijfnr
            , $relatieId
            , $factuurRelatieId
            , 1 // termijnnr
            , TRUE //viainschrijving
            , FALSE //startdatum
        );

        if ($_onderdelen['GRATISONDERDEEL']) {
            foreach($_onderdelen['GRATISONDERDEEL'] as $_onderdeel) {
                list($onderdeelcursuscode, $onderdeelcode, $omschrijving) = explode('!',$_onderdeel);
                if ($cursuscode == $onderdeelcursuscode) {

                    $this->_BewaarOPLRegel(
                          $_artikelenbufferen
                        , $allesRecord['_hdnDatabase']
                        , $_verzendingnummer
                        , 1 // termijn
                        , 1 // aantal
                        , 'A' // pictypesoort
                        , $onderdeelcode // picktypecode
                        , $omschrijving
                        , 'A-'.$onderdeelcode // displaycode
                        , $inschrijfnr
                    );

                    $result[] = array("DISPLAYCODE"=>'A-'.$onderdeelcode, "OMSCHRIJVING"=>$omschrijving);
                }
            }
        }

        return $result;
    }

    function BewaarArtikelen($_allesRecord, $inschrijfnr, $relatieId, $factuurRelatieId, $index) {
        /* Bewaar de artikelen */
        $viainschrijving = true;
        return $this->BewaarArtikelVerkoopArtikelen($_allesRecord, $relatieId, $factuurRelatieId, $inschrijfnr, $index, $viainschrijving);
    }

    function InschrijfGeldOpSA($inschrijfnr) {
        $_sql = 'UPDATE BETALINGSVERZOEK SET STATUS = \'SA\' WHERE BETALINGSKENMERK = \'I\' AND INSCHRIJFNR = :INSCHRIJFNR';
        $this->database->Execute($_sql, array('INSCHRIJFNR'=>$inschrijfnr));
    }

    function WijzigInschrijfgeld($inschrijfnr, $inschrijfgeld) {
        $_sp = $this->database->PrepareSP("BEGIN NHA.PackInschrijvingAanpassing.WijzigInschrijfgeld(:INSCHRIJFNR, :NIEUWBEDRAG); END;");
        $this->database->InParameter($_sp, $inschrijfnr, 'INSCHRIJFNR');
        $this->database->InParameter($_sp, $inschrijfgeld, 'NIEUWBEDRAG');
        $this->database->Execute($_sp);
    }

    function BewaarInschrijvingRecords($_allesRecord, $_relatieId, $_factuurRelatieId, &$gratisonderdelen, &$artikelen) {
        global $polaris;
        require_once('modules/nha/module.nha1/php/bufferinschrijvingen.class.php');

        $result = Array();
        $_inschrijvingen = ExtractTableFields($_allesRecord, 'INSCHRIJVING');
        $_relatierecord = ExtractTableFields($_allesRecord, 'RELATIE');
        foreach($_inschrijvingen['AANGEVINK'] as $_value) {
            $key = array_search($_value, $_inschrijvingen['CURSUSCODE']);

            if ($key !== FALSE) {
                $_inschrijving = Array();
                $_inschrijving['_hdnDatabase'] = $_allesRecord['_hdnDatabase'];
                $_inschrijving['_hdnTable'] = 'NHA.INSCHRIJVING';
                $_inschrijving['_hdnAction'] = 'save';
                $_inschrijving['_hdnState'] = 'insert';
                $_inschrijving['INSCHRIJFNR'] = $_inschrijvingen['INSCHRIJFNR'][$key];
                $_inschrijving['CURSISTNR'] = $_relatieId;
                $_inschrijving['CURSUSCODE'] = $_inschrijvingen['CURSUSCODE'][$key];
                $_inschrijving['STUDIETEMPOCODE'] = $_inschrijvingen['STUDIETEMPOCODE'][$key];
                if (substr($_inschrijvingen['BETAALWIJZE'][$key],0,3) == 'AUT') {
                    $parts = explode('_', $_inschrijvingen['BETAALWIJZE'][$key]);
                    $_inschrijving['BETAALDAG'] = $parts[1];
                    $_inschrijving['BETAALWIJZE'] = $parts[0];
                } else {
                    $_inschrijving['BETAALWIJZE'] = $_inschrijvingen['BETAALWIJZE'][$key];
                }
                $_inschrijving['AANTALBETAALTERMIJNEN'] = $_inschrijvingen['AANTALBETAALTERMIJNEN'][$key];
                $_inschrijving['STARTDATUM'] = $_inschrijvingen['STARTDATUM'][$key];
                $_inschrijving['KORTINGSPERCENTAGE'.FLOATCONVERT] = $_inschrijvingen['KORTINGSPERCENTAGE'][$key] != ""?$_inschrijvingen['KORTINGSPERCENTAGE'][$key]:"0";
                if ($_inschrijvingen['KORTINGSPERCENTAGE'][$key] != '') {
                    $_inschrijving['KORTINGOMSCHRIJVING'] = $_inschrijvingen['KORTINGOMSCHRIJVING'][$key];
                }
                if ($_factuurRelatieId != '') $_inschrijving['FACTUURRELATIE'] = $_factuurRelatieId;

                if (!isset($_inschrijvingen['ZELFSTUDIEJANEE']))
                    $zelfstudieJANEE = 'N';
                else
                    $zelfstudieJANEE = in_array($_inschrijving['CURSUSCODE'], $_inschrijvingen['ZELFSTUDIEJANEE'])?'J':'N';
                $_inschrijving['ZELFSTUDIEJANEE'] = $zelfstudieJANEE;

                // RFC50

                if (isset($_inschrijvingen['BEPAALINSCHRIJFGELD'])) {
                    $inschrijfGeldOpNulZetten = in_array($_inschrijving['CURSUSCODE'], $_inschrijvingen['BEPAALINSCHRIJFGELD'])?false:true;
                } else {
                    $inschrijfGeldOpNulZetten = true;
                }

                if (!isset($_inschrijvingen['HEEFTVOOROPLEIDING']))
                    $heeftvooropleidingJANEE = 'N';
                else
                    $heeftvooropleidingJANEE = in_array($_inschrijving['CURSUSCODE'], $_inschrijvingen['HEEFTVOOROPLEIDING'])?'J':'N';
                $_inschrijving['HEEFTVOOROPLEIDING'] = $heeftvooropleidingJANEE;

                if (!isset($_inschrijvingen['VRIJSTELLINGDIPLOMA']))
                    $vrijstellingDiplomaJANEE = 'N';
                else
                    $vrijstellingDiplomaJANEE = in_array($_inschrijving['CURSUSCODE'], $_inschrijvingen['VRIJSTELLINGDIPLOMA'])?'J':'N';
                $_inschrijving['VRIJSTELLINGDIPLOMA'] = $vrijstellingDiplomaJANEE;

                if (!isset($_inschrijvingen['VRIJSTELLINGWERKERVARING']))
                    $vrijstellingWerkervaringJANEE = 'N';
                else
                    $vrijstellingWerkervaringJANEE = in_array($_inschrijving['CURSUSCODE'], $_inschrijvingen['VRIJSTELLINGWERKERVARING'])?'J':'N';
                $_inschrijving['VRIJSTELLINGWERKERVARING'] = $vrijstellingWerkervaringJANEE;

                if (!isset($_inschrijvingen['VERKLARING21JAAROFOUDER']))
                    $Verklaring21JaarJANEE = 'N';
                else
                    $Verklaring21JaarJANEE = in_array($_inschrijving['CURSUSCODE'], $_inschrijvingen['VERKLARING21JAAROFOUDER'])?'J':'N';
                $_inschrijving['VERKLARING21JAAROFOUDER'] = $Verklaring21JaarJANEE;

                if (!isset($_inschrijvingen['DIRECTINSCHRIJVING']))
                    $directInschrijvingJANEE = FALSE;
                else
                    $directInschrijvingJANEE = in_array($_inschrijving['CURSUSCODE'], $_inschrijvingen['DIRECTINSCHRIJVING'])?TRUE:FALSE;

                $_inschrijving['DATUMINSCHRIJVING'] = date('d-m-Y');
                $_inschrijving['MEDEWERKERNR'] = $_SESSION['userid'];
                $_inschrijving['ADVERTENTIECODE'] = $_allesRecord['ADVERTENTIECODE'];
                $_inschrijving['ONTVANGSTMEDIUM'] = $_allesRecord['ONTVANGSTMEDIUM'];
                $_inschrijving['MAILINGNR'] = null;
                // RFC33 $_inschrijving['BANKGIROREKENINGNR'] = intval($_allesRecord['BANKGIROREKENINGNR']) > 0?$_allesRecord['BANKGIROREKENINGNR']:null;
                $_inschrijving['BANKGIROREKENINGNR'] = $_allesRecord['BANKGIROREKENINGNR'];
                $_inschrijving['ONDERTEKENDJANEE'] = $_allesRecord['ONDERTEKENDJANEE']=="J"?"J":"N";
                $_leeftijd = $this->GetLeeftijd($_relatierecord['GEBDATUM']);
                if ($_relatierecord['GEBDATUM'] != '' and ($_leeftijd < 18 )) {
                    $_inschrijving['MINDERJARIGAKKOORDJANEE'] = "J";
                }

                $_asv = SaveRecord($_inschrijving);

                $inschrijfnr = $_asv['INSCHRIJFNR'];

                /* Bewaar de (gratis) Onderdelen records */
                $gratisonderdelen[$inschrijfnr] = $this->BewaarGratisOnderdelen($inschrijfnr, $_inschrijving['CURSUSCODE'], $_relatieId, $_factuurRelatieId, $_allesRecord);

                /* Bewaar de Artikel records */
                $_index = 0;
                $artikelen[$inschrijfnr] = $this->BewaarArtikelen($_allesRecord, $inschrijfnr, $_relatieId, $_factuurRelatieId, $_index);

                /* Inschrijving verwerken */
                $this->InschrijvingVerwerken($_asv['INSCHRIJFNR'], $directInschrijvingJANEE);

                /* RFC50 */
                if ($inschrijfGeldOpNulZetten) {
                    $this->InschrijfGeldOpSA($inschrijfnr);
                    $this->WijzigInschrijfgeld($inschrijfnr, 0);
                    if ($_inschrijving['CURSUSCODE'][0] !== 'Q') {
                        $_opmerking = 'Inschrijfgeld op SA gezet';
                        $this->BewaarRelatieOpmerking($_inschrijving, $_opmerking, $_relatieId, false, $inschrijfnr);
                    }
                }

                $result[$_asv['INSCHRIJFNR']] = $_inschrijving['CURSUSCODE']; // verzamel alle toegevoegde cursuscodes en geef deze terug
            }
        }

        return $result;
    }

    function BewaarArtikelVerkoopArtikelen($_allesRecord, $_relatieId, $_factuurRelatieId, $_inschrijfnr=false, $index, $viainschrijving=false) {
        global $polaris;

        $result = array();

        $_orderpicklijst = ExtractTableFields($_allesRecord, 'ORDERPICKLIJST');
        $_artikelen = ExtractTableFields($_allesRecord, 'ORDERPICKLIJSTREGEL');
        $_inschrijvingen = ExtractTableFields($_allesRecord, 'INSCHRIJVING');

        $_artikelenbufferen = $_inschrijfnr !== false;

        // alleen orderpicklijst(regel) opslaan als er artikelen zijn
        $_aantal = 0;
        foreach($_artikelen['_hdnAction'] as $key => $_value) {
            if ($_artikelen['PICKTYPECODE'][$key] != '' and intval($_artikelen['AANTAL'][$key]) > 0)
                $_aantal = $_aantal + 1;
        }
        if ($_aantal == 0) return false;

        $_verzendingnummer = $this->_BewaarOPL(
              $_artikelenbufferen
            , $_allesRecord['_hdnDatabase']
            , $_orderpicklijst
            , $_inschrijfnr
            , $_relatieId
            , $_factuurRelatieId
            , false // termijnnr
            , $viainschrijving
            , $_inschrijvingen['STARTDATUM'][$index]);

        foreach($_artikelen['_hdnAction'] as $key => $_value) {
            $_saveresult = $this->_BewaarOPLRegel(
                  $_artikelenbufferen
                , $_allesRecord['_hdnDatabase']
                , $_verzendingnummer
                , false // Termijnnr
                , $_artikelen['AANTAL'][$key]
                , 'A' // Pictypesoort
                , $_artikelen['PICKTYPECODE'][$key]
                , $_artikelen['OMSCHRIJVING'][$key]
                , 'A-'.$_artikelen['PICKTYPECODE'][$key]
                , $_artikel['INSCHRIJFNR']
                , 'EUR'
                , $_artikelen['PRIJSPERSTUKINEUROS'][$key]
                , $_artikelen['PRIJSPERSTUKINVREEMDEVALUTA'][$key]
                , $_artikelen['VERWIJDERINGSBIJDRAGEPERSTUK'][$key]
            );
            if ($_saveresult)
                $result[] = array("DISPLAYCODE"=>'A-'.$_artikelen['PICKTYPECODE'][$key], "OMSCHRIJVING"=>$_artikelen['OMSCHRIJVING'][$key],"AANTAL"=>$_artikelen['AANTAL'][$key]);
        }

        return $result;
    }

    /***
    *
    *   Bepaal taalcode op basis van eigenadres van Cursus/Studiegids
    *
    */
    function BepaalTaalcode($eersteCursusStudiegids) {
/*
        SELECT T.TAALCODE
        FROM EIGENADRES E1, CURSUS C, TAAL T

        UNION
        SELECT T.TAALCODE
        FROM VERZENDONDERDEEL V, EIGENADRES E2
        WHERE

         WHERE E.ACADEMIEOFPW = C.ACADEMIEOFPW
         AND E.ADRESNR = C.EIGENADRES
         AND E.TAALCODE = T.DISPLAYTAALCODE
         AND C.CURSUSSTUDIEGIDSCODE
*/
        $_sql = "SELECT T.TAALCODE FROM EIGENADRES E, CURSUSENSTUDIEGIDS C, TAAL T
         WHERE E.ACADEMIEOFPW = C.ACADEMIEOFPW
         AND E.ADRESNR = C.EIGENADRES
         AND E.TAALCODE = T.DISPLAYTAALCODE
         AND C.CURSUSSTUDIEGIDSCODE = :studiegidscursuscode";
        try {
            $_taalcode = $this->database->GetOne($_sql, array('studiegidscursuscode' => $eersteCursusStudiegids));
        } catch(Exception $e) {
            $_taalcode = '1';
        }
        return $_taalcode;
    }

    function BewaarVooropleidingen($allesRecord, $relatieNr) {
        $_vooroplRec = ExtractTableFields($allesRecord, 'VOOROPLEIDING');
        $result = true;
        $_sqlInsert = 'INSERT INTO VOOROPLEIDING (RELATIENR, VOOROPLEIDING, DIPLOMAJANEE, GEACCORDEERDJANEE)
        VALUES (:RELATIENR, :VOOROPLEIDING, :DIPLOMAJANEE, \'N\')';
        if (isset($_vooroplRec['VOOROPLEIDING']) and $_vooroplRec['VOOROPLEIDING'] !== '') {
            $_diplomas = array();
            if (isset($_vooroplRec['VOOROPLDIPLOMA']))
                $_diplomas = array_values($_vooroplRec['VOOROPLDIPLOMA']);
            foreach($_vooroplRec['VOOROPLEIDING'] as $_vooropleiding) {
                $_diploma = in_array($_vooropleiding, $_diplomas)? 'J' : 'N';
                try {
                    $result = $this->database->Execute($_sqlInsert, array(
                        'RELATIENR' => $relatieNr
                        , 'VOOROPLEIDING' => $_vooropleiding
                        , 'DIPLOMAJANEE' => $_diploma
                    ));
                } catch (Exception $E) {
                    null;
                }
            }
        }
        if (isset($_vooroplRec['VOOROPLEIDING_ANDERS']) and $_vooroplRec['VOOROPLEIDING_ANDERS'] !== '') {
            try {
                $result = $this->database->Execute($_sqlInsert, array(
                    'RELATIENR' => $relatieNr
                    , 'VOOROPLEIDING' => $_vooroplRec['VOOROPLEIDING_ANDERS']
                    , 'DIPLOMAJANEE' => isset($_vooroplRec['VOOROPLDIPLOMA_ANDERS']) ? 'J' : 'N'
                ));
            } catch (Exception $E) {
                null;
            }
        }
        return $result;
    }

    /***
    *
    *   Nieuwe Aanvraag
    *
    */
    function BewaarNieuweAanvraag($_allesRecord) {
        global $polaris;

        $_cursussen = $_allesRecord['VERZOEKCURSUSINFO!CURSUSSTUDIEGIDSCODE'];
        $_eersteCursusStudiegids = $_cursussen[0];
        $_taalcode = $this->BepaalTaalcode($_eersteCursusStudiegids);
        if ($_taalcode == NULL)
            $_taalcode = '1';

        /* Bewaar het Relatie record */
        $_relatieId = $this->BewaarRelatieRecord($_allesRecord, $_taalcode);

        /* Bewaar het relatie record als pinitem */
//        $relatierecord = ExtractTableFields($_allesRecord, 'RELATIE');
//        $relatierecord['_hdnDatabase'] = $_allesRecord['_hdnDatabase'];
//        $relatierecord['_hdnPinConstruct'] = 'relatie2';
//        $polaris->addToHistory($relatierecord, array('relatienr' => $_relatieId));

        /* Bewaar het Verzoek record */
        $_verzoekRecord['_hdnDatabase'] = $_allesRecord['_hdnDatabase'];
        $_verzoekRecord['_hdnTable'] = 'NHA.VERZOEK';
        $_verzoekRecord['_hdnState'] = 'insert';
        $_verzoekRecord['VERZOEKNR'] = '@GEN.seqVerzoek'; //__auto_increment__
        $_verzoekRecord['RELATIENR'] = $_relatieId;
        $_verzoekRecord['SOORTVERZOEK'] = 'C';
        $_verzoekRecord['ADVERTENTIECODE'] = $_allesRecord['ADVERTENTIECODE'];
        $_verzoekRecord['MAILINGNR'] = null;
        $_verzoekRecord['ONTVANGSTMEDIUM'] = $_allesRecord['ONTVANGSTMEDIUM'];
        $_verzoekIds = SaveRecord($_verzoekRecord);

        /* Bewaar het VerzoekCursusInfo record */
        $_verzoekCursusInfoRecord['_hdnDatabase'] = $_allesRecord['_hdnDatabase'];
        $_verzoekCursusInfoRecord['_hdnTable'] = 'NHA.VERZOEKCURSUSINFO';
        $_verzoekCursusInfoRecord['_hdnState'] = 'insert';
        $_verzoekCursusInfoRecord['VERZOEKNR'] = $_verzoekIds['VERZOEKNR'];
        foreach($_cursussen as $_cursus) {
            if (isset($_cursus) and $_cursus != '') {
                $_verzoekCursusInfoRecord['CURSUSSTUDIEGIDSCODE'] = $_cursus;
                $_succes = SaveRecord($_verzoekCursusInfoRecord);
            }
        }
        return true;
    }

    function InschrijvingenVerwerken($inschrijfcursuscodes) {
        foreach($inschrijfcursuscodes as $inschrijfnr => $cursuscode) {
            $this->InschrijvingVerwerken($inschrijfnr);
        }
    }

    function InschrijvingVerwerken($inschrijfnr, $directVerwerken=false) {
        $directVerwerkenString = $directVerwerken?'J':'N';
        $_sp = $this->database->PrepareSP("BEGIN NHA.PackInschrijving.CheckInschrijvingVerwerken(:INSCHRIJFNR, :DIRECTVERWERKEN); COMMIT; END;");
        $this->database->InParameter($_sp, $inschrijfnr, 'INSCHRIJFNR');
        $this->database->InParameter($_sp, $directVerwerkenString, 'DIRECTVERWERKEN');
        $this->database->Execute($_sp);
    }

    /***
    *
    *   Nieuwe Inschrijving
    *
    */
    function BewaarNieuweInschrijvingen($_allesRecord) {
        global $polaris;

//        $polaris->logUserAction('SQL logging', session_id(), $_SESSION['name'], var_export($_allesRecord, true));

        $this->database->StartTrans();

        // quick fix. indien er geen relatienr bekend is, zet het record dan in Insert mode. Soms staat deze op edit.
        // zou eigenlijk in JS gefixed moeten worden.
        if ($_allesRecord['RELATIE!RELATIENR'] == '__auto_increment__')
            $_allesRecord['RELATIE!_hdnState'] = 'insert';

        $_insch = ExtractTableFields($_allesRecord, 'INSCHRIJVING');
        $_eersteCursus = $_insch['CURSUSCODE'][0];
        $polaris->logUserAction('SQL logging1', session_id(), $_SESSION['name'], var_export($_allesRecord, true));
        $_taalcode = $this->BepaalTaalcode($_eersteCursus);
        if ($_taalcode == NULL)
            $_taalcode = '1'; // Nederlands
        /* Bewaar het Relatie record */
        $_relatieId = $this->BewaarRelatieRecord($_allesRecord, $_taalcode);
        $this->BewaarVooropleidingen($_allesRecord, $_relatieId);

        /* Bewaar het FactuurRelatie record */
        $_factuurRelatieId = $this->BewaarFactuurRelatieRecord($_allesRecord, $_relatieId, $_taalcode);

        /* Bewaar het relatie record als pinitem */
        $relatierecord = ExtractTableFields($_allesRecord, 'RELATIE');
        $relatierecord['_hdnDatabase'] = $_allesRecord['_hdnDatabase'];
        $relatierecord['_hdnPinConstruct'] = 'relatie2';
//        $polaris->addToHistory($relatierecord, array('relatienr' => $_relatieId));
        if ($_factuurRelatieId) {
            $factuurrelatierecord = ExtractTableFields($_allesRecord, 'FACTUURRELATIE');
            $factuurrelatierecord['_hdnDatabase'] = $_allesRecord['_hdnDatabase'];
            $factuurrelatierecord['_hdnPinConstruct'] = 'relatie2';
//            $polaris->addToHistory($factuurrelatierecord, array('relatienr' => $_factuurRelatieId), $add=true);
        }

        /* Bewaar het Cursist record */
        $_cursistRecord['_hdnDatabase'] = $_allesRecord['_hdnDatabase'];
        $_cursistRecord['_hdnTable'] = 'NHA.CURSIST';
        $_cursistRecord['_hdnState'] = 'insert';
        $_cursistRecord['CURSISTNR'] = $_relatieId;
        $_cursistRecord['DATUMAANGEMELD'] = date('d-m-Y');
        if ($this->database->GetOne('SELECT COUNT(*) FROM CURSIST WHERE CURSISTNR = :cursistnr', array('cursistnr'=>$_relatieId)) == 0) {
            $_cursistIds = SaveRecord($_cursistRecord);
        }

        $gratisonderdelen = array();
        $artikelen = array();

        /* Bewaar de Inschrijving records */
        $inschrijfcursuscodes = $this->BewaarInschrijvingRecords($_allesRecord, $_relatieId, $_factuurRelatieId, $gratisonderdelen, $artikelen);

        // NIET ALS BULK, IVM DUBBEL INSCHRIJFGELD
        //  $this->InschrijvingenVerwerken($inschrijfcursuscodes);

        /* Indien de inschrijving volgt uit een TM actie, dan de geplande actie aanpassen */
        $_actienr = $_POST['TM_ACTIENR'];
        if ($_actienr != '') {
            $_geplandeActie['_hdnDatabase'] = $_allesRecord['_hdnDatabase'];
            $_geplandeActie['RESULTAAT'] = 'INSCHRIJVING';

            $this->BewaarGeplandeActieRecord($_geplandeActie, $_actienr);

            /* Bewaar de (optionele) opmerking */
            $this->BewaarRelatieOpmerking($_allesRecord, $_allesRecord['RELATIEOPMERKING'], $_relatieId);
        }

        $this->database->CompleteTrans();

        $_relatieInfo = $this->GetRelatieInfo($_relatieId);
        $_factuurRelatieInfo = $this->GetRelatieInfo($_factuurRelatieId);

        foreach($inschrijfcursuscodes as $inschrijfnr => $cursuscode) {
            $_sql = "
                SELECT I.INSCHRIJFNR
                , I.CURSUSCODE
                , C.CURSUSNAAM AS OMSCHRIJVING
                , I.INSCHRIJFGELD
                FROM INSCHRIJVING I, CURSUS C
                WHERE I.CURSUSCODE = C.CURSUSCODE
                AND INSCHRIJFNR = :inschrijfnr
            ";
            $_inschrijvingInfo[] = $this->database->GetRow($_sql, array('inschrijfnr'=>$inschrijfnr));
            if ($gratisonderdelen[$inschrijfnr])
                foreach($gratisonderdelen[$inschrijfnr] as $i => $rec) {
                    $_inschrijvingInfo[]['OMSCHRIJVING'] = $rec['OMSCHRIJVING'];
                }

            if ($artikelen[$inschrijfnr])
                foreach($artikelen[$inschrijfnr] as $i => $rec) {
                    $_inschrijvingInfo[]['OMSCHRIJVING'] = $rec['OMSCHRIJVING'];
                }
        }
        $result = array('relatie'=>$_relatieInfo, 'factuurrelatie'=>$_factuurRelatieInfo, 'inschrijving'=>$_inschrijvingInfo);
        return $result;
    }

    /***
    *
    *   Artikel Verkoop
    *
    */
    function BewaarArtikelVerkoop($_allesRecord) {
        $taalcode = false; // bestaande relaties, alleen niet in Belgie, maar dan krijg je default taalcode 1
        /* Bewaar het Relatie record */
        $_relatieId = $this->BewaarRelatieRecord($_allesRecord, $taalcode);
        /* Bewaar het FactuurRelatie record */
        $_factuurRelatieId = $this->BewaarFactuurRelatieRecord($_allesRecord, $_relatieId, $taalcode);

        $_relatieInfo = $this->GetRelatieInfo($_relatieId);
        $_factuurRelatieInfo = $this->GetRelatieInfo($_factuurRelatieId);

        /* Bewaar de artikelen */
        $_artikelen = $this->BewaarArtikelVerkoopArtikelen($_allesRecord, $_relatieId, $_factuurRelatieId, false, $index=0);

        $result = array('relatie'=>$_relatieInfo, 'factuurrelatie'=>$_factuurRelatieInfo, 'artikelen'=>$_artikelen);
        return $result;
    }

    function BewaarRelatieOpmerking($_allesRecord, $_opmerking, $_relatieId, $_fillAllValues = false, $_inschrijfnr = false) {
        global $polaris;

        $_succes = false;

        if (trim($_opmerking) != '') {
            $_opmerkingRecord['_hdnDatabase'] = $_allesRecord['_hdnDatabase'];
            $_opmerkingRecord['_hdnTable'] = 'OPMERKING';
            $_opmerkingRecord['_hdnAction'] = 'save';
            $_opmerkingRecord['_hdnState'] = 'insert';
            $_opmerkingRecord['RELATIENR'] = $_relatieId;
            $_opmerkingRecord['DATUM'] = date('d-m-Y');
            $_opmerkingRecord['OPMERKING'] = $_opmerking;
            $_opmerkingRecord['MEDEWERKERNR'] = $_SESSION['userid'];
            if ($_inschrijfnr)
                $_opmerkingRecord['INSCHRIJFNR'] = $_inschrijfnr;

            if ($_fillAllValues) {
                $_sqlGetValues = "SELECT INSCHRIJFNR, CURSUSCODE FROM INSCHRIJVING WHERE ROWIDTOCHAR(ROWID) = '".$_allesRecord['_hdnRecordID']."'";
                $_InschrijfValues = $this->database->GetRow($_sqlGetValues);
                $_opmerkingRecord['INSCHRIJFNR'] = $_InschrijfValues['INSCHRIJFNR'];
                $_opmerkingRecord['CURSUSCODE'] = $_InschrijfValues['INSCHRIJFNR'];
            }

            $_succes = SaveRecord($_opmerkingRecord);
        }
        return $_succes;
    }

    function BewaarGeplandeActieRecord($_allesRecord, $_actieNr) {
        global $polaris;

        $_huidigeGeplandeActie = $this->database->GetRow("SELECT G.*, ROWIDTOCHAR(G.ROWID) AS PLR__RECORDID FROM GEPLANDEACTIE G WHERE ACTIENR = $_actieNr");

        $_geplandeActieRecord['_hdnDatabase'] = $_allesRecord['_hdnDatabase'];
        $_geplandeActieRecord['_hdnTable'] = 'GEPLANDEACTIE';
        $_geplandeActieRecord['_hdnAction'] = 'save';
        $_geplandeActieRecord['_hdnState'] = 'edit';
        $_geplandeActieRecord['_hdnRecordID'] = $_huidigeGeplandeActie['PLR__RECORDID'];

        switch($_allesRecord['RESULTAAT']) {
        case 'NABELLEN':
        case 'LATERNABELLEN':
        case 'AFSPRAAKMAKEN':
            // Vervolg actie
            $_vervolgActieRecord = $_geplandeActieRecord;

            // huidige actie opslaan
            $_geplandeActieRecord['GEREALISEERDEMEDEWERKER'] = intval($_SESSION['userid']);
            $_geplandeActieRecord['GEREALISEERDEDATUM'] = $this->database->DBTimeStamp(date('d-m-Y H:i'));
            $succes = SaveRecord($_geplandeActieRecord);

            // vervolg actie opslaan
            $_vervolgActieRecord['_hdnState'] = 'insert';
            $_vervolgActieRecord['ACTIENR'] = '__auto_increment__';
            $_vervolgActieRecord['RELATIENR'] = $_huidigeGeplandeActie['RELATIENR'];
            $_vervolgActieRecord['ACTIESOORT'] = 'B';
            $_vervolgActieRecord['OORZAAKSOORT'] = $_huidigeGeplandeActie['OORZAAKSOORT'];
            $_vervolgActieRecord['OORZAAKNR'] = $_huidigeGeplandeActie['OORZAAKNR'];
            $_vervolgActieRecord['OUDACTIENR'] = $_huidigeGeplandeActie['ACTIENR'];
            $_vervolgActieRecord['GEPLANDEDATUM'] = $_allesRecord['GEPLANDEDATUM'];
            $_vervolgActieRecord['BEGINTIJD'] = $_allesRecord['BEGINTIJD'];
            $_vervolgActieRecord['EINDTIJD'] = $_allesRecord['EINDTIJD'];
            $_vervolgActieRecord['ACTIEOMSCHRIJVING'] = $_allesRecord['ACTIEOMSCHRIJVING'];
            $_vervolgActieRecord['GEREALISEERDEMEDEWERKER'] = '';
//            if ($_allesRecord['PERSOONLIJKAFHANDELEN'] == 'J') // indien GEPLANDEMEDEWERKER niet is ingevuld dan
                                                                 // wordt deze op de database ingevuld met de ingelogde gebruiker
                                                                 // : PERSOONLIJKAFHANDELEN heeft dus geen enkel nut
            $_vervolgActieRecord['GEPLANDEMEDEWERKER'] = $_SESSION['userid'];
            $succes = SaveRecord($_vervolgActieRecord);
            break;
        case 'GEENINTERESSE':
            // Geen Vervolg actie
            $_geplandeActieRecord['GEREALISEERDEMEDEWERKER'] = intval($_SESSION['userid']);
            $_geplandeActieRecord['GEREALISEERDEDATUM'] = $this->database->DBTimeStamp(date('d-m-Y H:i'));
            $_geplandeActieRecord['OORZAAKGEENINTERESSE'] = $_allesRecord['OORZAAKGEENINTERESSE'];
            $_geplandeActieRecord['ANDERRESULTAAT'] = $_allesRecord['ANDERRESULTAAT'];
            $_geplandeActieRecord['TOELICHTINGGEENINTERESSE'] = $_allesRecord['TOELICHTINGGEENINTERESSE'];
            if ($_allesRecord['MAILINGSTUREN'] == 'J') {
                $_geplandeActieRecord['INSCHRIJVINGGEENINTERESSE'] = 'GI';
            } else {
                $_geplandeActieRecord['INSCHRIJVINGGEENINTERESSE'] = 'GM';
            }
            $succes = SaveRecord($_geplandeActieRecord);
            break;
        case 'INSCHRIJVING':
            // Geen Vervolg actie
            $_geplandeActieRecord['GEREALISEERDEMEDEWERKER'] = intval($_SESSION['userid']);
            $_geplandeActieRecord['GEREALISEERDEDATUM'] = $this->database->DBTimeStamp(date('d-m-Y H:i'));
            $_geplandeActieRecord['INSCHRIJVINGGEENINTERESSE'] = 'I';
            $_succes = SaveRecord($_geplandeActieRecord);
            break;
        case 'STUDIEGIDSOPSTUREN':
            // Geen Vervolg actie
            if ($_allesRecord['STUDIEGIDSNOGMAALSOPSTUREN'] == 'J') {
                $_sp = $this->database->Prepare("BEGIN PackVerzoek.CopyVerzoek(".$_huidigeGeplandeActie['OORZAAKNR']."); END;");
                $this->database->Execute($_sp);

                $_geplandeActieRecord['GEREALISEERDEMEDEWERKER'] = intval($_SESSION['userid']);
                $_geplandeActieRecord['GEREALISEERDEDATUM'] = $this->database->DBTimeStamp(date('d-m-Y H:i'));
                $_geplandeActieRecord['INSCHRIJVINGGEENINTERESSE'] = 'V';
                $_succes = SaveRecord($_geplandeActieRecord);
            }
            break;
        }

        return $_succes;
    }

    function BewaarGeplandeActie($_allesRecord) {
        $_taalcode = false;
        /* Bewaar het Relatie record */
        $_relatieId = $this->BewaarRelatieRecord($_allesRecord, $_taalcode);

        /* Bewaar de (optionele) opmerking */
        $this->BewaarRelatieOpmerking($_allesRecord, $_allesRecord['RELATIEOPMERKING'], $_relatieId);

        /* Bewaar de geplande actie */
        $this->BewaarGeplandeActieRecord($_allesRecord, $_allesRecord['ACTIENR']);

        return true;
    }

    function BewaarBetaalwijze($_allesRecord) {
        if ($_allesRecord['BETAALWIJZE'] == 'AUT' and $_allesRecord['BANKGIROREKENINGNR'] == '') {
            $msg = lang_get('bankgiroIsVerplicht');
            adodb_plr_throw(null, null, '2', $msg, false, false, $this->database, $msg);
        }
        $_betaalwijze['_hdnDatabase'] = $_allesRecord['_hdnDatabase'];
        $_betaalwijze['_hdnTable'] = 'INSCHRIJVING';
        $_betaalwijze['_hdnAction'] = 'save';
        $_betaalwijze['_hdnState'] = 'edit';
        $_betaalwijze['_hdnRecordID'] = $_allesRecord['_hdnRecordID'];
        $_betaalwijze['BETAALWIJZE'] = $_allesRecord['BETAALWIJZE'];
        $_betaalwijze['BANKGIROREKENINGNR'] = $_allesRecord['BANKGIROREKENINGNR'];
        $_betaalwijze['BETAALDAG'] = $_allesRecord['BETAALDAG'];
        $_succes = SaveRecord($_betaalwijze);
        return $_succes;
    }

    function BewaarInschrijvingOpzeggen($_allesRecord) {
        $_opzeggen['_hdnDatabase'] = $_allesRecord['_hdnDatabase'];
        $_opzeggen['_hdnTable'] = 'INSCHRIJVING';
        $_opzeggen['_hdnAction'] = 'save';
        $_opzeggen['_hdnState'] = 'edit';
        $_opzeggen['_hdnRecordID'] = $_allesRecord['_hdnRecordID'];
        $_opzeggen['REDENOPZEGGING'] = $_allesRecord['REDENOPZEGGING'];
        $_opzeggen['TOELICHTINGREDENOPZEGGING'] = $_allesRecord['TOELICHTINGREDENOPZEGGING'];
        $_opzeggen['OPZEGGINGSDATUM'] = $_allesRecord['OPZEGGINGSDATUM'];
        $_succes = SaveRecord($_opzeggen);
        return $_succes;
    }

    function BewaarBetalingsverzoekenSamenvoegen($_allesRecord) {
        $_result = false;
        if ($_allesRecord['SAMEN']) {
            $_sqlParameters = 'SELECT * FROM PARAMETERS';
            $_parameters = $this->database->GetRow($_sqlParameters);

            $_nieuwverzoek['_hdnDatabase'] = $_allesRecord['_hdnDatabase'];
            $_nieuwverzoek['_hdnTable'] = 'BETALINGSVERZOEK';
            $_nieuwverzoek['_hdnAction'] = 'save';
            $_nieuwverzoek['_hdnState'] = 'insert';
            foreach($_allesRecord['SAMEN'] as $_recordid) {
                // bepaalde het volgende betalingsregelingnummer
                $_sqlVolgendBetalingsRegelingNr = "
                SELECT NVL(MAX(VERWERKTINBETALINGSREGELING),0) + 1 AS NIEUWNR
                FROM NHA.BETALINGSVERZOEK
                WHERE RELATIENR = :relatienr
                ";

                $_betalingsregelingnr = $this->database->GetOne($_sqlVolgendBetalingsRegelingNr, array('relatienr'=>$_allesRecord['RELATIENR']));

                // update het betalingsverzoekrecord
                $_updateBetalingsverzoek = "UPDATE betalingsverzoek SET STATUS = 'BB', VERWERKTINBETALINGSREGELING = :betalingsregeling WHERE rowid = :recordid";
                $this->database->Execute($_updateBetalingsverzoek, array('betalingsregeling'=>$_betalingsregelingnr, 'recordid'=>$_recordid));

                // selecteer het betalingsverzoekrecord voor gewenste info
                $_selectBetalingsverzoek = "
                SELECT BV.BEDRAGNOGTEBETALENINEUROS, BV.BETALINGSKENMERK, BV.TERMIJNNR, BV.ORDERPICKLIJSTNR, BV.ORDERPICKLIJSTREGELNR
                , BV.BETAALWIJZECODE, BV.HERINNERINGJANEE
                , OPL.VERWERKDATUM
                FROM betalingsverzoek BV, orderpicklijst OPL
                WHERE OPL.ORDERPICKLIJSTNR (+) = BV.ORDERPICKLIJSTNR
                AND BV.rowid = :recordid";
                $_bv = $this->database->GetRow($_selectBetalingsverzoek, array('recordid'=>$_recordid));

                // kopieer een aantal waarden van het betreffende betalingsverzoek
                $_nieuwverzoek['BETALINGSKENMERK'] = $_bv['BETALINGSKENMERK'];
                $_nieuwverzoek['TERMIJNNR'] = $_bv['TERMIJNNR'];
                $_nieuwverzoek['HERINNERINGJANEE'] = $_bv['HERINNERINGJANEE'];
                $_nieuwverzoek['BEDRAGINEUROS'] = $_bv['BEDRAGNOGTEBETALENINEUROS']; // What?? * ($_allesRecord['BEDRAGINEUROS'] / BEDRAGINEUROS)
                if ($_bv['VERWERKDATUM'] == '')
                    $_nieuwverzoek['ORDERPICKLIJSTNR'] = $_bv['ORDERPICKLIJSTNR'];
                else
                    $_nieuwverzoek['ORDERPICKLIJSTNR'] = '';
                $_nieuwverzoek['ORDERPICKLIJSTREGELNR'] = $_bv['ORDERPICKLIJSTREGELNR'];

                $_nieuwverzoek['BETAALWIJZECODE'] = $_allesRecord['BETAALWIJZECODE'];  // of die van $_bv nemen?
                $_nieuwverzoek['BETALINGSREGELINGJANEE'] = 'J';
                $_nieuwverzoek['ONDERDEELVANBETALINGSREGELING'] = $_betalingsregelingnr;
                $_nieuwverzoek['VERWERKTINBETALINGSREGELING'] = '';
                $_nieuwverzoek['STATUS'] = 'GP';
                $_nieuwverzoek['VERZENDDATUM'] = $_allesRecord['VERZENDDATUM'];
                $_nieuwverzoek['FACTUURDATUM'] = $_allesRecord['VERZENDDATUM'];
                $_nieuwverzoek['FACTUURBOEKINGDATUMTIJD'] = '';
                $_nieuwverzoek['CURSISTVERVALDATUM'] = date('d-m-Y',strtotime($_allesRecord['VERZENDDATUM'].' + '.$_parameters['AANTALDAGACPTVERVALLENCURSIST'].' days'));
                $_nieuwverzoek['SYSTEEMVERVALDATUM'] = date('d-m-Y',strtotime($_allesRecord['VERZENDDATUM'].' + '.$_parameters['AANTALDAGACPTVERVALLENSYSTEEM'].' days'));
                $_nieuwverzoek['BETALINGSVERZOEKNR'] = ''; // sequence
                $_nieuwverzoek['BEDRAGBETAALDINEUROS'] = 0;
                $_nieuwverzoek['BEDRAGNOGTEBETALENINEUROS'] = 0;
                $_nieuwverzoek['INSCHRIJFNR'] = $_allesRecord['INSCHRIJFNR'];
                $_nieuwverzoek['RELATIENR'] = $_allesRecord['RELATIENR'];

                $_result = SaveRecord($_nieuwverzoek);
            }
        }
        return $_result;
    }

    function BepaalTermijnNr($inschrijfnr, $datum) {
        $dval = 'TO_DATE(\''.$datum."','dd-mm-yyyy')";

        $_sp = $this->database->PrepareSP(
        "declare RETVAL integer; BEGIN :RETVAL := NHA.PackBetalingsRegeling.CalculateAbsTermijnNr($inschrijfnr, $dval); END;"
        );
        $result = $this->database->OutParameter($_sp,$returnvalue,'RETVAL');
        $result = $this->database->Execute($_sp);
        return $returnvalue;
    }

    function BewaarInschrijvingTermijnWijzigen($_allesRecord) {
        global $polaris;

        $_termijnwijziging['_hdnDatabase'] = $_allesRecord['_hdnDatabase'];
        $_termijnwijziging['_hdnTable'] = 'INSCHRIJVING';
        $_termijnwijziging['_hdnAction'] = 'save';
        $_termijnwijziging['_hdnState'] = 'edit';
        $_termijnwijziging['_hdnRecordID'] = $_allesRecord['_hdnRecordID'];
        $_termijnwijziging['ZELFSTUDIEJANEE'] = isset($_allesRecord['ZELFSTUDIE'])?'J':'N';
        $_termijnwijziging['STUDIETEMPOCODE'] = $_allesRecord['STUDIETEMPO'];
        $_termijnwijziging['AANTALBETAALTERMIJNEN'] = $_allesRecord['BETAALTERMIJNEN'];

        if (trim($_allesRecord['CALLOPMERKING']) != '') {
            /* Bewaar de (optionele) CALL opmerking */
            $_fillAllValues = true;
            $this->BewaarRelatieOpmerking($_allesRecord, 'CALL: '.$_allesRecord['CALLOPMERKING'], $_relatieId, $_fillAllValues);
        }

        $_succes = SaveRecord($_termijnwijziging);
        return $_succes;
    }

    function BewaarBetalingsRegeling($_allesRecord) {
        global $polaris;

        $result = false;

        if ($_allesRecord['INSCHRIJFNR'] != '') {
            $_termijnnr = $this->BepaalTermijnNr($_allesRecord['INSCHRIJFNR'], $_allesRecord['INGANGSDATUM']);
            $_openstaand = $polaris->ConvertFloatValue($_allesRecord['OPENSTAAND']);
            if (!isset($_allesRecord['ADMINKOSTENKWIJTSCHELDEN']))
                $_openstaand = $_openstaand + $polaris->ConvertFloatValue($_allesRecord['OPENSTAANDADMINKOSTEN']);

            $_restbedrag = $_allesRecord['RESTBEDRAG'];
            if ($_restbedrag == '') $_restbedrag = 0;

            $_sql =
            "BEGIN NHA.PackBetalingsRegeling.NieuweBetalingsRegeling(".
                $_allesRecord['INSCHRIJFNR'].","
                ."0," /* administratiekosten ALTIJD NUL ??*/
                .$polaris->ConvertFloatValue($_openstaand).","
                .$polaris->ConvertFloatValue($_allesRecord['MAANDBEDRAG']).","
                .$polaris->ConvertFloatValue($_restbedrag).","
                .$_termijnnr.","
                .$_allesRecord['AANTALMAANDEN'].","
                .'\''.$_allesRecord['TOELICHTING'].'\''
            ."); END;";

            $_sp = $this->database->Prepare($_sql);
            $this->database->Execute($_sp);
            $result = true;
        }

        return $result;
    }

    function BewaarTeveelBetaaldBedragenVerwerken($_allesRecord) {
        global $polaris;

        $_sp = $this->database->PrepareSP(
            "BEGIN NHA.PackBetalingen.VerplaatsBetalingsOverschot(:inschrijfnr, :betalingsverzoeknr, :overigebedragenmeenemen, :toegewezenbedrag); END;"
        );

        $inschrijfnr = intval($_allesRecord['INSCHRIJFNR']);
        $overigebedragenmeenemen = 1; // JA

        if ($_allesRecord['BETALEN']) {
            foreach($_allesRecord['BETALEN'] as $_rec) {
                $_index = array_search($_rec, $_allesRecord['PLR__RECORDID']);

                $betalingsverzoeknr = intval($_allesRecord['BETALINGSVERZOEKNR'][$_index]);
                $toegewezenbedrag = $polaris->ConvertFloatValue($_allesRecord['TOEGEWEZEN'][$_index]);

                if ($toegewezenbedrag > 0) {
                    $this->database->InParameter($_sp, $inschrijfnr, 'inschrijfnr');
                    $this->database->InParameter($_sp, $betalingsverzoeknr, 'betalingsverzoeknr');
                    $this->database->InParameter($_sp, $overigebedragenmeenemen, 'overigebedragenmeenemen');
                    $this->database->InParameter($_sp, $toegewezenbedrag, 'toegewezenbedrag');
                    $this->database->Execute($_sp);
                }
            }
        }

        if ($_allesRecord['BUITENGEWONEBATEN']) {
            $betalingsverzoeknr = -1;
            $toegewezenbedrag = $polaris->ConvertFloatValue($_allesRecord['BUITENGEWONEBATENBEDRAG']);

            $this->database->InParameter($_sp, $inschrijfnr, 'inschrijfnr');
            $this->database->InParameter($_sp, $betalingsverzoeknr, 'betalingsverzoeknr');
            $this->database->InParameter($_sp, $overigebedragenmeenemen, 'overigebedragenmeenemen');
            $this->database->InParameter($_sp, $toegewezenbedrag, 'toegewezenbedrag');
            $this->database->Execute($_sp);
        }

        $result = true;
        return $result;
    }

    function BewaarLosseFactuurAanmaken($_allesRecord) {
        if (count($_allesRecord['FACTUUR']) > 0) {
            $_sqlParameters = 'SELECT * FROM PARAMETERS';
            $_parameters = $this->database->GetRow($_sqlParameters);

            $_factuur['_hdnDatabase'] = $_allesRecord['_hdnDatabase'];
            $_factuur['_hdnTable'] = 'FACTUUR';
            $_factuur['_hdnAction'] = 'save';
            $_factuur['_hdnState'] = 'insert';
            $_factuur['FACTUURNR'] = '@READGEN.seqAcceptGiroIncasso';
            $_factuur['BETAALWIJZECODE'] = 'ACP';
            $_factuur['VERZENDDATUM'] = $_allesRecord['VERZENDDATUM'];
            $_factuur['BEDRAGINEUROS'] = '0';
            $_factuur['BEGELEIDENDEBRIEFCODE'] = $_allesRecord['BEGELEIDENDEBRIEFCODE'];
            $_factuur['INSCHRIJFNR'] = $_allesRecord['INSCHRIJFNR'];
            $_factuur['AANTALKERENVERWERKT'] = '0';
            $_factuur['INZAKE'] = $_allesRecord['INZAKE'];
            $_factuur['AFDELINGNR'] = 2; // CursistAdministratie
            $_factuur['VERVALDATUM'] = date('d-m-Y',strtotime($_allesRecord['VERZENDDATUM'].' + '.$_parameters['AANTALDAGACPTVERVALLENCURSIST'].' days'));
            $_factuur['INHOUDBRIEF_PLR'] = $_allesRecord['INHOUDBRIEF_PLR'];

            if ($_allesRecord['VERSTUURDJANEE'] == 'J') {
                $_factuur['VERWERKTJANEE'] = 'J';
                $_factuur['VERWERKDATUMTIJD'] = $this->database->DBTimeStamp(date('d-m-Y H:i'));
            }

            $_factuurreturnvalues = SaveRecord($_factuur);

            $_factuurBetalingsVerzoek['_hdnDatabase'] = $_allesRecord['_hdnDatabase'];
            $_factuurBetalingsVerzoek['_hdnTable'] = 'FACTUURBETALINGSVERZOEK';
            $_factuurBetalingsVerzoek['_hdnAction'] = 'save';
            $_factuurBetalingsVerzoek['_hdnState'] = 'insert';

            foreach($_allesRecord['FACTUUR'] as $_recordid) {
                $_index = array_search($_recordid, $_allesRecord['PLR__RECORDID']);

                // selecteer het betalingsverzoekrecord voor gewenste info
                $_selectBetalingsverzoek = "
                SELECT BV.BETALINGSVERZOEKNR, BV.BEDRAGINEUROS, BV.BEDRAGBETAALDINEUROS
                , BV.BEDRAGINEUROS - BV.BEDRAGBETAALDINEUROS AS BEDRAGINEUROSOPFACTUUR
                , BV.BETALINGSKENMERK, BV.TERMIJNNR
                FROM betalingsverzoek BV
                WHERE BV.rowid = :recordid";
                $_bv = $this->database->GetRow($_selectBetalingsverzoek, array('recordid'=>$_recordid));

                $_factuurBetalingsVerzoek['FACTUURNR'] = $_factuurreturnvalues['FACTUURNR'];
                $_factuurBetalingsVerzoek['BETALINGSVERZOEKNR'] = $_bv['BETALINGSVERZOEKNR'];
                $_factuurBetalingsVerzoek['BEDRAGINEUROS'] = $_bv['BEDRAGINEUROS'];
                $_factuurBetalingsVerzoek['BEDRAGBETAALDINEUROS'] = $_bv['BEDRAGBETAALDINEUROS'];
                $_factuurBetalingsVerzoek['BEDRAGINEUROSOPFACTUUR'] = $_bv['BEDRAGINEUROSOPFACTUUR'];
                $_factuurBetalingsVerzoek['TERMIJNNR'] = $_bv['TERMIJNNR'];
                $_factuurBetalingsVerzoek['BETALINGSKENMERK'] = $_bv['BETALINGSKENMERK'];

                SaveRecord($_factuurBetalingsVerzoek);
            }
            return true;
        } else {
            return false;
        }
    }

    /*****
    * Bewaar Cursusgegevens
    */
    function BewaarCursusGegevens($_allesRecord, $_lessen) {
        global $polaris;

        $this->database->StartTrans();
        $_cursuscode = strtoupper($_allesRecord['CURSUSCODE']);
        // inschrijvingen met foutieve studietempocode goedzetten; op basis van de gekozen studietempo in het Cursusscherm
        if (isset($_allesRecord['JUISTSTUDIETEMPO']) and $_allesRecord['JUISTSTUDIETEMPO'] != 'undefined' and $_allesRecord['JUISTSTUDIETEMPO'] != '') {
            $_updateOnjuisteStudieTempo = "
                UPDATE INSCHRIJVING_MATINCOMPLEET I
                SET I.STUDIETEMPOCODE = :STUDIETEMPOCODE
                WHERE I.CURSUSCODE = :CURSUSCODE
                AND I.STUDIETEMPOCODE NOT IN (
                    SELECT DISTINCT C.STUDIETEMPOCODE
                    FROM CURSUSSTUDIETEMPOTERMIJN C
                    WHERE C.CURSUSCODE = I.CURSUSCODE
                )
            ";
            $this->database->Execute($_updateOnjuisteStudieTempo, array("CURSUSCODE"=>$_cursuscode, "STUDIETEMPOCODE"=>$_allesRecord['JUISTSTUDIETEMPO']));
        }

        $_cursusRecord['_hdnDatabase'] = $_allesRecord['_hdnDatabase'];
        $_cursusRecord['_hdnTable'] = $_allesRecord['_hdnTable'];
        $_cursusRecord['_hdnAction'] = 'save';
        $_cursusRecord['_hdnState'] = $_allesRecord['_hdnState'] == 'insert' ? 'insert' : 'edit';
        $_cursusRecord['_hdnRecordID'] = $_allesRecord['_hdnRecordID'];
        $_cursusRecord['CURSUSCODE'] = $_cursuscode;
        $_cursusRecord['CURSUSNAAM'] = $_allesRecord['CURSUSNAAM'];
        $_cursusRecord['CURSUSGROEP'] = $_allesRecord['CURSUSGROEP'];
        $_cursusRecord['BEGELEIDINGMETOFZONDER'] = $_allesRecord['BEGELEIDINGMETOFZONDER'];
//        $_cursusRecord['TOTAALAANTALLESSEN'] = $_allesRecord['TOTAALAANTALLESSEN'];
        $_cursusRecord['AANTALRINGBANDEN'] = $_allesRecord['AANTALRINGBANDEN'];
        $_cursusRecord['EXTERNINTERNEXAMEN'] = $_allesRecord['EXTERNINTERNEXAMEN'];
        $_cursusRecord['EXAMENGELD@@FLOATCONVERT'] = $_allesRecord['EXAMENGELD@@FLOATCONVERT'];
        $_cursusRecord['AANTALLESSENOPZEGGING'] = $_allesRecord['AANTALLESSENOPZEGGING'];
        $_cursusRecord['AANTALMAANDENONDERBREKING'] = $_allesRecord['AANTALMAANDENONDERBREKING'];
        $_cursusRecord['BELLENNAGEENREACTIEJANEE'] = $_allesRecord['BELLENNAGEENREACTIEJANEE'];
        // Doet niet meer mee $_cursusRecord['MIDDELBAARONDERWIJSSOORT'] = $_allesRecord['MIDDELBAARONDERWIJSSOORT'];
        // Doet niet meer mee $_cursusRecord['NIVEAU'] = $_allesRecord['NIVEAU'];
        $_cursusRecord['STUDIEGIDSCODE'] = $_allesRecord['STUDIEGIDSCODE'];
        $_cursusRecord['STANDAARDSTUDIETEMPO'] = $_allesRecord['STANDAARDSTUDIETEMPO'];
        // Doet niet meer mee $_cursusRecord['HBOOVERIGE'] = $_allesRecord['HBOOVERIGE'];
        $_cursusRecord['RETOURNEERBAARJANEE'] = $_allesRecord['RETOURNEERBAARJANEE'];
        $adminParts = explode('-', $_allesRecord['ADMINISTRATIE']);
        $_cursusRecord['ACADEMIEOFPW'] = $adminParts[0];
        $_cursusRecord['EIGENADRES'] = $adminParts[1];
        $_cursusRecord['GEBLOKKEERDJANEE'] = $_allesRecord['GEBLOKKEERDJANEE'];
        $_cursusRecord['STANDAARDBETAALTEMPOTERMIJNEN'] = $_allesRecord['STANDAARDBETAALTEMPOTERMIJNEN'];
        $_cursusRecord['STICKERVENSTERENVELOPE1'] = $_allesRecord['STICKERVENSTERENVELOPE1'];
        $_cursusRecord['STICKERVENSTERENVELOPEN'] = $_allesRecord['STICKERVENSTERENVELOPEN'];
        $_cursusRecord['OPZEGBAARJANEE'] = $_allesRecord['OPZEGBAARJANEE'];
        $_cursusRecord['LIGTOPSORTEERPOSITIE'] = $_allesRecord['LIGTOPSORTEERPOSITIE'];
        $_cursusRecord['LEAFLETCODE'] = $_allesRecord['LEAFLETCODE'];
        $_cursusRecord['AANTALLESSENPERMAANDBIJOPZEG'] = $_allesRecord['AANTALLESSENPERMAANDBIJOPZEG'];
        $_cursusRecord['ZENDINGINDIENBETAALDJANEE'] = $_allesRecord['ZENDINGINDIENBETAALDJANEE'];
        $_cursusRecord['CURSUSDUURMAANDEN'] = $_allesRecord['CURSUSDUURMAANDEN'];
        $_cursusRecord['MATERIAALCOMPLEETJANEE'] = $_allesRecord['MATERIAALCOMPLEETJANEE'];
        $_cursusRecord['MATERIAALVERWACHTDATUM'] = $_allesRecord['MATERIAALVERWACHTDATUM'];
        $_cursusRecord['TEGEMOETKOMINGWACHTTIJD'] = $_allesRecord['TEGEMOETKOMINGWACHTTIJD'];
        $_cursusRecord['INSCHRIJVINGENBLOKKERENJANEE'] = $_allesRecord['INSCHRIJVINGENBLOKKERENJANEE'];
        $_cursusRecord['MBOHBONIVEAU'] = $_allesRecord['MBOHBONIVEAU'];
        $_cursusRecord['WEBOMSCHRIJVING'] = $_allesRecord['WEBOMSCHRIJVING'];
        $_cursusRecord['ALLEENZICHTBAARVOORBACKOFF'] = $_allesRecord['ALLEENZICHTBAARVOORBACKOFF'];
        $_cursusRecord['LESBLOKPLUSENVELOP'] = $_allesRecord['LESBLOKPLUSENVELOP'];
        $_cursusRecord['SOORT_CURSUS'] = $_allesRecord['SOORT_CURSUS'];
        $_cursusRecord['SOORTONDERWIJS'] = $_allesRecord['SOORTONDERWIJS'];
        $_cursusRecord['LEERWEG'] = $_allesRecord['LEERWEG'];
        $_cursusRecord['DATUMGELDIGVANAF'] = $_allesRecord['DATUMGELDIGVANAF'];
        $_cursusRecord['DATUMGELDIGTM'] = $_allesRecord['DATUMGELDIGTM'];
        $_cursusRecord['ONDERTEK_VOOR_VERSTUREN_JN'] = $_allesRecord['ONDERTEK_VOOR_VERSTUREN_JN'];
        $_cursusRecord['INSCHRIJFFORM_PER_EMAIL_JN'] = $_allesRecord['INSCHRIJFFORM_PER_EMAIL_JN'];
        $_cursusRecord['PLATHOSID'] = $_allesRecord['PLATHOSID'];
        $_cursusRecord['GETUIGSCHRIFTJANEE'] = $_allesRecord['GETUIGSCHRIFTJANEE'];
        $_cursusRecord['CURSUSCLUSTERNR'] = $_allesRecord['CURSUSCLUSTERNR'];
        if (!is_array($_allesRecord['CNUMMER']))
            $_cursusRecord['CNUMMER'] = $_allesRecord['CNUMMER'];

        if ($_allesRecord['SOORT_CURSUS'] == 'C') {
            $_cursusRecord['COHORT'] = $_allesRecord['COHORT_COHORT'];
            $_cursusRecord['KDCODE'] = $_allesRecord['COHORT_KDCODE'];
            $_cursusRecord['DOSSIERCOHORT'] = $_allesRecord['COHORT_DOSSIERCOHORT'];
            $_cursusRecord['CNUMMER'] = $_allesRecord['COHORT_CNUMMER'];
        }
        $_result = SaveRecord($_cursusRecord);

        if (intval($_allesRecord['AANTALSEALPAKKETTEN']) > 0) {
            $_cursuscode = $_allesRecord['CURSUSCODE'];
            $_aantalNieuweSP = intval($_allesRecord['AANTALSEALPAKKETTEN']);
            $_aantalSP = $this->database->GetOne("SELECT COUNT(*) FROM CURSUSSEALPAKKETVOORRAAD WHERE CURSUSCODE = :CURSUSCODE", array("CURSUSCODE"=>$_cursuscode));
            if ($_aantalNieuweSP > 0 and $_aantalSP <> $_aantalNieuweSP) {
                $_deleteSQL = "DELETE FROM CURSUSSEALPAKKETVOORRAAD WHERE CURSUSCODE = :CURSUSCODE";
                $this->database->Execute($_deleteSQL, array("CURSUSCODE"=>$_cursuscode));

                $_sql = "INSERT INTO CURSUSSEALPAKKETVOORRAAD (CURSUSCODE, SEALPAKKETNR, VOORRAAD)
                VALUES (:CURSUSCODE, :SEALPAKKETNR, :VOORRAAD)";
                for($i=1;$i<=$_aantalNieuweSP;$i++) {
                    $this->database->Execute($_sql, array(
                        "CURSUSCODE"=>$_cursuscode,
                        "SEALPAKKETNR"=>$i,
                        "VOORRAAD"=>20000,
                    ));
                }
            }
        }

        /*
        if ($_allesRecord['SOORT_CURSUS'] == 'C') {
            if ($_allesRecord['GEBUFFERDJANEE'] == 'N') {
                $_bufferSQL = "DELETE FROM GEBUFFERDEMBOCURSUSSEN WHERE CURSUSCODE = :CURSUSCODE";
            } elseif ($_allesRecord['GEBUFFERDJANEE'] == 'J') {
                $_bufferSQL = "INSERT INTO GEBUFFERDEMBOCURSUSSEN (CURSUSCODE) VALUES(:CURSUSCODE)";
            }
            $this->database->Execute($_bufferSQL, array("CURSUSCODE"=>$_allesRecord['CURSUSCODE']));
        }
        */

        $_deleteSQL = "DELETE FROM CURSUSSTUDIETEMPOTERMIJN WHERE CURSUSCODE = :CURSUSCODE";
        $this->database->Execute($_deleteSQL, array("CURSUSCODE"=>$_allesRecord['CURSUSCODE']));

        $_deleteSQL = "DELETE FROM CURSUSTERMIJNVERZENDKOSTEN WHERE CURSUSCODE = :CURSUSCODE";
        $this->database->Execute($_deleteSQL, array("CURSUSCODE"=>$_allesRecord['CURSUSCODE']));

        if (count($_allesRecord['CURSUSCODE_STT']) > 0) {
            array_walk($_lessen, 'LessenCallback');

            $_doneStudieTempo = array();
            $_sqlCTVK = "INSERT INTO CURSUSTERMIJNVERZENDKOSTEN (CURSUSCODE, STUDIETEMPOCODE, TERMIJNNR, VERZENDKOSTEN, VERZENDKOSTENBELGIE)
            VALUES (:CURSUSCODE, :STUDIETEMPOCODE, :TERMIJNNR, 0, 0)";
            foreach($_allesRecord['CURSUSCODE_STT'] as $_index => $_rec) {

                if (array_search($_allesRecord['STUDIETEMPOCODE'][$_index].'_'.$_allesRecord['TERMIJNNR'][$_index], $_doneStudieTempo) === FALSE) {
                    $this->database->Execute($_sqlCTVK, array(
                        "CURSUSCODE"=>$_allesRecord['CURSUSCODE_STT'][$_index],
                        "STUDIETEMPOCODE"=>$_allesRecord['STUDIETEMPOCODE'][$_index],
                        "TERMIJNNR"=>$_allesRecord['TERMIJNNR'][$_index],
                    ));
                    $_doneStudieTempo[] = $_allesRecord['STUDIETEMPOCODE'][$_index].'_'.$_allesRecord['TERMIJNNR'][$_index];
                }

                $_begin = intval($_allesRecord['BEGINLES'][$_index]);
                $_eind = intval($_allesRecord['EINDLES'][$_index]);
                $_sqlCSTT = "INSERT INTO CURSUSSTUDIETEMPOTERMIJN (CURSUSCODE, STUDIETEMPOCODE, TERMIJNNR, LESNR)
                VALUES (:CURSUSCODE, :STUDIETEMPOCODE, :TERMIJNNR, :LESNR)";
//                $this->database->debug=true;
                for($i=$_begin;$i<=$_eind;$i++) {
                    if (array_search($i, $_lessen) !== FALSE) {
                        $this->database->Execute($_sqlCSTT, array(
                            "CURSUSCODE"=>$_allesRecord['CURSUSCODE_STT'][$_index],
                            "STUDIETEMPOCODE"=>$_allesRecord['STUDIETEMPOCODE'][$_index],
                            "TERMIJNNR"=>$_allesRecord['TERMIJNNR'][$_index],
                            "LESNR"=>$i,
                        ));
                    }
                }
            }
        }

        $_deleteSQL = "DELETE FROM CURSUSBETAALTEMPOTERMIJN WHERE CURSUSCODE = :CURSUSCODE";
        $this->database->Execute($_deleteSQL, array("CURSUSCODE"=>$_allesRecord['CURSUSCODE']));

        if (count($_allesRecord['CURSUSCODE_BTT']) > 0) {
            array_walk($_lessen, 'LessenCallback');

            foreach($_allesRecord['CURSUSCODE_BTT'] as $_index => $_rec) {
                $_aantaltermijnen = intval($_allesRecord['AANTALTERMIJNEN'][$_index]);
                $_zelfstudie = $_allesRecord['ZELFSTUDIEJANEE'][$_index];
                $_bedrag = $polaris->ConvertFloatValue($_allesRecord['BEDRAGINEUROS'][$_index]);
                $_bedragbe = $polaris->ConvertFloatValue($_allesRecord['BEDRAGINEUROS_BELGIE'][$_index]);
                $_bedragbeoorspr = $polaris->ConvertFloatValue($_allesRecord['BEDRAGINEUROS_BELGIE_OORSPR'][$_index]);

                $_sql = "INSERT INTO CURSUSBETAALTEMPOTERMIJN (CURSUSCODE, AANTALTERMIJNEN, ZELFSTUDIEJANEE, BETAALTERMIJNNR, VERZENDTERMIJNNR, BEDRAGINEUROS, BEDRAGINEUROS_BELGIE, BEDRAGINEUROS_BELGIE_OORSPR)
                VALUES (:CURSUSCODE, :AANTALTERMIJNEN, :ZELFSTUDIEJANEE, :BETAALTERMIJNNR, :VERZENDTERMIJNNR, :BEDRAGINEUROS, :BEDRAGINEUROS_BELGIE, :BEDRAGINEUROS_BELGIE_OORSPR)";
                for($i=1;$i<=$_aantaltermijnen;$i++) {
                    $this->database->Execute($_sql, array(
                        "CURSUSCODE"=>$_allesRecord['CURSUSCODE_BTT'][$_index],
                        "AANTALTERMIJNEN"=>$_aantaltermijnen,
                        "ZELFSTUDIEJANEE"=>$_zelfstudie,
                        "BETAALTERMIJNNR"=>$i,
                        "VERZENDTERMIJNNR"=>$i,
                        "BEDRAGINEUROS"=>$_bedrag,
                        "BEDRAGINEUROS_BELGIE"=>$_bedragbe,
                        "BEDRAGINEUROS_BELGIE_OORSPR"=>$_bedragbeoorspr,
                    ));
                }
            }
        }

        $_sqlCohort = "UPDATE CURSUS SET
        DATUMGELDIGVANAF = :DATUMGELDIGVANAF,
        DATUMGELDIGTM = :DATUMGELDIGTM,
        COHORT = :COHORT,
        ISCOHORTCURSUSVAN = :ISCOHORTCURSUSVAN,
        KDCODE = :KDCODE,
        DOSSIERCOHORT = :DOSSIERCOHORT,
        CNUMMER = :CNUMMER
        WHERE CURSUSCODE = :CURSUSCODE";

        if (count($_allesRecord['CURSUSCODE_CH']) > 0) {

            $_updateSQL = "UPDATE CURSUS SET SOORT_CURSUS = 'A' WHERE CURSUSCODE = :CURSUSCODE";
            $this->database->Execute($_updateSQL, array("CURSUSCODE"=>$_cursusRecord['CURSUSCODE']));

            foreach($_allesRecord['CURSUSCODE_CH'] as $_index => $_rec) {
                $_datumGeldigVanaf = $_allesRecord['COHORT_DATUMGELDIGVANAF'][$_index];
                $_datumGeldigTM = $_allesRecord['COHORT_DATUMGELDIGTM'][$_index];
                $_cohort = $_allesRecord['COHORT'][$_index];
                $_isCohortCursusVan = $_allesRecord['ISCOHORTCURSUSVAN'][$_index];
                $_kdCode = $_allesRecord['KDCODE'][$_index];
                $_dossierCohort = $_allesRecord['DOSSIERCOHORT'][$_index];
                $_cNummer = $_allesRecord['CNUMMER'][$_index];

                $this->database->Execute($_sqlCohort, array(
                    "DATUMGELDIGVANAF"=>$_datumGeldigVanaf,
                    "DATUMGELDIGTM"=>$_datumGeldigTM,
                    "COHORT"=>$_cohort,
                    "ISCOHORTCURSUSVAN"=>$_isCohortCursusVan,
                    "KDCODE"=>$_kdCode,
                    "DOSSIERCOHORT"=>$_dossierCohort,
                    "CNUMMER"=>$_cNummer,
                    "CURSUSCODE"=>$_rec,
                ));
            }
        }
        // Zet DATUMGELDIGTM van algemene cursus op NUL, zodat deze 's nachts wordt gesynced
        if ($_allesRecord['SOORT_CURSUS'] == 'C') {
            $_updateAlgemene = "UPDATE CURSUS SET DATUMGELDIGTM = NULL WHERE CURSUSCODE = (SELECT ISCOHORTCURSUSVAN FROM CURSUS WHERE CURSUSCODE = :CURSUSCODE)";
            $this->database->Execute($_updateAlgemene, array("CURSUSCODE"=>$_cursusRecord['CURSUSCODE']));
        }

        $this->ClusterInschrijvingWijzigen($_cursusRecord['CURSUSCODE']);

        if ($_result) {
            $_result = $this->database->CompleteTrans();
        }

        return $_result;
    }

    function ClusterInschrijvingWijzigen($cursuscode) {
        $_sp = $this->database->PrepareSP(
        "BEGIN
            NHA.PackCursus.ClusterInschrijvingWijzigen(:CURSUSCODE);
        END;"
        );
        $this->database->InParameter($_sp, $cursuscode, 'CURSUSCODE');
        $result = $this->database->Execute($_sp);

        return $returnvalue;
    }

    // function SyncNaarAlgemeneCursus($_cohortCursusCode) {
    //     $_sp = $this->database->PrepareSP(
    //     "BEGIN
    //         NHA.PackCursus.CohortCursusSyncen(:CURSUSCODE);
    //     END;"
    //     );
    //     $this->database->InParameter($_sp, $_cohortCursusCode, 'CURSUSCODE');
    //     $result = $this->database->Execute($_sql);

    //     return $returnvalue;
    // }

    function CohortCursusOpslaan($_allesRecord, $soortActie) {
        if ($soortActie == 'nieuw') {
            if ($_allesRecord['NEEMINFOVANALGEMENE'] == 'N')
                $_procname = 'CohortCursusToevoegen';
            else
                $_procname = 'MaakCohortKopieVanAlgemene';
        } elseif ($soortActie == 'kopie') {
            $_procname = 'MaakCompleteKopieVanCohort';
        }
        $_sp = $this->database->PrepareSP(
        "DECLARE
            lNieuweCohortCursus VARCHAR2(15);
        BEGIN
            :lNieuweCohortCursus := NHA.PackCursus.$_procname(:CURSUSCODE, :JAARGELDIGVANAF, :JAARGELDIGTM);
        END;"
        );

        $this->database->InParameter($_sp, $_allesRecord['CURSUSCODE'], 'CURSUSCODE');
        $this->database->InParameter($_sp, $_allesRecord['JAARGELDIGVANAF'], 'JAARGELDIGVANAF');
        $this->database->InParameter($_sp, $_allesRecord['JAARGELDIGTM'], 'JAARGELDIGTM');
        $result = $this->database->OutParameter($_sp, $returnvalue,'lNieuweCohortCursus');
        $result = $this->database->Execute($_sp);
        return $returnvalue;
    }

    function BewaarCohortCursus($_allesRecord) {
        return $this->CohortCursusOpslaan($_allesRecord, 'nieuw');
    }

    function KopieerCohortCursus($_allesRecord) {
        return $this->CohortCursusOpslaan($_allesRecord, 'kopie');
    }

    function BewaarCohort($_allesRecord) {
        $_sql = "INSERT INTO NHA.COHORT(COHORTCODE, SOORTCOHORT) VALUES (:COHORTCODE, :SOORTCOHORT)";

        try {
            $_result = $this->database->Execute($_sql, array("COHORTCODE"=>$_allesRecord['jaarperiode'], "SOORTCOHORT"=>$_allesRecord['soortcohort']));
        } catch (ADODB_Exception $E) {
            $msg = 'Deze cohort bestaat al. ('.$E->msg.')';
            adodb_plr_throw($E->dbms, $E->fn, $E->getCode(), $E->msg, $E->p1, $E->p2, $this->database, $msg);
        }

        return $_result;
    }

    /*****
    * Verwijder cursus
    */
    function VerwijderCursus($_allesRecord) {
        global $polaris;

        $_result = false;
        try {
            if (isset($_allesRecord['CURSUSCODE'])) {
                $_cursusCode = strtoupper($_allesRecord['CURSUSCODE']);

                $_sp = $this->database->PrepareSP("BEGIN NHA.PackCursus.VerwijderCompleteCursus(:CURSUSCODE); END;");
                $this->database->InParameter($_sp, $_cursusCode, 'CURSUSCODE');
                $this->database->Execute($_sp);
                $_result = true;
            }
        } catch (ADODB_Exception $E) {
            $msg = 'Deze cursuscode kan niet verwijderd worden. ('.$E->msg.')';
            adodb_plr_throw($E->dbms, $E->fn, $E->getCode(), $E->msg, $E->p1, $E->p2, $this->database, $msg);
        }

        return $_result;
    }

    /*****
    * Bewaar module
    */
    function BewaarModule($_allesRecord) {
        global $polaris;

        $_result = false;
        try {
            if (isset($_allesRecord['MODULECODE'])) {
                $_moduleCode = strtoupper($_allesRecord['MODULECODE']);
                $_moduleNaam = $_allesRecord['MODULENAAM'];
                $_sql ="INSERT INTO MODULE (MODULECODE, NAAM) VALUES (:MODULECODE, :MODULENAAM)";
                $_result = $this->database->Execute($_sql, array(
                    "MODULECODE"=>$_moduleCode,
                    "MODULENAAM"=>$_moduleNaam
                ));
            }
        } catch (ADODB_Exception $E) {
            $msg = 'Deze modulecode bestaat al. ('.$E->msg.')';
            adodb_plr_throw($E->dbms, $E->fn, $E->getCode(), $E->msg, $E->p1, $E->p2, $this->database, $msg);
        }
        if ($_result and intval($_allesRecord['AANTALLESSEN']) > 0) {
            $_aantallessen = intval($_allesRecord['AANTALLESSEN']);
            $_sql = "INSERT INTO MODULELES (MODULECODE, LESNR, OMSCHRIJVING, UREN, INTRINSIEKEWAARDE)
            VALUES (:MODULECODE, :LESNR, :OMSCHRIJVING, 1, 0) ";
            for($i=1;$i<=$_aantallessen;$i++) {
                $_result = $this->database->Execute($_sql, array(
                    "MODULECODE"=>$_moduleCode,
                    "LESNR"=>$i,
                    "OMSCHRIJVING"=>$_moduleNaam
                ));
            }
            $_result = true;
        }

        return $_result;
    }

    /*****
    * Verwijder module
    */
    function VerwijderModule($_allesRecord) {
        global $polaris;

        $_result = false;
        try {
            if (isset($_allesRecord['MODULECODE'])) {
                $_moduleCode = strtoupper($_allesRecord['MODULECODE']);

                $_sql = "DELETE MODULELES WHERE MODULECODE= :MODULECODE";
                $_result = $this->database->Execute($_sql, array(
                    "MODULECODE"=>$_moduleCode,
                ));

                $_sql = "DELETE MODULE WHERE MODULECODE= :MODULECODE";
                $_result = $this->database->Execute($_sql, array(
                    "MODULECODE"=>$_moduleCode,
                ));
            }
        } catch (ADODB_Exception $E) {
            $msg = 'Deze modulecode kan niet verwijderd worden. ('.$E->msg.')';
            adodb_plr_throw($E->dbms, $E->fn, $E->getCode(), $E->msg, $E->p1, $E->p2, $this->database, $msg);
        }

        return $_result;
    }

    /*****
    * Bewaar modulelessen
    */
    function BewaarModuleLessen($_allesRecord) {
        global $polaris;

        $_result = true;
        $this->database->debug = false;
        try {
            if (isset($_allesRecord['MODULECODE'])) {
                $_moduleCode = strtoupper($_allesRecord['MODULECODE']);
                $_moduleNaam = $_allesRecord['MODULENAAM'];
                $_sql ="UPDATE MODULE SET NAAM = :MODULENAAM WHERE MODULECODE = :MODULECODE";
                $this->database->Execute($_sql, array(
                    "MODULENAAM"=>$_moduleNaam,
                    "MODULECODE"=>$_moduleCode,
                ));
            }
        } catch (ADODB_Exception $E) {
            $msg = 'Het wijzigen van de modulenaam is niet mogelijk. ('.$E->msg.')';
            adodb_plr_throw($E->dbms, $E->fn, $E->getCode(), $E->msg, $E->p1, $E->p2, $this->database, $msg);
        }

        if ($_result) {
            foreach($_allesRecord['LESNR'] as $_index => $_lesnr) {
                $_state = $_allesRecord['STATE'][$_index];
                if ($_state == 'DELETE') {
                    $_sql = "DELETE MODULELES WHERE MODULECODE = '$_moduleCode' AND LESNR = $_lesnr";
                    $this->database->Execute($_sql);
                }
            }

            $_sqlUpdate = "UPDATE MODULELES SET OMSCHRIJVING = :OMSCHRIJVING WHERE MODULECODE = :MODULECODE AND LESNR = :LESNR";
            $_sqlInsert = "INSERT INTO MODULELES (MODULECODE, LESNR, OMSCHRIJVING, UREN, INTRINSIEKEWAARDE) VALUES (:MODULECODE, :LESNR, :OMSCHRIJVING, 1, 0) ";

            foreach($_allesRecord['LESNR'] as $_index => $_lesnr) {
                $_state = $_allesRecord['STATE'][$_index];
                $_omschrijving = $_allesRecord['OMSCHRIJVING'][$_index];
                if ($_state == 'UPDATE') {
                    $_stmt = $_sqlUpdate;
                } elseif ($_state == 'INSERT') {
                    $_stmt = $_sqlInsert;
                } else {
                    $_stmt = false;
                }
                if ($_stmt) {
                    $this->database->Execute($_stmt, array(
                        "MODULECODE"=>$_moduleCode,
                        "LESNR"=>$_lesnr,
                        "OMSCHRIJVING"=>$_omschrijving
                    ));
                }
            }
        }

        return $_result;
    }

    function BewaarLeaflets($allesRecord) {
        global $polaris;

        $_inschrijfnrOk = Array();
        foreach($allesRecord['inschrijfnr'] as $_key => $_inschrijfnr) {
            if ($allesRecord['_hdnStatus'][$_key] == 'ok') {
                $_sql = "UPDATE INSCHRIJVING SET ONTVANGENLEAFLET = :LEAFLET, DATUMONTVANGENLEAFLET = SYSDATE WHERE INSCHRIJFNR = :INSCHRIJFNR";
                $_leaflet = $allesRecord['leaflet'][$_key];
                if ($this->database->Execute($_sql, array(
                    "LEAFLET" => $_leaflet,
                    "INSCHRIJFNR" => $_inschrijfnr
                ))) {
                    $_inschrijfnrOk[] = $_inschrijfnr;
                };
            }
        }
        return $_inschrijfnrOk;
    }

}

?>