<?php
require_once('includes/mainactions.inc.php');
require_once('_shared/module._base.php');

class Module extends _BaseModule {
    var $database; // plr database object
    var $cacheTimeOut;

    function Module($moduleid, $module) {
        global $_GVARS;
        global $polaris;

        parent::_BaseModule($moduleid, $module);


        $this->processed = false;
        /**
        * What client are we talking about? (nog doen: eventueel selecteerbaar maken)
        */
        $this->clientid = $_SESSION['clientid'];

        $this->currentdb = 3;

        $this->database = new plrDatabase($polaris);
        $this->database->LoadRecord(array($this->clientid, $this->currentdb));
        $this->userdatabase = $this->database->connectUserDB();

        /* Show the Delete button in list view */
        $this->showDefaultButtons = true;
    }

    function Process() {
        if ($this->form->database)
            $this->form->database->SetOracleUserID($_SESSION['userid']);
    }
    
    function GetExportRecords($exportsetid) {
        $_sql = 'SELECT * FROM EXPORTRECORD WHERE EXPORTSET = :EXPORTSET';
        $_rs = $this->userdatabase->GetAll($_sql, array('EXPORTSET' => $exportsetid));

        return json_encode($_rs);
    }
    
    function GetExportFields($exportsetid, $recordid) {
        $_sql = 'SELECT * FROM EXPORTFIELD WHERE EXPORTSET = :EXPORTSET AND RECORDID = :RECORDID';
        $_rs = $this->userdatabase->GetAll($_sql, array('EXPORTSET' => $exportsetid, 'RECORDID' => $recordid));

        return json_encode($_rs);
    }
    
    function DisplayImportExport($moduleid, $params) {
        global $_GVARS;
        
        $this->smarty->display("impexp.tpl.php");
    }

    function Show($outputformat='xhtml', $rec=false) {
        global $_GVARS;
        global $polaris;

        parent::Show($outputformat, $rec);

        if (isset($_GET['action'])) {
            /**
            * Show the Polaris generated forms when the user wants to insert or edit an item
            */
            switch($_GET['action']) {
            case 'edit':
            case 'insert':
                $this->form->ShowFormView($this->state, $this->permission, $detail=false, $masterrecord=false);
                $this->form->ShowButtons($this->state, $this->permission);
                $this->form->ShowNavigationLinks($this->state, $this->permission, $detail=false);
            break;
            case 'searchext':
                $this->form->ShowSearchForm($this->state, $this->permission, $detail=false, $masterrecord=false);
                $this->form->ShowButtons($this->state, $this->permission);
                $this->form->ShowNavigationLinks($this->state, $this->permission, $detail=false);
            break;
            }
        } else {
            /**
            * Show the custommade forms when user views a list of items
            */
            switch ($this->moduleid) {
            case 1:
                /***
                * Import Export 
                */
                
                if ($_GET['func'] != '') {
                    if ($_GET['func'] == 'exportrecords') {
                        echo $this->GetExportRecords($_GET['exportset']);
                    }
                    if ($_GET['func'] == 'exportfields') {
                        echo $this->GetExportFields($_GET['exportset'], $_GET['recordid']);
                    }
                    
                } else {
                    $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/sharedlib.js';
                    $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/impexp.js';
                    $this->DisplayImportExport($this->moduleid, $_GET);
                }
            break;
            }
        }
    }

}