<?php
require_once('includes/mainactions.inc.php');
require_once('_shared/module._base.php');

class Module extends _BaseModule {
    var $database; // plr database object
    var $cacheTimeOut;

    function Module($moduleid, $module) {
        global $_GVARS;
        global $polaris;

        parent::_BaseModule($moduleid, $module);

        $this->processed = false;
        /**
        * What client are we talking about? (nog doen: eventueel selecteerbaar maken)
        */
        $this->clientid = $_SESSION['clientid'];

        $this->currentdb = 14;

        $this->database = new plrDatabase($polaris);
        $this->database->LoadRecord(array($this->clientid, $this->currentdb));
        $this->userdatabase = $this->database->connectUserDB();
        /* Show the Delete button in list view */
        $this->showDefaultButtons = true;
    }

    function Process() {
        if ($this->form->database)
            $this->form->database->SetOracleUserID($_SESSION['userid']);
        if (isset($this->userdatabase))
            $this->userdatabase->SetFetchMode(ADODB_FETCH_ASSOC);

        if (isset($_POST['_hdnProcessedByModule'])) {
            $_allesRecord = $_POST;

            if (isset($_allesRecord['_hdnFORMTYPE'])) {
                try {
                    switch($_allesRecord['_hdnFORMTYPE']) {
                    /***
                    * Acties verwerken
                    */
                    case 'batchverwerken':
                        $result = $this->VerwerkBatch($_allesRecord);
                    break;
                    case 'signaalverwerken':
                        $result = $this->VerwerkSignaal($_allesRecord);
                    case 'bronoverzicht':
                        $result = $this->VoerQueryUit($_allesRecord);
                    break;
                    case 'gbagegevensverwerken':
                        $result = $this->VerwerkGBAGegevens($_allesRecord);
                    break;
                    case 'nogmaalsdoorsturen':
                        $result = $this->NogmaalsDoorsturen($_allesRecord);
                    break;
                    }

                    $resultArray = Array('result'=>$result, 'query'=>$_SERVER['REQUEST_URI'], 'error'=>false);
                    echo json_encode($resultArray);
                    exit;
                } catch (Exception $E) {
                    $detailedmessage = $E->completemsg."\r\n".parse_backtrace(debug_backtrace());

                    $resultArray = Array('result'=>false, 'query'=>$_SERVER['REQUEST_URI'], 'error'=>$E->msg, 'detailed_error'=>$detailedmessage);
                    echo  json_encode($resultArray);
                }
            }
        }
    }

    function VerwerkBatch($_allesRecord) {
        $_sql = "UPDATE BRON.imp_signaal_499 SET VERWERKT_YN = :ONOFF WHERE BATCHNUMMER_AANLEVERBESTAND = :BATCHID";
        $_rs = $this->userdatabase->Execute($_sql, array('ONOFF'=>$_allesRecord['SIGNAAL_ONOFF'],'BATCHID'=>$_allesRecord['BATCHID']));
        return $_rs;
    }

    function VerwerkSignaal($_allesRecord) {
        $_sql = "UPDATE BRON.imp_signaal_499 SET VERWERKT_YN = :ONOFF WHERE ROWID = CHARTOROWID(:THEROWID)";
        $_rowid = $this->database->customUrlDecode(urldecode($_allesRecord['SIGNAAL_ROWID']));
        $_rs = $this->userdatabase->Execute($_sql, array('ONOFF'=>$_allesRecord['SIGNAAL_ONOFF'],'THEROWID'=>$_rowid));
        return $_rs;
    }

    function GetBatchnummers(&$gekozenBatchNummer) {
        $_sql = 'SELECT s.*, BATCHNUMMER_AANLEVERBESTAND AS BATCHID
        , (select count(*) from bron.imp_signaal_499 sig where s.BATCHNUMMER_AANLEVERBESTAND = sig.BATCHNUMMER_AANLEVERBESTAND
        AND sig.VERWERKT_YN <> \'Y\') AS NIET_VERWERKT
        FROM bron.imp_batchgegevens_402 s
        WHERE BRONBATCHNUMMER IN (SELECT MAX(BRONBATCHNUMMER) FROM bron.imp_batchgegevens_402 s2 WHERE s2.BATCHNUMMER_AANLEVERBESTAND = s.BATCHNUMMER_AANLEVERBESTAND )
        ORDER BY BATCHNUMMER_AANLEVERBESTAND DESC';
        $_rs = $this->userdatabase->GetAll($_sql);

        if ($_POST['batchnummer_select']) {
            $gekozenBatchNummer = $_POST['batchnummer_select'];
        } else {
            $gekozenBatchNummer = $_rs[0]['BATCHNUMMER_AANLEVERBESTAND'];
        }
        return $_rs;
    }

    function GetSignalen($batchid) {
        $_sql = '
        SELECT s.bronbatchnummer, s.batchnummer_aanleverbestand, s.meldingnummer, s.recordsoort_terugkoppel
        , s.recordidentificatie, s.signaalcode, s.omschrijvingsignaal, s.ernst
        , sig.oplossing, s.RELATIENR, NVL(s.INSCHRIJFNR, -1) AS INSCHRIJFNR
        , ROWIDTOCHAR(r.ROWID) AS RELATIE_ROWID
        , ROWIDTOCHAR(i.ROWID) AS INSCHRIJVING_ROWID
        , s.VERWERKT_YN
        , ROWIDTOCHAR(s.ROWID) AS SIGNAAL_ROWID
        , CASE WHEN NVL(r.BRON_WIJZIGINGNAEXPORT, \'N\') = \'J\' OR NVL(i.BRON_WIJZIGINGNAEXPORT, \'N\') = \'J\'
        THEN \'J\' ELSE \'N\' END AS REL_WIJZIGINGNAEXPORT
        FROM bron.imp_signaal_499 s
        , bron.signaalcode sig
        , nha.relatie r
        , nha.inschrijving i
        WHERE s.signaalcode = sig.signaalcode (+)
        AND s.batchnummer_aanleverbestand = :batchid
        AND s.INSCHRIJFNR = i.INSCHRIJFNR (+)
        AND s.RELATIENR =  r.RELATIENR (+)
        UNION
        SELECT s.bronbatchnummer, s.batchnummer_aanleverbestand, s.meldingnummer, s.recordsoort_terugkoppel
        , s.recordidentificatie, s.signaalcode, s.omschrijvingsignaal, s.ernst
        , sig.oplossing, s.RELATIENR, -1
        , ROWIDTOCHAR(r.ROWID) AS RELATIE_ROWID, null
        , s.VERWERKT_YN
        , ROWIDTOCHAR(s.ROWID) AS SIGNAAL_ROWID
        , NVL(r.BRON_WIJZIGINGNAEXPORT, \'N\')
        FROM bron.imp_signaal_499 s
        , BRON.imp_melding_405 m
        , bron.signaalcode sig
        , nha.relatie r
        WHERE
        (s.recordidentificatie = \'nvt\' or s.recordidentificatie = \'0\') AND
        s.signaalcode = sig.signaalcode (+)
        AND m.batchnummer_aanleverbestand = s.batchnummer_aanleverbestand
        AND m.bronbatchnummer = s.bronbatchnummer
        AND m.meldingnummer = s.meldingnummer
        AND m.CURSISTNR = r.RELATIENR (+)
        AND s.batchnummer_aanleverbestand = :batchid
        ORDER BY ernst, meldingnummer, recordsoort_terugkoppel
        ';

        $_rs = $this->userdatabase->GetAll($_sql , array('batchid' => $batchid));
        foreach($_rs as $_index => $_rec) {
            $_rs[$_index]['RELATIE_ROWID'] = $this->database->customUrlEncode($_rec['RELATIE_ROWID']);
            $_rs[$_index]['INSCHRIJVING_ROWID'] = $this->database->customUrlEncode($_rec['INSCHRIJVING_ROWID']);
            $_rs[$_index]['SIGNAAL_ROWID'] = $this->database->customUrlEncode($_rec['SIGNAAL_ROWID']);
        }
        return $_rs;
    }

    function VoerQueryUit($_allesRecord) {
        switch ($_allesRecord['query']) {
            case 'elem_0':
                // Aantal naar BRON gestuurd
                $_sql = "SELECT COUNT(*) FROM NHA.INSCHRIJVING WHERE BRON_EXPORTDATUMTIJD IS NOT NULL";
                break;
            case 'elem_1':
                // Aantal goedgekeurd
                $_sql = "SELECT COUNT(*) FROM NHA.INSCHRIJVING WHERE BRON_EXPORTDATUMTIJD IS NOT NULL AND BRON_INITGEACCEPTEERD = 'J'";
                break;
            case 'elem_2':
                // Aantal afgekeurd
                $_sql = "select count(*) from NHA.inschrijving i where i.BRON_EXPORTDATUMTIJD is not null and i.BRON_INITGEACCEPTEERD = 'N'";
                break;
            case 'elem_3':
                // Aantal nog te keuren
                $_sql = "select count(*) from NHA.inschrijving i where i.BRON_EXPORTDATUMTIJD is not null and i.BRON_INITGEACCEPTEERD IS NULL";
                break;
            case 'elem_4':
                // Nog te sturen (eerste keer)
                $_sql = "SELECT COUNT(*) FROM NHA.INSCHRIJVING_BRON i WHERE i.BRON_EXPORTDATUMTIJD is NULL";
                break;
            case 'elem_5':
                // - Met BSN
                $_sql = "SELECT COUNT(*) FROM NHA.INSCHRIJVING_BRON i WHERE i.BRON_EXPORTDATUMTIJD is NULL AND i.BSN IS NOT NULL
                AND bron_batchnummer is null
                and datumdiplomaverstuurd is null
                    and leerweg is not null
                    AND ((i.LEERBEDRIJF IS NULL AND i.AANTALURENBPV IS NULL AND i.BINNENKOMSTPOK IS NULL AND i.STARTSTAGE IS NULL AND i.EINDESTAGE IS NULL)
                    OR (i.LEERBEDRIJF IS NOT NULL AND i.AANTALURENBPV IS NOT NULL AND BINNENKOMSTPOK IS NOT NULL AND i.STARTSTAGE IS NOT NULL AND i.EINDESTAGE IS NOT NULL))
                AND UITSCHRIJFDATUM IS NULL
                AND AANMANINGSCODE < '5'
                AND OPZEGGINGSDATUM IS NULL
                AND DATUMGERETOURNEERD IS NULL
                ";
                break;
            case 'elem_6':
                // - Zonder BSN
                $_sql = "SELECT COUNT(*) FROM NHA.INSCHRIJVING_BRON i WHERE i.BRON_EXPORTDATUMTIJD is NULL AND i.BSN IS NULL
                and datumdiplomaverstuurd is null
                AND UITSCHRIJFDATUM IS NULL
                AND AANMANINGSCODE < '5'
                AND OPZEGGINGSDATUM IS NULL
                AND DATUMGERETOURNEERD IS NULL
                    and leerweg is not null
                    AND ((i.LEERBEDRIJF IS NULL AND i.AANTALURENBPV IS NULL AND i.BINNENKOMSTPOK IS NULL AND i.STARTSTAGE IS NULL AND i.EINDESTAGE IS NULL)
                    OR (i.LEERBEDRIJF IS NOT NULL AND i.AANTALURENBPV IS NOT NULL AND BINNENKOMSTPOK IS NOT NULL AND i.STARTSTAGE IS NOT NULL AND i.EINDESTAGE IS NOT NULL))
                ";
                break;
            case 'elem_7':
                // Nog te sturen (wijzigingen)
                $_sql = "SELECT COUNT(*) FROM NHA.INSCHRIJVING_BRON i WHERE i.BRON_EXPORTDATUMTIJD is NOT NULL AND BRON_WIJZIGINGNAEXPORT = 'J'";
                break;
            case 'elem_8':
                // Inschrijvingen zonder onderwijsovereenkomst
                $_sql = "SELECT COUNT(*) FROM NHA.INSCHRIJVING_BRON_BASIS i
                WHERE i.BINNENKOMSTOOVK IS NULL
                AND i.LEERWEG IS NOT NULL
                AND (
                  -- Nog niet geexporteerde inschrijvingen, na 01-SEP-2010, die niet geretoureerd/opgezegd zijn
                  (i.startdatum >= '01-09-2010' and i.BRON_EXPORTDATUMTIJD IS NULL AND DATUMGERETOURNEERD IS NULL AND DATUMDIPLOMAVERSTUURD IS NULL AND OPZEGGINGSDATUM IS NULL AND UITSCHRIJFDATUM IS NULL AND AANMANINGSCODE < '5')
                  OR
                  -- Reeds geexporteerde inschrijvingen, na 01-OKT-2007, die ondertussen zijn gewijzigd
                  (i.startdatum > '01-10-2007' AND i.BRON_EXPORTDATUMTIJD IS NOT NULL AND (i.BRON_WIJZIGINGNAEXPORT = 'J' OR BRON_WIJZIGINGNAEXPORT_REL = 'J'))
                )
                ";
                break;
        }
        return array('count' => $this->form->database->userdb->CacheGetOne($_sql));
    }

    function VerwerkGBAGegevens($_allesRecord) {
        $_sets = FALSE;
        $_select = FALSE;
        $_cols = StripHiddenFields($_allesRecord);
        if (is_array($_cols) > 0) {
            foreach($_cols as $_col => $_val) {
                $_select[] = $_col;             // A, B, C, ...
                $_sets[] = $_col." = :".$_col;  // A = A:, B = :B, C = :C, ...
                $_params[$_col] = $_val;        // A => a, B => b, ...
            }
        }
        if (is_array($_sets)) {
            $_params['RELATIENR'] = $_allesRecord['_hdnRelatieNr'];
            $_setStatement = implode(', ', $_sets);
            $_sql = "UPDATE NHA.RELATIE SET $_setStatement WHERE RELATIENR = :RELATIENR";
            $_rs = $this->userdatabase->Execute($_sql, $_params);
            if ($this->userdatabase->Affected_Rows() > 0)
                $_updateBron = TRUE;
        } else {
            $_updateBron = TRUE;
        }

        if ($_updateBron) {
            $_updateBronSQL = "UPDATE BRON.imp_melding_405 SET GBAOVERGEZET = 'J'
            WHERE BATCHNUMMER_AANLEVERBESTAND = :BATCHNUMMER_AANLEVERBESTAND
            AND MELDINGNUMMER = :MELDINGNUMMER";
            $_tmp = $this->userdatabase->Execute($_updateBronSQL, array('BATCHNUMMER_AANLEVERBESTAND'=>$_allesRecord['_hdnBatchNummerAanleverbestand'], 'MELDINGNUMMER'=>$_allesRecord['_hdnMeldingNr']));
        }

        if ($_select !== FALSE) {
            $_selectStatement = implode(', ', $_select);
            $_syncSQL = "SELECT $_selectStatement FROM NHA.RELATIE WHERE RELATIENR = :RELATIENR";
            $_rs = $this->userdatabase->GetAll($_syncSQL, array('RELATIENR' => $_allesRecord['_hdnRelatieNr']));
            $_rs = $_rs[0]; // neem de eerste rij, er is er toch maar een.
        }
        $_rs['_hdnCurrentIndex'] = $_allesRecord['_hdnCurrentIndex'];
        $_rs['_hdnBatchNummerAanleverbestand'] = $_allesRecord['_hdnBatchNummerAanleverbestand'];
        $_rs['_hdnMeldingNr'] = $_allesRecord['_hdnMeldingNr'];

        return $_rs;
    }

    function NogmaalsDoorsturen($_allesRecord) {
        $_sql = "UPDATE BRON.imp_signaal_499 SET VERWERKT_YN = 'Y' WHERE ROWID = CHARTOROWID(:THEROWID) RETURNING RELATIENR INTO :RELATIENR";
        $_stmt = $this->userdatabase->Prepare($_sql);
        $_rowid = $this->database->customUrlDecode(urldecode($_allesRecord['SIGNAAL_ROWID']));
        $this->userdatabase->InParameter($_stmt, $_rowid, 'THEROWID');
        $this->userdatabase->OutParameter($_stmt, $_relatienr, 'RELATIENR');
        $_rs = $this->userdatabase->Execute($_stmt);

        $_sql = "UPDATE NHA.RELATIE SET BRON_WIJZIGINGNAEXPORT = 'J' WHERE RELATIENR = :RELATIENR";
        $_rs = $this->userdatabase->Execute($_sql, array('RELATIENR'=>$_relatienr));

        return true;
    }

    function DisplayBronSignalen($moduleid, $params) {
        if ($this->outputformat == 'xhtml') {
            $this->smarty->assign("batchnummers", $this->GetBatchnummers($_huidigeBatchnummer));
            $this->smarty->assign("current_batchnummer", $_huidigeBatchnummer);
            $this->smarty->assign("signalen", $this->GetSignalen($_huidigeBatchnummer));
            $this->smarty->display("bronsignalen.tpl.php");
        }
    }

    function DisplayBronOverzicht($moduleid, $params) {
        if ($this->outputformat == 'xhtml') {
            $this->smarty->display("bronoverzicht.tpl.php");
        }
    }

    function GetGbaGegevens() {
        $_sql = "
            select ROWIDTOCHAR(r.ROWID) AS ROW_ID, r.RELATIENR as REL_RELATIENR, g.DATUMINGANGADRES, r.VOORNAAM AS REL_VOORNAAM, r.voornamen AS REL_VOORNAMEN, r.voorletters AS REL_VOORLETTERS, r.TUSSENVOEGSEL AS REL_TUSSENVOEGSEL
            , r.ACHTERNAAM REL_ACHTERNAAM, r.STRAAT REL_STRAAT, r.HUISNR REL_HUISNR, r.HUISNUMMER REL_HUISNUMMER, r.HUISNUMMERTOEVOEGING REL_HUISNUMMERTOEVOEGING
            , r.BSN REL_BSN, r.ONDERWIJSNR REL_ONDERWIJSNUMMER, r.PLAATS REL_PLAATS, r.POSTCODE REL_POSTCODE, r.GEBDATUM REL_GEBDATUM, r.GEBPLAATS REL_GEBPLAATS
            , m.meldingnummer, m.BATCHNUMMER_AANLEVERBESTAND, m.BSN GBA_BSN, m.ONDERWIJSNUMMER GBA_ONDERWIJSNUMMER
            , g.POSTCODE_GBA AS GBA_POSTCODE, g.NATIONALITEIT1 AS GBA_NATIONALITEIT1, g.NATIONALITEIT2 GBA_NATIONALITEIT2
            , p.VOORNAMEN GBA_VOORNAMEN, p.VOORVOEGSEL GBA_TUSSENVOEGSEL, p.ACHTERNAAM GBA_ACHTERNAAM, p.STRAATNAAM GBA_STRAAT
            , p.HUISNUMMER GBA_HUISNUMMER, p.HUISNUMMER_TOEV AS GBA_HUISNUMMERTOEVOEGING, m.GEBOORTEDATUM GBA_GEBDATUM
            , INITCAP(LOWER(p.PLAATSNAAM)) GBA_PLAATS
            , REPLACE(TRIM(REPLACE(TRANSLATE(REPLACE(p.VOORNAMEN, '-', '.'),'éëèøöíïìüabcdefghijklmnopqrstuvwxyz',' '), '  ', ' ')), ' ', '.') || '.' GBA_VOORLETTERS
            , NVL(GBAOVERGEZET, 'N') GBAOVERGEZET
            from nha.relatie r, bron.imp_melding_405 m, bron.imp_gbagegevens_411 g, bron.imp_persoon_410 p
            where r.relatienr = m.cursistnr
            and NVL(m.GBAOVERGEZET, 'N') = 'N'
            and m.BATCHNUMMER_AANLEVERBESTAND = g.BATCHNUMMER_AANLEVERBESTAND
            and m.MELDINGNUMMER = g.MELDINGNUMMER
            and m.BATCHNUMMER_AANLEVERBESTAND = p.BATCHNUMMER_AANLEVERBESTAND
            and m.MELDINGNUMMER = p.MELDINGNUMMER
        ";
        $_rs = $this->userdatabase->Execute($_sql);
        $_records = $_rs->GetAll();
        $_resultArray = Array('result'=>$_records, 'query'=>$_SERVER['REQUEST_URI'], 'error'=>false);
        return json_encode($_resultArray);
    }

    function DisplayGBAOverzicht($moduleid, $params) {
        if ($this->outputformat == 'xhtml') {
            $this->smarty->display("gbagegevens.tpl.php");
        }
    }

    function Show($outputformat='xhtml', $rec=false) {
        global $_GVARS;
        global $polaris;

        parent::Show($outputformat, $rec);

        if (isset($_GET['action'])) {
            /**
            * Show the Polaris generated forms when the user wants to insert or edit an item
            */
            switch($_GET['action']) {
            case 'edit':
            case 'insert':
                $this->form->ShowFormView($this->state, $this->permission, $detail=false, $masterrecord=false);
                $this->form->ShowButtons($this->state, $this->permission);
//                $this->form->ShowNavigationLinks($this->state, $this->permission, $detail=false);
            break;
            case 'searchext':
                $this->form->ShowSearchForm($this->state, $this->permission, $detail=false, $masterrecord=false);
                $this->form->ShowButtons($this->state, $this->permission);
//                $this->form->ShowNavigationLinks($this->state, $this->permission, $detail=false);
            break;
            }
        } else {
            /**
            * Show the custommade forms when user views a list of items
            */
            switch ($this->moduleid) {
            case 1:
                /***
                * Terugkoppel bestanden verwerken
                */
                if ($_GET['func'] != '') {
                    if ($_GET['func'] == 'exportrecords') {
                        echo $this->GetExportRecords($_GET['exportset']);
                    }
                    if ($_GET['func'] == 'exportfields') {
                        echo $this->GetExportFields($_GET['exportset'], $_GET['recordid']);
                    }

                } else {
                    $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/sharedlib.js';
                    $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/bron.js';
                    $this->DisplayBronSignalen($this->moduleid, $_GET);
                }
            break;
            case 2:
                /***
                * Overzicht
                */
                if ($_GET['func'] != '') {
                    if ($_GET['func'] == 'exportrecords') {
                        echo $this->GetExportRecords($_GET['exportset']);
                    }
                    if ($_GET['func'] == 'exportfields') {
                        echo $this->GetExportFields($_GET['exportset'], $_GET['recordid']);
                    }

                } else {
                    $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/sharedlib.js';
                    $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/bron.js';
                    $this->DisplayBronOverzicht($this->moduleid, $_GET);
                }
            break;
            case 3:
                /***
                * GBA gegevens overzetten
                */
                if ($_GET['func'] != '') {
                    if ($_GET['func'] == 'gbagegevens') {
                        echo $this->GetGbaGegevens();
                    }
                } else {
                    $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/sharedlib.js';
                    $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/brongba.js';
                    $this->DisplayGBAOverzicht($this->moduleid, $_GET);
                }
            break;
            }
        }
    }

}