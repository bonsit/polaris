{include file="shared.tpl.php"}
<div id="formview" class="formview frmImporteerPostcodeBestand">
<input type="hidden" name="_hdnFORMTYPE" value="postcodeimporteren" />
<input type="hidden" name="_hdnProcessedByModule" value="true" />

<h3>Importeren van Nederlandse postcodes</h3>
{include file="table.tpl.php" import=$import_nl landcode=nl}


<h3>Importeren van Belgische postcodes</h3>
{include file="table.tpl.php" import=$import_be landcode=be}
<br /><br />
<div class="drop-shadow curved curved-hz-2 buttons small" style="float:right;text-align:center">Gebruik met beleid!<br /><br /> <button id="btnVerwerkNL" class="secondary">Verwerk NL nu!</button><button id="btnVerwerkBE" class="secondary">Verwerk BE nu!</button></div>
<br />
<h1>Huidige situatie postcodetabellen</h1>
<p>Aantal Nederlandse postcodes: {$nl_count}</p>
<p>Aantal Belgische postcodes: {$be_count}</p>

</div /* frmNieuweAanvraag */>