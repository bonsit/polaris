<link rel="stylesheet" type="text/css" media='all' href="{$serverpath}/min/f={$modulepath}/css/nhalogging.css&{#BuildVersion#}" />

<div class="buttons toggleErnst small">
    Ernst:
    <input type="checkbox" id="btnTrace" value="1" /><label for="btnTrace"> Trace &nbsp</label>
    <input type="checkbox" id="btnDebug" value="2" /><label for="btnDebug"> Debug &nbsp;</label>
    <input type="checkbox" id="btnInfo" value="4" /><label for="btnInfo"> Info &nbsp;</label>
    <input type="checkbox" id="btnWarn" value="8" /><label for="btnWarn"> Warn &nbsp;</label>
    <input type="checkbox" id="btnError" value="16" /><label for="btnError"> Error &nbsp;</label>
    <input type="checkbox" id="btnFatal" value="32" /><label for="btnFatal"> Fatal &nbsp;</label>
    <input type="checkbox" id="btnAllesErnst" value="63" /><label for="btnAllesErnst"> Alles &nbsp;</label>
    &nbsp; <button type="button" id="ernstUnset">Reset</button>
    Context: <select id="btnContext"><option value="">&lt;Alles&gt;</option>{html_options options=$contextselect}</select>
</div>

<div class="buttons toggleTijd small">
    Periode:
    <input type="radio" id="btn24uur" value="2" name="tijd" /><label for="btn24uur"> 24 uur &nbsp;</label>
    <input type="radio" id="btnVandaag" value="1" name="tijd" /><label for="btnVandaag"> Vandaag &nbsp;</label>
    <input type="radio" id="btn30Dagen" value="16" name="tijd" /><label for="btn30Dagen"> 30 dagen &nbsp;</label>
    <input type="radio" id="btnAlles" value="4" name="tijd" /><label for="btnAlles"> Alles &nbsp;</label>
    <input type="radio" id="btnDatum" value="8" name="tijd" /><label for="btnDatum"> Datum: <input type="date" id="fldDatum" class="validation_date" /></label>
    &nbsp; <button type="button" id="tijdUnset">Reset</button>
</div>

<div class="buttons selectContext small">

</div>

<div class="bodyscroll scrollarea" style="max-height: 154px; ">

<table id="loglist" class="data striped">
    <thead>
        <tr>
            <th>Ernst</th>
            <th style="width:130px">Datum</th>
            <th>Melding</th>
            <th>Context</th>
            <th>DB-melding</th>
        </tr>
    </thead>
    <tbody class="bodyscroll"></tbody>
</table>

</div>
