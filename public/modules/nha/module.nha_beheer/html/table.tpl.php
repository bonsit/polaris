<div class="bodyscroll scrollarea" style="width:auto;height:150px;">
<table style="width:100%;" class="data striped group-table right">
<thead>
<tr>
    <th>Bestand</th>
    <th>Uploaddatum</th>
    <th>Grootte</th>
    <th>Status</th>
    <th>Actie</th>
</tr>
</thead>
<tbody>
{section name=i loop=$import}
<tr style="{if $import[i].VERWERKT == 'N'}color:#206ED3{elseif $import[i].VERWERKT == 'O'}color:#777{else}color:green{/if}">
    <td style="width:180px;"><span title="{$import[i].BESTANDSNAAM}">{$import[i].BESTANDSNAAM|truncate:40:"..."}</span></td>
    <td>{$import[i].DATUM_FORMAT}</td>
    <td>{$import[i].GROOTTE|formatBytes} / {$import[i].AANTALRECORDS} postcodes</td>
    <td>{if $import[i].VERWERKT == 'N'}Wordt verwerkt
        {elseif $import[i].VERWERKT == 'O'}Overgeslagen
        {elseif $import[i].VERWERKT == 'J'}OK! ({$import[i].AANTALRECORDSVERWERKT} records verwerkt)
        {else}{$import_nl[i].STATUS}
        {/if}</td>
    <td>{if $import[i].VERWERKT == 'N'}<button id="butDeleteImport{$landcode}" class="deletePostcode">Verwijder</button>{/if}</td>
</tr>
{sectionelse}
    <td colspan="5">Geen bestanden gevonden.</td>
{/section}
</tbody>
</table>
</div>

{if $landcode == 'nl'}
<p>Nederlandse postcode bestanden kan de sysadmin plaatsen op de productie Oracle server.<br />
Folder: e:\oracle\admin\nhaprod\postcodetabel<br />
Bestandsnaam: pctr.txt</p>
{else}
<input type="file" name="{$landcode}_bestand" />
<input type="submit" value="Uploaden" />
{/if}
