function htmlEntities(str) {
    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}

var NHALogging = {};

NHALogging = {
    exportResults: [],

    initialize: function() {
        var Self = NHALogging;

        $("#ernstUnset").click(function() {
            $(".toggleErnst input").removeAttr('checked');
            Self.fetchLogging();
        });
        $("#btnAllesErnst").click(function() {
            if (!this.checked)
                this.checked = true;
            else
                $(".toggleErnst input:not(#btnAllesErnst)").removeAttr('checked');
            Self.fetchLogging();
        });

        $("#tijdUnset").click(function() {
            $(".toggleTijd input").removeAttr('checked');
        });

        $(".toggleErnst input:not(#btnAllesErnst)").click(function() {
            $("#btnAllesErnst").removeAttr('checked');
            Self.fetchLogging();
        });

        $(".toggleTijd input:not(#btnDatum)").change(function() {
            Self.fetchLogging();
        });

        $("#btnDatum").change(function() {
            if ($("#fldDatum").val() !== '') {
                Self.fetchLogging();
            }
        });

        $("#btnContext").change(function() {
            Self.fetchLogging();
        });

        $($("#btnTrace")[0].form).submit(function(e) {
            e.preventDefault();
        })

        $("#btnAllesErnst").attr('checked', 'checked');
        $("#btn24uur").attr('checked', 'checked');
        Self.fetchLogging();
    },
    fetchLogging: function(e) {
        var Self = NHALogging;

//        e.preventDefault();

        var senddata = [];

        var totalErnst = 0;
        $(".toggleErnst input:checked").each(function(i, elem) {
            totalErnst += parseInt($(elem).val());
        });
        log(totalErnst);

        var totalTijd = 0;
        $(".toggleTijd input:checked").each(function(i, elem) {
            totalTijd += parseInt($(elem).val());
        });

//        senddata.push({name: 'TIJD', value: $(this).parents("tr").find("input.recordid").val()});
        senddata.push({name: 'func', value: 'nhalogging'});
        senddata.push({name: 'ernst', value: totalErnst});
        senddata.push({name: 'tijd', value: totalTijd});
        senddata.push({name: 'datum', value: $("#fldDatum").val()});
        var context = $("#btnContext option:selected").val();
        senddata.push({name: 'context', value: context});
        if (context !== '') {
            $("#formtaglist li").removeClass("selected").first().addClass("selected");
        }
        var tag = (context == '') ? Polaris.Base.getLocationVariable('tag') : '';
        senddata.push({name: 'tag', value: tag});

        $('#loglist tbody').html("Bezig met ophalen van data...");
        jQuery.getJSON(_servicequery, senddata, function(data, textStatus) {
//            $(".toggleTijd input").prop('disabled', false);
            $('#loglist tbody').empty();
            if (textStatus == 'success') {
                log('Logging opgehaald');
                log(data.query);
                if (data.error === false) {
                    Self.displayLogging(data.result, 'masters');
                    Self.displayLogging(data.result, 'details');
                    Polaris.Dataview.enableTooltips($("#loglist"));

                    $("tr.togglerow").click( function() {
                        $("tr.MASTER"+this.id).fadeToggle();
                    });
                } else {
                    Polaris.Base.errorDialog(data.error, data.detailed_error);
                }
            } else {
                Macs.SharedLib.ajaxFeedback('Fout opgetreden bij JSON post.');
            }
        });
    },
    voegLogItemToe: function(item, type) {
        var idstr = '';
        var classstr = '';
        if (item.MASTERID != null) {
            classstr = 'class="closed detail MASTERLOG_'+item.MASTERID+'"';
        }
        if (item.LOGID != null) {
            idstr='id="LOG_'+item.LOGID+'" ';
        }
        var tr = $('<tr '+idstr+classstr+'></tr>');
        $('<td class="first '+item.ERNST+'">' + item.ERNST + '</td>').appendTo(tr);
        $('<td>' + item.DATUM + '</td>').appendTo(tr);
        $('<td>' + item.MELDING + '</td>').appendTo(tr);
        $('<td>' + item.CONTEXT + '</td>').appendTo(tr);
        if (item.ORA_ERROR != null) {
            $('<td class="hoverpop"><input type="hidden" value="'+htmlEntities(item.ORA_ERROR)+'"><i class="fa fa-info-circle"></i></td>').appendTo(tr);
        } else {
            $('<td></td>').appendTo(tr);
        }
        if (item.MASTERID == null && type == 'masters') {
            tr.appendTo('#loglist tbody');
        } else if (type != 'masters') {
            masterTr = $('tr#LOG_' + item.MASTERID);
            masterTr.addClass('togglerow');
            tr.insertAfter(masterTr);
        }
    },
    displayLogging: function(data, type) {
        if (data.length == 0) {
            $('#loglist tbody').html("Geen items gevonden.");
        } else {
            $.each(data, function(key, val) {
                if (type == 'masters') {
                    NHALogging.voegLogItemToe(val, type);
                } else {
                    NHALogging.voegLogItemToe(val, type);
                }
            });
        }
    }
};

$(document).ready(function(){
    var Self = NHALogging;
    Self.initialize();

});
