$(document).ready(function(){
    $("#btnVerwerkNL,#btnVerwerkBE,.deletePostcode").click(function(e) {
        e.preventDefault();
        if (confirm("Weet u het zeker?")) {
            $("input[name=_hdnAction]").val(this.id);
            $("#dataform").submit();
        }
    });
});
