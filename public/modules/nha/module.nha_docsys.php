<?php
require_once('_shared/module._base.php');
use Geshi\Geshi;

function _transformFlatArrays(&$values, $key, $sep=',') {
    if (substr($values, 0, 6) == 'ARRAY:') {
        $flatArray = substr($values, 6);
        $values = explode($sep,$flatArray);
        foreach($values as $k => $val) {
            if ($key == 'PARAMETERS' or $key == 'USES' or $key == 'USEDBY' or $key == 'UIELEMENTS') {
                /* don't explode 'PACKAGE BODY', eventhough it contains a space*/
                $val = str_replace('PACKAGE BODY','PACKAGE_BODY',$val);

                $values[$k] = explode(' ',$val);
            }
        }
    }
}

class Module extends _BaseModule {
    var $domains;
    var $domainid;

    var $_standardCommentIds = array("config" => "DOCSYS.CONFIG");

    var $transforms = '608, 1007, 1006, 1565, 1548, 558, 607, 565, 829, 825, 1314, 568, 1016, 1600, 1610, 422, 5401, 5400';
    var $translationData = array(
        'plr_action' => array(
            'table' => 'plr_action_l10n',
            'label' => 'Acties',
            'keycolumns' => array('CLIENTID', 'FORMID', 'ACTIONID'),
            'transcolumns' => array('ACTIONNAME'),
            'filter' => 'FORMID IN (__TRANSFORMS__)',
        ),
        'plr_construct' => array(
            'table' => 'plr_construct_l10n',
            'label' => 'Menuitems',
            'keycolumns' => array('CLIENTID', 'APPLICATIONID', 'CONSTRUCTID'),
            'transcolumns' => array('CONSTRUCTNAME'),
            'filter' => 'MASTERFORMID IN (__TRANSFORMS__)
            OR DETAILFORMID IN (__TRANSFORMS__)',
        ),
        'plr_form' => array(
            'table' => 'plr_form_l10n',
            'label' => 'Formuliernamen',
            'keycolumns' => array('CLIENTID', 'FORMID'),
            'transcolumns' => array('FORMNAME'),
            'filter' => 'FORMID IN (__TRANSFORMS__)',
        ),
        'plr_form_items' => array(
            'table' => 'plr_form_l10n',
            'label' => 'Formulier-items',
            'keycolumns' => array('CLIENTID', 'FORMID'),
            'transcolumns' => array('ITEMNAME'),
            'filter' => 'FORMID IN (__TRANSFORMS__) AND ITEMNAME IS NOT NULL',
        ),
        'plr_page' => array(
            'table' => 'plr_page_l10n',
            'label' => 'Pagina\'s',
            'keycolumns' => array('CLIENTID', 'FORMID', 'PAGEID'),
            'transcolumns' => array('PAGENAME'),
            'filter' => 'FORMID IN (__TRANSFORMS__)',
        ),
        'plr_line' => array(
            'table' => 'plr_line_l10n',
            'label' => 'Regels',
            'keycolumns' => array('CLIENTID', 'FORMID', 'PAGEID', 'LINEID'),
            'transcolumns' => array('LINEDESCRIPTION'),
            'filter' => 'FORMID IN (__TRANSFORMS__) AND (LINEDESCRIPTION IS NOT NULL OR LINEDESCRIPTION <>\'\')',
        )
    );

    function Module($moduleid, $module) {
        global $_GVARS;
        global $polaris;

        parent::_BaseModule($moduleid, $module, 'nha');
        require_once($this->modulefolder.'/php/modifier.hyperlinks.php');
        $this->smarty->register_modifier('hyperlinks', 'smarty_modifier_hyperlinks');

        $this->processed = false;

        /**
        * What client are we talking about? (nog doen: eventueel selecteerbaar maken)
        */
        $this->clientid = $_SESSION['clientid'];

        /**
        * Store the domainid as a session var so it can be used by other constructs
        */
        if (isset($_GET['domain']))
            $this->domainid = $_GET['domain'];
        elseif (isset($_SESSION['domainid']))
            $this->domainid = $_SESSION['domainid'];
        else
            $this->domainid = 2; // standaard kijken we naar domain 2 (nieuwe Macs)
        $_SESSION['domainid'] = $this->domainid;
    }

    function Process() {
        global $_GVARS;
        global $scramble;

        /***
        * Show the available domains
        */
        $_sqlDomains = "
        SELECT ttt.CLIENTID , ttt.DOMAINID , ttt.DOMAINNAME
        , MD5(CONCAT('{$scramble}','_', ttt.CLIENTID,'_', ttt.DOMAINID)) AS "._RECORDID."
        FROM mod_domain ttt
        WHERE ttt.CLIENTID = $this->clientid";

        if ($this->form)
            $this->domains = $this->form->database->dbGetAll($_sqlDomains);

        /**
        * Process the POST/AJAX events
        */
        if (isset($_POST['method'])) {
            ob_start();
            $_id = $_POST['recordid'];
//            var_dump(date('r'),$_POST);
            switch(strtolower($_POST['method'])) {
                /**
                * process the editor methods
                */
                case 'savelink':
                    $this->form->dbExecute("UPDATE mod_ui_element SET `top`=?, `left`=?, `width`=?, `height`=? WHERE MD5(CONCAT(`clientid`,'_',`domainid`,'_',`uiid`,'_',`id`)) = ?",
                    array($_POST['top'], $_POST['left'], $_POST['width'], $_POST['height'], $_id) );
                    $_result = $_POST['uiid'].'#'.$_id;
                break;
                case 'createlink':
                    $this->form->dbExecute("INSERT INTO mod_ui_element (`clientid`,`domainid`,`uiid`, `top`, `left`, `width`, `height`) VALUES (?,?,?,?,?,?,?)",
                    array( $this->clientid, $this->domainid, $_POST['uiid'], $_POST['top'], $_POST['left'], $_POST['width'], $_POST['height']) );
                    $_result = md5($this->clientid.'_'.$this->domainid.'_'.$_POST['uiid'].'_'.$this->form->dbInsertID());
                break;
                case 'deletelink':
                    $this->form->dbExecute("DELETE FROM mod_ui_element WHERE MD5(CONCAT(`clientid`,'_',`domainid`,'_',`uiid`,'_',`id`)) = ?", array( $_id) );
                break;
                case 'saveelement':
                    $this->form->dbExecute("UPDATE mod_ui_element SET ACTIONTYPE = ?, ACTION = ?, NAME = ?, LINKEDOBJECT = ? WHERE MD5(CONCAT(`clientid`,'_',`domainid`,'_',`uiid`,'_',`id`)) = ?",
                    array($_POST['actiontype'], $_POST['action'], $_POST['linkname'], $_POST['linkedobject'], $_id), true );
                    $_result = $_id;
                break;
                case 'importtables':
                    var_dump(date('r'),$_POST);
                    $databasehash = $_POST['RECORDID'];

                    $database = new plrDatabase( $this->form->polaris() );
//                    $tables = $_POST['TABLENAME'];
                    $_overwritefacttypes = $_POST['overwritefacttypes'] == 'Y';
                    $database->LoadRecordHashed($databasehash);
                    $sp_results = $database->ImportFactTypes($this->clientid, $this->domainid, $tables, $_overwritefacttypes);

                    $_result = true;
                break;
                case 'adddomain':
                    $this->form->dbExecute("INSERT INTO mod_domain (CLIENTID, DOMAINNAME) VALUES (?, ?)",
                        array($this->clientid, $_POST['domainname']));
                    $_result = $_POST['domainname'];
                break;
                case 'savetranslation':
                    if ($_POST['_hdnAction'] == 'save') {
                        $_context = $this->translationData[$_POST['context']];
                        $_tablename = $_context['table'];
                        $_column = $_context['transcolumns'][0];
                        $_keycolumn = implode(' = ? AND ', $_context['keycolumns']);
                        $_keycolumn .= ' = ?';
                        $_sql = "UPDATE $_tablename SET $_column = ? WHERE $_keycolumn";
                        foreach($_POST['id'] as $_v) {
                            $_value = 'NL:'.$_POST[$_v.'_NL'].'|DE:'.$_POST[$_v.'_DE'];
                            $_params = explode('_', $_v);
                            $_params = array_merge((array)$_value, $_params);
                            $this->form->dbExecute($_sql, $_params);
                        }
                        $this->transfeedback = 'saved';
                    }
                break;
                default:
                    var_dump(date('r'),$_POST);
                break;
            }
//            $_content = ob_get_contents();
//            ob_end_clean();
//            file_put_contents('log.txt', $_content);

            echo "{$_result}";
//            exit();
        }

        /**
        * Process the GET/AJAX events
        */
        if (isset($_GET['method'])) {
            switch(strtolower($_GET['method'])) {
            case 'facttypestruct':
                require_once('classes/base/plr_modeler.class.php');
                $_modeler = new plrModeler($this->form->polaris());
                $_domainhash = encodeString($this->clientid.'_'.$this->domainid);
                $_currentfacttype = $_GET['facttype'];
                $_modeler->getSchema($_domainhash);
                /**
                * Get the concepts to integrate with the sentencepattern (for easy reading)
                */
                $_sqlConcepts = "SELECT CONCEPT, DEFINITION FROM mod_semantic_concept WHERE clientid = ? AND domainid = ? AND COMMONKNOWLEGDE = 'N'";
                $_concepts = $this->form->database->dbGetAll($_sqlConcepts, array($this->clientid, $this->domainid));
                /* Remove carriage returns */
                foreach($_concepts as $k => $_concept) {
                    $_concepts[$k] = str_replace("\r\n"," ",$_concept);
                }

                /**
                * Get the fact type data
                */
                $_facttype = $_modeler->getFactType($_domainhash, $_currentfacttype, $_concepts);
                foreach($_facttype['columns'] as $k => $_column) {
                    $_facttype['columns'][$k]['PLACEHOLDERLINK'] = $_GVARS['serverroot'].'/app/docsys/const/triggers/?qc[]=SOURCECODE&qv[]=:NEW.'.$_column['PLACEHOLDERNAME'].'&facttype='.$_column['FACTTYPENAME'];
                }
                echo json_encode($_facttype);
                die();
            break;
            }
        }
    }

    function ColorSyntax($sourcecode, $language='plsql') {
        //
        // Create a GeSHi object
        //
        $geshi = new GeSHi($sourcecode, $language);

        $geshi->enable_line_numbers(GESHI_NORMAL_LINE_NUMBERS);

        //
        // And echo the result!
        //
        return $geshi->parse_code();
    }

    function ProcessReferences($description) {
        global $_GVARS;

        /**
        * Process the references to other objects,
        * like <storedproc>....</storedproc>, <function>...</function>, <packproc>...</packproc>
        */
        $pattern = '/(<storedproc>)(.+)(<\/storedproc>)/i';
        $replacement = '<a href="'.$_GVARS['serverroot'].'/app/docsys/const/storedprocs/?itemname=${2}">${2}</a>';
        $description = preg_replace($pattern, $replacement, $description);

        $pattern = '/(<function>)(.+)(<\/function>)/i';
        $replacement = '<a href="'.$_GVARS['serverroot'].'/app/docsys/const/functions/?itemname=${2}">${2}</a>';
        $description = preg_replace($pattern, $replacement, $description);

        $pattern = '/(<packproc>)(.+)\.(.+)(<\/packproc>)/i';
        $replacement = '<a href="'.$_GVARS['serverroot'].'/app/docsys/const/packages/?itemname=${3}&pack=${2}">${3}</a>';
        $description = preg_replace($pattern, $replacement, $description);

        return $description;
    }

    function DisplaySemanticConcepts($clientid, $domainid) {
        if ($this->outputformat == 'xhtml') {
            $rs = $this->form->GetViewContent($detail=false, $masterrecord=false, $limit=1000);
            $_items = $rs->GetAll();
            $this->smarty->assign("items", $_items);
            $this->showControls = true;
            $this->smarty->display("semantic_concepts.tpl.php");
        } else {
            echo $this->form->GetFormattedViewContent($this->outputformat, $detail=false, $masterrecord=false, $limit=1000);
        }
    }

    function GetUI_Processes($clientid, $domainid, $mainprocesses) {
        $_newline = "\r\n";
        foreach($mainprocesses as $_mainproc) {
            $_mainproc = str_replace("\r","",$_mainproc); // verwijder roque 'carriage returns'
            $_parts = explode('.',$_mainproc);
            $_sql = 'SELECT DESCRIPTION FROM mod_method WHERE clientid = ? AND domainid = ? AND methodname = ?';
            if (!isset($_parts[1])) {
                $_mainprocess = $_parts[0];
                $_params = array($clientid, $domainid, $_mainprocess);
            } else {
                $_mainpackage = $_parts[0];
                $_mainprocess = $_parts[1];
                $_sql .= ' AND package = ?';
                $_params = array($clientid, $domainid, $_mainprocess, $_mainpackage);
            }
            $_rsprocs = $this->form->database->userdb->GetOne($_sql, $_params);
            if ($_rsprocs) {
                $_rsprocs = preg_replace("/    (\d+)\)/","  ".$_newline."$0", $_rsprocs);
                $_rsprocs = $this->ProcessReferences($_rsprocs);
                $_processes[] = array('name' => $_mainprocess, 'description' => $_rsprocs);
            }
        }
        return $_processes;
    }

    function GetUI_Elements($clientid, $domainid, $currentui) {
        global $scramble;
        $_newline = "\r\n";
        $_extrawhere = 'MD5(CONCAT("'.$scramble.'","_",e.CLIENTID,"_",e.DOMAINID,"_",UIID)) = "'.$currentui.'"';

        $_sql = 'SELECT UIID, ID, ACTIONTYPE, NAME, ACTION, LINKEDOBJECT, `LEFT`, `TOP`, `WIDTH`, `HEIGHT`
        , MD5(CONCAT(e.CLIENTID,"_",e.DOMAINID,"_",UIID,"_",ID)) AS '._RECORDID.', m.DESCRIPTION, e.DESCRIPTION AS UIDESCRIPTION

        FROM mod_ui_element e

        LEFT JOIN mod_method m on substring(e.linkedobject, locate(\'.\', e.linkedobject)+1) = m.methodname
        AND substring(e.linkedobject, 1, locate(\'.\', e.linkedobject)-1) = m.package
        WHERE '.$_extrawhere;

        $_rselems = $this->form->database->userdb->GetAll($_sql);

        if ($_rselems) {
            foreach($_rselems as $_index => $_rselem) {
                $_rselems[$_index]['DESCRIPTION'] = preg_replace("/    (\d+)\)/","  ".$_newline."$0", $_rselem['DESCRIPTION']);
            }
        }
        return $_rselems;
    }

    function GetUI_Triggers($clientid, $domainid, $facttype) {
        $_sql = 'SELECT METHODNAME, REPLACE(DESCRIPTION,":",":  \n") AS DESCRIPTION, REPLACE(REPLACE(REPLACE(REPLACE(TRIGGERTYPE
        ,"AFTER STATEMENT","Nadat de %s-records zijn")
        ,"BEFORE STATEMENT","Voordat de %s-records worden")
        ,"AFTER EACH ROW","Nadat een %s-record is")
        ,"BEFORE EACH ROW","Voordat een %s-record wordt")
        AS TRIGGERTYPE_TRANS
        , REPLACE(REPLACE(REPLACE(REPLACE(TRIGGEREVENT
        ,"UPDATE", "gewijzigd")
        ,"INSERT", "toevoegd")
        ,"DELETE", "verwijderd")
        ," OR ", " of ")
        AS TRIGGEREVENT_TRANS ,
        SENTENCEPATTERN
        FROM mod_method m, mod_facttype f WHERE m.clientid = f.clientid AND m.domainid = f.domainid
        AND m.clientid = ? AND m.domainid = ? AND methodtype = "trigger" AND triggertable = ? AND facttypename = ? AND DESCRIPTION <> "" ORDER BY 3 DESC';

        if ($facttype != '') {
            $_fts = explode(',',$facttype);
            foreach($_fts as $_ft) {
                $_pos1 = strpos($_ft, '[');
                if ($_pos1 !== false) {
                    $_ftaction = substr($_ft, $_pos1 + 1, -1);
                    $_ft = substr($_ft, 0, $_pos1);
                } else {
                    $_ftaction = '';
                }
                $_rstriggers = $this->form->database->userdb->GetAll($_sql, array($clientid, $domainid, $_ft, $_ft));
                $_result[] = array('facttype' => $_ft, 'sentencepattern' => $_rstriggers[0]['SENTENCEPATTERN'], 'triggers' => $_rstriggers, 'action' => $_ftaction);
            }
        }

        return $_result;
    }

    function DisplayUserInterfaces($clientid, $domainid) {
        global $scramble;
        $GLOBALS['includeGreyBox'] = true;

        if (isset($this->rec)) {
            $_CurrentUI = $this->rec;
            $extrawhere = 'MD5(CONCAT("'.$scramble.'","_",CLIENTID,"_",DOMAINID,"_",UIID)) = "'.$_CurrentUI.'"';
        } else {
            $extrawhere = 'DOMAINID = '.$this->domainid;
        }

        /* Get the user interfaces */
        $_rs = $this->form->GetViewContent($detail=false, $masterrecord=false, false, $extrawhere);
        $_items = $_rs->GetAll();
        if ($_GET['overview'] == 'true') {
            foreach($_items as $_index => $_item) {
                if (trim($_item['PROCESSES']) != '') {
                    $_mainprocesses = explode("\n",$_item['PROCESSES']);
                    $_items[$_index]['processlist'] = $this->GetUI_Processes($clientid, $domainid, $_mainprocesses);
                }
                $_items[$_index]['facttypes'] = $this->GetUI_Triggers($clientid, $domainid, $_item['FACTTYPE']);

                $_items[$_index]['elements'] = $this->GetUI_Elements($clientid, $domainid, $_item[_RECORDID]);
            }

            $this->smarty->assign("items", $_items);
            $this->smarty->display("ui_overview.tpl.php");

        } elseif ($_CurrentUI) {
            $this->smarty->assign("uiid", $_items[0]['UIID']);

            $_elements = $this->GetUI_Elements($clientid, $domainid, $_CurrentUI);
            $this->smarty->assign("elements", $_elements);

            $_sql = 'SELECT METHODNAME, PACKAGE, IFNULL(PACKAGE, " Storedprocs") AS `GROUP` FROM mod_method WHERE clientid = ? AND domainid = ? AND (methodtype = "storedproc" or methodtype = "packproc") ORDER BY 2,1';
            $_rsstoredprocs = $this->form->database->userdb->GetAll($_sql, array($clientid, $domainid));
            $this->smarty->assign("storedprocs", $_rsstoredprocs);

            $_triggers = $this->GetUI_Triggers($clientid, $domainid, $_items[0]['FACTTYPE']);
            $this->smarty->assign("facttypes", $_triggers);

            $_mainprocesses = explode("\n",$_items[0]['PROCESSES']);
            $_processes = $this->GetUI_Processes($clientid, $domainid, $_mainprocesses);
            $this->smarty->assign("processlist", $_processes);

            $this->smarty->assign("items", $_items);
            $this->smarty->display("ui_show.tpl.php");
        } else {
            $this->smarty->assign("items", $_items);
            $this->smarty->display("ui.tpl.php");
        }
    }

    function DisplayMethods($clientid, $domainid) {
        global $_GVARS;

        $_GVARS['_includeJavascript'][] = $_GVARS['serverpath'].'/javascript/jquery/jquery.accordion.js';

        /* Get the method for the list */
        if ($_GET['const'] == 'packages') {
            $_sql = '
            SELECT m2.METHODID, s.METHODID as PACKAGEID, s.METHODNAME AS PACKAGE, s.REMARK AS PACKAGEREMARK, m2.METHODNAME, m2.REMARK
            FROM mod_method s LEFT JOIN mod_method m2
            ON s.clientid = m2.clientid AND s.domainid = m2.domainid AND s.methodname = m2.package
            WHERE s.clientid = ? AND s.domainid = ? AND s.methodtype = "package"';
        } else {
            $_sql = "SELECT METHODID, METHODNAME, REMARK, s.TRIGGERTABLE, s.TRIGGERTYPE, s.TRIGGEREVENT FROM mod_method s WHERE s.clientid = ? AND domainid = ?";
            $_sql = $this->form->owner->AddConstructFilter($_sql, false);
        }
        $_sql .= $this->form->owner->GetConstructOrderBy(false);
        $_methodlist = $this->form->database->dbGetAll($_sql, array($clientid, $domainid));

        /**
        * Process the search string for the methods
        */
        /* Get the stored procs, packages or functions */
        $_rs = $this->form->GetViewContent($detail=false, $masterrecord=false, 1000);
        $_items = $_rs->GetALl();

        /* Combine the items found via Polaris with the method list items */
        if (isset($_GET['q']))
            foreach($_items as $_item) {
                foreach($_methodlist as $k => $_method) {
                    if ($_method['METHODNAME'] == $_item['METHODNAME']) {
                        $_method['HASSEARCH'] = true;
                        $_methodlist[$k] = $_method;
                    }
                    if ($_method['PACKAGE'] == $_item['METHODNAME'])
                        $_methodlist[$k]['PACKAGEHASSEARCH'] = true;
                }
/*
                if ($_item['METHODNAME'] == $_currentMethod) {
                    $_currentitem = $_item;
                }
*/
            }


        $_currentitem = false;
        if (isset($_GET['item']) or isset($_GET['itemname']) or isset($_GET['pack'])) {
            if (isset($_GET['item'])) {
                $_currentMethod = $_GET['item'];
                $_varType = 'ID';
            } elseif (isset($_GET['itemname'])) {
                $_currentMethod = $_GET['itemname'];
                $_varType = 'NAME';
            } else {

            }
            $_currentpackage = $_GET['pack'];
            $_currentMethod = strtoupper($_currentMethod);
            $_sql = $this->form->MakeSQL($edit = false, $countselect = false, $thesearchvalue = false, $masterfields = false, $countdetails = false, $detailtable = false, $customfilter = 's.METHOD'.$_varType.' = ?', $skipsearchpart = true);
            $_currentitem = $this->form->database->dbGetAll($_sql, array($_currentMethod));
            $_currentitem = $_currentitem[0];

            if ($_currentitem) {
                if ($_currentitem['METHODTYPE'] == 'trigger') {
                    /**
                    * Fetch the columns for this Trigger
                    */
                    $_sql = 'SELECT * FROM mod_method_column WHERE clientid = ? AND domainid = ? AND methodid = ?';
                    $_columns = $this->form->database->dbGetAll($_sql, array($clientid, $domainid, $_currentitem['METHODID']));

                    foreach($_columns as $k => $_column) {
                        $_column['COLUMNUSAGE'] = str_replace('OLD','#OLD',$_column['COLUMNUSAGE']);
                        $_columns[$k]['COLUMNUSAGE'] = explode('#',$_column['COLUMNUSAGE']);
                    }
                    $this->smarty->assign('columns', $_columns);
                }
                /**
                * Place the main PACKAGE source in the packproc of packfunc because these don't have sources available
                */
                if ($_currentitem['PACKAGE'] != '') {
                    foreach($_items as $_item) {
                        if ($_item['METHODNAME'] == $_currentitem['PACKAGE']) {
                            $_currentitem['SOURCECODE'] = $_item['SOURCECODE'];
                            break;
                        }
                    }
                }
                /* Make nice array from arraylike structures SELECTed from MySQL */
                array_walk_recursive($_currentitem, '_transformFlatArrays', $sep = '###');

                /* Highlight the search string in the code and description */
                if (isset($_GET['q'])) {
                    $_currentitem['DESCRIPTION'] = str_replace($_GET['q'], '<span class="highlight">'.$_GET['q'].'</span>', $_currentitem['DESCRIPTION']);
// TAGS WERKEN NIET                    $_currentitem['SOURCECODE'] = str_replace($_GET['q'], '<span class="highlight">'.$_GET['q'].'</span>', $_currentitem['SOURCECODE']);
                }

                /* Process the references to other objects in the description */
                $_currentitem['DESCRIPTION'] = $this->ProcessReferences($_currentitem['DESCRIPTION']);

                /* ColorSyntax the sourcecode */
                $_currentitem['SOURCECODE'] = $this->ColorSyntax($_currentitem['SOURCECODE']);
            }
        }
        array_walk_recursive($_items, '_transformFlatArrays');
//var_dump($_currentitem);
        $this->smarty->assign("currentmethod", $_currentMethod);
        $this->smarty->assign("currentpackage", $_currentpackage);
        $this->smarty->assign("currentitem", $_currentitem);
        $this->smarty->assign("items", $_items);
        $this->smarty->assign("methoditems", $_methodlist);
        $this->smarty->display("storedprocs.tpl.php");
    }

    function DisplayConfiguration($clientid, $domainid) {
        global $_GVARS;
        global $scramble;

        $_GVARS['_includeJavascript'][] = $_GVARS['serverpath'].'/javascript/jquery/jquery.progressbar.min.js';

        /**
        * Statistics of the documentation progress
        */
        $_sql = "
        select
        (select count(*) from mod_method where (methodtype = 'trigger') and remark = 'N' and description is not null) as triggers_done,
        (select count(*) from mod_method where (methodtype = 'trigger') and remark = 'N' ) as triggers_total,
        (select count(*) from mod_method m1 where (methodtype = 'storedproc' or methodtype = 'packproc') and remark = 'N' and ((description is not null) OR (description is null AND package is not null AND (select description FROM mod_method m2 WHERE m1.package = m2.methodname) <> '') )) as storedprocs_done,
        (select count(*) from mod_method where (methodtype = 'storedproc' or methodtype = 'packproc') and remark = 'N' ) as storedprocs_total,
        (select count(*) from mod_method m1 where (methodtype = 'function' or methodtype = 'packfunc') and remark = 'N'  and ((description is not null) OR (description is null AND package is not null AND (select description FROM mod_method m2 WHERE m1.package = m2.methodname) <> '') )) as functions_done,
        (select count(*) from mod_method where (methodtype = 'function' or methodtype = 'packfunc') and remark = 'N') as functions_total,
        (select count(*) from mod_method where methodtype = 'package' and description is not null) as packs_done,
        (select count(*) from mod_method where methodtype = 'package') as packs_total,
        (select count(*) from mod_semantic_concept where trim(DEFINITION) <> '') as concepts_done,
        (select count(*) from mod_semantic_concept) as concepts_total ,
        (select count(*) from mod_event where description is not null) as events_done,
        (select count(*) from mod_event) as events_total ,
        (select count(*) from mod_ui where status = 'DEB' or status = 'NHA') as ui_done,
        (select count(*) from mod_ui) as ui_total ,
        (select count(*) from mod_facttype where comment LIKE '%OVERBODIG%' OR (sentencepattern is not null and substr(sentencepattern, 1, 1) <> '<')) as ftd_done,
        (select count(*) from mod_facttype) as ftd_total
        from mod_method
        where clientid = ? and domainid = ?
        limit 0,1
        ";
        $prog = $this->form->database->dbGetAll($_sql, array($clientid, $domainid));
        $this->smarty->assign("progress", $prog);

        $_sql = 'SELECT COMMENT, MD5(CONCAT(?,"_",CLIENTID,"_",DOMAINID,"_",COMMENTID)) as '._RECORDID.' FROM mod_comment WHERE clientid = ? AND domainid = ? AND SUBJECTID = ?';
        $comments = $this->form->database->dbGetAll($_sql, array($scramble, $clientid, $domainid, $this->_standardCommentIds['config']));
        $this->smarty->assign("commenttablename", 'mod_comment');
        $this->smarty->assign("comment", $comments[0]);

        require_once('classes/base/plrDatabase.class.php');
        $_databases = new plrDatabases($this->form->polaris());
        $_databases->LoadRecords($_SESSION['clienthash']);
        $this->smarty->assign("databaseSelect", $_databases->GetSelectList());

        $this->smarty->display("config.tpl.php");
    }

    function DisplayScheduledTasks($clientid, $domainid) {
        global $_GVARS;

//        $_GVARS['_includeJavascript'][] = $_GVARS['serverroot'].'/javascript/master-raphael.js';

        $rs = $this->form->GetViewContent($detail=false, $masterrecord=false);
        $items = $rs->GetAll();

        /* Highlight the sourcecode */
        foreach($items as $k => $item) {
            $item['SOURCECODE'] = $this->ColorSyntax($item['SOURCECODE'], $item['CODELANGUAGE']);
            $items[$k] = $item;
        }

        $this->smarty->assign('items', $items);
        $this->smarty->display("scheduledtasks.tpl.php");
    }

    function DisplayFactTypes($clientid, $databaseid) {
        $_rs = $this->form->GetViewContent($detail=false, $masterrecord=false);
        $_items = $_rs->GetAll();
        $this->smarty->assign('items', $_items);
        $this->smarty->display("facttypes.tpl.php");
    }

    function DisplayPolarisDocs($clientid, $databaseid) {
        $this->smarty->assign('domains', false);
        $this->smarty->assign('testserver', '172.16.1.10');
        $this->smarty->assign('prodserver', '172.16.1.16');
        $this->smarty->display("polarisdocs.tpl.php");
    }

    function getTransContexts($current) {
        $_r = '<select class="contextselection" id="contextselection" name="context">';
        foreach($this->translationData as $k => $c) {
            $_selected = '';
            if ($current == $k) {
                $_selected = 'selected="selected"';
            }
            $_r .= '<option value="'.$k.'" '.$_selected.'>'.$c['label'].'</option>';
        }
        $_r .= '</select>';
        return $_r;
    }

    function getTransRecords($context) {
        if ($context) {
            $_contextData = $this->translationData[$context];
            $_tablename = $_contextData['table'];
            $_keycolumns = $_contextData['keycolumns'];
            $_keylist = implode(',', $_keycolumns);
            $_columns = $_contextData['transcolumns'];
            $_columnlist = implode(',', $_columns);
            $_filter = str_replace('__TRANSFORMS__', $this->transforms, $_contextData['filter']);
            $_filter .= ' AND CLIENTID = 2';
            $_sql = "SELECT $_keylist, $_columnlist FROM $_tablename WHERE $_filter ORDER BY $_keylist";
            $_rs = $this->form->dbGetAll($_sql);
            foreach($_rs as $_k => $v) {
                foreach($_keycolumns as $_keycolumn) {
                    $_rs[$_k]['_KEYVALUE_'] .= $_rs[$_k][$_keycolumn].'_';
                }
                $_rs[$_k]['_KEYVALUE_'] = substr($_rs[$_k]['_KEYVALUE_'], 0, -1);
                foreach($_columns as $_column) {
                    $_a = explode('|', $v[$_column]);
                    foreach($_a as $_part) {
                        if (substr($_part, 0, 3) == 'NL:') {
                            $_rs[$_k]['TRANS_NL'] = substr($_part, 3);
                        } elseif (substr($_part, 0, 3) == 'DE:') {
                            $_rs[$_k]['TRANS_DE'] = substr($_part, 3);
                        } else {
                            if (!isset($_rs[$_k][$_column.'_NL']))
                                $_rs[$_k]['TRANS_NL'] = $_part;
                        }
                    }
                    if ($_rs[$_k]['TRANS_DE'] == FALSE) {
                        $_rs[$_k]['TRANS_DE_PENDING'] = TRUE;
                        $_rs[$_k]['TRANS_DE'] = $_rs[$_k]['TRANS_NL'];
                    }
                    if ($_rs[$_k]['TRANS_DE'] == $_rs[$_k]['TRANS_NL']) {
                        $_rs[$_k]['TRANS_DE_PENDING'] = TRUE;
                    }
                }
            }
        } else {
            $_rs = null;
        }
        return $_rs;
    }

    function DisplayTranslations($clientid, $databaseid) {
        $_defaultContext = array_keys($this->translationData);
        $_defaultContext = $_defaultContext[0];
        $_context = isset($_POST['context']) ? $_POST['context'] : $_defaultContext;

        $this->smarty->assign('domains', false);
        $this->smarty->assign('contextselection', $this->getTransContexts($_context));
        $this->smarty->assign('records', $this->getTransRecords($_context));
        $this->smarty->assign('feedback', $this->transfeedback);
        $this->smarty->display("translations.tpl.php");
    }

    function Show($outputformat='xhtml', $rec=false) {
        global $_GVARS;

        parent::Show($outputformat, $rec);

        $this->smarty->assign("domains", $this->domains);
        $this->smarty->assign("currentdomain", $this->domainid);

        if (in_array(intval($this->moduleid), range(1,7))) {
            $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/nha_docsys.js';
            $_GVARS['_includeJavascript'][] = $_GVARS['serverpath'].'/javascript/wmarkdown/showdown.js';
            $_GVARS['_includeJavascript'][] = $_GVARS['serverpath'].'/javascript/polaris/Modeler.js';
        }

        if (isset($_GET['action'])) {
            /**
            * Show the Polaris generated forms when the user wants to insert or edit an item
            */
            switch($_GET['action']) {
            case 'edit':
            case 'insert':
                $this->form->ShowFormView($this->state, $this->permission, $detail=false, $masterrecord=false);
                $this->form->ShowNavigationLinks($this->state, $this->permission, $detail=false);
            break;
            case 'searchext':
                $this->form->ShowSearchForm($this->state, $this->permission, $detail=false, $masterrecord=false);
                $this->form->ShowNavigationLinks($this->state, $this->permission, $detail=false);
            break;
            case 'view':
                /***
                * User interface overview
                */
                $this->DisplayUserInterfaces($this->clientid, $this->domainid);
            }
        } else {
            /**
            * Show the custommade forms when user views a list of items
            */
            switch ($this->moduleid) {
            case 1:
                /***
                * Semantic concepts
                */
                $this->DisplaySemanticConcepts($this->clientid, $this->domainid);
            break;
            case 2:
                /***
                * User interface overview
                */
                $this->showControls = false;
                $this->showDefaultButtons = false;
                $this->DisplayUserInterfaces($this->clientid, $this->domainid);
            break;
            case 3:
                /***
                * Stored Procedures
                */
                $this->DisplayMethods($this->clientid, $this->domainid);
            break;
            case 4:
                /***
                * Settings, configuration and syncing
                */
                $this->DisplayConfiguration($this->clientid, $this->domainid);
            break;
            case 5:
                /***
                * Scheduled Tasks
                */
                $this->DisplayScheduledTasks($this->clientid, $this->domainid);
            break;
            case 6:
                /***
                * Sentence patterns
                */
                $this->DisplayFactTypes($this->clientid, $this->domainid);
                $this->showControls = true;
            break;
            case 7:
                /***
                * Sentence patterns
                */
                $this->showControls = false;
                $this->DisplayPolarisDocs($this->clientid, $this->domainid);
            break;
            case 8:
                /***
                * Translations
                */
                $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/nha_trans.js';
                $this->showControls = false;
                $this->DisplayTranslations($this->clientid, $this->domainid);
            break;
            }
        }
    }

}