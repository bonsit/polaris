<?php
require('_shared/module._base.php');

use Michelf\Markdown;

class ModuleAertes extends _BaseModule {
    var $stylesheet;

    function ModuleAertes($moduleid, $module, $clientname=false) {
        global $_GVARS;
        parent::__construct($moduleid, $module, $clientname);

        $this->processed = false;
        $this->stylesheet = 'default.css';
    }

    function __GetCustomFilter() {
		return 'sss1.ACTIEF = \'Y\'';
    }

    function ProcessJson() {
        $_content = false;
        $this->form->database->userdb->SetFetchMode(ADODB_FETCH_ASSOC);

        $_func = $_GET['func'];
        $_plr_recordid = $_GET['plr__recordid'];
        if (!empty($_func)) {
            switch ($_func) {
                case 'aertes_cursusdata':
                    $_content = $this->GetCursusData($_plr_recordid);
                    break;
                default:
                    break;
            }
        }
        if ($_content !== false) {
            echo $_content;
            exit; // stop with the rest of the html
        }
        return false;
    }

    function ProcessRequest($_allesRecord) {
        if (isset($_POST['_hdnAction'])) {
            switch ($_POST['_hdnAction']) {
            case 'publish':
                return $this->PublishArticle($_POST['_hdnRecordID']);
            break;
            case 'plr_save':
                $_POST['UPDATED_AT'] = date('Y-m-d H:i:s');
                $_POST['BLOG_ID'] = 1;
                $result = SaveRecord($_POST);
                if ($result == true) {
                    $_GET['action'] == '';
                }
            break;
            }
        }
    }

    function GetCursusData($_plr_recordid) {
        $_array = Array();
        $_recordSelector = $this->form->database->makeRecordIDColumn('CURSUS', 'c');

        $_sql = "
        SELECT startdatum, datumid
        FROM cursusdata d
        LEFT JOIN cursus c
        ON d.cursusid = c.id
        WHERE $_recordSelector = ?
        ORDER BY datumid";
        $_rs = $this->form->database->userdb->GetAll($_sql, array($_plr_recordid));
        foreach ($_rs as $_key => $_value) {
            $_items[$_value['datumid']][] = $_value['startdatum'];
        }
        $_return = array();
        if ($_items)
            foreach($_items as $_key => $_value) {
                $_return[] = array('cursusblok' => $_key, 'datums' => $_value);
            }
        return json_encode($_return);
    }

    function ShowListView($state, $permission, $detail=false, $masterrecord=false) {
        $_customfilter = $this->__GetCustomFilter();
        $_rs = $this->form->GetViewContent($detail=false, $masterrecord=false, $limit=false, $_customfilter);
        // foreach($_rs as $_rec) {
        //     $_items[$_rec['CURSUSCODE']] = $_rec['CURSUSCODE'].' - '.$_rec['CURSUSNAAM'];
        // }
        $this->smarty->assign('items', $_rs);
        $this->smarty->assign('selecteditem', $_GET['cursuscode']);
        $this->smarty->assign('stylesheet', $this->stylesheet);
        $this->AddModuleJS(['main.js']);
        // $this->AddCustomJS('/js/plugins/fullcalendar/moment.min.js');
        // $this->AddCustomJS('/js/plugins/fullcalendar/fullcalendar.min.js');

        echo $this->smarty->Fetch('cursusdatalist.tpl.php');
    }

    function ShowFormView($state, $permission, $detail=false, $masterrecord=false) {
        $_customfilter = $this->__GetCustomFilter();
        $_rs = $this->form->GetViewContent($detail=false, $masterrecord=false, $limit=false, $_customfilter);

        $_rs[0]['BODY_PREVIEW'] = Markdown::defaultTransform($_rs[0]['BODY']);
        $this->smarty->assign('stylesheet', $this->stylesheet);
        $this->smarty->assign('items', $_rs);
        echo $this->smarty->Fetch("blogedit.tpl.php");
    }

    function Show($outputformat='xhtml', $rec=false) {
        global $_GVARS;

        if ($this->form->outputformat == 'json') {
            $this->ProcessJson();
        } else {
            parent::Show();

            switch($_GET['action']) {
            case '':
                $this->ShowListView($this->state, $this->permission, $detail=false, $masterrecord=false);
            break;
            case 'edit':
            case 'insert':
                $this->ShowFormView($this->state, $this->permission, $detail=false, $masterrecord=false);
            break;
            }
        }
    }

    function QueueDeelnemerUitnodiging($email, $values, $deelnemerid, $recordid) {
       global $polaris;
       global $_GVARS;

       # Add the neccessary values
       $values['...'] = '';

       # And send this message to the queue
       $polaris->AddToQueue('queue_someaction', array('insertValues' => $values));
    }

    public function queue_verwerk($data) {
       # Do something with the #data
       // ...

       # Always return true, when everything is handled correctly
       return true;
    }

}
