Aertes = {

    initialize: function() {
        var Self = Aertes;

        $("#cursuslijst").click(function(e) {
        	$("#cursuslijst li").removeClass('active');
        	$(e.target).parent('li').addClass('active');
        	Self.refreshData($(this).val());
        });

        //Self.initializeCalendar();
        Self.initializeCursusLijst();
    },

    getCursusData: function(plr__recordid) {
    	log(plr__recordid);
        $_datumlijst = $("#datumlijst");
        $_datumlijst.empty();
        $.getJSON(_servicequery, {
            'func': 'aertes_cursusdata',
            'plr__recordid': plr__recordid
        }, function(data, textStatus) {
            if (textStatus == 'success') {
            	$(data).each(function(i, elem) {
	            	$_ul = $('<ul>', {'class': 'folder-list cursusblok', 'style': 'padding: 0'});
    	        	$_datumlijst.append($_ul);
            		$(elem.datums).each(function(j, datum) {
            			$_ul.append('<li data-cursusid="'+datum+'"><a href="#"><i class="fa fa-folder"></i>'+datum+'</li>');
            		})

            	});
            }
        });
    },

    initializeCursusLijst: function() {
        var Self = Aertes;

    	$("#cursuslijst a").click(function(e) {
    		e.preventDefault();
    		Self.getCursusData($(this).parent().data('cursusid'));

    	});
    },

    initializeCalendar: function() {
        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();

        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            editable: true,
            droppable: true, // this allows things to be dropped onto the calendar
            drop: function() {
                // is the "remove after drop" checkbox checked?
                if ($('#drop-remove').is(':checked')) {
                    // if so, remove the element from the "Draggable Events" list
                    $(this).remove();
                }
            },
            events: [
                {
                    title: 'All Day Event',
                    start: new Date(y, m, 1)
                },
                {
                    title: 'Long Event',
                    start: new Date(y, m, d-5),
                    end: new Date(y, m, d-2)
                },
                {
                    id: 999,
                    title: 'Repeating Event',
                    start: new Date(y, m, d-3, 16, 0),
                    allDay: false
                },
                {
                    id: 999,
                    title: 'Repeating Event',
                    start: new Date(y, m, d+4, 16, 0),
                    allDay: false
                },
                {
                    title: 'Meeting',
                    start: new Date(y, m, d, 10, 30),
                    allDay: false
                },
                {
                    title: 'Lunch',
                    start: new Date(y, m, d, 12, 0),
                    end: new Date(y, m, d, 14, 0),
                    allDay: false
                },
                {
                    title: 'Birthday Party',
                    start: new Date(y, m, d+1, 19, 0),
                    end: new Date(y, m, d+1, 22, 30),
                    allDay: false
                },
                {
                    title: 'Click for Google',
                    start: new Date(y, m, 28),
                    end: new Date(y, m, 29),
                    url: 'http://google.com/'
                }
            ]
        });
    },

    refreshData: function(cursuscode) {
        $.getJSON(_servicequery, {
            'func': 'aertes_cursusdata',
            'cursuscode': cursuscode
        }, function(data, textStatus) {
            if (textStatus == 'success') {
                $.map(data, function(elem, i) {
                    //Self.dataZorgverleners.push({"id": elem.VOLGNUMMER, "name": elem.VOORNAAM + ' ' + elem.ACHTERNAAM + '  |  ' + (elem.SOORT_ZORG||'') + '  |  ' + (elem.PLAATSNAAM||'')});
                });
                //$("#zorgVerlenerID").addOptions(Self.dataZorgverleners, 'id','name');
            }
        });
    }

};

$(document).ready(function() {
    var Self = Aertes;

    Self.initialize();
});

