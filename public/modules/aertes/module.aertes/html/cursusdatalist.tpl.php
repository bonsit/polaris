
{include file="shared.tpl.php"}

<div class="wrapper wrapper-content">

    <div class="row">
        <div class="col-lg-4">
            <div class="ibox">

<!--     				{html_options name="cursusselect" selected=$selecteditem options=$items id="cursusselect"}
 -->

 <div class="panel panel-primary">
  <div class="panel-heading">Opleidingen</div>
  <div class="panel-body" style="max-height: 500px;overflow-y: scroll;">
	<ul class="folder-list" id="cursuslijst" style="padding: 0">
		{section loop=$items name=i}
		<li data-cursusid="{$items[i].PLR__RECORDID}">
      <a href="#"><i class="fa fa-folder"></i>{$items[i].CURSUSCODE} - {$items[i].CURSUSNAAM}</a></li>
		{/section}
	</ul>

  </div>
</div>


			</div>
		</div>

        <div class="col-lg-4">
            <div class="ibox">
 <div class="panel panel-primary">
 	<div class="btn-group pull-right">
  		<div class="btn btn-primary"><i class="fa fa-plus"></i></div>
  	</div>
  <div class="panel-heading">

	Geplande cursussen
  </div>
  <div class="panel-body datumlijst" id="datumlijst" style="max-height: 500px;overflow-y: scroll;">

  </div>
</div>


			</div>
		</div>


<!--
		<div class="col-lg-9">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Striped Table </h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user" x-placement="bottom-start" style="position: absolute; top: 18px; left: 26px; will-change: top, left;">
                            <li><a href="#" class="dropdown-item">Config option 1</a>
                            </li>
                            <li><a href="#" class="dropdown-item">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div id="calendar" class="fc fc-unthemed fc-ltr"></div>
                </div>
            </div>
        </div>
 -->
    </div>
</div>

