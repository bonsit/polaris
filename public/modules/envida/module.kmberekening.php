<?php
require('_shared/module._base.php');

class Module extends _BaseModule {

    function Module($moduleid, $module) {
        global $_GVARS;
        global $polaris;

        parent::__construct($moduleid, $module);

        $this->processed = false;

        /**
        * What client are we talking about? (nog doen: eventueel selecteerbaar maken)
        */
        $this->clientid = $_SESSION['clientid'];

        /* Load the user database */
        $_databaseid = null;
        if (isset($_databaseid)) {
            $db = new plrDatabase($polaris);
            $db->LoadRecord(array($this->clientid, $_databaseid));
            $this->userdatabase = $db->connectUserDB();
        }

        if (isset($_POST['_hdnProcessedByModule'])) {
            $_allesRecord = $_POST;

            try {
                switch($_POST['_hdnFORMTYPE']) {
                /***
                * Upload postcodebestand verwerken
                */
                case 'uploadbestand':
                    $_tmp = $_FILES['file']['tmp_name'];
                    $_data = file_get_contents($_tmp);
                    $result = $this->VerwerkKMBestand($_data, $_skipFirstLine=true);
                    $tempName = md5(rand());
                    file_put_contents('/tmp/'.$tempName, $result);
                    header("Content-Disposition: attachment; filename=".$_GET['outputfile'].".csv");
                    header('Content-Type: application/csv');
                    var_dump('bart');
                    //header("Location: http://kmberekening.polaris.dev/services/csv/app/envida/const/km_berekening/?outputfile=$tempName");
                break;
                }

            } catch (Exception $E) {
                $detailedmessage = $E->completemsg."\r\n".parse_backtrace(debug_backtrace());

                $resultArray = Array('result'=>false, 'query'=>$_SERVER['REQUEST_URI'], 'error'=>$E->msg, 'detailed_error'=>$detailedmessage);
                echo  json_encode($resultArray);
            }
        }
    }

    function Process() {
        global $_GVARS;
    }

    function bepaalAfstand($start, $end) {
        // Google Map API which returns the distance between 2 postcodes
        $postcode1 = preg_replace('/\s+/', '', $start);
        $postcode2 = preg_replace('/\s+/', '', $end);
        $result    = array();

        $url = "http://maps.googleapis.com/maps/api/distancematrix/json?origins=$postcode1&destinations=$postcode2&mode=driving&language=nl-nl&sensor=false";

        $data   = @file_get_contents($url);
        $result = json_decode($data, true);

        return $result["rows"][0]["elements"][0]["distance"]["value"];
    }

    function VerwerkKMBestand($data, $skipFirstLine=false) {
        $lines =  explode(PHP_EOL,$data);
        $_c = array('medewerkernr'=>0, 'contractnr'=>1, 'datum'=>2, 'postcode'=>3, 'initialen'=>4, 'achternaam'=>5, 'postcode2'=>6, 'afstand'=>7);
        $_cOut = array('medewerkernr'=>0, 'dienstverband'=>1, 'contractnr'=>2, 'empty1'=>3, 'empty2'=>4, 'empty3'=>5, 'empty4'=>6, 'aantalKM'=>7);
        $_huidigeMedewerker = null;
        $_postcodes = array();
        $_totaalAfstand = 0;
        $_totaalLijst = array();

        foreach($lines as $idx => $line) {
            if (!$skipFirstLine or ($skipFirstLine and $idx > 0)) {
                $_v = explode(';', $line);
                $_nextLine = explode(';', $lines[$idx+1]);
                if (!is_null($_nextLine) and ($_v[$_c['medewerkernr']] == $_nextLine[$_c['medewerkernr']])) {
                    // nieuwe medewerker
                    $_postcodes = array();
                }
                if (!is_null($_nextLine) and ($_v[$_c['datum']] == $_nextLine[$_c['datum']])
                and ($_v[$_c['medewerkernr']] == $_nextLine[$_c['medewerkernr']])) {
                    $_v[$_c['postcode2']] = $_nextLine[$_c['postcode']];
                    $_v[$_c['afstand']] =  $this->bepaalAfstand($_v[$_c['postcode']], $_v[$_c['postcode2']]);
                }
                $_totaalLijst[] = $_v;
            }
        }

        $_aggrLijst = array();
        $_totaal = 0;
        foreach($_totaalLijst as $idx => $_line) {
            $_v = $_line;
            $_nextLine = $_totaalLijst[$idx+1];
            if (isset($_v[$_c['afstand']])) {
                $_totaal = $_totaal + $_v[$_c['afstand']];
            }
            if ($_v[$_c['medewerkernr']] !== $_nextLine[$_c['medewerkernr']] || !isset($_nextLine[$_c['medewerkernr']])) {
                $_result[$_cOut['medewerkernr']] = $_v[$_c['medewerkernr']];
                $_result[$_cOut['dienstverband']] = 1;
                $_result[$_cOut['contractnr']] = $_v[$_c['contractnr']];
                $_result[$_cOut['empty1']] = '';
                $_result[$_cOut['empty2']] = '';
                $_result[$_cOut['empty3']] = '';
                $_result[$_cOut['empty4']] = '';
                $_result[$_cOut['aantalKM']] = round($_totaal / 1000, 2);
                $_aggrLijst[] = $_result;
                $_totaal = 0;
            }
        }

        return $_aggrLijst;
    }

    function Show($outputformat='xhtml', $rec=false) {
 $_database = '538;1;01-nov.-16;6225JN;JOLANDA;Dahmen - Huben
538;1;01-nov.-16;6221SZ;JOLANDA;Dahmen - Huben
538;1;01-nov.-16;6229AE;JOLANDA;Dahmen - Huben
538;1;02-nov.-16;6225JN;JOLANDA;Dahmen - Huben
538;1;02-nov.-16;6226HM;JOLANDA;Dahmen - Huben
538;1;03-nov.-16;6225JN;JOLANDA;Dahmen - Huben
538;1;03-nov.-16;6228GR;JOLANDA;Dahmen - Huben
538;1;03-nov.-16;6228HK;JOLANDA;Dahmen - Huben
538;1;04-nov.-16;6225JN;JOLANDA;Dahmen - Huben
538;1;04-nov.-16;6221SZ;JOLANDA;Dahmen - Huben
538;1;04-nov.-16;6229EC;JOLANDA;Dahmen - Huben
538;1;07-nov.-16;6225JN;JOLANDA;Dahmen - Huben
538;1;07-nov.-16;6227CL;JOLANDA;Dahmen - Huben
538;1;08-nov.-16;6225JN;JOLANDA;Dahmen - Huben
538;1;08-nov.-16;6221SZ;JOLANDA;Dahmen - Huben
538;1;08-nov.-16;6229AE;JOLANDA;Dahmen - Huben
540;1;01-nov.-16;6225JN;JOHAN;Janssen
540;1;01-nov.-16;6221SZ;JOHAN;Janssen
540;1;01-nov.-16;6229AE;JOHAN;Janssen
540;1;02-nov.-16;6225JN;JOHAN;Janssen
540;1;02-nov.-16;6226HM;JOHAN;Janssen
540;1;03-nov.-16;6225JN;JOHAN;Janssen
540;1;03-nov.-16;6228GR;JOHAN;Janssen
540;1;03-nov.-16;6228HK;JOHAN;Janssen
540;1;04-nov.-16;6225JN;JOHAN;Janssen
540;1;04-nov.-16;6229EC;JOHAN;Janssen
540;1;07-nov.-16;6225JN;JOHAN;Janssen
540;1;07-nov.-16;6227CL;JOHAN;Janssen
540;1;08-nov.-16;6221SZ;JOHAN;Janssen
540;1;08-nov.-16;6229AE;JOHAN;Janssen';

        if ($outputformat == 'xhtml') {
            parent::Show($outputformat, $rec);

            $this->showControls = false;
            $this->showDefaultButtons = false;
            $this->smarty->display("main.tpl.php");
        } elseif ($outputformat == 'csv') {
            if (isset($_GET['outputfile'])) {
                //header("Content-Disposition: attachment; filename=".$_GET['outputfile'].".csv");
                //header('Content-Type: application/csv');
//                echo file_get_contents('/tmp/'.$outputfile);
            } else {
                echo "Geen bestand gevonden.";
            }
        }

        // echo '<pre>';
        // $result = $this->VerwerkKMBestand($_database);
        // foreach($result as $_line) {
        //     var_dump(implode(',', $_line));
        // }
        // echo '</pre>';
    }

}