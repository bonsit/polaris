
{include file="shared.tpl.php"}

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">

                    <input type="hidden" name="_hdnFORMTYPE" value="uploadbestand" />
                    <input type="hidden" name="_hdnProcessedByModule" value="true" />

                    <div class="dz-message needsclick">
                        Plaats hier uw CSV bestanden...
                    </div>


            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var dataform = document.getElementById('dataform');
    dataform.className += ' dropzone';
    dataform.action = '/services/csv/app/envida/const/km_berekening/';
</script>