<?php
/***************************************************************************
 * Copyright (c) 2004 by the Polaris developers
 *
 * Polaris Settings file
 *
 * THIS FILE IS GENERATED. PLEASE, BE CAREFULL WHEN EDITING THIS FILE MANUALLY
 *
 ***************************************************************************/
require_once('includes/basefunctions.inc.php');

# timezone GMT management
$gmt_timezone = '';

# language choice
define('PLRDEFAULTLANG', '');
