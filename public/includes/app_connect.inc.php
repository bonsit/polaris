<?php

// *********************************
// POLARIS CONNECTION INITIALISATION
// *********************************
require_once($_GVARS['docroot'].'/includes/plrsettings.inc.php');
require_once('adodb.inc.php');

$plrdb = NewADOConnection($DBType);
$plrhost = PLRHOST;
if (PLRPORT > 0) $plrhost .= ':'.PLRPORT;

if (!$plrdb->PConnect($plrhost, PLRUSERNAME, PLRPASSWORD, PLRDATABASE)) {
//	header("Location: $_GVARS['serverroot']/plrsettings.php");
//	exit;
}
if ( $_CONFIG['logperformance'] ) $plrdb->LogSQL(); // log all sql queries
if ( $_CONFIG['cachequeries'] )   $ADODB_CACHE_DIR = $_CONFIG['cachequeries_folder'];
