<?php
require_once 'includes/dbroutines.inc.php';

function parse_backtrace($raw){
    $output="";

    foreach($raw as $entry){
            $output.="\nFile: ".$entry['file']." (Line: ".$entry['line'].")\n";
            $output.="Function: ".$entry['function']."\n";
            // $output.="Args: ".implode(", ", $entry['args'])."\n";
    }

    return $output;
}

function DeleteRecords($RecordArray) {
    global $polaris;

    $databaseid = $RecordArray['_hdnDatabase'];
    $plrdatabase = new base_plrDatabase($polaris);
    $plrdatabase->LoadRecordHashed($databaseid);
    $plrdatabase->connectUserDB();

	$affectedRows = 0;
    $tablename = $RecordArray['_hdnTable'];
    foreach($RecordArray as $recid => $onoff) {
        if (substr($recid, 0, 4) == 'rec@') {
            $recordid = substr($recid, 4, 32);
            $recordid = $plrdatabase->customUrlDecode($recordid);
            $keyfieldselect = $plrdatabase->makeEncodedKeySelect($tablename, $recordid);
            $affectedRows = $affectedRows + $plrdatabase->deleteRecord($tablename, $keyfieldselect);
        }
    }
    return($affectedRows);
}

function DeleteOneRecord($RecordArray) {
    global $polaris;

    $databaseid = $RecordArray['_hdnDatabase'];
    $plrdatabase = new base_plrDatabase($polaris);
    $plrdatabase->LoadRecordHashed($databaseid);
    $plrdatabase->connectUserDB();

    $tablename = $RecordArray['_hdnTable'];
    // convert url-safe ROWID back to real Oracle ROWID
    $recordid = $plrdatabase->customUrlDecode($RecordArray['_hdnRecordID']);

    $keyfieldselect = $plrdatabase->makeEncodedKeySelect($tablename, $recordid);
    $affectedRows = $plrdatabase->deleteRecord($tablename, $keyfieldselect);
    return($affectedRows);
}

function show_db_results($buffer) {
    global $polaris;

    $GLOBALS['debuggingview'] = true;
    $polaris->tpl->assign('debug_message', $buffer);
}

function allFieldsAreNull($fields) {
  if (is_array($fields))
    foreach ($fields as $value) {
      if ($value != '') {
        return FALSE;
      }
    }
  return TRUE;
}

function fillRecordWithAutoValues(&$record, $autosupervalues) {
  $superkey = array_keys($autosupervalues);
  foreach($record as $field => $value) {
    /**
    * if the record has a column which exists in the autosupervalues array
    * and the column is empty ('') then feed the record with the new (auto) value
    */
    if ($value == '') {
      foreach($autosupervalues as $superkey => $supervalue) {
        if ($field == $superkey) {
          $record[$field] = $supervalue;
        }
      }
    }
  }
}

function __saveFiles(&$oneRecord) {
    $dir = false;
    foreach($oneRecord as $field => $value) {
        if (substr($field, -4) == '_DIR') {
            $dir = $value;
            $realfieldname = substr($field, 4, -4);
            break;
        }
    }
    if ($dir) {
        require_once 'includes/func_imageupload.inc.php';
        // check if a file was uploaded
        if (isset($_FILES[$realfieldname.'_FILE'])) {
            if ( substr($dir, -1) != '/') $dir .= '/';
            $movedfiles = moveuploadedimage($realfieldname.'_FILE', $dir);
            $uploadcount = count($movedfiles);
        }
        if ($uploadcount == 1) {
            $oneRecord[$realfieldname] = basename($movedfiles[0]);
            return basename($movedfiles[0]);
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function ExtractTableFields($array, $tablename=false) {
    $result = Array();
    if (is_array($array))
        foreach($array as $field => $value) {
            $possep = strpos($field, '!');
            if ($possep !== FALSE) {
                $parts = explode('!', $field);
                if ($parts[0] == $tablename)
                    $result[$parts[1]] = $value;
            } else {
                if ($tablename == false)
                    $result[$field] = $value;
            }
        }
    return $result;
}

function __saveRecord(&$plrdatabase, $oneRecord, &$autosupervalues, $skipemptyfields=false) {
    global $polaris;

    $fields = StripHiddenFields($oneRecord);
    $tablename = $oneRecord['_hdnTable'];
    $constructname = $oneRecord['_hdnConstruct'] ?? false;
    if (!allFieldsAreNull($fields) or count($fields) == 1) {
        $state = $oneRecord['_hdnState'];
        $recordid = $oneRecord['_hdnRecordID'] ?? false;
        $fields = $polaris->ConvertFloatValues($fields);

        $_justtablename = explode('.',$tablename);
        if (isset($_justtablename[1]) and $_justtablename[1])
            $_justtablename = $_justtablename[1];
        else
            $_justtablename = $_justtablename[0];

        // Process the multifields before replacing the dynamic variables (because the function cannot process arrays)
        $fields = $polaris->ConvertJSONMultiField($fields);
        $fields = $polaris->KeepDefaultColumns($constructname, $_justtablename, $fields);
        $fields = $polaris->ReplaceDynamicVariables($fields);

        if ($state == 'edit') {
            $keyfieldselect = $plrdatabase->makeEncodedKeySelect($tablename, $recordid);
            $autovalue = $plrdatabase->updateRecord($tablename, $keyfieldselect, $fields);
        } elseif ($state == 'insert' or $state == 'clone') {
            $autovalue = $plrdatabase->insertRecord($tablename, $fields, $autosupervalues);
            // when the option Pin After Insert is active, then also Pin the inserted record
            if ($state == 'insert' and isset($oneRecord['_hdnPinAfterInsert']) and $oneRecord['_hdnPinAfterInsert'] == 'true') {
                $keyfieldselect = $plrdatabase->MakeFieldSetValueString($autovalue);
                $recordid = $plrdatabase->customUrlEncode($plrdatabase->userdb->GetOne("SELECT ROWIDTOCHAR(ROWID) FROM $tablename WHERE $keyfieldselect"));
                $pinrec = new controllers_plr_pinrecords();
                $pinrec->pinRecords($oneRecord, $recordid, TRUE);
            }
        } elseif ($state == 'replace') {
            $keys = array_toupper($plrdatabase->userdb->MetaPrimaryKeys($tablename));
            $autovalue = $plrdatabase->replaceRecord($tablename, $keys, $fields, $autosupervalues, $autoquote = true, $skipemptyfields);
        }
        return $autovalue;
    }
}

function AddDataScopeValuesToRecord($datascopes, $recordArray) {
    $_result = $recordArray;
    if (count($datascopes) > 0) {
        foreach($datascopes as $scope => $scopevalues) {
            if (!isset($_result[$scope]))
                $_result[$scope] = $scopevalues['filtervalue'];
        }
    }
    return $_result;
}

function SaveRecord($recordArray, $plrdatabase=false, $debug=false) {
    global $polaris;

    $subrecordArray = CollectAndStripSubrecords($recordArray);

    if ($plrdatabase === false) {
        $plrdatabase = new base_plrDatabase($polaris);
        $plrdatabase->LoadRecordHashed($recordArray['_hdnDatabase']);
        $plrdatabase->connectUserDB();
    }

    // Important part of Polaris for always storing a client-wide datascope to a record
    // Also, if a certain column is not accessible to the frontend, it will be added to the record before saving anyway
    $_datascopes = $polaris->plrClient->GetDataScope($plrdatabase->record->DATABASEID, $recordArray['_hdnTable'], onlystoragescope:true);
    $recordArray = AddDataScopeValuesToRecord($_datascopes, $recordArray);

    // $_POST['_hdnShowsql'] = true;
    if (isset($_POST['_hdnShowsql']) and ($_POST['_hdnShowsql'] == true) or $debug) {
        $plrdatabase->userdb->debug = true;
        var_dump($recordArray);
        // var_dump($subrecordArray);
    }
    $autosupervalues = false;
    if (isset($recordArray['_hdnRecordID']) and is_array($recordArray['_hdnRecordID'])) {
        // array situation where multiple records get posted
        $fields = StripHiddenFields($recordArray);
        $_index = 0;
        foreach($recordArray['_hdnRecordID'] as $_hdnRecordID) {
            unset($oneRecord);
            // convert url-safe ROWID back to real Oracle ROWID
            $oneRecord['_hdnRecordID'] = $plrdatabase->customUrlDecode($_hdnRecordID);
            $oneRecord['_hdnDatabase'] = $recordArray['_hdnDatabase'];
            $oneRecord['_hdnState']    = ($recordArray['_hdnState'] != '') ? $recordArray['_hdnState'] : 'edit';
            $oneRecord['_hdnTable']    = $recordArray['_hdnTable'];
            foreach($fields as $field => $fieldvalue) {
                $oneRecord[$field] = $fieldvalue[$_index];
            }
            $_index++;
            return __saveRecord($plrdatabase, $oneRecord, $autosupervalues);
        }
    } else {
        // normal situation where one record gets posted
        if ($autosupervalues) {
            fillRecordWithAutoValues($recordArray, $autosupervalues);
        }

        // convert url-safe ROWID back to real Oracle ROWID
        if (isset($recordArray['_hdnRecordID']))
            $recordArray['_hdnRecordID'] = $plrdatabase->customUrlDecode($recordArray['_hdnRecordID']);
        else
            $recordArray['_hdnRecordID'] = false;

        // save files, if there are any
        __saveFiles($recordArray);

        $autoincrement = __saveRecord($plrdatabase, $recordArray, $autosupervalues);
        // post all the subrecords in the form ALWAYS after the mainrecord (to respect the ref.ints)
        if ($subrecordArray and count($subrecordArray) > 0) {
            $autosupervalues = saveSubRecords($plrdatabase, $subrecordArray, $autosupervalues, $recordArray, $autoincrement);
        }
    }

    if ((isset($_POST['_hdnShowsql']) and $_POST['_hdnShowsql'] == true) or $debug) {
        $plrdatabase->userdb->debug=false;
//        ob_get_clean();
    }
    return $autoincrement;
}

function saveSubRecords(&$plrdatabase, $subrecordArray, $supervalues, $recordArray, $autoincrement=false) {
    $oneRecordBase['_hdnState'] = 'replace';
    $oneRecordBase['_hdnDatabase'] = $subrecordArray['_hdnDatabase'];

    if (is_array($supervalues))
        $superkeys = array_keys($supervalues);

    $_subrecordfields = StripHiddenFields($subrecordArray);
    foreach ($_subrecordfields as $table => $fields) {
        //    $table = strtolower($table);
        if (is_array($fields)) {
            $oneRecordBase['_hdnTable'] = $table; //strtolower($table);
            $oneRecordBase['_hdnState'] = $fields['_hdnState'];
            $keys = $plrdatabase->userdb->MetaPrimaryKeys($table);
            $keys = array_toupper($keys);
            $fieldsarray = array_keys($fields);
            $firstfield = $fieldsarray[0];
            for ($i = 0; $i < count($fields[$firstfield]); $i++) {
                $oneRecord = $oneRecordBase;
                foreach ($fields as $field => $value) {
                    if (is_array($value)) {
                        if ($supervalues and $value[$i] == '') {
                            if (isset($superkeys)) {
                                foreach ($superkeys as $superkey) {
                                    if ($field == $superkey)
                                        $oneRecord[$field] = $supervalues[$superkey];
                                }
                            }
                        } elseif (strpos($value[$i], '__parentvalue__') === 0) {
                            $parentValueParts = explode(':', $value[$i]);
                            $parentField = $field;

                            // Check if a specific parent field is specified
                            if (count($parentValueParts) > 1) {
                                $parentField = trim($parentValueParts[1]);
                            }

                            if (isset($recordArray[$parentField]) && $recordArray[$parentField] == '__auto_increment__') {
                                $oneRecord[$field] = $supervalues[$parentField];
                            } else {
                                if (!isset($recordArray[$parentField]) && $autoincrement) {
                                    $oneRecord[$field] = $autoincrement;
                                } else {
                                    $oneRecord[$field] = $recordArray[$parentField];
                                }
                            }
                        } else {
                            $oneRecord[$field] = $value[$i];
                        }
                    } else {
                        $oneRecord[$field] = $value;
                    }
                }

                switch ($oneRecord['_hdnState']) {
                    case 'delete':
                        $recordid = $oneRecord['_hdnRecordID'];
                        $keyfieldselect = $plrdatabase->makeEncodedKeySelect($table, $recordid);
                        $plrdatabase->deleteRecord($table, $keyfieldselect);
                        break;
                    case 'insert':
                    case 'edit':
                    case 'update':
                        $skip_empty_fields = isset($oneRecord['_hdnSkipEmptyFields']) && $oneRecord['_hdnSkipEmptyFields'];
                        $insert_ID = __saveRecord($plrdatabase, $oneRecord, $supervalues, $skip_empty_fields);

                        /**
                         * Store the generated values from the record, so we can use it
                         * for the subrecords being inserted
                         */
                        if ($insert_ID and $supervalues) {
                            if (is_array($value) and isset($superkeys)) {
                                if ($supervalues[$superkeys[0]] == '')
                                    $supervalues[$superkeys[0]] = $insert_ID;
                            } else {
                                if (($supervalues[$keys[0]] ?? false) == '')
                                    $supervalues[$keys[0]] = $insert_ID;
                            }
                            $insert_ID = false;
                        }
                        break;
                }

            }
        }
    }

    return $supervalues;
}

function SaveResources($RecordArray) {
	global $polaris, $_sqlDeleteResource, $_sqlInsertNewResource, $_sqlInsertResource;

	$databaseid = $RecordArray['_hdnDatabase'];
    $plrdatabase = new plrDatabase($polaris);
    $plrdatabase->LoadRecordHashed($databaseid);
    $plrdatabase->connectUserDB();

	$result = $RecordArray;
	foreach($result as $field => $resourceid) {
		if (substr($field, -5) == '_rsc_') {
			if ($plrdatabase->userdb) {
				/***
				* if there is no resourceid then insert a new resource and catch the
				* lastinsertindex
				*/
				$idx=0;
				if ($resourceid == '') {
					$plrdatabase->userdb->Execute($_sqlInsertNewResource, array($result[$field.'lang'][$idx], $result[$field.'value'][$idx]));
					$resourceid = $plrdatabase->userdb->Insert_ID();
					$idx++;
				} else {
					$plrdatabase->userdb->Execute($_sqlDeleteResource, array($resourceid));
				}

				while($idx < count($result[$field.'value']) ) {
					$plrdatabase->userdb->Execute($_sqlInsertResource, array($resourceid, $result[$field.'lang'][$idx], $result[$field.'value'][$idx]));
					$idx++;
				}
			}
			unset($result[$field]);
			unset($result[$field.'lang']);
			unset($result[$field.'value']);
		}
	}
	return $result;
}
