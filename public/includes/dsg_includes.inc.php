<?php
/**
 * Set PLR_DIR to the directory where the root of Polaris resides...
 */
if (!defined('PLR_DIR')) define('PLR_DIR', dirname(dirname(__FILE__)));

/**
 * Set the class autoloader, so it finds the Polaris classes in the right folder
 */
spl_autoload_register(function($class) {
    $class = str_replace('_', DIRECTORY_SEPARATOR, $class);
    // get full name of file containing the required class
    $file = PLR_DIR.'/classes/'.$class.'.class.php';
    // get file if it is readable
    if (is_readable($file)) {
        require $file;
    }
});

// require_once PLR_DIR."/vendor/autoload.php";
// require_once PLR_DIR."/includes/global.inc.php";
// // require_once PLR_DIR."/includes/constants.inc.php";
// require_once PLR_DIR."/classes/temp_engine/dsg_smarty.class.php";
// require_once PLR_DIR."/classes/temp_engine/app_smarty.class.php";
// require_once PLR_DIR."/sql/mysql-lang.inc.php";
// require_once PLR_DIR."/includes/basefunctions.inc.php";
// require_once PLR_DIR."/includes/dbroutines.inc.php";
// require_once PLR_DIR."/languages/lang_api.php";


// require_once('global.inc.php');
// require_once('sql/mysql-lang.inc.php');
// require_once('setlanguage.inc.php');
// //	if (!$_CONFIG['debugmode']) require_once('error_handler.inc.php');
// require_once('languages/lang_api.php');
// lang_load($_GVARS['lang']);
// require_once('basefunctions.inc.php');
// require_once('dbroutines.inc.php');
// require_once('classes/base/polaris.class.php');
// require_once('dsg_breadcrums.inc.php');
// require_once('dsg_actions.inc.php');
// require_once('classes/temp_engine/dsg_smarty.class.php');
