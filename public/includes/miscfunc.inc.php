<?php

function format_uuidv4($data) {
  assert(strlen($data) == 16);

  $data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0100
  $data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10

  return vsprintf('%s%s%s%s%s%s%s%s', str_split(bin2hex($data), 4));
}

function encodeString($string) {
    global $scramble;
    return hashstr("{$scramble}_{$string}");
}

function hashstr($str) {
    return hash(DEFAULT_HASH_FUNCTION, $str);
}

function encodeStringForUrl($string) {
    $string = base64_encode(hex2bin($string));
    $string = strtr($string, '+/=', '-_,'); // Replace URL-unsafe characters
    return $string;
}

function decodeStringForUrl($string) {
    $string = strtr($string, '-_,', '+/='); // Restore URL-safe characters
    $string = bin2hex(base64_decode($string));
    return $string;
}

function abbreviateFilename($filename, $maxLength = 30) {
    if (strlen($filename) <= $maxLength) {
        return $filename;
    }

    $prefixLength = floor(($maxLength - 3) / 2);
    $suffixLength = ceil(($maxLength - 3) / 2);

    $prefix = substr($filename, 0, $prefixLength);
    $suffix = substr($filename, -$suffixLength);

    return $prefix . '&shy;... ' . $suffix;
}

function getHost($url, $accept_www=false){
    $URIs = parse_url(trim($url));
    $host = !empty($URIs['host'])? $URIs['host'] : explode('/', $URIs['path'])[0];
    return $accept_www == false? str_ireplace('www.', '', $host) : $host;
}

function validateEmail($email) {
    return filter_var($email, FILTER_VALIDATE_EMAIL);
}

function search_sort($a, $b) {
    if (get_resource($a['1']) == get_resource($b['1']))
      return 0;
    return (get_resource($a['1']) < get_resource($b['1'])) ? -1 : 1;
}

function getPlatteBox($lat_degrees,$lon_degrees,$distance_in_km) {
    $radius = 6371.0;
    $rlat=$radius*sin(deg2rad(90-$lat_degrees));

    $dphi_lon=asin($distance_in_km/$rlat);
    $dphi_lat=asin($distance_in_km/$radius);

    $lat1=$lat_degrees-rad2deg($dphi_lat);
    $lat2=$lat_degrees+rad2deg($dphi_lat);
    $lon1=$lon_degrees-rad2deg($dphi_lon);
    $lon2=$lon_degrees+rad2deg($dphi_lon);

    return array($lat1,$lat2,$lon1,$lon2);
}

/**
 * Better GI than print_r or var_dump -- but, unlike var_dump, you can only dump one variable.
 * Added htmlentities on the var content before echo, so you see what is really there, and not the mark-up.
 *
 * Also, now the output is encased within a div block that sets the background color, font style, and left-justifies it
 * so it is not at the mercy of ambient styles.
 *
 * Inspired from:     PHP.net Contributions
 * Stolen from:       [highstrike at gmail dot com]
 * Modified by:       stlawson *AT* JoyfulEarthTech *DOT* com
 *
 * @param mixed $var  -- variable to dump
 * @param string $var_name  -- name of variable (optional) -- displayed in printout making it easier to sort out what variable is what in a complex output
 * @param string $indent -- used by internal recursive call (no known external value)
 * @param unknown_type $reference -- used by internal recursive call (no known external value)
 */
function do_dump($var, $var_name = NULL, $indent = NULL, $reference = NULL)
{
    $do_dump_indent = "<span style='color:#666666;'>|</span> &nbsp;&nbsp; ";
    $reference = $reference.$var_name;
    $keyvar = 'the_do_dump_recursion_protection_scheme'; $keyname = 'referenced_object_name';

    // So this is always visible and always left justified and readable
    echo "<div style='text-align:left; background-color:white; font: 100% monospace; color:black;'>";

    if (is_array($var) && isset($var[$keyvar]))
    {
        $real_var = &$var[$keyvar];
        $real_name = &$var[$keyname];
        $type = ucfirst(gettype($real_var));
        echo "$indent$var_name <span style='color:#666666'>$type</span> = <span style='color:#e87800;'>&amp;$real_name</span><br>";
    }
    else
    {
        $var = array($keyvar => $var, $keyname => $reference);
        $avar = &$var[$keyvar];

        $type = ucfirst(gettype($avar));
        if($type == "String") $type_color = "<span style='color:green'>";
        elseif($type == "Integer") $type_color = "<span style='color:red'>";
        elseif($type == "Double"){ $type_color = "<span style='color:#0099c5'>"; $type = "Float"; }
        elseif($type == "Boolean") $type_color = "<span style='color:#92008d'>";
        elseif($type == "NULL") $type_color = "<span style='color:black'>";

        if(is_array($avar))
        {
            $count = count($avar);
            echo "$indent" . ($var_name ? "$var_name => ":"") . "<span style='color:#666666'>$type ($count)</span><br>$indent(<br>";
            $keys = array_keys($avar);
            foreach($keys as $name)
            {
                $value = &$avar[$name];
                do_dump($value, "['$name']", $indent.$do_dump_indent, $reference);
            }
            echo "$indent)<br>";
        }
        elseif(is_object($avar))
        {
            echo "$indent$var_name <span style='color:#666666'>$type</span><br>$indent(<br>";
            foreach($avar as $name=>$value) do_dump($value, "$name", $indent.$do_dump_indent, $reference);
            echo "$indent)<br>";
        }
        elseif(is_int($avar)) echo "$indent$var_name = <span style='color:#666666'>$type(".strlen($avar).")</span> $type_color".htmlentities($avar)."</span><br>";
        elseif(is_string($avar)) echo "$indent$var_name = <span style='color:#666666'>$type(".strlen($avar).")</span> $type_color\"".htmlentities($avar)."\"</span><br>";
        elseif(is_float($avar)) echo "$indent$var_name = <span style='color:#666666'>$type(".strlen($avar).")</span> $type_color".htmlentities($avar)."</span><br>";
        elseif(is_bool($avar)) echo "$indent$var_name = <span style='color:#666666'>$type(".strlen($avar).")</span> $type_color".($avar == 1 ? "TRUE":"FALSE")."</span><br>";
        elseif(is_null($avar)) echo "$indent$var_name = <span style='color:#666666'>$type(".strlen($avar).")</span> {$type_color}NULL</span><br>";
        else echo "$indent$var_name = <span style='color:#666666'>$type(".strlen($avar).")</span> ".htmlentities($avar)."<br>";

        $var = $var[$keyvar];
    }

    echo "</div>";
}

function utf8json($inArray) {

    static $depth = 0;

    /* our return object */
    $newArray = array();

    /* safety recursion limit */
    $depth ++;
    if($depth >= '3000') {
        return false;
    }

    /* step through inArray */
    foreach($inArray as $key=>$val) {
        if(is_array($val)) {
            /* recurse on array elements */
            $newArray[$key] = utf8json($val);
        } else {
            /* encode string values */
            $newArray[$key] = utf8_encode($val);
        }
    }

    /* return utf8 encoded array */
    return $newArray;
}

function formatBytes($b,$p = null) {
    /**
     *
     * @author Martin Sweeny
     * @version 2010.0617
     *
     * returns formatted number of bytes.
     * two parameters: the bytes and the precision (optional).
     * if no precision is set, function will determine clean
     * result automatically.
     *
     **/
    $units = array("B","kB","MB","GB","TB","PB","EB","ZB","YB");
    $c=0;
    if(!$p && $p !== 0) {
        foreach($units as $k => $u) {
            if(($b / pow(1024,$k)) >= 1) {
                $r["bytes"] = $b / pow(1024,$k);
                $r["units"] = $u;
                $c++;
            }
        }
        return number_format($r["bytes"],2) . " " . $r["units"];
    } else {
        return number_format($b / pow(1024,$p)) . " " . $units[$p];
    }
}

if ( !function_exists('sys_get_temp_dir')) {
  function sys_get_temp_dir() {
      if( $temp=getenv('TMP') )        return $temp;
      if( $temp=getenv('TEMP') )        return $temp;
      if( $temp=getenv('TMPDIR') )    return $temp;
      $temp=tempnam(__FILE__,'');
      if (file_exists($temp)) {
          unlink($temp);
          return dirname($temp);
      }
      return null;
  }
}

function nameize($str,$a_char = array("'","-"," ")){
    //$str contains the complete raw name string
    //$a_char is an array containing the characters we use as separators for capitalization. If you don't pass anything, there are three in there as default.
    $string = mb_strtolower($str, 'UTF-8');
    foreach ($a_char as $temp) {
        $pos = strpos($string, $temp);
        if ($pos) {
            //we are in the loop because we found one of the special characters in the array, so lets split it up into chunks and capitalize each one.
            $mend = '';
            $a_split = explode($temp, $string);
            foreach ($a_split as $temp2) {
                //capitalize each portion of the string which was separated at a special character
                $mend .= ucfirst($temp2).$temp;
            }
            $string = substr($mend,0,-1);
        }
    }
    return ucfirst($string);
}

function sanitizeClassName($input) {
    // Remove any characters that are not allowed in CSS class names
    $sanitized = preg_replace('/[^a-zA-Z0-9_-]/', '', $input);

    // Ensure the class name starts with a letter
    $sanitized = preg_replace('/^[^a-zA-Z]+/', '', $sanitized);

    return $sanitized;
}

function sanitize_filename($filename, $forceextension="")
{
/*
1. Remove leading and trailing dots
2. Remove dodgy characters from filename, including spaces and dots except last.
3. Force extension if specified
*/

$defaultfilename = "none";
$dodgychars = "[^0-9a-zA-z()_-]"; // allow only alphanumeric, underscore, parentheses and hyphen

$filename = preg_replace("/^[.]*/","",$filename); // lose any leading dots
$filename = preg_replace("/[.]*$/","",$filename); // lose any trailing dots
$filename = $filename?$filename:$defaultfilename; // if filename is blank, provide default

$lastdotpos=strrpos($filename, "."); // save last dot position
$filename = preg_replace("/$dodgychars/","_",$filename); // replace dodgy characters
$afterdot = "";
if ($lastdotpos !== false) { // Split into name and extension, if any.
$beforedot = substr($filename, 0, $lastdotpos);
if ($lastdotpos < (strlen($filename) - 1))
$afterdot = substr($filename, $lastdotpos + 1);
}
else // no extension
$beforedot = $filename;

if ($forceextension)
$filename = $beforedot . "." . $forceextension;
elseif ($afterdot)
$filename = $beforedot . "." . $afterdot;
else
$filename = $beforedot;

return $filename;
}

function php_combined_lcg() {
    $tv = gettimeofday();
    $lcg['s1'] = $tv['sec'] ^ (~$tv['usec']);
    $tv = gettimeofday();
    $lcg['s2'] = $tv['usec'];

    $q = (int) ($lcg['s1'] / 53668);
    $lcg['s1'] = (int) (40014 * ($lcg['s1'] - 53668 * $q) - 12211 * $q);
    if ($lcg['s1'] < 0)
        $lcg['s1'] += 2147483563;

    $q = (int) ($lcg['s2'] / 52774);
    $lcg['s2'] = (int) (40692 * ($lcg['s2'] - 52774 * $q) - 3791 * $q);
    if ($lcg['s2'] < 0)
        $lcg['s2'] += 2147483399;

    $z = (int) ($lcg['s1'] - $lcg['s2']);
    if ($z < 1) {
        $z += 2147483562;
    }

    return $z * 4.656613e-10;
}

if (!function_exists('money_format')) {
function money_format($format, $number) {
    $regex  = '/%((?:[\^!\-]|\+|\(|\=.)*)([0-9]+)?'.
              '(?:#([0-9]+))?(?:\.([0-9]+))?([in%])/';
    if (setlocale(LC_MONETARY, 0) == 'C') {
        setlocale(LC_MONETARY, '');
    }
    $locale = localeconv();
    preg_match_all($regex, $format, $matches, PREG_SET_ORDER);
    foreach ($matches as $fmatch) {
        $value = floatval($number);
        $flags = array(
            'fillchar'  => preg_match('/\=(.)/', $fmatch[1], $match) ?
                           $match[1] : ' ',
            'nogroup'   => preg_match('/\^/', $fmatch[1]) > 0,
            'usesignal' => preg_match('/\+|\(/', $fmatch[1], $match) ?
                           $match[0] : '+',
            'nosimbol'  => preg_match('/\!/', $fmatch[1]) > 0,
            'isleft'    => preg_match('/\-/', $fmatch[1]) > 0
        );
        $width      = trim($fmatch[2]) ? (int)$fmatch[2] : 0;
        $left       = trim($fmatch[3]) ? (int)$fmatch[3] : 0;
        $right      = trim($fmatch[4]) ? (int)$fmatch[4] : $locale['int_frac_digits'];
        $conversion = $fmatch[5];

        $positive = true;
        if ($value < 0) {
            $positive = false;
            $value  *= -1;
        }
        $letter = $positive ? 'p' : 'n';

        $prefix = $suffix = $cprefix = $csuffix = $signal = '';

        $signal = $positive ? $locale['positive_sign'] : $locale['negative_sign'];
        switch (true) {
            case $locale["{$letter}_sign_posn"] == 1 && $flags['usesignal'] == '+':
                $prefix = $signal;
                break;
            case $locale["{$letter}_sign_posn"] == 2 && $flags['usesignal'] == '+':
                $suffix = $signal;
                break;
            case $locale["{$letter}_sign_posn"] == 3 && $flags['usesignal'] == '+':
                $cprefix = $signal;
                break;
            case $locale["{$letter}_sign_posn"] == 4 && $flags['usesignal'] == '+':
                $csuffix = $signal;
                break;
            case $flags['usesignal'] == '(':
            case $locale["{$letter}_sign_posn"] == 0:
                $prefix = '(';
                $suffix = ')';
                break;
        }
        if (!$flags['nosimbol']) {
            $currency = $cprefix .
                        ($conversion == 'i' ? $locale['int_curr_symbol'] : $locale['currency_symbol']) .
                        $csuffix;
        } else {
            $currency = '';
        }
        $space  = $locale["{$letter}_sep_by_space"] ? ' ' : '';

        $value = number_format($value, $right, $locale['mon_decimal_point'],
                 $flags['nogroup'] ? '' : $locale['mon_thousands_sep']);
        $value = @explode($locale['mon_decimal_point'], $value);

        $n = strlen($prefix) + strlen($currency) + strlen($value[0]);
        if ($left > 0 && $left > $n) {
            $value[0] = str_repeat($flags['fillchar'], $left - $n) . $value[0];
        }
        $value = implode($locale['mon_decimal_point'], $value);
        if ($locale["{$letter}_cs_precedes"]) {
            $value = $prefix . $currency . $space . $value . $suffix;
        } else {
            $value = $prefix . $value . $space . $currency . $suffix;
        }
        if ($width > 0) {
            $value = str_pad($value, $width, $flags['fillchar'], $flags['isleft'] ?
                     STR_PAD_RIGHT : STR_PAD_LEFT);
        }

        $format = str_replace($fmatch[0], $value, $format);
    }
    return $format;
}
}

define('DEFAULT_CHARSET', 'ISO-8859-1');

function json_safe_encode($var)
{
    return json_encode(json_fix_cyr($var));
}

function json_fix_cyr($var)
{
    if (is_array($var)) {
        $new = array();
        foreach ($var as $k => $v) {
            $new[json_fix_cyr($k)] = json_fix_cyr($v);
        }
        $var = $new;
    } elseif (is_object($var)) {
        $vars = get_class_vars(get_class($var));
        foreach ($vars as $m => $v) {
            $var->$m = json_fix_cyr($v);
        }
    } elseif (is_string($var)) {
        $var = iconv(DEFAULT_CHARSET, 'utf-8', $var);
    }
    return $var;
}

/**
 * Create Unique Arrays using an md5 hash
 *
 * @param array $array
 * @return array
 */
function arrayUnique($array, $preserveKeys = false)
{
    // Unique Array for return
    $arrayRewrite = array();
    // Array with the md5 hashes
    $arrayHashes = array();
    foreach($array as $key => $item) {
        // Serialize the current element and create a md5 hash
        $hash = md5(serialize($item));
        // If the md5 didn't come up yet, add the element to
        // to arrayRewrite, otherwise drop it
        if (!isset($arrayHashes[$hash])) {
            // Save the current element hash
            $arrayHashes[$hash] = $hash;
            // Add element to the unique Array
            if ($preserveKeys) {
                $arrayRewrite[$key] = $item;
            } else {
                $arrayRewrite[] = $item;
            }
        }
    }
    return $arrayRewrite;
}


function arrayToObject($data) {
    $object = (object) $data;
    foreach ($object as $property) {
        if (is_array($property)) arrayToObject($property);
    }
    return $object;
}

function debug() {
   $debug_array = debug_backtrace();
   $counter = count($debug_array);
   for($tmp_counter = 0; $tmp_counter != $counter; ++$tmp_counter)
   {
    ?>
    <table width="558" height="116" border="1" cellpadding="0" cellspacing="0" bordercolor="#000000">
     <tr>
      <td height="38" bgcolor="#D6D7FC"><font color="#000000">function <font color="#FF3300"><?
      echo($debug_array[$tmp_counter]["function"]);?>(</font> <font color="#2020F0"><?
      //count how many args a there
      $args_counter = count($debug_array[$tmp_counter]["args"]);
      //print them
      for($tmp_args_counter = 0; $tmp_args_counter != $args_counter; ++$tmp_args_counter)
      {
        if (is_array($debug_array[$tmp_counter]["args"][$tmp_args_counter])) {
            print_r($debug_array[$tmp_counter]["args"][$tmp_args_counter]);
        } else {
            echo($debug_array[$tmp_counter]["args"][$tmp_args_counter]);
        }

        if (($tmp_args_counter + 1) != $args_counter)
            echo(", ");
        else
            echo(" ");
      }
      ?></font><font color="#FF3300">)</font></font></td>
     </tr>
     <tr>
      <td bgcolor="#5F72FA"><font color="#FFFFFF">{</font><br>
       <font color="#FFFFFF">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;file: <?
       echo($debug_array[$tmp_counter]["file"]);?></font><br>
       <font color="#FFFFFF">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;line: <?
       echo($debug_array[$tmp_counter]["line"]);?></font><br>
       <font color="#FFFFFF">}</font></td>
     </tr>
    </table>
    <?php
    if(($tmp_counter + 1) != $counter)
    {
     echo("<br>was called by:<br>");
    }
   }
   exit();
}

function safeSetCookie($cName, $cValue) {
//    $cookieExpire = time() + (60*60*24); // 1 day ("= 0" for Session)
//    $cookieExpire = null; // ends when browser closes
//    $safecookiehost = preg_replace('/^[Ww][Ww][Ww]\./', '', preg_replace('/:[0-9]*$/', '', $_SERVER['HTTP_HOST']));

    if (strcmp($cValue, "") == 0) {
        setcookie($cName, "", 1) ;//, "/", "$safecookiehost", FALSE);  // Delete
    } else {
        setcookie($cName, $cValue); //, $cookieExpire, "/", "$safecookiehost", FALSE);
        $_COOKIE[$cName] = $cValue;    // Pretend it's already set
    }
}

function randomString($len) {
    srand(date("s"));
    $possible="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890!@#$%";
    $str="";
    while(strlen($str)<$len) {
        $str.=substr($possible,(rand()%(strlen($possible))),1);
    }
    return($str);
}

function _sortcmp($a, $b) {
	return strcmp($a['nr'], $b['nr']);
}

/**
 * Replace strripos()
 *
 * @category    PHP
 * @package     PHP_Compat
 * @link        http://php.net/function.strripos
 * @author      Aidan Lister <aidan@php.net>
 * @version     $Revision: 1.24 $
 * @since       PHP 5
 * @require     PHP 4.0.0 (user_error)
 */
if (!function_exists('strripos')) {
    function strripos($haystack, $needle, $offset = null)
    {
        // Sanity check
        if (!is_scalar($haystack)) {
            user_error('strripos() expects parameter 1 to be scalar, ' .
                gettype($haystack) . ' given', E_USER_WARNING);
            return false;
        }

        if (!is_scalar($needle)) {
            user_error('strripos() expects parameter 2 to be scalar, ' .
                gettype($needle) . ' given', E_USER_WARNING);
            return false;
        }

        if (!is_int($offset) && !is_bool($offset) && !is_null($offset)) {
            user_error('strripos() expects parameter 3 to be long, ' .
                gettype($offset) . ' given', E_USER_WARNING);
            return false;
        }

        // Initialize variables
        $needle         = strtolower($needle);
        $haystack       = strtolower($haystack);
        $needle_fc      = $needle[0];
        $needle_len     = strlen($needle);
        $haystack_len   = strlen($haystack);
        $offset         = (int) $offset;
        $leftlimit      = ($offset >= 0) ? $offset : 0;
        $p              = ($offset >= 0) ?
                                $haystack_len :
                                $haystack_len + $offset + 1;

        // Reverse iterate haystack
        while (--$p >= $leftlimit) {
            if ($needle_fc === $haystack[$p] &&
                substr($haystack, $p, $needle_len) === $needle) {
                return $p;
            }
        }

        return false;
    }
}

function sqlHasWherePart($sql) {
	$frompos = strripos($sql, ' from ');
	$wherepos = strpos(strtolower($sql), 'where ', $frompos);
	return ($wherepos !== false);
}

function sqlCombineWhere($sql, $filter, $keyword=false) {
	if ($filter and $filter != '') {
        $sql = str_replace("\r", " ", $sql);
        $sql = str_replace("\n", " ", $sql);
        $frompos = strripos($sql, ' from ');
        $wherepos = strpos(strtolower($sql), 'where ', $frompos);
        $orderpos = strpos(strtolower($sql), 'order by ', $frompos);
        $grouppos = strpos(strtolower($sql), 'group by ', $frompos);
        $havingpos = strpos(strtolower($sql), 'having ', $frompos);

        $fromhaving = substr($sql, $havingpos);
        $untilhaving = substr($sql, 0, $havingpos);
        $fromgroup = substr($sql, $grouppos);
        $untilgroup = substr($sql, 0, $grouppos);
        $fromorder = substr($sql, $orderpos);
        $untilorder = substr($sql, 0, $orderpos);
        if ($keyword)
            $_wh_keyword = $keyword;
        else
            $_wh_keyword = 'WHERE';

        if ($havingpos !== FALSE) {
            // indien GROUP BY aanwezig
            if ($wherepos === FALSE)
                $sql = "$untilhaving $_wh_keyword $filter $fromhaving";
            else {
                $sql = "$untilhaving AND $filter $fromhaving";
            }
        } elseif ($grouppos !== FALSE) {
            // indien GROUP BY aanwezig
            if ($wherepos === FALSE) {
                if ($_wh_keyword == 'HAVING')
                    $sql = "$untilgroup $fromgroup $_wh_keyword $filter";
                else
                    $sql = "$untilgroup $_wh_keyword $filter $fromgroup";
            } else {
                $sql = "$untilgroup AND $filter $fromgroup";
            }
        } elseif ($orderpos !== FALSE) {
                if ($wherepos === FALSE) {
                    $sql = "$untilorder $_wh_keyword $filter $fromorder";
                } else {
                    $sql = "$untilorder AND $filter $fromorder";
                }
        } else {
            if ($frompos === FALSE) { // no FROM so we add the filter cause without the WHERE
                if ($sql == '')
                    $sql = "$filter";
                else
                    $sql = "$sql AND $filter";
            } else { 									// there is a FROM so we add the filter cause with the WHERE
                if ($wherepos === FALSE)
                    $sql = "$sql $_wh_keyword $filter";
                else
                    $sql = "$sql AND $filter";
            }
        }
    }
    return $sql;
}

function RoundToFives($time) {
	$parts = explode(':', $time);
	$hour = intval($parts[0]);
	$mins = $parts[1];
	$minslast = $mins[1];
	switch ($minslast) {
	case "0":case "1":case "2":
		$minsadd = 0;
	break;
	case "3":case "4":case "5":case "6":case "7":
		$minsadd = 5;
	break;
	case "8":case "9":
		$minsadd = 10;
	break;
	}
	$minvalue = intval($mins[0]) * 10 + $minsadd;

	if ($minvalue >= 60) {
		$hour++;
		$minvalue = 0;
	}

	$mins = substr('00'.$minvalue, -2);
	$hours = substr('00'.$hour, -2);
	return "$hour:$mins";
}

if (!function_exists("strrpos")) {
function strrpos($haystack, $needle) {
   $index = strpos(strrev($haystack), strrev($needle));
   if($index === false) {
       return false;
   }
   $index = strlen($haystack) - strlen($needle) - $index;
   return $index;
}}

function extractParams($parameters) {
  $result = '';

  if (isset($parameters)) {
      $tmparray = explode(';', $parameters);
      foreach($tmparray as $param) {
        $tmpvalues = explode('=', $param);
        $result[$tmpvalues[0]] = $tmpvalues[1];
      }
  }
  return $result;
}

function implode_r ($glue, $pieces){
  $out = "";
  foreach ($pieces as $piece)
    if (is_array ($piece)) $out .= implode_r ($glue, $piece); // recurse
    else                   $out .= $glue.$piece;

  return $out;
}

function str_format($format, $value, $sep) {
	if ($value != '') {
		$array = sscanf($value, $format);
		return implode( $sep, $array );
	} else {
		return $value;
	}
}

if ( !function_exists('array_combine') ) {
  function array_combine( $keys, $vals ) {
   $keys = array_values( (array) $keys );
   $vals = array_values( (array) $vals );
   $n = max( count( $keys ), count( $vals ) );
   $r = array();
   for( $i=0; $i<$n; $i++ ) {
    $r[ $keys[ $i ] ] = $vals[ $i ];
   }
   return $r;
  }
}

function match_ipaddress($haystack, $needle) {
   $bFound = FALSE;
   foreach ($needle as $value) {
       $pos = strpos($haystack, $value);
       if ($pos !== false and $pos == 0) {
           $bFound = TRUE;
       }
   }
   return $bFound;
}

function array_toupper ($array) {
  if (is_array($array))
    array_walk($array, function($elem) { $elem = strtoupper($elem); });
    // array_walk($array, create_function('&$elem','$elem = strtoupper($elem);'));
  return $array;
}

function in_array_cin($arItems, $strItem) {
   $bFound = FALSE;
   foreach ($arItems as $strValue) {
       if (strtoupper($strItem) == strtoupper($strValue)) {
           $bFound = TRUE;
       }
   }
   return $bFound;
}

function array_exists($haystack, $needle) {
	$array = explode(',',$haystack);
	return array_search($needle, $array) !== FALSE;
}

function in_array_recursive($needle, $haystack, $alsokeys=false)
    {
        if(!is_array($haystack)) return false;
        if(in_array($needle, $haystack) || ($alsokeys && in_array($needle, array_keys($haystack)) )) return true;
        else {
            foreach($haystack AS $element) {
                $ret = in_array_recursive($needle, $element, $alsokeys);
            }
        }

        return $ret;
    }


function MakeSQLSelect($tableorsql, $distinct=false) {
  global $_GVARS;

  if ($tableorsql != '') {
  	if (substr($tableorsql, 0, 5) == 'SQL::') {
  	  $sql = substr($tableorsql, 5, 999);
  	  $sql = str_replace(':language', "'{$_GVARS['lang']}'", $sql);
      foreach ($_GET as $key => $waarde) {
        $sql = str_replace(":$key", "$waarde", $sql);
      }
  	}
  	else {
      if ($distinct)
        $distinctstr = 'DISTINCT';
      else
        $distinctstr = '';
  	  $sql = "SELECT $distinctstr * FROM $tableorsql";
    }
  }
  return $sql;
}

function keep_previous_post_values($fields) {

  if (is_array($fields)) {
    while (list ($key, $val) = each ($fields)) {
      $return[$key] = $val;
    }
    return $return;
  } else {
    return $_POST;
  }
}

/**
 * get the right language part from a resource string
 *
 */
function get_resource($resource, $plang=false) {
    $result = $resource;

    if ($plang == false) $plang = $_SESSION['lang'];
    if ($resource != '') {
        $result = '';
        $languages = explode('|', $resource); // separate all the languages in the string
        foreach ($languages as $language) {
            if (substr($language, 3) == FALSE) {
                $result = substr($languages[0], 3);
                if (!$result)
                    $result = $language;
            } elseif ( (substr($language, 0, 3) == $plang.':') or (substr($language, 0, 3) == 'AA:') ) {
                $result = substr($language, 3); // remove AA: from AA:bla;blas
            } elseif (substr($language, 2, 1) != ':') {
                $result = $language;
            }
        }
        if ($result == '')
            $result = $language;//'&lt;'.$language.' resource not found in \''.$resource.'\'&gt;';
        elseif (substr($result, 2, 1) == ':')
            $result = substr($language, 3);
    }
    return $result;
}

function getFragmentFromQuery($query) {
    /***
        transforms .....?var1=a&var2=b&_fragment=1002
        to         .....?var1=a&var2=b#1002
        This is done because fragments are not posted back to the server.
        In this way you can tell the server to which fragment it should 'navigate'
    **/

    $parts_arr = parse_url($query);
    if (isset($parts_arr['query'])) {
        if (!strcmp($parts_arr['query'], '')) return $query;
        $parts = explode('&',$parts_arr['query']);
        foreach($parts as $part) {
            $var = explode('=',$part);
            if ($var[0] == '_fragment')
                $fragment = $var[1];
        }
        $query = remove_query_arg($query,'_fragment');
        if ($fragment)
            $query .= '#'.$fragment;
    }
    return $query;
}

/**
 * Rebuilds a query which was parsed previously
 *
 */
function unparse_url($parts_arr) {
  $ret_url = '';
  if (isset($parts_arr['scheme']) and strcmp($parts_arr['scheme'], '') != 0) {
   $ret_url .= $parts_arr['scheme'] . '://';
  }
  if (isset($parts_arr['user']))
    $ret_url .= $parts_arr['user'];
  if (isset($parts_arr['pass']) and strcmp($parts_arr['pass'], '') != 0) {
   $ret_url .= ':' . $parts_arr['pass'];
  }
  if ((isset($parts_arr['user']) and strcmp($parts_arr['user'], '') != 0) || (isset($parts_arr['pass']) and strcmp($parts_arr['pass'], '') != 0)) {
   $ret_url .= '@';
  }
  if (isset($parts_arr['host']))
    $ret_url .= $parts_arr['host'];
  if (isset($parts_arr['port']) and strcmp($parts_arr['port'], '') != 0) {
   $ret_url .= ':' . $parts_arr['port'];
  }
  if (isset($parts_arr['path']))
    $ret_url .= $parts_arr['path'];
  if (isset($parts_arr['query']) and strcmp($parts_arr['query'], '') != 0) {
   $ret_url .= '?' . $parts_arr['query'];
  }
  if (isset($parts_arr['fragment']) and strcmp($parts_arr['fragment'], '') != 0) {
   $ret_url .= '#' . $parts_arr['fragment'];
  }

  return $ret_url;
}

/**
 * Adds an argument of a query string
 *
 * takes ? or & into account
 */
function add_query_arg($url, $arg, $val) {
//    $url = remove_query_arg($url, $arg);

    $parts_arr = parse_url($url);

    if (isset($parts_arr['query'])) {
        if (strcmp($parts_arr['query'], '') != 0) {
            $parts_arr['query'] .= '&';
        }
    } else {
        $parts_arr['query'] = '';
    }
    if (is_array($arg)) {
        foreach( $arg as $k => $a )
            $parts_arr['query'] .= $arg[$k] . '=' . $val[$k] .'&';
    } else {
      $parts_arr['query'] .= $arg . '=' . $val;
    }

    return unparse_url($parts_arr);
}

/**
 * Strips an argument of a query string
 *
 */
function remove_query_arg($url, $arg) {
  $parts_arr = parse_url($url);
  if (isset($parts_arr['query'])) {
    if (!strcmp($parts_arr['query'], '')) return $url;

    $query_arr = explode('&', $parts_arr['query']);
    foreach ($query_arr as $k => $v) {
     if ((preg_match('/^'.$arg.'=/', $v)) || (preg_match('/^'.$arg.'$/', $v))) {
       unset($query_arr[$k]);
     }
    }
    $parts_arr['query'] = implode('&', $query_arr);
    return unparse_url($parts_arr);
  } else {
    return $url;
  }
}

/**
 * Strips a string of any tag
 *
 * What it does is to extract the content of all tags "$tag"
 * in string "$string". When "$mode" is 1 it returns the
 * content as an array, otherwise as a string.
 */
function untag($string,$tag,$mode=0){
    $tmpval="";
    $preg="/<".$tag.">(.*?)</".$tag.">/si";
    preg_match_all($preg,$string,$tags);
    foreach ($tags[1] as $tmpcont){
        if ($mode==1){$tmpval[]=$tmpcont;}
        else {$tmpval.=$tmpcont;}
        }
    return $tmpval;
}

/**
 * Writes a string to a file
 *
 * When the function 'file_put_contents' does not exists because
 * PHP < 5 is used, this will create a similar function
 * This function writes a given string ($data) to a file ($filename)
 */
if (!function_exists('file_put_contents')) {
   function file_put_contents($filename, $data)
   {
       if (($h = @fopen($filename, 'w')) === false) {
           return false;
       }
       if (($bytes = @fwrite($h, $data)) === false) {
           return false;
       }
       fclose($h);
       return $bytes;
   }
}

/**
 * Print the headers to cause the page to redirect to $p_url
 * If $p_die is true (default), terminate the execution of the script
 * immediately
 * If we have handled any errors on this page and the 'stop_on_errors' config
 * option is turned on, return false and don't redirect.
 */
function header_redirect( $p_url=false, $p_die=true ) {
	global $protocol;
	if ($p_url == false)
		$p_url = $protocol.'://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];

  header( 'Content-Type: text/html' );
  header( 'Pragma: no-cache' );
  header( 'Expires: Fri, 01 Jan 1999 00:00:00 GMT' );
  header( 'Cache-control: no-cache, no-cache="Set-Cookie", private' );
//  	header( "Refresh: 0;url=$p_url" );
 	header( 'Location: '.$p_url );

  if ( $p_die ) {
  	die; /* additional output can cause problems so let's just stop output here */
  }

  return true;
}

function chkgd2(){
  $testGD = get_extension_funcs("gd"); // Grab function list
  if (!$testGD){ echo "GD not even installed."; exit; }
  if (in_array ("imagegd2",$testGD)) $gd_version = "<2"; // Check
  if ($gd_version == "<2") return false; else return true;
}

/*
	Function createthumb($name,$filename,$new_w,$new_h)
	creates a resized image
	variables:
	$name		Original filename
	$filename	Filename of the resized image
	$new_w		width of resized image
	$new_h		height of resized image
*/
function createthumb($name,$filename,$new_w,$new_h){
	$GD2Exists = chkgd2();

//	global $gd2;
//	$gd2 = 'ja';
	$ext = strtolower(substr($filename, -3));
	if (preg_match("/jpg|jpeg/",$ext)){$src_img=imagecreatefromjpeg($name);}
	if (preg_match("/png/",$ext)){$src_img=imagecreatefrompng($name);}
	if (preg_match("/gif/",$ext)){$src_img=imagecreatefromgif($name);}
	if ($src_img) {
   	$old_x=imageSX($src_img);
   	$old_y=imageSY($src_img);
   	if ($old_x > $old_y) {
   		$thumb_w=$new_w;
   		$thumb_h=$old_y*($new_h/$old_x);
   	}
   	if ($old_x < $old_y) {
   		$thumb_w=$old_x*($new_w/$old_y);
   		$thumb_h=$new_h;
   	}
   	if ($old_x == $old_y) {
   		$thumb_w=$new_w;
   		$thumb_h=$new_h;
   	}
   	if ($GD2Exists){
   		$dst_img=ImageCreate($thumb_w,$thumb_h);
   		imagecopyresized($dst_img,$src_img,0,0,0,0,$thumb_w,$thumb_h,$old_x,$old_y);
   	} else {
   		$dst_img=ImageCreateTrueColor($thumb_w,$thumb_h);
   		imagecopyresampled($dst_img,$src_img,0,0,0,0,$thumb_w,$thumb_h,$old_x,$old_y);
   	}
   	if (preg_match("/png/",$ext)){
   	   imagepng($dst_img,$filename);
   	} elseif (preg_match("/jpg|jpeg/",$ext)){
         imagejpeg ($dst_img,$filename);
   	} else {
         imagejpeg ($dst_img,$filename);
//   	   imagegif($dst_img,$filename);
   	}
   	imagedestroy($dst_img);
   	imagedestroy($src_img);
	} else {
		echo "Could not create a thumbnail.";
	}
}

function ResizeImage($filename,$newname,$maxwidth,$maxheight){
	$GD2Exists = chkgd2();

	$ext = strtolower(substr($newname, -3));
	if ($ext == 'jpg')
		$im = imagecreatefromjpeg($filename);
	elseif($ext == 'png')
		$im = imagecreatefrompng($filename);
	elseif($ext == 'gif')
		$im = imagecreatefromgif($filename);
	if ($im) {
   	$width = imagesx($im);
   	$height = imagesy($im);
   	if(($maxwidth && $width > $maxwidth) || ($maxheight && $height > $maxheight)){
   		if($maxwidth && $width > $maxwidth){
   			$widthratio = $maxwidth/$width;
   			$RESIZEWIDTH=true;
   		}
   		if($maxheight && $height > $maxheight){
   			$heightratio = $maxheight/$height;
   			$RESIZEHEIGHT=true;
   		}
   		if($RESIZEWIDTH && $RESIZEHEIGHT){
   			if($widthratio < $heightratio){
   				$ratio = $widthratio;
   			} else {
   				$ratio = $heightratio;
   			}
   		} elseif($RESIZEWIDTH) {
   			$ratio = $widthratio;
   		} elseif($RESIZEHEIGHT) {
   			$ratio = $heightratio;
   		}
      $newwidth = $width * $ratio;
      $newheight = $height * $ratio;
   		if($GD2Exists){
         $newim = imagecreatetruecolor($newwidth, $newheight);
         imagecopyresampled($newim, $im, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
   		} else {
   			$newim = imagecreate($newwidth, $newheight);
         imagecopyresized($newim, $im, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
   		}
      ImageJpeg ($newim,$newname); //  . ".jpg"
   		ImageDestroy ($newim);
   	} else {
   		ImageJpeg ($im,$newname); // . ".jpg"
   	}
	}
}

// Resize an image if it exceeds a predefined size.
function imageresize($filename,$newfilename,$maxwidth,$maxheight,$deleteoriginal=FALSE) {
 if (is_file($filename)) { /* check if the file is really one ;) */
  $fd = @fopen($filename,"r");
  $image_string = fread($fd,filesize($filename));
  fclose($fd);
	if ($deleteoriginal)
	  unlink($filename); /* we need to delete the file,
                       because we want to replace
                       the original file with the
                       resized one              */
  $im = ImageCreateFromString($image_string);
  $currwidth = ImageSX($im);
  $currheight = ImageSY($im);

/* now we have to check if the image
   is higher than wider and larger
   than the defined size, then
   calculte it and resize it proportional */
  if ($currwidth > $currheight && $currwidth > $maxwidth) {
   $percent = ($maxwidth * 100) / $currwidth;
   $nwidth = $maxwidth;
   $nheight = ($percent * $currheight) / 100;
  } elseif ($currwidth < $currheight && $currheight > $maxheight) {
   $percent = ($maxheight * 100) / $currheight;
   $nheight = $maxheight;
   $nwidth = ($percent * $currwidth) / 100;
  }
  $nwidth = intval($nwidth);
  $nheight = intval($nheight);

/* ok. let's create the new smaller image
   with calculated size... */
  $newim = imagecreate ($nwidth, $nheight);

/* ...and put the lager image into the
   smaller one. */
	imagecopyresized($newim,$im,0,0,0,0,$nwidth,$nheight,$width,$height);

  /* now save to file and clear up the memory ;) */
  imagepng($newim,$newfilename);
  ImageDestroy($im);
  ImageDestroy($newim);

/* if you want, use the returned HTML-Code
   and insert it into an <img>-tag */
  return "width=\"".$nwidth."\" height=\"".$nheight."\"";
 } else {
  /* if the file wasn't a file ;) return false */
  return false;
 }
}

function remote_file_exists ($url) {
/*
   Return error codes:
   1 = Invalid URL host
   2 = Unable to connect to remote host
*/

  $head = "";
  $url_p = parse_url ($url);
  if (isset ($url_p["host"])) {
		$host = $url_p["host"];
	} else {
		return 1;
	}

   if (isset ($url_p["path"]))
   { $path = $url_p["path"]; }
   else
   { $path = ""; }
   $fp = fsockopen ($host, 80, $errno, $errstr, 20);
   if (!$fp)
   { return 2; }
   else
   {
       $parse = parse_url($url);
       $host = $parse['host'];

       fputs($fp, "HEAD ".$url." HTTP/1.1\r\n");
       fputs($fp, "HOST: ".$host."\r\n");
       fputs($fp, "Connection: close\r\n\r\n");
       $headers = "";
       while (!feof ($fp))
       { $headers .= fgets ($fp, 128); }
   }
   fclose ($fp);
   $arr_headers = explode("\n", $headers);
   $return = false;
   if (isset ($arr_headers[0]))
   { $return = strpos ($arr_headers[0], "404") === false; }
   return $return;
}

if (!function_exists('file_get_contents')) {
  function file_get_contents($filename, $use_include_path = 0) {
    $file = @fopen($filename, 'rb', $use_include_path);
    if ($file) {
      if ($fsize = @filesize($filename)) {
         $data = fread($file, $fsize);
      } else {
        while (!feof($file)) {
          $data .= fread($file, 1024);
        }
      }
      fclose($file);
    }
	  return $data;
  }
}

function file_copy($file_origin, $destination_directory, $file_destination, $overwrite, $fatal) {
	if ($fatal) {
		$error_prefix = 'FATAL: File copy of \'' . $file_origin . '\' to \'' . $destination_directory . $file_destination . '\' failed.';
		$fp = @fopen($file_origin, "r");
   	if (!$fp) {
   		echo $error_prefix . ' Originating file cannot be read or does not exist.';
   		exit();
   	}

  	$dir_check = @is_writeable($destination_directory);
   	if (!$dir_check) {
   		echo $error_prefix . ' Destination directory is not writeable or does not exist.';
   		exit();
   	}

   	$dest_file_exists = file_exists($destination_directory . $file_destination);

    if ($dest_file_exists) {
    	if ($overwrite) {
      	$fp = @is_writeable($destination_directory . $file_destination);
      	if (!$fp) {
        	echo  $error_prefix . ' Destination file is not writeable [OVERWRITE].';
          exit();
        }
        $copy_file = @copy($file_origin, $destination_directory . $file_destination);
      }
  	 } else {
	  	 $copy_file = @copy($file_origin, $destination_directory . $file_destination);
	 	}
  } else {
    $copy_file = @copy($file_origin, $destination_directory . $file_destination);
  }
}

$IMAGEEXTENSIONS = array('jpg', 'png', 'jpeg', 'gif');

function GetFileList($path, $filenameonly=true, $extensions=false, $reverse=false) {
	global $IMAGEEXTENSIONS;

    if($extensions == false) //EXTENSIONS OF FILE YOU WANNA SEE IN THE ARRAY
        $extensions = $IMAGEEXTENSIONS;
    if ($handle = opendir($path)) {
    while (false !== ($file = readdir($handle))) {
      for ($i = 0; $i < count($extensions); $i++) {
        if (strpos($file, ".". $extensions[$i]) !== false) {
          if ($filenameonly) {
            $parts = explode("/", $file);
            $file = $parts[count($parts) - 1];
          }
          $returnfiles[] = $file;
        }
      }
		}
		if ($returnfiles) {
    //ORDER OF THE ARRAY
	    if ($reverse) {
  	    rsort($returnfiles);
	    } else {
  	    sort($returnfiles);
	    };
		}
  }

	return $returnfiles;
}

function GetRemoteFileList($ftpserver, $user, $pw, $path, $filenameonly=true, $extensions = FALSE, $reverse = FALSE) {
	global $IMAGEEXTENSIONS;

  if(!$extension) //EXTENSIONS OF FILE YOU WANNA SEE IN THE ARRAY
    $extensions = $IMAGEEXTENSIONS;

  $returnfiles = array();
	$ftphandle = ftp_connect($ftpserver);
	$login_result = ftp_login($ftphandle, $user, $pw);
	if ( $login_result == 1 ) {
    $files = ftp_nlist($ftphandle, $path);
		if ($files) {
      foreach($files as $key => $value) {
        //GET THE FILES ACCORDING TO THE EXTENSIONS ON THE ARRAY
        for ($i = 0; $i < count($extensions); $i++) {
          if (eregi("\.". $extensions[$i] ."$", $value)) {
            if ($filenameonly) {
              $parts = explode("/", $value);
              $value = $parts[count($parts) - 1];
            }
            $returnfiles[] = $value;
          }
        }
      }
		}
  }
  //CLOSE THE HANDLE
// NOT SUPPORTED ON RAQ4  ftp_close($ftphandle);

  //ORDER OF THE ARRAY
  if ($reverse) {
    rsort($returnfiles);
  } else {
    sort($returnfiles);
  };

  return $returnfiles;
}

function remote_file_size ($url) {
   $head = "";
   $url_p = parse_url($url);
   $host = $url_p["host"];
   $path = $url_p["path"];

   $fp = fsockopen($host, 80, $errno, $errstr, 20);
   if(!$fp)
   { return false; }
   else
   {
       fputs($fp, "HEAD ".$url." HTTP/1.1\r\n");
       fputs($fp, "HOST: dummy\r\n");
       fputs($fp, "Connection: close\r\n\r\n");
       $headers = "";
       while (!feof($fp)) {
           $headers .= fgets ($fp, 128);
       }
   }
   fclose ($fp);
   $return = false;
   $arr_headers = explode("\n", $headers);
   foreach($arr_headers as $header) {
       $s = "Content-Length: ";
       if(substr(strtolower ($header), 0, strlen($s)) == strtolower($s)) {
           $return = substr($header, strlen($s));
           break;
       }
   }
   return $return;
}

function ShowLanguages() {
	echo '<span id="lang">';
	$location = $PHP_SELF.'?';
	if ($_SERVER['QUERY_STRING'] != '')
 	  $location = $location.$_SERVER['QUERY_STRING'].'&';
	echo '<a href="'.$location.'lang=NL"><img src="images/nl.gif" alt="Nederlands" /></a>';
	echo '<a href="'.$location.'lang=EN"><img src="images/en.gif" alt="English" /></a>';
	echo '</span>';
}

function GetPageHTML($db, $page, $alang='NL') {
	$page = strtoupper($page);
	$query = "SELECT html FROM web_page WHERE upper(pagename) = '$page' and language = '$alang'";
	$rs = $db->Execute($query);
	if ($rs->RecordCount() == 0) {
		$page = 'START';
		$query = "SELECT html FROM web_page WHERE upper(pagename) = '$page' and language = '$alang'";
		$rs = $db->Execute($query);
	}

	if ($rec = $rs->FetchNextObject(true))
		return $rec->HTML;
	else
		return false;
}

function getOnlineUsers(){
	if ( $directory_handle = opendir( session_save_path() ) ) {
		$count = 1;
		while ( false !== ( $file = readdir( $directory_handle ) ) ) {
			if($file != '.' && $file != '..'){
				// Comment the 'if(...){' and '}' lines if you get a significant amount of traffic
				if(time()- fileatime(session_save_path() . '/' . $file) < MAX_IDLE_TIME * 60) {
					$count++;
				}
			}
		}
		closedir($directory_handle);
		return $count;
	}
	else {
		return false;
	}
};

function MaskPassword($password) {
	for ($i=0;$i<strlen($password)-1;$i++) {
		if (($i != 0) and ($i != strlen($password)-1))
			$password[$i] = '*';
	}
	return $password;
}

//	************************************************************
//	Checks a string for whitespace. True or false
//	************************************************************
function has_space ($text) {
	if( ereg("[ 	]",$text) )
		return true;
	else
		return false;
}

//	************************************************************
//	Strips whitespace (tab or space) from a string
//	************************************************************
function strip_space ($text) {
	$Return = ereg_replace("([ 	]+)","",$text);
	return ($Return);
}

//	************************************************************
//	Returns true if string contains only letters
//	************************************************************
function is_allletters ($text) {
  $Bad = strip_letters($text);
  if(empty($Bad))
  	return true;
	else
	  return false;
}

//	************************************************************
//	Strips letters from a string
//	************************************************************
function strip_letters ($text) {
  $Stripped = eregi_replace("([A-Z]+)","",$text);
  return $Stripped;
}
//<meta http-equiv="refresh" content="0.5;URL=?page=geheim/pbord">

function count_longwords($text, $maxlength) {
	$words = explode(" ", $text);
	$count = 0;
	foreach($words as $word) {
		if ((strlen($word) > $maxlength)
		 and (strpos($word, 'http:') === false)
   	 and (strpos($word, 'www.') === false)) {
			$count++;
		}
	}
	return $count;
}

function strip_longwords($text, $maxlength) {
	$words = explode(" ", $text);
	foreach($words as $word) {
		if (strlen($word) > $maxlength) {
			$text = str_replace($word, ' ', $text);
		}
	}
	return $text;
}

function ProcessGuestbookText($text) {
	$text = strip_tags($text);
	$text = AddSlashes($text);
	//$text = strip_space($text);
	if (count_longwords($text, 20) > 0)
		$text = '';
	if (is_allletters($text) and (strlen($text) > 10))
		$text = '';
	return $text;
}

function GetWebCounter($counter) {
	return GetImageNumber($counter);
}

function GetImageNumber($number) {
	$counterstr = $number;
	for ($i==1;$i<strlen($counterstr);$i++) {
		$digit = substr($counterstr, $i, 1);
		if ($digit == '.')
			$htm .= '.';
		else
			$htm .= "<img src='digits/$digit.gif' alt='$digit'>";
	}
	return $htm; // .'\''.$extensie
}

function isDatabaseFunction($text) {
	// functions in UPPERCASE
	$functions = 'NOW,ENCODE,ETC,TO_DATE';
	$text = trim($text);
	if ($text == '') $text = 'none';
	$text = strtoupper($text);
	$text = substr($text, 0, strpos($text,'('));
	//$pos = strpos($functions, $text);
	$functionarray = explode(",",$functions);
	$isFunction = false;
	foreach ($functionarray as $function) {
		if ($function == $text) {
			$isFunction = true;
			break;
		};
	}
	return $isFunction;
}

function IfNull($Var, $Value) {
	if (($Var == NULL) or ($Var == ''))
		return $Value;
	else
		return $Var;
}

function filesizeEx($file) {
  // First check if the file exists.
  if(!is_file("./".$file)) exit("File does not exist!");

  // Get the file size in bytes.
  $size = filesize($file);

	// return bytes in human readable form
  return '('.hbytes($size).')';
}

function hbytes ($bytes, $base10=false, $round=0, $labels=array('bytes', 'Kb', 'Mb', 'Gb')) {
  if (($bytes <= 0) ||
      (! is_array($labels)) ||
     (count($labels) <= 0))
      return null;

  $step = $base10 ? 3 : 10 ;
  $base = $base10 ? 10 : 2;

  $log = (int)(log10($bytes)/log10($base));

  krsort($labels);

  foreach ($labels as $p=>$lab) {
    $pow = $p * $step;
    if ($log < $pow) continue;
    $text = round($bytes/pow($base,$pow),$round) . $lab;
    break;
  }

  return $text;
}

function evennumber($num){
    return (int)!($num & 1);
    //return ($num % 2 == 0)
};

function getmodnumber($total) {
	// moet zijn: fmod(date('z'),$total) + 1;
	// als php4.2 is geinstalleerd
	if (evennumber(date('w')))
 		return 1;
	else
		return 2;
}

// support date format as : 23 1 2003, 23-01-2003, 23/01/2003,
// return timestamp
function str2date($in){

$t = explode("/",$in);
if (count($t)!=3) $t = explode("-",$in);
if (count($t)!=3) $t = explode(" ",$in);

if (count($t)!=3) return -1;
if (!is_numeric($t[0])) return -1;
if (!is_numeric($t[1])) return -2;
if (!is_numeric($t[2])) return -3;

if ($t[0]<=0 || $t[0]>31) return -3;
if ($t[1]<=0 || $t[1]>31) return -3;
if ($t[2]<=0 || $t[2]>3999) return -3;

$time = mktime (0,0,0, $t[1], $t[0], $t[2]);
return $time;
}

function str2datetime($in){
$parts = explode(" ",$in);
$t = explode("/",$parts[0]);
if (count($t)!=3) $t = explode("-",$parts[0]);
if (count($t)!=3) $t = explode(" ",$parts[0]);

if (count($t)!=3) return -1;
if (!is_numeric($t[0])) return -1;
if (!is_numeric($t[1])) return -2;
if (!is_numeric($t[2])) return -3;

if ($t[0]<=0 || $t[0]>31) return -3;
if ($t[1]<=0 || $t[1]>31) return -3;
//if ($t[2]<=0 || $t[2]>3999) return -3;

$h = explode(":", $parts[1]);
$hours = $h[0]?$h[0]:0;
$mins = $h[1]?$h[1]:0;

$time = mktime ($hours,$mins,0, $t[1], $t[0], $t[2]);
return $time;
}

/***********************************************************************
* function isDate
*
* boolean isDate(string)
* Summary: checks if a date is formatted correctly: dd/mm/yyyy (european)
* Author: Laurence Veale
* Date: 30/07/2001
***********************************************************************/
function isDate($i_sDate, $mindate='01-01-0001', $maxdate='01-01-3999')
{
	$blnValid = TRUE;
  // check the format first (may not be necessary as we use checkdate() below)
  if(!ereg ("^[0-9]{2}/[0-9]{2}/[0-9]{4}$", $i_sDate)) {
    $blnValid = FALSE;
  }
	else { //format is okay, check that days, months, years are okay
    $arrDate = explode("/", $i_sDate); // break up date by slash
    $intDay = $arrDate[0];
    $intMonth = $arrDate[1];
    $intYear = $arrDate[2];

    $intIsDate = checkdate($intMonth, $intDay, $intYear);

    if($intIsDate) {
			$dateunix = str2date($i_sDate);
			$minunix = strtotime($mindate);
			$maxunix = strtotime($maxdate);
		 	if (($dateunix < $minunix) or ($dateunix > $maxunix))
				$blnValid = FALSE;
		}
		else
			$blnValid = FALSE;
   }//end else

   return ($blnValid);
} //end function isDate

function directory($dir,$filters){
	$handle=opendir($dir);
	$files=array();
	if ($filters == "all"){while(($file = readdir($handle))!==false){$files[] = $file;}}
	if ($filters != "all"){
		$filters=explode(",",$filters);
		while (($file = readdir($handle))!==false) {
			for ($f=0;$f<sizeof($filters);$f++):
				$system=explode(".",$file);
				if ($system[1] == $filters[$f]){$files[] = $file;}
			endfor;
		}
	}
	closedir($handle);
	return $files;
}

/**
 * @param string $domain Pass $_SERVER['SERVER_NAME'] here
 * @param bool $debug
 *
 * @debug bool $debug
 * @return string
 */
function get_domain($domain, $debug = false)
{
    $original = $domain = strtolower($domain);

    if (filter_var($domain, FILTER_VALIDATE_IP)) { return $domain; }

    $debug ? print('<strong style="color:green">&raquo;</strong> Parsing: '.$original) : false;

    $arr = array_slice(array_filter(explode('.', $domain, 4), function($value){
        return $value !== 'www';
    }), 0); //rebuild array indexes

    if (count($arr) > 2)
    {
        $count = count($arr);
        $_sub = explode('.', $count === 4 ? $arr[3] : $arr[2]);

        $debug ? print(" (parts count: {$count})") : false;

        if (count($_sub) === 2) // two level TLD
        {
            $removed = array_shift($arr);
            if ($count === 4) // got a subdomain acting as a domain
            {
                $removed = array_shift($arr);
            }
            $debug ? print("<br>\n" . '[*] Two level TLD: <strong>' . join('.', $_sub) . '</strong> ') : false;
        }
        elseif (count($_sub) === 1) // one level TLD
        {
            $removed = array_shift($arr); //remove the subdomain

            if (strlen($_sub[0]) === 2 && $count === 3) // TLD domain must be 2 letters
            {
                array_unshift($arr, $removed);
            }
            else
            {
                // non country TLD according to IANA
                $tlds = array(
                    'aero',
                    'arpa',
                    'asia',
                    'biz',
                    'cat',
                    'com',
                    'coop',
                    'edu',
                    'gov',
                    'info',
                    'jobs',
                    'mil',
                    'mobi',
                    'museum',
                    'name',
                    'net',
                    'org',
                    'post',
                    'pro',
                    'tel',
                    'travel',
                    'xxx',
                );

                if (count($arr) > 2 && in_array($_sub[0], $tlds) !== false) //special TLD don't have a country
                {
                    array_shift($arr);
                }
            }
            $debug ? print("<br>\n" .'[*] One level TLD: <strong>'.join('.', $_sub).'</strong> ') : false;
        }
        else // more than 3 levels, something is wrong
        {
            for ($i = count($_sub); $i > 1; $i--)
            {
                $removed = array_shift($arr);
            }
            $debug ? print("<br>\n" . '[*] Three level TLD: <strong>' . join('.', $_sub) . '</strong> ') : false;
        }
    }
    elseif (count($arr) === 2)
    {
        $arr0 = array_shift($arr);

        if (strpos(join('.', $arr), '.') === false
            && in_array($arr[0], array('localhost','test','invalid')) === false) // not a reserved domain
        {
            $debug ? print("<br>\n" .'Seems invalid domain: <strong>'.join('.', $arr).'</strong> re-adding: <strong>'.$arr0.'</strong> ') : false;
            // seems invalid domain, restore it
            array_unshift($arr, $arr0);
        }
    }

    $debug ? print("<br>\n".'<strong style="color:gray">&laquo;</strong> Done parsing: <span style="color:red">' . $original . '</span> as <span style="color:blue">'. join('.', $arr) ."</span><br>\n") : false;

    return join('.', $arr);
}
