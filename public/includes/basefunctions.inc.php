<?php

function displayFontIcon($name, $size='1x') {
    $size = $size??'1x';
    $_icontype = substr($name, 0, strpos($name, '-'));
    $_iconname = str_replace($_icontype.'-', '', $name);
    if (!empty($_iconname)) {
        switch ($_icontype) {
            case 'fa':
                $_result = "<i class='fa-light fa-fw fa-{$size} fa-{$_iconname}'></i>";
                break;
            case 'md':
                $_result = "<i class='material-symbols-outlined md-{$size}'>{$_iconname}</i>";
                break;
            case 'icon':
                $_result = "<i class='icon {$_iconname}'></i>";
                break;
            default:
                trigger_error("Icon type {$_icontype} not supported");
                return;
        }
    }

    return $_result;
}


function createEvents($ev, $recordid=false) {
    $result = '';
    $events = explode('\n\r', $ev);

    foreach($events as $event) {
        $eventname = strtolower(substr($event, 0, strpos($event, ':')));
        if ($eventname != '') {
        $eventcode = substr($event, strpos($event, ':') + 1);
        if ($recordid)
            $eventcode = str_replace('%recordid%', $recordid, $eventcode);
            $result .= 'on'.$eventname.'="'.$eventcode.'" ';
        }
    }
    return $result;
}
