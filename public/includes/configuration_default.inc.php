<?php

/**
 * Here you can set the different Polaris instances (databases)
 * that Polaris can connect to.
 *
 * This is practical when you want to use different PLR databases
 * with only one source code.
$_PLRINSTANCE[0]['name'] = 'mypolarisdatabase';
$_PLRINSTANCE[0]['dbtype'] = 'mysql';
$_PLRINSTANCE[0]['host'] = 'localhost';
$_PLRINSTANCE[0]['port'] = 3306;
$_PLRINSTANCE[0]['database'] = 'polaris';
$_PLRINSTANCE[0]['username'] = '<youruser>';
$_PLRINSTANCE[0]['password'] = '<yourpassword>';
$_PLRINSTANCE[0]['clientselect'] = false;
*/

/**
 * What is de default Polaris instance?
 * This is also the instance that is used when processing (formmail) messages,
 * Rest service calls, and other direct access calls
 */
$_CONFIG['currentinstance'] = 0;

/**
 * Set if user should be able to choose a Polaris instance (see above)
 */
$_CONFIG['chooseinstance'] = false;

/**
 * Set if Polaris should log the performance of queries (via ADOBD)
 */
$_CONFIG['logperformance'] = false;

/**
 * If set to false, no memcaching is used.
 */
$_CONFIG['usememcached'] = false;
$_CONFIG['memcached_servers'] = array('memcached');
$_CONFIG['memcached_port'] = 11211;

/**
 * Set the dbcache folder of ADOBD to speed up Polaris queries.
 * If set to null, no caching is used.
 */
$_CONFIG['cachequeries'] = false;
$_CONFIG['cachequeries_folder'] = '/var/tmp/polaris/cache';
$_CONFIG['cachequeries_timeout'] = 3600;

/**
 * Is this Polaris instance part of an OEM solution?
 * If so, don't show Internet related stuff
*/
$_CONFIG['oeminstance'] = true;


/**
 * Set the smarty generated files folder
 */
$_CONFIG['smarty_template_folder'] = '/var/tmp/polaris/smarty';

/**
 * Set the smarty cache files folder
 */
$_CONFIG['smarty_cache_folder'] = '/var/tmp/polaris/cache'; // wordt niet gebruikt, dynamic content

/**
 * Set the lifetime of a session (in seconds and as string).
 */
$_CONFIG['sessionlifetime'] = "3600";

/**
 * Is this Polaris instance part of an Intranet solution?
 * If so, don't show Internet related stuff
 */
$_CONFIG['intranetinstance'] = true;

/**
 * Should it be possible to log in with the same username more that once?
 */
$_CONFIG['multiplelogins'] = true;

/**
 * Should it be possible to order a recordset on multiple columns
 */
$_CONFIG['multicolumnsort'] = true;

/**
 * If this Polaris settings is True
 * then you get more visual eye candy, like smooth transitions and rounded corners
 */
$_CONFIG['eyecandy'] = true;

/**
 * Select the method by which Polaris authenticates users,
 * shows users and groups.
 */
$_CONFIG['usergrouphandler_type'] = 'PLR'; /* { PLR | LDAP} */

/**
 * LDAP settings (see usergroupHandler.LDAP.class.php for more info)
 */
$_CONFIG['LDAP.account_suffix'] = "@mydcomain.local";
$_CONFIG['LDAP.base_dn'] = "DC=mydomain,DC=local";
/*  Specify multiple controllers if you would like the class to balance the LDAP queries amongst multiple servers */
$_CONFIG['LDAP.domain_controllers'] = array ("dc01.mydomain.local");
/*  optional account with higher privileges for searching */
$_CONFIG['LDAP.ad_username'] = "username";
$_CONFIG['LDAP.ad_password'] = "password";
/*
    AD does not return the primary group. http://support.microsoft.com/?kbid=321360
    This tweak will resolve the real primary group, but may be resource intensive.
    Setting to false will fudge "Domain Users" and is much faster. Keep in mind though that if
    someone's primary group is NOT domain users, this is obviously going to bollocks the results
*/
$_CONFIG['LDAP.real_primarygroup'] = true;
/*  When querying group memberships, do it recursively */
$_CONFIG['LDAP.recursive_groups'] = true;


/**
 * Should this Polaris show all 'raw' error messages?
 */
$_CONFIG['debugmode'] = false;


$_CONFIG['default_date_format'] = 'd-m-Y';
$_CONFIG['default_time_format'] = 'H:i';
$_CONFIG['default_record_limit'] = 20;
$_CONFIG['default_refresh_interval'] = 0;
$_CONFIG['navigator_button_style'] = true;

/**
 * The format of the date-time in the top menu
 */
$_CONFIG['currentdatetimeformat'] = '%e %B %Y';

/**
 * The maximum of subgroups in a group (plr_usergroup, plr_membership)
 */
$_CONFIG['maxrecursivegrouplevel'] = 10;

/**
 * What is the name of Polaris (when it's being used
 * as part of another system or in a Intranet)
 */
$_CONFIG['polarisname'] = 'Polaris';

/**
 * Stylesheets
 */
$_CONFIG['plrcss'] = $_GVARS['serverpath'].'/css/plrdefaultdark.css';
//$_CONFIG['plrcss_tabs'] = $_GVARS['serverpath'].'/css/pagetabs.css';
$_CONFIG['plrprintcss'] = $_GVARS['serverpath'].'/css/plrprint.css';
//$_CONFIG['plrfonticons'] = $_GVARS['serverpath'].'/bower_components/fontawesome/css/font-awesome.css,'.$_GVARS['serverpath'].'/css/font-financial.css,'.$_GVARS['serverpath'].'/css/font-line-icons.css';

/**
 * What is the name of Polaris (when it's being used
 * as part of another system or in a Intranet)
 */
$_CONFIG['showuserimage'] = false;
$_CONFIG['userimage_maxheight'] = 100;
$_CONFIG['userimage_maxwidth'] = 75;

/**
 * Branding the Polaris environment
 * Specify the Brand of Polaris (see folder 'branding' for possible options)
 */
$_CONFIG['brand'] = '_default';

$_CONFIG["polarisreporturl"] = $_GVARS['serverroot'].'/showreport/pdf/%REPORTNAME%/?%REPORTPARAMS%';

$_CONFIG["cupsprinterhost"] = "172.16.1.10";

/**
 * Bugtracking software aanzetten
 */
$_CONFIG['bugtracking'] = false;
$_CONFIG['bugtracking_url'] = $_GVARS['serverroot'].'/app/bugtracker/const/bugtracking/';

/**
 * Show the db connection after the username (top of screen)
 */
$_CONFIG['showdatabaseconnection'] = true;

/**
 * Show time it takes for the page to load, including DB sql execution times (right corner of screen)
 */
$_CONFIG['displaytimers'] = false;

/**
 * Some forms have detail records set up as Ajax module. With this setting you can turn this Ajax behaviour on or off
 */
$_CONFIG['showajaxdetails'] = true;

/**
 * Is it possible to sort a listview with multiple columns? Default yes
 */
$_CONFIG['multicolumnsort'] = true;

/**
 * For Oracle: What is the decimal seperator of a float value?
 */
$_CONFIG['floatdecimalchar'] = '.';

/**
 * What OS does Polaris run on (UNIX or WINDOWS)?
 */
$_CONFIG['ostype'] = 'UNIX';

/**
 * What is the path of Curl on Windows
 */
$_CONFIG['windows_curl_path'] = 'C:\\curl.exe';

/**
 * What is the path of PrintPDF on Windows (use short 8.3 naming convention)
 */
$_CONFIG['windows_printpdf_path'] = 'C:\\Progra~2\\pdfprint_cmd\\pdfprint.exe -useembedfont';

/**
 * How strong should the hashing algoritm be?
 */
$_CONFIG['hash_cost_log2'] = 8;

/**
 * Should the Password Hasher use the weaker MD5 function?
 */
$_CONFIG['hash_portable'] = FALSE;

date_default_timezone_set("Europe/Amsterdam");

/**
 * What SMTP server should the Polaris Alert use?
 */
$_CONFIG['smtpserver'] = 'smtp.someserver.com';
$_CONFIG['smtpserver_port'] = 465;
$_CONFIG['smtpserver_encryption'] = 'ssl';
$_CONFIG['smtpserver_username'] = '<username>';
$_CONFIG['smtpserver_password'] = '<password>';
$_CONFIG['smtpserver_sendmaximum'] = 3;

$_CONFIG['beanstalkd_host'] = 'beanstalkd';
$_CONFIG['beanstalkd_port'] = 11300;
