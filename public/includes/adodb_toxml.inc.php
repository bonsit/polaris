<?php

/**
 * Creates XML from the ADODB record set
 *
 * @param 	object 		$rs 		- record set object
 * @param 	string		$tablename	- what is the name of the table, hence the name of each record instance
 * @return 	string		$xml		- resulting xml
 * @version V1.1  28 August 2006  (c) 2006 Bart Bons ( http://www.debster.nl/ ).
 * BASED ON:
 * @version V1.0  10 June 2006  (c) 2006 Rich Zygler ( http://www.boringguys.com/ ). All rights reserved.
 *
 */

function rs2xml($rs, $tablename)
{
	if (!$rs) {
		printf(ADODB_BAD_RS,'rs2xml');
		return false;
	}
	$xml = '';
	$totalRows = 0;
	$totalRows = $rs->length;
	$domxml = new DOMDocument('1.0');
	$root = $domxml->appendChild($domxml->createElement('records'));
	$root->setAttribute('recordcount', $totalRows);

	while($line = $rs->fetchRow())
	{
		$row = $root->appendChild($domxml->createElement($tablename));

		foreach ($line as $col_key => $col_val)
		{
			$col = $row->appendChild($domxml->createElement(strtolower($col_key)));
			$col->appendChild($domxml->createTextNode($col_val));
		}
	}
	$domxml->formatOutput = true;
	$xml = $domxml->saveXML();
	$domxml = null;

	return $xml;
}

?>