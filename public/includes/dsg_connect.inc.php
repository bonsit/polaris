<?php

// *********************************
// POLARIS CONNECTION INITIALISATION
// *********************************
var_dump($_GVARS['docroot'].'/includes/plrsettings.inc.php');
require_once($_GVARS['docroot'].'/includes/plrsettings.inc.php');
include_once('adodb.inc.php');
$plrdb = NewADOConnection($DBType);
if ($_SERVER['PHP_SELF'] != 'plrsettings.php') {
	$plrhost = PLRHOST;
	if (PLRPORT > 0) $plrhost .= ':'.PLRPORT;
	if (!$plrdb->Connect($plrhost, PLRUSERNAME, PLRPASSWORD, PLRDATABASE)) {
//		header("Location: $_GVARS['serverroot']/plrsettings.php");
//		exit;
	}
	$plrdb->debug = isset($_GET['debug']);
}
?>