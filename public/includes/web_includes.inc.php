<?php
	require_once('includes/global.inc.php');
	require_once('includes/dbroutines.inc.php');
	require_once('includes/setlanguage.inc.php');
	require_once('core/lang_api.php');
	lang_load($_GVARS['lang']);

	require_once('includes/web_connect.inc.php');
	require_once('sql/mysql-lang.inc.php');
	require_once('sql/mysql-web.inc.php');
	require_once('classes/web_smarty.class.php');
	require_once('includes/web_functions.inc.php');
