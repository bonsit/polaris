<?php

function moveuploadedimages($path, $overwritefiles=false) {
    if ( substr($path, -1) != '/') $path .= '/';

    for ($i=1;$i<=5;$i++) {
        $result = moveuploadedimage($imagename='imagefile'.$i, $path, $overwritefiles);
    }
    return $result;
}

function moveuploadedimage($imagename, $path, $overwritefiles=false) {
    $basename = str_replace(' ','_',$_FILES[$imagename]['name']);
    $name = $basename;
    $tmp  = $_FILES[$imagename]['tmp_name'];    // this is the temporary name of your file in temporary directory on the server
    if (is_uploaded_file ($tmp)) {                      // is this temporary file really uploaded?
        if (!$overwritefiles) {
            $i=1;
            while (file_exists($path.$name)) {
                $name = $i.'_'.$basename;
                $i++;
            }
        }    
        $url = $path.preg_replace("[^A-Za-z0-9]", "", $name );
        move_uploaded_file($tmp, $url); 
        $result[] = $url;
    }
    return $result;
}

function createthumbs($files, $path) {
    if (is_array($files)) {
        foreach($files as $filename) {
            $thumbfile = $path.THUMBPREFIX.basename($filename);
            createthumb($filename,$thumbfile,$new_w=80,$new_h=80);
        }
    } else {
        echo "<p>Could not create the thumbnail pictures.</p>";
    }
}

?>