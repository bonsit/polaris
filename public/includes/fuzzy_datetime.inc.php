<?php 
function FuzzyTime($time) { 
        define("ONE_DAY",86400); 
     
        $now = time(); 
        // sod = start of day :) 
        $sod = mktime(0,0,0,date("m",$time),date("d",$time),date("Y",$time)); 
        $sod_now = mktime(0,0,0,date("m",$now),date("d",$now),date("Y",$now)); 

        // check 'today' 
        if ($sod_now == $sod) { 
            return "vandaag om " . date("G:i",$time); 
        } 
        // check 'yesterday' 
        if (($sod_now-$sod) <= 86400) { 
            return "gisteren om " . date("G:i",$time); 
        }
        // give a day name if within the last 5 days 
        if (($sod_now-$sod) <= (ONE_DAY*5)) { 
            return date("l \o\\m G:i",$time); 
        } 
        // miss off the year if it's this year 
        if (date("Y",$now) == date("Y",$time)) { 
            return date("j F \o\\m G:i",$time); 
        } 
        // return the date as normal 
        return date("j F, Y \o\\m G:i",$time); 
} 
?>