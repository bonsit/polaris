<?php
define('_SEARCH',     'Search');
define('GROUP_PREFIX', '_GRP_');
// define('TIMESTAMP_POSTFIX', '_UTS');
define('TIMESTAMP_POSTFIX', '');
define('CHANGEPASSWORDPERIOD' , 25);
define('THUMBPREFIX' , '_THUMB_');
define('_RECORDID' , 'PLR__RECORDID');
define('FLOATCONVERT' , '@@FLOATCONVERT');
define('DEFAULTCOLUMN' , '@@DEFAULTCOLUMN');
define('JSONMULTIFIELD', '#JSONMULTIFIELD');

define('DEFAULT_HASH_FUNCTION', 'md5');
define('DEFAULT_DB_HASH_FUNCTION', 'md5');

define('PASSWORD_TOO_WEAK', 1);
define('PASSWORD_STORED', 0);
define('PASSWORD_NOT_STORED', 2);

define('BASEURL_NORMAL', 'normal');
define('BASEURL_AJAX', 'ajax');
define('BASEURL_APPLICATION', 'application');
define('BASEURL_SERVICE', 'service');
define('BASEURL_CALLER', 'caller');

# MySQL Errors code
define('MYSQL_DUPLICATE_ENTRY', 1062);

## Permission constants
define('HIDDEN', 1);
define('SELECT', 2);
define('INSERT', 4);
define('UPDATE', 8);
define('DELETE', 16);
define('IMPORT', 32);
define('EXPORT', 64);
define('DOCLONE', 128);
define('SEARCH', 256);
define('PRINT', 512);
/* FULL => 1023 */
/* SIUDS => 286 */

## Access Key for accessibility
define('AK_NEWRECORD', 'F10');
define('AK_ALLRECORDS', 'F11');
define('AK_FIND', 'f');

## the Actions used in html FORMS: webpage.php?action=....
define('ACTION_EDIT', 'edit');
define('ACTION_ADD', 'add');
define('ACTION_ADD_MENU', 'add_menu');
define('ACTION_ADD_MENUITEM', 'add_menuitem');

define('ACTION_CREATEMEMBERSHIP', 'createmembership');
define('ACTION_CREATEMEMBERS', 'createmembers');
define('ACTION_GENERATE_APP', 'genapp');
define('ACTION_CANCELMEMBERSHIP', 'cancelmembership');

$GLOBALS['REPEATHEADERAT'] = 15;

## defining the languages which are currently supported in Polaris
$PLRLANGUAGES = array( 'NL' => 'nl_NL.UTF8','EN' => 'en','SE' => 'se','IT' => 'it', 'DE'=>'de_DE');

$BASEURL = 'polaris.php?';
$BASEROOTADMINURL = 'plradmin.php';
$TABLEPREFIX = ' ttt';
