<?php


// set the error reporting level for this script
error_reporting (E_USER_ERROR | E_USER_WARNING );

$GLOBALS['errorhasoccured'] = false;

$skip_undefined_index = true;
$debugging = false;
// error handler function
function PolarisErrorHandler($errno, $errstr, $errfile, $errline) {
	global $polaris;
	global $g_lang_strings;
	global $skip_undefined_index;
	global $debugging;


  switch ($errno) {
  case E_USER_ERROR:
    $GLOBALS['errorhasoccured'] = true;
    echo "<b>FATAL</b> [$errno] $errstr<br>\n";
    echo "  Fatal error in line ".$errline." of file ".$errfile;
    echo ", PHP ".PHP_VERSION." (".PHP_OS.")<br>\n";
    echo "Aborting...<br>\n";
    exit(1);
    break;
  case E_USER_WARNING:
    $GLOBALS['errorhasoccured'] = true;
		if ($polaris) {
		    /* Log the error in the Polaris logbase */
            $polaris->logUserAction('SQL Error', session_id(), 'hotupdate', 'message: ' .$errno.' '.$errstr.' '.$errfile.' '.$errline);

			$assignederror = $polaris->tpl->getTemplateVars('error_message');
			if (!isset($assignederror)) {
				$polaris->tpl->assign('error_no', $errno);
                $polaris->tpl->assign('error_message', $errstr);
                $polaris->tpl->assign('pretty_error_message', $errstr);
                if (isset($g_lang_strings['error_'.$errno]))
                    $polaris->tpl->assign('pretty_error_message', lang_get('error_'.$errno));
                elseif (isset($g_lang_strings[$errstr]))
                    $polaris->tpl->assign('pretty_error_message', lang_get($errstr));
            }
		} else
	        echo "<b>ERROR</b> [$errno] $errstr<br>\n";
    break;
  default:
	/*
		if ($smarty) {
			$assignederror = $polaris->tpl->getTemplateVars('error_message');
			if (!isset($assignederror)) {
				$polaris->tpl->assign('error_no', $errno);
				if (isset($g_lang_strings[$errstr]))
					$polaris->tpl->assign('error_message', $g_lang_strings[$errstr]);
				else {
    	    $errstr .= "<br />Error in line $errline of file $errfile";
					$polaris->tpl->assign('error_message', $errstr);
				}
			}
		}
		else
	    echo "<b>ERROR</b> [$errno] $errstr<br>\n";
    break;
  }
 */
		if ($debugging) {
  		if (!($skip_undefined_index and $errno == 8)) {
  	    echo "Unknown error type: [$errno] $errstr<br>\n";
    	  echo "  Error in line ".$errline." of file ".$errfile;
      	echo ", PHP ".PHP_VERSION." (".PHP_OS.")<br>\n";
  		}
		}
    break;
	}
}
// set to the user defined error handler
//$old_error_handler = set_error_handler("PolarisErrorHandler");

?>
