<?php
/**
 * Load the environment variables from the .env file in the parent directory.
 */
require_once __DIR__ . '/../vendor/autoload.php'; // Ensure the composer autoload file is included.

$dotenv = Dotenv\Dotenv::createMutable(__DIR__.'/..');
$dotenv->load();

/**
 * A convenience function to print data within <pre> tags for better readability.
 *
 * @param mixed ...$data
 */
function pr(...$data) {
    echo '<pre style="font-size:0.8em;-webkit-user-select:text;user-select:text;white-space: pre-wrap">', var_dump($data), '</pre>';
}

/**
 * Define constants for directory paths if they are not already defined.
 */
if (!defined('PLR_FOLDER')) {
    define('PLR_FOLDER', trim(dirname($_SERVER['PHP_SELF']), '/\\'));
}

/**
 * Load the configuration settings.
 */
require_once PLR_DIR . '/configs/configuration_default.inc.php';
require_once PLR_DIR . '/conf/configuration.inc.php';

/**
 * Right after the configuration settings are loaded, we must removed the passwords from the $_SERVER global var.
 */
foreach($_SERVER as $key => $value) {
    if (strpos($key, '_PASSWORD') !== false) {
        unset($_SERVER[$key]);
    }
}

$protocol = empty($_CONFIG['polarisport']) ? ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') ? 'https' : 'http') : $_CONFIG['polarisport'];

define('ADODB_ASSOC_CASE', 2); // Assume db case in column names in result sets.

$ServerName = $_SERVER['HTTP_HOST'] ?? '';

// Define global variables for consistent access throughout the application.
$_GVARS = [
    'module_dir' => 'modules',
    'host' => "{$protocol}://{$ServerName}",
    'serverroot' => rtrim("{$protocol}://{$ServerName}/" . PLR_FOLDER, '/'),
    'serverpath' => PLR_FOLDER,
    'docroot' => PLR_DIR,
];

// Setup include paths for additional resources.
$includePaths = [
    PLR_DIR . '/',
    PLR_DIR . '/php_includes',
    PLR_DIR . '/modules',
    PLR_DIR . '/vendor/adodb/adodb-php',
];
ini_set('include_path', implode(PATH_SEPARATOR, $includePaths));
// Enable error reporting based on a GET parameter.
if (filter_input(INPUT_GET, 'display_errors', FILTER_VALIDATE_BOOLEAN)) {
    ini_set('display_errors', 'On');
}
