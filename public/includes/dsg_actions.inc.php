<?php
	$actions_usergroup =
		array ( array ('location' => $_GVARS['serverroot'].'/designer/users/?action=add&type=user', 'icon' => 'beos/person.png', 'caption' => $g_lang_strings['make_new_user'].'...'),
					  array ('location' => $_GVARS['serverroot'].'/designer/users/?action=add&type=group', 'icon' => 'beos/people.png', 'caption' => $g_lang_strings['make_new_group'].'...'),
          );

	$actions_client =
		array ( array ('location' => $_GVARS['serverroot'].'/designer/clients/?action=add', 'icon' => 'beos/logo_3d.png', 'caption' => 'Voeg een klant toe...')
          );
					
	$actions_app = 
		array ( array ('location' => $_GVARS['serverroot'].'/designer/appgen/', 'icon' => 'beos/flash.png', 'caption' => 'Maak nieuwe applicatie...'),
				array ('location' => $_GVARS['serverroot'].'/designer/formgen/', 'icon' => 'beos/workspaces2.png', 'caption' => 'Quick Form Generator...')
          );

	$actions_db = 
		array ( array ('location' => '#', 'icon' => 'beos/flash.png', 'caption' => 'Import database'),
						array ('location' => '#', 'icon' => 'beos/workspaces2.png', 'caption' => 'Export database')
          );

	$actions_sitegen =
		array ( array ('location' => $_GVARS['serverroot'].'/designer/sitegen/?action=add', 'icon' => 'beos/globe_html_editor.png', 'caption' => 'Voeg een website toe...')
          );
?>