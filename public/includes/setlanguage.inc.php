<?php
// Load the GETted language resource and store is as a session variable
if (isset($_GET['lang']))
	$_SESSION['lang'] = $_GET['lang'];
// Load t$_SESSION['lang']he POSTed language resource and store is as a session variable
if (isset($_POST['_hdnLanguage']))
  $_SESSION['lang'] = $_POST['_hdnLanguage'];
// Load the SESSION language resource.
// If none is supplied, then language is defaulted to NL
if (!isset($_SESSION['lang'])) {
    // set the default language or the even more default language
    $_SESSION['lang'] = (@constant('PLRDEFAULTLANG') != '') ? constant('PLRDEFAULTLANG') : 'NL';
}
$_GVARS['lang'] = $_SESSION['lang'];
