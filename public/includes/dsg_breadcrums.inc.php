<?php

	$basemenu = array (  array('Class' => '', 'Name' => $g_lang_strings['applications'],'link' => 'polaris.php'),
 											 array('Class' => '', 'Name' => $g_lang_strings['designer'],'link' => 'plrdesigner.php'),
											 array('Class' => '', 'Name' => $g_lang_strings['ctrlpanel'],'link' => 'plrsettings.php'),
											 array('Class' => 'standard', 'Name' => $g_lang_strings['help'],'link' => 'plrhelp.php'),
											 array('Class' => 'standard', 'Name' => $g_lang_strings['logout'],'link' => 'plrdesigner.php?logout=true') );
	$functions = array ( array('Name' => 'Klanten','link' => 'plrclients.php'),
 											 array('Name' => 'Gebruikers','link' => 'plrusers.php'),
											 array('Name' => 'Applicaties','link' => 'plrapps.php'),
											 array('Name' => 'Databases','link' => 'plrdatabases.php') );
	$subfunctions = array ( 
                   array('Name' => 'Overzicht','link' => 'plrapps.php'),
									 array('Name' => 'Zoeken','link' => 'plrsearch.php'),
									 array('Name' => 'Importeren','link' => 'plrapps.php'),
									 array('Name' => 'Extra','link' => 'plrapps.php')
									  );
										
	$bc_start = array('label' => lang_get('polaris_overview'),'link' => $_GVARS['serverroot'].'/designer/');

?>