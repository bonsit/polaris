<?php

// ******************************************************
// Function that returns the dates for each day in a week
// ******************************************************
function week_dates($weekNumber,$year) {
    // Count from '0104' because January 4th is always in week 1
    // (according to ISO 8601).
    $time = strtotime('+' . ($weekNumber - 1). ' weeks', strtotime($year . '0104'));
// WERKT NIET IN PHP4.3.9    $time = strtotime($year . '0104 +' . ($weekNumber - 1). ' weeks');
    // Get the time of the first day of the week
    $diff = (date('w', $time) - 1 >= 0) ? (date('w', $time) - 1) : 6;
    $mondayTime = strtotime('-' . $diff . ' days', $time);
    // Get the times of days 0 -> 6
    $dayTimes = array ();
    for ($i = 0; $i < 7; ++$i) {
        $dayTimes[] = strtotime('+' . $i . ' days', $mondayTime);
    }
    // Return timestamps for mon-sun.
    return $dayTimes;
}

function TimeToMinutes($time) {
  $timeparts = explode(':',$time);

  return ($timeparts[0] * 60) + $timeparts[1];
}

function MinutesToTime($minutes) {
  $mins = $minutes % 60;
  $hours = ($minutes - $mins) / 60;

  $retval = sprintf("%02d", $hours) . ":" . sprintf("%02d", $mins);

  return $retval;
}

function SecondsToTime($seconds) {
  $mins = $seconds / 60;
  return MinutesToTime($mins);
}

function GetRightDay($daynumber) {
  // in:  sun, mon, tue, wed, thu, fri, sat
  // out: mon, tue, wed, thu, fri, sat, sun
  switch($daynumber) {
  case 0: return 6;
  break;
  case 1: return 0;
  break;
  case 2: return 1;
  break;
  case 3: return 2;
  break;
  case 4: return 3;
  break;
  case 5: return 4;
  break;
  case 6: return 5;
  break;
  }
}

// How many days are in a given month.
function daysInMonth($month, $year) {
  $feb = daysInFeb($year);
  $daysInMonthsArr = array(31, $feb, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
  $monthIndex = $month -1;
  return $daysInMonthsArr[$monthIndex];
}

// Deals with the leap year issue.
// Not Y2.2k compliant.
function daysInFeb($year) {
  if (is_int($year / 4) && ($year != 1900 && $year != 2100) ) { return 29; }
  else { return 28; }
}
