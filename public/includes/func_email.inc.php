<?php

use Postmark\PostmarkClient;
use Postmark\Models\PostmarkException;

function sendmail(&$resp, $from, $subject, $sendto, $textMessage, $tag=null, $headers=false, $replyToAddress=NULL) {
    global $_CONFIG;

    $urlarray = parse_url ( $_SERVER['HTTP_REFERER']??'' );
    if ($urlarray['host']??false) {
        $_cleanHost = getHost($urlarray['host']);
        var_dump($_cleanHost);
        $api_key = $_CONFIG['postmark_api_token.'.$_cleanHost];
    } else {
        $api_key = $_CONFIG['postmark_api_token'];
    }

    $HTMLmessage = null;
    try {
        $postmark = new PostmarkClient($api_key);
        $result = $postmark->sendEmail($from,
            $sendto,
            $subject,
            $HTMLmessage,
            $textMessage,
            $tag,
            trackOpens:true,
            replyTo:$replyToAddress);
    } catch(PostmarkException $ex) {
        // If client is able to communicate with the API in a timely fashion,
        // but the message data is invalid, or there's a server error,
        // a PostmarkException can be thrown.
        echo $ex->httpStatusCode;
        echo $ex->message;
        echo $ex->postmarkApiErrorCode;
    } catch(Exception $generalException) {
        // A general exception is thrown if the API
        // was unreachable or times out.
    }

    return $result;
}

function sendmailWithTemplate($from, $sendto, $templateid, $templatemodel, $api_token=null, $tag=null, $headers=false, $replyToAddress=NULL) {
    global $_CONFIG;

    $result = null;
    // If the Polaris client has no api_token, try to get it from the host
    if (empty($api_token)) {
        $urlarray = parse_url ( $_SERVER['HTTP_REFERER']??'' );
        if ($urlarray['host']??false) {
            $_cleanHost = getHost($urlarray['host']);
            $api_token = $_CONFIG['postmark_api_token.'.$_cleanHost];
        } else {
            $api_token = $_CONFIG['postmark_api_token'];
        }
    }

    try {
        $postmark = new PostmarkClient($api_token);
        $result = $postmark->sendEmailWithTemplate($from,
            $sendto,
            $templateid,
            $templatemodel,
            $tag,
            trackOpens:true,
            replyTo:$replyToAddress);

    } catch(PostmarkException $ex) {
        // If client is able to communicate with the API in a timely fashion,
        // but the message data is invalid, or there's a server error,
        // a PostmarkException can be thrown.
        echo $ex->httpStatusCode;
        echo $ex->message;
        echo $ex->postmarkApiErrorCode;
    } catch(Exception $generalException) {
        // A general exception is thrown if the API
        // was unreachable or times out.
    }

    return $result;
}

function ValidEmail_old($EMAIL) {
    # RETURN VALUE FUNCTION
    $valid = 0;
    $EMAIL = chop($EMAIL); # remove white space
    $ValidAt = strpos($EMAIL,"@"); # check for @
    $ValidLen = strlen($EMAIL);
    $ValidCom = strpos(substr($EMAIL,$ValidAt,$ValidLen - $ValidAt),".");
    if (($ValidAt > 0)&&($ValidAt < $ValidLen)&&($ValidCom > 0))
    { $valid = 1; }
    return($valid);
}

function ValidEmail($email) {
    return filter_var($email, FILTER_VALIDATE_EMAIL) !== false;
}