<?php
include_once(PLR_DIR.'/includes/miscfunc.inc.php');

function trim_array($_array) {
	if (empty($_array) or !is_array($_array)) {
	  return null;
	} else {
	  return array_filter($_array, function($value) { return $value !== ''; });
	}
}

function ProcessSQL($db, $text) {
	if (!isset($db)) return $text;

	try {
		if (substr($text, 0, 5) == 'SQL::') {
			$sql = substr($text, 5);
			$rs = $db->GetAll($sql);
			return $rs[0][0]; // return the first field from the first record
		} elseif (strpos($text, '{{SQL::') !== false ) {
			// process a dynamic interpolated SQL statement
			$start = strpos($text, '{{SQL::');
			$end = strpos($text, '}}');
			$sql = substr($text, $start+7, $end-$start-7);
			$rs = $db->GetOne($sql);
			return str_replace(['{{SQL::'.$sql.'}}'], $rs, $text);
		} else {
			return $text;
		}
	} catch (Exception $e) {
		return '';
	}
}

function datadump ($db, $table) {
  $result .= "# Dump of $table \n";
  $result .= "# Dump DATE : " . date("d-M-Y") ."\n\n";

  $db->SetFetchMode(ADODB_FETCH_NUM);
  $rs = $db->Execute("select * from $table");
  $num_fields = $rs->FieldCount();
  while ($row = $rs->FetchRow()) {
    $result .= "INSERT INTO ".$table." VALUES(";
    for($j=0; $j<$num_fields; $j++) {
      $row[$j] = addslashes($row[$j]);
      $row[$j] = ereg_replace("\n","\\n",$row[$j]);
      if (isset($row[$j])) $result .= "\"$row[$j]\"" ; else $result .= "\"\"";
      if ($j<($num_fields-1)) $result .= ",";
    }
    $result .= ");\n";
  }
  return $result . "\n\n\n";
}

function getClientHash($db, $clientid ) {
	global $_sqlDSGGetHashClient;
	$rs = $db->Execute($_sqlDSGGetHashClient, array($clientid)) or Die('SQL failed: '. $_sqlDSGGetHashClient);
	if ($rs) {
		$record = $rs->FetchNextObject(true);
		return $record->RECORDID;
	} else {
	  return false;
	}
}

function GetGroupValues($db, $clientid, $usergroupid) {
    global $_sqlGroup;

    $_result = $usergroupid;

    if (!$db) return $_result;
    $rs = $db->GetAll($_sqlGroup, array($clientid, $usergroupid));
    $_allGroups = [];
    if (count($rs) > 0) {
        foreach($rs as $msrec) {
            $_allGroups[] = $msrec['LVL1'];
            $_allGroups[] = $msrec['LVL2'];
            $_allGroups[] = $msrec['LVL3'];
            $_allGroups[] = $msrec['LVL4'];
            $_allGroups[] = $msrec['LVL5'];
            $_allGroups[] = $msrec['LVL6'];
            $_allGroups[] = $msrec['LVL7'];

			/* EVEN NIET ivm mysqli recurvice probleem... SERVERMAPPING los ophalen na vorige query
						if (isset($msrec['SERVERMAPPING'])) {
							$servers = explode(',', $msrec['SERVERMAPPING']);
							if (!match_ipaddress($_SERVER['SERVER_NAME'], $servers) and !match_ipaddress($_SERVER['SERVER_ADDR'], $servers)) {
								$addgroup = false;
							}
						}
			*/
        }
        $_allGroups = array_unique($_allGroups);
        $_allGroups = array_filter($_allGroups, function($value) { return !empty($value); });
        // Sorting is important because of issues with mysql IN function:
        // A IN (2,1) does not work, A IN (1,2) does work.
        asort($_allGroups);
    } else {
		$_allGroups = [$usergroupid];
	}
    $_result = implode(',', $_allGroups);
    return $_result;
}

function CheckboxList($items, $cbname, $defaultcheckedall=false, $includecheckall=false, $keyisname=false) {
	$htm = '';
	if ($includecheckall) {

	}
	$_checked = '';
	if ($defaultcheckedall) $_checked = ' checked';
	if ($items) {
		foreach($items as $key => $name) {
			if ($keyisname)
				$key = $name;
			$htm .= '<label class="checkboxitem"><input type="checkbox" class="xcheckbox" name="'.$cbname.'[]" value="'.$key.'" '.$_checked.'> '.$name.'</label><br/>';
		}
	}
	return $htm;
}

function MakeHiddenFields($fields) {
	$htm = '';
  if ($fields) {
  	foreach($fields as $key => $value) {
  		$htm .= "<input type='hidden' name='$key' value='$value'>";
  	}
  	return $htm;
  }
}

function ShowColumnSourceArray($columnSourceArray, $columnname) {
    $r .= '<script type="text/javascript"> // <![CDATA[
    var ColumnSource_'.$columnname.' = new Array( ';

    while($rec = $columnSourceArray->FetchNextObject(true)) {
        $r .= 'new Array("'.$rec->COLUMNNAME.'","'.addslashes($rec->VALUE).'","'.$rec->ID.'")';
        if (!$columnSourceArray->EOF)
            $r .= ',';
    }
    $r .= ');
    // ]]></script>';
    return $r;
}


function RecordSetAsSelectList(&$recordset, $keycolumn, $namecolumn, $currentvalue, $id='idselect', $selectname='', $attr='', $required=false, $data, $multiselect=false) {
	if ($required) {
		$_required_data = "data-allow-clear='false'";
		$_required_class = 'required';
	}
	$_placeholder = strtolower(str_replace(array('_','-'), ' ', $selectname));
	$_dataTag = '';

	if (is_array($data) and count($data) > 0) {
		foreach($data as $_key => $_value) {
			$_dataTag .= "data-{$_key}=\"{$_value}\" ";
		}
	}
	if (is_array($data) and !isset($data['placeholder'])) {
		$_placeholder = strtolower(str_replace(array('_','-'), ' ', $selectname));
		$_dataTag .= "data-placeholdertext=\"{$_placeholder}\" ";
	}
	$htm = '';
	$size = 1;
	if ( $required == false) {
		$htm .= "<option value=''>&nbsp;</option>";
	}
	if ($recordset) {
	  	$recordset->MoveFirst();
		while ($rec = $recordset->FetchNextObject(false)) {
			// If it is a multiselect, we have to set the values with javascript
			if ($currentvalue == $rec->$keycolumn and !$multiselect)
				$selected = ' selected';
			else
				$selected = '';
			$htm .= "<option value=\"".$rec->$keycolumn."\"$selected>";
			$htm .= $rec->$namecolumn;
			$size = strlen($rec->$namecolumn) > $size ? strlen($rec->$namecolumn) : $size;
			$htm .= "</option>";
		}
	}
	$_pxsize = $size * 0.6;
	$starthtm = "<select name='{$selectname}' xdata-select_width='{$_pxsize}em' $_dataTag class='select2 add-items form-control {$_required_class}' id='$id' $attr $_required_data>";
	$htm = $starthtm . $htm;
	$htm .= "</select>";
	return $htm;
}

function ObjectSetAsSelectList($recordset, $keycolumn, $namecolumn, $currentvalue, $id='idselect', $selectname='', $attr='') {
	$htm = "<select size='1' name='$selectname' class='form-control' id='$id' $attr>";
	$htm .= "<option value=''>&lt;Kies $selectname&gt;</option>";
	foreach($recordset as $record) {
		$rec = $record->record;
		if ($currentvalue == $rec->$keycolumn)
			$selected = ' selected';
		else
			$selected = '';
		$htm .= "<option value=\"".$rec->$keycolumn."\"$selected>";
		$htm .= $rec->$namecolumn;
		$htm .= "</option>";
	}
	$htm .= "</select>";
	return $htm;
}

function AssocSetAsSelectList($assoc, $currentvalue, $id='idselect', $selectname='', $attr='') {
	$htm = "<select size='1' name='$selectname' class='form-control' id='$id' $attr>";
	$htm .= "<option value=''></option>";
	foreach($assoc as $_key => $_record) {
		if ($currentvalue == $_key)
			$selected = ' selected';
		else
			$selected = '';
		$htm .= "<option value=\"".$_key."\"$selected>";
		$htm .= $_record;
		$htm .= "</option>";
	}
	$htm .= "</select>";
	return $htm;
}

function SQLAsSelectList($db, $query, $keycolumn, $namecolumn, &$currentvalue, $id='idselect', $selectname='', $extra='', $required=false, $cachedata=false) {
    global $_CONFIG;

    if ($cachedata)
        $rs = $db->CacheExecute($_CONFIG['cachequeries_timeout'], $query);
    else
        $rs = $db->Execute($query);

    $keycolumn = str_replace('DISTINCT ', '', $keycolumn);
    if ($rs === false) die("query failed: ". $query);
    if ($selectname == '')
        $selectname = $keycolumn;
    if (!isset($namecolumn))
        $namecolumn = $keycolumn;
    if ($required == true)
        $_req = 'required';
    $htm = '';
    if ($required == false) {
        $htm .= '<option value=\'\'></option>';
    }
    $_size = 1;
    while ($rec = $rs->FetchRow(false)) {
        // Handle both string and numeric indexes for $keycolumn and $namecolumn
        $keyvalue = isset($rec[$keycolumn]) ? $rec[$keycolumn] : (isset($rec[0]) ? $rec[0] : null);
        $namevalue = isset($rec[$namecolumn]) ? $rec[$namecolumn] : (isset($rec[1]) ? $rec[1] : null);

        $keyvalue = htmlentities($keyvalue, ENT_QUOTES, "UTF-8");
        if (!isset($firstkeyvalue)) $firstkeyvalue = $keyvalue;
        if ( ($currentvalue != '') and ($currentvalue == $keyvalue) ) {
            $selected = ' selected';
            $currentvalue = $keyvalue;
        } else {
            $selected = '';
        }
        $htm .= "<option value=\"$keyvalue\"$selected>";
        if ( $namevalue ) {
            $_size = strlen($namevalue) > $_size ? strlen($namevalue) : $_size;
            $htm .= $namevalue;
        } else {
            $_size = strlen($keyvalue) > $_size ? strlen($keyvalue) : $_size;
            $htm .= $keyvalue;
        }
        $htm .= "</option>";
    }
    $starthtml = "<select size='1' name='$selectname' class='select2 form-control $_req' id='$id' $extra>";
    $htm = $starthtml . $htm;
    $htm .= "</select>";
    return $htm;
}

function TableAsSelectList($db, $tablename, $keycolumn, $namecolumn, $currentvalue, $id='idselect', $selectname='', $whereclause='', $required=false, $extra='', $orderby='', $cachedata=false) {
	$tablename = strtolower($tablename);
	/***
	* Transformeer meerdere kolommen gescheiden door komma naar een CONCAT variant
	*/
	if (strpos($namecolumn, ',') !== false) {
	    $namecolumn = 'CONCAT('.str_replace(',','," ",',$namecolumn).')';
	}
	$sql = " SELECT DISTINCT $keycolumn, $namecolumn FROM $tablename";
	if ($whereclause <> '')
		$sql .= " WHERE $whereclause";
	if ($orderby <> '')
		$sql .= " ORDER BY $orderby";
	return SQLAsSelectList($db, $sql, $keycolumn, $namecolumn, $currentvalue, $id, $selectname, $extra, $required, $cachedata);
}

function SetAsOptionGroup($setarray, $itemname, $currentvalue, $id='idselect', $selectname='', $required=false, $classes='', $extra='') {
	$_class = "xradio ";
	if ( $required )
		$_class .= "required";
	// $htm = "<div class=\"$_class\">";
	foreach($setarray as $setstring) {
		$set = explode('=',$setstring);
		if ($currentvalue == $set[0])
			$selected = ' checked="checked"';
		else
			$selected = '';
		if ($set[1] == '')
			$screenvalue = $set[0];
		else
			$screenvalue = $set[1];
		$htm .= "<input type=\"radio\" id=\"$id:$set[0]\" $selected name=\"$selectname\" value=\"$set[0]\" class=\"xicheck $classes\" $extra />";
		$htm .= " <label for='$id:$set[0]'>";
		$htm .= "$screenvalue</label> &nbsp;";
	}
	// $htm .= "</div>";
	return $htm;
}

function RecordSetAsOptionGroup($recordset, $keycolumn, $namecolumn, $currentvalue, $id='idselect', $selectname='', $required=false, $classes='', $extra='') {
	$_class = "xradio ";
	if ( $required )
		$_class .= "required";
	$htm = "<span class=\" $_class\">";

	if ($recordset) {
		$recordset->MoveFirst();
	  	while ($rec = $recordset->FetchNextObject(false)) {
			if ($currentvalue == $rec->$keycolumn)
				$selected = ' checked="checked"';
			else
				$selected = '';
			$htm .= "<input type=\"radio\" id=\"$id:{$rec->$keycolumn}\" $selected name=\"$selectname\" value=\"{$rec->$keycolumn}\" class=\"xicheck $classes\" $extra />";
			$htm .= " <label for='{$id}:{$rec->$keycolumn}'>";
			$htm .= "{$rec->$namecolumn}</label> &nbsp; &nbsp;";
		}
  	}
	$htm .= "</span>";
	return $htm;
}

function TableAsCheckboxGroup($db, $tablename, $keycolumn, $showcolumn, $currentvalue, $id='idselect', $selectname='', $required=false, $classes='', $extra='', $whereclause='', $orientation='horizontal') {
	$query = "SELECT $keycolumn, $showcolumn FROM $tablename";
	if ($whereclause != '') $query .= ' WHERE '.$whereclause;
	$rs = $db->GetAll($query);
	foreach($rs as $rec) {
		$setarray[] = $rec[0].'='.$rec[1];
	}

	return SetAsCheckboxGroup($setarray, $itemname, $currentvalue, $id, $selectname, $required, $classes, $extra, $orientation);
}

function RecordSetAsCheckboxGroup($recordset, $keycolumn, $showcolumn, $currentvalue, $id='idselect', $selectname='', $required=false, $classes='', $extra='', $orientation='horizontal') {
	if ($recordset) {
	  	$recordset->MoveFirst();
	  	while (!$recordset->EOF) {
	  		$setarray[] = $recordset->fields[0].'='.$recordset->fields[1];
			$recordset->MoveNext();
		}
		return SetAsCheckboxGroup($setarray, $itemname, $currentvalue, $id, $selectname, $required, $classes, $extra, $orientation);
	} else {
		return false;
	}
}

function SetAsCheckboxGroup($setarray, $itemname, $currentvalue, $id='idselect', $selectname='', $required=false, $classes='', $extra='', $orientation='horizontal') {
	$htm = "<input type=\"hidden\" id=\"binary_$id\" name=\"$selectname\" value=\"$currentvalue\" />";
	$htm .= "<span id=\"$id-E\"></span>";
	if ($orientation == 'horizontal') {
		$htm .= "<span id=\"div_$id\" class=\"";
		if ( $required )
			$htm .= " required";
		$htm .= "\">";
		foreach($setarray as $setstring) {
			$set = explode('=',$setstring);

			if ($set[1] == '') {
				$screenvalue = $set[0];
			} else {
				$parts = explode('|', $set[1]);
				$screenvalue = $parts[0];
			}

			if (!empty($currentvalue) and (intval($currentvalue) & intval($set[0])) == $set[0]) {
				$selected = ' checked="checked"';
			} else {
				$selected = '';
			}
			$htm .= '<label>';
			$htm .= "<input type=\"checkbox\" $selected data-id=\"$id\" value=\"$set[0]\" style=\"position: absolute; opacity: 0;\" class=\"icheck binarycheckbox $classes\" $extra />&nbsp;";
			$htm .= str_replace(' ', '&nbsp;', $screenvalue).'&nbsp;</label>&nbsp;';
		}
		$htm .= "</span>";
	} else {
		$htm .= "<input type=\"button\" onclick=\"if ($('div_$id').style.display=='block') $('div_$id').style.display = 'none'; else $('div_$id').style.display='block';\" class=\"binarycheckboxgroup_button\" value=\"Items kiezen\" />";
		$htm .= "<div id=\"div_$id\" class=\"binarycheckboxgroup alwayscheckrequired\">";
		$htm .= "<div id=\"$id\" class=\"binarycheckboxgroup_content";
		if ( $required )
			$htm .= " required";
		$htm .= "\">";
		foreach($setarray as $setstring) {
			$set = explode('=',$setstring);

			if ($set[1] == '')
				$screenvalue = $set[0];
			else
				$screenvalue = $set[1];

			if ((intval($currentvalue) & intval($set[0])) == $set[0]) {
				$selected = ' checked="checked"';
				$caption .= $screenvalue.', ';
			}
			else
				$selected = '';
			$htm .= "<label><input type=\"checkbox\" $selected onclick=\"updateCheckboxValue('$id')\" screenvalue=\"$screenvalue\" value=\"$set[0]\" class=\"binarycheckbox $classes\" $extra />&nbsp;";
			$htm .= str_replace(' ', '&nbsp;', $screenvalue).'</label><br /> ';
		}
		$caption = substr($caption,0,-2);
		$maxlengthcaption = 50;
		if (strlen($caption) > $maxlengthcaption) $caption = substr($caption,0,$maxlengthcaption) . '...';
		if ($caption=='') $caption = '&lt;geen items&gt;';
		$htm .= "</div><span class=\"actions\"><input type=\"button\" onclick=\"if ($('div_$id').style.display=='block') $('div_$id').style.display = 'none'; else $('div_$id').style.display='block';\" value=\"Ok\" /> &nbsp;<input type=\"button\" onclick=\"cb_selectChange('div_$id', true);updateCheckboxValue('$id')\" value=\"Alles\" />  &nbsp;<input type=\"button\" onclick=\"cb_selectChange('div_$id', false);updateCheckboxValue('$id')\" value=\"Niets\" /></span>";
		$htm .= "</div>";
		$htm .= " <span id=\"divtext_$id\" class=\"binarycheckboxgroup_text\">$caption</span>";
	}
	return $htm;
}

function SetAsSelectList($setarray, $itemname, $currentvalue, $id='idselect', $selectname='', $required=false, $classes='', $extra='') {
	$htm = '';
    if ( !$required ) {
      $htm .= "<option value=''></option>";
    }
	foreach($setarray as $setstring) {
		$set = explode('=',$setstring);
		if ($currentvalue == $set[0])
			$selected = ' selected';
		else
			$selected = '';
		$htm .= "<option value=\"$set[0]\"$selected>";
		if ($set[1] == '') {
			$screenvalue = $set[0];

		} else {
			$screenvalue = $set[1];
		}
		$_size = strlen($screenvalue) > $_size ? strlen($screenvalue) : $_size;
		$htm .= $screenvalue;
		$htm .= "</option>";
	}
	$_pxsize = $_size * 15;
	$starthtm = "<select size='1' data-select_width='$_pxsize' name='$selectname' id='$id' class='select2 form-control $classes' $extra>";
	$htm = $starthtm . $htm;
	$htm .= "</select>";
	return $htm;
}

function SetAsJson($setarray) {
	$htm = "{";
	foreach($setarray as $setstring) {
		$set = explode('=',$setstring);
        $htm .= '"'.$set[0].'":"';
		if ($set[1] == '')
			$screenvalue = $set[0];
		else
			$screenvalue = $set[1];
		$htm .= $screenvalue."\",";
	}
	$htm = substr($htm, 0, -1);
	$htm .= "};";
	return $htm;
}

function StripHiddenFields($fieldsarray, $takeoldvalues=false) {
	foreach($fieldsarray as $fieldname => $fieldvalue) {
		if ((strpos($fieldname, '_hdn') === false) and (strpos($fieldname, '__') === false) and ($fieldname != 'MAX_FILE_SIZE')) {
            if (isset($fieldsarray['_old'.$fieldname]) and $takeoldvalues)
                $returnfields[$fieldname] = $fieldsarray['_old'.$fieldname];
            else {
				$returnfields[$fieldname] = $fieldvalue;
			}
		}
	}
	return $returnfields;
}

function StripFields($sourcearray, $keepkeys) {
	foreach($sourcearray as $fieldname => $fieldvalue) {
	  if ( in_array_cin($keepkeys, $fieldname) ) {
			$returnfields[$fieldname] = $fieldvalue;
		}
	}
	return $returnfields;
}

function RemoveTablePart($recordArray) {
    $result = Array();
	foreach($recordArray as $fieldname => $fieldvalue) {
		if ( strpos($fieldname, '!') !== false ) {
		    $values = explode('!', $fieldname);
		    $result[$values[1]] = $fieldvalue;
		} else {
		    $result[$fieldname] = $fieldvalue;
		}
	}
    return $result;
}

function CollectAndStripSubrecords(&$recordArray) {
	$subrecordsfound = false;
	foreach($recordArray as $fieldname => $fieldvalue) {
		if ((strpos($fieldname, '!') !== false) or (strpos($fieldname, '_hdn') !== false) ) {
			if (strpos($fieldname, '!') !== false) {
			 	$values = explode('!', $fieldname);
			 	if (substr($values[1],-4,-2) == '[]')
			 	   $values[1] = substr($values[1],0,-2);
  				$returnfields[ $values[0] ][ $values[1] ] = $fieldvalue;
  				unset($recordArray[$fieldname]);
				$subrecordsfound = true;
  			} else {
  				$returnfields[$fieldname] = $fieldvalue;
  			}
		}
	}
	if ($subrecordsfound)
		return $returnfields;
	else
		return false;
}

function GetQueryRecordAsObject($db, $query, $keyfields=false, $toupper=true) {
	if (!$db) return;
	$rs = $db->Execute($query, $keyfields);
	if ($rs === false) die("query failed: ". $query);
	return $rs->FetchNextObject($toupper);
}

function GetRecordAsArray($db, $tablename, $keyfields, $toupper=true, $hashedkey=false, $datatype='C') {
	$keyfieldsAsSQL = $db->MakeFieldSetValueString($keyfields, and:true, datatype:$datatype);

	$query  = ' SELECT * FROM '.strtolower($tablename);
	$query .= ' WHERE '.$keyfieldsAsSQL;
	$rs = $db->userdb->Execute($query);
	if ($rs === false) die("query failed: ". $query);

	// Get all records as an associative array
	$result = $rs->GetAll();

	// If $toupper is true, convert column names (keys) to uppercase
	if ($toupper) {
		$result = array_map('array_change_key_case', $result, array_fill(0, count($result), CASE_UPPER));
	}

	// Return the resulting array
	return $result;
}

function GetQueryAsRecordSet($db, $query, $keys = false) {
	$rs = $db->Execute($query, $keys);
	if ($rs === false) die("query failed: ". $query);
	return $rs;
}

function GetRecordSet($db, $tablename, $keyfields=false, $orderby=false) {
	$query  = ' SELECT * FROM '.strtolower($tablename);

	if ($keyfields)
		$keyfieldsAssignSQL = $db->MakeFieldSetValueString($keyfields, $and=true);

	if ($keyfields)
		$query .= ' WHERE '.$keyfieldsAssignSQL;
	if ($orderby)
		$query .= ' ORDER BY '.$orderby;

	return GetQueryAsRecordSet($db->userdb, $query);
}

function deleteResources($db, $RecordArray) {
	global $_sqlDeleteResources;
	foreach($RecordArray as $recid => $onoff) {
		if (substr($recid, 0, 4) == 'rec:') {
			$recordid = substr($recid, 4, 32);
			$recs .= "'$recordid',";
		}
	}
	$recs = substr($recs, 0, -1);
	$db->Execute($_sqlDeleteResources, array($recs));
}

function AllKeysExists($db, $tablename, $fieldvalues, $keys=false) {
    if (!is_array($keys))
        $keys = array_toupper($db->MetaPrimaryKeys(strtolower($tablename)));
    $result = true;

    foreach($keys as $key) {
        if ($fieldvalues[$key] == '') {
            $result = false;
            break;
        }
    }
    return $result;
}
