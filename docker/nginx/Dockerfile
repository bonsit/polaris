FROM nginx:1.25.4-alpine3.18-slim
#${NGINX_VERSION}
LABEL maintainer="bartb <https://github.com/bartb>"
# docker build -t bartbons/nginx .
# docker push bartbons/nginx

# In what type of environment is this container running? dev, tst or prd
ARG ENVIRONMENT
ENV ENVIRONMENT=${ENVIRONMENT}

# Copy the Nginx configuration
COPY conf/nginx/* /etc/nginx/
COPY conf/nginx/sites-enabled/maxia.nl.${ENVIRONMENT} /etc/nginx/sites-available/maxia.nl.${ENVIRONMENT}.conf
RUN mkdir /etc/nginx/sites-enabled
RUN ln -s /etc/nginx/sites-available/maxia.nl.${ENVIRONMENT}.conf /etc/nginx/sites-enabled/maxia.nl.${ENVIRONMENT}.conf

# Skipping for now... script does not work with Alpine shell
# RUN apk add curl
# RUN curl -Ls https://download.newrelic.com/install/newrelic-cli/scripts/install.sh | sh && NEW_RELIC_API_KEY=${NEW_RELIC_API_KEY} NEW_RELIC_ACCOUNT_ID=${NEW_RELIC_ACCOUNT_ID} NEW_RELIC_REGION=EU /usr/local/bin/newrelic install -n nginx-open-source-integration -y

WORKDIR /var/www/html

# Generate JS, CSS and doc files
COPY resources resources
COPY mkdocs.yml .
COPY mkdocs_enduser.yml .
COPY Gruntfile.js .
COPY *.json .
RUN apk add --no-cache nodejs npm py-pip git yarn
RUN pip install mkdocs mkdocs-material mkdocs-mermaid2-plugin
RUN npm install
RUN yarn
RUN node_modules/grunt-cli/bin/grunt --version
RUN node_modules/grunt-cli/bin/grunt --environment=production
RUN cat yarn.lock

# Copy the public and client module files
COPY public .
COPY conf/polaris/maxia.nl ./conf

# Reset the files to the nginx owner, but only if it is not already so.
# Use find to speed up the process.
RUN  find /var/www/html/assets -not -user nginx -exec chown nginx {} \;
