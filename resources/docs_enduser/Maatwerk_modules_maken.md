## Opbouw van een module

In dit document beschrijven we de opbouw van de module MijnTestModule van de applicatie MijnApp. De module bestaat uit 12 verschillende bestanden die samen het dialoogformulier vormen. Deze bestanden staan allemaal in de folder <polaris>/modules/.

```html
module.mijn_app.php
module.mijn_app
        css
        default.css
    html
        _mijntest_a.tpl.php
        _mijntest_b.tpl.php
        _shared.tpl.php
        mijntestmodule.tpl.php
    js
        sharedlib.js
        mijntestmodule.js
```

Het bestand module.mijn_app.php wordt door Polaris aangeroepen. Hierin is de module klasse gedefinieerd die door Polaris als object geïnstantieerd wordt. Van dit object wordt de Show methode opgeroepen die ervoor zorgt dat het dialoogformulier op het scherm komt.

In de gelijknamige folder module.mijn_app staan drie standaard folders: css, html en js.
In de css folder staat het bestand default.css. Hierin staan alle stijldefinities van het dialoogformulier.
In de folder html staan de bestanden met HTML code. Deze bepalen de structuur van het dialoogformulier.
In de folder js staan de javascript bestanden die zorgen voor de interactie en vulling van het formulier.

### Koppeling met Polaris
In het bestand module.mijn_app.php staat de functie Show, die door Polaris wordt aangeroepen. We zien dat deze functie twee parameters heeft: $outputformat en $rec. $outputformat is standaard “xhtml”, maar kan door Polaris ook een andere waarde krijgen zoals JSON of PHP. Op deze manier weet de module op welke wijze hij zijn data moet tonen. (In de module MijnTestModule module wordt alleen xhtml verwerkt.) De parameter $rec wordt door Polaris gebruikt om het geselecteerde record door te geven, in het geval dat de module gebruikt wordt in combinatie met een Listview.

```php
<?
function Show($outputformat='xhtml', $rec=false) {
    global $_GVARS;

    parent::Show($outputformat, $rec);
    $_GVARS['_includeJavascript'][] = $_GVARS['serverpath'].'/javascript/pure/pure2.js';
    $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/sharedlib.js';
    $_GVARS['_includeJavascript'][] = $this->modulepath.'/js/mijntestmodule.js';
    switch ($this->moduleid) {
    case 1:
        $this->DisplayMijnTestModule();
    break;
    }
}
```

Tevens worden de algemene Javascript bestanden toegevoegd, zodat deze optimaal worden doorgestuurd naar de browser (onderaan, net voor de `</body>` tag; omdat dit sneller werkt dan bovenaan in de header tag).

De moduleid is gemaakt om ervoor te zorgen dat je meerdere dialogen in een module kunt zetten. Bij een Polaris form kun je naast de module ook een moduleid opgeven. Deze wordt door Polaris doorgegeven aan de module zodat de module weet welk dialoogformulier hij moet tonen. In ons voorbeeld is er maar één dialoogformulier gedefinieerd dat getoond wordt met de functie DisplayMijnTestModule().

### Het tonen van een dialoogformulier
In de functie DisplayMijnTestModule worden de drie Javascript bestanden toegevoegd aan de globale variabele _includeJavascript. Polaris zorgt ervoor dat deze Javascript bestanden optimaal worden doorgestuurd naar de browser (d.w.z. onderaan de webpagina en minified [ http://code.google.com/p/minify/ ]).

```php
<?
function DisplayMijnTestModule() {
    global $_GVARS;

    /***
    * Mijn Test Module
    */
    if ($this->outputformat == 'xhtml') {
        $this->smarty->display("mijntestmodule.tpl.php");
    }
}
```

Met de regel $this->smarty->display("mijntestmodule.tpl.php") wordt het formulier daadwerkelijk getoond. ‘this’ verwijst naar de module zelf en ‘smarty’ verwijst naar het Smarty object van de module. Smarty is een template engine die ons in staat stelt om bedrijfslogica-code en presentatie-code netjes te scheiden.

Met de functie display kunnen we een smarty template op het scherm tonen. Zoals we dadelijk kunnen zien is een smarty template een gewoon HTML bestand, aangevuld met Smarty tags.

### De opbouw van een dialoogformulier
We hebben ervoor gekozen om de HTML code te verdelen over verschillende bestanden. Het zou allemaal in één bestand kunnen, maar dan wordt het erg onoverzichtelijk. Het hoofdbestand is mijntestmodule.tpl.php.

Met de Smarty tag ‘include’ kunnen we andere bestanden invoegen aan het hoofdbestand. Zo zien we hieronder dat er vier subbestanden worden ingevoegd: _shared.tpl.php, _mijntest_a.tpl.php en _mijntest_b.tpl.php. In deze subbestanden staat ook HTML code met Smarty tags. De reden dat de subbestandnamen beginnen met een liggend streepje is omdat we zo snel onderscheid kunnen maken tussen hoofdbestand en subbestanden.


Verder zien we in het hoofdbestand wat `<div>` tags om HTML code te groeperen en `<button>` tags voor het weergeven van de Opslaan en Annuleren knoppen.

#### mijntestmodule.tpl.php:

``` html
{include file="_shared.tpl.php"}

<div id="formview" class="frmMijnTestModule">

    {include file="_mijntest_a.tpl.php"}
    {include file="_mijntest_b.tpl.php" type=""}

    <div class="buttons">
        <button id="btnSAVE" type="button" class="savebutton positive disabled" accesskey="F9">Opslaan</button>
        <button id="btnCANCEL" type="button" class="cancelbutton negative" accesskey="F2">Annuleren</button>
    </div>
</div>
```

In onderstaande afbeelding zien we het dialoogformulier en welke subbestanden verantwoordelijk zijn voor de invulling van welk onderdeel. De _shared.lib.tpl hebben we maar weergegeven als ‘punt’ omdat deze onzichtbare elementen bevat.

#### _shared.tpl.php:

``` html
<link rel="stylesheet" type="text/css" media='all' href="{$moduleroot}/css/default.css" />
<input type="hidden" name="_hdnProcessedByModule" value="true" />
<span id="ajaxfeedback"></span>
<div id="errorMessage"></div>
```

In het bestand _shared.tpl.php staat als eerste de verwijzing naar het stijlbestand default.css.
Vervolgens wordt het verborgen formulierveld _hdnProcessedByModule op true gezet. Dit betekent dat Polaris zelf niets doet wanneer het formulier wordt opgeslagen (ge-submit), maar de afhandeling van het opslaan ($_POST) van het formulier overlaat aan de module.
Verder staan er wat tags waar eventuele foutmeldingen en ajax feedback worden weergegeven.

#### _inschrijving_selectie.tpl.php:

``` html
<div id="inschrijving_relatie" class="alignlabels">
    ...
    <div class="formviewrow"><label class="label">Inschrijfnr: </label>
        <div class="formcolumn"><input tabindex="1" type="search" id="_fldZOEKINSCHRIJVING" size="12" />
            <button tabindex="9" id="btnSEARCHINSCHRIJVING" type="button">Zoek</button>
        </div>
    </div>

    <div class="formviewrow"><label class="label">Cursuscode: </label>
        <div class="formcolumn"><label class="CURSUSCODE"></label></div>
    </div>
    <div class="formviewrow"><label class="label">Cursistnr: </label>
        <div class="formcolumn"><label class="CURSISTNR"></label></div>
    </div>
    <div class="formviewrow"><label class="label">Datum inschrijving: </label>
        <div class="formcolumn"><label class="DATUMINSCHRIJVING"></label></div>
    </div>
    ...
</div>
```

Van het bestand _inschrijving_selectie.tpl.php hebben we een stukje broncode getoond. Dit betreft het invulveld Inschrijfnr en de display velden Cursuscode, Cursistnr en Datum inschrijving. Let op, het invulveld met id _fldZOEKINSCHRIJVING heeft geen name property, omdat het inschrijfnummer niet opgeslagen hoeft te worden. Het veld wordt gebruikt om een inschrijving mee te zoeken (via JavaScript).

De bestanden _examenaanmeldingen.tpl.php en _examenselectie.tpl.php bevatten dezelfde soort HTML code als  het vorige bestand. We beschrijven die HTML code op een later moment.

Voor nu is het voldoende om te weten dat het hoofdbestand aanmeldingen.tpl.php onderstaand formulier als resultaat heeft. (Uiteraard in combinatie met de CSS stijlen van het bestand default.css.)

Het formulier is leeg en doet verder nog niets. De JavaScript zorgt ervoor dat het formulier reageert op input van de gebruiker en dat het de noodzakelijke acties onderneemt.

### Actie!

Voordat we de javascript functies gaan beschrijven kijken we eerst even naar de opbouw van de Javascript bestanden en onderlinge relatie:
sharedlib.js => algemene functies die gebruikt kunnen worden bij verschillende dialoogformulieren.
onderwijs.sharedlib.js => algemene functies die gebruikt kunnen worden bij de module Onderwijs. Nu nog leeg.
aanmelding.js => definitie van de hoofdacties, zoals: Opslaan, Annuleren en Verwijderen.
inschrijvingselectie_form.js => functies behorende bij het formulieronderdeel _inschrijving_selectie.tpl.php.
examenaanmelding_form.js => functies behorende bij het formulieronderdeel _examenaanmeldingen.tpl.php.
Om ervoor te zorgen dat een gebruiker kan werken met het formulier dienen er JavaScript functies gekoppeld te worden aan formulier ‘events’. Zo moet bijvoorbeeld een inschrijving gezocht worden wanneer de gebruiker een inschrijfnr intoets en op Enter drukt of op Zoek klikt.


Deze functie zit in het bestand inschrijvingselectie_form.js.

```javascript
<meer code>
...
$(document).ready(function(){
    Onderwijs.InschrijvingSelectieForm.initialiseerEvents();
 });

Wanneer het formulier en alle bestanden geladen zijn, dan gaat de event ready af van het object document ($(document).ready(...)). Aan deze event kunnen we alle initialisatie acties hangen.
De functie initialiseerEvents() ziet er als volgt uit:
    initialiseerEvents: function() {
        var Self = Onderwijs.InschrijvingSelectieForm;

        /* Zoek Inschrijving events */
        $('#btnSEARCHINSCHRIJVING').click(function(e) {
            e.preventDefault();
            Self.zoekInschrijving();
        });
        $("#_fldZOEKINSCHRIJVING").keyup(function(event) {
            if (event.keyCode == 13 /*Enter*/) {
                Self.zoekInschrijving();
            }
        });

        ...meer code...
    }
```

Allereerst wordt op de click event van de button met id btnSEARCHINSCHRIJVING de functie zoekInschrijving() gestart. Hetzelfde wordt gedaan wanneer de gebruiker op enter drukt wanneer het veld _fldZOEKINSCHRIJVING de focus heeft.
De functie zoekInschrijving() is hieronder vereenvoudigd weergegeven:

```javascript
    zoekInschrijving: function() {
        var Self = Onderwijs.InschrijvingSelectieForm;

        var url = “http://<polaris>/services/json/app/onderwijs/form/inschrijvingrelaties/?limit=20”
          +"&qc[]=INSCHRIJFNR&qv[]='" + $('#_fldZOEKINSCHRIJVING').val() +"'";

        $.getJSON(url, function(data, textStatus) {
            if (textStatus == 'success' && data.length > 0) {
                /* Leg data vast als lokale/globale var */
                Self.inschrijvingData = data;
                /* Vul de gevonden inschrijving */
                Self.vulInschrijving();
            } else {
           <toon error>
            }
        });
    }
```

Hier wordt via een zogenaamde Ajax call (getJSON) een webadres opgeroepen. Het webadres is zoiets als:http://<polaris>/services/json/app/onderwijs/form/inschrijvingrelaties/?limit=20&qc[]=INSCHRIJFNR&qv[]=‘56078’

De parameter limit geeft aan hoeveel records Polaris maximaal moet teruggeven. De parameters qc[] en qv[] geeft aan Polaris door op welke velden/waarden er gefilterd moet worden. Er wordt dus een query uitgevoerd die lijkt op SELECT * FROM INSCHRIJVING ... WHERE INSCHRIJFNR = ‘56078’.
Wanneer je dit webadres in een browser invult, dan krijg je het volgende resultaat:

Het resultaat is een Inschrijf record in JSON formaat (JavaScript Object Notation). Polaris weet doordat het adres de optie /services/json/ bevat, hij alleen maar JSON mag teruggeven.
De javascript functie $.getJSON() gaat er vanuit dat het resultaat in JSON formaat is en zal deze automatisch omzetten naar een Javascript object. Zodra de getJSON functie het JSON resultaat heeft ontvangen zal hij de callback functie gaan uitvoeren (die is meegegeven als parameter bij het aanroepen van de getJSON functie).

```javascript
    $.getJSON(url, function(data, textStatus) {
        if (textStatus == 'success' && data.length > 0) {
            /* Leg data vast als lokale/globale var */
            Self.inschrijvingData = data[0];
            /* Vul de gevonden inschrijving */
            Self.vulInschrijving();
        } else {
       <toon error>
        }
    });
```

De callback functie – rood weergegeven – heeft twee parameters: data, waar het JSON resultaat in zit en textStatus, die weergeeft of de Ajax call is gelukt of niet.
Als de parameter textStatus de waarde ‘success’ heeft en er is een record gevonden (data.length > 0) dan wordt het (eerste) data object vastgelegd voor later en de functie vulInschrijving() uitgevoerd.
De functie vulInschrijving() is hieronder vereenvoudigd weergegeven:

```javascript
    vulInschrijving: function() {
        var Self = Onderwijs.InschrijvingSelectieForm;

        var _data = Self.inschrijvingData;

        $('#inschrijving_relatie').autoRender(_data);
        ...
        Self.vulDatums(_data.INSCHRIJFNR);
    },
```

Hier worden de waarden in het (JSON) _data object ingevuld in het DOM element met ID inschrijving_relatie of  de kind-elementen daarvan. Deze autoRender functie wordt zometeen uitgebreid beschreven.
Vervolgens wordt de dropdownbox met examendata gevuld op basis van het huidige inschrijfnummer m.b.v. de functie vulDatums(). Iedere keer wanneer er een andere inschrijving is gevonden moeten ook de bijbehorende datums worden opgehaald.
Automagische vulling van DOM elementen
De functie autoRender() is een functie van de plugin Pure. Hij zorgt ervoor dat je met weinig code bepaalde DOM elementen een waarde kunt geven.
Vroeger ging het zo
HTML:

```html
<div id=”form”>  <label class=”INSCHRIJFNR”><label>  <label class=”CURSISTNR”><label>  <label class=”CURSUSCODE”><label>  …</div>
JS:
var data = [{"INSCHRIJFNR":"56078","CURSISTNR":"525859","CURSUSCODE":"MBOAPO", … }]  /* JSON object */$(“.INSCHRIJFNR”).val(data[0].INSCHRIJFNR);$(“.CURSISTNR”).val(data[0].CURSISTNR);$(“.CURSUSCODE”).val(data[0].CURSUSCODE);…
Met Pure gaat het nu zo
HTML:
<div id=”form”>  <label class=”INSCHRIJFNR”><label>  <label class=”CURSISTNR”><label>  <label class=”CURSUSCODE”><label>  …</div>
```
```js
JS:
var data = [{"INSCHRIJFNR":"56078","CURSISTNR":"525859","CURSUSCODE":"MBOAPO", … }]  /* JSON object */$(“#form”).autoRender(data);
```

HET RESULTAAT (in beide gevallen):

```html
<div id=”form”>  <label class=”INSCHRIJFNR”>56708<label>  <label class=”CURSISTNR”>525859<label>  <label class=”CURSUSCODE”>MBOAPO<label>  …</div>
```

Zeker als je veel velden een waarde moet geven is dit een enorme tijdwinst. Helemaal wanneer je een tabel moet vullen met meerdere records. Je geeft één keer aan hoe een rij van de tabel eruit ziet, en Pure gebruikt die rij bij het vullen van de tabel met de records uit het JSON object. Hierover later meer.

### Het resultaat
Om het hele formulier te voorzien van de juiste gegevens (op basis van het inschrijfnummer) is er een aantal Ajax calls nodig. De eerste hebben we zojuist gezien en er zijn er nog drie: [1] het ophalen van de aanmeldingsdata, [2] het ophalen van de examendata en [3] het ophalen van de laatst gebruikte bankrekening.
Wanneer die drie Ajax calls hun JSON resultaat hebben teruggegeven en vervolgens door Pure (met autoRender) op de juiste plek wordt getoond, is het formulier voorzien van alle relevante informatie.

### Het vullen van een tabel met records
Zoals gezegd kun je met de Pure plugin eenvoudig een HTML tabel vullen met JSON records.

In het geval van de examen-selectie tabel, hebben we onderstaande HTML code. Dit betreft een tabel met een header-rij en een body-rij. In de body-rij staan kolommen (<td>) of input velden die een waarde krijgen door middel van de autoRender() functie.

```html
<div class="selectable">
    <table id="examens" class="examens striped">
      <thead>
        <tr>
          <th></th>
          <th colspan="2">Examen</th>
          <th colspan="2">Deelkwalificatie</th>
          <th>Geannuleerd op</th>
          <th>Reden</th>
          <th>Gegronde reden?</th>
        </tr>
      </thead>
      <tbody style="font-size:0.85em;">
        <tr class="examen">
          <td>
              <input type="hidden" class="plr__record" name="rec[]" />
              <input type="hidden" class="aanmeldingnr" name="aanmeldingnr[]" />
              <input type="hidden" class="datums_examennr" name="datums_examennr[]" />
              <input type="checkbox" class="aangemeld" name="aangemeld[]" />
          </td>
          <td class="examencode"></td>
          <td class="examenomschrijving"></td>
          <td class="deelkwalificatiecode"></td>
          <td class="deelkwalificatieomschrijving"></td>
          <td><input type="text" class="datumannulering date_input" name="datum_annulering[]" value="" size="10" /></td>
          <td><input type="text" class="redenannulering" name="reden_annulering[]" value="" size="15" /></td>
          <td><input type="checkbox" class="gegrondenannulering" name="gegronde_annulering[]" /></td>
        </tr>
      </tbody>
    </table>
</div>
```

Het class attribuut wordt door de autorender functie gebruikt om te achterhalen welk JSON veld ingevuld moet worden.

```javascript
    initialiseer: function() {
     ...
        Self.htmlExamens = $( '#examens tbody' );
        //directive to render the template
        var directivesExamens = {
            'tr' : {
                'examen<-': {
                    'input.aangemeld@id': function(arg){ return arg.item.PLR__RECORDID?'rec@' + Polaris.Base.customUrlEncode(arg.item.PLR__RECORDID):'' },
                    'input.aangemeld@value': 'examen.EXAMENNR',
                    'input.aangemeld@checked': function(arg){ return arg.item.AANGEMELD == 'JA'?"checked":"" },
                    'input.plr__record@value': function(arg){ return (arg.item.AANGEMELD == 'JA')&&(arg.item.PLR__RECORDID!=null)?'rec@' + Polaris.Base.customUrlEncode(arg.item.PLR__RECORDID):'' },
                    'input.aanmeldingnr@value': function(arg){ return (arg.item.AANGEMELD == 'JA')?arg.item.EXAMENAANMELDINGNR:'' },
                    'input.datums_examennr@value': 'examen.EXAMENNR',
                    'td.deelkwalificatiecode': 'examen.DEELKWALIFICATIECODE',
                    'td.deelkwalificatieomschrijving': 'examen.OMSCHRIJVING',
                    'td.examencode': 'examen.EXAMENCODE',
                    'td.examenomschrijving': 'examen.EXAMENOMSCHRIJVING',
                    'input.datumannulering@value': 'examen.DATUM_ANNULERING',
                    'input.gegrondeannulering@value': function(arg){ return arg.item.GEGRONDE_ANNULERING == 'J'?"checked":"" },
                    'input.redenannulering@value': 'examen.REDEN_ANNULERING',
                    '@class': function(arg){
                        //arg => {data:data, items:items, pos:pos, item:items[pos]};
                        var oddEven =  (arg.pos % 2 == 0) ? 'even' : 'odd';
                        var firstLast = (arg.pos == 0) ? 'first': (arg.pos == arg.items.length -1) ? 'last':'';
                        return ' '+oddEven+' '+firstLast;
                    }
                },
            }
        };
        Self.templateExamens = Self.htmlExamens.compile( directivesExamens );
    ...
    },
    vulExamens: function(data) {
        var Self = Onderwijs.ExamenAanmeldingForm;
        Self.htmlExamens = Self.htmlExamens.render( data, Self.templateExamens );
    }
```
