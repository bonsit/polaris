<?php

/**
 * Smarty {json} plugin
 *
 * Type:     function
 * Name:     json
 * Purpose:  fetch json file and  assign result as a template variable (array)
 * @param url (url to fetch)
 * @param assign (element to assign to)
 * @return array|null if the assign parameter is passed, Smarty assigns the
 *                     result to a template variable
 */
function smarty_function_json($params, &$smarty)
{
    if (empty($params['array'])) {
        $smarty->_trigger_fatal_error("[json] parameter 'array' cannot be empty");
        return;
    }

    if(is_callable('json_decode')) {
    	$content = json_encode($params['array']);
    } else {
    	require_once 'JSON/JSON.php';
	 	$json = new Services_JSON();
	 	$content = $json->encode($params['array']);
    }

    return $content;
}