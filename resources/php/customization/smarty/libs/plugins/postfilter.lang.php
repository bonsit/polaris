<?php
/*
 * Smarty plugin
 * -------------------------------------------------------------
 * File:     postfilter.lang.php
 * Type:     postfilter
 * Name:     lang
 * Version:  1.0
 * Date:     August 12, 2002
 * Purpose:  Parses the intermediate tags left by compiler.lang
 *           and replaces them with the translated strings,
 *           according to the $compile_id value (language code).
 *
 * Install:  Drop into the plugin directory, call
 *           $smarty->load_filter('post','lang');
 *            or
 *           $smarty->autoload_filters = array('post' => array('lang'));
 *           from application.
 * Author:   Alejandro Sarco <ale@sarco.com.ar>
 * -------------------------------------------------------------
 */

function smarty_postfilter_lang($tpl_source, Smarty_Internal_Template $template) {
 	include_once(PLR_DIR.'/languages/lang_api.php');
	lang_load($template->language);

	// Replace: ($lang.'some_label')
	// With: 		some_label_translated

    $offset = 0;
    $searchstr = '($lang.';
    $start = strpos( $tpl_source, $searchstr, $offset );
    while ($start > -1 ) {
        $end = strpos($tpl_source, ')', $start );
        $rplstr = substr($tpl_source, $start + strlen($searchstr), $end - ($start + strlen($searchstr)));
    //  echo substr($tpl_source, $start, 10);
        $tpl_source = substr_replace($tpl_source, lang_get($rplstr), $start, $end - ($start - 1));
        $offset = $end - ($start - 1) + 1;
        if ( $offset <= strlen($tpl_source) )
            $start = strpos($tpl_source, $searchstr, $offset);
        else
            $start = -1;
    }
    return $tpl_source;
}

