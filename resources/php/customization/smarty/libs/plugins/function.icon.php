<?php
/*
 * Smarty plugin
 * -------------------------------------------------------------
 * File:     function.icon.php
 * Type:     function
 * Name:     icon
 * Purpose:  creates a font-awesome or material icon tag
 * -------------------------------------------------------------
 */
// function smarty_function_json($params, &$smarty)
function smarty_function_icon($params, $template)
{
    $_iconsize = $params['size']??'1x';
    $_icontype = substr($params['name'], 0, strpos($params['name'], '-'));
    $_iconname = str_replace($_icontype.'-', '', $params['name']);
    if ($_iconname) {
        switch ($_icontype) {
            case 'fa':
                $_result = "<i class='fa-light fa-fw fa-{$_iconsize} fa-{$_iconname}'></i>";
                break;
            case 'md':
            case '':
                $_result = "<i class='material-symbols-outlined md-{$_iconsize}'>{$_iconname}</i>";
                break;
            case 'icon':
                $_result = "<i class='icon {$_iconname}'></i>";
                break;
            default:
                trigger_error("Icon type {$_icontype} not supported");
                return;
        }
    }

    return $_result;
}
