<?php

/*
 * Smarty plugin
 * -------------------------------------------------------------
 * Type:     modifier
 * Name:     get_resource
 * Purpose:  get the right language resource from the string
 * Example:  Resource                   Language    Result
 *           --------                   --------    ------
 *           Sometext                   NL          Sometext
 *           Sometext                   AA          Sometext
 *           NL:Sometext                NL          Sometext
 *           NL:Sometext                EN          <null>
 *           NL:Sometext|EN:Sometext2   EN          Sometext2
 * -------------------------------------------------------------
 */

function smarty_modifier_get_resource($resource, $plang='NL') {
  $result = $resource;
  if ($resource != '') {
    $result = '';
    $languages = explode('|', $resource); // separate all the languages in the string
    foreach ($languages as $language) {
      if ( (substr($language, 0, 3) == $plang.':') or (substr($language, 0, 3) == 'AA:') ) {
        $result = substr($language, 3); // remove AA: from AA:bla;blas
      } elseif (substr($language, 2, 1) != ':')
        $result = $language;
    }
    if ($result == '')
      $result = $language;//'&lt;resource not found in \''.$resource.'\'&gt;';
    elseif (substr($result, 2, 1) == ':')
      $result = substr($language, 3);
  }
  return $result;
}
