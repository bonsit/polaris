<?php

require "adodb-exceptions.inc.php";

if (!defined('ADODB_ERROR_HANDLER_TYPE')) define('ADODB_ERROR_HANDLER_TYPE',E_USER_ERROR);
if (!defined('ADODB_ERROR_HANDLER')) define('ADODB_ERROR_HANDLER','adodb_plr_throw');

class ADODB_PLR_Exception extends ADODB_Exception {
    var $dbms;
    var $fn;
    var $sql = '';
    var $params = '';
    var $host = '';
    var $database = '';
    var $code = '';

	function __construct($dbms, $fn, $errno, $errmsg, $p1, $p2, $thisConnection, $completemsg='')
	{
    	parent::__construct($dbms, $fn, $errno, $errmsg, $p1, $p2, $thisConnection);

		$this->completemsg = $completemsg;
	}
}

/**
* Default Error Handler. This will be called with the following params
*
* @param $dbms		the RDBMS you are connecting to
* @param $fn		the name of the calling function (in uppercase)
* @param $errno		the native error number from the database
* @param $errmsg	the pretty error msg from the database
* @param $p1		$fn specific parameter - see below
* @param $P2		$fn specific parameter - see below
* @param $completemsg	the complete error msg from the database
*/

function adodb_plr_throw($dbms, $fn, $errno, $errmsg, $p1, $p2, $thisConnection, $completemsg) {
    global $ADODB_EXCEPTION;
    global $polaris;

    $GLOBALS['errorhasoccured'] = true;

    if ($polaris) {
        /* Log the error in the Polaris logbase, so we know what's going on */
        $polaris->logUserAction('SQL Error', session_id(), $_SESSION['name'], 'url: '.$_SERVER['REQUEST_URI'].' message: ' .$errno.' '.$errmsg.' '.$completemsg." \r\n\r\n".var_export($_POST, true) );

        $_errmsg = $polaris->getPrettyErrorMessage($errno, $errmsg);

        $assignederror = $polaris->tpl->getTemplateVars('error_message');
        if (!isset($assignederror)) {
            $polaris->tpl->assign('error_no', $errno);
            $polaris->tpl->assign('error_message', $completemsg);
            if (isset($g_lang_strings['error_'.$errno]))
                $polaris->tpl->assign('pretty_error_message', lang_get('error_'.$errno));
            elseif (isset($g_lang_strings[$errno]))
                $polaris->tpl->assign('pretty_error_message', lang_get($errno));
            else
                $polaris->tpl->assign('pretty_error_message', $_errmsg);
            $polaris->tpl->assign('showbackbutton', $_POST['_hdnAction'] == 'save');
        }
    }

    //var_dump($dbms, $fn, $errno, $errmsg, $p1, $p2, $thisConnection);
	if (error_reporting() == 0) return; // obey @ protocol
	if (is_string($ADODB_EXCEPTION))
	    $errfn = $ADODB_EXCEPTION;
	else
	    $errfn = 'ADODB_PLR_Exception';
	throw new $errfn($dbms, $fn, $errno, $errmsg, $p1, $p2, $thisConnection, $completemsg);
}
