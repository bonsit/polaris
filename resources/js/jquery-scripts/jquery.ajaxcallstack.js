/* Usage:
-----------------------------------------------------------------------------------------------
var Stack = new Callstack();
$.getJSON("jsonA.js", {}, Stack.add('requestA'));
$.getJSON("jsonB.js", {}, Stack.add('requestB'));

Stack.onComplete = function(stack) {
   console.log('All request are completed, see:', stack);
   console.log('Response from requestA:', stack.requestA.arguments[0]);
}
-----------------------------------------------------------------------------------------------
Developed by Felix Geisendörfer @ http://www.debuggable.com/
*/

// Code you need to use the above:
// -----------------------------------------------------------------------------------------------
var Callstack = function() {}
$.extend(Callstack.prototype, {
   stack: {_length: 0}
   , add: function(name, callback) {
       if ($.isFunction(name)) {
           callback = name;
       }
       if (arguments[1] && typeof arguments[1] == 'string') {
           name == arguments[1];
       }
       if (name == '_length') {
           throw 'Callstack::add - Forbidden stack item name "_length"!';
       }
       if (!name) {
           name = this.stack._length;
       }
       var item = {
           arguments: []
           , callback: callback
           , completed: false
       };
       this.stack[name] = item;
       this.stack._length++;
       var self = this;
       return function() {
           self.handle(item, arguments);
       }
   }
   , update: function() {
       var completed = true;
       $.each(this.stack, function(p) {
           if (p != '_length') {
               completed = completed && this.completed;
           }
       });
       if (completed == true) {
           delete this.stack['_length'];
           this.onComplete(this.stack);
       }
   }
   , handle: function(item, args) {
       var r;
       if ($.isFunction(item.callback)) {
           r = callback.apply(item.callback, args);
           if (r === false) {
               return r;
           }
       }
       item.arguments = args;
       item.completed = true;
       this.update();
       return r;
   }
   , onComplete: function() {
       alert('Stack done executing!');
   }
   , reset: function() {
       this.stack = {};
   }
});
//-----------------------------------------------------------------------------------------------

/*
I also threw in some good stuff, for example you do not have to explicitly name you stack items, it will use auto-incrementing values automatically if you don't. 
Also: You can pass a callback to the Stack.add() function that acts as a gatekeeper for determining if the stack item has executed successfully. 
If that function returns falls then the stack item will not be thought of as completed and the item can be triggered again using Callstack.handle().

One could also base a little plugin on this that will only trigger a function if all elements in the current selection have triggered a certain event:
Sample usage:
-----------------------------------------------------------------------------------------------
$(function() {
   $('a').stack('click', function(stack) {
console.log('All links in this document where clicked once! See:', stack);
   }, function() {
       // Cancel link default event
       return false;
   });
});
-----------------------------------------------------------------------------------------------
*/

// Plugin code:
// -----------------------------------------------------------------------------------------------
$.fn.stack = function(event, onComplete, fn) {
   var Stack = new Callstack();
   this.each(function() {
       var stackFn = Stack.add();
       $(this)[event](function() {
           var r;
           if ($.isFunction(fn)) {
               r = fn.apply(this, arguments);
           }
           stackFn.apply(this, arguments);
           return r;
       });
   });
   Stack.onComplete = onComplete;
}