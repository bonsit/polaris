/*
 * Dateinput for Jeditable
 *
 * Copyright (c) 2009 Bart Bons - Debster
 *
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 *
 * Depends on Date Input jQuery plugin by Jonathan Leighton:
 *   http://kelvinluck.com/assets/jquery/datePicker/v2/demo/
 *
 * Project home:
 *   http://www.appelsiini.net/projects/jeditable
 *
 * Revision: $Id$
 *
 */
 
$.editable.addInputType('dateinput', {
    /* create input element */
    element : function(settings, original) {
        var input = $('<input />');
        if (settings.width  != 'none') { input.width(settings.width);  }
        if (settings.height != 'none') { input.height(settings.height); }
        input.attr('autocomplete','off');
        $(this).append(input);
        return(input);
    },
    /* attach 3rd party plugin to input element */
    plugin : function(settings, original) {
        settings.onblur = 'cancel';
        $("input", this).date_input();
    }
});