if (typeof(Polaris.Dashboard) == "undefined") {
    Polaris.Dashboard = {};
}

Polaris.Dashboard.NAME = "Polaris.Dashboard";
Polaris.Dashboard.VERSION = "1.0";

try {
    if (typeof(Polaris.Base) === 'undefined') {
        throw "";
    }
} catch (e) {
    throw "Polaris.Dashboard depends on Polaris.Base";
};

Base.update(Polaris.Dashboard, {

    urlPolarisDashboardProcessor: 'dashboard_ajax.php',
    
    initialize: function() {
        var self = Polaris.Dashboard;
        this.hasEffectLib = String.prototype.parseColor != null;
        /***
            round the boxes
        ***/
        //roundClass(null,"contentblock", {bgColor:'#000'});
        /***
            initially hide the AddContent form
        ***/
        self.toggleContentForm();
        self.toggleToggleImage();

        /***
            connect all the eventhandlers to the proper DOM elements
        ***/
        self.initializeEvents();
    },
    
    initializeEvents: function() {
        var self = Polaris.Dashboard;
        if ($('#addcontenttoggle').length) {
            /***
            * Create the button that toggles the AddContent form
            **/
            $('#addcontenttoggle').click( self.toggleContentForm);
    
            /***
            * Create button that toggles the AddContent form
            **/
            $('#PLR_addcontent').click( partial(self.addNewContent, 'PLR'));
            $('#RSS_addcontent').click( partial(self.addNewContent, 'RSS'));
            $('#URL_addcontent').click( partial(self.addNewContent, 'URL'));
    
            /***
            * Windows events (Windows.js)
            **/
            observer = { onEndResize: self.updateWindow };
            Windows.addObserver(observer);
            observer = { onEndMove: self.updateWindow };
            Windows.addObserver(observer);
        }
    },
    
    addWindow: function(id) {
        this.options = {
            title:  "&nbsp;",
            url:    null,
            content:null,
            top:    200,
            left:   100,
            width:  400,
            height: 300,
            show:   false,
            windowmode: "normal"
        };
    	update(this.options, arguments[1] || {});
        if (!id)
            id = Math.round(100000*Math.random());
            
        var theUrl = (!this.options.content)?this.options.url:null;
        if ((theUrl != null) && (theUrl.substring(0,7) != 'http://')) {
            theUrl = document.location.protocol + '//' + document.location.hostname + document.location.pathname.substring(0, document.location.pathname.lastIndexOf('/')) + theUrl;
        }
        this.classname = /Safari/.test(navigator.userAgent)?"mac_os_x":"alphacube";
        if (this.options.windowmode == 'modal') {
            Dialog.alert({url: theUrl, options: {method: 'get'}}
                , {windowParameters: {className:this.classname, width:400}
                , okLabel: "Opslaan"
                , cancelLabel: "Annuleren",
                  ok:function(win){
                    for(var i=0;i<Windows.windows.length;i++) {
                        if (Windows.windows[i].getContent().tagName.toLowerCase() == 'iframe') 
                            Dashboard.frameDocument(Windows.windows[i].getContent()).location.reload();
                    }
                    return true;}
               });
            return false;
        } else {
            var win = new Window(id,
                {className: this.classname
                , title: this.options.title
                , top:this.options.top, left:this.options.left, width:this.options.width, height:this.options.height
                , resizable: true, url:theUrl, showEffectOptions: {duration:0.5}});
            if (this.options.content)
                win.getContent().innerHTML = this.options.content;
            win.setDelegate(this);
            win.setDestroyOnClose();
            
            if (this.options.show)
                win.showCenter(false);
            return win;    
        }
    },
    
    frameDocument: function(IFrameObj) {
        if (IFrameObj.contentDocument) {
            // For NS6
            return IFrameObj.contentDocument; 
        } else if (IFrameObj.contentWindow) {
            // For IE5.5 and IE6
            return IFrameObj.contentWindow.document;
        } else if (IFrameObj.document) {
            // For IE5
            return IFrameObj.document;
        } else {
            return false;
        }
    },

    toggleToggleImage: function() {
        if ($('#addcontentform').is(":visible")) 
            $('#addcontenttoggle').css('background-image', 'url(images/dash_addcontent4.png)');
        else        
            $('#addcontenttoggle').css('background-image', 'url(images/dash_addcontent3.png)');
    },
    
    toggleContentForm: function() {
        if ($('#addcontentform').length)
            $("#addcontentform").slideToggle("fast", Polaris.Dashboard.toggleToggleImage);
    },

    addNewContent: function(theType) {
        var self = Polaris.Dashboard;
        /***
            Make the form persistent in Polaris via Ajax
        **/
        var gotResponseData = function (response) {
//           alert(SerializeJSON(response));
            var res = evalJSONRequest(response);
            /***
                Add the form to the Dashboard
            ***/
            if (res.formid) {
                self.addWindow(res.formid, { 
                    show:true, 
                    title:res.title?res.title:'Dashboard',
                    url:res.url,
                    content:res.rss
                })
            }
        };
        var responseFetchFailed = function (err) {
          alert('Het item kon niet worden toegevoegd. Controleer uw parameters. ('+err+')');
        };
        var linkelem = $("#"+theType+'link');
        
        $.getJSON(self.urlPolarisDashboardProcessor, {
                method: 'dash_addcontent',
                type: theType,
                title: linkelem.value,
                url: linkelem.value
            },
            function(data){
              $.each(data.items, function(i,item){
                $("<img/>").attr("src", item.media.m).appendTo("#images");
                if ( i == 3 ) return false;
              });
            }
        );
    },
    
    canClose: function(win) {
        var self = Polaris.Dashboard;
        /***
            Remove the form from Polaris via Ajax
        **/
        result = confirm("Weet u zeker dat het window "+win.options.title+" wilt verwijderen?");
        if (result) {
            var gotResponseData = function (response) {
                var res = evalJSONRequest(response);
                /***
                    Add the form to the Dashboard
                ***/
                if (res.result) {
                    win.hide();
                    log('window destroyed');
                } else {
                    alert('Dit scherm is vast en kan niet van het Dashboard verwijderd worden.');
                    log('could not destroy window');
                }
            };
            var responseFetchFailed = function (err) {
                alert('Het venster kon niet worden verwijderd. Controleer uw parameters.');
            };
            doSimpleXMLHttpRequest(self.urlPolarisDashboardProcessor, {
                method: 'dash_removecontent',
                id: win.getId()
            }).addCallbacks(gotResponseData, responseFetchFailed);
        }
        return false;
    },
    
    updateWindow: function(eventname, win)  {
        var self = Polaris.Dashboard;
        var gotResponseData = function (response) {
            log('window updated');
        };
        var responseFetchFailed = function (err) {
            logError('window could not be updated in the database: '+err);
        };
        doSimpleXMLHttpRequest(self.urlPolarisDashboardProcessor, {
            method: 'dash_updatewindow',
            id: win.getId(),
            top: getStyle(win.element,'top'), bottom: getStyle(win.element,'bottom'),
            left: getStyle(win.element,'left'), right: getStyle(win.element,'right'),
            width: win.width, height: win.height             
        }).addCallbacks(gotResponseData, responseFetchFailed);
    },

    showActionList: function(id) {
        $(id+'_action').firstChild.innerHTML = '<div class="actionlist"><a href=\"\">Stuur door naar...</a></div>';
    }
});
