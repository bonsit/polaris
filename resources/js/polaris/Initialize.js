if (typeof(Polaris.Initialize) == "undefined") {
    Polaris.Initialize = {};
}

Polaris.Initialize.NAME = "Polaris.Initialize";
Polaris.Initialize.VERSION = "1.0";

try {
    if (typeof(Polaris.Base) === 'undefined') {
        throw "";
    }
} catch (e) {
    throw "Polaris.Initialize depends on Polaris.Base";
}

/***
*
*    Polaris Main Events and Routines, for listview, formview, searchview, ....
*
**/

Base.update(Polaris.Initialize, {

    /***
    The events and routines that should be executed on every invocation of Polaris dialogs
    ***/
    generalMethods: function() {
        var P = Polaris;
        var W = Polaris.Window;
        var B = Polaris.Base;
        var D = Polaris.Dataview;

        log('Polaris.Initialize.generalMethods');

        B.initializeLocalization();

        B.initializeMaintenance();

        B.initializeUserSettings();

        $("#clientselect").on('change', function() {
            $(this).trigger('submit');

        });

        B.initializeAppsMenu();

        B.initializeIconSelect();

        B.initializeIChecks();

        B.initializeSelect2();

        Polaris.Visual.initializeTaskView();

        Polaris.Onboarding.initializeIntrojs();

        // Prevent the Backspace key from going back to the previous webpage
        var cancelBackspace = function(event) { if (event.key == 'Backspace') {return false;}}
        $('select').on('keypress keydown', function(event) { return cancelBackspace(event) });

        Polaris.Visual.initializeCopyToClipboard();

        // Every time a modal is shown, if it has an autofocus element, focus on it.
        $('.modal').on('shown.bs.modal', function() {
            $(this).find('[autofocus]').trigger('focus');
        });
        // For when we use the offcanvas panel:

        // $("#action_insert_side").on('click', function(e) {
        //     e.preventDefault();
        //     let url = _callerquery.replace('const','basicform') + '/insert/';
        //     Polaris.Form.showOverlay(url);
        // });

        // $(document).on('keypress', function(event) {
        //     if (event.key == 'Escape') {
        //         Polaris.Form.hideOverlay();
        //     }
        // });
        // $(document).on('mouseup touchend', function(event) {
        //     var offCanvas = $('.off-canvas')
        //     if (!offCanvas.is(event.target) && offCanvas.has(event.target).length === 0) {
        //         Polaris.Form.hideOverlay();
        //     }
        // });

        /***
         * Cleanup the header-actions, if there are no LI or A elements inside
         */
        if ($('.header-actions li,.header-actions a').length == 0) {
            $('.header-actions').remove();
        }

        /***
        * Setup the Blowup magnifier
        */
        Polaris.Visual.initializeMagnifier();

        /***
        * Setup the extra Popovers function so they don't go beyond the screen limits
        */
        Polaris.Visual.initializePopovers();

        /***
        * Show a property dialog when clicking a formaction, when neccessary
        */
        D.initializeFormActions();

        D.initializeDetailsview();

        $("#recordLimitSelect").on('change', function(e) {
            e.preventDefault();
            var $form = $(this).parents("form");
            $form.find("input[name=_hdnAction]").val('plr_recordlimit');
            $form.find("input[name=_hdnRecordLimit]").val($(this).val());
            $form.trigger('submit');
        });

        $("#grpFilter").on('change', Polaris.Form.groupFilter);

        /***
        * if some action has been executed, then fade the action (breadcrum) after several seconds
        */
        if (typeof(fadingBreadcrum) != "undefined") {
            Polaris.Visual.fadeBreadcrum(fadingBreadcrum);
        }

        $('.bigsearch .dropdown-menu input').on('click', function (e) {
            e.stopPropagation();
        });

        $("#logboek-item-quick-toevoegen").on('click', function(e) {
            e.preventDefault();
            let url = _serverroot+'/app/facility/basicform/logboek/insert/';
            Polaris.Window.showModuleForm(url, function(data) {
            });
        });

        /***
        * Setup the Mask on text fields
        */
        if ($.mask) {
            $.mask.options =  {
                attr: 'alt', // an attr to look for the mask name or the mask itself
                mask: null, // the mask to be used on the input
                type: 'fixed', // the mask of this mask
                maxLength: -1, // the maxLength of the mask
                defaultValue: '', // the default value for this input
                textAlign: true, // to use or not to use textAlign on the input
                selectCharsOnFocus: true, //selects characters on focus of the input
                setSize: true, // sets the input size based on the length of the mask (work with fixed and reverse masks only)
                autoTab: false, // auto focus the next form element
                fixedChars : '[(),.:/_-]', // fixed chars to be used on the masks.
                onInvalid : function(){},
                onValid : function(){},
                onOverflow : function(){}
            };
            $.mask.rules = $.extend($.mask.rules,{
                'z': /[a-z]/,
                'Z': /[A-Z]/,
                'a': /[a-zA-Z]/,
                '*': /[0-9a-zA-Z]/,
                'X': /[0-9A-Z ]/
            });

            // apply date mask
            $('input:text.validation_date,input[type=search].validation_date').setMask('39-19-9999');
        }
        // apply autoNumeric validations/masks
        $('input:text.validation_float,input[type=search].validation_float').autoNumeric({aSep: '.', aDec: ','});
        $('input:text.validation_integer,input[type=search].validation_integer').autoNumeric({aSep: '', aDec: ',', mDec:'0'});

        /***
        * Make the horizontal or vertical application menu
        **/
        P.Visual.makeMenus();

        /***
        * Reset the height of the scrollable listview so that it does not get larger that the browser height
        **/
        D.autoSizeHeight($("#dataform"));

        /***
        * Show the listview (which is hidden via css)
        **/
        $(".spinner").hide();
        $("table.data").show();

        /***
        * make the header and row cells the same size
        **/
        // Still necessary?
        // D.sizeToFitCells($(".listview"));

        /***
        * Load the Pinned Records
        **/
        if ($("#pinnedrecords").length > 0) {
            Polaris.Ajax.loadPins();
        }

        $("#error_showdetails").on('click', function() { $("#error_originalmessage").toggle(); });

        /***
        * initialize the tabs of the form pages
        **/
        P.Visual.initializeTabs($(".tabs-container"));

        /***
        * enable tooltip for hover columns in listview
        */
        D.enableTooltips($('body'));

        /***
        * Protect form buttons, so they don not get pressed twice
        **/
        // EVEN NIET B.mConnect( getElementsByTagAndClassName('input', 'protect'), 'onclick', Polaris.Form.protectButton );

        $('#app_select').on('change', B.appSelect );

        /***
        * Form action buttons [ maximize, separate ]
        **/
        var $tm = $('#toggleMaximize');
        if ($tm.length > 0) {
            $tm.on('click', W.maximizeForm);
            $(document).on('keydown', function(e){ if (e.keyCode == 27 && $('#headercomplete').css('height') == 'auto') { W.maximizeForm(); } });
        }

        /***
        * Setup the 'refresh page' timer
        **/
        if ($('#refresh_interval').length) {
            setTimeout("refreshCountDown("+$('#refresh_interval').options[$('#refresh_interval').selectedIndex].value+")",1000);
            $(".pause_countdown").on('click', P.Timer.pauseCountdown);
        }

        /***
        * Resize the iFrame of a customform to math the size of the window
        **/
        const iframe = $('#customform_iframe');
        if (iframe.length && (iframe.attr('autosize') == 'true')) {
            W.resizeCustomForm(iframe);
            iframe.on('load', function() {
                if (iframe.contents().find(".EmbedFrame-footer").length) {
                    iframe.contents().find(".EmbedFrame-footer")[0].style.display='none';
                }
            })
            $(window).on('resize', function() { W.resizeCustomForm(iframe) });
        }

        /***
        * Setup the 'Select all rows' checkbox
        */
        if ($('#_hdnAllBox').length) { $('#_hdnAllBox').on('click', D.checkAllCheckBoxes ); }

        /***
        * When user clicks the generic delete button, confirm it's action
        **/
        $("#_hdnDeleteButton").on('click', D.deleteRecordsAjax);

        /***
        * When user clicks a delete button of a single item, confirm the action
        **/
        $(".confirmdelete").on('click', function(){ return D.confirmDeleteAction(1); });

        /***
        * When user clicks the generic pin button, pin the selected items
        **/
        $("#_hdnPinButton").on('click',  D.pinRecordAjax );

        $("a.pinrecord").on('click', D.pinRecordAjax);
        $("#pinrecord_single").on('click', D.pinRecordAjax);
        if (Polaris.Base.getLocationVariable('autopin') == 'true') {
            $("#pinrecord_single").trigger('click');
        }

        /* Put the AccessKey in the text of the element, and trigger the click event if the Fx accesskey is pressed */
        Polaris.Visual.initializeAccessKeys();

        /***
        * Initialize the "select record" events when a user has clicked the detail-icon in an autosuggest field
        **/
        $("button.selectrow").on('click', Polaris.Form.selectAutoSuggestRecord);

        /***
        * make the searchpanel expandable
        **/
        Polaris.Visual.initSearchPanel($("#searchpnl"));

        /***
        * Initialize the Navigation menu bar
        **/
        Polaris.Visual.initNavBar($("#sidebar"));

        /***
        * Initialize the collapseble alert boxes
        **/
        Polaris.Visual.initPanelHelpText();

        /***
        * Initialize the searchpanel action
        **/
        $("#search_dataform").on('submit', function(e) {
            D.addQueryStringAsHidden(this);
            D.processSearchValues(this, sessionStorage['casesensitive_'+formid]=='true', sessionStorage['searchfullword_'+formid]=='true', sessionStorage['searchfromstart_'+formid]=='true');
        }).on('keydown', function(event) {
            if (event.key == 'Enter' && !event.ctrlKey) {
                this.submit();
            }
        });

        /***
         * Initialize the Detail searchpanel action
         */
        $("#btnSearchDetailform").on('click', function(e) {
            e.preventDefault();
            var search = $("#fldDetailSearch").val();
            window.location.href = Polaris.Base.addLocationVariable('qd[]', search);
        });
        $("#fldDetailSearch").on('keypress', function(e) {
            if (e.which == 13) {
                $("#btnSearchDetailform").trigger('click');
            }
        });

        /***
        * Initialize the Live Search code
        **/
        if (P.Livesearch && P.Livesearch.liveSearchInit && $('#livesearchimage').length) { P.Livesearch.liveSearchInit(); }

        $("#showtimertoggle a").on('click', function(e) {
            e.preventDefault();
            Polaris.Base.modalDialog($("#pagestats").html(),
                {
                  close: function() {},
                  type: 'type-primary'
                });
        });

        $("#showadvancedinfo a").on('click', function(e) {
            e.preventDefault();
            Polaris.Base.modalDialog($("#advancedinfo").html(),
                {
                  close: function() {},
                  type: 'type-primary'
                });
        });

        $("a.popbox").on('click', function(event) {
            event.preventDefault();
            P.Form.showAsPopBox('', $(this).attr('href'));
        });
        $(".togglemaster").on('click', function(event) {
            event.preventDefault();
            if ($(this).parents('a').hasClass('selected')) {
                $("#masterform,#masterrecordlink").toggle('appear', function() { $(window).resize() });
            } else {
                window.location.href = $(this).attr('link');
            }
        });

        $("#closewindow").on('click', function() {
            Polaris.Form.closePopup();
        });

        $("a.cancelbutton").on('click', function(event) {
            var isInIFrame = (window.parent && window.location != window.parent.location) ? true : false;
            if (isInIFrame) {
                event.preventDefault();
                Polaris.Form.closePopup();
            } else {
                // continue with normal action (href)
            }
        });

        $(".popup").on('click', function(event) {
            event.preventDefault();
            P.Base.linkPopup(this);
        });

        /***
        * Adjust the width of the labels so they have the same width
        **/
        //P.Form.adjustLabelWidth($('.alignlabels'));

        /***
        * Initialize all the model diagrams (if they exists)
        */
        if (typeof ftdInit != "undefined") {
            Polaris.Modeler.drawFTD(ftdInit.canvas, ftdInit.object);
        }

        /***
        * Initialize all the Selectsource selects tags on the page
        */
        if (typeof Polaris.Selectsource != "undefined") {
            Polaris.Selectsource.__init__();
        }

        /***
        * put some validation on the form fields
        **/
        if ($.fn.bestupper) $("input.validation_uppercase").bestupper();

        /***
        * Initialize the pinned records (delete action)
        */
        P.Base.initializePinnedRecords();

        /***
        * Start the onload events
        **/
        // var ii = _loadevents_.length;
        // for(var i=0;i<ii;i++) {
        //     _loadevents_[i]();
        // }

        BootstrapDialog.DEFAULT_TEXTS[BootstrapDialog.TYPE_DEFAULT] = 'Informatie';
        BootstrapDialog.DEFAULT_TEXTS[BootstrapDialog.TYPE_INFO] = 'Informatie';
        BootstrapDialog.DEFAULT_TEXTS[BootstrapDialog.TYPE_PRIMARY] = 'Informatie';
        BootstrapDialog.DEFAULT_TEXTS[BootstrapDialog.TYPE_SUCCESS] = 'Succes';
        BootstrapDialog.DEFAULT_TEXTS[BootstrapDialog.TYPE_WARNING] = 'Waarschuwing';
        BootstrapDialog.DEFAULT_TEXTS[BootstrapDialog.TYPE_DANGER] = 'Fout';
        BootstrapDialog.DEFAULT_TEXTS['OK'] = $.t('plr.ok');
        BootstrapDialog.DEFAULT_TEXTS['CANCEL'] = $.t('plr.cancel');
        BootstrapDialog.DEFAULT_TEXTS['CONFIRM'] = $.t('plr.confirm');
    },

    /***

    The events and routines that should be executed on Polaris LISTVIEW dialogs

    ***/
    listViewMethods: function() {
        var D = Polaris.Dataview;

        log('Polaris.Initialize.listViewMethods');

        D.initializeRecordCount();
        D.initializeSortableTable();
        D.initializeColorPickers();
        D.toggleListviewFormActions();
        D.initializeFormulas();
        D.initializeRecordMenu();
        D.initializeStatusLog();

        /***
        * Make the table rows selectable, by clicking on the row or on the accompanied input box
        */
        $('#datalistview').on('click', D.eventDelegate);

        D.initializeKeyTable();

        /***
        * Make sure that the checked rows are highlighted when a user goes back to an older form and the browser
        * restores the checked checkboxes
        */
        D.syncCheckboxesFromGUI();

        // $("#linkedconstructs a").on('click', function(e) {
        //     e.preventDefault();
        //     window.location = $(this).attr('href').replace('__ids__', D.selectedRowsValues('RELATIENR'));
        // });

        /***
        * if some record has changed, then yellowfade the table row
        */
        if (typeof(fadingRowId) != "undefined") {
            D.fadeRow(fadingRowId);
        }
    },

    /***
    The events and routines that should be executed on Polaris FORMVIEW dialogs
    ***/
    formViewMethods: function() {
        var F = Polaris.Form;
        var B = Polaris.Base;

        log('Polaris.Initialize.formViewMethods');

        // When in Formview (in a generated form), check if the user is editing the form
        $("#formview").dirrty({
          preventLeaving: false,
          leavingMessage: 'message',
          onDirty: function(){
            Polaris.Form.userIsEditing = true;
          }
        });

        /***
        * Setup star-rating fields
        */
        $("#formview ul.star-rating").on('click', processRanking);

        $("#btnSaveForm").on('click', function(e) {
            e.preventDefault();
            if ($('.dropzone').length) {
                var myDropzone = Dropzone.forElement("#dropzone");
                myDropzone.processQueue();
            }
            $('form.dataform').trigger('submit');
        });

        $("#btnEditForm").on('click', function(e) {
            e.preventDefault();
            var url = window.location.href.replace('/view/','/edit/');
            window.location.href = url;
        });

        /***
        * Setup add and remove buttons for component block
        */
        $("input.add_item").on('click', function() { Polaris.Components.AddComponentItem(this); });
        $("input.remove_item").on('click', function() { Polaris.Components.RemoveComponentItem(this); });

        /***
        * Setup the Markdown editor
        */
        if ($(".markdownpreview").length > 0) {
            //$(".markdownpreview").markItUp(mySettings); // ?? mySettings
        }

        /***
        * Setup the form Wizard
        */
        if ($('.wizard').length) {
            Polaris.Wizard.initialize('.wizard');
        }

        if ($('.dropzone').length) {
            $("div#dropzone").dropzone({
                url: "/",
                uploadMultiple: false,
                createImageThumbnails: true,
                thumbnailMethod: 'contain',
                resizeMethod: 'contain',
                resizeWidth: 20,
                maxFiles: 1,
                acceptedFiles: '.jpg,.png,.jpeg',
                autoProcessQueue: false,
                paramName: "FOTO", // The name that will be used to transfer the file
                maxFilesize: 5, // MB
                init: function () {
                    this.on("addedfile", function (file) {
                        // When a file is added, convert its data to Base64 and store it in the hidden input field
                        const reader = new FileReader();
                        reader.onload = function(event) {
                            const base64String = event.target.result.split(',')[1];
                            $("#_hdnFOTO").val(base64String);
                            $("#FOTOPREVIEW").hide();
                        };
                        reader.readAsDataURL(file);
                    });
                }
            });
        }
    },

    /***
    The events and routines that should be executed on Polaris SEARCHVIEW dialogs
    ***/
    searchViewMethods: function() {
        var F = Polaris.Form;

        log('Polaris.Initialize.searchViewMethods');

        /***
        * Set the focus on the first empty field
        **/
        F.setFocusFirstField($('#searchpanel'));
    },

    /***
    The events and routines that should be executed on Polaris Designer dialogs
    ***/
    designerMethods: function() {
        log('Polaris.Initialize.designerMethods');
    }

});

/**
 * Step back, while Polaris does its thing...!
 */
jQuery(function () {
    Polaris.Initialize.generalMethods();

    if ($('#datalistview').length) {
        Polaris.Initialize.listViewMethods();
    }

    if ($('.formview').length) {
        Polaris.Initialize.formViewMethods();
    }

    if ($('#plrdesigner').length) {
        Polaris.Initialize.designerMethods();
    }

    Polaris.Base.setMainFocus();

    $(window).trigger('resize')
    // initialize the special keys
    .data('keypressed_shift', false)
    .data('keypressed_ctrl', false)
    .data('keypressed_alt', false);

    // remove the Loading App image
    $("#loadingapp").remove();

    deltaheight = $("#pageheader").outerHeight(true) + $("#pagefooter").outerHeight(true);
    $(".fh-breadcrumb").css('height', 'calc(100% - '+deltaheight+'px)');

});
