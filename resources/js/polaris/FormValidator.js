if (typeof(Polaris.FormValidator) == "undefined") {
    Polaris.FormValidator = {};
}

Polaris.FormValidator.NAME = "Polaris.FormValidator";
Polaris.FormValidator.VERSION = "1.0";

try {
    if (typeof(Polaris.Base) === 'undefined') {
        throw "";
    }
} catch (e) {
    throw "Polaris.FormValidator depends on Polaris.Base";
}

Base.update(Polaris.FormValidator, {
	// CSS class name definitions.
	classNamePrefix_switch:"switch",
	className_required:"required",
	className_validationError_msg:"errMsg",
	className_validationError_fld:"errFld",
	classNamePrefix_validation:"validation",
	// id attribute suffixes
	idSuffix_fieldHint:"-H",
	idSuffix_fieldLabel:"-L",
	idSuffix_fieldError:"-E",

	showAlertOnError:false, // set to false to not show the alert when a validation error occurs.
	preventSubmissionOnEnter:false, // prevent submission when pressing the 'enter' key. Set to true if pagination behavior is used.

    arrErrorMsg: {
    0: "Dit veld is verplicht.", // required
    1: "De tekst kan uitsluitend alfanumerieke tekens bevatten (a-z, A-Z). Cijfers zijn niet toegestaan.", // validate_alpha
    2: "Dit email adres is niet juist.", // validate_email
    3: "Voer een geheel getal in.", // validate_integer
    4: "Voer een getal met cijfers achter de komma in (bijv. 12.34).",
    5: "Onveilig wachtwoord. Het wachtwoord zou tussen de 4 en 12 karakters lang moeten zijn en gebruik maken van hoofdletters en kleine letters.",
    6: "Gebruik uitsluitend alfanumerieke karakters [a-z 0-9].",
    7: "Dit is geen geldige datum.",
    8: "%% fouten ontdekt. Het formulier is nog niet verzonden.\nControleer a.u.b. de informatie die is ingevoerd." // %% will be replaced by the actual number of errors.
    },

    arrMsg: {
    0: "Klik om een rij toe te voegen", 	// repeat link
    1: "Voeg een rij toe", // title attribute on the repeat link
    2: "Verwijder de rij", 	// remove link
    3: "Verwijdert het voorgaande veld of veld groep.", // title attribute on the remove link
    4: "Volgende pagina",
    5: "Vorige pagina"
    },

	// =======================================================================================
	// FORM VALIDATION
	// =======================================================================================
	formValidation: function(e) {
		var srcE = e.target();
        var self = Polaris.FormValidator;

		if(self.preventSubmissionOnEnter) {
			// prevent form submission if 'enter' key is pressed
			// (doesn't work in Opera. Further tests needed in IE and Safari)
			if(!e) e = window.event;
			if(srcE.type && srcE.type.toLowerCase()=='text')
				return stop(e);
		}

        /**
        * clear all 'need attention' markers from pagetabs
        */
        if ($('pagetabcontainer'))
            map(function(el){ removeElementClass(el, 'needsattention') }, $('pagetabcontainer').getElementsByTagName('a'));

		while (srcE && srcE.tagName.toUpperCase() != 'FORM') {
			srcE = srcE.parentNode;
		}

		var x = self.getElements(srcE); //srcE.elements;
		var nbTotalErrors = 0;
		var isVisible = false;

		for (var i=0;i<x.length;i++) {
			var nbErrors = 0;
            isVisible = Polaris.Form.checkVisibility(x[i]);
			if ((' '+x[i].className+' ').indexOf(' '+self.className_required+' ') != -1) {
                var v = true; // is Valid
                switch(x[i].tagName.toUpperCase()) {
                    case "INPUT":
                        switch(x[i].getAttribute("type").toUpperCase()) {
                            case "CHECKBOX":
                                v = x[i].checked;
                                break;
                            case "RADIO":
                                v = x[i].checked;
                                break;
                            default:
                                v = !self.isEmpty(x[i].value);
                        }
                        break;
                    case "SELECT":
                        v = !self.isEmpty(x[i].options[x[i].selectedIndex].value);
                        break;
                    case "TEXTAREA":
                        v = !self.isEmpty(x[i].value);
                        break;
                    case "FIELDSET":
                        v = self.checkOneRequired(x[i]);
                        break;
                    case "DIV":
                        v = self.checkOneRequired(x[i]);
                        break;
                    case "SPAN":
                        v = self.checkOneRequired(x[i]);
                        break;
                } // end switch

                if (!v) {
                    // flag error
                    self.showError(x[i],self.arrErrorMsg[0]);
                    nbErrors++;
                    var page = getFirstParentByTagAndClassName(x[i],'div','pagecontents');
                    if (page) {
                        var tabid = 'tab_'+page.id;
                        addElementClass(tabid, 'needsattention');
                    }
                } else {
                    // remove required error flag if any.
                    var rErrClass = new RegExp(self.className_validationError_fld,"gi");
                    x[i].className = x[i].className.replace(rErrClass,"");
                    var fe = $(x[i].id +  self.idSuffix_fieldError);
                    if(fe) fe.parentNode.removeChild(fe);
                }
			} // end test=required

			// input validation
			if (x[i].className.indexOf(self.classNamePrefix_validation) != -1) {
				if(!isVisible) isVisible = Polaris.Form.checkVisibility(x[i]);
				if(isVisible) {
					var arrClasses = x[i].className.split(" ");
					for (j=0;j<arrClasses.length;j++) {
						switch(arrClasses[j]) {
							case "validation_alpha":
								if(!self.isAlpha(x[i].value)) {
									self.showError(x[i],self.arrErrorMsg[1]);
									nbErrors++;
								}
								break;
							case "validation_alphanum":
								if(!self.isAlphaNum(x[i].value)) {
									self.showError(x[i],self.arrErrorMsg[6]);
									nbErrors++;
								}
								break;
							case "validation_allchars":
								if(!self.isAllChars(x[i].value)) {
									self.showError(x[i],self.arrErrorMsg[6]);
									nbErrors++;
								}
								break;
							case "validation_date":
								if(!self.isDate(x[i].value)) {
									self.showError(x[i],self.arrErrorMsg[7]);
									nbErrors++;
								}
								break;
							case "validation_time":
								/* NOT IMPLEMENTED */
								break;
							case "validation_email":
								if(!self.isEmail(x[i].value)) {
									self.showError(x[i],self.arrErrorMsg[2]);
									nbErrors++;
								}
								break;
							case "validation_integer":
								if(!self.isInteger(x[i].value)) {
									self.showError(x[i],self.arrErrorMsg[3]);
									nbErrors++;
								}
								break;
							case "validation_float":
								if(!self.isFloat(x[i].value)) {
									self.showError(x[i],self.arrErrorMsg[4]);
									nbErrors++;
								}
								break;
							case "validation_strongpassword": // NOT IMPLEMENTED
								if(!self.isPassword(x[i].value)) {
									self.showError(x[i],self.arrErrorMsg[5]);
									nbErrors++;
								}
								break;
						} // end switch
					} // end for
				}  else { // not visible
				}
			} // end validation check

			if(nbErrors>0) {
				nbTotalErrors+= nbErrors;
			} else {
				var rErrClass = new RegExp(self.className_validationError_fld,"gi");
				x[i].className = x[i].className.replace(rErrClass,"");
				var fe = $(x[i].id +  self.idSuffix_fieldError);
				if(fe) fe.parentNode.removeChild(fe);
			}
		}
		if (nbTotalErrors > 0) {
			// force redraw in case of Safari
			if(self.showAlertOnError){  self.showAlert(nbTotalErrors); }
			return e.stop();
		}
		return true;
	},
	isEmpty: function(s) {
		var regexpWhitespace = /^\s+$/;
		return  ((s == null) || (s.length == 0) || regexpWhitespace.test(s));
	},
    isAlpha: function(s) {
        var reg = /^[\u0041-\u007A\u00C0-\u00FF\u0100�\u017F]+$/;
        return Polaris.FormValidator.isEmpty(s) || reg.test(s);
    },
    isAlphaNum: function(s) {
        var reg = /^[\u0030-\u0039\u0041-\u007A\u00C0-\u00FF\u0100�\u017F]+$/;
        return Polaris.FormValidator.isEmpty(s) || reg.test(s);
    },
	isAllChars: function(s) {
		return true;
	},
	isDate: function(s) {
		var testDate = new Date(s);
		return Polaris.FormValidator.isEmpty(s) || !isNaN(testDate);
	},
	isEmail: function(s) {
		var regexpEmail = /\w{1,}[@][\w\-]{1,}([.]([\w\-]{1,})){1,3}$/;
		return Polaris.FormValidator.isEmpty(s) || regexpEmail.test(s);
	},
	isInteger: function(s) {
		//var regexp = /^[+]?\d+$/;
		//return self.isEmpty(s) || regexp.test(s);
		return Polaris.FormValidator.isEmpty(s) || (parseInt(s) == parseFloat(s));
	},
	isFloat: function(s) {
		return Polaris.FormValidator.isEmpty(s) || !isNaN(parseFloat(s));
	},
	// NOT IMPLEMENTED
	isPassword: function(s) {
		// Matches strong password : at least 1 upper case latter, one lower case letter. 4 characters minimum. 12 max.
		//var regexp = /^(?=.*[a-z])(?=.*[A-Z])(?!.*\s).{4,12}$/;  // <= breaks in IE5/Mac
		return Polaris.FormValidator.isEmpty(s);
	},

	showError: function (n,errorMsg) {
        var self = Polaris.FormValidator;
		if(n.className.indexOf(self.className_validationError_fld)!= -1) {
			return;
		}
		if (!n.id) n.id = Polaris.Base.randomId(); // we'll need an id here.
		// Add error flag to the field
		n.className += " " + self.className_validationError_fld;
		// Prepare error message
		var msgNode = document.createTextNode(" " + errorMsg);
		// Find error message placeholder.
		var fe = $(n.id +  self.idSuffix_fieldError);
		if(!fe) { // create placeholder.
			fe = document.createElement("div");
			fe.setAttribute('id', n.id +  self.idSuffix_fieldError);
			// attach the error message after the field label if possible
			var fl = $(n.id +  self.idSuffix_fieldLabel);
			if(fl) {
				fl.parentNode.insertBefore(fe,fl.nextSibling);
			}
			else {
				// otherwise, attach it after the field tag.
				n.parentNode.insertBefore(fe,n.nextSibling);
			}
		}
		// Finish the error message.
		fe.appendChild(msgNode);
		fe.className += " " + self.className_validationError_msg;
	},

	showAlert: function (nbTotalErrors) {
 	   alert(Polaris.FormValidator.arrErrorMsg[8].replace('%%',nbTotalErrors));
	},
	// Validation Private Method
	// -------------------------
	checkOneRequired: function(n) {
		var v=null;
		var self=Polaris.FormValidator;
		if(n.nodeType != 1) return false;
		if(n.tagName.toUpperCase() == "INPUT") {
			switch(n.type.toLowerCase()) {
				case "checkbox":
					v = n.checked;
					break;
				case "radio":
					v = n.checked;
					break;
				default:
					v = n.getAttribute("value");
			}
		} else v = n.getAttribute("value");
		if(v && !self.isEmpty(v)) {
			return true;
		}
		for(var i=0; i<n.childNodes.length;i++) {
			if(self.checkOneRequired(n.childNodes[i])) return true;
		}
		return false;
	},

    init: function(form) {
        $(".validate",document.forms).each( function(index, elem) {
			elem = $(elem);
            if(!elem.attr('id')) elem.attr('id', Polaris.Base.randomId());
            Polaris.FormValidator.addBehaviors(elem);
        });
    },

    addBehaviors: function (fId) {
		log(fId);
        var f=$(fId);
        if(!f) return;

        var thisForm; 				// Pointer to keep track of the current form being processed.

        // loop through the fields
        var x = Polaris.FormValidator.getElements(f[0]);
        for (var i=0;i<x.length;i++) {
            // add form validation behavior
            if(x[i].tagName.toUpperCase()=="FORM" && hasElementClass(x[i],"validate")) {
                connect(x[i],'onsubmit',Polaris.FormValidator.formValidation);
                thisForm = x[i];	// Pointer to keep track of the current form being processed.
            }
        }
    },

    getElements: function(n, list) {
        if(!list) list = new Array();
        if(n.nodeType==1) {
            list[list.length]= n;
            for(var i=0; i<n.childNodes.length;i++)
                Polaris.FormValidator.getElements(n.childNodes[i], list);
            return list;
        }
    }
});
