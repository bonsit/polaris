if (typeof(Polaris.Selectsource) == "undefined") {
    Polaris.Selectsource = {}
}

Polaris.Selectsource.NAME = "Polaris.Selectsource";
Polaris.Selectsource.VERSION = "1.0";

try {
    if (typeof(Polaris.Base) === 'undefined') {
        throw "";
    }
} catch (e) {
    throw "Polaris.Selectsource depends on Polaris.Base";
}

Base.update(Polaris.Selectsource, {
    __init__: function() {
        var self = Polaris.Selectsource;
        var columnsource_columns = $(".columnsource_column");
        for(var i=0;i<columnsource_columns.length;i++) { 
          self.setColumnSourceValues(columnsource_columns[i]);
        } 
    },

    setColumnSourceValues: function (parentObject) {
      if (parentObject.target) parentObject = parentObject.target;

      var columnsource = window[$(parentObject).attr('arrayname')];

      // find the other select field by relative position (not absolute)
      var li_object = parentObject;

      while (li_object && li_object.tagName.toUpperCase() != "LI") {
          li_object = li_object.parentNode;
      }

      var childObject = $(li_object).find("select.comp_valuefield:first")[0];
//      var childObject = $(parentObject.getAttribute('valuepicklist'));
      if (isUndefined(childObject)) {
          alert("no child found");
          return;
      }

      //Clear child listbox
      for(var i=childObject.length;i>=0;i--) {
          childObject.options[i] = null;
      }

      var sel_index = parentObject.options[parentObject.selectedIndex].value;
      if (sel_index == "") {
//        childObject.disabled = true;
        if (parentObject.parentNode.firstChild.className == "recordstate")
            parentObject.parentNode.firstChild.value = "delete"; // set the flag for deletion
      } else {
        childObject.disabled = false;

        if (parentObject.parentNode.firstChild.className == "recordstate")
            // set the flag for edit or insert
            if (window.location.pathname.indexOf('/insert/') > 0)
                parentObject.parentNode.firstChild.value = 'insert';
            else
                parentObject.parentNode.firstChild.value = 'edit';
                
        if (childObject.options) {
            if ($(childObject).hasClass("required"))
              var childIndex = 0;
            else
              var childIndex = 1;
            var id = 0;
            for (i = 0; i < columnsource.length; i++) {
              if (columnsource[i][0] == sel_index) {
                if (columnsource[i][2] != "")
                    id = columnsource[i][2];
                else
                    id = columnsource[i][1];
                childObject.options[childIndex] = new Option(columnsource[i][1], id);
                childIndex++;
              }
            }
         }
      }
      //Select the current option
      childObject.value = childObject.getAttribute("currentvalue");
    }
});
