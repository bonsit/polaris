/***
*
*    The Polaris AJAX functions
*
**/
Polaris.Ajax = {};
Base.update(Polaris.Ajax, {
    /** @id Polaris.Ajax.postJSON */
    postJSON: function(urlquery, senddata, message, feedback_ok, callback_ok, callback_error) {
        if (typeof message !== 'undefined' && message !== null)
            Polaris.Base.modalMessage(message, 0);

        $.post(urlquery, senddata, function(data, textStatus) {
            Polaris.Base.closeModalMessage();
            if (textStatus == 'success') {
                if (data == null || (data != null && data.error == false)) {
                    if (callback_ok) callback_ok(data);
                    if (typeof feedback_ok !== 'undefined' && feedback_ok !== null) {
                        Polaris.Base.modalMessage(feedback_ok, 1500);
                    }
                } else {
                    if (callback_error) {
                        callback_error(data);
                    } else {
                        Polaris.Base.modalDialog(
                            data.error + '<br>' + data.detailed_error,
                            { close: function() {}, type: 'type-danger' }
                        );
                    }
                }
            } else {
                Polaris.Base.modalMessage('An error occured during the JSON post.');
                if (callback_error) callback_error(data);
            }
        }, 'json');
    },

    /** @id Polaris.Ajax.loadPins */
    loadPins: function() {
        var $container = $("#pinnedrecords");
        var url = _ajaxquery.replace('json','xhtml')+'?method=plr_loadpinrecords'; //&_hdnPlrSes='+sessionStorage.plrses;
        if ($container.hasClass('pinnedtags'))
            url = url + '&pinnedtags';
        $("#pinnedrecords").load(url);
    },

    _deletePin: function(databaseid, tablename, recordid, callback) {
        $.post(_ajaxquery, {
            _hdnDatabase: databaseid,
            _hdnAction: 'plr_deletepin',
            _hdnTable: tablename,
            _hdnRecordID: recordid,
            _hdnPlrSes: sessionStorage.plrses
        }, function(data, textStatus) {
            if (typeof callback == 'function') {
                callback(data, textStatus);
            }
        },'json');
    },

    deletePin: function(databaseid, tablename, recordid, callback) {
        Polaris.Ajax._deletePin(
            databaseid,
            tablename,
            recordid,
            function (data, textStatus) {
                if (textStatus == 'success') {
                    Polaris.Ajax.loadPins();
                } else {
                    alert('Pin not deleted: '+data);
                }
            }
        );
    },

    deletePins: function(database) {
        Polaris.Ajax._deletePin(
            database,
            'ALL',
            'ALL',
            function (data, textStatus) {
                if (textStatus == 'success') {
                    Polaris.Ajax.loadPins();
                } else {
                    alert('Pin not deleted: '+data);
                }
            }
        );
    },

    recordDelete: function(databaseid, tablename, recordid, callback) {
        $.post(_ajaxquery, {
            _hdnDatabase: databaseid,
            _hdnTable: tablename,
            _hdnAction: 'plr_delete',
//            _hdnShowsql: 'true',
            _hdnRecordID: recordid
        }, function(data, textStatus) {
            if (typeof callback == 'function') {
                callback(data, textStatus);
            }
        },'json');
    },

    recordUpdate: function(databaseid, tablename, recordid, columnname, value, callback) {
        var _params = {
            _hdnDatabase: databaseid,
            _hdnTable: tablename,
            _hdnAction: 'plr_hotupdate',
//            _hdnShowsql: 'true',
            _hdnHotUpdateRecord: recordid,
            _hdnHotUpdateFields: columnname,
            _hdnHotUpdateValues: value
        };
        $.post(_ajaxquery, _params, function(data, textStatus) {
            if (typeof callback == 'function') {
                callback(data, textStatus);
            }
        },'json');
    },
    /* Polaris.Ajax.loadRowsValues */
    loadRowsValues: function(databaseid, tablename, recordids, callback) {
        recordids = recordids.join(',');
        var _params = {
            _hdnDatabase: databaseid,
            _hdnTable: tablename,
            _hdnAction: 'plr_loadrecordvalues',
            _hdnRecordIDs: recordids
        };
		Polaris.Ajax.postJSON(_ajaxquery, _params, null, null,
            function(data, textStatus) {
                callback(data);
            },
            function(data) {
                console.log('No recordvalues found: ' + data);
            }
        );
    },
    showDetailHeader: function(detailform, callback) {
    	var tmp_servicequery = _servicequery.substr(0, _servicequery.substr(0, _servicequery.length - 1).lastIndexOf('/'));

        var url = tmp_servicequery+'/'+detailform+'/';
        url = url.replace('/const/','/form/');
        Polaris.Ajax.getDetailHeader(url, function(data) {
            var temp = '<div id="ajaxdetaillistview"><div class="headerscroll scrollarea">'+
            '<table id="datadetailheaderview" class="data striped group-table right " cellspacing="0" cellpadding="0">'+
            '<thead class="">'+
            '<tr>';
            var index = 0;
            $.each(data, function(i, e) {
                temp = temp + '<th class="'+e.COLUMNNAME+' '+e.ALIGN+' nowrap '+e.NUMERICMASK+'">&nbsp;'+e.DISPLAYNAME+'&nbsp;';
                temp = temp + '</th>';

                if (index == 0) {
                    temp = temp + '<th class="editlink"></th>'
                }
                index++;
            });

            temp = temp + '<th class="group-table right">&nbsp;</th></tr></thead></table></div>'+
            '<div class="bodyscroll scrollarea">'+
            '<table id="datadetaillistview" class="data striped group-table right " cellspacing="0" cellpadding="0">'+
            '<tbody></tbody></table></div></div>';

            $("#detailajax").html( temp ).css("height","10px;");

            if (callback) callback();
        });
    },
    getDetailHeader: function(url, callback) {
		$.getJSON(url,
		    {'headers':'true'},
            function(data, textStatus) {
                if (textStatus == 'success') {
                    callback(data);
                } else {
                    console.log('Geen detailheader gevonden: ' + textStatus);
                }
            }
        );
    },
    nl2br: function(str, is_xhtml) {
        var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
        return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1'+ breakTag +'$2');
    },
    showDetailRecords: function(detailform, row) {
        if ($(row).length == 0) return false;

        // kan ook via: Polaris.Dataview.selectedRowsValues('ROWID')
        var recordid = $(row).attr('id').substr(4,32);
        var url = _servicequery+'detail/'+recordid+'/'+detailform+'/';

        Polaris.Ajax.getDetailRecords(url, 0, 500, row, function(data) {
            var val = '';
            var txt = [];
            var index;
            var txtIndex = 0;
            $("#datadetaillistview tbody").html('');
            $headercolumns = $("#datadetailheaderview th");
            $.each($.makeArray(data), function(i,e) {
                txt[txtIndex++] = "<tr>";
                index = 0;
                for (var i in e) {
                    var columnExists = $headercolumns.hasClass(i);
                    if (columnExists) {
                        val = e[i];
                        if (val == null) val = '';

//                        var numericformat = $headercolumns.eq(index).metadata();
//                        if (numericformat.mDec > 0) {
//                            val = $.fn.autoNumeric.Format(false, val, numericformat);
//                        }
                        var align = '';
                        if (typeof $headercolumns.get(index) != "undefined") {
                            var cn = $headercolumns.get(index).className;
                            if ((' '+cn+' ').indexOf(' right ') != -1)
                                align = 'right ';
                        }

                        val = Polaris.Ajax.nl2br(val.replace(' ', '&nbsp;'));
                        txt[txtIndex++] = "<td class=\""+align+"nowrap cell_disabled\">" + val;
                    }

                    if (index == 0 && e['PLR__RECORDID'] != '') {
                        var masterrecid = Polaris.Base.customUrlEncode(recordid);
                        var detailrecid = Polaris.Base.customUrlEncode(e['PLR__RECORDID']);
                        var link = _callerquery+'detail/'+masterrecid+'/'+detailform+'/edit/'+detailrecid+'/?_hdnPlrSes='+sessionStorage.plrses;
                        txt[txtIndex++] = '<td class="editlink cell_disabled"><div class="buttons small"><a class="edit_but jump" href="'+link+'"><i class="fa fa-th-list"></i></a></div></td>'
                    }

                    txt[txtIndex++] = "</td>";

                    index++;
                    if (index == 1) index++;
                }
                txt[txtIndex++] = "</tr>";

            });
            $("#datadetaillistview tbody").html( txt.join('') );

            Polaris.Dataview.autoSizeHeightDetail($("#ajaxdetaillistview"));
            // Polaris.Dataview.sizeToFitCells($("#ajaxdetaillistview"));
        });
    },
    getDetailRecords: function(url, offset, limit, row, callback) {
        // added: caching system for json data
        row = $(row);
        if (row.data('detailrecords')) {
            log('Cache data used.');
            callback(row.data('detailrecords'));
        } else {
            $.getJSON(url, {
                'offset': offset,
                'limit': limit
                },
                function(data, textStatus) {
                    if (textStatus == 'success') {
                        row.data('detailrecords', data);
                        callback(data);
                    } else {
                        console.log('No detailrecords found: ' + textStatus);
                    }
                }
            );
        }
    }
});