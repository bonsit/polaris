if (typeof(Polaris.AjaxUploader) == "undefined") {
    Polaris.AjaxUploader = {};
}

Polaris.AjaxUploader.NAME = "Polaris.AjaxUploader";
Polaris.AjaxUploader.VERSION = "1.0";

try {
    if (typeof(Polaris.Base) === 'undefined') {
        throw "";
    }
} catch (e) {
    throw "Polaris.AjaxUploader depends on Polaris.Base";
}

Base.update(Polaris.AjaxUploader, {
    aufprefix: 'ajaxuploadform',
    blankImage: _serverroot+'/images/blank.gif',


    init: function(parent) {
        var elems = $('input.ajaxuploader', $(parent));
        elems.each(Polaris.AjaxUploader.createGUI);
    },

    createGUI: function (index, e) {
        if (!e) return;

        e = $(e);

        var self = Polaris.AjaxUploader;
        var originalform = e.form;
        var rootId = Polaris.Base.randomId();

        e.attr('type', 'hidden');
        var inputname = e.attr('name');
        var realvalue = e.attr('value');
        var showvalue = realvalue.substr(realvalue.indexOf('^')+1);
        $("<iframe></iframe>").attr({'id':self.aufprefix+rootId+'_iframe', 'name':self.aufprefix+rootId+'_iframe', 'style':'display:none'
        , 'src':_serverroot+'/blank.html', 'class':'loader'}).appendTo(originalform);

        var inp = $("<input type='text'/>").attr({'type':'text', 'id':'_fld'+inputname+rootId, 'value':showvalue, 'size':'40','readonly':'readonly', 'class':'readonly'});
        var but = $("<button>Kies afbeelding</button>").attr({'type':'button'}).click(
            function() {
                $("#"+inputname+rootId).click();
                return false;
            });
        var div = $("<div><span></span></div>").attr({'id':self.aufprefix+rootId+'_feedback', 'class':'ajaxuploadfeedback'});

        var active = (realvalue == '')?'':' active';
        var imageurl = e.attr('imgurl')+realvalue;
        var thumburl = _serverroot+'/thumb?src='+imageurl+'&h=150&w=150';
        var imagewidth = '150px';

        var img = $("<img/>").attr({'id':rootId+'_image', 'class':'formpicture', 'src':thumburl, 'style':'width:'+imagewidth});
        var awg = $("<a href='javascript:void(0)' class='popup'>Toon op ware grootte</a>").click( function(e){
                e.preventDefault();
                openCenteredWindow(imageurl);
            });
        var adl = $("<a href='#'>Verwijder</a>").click(function(){
                e.val('');
                $('#_fld'+inputname+rootId).val('');
                $('#'+rootId+'_image').attr('src', self.blankImage);
            });
        var imageset = $("<div></div>").attr({'id':rootId+'_imagecontainer', 'class':'imagecontainer'+active}
        ).append(img).after(awg).after(adl);

        e.after($(inp, div, but, imageset));

        var fileinput = $("<input type='file' />").attr({'id':inputname+rootId,'name':inputname+rootId,'class':'file','value':rootId}
        ).bind('change mouseout', function(e) {
            Polaris.AjaxUploader.uploadAjaxImage(e.target, rootId);
        });

        var ajaxform = $("<form></form>").attr({'id':self.aufprefix+rootId,
                             'event':'POST',
                             'enctype':'multipart/form-data',
                             'name':self.aufprefix+rootId,
                             'action':_serverroot+'/uploadajaxfile.php?sID='+rootId,
                             'target':self.aufprefix+rootId+'_iframe'}).after(
            $("<input type='hidden' />")).attr({'name':'_hdnFolder', 'value':e.attr('imgfolder')}).after(
            $("<input type='hidden' />")).attr({'name':'_hdnUrl', 'value':e.attr('imgurl')}).after(
            $("<input type='hidden' />")).attr({'name':'_hdnColumnName', 'value':inputname}).after(
            $("<input type='hidden' />")).attr({'name':'_hdnIdentifier', 'value':rootId}).after(
            fileinput);

        document.body.append(ajaxform);
    },

    uploadAjaxImage: function(e, sid) {
        var theForm = e.form;

        // The variable request is an object that will contain details of the request.
        var request = {};
        var fileName = '';

        // This little bit loops through all the elements in the form, locates the
        // file input field, and extracts the file name of the file being uploaded.
        for (var i=0; i < theForm.elements.length; i++) {
          ele = theForm.elements[i];
          if (ele.type == 'file') {
            var fileName = ele.value;
            if (fileName.indexOf('/') > -1) {
                fileName = fileName.substring(fileName.lastIndexOf('/')+1, fileName.length);
            }	else {
                fileName = fileName.substring(fileName.lastIndexOf('\\')+1, fileName.length);
            }
            //Since there can only be one file per form for this script, we'll exit the loop here.
            break;
          }
        }
        if (fileName.replace('/\s/', '').toString() === '') {return;}
        theForm.submit();
        $('#'+theForm.id+'_feedback').getElementsByTagName('span')[0].className = 'wait';
    }
});
