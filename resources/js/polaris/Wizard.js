/***
*
*    Polaris Wizard functions
*
**/
Polaris.Wizard = {};
Base.update(Polaris.Wizard, {
    __wizard: null,
    initialize: function(wizard) {
        Polaris.Wizard.__wizard = wizard;
        Polaris.Wizard.initializeWizard(Polaris.Wizard.__wizard);
        Polaris.Wizard.initializeExternalButtons();
    },
    initializeWizard: function(wizard) {
        var self = $(Polaris.Wizard.__wizard);
        /***
        * Initialize Wizard steps
        */
        self.smartWizard({
            selected: 0, // Initial selected step, 0 = first step
            // themes: basic, arrows, dots, square, round
            theme: 'arrows', // theme for the wizard, related css need to include for other than default theme
            justified: true, // Nav menu justification. true/false
            autoAdjustHeight: false, // Automatically adjust content height
            backButtonSupport: true, // Enable the back button support
            enableUrlHash: false, // Enable selection of the step based on url hash
            transition: {
                animation: 'none', // Animation effect on navigation, none|fade|slideHorizontal|slideVertical|slideSwing|css(Animation CSS class also need to specify)
                speed: '400', // Animation speed. Not used if animation is 'css'
                easing: '', // Animation easing. Not supported without a jQuery easing plugin. Not used if animation is 'css'
                prefixCss: '', // Only used if animation is 'css'. Animation CSS prefix
                fwdShowCss: '', // Only used if animation is 'css'. Step show Animation CSS on forward direction
                fwdHideCss: '', // Only used if animation is 'css'. Step hide Animation CSS on forward direction
                bckShowCss: '', // Only used if animation is 'css'. Step show Animation CSS on backward direction
                bckHideCss: '', // Only used if animation is 'css'. Step hide Animation CSS on backward direction
            },
            toolbar: {
                position: 'none', // none|top|bottom|both
                showNextButton: true, // show/hide a Next button
                showPreviousButton: true, // show/hide a Previous button
                extraHtml: '' // Extra html to show on toolbar
            },
            anchor: {
                enableNavigation: false, // Enable/Disable anchor navigation
                enableNavigationAlways: false, // Activates all anchors clickable always
                enableDoneState: true, // Add done state on visited steps
                markPreviousStepsAsDone: true, // When a step selected by url hash, all previous steps are marked done
                unDoneOnBackNavigation: false, // While navigate back, done state will be cleared
                enableDoneStateNavigation: true // Enable/Disable the done state navigation
            },
            keyboard: {
                keyNavigation: false, // Enable/Disable keyboard navigation(left and right keys are used if enabled)
                keyLeft: [37], // Left key code
                keyRight: [39] // Right key code
            },
            lang: { // Language variables for button
                next: 'Volgende',
                previous: 'Vorige'
            },
            disabledSteps: [], // Array Steps disabled
            errorSteps: [], // Array Steps error
            warningSteps: [], // Array Steps warning
            hiddenSteps: [], // Hidden steps
            getContent: null // Callback function for content loading
        });

        // Leave step event is used for validating the forms
        self.on("leaveStep", function(e, anchorObject, currentStepIdx, nextStepIdx, stepDirection) {
            // Validate only on forward movement
            if (stepDirection == 'forward') {
              let section = document.querySelector('.tab-pane:nth-child('+(currentStepIdx + 1)+')');
              if (section) {
                let elements = $(section).find(':input:not([type=hidden])');
                if (elements.length > 0) {
                    if (!elements.valid()) {
                        section.classList.add('was-validated');
                        self.smartWizard("setState", [currentStepIdx], 'error');
                        self.smartWizard('fixHeight');
                        return false;
                    }
                    self.smartWizard("unsetState", [currentStepIdx], 'error');
                }
              }
            }
        });

        var prevButton = $("#wizard-prev-btn");
        var nextButton = $("#wizard-next-btn");

        self.on("showStep", function(e, anchorObject, stepIndex, stepDirection, stepPosition) {
            prevButton.removeClass('disabled').prop('disabled', false);
            nextButton.removeClass('disabled').prop('disabled', false);
            if(stepPosition === 'first') {
                prevButton.addClass('disabled').prop('disabled', true);
            } else if(stepPosition === 'last') {
                nextButton.addClass('disabled').prop('disabled', true);
            } else {
                prevButton.removeClass('disabled').prop('disabled', false);
                nextButton.removeClass('disabled').prop('disabled', false);
            }

            if (stepPosition == 'last') {
                nextButton.hide();
                $("#wizard-finish-btn").prop('disabled', false).show();
            } else {
                nextButton.show();
                $("#wizard-finish-btn").prop('disabled', true).hide();
            }

            let currentID = document.querySelector('.nav-progress li:nth-child('+(stepIndex + 1)+') a');
            if ($(currentID).attr('href') != '') {
                let currentSection = $($(currentID).attr('href'));
                $(currentSection).find(':input:not([type=hidden])').first().trigger('focus');
            }
        });

    },
    initializeExternalButtons: function() {
        var self = $(Polaris.Wizard.__wizard);

        // External Button Events

        $("#wizard-finish-btn").on("click", function() {
            // Navigate previous
            $("#dataform").trigger('submit');
        });

        $("#wizard-prev-btn").on("click", function() {
            // Navigate previous
            self.smartWizard("prev");
            return true;
        });

        $("#wizard-next-btn").on("click", function() {
            // Navigate next
            self.smartWizard("next");
            return true;
        });
    }
});
