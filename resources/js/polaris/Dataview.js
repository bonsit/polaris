
/***
*
*    The Polaris Dataview (ListView) functions
*
**/
Polaris.Dataview = {};
Base.update(Polaris.Dataview, {
    initializeFormActions: function() {
        /***
        * Connect the StoredProc form actions to the ajax handler (with parameter form)
        */
        $(document).on('click', 'button.storedprocedure,a.storedprocedure,li a.module.standardparamform,.buttons a.module.standardparamform', function(e) {
            Polaris.Dataview.executeFormAction(e);
        });
        $(document).on('click', 'button.customparamform,.dropdown-menu a.module.customparamform,.buttons a.module.customparamform', function(e) {
            Polaris.Dataview.executeCustomFormAction(e);
        });
        $(document).on('click', 'button.jasper,a.jasper', function(e) {
            Polaris.Dataview.executeFillParamAction(e);
        });
        $(document).on('click', 'button.jasperside,a.jasperside', function(e) {
            e.preventDefault();
            var url = Polaris.Dataview.enrichUrl(this);
            Polaris.Form.showOverlay(url);
        });
        $(document).on('change', 'select.select2.add-items', function(e) {
            if ($(this).val() === 'show_select_additem_modal') {
                e.preventDefault();
                var url = $(this).data('url');
                window.open(url, '_blank');
                // Polaris.Form.showOverlay(url);
                // // Reset the select to its initial state
                $(this).val(null).trigger('change');
            }
        });

        $(document).on('click', 'button.url', function(e) {
            window.location.href = $(e.target).attr('href');
        });
    },
    initializeFormulas: function() {
        $(".formula").each(function (index, e) {
            Polaris.Dataview.updateFormulas($(e).data('formulafield'));
        });
    },
    initializeRecordCount: function() {
        // When the recordcount is slow, then give user the option to calculate the recordcount
        $("#countrecords").on('click', function() {
            var url = _servicequery+location.search+"&method=plr_recordcount";
            if (confirm($.t('plr.long_operation'))) {
                $("#recordcount_unknown img").attr('src',_serverroot+'/images/wait.gif');
                $("#recordcount_unknown").load(url, function(recordcount) {
                    $(".recordnavigator .last").attr('href', _callerquery+'?_hdnPlrSes='+sessionStorage.plrses+'&qv[]=&offset='+(recordcount-49)).find('i').removeClass('disabled');
                });
            }
        });
    },
    initializeStatusLog: function() {
        $('.hoverstatus').popover({
            container: 'body',
            trigger: 'hover',
            placement: 'bottom',
            offset: 100,
            delay: { "show": 300, "hide": 100 },
            boundary: 'viewport',
            html: true,
            content: function() {
                let id = $(this).data('hoverid');
                return $("#statushover__" + id).html();
            }
        }).on("show.bs.popover", function(){
            $(this).data("bs.popover").tip().css("width", $(this).data('hoverwidth') || 'none');
        });
        //FOR TESTING
        // $('.hoverstatus[data-hoverid=930d2ac0ff9cf0bbf8e51c720d9cebcf]').popover('show');
    },
    initializeSortableTable: function() {
        log(window.hasOwnProperty('Sortable'));
        if (window.hasOwnProperty('Sortable')) {
            const table = document.querySelector('.sortable-table');
            const sortable = Sortable.create(table, {
                sort: true,  // sorting inside list
                store: {
                    set: function (sortable) {
                        const createArrayWithIntegers = n => Array.from({ length: n }, (_, i) => (i + 1) * 10);
                        const recordArray = sortable.toArray();
                        const recordids = recordArray.join(';').replaceAll('row@','');
                        const orderArray = createArrayWithIntegers(recordArray.length);
                        const ordervalues = orderArray.join(';');

                        Polaris.Dataview.hotAjaxUpdate('frmhotupdate', $(sortable.el).data('column'), recordids, ordervalues);
                    }
                },
                animation: 150,  // ms, animation speed moving items when sorting, `0` — without animation
                handle: ".sortable-handle",  // Drag handle selector within list items
                draggable: "tr",  // Specifies which items inside the element should be draggable
                dataIdAttr: 'id', // HTML attribute that is used by the `toArray()` method
                ghostClass: "sortable-ghost"  // Class name for the drop placeholder
            });
        }
    },
    initializeSortableMedia: function(id, options) {
        if (window.hasOwnProperty('Sortable')) {
            options = options || {};
            const grid = document.getElementById(id);
            log(id, options);
            const sortable = Sortable.create(grid, {
                group: {
                    name: 'shared-media',
                    pull: options.grouppull || 'clone' // To clone: set pull to 'clone'
                },
                sort: options.sort || true,  // sorting inside list
                filter: '.filtered',
                animation: 150,  // ms, animation speed moving items when sorting, `0` — without animation
                handle: ".sortable-handle",  // Drag handle selector within list items
                draggable: ".sortable-draggable",  // Specifies which items inside the element should be draggable
                dataIdAttr: 'id', // HTML attribute that is used by the `toArray()` method
                ghostClass: "sortable-ghost", // Class name for the drop placeholder
                onChoose: function (/**Event*/evt) {
                    // select the current item
                    const item = $(evt.item);
                    $('.selected', grid).removeClass('selected');
                    item.addClass('selected');

                },
                onSort: function (evt) {
                    if(options.fieldid) {
                        $('#'+options.fieldid).val(sortable.toArray().join(','));
                    }
                },
            });
        }
    },
    initializeKeyTable: function() {
        var D = Polaris.Dataview;

        var form_editable = true; //(cfp & 8) == 8; // 8 == UPDATE

        var keys = new KeyTable( {
            "table": document.getElementById('datalistview'),
            "form": true,
            "selectRow": !form_editable,
            "continueScroll": false,
            'initScroll':true,
            'focus': [0, sessionStorage.keyTableFocusY||0],
            'useStoredFocus': false
        } );

        var cellAction = function(nCell, event) {
            var keypressed = null;
            if (typeof event != 'undefined' && (event.metaKey == false && event.CtrlKey == false && event.altKey == false)) {
                var keypressed = String.fromCharCode(event.keyCode);
            }

            if ($(nCell).find("span.edit").length > 0) {
                if (form_editable) {
                    /* Block KeyTable from performing any events while jEditable is in edit mode */
                    keys.block = true; // not working, should
                    var originalvalue = null;

                    /* What editor should we display */
                    var edittype = 'text';
                    if ($(nCell).find("span.edit").hasClass("select")) {
                        edittype = 'select';
                    } else if ($(nCell).find("span.edit").hasClass("dateinput")) {
                        edittype = 'dateinput';
                    }

                    var originalfield = $(nCell).find("input:hidden");
                    if (edittype == 'text') {
                        /* What is the value that should be edited */
                        originalvalue = originalfield.val();
                        var displayvalue = $(nCell).find("span.edit").text();

                        /* If the display value is no equal to the real value, then the display value must be 'cutoff' ...
                           In that case show the real value in the edit field
                        */
                        if (originalvalue != displayvalue) {
                            $(nCell).find("span.edit").text(originalvalue);
                        }
                        originalvalue = null;
                    } else if (edittype == 'select') {
                        var columnname = originalfield.attr('id').split('@')[0].replace('_fld','');
                        if (window[columnname+'_lookuppicklist']) {
                            originalvalue = window[columnname+'_lookuppicklist'];
                        }
                    }
                    var selecttext = true;
                    if (keypressed) {originalvalue = keypressed; selecttext = false; console.log(originalvalue, 'keypressed')};

                    /* Initialise the Editable instance for this table */
                    $(nCell).find("span.edit").editable(
                        function (newvalue) {
                            var originalfield = $(this).parents("td").find("input:hidden");
                            var orivalue = originalfield.val();
                            /* Submit function (local only) - unblock KeyTable */
                            if (orivalue != newvalue) {
                                var fielddata = originalfield.attr('id');
                                var columnname = originalfield.attr('name')//fielddata.split('@')[0].replace('_fld','');
                                var recordId = fielddata.split('@')[1];
                                $(nCell).addClass('ajaxupdating');
                                var currentform = $(this).parents("form");
                                Polaris.Ajax.recordUpdate(
                                    $('input[name=_hdnDatabase]', currentform).val(),
                                    $('input[name=_hdnTable]', currentform).val(),
                                    recordId,
                                    columnname,
                                    newvalue,
                                    function (data, textStatus) {
                                        $(nCell).removeClass('ajaxupdating');
                                        if (textStatus == 'success') {
                                            if (data.error === false) {
                                                originalfield.val(newvalue);
                                                orivalue = newvalue;
                                                D.populateRow(recordId, data.new_values);
                                                D.updateFormulas(columnname);
                                            } else {
                                                Polaris.Base.errorDialog(data.error, data.detailed_error);
                                                $(nCell).find("span.edit").text(orivalue);
                                            }
                                        } else {
                                            alert('Wijziging niet opgeslagen: '+data);
                                        }
                                    }
                                );
                            }
                            keys.block = false;
                            if (originalvalue) {
                                return originalvalue[newvalue];
                            } else {
                                return newvalue;
                            }
                        },
                        {
                        "data": originalvalue,
                        "type": edittype,
                        "select": selecttext,
                        "placeholder": "",
                        "onblur": "submit",
                        "mask": $(".headerscroll th").eq($(nCell).index()).attr('mask'),
                        "maskoptions": $(".headerscroll th").eq($(nCell).index()).metadata(),
                        "width": $(nCell).width()+'px',
                        "cssclass": "hotedit",
                        //"height": "14",
                        "onreset": function(){
                            /* Unblock KeyTable, but only after this 'esc' key event has finished. Otherwise
                             * it will 'esc' KeyTable as well
                             */
                            setTimeout( function () {keys.block = false;}, 0);
                        }
                    } );

                    /* Dispatch click event to go into edit mode - Saf 4 needs a timeout... */
                    setTimeout( function () { $(nCell).find("span.edit").click(); }, 0 );
                } else {
                    alert($.t('insufficient_rights_cannot_update'));
                }
            }
        };

        var rowOpenEdit = function(nCell, event) {
            if (D.onSelectRowHandler != null) {
                D.onSelectRowHandler(nCell, event);
            } else {
                var alink = !form_editable ? $(nCell).find("a.jump") : $(nCell).parent().find("a.jump") ;
                if (alink.length > 0) {
                    /* If the cell is a link (and has the 'jump' class) then goto the link  */
                    window.location = alink.attr('href');
                }
            }
        };

        if (Polaris.Base.getLocationVariable('select') != 'true') {
            /* Apply a return key event to each cell in the table */
            var i = 0;
            $('#datalistview tbody tr').each( function () {
                if (!$(this).hasClass('editdisabled')) {
                    keys.event.focus(this, function(nCell) {
                        D.toggleListviewFormActions();
                    });
                    keys.event.blur(this, function(nCell) {
                        D.toggleListviewFormActions();
                    });
                }
                keys.event.action( this, cellAction );
                keys.event.selectrow( this, D.rowHighlite);
                keys.event.open( this, rowOpenEdit );
            });

            if ($('#datalistview').hasClass('showdetaillist')) {
                Polaris.Ajax.showDetailHeader($('#datalistview').attr('ref'),
                function() {
                    // set focus on first row and load the detail records
                    keys.setFocus(keys.cellFromCoords(1, 0));
                });
            }

            $('#datalistview.showdetaillist tbody tr').each( function () {
                i++;
                keys.event.browse( this, function(aRow) {Polaris.Ajax.showDetailRecords($('#datalistview').attr('ref'), aRow); });
            });
        }
    },
    initializeDetailsview: function() {
        $(".generic-detailsview-delete-link").on('click', function(e) {
            let elem = $(e.target);
            if (elem.length == 1) {
                Polaris.Dataview.deleteOneRecordAjax(e, elem.data('id'), function(status) {
                    if (status == 'success') {
                        window.location.reload();
                    }
                });
            }
        });
    },
    initializeRecordMenu: function() {
        //save the selector so you don't have to do the lookup everytime
        $dropdown = $("#generic-record-menu");
        $("#generic-delete-link").on('click', function(e) {
            let tr = $(e.target).parents('tr');
            if (!tr || tr.length === 0) {
                tr = $(e.target).closest('.dr');
            }
            if (tr.length == 1) {
                let recordid = $(tr[0]).prop('id').substr(4,32);
                Polaris.Dataview.deleteOneRecordAjax(e, recordid);
            }
        });

        $(".edit_mnu").on('click', function(e) {
            e.preventDefault();
            //move dropdown menu
            $(this).after($dropdown);
            // update links
            $("#generic-edit-link").attr("href", $(this).attr('href'));
        });
    },
    initializeColorPickers: function() {
        $('.colorpicker').on('input', function(e) {
            var p = $(e.target);
            var recordid = p.attr('name').replace('_hdnColorButton_','');
            var togglecolumn = p.attr('column');
            return Polaris.Dataview.hotAjaxUpdate(p.data('target'), togglecolumn, recordid, p.val());
        });
    },
    numberWithThousandSep: function(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    },
    updateFormulas: function(field) {
        if (field.indexOf('@@') > 0) field = field.substr(0, field.indexOf('@@'));

        var formulafield = $('td[data-formulafield='+field+']');
        if (formulafield.length > 0) {
            var fields = $('._fld'+field);
            var formula = formulafield.data('formula');
            var result = 0;
            if (formula == 'SUM') {
                fields.each(function(index, elem) {
                    result = result + parseFloat(elem.value || 0);
                });
            }
            if (formula == 'AVG') {
                fields.each(function(index, elem) {
                    result = result + parseFloat(elem.value || 0);
                });
                result = result / fields.length;
            }
            formulafield.find("span").text(Polaris.Dataview.numberWithThousandSep(result));
        }
    },
    // Polaris.Dataview.sizeToFitCells
    sizeToFitCells: function(listview) {
        $bs = $(".bodyscroll", listview);
        if ($bs.length > 0) {
            var hs = $(".headerscroll th", listview);
            var tds = $("tr:first td", $bs[0]);
            tds.each(function(i, elem) {
                var headertd = hs.eq(i);
                var bodytd = $(elem);
                var maxwidth = Math.max(headertd.width(), bodytd.width());
                var maxheaderwidth = headertd.hasClass('headerfiller')?40:maxwidth;
                //bodytd.css({'min-width':maxwidth+"px",'max-width':maxwidth+"px"});
                //headertd.css({'min-width':maxheaderwidth+"px",'max-width':maxheaderwidth+"px"});
                bodytd.css({'width':maxwidth+"px"});
                headertd.css({'width':maxheaderwidth+"px",'max-width':maxheaderwidth+"px"});
            });
            var listview_scope = listview;

            $bs.scroll(function() {
                if ($(".headerscroll", listview_scope).length > 0) {
                    $bs = $(".bodyscroll", listview_scope);
                    $(".headerscroll", listview_scope)[0].scrollLeft = $bs[0].scrollLeft;
                }
            });
        }
    },
    autoSizeHeight: function(listview) {
        $bs = $(".bodyscroll", listview);
        if ($bs.length > 0) {
            $(window).on('resize', function() {
                log('resizing');
                var head_height = $(".headerscroll", listview).outerHeight() || 0;
                var foot_height = $(".footerscroll", listview).outerHeight() || 20; // should be fixed by height of footerscroll
                var height = $(window).height() - head_height- foot_height - 20;
                if ($bs.offset()) height = height - $bs.offset().top;
                if ($("#detailform").length > 0) height -= 0;
                if ($(".fixedheight").length > 0) height -= $(".fixedheight").outerHeight();
                if ($("#formcontrols").length > 0) height = Math.max(100, height - $("#formcontrols").outerHeight());
                if ($("#datadetailform").length > 0) {
                   height = height * 0.4;
                }
                $bs.css('max-height', height + 'px');
            });
        };
    },
    autoSizeHeightDetail: function(listview) {
        $bs = $(".bodyscroll", listview);
        if ($bs.length > 0) {
            $(window).bind('resize', function() {
                $bs = $(".bodyscroll", listview);
                var height = $(window).height() - $bs.offset().top - 36;
                $bs.css(($.browser.msie && $.browser.version < 7 ? '' : 'max-') + 'height', height + 'px');
            }).trigger('resize');
        };
    },
    onSelectRowHandler: null,
    makeEditable: function(table) {

        var keys = new KeyTable( {
            "table": table[0]
        } );

        /* Apply a return key event to each cell in the table */
        $('tbody td', table).each( function () {
            keys.event.action( this, function (nCell) {
                if ($(nCell).find("span.edit").length > 0) {

                    /* Block KeyTable from performing any events while jEditable is in edit mode */
                    keys.block = true;

                    /* Initialise the Editable instance for this table */
                    $(nCell).find("span.edit").editable( function (currentvalue) {
                        /* Submit function (local only) - unblock KeyTable */
//                        var originalvalue = $(this).parent().find("input:hidden").val();
                        keys.block = false;
                        return sVal;
                    }, {
                        "onblur": 'submit',
                        "onreset": function(){
                            /* Unblock KeyTable, but only after this 'esc' key event has finished. Otherwise
                             * it will 'esc' KeyTable as well
                             */
                            setTimeout( function () {keys.block = false;}, 0);
                        }
                    } );

                    /* Dispatch click event to go into edit mode - Saf 4 needs a timeout... */
                    setTimeout( function () { $(nCell).find("span.edit").click(); }, 0 );
                }
            } );
        } );
    },
    enableTooltips: function(container, options) {
        $(container).tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body",
            html: true,
        });
    },
    pinRecords: function(e) {
        e.preventDefault();
//        var currentRow = $("#datalistview tr:has(td.focus)").add("#datalistview tr.focus").add("#datalistview tr.focusrow");
        var currentRow = $("#datalistview input:checkbox.deletebox:checked");
        if (currentRow.length == 0) {
            alert('Selecteer eerst een of meerdere records.');
            $(window).data('keypressed_ctrl', false);
        } else {
            if (Polaris.Dataview.selectedRows().length == 0) {
                $("input:checkbox.deletebox",currentRow).trigger('click');
            }
            $("#dataform input[name=_hdnAction]").val("plr_pinrecords");
            $("#dataform input[name=_hdnKeyPressed]").val($(window).data('keypressed_ctrl') == true ? 'ctrl' : '');
            $("#dataform input[name=_hdnPlrSes]").val(sessionStorage.plrses);
            $("#dataform").trigger('submit');
        }
    },
    pinRecord: function(e) {
        e.preventDefault();
        var currentRow = $(e.target).parents('tr');
        var cb = $("input:checkbox.deletebox", currentRow);
        cb.prop('checked', true);
        $("input[name=_hdnAction]", cb[0].form).val("plr_pinrecords");
        $("input[name=_hdnKeyPressed]", cb[0].form).val($(window).data('keypressed_ctrl') == true ? 'ctrl' : '');
        $("input[name=_hdnPlrSes]", cb[0].form).val(sessionStorage.plrses);
        cb[0].form.submit();
    },

    pinRecordAjax: function(e) {
        e.preventDefault();

        var $form = $("#dataform");
        var recordid = false;
        var elem = $(e.target);
        var $currentRow = elem.parents('tr');

        if (!isUndefined(elem.data('pinrecordid'))) {
            recordid = elem.data('pinrecordid');
        } else if ($currentRow.length == 0) {
            var selrows = Polaris.Dataview.selectedRowsValues('ROWID', false, true);
            if (selrows.length > 0) {
                recordid = selrows.join('|');
            } else {
                recordid = $form.find(":input[name=_hdnRecordID]").val();
            }
        } else {
            var $cb = $("input:checkbox.deletebox", $currentRow);
            $form = $($cb[0].form);
            recordid = $currentRow[0].id.split('@')[1];
        }

        var url = _ajaxquery;

        var data = {
          '_hdnAction':'plr_pinrecords'
        , '_hdnDatabase': $form.find(":input[name=_hdnDatabase]").val()
        , '_hdnKeyPressed': ($(window).data('keypressed_ctrl') == true ? 'ctrl' : '')
        , '_hdnPlrSes': sessionStorage.plrses
        , '_hdnTable': $form.find(":input[name=_hdnTable]").val()
        , '_hdnForm': $form.find(":input[name=_hdnForm]").val()
        , '_hdnRecordID': recordid
        };

        $.postJSON(url, data, function(data, textStatus) {
            if (textStatus == 'success' && data.result != false) {
                Polaris.Ajax.loadPins();
            } else {
                alert("An error has occured: "+data.error);
            }
        });
    },
    __deleteRecordsAjax: function(form, recordids, callback) {
        // const $form = $("#dataform");
        const json_recordids = Array.isArray(recordids) ? recordids.join('|') : recordids;
        let data = {
          '_hdnAction':'plr_delete'
        , '_hdnDatabase': form.find(":input[name=_hdnDatabase]").val()
        , '_hdnKeyPressed': ($(window).data('keypressed_ctrl') == true ? 'ctrl' : '')
        , '_hdnPlrSes': sessionStorage.plrses
        , '_hdnTable': form.find(":input[name=_hdnTable]").val()
        , '_hdnForm': form.find(":input[name=_hdnForm]").val()
        , '_hdnRecordID': json_recordids
        };
        $.postJSON(_ajaxquery, data, function(data, textStatus) {
            if (callback) callback(textStatus);

            if (textStatus == 'success' && data.result != false) {
                Polaris.Dataview.removeRows(Polaris.Dataview.selectedRows(recordids));
            } else {
                alert("An error has occured: "+data.error);
            }
        });
    },
    deleteOneRecordAjax: function(e, recordid, callback) {
        e.preventDefault();
        let form = $(e.target).parents('form') || $("#dataform");
        Polaris.Dataview.confirmDeleteAction(1, function(dialogItself) {
            if (!dialogItself) return;
            Polaris.Dataview.__deleteRecordsAjax(form, recordid, callback);
            dialogItself.close();
        });
    },
    deleteRecordsAjax: function(e, callback) {
        e.preventDefault();
        let form = $(e.target).parents('form').length > 0 ? $(e.target).parents('form') : $("#dataform");
        Polaris.Dataview.confirmDelete(form, function(dialogItself) {
            if (!dialogItself) return;

            var recordids = Polaris.Dataview.selectedRowsValues('ROWID', false, true);
            if (recordids.length > 0) {
                Polaris.Dataview.__deleteRecordsAjax(form, recordids, callback);
                dialogItself.close();
            }
        });
    },
    removeRows: function(rows) {
        rows.fadeOut('slow', function() {
            $(this).remove();
        });
    },
    copyRecord: function(e) {
        e.preventDefault();

        var currentRow = $("#datalistview tr:has(td.focus)");
        currentRow.clone(true).prependTo(currentRow.parents("tbody"));
    },

    currentFormAction: null,

    hideModalForm: function(hash) {
        if (typeof hash !== 'undefined') {
            hash.w.hide();
            hash.o.remove();
        }
    },
    hideFormActionDialog: function(hash) {
        const Self = Polaris.Dataview;

        Self.hideModalForm(hash);
        var actionExecutionOkay = hash.w && $("input[name=actionnotexecuted]",hash.w).length == 0;
        if (actionExecutionOkay) {
            var $refreshType = $(Polaris.Dataview.currentFormAction);
            if ($refreshType.hasClass("refresh")) {
                setTimeout(() => { location.reload(true); }, 200);
            } else if ($refreshType.hasClass("ajaxrefresh")) {
                Polaris.Dataview.reloadSelectedRows();
            }
        }
    },

    confirmFormAction: function(hash) {
        const Self = Polaris.Dataview;

        if ($("#_fldDONTSHOWMESSAGE").length == 1) {
            // if the parameterform is empty then don't show it (when OKMESSAGE is empty)
            $("#ajaxdialog").jqmHide();
        } else {
            $("#parameterform").on('submit', function(e) { e.preventDefault(); });
            $("#parameterform button.savebutton,#parameterform button.cancelbutton").on('click', function(e){
                e.preventDefault();
                if ($(this).hasClass('disabled')) return false;
                if (this.value == 'yes') {
                   if ($("#parameterform").valid()) {
                        if ($("#parameterform").attr('target') == '_blank') {
                            $("#parameterform").trigger('submit');
                            $("#ajaxdialog").jqmHide();
                            hash.c.onHide = Self.hideFormActionDialog;
                        } else {
                            $("#ajaxdialog").jqmHide();
                            hash.c.onHide = Self.hideFormActionDialog;
                            Self.executeFormAction(hash.t, $("#parameterform").serialize());
                        }
                    } else {
                        alert($.t('plr.enter_required_fields'));
                    }
                } else {
                    hash.c.onHide = null;
                    $("#ajaxdialog").jqmHide();
                }
            });

            // uitzoeken ivm backspace en spatie probleem
            $(":input[autofocus]:first", hash.w).trigger('focus');
            $(":input:visible:enabled[type=text][value='']:first", hash.w).trigger('focus');

        }
    },

    beforeShowParamterForm: function(hash) {
        hash.w.show().html('<h3>'+$.t('plr.please_wait')+'</h3><i class="fa fa-spinner fa-spin"></i>');
    },

    enrichUrl: function(target, params, raw, onlywhenchecked) {
        const Self = Polaris.Dataview;

        let url = $(target).attr('href');
        let rel = $(target).attr('rel');
        rel = (rel == 'undefined')?'':rel;

        $.each(rel.split(' '), function(i, text) {
            var values = Self.selectedRowsValues(text, raw, onlywhenchecked);
            if (raw) {
                values = escape(values);
            }
            if (text == 'ROWIDFILTER' || text == 'ROWID') rowidvalues = values;
            url = url + '&' + text + '=' + values;

        });

        if (typeof params != 'undefined') {
            url = url + '&' + params;
        }

        if (rowidvalues == '') {
            url = url + '&geenrowid';
            log('geen rowid');
        }

        return url;
    },

    executeFormAction: function(e, params) {
        log('executeFormAction');
        const Self = Polaris.Dataview;

        e.preventDefault();
        var isDisabled = $(e.target).prop('disabled') || 'false';
        if (isDisabled == 'false') {

            if ($(e.target).attr('href') != '') {
                Self.currentFormAction = e.target;
            }
            var selectedRows = Self.selectedRows();
            if (!$(Self.currentFormAction).hasClass('needsonerow')
            || selectedRows.length == 1
            || $("input[name=_hdnRecordID]").length == 1) {
                var url = Polaris.Dataview.enrichUrl(Self.currentFormAction, params);
                var $ad = $("#ajaxdialog");
                $ad.jqm({onLoad:Self.confirmFormAction, ajax:url, onShow: Self.beforeShowParamterForm, modal: true}).jqmShow();

            } else {
                if (selectedRows.length == 0)
                    alert($.t("plr.no_item_selected"));
                else
                    alert($.t('plr.more_than_one_item_selected'));
            }
        }
    },

    executeCustomFormAction: function(e, params) {
        log('executeCustomFormAction');
        const Self = Polaris.Dataview;

        e.preventDefault();
        var isDisabled = $(e.target).attr('disabled') || 'false';
        if (isDisabled == 'false') {
            if ($(e.target).prop('href') != '') {
                Self.currentFormAction = e.target;
            }
            var selectedRows = Self.selectedRows();
            if (!$(Self.currentFormAction).hasClass('needsonerow')
            || $("form.dataform.formview").length == 1
            || selectedRows.length == 1) {
                var raw = false;
                var onlywhenchecked = $(Self.currentFormAction).hasClass('needsonerowchecked')||$(Self.currentFormAction).hasClass('needsonerowormorechecked');
                var url = Polaris.Dataview.enrichUrl(Self.currentFormAction, params, raw, onlywhenchecked);
                // DIRECT AFDRUKKEN
                if ($(Self.currentFormAction).attr('target')=='_blank') {
                    Polaris.Base.rawPopup(url+'&notfirst=true');
                } else {
                    $ad = $("#ajaxdialog");
                    $ad.jqm({onLoad:Self.confirmFormAction, onHide: Self.hideFormActionDialog, ajax:url, onShow: Self.beforeShowParamterForm, modal: true}).jqmShow();
                }
            } else {
                if (selectedRows.length == 0)
                    alert($.t("plr.no_item_selected"));
                else
                    alert($.t('plr.more_than_one_item_selected'));
            }
        }
    },

    executeFillParamAction: function(e, params) {
        log('executeFillParamAction');
        const Self = Polaris.Dataview;

        e.preventDefault();

        if ($(e.target).prop('href') != '') {
            Self.currentFormAction = e.target;
        }
        var selectedRows = Self.selectedRows();
        if (($(Self.currentFormAction).hasClass('needsonerow') && selectedRows.length == 1)
            ||
            ($(Self.currentFormAction).hasClass('needsonerowormore') && selectedRows.length >= 1)
            || $("form.dataform.formview").length == 1
            ) {
            var raw = true; // no translation of special characters, because Jasper needs the real ROWID's
            var onlywhenchecked = false;
            var url = Polaris.Dataview.enrichUrl(Self.currentFormAction, params, raw, onlywhenchecked);

            Polaris.Base.rawPopup(url);
        } else {
            if (selectedRows.length == 0)
                alert($.t("plr.no_item_selected"));
            else
                alert($.t('plr.more_than_one_item_selected'));
        }
    },

    hotAjaxUpdate: function(form, field, recordid, value, disablenotification) {
        $form = $('#'+form);
        if ($form.length > 0) {
            disablenotification = disablenotification || false;
            var dbid = $('input[name=_hdnDatabase]', $form).val();
            var tablename = $('input[name=_hdnTable]', $form).val();
            Polaris.Ajax.recordUpdate(dbid, tablename, recordid, field, value, function() {
                if (!disablenotification)
                    Polaris.Base.modalMessage($.t('plr.changes_saved'));
            });
        } else {
            alert('Form '+form+' could not be found.');
        }
    },

    hotRecordUpdate: function(form, recordid, fieldnames, fieldvalues) {
        form = $('#'+form.replace('.','\\.'));
        if (form.length > 0) {
            $('input[name=_hdnHotUpdateRecord]', form).val(recordid);
            $('input[name=_hdnHotUpdateFields]', form).val(fieldnames);
            $('input[name=_hdnHotUpdateValues]', form).val(fieldvalues);
            form.submit();
        } else {
            alert('Form hotupdate could not be found.');
        }
    },

    swapRecords: function(form, swaprecord1, swaprecord2, swapcolumn) {
        form = $('#'+form);
        if (form.length > 0) {
            $('input[name=_hdnSwapRecord1]', form).val(swaprecord1);
            $('input[name=_hdnSwapRecord2]', form).val(swaprecord2);
            $('input[name=_hdnSwapColumn]', form).val(swapcolumn);
            form.submit();
        } else {
            alert('Form swapform could not be found.');
        }
    },

    // Polaris.Dataview.focusedRow
    focusedRow: function() {
        var row = $("#datalistview tr:has(td.focus)").add("#datalistview tr.focus").add("#datalistview tr.focusrow");
        row = $.uniqueSort(row);
        return row;
    },

    selectedRows: function(recordids) {
        recordids = recordids || false;
        let rows = [];

        if (recordids) {
            rows = $('.dr').filter(function(index) { return recordids.includes(this.id.substring(4,32+4)); });
        } else {
            rows = $("#datalistview tr.H").each(function(){ var elem = $(this); elem.prop('rowid', elem.prop('id').substr(4,32)); });
        }

        if (rows.length == 0) {
            rows = $("#datalistview tr:has(td.focus)").add("#datalistview tr.focus").add("#datalistview tr.focusrow");
        }
        if (rows.length == 0 && $("#datalistview tr.dr").length == 1) {
            rows = $("#datalistview tr.dr:first");
        }
        rows = $.uniqueSort(rows);
        return rows;
    },

    selectedRowsValues: function(fieldname, raw, checkboxonly) {
        var result = [];

        raw = raw || false;
        checkboxonly = checkboxonly || false;

        if (fieldname == 'ROWID' || fieldname == 'ROWIDFILTER' || fieldname == '_hdnRecordID') {

            // Take all checked checkboxes and return the ID's as an array
            $("#datalistview tr.H").each(function() {
                if ($(this).val() != '')
                    var vl = $(this).val();
                else
                    var vl = $(this).prop('id').substr(4,32);

                if (raw) vl = encodeURIComponent(Polaris.Base.customUrlDecode(vl));
                if (fieldname == 'ROWIDFILTER') {
                    vl = "'"+vl+"'";
                }
                result.push( vl );
            });
            // If there are no checked checkboxes, then take all the focused rows and return the ID's as an array
            if (result.length == 0 && !checkboxonly) {
                $("#datalistview tr:has(td.focus)").add("#datalistview tr.focus").add("#datalistview tr.focusrow").each(function() {
                    var vl = $(this).prop('id').substr(4,32);
                    if (raw) vl = encodeURIComponent(Polaris.Base.customUrlDecode(vl));
                    if (fieldname == 'ROWIDFILTER') {
                        vl = "'"+vl+"'";
                    }
                    result.push( vl );
                });
            }
            // As a last resort, if we are in formview, return the current record ID
            if (result.length == 0 && !checkboxonly) {
                const recordfield = $('input[name=_hdnRecordID]');
                if (recordfield.length > 0) {
                    let vl = recordfield.val();
                    if (fieldname == 'ROWIDFILTER') {
                        vl = "'"+vl+"'";
                    }

                    result.push( vl );
                }
            }
        } else {
            var selector = "_fld"+fieldname;
            // take the checked rows
            $("#datalistview tr.H input."+selector+",#"+selector).each(function() {
                var vl = $(this).val();
                if (vl != '') {
                    if (raw) vl = Polaris.Base.customUrlDecode(vl);
                    result.push( vl );
                }
            });
            // or use the one that has the focus
            if (result.length == 0 && !checkboxonly) {
                $("#datalistview tr:has(td.focus) input."+selector).add("#datalistview tr.focus input."+selector).add("#datalistview tr.focusrow input."+selector).each(function() {
                    var vl = $(this).val();
                    if (raw) vl = Polaris.Base.customUrlDecode(vl);
                    result.push( vl );
                });
            }
            // one row selected but not highlighted, then use that one
            if (result.length == 0 && $("#datalistview tr.dr").length == 1) {
                $("#datalistview tr.dr:first input."+selector).each(function() {
                    var vl = $(this).val();
                    if (raw) vl = Polaris.Base.customUrlDecode(vl);
                    result.push( vl );
                });
            }
        }
        return result;
    },

    fadeRow: function(rowid) {
        if (document.getElementById(rowid)) {
            Polaris.Visual.yellowFade(document.getElementById(rowid));
        }
    },

    confirmDeleteAction: function(count, callback_ok) {
        const self = Polaris.Dataview;

        if (count === 0) {
            alert($.t('plr.no_item_selected'));
            return false;
        } else {
            var txt = $.t('plr.confirm_delete_item', {'count':count});
            Polaris.Base.modalDialog(txt, {
                yes: callback_ok,
                cancel: function (dialogItself) {
                    dialogItself.close();
                }
            });
        }
    },

    confirmDelete: function(theform, callback_ok) {
        Polaris.Dataview.confirmDeleteAction(Polaris.Dataview._countChecked(theform || this.form), callback_ok);
    },

    _countChecked: function(form) {
        return $("input:checked.deletebox",form).not('[name=_hdnAllBox]').length;
    },

    rowHighlite: function(row) {
        const self = Polaris.Dataview;

        var rowobject = $($(row).parents("tr")[0]);

        var rowid = $(rowobject).prop('id');
        var checkboxname = rowid.replace('row','rec');//.replace(/:/g,"\\\\:").replace(/\./g,"\\\\.");
        var cb = $(document.getElementById(checkboxname)); // do it with normal DOM, because @ does not work in jQuery (?)

        if (cb.length) {
            cb.trigger('click');
            self.checkCheckBox(cb, true);
        }
    },

    _checkCheckBox: function(row) {
        const self = Polaris.Dataview;
        if ($(row).prop('checked')) {
            self._hL(row);
        }	else {
            self._dL(row);
        }
    },

    checkCheckBox: function(row, userevent) {
        const self = Polaris.Dataview;
        if (!userevent)
            $(row).prop('checked', !$(row).prop('checked'));
        self._checkCheckBox(row);
        self.syncCheckboxes(row.form);
        Polaris.Dataview.toggleListviewButtons($(row).prop('checked'));
    },

    checkAllCheckBoxes: function(e) {
        const self = Polaris.Dataview;
        $("input[type=checkbox][name!=_hdnAllBox].deletebox").each( function() {
            this.checked = e.target.checked;

            self._checkCheckBox(this);
        });
        Polaris.Dataview.toggleListviewButtons(this.checked);
    },

    toggleListviewButtons: function(active) {
        // EXPLICIET AANVINKEN, NIET ALLEEN FOCUS        if ($("table#datalistview tr.focusrow").length > 0) active = true;
        var countChecked = Polaris.Dataview._countChecked();
        var buttons = $('#_hdnDeleteButton,#_hdnPinButton,#_hdnBulkChangeButton');

        if (countChecked === 0) {
            buttons.attr('disabled','true')
                   .addClass('disabled');
            buttons.each(function() {
                $(this).removeClass('btn-'+$(this).data('colorclass'));
                $(this).addClass('btn-default');
            });
        } else {
            buttons.removeAttr('disabled')
                   .removeClass('disabled');
            buttons.each(function() {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-'+$(this).data('colorclass'));
            });
        }

        Polaris.Dataview.toggleListviewFormActions();
    },

    toggleListviewFormActions: function() {
        var countChecked = Polaris.Dataview._countChecked();
        var countChecked = Polaris.Dataview._countChecked();

        if (countChecked == 0 && $("table#datalistview tr.dr").length == 1)
            countChecked = 1;
        if (countChecked == 0 && $("form.dataform.formview").length == 1)
            countChecked = 1;
        if (countChecked == 0)
            count = $("table#datalistview tr.focusrow").length + $("table#datalistview tr.focus").length;
        else
            count = countChecked;

        if (count == 0) {
            $('.buttons .needsonerow').attr('disabled',true).addClass('disabled');
            $('.buttons .needsonerowormore').attr('disabled',true).addClass('disabled');
        } else if (count == 1) {
            $('.buttons .needsonerow').attr('disabled',false).removeClass('disabled');
            $('.buttons .needsonerowormore').attr('disabled',false).removeClass('disabled');
        } else if (count > 1) {
            $('.buttons .needsonerow').attr('disabled',true).addClass('disabled');
            $('.buttons .needsonerowormore').attr('disabled',false).removeClass('disabled');
        }

        if (count == 1) {
            $('.buttons .needsonerowchecked').attr('disabled',false).removeClass('disabled');
            $('.buttons .needsonerowormorechecked').attr('disabled',false).removeClass('disabled');
        } else if (count > 1) {
            $('.buttons .needsonerowchecked').attr('disabled',true).addClass('disabled');
            $('.buttons .needsonerowormorechecked').attr('disabled',false).removeClass('disabled');
        }
    },

    syncCheckboxesFromGUI: function() {
        $("input[type=checkbox][name!=_hdnAllBox].deletebox:checked").each( function() {
            Polaris.Dataview._hL(this);
        });
    },

    syncCheckboxes: function(f) {
        if (f) {
            var total = $("input[type=checkbox][name!=_hdnAllBox].deletebox", f).length;
            var active = Polaris.Dataview._countChecked(f);
            if (f._hdnAllBox) { f._hdnAllBox.checked = (total==active); }
        }
    },

    _hL: function(row){
        $($(row).parents("tr")[0]).addClass("H");
    },

    _dL: function(row){
        $($(row).parents("tr")[0]).removeClass("H");
    },

    eventDelegate: function(e) {
        var object = e.target;
        var p = $(object);

        if (object.tagName.toLowerCase() === 'input' && p.hasClass('rowselect')) {
            Polaris.Dataview.checkCheckBox(object, true);
        }

        if (object && object.tagName.toLowerCase() === 'a' && p.hasClass('swaprecords')) {
            var swaprecord1 = p.parent().parent().attr('id').substr(4,32);
            var swaprecord2 = p.attr('swap');
            var swapcolumn = p.attr('column');
            return Polaris.Dataview.swapRecords(p.attr('target'), swaprecord1, swaprecord2, swapcolumn);
        }

        if (object && object.tagName.toLowerCase() === 'input' && p.hasClass('togglevisible')) {
            var recordid = p.attr('name').replace('_hdnVisibleButton_','');
            var state = p.prop('checked');
            var values = p.data('values').split('|');
            var togglecolumn = p.attr('column');
            return Polaris.Dataview.hotAjaxUpdate(p.data('target'), togglecolumn, recordid, values[+state]);
        }
    },
    addQueryStringAsHidden: function (form){
        form = $(form);
        let url = form.attr("action");
        if (url.includes("?") === false) return false;

        let index = url.indexOf("?");
        let action = url.slice(0, index)
        let params = url.slice(index);
        url = new URLSearchParams(params);
        for (param of url.keys()){
            let paramValue = url.get(param);
            let attrObject = {"type":"hidden", "name":param, "value":paramValue};
            let hidden = $("<input>").attr(attrObject);
            form.append(hidden);
        }
        form.attr("action", action)
    },
    processSearchValues: function(form, casesensitive, fullword, fromstart) {
        if ($(form).length > 0) {

            $("input[type=search]", $(form)).each(function() {
                var value = $(this).val();
                if (value != '') {
                    // add the year if only the day and month are supplied
                    if ($(this).attr('alt') == 'date_nl') {
                        var matches;
                        if (matches = value.match(/^(\d{1,2})-(\d{1,2})$/)) {
                            var d = new Date();
                            value = value + '-' + d.getFullYear();
                            $(this).val(value);
                        }
                    }
                    if (value.indexOf('%') > -1) {
                        // leave the value as is
                    } else if (value != '') {
                        if (!fullword && !fromstart) {
                            value = '%' + value + '%';
                        } else {
                            if (fullword) {
                                value = "'" + value + "'";
                            } else if (fromstart) {
                                value = value + "%";
                            } else {
                                value = "%" + value;
                            }
                        }
                        $(this).val(value);
                    }
                }
            });
        }
    },

    /**
    * In a hotedit listview the select tags make use of a common array of
    * values (for the options).
    * This function fills the select tag with the items of the associated array
    */
    setSelectValues: function(index, elem) {
        var lplsource = window[elem.getAttribute("valuepicklist")];

        var childIndex = hasElementClass(elem,"required") ? 0 : 1;
        var ii = lplsource.length;
        for (i = 0; i < ii; i++) {
            elem.options[childIndex] = new Option(lplsource[i][1], lplsource[i][1]);
            childIndex++;
        }
        //Select the current option
        elem.value = elem.getAttribute("currentvalue");
    },

    getLookupPicklistValue: function(id, key) {
        if (typeof id == 'undefined') return key;

        var lplsource = window[id+'_lookuppicklist'];
        if (typeof lplsource != 'undefined') {
            return lplsource[key];
        } else {
            return key;
        }
    },

    reloadSelectedRows: function() {
        const Self = Polaris.Dataview;

        var $selectedRows = Self.selectedRows();
        var databaseid = $('#dataform input[name=_hdnDatabase]').val();
        var tablename = $('#dataform input[name=_hdnTable]').val();
        var selectedRecordIDs = Polaris.Dataview.selectedRowsValues('ROWID', false, false);
        Polaris.Ajax.loadRowsValues(databaseid, tablename, selectedRecordIDs, function(data) {
            if (data.result == true) {
                $.each(data.new_values, function(index, elems) {
                    var row = Polaris.Base.customUrlEncode(elems['PLR__RECORDID']);
                    Polaris.Dataview.populateRow(row, elems);
                });
            } else {
                alert("An error has occured: "+data.error);
            }
        });
    },

    /**
     * Sanitize a string to be used as a CSS class name
     * @param {*} input
     * @returns string
     */
    sanitizeClassName: function(input) {
        // Remove any characters that are not allowed in CSS class names
        var sanitized = input.replace(/[^a-zA-Z0-9_-]/g, '');

        return sanitized;
    },

    /**
    * In a hotedit listview when a value changed, the whole row is returned.
    * With this function a table row (TR) is populated with these returned values.
    */
    /* Polaris.Dataview.populateRow */
    populateRow: function(rowid, fieldvalues) {
        var $hiddenfield = showvalue = displayfield = null;
        for (field in fieldvalues) {
            var f = field.toUpperCase();
            var selector = "_fld"+f+"@"+rowid; //.replace('^',"\\^").replace('+',"\\+");
            $hiddenfield = $(document.getElementById(selector));
            if ($hiddenfield.length > 0) {
                let td = $hiddenfield.parents("td");
                let tr = $hiddenfield.parents("tr");

                $displayfield = td.find('span');
                value = fieldvalues[field];

                if ($hiddenfield.attr('name').indexOf('@@FLOATCONVERT') > -1) {
                    value = Polaris.Base.formatFloat(value);
                }
                let prev_value = $hiddenfield.val();
                $hiddenfield.val(value);

                showvalue = value||'';

                if ($displayfield.hasClass('select')) {
                    showvalue = Polaris.Dataview.getLookupPicklistValue(f, value);
                }
                $displayfield.text(showvalue);

                // Fix css styling (fval)
                const fieldUpper = field.toUpperCase();
                const baseCssValue = 'fval_';
                const cssValueToRemove = Polaris.Dataview.sanitizeClassName(`${baseCssValue}${prev_value}`);
                const cssValueToAdd = Polaris.Dataview.sanitizeClassName(`${baseCssValue}${value}`);
                const cssFieldValueToRemove = Polaris.Dataview.sanitizeClassName(`${baseCssValue}${fieldUpper}_${prev_value}`);
                const cssFieldValueToAdd = Polaris.Dataview.sanitizeClassName(`${baseCssValue}${fieldUpper}_${value}`);
                td.removeClass(cssValueToRemove).addClass(cssValueToAdd);
                td.find('span:first').removeClass(cssFieldValueToRemove).addClass(cssFieldValueToAdd);
            }
        }
    }
});
