/***
*
*    All the form functions of Polaris
*
**/
Polaris.Form = {};
Base.update(Polaris.Form, {

    /** @id Polaris.Form.userIsEditing **/
    userIsEditing: false,

    /** @id Polaris.Form.validateLowerCase */
    validateLowerCase: function(e) {
        e = e.target;
        if (e) { e.value = e.value.toLowerCase(); }
    },

    /** @id Polaris.Form.validateUpperCase */
    validateUpperCase: function(e) {
        e = e.target;
        if (e) { e.value = e.value.toUpperCase(); }
    },

    /** @id Polaris.Form.validateFloat */
    validateFloat: function(e) {
        e = e.target;
        if (e) { e.value = e.value.replace(',','.'); }
    },

    lastKeyPressed: null,

    co2nvertToKeyValuePairs: function(array) {
        var result = {};
        $.each(array, function(index, item) {
            result[item.name] = item.value;
        });
        return result;
    },

    convertToKeyValuePairs: function(array) {
        var result = {};
        $.each(array, function(index, item) {
            if (result[item.name] !== undefined) {
                if (!Array.isArray(result[item.name])) {
                    // Convert to array if not already an array
                    result[item.name] = [result[item.name]];
                }
                // Append the value to the existing array
                result[item.name].push(item.value);
            } else {
                // If the key doesn't exist yet, create it
                result[item.name] = item.value;
            }
        });
        return result;
    },
    /** @id Polaris.Form.postFormViaAjax */
    postFormViaAjax: function($form) {
        $("#btnSaveForm").addClass('disabled');
        var senddata = $form.serializeArray();
        Polaris.Ajax.postJSON(_ajaxquery, senddata, null, $.t('plr.changes_saved')
        , function(data) {
            $("#btnSaveForm").removeClass('disabled');

            // Save action valid
            var newActionOption = false;

            for (i=0; i<senddata.length; i += 1) {
                if (senddata[i].name === "_hdnNewAction") {
                    newActionOption = senddata[i].value == 'true';
                }
            }

            // check if save action should result in New record form
            if (newActionOption) {
                window.location.reload();
            } else {
                $(".cancelbutton").trigger('click');
                //window.location = $(".cancelbutton").prop('href');
            }
        }, function(data) {
            $("#btnSaveForm").removeClass('disabled');
            Polaris.Base.modalDialog(
                data.error + '<br>' + data.detailed_error,
                { close: function() {}, type: 'type-danger' }
            );
        });
    },
    /** @id Polaris.Form.addFormValidationMethods */
    addFormValidationMethods: function() {
        jQuery.validator.addMethod("validation_elfproef", function( value, element ) {
            return this.optional(element) || Polaris.Base.validateElfProef(value);
        }, $.t('validation.enter_valid_elf_proef') );
        jQuery.validator.addMethod("validation_bsn", function(value, element) {
    		return this.optional(element) || Polaris.Base.validateBSN(value);
        	}, $.t("validation.enter_valid_bsn")
        );
        jQuery.validator.addMethod("validation_email", function(value, element) {
    		return this.optional(element) || /\w{1,}[@][\w\-]{1,}([.]([\w\-]{1,})){1,3}$/.test(value);
        	}, $.t("validation.invalid_email")
        );
        jQuery.validator.addMethod("validation_date", function(value, element) {
            return this.optional(element) || /^\d{2}-\d{2}-\d{4}$/.test(value);
        }, $.t("validation.invalid_date"));
        jQuery.validator.addMethod("telefoon", function(phone_number, element) {
    		return this.optional(element) || phone_number.length >= 10;
        }, $.t("validation.phone_number_check"));
        jQuery.validator.addMethod("gebdatum", function(datum, element) {
    		return this.optional(element) || Polaris.Base.isMinimumAge(datum, 10)
        }, $.t('validation.person_older_than_10'));
        jQuery.validator.addMethod("huisnr", function(huisnr, element) {
            return this.optional(element) || Polaris.Base.isInteger(huisnr)
        }, $.t('validation.housenumber_check'));
        jQuery.validator.addMethod( "nowhitespace", function( value, element ) {
            return this.optional( element ) || /^\S+$/i.test( value );
        }, $.t('validation.no_whitespace') );
    },
    /** @id Polaris.Form.initializeFormValidation */
    initializeFormValidation: function(form) {
        this.addFormValidationMethods();

        $(form).validate({
            debug: false,
            validateHiddenFields: true,
            focusInvalid: true,
            ignore: ":hidden:not(.required)",
            errorPlacement: function(error, element) {
                if(element.hasClass('select2') && element.next('.select2-container').length) {
                    error.insertAfter(element.next('.select2-container'));
                } else if (element.prop('type') === 'radio' && element.parent('.radio-inline').length) {
                    error.insertAfter(element.parent().parent());
                } else if (element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
                    error.appendTo(element.parent().parent());
                } else {
                    error.insertAfter(element);
                }
            },
            invalidHandler: function(event, validator) {
                $(validator.invalidElements()).each(function(index, elem) {
                    if ($(elem).parents('.tab-pane').length > 0) {
                        let id = $(elem).parents('.tab-pane').prop('id');
                        let warning = $('<i class="material-symbols-outlined md-1x" style="color:red">error</i>');
                        if ($('#tab_'+id+ ' .material-symbols-outlined').length == 0) {
                            $('#tab_'+id).append(warning);
                        }
                    } else {
                        log('no pane no gain');
                    };

                });
                // return false;
            },
            submitHandler: function(form) {
                // Remove all alerts from tabs
                $('.tabs-container .material-symbols-outlined').remove();

                if (Polaris.Form.lastKeyPressed == '13') {
                    event.preventDefault();
                    Polaris.Form.lastKeyPressed = 0;
                    if (Polaris.Base.modalDialog($.t("plr.save_record"),
                        {
                            yes: function() {
                                if ($(form).valid()) {
                                    Polaris.Form.postFormViaAjax($(form));
                                }
                            },
                            no: function(e) {
                                $(".cancelbutton").trigger('click');
                            }
                        }
                    ));
                } else {
                    Polaris.Form.postFormViaAjax($(form));
                }
            }
        });

        $(form).on('keydown', function(event) {
            Polaris.Form.lastKeyPressed = event.keyCode;
        });
    },
    /** @id Polaris.Form.initializeDatePickers */
    initializeDatePickers: function(container) {
        container = container || document;
        $('.input-daterange', container).datepicker({
            format: "dd-mm-yyyy",
            weekStart: 1,
            daysOfWeekHighlighted: "0,6",
            showOnFocus: false,
            calendarWeeks: true,
            todayHighlight: true,
            todayBtn: true,
            language: "nl",
            keyboardNavigation: false,
            forceParse: false,
            keepEmptyValues: true,
            autoclose: true
        });

        $('input.date_input,.input-group.date', container).datepicker({
            format: "dd-mm-yyyy",
            weekStart: 1,
            daysOfWeekHighlighted: "0,6",
            calendarWeeks: true,
            todayHighlight: true,
            todayBtn: true,
            language: "nl",
            keyboardNavigation: false,
            forceParse: false,
            keepEmptyValues: true,
            autoclose: true,
            orientation: 'bottom'
        });
    },
    /** @id Polaris.Form.initializeTimePickers */
    initializeTimePickers: function(container) {
        $('.inputtime').timepicker({
            timeFormat: 'H:i',
            forceRoundTime: true,
            showDuration: false,
            scrollDefault: 'now',
            startTime: '07:00',
            minTime: '00:00',
            maxTime: '23:45',
            dropdown: true,
            step: 15,
        }).on('mouseup', function(e) {
            e.currentTarget.select();
        });

        container = container || $('.datepair:first', container).parents('.panel-body');
        $(container).datepair({
            'timeClass': 'datepair',
            'anchor': 'null',
            // 'defaultTimeDelta': 3600000 // milliseconds
        })
    },

    /** @id Polaris.Form.setFocusFirstField */
    setFocusFirstField: function(form) {
        $(form).find('*').filter(':input:visible:not([readonly]):not([nofocus]):first')
        .one('focus',
            function() { var that = this; setTimeout( function() {
                $(that).trigger('select');
            }, 10) }
        )
        .trigger('focus');
    },

    /** @id Polaris.Form.checkVisibility */
    checkVisibility: function(elem) {
        /***
        Check the visibility of a element by traversing up to the top parent.
        ***/
        return (hasElementClass(elem, 'alwayscheckrequired'))?true:$(elem).is(":visible");
    },

    /** @id Polaris.Form.adjustLabelWidth */
    adjustLabelWidth: function(elements) {
        $(elements).each(function() {
            var min = 80;
            var lbls = $("label.label", this);
            lbls.each(function(i, val) { min = Math.max(min, $(val).text().length * 7); });
            lbls.each(function() {
                try {
                    $(this).css('width',(min+0)+'px');
                } catch(ex) {
                }
            });
        });
    },

    /** @id Polaris.Form.showAsPopBox */
    showAsPopBox: function(caption = 'Polaris', url, /* optional */ height, width) {
        height = height || (document.documentElement.clientHeight*0.70);
        width = width || (document.documentElement.clientWidth*0.70);
        GB_show(caption,url,height,width);
        return false;
    },

    /** @id Polaris.Form.closePopup */
    closePopup :function() {
        if (window.parent)
            GB_hide(window.parent.document);
        else
            GB_hide();
    },

    showImageUploader: function(e) {
        Polaris.Form.showOverlay('/app/facility/basicform/mediabeheer/?compact=1', '30vmax', null, {noblocking: true});
    },

    /** @id Polaris.Form.setAutoSuggest */
    setAutoSuggest: function() {
        var as_input = $(this);
//        var as_visibleinput = as_input.next('.as_showfield');
//        var valuefield = elem.sibling()prop('realname');
        var valuefieldname = as_input.attr('selectmasterfield');
        var varsearchname = "q";
        // in case the search covers one or more specific fields, then use the Extended Search option
        if (as_input.attr('searchflds') != '') {
            varsearchname = "qv[]";
        }
        /* Create search field and button */
        as_visibleinput = $('<input tabindex="2" type="text" label="..." class="as_showfield text appear_required" size="'+as_input.prop('size')+'" value="" />');
        as_button = $('<a href="'+_serverroot+'/app/macs/const/cursus/?limit=100&maximize=true&select=true&selectshowfield='+as_input.prop('captionflds')+'&qc[]='+valuefieldname+'&qv[]=" class="popbox" style="font-size:1.8em;text-decoration:none;"><img src="'+_serverroot+'/images/application_view_list.png" alt="+" /></a>');
        as_button.click(function(event) {
            event.preventDefault();
            Polaris.Form.showAsPopBox('', $(this).attr('href'));
        }).data('selectfield', as_input);

        $(this).after(as_visibleinput.add(as_button));

        Polaris.AutoSuggest.createAutoSuggest(as_input, as_visibleinput, {
            script: _serverroot+"/services/json"+as_input.attr('ajaxurl')+"/",
            params: {output:"json",varname: varsearchname, qcomp:"OR"},
            valuefield: valuefieldname,
            captionfields: as_input.attr('captionflds'),
            searchfields: as_input.attr('searchflds')
        });

    },

    /** @id Polaris.Form.setAutoSuggest3 */
    setAutoSuggest3: function(elem, params) {
        var me = $(elem);
        if (me.data('processed') == true) return;

        var uniqueId = new Date;
        me.prop('id', uniqueId.getTime());

        var valuefieldname = me.attr('selectmasterfield');
        var varsearchname = "q";
        // in case the search covers one or more specific fields, then use the Extended Search option
        if (me.attr('searchflds') != '') {
            varsearchname = "qv[]";
        }
        /* Create search field and button */
        var as_visibleinput = $('<input type="text" label="..." class="as_showfield text appear_required" value="" />');
        as_visibleinput.css('width',me.css('width'));

        // autoselect all text when clicking on field
        as_visibleinput
        .on('click',
            function() {$(this).trigger('focus').trigger('select')}
        )
        .on('keyup',
            function() { if (this.value == '') me.val(''); }
        );

        /* Copy the tabindex and delete the original one */
        as_visibleinput.attr('tabindex',me.attr('tabindex'));
        me.prop('tabindex', 0);

        as_button = $('<a tabindex="-1" href="'+_serverroot+me.attr("ajaxurl")+'?limit=200&maximize=true&select=true&selectfield='+me.attr('selectfield')+'&selectmasterfield='+me.attr('selectmasterfield')+'&selectshowfield='+me.attr('captionflds')+'&qc[]='+valuefieldname+'&qv[]=" class="popbox"><i class="fa fa-search-plus fa-lg"></i></a>');
        as_button.click(function(event) {
            event.preventDefault();
            Polaris.Form.showAsPopBox(false, $(this).attr('href'));
            // Store the selectfield (input) in the frame for easy reference
            $("#GB_frame").attr("selectfield", me.attr('id'));
        });

        as_button.attr('selectfield', me);

        $(elem).after(as_visibleinput.add(as_button));

        var url = _serverroot+'/services/list'+me.attr("ajaxurl");

        var saveValue = function($li) {
            var value = $li.data("extra")[0];
            // store value in the real form field (that gets posted)
            me.val(value);
            // call the onItemSelect function when a value or no value has been selected
            if (params && params.onItemSelect) {
                params.onItemSelect(me, value, as_visibleinput);
            }
        }
        as_visibleinput.autocomplete(url, {onItemSelect: saveValue, selectFirst: false, selectOnly: true, matchCase:1, autoFill: false, minChars: 1, cacheLength: false, maxItemsToShow: 200, mustMatch: 0, delay: 100, extraParams: params?params.extraParams:''});
        me.data('processed', true);
    },

    /** @id Polaris.Form.selectAutoSuggestRecord */
    selectAutoSuggestRecord: function(e) {
//        e.preventDefault();
        var elem = $(this);
        if (window.parent.parent) {
//            var maindoc = window.parent.parent.document;
            var maindoc = window.parent.document;

            var asfieldobject = false;

            var iframe = $("#GB_frame", maindoc);
            var asfieldobject = $("#"+iframe.attr('selectfield'), maindoc);
            if (asfieldobject.length == 0)
                asfieldobject = $("#"+Polaris.Base.getLocationVariable('selectfield'), maindoc);

//            var asfieldobject = maindoc.getElementById(Polaris.Base.getLocationVariable('selectfield'));
            if (asfieldobject.length > 0) {
                var asmasterfield = Polaris.Base.getLocationVariable('selectmasterfield'); //elem.prop('autosuggestmasterfield');
                var recordid = elem.attr('recordid');
                var doc = elem.parents("tr")[0].ownerDocument;
                var sourcefield;
                sourcefield = doc.getElementById("_fld"+asmasterfield+'@'+recordid);
                if (!sourcefield) {
                    sourcefield = $("#_fld"+asmasterfield, maindoc);
                }
    //                sourcefield = doc.getElementById("_fld"+asmasterfield);
                if (sourcefield) {
                    var showfield = asfieldobject.parent().find(".as_showfield");
                    var captionfields = asfieldobject.attr('captionflds');
                    if (captionfields) {
                        captionfields = captionfields.split(',');
                        var ii = captionfields.length;
                        var concatvalue = '';
                        for (var i=0;i<ii;i++) {
                            if (doc.getElementById('_fld'+captionfields[i]+'@'+recordid))
                                concatvalue += doc.getElementById('_fld'+captionfields[i]+'@'+recordid).value + ' ';
                        }
                        concatvalue = concatvalue.substring(0, concatvalue.length - 1);
                        showfield.val( concatvalue );
                        showfield.attr('lastSelected', concatvalue); // if not set, then autocomplete2.js clears the box
                    }
                    asfieldobject.val(sourcefield.value);

                    // trigger een 'echte' onchange() event, omdat het veld in een ander frame zit
                    if (asfieldobject[0].onchange)
                        asfieldobject[0].onchange();
                } else {
                    alert(sourcefield+" was not found in the current form. Please include the field in the form.");
                }
            } else {
                alert('Value was not set. Field "'+asfieldobject+'" was not available');
            }
        } else {
            alert('Value was not set. Main window was not available');
        }
        Polaris.Form.closePopup();
    },

    /** @id Polaris.Form.insertAtCursor */
    insertAtCursor: function(fieldId, myValue) {
        $('#'+fieldId).each(function() {
            var field = this;
            //IE support
            if (document.selection) {
                field.focus();
                sel = document.selection.createRange();
                sel.text = myValue;
            }
            //MOZILLA/NETSCAPE support
            else if (field.selectionStart || field.selectionStart ===  0 ) {
                var startPos = field.selectionStart;
                var endPos = field.selectionEnd;
                field.value = field.value.substring(0, startPos) + myValue + field.value.substring(endPos, field.value.length);
            } else {
                field.value += myValue;
            }
       });
    },

    groupFilter: function(event) {
        var $elem = $(event.target || event);
        var value, name;

        // Check if $elem is a button or other element
        if ($elem.is('button')) {
            value = $elem.data('value');
            name = $elem.data('name');
        } else {
            value = $elem.val();
            name = $elem.attr('name');
        }
        window.location = Polaris.Base.addLocationVariable(name, value);
    },

    showOverlay(url, width, beforeOverlay, options) {
        // initialize the overlay
        width = width || '60vmax';
        options = options || {};
        $(document).on('keypress', handleKeyPressOffCanvas);

        $('body').toggleClass('off-canvas-active');
        if (options.noblocking) {
            $('body').toggleClass('off-canvas-noblocking');
        }

        $(".off-canvas").css({'width':width}).load(url, function() {
            if (beforeOverlay) beforeOverlay();
            $(".jqmClose").on('click', Polaris.Form.hideOverlay);
        });

        $(document).on('mouseup touchend', handleMouseUpOffCanvas);
    },

    hideOverlay() {
        // remove the event listeners
        $(document).off('keypress', handleKeyPressOffCanvas);
        $(document).off('mouseup touchend', handleMouseUpOffCanvas);

        Polaris.Dataview.reloadSelectedRows();

        $('body').removeClass('off-canvas-active');
        $(".jqmClose").off('click', Polaris.Form.hideOverlay);
    }

});

// named functions for event handlers
function handleKeyPressOffCanvas(event) {
    if (event.key == 'Escape') {
        Polaris.Form.hideOverlay();
    }
}

function handleMouseUpOffCanvas(event) {
    var offCanvas = $('.off-canvas')
    if (!offCanvas.is(event.target) && offCanvas.has(event.target).length === 0) {
        Polaris.Form.hideOverlay();
    }
}