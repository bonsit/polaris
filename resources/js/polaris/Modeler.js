if (typeof(Polaris.Modeler) == "undefined") {
    Polaris.Modeler = {};
}

Polaris.Modeler.NAME = "Polaris.Modeler";
Polaris.Modeler.VERSION = "1.0";

try {
    if (typeof(MochiKit.Base) === 'undefined') {
  //      throw "";
    }
} catch (e) {
//    throw "Polaris.Modeler depends on MochiKit.Base";
};
Base.update(Polaris.Modeler, {

    deltakeyline: 10,
    delta: 150,
    deltasubset: 35,
    deltasubsetspacing: 15,
    deltasentence: 20,
    keysize: 5,
    minplaceholderwidth: 150,
    placeholderwidth: 0,
    nameletterwidth: 10,
    placeholderheight: 50,
    subsetradius: 15,
    ftdTopSpace: 20,
    ftdNameSpace: 60,
    ftdPopulationLineHeight: 15,
    ftdLetterWidth: 7,
    nnwidth: 10,
    nnheight: 7,
    placeholderBorder: "#C0B47F",
    placeholderFill: '#EBE3BF',
    placeholderNameColor: "#4D4730",
    keyConstraintColor: "#4D4730",
    datatypeColor: '#A39558',
    subsetcolors: ['#639a91','#6ddd28','#402222','#402222'],
    surface: null,

    drawArrow: function(canvas, x, y, type) {
        if (type == 'right') {
            x = x + this.delta;
            canvas.path({fill: this.keyConstraintColor, stroke:'1px'})
            .moveTo(x,y-this.deltakeyline).relatively()
            .lineTo(-this.keysize, -this.keysize)
            .lineTo(0, this.keysize * 2)
            .lineTo(this.keysize,-this.keysize);
        } else {
            canvas.path({fill: this.keyConstraintColor, stroke:'none'})
            .moveTo(x,y-this.deltakeyline).relatively()
            .lineTo(this.keysize, this.keysize)
            .lineTo(0, -this.keysize * 2)
            .lineTo(this.keysize,-this.keysize);
        }
    },

    drawConstraint: function(canvas, constraint, x, y, text) {
        if (constraint == 'listvalue') {
            x = x + this.delta;
            listvalue = '{' + text + '}';
            canvas.text(x - this.placeholderwidth, y - 20, listvalue).attr({ "font-size": 10, "font-family": "Verdana", "fill": this.placeholderNameColor });
        }
    },

    drawPrimaryKey: function(canvas, x, y, keyindex, lastplaceholder) {
        canvas.path({stroke:this.keyConstraintColor, 'stroke-width': 1})
        .moveTo(x,y - this.deltakeyline).relatively()
        .lineTo(this.placeholderwidth,0);
        if (keyindex == 1) {
            Polaris.Modeler.drawArrow(canvas, x, y, 'left');
        }
        if ( lastplaceholder ) {
            Polaris.Modeler.drawArrow(canvas, x + this.placeholderwidth - this.minplaceholderwidth, y, 'right');
        }
    },

    drawFTD: function (canvas, ftd) {
        var self = Polaris.Modeler;
        var maxx = 500;
        var x = 0;
        var y = this.ftdTopSpace;
        var FTDName = ftd['FTDNAME'];

        /**
        * Initialize the DIV as a SVG container
        */
        var paper = Raphael($('.diagram', canvas)[0], '99%', '100');

        /**
        * Draw the FTD Name
        */
        paper.text(x, y, FTDName).attr({ "font-size": 20, "fill": "#555", "text-anchor": "left" });

        if (ftd['columns']) {
            var addextraspace = false;
            var lastPoplength = 0;
            var collength = ftd['columns'].length;
            y = y + this.ftdNameSpace;
            var subsets = new Array();
            var subset = 0; var stable = scolumn = subsettext = '';
            var keyindex = 1;
            this.placeholderwidth = this.minplaceholderwidth;
            var placeholdername = datatype = kkdatatype = '';
            for(i=0;i<collength;i++) {
        //       placeholderwidth = Math.max(placeholdername.length * nameletterwidth, minplaceholderwidth);
                switch (ftd['columns'][i]['DATATYPE']) {
                case 'C': kkdatatype = 'tekens'; break;
                case 'X': kkdatatype = 'tekst'; break;
                case 'D': kkdatatype = 'datum'; break;
                case 'T': kkdatatype = 'tijd'; break;
                case 'I':
                case 'N': kkdatatype = 'numeriek'; break;
                default:  kkdatatype = ftd['columns'][i]['DATATYPE'];
                }
                datatype = kkdatatype;
                if ( ftd['columns'][i]['DATATYPE'] == 'C' || ftd['columns'][i]['DATATYPE'] == 'I'  || ftd['columns'][i]['DATATYPE'] == 'N' ) {
                    datatype += '(' + ftd['columns'][i]['TOTALLENGTH'];
                    if (ftd['columns'][i]['DECIMALCOUNT'] > 0)
                        datatype += ',' + ftd['columns'][i]['DECIMALCOUNT'];
                    datatype += ')';
                }

                placeholdername = ftd['columns'][i]['PLACEHOLDERNAME'];
                if (placeholdername && (placeholdername.toUpperCase() == placeholdername)) {
                    placeholdername = placeholdername.toLowerCase();
                }

                this.placeholderwidth = placeholdername.length * self.ftdLetterWidth;

                var pop = ftd['columns'][i]['POPULATION'];
                if (pop) {
                    pop = pop.split('\n');
                    var longest = pop.reduce(function (a, b) { return a.length > b.length ? a : b; });

                    if (longest.length > placeholdername.length)
                        this.placeholderwidth = longest.length * self.ftdLetterWidth;
                }

                $(pop).each(function(i, elem) {
                    paper.text(x+(self.placeholderwidth/2), y + 70 + i * self.ftdPopulationLineHeight, elem).attr({"text-align": "center"});
                })

                if (this.placeholderwidth < 80) this.placeholderwidth = 80;

                var _placeholder = paper.rect(x, y, this.placeholderwidth, this.placeholderheight).attr({'fill':this.placeholderFill,'stroke':this.placeholderBorder, 'stroke-width': '1', cursor:"pointer"});
                var _placeholdertext = paper.text(x + (this.placeholderwidth / 2), y + (this.placeholderheight / 2) + 3, placeholdername).attr({"fill":this.placeholderNameColor,"text-anchor": "middle", cursor:"pointer"});
                $(_placeholdertext[0]).data('link', ftd['columns'][i]['PLACEHOLDERLINK']);
                $(_placeholder[0]).data('link', ftd['columns'][i]['PLACEHOLDERLINK']);
                _placeholdertext[0].onclick = function() {window.location = $(this).data('link') };
                _placeholder[0].onclick = function() {window.location = $(this).data('link') };

                paper.text(x + 5, y + 12, datatype).attr({'font-size':'11', 'fill':this.datatypeColor, "text-anchor": "left"});


                /* Draw notnull constraint */
                if (ftd['columns'][i]['REQUIRED'] == 'Y' && ftd['columns'][i]['KEYCOLUMN'] != 'Y') {
                    paper.rect(x + this.placeholderwidth - this.nnwidth, y + this.placeholderheight - this.nnheight, this.nnwidth, this.nnheight).attr({"stroke-width":"0",fill:"#666"});
                }

                /* Draw listvalue constraint */
                if (ftd['columns'][i]['LISTVALUES']) {
                    self.drawConstraint(paper, 'listvalue', x + this.placeholderwidth - 50, y, ftd['columns'][i]['LISTVALUES'] );
                }

                /* Draw key constraint */
                if (ftd['columns'][i]['KEYCOLUMN'] == 'Y') {
                    lastplaceholder = !ftd['columns'][i+1] || (ftd['columns'][i+1] && ftd['columns'][i+1]['KEYCOLUMN'] != 'Y');
                    self.drawPrimaryKey(paper, x, y, keyindex, lastplaceholder);
                    keyindex++;
                }
                /* Draw subsets */
                if (false && ftd['columns'][i]['SUBSETID'] != '') {
                    subset = ftd['columns'][i]['SUBSETID'];
                    if (isUndefined(subsets[subset])) subsets[subset] = 0;
                    else subsets[subset] = subsets[subset] + 1;

                    if (subsets[subset] == 0) {
                        stable = ftd['columns'][i]['SUPERTABLENAME'];
                        scolumn = ftd['columns'][i]['SUPERCOLUMNNAME'];
                        subsettext  = 'Deelverzamelingsregel ' + subset + '<br />';
                        subsettext += 'subkant: ' + ftd['FACTTYPENAME'] + '.' + placeholdername + '<br />';
                        subsettext += 'superkant: ' + stable + '.' + scolumn;
//                        subsetlink = '<a href="#link_'+stable+'" onmouseover="this.T_WIDTH=200;this.T_FONTCOLOR=\'#003399\';return escape(\''+subsettext+'\')">dvr ' + subset + '</a>';
                        paper.ellipse(x + (this.placeholderwidth / 2) - (this.subsetradius / 2), y - this.deltasubset, this.subsetradius, this.subsetradius);
                        paper.path().lineTo(x + (this.placeholderwidth / 2), y - this.deltasubset + this.subsetradius, x + (this.placeholderwidth / 2), y);
                        paper.path().lineTo(x + (this.placeholderwidth / 2) - 5, y - this.deltasubset + this.subsetradius -5 , x + (this.placeholderwidth / 2), y - this.subsetradius * 2);
                        paper.path().lineTo(x + (this.placeholderwidth / 2) + 5, y - this.deltasubset + this.subsetradius -5 , x + (this.placeholderwidth / 2), y - this.subsetradius * 2);
                        paper.text(x + (this.placeholderwidth / 2) + this.subsetradius + 0, y - this.deltasubset, this.subsetlink, {"fill":this.placeholderNameColor,"text-anchor": "middle"});
                    } else {
                        paper.path().lineTo(x + (this.placeholderwidth / 2), y - this.deltasubsetspacing, x + (this.placeholderwidth / 2), y);
                        paper.path().lineTo(x + (this.placeholderwidth / 2), y - this.deltasubsetspacing, x - this.placeholderwidth + (this.placeholderwidth / 2), y - this.deltasubsetspacing);
                    }
                }

                x = x + this.placeholderwidth;
                if (x > maxx) maxx = x;


                if (x > document.body.clientWidth*0.70) {
                    y = y + this.ftdNameSpace + 50;
                    x = 10;
                    addextraspace = true;
                }
            }
            if (pop) {
                y = y + this.ftdPopulationLineHeight * pop.length;
                lastPoplength = Math.max(lastPoplength, pop.length);
            }
            if (addextraspace) {
                log(lastPoplength);
                y = y + this.ftdPopulationLineHeight * Math.min(3, lastPoplength);
            }
        } else {
            //	  canvas.setFont("Verdana","1.0em",Font.PLAIN);
            paper.text(0, this.placeholderheight + this.deltasentence + y, 'Geen kolommen gedefinieerd.').attr({"fill":this.placeholderNameColor,"text-anchor": "middle", "onclick": "alert('a')"});
            y = y + 40;
        }

        if ( ftd['SENTENCEPATTERN'] ) {
            ZS = ftd['SENTENCEPATTERN'];
            var converter = new showdown.Converter();
            ZS = converter.makeHtml(ZS);
        } else {
            ZS = 'Geen aanwezig';
        }
        var cnt = $('.sentencepattern', canvas);
        cnt.html('<b>Zinssjabloon</b>: '+ZS).width(maxx);

        /*
        if (ftd['triggers'][0]['DESCRIPTION']) {
            cnt.append('<b>Triggers</b>:<ul>');
            for (i=0;i<ftd['triggers'].length;i++) {
                if (ftd['triggers'][i]['DESCRIPTION']) {
                    var d = '<pre>'+ftd['triggers'][i]['DESCRIPTION'].replace('   ','')+'</pre>';
                    cnt.append('<li><a href="'+_serverroot+'/app/docsys/const/triggers/?itemname='+ftd['triggers'][i]['METHODNAME']+'">'+ftd['triggers'][i]['METHODNAME'] + '</a> ('+ftd['triggers'][i]['TRIGGERTYPE'].toLowerCase()+' '+ftd['triggers'][i]['TRIGGEREVENT'].toLowerCase()+')<br />' + d + '</li>');
                }
            }
            cnt.append('</ul>');
        }

        if (ftd['ui'][0]['PLR__RECORDID']) {
            cnt.append('<b>Wordt gebruikt in Macs formulier(en)</b>:<ul>');
            for (i=0;i<ftd['ui'].length;i++) {
                if (ftd['ui'][i]['PLR__RECORDID']) {
                    cnt.append('<li><a href="'+_serverroot+'/app/docsys/const/overview/view/'+ftd['ui'][i]['PLR__RECORDID']+'/">'+ftd['ui'][i]['NAME'] + '</a></li>');
                }
            }
            cnt.append('</ul>');
        }
        */
        paper.canvas.setAttribute("height", y + this.ftdNameSpace);
    },

    /***
    *
    *  Business Process Modelling Notation (BPMN)
    *
    */

    defModelerFont: 'Lucida Grande,Arial',
    activityBoxWidth: 360,
    activityBoxHeight: 140,
    activityBoxCornerRadius: 10,

    activityBoxExpandWidth: 820,
    activityBoxExpandHeight: 395,
    activityBoxExpandCornerRadius: 10,

    activitySubBoxWidth: 50,
    activitySubBoxHeight: 30,
    activitySubBoxCornerRadius: 5,

    documentWidth: 100,
    documentHeight: 130,

    paramBoxWidth: 220,
    paramBoxHeight: 25,
    arrowWidth: 6,
    OutIndex: 0,
    InIndex: 0,

    border1Color: "#73AFB9",
    borderColor: "#AED4DB",
    textColor: "#39737E",

    _drawBaseDocument: function(paper, x, y, w, h, name, options) {
        var self = Polaris.Modeler;
        var options = options || {};
        var documentCorner = h * 0.15;
        if (documentCorner < 10) documentCorner = 10;
        var p = paper.path({stroke: self.borderColor,fill:"#fff", 'cursor':'pointer'}).absolutely()
        .moveTo(x, y).relatively()
        .lineTo(w - documentCorner,0)
        .lineTo(documentCorner,documentCorner)
        .lineTo(-documentCorner,0)
        .lineTo(0,-documentCorner)
        .lineTo(documentCorner,documentCorner)
        .lineTo(0,h-documentCorner)
        .lineTo(-w,0)
        .lineTo(0,-h)
        .andClose();
        if (options.onclick)
            p[0].onclick = options.onclick
        var wordlength = 8;
        for (i=0; i*wordlength <= name.length; i++) {
            var t = paper.text(x + 5, y + 3 + h/2 - (Math.floor((name.length-2)/wordlength)) - 1 * 10 + i * 10, name.substr(i * wordlength, wordlength)).attr({fontSize:'8px',fill:self.textColor,fontFamily:self.defModelerFont,'text-anchor':'left', 'cursor':'pointer'});
            if (options.onclick)
                t[0].onclick = options.onclick
        }
        return p;
    },

    _drawBaseParameter: function(paper, x, y, w, h, type, name) {
        var self = Polaris.Modeler;
        var corner = h/2;
        var p = paper.path({stroke: self.borderColor,fill:"#FCFEFF"});
        if (type == "OUT" || type == "IN/OUT") {
            p.moveTo(x-h/2, y).relatively()
            .lineTo(w,0)
            .lineTo(0,h)
            .lineTo(-w,0)
            .lineTo(corner,-corner)
            .lineTo(-corner,-corner)
            .andClose();
        } else if (type == "IN") {
            p.moveTo(x, y).relatively()
            .lineTo(w-corner,0)
            .lineTo(corner,corner)
            .lineTo(-corner,corner)
            .lineTo(-w+corner,0)
            .lineTo(0,-h)
            .andClose();
        }
        var t = paper.text(x + 10, y + 3 + h/2, name).attr({fontSize:'10px',fill:self.textColor,fontFamily:self.defModelerFont,'text-anchor':'left'});
    },

    _drawDocument: function(paper, x, y, name, scale, options) {
        var self = Polaris.Modeler;

        var scale = scale || 1;
        var w = self.documentWidth * scale;
        var h = self.documentHeight * scale;
        return self._drawBaseDocument(paper, x, y, w, h, name, options);
    },

    drawActivityParam: function(paper, x, y, type, name, options, totalcount) {
        var self = Polaris.Modeler;
        var options = options || {};

        var boxWidth = (options.expanded)?self.activityBoxExpandWidth:self.activityBoxWidth;
        var boxHeight = (options.expanded)?self.activityBoxExpandHeight:self.activityBoxHeight;

        if (type == 'OUT' || type == 'IN/OUT') {
            var index = self.OutIndex;
            var startX = x + boxWidth;
            var arrowX = startX-self.paramBoxOffset;
        } else if (type == 'IN') {
            var index = self.InIndex;
            var startX = x - self.paramBoxWidth + self.paramBoxHeight/2;
            var arrowX = startX+self.paramBoxWidth;
        }

        /* Draw parameter */
        var offsetTop = y + 15 + index * (self.paramBoxHeight + 5);

        self._drawBaseParameter(paper, startX, offsetTop, self.paramBoxWidth, self.paramBoxHeight, type, name);

        if (type == 'OUT' || type == 'IN/OUT') {
            self.OutIndex++;
        } else if (type == 'IN') {
            self.InIndex++;
        }
    },

    _drawSubTable: function(paper, x, y, index, tablename) {
        var self = Polaris.Modeler;
//        var t = paper.text(x + 10, y + 40 + (20 * index), tablename).attr({fill:"#73AFB9",fontFamily:self.defModelerFont,fontSize:'10px',fontWeight:'bold', "text-anchor":"left"});
        var c = paper.rect(x + 10, y + 40 + (20 * index), self.activitySubBoxWidth, self.activitySubBoxHeight, self.activitySubBoxCornerRadius).attr({fill: "#FFF",stroke:self.borderColor});
    },

    _drawBaseEvent: function(paper, x, y, r, type, direction, fase) {
        var self = Polaris.Modeler;
        paper.circle(x+r/2, y - r/4, r).attr({stroke:self.borderColor, fill:'#fff'});
        var direction = direction || 'CATCH';
        var fase = fase || 'START';

        switch(type) {
        case 'SIGNAL':
            paper.path({stroke: self.borderColor})
            .moveTo(x+r/2, y-r/1.2).relatively()
            .lineTo(r/2,r/1.2)
            .lineTo(-r,0)
            .lineTo(r/2,-r/1.2);
        break;
        case 'TIMER':
            paper.circle(x+r/2, y - r/4, r).attr({stroke:self.borderColor, fill:'#fff'}).scale("0.8");
        break;
        }
    },

    drawSubDocument: function(paper, x, y, name, count, options) {
        var self = Polaris.Modeler;
        var delta = 6;
        var scale = 0.5
//        if (count > 2) self._drawDocument(paper, x + (delta*3), y + (delta*3), '', scale);
//        if (count > 1) self._drawDocument(paper, x + (delta*2), y + (delta*2), '', scale);
        if (count > 0) var doc = self._drawDocument(paper, x + (delta*1), y + (delta*1), name, scale, options);
        if (count > 0) paper.path({'stroke-width':'2',stroke:self.borderColor}).moveTo(x+(self.documentWidth/2)*scale,y+(self.documentHeight*scale)+5).relatively().lineTo(0,30).andClose();
        return doc;
    },

    drawUsedBy: function(paper, x, y, type, name) {
        var self = Polaris.Modeler;
        switch(type) {
        case 'TRIGGER':
            var r = 11;
            self._drawBaseEvent(paper, x + 2 + r/2, y, r, 'SIGNAL', 'START');
        break;
        case 'PROCEDURE':
        case 'PACKAGE_BODY':
            var c = paper.rect(x+1, y-12, 25, 18, self.activityBoxCornerRadius/3).attr({fill: "#F1FBFD",stroke:self.borderColor});
        break;
        }
        paper.text(x + 35, y, name).attr({"text-anchor":"left", fontSize:"12px", fill:self.textColor});
    },

    drawSubPackage: function(paper, x, y, count) {
        var self = Polaris.Modeler;
        var delta = 6;
//        if (count > 0) self._drawPackage(paper, x + (delta*1), y + (delta*1), '', 0.4);
        if (count > 1) self._drawDocument(paper, x + (delta*2), y + (delta*2), '', 0.4);
        if (count > 2) self._drawDocument(paper, x + (delta*3), y + (delta*3), '', 0.4);
    },

    _drawBaseActivity: function(paper, x, y, w, h, type, name, collapsed, options) {
        var self = Polaris.Modeler;
        var options = options || {};

        var realwidth = w;
        if (options.autoSize) {
            realwidth = name.length*16;
        }

        self.OutIndex = self.InIndex = 0;
        if (type == 'packproc') type = 'Package procedure';
        else if (type == 'packfunc') type = 'Package function';
        else if (type == 'storedproc') type = 'Stored procedure';
        var c = paper.rect(x, y, realwidth, h, self.activityBoxCornerRadius).attr({fill: "#F1FBFD",stroke:self.borderColor});
        var t1 = paper.text(x + 20, y + 20, type).attr({fill:"#73AFB9",fontFamily:self.defModelerFont,fontSize:'12px',fontWeight:'bold', "text-anchor":"left"});
        var t2 = paper.text(x + (realwidth / 2), y + h/2, name).attr({fill:self.textColor,fontFamily:self.defModelerFont});

        /* Draw Collapsed 'Plus' */
        if (!options.expanded && collapsed) {
            var plusBoxSize = 20;
            var pl = paper.rect(x + w/2 - plusBoxSize/2, y + h - plusBoxSize - 10, plusBoxSize, plusBoxSize).attr({fill:"#fff",stroke:"#AED4DB", cursor:"pointer"});
            var pltext = paper.text(x + w/2, y + h - 15, '+').attr({fill:"#AED4DB", fontSize:"15px", cursor:"pointer"});
            pltext[0].onclick = pl[0].onclick = function() {
                window.location = window.location + '&expand=true';
            }
        }

        if (options.onclick) {
            c[0].onclick = t1[0].onclick = t2[0].onclick = options.onclick;
        }

        if (options.scale) {
            c.scale(options.scale);
            t1.scale(options.scale).attr({fontSize:'16px'});
            t2.scale(options.scale).attr({fontSize:'20px'});
        }
        return [c,realwidth+10];
    },

    drawSubActivity: function(paper, x, y, type, name, collapsed, options) {
        var self = Polaris.Modeler;
        var options = options || {};
        options.autoSize = true;

        var sa = self._drawBaseActivity(paper, x, y, self.activityBoxWidth, self.activityBoxHeight, type, name, collapsed, options);
        sa[0].attr({fill:'#F8FEFF', cursor:"pointer"});
        return sa;
    },

    drawActivity: function(paper, x, y, type, name, collapsed, options, paramcount) {
        var self = Polaris.Modeler;
        var options = options || {};

        var boxWidth = (options.expanded == true)?self.activityBoxExpandWidth:self.activityBoxWidth;
        var boxHeight = (options.expanded == true)?self.activityBoxExpandHeight:Math.max(self.activityBoxHeight, paramcount * (self.paramBoxHeight + 8));

        return self._drawBaseActivity(paper, x, y, boxWidth, boxHeight, type, name, collapsed, options);
    },

    drawEvent: function(paper, x, y, r, type, options) {
        var self = Polaris.Modeler;
        var options = options || {};
        var r = r || 50;

        self._drawBaseEvent(paper, x, y, r, type);
    }
});
