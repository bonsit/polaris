/***
*
*    All the timer functions of Polaris
*
**/
Polaris.Timer = {};
Base.update(Polaris.Timer, {

    /* is de form countdown gepauseerd ivm invullen van form */
    countdownPaused: false,

    /** @id Polaris.Timer.pauseCountdown */
    pauseCountdown: function() {
        Polaris.Timer.countdownPaused = true;
        // >>  uitbreiden met js timers <<
    },

    /** @id Polaris.Timer.refreshCountDown */
    refreshCountDown: function(timer) {
        timer--;
        var ri = $('#refresh_interval');
        if (Polaris.Timer.countdownPaused) {
            if (ri.length) {
                if (ri.options[ri.selectedIndex].text != 'paused') {
                    var elOptNew = document.createElement('option');
                    elOptNew.text = 'paused';
                    elOptNew.value = '';
                    try {
                        ri.add(elOptNew, null); // standards compliant; doesn't work in IE
                    }
                    catch(ex) {
                        ri.add(elOptNew); // IE only
                    }
                    ri.selectedIndex = ri.options.length - 1;
                }
            }
        } else {
            if (timer <= 0) {
                window.location.reload();
            } else {
                if (ri.length) { ri.options[ri.selectedIndex].text = timer + ' sec'; }
                setTimeout("Polaris.Timer.refreshCountDown("+timer+")", 1000);
            }
        }
    }

});