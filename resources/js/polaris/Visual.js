/***
 *
 *    Polaris Visual functions
 *
 **/
Polaris.Visual = {};
Base.update(Polaris.Visual, {
  initializeOptionKey: function () {
    /***
     * Is the Option key toggled?
     */
    $.fn.toggleText = function (toggled) {
      return this.each(function () {
        var toggledtext = "";
        if (toggled) toggledtext = $(this).attr("toggled");
        else toggledtext = $(this).attr("oritext");
        $("span", this).text(toggledtext);
      });
    };

    $(window)
      .keydown(function (e) {
        if (e.keyCode == 16) $(window).data("keypressed_shift", true);
        if (e.keyCode == 17) $(window).data("keypressed_ctrl", true);
        if (e.keyCode == 18) $(window).data("keypressed_alt", true);
        if ($(window).data("keypressed_ctrl") == true) {
          /* toggle the text of elements (.toggletext) when the Ctrl is pressed */
          $(".toggletext").toggleText(true);
        }
      })
      .keyup(function (e) {
        if (e.keyCode == 16) $(window).data("keypressed_shift", false);
        if (e.keyCode == 17) $(window).data("keypressed_ctrl", false);
        if (e.keyCode == 18) $(window).data("keypressed_alt", false);

        if (e.keyCode == 17) {
          /* toggle the text back of elements (.toggletext) when the Ctrl is released */
          $(".toggletext").toggleText(false);
        }
      });
  },
  initializeCopyToClipboard: function() {
    $(".copy-clipboard").on('click', function() {
      var range, selection;

      if (window.getSelection && document.createRange) {
          selection = window.getSelection();
          range = document.createRange();
          range.selectNodeContents(this);
          selection.removeAllRanges();
          selection.addRange(range);
      } else if (document.selection && document.body.createTextRange) {
          range = document.body.createTextRange();
          range.moveToElementText(this);
          range.select();
      }
    });
  },
  initializeAccessKeys: function () {
    Polaris.Visual.initializeOptionKey();

    $(document).on("keydown", function (e) {
      if (
        e.altKey &&
        e.ctrlKey &&
        $.charcode(e) in { e: 1, r: 1, a: 1, n: 1 }
      ) {
        // set / object literal
        var elem = $(e.target);
        if ($.charcode(e) in { e: 1, r: 1 }) {
          // set / object literal
          // ingeval van een Edit (e) of Clone (r) dan kijken naar de huidig gefocuste rij
          var rij = Polaris.Dataview.focusedRow();
          elem = $("a[accesskey=" + $.charcode(e) + "]", rij);
        } else {
          elem = $("#masteractions a[accesskey=" + $.charcode(e) + "]");
        }

        if (
          elem.length > 0 &&
          elem.is(":visible") &&
          !elem.hasClass("disabled")
        ) {
          if (
            elem[0].tagName == "A" &&
            window.location.href != elem.prop("href")
          ) {
            // works only in WebKit
            var e = document.createEvent("MouseEvents");
            e.initEvent("click", true, true);
            elem[0].dispatchEvent(e);

            // this works in IE
            //                        window.location = elem.prop('href');
          }
        }
      }
    });

    $(":button[accesskey],a[accesskey],:input[accesskey]").each(function () {
      var elem = $(this);
      var ak = $(this).attr("accesskey");
      var specKey = ak.split("+")[0].toUpperCase();
      if (ak[0] == "F" || specKey == "CTRL" || specKey == "SHIFT") {
        if ($(this).parents(".buttons").length == 0) {
          /* was eerst > 0 */
          var acckey = ""; //' <em>' + ak + '</em>';
        } else {
          var acckey = " <span>(" + ak + ")</span>";
        }
        $(this).html($(this).html() + acckey);
        var ctrlSwitch = false;
        if (specKey == "CTRL") {
          ctrlSwitch = true;
          ak = ak.split("+")[1].toLowerCase();
        } else if (specKey == "SHIFT") {
          var shiftSwitch = true;
          ak = ak.split("+")[1].toLowerCase();
        } else {
          ak = ak.toLowerCase();
        }
        $(window).keydown(function (e) {
          if (
            e.ctrlKey == ctrlSwitch &&
            $.charcode(e) == ak &&
            elem.is(":visible") &&
            !elem.hasClass("disabled")
          ) {
            var clickevents =
              $.data(elem.get(0), "events") != null
                ? $.data(elem.get(0), "events").click
                : null;
            if ((clickevents = null && elem[0].tagName == "A")) {
              window.location = elem[0].href;
            } else {
              var e = document.createEvent("MouseEvents");
              if (elem[0].tagName == "INPUT") {
                e.initEvent("focus", true, true);
                elem.focus();
              } else {
                e.initEvent("click", true, true);
              }
              elem[0].dispatchEvent(e); // op de DOM element, niet jQuery element
            }
          }
          if (
            e.shiftKey == shiftSwitch &&
            $.charcode(e) == ak &&
            elem.is(":visible") &&
            !elem.hasClass("disabled")
          ) {
            var e = document.createEvent("MouseEvents");
            e.initEvent("click", true, true);
            elem[0].dispatchEvent(e);
          }
        });
      }
    });
  },
  showTab: function (container, tabid) {
    var tabs = $("a", container);

    var idList = []; //save possible elements
    tabs.each(function () {
      if (this.href.match(/#/)) {
        idList.push("#" + this.href.split("#")[1]);
      }
    });
    $(idList.join(",")).hide();

    tabs
      .removeClass("selected")
      .filter("[href='" + tabid + "']", container)
      .addClass("selected");
    var page = $(tabid);

    page.fadeIn();
  },
  activeTab: function (container) {
    return $("a.selected", container);
  },
  initializeTabs: function (container) {
    var tabEl = $('.nav-tabs [data-toggle="tab"]', container);
    tabEl.on("shown.bs.tab", function (event) {
      let pageid = event.target.id.replace("tab_", "");
      var page = $("#" + pageid);
      if (page.find("iframe").length > 0) {
        var frame = page.find("iframe");
        if (typeof frame.attr("src") == "undefined") {
          frame.on("load", function () {
            $("#evengeduld").remove();
          });
          frame.attr("src", frame.attr("do_src"));
        }
      }
    });
  },
  initializeMagnifier: function () {
    $('img[data-toggle="magnifier"]').each(function() {
        let img = $(this);
        img.blowup({width:img.data('magnifier_size'),height:img.data('magnifier_size'),scale:img.data('magnifier_scale')});
    });
  },
  initializePopovers: function () {
    $(document.body).on('click', '[data-toggle=dropdown]', function(){
        var dropmenu = $(this).next('.dropdown-menu');

        dropmenu.css({
            visibility: "hidden",
            display: "block"
        });

        // Necessary to remove class each time so we don't unwantedly use dropup's offset top
        dropmenu.parent().removeClass("dropup");

        // Determine whether bottom of menu will be below window at current scroll position
        if (dropmenu.offset().top + dropmenu.outerHeight() > $(window).innerHeight() + $(window).scrollTop()){
            dropmenu.parent().addClass("dropup");
        }

        // Return dropdown menu to fully hidden state
        dropmenu.removeAttr("style");
    });
  },
  storeMenuState: function (group) {
    // Store the group.data('groupid') in the session of the browser
    // so we can remember the state of the menu
    if (typeof Storage !== "undefined") {
      var menustates = JSON.parse(localStorage.getItem("menustates")) || {};
      menustates[group.data("groupid")] = group.hasClass("closed")
        ? "closed"
        : "open";
      localStorage.setItem("menustates", JSON.stringify(menustates));
    }
  },
  makeMenus: function () {
    /***
     * Prevent users from switching to other forms without saving the current record
     **/
    var menuClickProcessor = function (e) {
      // .normalmenu?
      e.preventDefault();
      if (Polaris.Form.userIsEditing) {
        Polaris.Base.modalDialog($.t("plr.changes_not_saved"), {
          yes: function () {
            $("#dataform").trigger("submit");
          },
          no: function () {
            if (
              typeof e.target != "undefined" &&
              typeof e.target.href != "undefined"
            ) {
              e.preventDefault();
              window.location =
                e.target.href +
                "?" +
                Polaris.Base.addQueryParameter(
                  "_hdnPlrSes",
                  sessionStorage.plrses,
                  e.target.href,
                );
            }
          },
          cancel: function () {},
        });
      } else {
        var link = $("A", e.currentTarget);
        if (
          link.length > 0 &&
          link.prop("href") != "" &&
          link.prop("href") != "javascript:void(0)"
        ) {
          var sep = "?";
          if (
            typeof link.attr("href") != "undefined" &&
            link.attr("href").indexOf("?") > 0
          ) {
            sep = "&";
          } else {
          }
          window.location =
            link.prop("href") +
            sep +
            Polaris.Base.addQueryParameter(
              "_hdnPlrSes",
              sessionStorage.plrses,
              link.prop("href"),
            );
        }
      }
    };

    $(".groupname").on("click", function (e) {
      e.preventDefault();

      let toggler = $(".toggler", this);
      let group = toggler.parents(".menugroup");
      let menuitems = group.find(".menuitem,.separator");
      menuitems.toggle();
      group.toggleClass("closed", menuitems.is(":hidden"));

      Polaris.Visual.storeMenuState(group);
    });

    // $(".menugroup.closed .menuitem").hide();

    if (jQuery().dropdown) $("#usermenu").dropdown();
  },

  spDuration: 150,
  spWidth: 260,

  /** @id Polaris.Visual.initSearchPanel */
  initSearchPanel: function (searchpanel) {
    $(".kill-filter").on("click", function () {
      let form = $(this).parents("form");
      $(this).parent(".searchtag").remove();
      log(form.serializeArray());
    });
    $(".kill-all-filters").on("click", function () {
      $(this).parent().parent().find("input").remove();
    });

    $("#advancedSearch").on("click", function () {
      Polaris.Base.showAdvancedSearchModal();
    });
    $("#modSaveSearch").on("shown.bs.modal", function () {
      $(this).find("input:visible:first").focus();
    });
    var saveSearchAction = function (e) {
      if (e.target.tagName == "FORM") {
        var form = e.target;
      } else {
        var form = e.target.form;
      }

      var senddata = $(form).serialize();
      Polaris.Ajax.postJSON(
        _ajaxquery,
        senddata,
        null,
        "Zoekactie opgeslagen",
        function (data, textStatus) {
          if (data.result) {
            $("#modSaveSearch").modal("hide");
          } else {
            $("#modSaveSearchError").text(
              "Deze naam bestaat al. Probeer een andere...",
            );
          }
        },
      );
    };
    $("#frmSaveSearch").on("submit", function (e) {
      e.preventDefault();
      saveSearchAction(e);
    });
    $("#btnSaveSearchSave").on("click", saveSearchAction);

    var $searchpanel = $(searchpanel);

    if (
      !$("body").hasClass("REST") &&
      Polaris.Base.getLocationVariable("maximize") != "true" &&
      $searchpanel.length > 0
    ) {
      var Self = Polaris.Visual;

      // searchdocker toggle switch
      var switches = document.querySelectorAll(
        'label.switch input[type="checkbox"]',
      );
      for (var i = 0, sw; (sw = switches[i++]); ) {
        var div = document.createElement("div");
        div.className = "switch";
        sw.parentNode.insertBefore(div, sw.nextSibling);
      }

      $(window).bind("resize", function () {
        /***
         * Resize the searchpanel, pinnedrecords and maincontent to fit the browser height
         **/
        //$("#searchpanel").css('height', ($(window).height() - $("#searchpanel").offset().top ) + 'px');
      });

      if ($.cookies) {
        $.cookies.setOptions({ hoursToLive: 3000 });
      }

      var dockPanel = function (switchon) {
        if (switchon) {
          $searchpanel.data("docked", true);
          //$("#searchpaneldocker span").text('Undock');
          $.cookies.set("searchpanel", "on");
          $(".switch-link i")
            .removeClass("glyphicons-chevron-right")
            .addClass("glyphicons-chevron-left");
        } else {
          $searchpanel.data("docked", false);
          //$("#searchpaneldocker span").text('Dock');
          $.cookies.set("searchpanel", "off");
          $(".switch-link i")
            .removeClass("glyphicons-chevron-left")
            .addClass("glyphicons-chevron-right");
        }
        $searchpanel.add("#maincontent").toggleClass("docked", switchon);
      };

      if (
        $.cookies &&
        ($.cookies.get("searchpanel") == "on" ||
          $.cookies.get("searchpanel") == null) &&
        !$searchpanel.hasClass("switchedoff")
      ) {
        $searchpanel.data("docked", true);
        //                $.cookies.set('searchpanel','on');
        $searchpanel.add("#maincontent").toggleClass("docked", true);
        $(".switch-link i")
          .removeClass("glyphicons-chevron-right")
          .addClass("glyphicons-chevron-left");
      } else {
        $searchpanel.data("docked", false);
        //                $.cookies.set('searchpanel','off');
        $searchpanel.add("#maincontent").toggleClass("docked", false);
        $(".switch-link i")
          .removeClass("glyphicons-chevron-left")
          .addClass("glyphicons-chevron-right");
      }

      $(".switch-link").click(function () {
        dockPanel(!searchpanel.data("docked"));
      });
      // if ($.fn.hoverIntent) {
      //     $searchpanel.hoverIntent({
      //         sensitivity: 7, // number = sensitivity threshold (must be 1 or higher)
      //         interval: 300,   // number = milliseconds of polling interval
      //         over: function() { Polaris.Visual.toggleSearchPanel(true) },  // function = onMouseOver callback (required)
      //         timeout: 400,   // number = milliseconds delay before onMouseOut function call
      //         out: function() { Polaris.Visual.toggleSearchPanel(false) }    // function = onMouseOut callback (required)
      //     });
      // };

      $("#action_mastersearch").click(function (e) {
        e.preventDefault();
        Polaris.Visual.toggleSearchPanel(true);
      });

      $(".searchallcolumns_column").change(function () {
        $completeRow = $(this).parents(".row");
        if (!$(this).data("done")) {
          log($completeRow.clone(true));
          $completeRow.after($completeRow.clone(true));
          $(this).data("done", true);
        }
        $(".searchallcolumns_value", $completeRow).focus();
      });

      $("#_fldNEWSEARCH").click(function (e) {
        e.preventDefault();
        // empty the search fields
        $("#search_dataform :input[type=search]")
          .val("")
          .filter(":first")
          .focus();

        // delete pins
        Polaris.Ajax.deletePins($("#dataform input[name=_hdnDatabase]").val());
      });

      // Store search options in local JS session
      $("#_fldCASESENSITIVE").click(function (e) {
        sessionStorage["casesensitive_" + formid] = $(this).prop("checked")
          ? "true"
          : "false";
      });
      $("#_fldSEARCHFULLWORD").click(function (e) {
        sessionStorage["searchfullword_" + formid] = $(this).prop("checked")
          ? "true"
          : "false";
      });
      $("#_fldSEARCHFROMSTART").click(function (e) {
        sessionStorage["searchfromstart_" + formid] = $(this).prop("checked")
          ? "true"
          : "false";
      });

      // Give search options initial settings
      // EVEN NIET, totdat ORA11 actief is: $("#_fldCASESENSITIVE").prop('checked', sessionStorage["casesensitive_"+formid] || true);
      sessionStorage["casesensitive_" + formid] =
        typeof sessionStorage["casesensitive_" + formid] == "undefined"
          ? "true"
          : sessionStorage["casesensitive_" + formid] == "true";
      sessionStorage["searchfullword_" + formid] =
        typeof sessionStorage["searchfullword_" + formid] == "undefined"
          ? "true"
          : sessionStorage["searchfullword_" + formid] == "true";
      sessionStorage["searchfromstart_" + formid] =
        typeof sessionStorage["searchfromstart_" + formid] == "undefined"
          ? "false"
          : sessionStorage["searchfromstart_" + formid] == "true";
      $("#_fldCASESENSITIVE").attr(
        "checked",
        sessionStorage["casesensitive_" + formid] == "true",
      );
      $("#_fldSEARCHFULLWORD").attr(
        "checked",
        sessionStorage["searchfullword_" + formid] == "true",
      );
      $("#_fldSEARCHFROMSTART").attr(
        "checked",
        sessionStorage["searchfromstart_" + formid] == "true",
      );
    }
  },
  /** @id Polaris.Visual.toggleSearchPanel */
  toggleSearchPanel: function (switchon) {
    var Self = Polaris.Visual;
    var searchpanel = $("#searchpanel");

    if (searchpanel.data("docked") == true) return;
    if (typeof switchon == "undefined")
      switchon = searchpanel.css("width") == "0px";

    if (switchon) {
      searchpanel.animate(
        { width: Self.spWidth + "px" },
        Self.spDuration,
        null,
        function () {
          $("#searchpanelinner").fadeIn();
        },
      );
    } else {
      $("#searchpanelinner").hide();
      searchpanel.animate({ width: "0px" }, Self.spDuration);
    }
  },
  /** @id Polaris.Visual.initPanelHelpText */
  initPanelHelpText: function () {
    $(".collapse-link").on("click", function () {
      var alertbox = $(this).closest("div.alert");
      var button = $(this).find("i");
      var content = alertbox.find("div.alert-content");
      content.slideToggle(200);
      button.toggleClass("fa-chevron-up").toggleClass("fa-chevron-down");
      alertbox.toggleClass("").toggleClass("border-bottom");
      setTimeout(function () {
        alertbox.trigger("resize");
        alertbox.find("[id^=map-]").trigger("resize");
      }, 50);
    });
  },
  /** @id Polaris.Visual.initNavBar */
  initNavBar: function (navbar) {
      const $sb = $("#sidebar");

      const dockNavBar = function (switchon) {
          if (switchon) {
              $("body").addClass("mini-navbar");
              navbar.data("docked", true);
              $.cookies.set("navbar", "on");
              $sb.removeClass('menuEnter').addClass('menuLeave');
              $("#wrapper").css({'padding-left': '30px'});
          } else {
              $("body").removeClass("mini-navbar");
              navbar.data("docked", false);
              $.cookies.set("navbar", "off");
              $sb.removeClass('menuEnter menuLeave');
              $("#wrapper").css({'padding-left': '0px'});
          }
          // SmoothlyMenu();
      };

      $sb.on('mouseenter mouseleave', function(e) {
          $sb.toggleClass('menuEnter', e.type === 'mouseenter').toggleClass('menuLeave', e.type === 'mouseleave');
      });

      document.addEventListener('mousemove', function (event) {
          sessionStorage.setItem('mouseX', event.clientX);
      });

      const isNavbarDocked = $.cookies && $.cookies.get("navbar") === "on";
      navbar.data("docked", isNavbarDocked);

      if (typeof SmoothlyMenu !== "undefined") {
          // Minimalize menu
          $(".navbar-minimalize").on("click", function () {
              dockNavBar(!navbar.data("docked"));
          });
          dockNavBar(navbar.data("docked"));
      }

      const $constructs = $("#constructs");
      if ($constructs.length > 0) {
          $(window).on("resize", function () {
              const overflow = constructs.scrollHeight - constructs.clientHeight > 0;
              $(".application-group").toggleClass("nofit", overflow);
          });
      }

      $constructs.on("click", function () {
          sessionStorage.setItem("tempScrollTop", $(this).scrollTop());
      });
  },
  /** @id Polaris.Visual.breadcrumFeedback */
  breadcrumFeedback: function (text) {
    var li = $("<li>");
    li.text(text);
    li.addClass("last feedback");
    $(".breadcrumb li:last")
      .removeClass("last")
      .addClass("prelast feedback")
      .prev()
      .removeClass("prelast");
    $(".breadcrumb").append(li);
    Polaris.Visual.fadeBreadcrum(li);
  },
  /** @id Polaris.Visual.fadeBreadcrum */
  fadeBreadcrum: function (elem) {
    if (typeof elem == "string") {
      elem = $("#" + elem);
    }
    setTimeout(function () {
      if (elem.prev().length) {
        //                elem.removeClass('prelast');
        elem.prev().addClass("last").removeClass("prelast feedback");
        if (elem.prev().prev().length) {
          elem.prev().prev().addClass("prelast");
        }
      }
      elem.css({ "font-weight": "bold" }).fadeOut("slow", function () {
        $(this).remove();
      });
    }, 2000);
  },

  /** @id Polaris.Visual.yellowFade */
  yellowFade: function (elem, callback) {
    var easeInOut = function (
      minValue,
      maxValue,
      totalSteps,
      actualStep,
      powr,
    ) {
      var delta = maxValue - minValue;
      var stepp =
        minValue + Math.pow((1 / totalSteps) * actualStep, powr) * delta;
      return Math.ceil(stepp);
    };

    var doBGFade = function (
      elem,
      startRGB,
      endRGB,
      finalColor,
      steps,
      intervals,
      powr,
    ) {
      if (elem.bgFadeInt) {
        window.clearInterval(elem.bgFadeInt);
      }
      var actStep = 0;
      elem.bgFadeInt = window.setInterval(function () {
        elem.style.backgroundColor =
          "rgb(" +
          easeInOut(startRGB[0], endRGB[0], steps, actStep, powr) +
          "," +
          easeInOut(startRGB[1], endRGB[1], steps, actStep, powr) +
          "," +
          easeInOut(startRGB[2], endRGB[2], steps, actStep, powr) +
          ")";
        actStep++;
        if (actStep > steps) {
          elem.style.backgroundColor = finalColor;
          window.clearInterval(elem.bgFadeInt);
          if (typeof callback == "function") {
            callback.call();
          }
        }
      }, intervals);
    };

    var finalcol = $(elem).hasClass("odd")
      ? $(elem).css("background-color")
      : "transparent";
    var endCol = $(elem).hasClass("odd") ? [224, 224, 224] : [243, 245, 245];
    doBGFade(elem, [183, 246, 220], endCol, finalcol, 100, 20, 7);
  },
  saveTaskViewItem: function (data) {
    var Self = Polaris.Visual;

    var senddata = data;
    log(senddata);

    // senddata.push({name: '_hdnDatabase', value: data['_hdnDatabase']});
    // senddata.push({name: '_hdnProcessedByModule', value: 'true'});
    // senddata.push({name: 'BRIEFKOPPELING', value: $("#_fldNIEUWEBRIEFKOPPELING").val()});
    // senddata.push({name: 'BRIEFCODE', value: $("#_fldBRIEFCODE").val()});

    var message = false;
    var feedback_ok = false;
    Polaris.Ajax.postJSON(_ajaxquery, senddata, message, feedback_ok, false);
    return true;
  },
  initializeTaskView: function () {
    $(".taskview").each(function (index, elem) {
      const sortable = Sortable.create(elem, {
        sort: true, // sorting inside list
        group: "somegroup",
        onEnd: function (event) {
          const tovalue = $(event.to).data("statusvalue");
          const fromvalue = $(event.from).data("statusvalue");
          if (tovalue == fromvalue) return;
          const statusfield = $(event.item).parents("ul").data("statusfield");
          const recordid = $(event.item).attr("id");
          log("onend " + tovalue + "  " + fromvalue);
          Polaris.Dataview.hotAjaxUpdate(
            "frmhotupdate",
            statusfield,
            recordid,
            tovalue,
            true,
          );
        },
        store: {
          set: function (sortable) {
            const createArrayWithIntegers = (n) =>
              Array.from({ length: n }, (_, i) => (i + 1) * 10);
            const recordArray = sortable.toArray();
            const recordids = recordArray.join(";").replaceAll("row@", "");
            const orderArray = createArrayWithIntegers(recordArray.length);
            const ordervalues = orderArray.join(";");
            log("set " + recordids);
            Polaris.Dataview.hotAjaxUpdate(
              "frmhotupdate",
              $(sortable.el).data("sortfield"),
              recordids,
              ordervalues,
              true,
            );
          },
        },
        animation: 150, // ms, animation speed moving items when sorting, `0` — without animation
        dataIdAttr: "id", // HTML attribute that is used by the `toArray()` method
        ghostClass: "sortable-ghost", // Class name for the drop placeholder
      });
    });

    $(".taskview-item-more").on("click", function (e) {
      e.stopPropagation();
    });

    $(".taskview-add,.taskview-add-item").on("click", function (e) {
      e.preventDefault();

      let basicformurl = _callerquery.replace("/const/", "/basicform/");

      Polaris.Form.showOverlay(basicformurl + "insert/");

      // Polaris.Window.showModuleForm(
      //   basicformurl + "insert/",
      //   Polaris.Visual.saveTaskViewItem,
      //   /* on Show   */ function () {
      //     $("#_fldSTATUS").val(
      //       $(e.target).parents(".taskview-pane").data("status"),
      //     );
      //   },
      // );
    });
    $(".taskview-item").on("click", function (e) {
      e.preventDefault();

      li = $(e.target);

      if (!li.hasClass("taskview-item"))
        li = $(e.target).parents("li.taskview-item");
      let basicformurl = _callerquery.replace("/const/", "/basicform/");

      Polaris.Form.showOverlay(basicformurl + "edit/" + li.attr("id") + "/");

      // Polaris.Window.showModuleForm(
      //   basicformurl + "edit/" + li.attr("id") + "/",
      //   Polaris.Visual.saveTaskViewItem,
      //   /* on Show   */ function () {},
      // );
    });
  },
});
