/***
*
*    The Polaris Onboarding functions
*
**/
Polaris.Onboarding = {};
Base.update(Polaris.Onboarding, {
    tourInstance: null,
    steps: [],
    initializeIntrojs: function() {
        this.tourInstance = introJs().setOptions({
            steps: [{
              title: 'Welkom 👋',
              intro: 'Welkom bij Maxia, het facilitair systeem. Ik neem je doormiddel van deze onboarding mee door het systeem zodat je zelf snel aan de slag kunt. Geen zorgen, later heb je altijd de mogelijkheid om bepaalde dingen terug te zoeken als iets niet duidelijk is. Veel succes!'
            },
            {
              element: document.querySelector('#sidebar'),
              intro: 'Hier links zie je de taakbalk staan van Maxia. Vanuit hier kun je navigeren naar alle mogelijke functionaliteiten die horen bij facility beheer. Laten we er samen doorheen lopen!'
            },
            {
                element: document.querySelector('#constructs li:nth-child(2)'),
                intro: 'We beginnen met Direct regelen. Vanuit hier kom je terecht op een overzichtspagina waar vandaan je de belangrijkste functionaliteiten kunt gebruiken die Maxia te bieden heeft.'
            },
            {
                element: document.querySelector('#qa_8'),
                intro: 'Vergadering houden met collega\'s of externen? Boek hier je ruimte.'
            },
            {
                element: document.querySelector('#qa_13'),
                intro: 'Voor wanneer je collega\'s een werkplek willen reserveren!'
            },
            // {
            //     element: document.querySelector('#qa_9'),
            //     intro: 'Gebruik deze functie om terugkomend onderhoud te plannen.'
            // },
            {
                element: document.querySelector('#qa_10'),
                intro: 'Lamp kapot, deur die klemt, vloer nat? Meld het met deze functie.'
            },

        ]
        });

        $("#btnShowHelp").on('click', function() {
            Polaris.Onboarding.run();
        });
        $("#btnShowHints").on('click', function() {
            Polaris.Onboarding.showHints();
        });

    },
    initialize: function() {

        // Welkom bij Maxia, het facilitaire systeem.
        // Tijdens deze onboarding neem ik je stap voor stap mee door het systeem,
        // zodat je snel zelf aan de slag kunt gaan.
        // Maak je geen zorgen, je hebt altijd de optie om bepaalde informatie
        // opnieuw op te zoeken als iets niet helemaal duidelijk is.
        // Ik wens je veel succes!

        // this.steps['global'] = [
        // ];
        this.steps['const_quickactions'] = [
            {
                selector:'#qa_102',
                event:'next',
                // event_type:'custom',
                description:'Klik op het blokje om een storing te melden.',
                onBeforeStart: function(){
                    $('#qa_melding').on('click', function(e){
                        Polaris.Onboarding.tourInstance.trigger('new_melding');
                    });
                }
            }
        ];
        this.steps['const_meldingen'] = [
            {
                selector: '#sidebar',
                description: 'Dit is het menu. Hier vind je alle formulieren van de applicatie.',
                event:'click',
                // event_type:'custom',
                shape: 'rectangle'
            },
            {
                selector:'#datalistview',
                description: 'Alle informatie van dit dossier is te vinden onder verschillende tabbladen',
                event:'click',
                shape: 'rectangle'
            }
        ];

        var toursteps = document.querySelector('body').id;
        if (this.steps[toursteps]) {
            this.tourInstance = new EnjoyHint({
                nextButtonLabel: 'Volgende',
                prevButtonLabel: 'Vorige'
            });

            this.tourInstance.setScript(this.steps[toursteps]);
        }

        $(document).on('tour:help-start', function() {
            Polaris.Onboarding.run();
        });

        // $(document).trigger('tour:help-start');
    },
    run: function() {
        //run Enjoyhint script
        this.tourInstance.start();
    },
    showHints: function() {
        //run Enjoyhint script
        this.tourInstance.addHints();
    }
});
