function SC_MoveSelected(GroupFrom, GroupTo) {
  GroupFrom = $('#'+GroupFrom)[0];
  GroupTo = $('#'+GroupTo)[0];
	for (loop=GroupFrom.options.length - 1; loop >= 0; loop--) {
		if (GroupFrom.options[loop].selected == true) {
      GroupTo.options[GroupTo.options.length] = new Option(GroupFrom.options[loop].text,GroupFrom.options[loop].value);
      GroupFrom.options[loop] = null;
		}
	}
}

function SC_MoveAll(GroupFrom, GroupTo) {
  GroupFrom = $('#'+GroupFrom)[0];
  GroupTo = $('#'+GroupTo)[0];
	for (loop=GroupFrom.options.length - 1; loop >= 0; loop--) {
    GroupTo.options[GroupTo.options.length] = new Option(GroupFrom.options[loop].text,GroupFrom.options[loop].value);
    GroupFrom.options[loop] = null;
	};
  SC_ContainersNoSelection (GroupFrom, GroupTo);
}

function SC_ContainersNoSelection (Group1, Group2) {
  Group1 = $('#'+Group1)[0];
  Group2 = $('#'+Group2)[0];
  Group1.options.selectedIndex = -1;
  Group2.options.selectedIndex = -1;
}

function CS_SelectAll(Group) {
  Group = $('#'+Group)[0];
	for (loop=Group.options.length - 1; loop >= 0; loop--) {
    Group.options[loop].selected = true;
  }  
}