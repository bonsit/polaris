/***
*
* Generic (Base) Polaris Javascript Functions
*
**/
if (typeof(Polaris) == 'undefined') {
    Polaris = {};
}
if (typeof(Polaris.Base) == 'undefined') {
    Polaris.Base = {};
}
Polaris.VERSION = "2.6";
Polaris.NAME = "Polaris";

var Base = Polaris.Base;
/** @id Polaris.Base.update */
Polaris.Base.update = function (self, obj/*, ... */) {
    if (self === null) {
        self = {};
    }
    for (var i = 1; i < arguments.length; i++) {
        var o = arguments[i];
        if (typeof(o) != 'undefined' && o !== null) {
            for (var k in o) {
                if (o.hasOwnProperty(k)) {
                    self[k] = o[k];
                }
            }
        }
    }
    // console.log(self);
    return self;
};

Polaris.Base.update(Polaris.Base, {

    lastFocusedField: null,
    hideNavbarTimer: null,

    /** @id Polaris.Base.nl2br */
    nl2br: function(text, is_xhtml) {
        var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br ' + '/>' : '<br>';
        return (text + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
    },

    /** @id Polaris.Base.stringToDate */
    stringToDate: function(string) {
      var matches = string.match(/^(\d{2,2})-(\d{2,2})-(\d{4,4})$/);
      if (matches) {
        return new Date(matches[3], matches[2] - 1, matches[1]);
      } else {
        return null;
      }
    },

    /** @id Polaris.Base.dateToString */
    dateToString: function(date) {
      var month = (date.getMonth() + 1).toString();
      var dom = date.getDate().toString();
      if (month.length == 1) { month = "0" + month; }
      if (dom.length == 1) { dom = "0" + dom; }
      return dom + "-" + month + "-" + date.getFullYear();
    },

    /** @id Polaris.Base.firstDayOfWeek **/
    firstDayOfWeek: function (week, year) {
        // Jan 1 of 'year'
        var d = new Date(year, 0, 1),
            offset = d.getTimezoneOffset();

        // ISO: week 1 is the one with the year's first Thursday
        // so nearest Thursday: current date + 4 - current day number
        // Sunday is converted from 0 to 7
        d.setDate(d.getDate() + 4 - (d.getDay() || 7));

        // 7 days * (week - overlapping first week)
        d.setTime(d.getTime() + 7 * 24 * 60 * 60 * 1000
            * (week + (year == d.getFullYear() ? -1 : 0 )));

        // daylight savings fix
        d.setTime(d.getTime()
            + (d.getTimezoneOffset() - offset) * 60 * 1000);

        // back to Monday (from Thursday)
        d.setDate(d.getDate() - 3);

        return d;
    },

    /** @id Polaris.Base.getWeekNumber */
    getWeekNumber: function(d) {
        // Copy date so don't modify original
        d = new Date(d);
        d.setHours(0,0,0);
        // Set to nearest Thursday: current date + 4 - current day number
        // Make Sunday's day number 7
        d.setDate(d.getDate() + 4 - (d.getDay()||7));
        // Get first day of year
        var yearStart = new Date(d.getFullYear(),0,1);
        // Calculate full weeks to nearest Thursday
        var weekNo = Math.ceil(( ( (d - yearStart) / 86400000) + 1)/7)
        // Return array of year and week number
        return weekNo;
    },

    /** @id Polaris.Base.isInteger */
	isInteger: function(val) {
		return (val == null || isNaN(val)) ? false : (((1.0 * val) == Math.floor(val)) && ((val+"").indexOf(".") == -1));
	},

    /** @id Polaris.Base.isFloat */
	isFloat: function(val) {
		var n = (val+"").replace(/^\s+|\s+$/g, "");
		return n.length > 0 && !(/[+-][^0-9.]/).test(n) && (/\.\d/).test(n);
	},

    /** @id Polaris.Base.floatValue */
	floatValue: function(str) {
		str = str || "0";
		str = (str + '').replace(',', '.');
		return parseFloat(str);
	},

    /** @id Polaris.Base.formatFloat */
	formatFloat: function(val, decs) {
		decs = decs || 2;

		val = (val + '').replace(',', '.');
		if (Polaris.Base.isFloat(val) || Polaris.Base.isInteger(val)) {
			var value = parseFloat(val).toFixed(decs);
			return value.replace('.', ',');
		} else {
			return 'NaN';
		}
	},

    /** @id Polaris.Base.mConnect */
    mConnect: function(elem_list, event, func) {
        /***

        (Multi) Connect several element to an event

        ***/
        $(elem_list).bind(event, func);
    },

    appSelect: function() {
        var applink = this.value.split('#');
        if (applink[0].length !== 0) {
            window.location.href = _serverroot+'/cln/'+applink[0]+'/app/'+applink[1]+'/';
        } else {
            window.location.href = _serverroot+'/app/'+applink[1]+'/';
        }
    },

    /** @id Polaris.Base.trim */
    trim: function(s) {
        return s.replace(/^\s*(\S*(\s+\S+)*)\s*$/, "$1");
    },

    /** @id Polaris.Base.concat */
    concat: function (/* lst... */) {
        var rval = [];
        for (var i = 0; i < arguments.length; i++) {
            rval.concat(arguments[i]);
        }
        return rval;
    },

    remove: function(s, t) {
        /*
        **  Remove all occurrences of a token in a string
        **    s  string to be processed
        **    t  token to be removed
        **  returns new string
        */
        return s.replace(new RegExp(t,"g"), '');
        /*
        var i = s.indexOf(t);
        var r = "";
        if (i == -1) return s;
        r += s.substring(0,i) + remove(s.substring(i + t.length), t);
        return r;
        */
    },

    /** @id Polaris.Base.isOracle */
    isOracle: function() {
        return (isoracle === true);
    },

    /** @id Polaris.Base.customUrlEncode */
    customUrlEncode: function(url) {
	    if (Polaris.Base.isOracle()) {
	        return url.replace(/\+/g,'^').replace(/\//g,'$');
        } else {
            return url;
        }
	},

    /** @id Polaris.Base.customUrlDecode */
    customUrlDecode: function(url) {
	    if (Polaris.Base.isOracle() && typeof url != 'undefined') {
	        return url.replace(/\^/g,'+').replace(/\$/g,'/');
        } else {
            return url;
        }
	},
    /** @id Polaris.Base.getContrastYIQ */
    getContrastYIQ: function(hexcolor){
        var r = parseInt(hexcolor.substr(0,2),16);
        var g = parseInt(hexcolor.substr(2,2),16);
        var b = parseInt(hexcolor.substr(4,2),16);
        var yiq = ((r*299)+(g*587)+(b*114))/1000;
        return (yiq >= 128) ? 'light' : 'dark';
    },
    /** @id Polaris.Base.randomId */
    randomId: function () {
        /*
        ** Return an Id of six characters long in the range of a-z
        */
        var rId = "";
        for (var i=0; i<6;i++) {
            rId += String.fromCharCode(97 + Math.floor((Math.random()*24)));
        }
        return rId;
    },

    /** @id Polaris.Base.checkPattern */
    checkPattern: function(e,pattern) {
        e = e.target();

        if (pattern.test(e.value)===false) {
            alert("Not numeric in " + e.name);
            field.value = 0;
        }
    },

    /**
    * in case of a binary checkbox field, set the total value of the checkboxes in
    * element (id)
    */
    updateCheckboxValue: function(e) {
        const id = $(e.target).data('id');
        const elems = $('input.binarycheckbox', $('#div_'+id));
        const maxlengthcaption = 50;
        let total = 0;
        let caption = '';
        for (let i=0, elem; elem = elems[i++];) {
            if (elem.checked === true ) {
                total += parseInt(elem.value, 10);
                if ($('#divtext_'+id)) { caption += $(elem).attr('screenvalue') + ', '; }
            }
        }
        if (total === 0) {
            $('#binary_'+id).val(null);
        } else {
            $('#binary_'+id).val(total);
        }
        if ($('#divtext_'+id)) {
            caption = caption.substring(0, caption.length-2);
            if (caption.length > maxlengthcaption) { caption = caption.substr(0,maxlengthcaption) + '...'; }
            $('#divtext_'+id).innerHTML = caption;
        }
    },

    /** @id Polaris.Base.initPasswordValidation */
    initPasswordValidation: function(container, callback) {
        container = $(container) || $('body');
        if (container.length == 0) return;

        var passwordField = container.find('.cp_password');
        var confirmField = container.find('.cp_password_comfirm');
        var passwordStrength = container.find('.cp_password_strength');

        var checkBothPasswords = function() {
            if (confirmField.val() != '') {
                if (callback) callback((confirmField.val() == passwordField.val()) ? 'passwords_ok' : 'passwords_fail');
            } else {
                $("#retypecorrect").text('');
            }
        }

        passwordField.on('keyup', function(e){
            if (passwordField.val().trim() !== '')
                passwordStrength.html(Polaris.Base.checkPassword(passwordField.val()));
            checkBothPasswords();
        });

        confirmField.on('keyup', function(e){
            checkBothPasswords();
        });
    },

    /** @id Polaris.Base.checkPassword */
    checkPassword: function(password) {
        var strength = $.t('plr.password_strengths').split('_');

        var score = 1;
        var result = "";

        if (password.length < 1)
            result = strength[1];
        if (password.length < 4)
            result = strength[1];

        if (result == "") {
            if (password.length >= 8)
                score++;
            if (password.length >= 16)
                score = score + 3;
            if (password.match(/\d+/))
                score++;
            if (password.match(/[a-z]/) &&
                password.match(/[A-Z]/))
                score++;
            if (password.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,£,(,)]/))
                score++;
            if (score > 5) score = 5;
            result = strength[score];
        }
        var color = '#F04713';
//        if (score == 3) color = '#F3CA25';
        if (score >= 4) color = '#17A704';
        result = '<span style="font-weight:bold;color:'+color+'">' + result + '</span>';
        return result;
    },

    /** @id Polaris.Base.queryString */
    queryString: function(obj) {
        var chunks = [];

        var is = function(o, t) {
            return (typeof o !== 'undefined') && o !== null && (!!t ? o.constructor == t : true);
        };

        var addFields = function(arr, key, value) {
          if (!is(value) || value === false) { return; }
          var o = [key];
          if (value !== true) {
            o.push("=");
            o.push(encodeURIComponent(value));
          }
          arr.push(o.join(""));
        };

        $.each(obj, function(key, value) {
            addFields(chunks, key, value);
        });

        return chunks.join("&");
    },

    /** @id Polaris.Base.encodeUrlValue */
    encodeUrlValue: function(value) {
        var encodedValue = escape(value);
        /*
        encodedValue = encodedValue.replace(/\//g,"%2F");
        encodedValue = encodedValue.replace(/\?/g,"%3F");
        encodedValue = encodedValue.replace(/=/g,"%3D");
        encodedValue = encodedValue.replace(/&/g,"%26");
        encodedValue = encodedValue.replace(/@/g,"%40");
        */
        return encodedValue;
    },

    /** @id Polaris.Base.parseQueryString */
    parseQueryString: function (encodedString, useArrays) {
        if (typeof(encodedString) == "undefined" || encodedString == '') { return false; }
        // strip a leading '?' from the encoded string
        var qstr = (encodedString.charAt(0) == "?") ? encodedString.substring(1) : encodedString;
        var pairs = qstr.replace(/\+/g, "%20").split(/(\&amp\;|\&\#38\;|\&#x26;|\&)/);
        var o = {};
        var decode;
        if (typeof(decodeURIComponent) != "undefined") {
            decode = decodeURIComponent;
        } else {
            decode = unescape;
        }
        var i, pair, j = pairs.length;
        if (useArrays) {
            for (i = 0; i < j; i++) {
                pair = pairs[i].split(/=(.+)/);  // Split on first '=' only
                if (pair.length < 2) {
                    continue;
                }
                var name = decode(pair[0]);
                var arr = o[name];
                if (!(arr instanceof Array)) {
                    arr = [];
                    o[name] = arr;
                }
                arr.push(decode(pair[1]));
            }
        } else {
            for (i = 0; i < j; i++) {
                pair = pairs[i].split(/=(.+)/);  // Split on first '=' only
                if (pair.length < 2) {
                    continue;
                }
                o[decode(pair[0])] = decode(pair[1]);
            }
        }
        return o;
    },

    /** @id Polaris.Base.getQueryParameter */
    getQueryParameter: function(key, querystring) {
        var query = Polaris.Base.parseQueryString(querystring);
        return query[key];
    },

    /** @id Polaris.Base.removeQueryParameter */
    removeQueryParameter: function(key, querystring) {
        if (querystring == '') { return []; }
        var query = Polaris.Base.parseQueryString(querystring);
        query[key] = '';
        return Polaris.Base.queryString(query);
    },

    /** @id Polaris.Base.addQueryParameter */
    addQueryParameter: function(key, value, querystring) {
        querystring = Polaris.Base.removeQueryParameter(key, querystring);
        var query = Polaris.Base.parseQueryString(querystring);
        if (!query) query = {};
        query[key] = value;
        return Polaris.Base.queryString(query);
    },

    /** @id Polaris.Base.getLocationVariable */
    getLocationVariable: function(key) {
        return Polaris.Base.getQueryParameter(key, window.location.search);
    },

    /** @id Polaris.Base.removeLocationVariable */
    removeLocationVariable: function(key) {
        var query = Polaris.Base.removeQueryParameter(key, window.location.search);
        return window.location.protocol+'//'+window.location.host+window.location.pathname+'?'+query;
    },

    /** @id Polaris.Base.addLocationVariable */
    addLocationVariable: function(key, value) {
        var query = Polaris.Base.addQueryParameter(key, value, window.location.search);
        return window.location.protocol+'//'+window.location.host+window.location.pathname+'?'+query;
    },

    /** @id Polaris.Base.masterItemSelect */
    masterItemSelect: function(on) {
        $("body").toggleClass('insert', !on).toggleClass('allitems', on);
        if (on)
            $("body").removeClass('search');
    },

    /** @id Polaris.Base.rawPopup */
    rawPopup: function(url, target, features) {
        var width = Math.floor(document.documentElement.clientWidth*0.80);
        var height = Math.floor(document.documentElement.clientHeight*0.80);
        var _POPUP_FEATURES = 'width='+width+',height='+height+',scrollbars,resizable,toolbar,status,menubar,location';
        var sUserAgent = navigator.userAgent.toLowerCase();
        var isOp = (sUserAgent.indexOf('opera')!=-1)?true:false;

        // pops up a window containing url optionally named target, optionally having features
        if (isUndefined(features)) { features = _POPUP_FEATURES; }
        if (isUndefined(target  )) { target   = '_blank'; }
        var theWindow = window.open(url, target, features);
        // An Opera bug returns too early if you focus the window, so we don't focus it in that browser.
        // Only a noticable defect (in that browser) if a window is already open and hidden behind another window.
        if (theWindow && !isOp) { theWindow.focus(); }
        return (theWindow)?false:true;
    },
    /** @id Polaris.Base.linkPopup */
    linkPopup: function(src, features) {
        // to be used in an html event handler as in: <a href="..." onclick="link_popup(this,...)" ...
        // pops up a window grabbing the url from the event source's href
        if (typeof(features) == 'undefined') {
            var width = Math.floor(document.documentElement.clientWidth*0.80);
            var height = Math.floor(document.documentElement.clientHeight*0.80);
            var left = (Math.floor(document.documentElement.clientWidth) - width) / 2;
            var top = (Math.floor(document.documentElement.clientHeight) - height) / 2;
            features = 'width='+width+',height='+height+',left='+left+',top='+top;
        }
        return Polaris.Base.rawPopup($(src).prop('href'), $(src).prop('target') || '_blank', features);
    },
    /** @id Polaris.Base.modalMessage */
    modalMessage: function(str, customtimeout, typemessage) {
        customtimeout = customtimeout || 1200;
        typemessage = typemessage || 'success';

        toastr.options = {
          "closeButton": true,
          "debug": false,
          "progressBar": false,
          "positionClass": "toast-top-right",
          "escapeHtml": true,
          "onclick": null,
          "showDuration": "400",
          "hideDuration": "1000",
          "timeOut": customtimeout,
          "extendedTimeOut": "1000",
          "showEasing": "swing",
          "hideEasing": "linear",
          "showMethod": "fadeIn",
          "hideMethod": "fadeOut"
        };
        toastr[typemessage]('', str);
    },
    /** @id Polaris.Base.modalMessage2 */
    modalMessage2: function(str, customtimeout, overlay, whenblocked) {
        if (typeof customtimeout == 'undefined') customtimeout = 1500;
        if (typeof overlay == 'undefined') overlay = true;
        $.blockUI({
            message: str,
            fadeIn: 0,
            fadeOut: 0,
            baseZ: 3000,
            timeout: customtimeout,
            showOverlay: overlay,
            onBlock: whenblocked,
            css: {
                border: 'none',
                padding: '1em',
                backgroundColor: '#ddd',
                opacity: 0.9,
                color: '#222',
                fontSize: '1.4em',
                letterSpacing: '1px'
            }
         });
    },
    /** @id Polaris.Base.modalDialog */
    modalDialog: function(str, options) {
        var buttons = [];
        if (options) {
            if (typeof options.no == 'function')
                buttons.push({
                    cssClass: 'btn-default',
                    label: $.t('plr.no'),
                    action: options.no
                });
            if (typeof options.cancel == 'function')
                buttons.push({
                    cssClass: 'btn-default',
                    label: $.t('plr.cancel'),
                    action: options.cancel
                });
            if (typeof options.close == 'function')
                buttons.push({
                    cssClass: 'btn-default',
                    label: $.t('plr.close'),
                    action: function(dialogItself){
                        options.close();
                        dialogItself.close();
                    }
                });
            if (typeof options.yes == 'function')
                buttons.push({
                    cssClass: 'btn-primary',
                    label: $.t('plr.yes'),
                    action: options.yes
                });
        } else {
            buttons.push({
                label: $.t('plr.close'),
                action: function(dialogItself){
                    dialogItself.close();
                }
            });
        }
        BootstrapDialog.show({
            type: options && options.type || BootstrapDialog.TYPE_WARNING,
            closable: true,
            closeByBackdrop: true,
            closeByKeyboard: true,
            message: str,
            buttons: buttons
        });
    },

    /** @id Polaris.Base.closeModalMessage */
    closeModalMessage: function() {
        if ($.unblockUI)
            $.unblockUI({fadeOut: 0});
        if (Polaris.Base.lastFocusedField)
            $(Polaris.Base.lastFocusedField).focus();
    },
    /** @id Polaris.Base.errorDialog */
    errorDialog: function(str, detailedstr) {
        this.modalDialog(str + "\n" + detailedstr, { close: function() {}, type: 'type-danger' })
    },
    /** @id Polaris.Base.showAdvancedSearchModal */
    showAdvancedSearchModal: function() {
        BootstrapDialog.show({
            type: BootstrapDialog.TYPE_INFORMATION,
            closable: true,
            closeByBackdrop: true,
            closeByKeyboard: true,
            title: 'Advanced search options',
            message: 'Search options not yet available',
            buttons: [{
                label: $.t('plr.ok'),
                action: function(dialogItself){
                    dialogItself.close();
                }
            }]
        });
    },
    /***
    * Set the focus on the first empty field or on the first (filled) search field
    **/
    setMainFocus: function() {
        if ($('.formview').length > 0) {
            if ($('.formview :input[autofocus]').length == 0)
                Polaris.Form.setFocusFirstField('.formview');
        } else if ($.cookies.get('searchpanel')=='on') {
            if ($("#searchpanel input[type=search]:empty").length > 0) {
                $("#searchpanel input[type=search]:empty").focus();
            } else {
                Polaris.Form.setFocusFirstField('#searchpanel');
            }
        } else if (Polaris.Base.getLocationVariable('select') == 'true') {
            $("#searchquery").focus();
        } else if ($('#datalistview').length > 0) {
            $(".bodyscroll:first").scrollTop(0);
            //$('#datalistview').data('input').focus(); //Deze stoort de focus van de listview, scrollt helemaal naar beneden
        } else {
            $("#loginform").find('input:text[value=""]').add('input:password[value=""]').first().focus();
        }
    },
    /** @id Polaris.Base.userIsSearching */
    userIsSearching: function() {
        return (typeof Polaris.Base.getLocationVariable('q') != 'undefined'
        || typeof Polaris.Base.getLocationVariable('q[]') != 'undefined');
    },
    initializeLocalization: function() {
        $.jsperanto.init(function(t){
            Polaris.Base.afterLanguageResourceLoaded(t);
        }, {
            fallbackLang: 'nl',
            dicoPath: _serverroot+'/languages',
            setDollarT: true,
            context: 'polaris',
            async : false,
            lang:'nl' //PROBLEM WITH LANGUAGE g_lang.toLowerCase()
        });
    },
    initializeUserSettings: function() {
        $("#topcontent").data('closed', true);
        if ($("#usersettingscontent").length == 0) {
            $topcontent = $("#topcontent");
            $usersettings = $("<div id=\"usersettingscontent\" style=\"display:none\"></div>");
            $usersettings.appendTo($topcontent);
        }

        $("#usersettings").click(function(e) {
            e.preventDefault();

            Polaris.Window.showModuleForm('/user/', function(data) {
                log(data);
            });
        });
    },
    initializePinnedRecords: function() {
        var $pr = $("#pinnedrecords");

        if ($pr.length > 0) {
            var processPins = function(e) {
                var self = this;
                var _table_recordid = $(this).data('pin').split(':');
                _table = _table_recordid[0];
                _recordId = _table_recordid[1];

                if (_table == 'ALL' && _recordId == 'ALL') {
                    Polaris.Ajax.deletePins($('#dataform input[name=_hdnDatabase]').val());
                } else {
                    Polaris.Ajax.deletePin($('#dataform input[name=_hdnDatabase]').val(), _table, _recordId);
                }
            };

            $("label", $pr[0]).css({'cursor':'pointer'}).click(processPins);
            $(".deleteallpins label").click(processPins);
            $($pr).on('click', "label", processPins);
            $($pr).on('click', ".deletepin", processPins);
        }
    },
    initializeAppsMenu: function() {
        $("#appselect").on('click', function() {
            if ($("#appsmenu").is(":visible"))
                $("#appsmenu").hide();
            else
                $("#appsmenu").show();
        });
        $(document).on('keydown', function(event) {
            if (event.keyCode == 27 && $("#appsmenu").is(":visible"))
                $("#appsmenu").hide();
        });
        $('body').on('click', function(event) {
            if (($(event.target).parents('#appselect').length == 0) && $(event.target).closest("#appsmenu").length == 0)
                $("#appsmenu").hide();
        });
    },
    initializeIconSelect: function(container) {
        $("[data-toggle='overlay']").on('click', function(e) {
            e.preventDefault();

            const self = $(this);

            var beforeOverlay = function() {
                $("#icons-picker-search").trigger('focus');

                $("#icons-picker-search").on("keyup", function() {
                    var value = $(this).val().toLowerCase();
                    $(".icons-picker-icon-filter").filter(function() {
                        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                    });
                });

                $(".icons-picker-icon-filter").on("click", function() {
                    var icon = $(this).find("i").text();
                    $("#"+self.data('field')).val(icon);
                    self.text(icon);
                    self.parents("div").find(".icon-picker-name").text(icon);
                    Polaris.Form.hideOverlay();
                });
            };

            Polaris.Form.showOverlay($(this).data('overlay'), '40vmax', beforeOverlay);
        });
    },
    select2Cascade: function(parent, child, parentfield, idcolumn, showcolumn, valuepicklist, select2Options) {
        var afterActions = [];
        var options = select2Options || {};

        // Register functions to be called after cascading data loading done
        this.then = function(callback) {
            afterActions.push(callback);
            return this;
        };
        parent.select2(select2Options).on("change", function (e) {
            child.prop("disabled", true);

            // loop through all ..._lookuppicklist items
            // take the variabel from the global window object
            var items = window[child.attr('name')+'_lookuppicklist'];
            var newOptions = '<option value="">--Selecteer--</option>';
            for(var rec in items) {
                if (items[rec][parentfield] == parent.val()) {
                    newOptions += '<option value="'+ items[rec][idcolumn]+'">'+ items[rec][showcolumn] +'</option>';
                }
                child.select2('destroy').html(newOptions).select2(options);
                // disabled if items var is empty
                child.prop("disabled", items.length === 0);
            }
            afterActions.forEach(function (callback) {
                callback(parent, child, items);
            });
        });
        return this;
    },
    initializeSelect2: function(container) {
        container = container || document;
        $('select.select2', container).each(function(index, elem) {
            var isMultiselect = $(elem).prop('multiple');
            const placeholder = 'Kies ' + ($(elem).data('placeholdertext') || 'waarde');
            $(elem).select2({
                placeholder: {
                    id: "",
                    text: placeholder
                },
                maximumSelectionLength: 30,
                allowClear: $(elem).hasClass('required') ? false : true,
                dropdownAutoWidth : 'true',
                width: $(this).data('select_width') ?? 'resolve',
                minimumResultsForSearch: ($(this).data('select_searchable') ?? -1),
                templateResult: function (data) {
                    // Check if the option is 'show_select_additem_modal'
                    return $('<span style="font-weight: bold;">' + data.text + '</span>');
                },
                templateSelection: function (data) {
                    // For the selected option, use the default styling
                    return data.text;
                }
            });

            if (isMultiselect) {
                var multivalue = $(elem).data('currentvalue');
                if (multivalue) {
                    // convert object to array
                    var multivalueArray = Object.keys(multivalue).map(key => multivalue[key])
                    $(elem).val(multivalueArray).trigger('change');
                }
            }

            if ($(elem).data('cascade-parent')) {
                const $parent = $("#"+$(elem).data('cascade-parent'));
                const $child = $(elem);
                const idcolumn = $child.data('cascade-idcolumn');
                const showcolumn = $child.data('cascade-showcolumn');
                const valuepicklist = $child.data('cascade-valuepicklist');
                const parentfield = $child.data('cascade-parent-field');

                var tempValue = '';
                if ($parent) {
                    if ($child.data('currentvalue') !== '') {
                        tempValue = $child.data('currentvalue');
                    } else {
                        tempValue = $child.val();
                    }

                    select2Options = {};
                    Polaris.Base.select2Cascade($parent, $child, parentfield, idcolumn, showcolumn, valuepicklist, select2Options)
                    .then(function(parent, child, items) {
                        // Restore the value because the ajax call will reset the value
                        child.val(tempValue).trigger('change');
                    });
                    $parent.trigger('change');
                };
            }
        });
    },
    initializeIChecks: function(container) {
        /***
        * Setup iCheck binary checkbox callbacks
        */
        $('input.icheck.binarycheckbox').on('ifToggled', function(e) {
            Polaris.Base.updateCheckboxValue(e)
        });

        container = container || 'body';
        /***
        * Initialize all the iChecks
        */
        $('input.icheck', container).iCheck({
            checkboxClass: 'icheckbox_square-grey',
            radioClass: 'iradio_square-grey',
            increaseArea: '50%' // optional
        });

    },
    elfProef: function(input, weights, positiveSum) {
        if (typeof input === 'number') {
            input = input.toString();
        }
        if (input.length !== weights.length) {
            throw new Error('Amount of weights must be equal to length of input');
        }
        const numbers = input.split('');
        const sum = numbers
            .map((value, index) => {
                const number = parseInt(value, 10);
                const weight = weights[index];
                return number * weight;
            })
            .reduce((a, b) => a + b);
        const validMod = sum % 11 === 0;
        const validRange = positiveSum === false || sum > 0;
        return validMod && validRange;
    },
    validateElfProef: function(input) {
        return Polaris.Base.elfProef(input, [9, 8, 7, 6, 5, 4, 3, 2, -1], false);
    },
    validateBSN: function(input) {
        if (typeof input === 'number') {
            input = input.toString();
        }
        const validFormat = /^[\d]{8,9}$/.test(input);
        if (!validFormat) {
            return false;
        }
        const prependedInput = input.length === 8 ? `0${input}` : input;
        if (prependedInput.startsWith('00')) {
            return false;
        }
        const validElfProef = Polaris.Base.elfProef(prependedInput, [9, 8, 7, 6, 5, 4, 3, 2, -1], false);
        return validElfProef;
    },
    isMinimumAge: function(date, minimumAge) {
        var parts = date.split('-');
        var day = parts[0];
        var month = parts[1];
        var year = parts[2];

        var mydate = new Date();
        mydate.setFullYear(year, month-1, day);

        var currdate = new Date();
        currdate.setFullYear(currdate.getFullYear() - minimumAge);
        return (currdate - mydate) > 0;
    },
    maintenanceCounter: null,
    durationCounter:null,
    durationTmp:0,
    durationUpdate: function(timer) {
        var durationtime = Math.round(Polaris.Base.durationTmp / 60);
        var durationext = ' min';
        if (Polaris.Base.durationTmp < 60) {
            durationtime = Polaris.Base.durationTmp;
            durationext = ' sec';
        }

        if (Polaris.Base.durationTmp < 0)
            Polaris.Base.durationCounter = null;

        $("#duration_time").text(''+durationtime + durationext);
        $("#mw_duration").text(durationtime);

        if (timer)
            Polaris.Base.durationTmp--;
    },
    initializeMaintenance: function() {
        Polaris.Base.initializeMaintenanceWindow();
        $("#maintenance-alert").on('mouseenter', function() {
            $("#maintenance_window").show();
        });
        $("#maintenance-alert").on('mouseleave', function() {
            $("#maintenance_window").delay(3000).fadeOut();
        });
        $("#maintenance-alert").on('click', function() {
            $("#maintenance_window").show().delay(3000).fadeOut();
        });
    },
    initializeMaintenanceWindow: function() {
        if (typeof mnt_duration !== 'undefined' && mnt_duration !== false) {
            if (Polaris.Base.maintenanceCounter == null)
                Polaris.Base.maintenanceCounter = setInterval('Polaris.Base.initializeMaintenance()', 1000);

            var starttime = Math.round(mnt_secondstostart / 60);
            var startext = ' min';
            if (mnt_secondstostart < 60) {
                starttime = mnt_secondstostart;
                startext = ' sec';
            }
            $("#mw_start").text(starttime+startext);
            $("#maintenance-alert").addClass('active');

            Polaris.Base.durationTmp = mnt_duration;
            Polaris.Base.durationUpdate(false);

            if (mnt_secondstostart !== false) {

                if (mnt_secondstostart <= 60) {
                    $("#maintenance-alert i").addClass('fa-shake');
                }
                if (mnt_secondstostart == 0) {
                    window.clearInterval(Polaris.Base.maintenanceCounter);
                    Polaris.Base.maintenanceCounter = null;
                    if (!mnt_skip_message) {
                        $("#maintenance_window").hide();
                        $("#maintenance-alert").removeClass('active');
                        $("#maintenance-alert i").removeClass('fa-shake');
                        var whenblocked = function() {
                            if (Polaris.Base.durationCounter == null)
                                Polaris.Base.durationCounter = setInterval('Polaris.Base.durationUpdate(true)', 1000);
                        };
                        Polaris.Base.modalMessage2('<i class="material-symbols-outlined md-2x">settings</i> ' + $.t('plr.polaris_maintenance', {brand_name: brand_name, reason: mnt_remark}) + '<br/><br/><small>' + $.t('plr.dont_close_polaris') + '</small>'
                         , mnt_duration * 1000, true, whenblocked);
                    }
                } else {
                    mnt_secondstostart--;
                }
            }
        }
    },
    /***
    The events and routines that should be executed after the language resource file has been loaded
    ***/
    afterLanguageResourceLoaded: function(t) {
        var F = Polaris.Form;

        if ($('.formview').length) {
            /***
            * initialize the form validation
            */
            F.initializeFormValidation("form.validate");
            /***
            * Setup the datepicker controls
            *  - Set the default Localized values
            */
            F.initializeDatePickers();
            /***
            * Setup the timepicker controls
            */
            F.initializeTimePickers();
        }
    },
});

/*  */
function processRanking(index,e) {
    var currentrating = parseInt(e.target().getAttribute("value"), 10);
    var ul = e.target().parentNode.parentNode;
    var currentratingli = MochiKit.DOM.getFirstElementByTagAndClassName('li','current-rating',ul);
    var ratingfield = MochiKit.DOM.getFirstElementByTagAndClassName('input','',ul);
    var ratingwidth = (currentrating * 25);
    currentratingli.style.width = ratingwidth+'px'; // 25 is the width of one star in the image
    ratingfield.value = currentrating;
}

function cb_selectChange(containerdiv, onoff) {
	var cbs = $(containerdiv).getElementsByTagName('input');
	var ii = cbs.length;
	for(i=0;i<ii;i++) {
        if (cbs[i].type=='checkbox') {
            cbs[i].checked = onoff;
        }
	}
}

/**
* sync the checked-value of the options fields when the total value field has changed
* element (id)
*/
function syncCheckboxValue(id) {
	if ($(id)) {
		var total = parseInt($(id).value, 10);
		var elems = getElementsByTagAndClassName('input', 'binarycheckbox', $('div_'+id) );
		var ii = elems.length;
		for(i=0;i<ii;i++) {
			if ((total & parseInt(elems[i].value, 10)) == parseInt(elems[i].value, 10)) {
				 elems[i].checked = true;
		    }
		}
	}
}

var isUndefined = function(v) {
    var undef;
    return  v===undef;
};

var isNullOrEmpty = function(value) {
    return value === undefined || value === null || value === '';
}

function lookupObject(array, name) {
    for (var i = 0, len = array.length; i < len; i++) {
        if (array[i].name === name) return array[i].value;
    }
}

Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

function truncateWithEllipses(text, max) {
    return text.substr(0,max-1)+(text.length>max?'&hellip;':'');
}

function testWhite(x) {
    var white = new RegExp(/^\s$/);
    return white.test(x.charAt(0));
}

// triggers a 'Can't create duplicate variable' error:
// const wrap = (s, w) => s.replace(
//     new RegExp(`(?![^\\n]{1,${w}}$)([^\\n]{1,${w}})\\s`, 'g'), '$1<br>'
// );

function wordWrap(str, maxWidth) {
    var newLineStr = "<br>"; done = false; res = '';
    do {
        found = false;
        // Inserts new line at first whitespace of the line
        for (i = maxWidth - 1; i >= 0; i--) {
            if (testWhite(str.charAt(i))) {
                res = res + [str.slice(0, i), newLineStr].join('');
                str = str.slice(i + 1);
                found = true;
                break;
            }
        }
        // Inserts new line at maxWidth position, the word is too long to wrap
        if (!found) {
            res += [str.slice(0, maxWidth), newLineStr].join('');
            str = str.slice(maxWidth);
        }

        if (str.length < maxWidth)
            done = true;
    } while (!done);

    return res + str;
}

var log = function()
{
    "use strict";
    if (typeof(console) !== "undefined" && console.log !== undefined) {
        try {
            console.log.apply(console, arguments);
        }
        catch (e) {
            var log = Function.prototype.bind.call(console.log, console);
            log.apply(console, arguments);
        }
    }
};

// fix the webkit layer deprecated message in the console
// STILL A PROBLEM?? $.event.props = $.event.props.join('|').replace('layerX|layerY|', '').split('|');

// fill Select box with Option values
jQuery.fn.addOptions = function(values, keyname, valuename, defaultvalue, addfunc) {
    var select = this;
    addfunc = addfunc || 'append';
    $.each(values, function(i, val) {
        var thevalue = '';
        if (valuename.indexOf(',') > -1) {
            var valuenames = valuename.split(',');
            for(vindex in valuenames) {
                if (valuenames[vindex].charAt(0) == '#') {
                    thevalue += valuenames[vindex].substr(1) + ' ';
                } else {
                    thevalue += val[valuenames[vindex]] + ' ';
                }
            }
        } else {
            thevalue = val[valuename];
        }
        var $opt = $(new Option(thevalue, val[keyname], false
        /*defaultSelected*/
        , defaultvalue == val[keyname]));
        $opt.data(val);
        if (addfunc == 'append')
            select.append($opt);
        else
            select.prepend($opt);
    });
    return this;
};

// fill Select box with Option values
jQuery.fn.addOptionsOptGroup = function(values, keyname, valuename, defaultvalue) {
    var select = this;
    $.each(values, function(key, group) {
        var $optgroup = $('<optgroup>');
        $optgroup.attr('label', key);
        $optgroup.addOptions(group, keyname, valuename, defaultvalue);
        select.append($optgroup);
    });
    return this;
};

jQuery.fn.liveUpdate = function(list){
    list = jQuery(list);

    if ( list.length ) {
        var rows = list.children('li'),
            cache = rows.map(function(){
                return this.innerHTML.toLowerCase();
            });

        this
            .keyup(filter).keyup()
            .parents('form').submit(function(){
                return false;
            });
    }

    return this;

    function filter(){
        var term = jQuery.trim( jQuery(this).val().toLowerCase() ), scores = [];

        if ( !term ) {
            rows.show();
        } else {
            rows.hide();

            cache.each(function(i){
                if (this.indexOf(term) != -1) { scores.push([i]); }
            });

            jQuery.each(scores, function(){
                jQuery(rows[ this[0] ]).show();
            });
        }
    }
};

jQuery.extend(
  jQuery.expr[':'],
  {
    /// check that a field's value property has a particular value
    'field-value': function (el, indx, args) {
      var a, v = $(el).val();
      if ( (a = args[3]) ) {
        switch ( a.charAt(0) ) {
          /// begins with
          case '^':
            return v.substring(0,a.length-1) == a.substring(1,a.length);
          break;
          /// ends with
          case '$':
            return v.substr(v.length-a.length-1,v.length) ==
              a.substring(1,a.length);
          break;
          /// contains
          case '*': return v.indexOf(a.substring(1,a.length)) != -1; break;
          /// equals
          case '=': return v == a.substring(1,a.length); break;
          /// not equals
          case '!': return v != a.substring(1,a.length); break;
          /// equals
          default: return v == a; break;
        }
      }
      else {
        return !!v;
      }
    }
  }
);

/**
 * Post function for JSON data
 * DEPRICATED (better use Polaris.Ajax.postJSON).
 */
$.postJSON = function(url, data, callback) {
    console.warn('The function $.postJSON is deprecated. Use Polaris.Ajax.postJSON');
    // zet alle disabled input velden 'aan', anders worden ze niet gePOST (raar)
    $(':checkbox[disabled]').prop('disabled', false).data('wasdisabled', "true");

    if (typeof data.serializeArray == 'function') {
        data = data.serializeArray();
    }
    $.post(url, data, callback, "json");
    $(":checkbox[data-wasdisabled]").prop('disabled', true).removeData('wasdisabled');
};

(function($){
    var _dataFn = $.fn.data;
    $.fn.data = function(key, val){
        if (typeof val !== 'undefined'){
            $.expr.attrHandle[key] = function(elem){
                return $(elem).prop(key) || $(elem).data(key);
            };
        }
        return _dataFn.apply(this, arguments);
    };
})(jQuery);
