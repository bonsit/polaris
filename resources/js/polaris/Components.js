if (typeof(Polaris.Components) == "undefined") {
    Polaris.Components = {};
}

Polaris.Components.NAME = "Polaris.Components";
Polaris.Components.VERSION = "1.0";

try {
    if (typeof(Polaris.Base) === 'undefined') {
        throw "";
    }
} catch (e) {
    throw "Polaris.Components depends on Polaris.Base";
};

Base.update(Polaris.Components, {

    toggleComponent: function(e) {
    },

    GetVisibleCount: function(parent) {
        return $(parent).children("li").length;
    },

    LastFieldEmpty: function(ulparent) {
        var totvalue = "";
        $(".comp_valuefield", ulparent).each(function(i, elem) {
            totvalue += elem[i].value;
        });
        return totvalue == "";
    },

    UpdateComponentblock: function(component, visiblecount) {
        var self = Polaris.Components;

        var nodes = $("input.remove_item", component);
        if (visiblecount == 1)  {
            nodes.prop("disabled","disabled");
        } else if (visiblecount == 0) {
            if (nodes.length == 1)
                nodes.prop("disabled","disabled");
        } else {
            nodes.prop("disabled","");
        }
    },

    UpdateComponentblocks: function(ul) {
        var self = Polaris.Components;

        var visiblecount = self.GetVisibleCount(ul);
        $(ul).children("li").each(function() {
            self.UpdateComponentblock(this, visiblecount)
        });
    },

    AddComponentItem: function(elem, callback) {
        var self = Polaris.Components;

        var $component = $(elem.target).parent().parent();
        var $parent = $component.parent();

        var set = $component.clone(true);
        $parent.append(set).find(":input:visible").focus();

        self.UpdateComponentblocks($parent);

        if ($.isFunction(callback))
            callback(set);
    },

    RemoveComponentItem: function(elem, callback) {
        var self = Polaris.Components;

        var $component = $(elem.target).parent().parent();
        var $parent = $component.parent();
        if (self.GetVisibleCount($parent) > 1) {
            $component.remove()
        }

        self.UpdateComponentblocks($parent);

        if ($.isFunction(callback))
            callback($component);
    }
});
