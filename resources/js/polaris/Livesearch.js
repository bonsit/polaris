if (typeof(Polaris.Livesearch) == "undefined") {
    Polaris.Livesearch = {}
}

Polaris.Livesearch.NAME = "Polaris.Livesearch";
Polaris.Livesearch.VERSION = "1.0";

try {
    if (typeof(Polaris.Base) === 'undefined') {
        throw "";
    }
} catch (e) {
    throw "Polaris.Livesearch depends on Polaris.Base";
}

Base.update(Polaris.Livesearch, {

    liveSearchReq: false,
    t: null,
    liveSearchLast: "",

    imageon: _serverroot+'/images/searching.gif',
    imageoff: _serverroot+'/images/livesearch3.gif',
    urlPolarisLivesearchProcessor: _serverroot+"/livesearch.php",
	
    isIE: false,

    liveSearchInit: function() {
        var self = Polaris.Livesearch;
        if ($('#livesearchimage')) {
            $('#livesearchimage').click( self.toggleLiveSearchBox );
            $('#livesearch').attr("autocomplete","off").keypress( self.liveSearchStart ).bind('onsearch',  self.liveSearchDoSearch );
    //		hideElement('livesearch');
        }
    },

    liveSearchSearching: function(on) {
        var self = Polaris.Livesearch;
        if (on) {
            $("#livesearchimage").src = self.imageon;
        } else {
            $("#livesearchimage").src = self.imageoff;
        }
    },

    closeResults: function() {
        $('#livesearch').hide();
    },

    toggleLiveSearchBox: function() {
        $('#livesearch').toggle().focus();
    },

    liveSearchHideDelayed: function() {
        window.setTimeout("Polaris.Livesearch.liveSearchHide()",400);
    },

    liveSearchHide: function() {
        Polaris.Livesearch.liveSearchSearching(false);
    
        $("#LSResult").hide();
        if ($("#LSHighlight").length) {
            $("#LSHighlight").removeAttr("id");
        }
    },

    liveSearchStart: function() {
        var self = Polaris.Livesearch;
        if (self.t) {
            window.clearTimeout(self.t);
        }
        self.t = window.setTimeout("Polaris.Livesearch.liveSearchDoSearch()",200);
    },

    liveSearchDoSearch: function() {
        var self = Polaris.Livesearch;
        self.liveSearchSearching(true);
    
        if (typeof self.liveSearchRoot == "undefined") {
            self.liveSearchRoot = "/";
        }
        if (typeof self.liveSearchRootSubDir == "undefined") {
            self.liveSearchRootSubDir = "";
        }
        if (typeof self.liveSearchParams == "undefined") {
            self.liveSearchParams2 = "";
        } else {
            self.liveSearchParams2 = "&" + self.liveSearchParams;
        }
        if (self.liveSearchLast != document.forms.searchform.q.value) {

            if ( document.forms.searchform.q.value == "") {
                self.liveSearchHide();
                return false;
            }

            var appindex = location.href.indexOf('/app/');
            var appmeta = location.href.substring(appindex+5, location.href.indexOf('/', appindex+5));
            
            $.get(self.urlPolarisLivesearchProcessor, 
            {
                app: appmeta,
                q: document.forms.searchform.q.value.replace(/\+/g,"%2B")
            },
            self.liveSearchProcessReqChange);
            
            self.liveSearchLast = document.forms.searchform.q.value;
        }
    },

    liveSearchProcessReqError: function(error) {
        log(error);
    },
    
    liveSearchProcessReqChange: function(response) {
        var self = Polaris.Livesearch;
        $("#LSResult").show();
        $("#LSShadow").html(response);
             
        self.liveSearchSearching(false);
    },
    
    liveSearchSubmit: function() {
        var self = Polaris.Livesearch;
        var highlight = $("#LSHighlight");
        if (highlight.length && highlight.children()[0]) {
            window.location = self.liveSearchRoot + self.liveSearchRootSubDir + highlight.firstChild.nextSibling.getAttribute("href");
            return false;
        } 
        else {
            return true;
        }
    }
});
