if (typeof(Polaris.AutoSuggest) == "undefined") {
    Polaris.AutoSuggest = {};
}

Polaris.AutoSuggest.NAME = "Polaris.AutoSuggest";
Polaris.AutoSuggest.VERSION = "1.0";

try {
    if (typeof(Polaris.Base) === 'undefined') {
        throw "";
    }
} catch (e) {
    throw "Polaris.AutoSuggest depends on Polaris.Base";
}
Base.update(Polaris.AutoSuggest, {

    _CreateAutoSuggest: function(valuefld, searchfld, param) {
        this.valuefld = valuefld;
        this.searchfld = searchfld;

        this.list_ul = [];

        if (!this.valuefld || !this.searchfld)
            return false;

        this.nInputChars = 0;
        this.aSuggestions = [];
        this.iHighlighted = 0;

        // parameters object
        this.oP = (param) ? param : {};
        // defaults
        if (!this.oP.minchars)		this.oP.minchars = 1;
        if (!this.oP.method)		this.oP.meth = "get";
        if (!this.oP.varname)		this.oP.varname = "input";
        if (!this.oP.valuefield)	this.oP.valuefield = false;
        if (!this.oP.captionfields)	this.oP.captionfields = false;
        if (!this.oP.searchfields)	this.oP.searchfields = false;
        if (!this.oP.params)	    this.oP.params;
        if (!this.oP.className)		this.oP.className = "autosuggest";
        if (!this.oP.timeout)		this.oP.timeout = 0;
        if (!this.oP.delay)			this.oP.delay = 500;
        if (!this.oP.maxheight && this.oP.maxheight !== 0)		this.oP.maxheight = 250;
        if (!this.oP.cache)			this.oP.cache = true;
        var self = this;

        this.searchfld.keyup(function (e) {if (e.keyCode != 9 && e.keyCode != -1) self._getSuggestions( this.value ) } );
    //    connect(this.searchfld, 'onfocus', function () { this.selectAll(); } );
        this.searchfld.attr('autocomplete','off');

        $(document).bind('click', function() { self._clearSuggestions() } );
    }
});

Polaris.AutoSuggest._CreateAutoSuggest.prototype = {

    _getSuggestions: function(val) {
        this.searchfld.addClass('as_invalid');
        this.valuefld.val('');

        if (val.length == this.nInputChars)
            return false;

        if (val.length < this.oP.minchars) {
            this.nInputChars = val.length;
            this.aSuggestions = [];
            this._clearSuggestions();
            return false;
        }

        if (val.length>this.nInputChars && this.aSuggestions.length && this.oP.cache)
        {
            // get from cache
            var arr = [];
            for (var i=0;i<this.aSuggestions.length;i++) {
                if (this.aSuggestions[i].substr(0,val.length).toLowerCase() == val.toLowerCase())
                    arr.push( this.aSuggestions[i] );
            }

            this.nInputChars = val.length;
            this.aSuggestions = arr;

            this._createList( this.aSuggestions );

            return false;
        }

        this.nInputChars = val.length;

        var self = this;
        clearTimeout(this.ajID);
        this.ajID = setTimeout( function() { self._doAjaxRequest() }, this.oP.delay );

        return false;
    },

    _doAjaxRequest: function () {
        // create ajax request

        if (this.oP.searchfields) {
            var url = this.oP.script+'?output='+this.oP.params.output+'&qcomp='+this.oP.params.qcomp;
            url += '&'+this.oP.params.varname+'='+this.searchfld.val()+'%';
            url += '&'+this.oP.params.varname+'='+this.searchfld.val().toLowerCase()+'%';
            url += '&'+this.oP.params.varname+'='+this.searchfld.val().toUpperCase()+'%';
            url += '&qc[]='+this.oP.searchfields;
        } else {
            var url = this.oP.script+'?output='+this.oP.params.output+'&qcomp='+this.oP.params.qcomp+'&'+this.oP.params.varname+'='+this.searchfld.val();
        }
        var onSuccessFunc = function (data) { self._setSuggestions(data) };
        var onErrorFunc = function (status) { alert("AJAX error: "+status); };
        var self = this;

        $.getJSON(url, onSuccessFunc);
    },

    _setSuggestions: function (req) {
        var jsondata = req; //eval('(' + req.responseText + ')');
//        this.idAs = "as_"+this.searchfld.attr('id');
        this._createList(jsondata);
    },

	_isArrayLike: function(elem) {
	    return (elem.length && elem[elem.length - 1] !== undefined && !elem.nodeType);
	},

    _createList: function(jsondata) {
        var capflds = this.oP.captionfields.split(",");
        var captionvalues = new Array();
        // clear previous list
        //
        this._clearSuggestions();
        // create and populate ul
        //
        var ul = $("<ul style='overflow-x:hidden;overflow-y:auto;z-index:900;position:absolute;'></ul>");//.attr({'id':this.idAs});
        ul.addClass(this.oP.className);
        var self = this;
        for (var i=0;i<jsondata.length;i++) {
            captionvalues = [];
            if (this._isArrayLike(capflds)) {
                for(var c=0;c<capflds.length;c++) captionvalues.push( jsondata[i][capflds[c]] );
            } else {
                captionvalues.push( jsondata[i][capflds] );
            }
            captionvalues = captionvalues.join(", ");

            var idvalue = jsondata[i][this.oP.valuefield];
            var a = $("<a>"+captionvalues+"</a>").attr({ 'href':"#", 'value':idvalue }).bind('click',self,self._setClickValue);
            var li = $("<li></li>").append(a);
            ul.append( li );
        }

        var pos = this.searchfld.offset();
        var ul_width = (this.searchfld.outerWidth() < 100)?170+"px":this.searchfld.outerWidth()+"px";
        ul.css({'left':pos.left + "px", 'top':pos.top + this.searchfld.outerHeight() + "px",'width':ul_width});//'position':'absolute','z-index':'800',
        ul.mouseover( function(){ self._killTimeout() } );
        ul.mouseout( function(){ self._resetTimeout() } );

        this.list_ul = ul;
        $("body").prepend(ul);

        if (ul.outerHeight() > this.oP.maxheight && this.oP.maxheight != 0) {
            ul.css('height', this.oP.maxheight);
        }

        var TAB = 9;
        var ESC = 27;
        var KEYUP = 38;
        var KEYDN = 40;
        var RETURN = 13;

        this.searchfld.keydown( function(ev) {
            var key = (window.event) ? window.event.keyCode : ev.keyCode;
            switch(key) {
                case TAB:
                self._setHighlightedValue();
                break;

                case ESC:
                self._clearSuggestions();
                break;

                case KEYUP:
                self._changeHighlight(key);
                return false;
                break;

                case KEYDN:
                self._changeHighlight(key);
                return false;
                break;
            }
        });

        this.iHighlighted = 0;

        // remove autosuggest after an interval
        //
        if (this.oP.timeout > 0) {
            clearTimeout(this.toID);
            var self = this;
            this.toID = setTimeout(function () { self._clearSuggestions() }, this.oP.timeout);
        }
    },

    _changeHighlight: function(key) {
        var list = this.list_ul;
        if (!list)
            return false;

        var item = list.find("li.highlight");

        if (key == 40) {
            if (item.length == 0) {
                list.find("li:first").addClass("highlight");
            } else {
                item.removeClass("highlight").next().addClass("highlight");
            }
        } else if (key = 38) {
            if (item.length == 0) {
                list.find("li:last").addClass("highlight");
            } else {
                item.removeClass("highlight");
                item.prev().addClass("highlight");
            }
        }

        this._killTimeout();
    },

    _killTimeout: function() {
        clearTimeout(this.toID);
    },

    _resetTimeout: function() {
        clearTimeout(this.toID);
        var self = this;
        this.toID = setTimeout(function () { self._clearSuggestions() }, 2000);
    },

    _clearSuggestions: function () {
        if (this.list_ul.length)
            this.list_ul.remove();
        this.searchfld.unbind('keydown');
    },

    _setHighlightedValue: function () {
        var item = this.list_ul.find("li.highlight");
        if (item.length==1) {
            this._setValue(item.text(), item.find("a").attr('value'));
            this._killTimeout();
            this._clearSuggestions();
        }
    },

    _setClickValue: function (e) {
        var self = e.data;
        self._setValue(e.target.childNodes[0].nodeValue, $(e.target).attr('value'));
//        self._resetTimeout();
            self._killTimeout();
            self._clearSuggestions();
        return false;
    },

    _setValue: function (searchval, idval) {
        this.searchfld.val(searchval);
        this.valuefld.val(idval);
        this.searchfld.removeClass('as_invalid');
    }

};

/** @id Polaris.AutoSuggest.createAutoSuggest */
Polaris.AutoSuggest.createAutoSuggest = function (val_e, search_e, options) {
    new Polaris.AutoSuggest._CreateAutoSuggest(val_e, search_e, options);
};
