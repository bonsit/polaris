/***
*
*    Polaris Window functions
*
**/
Polaris.Window = {};
Base.update(Polaris.Window, {

    /** @id Polaris.Window.maximizeForm */
    maximizeForm: function() {
        var _CompleteMaximizeForm = function() {
            if ($('#headercomplete').is(':visible')) {
                $('#toggleMaximize').attr('src', _serverroot+'/images/window_fullscreen.png');
            } else {
                $('#toggleMaximize').attr('src', _serverroot+'/images/window_nofullscreen.png');
            }
            Polaris.Window.resizeCustomForm();
        };
        $('#headercomplete').slideToggle("fast", _CompleteMaximizeForm);
    },

    /** @id Polaris.Window.resizeCustomForm */
    resizeCustomForm: function(frame) {
        if ($(frame).length) {
            const winInHt = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight;
            let delta = 0;

            const pageheader = document.querySelector('#pageheader');
            if (pageheader) {
                delta += pageheader.offsetHeight;
            }

            const footer = document.querySelector('.footer.fixed');
            if (footer) {
                delta += footer.offsetHeight;
            }

            $(frame).height(winInHt - delta);
        }
    },

    /** @id Polaris.Window.openCenteredWindow */
    openCenteredWindow: function(url, height, width, name, parms) {
        if (!height) { height = screen.height * 0.75; }
        if (!width) { width = screen.width * 0.75; }
        var left = Math.floor( (screen.width - width) / 2);
        var top = Math.floor( (screen.height - height) / 2);
        var winParms = "top=" + top + ",left=" + left + ",height=" + height + ",width=" + width;
        if (parms) { winParms += "," + parms; }
        var win = window.open(url, name, winParms);
        if (parseInt(navigator.appVersion, 10) >= 4) { win.window.focus(); }
        return win;
    },

    /** @id Polaris.Window.openMaxApp */
    openMaxApp: function(e, name) {
        return Polaris.Window.openCenteredWindow(e.href, screen.height*0.75, screen.width*0.75, name, 'status=yes,toolbar=yes,menubar=yes,location=yes');
    },

    /** @id Polaris.Window.showModuleForm */
    showModuleForm: function(form, onaccept, onshow) {
        var processButtons = function (hash) {
            $(".jqmAccept").on('click', function() {
                result = true;
                if (onaccept) {
                    $_allExceptCheckboxes = $("#ajaxdialog :input:not([type='checkbox'])");
                    var serializedObj = $_allExceptCheckboxes.serializeArray();

                    $_onlyCheckboxes = $("#ajaxdialog input:checkbox");
                    var serializedCheckboxes = $_onlyCheckboxes.map(function() {
                        if (this.checked)
                            return { name: this.name, value: this.checked ? this.value : "false" };
                    });

                    // merge the two arrays
                    serializedObj = $.merge(serializedObj, serializedCheckboxes);
                    result = onaccept(serializedObj);
                }
                if (typeof result == 'undefined' || result)
                    $("#ajaxdialog").jqmHide();
            });

            Polaris.Form.setFocusFirstField(form);
            if (onshow) onshow(hash);
        }
        var ajaxlink = null;
        if (typeof form == 'string') {
            if (form.indexOf('http://') == 0 && form.indexOf('https://') == 0) {
                ajaxlink = _serverroot + form;
            } else {
                ajaxlink = form;
            }
            form = $("#ajaxdialog");
            form.jqm({ajax:ajaxlink, modal: true, onLoad: processButtons}).jqmShow();
        } else {
            form.jqm({ajax:ajaxlink, modal: true, onShow: processButtons}).jqmShow();
        }
    },

});

