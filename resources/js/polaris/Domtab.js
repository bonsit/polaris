/* 
 * Domtab.js 
 * version 1.0
 * Based on domTab written by Chris Heilmann (http://www.onlinetools.org/tools/domtab.php)
*/

if (typeof(Polaris.Domtab) == "undefined") {
    Polaris.Domtab = {};
}

Polaris.Domtab.NAME = "Polaris.Domtab";
Polaris.Domtab.VERSION = "1.0";

try {
    if (typeof(Polaris.Base) === 'undefined') {
        throw "";
    }
} catch (e) {
    throw "Polaris.Domtab depends on Polaris.Base";
};

Base.update(Polaris.Domtab, {
    // Global variables
    currentTab: '', 
    currentLink: '',

    // Change if you want to use another class for highlighting
    tabHighlightClass: 'tabon',

    /** @id Polaris.Domtab.initPageTabs */
    initPageTabs: function() {
        var self = Polaris.Domtab;
        var navElement = '#pagetabs';
        var navElementTabbedId = 'pagetabstabbed';
    
        // pattern to check against to identify "back to menu" links
        var backToMenu = /#top/;
        if (document.getElementById && document.createTextNode) {
            var currentid = window.location.toString().match(/#(\w.+)/);
            if (currentid && currentid[1])
                currentid = currentid[1];
            var n = $(navElement);
            if (!n) return;

            n.attr('id', navElementTabbedId);
            n = $('#'+navElementTabbedId);
    
            var as = $("a", n);
            as.click( function(){ self.showTab(this); return false } );
            
            for (i=0;i<as.length;i++) {

                var id = as[i].href.match(/#(\w.+)/)[1];
                if (!currentid && i==0) {
                    self.currentTab=id;
                    self.currentLink = as[i];
                } else if (id==currentid) {
                    self.currentTab=id;
                    self.currentLink = as[i];
                }

                if ($('#'+id)) {
                    linklength = $("a",id).length;
                    if (linklength > 0) {
                        lastlink = $("a:last",id);
                        if (backToMenu.test(lastlink.href)) {
                            lastlink.remove();
                        }
                    }
                    $('#'+id).hide();
                }
            }		

            if ($('#'+self.currentTab)) $('#'+self.currentTab).show();
            
            $('#tab_'+self.currentTab).addClass(self.tabHighlightClass);


            $(document).keydown( function( e ) { 
                if( e.ctrlKey && e.keyCode == 219 /* KEY_LEFT_SQUARE_BRACKET */ ){ 
                    self.showPrevTab();
                };
                if( e.ctrlKey && e.keyCode == 221 /* KEY_RIGHT_SQUARE_BRACKET */ ){ 
                    self.showNextTab();
                } 
            }); 
        }
    },

    showTab: function(o) {
        var self = Polaris.Domtab;

        if (self.currentTab) {
            if ($('#'+self.currentTab)) {
                $('#'+self.currentTab).hide();
            }
            $('#tab_'+self.currentTab).removeClass(self.tabHighlightClass);
        }

        var id = $(o).attr('href').toString();
        id = id.match(/#(\w.+)/)[1];
        self.currentTab = id;
        self.currentLink = o;
        if ($('#'+id))	{
            $('#'+id).show();
            Polaris.Form.adjustLabelWidth(id);
        }
        $('#tab_'+self.currentTab).addClass(self.tabHighlightClass);
    },

    showNextTab: function(o) {
        var self = Polaris.Domtab;
        var nextTab = $('#tab_'+self.currentTab).parent().next("li").find("a");
//        if (nextTab.length == 0) nextTab = $('#tab_'+self.currentTab).parent().first("li").find("a");
        self.showTab(nextTab);
    },

    showPrevTab: function(o) {
        var self = Polaris.Domtab;
        var prevTab = $('#tab_'+self.currentTab).parent().prev("li").find("a");
//        if (prevTab.length == 0) prevTab = $('#tab_'+self.currentTab).parent().parent().last("li").find("a");
        self.showTab(prevTab);
    }
});
