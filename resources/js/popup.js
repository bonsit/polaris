// the functions in this file require the supplementary library lib.js

// These defaults should be changed the way it best fits your site
var _POPUP_FEATURES = 'width=640,height=480,scrollbars,resizable,toolbar,status,menubar,location';

var sUserAgent = navigator.userAgent.toLowerCase();
var isIE = document.all?true:false;
var isNS4 = document.layers?true:false;
var isOp = (sUserAgent.indexOf('opera')!=-1)?true:false;
var isMoz = (sUserAgent.indexOf('mozilla/5')!=-1 && sUserAgent.indexOf('opera')==-1 && sUserAgent.indexOf('msie')==-1)?true:false;

function raw_popup(url, target, features) {
    // pops up a window containing url optionally named target, optionally having features
    if (isUndefined(features)) features = _POPUP_FEATURES;
    if (isUndefined(target  )) target   = '_blank';
    var theWindow = window.open(url, target, features);
		// An Opera bug returns too early if you focus the window, so we don't focus it in that browser.
		// Only a noticable defect (in that browser) if a window is already open and hidden behind another window.
		if (theWindow && !isOp) theWindow.focus();
    return (theWindow)?false:true;
}

function link_popup(src, features) {
    // to be used in an html event handler as in: <a href="..." onclick="link_popup(this,...)" ...
    // pops up a window grabbing the url from the event source's href
    return raw_popup(src.getAttribute('href'), src.getAttribute('target') || '_blank', features);
}
