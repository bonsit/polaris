module.exports = function(grunt) {
    var environment = grunt.option('environment') || 'development';
    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        watch: {
            scripts: {
                files: ['resources/**/*.js', 'resources/**/*.css', 'Gruntfile.js'],
                tasks: ['uglify', 'cssmin','copy:main'],
                options: {
                    spawn: false,
                },
            },
        },
        shell: {
            build_docs: {
                command: 'mkdocs build --config-file mkdocs.yml'
            },
            build_docs_enduser: {
                command: 'mkdocs build --config-file mkdocs_enduser.yml'
            }
        },
        cssmin: {
            target: {
                files: [{
                    expand: false,
                    src: [
                        'node_modules/bootstrap/dist/css/bootstrap.css',
                        'node_modules/bootstrap3-dialog/src/css/bootstrap-dialog.css',
                        'node_modules/animate.css/animate.css',
                        'node_modules/bootstrap-markdown/css/bootstrap-markdown.min.css',
                        'node_modules/icheck/skins/square/_all.css',
                        'node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.css',
                        // 'node_modules/metismenu/dist/metisMenu.css',
                        'node_modules/select2/dist/css/select2.css',
                        'node_modules/timepicker/jquery.timepicker.css',
                        'node_modules/enjoyhint/enjoyhint.css',
                        'node_modules/smartwizard/dist/css/smart_wizard_all.css',
                        'node_modules/toastr/build/toastr.css',
                        'resources/js/jquery-markitup/sets/markdown/style.css',
                        'resources/js/jquery-markitup/skins/markitup/style.css',
                        'resources/css/titatoggle.css',
                        'resources/css/font-financial.css',
                        'resources/css/font-line-icons.css',
                        // 'resources/css/font-opensans.css',
                        // 'resources/css/font-roboto.css',
                        'resources/css/font-breeserif.css',
                        'resources/css/font-atkinson.css',
                        'resources/css/style-2.9.2.css',
                        'resources/css/style-custom.css',
                        'resources/css/plrdefaultdark.css',
                        'node_modules/intro.js/introjs.css',
                    ],
                    dest: environment === 'production' ? 'assets/dist/main.min.css' : 'public/assets/dist/main.min.css',
                    ext: '.min.css'
                }]
            }
        },
        uglify: {
            options: {
                compress: false,
                mangle : false
            },
            applib: {
                src: [
                    'resources/js/jquery-scripts/jquery.jsperanto.js',
                    'resources/js/jquery-scripts/jquery.colorPicker.js',
                    'resources/js/jquery-validate-1.11.1/jquery.validate.js',
                    'resources/js/jquery-validate-1.11.1/localization/messages_nl.js',
                    'node_modules/dirrty/dist/jquery.dirrty.js',
                    'node_modules/icheck/icheck.js',
                    'node_modules/toastr/toastr.js',
                    'node_modules/block-ui/jquery.blockUI.js',
                    'node_modules/marked/marked.min.js',
                    'node_modules/bootstrap/dist/js/bootstrap.js',
                    'node_modules/bootstrap3-dialog/src/js/bootstrap-dialog.js',
                    'node_modules/bootstrap-markdown/js/bootstrap-markdown.js',
                    'node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.js',
                    'node_modules/bootstrap-datepicker/dist/locales/bootstrap-datepicker.nl.min.js',
                    'node_modules/sparkline/dist/jquery.sparkline.js',
                    'node_modules/select2/dist/js/select2.js',
                    'node_modules/jquery-slimscroll/jquery.slimscroll.js',
                    'node_modules/kineticjs/kinetic.js',
                    'node_modules/jquery.scrollto/jquery.scrollTo.js',
                    'node_modules/enjoyhint/enjoyhint.js',
                    'node_modules/smartwizard/dist/js/jquery.smartWizard.js',
                    'node_modules/datepair.js/dist/datepair.js',
                    'node_modules/datepair.js/dist/jquery.datepair.js',
                    'node_modules/timepicker/jquery.timepicker.js',
                    'node_modules/jsviews/jsviews.js',
                    'node_modules/blowup/lib/blowup.js',
                    'resources/js/jquery-markitup/jquery.markitup.js',
                    'resources/js/jquery-markitup/sets/markdown/set.js',
                    'resources/js/nestable/jquery.nestable.js',
                    'resources/js/jquery-scripts/jquery.keycode.js',
                    'resources/js/jquery-scripts/KeyTable.js',
                    'resources/js/jquery-scripts/jquery.meio.mask.js',
                    'resources/js/jquery-scripts/jquery.autocomplete2.js',
                    'resources/js/jquery-scripts/jquery.editable.select.js',
                    'resources/js/jquery-scripts/jquery.jeditable-1.7.2.js',
                    'resources/js/jquery-scripts/jquery.cookies.2.1.0.js',
                    // 'resources/js/jquery-scripts/jquery.idTabs.js',
                    // 'resources/js/jquery-scripts/jquery.menu.js',
                    'resources/js/jquery-scripts/jquery.metadata.js',
                    'resources/js/jquery-scripts/jqModal.js',
                    'resources/js/jquery-scripts/jquery.ezpztooltip.js',
                    'resources/js/jquery-scripts/jquery.bestupper.min.js',
                    'resources/js/jquery-greybox/greybox.js',
                    'resources/js/jquery-scripts/autoNumeric-1.9.8.js',
                    'resources/js/flot/jquery.pie.js',
                    'resources/js/flot/jquery.flot.js',
                    'resources/js/flot/jquery.flot.time.js',
                    'resources/js/flot/jquery.flot.tooltip.min.js',
                    'resources/js/flot/jquery.flot.spline.js',
                    'resources/js/flot/jquery.flot.resize.js',
                    'resources/js/flot/jquery.flot.resize.js',
                    'resources/js/fullcalendar-scheduler-6.1.5/dist/index.global.js',
                    'node_modules/luxon/build/global/luxon.js',
                    'node_modules/intro.js/intro.js',

                    'resources/js/inspinia.js',
                    'resources/js/pure/pure2.js',
                    'resources/js/polaris/Base.js',
                    'resources/js/polaris/Components.js',
                    // 'resources/js/polaris/Livesearch.js',
                    'resources/js/polaris/Wizard.js',
                    'resources/js/polaris/Form.js',
                    'resources/js/polaris/Window.js',
                    'resources/js/polaris/Onboarding.js',
                    'resources/js/polaris/Timer.js',
                    'resources/js/polaris/Visual.js',
                    'resources/js/polaris/Ajax.js',
                    'resources/js/polaris/Dataview.js',
                    'resources/js/polaris/Initialize.js',
                ],
                dest: environment === 'production' ? 'assets/dist/main.js' : 'public/assets/dist/main.js'
            },
            modelerlib: {
                src: [
                    'resources/js/polaris/Modeler.js',
                    'node_modules/showdown/dist/showdown.js',
                    'node_modules/raphael/raphael.js',
                    'node_modules/jquery-svg/jquery-svg.js'
                ],
                dest: environment === 'production' ? 'assets/dist/modeler.js' : 'public/assets/dist/modeler.js'
            }
        },
        copy: {
            main: {
                files: [
                    // includes files within path
                    {
                        expand: true,
                        flatten: true,
                        cwd: 'node_modules/icheck/skins/square/',
                        src: '**',
                        dest: environment === 'production' ? 'assets/dist/' : 'public/assets/dist/',
                        filter: 'isFile'
                    },
                    {
                        expand: true,
                        flatten: true,
                        cwd: 'node_modules/jquery.svg/',
                        src: '*.css',
                        dest: environment === 'production' ? 'assets/css/' : 'public/assets/css/',
                        filter: 'isFile'
                    },
                    {
                        expand: true,
                        flatten: true,
                        cwd: 'node_modules/enjoyhint/src/Casino_Hand',
                        src: '**',
                        dest: environment === 'production' ? 'assets/dist/src/Casino_Hand/' : 'public/assets/dist/src/Casino_Hand/',
                        filter: 'isFile'
                    },
                    {
                        expand: true,
                        flatten: false,
                        cwd: 'resources/fonts/',
                        src: '**',
                        dest: environment === 'production' ? 'assets/css/' : 'public/assets/css/',
                        filter: 'isFile'
                    },
                    {
                        expand: true,
                        flatten: false,
                        cwd: 'node_modules/jquery/dist',
                        src: '**',
                        dest: environment === 'production' ? 'assets/js/jquery/dist' : 'public/assets/js/jquery/dist',
                        filter: 'isFile'
                    },
                    {
                        expand: true,
                        flatten: false,
                        cwd: 'node_modules/chart.js/dist',
                        src: '**',
                        dest: environment === 'production' ? 'assets/chart.js/dist' : 'public/assets/js/chart.js/dist',
                        filter: 'isFile'
                    },
                    {
                        expand: true,
                        flatten: false,
                        cwd: 'resources/js/dropzone',
                        src: ['dropzone-min.js', 'dropzone.css'],
                        dest: environment === 'production' ? 'assets/dist' : 'public/assets/dist',
                        filter: 'isFile'
                    },
                    {
                        expand: true,
                        flatten: false,
                        cwd: 'resources/js/',
                        src: [
                            'md5.js',
                            'summernote/summernote.min.js',
                            'summernote/lang/summernote-nl-NL.min.js',
                            'summernote/summernote.min.css',
                            'summernote/font/**',
                        ],
                        dest: environment === 'production' ? 'assets/dist' : 'public/assets/dist',
                        filter: 'isFile'
                    },
                    {
                        expand: true,
                        flatten: false,
                        cwd: 'resources/js/fullcalendar-rrule-plugin',
                        src: ['index.global.js'],
                        dest: environment === 'production' ? 'assets/dist' : 'public/assets/dist',
                        filter: 'isFile'
                    },
                    {
                        expand: true,
                        flatten: false,
                        cwd: 'node_modules/rrule/dist/es5',
                        src: ['rrule.min.js','rrule.min.js.map'],
                        dest: environment === 'production' ? 'assets/dist' : 'public/assets/dist',
                        filter: 'isFile'
                    },
                    {
                        expand: true,
                        flatten: false,
                        cwd: 'node_modules/sortablejs',
                        src: ['Sortable.min.js'],
                        dest: environment === 'production' ? 'assets/dist' : 'public/assets/dist',
                        filter: 'isFile'
                    },
                    {
                        expand: true,
                        flatten: false,
                        cwd: 'node_modules/minimasonry/build',
                        src: ['minimasonry.min.js'],
                        dest: environment === 'production' ? 'assets/dist' : 'public/assets/dist',
                        filter: 'isFile'
                    },
                    {
                        expand: true,
                        flatten: false,
                        cwd: 'resources/images',
                        src: '**',
                        dest: environment === 'production' ? 'assets/images' : 'public/assets/images',
                        filter: 'isFile'
                    },
                    {
                        expand: true,
                        flatten: false,
                        cwd: 'resources/favicons/maxia',
                        src: '**',
                        dest: environment === 'production' ? '' : 'public/',
                        filter: 'isFile'
                    }
                ],
            },
            php_customization: {
                files: [
                    {
                        expand: true,
                        flatten: true,
                        cwd: 'resources/php/customization/smarty/libs/plugins/',
                        src: '**',
                        dest: environment === 'production' ? 'vendor/smarty/smarty/libs/plugins/' : 'public/vendor/smarty/smarty/libs/plugins/',
                        filter: 'isFile'
                    },
                    {
                        expand: true,
                        flatten: true,
                        cwd: 'resources/php/customization/adodb/',
                        src: '**',
                        dest: environment === 'production' ? 'vendor/adodb/adodb-php/' : 'public/vendor/adodb/adodb-php/',
                        filter: 'isFile'
                    },
                    {
                        expand: true,
                        flatten: true,
                        cwd: 'resources/php/customization/phpjasper/drivers',
                        src: '**',
                        dest: environment === 'production' ? 'vendor/geekcom/phpjasper/bin/jasperstarter/jdbc' : 'public/vendor/geekcom/phpjasper/bin/jasperstarter/jdbc',
                        filter: 'isFile'
                    },
                ]
            }
        },
    });
    // Default task.
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-shell');
    grunt.registerTask('default', ['uglify', 'cssmin', 'shell', 'copy']);
    grunt.registerTask('php_customization', ['copy:php_customization']);
};