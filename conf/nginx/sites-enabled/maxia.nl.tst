server {
    index index.php index.html;
    server_name maxia.nl;

    listen 443 ssl http2;

    ssl_certificate     /etc/pki/tls/certs/tst.maxia.nl.pem;
    ssl_certificate_key /etc/pki/tls/certs/tst.maxia.nl.key;
    ssl_trusted_certificate /etc/pki/tls/certs/cf_origin.pem;

    error_log  /var/log/nginx/error.log;
    access_log /var/log/nginx/access.log;
    root /var/www/html;

    location / {
        # Include rewrite rules specific to this location
        include /etc/nginx/rewrite_rules.conf;
    }

    location ~ \.php$ {
        try_files $uri =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass php-fpm:9000;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        #fastcgi_param SCRIPT_FILENAME $fastcgi_script_name;
        fastcgi_param  SCRIPT_FILENAME $request_filename;
        fastcgi_param PATH_INFO $fastcgi_path_info;
        include fastcgi_params;
    }
}