<?php

/***
    Here you can set the different Polaris instances (databases)
    that Polaris can connect to.

    This is practical when you want to use different PLR databases
    with only one source code.
**/
$_PLRINSTANCE[0]['name'] = 'Local Polaris';
$_PLRINSTANCE[0]['dbtype'] = 'mysqli';
$_PLRINSTANCE[0]['host'] = 'db';
$_PLRINSTANCE[0]['port'] = 3306;
$_PLRINSTANCE[0]['database'] = 'admin_polaris5'; // just 3 apps
$_PLRINSTANCE[0]['username'] = 'polaris';
$_PLRINSTANCE[0]['password'] = $_ENV['DB_POLARIS_PASSWORD'];

$_CONFIG['currentinstance'] = 0;

//$_CONFIG['polarisname'] = 'Virtuele backoffice';

// MySQL date formatting
$_CONFIG['default_date_format'] = '%d-%m-%Y';
$_CONFIG['currentdatetimeformat'] = '%e %B %Y';
$_CONFIG['default_php_date_format'] = 'd-m-Y';

/***
    Here you can override the default Polaris settings (see includes/configuration_default.inc.php)
**/
//$_CONFIG['currentdatetimeformat'] = '%d %b %Y &nbsp;&bull;&nbsp; #week# %V  &nbsp;&bull;&nbsp;  #day# %j';


// $_CONFIG['brand'] = '_default';
// $_CONFIG['brand'] = 'dark';
// $_CONFIG['brand'] = 'datamundi';
// $_CONFIG['brand'] = 'werkvoorheerlen';
// $_CONFIG['brand'] = 'bovengrondsevakschool';
// $_CONFIG['brand'] = 'focusfriends';
// $_CONFIG['brand'] = 'ideebv';
// $_CONFIG['brand'] = 'kmberekening';
// $_CONFIG['brand'] = 'aertes';
// $_CONFIG['brand'] = 'maxia';
$_CONFIG['brand'] = 'porthos';
// $_CONFIG['brand'] = 'oogopzorg.net';
// $_CONFIG['brand'] = 'pna';
// $_CONFIG['brand'] = 'pnagroup';
// $_CONFIG['brand'] = 'saas';
// $_CONFIG['brand'] = 'saas2';
// $_CONFIG['brand'] = 'steeltrace';
// $_CONFIG['brand'] = 'vandort';


$_CONFIG['default_record_limit'] = 50;
$_CONFIG["reportserverurl"] = "http://localhost:8080/jasperserver/flow.html?_flowId=viewReportFlow&output=%REPORTOUTPUT%&reportUnit=%REPORTNAME%&%REPORTPARAMS%&j_username=jasperadmin&j_password=jasperadmin";
$_CONFIG['displaytimers'] = true;
$_CONFIG['multicolumnsort'] = false;
$_CONFIG['floatdecimalchar'] = '.';

$_CONFIG['LDAP.account_prefix'] = "nha\\";
$_CONFIG['LDAP.account_suffix'] = "";
$_CONFIG['LDAP.base_dn'] = "";
$_CONFIG['LDAP.domain_controllers'] = array ("172.16.1.1");
$_CONFIG['usergrouphandler_type'] = 'PLR'; /* { PLR | LDAP} */

$_CONFIG['showajaxdetails'] = false;
$_CONFIG['debug'] = false;

$_CONFIG['LAST_ACTIVITY_TIMEOUT'] = 60 * 60 * 4; // 4 hours

/**
 * If set to false, no memcaching is used.
 */
$_CONFIG['usememcached'] = true;
$_CONFIG['memcached_servers'] = array('memcached');
$_CONFIG['memcached_port'] = 11211;

/**
 * Color of the top header background
 */
// $_CONFIG['headercolor'] = '#304B79';

/**
 * nieuwe IBAN acceptgiro gebruiken (SEPA)?
 */
$_CONFIG['gebruikibanaccept'] = TRUE;

/**
 * Attention grabber (wordt altijd in de header getoond)
 */
$_CONFIG['attentiongrabber'] = 'Ontwikkelomgeving';

/**
 * How many days before the password expires?
 * No expiration if set to 0, because in WvH the admin issues a new password
 */
$_CONFIG['passwordexpiry'] = 0;


/**
 * Polaris Alerter en Replyer SMTP settings
 */
$_CONFIG['smtpserver'] = 'smtp.gmail.com';
$_CONFIG['smtpserver_port'] = 465;
$_CONFIG['smtpserver_encryption'] = 'ssl';
$_CONFIG['smtpserver_username'] = 'bartbons@gmail.com';
$_CONFIG['smtpserver_password'] = '';

// ivm het printen via de CLI
$_CONFIG["polarisreporthost"] = "http://localhost/polaris-test";

$_CONFIG['showuserimage'] = true;

$_CONFIG['postmark_api_token'] = '176cb9b0-238d-4107-a1f1-9aaa0f1647a8'; // generic support@polarisnow.com
// NEW WvH API $_CONFIG['postmark_api_token'] = 'a9f4f9a1-f463-4a0c-8512-ab797593c452';
$_CONFIG['postmark_api_token.vandortletselschade.nl'] = 'f1cdb40a-71d0-4b47-a1bb-18a2f0cb9eb8';
$_CONFIG['postmark_api_token.bedrijfsongeval.nl'] = '749b1310-c0f5-4bf5-ba5c-72de2a98cb80';
$_CONFIG['postmark_api_token.letsel.nl'] = '1d2cf4a4-fa49-4e6a-9645-1968c6592bfe';
$_CONFIG['postmark_api_token.bonsit.nl'] = 'cae4f10d-7973-42c5-a78c-adc56edd4c05';
$_CONFIG['postmark_api_token.bonsit.dev'] = 'cae4f10d-7973-42c5-a78c-adc56edd4c05';
$_CONFIG['postmark_api_token.rijschoolloomans.nl'] = 'feaf34ed-e3f9-47e5-8d59-5f9c478e3ae8';
$_CONFIG['postmark_api_token.innovatieversnellers.eu'] = '349f4398-133f-4175-bc4d-8b0fc3f24a3a';
$_CONFIG['postmark_api_token.werkvoorheerlen.maxia.local'] = '2008f1be-f849-4221-9665-9c0cb1387cbd';
$_CONFIG['postmark_api_token.werkvoorheerlen.maxia.nl'] = '2008f1be-f849-4221-9665-9c0cb1387cbd';
$_CONFIG['postmark_api_token.maxia.nl'] = '2008f1be-f849-4221-9665-9c0cb1387cbd';

$_CONFIG['alerter_sendmaximum'] = 300;
$_CONFIG['alerter_fromaddress'] = 'no-reply@maxia.nl';

$_CONFIG['servicedeskurl'] = 'https://bonsit.atlassian.net/servicedesk/';

$_CONFIG['2fa_provider'] = ''; // spryng, email, ...[ empty to disable ]
$_CONFIG['2fa_debug'] = false;

$_CONFIG['spryng_username'] = '';
$_CONFIG['spryng_password'] = '';
$_CONFIG['spryng_companyname'] = '';

$_CONFIG['postmark_webhook_user'] = 'postmarkhook';
$_CONFIG['postmark_webhook_password'] = $_ENV['POSTMARK_WEBHOOK_PASSWORD'];
