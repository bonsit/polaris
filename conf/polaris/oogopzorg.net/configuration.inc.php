<?php

/***
    Here you can set the different Polaris instances (databases)
    that Polaris can connect to.

    This is practical when you want to use different PLR databases
    with only one PLR instance.
**/
$_PLRINSTANCE[0]['name'] = 'Local Polaris';
$_PLRINSTANCE[0]['dbtype'] = 'mysql';
$_PLRINSTANCE[0]['host'] = 'db';
$_PLRINSTANCE[0]['port'] = 3306;
$_PLRINSTANCE[0]['database'] = 'admin_polaris5';
$_PLRINSTANCE[0]['username'] = 'root';
$_PLRINSTANCE[0]['password'] = 'HgJVPJMboaRVx6r';

$_CONFIG['currentinstance'] = 0;

/***
    Here you can override the default Polaris settings (see conf/configuration_default.inc.php)
**/
$_CONFIG['polarisname'] = 'PZN';
$_CONFIG['brand'] = '%domainname%';
$_CONFIG['brand'] = 'maxia.nl';
$_CONFIG['default_record_limit'] = 50;
$_CONFIG['multicolumnsort'] = false;
$_CONFIG['showuserimage'] = true;
$_CONFIG['debug'] = true;
//$_CONFIG['attentiongrabber'] = 'Testomgeving';

/**
 * Set the smarty generated files folder
 */
$_CONFIG['cachequeries_folder'] = '/tmp/polaris/cache';
$_CONFIG['smarty_template_folder'] = '/tmp/polaris/smarty';

/**
 * Postmark token
 */
//$_CONFIG['postmark_api_token'] = '176cb9b0-238d-4107-a1f1-9aaa0f1647a8'; // generic support@polarisnow.com
$_CONFIG['postmark_api_token'] = 'b52bea84-1ce0-4835-a41e-d69cc67d50b9'; // generic development oogopzorg
$_CONFIG['postmark_api_token.www.oogopzorg.net'] = 'b52bea84-1ce0-4835-a41e-d69cc67d50b9';

$_CONFIG['alerter_sendmaximum'] = 3;
$_CONFIG['alerter_fromaddress'] = 'secretariaat@aertes.com';

$_CONFIG['2fa_provider'] = 'spryng'; // spryng, email, ...[ empty to disable ]
$_CONFIG['2fa_debug'] = false;

$_CONFIG['spryng_username'] = 'pzn-oogopzorg';
$_CONFIG['spryng_password'] = 'BHE-Moh-4aY-CPp';
$_CONFIG['spryng_companyname'] = 'OogOpZorg';

