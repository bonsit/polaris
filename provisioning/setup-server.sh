yum install nano hg -y

yum install -y yum-utils \
  device-mapper-persistent-data \
  lvm2

yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo

yum install docker-ce -y
yum install hg -y

yum install -y epel-release
yum install -y python-pip
pip install --upgrade pip
pip install docker-compose
yum upgrade python*

yum install nginx -y

systemctl enable docker
systemctl start docker
systemctl enable nginx
systemctl start nginx


adduser service
adduser service sudo
passwd service
usermod -aG wheel service
usermod -aG docker service
echo "
AllowUsers root service" >> /etc/ssh/sshd_config

systemctl restart sshd



## on own computer
ssh-copy-id -i ~/.ssh/id_rsa service@pzn-vps-test 

## Certificates

sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/nginx-selfsigned.key -out /etc/ssl/certs/nginx-selfsigned.crt
sudo openssl dhparam -out /etc/ssl/certs/dhparam.pem 2048
