How to setup a Polaris server

### Install Ansible

1. brew install ansible
2. ansible-galaxy install nginxinc.nginx_config
3. ansible-galaxy install geerlingguy.certbot

cd <polaris>/provisioning

ansible-playbook -i inventory polariscontainerhost.yml -u bit00001
