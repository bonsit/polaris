#!/bin/bash
read -r -p "Are you sure you want to import the database dump? [y/N] " response
if [[ "$response" =~ ^([yY][eE][sS]|[yY])+$ ]]
then
    docker-compose exec -i db mysql --login-path=local < ./data/backup/plr_dump.sql
fi
