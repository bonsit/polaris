# ************************************************************
# Sequel Ace SQL dump
# Version 20050
#
# https://sequel-ace.com/
# https://github.com/Sequel-Ace/Sequel-Ace
#
# Host: 128.140.62.144 (MySQL 5.5.5-10.6.11-MariaDB-1:10.6.11+maria~ubu2004)
# Database: admin_maxia
# Generation Time: 2023-08-14 18:21:57 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
SET NAMES utf8mb4;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE='NO_AUTO_VALUE_ON_ZERO', SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table cp_bestuurder
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cp_bestuurder`;

CREATE TABLE `cp_bestuurder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_hash` varchar(32) NOT NULL,
  `voornaam` varchar(255) NOT NULL,
  `achternaam` varchar(255) NOT NULL,
  `telefoonnummer` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `adres` varchar(255) NOT NULL,
  `rijbewijsnummer` varchar(255) NOT NULL,
  `rijbewijsverloopdatum` date NOT NULL,
  `datum_indiensttreding` date NOT NULL,
  `datum_beëindiging` date DEFAULT NULL,
  `noodcontact_naam` varchar(255) NOT NULL,
  `noodcontact_telefoonnummer` varchar(255) NOT NULL,
  `bestuurder_select` varchar(70) GENERATED ALWAYS AS (concat(`voornaam`,' ',`achternaam`)) STORED,
  `__DELETED` int(1) DEFAULT 0,
  `__CHANGEDON` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `__CREATEDON` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

LOCK TABLES `cp_bestuurder` WRITE;
/*!40000 ALTER TABLE `cp_bestuurder` DISABLE KEYS */;

INSERT INTO `cp_bestuurder` (`id`, `client_hash`, `voornaam`, `achternaam`, `telefoonnummer`, `email`, `adres`, `rijbewijsnummer`, `rijbewijsverloopdatum`, `datum_indiensttreding`, `datum_beëindiging`, `noodcontact_naam`, `noodcontact_telefoonnummer`, `__DELETED`, `__CHANGEDON`, `__CREATEDON`)
VALUES
	(1,'547ce4f12bdd425641109c98e9a89acf','Kim','Van der Naald','12345678','kjavandernaald@hotmail.com','idhsf','12345678','2023-06-19','2023-06-01',NULL,'Bart','123456789',0,NULL,'2023-08-06 15:20:55');

/*!40000 ALTER TABLE `cp_bestuurder` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table cp_brandstof
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cp_brandstof`;

CREATE TABLE `cp_brandstof` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_hash` varchar(32) NOT NULL,
  `voertuig_id` int(11) NOT NULL,
  `bestuurder_id` int(11) NOT NULL,
  `datum_tijd` datetime NOT NULL,
  `kilometertellerstand` int(11) NOT NULL,
  `brandstof_type` varchar(255) NOT NULL,
  `liters` float NOT NULL,
  `kosten` float NOT NULL,
  `locatie` varchar(255) NOT NULL,
  `notities` varchar(255) DEFAULT NULL,
  `__DELETED` int(1) DEFAULT 0,
  `__CHANGEDON` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `__CREATEDON` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `voertuig_id` (`voertuig_id`),
  KEY `bestuurder_id` (`bestuurder_id`),
  CONSTRAINT `cp_brandstof_ibfk_1` FOREIGN KEY (`voertuig_id`) REFERENCES `cp_voertuig` (`id`),
  CONSTRAINT `cp_brandstof_ibfk_2` FOREIGN KEY (`bestuurder_id`) REFERENCES `cp_bestuurder` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

LOCK TABLES `cp_brandstof` WRITE;
/*!40000 ALTER TABLE `cp_brandstof` DISABLE KEYS */;

INSERT INTO `cp_brandstof` (`id`, `client_hash`, `voertuig_id`, `bestuurder_id`, `datum_tijd`, `kilometertellerstand`, `brandstof_type`, `liters`, `kosten`, `locatie`, `notities`, `__DELETED`, `__CHANGEDON`, `__CREATEDON`)
VALUES
	(1,'547ce4f12bdd425641109c98e9a89acf',1,1,'2023-06-07 00:00:00',20100,'Diesel',40,90,'?',NULL,0,NULL,'2023-08-06 15:20:55');

/*!40000 ALTER TABLE `cp_brandstof` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table cp_onderhoud
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cp_onderhoud`;

CREATE TABLE `cp_onderhoud` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_hash` varchar(32) NOT NULL,
  `voertuig_id` int(11) NOT NULL,
  `bestuurder_id` int(11) NOT NULL,
  `datum_tijd` datetime NOT NULL,
  `type` varchar(255) NOT NULL,
  `omschrijving` varchar(255) NOT NULL,
  `kosten` float NOT NULL,
  `locatie` varchar(255) NOT NULL,
  `notities` varchar(255) DEFAULT NULL,
  `__DELETED` int(1) DEFAULT 0,
  `__CHANGEDON` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `__CREATEDON` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `voertuig_id` (`voertuig_id`),
  KEY `bestuurder_id` (`bestuurder_id`),
  CONSTRAINT `cp_onderhoud_ibfk_1` FOREIGN KEY (`voertuig_id`) REFERENCES `cp_voertuig` (`id`),
  CONSTRAINT `cp_onderhoud_ibfk_2` FOREIGN KEY (`bestuurder_id`) REFERENCES `cp_bestuurder` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;



# Dump of table cp_voertuig
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cp_voertuig`;

CREATE TABLE `cp_voertuig` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_hash` varchar(32) NOT NULL,
  `naam` varchar(10) NOT NULL,
  `type` varchar(255) NOT NULL,
  `merk` varchar(30) NOT NULL,
  `model` varchar(20) NOT NULL,
  `bouwjaar` int(11) NOT NULL,
  `vin` varchar(20) NOT NULL,
  `kenteken` varchar(10) NOT NULL,
  `huidige_kilometerstand` int(11) NOT NULL,
  `voertuig_select` varchar(60) GENERATED ALWAYS AS (concat(`merk`,' ',`model`,' (',`kenteken`,')')) STORED,
  `__DELETED` int(1) DEFAULT 0,
  `__CHANGEDON` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `__CREATEDON` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

LOCK TABLES `cp_voertuig` WRITE;
/*!40000 ALTER TABLE `cp_voertuig` DISABLE KEYS */;

INSERT INTO `cp_voertuig` (`id`, `client_hash`, `naam`, `type`, `merk`, `model`, `bouwjaar`, `vin`, `kenteken`, `huidige_kilometerstand`, `__DELETED`, `__CHANGEDON`, `__CREATEDON`)
VALUES
	(1,'547ce4f12bdd425641109c98e9a89acf','WVH001','Pickup','Ford','Transit',2006,'123456789','NL-WH-01',20000,0,NULL,'2023-08-06 15:20:55');

/*!40000 ALTER TABLE `cp_voertuig` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table fb_aanvragen
# ------------------------------------------------------------

DROP TABLE IF EXISTS `fb_aanvragen`;

CREATE TABLE `fb_aanvragen` (
  `aanvraag_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_hash` varchar(32) NOT NULL,
  `aanvrager_naam` varchar(50) DEFAULT NULL,
  `gewenste_datum` date DEFAULT NULL,
  `gewenste_tijd` varchar(20) DEFAULT NULL,
  `aanvraag_omschrijving` varchar(200) DEFAULT NULL,
  `contactpersoon` varchar(100) DEFAULT NULL,
  `contactpersoon_email` varchar(100) DEFAULT NULL,
  `contactpersoon_telefoon` varchar(20) DEFAULT NULL,
  `bedrijfsnaam` varchar(200) DEFAULT NULL,
  `soort_ruimte` varchar(3) DEFAULT NULL,
  `aantal_personen` varchar(3) DEFAULT NULL,
  `soort_lunch` varchar(100) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `aanvraagdatum` timestamp NULL DEFAULT current_timestamp(),
  `opmerking` varchar(500) DEFAULT NULL,
  `geinformeerdop` date DEFAULT NULL,
  `sendto` varchar(100) DEFAULT NULL,
  `datetimesend` datetime DEFAULT NULL,
  `ipaddress` varchar(64) DEFAULT NULL,
  `transmissionok` varchar(10) DEFAULT NULL,
  `errormessage` varchar(255) DEFAULT NULL,
  `__DELETED` int(1) DEFAULT 0,
  `__CHANGEDON` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `__CREATEDON` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`aanvraag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

LOCK TABLES `fb_aanvragen` WRITE;
/*!40000 ALTER TABLE `fb_aanvragen` DISABLE KEYS */;

INSERT INTO `fb_aanvragen` (`aanvraag_id`, `client_hash`, `aanvrager_naam`, `gewenste_datum`, `gewenste_tijd`, `aanvraag_omschrijving`, `contactpersoon`, `contactpersoon_email`, `contactpersoon_telefoon`, `bedrijfsnaam`, `soort_ruimte`, `aantal_personen`, `soort_lunch`, `status`, `aanvraagdatum`, `opmerking`, `geinformeerdop`, `sendto`, `datetimesend`, `ipaddress`, `transmissionok`, `errormessage`, `__DELETED`, `__CHANGEDON`, `__CREATEDON`)
VALUES
	(4,'547ce4f12bdd425641109c98e9a89acf','Bart','2023-04-27','middag','Vergadering','Bart','bart.bons@werkvoorheerlen.nl','0624959159','WvH','6','8',NULL,NULL,'2023-04-25 09:56:57',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,'2023-08-06 18:02:24'),
	(5,'547ce4f12bdd425641109c98e9a89acf',NULL,'2023-05-12','heledag',NULL,'Bart','bart@hijgendhert.nl','06-24959159','Restaurant Het Hijgend Hert','17','6','Luxe',NULL,'2023-04-25 10:06:54','Alleen vega',NULL,'bart@bonsit.nl','2023-04-25 12:06:00','172.20.0.1','N','',0,NULL,'2023-08-06 18:02:24'),
	(6,'547ce4f12bdd425641109c98e9a89acf',NULL,'2023-05-17','middag',NULL,'Bart','bart@hema.nl','06-24959159','HEMA','60','2','Luxe',NULL,'2023-04-25 10:21:12','We gaan rond 14.00 wandelen',NULL,'bart@bonsit.nl','2023-04-25 12:21:00','172.20.0.1','N','',0,NULL,'2023-08-06 18:02:24'),
	(7,'547ce4f12bdd425641109c98e9a89acf',NULL,'2023-05-15','heledag',NULL,'Bart','bart@h.nl','06-24959159','conferentieX','17','6','Luxe',NULL,'2023-04-25 14:36:15','allergie',NULL,'bart@bonsit.nl','2023-04-25 16:36:00','172.20.0.1','N','',0,NULL,'2023-08-06 18:02:24');

/*!40000 ALTER TABLE `fb_aanvragen` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table fb_categorien
# ------------------------------------------------------------

DROP TABLE IF EXISTS `fb_categorien`;

CREATE TABLE `fb_categorien` (
  `categorie_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_hash` varchar(32) NOT NULL,
  `categorie_naam` varchar(50) DEFAULT NULL,
  `__DELETED` int(1) DEFAULT 0,
  `__CHANGEDON` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `__CREATEDON` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`categorie_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

LOCK TABLES `fb_categorien` WRITE;
/*!40000 ALTER TABLE `fb_categorien` DISABLE KEYS */;

INSERT INTO `fb_categorien` (`categorie_id`, `client_hash`, `categorie_naam`, `__DELETED`, `__CHANGEDON`, `__CREATEDON`)
VALUES
	(5,'547ce4f12bdd425641109c98e9a89acf','Internet',0,NULL,'2023-08-06 15:20:55'),
	(6,'547ce4f12bdd425641109c98e9a89acf','Kantoormeubels',0,NULL,'2023-08-06 15:20:55'),
	(7,'547ce4f12bdd425641109c98e9a89acf','Ruimte',0,NULL,'2023-08-06 15:20:55'),
	(8,'817238638476823742','Andere klant',0,NULL,'2023-08-06 15:20:55'),
	(9,'547ce4f12bdd425641109c98e9a89acf','Gebouw buiten',0,NULL,'2023-08-06 15:20:55'),
	(10,'547ce4f12bdd425641109c98e9a89acf','Keukenapparatuur',0,NULL,'2023-08-06 15:20:55'),
	(11,'547ce4f12bdd425641109c98e9a89acf','Algemeen',0,NULL,'2023-08-06 15:20:55');

/*!40000 ALTER TABLE `fb_categorien` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table fb_geplandonderhoud
# ------------------------------------------------------------

DROP TABLE IF EXISTS `fb_geplandonderhoud`;

CREATE TABLE `fb_geplandonderhoud` (
  `onderhoud_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_hash` varchar(32) NOT NULL,
  `aanvrager_naam` varchar(50) DEFAULT NULL,
  `soort_onderhoud` int(11) DEFAULT NULL,
  `categorie` int(11) DEFAULT NULL,
  `impact` int(11) DEFAULT NULL,
  `ruimte` int(11) DEFAULT NULL,
  `ontwikkelsfeer` int(11) DEFAULT NULL,
  `onderhoud_omschrijving` varchar(200) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `duur` int(11) DEFAULT NULL,
  `beschrijving` varchar(500) DEFAULT NULL,
  `oplossing` varchar(500) DEFAULT NULL,
  `schema_type` char(1) DEFAULT NULL,
  `schema_dagelijks_start_datum` date DEFAULT NULL,
  `schema_week_herhaling` varchar(10) DEFAULT NULL,
  `schema_maand_herhaling` varchar(10) DEFAULT NULL,
  `schema_maand_start_dag` varchar(10) DEFAULT NULL,
  `schema_periodiek` int(3) DEFAULT NULL,
  `schema_eenmalig_start_datum` date DEFAULT NULL,
  `schema_start_tijd` varchar(10) DEFAULT NULL,
  `melding_tijd` varchar(10) DEFAULT NULL,
  `aantal_kandidaten` int(3) DEFAULT NULL,
  `prettyplanning` varchar(255) DEFAULT NULL,
  `__DELETED` int(1) DEFAULT 0,
  `__CHANGEDON` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `__CREATEDON` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`onderhoud_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

LOCK TABLES `fb_geplandonderhoud` WRITE;
/*!40000 ALTER TABLE `fb_geplandonderhoud` DISABLE KEYS */;

INSERT INTO `fb_geplandonderhoud` (`onderhoud_id`, `client_hash`, `aanvrager_naam`, `soort_onderhoud`, `categorie`, `impact`, `ruimte`, `ontwikkelsfeer`, `onderhoud_omschrijving`, `status`, `duur`, `beschrijving`, `oplossing`, `schema_type`, `schema_dagelijks_start_datum`, `schema_week_herhaling`, `schema_maand_herhaling`, `schema_maand_start_dag`, `schema_periodiek`, `schema_eenmalig_start_datum`, `schema_start_tijd`, `melding_tijd`, `aantal_kandidaten`, `prettyplanning`, `__DELETED`, `__CHANGEDON`, `__CREATEDON`)
VALUES
	(3,'547ce4f12bdd425641109c98e9a89acf','Bart',5,9,7,NULL,11,'Ramen controleren','Actief',NULL,NULL,NULL,'E',NULL,NULL,NULL,NULL,NULL,'2023-05-12','19:32',NULL,2,'Op 12-05-2023 om 19:32',0,NULL,'2023-08-06 15:20:55'),
	(4,'547ce4f12bdd425641109c98e9a89acf','Rene',6,10,7,7,17,'Koffiekannen ontkalken','Actief',30,NULL,NULL,'D','2023-06-05',NULL,NULL,NULL,NULL,NULL,'15:02',NULL,6,'Dagelijks om 15:02 vanaf 05-06-2023',0,NULL,'2023-08-06 15:20:55'),
	(6,'547ce4f12bdd425641109c98e9a89acf','Bart',5,10,7,7,17,'Kastjes binnenkant','Actief',12,'Grote schoonmaak van de keuken.\r\n\r\nStappen:\r\n\r\n1. Kastjes binnenkant\r\n2. RVS werk\r\n3. Gootsteen\r\n',NULL,'M',NULL,NULL,'4095','3',NULL,NULL,'13:02',NULL,3,'Elke 3e dag in jan, feb, mrt, apr, mei, jun, jul, aug, sep, okt, nov, dec om 13:02',0,NULL,'2023-08-06 15:20:55'),
	(7,'547ce4f12bdd425641109c98e9a89acf','wvh_admin',7,9,5,NULL,12,'Sloten controleren','Actief',50,NULL,NULL,'M',NULL,'111','3','5',NULL,NULL,'16:30',NULL,4,'Fout in planning',0,NULL,'2023-08-06 15:20:55'),
	(8,'547ce4f12bdd425641109c98e9a89acf','wvh_admin',10,9,5,NULL,12,'BBBB','Actief',NULL,'gevel',NULL,'E',NULL,NULL,NULL,NULL,NULL,'2023-05-12','15:36',NULL,4,'Op 12-05-2023 om 15:36',0,NULL,'2023-08-06 15:20:55'),
	(11,'547ce4f12bdd425641109c98e9a89acf','wvh_admin',7,9,5,NULL,12,'Ramen controleren ','Actief',NULL,NULL,NULL,'W',NULL,'17',NULL,NULL,NULL,NULL,'16:17',NULL,4,'Elke ma, vr om 16:17',0,NULL,'2023-08-06 15:20:55'),
	(12,'547ce4f12bdd425641109c98e9a89acf','Rene',6,7,NULL,16,17,'Toiletten schoonmaken','Actief',15,'Schoonmaken toiletten\r\n',NULL,'D','2023-06-05',NULL,NULL,NULL,NULL,NULL,'15:02',NULL,2,'Dagelijks om 15:02 vanaf 05-06-2023',0,NULL,'2023-08-06 15:20:55'),
	(13,'547ce4f12bdd425641109c98e9a89acf','wvh_admin',7,7,NULL,6,13,'Vloer boenen','Actief',60,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'15:02',NULL,3,'Fout in planning',0,NULL,'2023-08-06 15:20:55'),
	(14,'547ce4f12bdd425641109c98e9a89acf','wvh_admin',9,11,NULL,NULL,13,'Vervangen van posters','Actief',500,NULL,NULL,'W',NULL,'2',NULL,NULL,NULL,NULL,'15:02',NULL,2,NULL,0,NULL,'2023-08-06 15:20:55');

/*!40000 ALTER TABLE `fb_geplandonderhoud` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table fb_impacts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `fb_impacts`;

CREATE TABLE `fb_impacts` (
  `impact_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_hash` varchar(32) NOT NULL,
  `impact_naam` varchar(50) DEFAULT NULL,
  `__DELETED` int(1) DEFAULT 0,
  `__CHANGEDON` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `__CREATEDON` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`impact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

LOCK TABLES `fb_impacts` WRITE;
/*!40000 ALTER TABLE `fb_impacts` DISABLE KEYS */;

INSERT INTO `fb_impacts` (`impact_id`, `client_hash`, `impact_naam`, `__DELETED`, `__CHANGEDON`, `__CREATEDON`)
VALUES
	(5,'547ce4f12bdd425641109c98e9a89acf','Bedrijf',0,NULL,'2023-08-06 15:20:55'),
	(6,'547ce4f12bdd425641109c98e9a89acf','Afdeling',0,NULL,'2023-08-06 15:20:55'),
	(7,'547ce4f12bdd425641109c98e9a89acf','Groep',0,NULL,'2023-08-06 15:20:55'),
	(8,'547ce4f12bdd425641109c98e9a89acf','Gebruiker',0,NULL,'2023-08-06 15:20:55');

/*!40000 ALTER TABLE `fb_impacts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table fb_locaties
# ------------------------------------------------------------

DROP TABLE IF EXISTS `fb_locaties`;

CREATE TABLE `fb_locaties` (
  `locatie_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_hash` varchar(32) NOT NULL,
  `locatie_naam` varchar(50) DEFAULT NULL,
  `locatie_code` varchar(20) DEFAULT NULL,
  `locatie_omschrijving` varchar(500) DEFAULT NULL,
  `foto` longblob DEFAULT NULL,
  `continue_bezet` char(1) DEFAULT NULL,
  `bezetting` int(11) DEFAULT NULL,
  `__DELETED` int(1) DEFAULT 0,
  `__CHANGEDON` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `__CREATEDON` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`locatie_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

LOCK TABLES `fb_locaties` WRITE;
/*!40000 ALTER TABLE `fb_locaties` DISABLE KEYS */;

INSERT INTO `fb_locaties` (`locatie_id`, `client_hash`, `locatie_naam`, `locatie_code`, `locatie_omschrijving`, `foto`, `continue_bezet`, `bezetting`, `__DELETED`, `__CHANGEDON`, `__CREATEDON`)
VALUES
	(1,'547ce4f12bdd425641109c98e9a89acf','Bovengrondse vakschool','BGR','Heerlen Diepenbrockstraat',NULL,'N',74,0,NULL,'2023-08-06 15:20:55'),
	(2,'547ce4f12bdd425641109c98e9a89acf','Heerlen Noord | Passartweg','HNR','Heerlen Noord',NULL,NULL,NULL,0,NULL,'2023-08-06 15:20:55'),
	(3,'1','Beekerweg','BKR','Beekerweg BONS IT',NULL,NULL,NULL,0,NULL,'2023-08-06 15:20:55');

/*!40000 ALTER TABLE `fb_locaties` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table fb_meldingen
# ------------------------------------------------------------

DROP TABLE IF EXISTS `fb_meldingen`;

CREATE TABLE `fb_meldingen` (
  `melding_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_hash` varchar(32) NOT NULL,
  `aanvrager_naam` varchar(50) DEFAULT NULL,
  `soort_melding` varchar(10) DEFAULT NULL,
  `ruimte` int(11) DEFAULT NULL,
  `melding_datum` date DEFAULT NULL,
  `melding_tijd` varchar(10) DEFAULT NULL,
  `melding_omschrijving` varchar(200) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `oplossing` varchar(200) DEFAULT NULL,
  `__DELETED` int(1) DEFAULT 0,
  `__CHANGEDON` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `__CREATEDON` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`melding_id`),
  KEY `ruimte` (`ruimte`),
  CONSTRAINT `fb_meldingen_ibfk_1` FOREIGN KEY (`ruimte`) REFERENCES `fb_ruimtes` (`ruimte_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

LOCK TABLES `fb_meldingen` WRITE;
/*!40000 ALTER TABLE `fb_meldingen` DISABLE KEYS */;

INSERT INTO `fb_meldingen` (`melding_id`, `client_hash`, `aanvrager_naam`, `soort_melding`, `ruimte`, `melding_datum`, `melding_tijd`, `melding_omschrijving`, `status`, `oplossing`, `__DELETED`, `__CHANGEDON`, `__CREATEDON`)
VALUES
	(1,'547ce4f12bdd425641109c98e9a89acf','Rene','Storing',7,'2023-04-11','14:04:48','Lamp is kapot','Nieuw','Lamp vervangen',0,NULL,'2023-08-06 15:20:56'),
	(2,'547ce4f12bdd425641109c98e9a89acf','René','Andere...',5,'2023-04-11','15:24:49','Zonwering toevoegen','Afgehandeld','test2345',0,NULL,'2023-08-06 15:20:56'),
	(3,'547ce4f12bdd425641109c98e9a89acf','Bart','Defect',71,'2023-04-23','23:14:11','Deurkruk hangt los','In behandeling',NULL,0,NULL,'2023-08-06 15:20:56');

/*!40000 ALTER TABLE `fb_meldingen` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table fb_ontwikkelsferen
# ------------------------------------------------------------

DROP TABLE IF EXISTS `fb_ontwikkelsferen`;

CREATE TABLE `fb_ontwikkelsferen` (
  `ontwikkelsfeer_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_hash` varchar(32) NOT NULL,
  `ontwikkelsfeer_naam` varchar(100) DEFAULT NULL,
  `actief` char(1) DEFAULT 'J',
  `__DELETED` int(1) DEFAULT 0,
  `__CHANGEDON` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `__CREATEDON` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`ontwikkelsfeer_id`),
  UNIQUE KEY `naam` (`client_hash`,`ontwikkelsfeer_naam`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

LOCK TABLES `fb_ontwikkelsferen` WRITE;
/*!40000 ALTER TABLE `fb_ontwikkelsferen` DISABLE KEYS */;

INSERT INTO `fb_ontwikkelsferen` (`ontwikkelsfeer_id`, `client_hash`, `ontwikkelsfeer_naam`, `actief`, `__DELETED`, `__CHANGEDON`, `__CREATEDON`)
VALUES
	(4,'123','Andere client sfeer','Y',0,NULL,'2023-08-06 15:20:56'),
	(10,'547ce4f12bdd425641109c98e9a89acf','Vakwerk | Groen','Y',0,NULL,'2023-08-06 15:20:56'),
	(11,'547ce4f12bdd425641109c98e9a89acf','Vakwerk | Techniek','Y',0,NULL,'2023-08-06 15:20:56'),
	(12,'547ce4f12bdd425641109c98e9a89acf','Vakwerk | Bouw','Y',0,NULL,'2023-08-06 15:20:56'),
	(13,'547ce4f12bdd425641109c98e9a89acf','Dienstwerk | Logistiek','Y',0,NULL,'2023-08-06 15:20:56'),
	(14,'547ce4f12bdd425641109c98e9a89acf','Dienstwerk | Toezicht','Y',0,NULL,'2023-08-06 15:20:56'),
	(15,'547ce4f12bdd425641109c98e9a89acf','Dienstwerk | Zakelijke dienstverlening','Y',0,NULL,'2023-08-06 15:20:56'),
	(16,'547ce4f12bdd425641109c98e9a89acf','Zorgwerk | Horeca','Y',0,NULL,'2023-08-06 15:20:56'),
	(17,'547ce4f12bdd425641109c98e9a89acf','Zorgwerk | Schoonmaak','Y',0,NULL,'2023-08-06 15:20:56'),
	(18,'547ce4f12bdd425641109c98e9a89acf','Zorgwerk | Persoonlijke verzorging','Y',0,NULL,'2023-08-06 15:20:56');

/*!40000 ALTER TABLE `fb_ontwikkelsferen` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table fb_parameters
# ------------------------------------------------------------

DROP TABLE IF EXISTS `fb_parameters`;

CREATE TABLE `fb_parameters` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `client_hash` varchar(32) NOT NULL,
  `parameternaam` varchar(100) DEFAULT NULL,
  `omschrijving` varchar(500) DEFAULT NULL,
  `datatype` char(1) DEFAULT NULL,
  `waarde` varchar(100) DEFAULT NULL,
  `wijzigbaar` char(1) DEFAULT NULL,
  `helptekst` text DEFAULT NULL,
  `__DELETED` int(1) DEFAULT 0,
  `__CHANGEDON` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `__CREATEDON` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

LOCK TABLES `fb_parameters` WRITE;
/*!40000 ALTER TABLE `fb_parameters` DISABLE KEYS */;

INSERT INTO `fb_parameters` (`id`, `client_hash`, `parameternaam`, `omschrijving`, `datatype`, `waarde`, `wijzigbaar`, `helptekst`, `__DELETED`, `__CHANGEDON`, `__CREATEDON`)
VALUES
	(1,'547ce4f12bdd425641109c98e9a89acf','Gebruikersbeheer via LDAP connectie','Met deze optie kunt u aangeven of Maxia een connectie maakt met een LDAP server om gebruikers te verifiëren.','B','Y','N',NULL,0,NULL,'2023-07-28 22:03:55'),
	(2,'1234','Andere klant','Enable IPv6 support and gateway.','B','N','Y',NULL,0,NULL,'2023-07-28 22:03:55'),
	(3,'547ce4f12bdd425641109c98e9a89acf','Een andere parameter test','Iets over deze parameter','B','Y','N',NULL,0,NULL,'2023-07-28 22:03:55');

/*!40000 ALTER TABLE `fb_parameters` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table fb_quickactions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `fb_quickactions`;

CREATE TABLE `fb_quickactions` (
  `quickaction_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_hash` varchar(32) NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `icoon` varchar(50) DEFAULT NULL,
  `omschrijving` varchar(150) DEFAULT NULL,
  `volgorde` int(3) DEFAULT NULL,
  `kleur` varchar(20) DEFAULT NULL,
  `__DELETED` int(1) DEFAULT 0,
  `__CHANGEDON` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `__CREATEDON` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`quickaction_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

LOCK TABLES `fb_quickactions` WRITE;
/*!40000 ALTER TABLE `fb_quickactions` DISABLE KEYS */;

INSERT INTO `fb_quickactions` (`quickaction_id`, `client_hash`, `url`, `icoon`, `omschrijving`, `volgorde`, `kleur`, `__DELETED`, `__CHANGEDON`, `__CREATEDON`)
VALUES
	(8,'547ce4f12bdd425641109c98e9a89acf','/app/facility/const/int_aanvragen/insert/','fa-calendar-check','Reserveer een ruimte',10,'#1bb394',0,'2023-08-11 14:26:43','2023-07-28 22:03:55'),
	(9,'547ce4f12bdd425641109c98e9a89acf','/app/facility/const/geplandonderhoud/insert/','fa-calendar','Plan een onderhoud',30,'#1d84c6',0,'2023-08-11 14:26:43','2023-07-28 22:03:55'),
	(10,'547ce4f12bdd425641109c98e9a89acf','/app/facility/const/meldingenwizard/insert/','fa-triangle-exclamation','Meld een storing',40,'#ed5565',0,'2023-08-05 22:04:02','2023-07-28 22:03:55'),
	(11,'547ce4f12bdd425641109c98e9a89acf','#','fa-person-digging','Zoek een werkinstructie',50,'#24c6c8',0,'2023-08-05 22:04:02','2023-07-28 22:03:55'),
	(12,'1234','#','fa-calendar-check','Andere klant',1,NULL,0,NULL,'2023-07-28 22:03:55'),
	(13,'547ce4f12bdd425641109c98e9a89acf','/app/facility/const/int_aanvragen/insert/?type=flexplek','fa-shuffle','Reserveer een flexplek',20,'#f8ac58',0,'2023-08-11 14:26:43','2023-07-28 22:03:55'),
	(15,'547ce4f12bdd425641109c98e9a89acf','asdasdasd','fa-person-digging','Zoek een werkinstructie',40,'#1bb394',1,'2023-08-03 16:55:09','2023-07-28 22:03:55');

/*!40000 ALTER TABLE `fb_quickactions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table fb_reserveringen
# ------------------------------------------------------------

DROP TABLE IF EXISTS `fb_reserveringen`;

CREATE TABLE `fb_reserveringen` (
  `reservering_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_hash` varchar(32) NOT NULL,
  `reserverings_naam` varchar(50) DEFAULT NULL,
  `reservering_datum` date DEFAULT NULL,
  `reservering_tijd` time DEFAULT NULL,
  `reservering_omschrijving` varchar(200) DEFAULT NULL,
  `contactpersoon` varchar(100) DEFAULT NULL,
  `bedrijfsnaam` varchar(200) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `ruimte` int(11) DEFAULT NULL,
  `__DELETED` int(1) DEFAULT 0,
  `__CHANGEDON` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `__CREATEDON` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`reservering_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;



# Dump of table fb_reserveringen_uitrusting
# ------------------------------------------------------------

DROP TABLE IF EXISTS `fb_reserveringen_uitrusting`;

CREATE TABLE `fb_reserveringen_uitrusting` (
  `reserveringen_uitrusting_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_hash` varchar(32) NOT NULL,
  `reservering_id` int(11) DEFAULT NULL,
  `uitrusting_id` int(11) DEFAULT NULL,
  `__DELETED` int(1) DEFAULT 0,
  `__CHANGEDON` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `__CREATEDON` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`reserveringen_uitrusting_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;



# Dump of table fb_ruimte_uitrusting
# ------------------------------------------------------------

DROP TABLE IF EXISTS `fb_ruimte_uitrusting`;

CREATE TABLE `fb_ruimte_uitrusting` (
  `ruimteuitrusting_id` int(11) NOT NULL AUTO_INCREMENT,
  `ruimte_id` int(11) NOT NULL,
  `client_hash` varchar(32) NOT NULL,
  `uitrusting_id` int(11) NOT NULL,
  `__DELETED` int(1) DEFAULT 0,
  `__CHANGEDON` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `__CREATEDON` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`ruimteuitrusting_id`),
  UNIQUE KEY `ruimte_uitrusting` (`ruimte_id`,`uitrusting_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

LOCK TABLES `fb_ruimte_uitrusting` WRITE;
/*!40000 ALTER TABLE `fb_ruimte_uitrusting` DISABLE KEYS */;

INSERT INTO `fb_ruimte_uitrusting` (`ruimteuitrusting_id`, `ruimte_id`, `client_hash`, `uitrusting_id`, `__DELETED`, `__CHANGEDON`, `__CREATEDON`)
VALUES
	(1,1,'1234',65,0,NULL,'2023-08-06 15:21:01'),
	(2,5,'547ce4f12bdd425641109c98e9a89acf',61,0,NULL,'2023-08-06 15:21:01'),
	(5,80,'547ce4f12bdd425641109c98e9a89acf',62,0,NULL,'2023-08-06 15:21:01'),
	(6,80,'547ce4f12bdd425641109c98e9a89acf',64,0,NULL,'2023-08-06 15:21:01'),
	(17,5,'547ce4f12bdd425641109c98e9a89acf',66,0,NULL,'2023-08-06 15:21:01'),
	(18,5,'547ce4f12bdd425641109c98e9a89acf',64,0,NULL,'2023-08-06 15:21:01'),
	(20,6,'547ce4f12bdd425641109c98e9a89acf',4,0,NULL,'2023-08-06 15:21:01'),
	(21,6,'547ce4f12bdd425641109c98e9a89acf',2,0,NULL,'2023-08-06 15:21:01');

/*!40000 ALTER TABLE `fb_ruimte_uitrusting` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table fb_ruimtes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `fb_ruimtes`;

CREATE TABLE `fb_ruimtes` (
  `ruimte_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_hash` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `ruimte_code` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `ruimte_naam` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `ruimte_omschrijving` varchar(500) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `beschikbaarvoorverhuur` char(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `bezetting` int(11) DEFAULT NULL,
  `max_capaciteit` int(11) DEFAULT NULL,
  `oppervlakte` float DEFAULT NULL,
  `etage` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `locatie` int(11) DEFAULT NULL,
  `ruimte_select` varchar(70) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci GENERATED ALWAYS AS (concat(`ruimte_code`,' ',`ruimte_naam`)) STORED,
  `foto` longblob DEFAULT NULL,
  `aantalflexplekken` int(11) DEFAULT 0,
  `__DELETED` int(1) DEFAULT 0,
  `__CHANGEDON` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `__CREATEDON` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`ruimte_id`),
  UNIQUE KEY `ruimtecode` (`client_hash`,`ruimte_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

LOCK TABLES `fb_ruimtes` WRITE;
/*!40000 ALTER TABLE `fb_ruimtes` DISABLE KEYS */;

INSERT INTO `fb_ruimtes` (`ruimte_id`, `client_hash`, `ruimte_code`, `ruimte_naam`, `ruimte_omschrijving`, `beschikbaarvoorverhuur`, `bezetting`, `max_capaciteit`, `oppervlakte`, `etage`, `locatie`, `foto`, `aantalflexplekken`, `__DELETED`, `__CHANGEDON`, `__CREATEDON`)
VALUES
	(1,'1234','99.99','ANDERE KLANT',NULL,'N',NULL,0,0,'ET1',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(4,'547ce4f12bdd425641109c98e9a89acf','1.18','Vergaderruimte',NULL,'Y',NULL,18,37,'ET1',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(5,'547ce4f12bdd425641109c98e9a89acf','0.03','Kantoor','WSP','N',NULL,8,85,'BEG',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(6,'547ce4f12bdd425641109c98e9a89acf','0.02.1','Standaard werkplek/vergader','Arend Zwaga','N',NULL,1,NULL,'BEG',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(7,'547ce4f12bdd425641109c98e9a89acf','0.02','Keuken','Keuken','N',NULL,6,NULL,'BEG',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(8,'547ce4f12bdd425641109c98e9a89acf','0.01','Kantine','Kantine','N',NULL,50,263,'BEG',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(11,'547ce4f12bdd425641109c98e9a89acf','0.15','Duale poort leslokaal ',NULL,'N',NULL,12,50,'BEG',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(12,'547ce4f12bdd425641109c98e9a89acf','0.16','MijnSpoor','Lisa | Sharon','N',NULL,12,50,'BEG',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(13,'547ce4f12bdd425641109c98e9a89acf','0.17','WSP / Beheer',NULL,'N',NULL,8,49,'BEG',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(14,'547ce4f12bdd425641109c98e9a89acf','0.42','Trappenhuis',NULL,'N',NULL,0,NULL,'BEG',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(15,'547ce4f12bdd425641109c98e9a89acf','0.45','Gang',NULL,'N',NULL,0,NULL,'BEG',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(16,'547ce4f12bdd425641109c98e9a89acf','0.41 ','Toiletten',NULL,'N',NULL,0,NULL,'BEG',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(17,'547ce4f12bdd425641109c98e9a89acf','0.14','Open Spreekkamers',NULL,'Y',NULL,NULL,NULL,'BEG',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(18,'547ce4f12bdd425641109c98e9a89acf','0.12','Team Beheer','Erik|Jannie|Dirk|Diederik|Daphne|Simone|Stephanie|Hussein|Jos| Bart W','N',NULL,10,120,'BEG',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(19,'547ce4f12bdd425641109c98e9a89acf','0.10','Alcander',NULL,'N',NULL,NULL,30,'BEG',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(20,'547ce4f12bdd425641109c98e9a89acf','0.09','Berging Hoofdingang',NULL,'N',NULL,NULL,7,'BEG',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(21,'547ce4f12bdd425641109c98e9a89acf','0.04','SK1',NULL,'N',NULL,0,1.4,'BEG',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(22,'547ce4f12bdd425641109c98e9a89acf','0.05','SK2',NULL,'N',1,NULL,NULL,'BEG',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(23,'547ce4f12bdd425641109c98e9a89acf','0.06','SK3',NULL,'N',7,NULL,NULL,'BEG',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(24,'547ce4f12bdd425641109c98e9a89acf','0.07','SK4',NULL,'N',NULL,NULL,NULL,'BEG',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(25,'547ce4f12bdd425641109c98e9a89acf','0.08','SK5',NULL,'N',NULL,NULL,NULL,'BEG',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(26,'547ce4f12bdd425641109c98e9a89acf','0.44','Trappenhuis',NULL,'N',NULL,NULL,NULL,'BEG',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(27,'547ce4f12bdd425641109c98e9a89acf','0.43','Toiletten',NULL,'N',NULL,NULL,NULL,'BEG',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(28,'547ce4f12bdd425641109c98e9a89acf','0.11','Receptie',NULL,'N',NULL,NULL,13,'BEG',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(29,'547ce4f12bdd425641109c98e9a89acf','0.18','Berging/ Opslag',NULL,'N',NULL,0,8,'BEG',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(30,'547ce4f12bdd425641109c98e9a89acf','0.19','Berging/ Opslag',NULL,'N',NULL,0,8,'BEG',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(31,'547ce4f12bdd425641109c98e9a89acf','0.20','Kantoor','Arthur','N',NULL,1,8,'BEG',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(32,'547ce4f12bdd425641109c98e9a89acf','0.21','Kantoor','Arthur','N',NULL,1,8,'BEG',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(33,'547ce4f12bdd425641109c98e9a89acf','0.24','Kantoor','Roland | Larissa ','N',NULL,2,15,'BEG',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(34,'547ce4f12bdd425641109c98e9a89acf','0.25','Printer',NULL,'N',NULL,0,12,'BEG',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(35,'547ce4f12bdd425641109c98e9a89acf','0.26','Kantoor','Niels | Twan ','N',NULL,2,12,'BEG',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(36,'547ce4f12bdd425641109c98e9a89acf','0.27','Kantoor','Judith | Melanie ','N',NULL,2,18,'BEG',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(37,'547ce4f12bdd425641109c98e9a89acf','0.29','Server Ruimte',NULL,'N',NULL,0,0,'BEG',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(38,'547ce4f12bdd425641109c98e9a89acf','0.30','Opslag',NULL,'N',NULL,NULL,18,'BEG',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(39,'547ce4f12bdd425641109c98e9a89acf','0.28','LOGISTIEK Machine Uitgifte',NULL,'N',NULL,NULL,69,'BEG',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(40,'547ce4f12bdd425641109c98e9a89acf','0.23','LOGISTIEK Centraal Magazijn',NULL,'N',NULL,NULL,60,'BEG',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(41,'547ce4f12bdd425641109c98e9a89acf','0.22','LOGISTIEK Kleding Magazijn',NULL,'N',NULL,NULL,52,'BEG',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(42,'547ce4f12bdd425641109c98e9a89acf','0.48','Trappenhuis',NULL,'N',NULL,NULL,NULL,'BEG',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(43,'547ce4f12bdd425641109c98e9a89acf','0.35','Fiets Werkplaats',NULL,'N',NULL,NULL,72,'BEG',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(44,'547ce4f12bdd425641109c98e9a89acf','0.34','LOGISTIEK Fiets Magazijn ',NULL,'N',NULL,NULL,45,'BEG',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(45,'547ce4f12bdd425641109c98e9a89acf','0.31','Opslag',NULL,'N',NULL,NULL,NULL,'BEG',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(46,'547ce4f12bdd425641109c98e9a89acf','0.32','Opslag ',NULL,'N',NULL,NULL,NULL,'BEG',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(47,'547ce4f12bdd425641109c98e9a89acf','0.33','MIVA',NULL,'N',NULL,NULL,NULL,'BEG',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(48,'547ce4f12bdd425641109c98e9a89acf','0.36','GROEN Werkplaats',NULL,'N',NULL,NULL,60,'BEG',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(49,'547ce4f12bdd425641109c98e9a89acf','0.49','Gang',NULL,'N',NULL,NULL,NULL,'BEG',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(50,'547ce4f12bdd425641109c98e9a89acf','0.37','Kantoor','Flex','N',NULL,1,12,'BEG',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(53,'547ce4f12bdd425641109c98e9a89acf','0.38','BOUW Werkplaats',NULL,'N',NULL,NULL,60,'BEG',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(54,'547ce4f12bdd425641109c98e9a89acf','0.50','Trappenhuis',NULL,'N',NULL,NULL,NULL,'BEG',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(55,'547ce4f12bdd425641109c98e9a89acf','0','Fietsenstalling','Fiets techniek','N',NULL,NULL,NULL,'BEG',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(56,'547ce4f12bdd425641109c98e9a89acf','0.47','Gang',NULL,'N',NULL,NULL,NULL,'BEG',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(57,'547ce4f12bdd425641109c98e9a89acf','0.39 ','Toiletten',NULL,'N',NULL,0,NULL,'BEG',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(58,'547ce4f12bdd425641109c98e9a89acf','0.40','Trappenhuis',NULL,'N',NULL,NULL,NULL,'BEG',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(59,'547ce4f12bdd425641109c98e9a89acf','1.01','Duaal 3.0',NULL,'Y',NULL,25,48,'ET1',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(60,'547ce4f12bdd425641109c98e9a89acf','1.02','Taalcafe',NULL,'Y',NULL,25,48,'ET1',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(61,'547ce4f12bdd425641109c98e9a89acf','1.03','Berging ',NULL,'N',NULL,0,10,'ET1',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(62,'547ce4f12bdd425641109c98e9a89acf','1.04','Leslokaal + WP','Joe','Y',NULL,25,48,'ET1',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(63,'547ce4f12bdd425641109c98e9a89acf','1.05','Leslokaal + WP','Nancy','Y',NULL,25,48,'ET1',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(64,'547ce4f12bdd425641109c98e9a89acf','1.06','Kantoor','Henri | Rene','N',NULL,2,23,'ET1',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(65,'547ce4f12bdd425641109c98e9a89acf','1.07','Kantoor','Renate | Flex','N',NULL,2,18,'ET1',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(66,'547ce4f12bdd425641109c98e9a89acf','1.08','Kantoor','Maxine | Bart S.','N',NULL,2,18,'ET1',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(67,'547ce4f12bdd425641109c98e9a89acf','1.09','Kantoor','Flex','N',NULL,2,17,'ET1',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(68,'547ce4f12bdd425641109c98e9a89acf','1.33','Toiletten',NULL,'N',NULL,0,0,'ET1',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(69,'547ce4f12bdd425641109c98e9a89acf','1.30','Gang',NULL,'N',NULL,0,0,'ET1',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(70,'547ce4f12bdd425641109c98e9a89acf','1.10','Vista Taal Plus ',NULL,'N',NULL,0,53,'ET1',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(71,'547ce4f12bdd425641109c98e9a89acf','1.11','Kantoor','Nathalie| Esther| Patrick| Michiel| Flex| Flex','N',NULL,6,58.5,'ET1',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(72,'547ce4f12bdd425641109c98e9a89acf','1.14','Kantoor','Annemie','N',NULL,1,25,'ET1',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(73,'547ce4f12bdd425641109c98e9a89acf','1.16','Vergaderruimte','Gabriella','N',NULL,NULL,47.5,'ET1',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(74,'547ce4f12bdd425641109c98e9a89acf','1.17','Vergaderruimte','Saskia','Y',NULL,NULL,24,'ET1',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(75,'547ce4f12bdd425641109c98e9a89acf','1.15','Vergaderruimte','Mandy','N',NULL,NULL,24,'ET1',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(76,'547ce4f12bdd425641109c98e9a89acf','1.13','Kantoor','Ina','N',NULL,1,12,'ET1',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(77,'547ce4f12bdd425641109c98e9a89acf','1.12','Kantoor','Flex','N',NULL,1,12,'ET1',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(78,'547ce4f12bdd425641109c98e9a89acf','1.19','Vrij',NULL,'N',NULL,NULL,24,'ET1',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(79,'547ce4f12bdd425641109c98e9a89acf','1.20','SK HR',NULL,'N',NULL,NULL,24,'ET1',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(80,'547ce4f12bdd425641109c98e9a89acf','1.22','Kantoor','Kim K | Flex','N',NULL,2,24,'ET1',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(82,'547ce4f12bdd425641109c98e9a89acf','1.24','Kantoor','Anouk| Raymond| Flex','N',NULL,3,24,'ET1',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(83,'547ce4f12bdd425641109c98e9a89acf','1.25','Kantoor','Elianne| Fietje| Marleen|Desiree| Elly','N',NULL,5,36,'ET1',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(84,'547ce4f12bdd425641109c98e9a89acf','1.27','Uitvoerders','Hugo','N',NULL,NULL,53.5,'ET1',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(85,'547ce4f12bdd425641109c98e9a89acf','1.32','Gang',NULL,'N',NULL,0,0,'ET1',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(86,'547ce4f12bdd425641109c98e9a89acf','1.28','CV',NULL,'N',NULL,0,0,'ET1',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(87,'547ce4f12bdd425641109c98e9a89acf','1.26','Archief',NULL,'N',NULL,0,17,'ET1',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(88,'547ce4f12bdd425641109c98e9a89acf','1.23','Kantoor','Geert| Evelien| Kim W','N',NULL,3,28,'ET1',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(89,'547ce4f12bdd425641109c98e9a89acf','1.21','Vergaderruimte',NULL,'N',NULL,18,36,'ET1',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(90,'547ce4f12bdd425641109c98e9a89acf','1.34','Toiletten',NULL,'N',NULL,0,0,'ET1',1,NULL,0,0,NULL,'2023-08-06 15:21:02'),
	(91,'547ce4f12bdd425641109c98e9a89acf','1.31','Gang',NULL,'N',NULL,0,0,'ET1',1,NULL,0,0,NULL,'2023-08-06 15:21:02');

/*!40000 ALTER TABLE `fb_ruimtes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table fb_soort_onderhoud
# ------------------------------------------------------------

DROP TABLE IF EXISTS `fb_soort_onderhoud`;

CREATE TABLE `fb_soort_onderhoud` (
  `soort_onderhoud_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_hash` varchar(32) NOT NULL,
  `soort_onderhoud_naam` varchar(50) DEFAULT NULL,
  `__DELETED` int(1) DEFAULT 0,
  `__CHANGEDON` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `__CREATEDON` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`soort_onderhoud_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

LOCK TABLES `fb_soort_onderhoud` WRITE;
/*!40000 ALTER TABLE `fb_soort_onderhoud` DISABLE KEYS */;

INSERT INTO `fb_soort_onderhoud` (`soort_onderhoud_id`, `client_hash`, `soort_onderhoud_naam`, `__DELETED`, `__CHANGEDON`, `__CREATEDON`)
VALUES
	(5,'547ce4f12bdd425641109c98e9a89acf','Reparatie',0,NULL,'2023-08-06 15:21:02'),
	(6,'547ce4f12bdd425641109c98e9a89acf','Schoonmaak',0,NULL,'2023-08-06 15:21:02'),
	(7,'547ce4f12bdd425641109c98e9a89acf','Controle',0,NULL,'2023-08-06 15:21:02'),
	(9,'547ce4f12bdd425641109c98e9a89acf','Vervanging',0,NULL,'2023-08-06 15:21:02'),
	(10,'547ce4f12bdd425641109c98e9a89acf','Klusje',0,NULL,'2023-08-06 15:21:02');

/*!40000 ALTER TABLE `fb_soort_onderhoud` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table fb_uitrusting
# ------------------------------------------------------------

DROP TABLE IF EXISTS `fb_uitrusting`;

CREATE TABLE `fb_uitrusting` (
  `uitrusting_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_hash` varchar(32) NOT NULL,
  `uitrusting_naam` varchar(50) DEFAULT NULL,
  `uitrusting_omschrijving` varchar(200) DEFAULT NULL,
  `uitrusting_aantal` int(11) DEFAULT NULL,
  `__DELETED` int(1) DEFAULT 0,
  `__CHANGEDON` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `__CREATEDON` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`uitrusting_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

LOCK TABLES `fb_uitrusting` WRITE;
/*!40000 ALTER TABLE `fb_uitrusting` DISABLE KEYS */;

INSERT INTO `fb_uitrusting` (`uitrusting_id`, `client_hash`, `uitrusting_naam`, `uitrusting_omschrijving`, `uitrusting_aantal`, `__DELETED`, `__CHANGEDON`, `__CREATEDON`)
VALUES
	(1,'547ce4f12bdd425641109c98e9a89acf','Beamer Sony','Beamer met HDMI en VGA aansluiting',2,0,NULL,'2023-08-06 15:21:02'),
	(2,'547ce4f12bdd425641109c98e9a89acf','WIFI','WIFI voor extern gebruik',99,0,NULL,'2023-08-06 15:21:02'),
	(3,'547ce4f12bdd425641109c98e9a89acf','Lunch Luxe','Lunch inclusief zalm broodjes, thee, koffie',99,0,NULL,'2023-08-06 15:21:02'),
	(4,'547ce4f12bdd425641109c98e9a89acf','Flipover','Flipover',5,0,NULL,'2023-08-06 15:21:02');

/*!40000 ALTER TABLE `fb_uitrusting` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table kb_artikelen
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kb_artikelen`;

CREATE TABLE `kb_artikelen` (
  `artikel_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_hash` varchar(32) NOT NULL,
  `artikelcode` varchar(25) NOT NULL,
  `titel` varchar(255) NOT NULL,
  `inhoud` text DEFAULT NULL,
  `aangemaakt_op` timestamp NOT NULL DEFAULT current_timestamp(),
  `aangepast_op` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `categorie` int(11) NOT NULL,
  `tags` varchar(500) DEFAULT NULL,
  `__DELETED` int(1) DEFAULT 0,
  `__CHANGEDON` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `__CREATEDON` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`artikel_id`),
  KEY `categorie_id` (`categorie`),
  CONSTRAINT `kb_artikelen_ibfk_1` FOREIGN KEY (`categorie`) REFERENCES `kb_categorien` (`categorie_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

LOCK TABLES `kb_artikelen` WRITE;
/*!40000 ALTER TABLE `kb_artikelen` DISABLE KEYS */;

INSERT INTO `kb_artikelen` (`artikel_id`, `client_hash`, `artikelcode`, `titel`, `inhoud`, `aangemaakt_op`, `aangepast_op`, `categorie`, `tags`, `__DELETED`, `__CHANGEDON`, `__CREATEDON`)
VALUES
	(11,'547ce4f12bdd425641109c98e9a89acf','KB1001','Plafond lamp vervangen','### Stap 1:\r\n\r\nEerste stap is...\r\n\r\n### Stap 2:\r\n\r\nEerste stap is...\r\n','2023-05-01 21:51:00',NULL,12,NULL,0,NULL,'2023-08-06 15:21:02');

/*!40000 ALTER TABLE `kb_artikelen` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table kb_categorien
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kb_categorien`;

CREATE TABLE `kb_categorien` (
  `categorie_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_hash` varchar(32) NOT NULL,
  `naam` varchar(100) DEFAULT NULL,
  `__DELETED` int(1) DEFAULT 0,
  `__CHANGEDON` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `__CREATEDON` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`categorie_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

LOCK TABLES `kb_categorien` WRITE;
/*!40000 ALTER TABLE `kb_categorien` DISABLE KEYS */;

INSERT INTO `kb_categorien` (`categorie_id`, `client_hash`, `naam`, `__DELETED`, `__CHANGEDON`, `__CREATEDON`)
VALUES
	(11,'547ce4f12bdd425641109c98e9a89acf','BINNEN',0,NULL,'2023-08-06 15:21:02'),
	(12,'547ce4f12bdd425641109c98e9a89acf','BUITEN',0,NULL,'2023-08-06 15:21:02');

/*!40000 ALTER TABLE `kb_categorien` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ur_projecten
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ur_projecten`;

CREATE TABLE `ur_projecten` (
  `project_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_hash` varchar(32) NOT NULL,
  `projectcode` varchar(25) NOT NULL,
  `projectnaam` varchar(255) NOT NULL,
  `beschrijving` varchar(500) DEFAULT NULL,
  `aangemaakt_op` timestamp NOT NULL DEFAULT current_timestamp(),
  `aangepast_op` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `__DELETED` int(1) DEFAULT 0,
  `__CHANGEDON` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `__CREATEDON` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

LOCK TABLES `ur_projecten` WRITE;
/*!40000 ALTER TABLE `ur_projecten` DISABLE KEYS */;

INSERT INTO `ur_projecten` (`project_id`, `client_hash`, `projectcode`, `projectnaam`, `beschrijving`, `aangemaakt_op`, `aangepast_op`, `__DELETED`, `__CHANGEDON`, `__CREATEDON`)
VALUES
	(11,'547ce4f12bdd425641109c98e9a89acf','PC101','Pc onderhoud','het onderhoud','2023-01-01 00:00:00','2023-05-23 20:17:17',0,NULL,'2023-08-06 15:21:02'),
	(12,'547ce4f12bdd425641109c98e9a89acf','PC202','2022222222222212','test','2023-05-02 15:09:18','2023-05-23 20:14:48',0,NULL,'2023-08-06 15:21:02');

/*!40000 ALTER TABLE `ur_projecten` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ur_registratie
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ur_registratie`;

CREATE TABLE `ur_registratie` (
  `registratie_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_hash` varchar(32) NOT NULL,
  `registratiedatum` date NOT NULL,
  `starttijd` time DEFAULT NULL,
  `eindtijd` time DEFAULT NULL,
  `timer_starttijd` datetime DEFAULT NULL,
  `timer_eindtijd` datetime DEFAULT NULL,
  `omschrijving` varchar(255) NOT NULL,
  `project_id` int(11) NOT NULL,
  `aangemaakt_op` timestamp NOT NULL DEFAULT current_timestamp(),
  `aangepast_op` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `__DELETED` int(1) DEFAULT 0,
  `__CHANGEDON` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `__CREATEDON` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`registratie_id`),
  KEY `project_id` (`project_id`),
  CONSTRAINT `ur_registratie_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `ur_projecten` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;


--
-- Dumping routines (FUNCTION) for database 'admin_maxia2'
--
DELIMITER ;;

# Dump of FUNCTION GetMonthsFromValue
# ------------------------------------------------------------

/*!50003 DROP FUNCTION IF EXISTS `GetMonthsFromValue` */;;
/*!50003 CREATE*/ /*!50020 DEFINER=`plr_clients`@`%`*/ /*!50003 FUNCTION `GetMonthsFromValue`(months INT) RETURNS varchar(255) CHARSET utf8mb3 COLLATE utf8mb3_general_ci
    DETERMINISTIC
BEGIN
  DECLARE month_names VARCHAR(255);
  DECLARE month_index INT DEFAULT 0;

  SET month_names = '';

  WHILE months > 0 AND month_index <= 12 DO
    IF months & (1 << month_index) > 0 THEN
      SET month_names = CONCAT(month_names, IF(month_names = '', '', ', '),
                               DATE_FORMAT(
                                 DATE_FORMAT(
                                   CONCAT('2021-',month_index+1,'-01')
                                   , '%Y-%m-%d'
                                 ), '%b'
                               ));
    END IF;

    SET month_index = month_index + 1;
  END WHILE;

  RETURN month_names;
END */;;

/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;;
# Dump of FUNCTION GetWeekdaysFromValue
# ------------------------------------------------------------

/*!50003 DROP FUNCTION IF EXISTS `GetWeekdaysFromValue` */;;
/*!50003 CREATE*/ /*!50020 DEFINER=`plr_clients`@`%`*/ /*!50003 FUNCTION `GetWeekdaysFromValue`(weekdays INT) RETURNS varchar(255) CHARSET utf8mb3 COLLATE utf8mb3_general_ci
    DETERMINISTIC
BEGIN
  DECLARE weekday_names VARCHAR(255);
  DECLARE weekday_index INT DEFAULT 0;

  SET weekday_names = '';

  WHILE weekdays > 0 AND weekday_index < 7 DO
    IF weekdays & (1 << weekday_index) > 0 THEN
      SET weekday_names = CONCAT(weekday_names, IF(weekday_names = '', '', ', '),
                               DATE_FORMAT(
                                 DATE_FORMAT(
                                   CONCAT('2023-05-',weekday_index+1)
                                   , '%Y-%m-%d'
                                 ), '%a'
                               ));
    END IF;

    SET weekday_index = weekday_index + 1;
  END WHILE;

  RETURN weekday_names;
END */;;

/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;;
DELIMITER ;

/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;