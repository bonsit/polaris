#!/bin/bash

# configuration
plrprod_host=oogopzorg.net
plrtest_host=test1.oogopzorg.net
plr_port=33062
plr_db=polaris

pznprod_host=oogopzorg.net
pzntest_host=test1.oogopzorg.net
pzn_port=33063
pzn_db=admin_pzn

PS3='Welke database wilt u overzetten naar de Testomgeving?: '
options=("Polaris database" "PZN database" "Beide" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "Polaris database")
            choice="plr"
            break
            ;;
        "PZN database")
            choice="pzn"
            break
            ;;
        "Beide")
            choice="beide"
            break
            ;;
        "Quit")
            break
            ;;
        *) echo invalid option;;
    esac
done

cfg_parser ()
{
    ini="$(<$1)"                # read the file
    ini="${ini//[/\[}"          # escape [
    ini="${ini//]/\]}"          # escape ]
    IFS=$'\n' && ini=( ${ini} ) # convert to line-array
    ini=( ${ini[*]//;*/} )      # remove comments with ;
    ini=( ${ini[*]/\    =/=} )  # remove tabs before =
    ini=( ${ini[*]/=\   /=} )   # remove tabs be =
    ini=( ${ini[*]/\ =\ /=} )   # remove anything with a space around =
    ini=( ${ini[*]/#\\[/\}$'\n'cfg.section.} ) # set section prefix
    ini=( ${ini[*]/%\\]/ \(} )    # convert text2function (1)
    ini=( ${ini[*]/=/=\( } )    # convert item to array
    ini=( ${ini[*]/%/ \)} )     # close array parenthesis
    ini=( ${ini[*]/%\\ \)/ \\} ) # the multiline trick
    ini=( ${ini[*]/%\( \)/\(\) \{} ) # convert text2function (2)
    ini=( ${ini[*]/%\} \)/\}} ) # remove extra parenthesis
    ini[0]="" # remove first element
    ini[${#ini[*]} + 1]='}'    # add the last brace
    eval "$(echo "${ini[*]}")" # eval the result
}

if [ -n $choice ]
then
    if [ $choice = "plr" ] || [ $choice = "beide" ]
    then
        # Polaris db
        mysqldump --defaults-file=../conf/mysql/login.cnf -h $plrprod_host --port $plr_port $plr_db > ./backup/plr_dump.sql
        mysql --defaults-file=../conf/mysql/login.cnf -h $plrtest_host --port $plr_port -e "CREATE DATABASE IF NOT EXISTS $plr_db;"
        mysql --defaults-file=../conf/mysql/login.cnf -h $plrtest_host --port $plr_port --database=$plr_db < backup/plr_dump.sql
        cfg_parser '../conf/mysql/login.cnf'
        cfg.section.client
        mysql --defaults-file=../conf/mysql/login.cnf -h $plrtest_host --port $plr_port --database=$plr_db -e "UPDATE plr_database SET ROOTPASSWORD = encode('$password', 'env.POLARIS_HASH') where clientid = 2 and databaseid = 1;"
    fi
    if [ $choice = "pzn" ] || [ $choice = "beide" ]
    then
        # PZN db
        mysqldump --defaults-file=../conf/mysql/login.cnf -h $pznprod_host --port $pzn_port $pzn_db > ./backup/pzn_dump.sql
        mysql --defaults-file=../conf/mysql/login.cnf -h $pzntest_host --port $pzn_port -e "CREATE DATABASE IF NOT EXISTS $pzn_db;"
        mysql --defaults-file=../conf/mysql/login.cnf -h $pzntest_host --port $pzn_port --database=$pzn_db < backup/pzn_dump.sql
    fi
fi