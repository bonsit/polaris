#!/bin/bash
docker-compose exec db-clients mysqldump --defaults-extra-file=/etc/mysql/conf.d/login.cnf --all-databases > ./docker-initdb-clients.d/plrclients_dump.sql