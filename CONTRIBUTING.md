# Contributing to Polaris

Thank you for considering contributing to Polaris! We appreciate your time and effort in making our SaaS application even better. This document provides guidelines and information on how to contribute to Polaris effectively.

## Table of Contents

- Getting Started
- Reporting Issues
- Feature Requests
- Submitting Pull Requests
- Coding Guidelines
- Font management
- Code of Conduct

## Getting Started

To get started with contributing to Polaris, please follow these steps:

1. Fork the repository on Bitbucket. https://bitbucket.org/bonsit1/polaris/
1. Clone your forked repository to your local development environment.
1. Install the necessary dependencies as specified in the project's README file.
1. Create a new branch for your contribution.
1. Make your changes or additions.
1. Test your changes thoroughly to ensure they don't introduce any regressions.
1. Commit your changes and push them to your forked repository.
1. Submit a pull request to the main repository, detailing the changes you made and the problem they address.

## Reporting Issues

If you encounter any issues or bugs while using Polaris, we appreciate your help in improving the application. To report an issue, please follow these steps:

1. Go to the [Issues](https://bitbucket.org/bonsit1/polaris/issues) section of the Bitbucket repository.
1. Click on the "Create" button.
1. Provide a clear and descriptive title for the issue.
Describe the steps to reproduce the issue, including any relevant information such as the browser version or operating 1. system.
1. Include any error messages or screenshots that can help identify and understand the issue.
1. Assign appropriate labels to the issue, such as "bug" or "enhancement," if applicable.
1. Submit the issue and wait for further instructions or responses from the project maintainers.

## Feature Requests

If you have an idea for a new feature or an enhancement to Polaris, we encourage you to submit a feature request. To submit a feature request, please follow these steps:

1. Go to the Issues section of the Bitbucket repository.
1. Click on the "Create" button.
1. Provide a clear and descriptive title for the feature request.
1. Describe the feature or enhancement in detail, explaining its benefits and use cases.
1. Include any relevant information or examples that can help clarify the feature request.
1. Assign appropriate labels to the feature request, such as "feature" or "enhancement," if applicable.
1. Submit the feature request and wait for further instructions or responses from the project maintainers.

## Submitting Pull Requests

If you have made changes or additions to Polaris and would like to contribute them back to the main repository, please follow these steps:

1. Ensure that your forked repository is up to date with the latest changes from the main repository.
1. Create a new branch for your pull request.
1. Make your changes or additions, ensuring they follow the project's coding guidelines.
1. Test your changes thoroughly to ensure they don't introduce any regressions.
1. Commit your changes and push them to your forked repository.
1. Submit a pull request to the main repository, detailing the changes you made and the problem they address.
1. Wait for feedback or further instructions from the project maintainers.

## Coding Guidelines

To maintain a consistent codebase, we follow a set of coding guidelines for Polaris. Please ensure that your code adheres to these guidelines before submitting a pull request. The guidelines may include:

1. Coding style (e.g., indentation, line length)
1. Naming conventions for variables, functions, and classes
1. Documentation requirements
1. Testing guidelines

Please refer to the project's documentation or coding guidelines for specific details.

## Downloading Fonts from Google Webfonts Helper

To host the fonts used in Polaris locally on the webserver, we will utilize Google Webfonts Helper. This allows us to download the fonts and include them in our application's assets. By hosting the fonts locally, we reduce reliance on external services like fonts.google.com and improve performance and privacy.

Follow these steps to download and include fonts from Google Webfonts Helper:

1. Visit the Google Webfonts Helper website. https://gwfh.mranftl.com/fonts
1. Use the search or browse through the font collection to find the desired fonts for Polaris.
1. Select the fonts you want to download by clicking on them. You can choose multiple fonts.
1. Customize the font settings as per your requirements, such as font variants, font weights, and character sets.
1. Once you have made your font selections and customized the settings, click on the "Download" button to initiate the font download.
1. Extract the downloaded font archive and copy the font files (e.g., .woff, .woff2) to the appropriate location in the Polaris application's resources/fonts directory.
2. The Gruntfile will automatically copy all the fonts in the resource/fonts directory and place them into the assets/css folder.
1. Create a css for each font and reference the downloaded font files using relative paths.

For example, if you place the font files in the resource/fonts directory, your CSS/SCSS file could include the following:

```css
/* bree-serif-regular - latin */
@font-face {
  font-display: swap;
  font-family: 'Bree Serif';
  font-style: normal;
  font-weight: 400;
  src: url('../css/bree-serif-v17-latin/bree-serif-v17-latin-regular.woff2') format('woff2');
}
```
1. Add the css file to the cssmin section of the Gruntfile, like so:
```js
        cssmin: {
            sitecss: {
                options: {
                    banner: ''
                },
                files: {
                    'public/assets/dist/main.min.css': [
                    	...
                        'resources/css/your-font.css',
                        ...
                    ]
                }
            }

```


## Code of Conduct

We expect all contributors to Polaris to adhere to our Code of Conduct. Please read and understand the code of conduct before participating in the project. If you encounter any violations or have concerns regarding conduct, please report them to the project maintainers.

Thank you for your contributions and helping make Polaris better!

